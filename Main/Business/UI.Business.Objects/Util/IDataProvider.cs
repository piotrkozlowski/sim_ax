﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Data;

namespace IMR.Suite.UI.Business.Objects
{
    public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region Action

        /// <summary>
        /// Indicates whether the Action is cached.
        /// </summary>
        bool ActionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Action objects
        /// </summary>
        List<OpAction> GetAllAction();

        /// <summary>
        /// Gets Action by id
        /// </summary>
        /// <param name="Id">Action Id</param>
        /// <returns>Action object</returns>
        OpAction GetAction(long Id);

        /// <summary>
        /// Gets Action by id
        /// </summary>
        /// <param name="Ids">Action Ids</param>
        /// <returns>Action list</returns>
        List<OpAction> GetAction(long[] Ids);

        /// <summary>
        /// Gets Action by id
        /// </summary>
        /// <param name="Id">Action Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Action object</returns>
        OpAction GetAction(long Id, bool queryDatabase);

        /// <summary>
        /// Deletes the Action object from the database
        /// </summary>
        /// <param name="toBeDeleted">Action to delete</param>
        void DeleteAction(OpAction toBeDeleted);

        /// <summary>
        /// Clears the Action object cache
        /// </summary>
        void ClearActionCache();
        #endregion

        #region ActionDef        

        /// <summary>
        /// Indicates whether the ActionDef is cached.
        /// </summary>
        bool ActionDefCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionDef objects
        /// </summary>
        List<OpActionDef> GetAllActionDef();


        /// <summary>
        /// Gets ActionDef by id
        /// </summary>
        /// <param name="Id">ActionDef Id</param>
        /// <returns>ActionDef object</returns>
        OpActionDef GetActionDef(long Id);

        /// <summary>
        /// Gets ActionDef by id
        /// </summary>
        /// <param name="Ids">ActionDef Ids</param>
        /// <returns>ActionDef list</returns>
        List<OpActionDef> GetActionDef(long[] Ids);

        /// <summary>
        /// Gets ActionDef by id
        /// </summary>
        /// <param name="Id">ActionDef Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionDef object</returns>
        OpActionDef GetActionDef(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionDef by id
        /// </summary>
        /// <param name="Ids">ActionDef Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionDef list</returns>
        List<OpActionDef> GetActionDef(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the ActionDef object cache
        /// </summary>
        void ClearActionDefCache();
        #endregion

        #region ActionHistory        

        /// <summary>
        /// Indicates whether the ActionHistory is cached.
        /// </summary>
        bool ActionHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionHistory objects
        /// </summary>
        List<OpActionHistory> GetAllActionHistory();


        /// <summary>
        /// Gets ActionHistory by id
        /// </summary>
        /// <param name="Id">ActionHistory Id</param>
        /// <returns>ActionHistory object</returns>
        OpActionHistory GetActionHistory(long Id);

        /// <summary>
        /// Gets ActionHistory by id
        /// </summary>
        /// <param name="Ids">ActionHistory Ids</param>
        /// <returns>ActionHistory list</returns>
        List<OpActionHistory> GetActionHistory(long[] Ids);

        /// <summary>
        /// Gets ActionHistory by id
        /// </summary>
        /// <param name="Id">ActionHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionHistory object</returns>
        OpActionHistory GetActionHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionHistory by id
        /// </summary>
        /// <param name="Ids">ActionHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionHistory list</returns>
        List<OpActionHistory> GetActionHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionHistory to save</param>
        /// <returns>ActionHistory Id</returns>
        long SaveActionHistory(OpActionHistory toBeSaved);

        /// <summary>
        /// Deletes the ActionHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionHistory to delete</param>
        void DeleteActionHistory(OpActionHistory toBeDeleted);

        /// <summary>
        /// Clears the ActionHistory object cache
        /// </summary>
        void ClearActionHistoryCache();
        #endregion

        #region ActionSmsMessage

        /// <summary>
        /// Indicates whether the ActionSmsMessage is cached.
        /// </summary>
        bool ActionSmsMessageCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionSmsMessage objects
        /// </summary>
        List<OpActionSmsMessage> GetAllActionSmsMessage();


        /// <summary>
        /// Gets ActionSmsMessage by id
        /// </summary>
        /// <param name="Id">ActionSmsMessage Id</param>
        /// <returns>ActionSmsMessage object</returns>
        OpActionSmsMessage GetActionSmsMessage(long Id);

        /// <summary>
        /// Gets ActionSmsMessage by id
        /// </summary>
        /// <param name="Ids">ActionSmsMessage Ids</param>
        /// <returns>ActionSmsMessage list</returns>
        List<OpActionSmsMessage> GetActionSmsMessage(long[] Ids);

        /// <summary>
        /// Gets ActionSmsMessage by id
        /// </summary>
        /// <param name="Id">ActionSmsMessage Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionSmsMessage object</returns>
        OpActionSmsMessage GetActionSmsMessage(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionSmsMessage by id
        /// </summary>
        /// <param name="Ids">ActionSmsMessage Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionSmsMessage list</returns>
        List<OpActionSmsMessage> GetActionSmsMessage(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionSmsMessage object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionSmsMessage to save</param>
        /// <returns>ActionSmsMessage Id</returns>
        long SaveActionSmsMessage(OpActionSmsMessage toBeSaved);

        /// <summary>
        /// Deletes the ActionSmsMessage object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionSmsMessage to delete</param>
        void DeleteActionSmsMessage(OpActionSmsMessage toBeDeleted);

        /// <summary>
        /// Clears the ActionSmsMessage object cache
        /// </summary>
        void ClearActionSmsMessageCache();
        #endregion

        #region ActionSmsText

        /// <summary>
        /// Indicates whether the ActionSmsText is cached.
        /// </summary>
        bool ActionSmsTextCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionSmsText objects
        /// </summary>
        List<OpActionSmsText> GetAllActionSmsText();

        /// <summary>
        /// Gets ActionSmsText by id
        /// </summary>
        /// <param name="Id">ActionSmsText Id</param>
        /// <returns>ActionSmsText object</returns>
        OpActionSmsText GetActionSmsText(long Id);

        /// <summary>
        /// Gets ActionSmsText by id
        /// </summary>
        /// <param name="Ids">ActionSmsText Ids</param>
        /// <returns>ActionSmsText list</returns>
        List<OpActionSmsText> GetActionSmsText(long[] Ids);

        /// <summary>
        /// Gets ActionSmsText by id
        /// </summary>
        /// <param name="Id">ActionSmsText Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionSmsText object</returns>
        OpActionSmsText GetActionSmsText(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionSmsText by id
        /// </summary>
        /// <param name="Ids">ActionSmsText Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionSmsText list</returns>
        List<OpActionSmsText> GetActionSmsText(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionSmsText object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionSmsText to save</param>
        /// <returns>ActionSmsText Id</returns>
        long SaveActionSmsText(OpActionSmsText toBeSaved);

        /// <summary>
        /// Deletes the ActionSmsText object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionSmsText to delete</param>
        void DeleteActionSmsText(OpActionSmsText toBeDeleted);

        /// <summary>
        /// Clears the ActionSmsText object cache
        /// </summary>
        void ClearActionSmsTextCache();
        #endregion

        #region ActionSmsTextData

        /// <summary>
        /// Indicates whether the ActionSmsTextData is cached.
        /// </summary>
        bool ActionSmsTextDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionSmsTextData objects
        /// </summary>
        List<OpActionSmsTextData> GetAllActionSmsTextData();


        /// <summary>
        /// Gets ActionSmsTextData by id
        /// </summary>
        /// <param name="Id">ActionSmsTextData Id</param>
        /// <returns>ActionSmsTextData object</returns>
        OpActionSmsTextData GetActionSmsTextData(long Id);

        /// <summary>
        /// Gets ActionSmsTextData by id
        /// </summary>
        /// <param name="Ids">ActionSmsTextData Ids</param>
        /// <returns>ActionSmsTextData list</returns>
        List<OpActionSmsTextData> GetActionSmsTextData(long[] Ids);

        /// <summary>
        /// Gets ActionSmsTextData by id
        /// </summary>
        /// <param name="Id">ActionSmsTextData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionSmsTextData object</returns>
        OpActionSmsTextData GetActionSmsTextData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionSmsTextData by id
        /// </summary>
        /// <param name="Ids">ActionSmsTextData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionSmsTextData list</returns>
        List<OpActionSmsTextData> GetActionSmsTextData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionSmsTextData object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionSmsTextData to save</param>
        /// <returns>ActionSmsTextData Id</returns>
        long SaveActionSmsTextData(OpActionSmsTextData toBeSaved);

        /// <summary>
        /// Deletes the ActionSmsTextData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionSmsTextData to delete</param>
        void DeleteActionSmsTextData(OpActionSmsTextData toBeDeleted);

        /// <summary>
        /// Clears the ActionSmsTextData object cache
        /// </summary>
        void ClearActionSmsTextDataCache();
        #endregion

        #region ActionStatus

        /// <summary>
        /// Indicates whether the ActionStatus is cached.
        /// </summary>
        bool ActionStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionStatus objects
        /// </summary>
        List<OpActionStatus> GetAllActionStatus();

        /// <summary>
        /// Gets ActionStatus by id
        /// </summary>
        /// <param name="Id">ActionStatus Id</param>
        /// <returns>ActionStatus object</returns>
        OpActionStatus GetActionStatus(int Id);

        /// <summary>
        /// Gets ActionStatus by id
        /// </summary>
        /// <param name="Ids">ActionStatus Ids</param>
        /// <returns>ActionStatus list</returns>
        List<OpActionStatus> GetActionStatus(int[] Ids);

        /// <summary>
        /// Gets ActionStatus by id
        /// </summary>
        /// <param name="Id">ActionStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionStatus object</returns>
        OpActionStatus GetActionStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionStatus by id
        /// </summary>
        /// <param name="Ids">ActionStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionStatus list</returns>
        List<OpActionStatus> GetActionStatus(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ActionStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionStatus to save</param>
        /// <returns>ActionStatus Id</returns>
        int SaveActionStatus(OpActionStatus toBeSaved);

        /// <summary>
        /// Deletes the ActionStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionStatus to delete</param>
        void DeleteActionStatus(OpActionStatus toBeDeleted);

        /// <summary>
        /// Clears the ActionStatus object cache
        /// </summary>
        void ClearActionStatusCache();
        #endregion

        #region ActionType

        /// <summary>
        /// Indicates whether the ActionType is cached.
        /// </summary>
        bool ActionTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets ActionType by id
        /// </summary>
        /// <param name="Id">ActionType Id</param>
        /// <returns>ActionType object</returns>
        OpActionType GetActionType(int Id);

        /// <summary>
        /// Gets ActionType by id
        /// </summary>
        /// <param name="Ids">ActionType Ids</param>
        /// <returns>ActionType list</returns>
        List<OpActionType> GetActionType(int[] Ids);

        /// <summary>
        /// Gets ActionType by id
        /// </summary>
        /// <param name="Id">ActionType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionType object</returns>
        OpActionType GetActionType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionType by id
        /// </summary>
        /// <param name="Ids">ActionType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionType list</returns>
        List<OpActionType> GetActionType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ActionType object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionType to save</param>
        /// <returns>ActionType Id</returns>
        int SaveActionType(OpActionType toBeSaved);

        /// <summary>
        /// Deletes the ActionType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionType to delete</param>
        void DeleteActionType(OpActionType toBeDeleted);

        /// <summary>
        /// Clears the ActionType object cache
        /// </summary>
        void ClearActionTypeCache();
        #endregion

        #region ActionTypeClass

        /// <summary>
        /// Indicates whether the ActionTypeClass is cached.
        /// </summary>
        bool ActionTypeClassCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionTypeClass objects
        /// </summary>
        List<OpActionTypeClass> GetAllActionTypeClass();


        /// <summary>
        /// Gets ActionTypeClass by id
        /// </summary>
        /// <param name="Id">ActionTypeClass Id</param>
        /// <returns>ActionTypeClass object</returns>
        OpActionTypeClass GetActionTypeClass(int Id);

        /// <summary>
        /// Gets ActionTypeClass by id
        /// </summary>
        /// <param name="Ids">ActionTypeClass Ids</param>
        /// <returns>ActionTypeClass list</returns>
        List<OpActionTypeClass> GetActionTypeClass(int[] Ids);

        /// <summary>
        /// Gets ActionTypeClass by id
        /// </summary>
        /// <param name="Id">ActionTypeClass Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeClass object</returns>
        OpActionTypeClass GetActionTypeClass(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionTypeClass by id
        /// </summary>
        /// <param name="Ids">ActionTypeClass Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeClass list</returns>
        List<OpActionTypeClass> GetActionTypeClass(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ActionTypeClass object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionTypeClass to save</param>
        /// <returns>ActionTypeClass Id</returns>
        int SaveActionTypeClass(OpActionTypeClass toBeSaved);

        /// <summary>
        /// Deletes the ActionTypeClass object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionTypeClass to delete</param>
        void DeleteActionTypeClass(OpActionTypeClass toBeDeleted);

        /// <summary>
        /// Clears the ActionTypeClass object cache
        /// </summary>
        void ClearActionTypeClassCache();
        #endregion

        #region ActionTypeGroup

        /// <summary>
        /// Indicates whether the ActionTypeGroup is cached.
        /// </summary>
        bool ActionTypeGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionTypeGroup objects
        /// </summary>
        List<OpActionTypeGroup> GetAllActionTypeGroup();


        /// <summary>
        /// Gets ActionTypeGroup by id
        /// </summary>
        /// <param name="Id">ActionTypeGroup Id</param>
        /// <returns>ActionTypeGroup object</returns>
        OpActionTypeGroup GetActionTypeGroup(int Id);

        /// <summary>
        /// Gets ActionTypeGroup by id
        /// </summary>
        /// <param name="Ids">ActionTypeGroup Ids</param>
        /// <returns>ActionTypeGroup list</returns>
        List<OpActionTypeGroup> GetActionTypeGroup(int[] Ids);

        /// <summary>
        /// Gets ActionTypeGroup by id
        /// </summary>
        /// <param name="Id">ActionTypeGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeGroup object</returns>
        OpActionTypeGroup GetActionTypeGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionTypeGroup by id
        /// </summary>
        /// <param name="Ids">ActionTypeGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeGroup list</returns>
        List<OpActionTypeGroup> GetActionTypeGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionTypeGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionTypeGroup to save</param>
        /// <returns>ActionTypeGroup Id</returns>
        int SaveActionTypeGroup(OpActionTypeGroup toBeSaved);

        /// <summary>
        /// Deletes the ActionTypeGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionTypeGroup to delete</param>
        void DeleteActionTypeGroup(OpActionTypeGroup toBeDeleted);

        /// <summary>
        /// Clears the ActionTypeGroup object cache
        /// </summary>
        void ClearActionTypeGroupCache();
        #endregion

        #region ActionTypeGroupType

        /// <summary>
        /// Indicates whether the ActionTypeGroupType is cached.
        /// </summary>
        bool ActionTypeGroupTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionTypeGroupType objects
        /// </summary>
        List<OpActionTypeGroupType> GetAllActionTypeGroupType();


        /// <summary>
        /// Gets ActionTypeGroupType by id
        /// </summary>
        /// <param name="Id">ActionTypeGroupType Id</param>
        /// <returns>ActionTypeGroupType object</returns>
        OpActionTypeGroupType GetActionTypeGroupType(int Id);

        /// <summary>
        /// Gets ActionTypeGroupType by id
        /// </summary>
        /// <param name="Ids">ActionTypeGroupType Ids</param>
        /// <returns>ActionTypeGroupType list</returns>
        List<OpActionTypeGroupType> GetActionTypeGroupType(int[] Ids);

        /// <summary>
        /// Gets ActionTypeGroupType by id
        /// </summary>
        /// <param name="Id">ActionTypeGroupType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeGroupType object</returns>
        OpActionTypeGroupType GetActionTypeGroupType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionTypeGroupType by id
        /// </summary>
        /// <param name="Ids">ActionTypeGroupType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeGroupType list</returns>
        List<OpActionTypeGroupType> GetActionTypeGroupType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionTypeGroupType object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionTypeGroupType to save</param>
        /// <returns>ActionTypeGroupType Id</returns>
        int SaveActionTypeGroupType(OpActionTypeGroupType toBeSaved);

        /// <summary>
        /// Deletes the ActionTypeGroupType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionTypeGroupType to delete</param>
        void DeleteActionTypeGroupType(OpActionTypeGroupType toBeDeleted);

        /// <summary>
        /// Clears the ActionTypeGroupType object cache
        /// </summary>
        void ClearActionTypeGroupTypeCache();
        #endregion

        #region ActionTypeInGroup

        /// <summary>
        /// Indicates whether the ActionTypeInGroup is cached.
        /// </summary>
        bool ActionTypeInGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionTypeInGroup objects
        /// </summary>
        List<OpActionTypeInGroup> GetAllActionTypeInGroup();


        /// <summary>
        /// Gets ActionTypeInGroup by id
        /// </summary>
        /// <param name="Id">ActionTypeInGroup Id</param>
        /// <returns>ActionTypeInGroup object</returns>
        OpActionTypeInGroup GetActionTypeInGroup(int Id);

        /// <summary>
        /// Gets ActionTypeInGroup by id
        /// </summary>
        /// <param name="Ids">ActionTypeInGroup Ids</param>
        /// <returns>ActionTypeInGroup list</returns>
        List<OpActionTypeInGroup> GetActionTypeInGroup(int[] Ids);

        /// <summary>
        /// Gets ActionTypeInGroup by id
        /// </summary>
        /// <param name="Id">ActionTypeInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeInGroup object</returns>
        OpActionTypeInGroup GetActionTypeInGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionTypeInGroup by id
        /// </summary>
        /// <param name="Ids">ActionTypeInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionTypeInGroup list</returns>
        List<OpActionTypeInGroup> GetActionTypeInGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionTypeInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionTypeInGroup to save</param>
        /// <returns>ActionTypeInGroup Id</returns>
        int SaveActionTypeInGroup(OpActionTypeInGroup toBeSaved);

        /// <summary>
        /// Deletes the ActionTypeInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionTypeInGroup to delete</param>
        void DeleteActionTypeInGroup(OpActionTypeInGroup toBeDeleted);

        /// <summary>
        /// Clears the ActionTypeInGroup object cache
        /// </summary>
        void ClearActionTypeInGroupCache();
        #endregion

        #region Activity

        /// <summary>
        /// Indicates whether the Activity is cached.
        /// </summary>
        bool ActivityCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Activity objects
        /// </summary>
        List<OpActivity> GetAllActivity();


        /// <summary>
        /// Gets Activity by id
        /// </summary>
        /// <param name="Id">Activity Id</param>
        /// <returns>Activity object</returns>
        OpActivity GetActivity(int Id);

        /// <summary>
        /// Gets Activity by id
        /// </summary>
        /// <param name="Ids">Activity Ids</param>
        /// <returns>Activity list</returns>
        List<OpActivity> GetActivity(int[] Ids);

        /// <summary>
        /// Gets Activity by id
        /// </summary>
        /// <param name="Id">Activity Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Activity object</returns>
        OpActivity GetActivity(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Activity by id
        /// </summary>
        /// <param name="Ids">Activity Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Activity list</returns>
        List<OpActivity> GetActivity(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Deletes the Activity object from the database
        /// </summary>
        /// <param name="toBeDeleted">Activity to delete</param>
        void DeleteActivity(OpActivity toBeDeleted);

        /// <summary>
        /// Clears the Activity object cache
        /// </summary>
        void ClearActivityCache();
        #endregion

        #region Actor

        /// <summary>
        /// Indicates whether the Actor is cached.
        /// </summary>
        bool ActorCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Actor objects
        /// </summary>
        List<OpActor> GetAllActor(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Actor by id
        /// </summary>
        /// <param name="Id">Actor Id</param>
        /// <returns>Actor object</returns>
        OpActor GetActor(int Id);

        /// <summary>
        /// Gets Actor by id
        /// </summary>
        /// <param name="Ids">Actor Ids</param>
        /// <returns>Actor list</returns>
        List<OpActor> GetActor(int[] Ids);

        /// <summary>
        /// Gets Actor by id
        /// </summary>
        /// <param name="Id">Actor Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Actor object</returns>
        OpActor GetActor(int Id, bool queryDatabase);

        /// <summary>
        /// Saves the Actor object to the database
        /// </summary>
        /// <param name="toBeSaved">Actor to save</param>
        /// <returns>Actor Id</returns>
        int SaveActor(OpActor toBeSaved);

        /// <summary>
        /// Deletes the Actor object from the database
        /// </summary>
        /// <param name="toBeDeleted">Actor to delete</param>
        void DeleteActor(OpActor toBeDeleted);

        /// <summary>
        /// Clears the Actor object cache
        /// </summary>
        void ClearActorCache();
        #endregion

        #region ActorGroup

        /// <summary>
        /// Indicates whether the ActorGroup is cached.
        /// </summary>
        bool ActorGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActorGroup objects
        /// </summary>
        List<OpActorGroup> GetAllActorGroup();


        /// <summary>
        /// Gets ActorGroup by id
        /// </summary>
        /// <param name="Id">ActorGroup Id</param>
        /// <returns>ActorGroup object</returns>
        OpActorGroup GetActorGroup(int Id);

        /// <summary>
        /// Gets ActorGroup by id
        /// </summary>
        /// <param name="Ids">ActorGroup Ids</param>
        /// <returns>ActorGroup list</returns>
        List<OpActorGroup> GetActorGroup(int[] Ids);

        /// <summary>
        /// Gets ActorGroup by id
        /// </summary>
        /// <param name="Id">ActorGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActorGroup object</returns>
        OpActorGroup GetActorGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActorGroup by id
        /// </summary>
        /// <param name="Ids">ActorGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActorGroup list</returns>
        List<OpActorGroup> GetActorGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActorGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">ActorGroup to save</param>
        /// <returns>ActorGroup Id</returns>
        int SaveActorGroup(OpActorGroup toBeSaved);

        /// <summary>
        /// Deletes the ActorGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActorGroup to delete</param>
        void DeleteActorGroup(OpActorGroup toBeDeleted);

        /// <summary>
        /// Clears the ActorGroup object cache
        /// </summary>
        void ClearActorGroupCache();
        #endregion

        #region Alarm

        /// <summary>
        /// Indicates whether the Alarm is cached.
        /// </summary>
        bool AlarmCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Alarm objects
        /// </summary>
        List<OpAlarm> GetAllAlarm();

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Id">Alarm Id</param>
        /// <returns>Alarm object</returns>
        OpAlarm GetAlarm(long Id);

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Ids">Alarm Ids</param>
        /// <returns>Alarm list</returns>
        List<OpAlarm> GetAlarm(long[] Ids);

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Id">Alarm Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Alarm object</returns>
        OpAlarm GetAlarm(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Ids">Alarm Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Alarm list</returns>
        List<OpAlarm> GetAlarm(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Alarm object to the database
        /// </summary>
        /// <param name="toBeSaved">Alarm to save</param>
        /// <returns>Alarm Id</returns>
        long SaveAlarm(OpAlarm toBeSaved);

        /// <summary>
        /// Deletes the Alarm object from the database
        /// </summary>
        /// <param name="toBeDeleted">Alarm to delete</param>
        void DeleteAlarm(OpAlarm toBeDeleted);

        /// <summary>
        /// Clears the Alarm object cache
        /// </summary>
        void ClearAlarmCache();
        #endregion

        #region AlarmData

        /// <summary>
        /// Indicates whether the AlarmData is cached.
        /// </summary>
        bool AlarmDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmData objects
        /// </summary>
        List<OpAlarmData> GetAllAlarmData();


        /// <summary>
        /// Gets AlarmData by id
        /// </summary>
        /// <param name="Id">AlarmData Id</param>
        /// <returns>AlarmData object</returns>
        OpAlarmData GetAlarmData(long Id);

        /// <summary>
        /// Gets AlarmData by id
        /// </summary>
        /// <param name="Ids">AlarmData Ids</param>
        /// <returns>AlarmData list</returns>
        List<OpAlarmData> GetAlarmData(long[] Ids);

        /// <summary>
        /// Gets AlarmData by id
        /// </summary>
        /// <param name="Id">AlarmData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmData object</returns>
        OpAlarmData GetAlarmData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmData by id
        /// </summary>
        /// <param name="Ids">AlarmData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmData list</returns>
        List<OpAlarmData> GetAlarmData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmData object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmData to save</param>
        /// <returns>AlarmData Id</returns>
        long SaveAlarmData(OpAlarmData toBeSaved);

        /// <summary>
        /// Deletes the AlarmData object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmData to delete</param>
        void DeleteAlarmData(OpAlarmData toBeDeleted);

        /// <summary>
        /// Clears the AlarmData object cache
        /// </summary>
        void ClearAlarmDataCache();
        #endregion

        #region AlarmDef

        /// <summary>
        /// Indicates whether the AlarmDef is cached.
        /// </summary>
        bool AlarmDefCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmDef objects
        /// </summary>
        List<OpAlarmDef> GetAllAlarmDef();


        /// <summary>
        /// Gets AlarmDef by id
        /// </summary>
        /// <param name="Id">AlarmDef Id</param>
        /// <returns>AlarmDef object</returns>
        OpAlarmDef GetAlarmDef(long Id);

        /// <summary>
        /// Gets AlarmDef by id
        /// </summary>
        /// <param name="Ids">AlarmDef Ids</param>
        /// <returns>AlarmDef list</returns>
        List<OpAlarmDef> GetAlarmDef(long[] Ids);

        /// <summary>
        /// Gets AlarmDef by id
        /// </summary>
        /// <param name="Id">AlarmDef Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmDef object</returns>
        OpAlarmDef GetAlarmDef(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmDef by id
        /// </summary>
        /// <param name="Ids">AlarmDef Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmDef list</returns>
        List<OpAlarmDef> GetAlarmDef(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmDef object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmDef to save</param>
        /// <returns>AlarmDef Id</returns>
        long SaveAlarmDef(OpAlarmDef toBeSaved);

        /// <summary>
        /// Deletes the AlarmDef object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmDef to delete</param>
        void DeleteAlarmDef(OpAlarmDef toBeDeleted);

        /// <summary>
        /// Clears the AlarmDef object cache
        /// </summary>
        void ClearAlarmDefCache();
        #endregion

        #region AlarmDefGroup

        /// <summary>
        /// Indicates whether the AlarmDefGroup is cached.
        /// </summary>
        bool AlarmDefGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmDefGroup objects
        /// </summary>
        List<OpAlarmDefGroup> GetAllAlarmDefGroup();


        /// <summary>
        /// Gets AlarmDefGroup by id
        /// </summary>
        /// <param name="Id">AlarmDefGroup Id</param>
        /// <returns>AlarmDefGroup object</returns>
        OpAlarmDefGroup GetAlarmDefGroup(long Id);

        /// <summary>
        /// Gets AlarmDefGroup by id
        /// </summary>
        /// <param name="Ids">AlarmDefGroup Ids</param>
        /// <returns>AlarmDefGroup list</returns>
        List<OpAlarmDefGroup> GetAlarmDefGroup(long[] Ids);

        /// <summary>
        /// Gets AlarmDefGroup by id
        /// </summary>
        /// <param name="Id">AlarmDefGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmDefGroup object</returns>
        OpAlarmDefGroup GetAlarmDefGroup(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmDefGroup by id
        /// </summary>
        /// <param name="Ids">AlarmDefGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmDefGroup list</returns>
        List<OpAlarmDefGroup> GetAlarmDefGroup(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmDefGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmDefGroup to save</param>
        /// <returns>AlarmDefGroup Id</returns>
        long SaveAlarmDefGroup(OpAlarmDefGroup toBeSaved);

        /// <summary>
        /// Deletes the AlarmDefGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmDefGroup to delete</param>
        void DeleteAlarmDefGroup(OpAlarmDefGroup toBeDeleted);

        /// <summary>
        /// Clears the AlarmDefGroup object cache
        /// </summary>
        void ClearAlarmDefGroupCache();
        #endregion

        #region AlarmEvent

        /// <summary>
        /// Indicates whether the AlarmEvent is cached.
        /// </summary>
        bool AlarmEventCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmEvent objects
        /// </summary>
        List<OpAlarmEvent> GetAllAlarmEvent();


        /// <summary>
        /// Gets AlarmEvent by id
        /// </summary>
        /// <param name="Id">AlarmEvent Id</param>
        /// <returns>AlarmEvent object</returns>
        OpAlarmEvent GetAlarmEvent(long Id);

        /// <summary>
        /// Gets AlarmEvent by id
        /// </summary>
        /// <param name="Ids">AlarmEvent Ids</param>
        /// <returns>AlarmEvent list</returns>
        List<OpAlarmEvent> GetAlarmEvent(long[] Ids);

        /// <summary>
        /// Gets AlarmEvent by id
        /// </summary>
        /// <param name="Id">AlarmEvent Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmEvent object</returns>
        OpAlarmEvent GetAlarmEvent(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmEvent by id
        /// </summary>
        /// <param name="Ids">AlarmEvent Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmEvent list</returns>
        List<OpAlarmEvent> GetAlarmEvent(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmEvent object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmEvent to save</param>
        /// <returns>AlarmEvent Id</returns>
        long SaveAlarmEvent(OpAlarmEvent toBeSaved);

        /// <summary>
        /// Deletes the AlarmEvent object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmEvent to delete</param>
        void DeleteAlarmEvent(OpAlarmEvent toBeDeleted);

        /// <summary>
        /// Clears the AlarmEvent object cache
        /// </summary>
        void ClearAlarmEventCache();
        #endregion

        #region AlarmGroup

        /// <summary>
        /// Indicates whether the AlarmGroup is cached.
        /// </summary>
        bool AlarmGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmGroup objects
        /// </summary>
        List<OpAlarmGroup> GetAllAlarmGroup();


        /// <summary>
        /// Gets AlarmGroup by id
        /// </summary>
        /// <param name="Id">AlarmGroup Id</param>
        /// <returns>AlarmGroup object</returns>
        OpAlarmGroup GetAlarmGroup(int Id);

        /// <summary>
        /// Gets AlarmGroup by id
        /// </summary>
        /// <param name="Ids">AlarmGroup Ids</param>
        /// <returns>AlarmGroup list</returns>
        List<OpAlarmGroup> GetAlarmGroup(int[] Ids);

        /// <summary>
        /// Gets AlarmGroup by id
        /// </summary>
        /// <param name="Id">AlarmGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmGroup object</returns>
        OpAlarmGroup GetAlarmGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmGroup by id
        /// </summary>
        /// <param name="Ids">AlarmGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmGroup list</returns>
        List<OpAlarmGroup> GetAlarmGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmGroup to save</param>
        /// <returns>AlarmGroup Id</returns>
        int SaveAlarmGroup(OpAlarmGroup toBeSaved);

        /// <summary>
        /// Deletes the AlarmGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmGroup to delete</param>
        void DeleteAlarmGroup(OpAlarmGroup toBeDeleted);

        /// <summary>
        /// Clears the AlarmGroup object cache
        /// </summary>
        void ClearAlarmGroupCache();
        #endregion

        #region AlarmGroupOperator

        /// <summary>
        /// Indicates whether the AlarmGroupOperator is cached.
        /// </summary>
        bool AlarmGroupOperatorCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmGroupOperator objects
        /// </summary>
        List<OpAlarmGroupOperator> GetAllAlarmGroupOperator();


        /// <summary>
        /// Gets AlarmGroupOperator by id
        /// </summary>
        /// <param name="Id">AlarmGroupOperator Id</param>
        /// <returns>AlarmGroupOperator object</returns>
        OpAlarmGroupOperator GetAlarmGroupOperator(int Id);

        /// <summary>
        /// Gets AlarmGroupOperator by id
        /// </summary>
        /// <param name="Ids">AlarmGroupOperator Ids</param>
        /// <returns>AlarmGroupOperator list</returns>
        List<OpAlarmGroupOperator> GetAlarmGroupOperator(int[] Ids);

        /// <summary>
        /// Gets AlarmGroupOperator by id
        /// </summary>
        /// <param name="Id">AlarmGroupOperator Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmGroupOperator object</returns>
        OpAlarmGroupOperator GetAlarmGroupOperator(int Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmGroupOperator by id
        /// </summary>
        /// <param name="Ids">AlarmGroupOperator Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmGroupOperator list</returns>
        List<OpAlarmGroupOperator> GetAlarmGroupOperator(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmGroupOperator object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmGroupOperator to save</param>
        /// <returns>AlarmGroupOperator Id</returns>
        int SaveAlarmGroupOperator(OpAlarmGroupOperator toBeSaved);

        /// <summary>
        /// Deletes the AlarmGroupOperator object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmGroupOperator to delete</param>
        void DeleteAlarmGroupOperator(OpAlarmGroupOperator toBeDeleted);

        /// <summary>
        /// Clears the AlarmGroupOperator object cache
        /// </summary>
        void ClearAlarmGroupOperatorCache();
        #endregion

        #region AlarmHistory

        /// <summary>
        /// Indicates whether the AlarmHistory is cached.
        /// </summary>
        bool AlarmHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmHistory objects
        /// </summary>
        List<OpAlarmHistory> GetAllAlarmHistory();


        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Id">AlarmHistory Id</param>
        /// <returns>AlarmHistory object</returns>
        OpAlarmHistory GetAlarmHistory(long Id);

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Ids">AlarmHistory Ids</param>
        /// <returns>AlarmHistory list</returns>
        List<OpAlarmHistory> GetAlarmHistory(long[] Ids);

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Id">AlarmHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmHistory object</returns>
        OpAlarmHistory GetAlarmHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Ids">AlarmHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmHistory list</returns>
        List<OpAlarmHistory> GetAlarmHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmHistory to save</param>
        /// <returns>AlarmHistory Id</returns>
        long SaveAlarmHistory(OpAlarmHistory toBeSaved);

        /// <summary>
        /// Deletes the AlarmHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmHistory to delete</param>
        void DeleteAlarmHistory(OpAlarmHistory toBeDeleted);

        /// <summary>
        /// Clears the AlarmHistory object cache
        /// </summary>
        void ClearAlarmHistoryCache();
        #endregion

        #region AlarmMessage

        /// <summary>
        /// Indicates whether the AlarmMessage is cached.
        /// </summary>
        bool AlarmMessageCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmMessage objects
        /// </summary>
        List<OpAlarmMessage> GetAllAlarmMessage();


        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Id">AlarmMessage Id</param>
        /// <returns>AlarmMessage object</returns>
        OpAlarmMessage GetAlarmMessage(long Id);

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Ids">AlarmMessage Ids</param>
        /// <returns>AlarmMessage list</returns>
        List<OpAlarmMessage> GetAlarmMessage(long[] Ids);

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Id">AlarmMessage Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmMessage object</returns>
        OpAlarmMessage GetAlarmMessage(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Ids">AlarmMessage Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmMessage list</returns>
        List<OpAlarmMessage> GetAlarmMessage(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmMessage object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmMessage to save</param>
        /// <returns>AlarmMessage Id</returns>
        long SaveAlarmMessage(OpAlarmMessage toBeSaved);

        /// <summary>
        /// Deletes the AlarmMessage object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmMessage to delete</param>
        void DeleteAlarmMessage(OpAlarmMessage toBeDeleted);

        /// <summary>
        /// Clears the AlarmMessage object cache
        /// </summary>
        void ClearAlarmMessageCache();
        #endregion

        #region AlarmStatus

        /// <summary>
        /// Indicates whether the AlarmStatus is cached.
        /// </summary>
        bool AlarmStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmStatus objects
        /// </summary>
        List<OpAlarmStatus> GetAllAlarmStatus();


        /// <summary>
        /// Gets AlarmStatus by id
        /// </summary>
        /// <param name="Id">AlarmStatus Id</param>
        /// <returns>AlarmStatus object</returns>
        OpAlarmStatus GetAlarmStatus(int Id);

        /// <summary>
        /// Gets AlarmStatus by id
        /// </summary>
        /// <param name="Ids">AlarmStatus Ids</param>
        /// <returns>AlarmStatus list</returns>
        List<OpAlarmStatus> GetAlarmStatus(int[] Ids);

        /// <summary>
        /// Gets AlarmStatus by id
        /// </summary>
        /// <param name="Id">AlarmStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmStatus object</returns>
        OpAlarmStatus GetAlarmStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmStatus by id
        /// </summary>
        /// <param name="Ids">AlarmStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmStatus list</returns>
        List<OpAlarmStatus> GetAlarmStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmStatus to save</param>
        /// <returns>AlarmStatus Id</returns>
        int SaveAlarmStatus(OpAlarmStatus toBeSaved);

        /// <summary>
        /// Deletes the AlarmStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmStatus to delete</param>
        void DeleteAlarmStatus(OpAlarmStatus toBeDeleted);

        /// <summary>
        /// Clears the AlarmStatus object cache
        /// </summary>
        void ClearAlarmStatusCache();
        #endregion

        #region AlarmText

        /// <summary>
        /// Indicates whether the AlarmText is cached.
        /// </summary>
        bool AlarmTextCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmText objects
        /// </summary>
        List<OpAlarmText> GetAllAlarmText();


        /// <summary>
        /// Gets AlarmText by id
        /// </summary>
        /// <param name="Id">AlarmText Id</param>
        /// <returns>AlarmText object</returns>
        OpAlarmText GetAlarmText(long Id);

        /// <summary>
        /// Gets AlarmText by id
        /// </summary>
        /// <param name="Ids">AlarmText Ids</param>
        /// <returns>AlarmText list</returns>
        List<OpAlarmText> GetAlarmText(long[] Ids);

        /// <summary>
        /// Gets AlarmText by id
        /// </summary>
        /// <param name="Id">AlarmText Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmText object</returns>
        OpAlarmText GetAlarmText(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmText by id
        /// </summary>
        /// <param name="Ids">AlarmText Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmText list</returns>
        List<OpAlarmText> GetAlarmText(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmText object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmText to save</param>
        /// <returns>AlarmText Id</returns>
        long SaveAlarmText(OpAlarmText toBeSaved);

        /// <summary>
        /// Deletes the AlarmText object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmText to delete</param>
        void DeleteAlarmText(OpAlarmText toBeDeleted);

        /// <summary>
        /// Clears the AlarmText object cache
        /// </summary>
        void ClearAlarmTextCache();
        #endregion

        #region AlarmTextDataType

        /// <summary>
        /// Indicates whether the AlarmTextDataType is cached.
        /// </summary>
        bool AlarmTextDataTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmTextDataType objects
        /// </summary>
        List<OpAlarmTextDataType> GetAllAlarmTextDataType();


        /// <summary>
        /// Gets AlarmTextDataType by id
        /// </summary>
        /// <param name="Id">AlarmTextDataType Id</param>
        /// <returns>AlarmTextDataType object</returns>
        OpAlarmTextDataType GetAlarmTextDataType(long Id);

        /// <summary>
        /// Gets AlarmTextDataType by id
        /// </summary>
        /// <param name="Ids">AlarmTextDataType Ids</param>
        /// <returns>AlarmTextDataType list</returns>
        List<OpAlarmTextDataType> GetAlarmTextDataType(long[] Ids);

        /// <summary>
        /// Gets AlarmTextDataType by id
        /// </summary>
        /// <param name="Id">AlarmTextDataType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmTextDataType object</returns>
        OpAlarmTextDataType GetAlarmTextDataType(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmTextDataType by id
        /// </summary>
        /// <param name="Ids">AlarmTextDataType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmTextDataType list</returns>
        List<OpAlarmTextDataType> GetAlarmTextDataType(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmTextDataType object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmTextDataType to save</param>
        /// <returns>AlarmTextDataType Id</returns>
        long SaveAlarmTextDataType(OpAlarmTextDataType toBeSaved);

        /// <summary>
        /// Deletes the AlarmTextDataType object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmTextDataType to delete</param>
        void DeleteAlarmTextDataType(OpAlarmTextDataType toBeDeleted);

        /// <summary>
        /// Clears the AlarmTextDataType object cache
        /// </summary>
        void ClearAlarmTextDataTypeCache();
        #endregion

        #region AlarmType

        /// <summary>
        /// Indicates whether the AlarmType is cached.
        /// </summary>
        bool AlarmTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AlarmType objects
        /// </summary>
        List<OpAlarmType> GetAllAlarmType();


        /// <summary>
        /// Gets AlarmType by id
        /// </summary>
        /// <param name="Id">AlarmType Id</param>
        /// <returns>AlarmType object</returns>
        OpAlarmType GetAlarmType(int Id);

        /// <summary>
        /// Gets AlarmType by id
        /// </summary>
        /// <param name="Ids">AlarmType Ids</param>
        /// <returns>AlarmType list</returns>
        List<OpAlarmType> GetAlarmType(int[] Ids);

        /// <summary>
        /// Gets AlarmType by id
        /// </summary>
        /// <param name="Id">AlarmType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmType object</returns>
        OpAlarmType GetAlarmType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmType by id
        /// </summary>
        /// <param name="Ids">AlarmType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmType list</returns>
        List<OpAlarmType> GetAlarmType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AlarmType object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmType to save</param>
        /// <returns>AlarmType Id</returns>
        int SaveAlarmType(OpAlarmType toBeSaved);

        /// <summary>
        /// Deletes the AlarmType object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmType to delete</param>
        void DeleteAlarmType(OpAlarmType toBeDeleted);

        /// <summary>
        /// Clears the AlarmType object cache
        /// </summary>
        void ClearAlarmTypeCache();
        #endregion

        #region Article

        /// <summary>
        /// Indicates whether the Article is cached.
        /// </summary>
        bool ArticleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Article objects
        /// </summary>
        List<OpArticle> GetAllArticle(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Article by id
        /// </summary>
        /// <param name="Id">Article Id</param>
        /// <returns>Article object</returns>
        OpArticle GetArticle(long Id);

        /// <summary>
        /// Gets Article by id
        /// </summary>
        /// <param name="Ids">Article Ids</param>
        /// <returns>Article list</returns>
        List<OpArticle> GetArticle(long[] Ids);

        /// <summary>
        /// Gets Article by id
        /// </summary>
        /// <param name="Id">Article Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Article object</returns>
        OpArticle GetArticle(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Article by id
        /// </summary>
        /// <param name="Ids">Article Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Article list</returns>
        List<OpArticle> GetArticle(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Gets article id for depository element, if device with SerialNumber doesnt exists
        /// it creates DEVICE_ORDER_NUMBER (Name, FirstQuarter, SecondQuarter, ThirdQuarter)
        /// then creates DEVICE, if doesn't exists article which in name contains (Name, FirstQuarter, SecondQuarter)
        /// it creates article
        /// </summary>
        long GetIdArticleForDepositoryElement(string SerialNumber, string Name, string FirstQuarter, string SecondQuarter, string ThirdQuarter, bool AIUTDevice, int IdDistributor);

        /// <summary>
        /// Saves the Article object to the database
        /// </summary>
        /// <param name="toBeSaved">Article to save</param>
        /// <returns>Article Id</returns>
        long SaveArticle(OpArticle toBeSaved);

        /// <summary>
        /// Deletes the Article object from the database
        /// </summary>
        /// <param name="toBeDeleted">Article to delete</param>
        void DeleteArticle(OpArticle toBeDeleted);

        /// <summary>
        /// Clears the Article object cache
        /// </summary>
        void ClearArticleCache();
        #endregion

        #region ArticleData

        /// <summary>
        /// Indicates whether the ArticleData is cached.
        /// </summary>
        bool ArticleDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ArticleData objects
        /// </summary>
        List<OpArticleData> GetAllArticleData();


        /// <summary>
        /// Gets ArticleData by id
        /// </summary>
        /// <param name="Id">ArticleData Id</param>
        /// <returns>ArticleData object</returns>
        OpArticleData GetArticleData(long Id);

        /// <summary>
        /// Gets ArticleData by id
        /// </summary>
        /// <param name="Ids">ArticleData Ids</param>
        /// <returns>ArticleData list</returns>
        List<OpArticleData> GetArticleData(long[] Ids);

        /// <summary>
        /// Gets ArticleData by id
        /// </summary>
        /// <param name="Id">ArticleData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ArticleData object</returns>
        OpArticleData GetArticleData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ArticleData by id
        /// </summary>
        /// <param name="Ids">ArticleData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ArticleData list</returns>
        List<OpArticleData> GetArticleData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ArticleData object to the database
        /// </summary>
        /// <param name="toBeSaved">ArticleData to save</param>
        /// <returns>ArticleData Id</returns>
        long SaveArticleData(OpArticleData toBeSaved);

        /// <summary>
        /// Deletes the ArticleData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ArticleData to delete</param>
        void DeleteArticleData(OpArticleData toBeDeleted);

        /// <summary>
        /// Clears the ArticleData object cache
        /// </summary>
        void ClearArticleDataCache();
        #endregion

        #region AtgType

        /// <summary>
        /// Indicates whether the AtgType is cached.
        /// </summary>
        bool AtgTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all AtgType objects
        /// </summary>
        List<OpAtgType> GetAllAtgType();


        /// <summary>
        /// Gets AtgType by id
        /// </summary>
        /// <param name="Id">AtgType Id</param>
        /// <returns>AtgType object</returns>
        OpAtgType GetAtgType(int Id);

        /// <summary>
        /// Gets AtgType by id
        /// </summary>
        /// <param name="Ids">AtgType Ids</param>
        /// <returns>AtgType list</returns>
        List<OpAtgType> GetAtgType(int[] Ids);

        /// <summary>
        /// Gets AtgType by id
        /// </summary>
        /// <param name="Id">AtgType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AtgType object</returns>
        OpAtgType GetAtgType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets AtgType by id
        /// </summary>
        /// <param name="Ids">AtgType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AtgType list</returns>
        List<OpAtgType> GetAtgType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the AtgType object to the database
        /// </summary>
        /// <param name="toBeSaved">AtgType to save</param>
        /// <returns>AtgType Id</returns>
        int SaveAtgType(OpAtgType toBeSaved);

        /// <summary>
        /// Deletes the AtgType object from the database
        /// </summary>
        /// <param name="toBeDeleted">AtgType to delete</param>
        void DeleteAtgType(OpAtgType toBeDeleted);

        /// <summary>
        /// Clears the AtgType object cache
        /// </summary>
        void ClearAtgTypeCache();
        #endregion

        #region Audit

        /// <summary>
        /// Indicates whether the Audit is cached.
        /// </summary>
        bool AuditCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Audit objects
        /// </summary>
        List<OpAudit> GetAllAudit();


        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <returns>Audit object</returns>
        OpAudit GetAudit(long Id);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <returns>Audit list</returns>
        List<OpAudit> GetAudit(long[] Ids);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit object</returns>
        OpAudit GetAudit(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit list</returns>
        List<OpAudit> GetAudit(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Audit object to the database
        /// </summary>
        /// <param name="toBeSaved">Audit to save</param>
        /// <returns>Audit Id</returns>
        long SaveAudit(OpAudit toBeSaved);

        /// <summary>
        /// Deletes the Audit object from the database
        /// </summary>
        /// <param name="toBeDeleted">Audit to delete</param>
        void DeleteAudit(OpAudit toBeDeleted);

        /// <summary>
        /// Clears the Audit object cache
        /// </summary>
        void ClearAuditCache();
        #endregion

        #region Code

        /// <summary>
        /// Indicates whether the Code is cached.
        /// </summary>
        bool CodeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Code objects
        /// </summary>
        List<OpCode> GetAllCode();


        /// <summary>
        /// Gets Code by id
        /// </summary>
        /// <param name="Id">Code Id</param>
        /// <returns>Code object</returns>
        OpCode GetCode(long Id);

        /// <summary>
        /// Gets Code by id
        /// </summary>
        /// <param name="Ids">Code Ids</param>
        /// <returns>Code list</returns>
        List<OpCode> GetCode(long[] Ids);

        /// <summary>
        /// Gets Code by id
        /// </summary>
        /// <param name="Id">Code Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Code object</returns>
        OpCode GetCode(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Code by id
        /// </summary>
        /// <param name="Ids">Code Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Code list</returns>
        List<OpCode> GetCode(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Code object to the database
        /// </summary>
        /// <param name="toBeSaved">Code to save</param>
        /// <returns>Code Id</returns>
        long SaveCode(OpCode toBeSaved);

        /// <summary>
        /// Deletes the Code object from the database
        /// </summary>
        /// <param name="toBeDeleted">Code to delete</param>
        void DeleteCode(OpCode toBeDeleted);

        /// <summary>
        /// Clears the Code object cache
        /// </summary>
        void ClearCodeCache();
        #endregion

        #region CodeType

        /// <summary>
        /// Indicates whether the CodeType is cached.
        /// </summary>
        bool CodeTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all CodeType objects
        /// </summary>
        List<OpCodeType> GetAllCodeType();


        /// <summary>
        /// Gets CodeType by id
        /// </summary>
        /// <param name="Id">CodeType Id</param>
        /// <returns>CodeType object</returns>
        OpCodeType GetCodeType(int Id);

        /// <summary>
        /// Gets CodeType by id
        /// </summary>
        /// <param name="Ids">CodeType Ids</param>
        /// <returns>CodeType list</returns>
        List<OpCodeType> GetCodeType(int[] Ids);

        /// <summary>
        /// Gets CodeType by id
        /// </summary>
        /// <param name="Id">CodeType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>CodeType object</returns>
        OpCodeType GetCodeType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets CodeType by id
        /// </summary>
        /// <param name="Ids">CodeType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>CodeType list</returns>
        List<OpCodeType> GetCodeType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the CodeType object to the database
        /// </summary>
        /// <param name="toBeSaved">CodeType to save</param>
        /// <returns>CodeType Id</returns>
        int SaveCodeType(OpCodeType toBeSaved);

        /// <summary>
        /// Deletes the CodeType object from the database
        /// </summary>
        /// <param name="toBeDeleted">CodeType to delete</param>
        void DeleteCodeType(OpCodeType toBeDeleted);

        /// <summary>
        /// Clears the CodeType object cache
        /// </summary>
        void ClearCodeTypeCache();
        #endregion

        #region CommandCodeParamMap

        /// <summary>
        /// Indicates whether the CommandCodeParamMap is cached.
        /// </summary>
        bool CommandCodeParamMapCacheEnabled { get; set; }

        /// <summary>
        /// Gets CommandCodeParamMap by id
        /// </summary>
        /// <param name="Id">CommandCodeParamMap Id</param>
        /// <returns>CommandCodeParamMap object</returns>
        OpCommandCodeParamMap GetCommandCodeParamMap(long Id);

        /// <summary>
        /// Gets CommandCodeParamMap by id
        /// </summary>
        /// <param name="Ids">CommandCodeParamMap Ids</param>
        /// <returns>CommandCodeParamMap list</returns>
        List<OpCommandCodeParamMap> GetCommandCodeParamMap(long[] Ids);

        /// <summary>
        /// Gets CommandCodeParamMap by id
        /// </summary>
        /// <param name="Id">CommandCodeParamMap Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>CommandCodeParamMap object</returns>
        OpCommandCodeParamMap GetCommandCodeParamMap(long Id, bool queryDatabase);

        /// <summary>
        /// Gets CommandCodeParamMap by id
        /// </summary>
        /// <param name="Ids">CommandCodeParamMap Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>CommandCodeParamMap list</returns>
        List<OpCommandCodeParamMap> GetCommandCodeParamMap(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the CommandCodeParamMap object to the database
        /// </summary>
        /// <param name="toBeSaved">CommandCodeParamMap to save</param>
        /// <returns>CommandCodeParamMap Id</returns>
        long SaveCommandCodeParamMap(OpCommandCodeParamMap toBeSaved);

        /// <summary>
        /// Deletes the CommandCodeParamMap object from the database
        /// </summary>
        /// <param name="toBeDeleted">CommandCodeParamMap to delete</param>
        void DeleteCommandCodeParamMap(OpCommandCodeParamMap toBeDeleted);

        /// <summary>
        /// Clears the CommandCodeParamMap object cache
        /// </summary>
        void ClearCommandCodeParamMapCache();
        #endregion

        #region CommandCodeType

        /// <summary>
        /// Indicates whether the CommandCodeType is cached.
        /// </summary>
        bool CommandCodeTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets CommandCodeType by id
        /// </summary>
        /// <param name="Id">CommandCodeType Id</param>
        /// <returns>CommandCodeType object</returns>
        OpCommandCodeType GetCommandCodeType(long Id);

        /// <summary>
        /// Gets CommandCodeType by id
        /// </summary>
        /// <param name="Ids">CommandCodeType Ids</param>
        /// <returns>CommandCodeType list</returns>
        List<OpCommandCodeType> GetCommandCodeType(long[] Ids);

        /// <summary>
        /// Gets CommandCodeType by id
        /// </summary>
        /// <param name="Id">CommandCodeType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>CommandCodeType object</returns>
        OpCommandCodeType GetCommandCodeType(long Id, bool queryDatabase);

        /// <summary>
        /// Gets CommandCodeType by id
        /// </summary>
        /// <param name="Ids">CommandCodeType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>CommandCodeType list</returns>
        List<OpCommandCodeType> GetCommandCodeType(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the CommandCodeType object to the database
        /// </summary>
        /// <param name="toBeSaved">CommandCodeType to save</param>
        /// <returns>CommandCodeType Id</returns>
        long SaveCommandCodeType(OpCommandCodeType toBeSaved);

        /// <summary>
        /// Deletes the CommandCodeType object from the database
        /// </summary>
        /// <param name="toBeDeleted">CommandCodeType to delete</param>
        void DeleteCommandCodeType(OpCommandCodeType toBeDeleted);

        /// <summary>
        /// Clears the CommandCodeType object cache
        /// </summary>
        void ClearCommandCodeTypeCache();
        #endregion

        #region Component

        /// <summary>
        /// Indicates whether the Component is cached.
        /// </summary>
        bool ComponentCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Component objects
        /// </summary>
        List<OpComponent> GetAllComponent(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Component by id
        /// </summary>
        /// <param name="Id">Component Id</param>
        /// <returns>Component object</returns>
        OpComponent GetComponent(int Id);

        /// <summary>
        /// Gets Component by id
        /// </summary>
        /// <param name="Ids">Component Ids</param>
        /// <returns>Component list</returns>
        List<OpComponent> GetComponent(int[] Ids);

        /// <summary>
        /// Gets Component by id
        /// </summary>
        /// <param name="Id">Component Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Component object</returns>
        OpComponent GetComponent(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Component by id
        /// </summary>
        /// <param name="Ids">Component Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Component list</returns>
        List<OpComponent> GetComponent(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Component object to the database
        /// </summary>
        /// <param name="toBeSaved">Component to save</param>
        /// <returns>Component Id</returns>
        int SaveComponent(OpComponent toBeSaved);

        /// <summary>
        /// Deletes the Component object from the database
        /// </summary>
        /// <param name="toBeDeleted">Component to delete</param>
        void DeleteComponent(OpComponent toBeDeleted);

        /// <summary>
        /// Clears the Component object cache
        /// </summary>
        void ClearComponentCache();
        #endregion

        #region ConfigurationProfile

        /// <summary>
        /// Indicates whether the ConfigurationProfile is cached.
        /// </summary>
        bool ConfigurationProfileCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConfigurationProfile objects
        /// </summary>
        List<OpConfigurationProfile> GetAllConfigurationProfile();


        /// <summary>
        /// Gets ConfigurationProfile by id
        /// </summary>
        /// <param name="Id">ConfigurationProfile Id</param>
        /// <returns>ConfigurationProfile object</returns>
        OpConfigurationProfile GetConfigurationProfile(long Id);

        /// <summary>
        /// Gets ConfigurationProfile by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfile Ids</param>
        /// <returns>ConfigurationProfile list</returns>
        List<OpConfigurationProfile> GetConfigurationProfile(long[] Ids);

        /// <summary>
        /// Gets ConfigurationProfile by id
        /// </summary>
        /// <param name="Id">ConfigurationProfile Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfile object</returns>
        OpConfigurationProfile GetConfigurationProfile(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConfigurationProfile by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfile Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfile list</returns>
        List<OpConfigurationProfile> GetConfigurationProfile(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConfigurationProfile object to the database
        /// </summary>
        /// <param name="toBeSaved">ConfigurationProfile to save</param>
        /// <returns>ConfigurationProfile Id</returns>
        long SaveConfigurationProfile(OpConfigurationProfile toBeSaved);

        /// <summary>
        /// Deletes the ConfigurationProfile object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConfigurationProfile to delete</param>
        void DeleteConfigurationProfile(OpConfigurationProfile toBeDeleted);

        /// <summary>
        /// Clears the ConfigurationProfile object cache
        /// </summary>
        void ClearConfigurationProfileCache();
        #endregion

        #region ConfigurationProfileData

        /// <summary>
        /// Indicates whether the ConfigurationProfileData is cached.
        /// </summary>
        bool ConfigurationProfileDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConfigurationProfileData objects
        /// </summary>
        List<OpConfigurationProfileData> GetAllConfigurationProfileData();


        /// <summary>
        /// Gets ConfigurationProfileData by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileData Id</param>
        /// <returns>ConfigurationProfileData object</returns>
        OpConfigurationProfileData GetConfigurationProfileData(long Id);

        /// <summary>
        /// Gets ConfigurationProfileData by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileData Ids</param>
        /// <returns>ConfigurationProfileData list</returns>
        List<OpConfigurationProfileData> GetConfigurationProfileData(long[] Ids);

        /// <summary>
        /// Gets ConfigurationProfileData by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileData object</returns>
        OpConfigurationProfileData GetConfigurationProfileData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConfigurationProfileData by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileData list</returns>
        List<OpConfigurationProfileData> GetConfigurationProfileData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConfigurationProfileData object to the database
        /// </summary>
        /// <param name="toBeSaved">ConfigurationProfileData to save</param>
        /// <returns>ConfigurationProfileData Id</returns>
        long SaveConfigurationProfileData(OpConfigurationProfileData toBeSaved);

        /// <summary>
        /// Deletes the ConfigurationProfileData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConfigurationProfileData to delete</param>
        void DeleteConfigurationProfileData(OpConfigurationProfileData toBeDeleted);

        /// <summary>
        /// Clears the ConfigurationProfileData object cache
        /// </summary>
        void ClearConfigurationProfileDataCache();
        #endregion

        #region ConfigurationProfileDeviceType

        /// <summary>
        /// Indicates whether the ConfigurationProfileDeviceType is cached.
        /// </summary>
        bool ConfigurationProfileDeviceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConfigurationProfileDeviceType objects
        /// </summary>
        List<OpConfigurationProfileDeviceType> GetAllConfigurationProfileDeviceType();


        /// <summary>
        /// Gets ConfigurationProfileDeviceType by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileDeviceType Id</param>
        /// <returns>ConfigurationProfileDeviceType object</returns>
        OpConfigurationProfileDeviceType GetConfigurationProfileDeviceType(long Id);

        /// <summary>
        /// Gets ConfigurationProfileDeviceType by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileDeviceType Ids</param>
        /// <returns>ConfigurationProfileDeviceType list</returns>
        List<OpConfigurationProfileDeviceType> GetConfigurationProfileDeviceType(long[] Ids);

        /// <summary>
        /// Gets ConfigurationProfileDeviceType by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileDeviceType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileDeviceType object</returns>
        OpConfigurationProfileDeviceType GetConfigurationProfileDeviceType(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConfigurationProfileDeviceType by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileDeviceType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileDeviceType list</returns>
        List<OpConfigurationProfileDeviceType> GetConfigurationProfileDeviceType(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConfigurationProfileDeviceType object to the database
        /// </summary>
        /// <param name="toBeSaved">ConfigurationProfileDeviceType to save</param>
        /// <returns>ConfigurationProfileDeviceType Id</returns>
        long SaveConfigurationProfileDeviceType(OpConfigurationProfileDeviceType toBeSaved);

        /// <summary>
        /// Deletes the ConfigurationProfileDeviceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConfigurationProfileDeviceType to delete</param>
        void DeleteConfigurationProfileDeviceType(OpConfigurationProfileDeviceType toBeDeleted);

        /// <summary>
        /// Clears the ConfigurationProfileDeviceType object cache
        /// </summary>
        void ClearConfigurationProfileDeviceTypeCache();
        #endregion

        #region ConfigurationProfileType

        /// <summary>
        /// Indicates whether the ConfigurationProfileType is cached.
        /// </summary>
        bool ConfigurationProfileTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConfigurationProfileType objects
        /// </summary>
        List<OpConfigurationProfileType> GetAllConfigurationProfileType();


        /// <summary>
        /// Gets ConfigurationProfileType by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileType Id</param>
        /// <returns>ConfigurationProfileType object</returns>
        OpConfigurationProfileType GetConfigurationProfileType(int Id);

        /// <summary>
        /// Gets ConfigurationProfileType by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileType Ids</param>
        /// <returns>ConfigurationProfileType list</returns>
        List<OpConfigurationProfileType> GetConfigurationProfileType(int[] Ids);

        /// <summary>
        /// Gets ConfigurationProfileType by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileType object</returns>
        OpConfigurationProfileType GetConfigurationProfileType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConfigurationProfileType by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileType list</returns>
        List<OpConfigurationProfileType> GetConfigurationProfileType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConfigurationProfileType object to the database
        /// </summary>
        /// <param name="toBeSaved">ConfigurationProfileType to save</param>
        /// <returns>ConfigurationProfileType Id</returns>
        int SaveConfigurationProfileType(OpConfigurationProfileType toBeSaved);

        /// <summary>
        /// Deletes the ConfigurationProfileType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConfigurationProfileType to delete</param>
        void DeleteConfigurationProfileType(OpConfigurationProfileType toBeDeleted);

        /// <summary>
        /// Clears the ConfigurationProfileType object cache
        /// </summary>
        void ClearConfigurationProfileTypeCache();
        #endregion

        #region ConfigurationProfileTypeStep

        /// <summary>
        /// Indicates whether the ConfigurationProfileTypeStep is cached.
        /// </summary>
        bool ConfigurationProfileTypeStepCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConfigurationProfileTypeStep objects
        /// </summary>
        List<OpConfigurationProfileTypeStep> GetAllConfigurationProfileTypeStep();


        /// <summary>
        /// Gets ConfigurationProfileTypeStep by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileTypeStep Id</param>
        /// <returns>ConfigurationProfileTypeStep object</returns>
        OpConfigurationProfileTypeStep GetConfigurationProfileTypeStep(int Id);

        /// <summary>
        /// Gets ConfigurationProfileTypeStep by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileTypeStep Ids</param>
        /// <returns>ConfigurationProfileTypeStep list</returns>
        List<OpConfigurationProfileTypeStep> GetConfigurationProfileTypeStep(int[] Ids);

        /// <summary>
        /// Gets ConfigurationProfileTypeStep by id
        /// </summary>
        /// <param name="Id">ConfigurationProfileTypeStep Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileTypeStep object</returns>
        OpConfigurationProfileTypeStep GetConfigurationProfileTypeStep(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConfigurationProfileTypeStep by id
        /// </summary>
        /// <param name="Ids">ConfigurationProfileTypeStep Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConfigurationProfileTypeStep list</returns>
        List<OpConfigurationProfileTypeStep> GetConfigurationProfileTypeStep(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConfigurationProfileTypeStep object to the database
        /// </summary>
        /// <param name="toBeSaved">ConfigurationProfileTypeStep to save</param>
        /// <returns>ConfigurationProfileTypeStep Id</returns>
        int SaveConfigurationProfileTypeStep(OpConfigurationProfileTypeStep toBeSaved);

        /// <summary>
        /// Deletes the ConfigurationProfileTypeStep object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConfigurationProfileTypeStep to delete</param>
        void DeleteConfigurationProfileTypeStep(OpConfigurationProfileTypeStep toBeDeleted);

        /// <summary>
        /// Clears the ConfigurationProfileTypeStep object cache
        /// </summary>
        void ClearConfigurationProfileTypeStepCache();
        #endregion

        #region Consumer

        /// <summary>
        /// Indicates whether the Consumer is cached.
        /// </summary>
        bool ConsumerCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Consumer objects
        /// </summary>
        List<OpConsumer> GetAllConsumer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Consumer by id
        /// </summary>
        /// <param name="Id">Consumer Id</param>
        /// <returns>Consumer object</returns>
        OpConsumer GetConsumer(int Id);

        /// <summary>
        /// Gets Consumer by id
        /// </summary>
        /// <param name="Ids">Consumer Ids</param>
        /// <returns>Consumer list</returns>
        List<OpConsumer> GetConsumer(int[] Ids);

        /// <summary>
        /// Gets Consumer by id
        /// </summary>
        /// <param name="Id">Consumer Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Consumer object</returns>
        OpConsumer GetConsumer(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Consumer by id
        /// </summary>
        /// <param name="Ids">Consumer Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Consumer list</returns>
        List<OpConsumer> GetConsumer(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Consumer object to the database
        /// </summary>
        /// <param name="toBeSaved">Consumer to save</param>
        /// <returns>Consumer Id</returns>
        int SaveConsumer(OpConsumer toBeSaved);

        /// <summary>
        /// Deletes the Consumer object from the database
        /// </summary>
        /// <param name="toBeDeleted">Consumer to delete</param>
        void DeleteConsumer(OpConsumer toBeDeleted);

        /// <summary>
        /// Clears the Consumer object cache
        /// </summary>
        void ClearConsumerCache();
        #endregion

        #region ConsumerData

        /// <summary>
        /// Indicates whether the ConsumerData is cached.
        /// </summary>
        bool ConsumerDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerData objects
        /// </summary>
        List<OpConsumerData> GetAllConsumerData();


        /// <summary>
        /// Gets ConsumerData by id
        /// </summary>
        /// <param name="Id">ConsumerData Id</param>
        /// <returns>ConsumerData object</returns>
        OpConsumerData GetConsumerData(long Id);

        /// <summary>
        /// Gets ConsumerData by id
        /// </summary>
        /// <param name="Ids">ConsumerData Ids</param>
        /// <returns>ConsumerData list</returns>
        List<OpConsumerData> GetConsumerData(long[] Ids);

        /// <summary>
        /// Gets ConsumerData by id
        /// </summary>
        /// <param name="Id">ConsumerData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerData object</returns>
        OpConsumerData GetConsumerData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerData by id
        /// </summary>
        /// <param name="Ids">ConsumerData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerData list</returns>
        List<OpConsumerData> GetConsumerData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerData object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerData to save</param>
        /// <returns>ConsumerData Id</returns>
        long SaveConsumerData(OpConsumerData toBeSaved);

        /// <summary>
        /// Deletes the ConsumerData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerData to delete</param>
        void DeleteConsumerData(OpConsumerData toBeDeleted);

        /// <summary>
        /// Clears the ConsumerData object cache
        /// </summary>
        void ClearConsumerDataCache();
        #endregion

        #region ConsumerNotification

        /// <summary>
        /// Indicates whether the ConsumerNotification is cached.
        /// </summary>
        bool ConsumerNotificationCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerNotification objects
        /// </summary>
        List<OpConsumerNotification> GetAllConsumerNotification();


        /// <summary>
        /// Gets ConsumerNotification by id
        /// </summary>
        /// <param name="Id">ConsumerNotification Id</param>
        /// <returns>ConsumerNotification object</returns>
        OpConsumerNotification GetConsumerNotification(long Id);

        /// <summary>
        /// Gets ConsumerNotification by id
        /// </summary>
        /// <param name="Ids">ConsumerNotification Ids</param>
        /// <returns>ConsumerNotification list</returns>
        List<OpConsumerNotification> GetConsumerNotification(long[] Ids);

        /// <summary>
        /// Gets ConsumerNotification by id
        /// </summary>
        /// <param name="Id">ConsumerNotification Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotification object</returns>
        OpConsumerNotification GetConsumerNotification(long Id, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerNotification object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerNotification to save</param>
        /// <returns>ConsumerNotification Id</returns>
        long SaveConsumerNotification(OpConsumerNotification toBeSaved);

        /// <summary>
        /// Deletes the ConsumerNotification object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerNotification to delete</param>
        void DeleteConsumerNotification(OpConsumerNotification toBeDeleted);

        /// <summary>
        /// Clears the ConsumerNotification object cache
        /// </summary>
        void ClearConsumerNotificationCache();
        #endregion

        #region ConsumerNotificationHistory

        /// <summary>
        /// Indicates whether the ConsumerNotificationHistory is cached.
        /// </summary>
        bool ConsumerNotificationHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerNotificationHistory objects
        /// </summary>
        List<OpConsumerNotificationHistory> GetAllConsumerNotificationHistory();


        /// <summary>
        /// Gets ConsumerNotificationHistory by id
        /// </summary>
        /// <param name="Id">ConsumerNotificationHistory Id</param>
        /// <returns>ConsumerNotificationHistory object</returns>
        OpConsumerNotificationHistory GetConsumerNotificationHistory(long Id);

        /// <summary>
        /// Gets ConsumerNotificationHistory by id
        /// </summary>
        /// <param name="Ids">ConsumerNotificationHistory Ids</param>
        /// <returns>ConsumerNotificationHistory list</returns>
        List<OpConsumerNotificationHistory> GetConsumerNotificationHistory(long[] Ids);

        /// <summary>
        /// Gets ConsumerNotificationHistory by id
        /// </summary>
        /// <param name="Id">ConsumerNotificationHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotificationHistory object</returns>
        OpConsumerNotificationHistory GetConsumerNotificationHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerNotificationHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerNotificationHistory to save</param>
        /// <returns>ConsumerNotificationHistory Id</returns>
        long SaveConsumerNotificationHistory(OpConsumerNotificationHistory toBeSaved);

        /// <summary>
        /// Deletes the ConsumerNotificationHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerNotificationHistory to delete</param>
        void DeleteConsumerNotificationHistory(OpConsumerNotificationHistory toBeDeleted);

        /// <summary>
        /// Clears the ConsumerNotificationHistory object cache
        /// </summary>
        void ClearConsumerNotificationHistoryCache();
        #endregion

        #region ConsumerNotificationTypeData
        /// <summary>
        /// Indicates whether the ConsumerNotificationTypeDataDict was filled trough GetAllConsumerNotificationTypeData or GetAllConsumerNotificationTypeDataFilter with mergeIntoCache = true
        /// </summary>
        bool ConsumerNotificationTypeDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets ConsumerNotificationTypeData by id
        /// </summary>
        /// <param name="Id">ConsumerNotificationTypeData Id</param>
        /// <returns>ConsumerNotificationTypeData object</returns>
        OpConsumerNotificationTypeData GetConsumerNotificationTypeData(long Id);

        /// <summary>
        /// Gets ConsumerNotificationTypeData by id
        /// </summary>
        /// <param name="Ids">ConsumerNotificationTypeData Ids</param>
        /// <returns>ConsumerNotificationTypeData list</returns>
        List<OpConsumerNotificationTypeData> GetConsumerNotificationTypeData(long[] Ids);

        /// <summary>
        /// Gets ConsumerNotificationTypeData by id
        /// </summary>
        /// <param name="Id">ConsumerNotificationTypeData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotificationTypeData object</returns>
        OpConsumerNotificationTypeData GetConsumerNotificationTypeData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets all ConsumerNotificationTypeData objects
        /// </summary>
        List<OpConsumerNotificationTypeData> GetAllConsumerNotificationTypeData();

        /// <summary>
        /// Gets ConsumerNotificationTypeData by id
        /// </summary>
        /// <param name="Ids">ConsumerNotificationTypeData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotificationTypeData list</returns>
        List<OpConsumerNotificationTypeData> GetConsumerNotificationTypeData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Gets ConsumerNotificationTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerNotificationTypeData funcion</param>
        /// <param name="IdConsumerNotificationTypeData">Specifies filter for ID_CONSUMER_NOTIFICATION_TYPE_DATA column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdNotificationType">Specifies filter for ID_NOTIFICATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_NOTIFICATION_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerNotificationTypeData list</returns>
        List<OpConsumerNotificationTypeData> GetConsumerNotificationTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerNotificationTypeData = null, int[] IdConsumer = null, int[] IdNotificationType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ConsumerNotificationTypeData object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerNotificationTypeData to save</param>
        /// <returns>ConsumerNotificationTypeData Id</returns>
        long SaveConsumerNotificationTypeData(OpConsumerNotificationTypeData toBeSaved);

        /// <summary>
        /// Deletes the ConsumerNotificationTypeData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerNotificationTypeData to delete</param>
        void DeleteConsumerNotificationTypeData(OpConsumerNotificationTypeData toBeDeleted);

        /// <summary>
        /// Clears the ConsumerNotificationTypeData object cache
        /// </summary>
        void ClearConsumerNotificationTypeDataCache();
        #endregion

        #region ConsumerPaymentModule

        /// <summary>
        /// Indicates whether the ConsumerPaymentModule is cached.
        /// </summary>
        bool ConsumerPaymentModuleCacheEnabled { get; set; }

        /// <summary>
        /// Gets ConsumerPaymentModule by id
        /// </summary>
        /// <param name="Id">ConsumerPaymentModule Id</param>
        /// <returns>ConsumerPaymentModule object</returns>
        OpConsumerPaymentModule GetConsumerPaymentModule(long Id);

        /// <summary>
        /// Gets ConsumerPaymentModule by id
        /// </summary>
        /// <param name="Ids">ConsumerPaymentModule Ids</param>
        /// <returns>ConsumerPaymentModule list</returns>
        List<OpConsumerPaymentModule> GetConsumerPaymentModule(long[] Ids);

        /// <summary>
        /// Gets ConsumerPaymentModule by id
        /// </summary>
        /// <param name="Id">ConsumerPaymentModule Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerPaymentModule object</returns>
        OpConsumerPaymentModule GetConsumerPaymentModule(long Id, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerPaymentModule object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerPaymentModule to save</param>
        /// <returns>ConsumerPaymentModule Id</returns>
        long SaveConsumerPaymentModule(OpConsumerPaymentModule toBeSaved);

        /// <summary>
        /// Deletes the ConsumerPaymentModule object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerPaymentModule to delete</param>
        void DeleteConsumerPaymentModule(OpConsumerPaymentModule toBeDeleted);

        /// <summary>
        /// Clears the ConsumerPaymentModule object cache
        /// </summary>
        void ClearConsumerPaymentModuleCache();
        #endregion

        #region ConsumerTransaction

        /// <summary>
        /// Indicates whether the ConsumerTransaction is cached.
        /// </summary>
        bool ConsumerTransactionCacheEnabledCORE { get; set; }

        /// <summary>
        /// Gets all ConsumerTransaction objects
        /// </summary>
        List<OpConsumerTransaction> GetAllConsumerTransactionCORE();


        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Id">ConsumerTransaction Id</param>
        /// <returns>ConsumerTransaction object</returns>
        OpConsumerTransaction GetConsumerTransactionCORE(long Id);

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Ids">ConsumerTransaction Ids</param>
        /// <returns>ConsumerTransaction list</returns>
        List<OpConsumerTransaction> GetConsumerTransactionCORE(long[] Ids);

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Id">ConsumerTransaction Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransaction object</returns>
        OpConsumerTransaction GetConsumerTransactionCORE(long Id, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerTransaction object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTransaction to save</param>
        /// <returns>ConsumerTransaction Id</returns>
        long SaveConsumerTransactionCORE(OpConsumerTransaction toBeSaved);

        /// <summary>
        /// Deletes the ConsumerTransaction object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTransaction to delete</param>
        void DeleteConsumerTransactionCORE(OpConsumerTransaction toBeDeleted);

        /// <summary>
        /// Clears the ConsumerTransaction object cache
        /// </summary>
        void ClearConsumerTransactionCacheCORE();
        #endregion

        #region ConsumerTransactionData

        /// <summary>
        /// Indicates whether the ConsumerTransactionData is cached.
        /// </summary>
        bool ConsumerTransactionDataCacheEnabledCORE { get; set; }

        /// <summary>
        /// Gets all ConsumerTransactionData objects
        /// </summary>
        List<OpConsumerTransactionData> GetAllConsumerTransactionDataCORE();


        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionData Id</param>
        /// <returns>ConsumerTransactionData object</returns>
        OpConsumerTransactionData GetConsumerTransactionDataCORE(long Id);

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionData Ids</param>
        /// <returns>ConsumerTransactionData list</returns>
        List<OpConsumerTransactionData> GetConsumerTransactionDataCORE(long[] Ids);

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionData object</returns>
        OpConsumerTransactionData GetConsumerTransactionDataCORE(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionData list</returns>
        List<OpConsumerTransactionData> GetConsumerTransactionDataCORE(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ConsumerTransactionData object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTransactionData to save</param>
        /// <returns>ConsumerTransactionData Id</returns>
        long SaveConsumerTransactionDataCORE(OpConsumerTransactionData toBeSaved);

        /// <summary>
        /// Deletes the ConsumerTransactionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTransactionData to delete</param>
        void DeleteConsumerTransactionDataCORE(OpConsumerTransactionData toBeDeleted);

        /// <summary>
        /// Clears the ConsumerTransactionData object cache
        /// </summary>
        void ClearConsumerTransactionDataCacheCORE();
        #endregion

        #region ConsumerType

        /// <summary>
        /// Indicates whether the ConsumerType is cached.
        /// </summary>
        bool ConsumerTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerType objects
        /// </summary>
        List<OpConsumerType> GetAllConsumerType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ConsumerType by id
        /// </summary>
        /// <param name="Id">ConsumerType Id</param>
        /// <returns>ConsumerType object</returns>
        OpConsumerType GetConsumerType(int Id);

        /// <summary>
        /// Gets ConsumerType by id
        /// </summary>
        /// <param name="Ids">ConsumerType Ids</param>
        /// <returns>ConsumerType list</returns>
        List<OpConsumerType> GetConsumerType(int[] Ids);

        /// <summary>
        /// Gets ConsumerType by id
        /// </summary>
        /// <param name="Id">ConsumerType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerType object</returns>
        OpConsumerType GetConsumerType(int Id, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerType object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerType to save</param>
        /// <returns>ConsumerType Id</returns>
        int SaveConsumerType(OpConsumerType toBeSaved);

        /// <summary>
        /// Deletes the ConsumerType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerType to delete</param>
        void DeleteConsumerType(OpConsumerType toBeDeleted);

        /// <summary>
        /// Clears the ConsumerType object cache
        /// </summary>
        void ClearConsumerTypeCache();
        #endregion

        #region ConsumerTypeData

        /// <summary>
        /// Indicates whether the ConsumerTypeData is cached.
        /// </summary>
        bool ConsumerTypeDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerTypeData objects
        /// </summary>
        List<OpConsumerTypeData> GetAllConsumerTypeData();


        /// <summary>
        /// Gets ConsumerTypeData by id
        /// </summary>
        /// <param name="Id">ConsumerTypeData Id</param>
        /// <returns>ConsumerTypeData object</returns>
        OpConsumerTypeData GetConsumerTypeData(long Id);

        /// <summary>
        /// Gets ConsumerTypeData by id
        /// </summary>
        /// <param name="Ids">ConsumerTypeData Ids</param>
        /// <returns>ConsumerTypeData list</returns>
        List<OpConsumerTypeData> GetConsumerTypeData(long[] Ids);

        /// <summary>
        /// Gets ConsumerTypeData by id
        /// </summary>
        /// <param name="Id">ConsumerTypeData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTypeData object</returns>
        OpConsumerTypeData GetConsumerTypeData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerTypeData by id
        /// </summary>
        /// <param name="Ids">ConsumerTypeData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTypeData list</returns>
        List<OpConsumerTypeData> GetConsumerTypeData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerTypeData object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTypeData to save</param>
        /// <returns>ConsumerTypeData Id</returns>
        long SaveConsumerTypeData(OpConsumerTypeData toBeSaved);

        /// <summary>
        /// Deletes the ConsumerTypeData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTypeData to delete</param>
        void DeleteConsumerTypeData(OpConsumerTypeData toBeDeleted);

        /// <summary>
        /// Clears the ConsumerTypeData object cache
        /// </summary>
        void ClearConsumerTypeDataCache();
        #endregion

        #region ConsumerTransactionType

        /// <summary>
        /// Indicates whether the ConsumerTransactionType is cached.
        /// </summary>
        bool ConsumerTransactionTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerTransactionType objects
        /// </summary>
        List<OpConsumerTransactionType> GetAllConsumerTransactionType();

        /// <summary>
        /// Gets ConsumerTransactionType by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionType Id</param>
        /// <returns>ConsumerTransactionType object</returns>
        OpConsumerTransactionType GetConsumerTransactionType(int Id);

        /// <summary>
        /// Gets ConsumerTransactionType by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionType Ids</param>
        /// <returns>ConsumerTransactionType list</returns>
        List<OpConsumerTransactionType> GetConsumerTransactionType(int[] Ids);

        /// <summary>
        /// Gets ConsumerTransactionType by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionType object</returns>
        OpConsumerTransactionType GetConsumerTransactionType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerTransactionType by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionType list</returns>
        List<OpConsumerTransactionType> GetConsumerTransactionType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ConsumerTransactionType object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTransactionType to save</param>
        /// <returns>ConsumerTransactionType Id</returns>
        int? SaveConsumerTransactionType(OpConsumerTransactionType toBeSaved);

        /// <summary>
        /// Deletes the ConsumerTransactionType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTransactionType to delete</param>
        void DeleteConsumerTransactionType(OpConsumerTransactionType toBeDeleted);

        /// <summary>
        /// Clears the ConsumerTransactionType object cache
        /// </summary>
        void ClearConsumerTransactionTypeCache();
        #endregion

        #region ConsumerSettlement

        /// <summary>
        /// Indicates whether the ConsumerSettlement is cached.
        /// </summary>
        bool ConsumerSettlementCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerSettlement objects
        /// </summary>
        List<OpConsumerSettlement> GetAllConsumerSettlement();

        /// <summary>
        /// Gets ConsumerSettlement by id
        /// </summary>
        /// <param name="Id">ConsumerSettlement Id</param>
        /// <returns>ConsumerSettlement object</returns>
        OpConsumerSettlement GetConsumerSettlement(long Id);

        /// <summary>
        /// Gets ConsumerSettlement by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlement Ids</param>
        /// <returns>ConsumerSettlement list</returns>
        List<OpConsumerSettlement> GetConsumerSettlement(long[] Ids);

        /// <summary>
        /// Gets ConsumerSettlement by id
        /// </summary>
        /// <param name="Id">ConsumerSettlement Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlement object</returns>
        OpConsumerSettlement GetConsumerSettlement(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerSettlement by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlement Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlement list</returns>
        List<OpConsumerSettlement> GetConsumerSettlement(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ConsumerSettlement object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerSettlement to save</param>
        /// <returns>ConsumerSettlement Id</returns>
        long SaveConsumerSettlement(OpConsumerSettlement toBeSaved);

        /// <summary>
        /// Deletes the ConsumerSettlement object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerSettlement to delete</param>
        void DeleteConsumerSettlement(OpConsumerSettlement toBeDeleted);

        /// <summary>
        /// Clears the ConsumerSettlement object cache
        /// </summary>
        void ClearConsumerSettlementCache();
        #endregion

        #region ConsumerSettlementDocument

        /// <summary>
        /// Indicates whether the ConsumerSettlementDocument is cached.
        /// </summary>
        bool ConsumerSettlementDocumentCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerSettlementDocument objects
        /// </summary>
        List<OpConsumerSettlementDocument> GetAllConsumerSettlementDocument();


        /// <summary>
        /// Gets ConsumerSettlementDocument by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementDocument Id</param>
        /// <returns>ConsumerSettlementDocument object</returns>
        OpConsumerSettlementDocument GetConsumerSettlementDocument(long Id);

        /// <summary>
        /// Gets ConsumerSettlementDocument by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementDocument Ids</param>
        /// <returns>ConsumerSettlementDocument list</returns>
        List<OpConsumerSettlementDocument> GetConsumerSettlementDocument(long[] Ids);

        /// <summary>
        /// Gets ConsumerSettlementDocument by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementDocument Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementDocument object</returns>
        OpConsumerSettlementDocument GetConsumerSettlementDocument(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerSettlementDocument by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementDocument Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementDocument list</returns>
        List<OpConsumerSettlementDocument> GetConsumerSettlementDocument(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerSettlementDocument object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerSettlementDocument to save</param>
        /// <returns>ConsumerSettlementDocument Id</returns>
        long SaveConsumerSettlementDocument(OpConsumerSettlementDocument toBeSaved);

        /// <summary>
        /// Deletes the ConsumerSettlementDocument object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerSettlementDocument to delete</param>
        void DeleteConsumerSettlementDocument(OpConsumerSettlementDocument toBeDeleted);

        /// <summary>
        /// Clears the ConsumerSettlementDocument object cache
        /// </summary>
        void ClearConsumerSettlementDocumentCache();
        #endregion

        #region ConsumerSettlementError

        /// <summary>
        /// Indicates whether the ConsumerSettlementError is cached.
        /// </summary>
        bool ConsumerSettlementErrorCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerSettlementError objects
        /// </summary>
        List<OpConsumerSettlementError> GetAllConsumerSettlementError();


        /// <summary>
        /// Gets ConsumerSettlementError by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementError Id</param>
        /// <returns>ConsumerSettlementError object</returns>
        OpConsumerSettlementError GetConsumerSettlementError(int Id);

        /// <summary>
        /// Gets ConsumerSettlementError by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementError Ids</param>
        /// <returns>ConsumerSettlementError list</returns>
        List<OpConsumerSettlementError> GetConsumerSettlementError(int[] Ids);

        /// <summary>
        /// Gets ConsumerSettlementError by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementError Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementError object</returns>
        OpConsumerSettlementError GetConsumerSettlementError(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerSettlementError by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementError Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementError list</returns>
        List<OpConsumerSettlementError> GetConsumerSettlementError(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerSettlementError object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerSettlementError to save</param>
        /// <returns>ConsumerSettlementError Id</returns>
        int SaveConsumerSettlementError(OpConsumerSettlementError toBeSaved);

        /// <summary>
        /// Deletes the ConsumerSettlementError object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerSettlementError to delete</param>
        void DeleteConsumerSettlementError(OpConsumerSettlementError toBeDeleted);

        /// <summary>
        /// Clears the ConsumerSettlementError object cache
        /// </summary>
        void ClearConsumerSettlementErrorCache();
        #endregion

        #region ConsumerSettlementErrorType

        /// <summary>
        /// Indicates whether the ConsumerSettlementErrorType is cached.
        /// </summary>
        bool ConsumerSettlementErrorTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerSettlementErrorType objects
        /// </summary>
        List<OpConsumerSettlementErrorType> GetAllConsumerSettlementErrorType();


        /// <summary>
        /// Gets ConsumerSettlementErrorType by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementErrorType Id</param>
        /// <returns>ConsumerSettlementErrorType object</returns>
        OpConsumerSettlementErrorType GetConsumerSettlementErrorType(int Id);

        /// <summary>
        /// Gets ConsumerSettlementErrorType by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementErrorType Ids</param>
        /// <returns>ConsumerSettlementErrorType list</returns>
        List<OpConsumerSettlementErrorType> GetConsumerSettlementErrorType(int[] Ids);

        /// <summary>
        /// Gets ConsumerSettlementErrorType by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementErrorType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementErrorType object</returns>
        OpConsumerSettlementErrorType GetConsumerSettlementErrorType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerSettlementErrorType by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementErrorType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementErrorType list</returns>
        List<OpConsumerSettlementErrorType> GetConsumerSettlementErrorType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerSettlementErrorType object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerSettlementErrorType to save</param>
        /// <returns>ConsumerSettlementErrorType Id</returns>
        int SaveConsumerSettlementErrorType(OpConsumerSettlementErrorType toBeSaved);

        /// <summary>
        /// Deletes the ConsumerSettlementErrorType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerSettlementErrorType to delete</param>
        void DeleteConsumerSettlementErrorType(OpConsumerSettlementErrorType toBeDeleted);

        /// <summary>
        /// Clears the ConsumerSettlementErrorType object cache
        /// </summary>
        void ClearConsumerSettlementErrorTypeCache();
        #endregion

        #region ConsumerSettlementReason

        /// <summary>
        /// Indicates whether the ConsumerSettlementReason is cached.
        /// </summary>
        bool ConsumerSettlementReasonCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerSettlementReason objects
        /// </summary>
        List<OpConsumerSettlementReason> GetAllConsumerSettlementReason();

        /// <summary>
        /// Gets ConsumerSettlementReason by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementReason Id</param>
        /// <returns>ConsumerSettlementReason object</returns>
        OpConsumerSettlementReason GetConsumerSettlementReason(int Id);

        /// <summary>
        /// Gets ConsumerSettlementReason by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementReason Ids</param>
        /// <returns>ConsumerSettlementReason list</returns>
        List<OpConsumerSettlementReason> GetConsumerSettlementReason(int[] Ids);

        /// <summary>
        /// Gets ConsumerSettlementReason by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementReason Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementReason object</returns>
        OpConsumerSettlementReason GetConsumerSettlementReason(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerSettlementReason by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementReason Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementReason list</returns>
        List<OpConsumerSettlementReason> GetConsumerSettlementReason(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ConsumerSettlementReason object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerSettlementReason to save</param>
        /// <returns>ConsumerSettlementReason Id</returns>
        int SaveConsumerSettlementReason(OpConsumerSettlementReason toBeSaved);

        /// <summary>
        /// Deletes the ConsumerSettlementReason object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerSettlementReason to delete</param>
        void DeleteConsumerSettlementReason(OpConsumerSettlementReason toBeDeleted);

        /// <summary>
        /// Clears the ConsumerSettlementReason object cache
        /// </summary>
        void ClearConsumerSettlementReasonCache();
        #endregion

        #region ConsumerSettlementStatus

        /// <summary>
        /// Indicates whether the ConsumerSettlementStatus is cached.
        /// </summary>
        bool ConsumerSettlementStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerSettlementStatus objects
        /// </summary>
        List<OpConsumerSettlementStatus> GetAllConsumerSettlementStatus();

        /// <summary>
        /// Gets ConsumerSettlementStatus by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementStatus Id</param>
        /// <returns>ConsumerSettlementStatus object</returns>
        OpConsumerSettlementStatus GetConsumerSettlementStatus(int Id);

        /// <summary>
        /// Gets ConsumerSettlementStatus by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementStatus Ids</param>
        /// <returns>ConsumerSettlementStatus list</returns>
        List<OpConsumerSettlementStatus> GetConsumerSettlementStatus(int[] Ids);

        /// <summary>
        /// Gets ConsumerSettlementStatus by id
        /// </summary>
        /// <param name="Id">ConsumerSettlementStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementStatus object</returns>
        OpConsumerSettlementStatus GetConsumerSettlementStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerSettlementStatus by id
        /// </summary>
        /// <param name="Ids">ConsumerSettlementStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerSettlementStatus list</returns>
        List<OpConsumerSettlementStatus> GetConsumerSettlementStatus(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ConsumerSettlementStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerSettlementStatus to save</param>
        /// <returns>ConsumerSettlementStatus Id</returns>
        int SaveConsumerSettlementStatus(OpConsumerSettlementStatus toBeSaved);

        /// <summary>
        /// Deletes the ConsumerSettlementStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerSettlementStatus to delete</param>
        void DeleteConsumerSettlementStatus(OpConsumerSettlementStatus toBeDeleted);

        /// <summary>
        /// Clears the ConsumerSettlementStatus object cache
        /// </summary>
        void ClearConsumerSettlementStatusCache();
        #endregion

        #region Content

        /// <summary>
        /// Indicates whether the Content is cached.
        /// </summary>
        bool ContentCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Content objects
        /// </summary>
        List<OpContent> GetAllContent();


        /// <summary>
        /// Gets Content by id
        /// </summary>
        /// <param name="Id">Content Id</param>
        /// <returns>Content object</returns>
        OpContent GetContent(int Id);

        /// <summary>
        /// Gets Content by id
        /// </summary>
        /// <param name="Ids">Content Ids</param>
        /// <returns>Content list</returns>
        List<OpContent> GetContent(int[] Ids);

        /// <summary>
        /// Gets Content by id
        /// </summary>
        /// <param name="Id">Content Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Content object</returns>
        OpContent GetContent(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Content by id
        /// </summary>
        /// <param name="Ids">Content Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Content list</returns>
        List<OpContent> GetContent(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Content object to the database
        /// </summary>
        /// <param name="toBeSaved">Content to save</param>
        /// <returns>Content Id</returns>
        int SaveContent(OpContent toBeSaved);

        /// <summary>
        /// Saves the Content object to the database
        /// </summary>
        /// <param name="toBeSaved">Content to save</param>
        /// <returns>Content Id</returns>
        int SaveContent2(OpContent toBeSaved);

        /// <summary>
        /// Saves the Content object to the database (by idModule and idContent)
        /// </summary>
        /// <param name="toBeSaved">Content to save</param>
        /// <returns>Content Id</returns>
        int SaveContentByModule(OpContent toBeSaved);

        /// <summary>
        /// Deletes the Content object from the database
        /// </summary>
        /// <param name="toBeDeleted">Content to delete</param>
        void DeleteContent(OpContent toBeDeleted);

        /// <summary>
        /// Clears the Content object cache
        /// </summary>
        void ClearContentCache();
        #endregion

        #region Contract

        /// <summary>
        /// Indicates whether the Contract is cached.
        /// </summary>
        bool ContractCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Contract objects
        /// </summary>
        List<OpContract> GetAllContract(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Contract by id
        /// </summary>
        /// <param name="Id">Contract Id</param>
        /// <returns>Contract object</returns>
        OpContract GetContract(int Id);

        /// <summary>
        /// Gets Contract by id
        /// </summary>
        /// <param name="Ids">Contract Ids</param>
        /// <returns>Contract list</returns>
        List<OpContract> GetContract(int[] Ids);

        /// <summary>
        /// Gets Contract by id
        /// </summary>
        /// <param name="Id">Contract Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Contract object</returns>
        OpContract GetContract(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Contract by id
        /// </summary>
        /// <param name="Ids">Contract Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Contract list</returns>
        List<OpContract> GetContract(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Contract object to the database
        /// </summary>
        /// <param name="toBeSaved">Contract to save</param>
        /// <returns>Contract Id</returns>
        int SaveContract(OpContract toBeSaved);

        /// <summary>
        /// Deletes the Contract object from the database
        /// </summary>
        /// <param name="toBeDeleted">Contract to delete</param>
        void DeleteContract(OpContract toBeDeleted);

        /// <summary>
        /// Clears the Contract object cache
        /// </summary>
        void ClearContractCache();
        #endregion

        #region ContractData

        /// <summary>
        /// Indicates whether the ContractData is cached.
        /// </summary>
        bool ContractDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ContractData objects
        /// </summary>
        List<OpContractData> GetAllContractData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ContractData by id
        /// </summary>
        /// <param name="Id">ContractData Id</param>
        /// <returns>ContractData object</returns>
        OpContractData GetContractData(long Id);

        /// <summary>
        /// Gets ContractData by id
        /// </summary>
        /// <param name="Ids">ContractData Ids</param>
        /// <returns>ContractData list</returns>
        List<OpContractData> GetContractData(long[] Ids);

        /// <summary>
        /// Gets ContractData by id
        /// </summary>
        /// <param name="Id">ContractData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ContractData object</returns>
        OpContractData GetContractData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ContractData by id
        /// </summary>
        /// <param name="Ids">ContractData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ContractData list</returns>
        List<OpContractData> GetContractData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ContractData object to the database
        /// </summary>
        /// <param name="toBeSaved">ContractData to save</param>
        /// <returns>ContractData Id</returns>
        long SaveContractData(OpContractData toBeSaved);

        /// <summary>
        /// Deletes the ContractData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ContractData to delete</param>
        void DeleteContractData(OpContractData toBeDeleted);

        /// <summary>
        /// Clears the ContractData object cache
        /// </summary>
        void ClearContractDataCache();
        #endregion

        #region Conversion

        /// <summary>
        /// Indicates whether the Conversion is cached.
        /// </summary>
        bool ConversionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Conversion objects
        /// </summary>
        List<OpConversion> GetAllConversion();


        /// <summary>
        /// Gets Conversion by id
        /// </summary>
        /// <param name="Id">Conversion Id</param>
        /// <returns>Conversion object</returns>
        OpConversion GetConversion(long Id);

        /// <summary>
        /// Gets Conversion by id
        /// </summary>
        /// <param name="Ids">Conversion Ids</param>
        /// <returns>Conversion list</returns>
        List<OpConversion> GetConversion(long[] Ids);

        /// <summary>
        /// Gets Conversion by id
        /// </summary>
        /// <param name="Id">Conversion Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Conversion object</returns>
        OpConversion GetConversion(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Conversion by id
        /// </summary>
        /// <param name="Ids">Conversion Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Conversion list</returns>
        List<OpConversion> GetConversion(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Conversion object to the database
        /// </summary>
        /// <param name="toBeSaved">Conversion to save</param>
        /// <returns>Conversion Id</returns>
        long SaveConversion(OpConversion toBeSaved);

        /// <summary>
        /// Deletes the Conversion object from the database
        /// </summary>
        /// <param name="toBeDeleted">Conversion to delete</param>
        void DeleteConversion(OpConversion toBeDeleted);

        /// <summary>
        /// Clears the Conversion object cache
        /// </summary>
        void ClearConversionCache();
        #endregion

        #region Currency

        /// <summary>
        /// Gets all Currency objects
        /// </summary>
        List<OpCurrency> Currency { get; }

        /// <summary>
        /// Indicates whether the Currency is cached.
        /// </summary>
        bool CurrencyCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Currency objects
        /// </summary>
        List<OpCurrency> GetAllCurrency();

        /// <summary>
        /// Gets Currency by id
        /// </summary>
        /// <param name="Id">Currency Id</param>
        /// <returns>Currency object</returns>
        OpCurrency GetCurrency(int Id);

        /// <summary>
        /// Gets Currency by id
        /// </summary>
        /// <param name="Ids">Currency Ids</param>
        /// <returns>Currency list</returns>
        List<OpCurrency> GetCurrency(int[] Ids);

        /// <summary>
        /// Gets Currency by id
        /// </summary>
        /// <param name="Id">Currency Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Currency object</returns>
        OpCurrency GetCurrency(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Currency by id
        /// </summary>
        /// <param name="Ids">Currency Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Currency list</returns>
        List<OpCurrency> GetCurrency(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Currency object to the database
        /// </summary>
        /// <param name="toBeSaved">Currency to save</param>
        /// <returns>Currency Id</returns>
        int SaveCurrency(OpCurrency toBeSaved);

        /// <summary>
        /// Deletes the Currency object from the database
        /// </summary>
        /// <param name="toBeDeleted">Currency to delete</param>
        void DeleteCurrency(OpCurrency toBeDeleted);

        /// <summary>
        /// Clears the Currency object cache
        /// </summary>
        void ClearCurrencyCache();
        #endregion

        #region Data

        /// <summary>
        /// Indicates whether the Data is cached.
        /// </summary>
        bool DataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Data objects
        /// </summary>
        List<OpData> GetAllData();


        /// <summary>
        /// Gets Data by id
        /// </summary>
        /// <param name="Id">Data Id</param>
        /// <returns>Data object</returns>
        OpData GetData(long Id);

        /// <summary>
        /// Gets Data by id
        /// </summary>
        /// <param name="Ids">Data Ids</param>
        /// <returns>Data list</returns>
        List<OpData> GetData(long[] Ids);

        /// <summary>
        /// Gets Data by id
        /// </summary>
        /// <param name="Id">Data Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Data object</returns>
        OpData GetData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Data by id
        /// </summary>
        /// <param name="Ids">Data Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Data list</returns>
        List<OpData> GetData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the Data object cache
        /// </summary>
        void ClearDataCache();
        #endregion

        #region DataChange

        /// <summary>
        /// Indicates whether the DataChange is cached.
        /// </summary>
        bool DataChangeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataChange objects
        /// </summary>
        List<OpDataChange> GetAllDataChange();


        /// <summary>
        /// Gets DataChange by id
        /// </summary>
        /// <param name="Id">DataChange Id</param>
        /// <returns>DataChange object</returns>
        OpDataChange GetDataChange(long Id);

        /// <summary>
        /// Gets DataChange by id
        /// </summary>
        /// <param name="Ids">DataChange Ids</param>
        /// <returns>DataChange list</returns>
        List<OpDataChange> GetDataChange(long[] Ids);

        /// <summary>
        /// Gets DataChange by id
        /// </summary>
        /// <param name="Id">DataChange Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataChange object</returns>
        OpDataChange GetDataChange(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DataChange by id
        /// </summary>
        /// <param name="Ids">DataChange Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataChange list</returns>
        List<OpDataChange> GetDataChange(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataChange object to the database
        /// </summary>
        /// <param name="toBeSaved">DataChange to save</param>
        /// <returns>DataChange Id</returns>
        long SaveDataChange(OpDataChange toBeSaved);

        /// <summary>
        /// Deletes the DataChange object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataChange to delete</param>
        void DeleteDataChange(OpDataChange toBeDeleted);

        /// <summary>
        /// Clears the DataChange object cache
        /// </summary>
        void ClearDataChangeCache();
        #endregion

        #region DataFormatGroup

        /// <summary>
        /// Indicates whether the DataFormatGroup is cached.
        /// </summary>
        bool DataFormatGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataFormatGroup objects
        /// </summary>
        List<OpDataFormatGroup> GetAllDataFormatGroup();


        /// <summary>
        /// Gets DataFormatGroup by id
        /// </summary>
        /// <param name="Id">DataFormatGroup Id</param>
        /// <returns>DataFormatGroup object</returns>
        OpDataFormatGroup GetDataFormatGroup(int Id);

        /// <summary>
        /// Gets DataFormatGroup by id
        /// </summary>
        /// <param name="Ids">DataFormatGroup Ids</param>
        /// <returns>DataFormatGroup list</returns>
        List<OpDataFormatGroup> GetDataFormatGroup(int[] Ids);

        /// <summary>
        /// Gets DataFormatGroup by id
        /// </summary>
        /// <param name="Id">DataFormatGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataFormatGroup object</returns>
        OpDataFormatGroup GetDataFormatGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataFormatGroup by id
        /// </summary>
        /// <param name="Ids">DataFormatGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataFormatGroup list</returns>
        List<OpDataFormatGroup> GetDataFormatGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataFormatGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DataFormatGroup to save</param>
        /// <returns>DataFormatGroup Id</returns>
        int SaveDataFormatGroup(OpDataFormatGroup toBeSaved);

        /// <summary>
        /// Deletes the DataFormatGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataFormatGroup to delete</param>
        void DeleteDataFormatGroup(OpDataFormatGroup toBeDeleted);

        /// <summary>
        /// Clears the DataFormatGroup object cache
        /// </summary>
        void ClearDataFormatGroupCache();
        #endregion

        #region DataType

        /// <summary>
        /// Indicates whether the DataType is cached.
        /// </summary>
        bool DataTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataType objects
        /// </summary>
        List<OpDataType> GetAllDataType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DataType by id
        /// </summary>
        /// <param name="Id">DataType Id</param>
        /// <returns>DataType object</returns>
        OpDataType GetDataType(long Id);

        /// <summary>
        /// Gets DataType by id
        /// </summary>
        /// <param name="Ids">DataType Ids</param>
        /// <returns>DataType list</returns>
        List<OpDataType> GetDataType(long[] Ids);

        /// <summary>
        /// Gets DataType by id
        /// </summary>
        /// <param name="Id">DataType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataType object</returns>
        OpDataType GetDataType(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DataType by id
        /// </summary>
        /// <param name="Ids">DataType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataType list</returns>
        List<OpDataType> GetDataType(long[] Ids, bool queryDatabase, bool loadNavigationProperties = true, bool mergeIntoCache = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DataType object to the database
        /// </summary>
        /// <param name="toBeSaved">DataType to save</param>
        /// <returns>DataType Id</returns>
        long SaveDataType(OpDataType toBeSaved);

        /// <summary>
        /// Deletes the DataType object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataType to delete</param>
        void DeleteDataType(OpDataType toBeDeleted);

        /// <summary>
        /// Clears the DataType object cache
        /// </summary>
        void ClearDataTypeCache();
        #endregion

        #region DataTypeClass

        /// <summary>
        /// Indicates whether the DataTypeClass is cached.
        /// </summary>
        bool DataTypeClassCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataTypeClass objects
        /// </summary>
        List<OpDataTypeClass> GetAllDataTypeClass(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DataTypeClass by id
        /// </summary>
        /// <param name="Id">DataTypeClass Id</param>
        /// <returns>DataTypeClass object</returns>
        OpDataTypeClass GetDataTypeClass(int Id);

        /// <summary>
        /// Gets DataTypeClass by id
        /// </summary>
        /// <param name="Ids">DataTypeClass Ids</param>
        /// <returns>DataTypeClass list</returns>
        List<OpDataTypeClass> GetDataTypeClass(int[] Ids);

        /// <summary>
        /// Gets DataTypeClass by id
        /// </summary>
        /// <param name="Id">DataTypeClass Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeClass object</returns>
        OpDataTypeClass GetDataTypeClass(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataTypeClass by id
        /// </summary>
        /// <param name="Ids">DataTypeClass Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeClass list</returns>
        List<OpDataTypeClass> GetDataTypeClass(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DataTypeClass object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTypeClass to save</param>
        /// <returns>DataTypeClass Id</returns>
        int SaveDataTypeClass(OpDataTypeClass toBeSaved);

        /// <summary>
        /// Deletes the DataTypeClass object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTypeClass to delete</param>
        void DeleteDataTypeClass(OpDataTypeClass toBeDeleted);

        /// <summary>
        /// Clears the DataTypeClass object cache
        /// </summary>
        void ClearDataTypeClassCache();
        #endregion

        #region DataTypeFormat

        /// <summary>
        /// Indicates whether the DataTypeFormat is cached.
        /// </summary>
        bool DataTypeFormatCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataTypeFormat objects
        /// </summary>
        List<OpDataTypeFormat> GetAllDataTypeFormat(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DataTypeFormat by id
        /// </summary>
        /// <param name="Id">DataTypeFormat Id</param>
        /// <returns>DataTypeFormat object</returns>
        OpDataTypeFormat GetDataTypeFormat(int Id);

        /// <summary>
        /// Gets DataTypeFormat by id
        /// </summary>
        /// <param name="Ids">DataTypeFormat Ids</param>
        /// <returns>DataTypeFormat list</returns>
        List<OpDataTypeFormat> GetDataTypeFormat(int[] Ids);

        /// <summary>
        /// Gets DataTypeFormat by id
        /// </summary>
        /// <param name="Id">DataTypeFormat Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeFormat object</returns>
        OpDataTypeFormat GetDataTypeFormat(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataTypeFormat by id
        /// </summary>
        /// <param name="Ids">DataTypeFormat Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeFormat list</returns>
        List<OpDataTypeFormat> GetDataTypeFormat(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DataTypeFormat object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTypeFormat to save</param>
        /// <returns>DataTypeFormat Id</returns>
        int SaveDataTypeFormat(OpDataTypeFormat toBeSaved);

        /// <summary>
        /// Deletes the DataTypeFormat object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTypeFormat to delete</param>
        void DeleteDataTypeFormat(OpDataTypeFormat toBeDeleted);

        /// <summary>
        /// Clears the DataTypeFormat object cache
        /// </summary>
        void ClearDataTypeFormatCache();
        #endregion

        #region DataTypeGroup

        /// <summary>
        /// Indicates whether the DataTypeGroup is cached.
        /// </summary>
        bool DataTypeGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataTypeGroup objects
        /// </summary>
        List<OpDataTypeGroup> GetAllDataTypeGroup();


        /// <summary>
        /// Gets DataTypeGroup by id
        /// </summary>
        /// <param name="Id">DataTypeGroup Id</param>
        /// <returns>DataTypeGroup object</returns>
        OpDataTypeGroup GetDataTypeGroup(int Id);

        /// <summary>
        /// Gets DataTypeGroup by id
        /// </summary>
        /// <param name="Ids">DataTypeGroup Ids</param>
        /// <returns>DataTypeGroup list</returns>
        List<OpDataTypeGroup> GetDataTypeGroup(int[] Ids);

        /// <summary>
        /// Gets DataTypeGroup by id
        /// </summary>
        /// <param name="Id">DataTypeGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeGroup object</returns>
        OpDataTypeGroup GetDataTypeGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataTypeGroup by id
        /// </summary>
        /// <param name="Ids">DataTypeGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeGroup list</returns>
        List<OpDataTypeGroup> GetDataTypeGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataTypeGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTypeGroup to save</param>
        /// <returns>DataTypeGroup Id</returns>
        int SaveDataTypeGroup(OpDataTypeGroup toBeSaved);

        /// <summary>
        /// Deletes the DataTypeGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTypeGroup to delete</param>
        void DeleteDataTypeGroup(OpDataTypeGroup toBeDeleted);

        /// <summary>
        /// Clears the DataTypeGroup object cache
        /// </summary>
        void ClearDataTypeGroupCache();
        #endregion

        #region DataTypeGroupType

        /// <summary>
        /// Indicates whether the DataTypeGroupType is cached.
        /// </summary>
        bool DataTypeGroupTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataTypeGroupType objects
        /// </summary>
        List<OpDataTypeGroupType> GetAllDataTypeGroupType();


        /// <summary>
        /// Gets DataTypeGroupType by id
        /// </summary>
        /// <param name="Id">DataTypeGroupType Id</param>
        /// <returns>DataTypeGroupType object</returns>
        OpDataTypeGroupType GetDataTypeGroupType(int Id);

        /// <summary>
        /// Gets DataTypeGroupType by id
        /// </summary>
        /// <param name="Ids">DataTypeGroupType Ids</param>
        /// <returns>DataTypeGroupType list</returns>
        List<OpDataTypeGroupType> GetDataTypeGroupType(int[] Ids);

        /// <summary>
        /// Gets DataTypeGroupType by id
        /// </summary>
        /// <param name="Id">DataTypeGroupType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeGroupType object</returns>
        OpDataTypeGroupType GetDataTypeGroupType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataTypeGroupType by id
        /// </summary>
        /// <param name="Ids">DataTypeGroupType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeGroupType list</returns>
        List<OpDataTypeGroupType> GetDataTypeGroupType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataTypeGroupType object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTypeGroupType to save</param>
        /// <returns>DataTypeGroupType Id</returns>
        int SaveDataTypeGroupType(OpDataTypeGroupType toBeSaved);

        /// <summary>
        /// Deletes the DataTypeGroupType object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTypeGroupType to delete</param>
        void DeleteDataTypeGroupType(OpDataTypeGroupType toBeDeleted);

        /// <summary>
        /// Clears the DataTypeGroupType object cache
        /// </summary>
        void ClearDataTypeGroupTypeCache();
        #endregion

        #region DataTransfer

        /// <summary>
        /// Indicates whether the DataTransfer is cached.
        /// </summary>
        bool DataTransferCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataTransfer objects
        /// </summary>
        List<OpDataTransfer> GetAllDataTransfer();


        /// <summary>
        /// Gets DataTransfer by id
        /// </summary>
        /// <param name="Id">DataTransfer Id</param>
        /// <returns>DataTransfer object</returns>
        OpDataTransfer GetDataTransfer(long Id);

        /// <summary>
        /// Gets DataTransfer by id
        /// </summary>
        /// <param name="Ids">DataTransfer Ids</param>
        /// <returns>DataTransfer list</returns>
        List<OpDataTransfer> GetDataTransfer(long[] Ids);

        /// <summary>
        /// Gets DataTransfer by id
        /// </summary>
        /// <param name="Id">DataTransfer Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTransfer object</returns>
        OpDataTransfer GetDataTransfer(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DataTransfer by id
        /// </summary>
        /// <param name="Ids">DataTransfer Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTransfer list</returns>
        List<OpDataTransfer> GetDataTransfer(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Deletes the DataTransfer object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTransfer to delete</param>
        void DeleteDataTransfer(OpDataTransfer toBeDeleted);

        /// <summary>
        /// Clears the DataTransfer object cache
        /// </summary>
        void ClearDataTransferCache();
        #endregion

        #region DaylightSavingTime

        /// <summary>
        /// Indicates whether the DaylightSavingTime is cached.
        /// </summary>
        bool DaylightSavingTimeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DaylightSavingTime objects
        /// </summary>
        List<OpDaylightSavingTime> GetAllDaylightSavingTime();


        /// <summary>
        /// Gets DaylightSavingTime by id
        /// </summary>
        /// <param name="Id">DaylightSavingTime Id</param>
        /// <returns>DaylightSavingTime object</returns>
        OpDaylightSavingTime GetDaylightSavingTime(int Id);

        /// <summary>
        /// Gets DaylightSavingTime by id
        /// </summary>
        /// <param name="Ids">DaylightSavingTime Ids</param>
        /// <returns>DaylightSavingTime list</returns>
        List<OpDaylightSavingTime> GetDaylightSavingTime(int[] Ids);

        /// <summary>
        /// Gets DaylightSavingTime by id
        /// </summary>
        /// <param name="Id">DaylightSavingTime Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DaylightSavingTime object</returns>
        OpDaylightSavingTime GetDaylightSavingTime(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DaylightSavingTime by id
        /// </summary>
        /// <param name="Ids">DaylightSavingTime Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DaylightSavingTime list</returns>
        List<OpDaylightSavingTime> GetDaylightSavingTime(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DaylightSavingTime object to the database
        /// </summary>
        /// <param name="toBeSaved">DaylightSavingTime to save</param>
        /// <returns>DaylightSavingTime Id</returns>
        int SaveDaylightSavingTime(OpDaylightSavingTime toBeSaved);

        /// <summary>
        /// Deletes the DaylightSavingTime object from the database
        /// </summary>
        /// <param name="toBeDeleted">DaylightSavingTime to delete</param>
        void DeleteDaylightSavingTime(OpDaylightSavingTime toBeDeleted);

        /// <summary>
        /// Clears the DaylightSavingTime object cache
        /// </summary>
        void ClearDaylightSavingTimeCache();
        #endregion

        #region Delivery

        /// <summary>
        /// Indicates whether the Delivery is cached.
        /// </summary>
        bool DeliveryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Delivery objects
        /// </summary>
        List<OpDelivery> GetAllDelivery();


        /// <summary>
        /// Gets Delivery by id
        /// </summary>
        /// <param name="Id">Delivery Id</param>
        /// <returns>Delivery object</returns>
        OpDelivery GetDelivery(int Id);

        /// <summary>
        /// Gets Delivery by id
        /// </summary>
        /// <param name="Ids">Delivery Ids</param>
        /// <returns>Delivery list</returns>
        List<OpDelivery> GetDelivery(int[] Ids);

        /// <summary>
        /// Gets Delivery by id
        /// </summary>
        /// <param name="Id">Delivery Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Delivery object</returns>
        OpDelivery GetDelivery(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Delivery by id
        /// </summary>
        /// <param name="Ids">Delivery Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Delivery list</returns>
        List<OpDelivery> GetDelivery(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Delivery object to the database
        /// </summary>
        /// <param name="toBeSaved">Delivery to save</param>
        /// <returns>Delivery Id</returns>
        int SaveDelivery(OpDelivery toBeSaved);

        /// <summary>
        /// Deletes the Delivery object from the database
        /// </summary>
        /// <param name="toBeDeleted">Delivery to delete</param>
        void DeleteDelivery(OpDelivery toBeDeleted);

        /// <summary>
        /// Clears the Delivery object cache
        /// </summary>
        void ClearDeliveryCache();
        #endregion

        #region DepositoryElement

        /// <summary>
        /// Indicates whether the DepositoryElement is cached.
        /// </summary>
        bool DepositoryElementCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DepositoryElement objects
        /// </summary>
        List<OpDepositoryElement> GetAllDepositoryElement();


        /// <summary>
        /// Gets DepositoryElement by id
        /// </summary>
        /// <param name="Id">DepositoryElement Id</param>
        /// <returns>DepositoryElement object</returns>
        OpDepositoryElement GetDepositoryElement(int Id);

        /// <summary>
        /// Gets DepositoryElement by id
        /// </summary>
        /// <param name="Ids">DepositoryElement Ids</param>
        /// <returns>DepositoryElement list</returns>
        List<OpDepositoryElement> GetDepositoryElement(int[] Ids);

        /// <summary>
        /// Gets DepositoryElement by id
        /// </summary>
        /// <param name="Id">DepositoryElement Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DepositoryElement object</returns>
        OpDepositoryElement GetDepositoryElement(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DepositoryElement by id
        /// </summary>
        /// <param name="Ids">DepositoryElement Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DepositoryElement list</returns>
        List<OpDepositoryElement> GetDepositoryElement(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DepositoryElement object to the database
        /// </summary>
        /// <param name="toBeSaved">DepositoryElement to save</param>
        /// <returns>DepositoryElement Id</returns>
        int SaveDepositoryElement(OpDepositoryElement toBeSaved);

        /// <summary>
        /// Deletes the DepositoryElement object from the database
        /// </summary>
        /// <param name="toBeDeleted">DepositoryElement to delete</param>
        void DeleteDepositoryElement(OpDepositoryElement toBeDeleted);

        /// <summary>
        /// Clears the DepositoryElement object cache
        /// </summary>
        void ClearDepositoryElementCache();
        #endregion

        #region DepositoryElementData

        /// <summary>
        /// Indicates whether the DepositoryElementData is cached.
        /// </summary>
        bool DepositoryElementDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DepositoryElementData objects
        /// </summary>
        List<OpDepositoryElementData> GetAllDepositoryElementData();


        /// <summary>
        /// Gets DepositoryElementData by id
        /// </summary>
        /// <param name="Id">DepositoryElementData Id</param>
        /// <returns>DepositoryElementData object</returns>
        OpDepositoryElementData GetDepositoryElementData(long Id);

        /// <summary>
        /// Gets DepositoryElementData by id
        /// </summary>
        /// <param name="Ids">DepositoryElementData Ids</param>
        /// <returns>DepositoryElementData list</returns>
        List<OpDepositoryElementData> GetDepositoryElementData(long[] Ids);

        /// <summary>
        /// Gets DepositoryElementData by id
        /// </summary>
        /// <param name="Id">DepositoryElementData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DepositoryElementData object</returns>
        OpDepositoryElementData GetDepositoryElementData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DepositoryElementData by id
        /// </summary>
        /// <param name="Ids">DepositoryElementData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DepositoryElementData list</returns>
        List<OpDepositoryElementData> GetDepositoryElementData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DepositoryElementData object to the database
        /// </summary>
        /// <param name="toBeSaved">DepositoryElementData to save</param>
        /// <returns>DepositoryElementData Id</returns>
        long SaveDepositoryElementData(OpDepositoryElementData toBeSaved);

        /// <summary>
        /// Deletes the DepositoryElementData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DepositoryElementData to delete</param>
        void DeleteDepositoryElementData(OpDepositoryElementData toBeDeleted);

        /// <summary>
        /// Clears the DepositoryElementData object cache
        /// </summary>
        void ClearDepositoryElementDataCache();
        #endregion

        #region Depot

        /// <summary>
        /// Indicates whether the Depot is cached.
        /// </summary>
        bool DepotCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Depot objects
        /// </summary>
        List<OpDepot> GetAllDepot();


        /// <summary>
        /// Gets Depot by id
        /// </summary>
        /// <param name="Id">Depot Id</param>
        /// <returns>Depot object</returns>
        OpDepot GetDepot(int Id);

        /// <summary>
        /// Gets Depot by id
        /// </summary>
        /// <param name="Ids">Depot Ids</param>
        /// <returns>Depot list</returns>
        List<OpDepot> GetDepot(int[] Ids);

        /// <summary>
        /// Gets Depot by id
        /// </summary>
        /// <param name="Id">Depot Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Depot object</returns>
        OpDepot GetDepot(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Depot by id
        /// </summary>
        /// <param name="Ids">Depot Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Depot list</returns>
        List<OpDepot> GetDepot(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Depot object to the database
        /// </summary>
        /// <param name="toBeSaved">Depot to save</param>
        /// <returns>Depot Id</returns>
        int SaveDepot(OpDepot toBeSaved);

        /// <summary>
        /// Deletes the Depot object from the database
        /// </summary>
        /// <param name="toBeDeleted">Depot to delete</param>
        void DeleteDepot(OpDepot toBeDeleted);

        /// <summary>
        /// Clears the Depot object cache
        /// </summary>
        void ClearDepotCache();
        #endregion

        #region Device

        /// <summary>
        /// Indicates whether the Device is cached.
        /// </summary>
        bool DeviceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Device objects
        /// </summary>
        List<OpDevice> GetAllDevice();


        /// <summary>
        /// Gets Device by id
        /// </summary>
        /// <param name="Id">Device Id</param>
        /// <returns>Device object</returns>
        OpDevice GetDevice(long Id);

        /// <summary>
        /// Gets Device by id
        /// </summary>
        /// <param name="Ids">Device Ids</param>
        /// <returns>Device list</returns>
        List<OpDevice> GetDevice(long[] Ids);

        /// <summary>
        /// Gets Device by id
        /// </summary>
        /// <param name="Id">Device Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Device object</returns>
        OpDevice GetDevice(long Id, bool queryDatabase);

        //List<OpDevice> GetDevice(long[] Ids, bool queryDatabase) in .User.cs

        /// <summary>
        /// Deletes the Device object from the database
        /// </summary>
        /// <param name="toBeDeleted">Device to delete</param>
        void DeleteDevice(OpDevice toBeDeleted);

        /// <summary>
        /// Clears the Device object cache
        /// </summary>
        void ClearDeviceCache();
        #endregion

        #region DeviceDetails

        /// <summary>
        /// Indicates whether the DeviceDetails is cached.
        /// </summary>
        bool DeviceDetailsCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceDetails objects
        /// </summary>
        List<OpDeviceDetails> GetAllDeviceDetails(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        List<OpDeviceDetails> GetAllDeviceDetailsCustom(int[] distributorFilter = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceDetails by id
        /// </summary>
        /// <param name="Id">DeviceDetails Id</param>
        /// <returns>DeviceDetails object</returns>
        OpDeviceDetails GetDeviceDetails(long Id);

        OpDeviceDetails GetDeviceDetailsCustom(long Id);

        /// <summary>
        /// Gets DeviceDetails by id
        /// </summary>
        /// <param name="Ids">DeviceDetails Ids</param>
        /// <returns>DeviceDetails list</returns>
        List<OpDeviceDetails> GetDeviceDetails(long[] Ids);

        List<OpDeviceDetails> GetDeviceDetailsCustom(long[] Ids);

        /// <summary>
        /// Gets DeviceDetails by id
        /// </summary>
        /// <param name="Id">DeviceDetails Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDetails object</returns>
        OpDeviceDetails GetDeviceDetails(long Id, bool queryDatabase);

        OpDeviceDetails GetDeviceDetailsCustom(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceDetails by id
        /// </summary>
        /// <param name="Ids">DeviceDetails Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDetails list</returns>
        List<OpDeviceDetails> GetDeviceDetails(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        List<OpDeviceDetails> GetDeviceDetailsCustom(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        List<OpDeviceDetails> GetDeviceDetailsByDistributorCustom(int[] IdDistributor, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 600);

        /// <summary>
        /// Saves the DeviceDetails object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceDetails to save</param>
        /// <returns>DeviceDetails Id</returns>
        long SaveDeviceDetails(OpDeviceDetails toBeSaved);

        /// <summary>
        /// Deletes the DeviceDetails object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceDetails to delete</param>
        void DeleteDeviceDetails(OpDeviceDetails toBeDeleted);

        /// <summary>
        /// Clears the DeviceDetails object cache
        /// </summary>
        void ClearDeviceDetailsCache();
        #endregion

        #region DeviceDistributorHistory

        /// <summary>
        /// Indicates whether the DeviceDistributorHistory is cached.
        /// </summary>
        bool DeviceDistributorHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceDistributorHistory objects
        /// </summary>
        List<OpDeviceDistributorHistory> GetAllDeviceDistributorHistory(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceDistributorHistory by id
        /// </summary>
        /// <param name="Id">DeviceDistributorHistory Id</param>
        /// <returns>DeviceDistributorHistory object</returns>
        OpDeviceDistributorHistory GetDeviceDistributorHistory(long Id);

        /// <summary>
        /// Gets DeviceDistributorHistory by id
        /// </summary>
        /// <param name="Ids">DeviceDistributorHistory Ids</param>
        /// <returns>DeviceDistributorHistory list</returns>
        List<OpDeviceDistributorHistory> GetDeviceDistributorHistory(long[] Ids);

        /// <summary>
        /// Gets DeviceDistributorHistory by id
        /// </summary>
        /// <param name="Id">DeviceDistributorHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDistributorHistory object</returns>
        OpDeviceDistributorHistory GetDeviceDistributorHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceDistributorHistory by id
        /// </summary>
        /// <param name="Ids">DeviceDistributorHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDistributorHistory list</returns>
        List<OpDeviceDistributorHistory> GetDeviceDistributorHistory(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DeviceDistributorHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceDistributorHistory to save</param>
        /// <returns>DeviceDistributorHistory Id</returns>
        long SaveDeviceDistributorHistory(OpDeviceDistributorHistory toBeSaved);

        /// <summary>
        /// Deletes the DeviceDistributorHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceDistributorHistory to delete</param>
        void DeleteDeviceDistributorHistory(OpDeviceDistributorHistory toBeDeleted);

        /// <summary>
        /// Clears the DeviceDistributorHistory object cache
        /// </summary>
        void ClearDeviceDistributorHistoryCache();
        #endregion

        #region DeviceDriver

        /// <summary>
        /// Indicates whether the DeviceDriver is cached.
        /// </summary>
        bool DeviceDriverCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceDriver objects
        /// </summary>
        List<OpDeviceDriver> GetAllDeviceDriver(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceDriver by id
        /// </summary>
        /// <param name="Id">DeviceDriver Id</param>
        /// <returns>DeviceDriver object</returns>
        OpDeviceDriver GetDeviceDriver(int Id);

        /// <summary>
        /// Gets DeviceDriver by id
        /// </summary>
        /// <param name="Ids">DeviceDriver Ids</param>
        /// <returns>DeviceDriver list</returns>
        List<OpDeviceDriver> GetDeviceDriver(int[] Ids);

        /// <summary>
        /// Gets DeviceDriver by id
        /// </summary>
        /// <param name="Id">DeviceDriver Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriver object</returns>
        OpDeviceDriver GetDeviceDriver(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceDriver by id
        /// </summary>
        /// <param name="Ids">DeviceDriver Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriver list</returns>
        List<OpDeviceDriver> GetDeviceDriver(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DeviceDriver object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceDriver to save</param>
        /// <returns>DeviceDriver Id</returns>
        int SaveDeviceDriver(OpDeviceDriver toBeSaved);

        /// <summary>
        /// Deletes the DeviceDriver object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceDriver to delete</param>
        void DeleteDeviceDriver(OpDeviceDriver toBeDeleted);

        /// <summary>
        /// Clears the DeviceDriver object cache
        /// </summary>
        void ClearDeviceDriverCache();
        #endregion

        #region DeviceDriverData

        /// <summary>
        /// Indicates whether the DeviceDriverData is cached.
        /// </summary>
        bool DeviceDriverDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceDriverData objects
        /// </summary>
        List<OpDeviceDriverData> GetAllDeviceDriverData();


        /// <summary>
        /// Gets DeviceDriverData by id
        /// </summary>
        /// <param name="Id">DeviceDriverData Id</param>
        /// <returns>DeviceDriverData object</returns>
        OpDeviceDriverData GetDeviceDriverData(long Id);

        /// <summary>
        /// Gets DeviceDriverData by id
        /// </summary>
        /// <param name="Ids">DeviceDriverData Ids</param>
        /// <returns>DeviceDriverData list</returns>
        List<OpDeviceDriverData> GetDeviceDriverData(long[] Ids);

        /// <summary>
        /// Gets DeviceDriverData by id
        /// </summary>
        /// <param name="Id">DeviceDriverData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriverData object</returns>
        OpDeviceDriverData GetDeviceDriverData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceDriverData by id
        /// </summary>
        /// <param name="Ids">DeviceDriverData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriverData list</returns>
        List<OpDeviceDriverData> GetDeviceDriverData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceDriverData object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceDriverData to save</param>
        /// <returns>DeviceDriverData Id</returns>
        long SaveDeviceDriverData(OpDeviceDriverData toBeSaved);

        /// <summary>
        /// Deletes the DeviceDriverData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceDriverData to delete</param>
        void DeleteDeviceDriverData(OpDeviceDriverData toBeDeleted);

        /// <summary>
        /// Clears the DeviceDriverData object cache
        /// </summary>
        void ClearDeviceDriverDataCache();
        #endregion

        #region DeviceDriverMemoryMap

        /// <summary>
        /// Indicates whether the DeviceDriverMemoryMap is cached.
        /// </summary>
        bool DeviceDriverMemoryMapCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceDriverMemoryMap objects
        /// </summary>
        List<OpDeviceDriverMemoryMap> GetAllDeviceDriverMemoryMap();


        /// <summary>
        /// Gets DeviceDriverMemoryMap by id
        /// </summary>
        /// <param name="Id">DeviceDriverMemoryMap Id</param>
        /// <returns>DeviceDriverMemoryMap object</returns>
        OpDeviceDriverMemoryMap GetDeviceDriverMemoryMap(int Id);

        /// <summary>
        /// Gets DeviceDriverMemoryMap by id
        /// </summary>
        /// <param name="Ids">DeviceDriverMemoryMap Ids</param>
        /// <returns>DeviceDriverMemoryMap list</returns>
        List<OpDeviceDriverMemoryMap> GetDeviceDriverMemoryMap(int[] Ids);

        /// <summary>
        /// Gets DeviceDriverMemoryMap by id
        /// </summary>
        /// <param name="Id">DeviceDriverMemoryMap Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriverMemoryMap object</returns>
        OpDeviceDriverMemoryMap GetDeviceDriverMemoryMap(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceDriverMemoryMap by id
        /// </summary>
        /// <param name="Ids">DeviceDriverMemoryMap Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriverMemoryMap list</returns>
        List<OpDeviceDriverMemoryMap> GetDeviceDriverMemoryMap(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceDriverMemoryMap object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceDriverMemoryMap to save</param>
        /// <returns>DeviceDriverMemoryMap Id</returns>
        int SaveDeviceDriverMemoryMap(OpDeviceDriverMemoryMap toBeSaved);

        /// <summary>
        /// Deletes the DeviceDriverMemoryMap object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceDriverMemoryMap to delete</param>
        void DeleteDeviceDriverMemoryMap(OpDeviceDriverMemoryMap toBeDeleted);

        /// <summary>
        /// Clears the DeviceDriverMemoryMap object cache
        /// </summary>
        void ClearDeviceDriverMemoryMapCache();
        #endregion

        #region DeviceDriverMemoryMapData

        /// <summary>
        /// Indicates whether the DeviceDriverMemoryMapData is cached.
        /// </summary>
        bool DeviceDriverMemoryMapDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceDriverMemoryMapData objects
        /// </summary>
        List<OpDeviceDriverMemoryMapData> GetAllDeviceDriverMemoryMapData();


        /// <summary>
        /// Gets DeviceDriverMemoryMapData by id
        /// </summary>
        /// <param name="Id">DeviceDriverMemoryMapData Id</param>
        /// <returns>DeviceDriverMemoryMapData object</returns>
        OpDeviceDriverMemoryMapData GetDeviceDriverMemoryMapData(long Id);

        /// <summary>
        /// Gets DeviceDriverMemoryMapData by id
        /// </summary>
        /// <param name="Ids">DeviceDriverMemoryMapData Ids</param>
        /// <returns>DeviceDriverMemoryMapData list</returns>
        List<OpDeviceDriverMemoryMapData> GetDeviceDriverMemoryMapData(long[] Ids);

        /// <summary>
        /// Gets DeviceDriverMemoryMapData by id
        /// </summary>
        /// <param name="Id">DeviceDriverMemoryMapData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriverMemoryMapData object</returns>
        OpDeviceDriverMemoryMapData GetDeviceDriverMemoryMapData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceDriverMemoryMapData by id
        /// </summary>
        /// <param name="Ids">DeviceDriverMemoryMapData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceDriverMemoryMapData list</returns>
        List<OpDeviceDriverMemoryMapData> GetDeviceDriverMemoryMapData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceDriverMemoryMapData object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceDriverMemoryMapData to save</param>
        /// <returns>DeviceDriverMemoryMapData Id</returns>
        long SaveDeviceDriverMemoryMapData(OpDeviceDriverMemoryMapData toBeSaved);

        /// <summary>
        /// Deletes the DeviceDriverMemoryMapData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceDriverMemoryMapData to delete</param>
        void DeleteDeviceDriverMemoryMapData(OpDeviceDriverMemoryMapData toBeDeleted);

        /// <summary>
        /// Clears the DeviceDriverMemoryMapData object cache
        /// </summary>
        void ClearDeviceDriverMemoryMapDataCache();
        #endregion

        #region DeviceHierarchy
        /// <summary>
        /// Indicates whether the DeviceHierarchy is cached.
        /// </summary>
        bool DeviceHierarchyCacheEnabled { get; set; }


        /// <summary>
        /// Gets all DeviceHierarchy objects
        /// </summary>
        List<OpDeviceHierarchy> GetAllDeviceHierarchy();


        /// <summary>
        /// Gets DeviceHierarchy by id
        /// </summary>
        /// <param name="Id">DeviceHierarchy Id</param>
        /// <returns>DeviceHierarchy object</returns>
        OpDeviceHierarchy GetDeviceHierarchy(long Id);

        /// <summary>
        /// Gets DeviceHierarchy by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchy Ids</param>
        /// <returns>DeviceHierarchy list</returns>
        List<OpDeviceHierarchy> GetDeviceHierarchy(long[] Ids);

        /// <summary>
        /// Gets DeviceHierarchy by id
        /// </summary>
        /// <param name="Id">DeviceHierarchy Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchy object</returns>
        OpDeviceHierarchy GetDeviceHierarchy(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceHierarchy by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchy Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchy list</returns>
        List<OpDeviceHierarchy> GetDeviceHierarchy(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Deletes the DeviceHierarchy object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceHierarchy to delete</param>
        void DeleteDeviceHierarchy(OpDeviceHierarchy toBeDeleted);

        /// <summary>
        /// Clears the DeviceHierarchy object cache
        /// </summary>
        void ClearDeviceHierarchyCache();
        #endregion

        #region DeviceHierarchyGroup

        /// <summary>
        /// Indicates whether the DeviceHierarchyGroup is cached.
        /// </summary>
        bool DeviceHierarchyGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceHierarchyGroup objects
        /// </summary>
        List<OpDeviceHierarchyGroup> GetAllDeviceHierarchyGroup();


        /// <summary>
        /// Gets DeviceHierarchyGroup by id
        /// </summary>
        /// <param name="Id">DeviceHierarchyGroup Id</param>
        /// <returns>DeviceHierarchyGroup object</returns>
        OpDeviceHierarchyGroup GetDeviceHierarchyGroup(long Id);

        /// <summary>
        /// Gets DeviceHierarchyGroup by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchyGroup Ids</param>
        /// <returns>DeviceHierarchyGroup list</returns>
        List<OpDeviceHierarchyGroup> GetDeviceHierarchyGroup(long[] Ids);

        /// <summary>
        /// Gets DeviceHierarchyGroup by id
        /// </summary>
        /// <param name="Id">DeviceHierarchyGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchyGroup object</returns>
        OpDeviceHierarchyGroup GetDeviceHierarchyGroup(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceHierarchyGroup by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchyGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchyGroup list</returns>
        List<OpDeviceHierarchyGroup> GetDeviceHierarchyGroup(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceHierarchyGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceHierarchyGroup to save</param>
        /// <returns>DeviceHierarchyGroup Id</returns>
        long SaveDeviceHierarchyGroup(OpDeviceHierarchyGroup toBeSaved);

        /// <summary>
        /// Deletes the DeviceHierarchyGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceHierarchyGroup to delete</param>
        void DeleteDeviceHierarchyGroup(OpDeviceHierarchyGroup toBeDeleted);

        /// <summary>
        /// Clears the DeviceHierarchyGroup object cache
        /// </summary>
        void ClearDeviceHierarchyGroupCache();
        #endregion

        #region DeviceHierarchyInGroup

        /// <summary>
        /// Indicates whether the DeviceHierarchyInGroup is cached.
        /// </summary>
        bool DeviceHierarchyInGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceHierarchyInGroup objects
        /// </summary>
        List<OpDeviceHierarchyInGroup> GetAllDeviceHierarchyInGroup();


        /// <summary>
        /// Gets DeviceHierarchyInGroup by id
        /// </summary>
        /// <param name="Id">DeviceHierarchyInGroup Id</param>
        /// <returns>DeviceHierarchyInGroup object</returns>
        OpDeviceHierarchyInGroup GetDeviceHierarchyInGroup(long Id);

        /// <summary>
        /// Gets DeviceHierarchyInGroup by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchyInGroup Ids</param>
        /// <returns>DeviceHierarchyInGroup list</returns>
        List<OpDeviceHierarchyInGroup> GetDeviceHierarchyInGroup(long[] Ids);

        /// <summary>
        /// Gets DeviceHierarchyInGroup by id
        /// </summary>
        /// <param name="Id">DeviceHierarchyInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchyInGroup object</returns>
        OpDeviceHierarchyInGroup GetDeviceHierarchyInGroup(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceHierarchyInGroup by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchyInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchyInGroup list</returns>
        List<OpDeviceHierarchyInGroup> GetDeviceHierarchyInGroup(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceHierarchyInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceHierarchyInGroup to save</param>
        /// <returns>DeviceHierarchyInGroup Id</returns>
        long SaveDeviceHierarchyInGroup(OpDeviceHierarchyInGroup toBeSaved);

        /// <summary>
        /// Deletes the DeviceHierarchyInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceHierarchyInGroup to delete</param>
        void DeleteDeviceHierarchyInGroup(OpDeviceHierarchyInGroup toBeDeleted);

        /// <summary>
        /// Clears the DeviceHierarchyInGroup object cache
        /// </summary>
        void ClearDeviceHierarchyInGroupCache();
        #endregion

        #region DeviceHierarchyPattern

        /// <summary>
        /// Indicates whether the DeviceHierarchyPattern is cached.
        /// </summary>
        bool DeviceHierarchyPatternCacheEnabled { get; set; }

        /// <summary>
        /// Gets DeviceHierarchyPattern by id
        /// </summary>
        /// <param name="Id">DeviceHierarchyPattern Id</param>
        /// <returns>DeviceHierarchyPattern object</returns>
        OpDeviceHierarchyPattern GetDeviceHierarchyPattern(long Id);

        /// <summary>
        /// Gets DeviceHierarchyPattern by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchyPattern Ids</param>
        /// <returns>DeviceHierarchyPattern list</returns>
        List<OpDeviceHierarchyPattern> GetDeviceHierarchyPattern(long[] Ids);

        /// <summary>
        /// Gets DeviceHierarchyPattern by id
        /// </summary>
        /// <param name="Id">DeviceHierarchyPattern Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchyPattern object</returns>
        OpDeviceHierarchyPattern GetDeviceHierarchyPattern(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceHierarchyPattern by id
        /// </summary>
        /// <param name="Ids">DeviceHierarchyPattern Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceHierarchyPattern list</returns>
        List<OpDeviceHierarchyPattern> GetDeviceHierarchyPattern(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceHierarchyPattern object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceHierarchyPattern to save</param>
        /// <returns>DeviceHierarchyPattern Id</returns>
        long SaveDeviceHierarchyPattern(OpDeviceHierarchyPattern toBeSaved);

        /// <summary>
        /// Deletes the DeviceHierarchyPattern object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceHierarchyPattern to delete</param>
        void DeleteDeviceHierarchyPattern(OpDeviceHierarchyPattern toBeDeleted);

        /// <summary>
        /// Clears the DeviceHierarchyPattern object cache
        /// </summary>
        void ClearDeviceHierarchyPatternCache();
        #endregion

        #region DeviceOrderNumber

        /// <summary>
        /// Indicates whether the DeviceOrderNumber is cached.
        /// </summary>
        bool DeviceOrderNumberCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceOrderNumber objects
        /// </summary>
        List<OpDeviceOrderNumber> GetAllDeviceOrderNumber(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceOrderNumber by id
        /// </summary>
        /// <param name="Id">DeviceOrderNumber Id</param>
        /// <returns>DeviceOrderNumber object</returns>
        OpDeviceOrderNumber GetDeviceOrderNumber(int Id);

        /// <summary>
        /// Gets DeviceOrderNumber by id
        /// </summary>
        /// <param name="Ids">DeviceOrderNumber Ids</param>
        /// <returns>DeviceOrderNumber list</returns>
        List<OpDeviceOrderNumber> GetDeviceOrderNumber(int[] Ids);

        /// <summary>
        /// Gets DeviceOrderNumber by id
        /// </summary>
        /// <param name="Id">DeviceOrderNumber Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceOrderNumber object</returns>
        OpDeviceOrderNumber GetDeviceOrderNumber(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceOrderNumber by id
        /// </summary>
        /// <param name="Ids">DeviceOrderNumber Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceOrderNumber list</returns>
        List<OpDeviceOrderNumber> GetDeviceOrderNumber(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DeviceOrderNumber object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceOrderNumber to save</param>
        /// <returns>DeviceOrderNumber Id</returns>
        int SaveDeviceOrderNumber(OpDeviceOrderNumber toBeSaved);

        /// <summary>
        /// Deletes the DeviceOrderNumber object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceOrderNumber to delete</param>
        void DeleteDeviceOrderNumber(OpDeviceOrderNumber toBeDeleted);

        /// <summary>
        /// Clears the DeviceOrderNumber object cache
        /// </summary>
        void ClearDeviceOrderNumberCache();
        #endregion

        #region DeviceOrderNumberData

        /// <summary>
        /// Indicates whether the DeviceOrderNumberData is cached.
        /// </summary>
        bool DeviceOrderNumberDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceOrderNumberData objects
        /// </summary>
        List<OpDeviceOrderNumberData> GetAllDeviceOrderNumberData();


        /// <summary>
        /// Gets DeviceOrderNumberData by id
        /// </summary>
        /// <param name="Id">DeviceOrderNumberData Id</param>
        /// <returns>DeviceOrderNumberData object</returns>
        OpDeviceOrderNumberData GetDeviceOrderNumberData(long Id);

        /// <summary>
        /// Gets DeviceOrderNumberData by id
        /// </summary>
        /// <param name="Ids">DeviceOrderNumberData Ids</param>
        /// <returns>DeviceOrderNumberData list</returns>
        List<OpDeviceOrderNumberData> GetDeviceOrderNumberData(long[] Ids);

        /// <summary>
        /// Gets DeviceOrderNumberData by id
        /// </summary>
        /// <param name="Id">DeviceOrderNumberData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceOrderNumberData object</returns>
        OpDeviceOrderNumberData GetDeviceOrderNumberData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceOrderNumberData by id
        /// </summary>
        /// <param name="Ids">DeviceOrderNumberData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceOrderNumberData list</returns>
        List<OpDeviceOrderNumberData> GetDeviceOrderNumberData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceOrderNumberData object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceOrderNumberData to save</param>
        /// <returns>DeviceOrderNumberData Id</returns>
        long SaveDeviceOrderNumberData(OpDeviceOrderNumberData toBeSaved);

        /// <summary>
        /// Deletes the DeviceOrderNumberData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceOrderNumberData to delete</param>
        void DeleteDeviceOrderNumberData(OpDeviceOrderNumberData toBeDeleted);

        /// <summary>
        /// Clears the DeviceOrderNumberData object cache
        /// </summary>
        void ClearDeviceOrderNumberDataCache();
        #endregion

        #region DeviceOrderNumberInArticle

        /// <summary>
        /// Indicates whether the DeviceOrderNumberInArticle is cached.
        /// </summary>
        bool DeviceOrderNumberInArticleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceOrderNumberInArticle objects
        /// </summary>
        List<OpDeviceOrderNumberInArticle> GetAllDeviceOrderNumberInArticle();


        /// <summary>
        /// Gets DeviceOrderNumberInArticle by id
        /// </summary>
        /// <param name="Id">DeviceOrderNumberInArticle Id</param>
        /// <returns>DeviceOrderNumberInArticle object</returns>
        OpDeviceOrderNumberInArticle GetDeviceOrderNumberInArticle(int Id);

        /// <summary>
        /// Gets DeviceOrderNumberInArticle by id
        /// </summary>
        /// <param name="Ids">DeviceOrderNumberInArticle Ids</param>
        /// <returns>DeviceOrderNumberInArticle list</returns>
        List<OpDeviceOrderNumberInArticle> GetDeviceOrderNumberInArticle(int[] Ids);

        /// <summary>
        /// Gets DeviceOrderNumberInArticle by id
        /// </summary>
        /// <param name="Id">DeviceOrderNumberInArticle Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceOrderNumberInArticle object</returns>
        OpDeviceOrderNumberInArticle GetDeviceOrderNumberInArticle(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceOrderNumberInArticle by id
        /// </summary>
        /// <param name="Ids">DeviceOrderNumberInArticle Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceOrderNumberInArticle list</returns>
        List<OpDeviceOrderNumberInArticle> GetDeviceOrderNumberInArticle(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceOrderNumberInArticle object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceOrderNumberInArticle to save</param>
        /// <returns>DeviceOrderNumberInArticle Id</returns>
        int SaveDeviceOrderNumberInArticle(OpDeviceOrderNumberInArticle toBeSaved);

        /// <summary>
        /// Saves the DeviceOrderNumberInArticle object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceOrderNumberInArticle to save</param>
        /// <returns>DeviceOrderNumberInArticle Id</returns>
        void SaveDeviceOrderNumberInArticle(long idArticle, int idDeviceOrderNumber);

        /// <summary>
        /// Deletes the DeviceOrderNumberInArticle object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceOrderNumberInArticle to delete</param>
        void DeleteDeviceOrderNumberInArticle(OpDeviceOrderNumberInArticle toBeDeleted);

        /// <summary>
        /// Clears the DeviceOrderNumberInArticle object cache
        /// </summary>
        void ClearDeviceOrderNumberInArticleCache();

        /// <summary>
        /// Deletes the DeviceOrderNumberInArticle object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceOrderNumberInArticle to delete</param>
        void DeleteDeviceOrderNumberInArticleByArticle(long idArticle);
        #endregion

        #region DevicePattern

        /// <summary>
        /// Indicates whether the DevicePattern is cached.
        /// </summary>
        bool DevicePatternCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DevicePattern objects
        /// </summary>
        List<OpDevicePattern> GetAllDevicePattern();

        /// <summary>
        /// Saves the DevicePattern object to the database
        /// </summary>
        /// <param name="toBeSaved">DevicePattern to save</param>
        /// <returns>DevicePattern Id</returns>
        long SaveDevicePattern(OpDevicePattern toBeSaved);

        /// <summary>
        /// Deletes the DevicePattern object from the database
        /// </summary>
        /// <param name="toBeDeleted">DevicePattern to delete</param>
        void DeleteDevicePattern(OpDevicePattern toBeDeleted);

        /// <summary>
        /// Clears the DevicePattern object cache
        /// </summary>
        void ClearDevicePatternCache();
        #endregion

        #region DevicePatternData

        /// <summary>
        /// Indicates whether the DevicePatternData is cached.
        /// </summary>
        bool DevicePatternDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DevicePatternData objects
        /// </summary>
        List<OpDevicePatternData> GetAllDevicePatternData();

        /// <summary>
        /// Saves the DevicePatternData object to the database
        /// </summary>
        /// <param name="toBeSaved">DevicePatternData to save</param>
        /// <returns>DevicePatternData Id</returns>
        long SaveDevicePatternData(OpDevicePatternData toBeSaved);

        /// <summary>
        /// Deletes the DevicePatternData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DevicePatternData to delete</param>
        void DeleteDevicePatternData(OpDevicePatternData toBeDeleted);

        /// <summary>
        /// Clears the DevicePatternData object cache
        /// </summary>
        void ClearDevicePatternDataCache();
        #endregion

        #region DeviceSimCardHistory

        /// <summary>
        /// Indicates whether the DeviceSimCardHistory is cached.
        /// </summary>
        bool DeviceSimCardHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceSimCardHistory objects
        /// </summary>
        List<OpDeviceSimCardHistory> GetAllDeviceSimCardHistory();


        /// <summary>
        /// Gets DeviceSimCardHistory by id
        /// </summary>
        /// <param name="Id">DeviceSimCardHistory Id</param>
        /// <returns>DeviceSimCardHistory object</returns>
        OpDeviceSimCardHistory GetDeviceSimCardHistory(long Id);

        /// <summary>
        /// Gets DeviceSimCardHistory by id
        /// </summary>
        /// <param name="Ids">DeviceSimCardHistory Ids</param>
        /// <returns>DeviceSimCardHistory list</returns>
        List<OpDeviceSimCardHistory> GetDeviceSimCardHistory(long[] Ids);

        /// <summary>
        /// Gets DeviceSimCardHistory by id
        /// </summary>
        /// <param name="Id">DeviceSimCardHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceSimCardHistory object</returns>
        OpDeviceSimCardHistory GetDeviceSimCardHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceSimCardHistory by id
        /// </summary>
        /// <param name="Ids">DeviceSimCardHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceSimCardHistory list</returns>
        List<OpDeviceSimCardHistory> GetDeviceSimCardHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceSimCardHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceSimCardHistory to save</param>
        /// <returns>DeviceSimCardHistory Id</returns>
        long SaveDeviceSimCardHistory(OpDeviceSimCardHistory toBeSaved);

        /// <summary>
        /// Deletes the DeviceSimCardHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceSimCardHistory to delete</param>
        void DeleteDeviceSimCardHistory(OpDeviceSimCardHistory toBeDeleted);

        /// <summary>
        /// Clears the DeviceSimCardHistory object cache
        /// </summary>
        void ClearDeviceSimCardHistoryCache();
        #endregion

        #region DeviceStateHistory

        /// <summary>
        /// Indicates whether the DeviceStateHistory is cached.
        /// </summary>
        bool DeviceStateHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceStateHistory objects
        /// </summary>
        List<OpDeviceStateHistory> GetAllDeviceStateHistory();


        /// <summary>
        /// Gets DeviceStateHistory by id
        /// </summary>
        /// <param name="Id">DeviceStateHistory Id</param>
        /// <returns>DeviceStateHistory object</returns>
        OpDeviceStateHistory GetDeviceStateHistory(long Id);

        /// <summary>
        /// Gets DeviceStateHistory by id
        /// </summary>
        /// <param name="Ids">DeviceStateHistory Ids</param>
        /// <returns>DeviceStateHistory list</returns>
        List<OpDeviceStateHistory> GetDeviceStateHistory(long[] Ids);

        /// <summary>
        /// Gets DeviceStateHistory by id
        /// </summary>
        /// <param name="Id">DeviceStateHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceStateHistory object</returns>
        OpDeviceStateHistory GetDeviceStateHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceStateHistory by id
        /// </summary>
        /// <param name="Ids">DeviceStateHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceStateHistory list</returns>
        List<OpDeviceStateHistory> GetDeviceStateHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the DeviceStateHistory object cache
        /// </summary>
        void ClearDeviceStateHistoryCache();
        #endregion

        #region DeviceStateType

        /// <summary>
        /// Indicates whether the DeviceStateType is cached.
        /// </summary>
        bool DeviceStateTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceStateType objects
        /// </summary>
        List<OpDeviceStateType> GetAllDeviceStateType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceStateType by id
        /// </summary>
        /// <param name="Id">DeviceStateType Id</param>
        /// <returns>DeviceStateType object</returns>
        OpDeviceStateType GetDeviceStateType(int Id);

        /// <summary>
        /// Gets DeviceStateType by id
        /// </summary>
        /// <param name="Ids">DeviceStateType Ids</param>
        /// <returns>DeviceStateType list</returns>
        List<OpDeviceStateType> GetDeviceStateType(int[] Ids);

        /// <summary>
        /// Gets DeviceStateType by id
        /// </summary>
        /// <param name="Id">DeviceStateType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceStateType object</returns>
        OpDeviceStateType GetDeviceStateType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceStateType by id
        /// </summary>
        /// <param name="Ids">DeviceStateType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceStateType list</returns>
        List<OpDeviceStateType> GetDeviceStateType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the DeviceStateType object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceStateType to delete</param>
        void DeleteDeviceStateType(OpDeviceStateType toBeDeleted);

        /// <summary>
        /// Clears the DeviceStateType object cache
        /// </summary>
        void ClearDeviceStateTypeCache();
        #endregion

        #region DeviceTemplate

        /// <summary>
        /// Indicates whether the DeviceTemplate is cached.
        /// </summary>
        bool DeviceTemplateCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTemplate objects
        /// </summary>
        List<OpDeviceTemplate> GetAllDeviceTemplate();

        /// <summary>
        /// Saves the DeviceTemplate object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTemplate to save</param>
        /// <returns>DeviceTemplate Id</returns>
        int SaveDeviceTemplate(OpDeviceTemplate toBeSaved);

        /// <summary>
        /// Deletes the DeviceTemplate object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTemplate to delete</param>
        void DeleteDeviceTemplate(OpDeviceTemplate toBeDeleted);

        /// <summary>
        /// Clears the DeviceTemplate object cache
        /// </summary>
        void ClearDeviceTemplateCache();
        #endregion

        #region DeviceTemplateData

        /// <summary>
        /// Indicates whether the DeviceTemplateData is cached.
        /// </summary>
        bool DeviceTemplateDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTemplateData objects
        /// </summary>
        List<OpDeviceTemplateData> GetAllDeviceTemplateData();

        /// <summary>
        /// Saves the DeviceTemplateData object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTemplateData to save</param>
        /// <returns>DeviceTemplateData Id</returns>
        int SaveDeviceTemplateData(OpDeviceTemplateData toBeSaved);

        /// <summary>
        /// Deletes the DeviceTemplateData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTemplateData to delete</param>
        void DeleteDeviceTemplateData(OpDeviceTemplateData toBeDeleted);

        /// <summary>
        /// Clears the DeviceTemplateData object cache
        /// </summary>
        void ClearDeviceTemplateDataCache();
        #endregion

        #region DeviceType

        /// <summary>
        /// Indicates whether the DeviceType is cached.
        /// </summary>
        bool DeviceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceType objects
        /// </summary>
        List<OpDeviceType> GetAllDeviceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceType by id
        /// </summary>
        /// <param name="Id">DeviceType Id</param>
        /// <returns>DeviceType object</returns>
        OpDeviceType GetDeviceType(int Id);

        /// <summary>
        /// Gets DeviceType by id
        /// </summary>
        /// <param name="Ids">DeviceType Ids</param>
        /// <returns>DeviceType list</returns>
        List<OpDeviceType> GetDeviceType(int[] Ids);

        /// <summary>
        /// Gets DeviceType by id
        /// </summary>
        /// <param name="Id">DeviceType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceType object</returns>
        OpDeviceType GetDeviceType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceType by id
        /// </summary>
        /// <param name="Ids">DeviceType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceType list</returns>
        List<OpDeviceType> GetDeviceType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the DeviceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceType to delete</param>
        void DeleteDeviceType(OpDeviceType toBeDeleted);

        /// <summary>
        /// Clears the DeviceType object cache
        /// </summary>
        void ClearDeviceTypeCache();
        #endregion

        #region DeviceTypeClass

        /// <summary>
        /// Indicates whether the DeviceTypeClass is cached.
        /// </summary>
        bool DeviceTypeClassCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeClass objects
        /// </summary>
        List<OpDeviceTypeClass> GetAllDeviceTypeClass(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceTypeClass by id
        /// </summary>
        /// <param name="Id">DeviceTypeClass Id</param>
        /// <returns>DeviceTypeClass object</returns>
        OpDeviceTypeClass GetDeviceTypeClass(int Id);

        /// <summary>
        /// Gets DeviceTypeClass by id
        /// </summary>
        /// <param name="Ids">DeviceTypeClass Ids</param>
        /// <returns>DeviceTypeClass list</returns>
        List<OpDeviceTypeClass> GetDeviceTypeClass(int[] Ids);

        /// <summary>
        /// Gets DeviceTypeClass by id
        /// </summary>
        /// <param name="Id">DeviceTypeClass Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeClass object</returns>
        OpDeviceTypeClass GetDeviceTypeClass(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeClass by id
        /// </summary>
        /// <param name="Ids">DeviceTypeClass Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeClass list</returns>
        List<OpDeviceTypeClass> GetDeviceTypeClass(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the DeviceTypeClass object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeClass to delete</param>
        void DeleteDeviceTypeClass(OpDeviceTypeClass toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeClass object cache
        /// </summary>
        void ClearDeviceTypeClassCache();
        #endregion

        #region DeviceTypeData

        /// <summary>
        /// Indicates whether the DeviceTypeData is cached.
        /// </summary>
        bool DeviceTypeDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeData objects
        /// </summary>
        List<OpDeviceTypeData> GetAllDeviceTypeData();


        /// <summary>
        /// Gets DeviceTypeData by id
        /// </summary>
        /// <param name="Id">DeviceTypeData Id</param>
        /// <returns>DeviceTypeData object</returns>
        OpDeviceTypeData GetDeviceTypeData(long Id);

        /// <summary>
        /// Gets DeviceTypeData by id
        /// </summary>
        /// <param name="Ids">DeviceTypeData Ids</param>
        /// <returns>DeviceTypeData list</returns>
        List<OpDeviceTypeData> GetDeviceTypeData(long[] Ids);

        /// <summary>
        /// Gets DeviceTypeData by id
        /// </summary>
        /// <param name="Id">DeviceTypeData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeData object</returns>
        OpDeviceTypeData GetDeviceTypeData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeData by id
        /// </summary>
        /// <param name="Ids">DeviceTypeData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeData list</returns>
        List<OpDeviceTypeData> GetDeviceTypeData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceTypeData object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeData to save</param>
        /// <returns>DeviceTypeData Id</returns>
        long SaveDeviceTypeData(OpDeviceTypeData toBeSaved);

        /// <summary>
        /// Deletes the DeviceTypeData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeData to delete</param>
        void DeleteDeviceTypeData(OpDeviceTypeData toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeData object cache
        /// </summary>
        void ClearDeviceTypeDataCache();
        #endregion

        #region DeviceTypeGroup

        /// <summary>
        /// Indicates whether the DeviceTypeGroup is cached.
        /// </summary>
        bool DeviceTypeGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeGroup objects
        /// </summary>
        List<OpDeviceTypeGroup> GetAllDeviceTypeGroup();


        /// <summary>
        /// Gets DeviceTypeGroup by id
        /// </summary>
        /// <param name="Id">DeviceTypeGroup Id</param>
        /// <returns>DeviceTypeGroup object</returns>
        OpDeviceTypeGroup GetDeviceTypeGroup(int Id);

        /// <summary>
        /// Gets DeviceTypeGroup by id
        /// </summary>
        /// <param name="Ids">DeviceTypeGroup Ids</param>
        /// <returns>DeviceTypeGroup list</returns>
        List<OpDeviceTypeGroup> GetDeviceTypeGroup(int[] Ids);

        /// <summary>
        /// Gets DeviceTypeGroup by id
        /// </summary>
        /// <param name="Id">DeviceTypeGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeGroup object</returns>
        OpDeviceTypeGroup GetDeviceTypeGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeGroup by id
        /// </summary>
        /// <param name="Ids">DeviceTypeGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeGroup list</returns>
        List<OpDeviceTypeGroup> GetDeviceTypeGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Deletes the DeviceTypeGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeGroup to delete</param>
        void DeleteDeviceTypeGroup(OpDeviceTypeGroup toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeGroup object cache
        /// </summary>
        void ClearDeviceTypeGroupCache();
        #endregion

        #region DeviceTypeProfileMap

        /// <summary>
        /// Indicates whether the DeviceTypeProfileMap is cached.
        /// </summary>
        bool DeviceTypeProfileMapCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeProfileMap objects
        /// </summary>
        List<OpDeviceTypeProfileMap> GetAllDeviceTypeProfileMap();


        /// <summary>
        /// Gets DeviceTypeProfileMap by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileMap Id</param>
        /// <returns>DeviceTypeProfileMap object</returns>
        OpDeviceTypeProfileMap GetDeviceTypeProfileMap(int Id);

        /// <summary>
        /// Gets DeviceTypeProfileMap by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileMap Ids</param>
        /// <returns>DeviceTypeProfileMap list</returns>
        List<OpDeviceTypeProfileMap> GetDeviceTypeProfileMap(int[] Ids);

        /// <summary>
        /// Gets DeviceTypeProfileMap by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileMap Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileMap object</returns>
        OpDeviceTypeProfileMap GetDeviceTypeProfileMap(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeProfileMap by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileMap Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileMap list</returns>
        List<OpDeviceTypeProfileMap> GetDeviceTypeProfileMap(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceTypeProfileMap object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeProfileMap to save</param>
        /// <returns>DeviceTypeProfileMap Id</returns>
        int SaveDeviceTypeProfileMap(OpDeviceTypeProfileMap toBeSaved);

        /// <summary>
        /// Deletes the DeviceTypeProfileMap object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeProfileMap to delete</param>
        void DeleteDeviceTypeProfileMap(OpDeviceTypeProfileMap toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeProfileMap object cache
        /// </summary>
        void ClearDeviceTypeProfileMapCache();
        #endregion

        #region DeviceTypeProfileMapConfig

        /// <summary>
        /// Indicates whether the DeviceTypeProfileMapConfig is cached.
        /// </summary>
        bool DeviceTypeProfileMapConfigCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeProfileMapConfig objects
        /// </summary>
        List<OpDeviceTypeProfileMapConfig> GetAllDeviceTypeProfileMapConfig();


        /// <summary>
        /// Gets DeviceTypeProfileMapConfig by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileMapConfig Id</param>
        /// <returns>DeviceTypeProfileMapConfig object</returns>
        OpDeviceTypeProfileMapConfig GetDeviceTypeProfileMapConfig(int Id);

        /// <summary>
        /// Gets DeviceTypeProfileMapConfig by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileMapConfig Ids</param>
        /// <returns>DeviceTypeProfileMapConfig list</returns>
        List<OpDeviceTypeProfileMapConfig> GetDeviceTypeProfileMapConfig(int[] Ids);

        /// <summary>
        /// Gets DeviceTypeProfileMapConfig by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileMapConfig Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileMapConfig object</returns>
        OpDeviceTypeProfileMapConfig GetDeviceTypeProfileMapConfig(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeProfileMapConfig by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileMapConfig Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileMapConfig list</returns>
        List<OpDeviceTypeProfileMapConfig> GetDeviceTypeProfileMapConfig(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceTypeProfileMapConfig object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeProfileMapConfig to save</param>
        /// <returns>DeviceTypeProfileMapConfig Id</returns>
        int SaveDeviceTypeProfileMapConfig(OpDeviceTypeProfileMapConfig toBeSaved);

        /// <summary>
        /// Deletes the DeviceTypeProfileMapConfig object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeProfileMapConfig to delete</param>
        void DeleteDeviceTypeProfileMapConfig(OpDeviceTypeProfileMapConfig toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeProfileMapConfig object cache
        /// </summary>
        void ClearDeviceTypeProfileMapConfigCache();
        #endregion

        #region DeviceTypeProfileStep

        /// <summary>
        /// Indicates whether the DeviceTypeProfileStep is cached.
        /// </summary>
        bool DeviceTypeProfileStepCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeProfileStep objects
        /// </summary>
        List<OpDeviceTypeProfileStep> GetAllDeviceTypeProfileStep();


        /// <summary>
        /// Gets DeviceTypeProfileStep by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileStep Id</param>
        /// <returns>DeviceTypeProfileStep object</returns>
        OpDeviceTypeProfileStep GetDeviceTypeProfileStep(int Id);

        /// <summary>
        /// Gets DeviceTypeProfileStep by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileStep Ids</param>
        /// <returns>DeviceTypeProfileStep list</returns>
        List<OpDeviceTypeProfileStep> GetDeviceTypeProfileStep(int[] Ids);

        /// <summary>
        /// Gets DeviceTypeProfileStep by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileStep Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileStep object</returns>
        OpDeviceTypeProfileStep GetDeviceTypeProfileStep(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeProfileStep by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileStep Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileStep list</returns>
        List<OpDeviceTypeProfileStep> GetDeviceTypeProfileStep(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceTypeProfileStep object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeProfileStep to save</param>
        /// <returns>DeviceTypeProfileStep Id</returns>
        int SaveDeviceTypeProfileStep(OpDeviceTypeProfileStep toBeSaved);

        /// <summary>
        /// Deletes the DeviceTypeProfileStep object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeProfileStep to delete</param>
        void DeleteDeviceTypeProfileStep(OpDeviceTypeProfileStep toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeProfileStep object cache
        /// </summary>
        void ClearDeviceTypeProfileStepCache();
        #endregion

        #region DeviceTypeProfileStepKind

        /// <summary>
        /// Indicates whether the DeviceTypeProfileStepKind is cached.
        /// </summary>
        bool DeviceTypeProfileStepKindCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeProfileStepKind objects
        /// </summary>
        List<OpDeviceTypeProfileStepKind> GetAllDeviceTypeProfileStepKind();


        /// <summary>
        /// Gets DeviceTypeProfileStepKind by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileStepKind Id</param>
        /// <returns>DeviceTypeProfileStepKind object</returns>
        OpDeviceTypeProfileStepKind GetDeviceTypeProfileStepKind(int Id);

        /// <summary>
        /// Gets DeviceTypeProfileStepKind by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileStepKind Ids</param>
        /// <returns>DeviceTypeProfileStepKind list</returns>
        List<OpDeviceTypeProfileStepKind> GetDeviceTypeProfileStepKind(int[] Ids);

        /// <summary>
        /// Gets DeviceTypeProfileStepKind by id
        /// </summary>
        /// <param name="Id">DeviceTypeProfileStepKind Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileStepKind object</returns>
        OpDeviceTypeProfileStepKind GetDeviceTypeProfileStepKind(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeProfileStepKind by id
        /// </summary>
        /// <param name="Ids">DeviceTypeProfileStepKind Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeProfileStepKind list</returns>
        List<OpDeviceTypeProfileStepKind> GetDeviceTypeProfileStepKind(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceTypeProfileStepKind object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeProfileStepKind to save</param>
        /// <returns>DeviceTypeProfileStepKind Id</returns>
        int SaveDeviceTypeProfileStepKind(OpDeviceTypeProfileStepKind toBeSaved);

        /// <summary>
        /// Deletes the DeviceTypeProfileStepKind object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeProfileStepKind to delete</param>
        void DeleteDeviceTypeProfileStepKind(OpDeviceTypeProfileStepKind toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeProfileStepKind object cache
        /// </summary>
        void ClearDeviceTypeProfileStepKindCache();
        #endregion

        #region DiagnosticAction

        /// <summary>
        /// Indicates whether the DiagnosticAction is cached.
        /// </summary>
        bool DiagnosticActionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DiagnosticAction objects
        /// </summary>
        List<OpDiagnosticAction> GetAllDiagnosticAction(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DiagnosticAction by id
        /// </summary>
        /// <param name="Id">DiagnosticAction Id</param>
        /// <returns>DiagnosticAction object</returns>
        OpDiagnosticAction GetDiagnosticAction(int Id);

        /// <summary>
        /// Gets DiagnosticAction by id
        /// </summary>
        /// <param name="Ids">DiagnosticAction Ids</param>
        /// <returns>DiagnosticAction list</returns>
        List<OpDiagnosticAction> GetDiagnosticAction(int[] Ids);

        /// <summary>
        /// Gets DiagnosticAction by id
        /// </summary>
        /// <param name="Id">DiagnosticAction Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticAction object</returns>
        OpDiagnosticAction GetDiagnosticAction(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DiagnosticAction by id
        /// </summary>
        /// <param name="Ids">DiagnosticAction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticAction list</returns>
        List<OpDiagnosticAction> GetDiagnosticAction(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DiagnosticAction object to the database
        /// </summary>
        /// <param name="toBeSaved">DiagnosticAction to save</param>
        /// <returns>DiagnosticAction Id</returns>
        int SaveDiagnosticAction(OpDiagnosticAction toBeSaved);

        /// <summary>
        /// Deletes the DiagnosticAction object from the database
        /// </summary>
        /// <param name="toBeDeleted">DiagnosticAction to delete</param>
        void DeleteDiagnosticAction(OpDiagnosticAction toBeDeleted);

        /// <summary>
        /// Clears the DiagnosticAction object cache
        /// </summary>
        void ClearDiagnosticActionCache();
        #endregion

        #region DiagnosticActionData

        /// <summary>
        /// Indicates whether the DiagnosticActionData is cached.
        /// </summary>
        bool DiagnosticActionDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DiagnosticActionData objects
        /// </summary>
        List<OpDiagnosticActionData> GetAllDiagnosticActionData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DiagnosticActionData by id
        /// </summary>
        /// <param name="Id">DiagnosticActionData Id</param>
        /// <returns>DiagnosticActionData object</returns>
        OpDiagnosticActionData GetDiagnosticActionData(long Id);

        /// <summary>
        /// Gets DiagnosticActionData by id
        /// </summary>
        /// <param name="Ids">DiagnosticActionData Ids</param>
        /// <returns>DiagnosticActionData list</returns>
        List<OpDiagnosticActionData> GetDiagnosticActionData(long[] Ids);

        /// <summary>
        /// Gets DiagnosticActionData by id
        /// </summary>
        /// <param name="Id">DiagnosticActionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticActionData object</returns>
        OpDiagnosticActionData GetDiagnosticActionData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DiagnosticActionData by id
        /// </summary>
        /// <param name="Ids">DiagnosticActionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticActionData list</returns>
        List<OpDiagnosticActionData> GetDiagnosticActionData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DiagnosticActionData object to the database
        /// </summary>
        /// <param name="toBeSaved">DiagnosticActionData to save</param>
        /// <returns>DiagnosticActionData Id</returns>
        long SaveDiagnosticActionData(OpDiagnosticActionData toBeSaved);

        /// <summary>
        /// Deletes the DiagnosticActionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DiagnosticActionData to delete</param>
        void DeleteDiagnosticActionData(OpDiagnosticActionData toBeDeleted);

        /// <summary>
        /// Clears the DiagnosticActionData object cache
        /// </summary>
        void ClearDiagnosticActionDataCache();
        #endregion

        #region DiagnosticActionInService

        /// <summary>
        /// Indicates whether the DiagnosticActionInService is cached.
        /// </summary>
        bool DiagnosticActionInServiceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DiagnosticActionInService objects
        /// </summary>
        List<OpDiagnosticActionInService> GetAllDiagnosticActionInService(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DiagnosticActionInService by id
        /// </summary>
        /// <param name="Id">DiagnosticActionInService Id</param>
        /// <returns>DiagnosticActionInService object</returns>
        OpDiagnosticActionInService GetDiagnosticActionInService(int Id);

        /// <summary>
        /// Gets DiagnosticActionInService by id
        /// </summary>
        /// <param name="Ids">DiagnosticActionInService Ids</param>
        /// <returns>DiagnosticActionInService list</returns>
        List<OpDiagnosticActionInService> GetDiagnosticActionInService(int[] Ids);

        /// <summary>
        /// Gets DiagnosticActionInService by id
        /// </summary>
        /// <param name="Id">DiagnosticActionInService Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticActionInService object</returns>
        OpDiagnosticActionInService GetDiagnosticActionInService(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DiagnosticActionInService by id
        /// </summary>
        /// <param name="Ids">DiagnosticActionInService Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticActionInService list</returns>
        List<OpDiagnosticActionInService> GetDiagnosticActionInService(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DiagnosticActionInService object to the database
        /// </summary>
        /// <param name="toBeSaved">DiagnosticActionInService to save</param>
        /// <returns>DiagnosticActionInService Id</returns>
        int SaveDiagnosticActionInService(OpDiagnosticActionInService toBeSaved);


        void SaveDiagnosticActionInService(int idService, int idDiagnosticAction, int indexNbr);

        /// <summary>
        /// Deletes the DiagnosticActionInService object from the database
        /// </summary>
        /// <param name="toBeDeleted">DiagnosticActionInService to delete</param>
        void DeleteDiagnosticActionInService(OpDiagnosticActionInService toBeDeleted);


        void DeleteDiagnosticActionInService(int? idDiagnosticAction, int? idService);

        /// <summary>
        /// Clears the DiagnosticActionInService object cache
        /// </summary>
        void ClearDiagnosticActionInServiceCache();
        #endregion

        #region DiagnosticActionResult

        /// <summary>
        /// Indicates whether the DiagnosticActionResult is cached.
        /// </summary>
        bool DiagnosticActionResultCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DiagnosticActionResult objects
        /// </summary>
        List<OpDiagnosticActionResult> GetAllDiagnosticActionResult(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DiagnosticActionResult by id
        /// </summary>
        /// <param name="Id">DiagnosticActionResult Id</param>
        /// <returns>DiagnosticActionResult object</returns>
        OpDiagnosticActionResult GetDiagnosticActionResult(int Id);

        /// <summary>
        /// Gets DiagnosticActionResult by id
        /// </summary>
        /// <param name="Ids">DiagnosticActionResult Ids</param>
        /// <returns>DiagnosticActionResult list</returns>
        List<OpDiagnosticActionResult> GetDiagnosticActionResult(int[] Ids);

        /// <summary>
        /// Gets DiagnosticActionResult by id
        /// </summary>
        /// <param name="Id">DiagnosticActionResult Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticActionResult object</returns>
        OpDiagnosticActionResult GetDiagnosticActionResult(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DiagnosticActionResult by id
        /// </summary>
        /// <param name="Ids">DiagnosticActionResult Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DiagnosticActionResult list</returns>
        List<OpDiagnosticActionResult> GetDiagnosticActionResult(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DiagnosticActionResult object to the database
        /// </summary>
        /// <param name="toBeSaved">DiagnosticActionResult to save</param>
        /// <returns>DiagnosticActionResult Id</returns>
        int SaveDiagnosticActionResult(OpDiagnosticActionResult toBeSaved);

        /// <summary>
        /// Deletes the DiagnosticActionResult object from the database
        /// </summary>
        /// <param name="toBeDeleted">DiagnosticActionResult to delete</param>
        void DeleteDiagnosticActionResult(OpDiagnosticActionResult toBeDeleted);

        /// <summary>
        /// Clears the DiagnosticActionResult object cache
        /// </summary>
        void ClearDiagnosticActionResultCache();
        #endregion

        #region DistributorData

        /// <summary>
        /// Indicates whether the DistributorData is cached.
        /// </summary>
        bool DistributorDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DistributorData objects
        /// </summary>
        List<OpDistributorData> GetAllDistributorData();


        /// <summary>
        /// Gets DistributorData by id
        /// </summary>
        /// <param name="Id">DistributorData Id</param>
        /// <returns>DistributorData object</returns>
        OpDistributorData GetDistributorData(long Id);

        /// <summary>
        /// Gets DistributorData by id
        /// </summary>
        /// <param name="Ids">DistributorData Ids</param>
        /// <returns>DistributorData list</returns>
        List<OpDistributorData> GetDistributorData(long[] Ids);

        /// <summary>
        /// Gets DistributorData by id
        /// </summary>
        /// <param name="Id">DistributorData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorData object</returns>
        OpDistributorData GetDistributorData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DistributorData by id
        /// </summary>
        /// <param name="Ids">DistributorData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorData list</returns>
        List<OpDistributorData> GetDistributorData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DistributorData object to the database
        /// </summary>
        /// <param name="toBeSaved">DistributorData to save</param>
        /// <returns>DistributorData Id</returns>
        long SaveDistributorData(OpDistributorData toBeSaved);

        /// <summary>
        /// Saves the DistributorData object to the database, updating by id distributor and id data type, set IdDistributorData to 1, to update
        /// </summary>
        /// <param name="toBeSaved">DistributorData to save</param>
        /// <returns>DistributorData Id</returns>
        long SaveDistributorDataByIdDistributorAndDataType(OpDistributorData toBeSaved);

        /// <summary>
        /// Deletes the DistributorData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DistributorData to delete</param>
        void DeleteDistributorData(OpDistributorData toBeDeleted);

        /// <summary>
        /// Clears the DistributorData object cache
        /// </summary>
        void ClearDistributorDataCache();

        /// <summary>
        /// Gets all DistributorData objects
        /// </summary>
        List<OpDistributorData> DistributorData { get; }

        #endregion

        #region DistributorHierarchy

        /// <summary>
        /// Indicates whether the DistributorHierarchy is cached.
        /// </summary>
        bool DistributorHierarchyCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DistributorHierarchy objects
        /// </summary>
        List<OpDistributorHierarchy> GetAllDistributorHierarchy();


        /// <summary>
        /// Gets DistributorHierarchy by id
        /// </summary>
        /// <param name="Id">DistributorHierarchy Id</param>
        /// <returns>DistributorHierarchy object</returns>
        OpDistributorHierarchy GetDistributorHierarchy(int Id);

        /// <summary>
        /// Gets DistributorHierarchy by id
        /// </summary>
        /// <param name="Ids">DistributorHierarchy Ids</param>
        /// <returns>DistributorHierarchy list</returns>
        List<OpDistributorHierarchy> GetDistributorHierarchy(int[] Ids);

        /// <summary>
        /// Gets DistributorHierarchy by id
        /// </summary>
        /// <param name="Id">DistributorHierarchy Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorHierarchy object</returns>
        OpDistributorHierarchy GetDistributorHierarchy(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DistributorHierarchy by id
        /// </summary>
        /// <param name="Ids">DistributorHierarchy Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorHierarchy list</returns>
        List<OpDistributorHierarchy> GetDistributorHierarchy(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DistributorHierarchy object to the database
        /// </summary>
        /// <param name="toBeSaved">DistributorHierarchy to save</param>
        /// <returns>DistributorHierarchy Id</returns>
        int SaveDistributorHierarchy(OpDistributorHierarchy toBeSaved);

        /// <summary>
        /// Deletes the DistributorHierarchy object from the database
        /// </summary>
        /// <param name="toBeDeleted">DistributorHierarchy to delete</param>
        void DeleteDistributorHierarchy(OpDistributorHierarchy toBeDeleted);

        /// <summary>
        /// Clears the DistributorHierarchy object cache
        /// </summary>
        void ClearDistributorHierarchyCache();
        #endregion

        #region DistributorSupplier

        /// <summary>
        /// Indicates whether the DistributorSupplier is cached.
        /// </summary>
        bool DistributorSupplierCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DistributorSupplier objects
        /// </summary>
        List<OpDistributorSupplier> GetAllDistributorSupplier();


        /// <summary>
        /// Gets DistributorSupplier by id
        /// </summary>
        /// <param name="Id">DistributorSupplier Id</param>
        /// <returns>DistributorSupplier object</returns>
        OpDistributorSupplier GetDistributorSupplier(int Id);

        /// <summary>
        /// Gets DistributorSupplier by id
        /// </summary>
        /// <param name="Ids">DistributorSupplier Ids</param>
        /// <returns>DistributorSupplier list</returns>
        List<OpDistributorSupplier> GetDistributorSupplier(int[] Ids);

        /// <summary>
        /// Gets DistributorSupplier by id
        /// </summary>
        /// <param name="Id">DistributorSupplier Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorSupplier object</returns>
        OpDistributorSupplier GetDistributorSupplier(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DistributorSupplier by id
        /// </summary>
        /// <param name="Ids">DistributorSupplier Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorSupplier list</returns>
        List<OpDistributorSupplier> GetDistributorSupplier(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DistributorSupplier object to the database
        /// </summary>
        /// <param name="toBeSaved">DistributorSupplier to save</param>
        /// <returns>DistributorSupplier Id</returns>
        int SaveDistributorSupplier(OpDistributorSupplier toBeSaved);

        /// <summary>
        /// Deletes the DistributorSupplier object from the database
        /// </summary>
        /// <param name="toBeDeleted">DistributorSupplier to delete</param>
        void DeleteDistributorSupplier(OpDistributorSupplier toBeDeleted);

        /// <summary>
        /// Clears the DistributorSupplier object cache
        /// </summary>
        void ClearDistributorSupplierCache();
        #endregion

        #region ErrorLog

        /// <summary>
        /// Indicates whether the ErrorLog is cached.
        /// </summary>
        bool ErrorLogCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ErrorLog objects
        /// </summary>
        List<OpErrorLog> GetAllErrorLog();


        /// <summary>
        /// Gets ErrorLog by id
        /// </summary>
        /// <param name="Id">ErrorLog Id</param>
        /// <returns>ErrorLog object</returns>
        OpErrorLog GetErrorLog(int Id);

        /// <summary>
        /// Gets ErrorLog by id
        /// </summary>
        /// <param name="Ids">ErrorLog Ids</param>
        /// <returns>ErrorLog list</returns>
        List<OpErrorLog> GetErrorLog(int[] Ids);

        /// <summary>
        /// Gets ErrorLog by id
        /// </summary>
        /// <param name="Id">ErrorLog Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ErrorLog object</returns>
        OpErrorLog GetErrorLog(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ErrorLog by id
        /// </summary>
        /// <param name="Ids">ErrorLog Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ErrorLog list</returns>
        List<OpErrorLog> GetErrorLog(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ErrorLog object to the database
        /// </summary>
        /// <param name="toBeSaved">ErrorLog to save</param>
        /// <returns>ErrorLog Id</returns>
        int SaveErrorLog(OpErrorLog toBeSaved);

        /// <summary>
        /// Deletes the ErrorLog object from the database
        /// </summary>
        /// <param name="toBeDeleted">ErrorLog to delete</param>
        void DeleteErrorLog(OpErrorLog toBeDeleted);

        /// <summary>
        /// Clears the ErrorLog object cache
        /// </summary>
        void ClearErrorLogCache();
        #endregion

        #region Etl

        /// <summary>
        /// Indicates whether the Etl is cached.
        /// </summary>
        bool EtlCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Etl objects
        /// </summary>
        List<OpEtl> GetAllEtl(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Id">Etl Id</param>
        /// <returns>Etl object</returns>
        OpEtl GetEtl(int Id);

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Ids">Etl Ids</param>
        /// <returns>Etl list</returns>
        List<OpEtl> GetEtl(int[] Ids);

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Id">Etl Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Etl object</returns>
        OpEtl GetEtl(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Ids">Etl Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Etl list</returns>
        List<OpEtl> GetEtl(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Ids">Etl Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Etl list</returns>
        List<OpEtl> GetEtl(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Etl object to the database
        /// </summary>
        /// <param name="toBeSaved">Etl to save</param>
        /// <returns>Etl Id</returns>
        int SaveEtl(OpEtl toBeSaved);

        /// <summary>
        /// Deletes the Etl object from the database
        /// </summary>
        /// <param name="toBeDeleted">Etl to delete</param>
        void DeleteEtl(OpEtl toBeDeleted);

        /// <summary>
        /// Clears the Etl object cache
        /// </summary>
        void ClearEtlCache();
        #endregion

        #region FaultCode

        /// <summary>
        /// Indicates whether the FaultCode is cached.
        /// </summary>
        bool FaultCodeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all FaultCode objects
        /// </summary>
        List<OpFaultCode> GetAllFaultCode();


        /// <summary>
        /// Gets FaultCode by id
        /// </summary>
        /// <param name="Id">FaultCode Id</param>
        /// <returns>FaultCode object</returns>
        OpFaultCode GetFaultCode(int Id);

        /// <summary>
        /// Gets FaultCode by id
        /// </summary>
        /// <param name="Ids">FaultCode Ids</param>
        /// <returns>FaultCode list</returns>
        List<OpFaultCode> GetFaultCode(int[] Ids);

        /// <summary>
        /// Gets FaultCode by id
        /// </summary>
        /// <param name="Id">FaultCode Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCode object</returns>
        OpFaultCode GetFaultCode(int Id, bool queryDatabase);

        /// <summary>
        /// Gets FaultCode by id
        /// </summary>
        /// <param name="Ids">FaultCode Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCode list</returns>
        List<OpFaultCode> GetFaultCode(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the FaultCode object to the database
        /// </summary>
        /// <param name="toBeSaved">FaultCode to save</param>
        /// <returns>FaultCode Id</returns>
        int SaveFaultCode(OpFaultCode toBeSaved);

        /// <summary>
        /// Deletes the FaultCode object from the database
        /// </summary>
        /// <param name="toBeDeleted">FaultCode to delete</param>
        void DeleteFaultCode(OpFaultCode toBeDeleted);

        /// <summary>
        /// Clears the FaultCode object cache
        /// </summary>
        void ClearFaultCodeCache();
        #endregion

        #region FaultCodeData

        /// <summary>
        /// Indicates whether the FaultCodeData is cached.
        /// </summary>
        bool FaultCodeDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all FaultCodeData objects
        /// </summary>
        List<OpFaultCodeData> GetAllFaultCodeData();


        /// <summary>
        /// Gets FaultCodeData by id
        /// </summary>
        /// <param name="Id">FaultCodeData Id</param>
        /// <returns>FaultCodeData object</returns>
        OpFaultCodeData GetFaultCodeData(long Id);

        /// <summary>
        /// Gets FaultCodeData by id
        /// </summary>
        /// <param name="Ids">FaultCodeData Ids</param>
        /// <returns>FaultCodeData list</returns>
        List<OpFaultCodeData> GetFaultCodeData(long[] Ids);

        /// <summary>
        /// Gets FaultCodeData by id
        /// </summary>
        /// <param name="Id">FaultCodeData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCodeData object</returns>
        OpFaultCodeData GetFaultCodeData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets FaultCodeData by id
        /// </summary>
        /// <param name="Ids">FaultCodeData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCodeData list</returns>
        List<OpFaultCodeData> GetFaultCodeData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the FaultCodeData object to the database
        /// </summary>
        /// <param name="toBeSaved">FaultCodeData to save</param>
        /// <returns>FaultCodeData Id</returns>
        long SaveFaultCodeData(OpFaultCodeData toBeSaved);

        /// <summary>
        /// Deletes the FaultCodeData object from the database
        /// </summary>
        /// <param name="toBeDeleted">FaultCodeData to delete</param>
        void DeleteFaultCodeData(OpFaultCodeData toBeDeleted);

        /// <summary>
        /// Clears the FaultCodeData object cache
        /// </summary>
        void ClearFaultCodeDataCache();
        #endregion

        #region FaultCodeSummary

        /// <summary>
        /// Indicates whether the FaultCodeSummary is cached.
        /// </summary>
        bool FaultCodeSummaryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all FaultCodeSummary objects
        /// </summary>
        List<OpFaultCodeSummary> GetAllFaultCodeSummary();


        /// <summary>
        /// Gets FaultCodeSummary by id
        /// </summary>
        /// <param name="Id">FaultCodeSummary Id</param>
        /// <returns>FaultCodeSummary object</returns>
        OpFaultCodeSummary GetFaultCodeSummary(int Id);

        /// <summary>
        /// Gets FaultCodeSummary by id
        /// </summary>
        /// <param name="Ids">FaultCodeSummary Ids</param>
        /// <returns>FaultCodeSummary list</returns>
        List<OpFaultCodeSummary> GetFaultCodeSummary(int[] Ids);

        /// <summary>
        /// Gets FaultCodeSummary by id
        /// </summary>
        /// <param name="Id">FaultCodeSummary Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCodeSummary object</returns>
        OpFaultCodeSummary GetFaultCodeSummary(int Id, bool queryDatabase);

        /// <summary>
        /// Gets FaultCodeSummary by id
        /// </summary>
        /// <param name="Ids">FaultCodeSummary Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCodeSummary list</returns>
        List<OpFaultCodeSummary> GetFaultCodeSummary(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the FaultCodeSummary object to the database
        /// </summary>
        /// <param name="toBeSaved">FaultCodeSummary to save</param>
        /// <returns>FaultCodeSummary Id</returns>
        int SaveFaultCodeSummary(OpFaultCodeSummary toBeSaved);

        /// <summary>
        /// Deletes the FaultCodeSummary object from the database
        /// </summary>
        /// <param name="toBeDeleted">FaultCodeSummary to delete</param>
        void DeleteFaultCodeSummary(OpFaultCodeSummary toBeDeleted);

        /// <summary>
        /// Clears the FaultCodeSummary object cache
        /// </summary>
        void ClearFaultCodeSummaryCache();
        #endregion

        #region FaultCodeType

        /// <summary>
        /// Indicates whether the FaultCodeType is cached.
        /// </summary>
        bool FaultCodeTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all FaultCodeType objects
        /// </summary>
        List<OpFaultCodeType> GetAllFaultCodeType();


        /// <summary>
        /// Gets FaultCodeType by id
        /// </summary>
        /// <param name="Id">FaultCodeType Id</param>
        /// <returns>FaultCodeType object</returns>
        OpFaultCodeType GetFaultCodeType(int Id);

        /// <summary>
        /// Gets FaultCodeType by id
        /// </summary>
        /// <param name="Ids">FaultCodeType Ids</param>
        /// <returns>FaultCodeType list</returns>
        List<OpFaultCodeType> GetFaultCodeType(int[] Ids);

        /// <summary>
        /// Gets FaultCodeType by id
        /// </summary>
        /// <param name="Id">FaultCodeType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCodeType object</returns>
        OpFaultCodeType GetFaultCodeType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets FaultCodeType by id
        /// </summary>
        /// <param name="Ids">FaultCodeType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>FaultCodeType list</returns>
        List<OpFaultCodeType> GetFaultCodeType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the FaultCodeType object to the database
        /// </summary>
        /// <param name="toBeSaved">FaultCodeType to save</param>
        /// <returns>FaultCodeType Id</returns>
        int SaveFaultCodeType(OpFaultCodeType toBeSaved);

        /// <summary>
        /// Deletes the FaultCodeType object from the database
        /// </summary>
        /// <param name="toBeDeleted">FaultCodeType to delete</param>
        void DeleteFaultCodeType(OpFaultCodeType toBeDeleted);

        /// <summary>
        /// Clears the FaultCodeType object cache
        /// </summary>
        void ClearFaultCodeTypeCache();
        #endregion

        #region HolidayDate

        /// <summary>
        /// Indicates whether the HolidayDate is cached.
        /// </summary>
        bool HolidayDateCacheEnabled { get; set; }

        /// <summary>
        /// Gets all HolidayDate objects
        /// </summary>
        List<OpHolidayDate> GetAllHolidayDate();


        /// <summary>
        /// Gets HolidayDate by id
        /// </summary>
        /// <param name="Id">HolidayDate Id</param>
        /// <returns>HolidayDate object</returns>
        OpHolidayDate GetHolidayDate(DateTime Id);

        /// <summary>
        /// Gets HolidayDate by id
        /// </summary>
        /// <param name="Ids">HolidayDate Ids</param>
        /// <returns>HolidayDate list</returns>
        List<OpHolidayDate> GetHolidayDate(DateTime[] Ids);

        /// <summary>
        /// Gets HolidayDate by id
        /// </summary>
        /// <param name="Id">HolidayDate Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>HolidayDate object</returns>
        OpHolidayDate GetHolidayDate(DateTime Id, bool queryDatabase);

        /// <summary>
        /// Gets HolidayDate by id
        /// </summary>
        /// <param name="Ids">HolidayDate Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>HolidayDate list</returns>
        List<OpHolidayDate> GetHolidayDate(DateTime[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the HolidayDate object to the database
        /// </summary>
        /// <param name="toBeSaved">HolidayDate to save</param>
        /// <returns>HolidayDate Id</returns>
        DateTime SaveHolidayDate(OpHolidayDate toBeSaved);

        /// <summary>
        /// Deletes the HolidayDate object from the database
        /// </summary>
        /// <param name="toBeDeleted">HolidayDate to delete</param>
        void DeleteHolidayDate(OpHolidayDate toBeDeleted);

        /// <summary>
        /// Clears the HolidayDate object cache
        /// </summary>
        void ClearHolidayDateCache();
        #endregion

        #region ImrServer

        /// <summary>
        /// Indicates whether the ImrServer is cached.
        /// </summary>
        bool ImrServerCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ImrServer objects
        /// </summary>
        List<OpImrServer> GetAllImrServer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ImrServer by id
        /// </summary>
        /// <param name="Id">ImrServer Id</param>
        /// <returns>ImrServer object</returns>
        OpImrServer GetImrServer(int Id);

        /// <summary>
        /// Gets ImrServer by id
        /// </summary>
        /// <param name="Ids">ImrServer Ids</param>
        /// <returns>ImrServer list</returns>
        List<OpImrServer> GetImrServer(int[] Ids);

        /// <summary>
        /// Gets ImrServer by id
        /// </summary>
        /// <param name="Id">ImrServer Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServer object</returns>
        OpImrServer GetImrServer(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ImrServer by id
        /// </summary>
        /// <param name="Ids">ImrServer Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServer list</returns>
        List<OpImrServer> GetImrServer(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the ImrServer object from the database
        /// </summary>
        /// <param name="toBeDeleted">ImrServer to delete</param>
        void DeleteImrServer(OpImrServer toBeDeleted);

        /// <summary>
        /// Clears the ImrServer object cache
        /// </summary>
        void ClearImrServerCache();
        #endregion

        #region ImrServerData

        /// <summary>
        /// Indicates whether the ImrServerData is cached.
        /// </summary>
        bool ImrServerDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ImrServerData objects
        /// </summary>
        List<OpImrServerData> GetAllImrServerData();


        /// <summary>
        /// Gets ImrServerData by id
        /// </summary>
        /// <param name="Id">ImrServerData Id</param>
        /// <returns>ImrServerData object</returns>
        OpImrServerData GetImrServerData(long Id);

        /// <summary>
        /// Gets ImrServerData by id
        /// </summary>
        /// <param name="Ids">ImrServerData Ids</param>
        /// <returns>ImrServerData list</returns>
        List<OpImrServerData> GetImrServerData(long[] Ids);

        /// <summary>
        /// Gets ImrServerData by id
        /// </summary>
        /// <param name="Id">ImrServerData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServerData object</returns>
        OpImrServerData GetImrServerData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ImrServerData by id
        /// </summary>
        /// <param name="Ids">ImrServerData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServerData list</returns>
        List<OpImrServerData> GetImrServerData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ImrServerData object to the database
        /// </summary>
        /// <param name="toBeSaved">ImrServerData to save</param>
        /// <returns>ImrServerData Id</returns>
        long SaveImrServerData(OpImrServerData toBeSaved);

        /// <summary>
        /// Deletes the ImrServerData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ImrServerData to delete</param>
        void DeleteImrServerData(OpImrServerData toBeDeleted);

        /// <summary>
        /// Clears the ImrServerData object cache
        /// </summary>
        void ClearImrServerDataCache();
        #endregion

        #region Issue

        /// <summary>
        /// Indicates whether the Issue is cached.
        /// </summary>
        bool IssueCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Issue objects
        /// </summary>
        List<OpIssue> GetAllIssue(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Issue by id
        /// </summary>
        /// <param name="Id">Issue Id</param>
        /// <returns>Issue object</returns>
        OpIssue GetIssue(int Id);

        /// <summary>
        /// Gets Issue by id
        /// </summary>
        /// <param name="Ids">Issue Ids</param>
        /// <returns>Issue list</returns>
        List<OpIssue> GetIssue(int[] Ids);

        /// <summary>
        /// Gets Issue by id
        /// </summary>
        /// <param name="Id">Issue Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Issue object</returns>
        OpIssue GetIssue(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Issue by id
        /// </summary>
        /// <param name="Ids">Issue Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Issue list</returns>
        List<OpIssue> GetIssue(int[] Ids, bool queryDatabase, bool loadNavigationProperties = false, bool loadCustomData = false, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Issue object to the database
        /// </summary>
        /// <param name="toBeSaved">Issue to save</param>
        /// <returns>Issue Id</returns>
        int SaveIssue(OpIssue toBeSaved);

        /// <summary>
        /// Deletes the Issue object from the database
        /// </summary>
        /// <param name="toBeDeleted">Issue to delete</param>
        void DeleteIssue(OpIssue toBeDeleted);

        /// <summary>
        /// Clears the Issue object cache
        /// </summary>
        void ClearIssueCache();
        #endregion

        #region IssueData

        /// <summary>
        /// Indicates whether the IssueData is cached.
        /// </summary>
        bool IssueDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all IssueData objects
        /// </summary>
        List<OpIssueData> GetAllIssueData();


        /// <summary>
        /// Gets IssueData by id
        /// </summary>
        /// <param name="Id">IssueData Id</param>
        /// <returns>IssueData object</returns>
        OpIssueData GetIssueData(long Id);

        /// <summary>
        /// Gets IssueData by id
        /// </summary>
        /// <param name="Ids">IssueData Ids</param>
        /// <returns>IssueData list</returns>
        List<OpIssueData> GetIssueData(long[] Ids);

        /// <summary>
        /// Gets IssueData by id
        /// </summary>
        /// <param name="Id">IssueData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueData object</returns>
        OpIssueData GetIssueData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets IssueData by id
        /// </summary>
        /// <param name="Ids">IssueData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueData list</returns>
        List<OpIssueData> GetIssueData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the IssueData object to the database
        /// </summary>
        /// <param name="toBeSaved">IssueData to save</param>
        /// <returns>IssueData Id</returns>
        long SaveIssueData(OpIssueData toBeSaved);

        /// <summary>
        /// Deletes the IssueData object from the database
        /// </summary>
        /// <param name="toBeDeleted">IssueData to delete</param>
        void DeleteIssueData(OpIssueData toBeDeleted);

        /// <summary>
        /// Clears the IssueData object cache
        /// </summary>
        void ClearIssueDataCache();
        #endregion

        #region IssueGroup

        /// <summary>
        /// Indicates whether the IssueGroup is cached.
        /// </summary>
        bool IssueGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all IssueGroup objects
        /// </summary>
        List<OpIssueGroup> GetAllIssueGroup();

        /// <summary>
        /// Gets IssueGroup by id
        /// </summary>
        /// <param name="Id">IssueGroup Id</param>
        /// <returns>IssueGroup object</returns>
        OpIssueGroup GetIssueGroup(int Id);

        /// <summary>
        /// Gets IssueGroup by id
        /// </summary>
        /// <param name="Ids">IssueGroup Ids</param>
        /// <returns>IssueGroup list</returns>
        List<OpIssueGroup> GetIssueGroup(int[] Ids);

        /// <summary>
        /// Gets IssueGroup by id
        /// </summary>
        /// <param name="Id">IssueGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueGroup object</returns>
        OpIssueGroup GetIssueGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets IssueGroup by id
        /// </summary>
        /// <param name="Ids">IssueGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueGroup list</returns>
        List<OpIssueGroup> GetIssueGroup(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the IssueGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">IssueGroup to save</param>
        /// <returns>IssueGroup Id</returns>
        int SaveIssueGroup(OpIssueGroup toBeSaved);

        /// <summary>
        /// Deletes the IssueGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">IssueGroup to delete</param>
        void DeleteIssueGroup(OpIssueGroup toBeDeleted);

        /// <summary>
        /// Clears the IssueGroup object cache
        /// </summary>
        void ClearIssueGroupCache();

        #endregion

        #region IssueHistory

        /// <summary>
        /// Indicates whether the IssueHistory is cached.
        /// </summary>
        bool IssueHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all IssueHistory objects
        /// </summary>
        List<OpIssueHistory> GetAllIssueHistory();


        /// <summary>
        /// Gets IssueHistory by id
        /// </summary>
        /// <param name="Id">IssueHistory Id</param>
        /// <returns>IssueHistory object</returns>
        OpIssueHistory GetIssueHistory(int Id);

        /// <summary>
        /// Gets IssueHistory by id
        /// </summary>
        /// <param name="Ids">IssueHistory Ids</param>
        /// <returns>IssueHistory list</returns>
        List<OpIssueHistory> GetIssueHistory(int[] Ids);

        /// <summary>
        /// Gets IssueHistory by id
        /// </summary>
        /// <param name="Id">IssueHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueHistory object</returns>
        OpIssueHistory GetIssueHistory(int Id, bool queryDatabase);

        /// <summary>
        /// Gets IssueHistory by id
        /// </summary>
        /// <param name="Ids">IssueHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueHistory list</returns>
        List<OpIssueHistory> GetIssueHistory(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the IssueHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">IssueHistory to save</param>
        /// <returns>IssueHistory Id</returns>
        int SaveIssueHistory(OpIssueHistory toBeSaved);

        /// <summary>
        /// Deletes the IssueHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">IssueHistory to delete</param>
        void DeleteIssueHistory(OpIssueHistory toBeDeleted);

        /// <summary>
        /// Clears the IssueHistory object cache
        /// </summary>
        void ClearIssueHistoryCache();
        #endregion

        #region IssueStatus

        /// <summary>
        /// Indicates whether the IssueStatus is cached.
        /// </summary>
        bool IssueStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all IssueStatus objects
        /// </summary>
        List<OpIssueStatus> GetAllIssueStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets IssueStatus by id
        /// </summary>
        /// <param name="Id">IssueStatus Id</param>
        /// <returns>IssueStatus object</returns>
        OpIssueStatus GetIssueStatus(int Id);

        /// <summary>
        /// Gets IssueStatus by id
        /// </summary>
        /// <param name="Ids">IssueStatus Ids</param>
        /// <returns>IssueStatus list</returns>
        List<OpIssueStatus> GetIssueStatus(int[] Ids);

        /// <summary>
        /// Gets IssueStatus by id
        /// </summary>
        /// <param name="Id">IssueStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueStatus object</returns>
        OpIssueStatus GetIssueStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets IssueStatus by id
        /// </summary>
        /// <param name="Ids">IssueStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueStatus list</returns>
        List<OpIssueStatus> GetIssueStatus(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the IssueStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">IssueStatus to save</param>
        /// <returns>IssueStatus Id</returns>
        int SaveIssueStatus(OpIssueStatus toBeSaved);

        /// <summary>
        /// Deletes the IssueStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">IssueStatus to delete</param>
        void DeleteIssueStatus(OpIssueStatus toBeDeleted);

        /// <summary>
        /// Clears the IssueStatus object cache
        /// </summary>
        void ClearIssueStatusCache();
        #endregion

        #region IssueType

        /// <summary>
        /// Indicates whether the IssueType is cached.
        /// </summary>
        bool IssueTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all IssueType objects
        /// </summary>
        List<OpIssueType> GetAllIssueType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets IssueType by id
        /// </summary>
        /// <param name="Id">IssueType Id</param>
        /// <returns>IssueType object</returns>
        OpIssueType GetIssueType(int Id);

        /// <summary>
        /// Gets IssueType by id
        /// </summary>
        /// <param name="Ids">IssueType Ids</param>
        /// <returns>IssueType list</returns>
        List<OpIssueType> GetIssueType(int[] Ids);

        /// <summary>
        /// Gets IssueType by id
        /// </summary>
        /// <param name="Id">IssueType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueType object</returns>
        OpIssueType GetIssueType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets IssueType by id
        /// </summary>
        /// <param name="Ids">IssueType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>IssueType list</returns>
        List<OpIssueType> GetIssueType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the IssueType object to the database
        /// </summary>
        /// <param name="toBeSaved">IssueType to save</param>
        /// <returns>IssueType Id</returns>
        int SaveIssueType(OpIssueType toBeSaved);

        /// <summary>
        /// Deletes the IssueType object from the database
        /// </summary>
        /// <param name="toBeDeleted">IssueType to delete</param>
        void DeleteIssueType(OpIssueType toBeDeleted);

        /// <summary>
        /// Clears the IssueType object cache
        /// </summary>
        void ClearIssueTypeCache();
        #endregion

        #region Language

        /// <summary>
        /// Indicates whether the Language is cached.
        /// </summary>
        bool LanguageCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Language objects
        /// </summary>
        List<OpLanguage> GetAllLanguage(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Language by id
        /// </summary>
        /// <param name="Id">Language Id</param>
        /// <returns>Language object</returns>
        OpLanguage GetLanguage(int Id);

        /// <summary>
        /// Gets Language by id
        /// </summary>
        /// <param name="Ids">Language Ids</param>
        /// <returns>Language list</returns>
        List<OpLanguage> GetLanguage(int[] Ids);

        /// <summary>
        /// Gets Language by id
        /// </summary>
        /// <param name="Id">Language Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Language object</returns>
        OpLanguage GetLanguage(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Language by id
        /// </summary>
        /// <param name="Ids">Language Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Language list</returns>
        List<OpLanguage> GetLanguage(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        /// <summary>
        /// Deletes the Language object from the database
        /// </summary>
        /// <param name="toBeDeleted">Language to delete</param>
        void DeleteLanguage(OpLanguage toBeDeleted);

        /// <summary>
        /// Clears the Language object cache
        /// </summary>
        void ClearLanguageCache();
        #endregion

        #region LanguageData

        /// <summary>
        /// Indicates whether the LanguageData is cached.
        /// </summary>
        bool LanguageDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LanguageData objects
        /// </summary>
        List<OpLanguageData> GetAllLanguageData();


        /// <summary>
        /// Gets LanguageData by id
        /// </summary>
        /// <param name="Id">LanguageData Id</param>
        /// <returns>LanguageData object</returns>
        OpLanguageData GetLanguageData(long Id);

        /// <summary>
        /// Gets LanguageData by id
        /// </summary>
        /// <param name="Ids">LanguageData Ids</param>
        /// <returns>LanguageData list</returns>
        List<OpLanguageData> GetLanguageData(long[] Ids);

        /// <summary>
        /// Gets LanguageData by id
        /// </summary>
        /// <param name="Id">LanguageData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LanguageData object</returns>
        OpLanguageData GetLanguageData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LanguageData by id
        /// </summary>
        /// <param name="Ids">LanguageData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LanguageData list</returns>
        List<OpLanguageData> GetLanguageData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the LanguageData object to the database
        /// </summary>
        /// <param name="toBeSaved">LanguageData to save</param>
        /// <returns>LanguageData Id</returns>
        long SaveLanguageData(OpLanguageData toBeSaved);

        /// <summary>
        /// Deletes the LanguageData object from the database
        /// </summary>
        /// <param name="toBeDeleted">LanguageData to delete</param>
        void DeleteLanguageData(OpLanguageData toBeDeleted);

        /// <summary>
        /// Clears the LanguageData object cache
        /// </summary>
        void ClearLanguageDataCache();
        #endregion

        #region Location

        bool LocationCacheEnabled { get; set; }
        /// <summary>
        /// Gets all Location objects
        /// </summary>
        List<OpLocation> GetAllLocation(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Location by id
        /// </summary>
        /// <param name="Id">Location Id</param>
        /// <returns>Location object</returns>
        OpLocation GetLocation(long Id);

        /// <summary>
        /// Gets Location by id
        /// </summary>
        /// <param name="Ids">Location Ids</param>
        /// <returns>Location list</returns>
        List<OpLocation> GetLocation(long[] Ids);

        /// <summary>
        /// Gets Location by id
        /// </summary>
        /// <param name="Id">Location Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Location object</returns>
        OpLocation GetLocation(long Id, bool queryDatabase);

        //List<OpLocation> GetLocation(long[] Ids, bool queryDatabase) in .User.cs

        /// <summary>
        /// Deletes the Location object from the database
        /// </summary>
        /// <param name="toBeDeleted">Location to delete</param>
        void DeleteLocation(OpLocation toBeDeleted);

        /// <summary>
        /// Clears the Location object cache
        /// </summary>
        void ClearLocationCache();
        #endregion

        #region LocationConsumerHistory

        /// <summary>
        /// Indicates whether the LocationConsumerHistory is cached.
        /// </summary>
        bool LocationConsumerHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationConsumerHistory objects
        /// </summary>
        List<OpLocationConsumerHistory> GetAllLocationConsumerHistory();


        /// <summary>
        /// Gets LocationConsumerHistory by id
        /// </summary>
        /// <param name="Id">LocationConsumerHistory Id</param>
        /// <returns>LocationConsumerHistory object</returns>
        OpLocationConsumerHistory GetLocationConsumerHistory(long Id);

        /// <summary>
        /// Gets LocationConsumerHistory by id
        /// </summary>
        /// <param name="Ids">LocationConsumerHistory Ids</param>
        /// <returns>LocationConsumerHistory list</returns>
        List<OpLocationConsumerHistory> GetLocationConsumerHistory(long[] Ids);

        /// <summary>
        /// Gets LocationConsumerHistory by id
        /// </summary>
        /// <param name="Id">LocationConsumerHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationConsumerHistory object</returns>
        OpLocationConsumerHistory GetLocationConsumerHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationConsumerHistory by id
        /// </summary>
        /// <param name="Ids">LocationConsumerHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationConsumerHistory list</returns>
        List<OpLocationConsumerHistory> GetLocationConsumerHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationConsumerHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationConsumerHistory to save</param>
        /// <returns>LocationConsumerHistory Id</returns>
        long SaveLocationConsumerHistory(OpLocationConsumerHistory toBeSaved);

        /// <summary>
        /// Deletes the LocationConsumerHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationConsumerHistory to delete</param>
        void DeleteLocationConsumerHistory(OpLocationConsumerHistory toBeDeleted);

        /// <summary>
        /// Clears the LocationConsumerHistory object cache
        /// </summary>
        void ClearLocationConsumerHistoryCache();
        #endregion

        #region LocationDetails

        /// <summary>
        /// Indicates whether the LocationDetails is cached.
        /// </summary>
        bool LocationDetailsCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationDetails objects
        /// </summary>
        List<OpLocationDetails> GetAllLocationDetails();


        /// <summary>
        /// Gets LocationDetails by id
        /// </summary>
        /// <param name="Id">LocationDetails Id</param>
        /// <returns>LocationDetails object</returns>
        OpLocationDetails GetLocationDetails(long Id);

        /// <summary>
        /// Gets LocationDetails by id
        /// </summary>
        /// <param name="Ids">LocationDetails Ids</param>
        /// <returns>LocationDetails list</returns>
        List<OpLocationDetails> GetLocationDetails(long[] Ids);

        /// <summary>
        /// Gets LocationDetails by id
        /// </summary>
        /// <param name="Id">LocationDetails Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationDetails object</returns>
        OpLocationDetails GetLocationDetails(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationDetails by id
        /// </summary>
        /// <param name="Ids">LocationDetails Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationDetails list</returns>
        List<OpLocationDetails> GetLocationDetails(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationDetails object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationDetails to save</param>
        /// <returns>LocationDetails Id</returns>
        long SaveLocationDetails(OpLocationDetails toBeSaved);

        /// <summary>
        /// Deletes the LocationDetails object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationDetails to delete</param>
        void DeleteLocationDetails(OpLocationDetails toBeDeleted);

        /// <summary>
        /// Clears the LocationDetails object cache
        /// </summary>
        void ClearLocationDetailsCache();
        #endregion

        #region LocationEquipment

        /// <summary>
        /// Indicates whether the LocationEquipment is cached.
        /// </summary>
        bool LocationEquipmentCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationEquipment objects
        /// </summary>
        List<OpLocationEquipment> GetAllLocationEquipment();


        /// <summary>
        /// Gets LocationEquipment by id
        /// </summary>
        /// <param name="Id">LocationEquipment Id</param>
        /// <returns>LocationEquipment object</returns>
        OpLocationEquipment GetLocationEquipment(long Id);

        /// <summary>
        /// Gets LocationEquipment by id
        /// </summary>
        /// <param name="Ids">LocationEquipment Ids</param>
        /// <returns>LocationEquipment list</returns>
        List<OpLocationEquipment> GetLocationEquipment(long[] Ids);

        /// <summary>
        /// Gets LocationEquipment by id
        /// </summary>
        /// <param name="Id">LocationEquipment Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationEquipment object</returns>
        OpLocationEquipment GetLocationEquipment(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationEquipment by id
        /// </summary>
        /// <param name="Ids">LocationEquipment Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationEquipment list</returns>
        List<OpLocationEquipment> GetLocationEquipment(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Deletes the LocationEquipment object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationEquipment to delete</param>
        void DeleteLocationEquipment(OpLocationEquipment toBeDeleted);

        /// <summary>
        /// Update location equipement
        /// </summary>
        /// <param name="IdLocation">ID location</param>
        /// <param name="IdMeterIMRSC">Id meter in IMR_Suite_CORE_IMRSC </param>
        /// <param name="SerialNbr">Device serial number which working with meter(id meter IMRSC)</param>
        /// <param name="isIMRPROD_IMRSC_DB">Is this directly IMR_SC database on IMRPROD server</param>
        /// <param name="IdDistributor">Id distributor of location</param>
        void UpdateLocationEquipmentWithTriada(long IdLocation, long? IdMeterIMRSC, long? SerialNbr, int IdDistributor, bool isIMRPROD_IMRSC_DB);

        /// <summary>
        /// Clears the LocationEquipment object cache
        /// </summary>
        void ClearLocationEquipmentCache();
        #endregion

        #region LocationHierarchy

        /// <summary>
        /// Indicates whether the LocationHierarchy is cached.
        /// </summary>
        bool LocationHierarchyCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationHierarchy objects
        /// </summary>
        List<OpLocationHierarchy> GetAllLocationHierarchy(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets LocationHierarchy by id
        /// </summary>
        /// <param name="Id">LocationHierarchy Id</param>
        /// <returns>LocationHierarchy object</returns>
        OpLocationHierarchy GetLocationHierarchy(long Id);

        /// <summary>
        /// Gets LocationHierarchy by id
        /// </summary>
        /// <param name="Ids">LocationHierarchy Ids</param>
        /// <returns>LocationHierarchy list</returns>
        List<OpLocationHierarchy> GetLocationHierarchy(long[] Ids);

        /// <summary>
        /// Gets LocationHierarchy by id
        /// </summary>
        /// <param name="Id">LocationHierarchy Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationHierarchy object</returns>
        OpLocationHierarchy GetLocationHierarchy(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationHierarchy by id
        /// </summary>
        /// <param name="Ids">LocationHierarchy Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationHierarchy list</returns>
        List<OpLocationHierarchy> GetLocationHierarchy(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Clears the LocationHierarchy object cache
        /// </summary>
        void ClearLocationHierarchyCache();
        #endregion

        #region LocationSeal

        /// <summary>
        /// Indicates whether the LocationSeal is cached.
        /// </summary>
        bool LocationSealCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationSeal objects
        /// </summary>
        List<OpLocationSeal> GetAllLocationSeal();


        /// <summary>
        /// Gets LocationSeal by id
        /// </summary>
        /// <param name="Id">LocationSeal Id</param>
        /// <returns>LocationSeal object</returns>
        OpLocationSeal GetLocationSeal(long Id);

        /// <summary>
        /// Gets LocationSeal by id
        /// </summary>
        /// <param name="Ids">LocationSeal Ids</param>
        /// <returns>LocationSeal list</returns>
        List<OpLocationSeal> GetLocationSeal(long[] Ids);

        /// <summary>
        /// Gets LocationSeal by id
        /// </summary>
        /// <param name="Id">LocationSeal Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationSeal object</returns>
        OpLocationSeal GetLocationSeal(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationSeal by id
        /// </summary>
        /// <param name="Ids">LocationSeal Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationSeal list</returns>
        List<OpLocationSeal> GetLocationSeal(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationSeal object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationSeal to save</param>
        /// <returns>LocationSeal Id</returns>
        long SaveLocationSeal(OpLocationSeal toBeSaved);

        /// <summary>
        /// Deletes the LocationSeal object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationSeal to delete</param>
        void DeleteLocationSeal(OpLocationSeal toBeDeleted);

        /// <summary>
        /// Clears the LocationSeal object cache
        /// </summary>
        void ClearLocationSealCache();
        #endregion

        #region LocationStateHistory

        /// <summary>
        /// Indicates whether the LocationStateHistory is cached.
        /// </summary>
        bool LocationStateHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationStateHistory objects
        /// </summary>
        List<OpLocationStateHistory> GetAllLocationStateHistory();



        /// <summary>
        /// Gets LocationStateHistory by id
        /// </summary>
        /// <param name="Id">LocationStateHistory Id</param>
        /// <returns>LocationStateHistory object</returns>
        OpLocationStateHistory GetLocationStateHistory(long Id);

        /// <summary>
        /// Gets LocationStateHistory by id
        /// </summary>
        /// <param name="Ids">LocationStateHistory Ids</param>
        /// <returns>LocationStateHistory list</returns>
        List<OpLocationStateHistory> GetLocationStateHistory(long[] Ids);

        /// <summary>
        /// Gets LocationStateHistory by id
        /// </summary>
        /// <param name="Id">LocationStateHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationStateHistory object</returns>
        OpLocationStateHistory GetLocationStateHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationStateHistory by id
        /// </summary>
        /// <param name="Ids">LocationStateHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationStateHistory list</returns>
        List<OpLocationStateHistory> GetLocationStateHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the LocationStateHistory object cache
        /// </summary>
        void ClearLocationStateHistoryCache();
        #endregion

        #region LocationStateType

        /// <summary>
        /// Indicates whether the LocationStateType is cached.
        /// </summary>
        bool LocationStateTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationStateType objects
        /// </summary>
        List<OpLocationStateType> GetAllLocationStateType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets LocationStateType by id
        /// </summary>
        /// <param name="Id">LocationStateType Id</param>
        /// <returns>LocationStateType object</returns>
        OpLocationStateType GetLocationStateType(int Id);

        /// <summary>
        /// Gets LocationStateType by id
        /// </summary>
        /// <param name="Ids">LocationStateType Ids</param>
        /// <returns>LocationStateType list</returns>
        List<OpLocationStateType> GetLocationStateType(int[] Ids);

        /// <summary>
        /// Gets LocationStateType by id
        /// </summary>
        /// <param name="Id">LocationStateType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationStateType object</returns>
        OpLocationStateType GetLocationStateType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationStateType by id
        /// </summary>
        /// <param name="Ids">LocationStateType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationStateType list</returns>
        List<OpLocationStateType> GetLocationStateType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the LocationStateType object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationStateType to delete</param>
        void DeleteLocationStateType(OpLocationStateType toBeDeleted);

        /// <summary>
        /// Clears the LocationStateType object cache
        /// </summary>
        void ClearLocationStateTypeCache();
        #endregion

        #region LocationSupplier

        /// <summary>
        /// Indicates whether the LocationSupplier is cached.
        /// </summary>
        bool LocationSupplierCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationSupplier objects
        /// </summary>
        List<OpLocationSupplier> GetAllLocationSupplier();


        /// <summary>
        /// Gets LocationSupplier by id
        /// </summary>
        /// <param name="Id">LocationSupplier Id</param>
        /// <returns>LocationSupplier object</returns>
        OpLocationSupplier GetLocationSupplier(int Id);

        /// <summary>
        /// Gets LocationSupplier by id
        /// </summary>
        /// <param name="Ids">LocationSupplier Ids</param>
        /// <returns>LocationSupplier list</returns>
        List<OpLocationSupplier> GetLocationSupplier(int[] Ids);

        /// <summary>
        /// Gets LocationSupplier by id
        /// </summary>
        /// <param name="Id">LocationSupplier Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationSupplier object</returns>
        OpLocationSupplier GetLocationSupplier(int Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationSupplier by id
        /// </summary>
        /// <param name="Ids">LocationSupplier Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationSupplier list</returns>
        List<OpLocationSupplier> GetLocationSupplier(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationSupplier object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationSupplier to save</param>
        /// <returns>LocationSupplier Id</returns>
        int SaveLocationSupplier(OpLocationSupplier toBeSaved);

        /// <summary>
        /// Deletes the LocationSupplier object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationSupplier to delete</param>
        void DeleteLocationSupplier(OpLocationSupplier toBeDeleted);

        /// <summary>
        /// Clears the LocationSupplier object cache
        /// </summary>
        void ClearLocationSupplierCache();
        #endregion

        #region LocationTariff

        /// <summary>
        /// Indicates whether the LocationTariff is cached.
        /// </summary>
        bool LocationTariffCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationTariff objects
        /// </summary>
        List<OpLocationTariff> GetAllLocationTariff();


        /// <summary>
        /// Gets LocationTariff by id
        /// </summary>
        /// <param name="Id">LocationTariff Id</param>
        /// <returns>LocationTariff object</returns>
        OpLocationTariff GetLocationTariff(int Id);

        /// <summary>
        /// Gets LocationTariff by id
        /// </summary>
        /// <param name="Ids">LocationTariff Ids</param>
        /// <returns>LocationTariff list</returns>
        List<OpLocationTariff> GetLocationTariff(int[] Ids);

        /// <summary>
        /// Gets LocationTariff by id
        /// </summary>
        /// <param name="Id">LocationTariff Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationTariff object</returns>
        OpLocationTariff GetLocationTariff(int Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationTariff by id
        /// </summary>
        /// <param name="Ids">LocationTariff Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationTariff list</returns>
        List<OpLocationTariff> GetLocationTariff(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationTariff object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationTariff to save</param>
        /// <returns>LocationTariff Id</returns>
        int SaveLocationTariff(OpLocationTariff toBeSaved);

        /// <summary>
        /// Deletes the LocationTariff object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationTariff to delete</param>
        void DeleteLocationTariff(OpLocationTariff toBeDeleted);

        /// <summary>
        /// Clears the LocationTariff object cache
        /// </summary>
        void ClearLocationTariffCache();
        #endregion

        #region LocationType

        /// <summary>
        /// Indicates whether the LocationType is cached.
        /// </summary>
        bool LocationTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationType objects
        /// </summary>
        List<OpLocationType> GetAllLocationType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets LocationType by id
        /// </summary>
        /// <param name="Id">LocationType Id</param>
        /// <returns>LocationType object</returns>
        OpLocationType GetLocationType(int Id);

        /// <summary>
        /// Gets LocationType by id
        /// </summary>
        /// <param name="Ids">LocationType Ids</param>
        /// <returns>LocationType list</returns>
        List<OpLocationType> GetLocationType(int[] Ids);

        /// <summary>
        /// Gets LocationType by id
        /// </summary>
        /// <param name="Id">LocationType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationType object</returns>
        OpLocationType GetLocationType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationType by id
        /// </summary>
        /// <param name="Ids">LocationType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationType list</returns>
        List<OpLocationType> GetLocationType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the LocationType object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationType to delete</param>
        void DeleteLocationType(OpLocationType toBeDeleted);

        /// <summary>
        /// Clears the LocationType object cache
        /// </summary>
        void ClearLocationTypeCache();
        #endregion

        #region LocationEntrance

        /// <summary>
        /// Indicates whether the LocationEntrance is cached.
        /// </summary>
        bool LocationEntranceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationEntrance objects
        /// </summary>
        List<OpLocationEntrance> GetAllLocationEntrance();


        /// <summary>
        /// Gets LocationEntrance by id
        /// </summary>
        /// <param name="Id">LocationEntrance Id</param>
        /// <returns>LocationEntrance object</returns>
        OpLocationEntrance GetLocationEntrance(long Id);

        /// <summary>
        /// Gets LocationEntrance by id
        /// </summary>
        /// <param name="Ids">LocationEntrance Ids</param>
        /// <returns>LocationEntrance list</returns>
        List<OpLocationEntrance> GetLocationEntrance(long[] Ids);

        /// <summary>
        /// Gets LocationEntrance by id
        /// </summary>
        /// <param name="Id">LocationEntrance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationEntrance object</returns>
        OpLocationEntrance GetLocationEntrance(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationEntrance by id
        /// </summary>
        /// <param name="Ids">LocationEntrance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationEntrance list</returns>
        List<OpLocationEntrance> GetLocationEntrance(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationEntrance object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationEntrance to save</param>
        /// <returns>LocationEntrance Id</returns>
        long SaveLocationEntrance(OpLocationEntrance toBeSaved);

        /// <summary>
        /// Deletes the LocationEntrance object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationEntrance to delete</param>
        void DeleteLocationEntrance(OpLocationEntrance toBeDeleted);

        /// <summary>
        /// Clears the LocationEntrance object cache
        /// </summary>
        void ClearLocationEntranceCache();
        #endregion

        #region LocationEntranceFloor

        /// <summary>
        /// Indicates whether the LocationEntranceFloor is cached.
        /// </summary>
        bool LocationEntranceFloorCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationEntranceFloor objects
        /// </summary>
        List<OpLocationEntranceFloor> GetAllLocationEntranceFloor();


        /// <summary>
        /// Gets LocationEntranceFloor by id
        /// </summary>
        /// <param name="Id">LocationEntranceFloor Id</param>
        /// <returns>LocationEntranceFloor object</returns>
        OpLocationEntranceFloor GetLocationEntranceFloor(long Id);

        /// <summary>
        /// Gets LocationEntranceFloor by id
        /// </summary>
        /// <param name="Ids">LocationEntranceFloor Ids</param>
        /// <returns>LocationEntranceFloor list</returns>
        List<OpLocationEntranceFloor> GetLocationEntranceFloor(long[] Ids);

        /// <summary>
        /// Gets LocationEntranceFloor by id
        /// </summary>
        /// <param name="Id">LocationEntranceFloor Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationEntranceFloor object</returns>
        OpLocationEntranceFloor GetLocationEntranceFloor(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationEntranceFloor by id
        /// </summary>
        /// <param name="Ids">LocationEntranceFloor Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationEntranceFloor list</returns>
        List<OpLocationEntranceFloor> GetLocationEntranceFloor(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationEntranceFloor object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationEntranceFloor to save</param>
        /// <returns>LocationEntranceFloor Id</returns>
        long SaveLocationEntranceFloor(OpLocationEntranceFloor toBeSaved);

        /// <summary>
        /// Deletes the LocationEntranceFloor object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationEntranceFloor to delete</param>
        void DeleteLocationEntranceFloor(OpLocationEntranceFloor toBeDeleted);

        /// <summary>
        /// Clears the LocationEntranceFloor object cache
        /// </summary>
        void ClearLocationEntranceFloorCache();
        #endregion

        #region Mapping

        /// <summary>
        /// Indicates whether the Mapping is cached.
        /// </summary>
        bool MappingCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Mapping objects
        /// </summary>
        List<OpMapping> GetAllMapping();


        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Id">Mapping Id</param>
        /// <returns>Mapping object</returns>
        OpMapping GetMapping(long Id);

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Ids">Mapping Ids</param>
        /// <returns>Mapping list</returns>
        List<OpMapping> GetMapping(long[] Ids);

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Id">Mapping Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Mapping object</returns>
        OpMapping GetMapping(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Ids">Mapping Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Mapping list</returns>
        List<OpMapping> GetMapping(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Mapping object to the database
        /// </summary>
        /// <param name="toBeSaved">Mapping to save</param>
        /// <returns>Mapping Id</returns>
        long SaveMapping(OpMapping toBeSaved);

        /// <summary>
        /// Deletes the Mapping object from the database
        /// </summary>
        /// <param name="toBeDeleted">Mapping to delete</param>
        void DeleteMapping(OpMapping toBeDeleted);

        /// <summary>
        /// Clears the Mapping object cache
        /// </summary>
        void ClearMappingCache();
        #endregion

        #region Message

        /// <summary>
        /// Indicates whether the Message is cached.
        /// </summary>
        bool MessageCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Message objects
        /// </summary>
        List<OpMessage> GetAllMessage();


        /// <summary>
        /// Gets Message by id
        /// </summary>
        /// <param name="Id">Message Id</param>
        /// <returns>Message object</returns>
        OpMessage GetMessage(long Id);

        /// <summary>
        /// Gets Message by id
        /// </summary>
        /// <param name="Ids">Message Ids</param>
        /// <returns>Message list</returns>
        List<OpMessage> GetMessage(long[] Ids);

        /// <summary>
        /// Gets Message by id
        /// </summary>
        /// <param name="Id">Message Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Message object</returns>
        OpMessage GetMessage(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Message by id
        /// </summary>
        /// <param name="Ids">Message Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Message list</returns>
        List<OpMessage> GetMessage(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Message object to the database
        /// </summary>
        /// <param name="toBeSaved">Message to save</param>
        /// <returns>Message Id</returns>
        long SaveMessage(OpMessage toBeSaved);

        /// <summary>
        /// Deletes the Message object from the database
        /// </summary>
        /// <param name="toBeDeleted">Message to delete</param>
        void DeleteMessage(OpMessage toBeDeleted);

        /// <summary>
        /// Clears the Message object cache
        /// </summary>
        void ClearMessageCache();
        #endregion

        #region MessageData
        /// <summary>
        /// Indicates whether the MessageData is cached.
        /// </summary>
        bool MessageDataCacheEnabled { get; set; }


        /// <summary>
        /// Gets all MessageData objects
        /// </summary>
        List<OpMessageData> GetAllMessageData();


        /// <summary>
        /// Gets MessageData by id
        /// </summary>
        /// <param name="Id">MessageData Id</param>
        /// <returns>MessageData object</returns>
        OpMessageData GetMessageData(long Id);

        /// <summary>
        /// Gets MessageData by id
        /// </summary>
        /// <param name="Ids">MessageData Ids</param>
        /// <returns>MessageData list</returns>
        List<OpMessageData> GetMessageData(long[] Ids);

        /// <summary>
        /// Gets MessageData by id
        /// </summary>
        /// <param name="Id">MessageData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MessageData object</returns>
        OpMessageData GetMessageData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets MessageData by id
        /// </summary>
        /// <param name="Ids">MessageData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MessageData list</returns>
        List<OpMessageData> GetMessageData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the MessageData object to the database
        /// </summary>
        /// <param name="toBeSaved">MessageData to save</param>
        /// <returns>MessageData Id</returns>
        long SaveMessageData(OpMessageData toBeSaved);

        /// <summary>
        /// Deletes the MessageData object from the database
        /// </summary>
        /// <param name="toBeDeleted">MessageData to delete</param>
        void DeleteMessageData(OpMessageData toBeDeleted);

        /// <summary>
        /// Clears the MessageData object cache
        /// </summary>
        void ClearMessageDataCache();
        #endregion

        #region Meter

        /// <summary>
        /// Indicates whether the Meter is cached.
        /// </summary>
        bool MeterCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Meter objects
        /// </summary>
        List<OpMeter> GetAllMeter(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Meter by id
        /// </summary>
        /// <param name="Id">Meter Id</param>
        /// <returns>Meter object</returns>
        OpMeter GetMeter(long Id);

        /// <summary>
        /// Gets Meter by id
        /// </summary>
        /// <param name="Ids">Meter Ids</param>
        /// <returns>Meter list</returns>
        List<OpMeter> GetMeter(long[] Ids);

        /// <summary>
        /// Gets Meter by id
        /// </summary>
        /// <param name="Id">Meter Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Meter object</returns>
        OpMeter GetMeter(long Id, bool queryDatabase);

        //List<OpMeter> GetMeter(long[] Ids, bool queryDatabase) in .User.cs

        /// <summary>
        /// Saves the Meter object to the database
        /// </summary>
        /// <param name="toBeSaved">Meter to save</param>
        /// <returns>Meter Id</returns>
        long SaveMeter(OpMeter toBeSaved);

        /// <summary>
        /// Deletes the Meter object from the database
        /// </summary>
        /// <param name="toBeDeleted">Meter to delete</param>
        void DeleteMeter(OpMeter toBeDeleted);

        /// <summary>
        /// Update meter serial number data type and meter id type
        /// </summary>
        /// <param name="IdMeterIMRSC">ID meter in IMR_Suite_CORE_IMRSC database</param>
        /// <param name="SerialNbr">Device serial number which working with meter(id meter)</param>
        /// <param name="isIMRPROD_IMRSC_DB">Is this directly IMR_SC database on IMRPROD server</param>
        /// <param name="IdMeterType">ID meter type</param>
        void UpdateMeterParameters(long IdMeterIMRSC, string SerialNbr, int IdMeterType, bool isIMRPROD_IMRSC_DB);

        /// <summary>
        /// Clears the Meter object cache
        /// </summary>
        void ClearMeterCache();
        #endregion

        #region MeterType

        /// <summary>
        /// Indicates whether the MeterType is cached.
        /// </summary>
        bool MeterTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterType objects
        /// </summary>
        List<OpMeterType> GetAllMeterType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets MeterType by id
        /// </summary>
        /// <param name="Id">MeterType Id</param>
        /// <returns>MeterType object</returns>
        OpMeterType GetMeterType(int Id);

        /// <summary>
        /// Gets MeterType by id
        /// </summary>
        /// <param name="Ids">MeterType Ids</param>
        /// <returns>MeterType list</returns>
        List<OpMeterType> GetMeterType(int[] Ids);

        /// <summary>
        /// Gets MeterType by id
        /// </summary>
        /// <param name="Id">MeterType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterType object</returns>
        OpMeterType GetMeterType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MeterType by id
        /// </summary>
        /// <param name="Ids">MeterType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterType list</returns>
        List<OpMeterType> GetMeterType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the MeterType object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterType to delete</param>
        void DeleteMeterType(OpMeterType toBeDeleted);

        /// <summary>
        /// Clears the MeterType object cache
        /// </summary>
        void ClearMeterTypeCache();
        #endregion

        #region MeterTypeClass

        /// <summary>
        /// Indicates whether the MeterTypeClass is cached.
        /// </summary>
        bool MeterTypeClassCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterTypeClass objects
        /// </summary>
        List<OpMeterTypeClass> GetAllMeterTypeClass(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets MeterTypeClass by id
        /// </summary>
        /// <param name="Id">MeterTypeClass Id</param>
        /// <returns>MeterTypeClass object</returns>
        OpMeterTypeClass GetMeterTypeClass(int Id);

        /// <summary>
        /// Gets MeterTypeClass by id
        /// </summary>
        /// <param name="Ids">MeterTypeClass Ids</param>
        /// <returns>MeterTypeClass list</returns>
        List<OpMeterTypeClass> GetMeterTypeClass(int[] Ids);

        /// <summary>
        /// Gets MeterTypeClass by id
        /// </summary>
        /// <param name="Id">MeterTypeClass Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeClass object</returns>
        OpMeterTypeClass GetMeterTypeClass(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MeterTypeClass by id
        /// </summary>
        /// <param name="Ids">MeterTypeClass Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeClass list</returns>
        List<OpMeterTypeClass> GetMeterTypeClass(int[] Ids, bool queryDatabase, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        /// <summary>
        /// Deletes the MeterTypeClass object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterTypeClass to delete</param>
        void DeleteMeterTypeClass(OpMeterTypeClass toBeDeleted);

        /// <summary>
        /// Clears the MeterTypeClass object cache
        /// </summary>
        void ClearMeterTypeClassCache();
        #endregion

        #region MeterTypeData

        /// <summary>
        /// Indicates whether the MeterTypeData is cached.
        /// </summary>
        bool MeterTypeDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterTypeData objects
        /// </summary>
        List<OpMeterTypeData> GetAllMeterTypeData();


        /// <summary>
        /// Gets MeterTypeData by id
        /// </summary>
        /// <param name="Id">MeterTypeData Id</param>
        /// <returns>MeterTypeData object</returns>
        OpMeterTypeData GetMeterTypeData(long Id);

        /// <summary>
        /// Gets MeterTypeData by id
        /// </summary>
        /// <param name="Ids">MeterTypeData Ids</param>
        /// <returns>MeterTypeData list</returns>
        List<OpMeterTypeData> GetMeterTypeData(long[] Ids);

        /// <summary>
        /// Gets MeterTypeData by id
        /// </summary>
        /// <param name="Id">MeterTypeData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeData object</returns>
        OpMeterTypeData GetMeterTypeData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets MeterTypeData by id
        /// </summary>
        /// <param name="Ids">MeterTypeData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeData list</returns>
        List<OpMeterTypeData> GetMeterTypeData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the MeterTypeData object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeData to save</param>
        /// <returns>MeterTypeData Id</returns>
        long SaveMeterTypeData(OpMeterTypeData toBeSaved);

        /// <summary>
        /// Deletes the MeterTypeData object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterTypeData to delete</param>
        void DeleteMeterTypeData(OpMeterTypeData toBeDeleted);

        /// <summary>
        /// Clears the MeterTypeData object cache
        /// </summary>
        void ClearMeterTypeDataCache();
        #endregion

        #region MeterTypeGroup

        /// <summary>
        /// Indicates whether the MeterTypeGroup is cached.
        /// </summary>
        bool MeterTypeGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterTypeGroup objects
        /// </summary>
        List<OpMeterTypeGroup> GetAllMeterTypeGroup();


        /// <summary>
        /// Gets MeterTypeGroup by id
        /// </summary>
        /// <param name="Id">MeterTypeGroup Id</param>
        /// <returns>MeterTypeGroup object</returns>
        OpMeterTypeGroup GetMeterTypeGroup(int Id);

        /// <summary>
        /// Gets MeterTypeGroup by id
        /// </summary>
        /// <param name="Ids">MeterTypeGroup Ids</param>
        /// <returns>MeterTypeGroup list</returns>
        List<OpMeterTypeGroup> GetMeterTypeGroup(int[] Ids);

        /// <summary>
        /// Gets MeterTypeGroup by id
        /// </summary>
        /// <param name="Id">MeterTypeGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeGroup object</returns>
        OpMeterTypeGroup GetMeterTypeGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MeterTypeGroup by id
        /// </summary>
        /// <param name="Ids">MeterTypeGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeGroup list</returns>
        List<OpMeterTypeGroup> GetMeterTypeGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the MeterTypeGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeGroup to save</param>
        /// <returns>MeterTypeGroup Id</returns>
        int SaveMeterTypeGroup(OpMeterTypeGroup toBeSaved);

        /// <summary>
        /// Deletes the MeterTypeGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterTypeGroup to delete</param>
        void DeleteMeterTypeGroup(OpMeterTypeGroup toBeDeleted);

        /// <summary>
        /// Clears the MeterTypeGroup object cache
        /// </summary>
        void ClearMeterTypeGroupCache();
        #endregion

        #region MobileNetworkCode

        /// <summary>
        /// Indicates whether the MobileNetworkCode is cached.
        /// </summary>
        bool MobileNetworkCodeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MobileNetworkCode objects
        /// </summary>
        List<OpMobileNetworkCode> GetAllMobileNetworkCode();


        /// <summary>
        /// Gets MobileNetworkCode by id
        /// </summary>
        /// <param name="Id">MobileNetworkCode Id</param>
        /// <returns>MobileNetworkCode object</returns>
        OpMobileNetworkCode GetMobileNetworkCode(int Id);

        /// <summary>
        /// Gets MobileNetworkCode by id
        /// </summary>
        /// <param name="Ids">MobileNetworkCode Ids</param>
        /// <returns>MobileNetworkCode list</returns>
        List<OpMobileNetworkCode> GetMobileNetworkCode(int[] Ids);

        /// <summary>
        /// Gets MobileNetworkCode by id
        /// </summary>
        /// <param name="Id">MobileNetworkCode Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MobileNetworkCode object</returns>
        OpMobileNetworkCode GetMobileNetworkCode(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MobileNetworkCode by id
        /// </summary>
        /// <param name="Ids">MobileNetworkCode Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MobileNetworkCode list</returns>
        List<OpMobileNetworkCode> GetMobileNetworkCode(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the MobileNetworkCode object to the database
        /// </summary>
        /// <param name="toBeSaved">MobileNetworkCode to save</param>
        /// <returns>MobileNetworkCode Id</returns>
        int SaveMobileNetworkCode(OpMobileNetworkCode toBeSaved);

        /// <summary>
        /// Deletes the MobileNetworkCode object from the database
        /// </summary>
        /// <param name="toBeDeleted">MobileNetworkCode to delete</param>
        void DeleteMobileNetworkCode(OpMobileNetworkCode toBeDeleted);

        /// <summary>
        /// Clears the MobileNetworkCode object cache
        /// </summary>
        void ClearMobileNetworkCodeCache();
        #endregion

        #region Module

        /// <summary>
        /// Indicates whether the Module is cached.
        /// </summary>
        bool ModuleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Module objects
        /// </summary>
        List<OpModule> GetAllModule();

        /// <summary>
        /// Gets Module by id
        /// </summary>
        /// <param name="Id">Module Id</param>
        /// <returns>Module object</returns>
        OpModule GetModule(int Id);

        /// <summary>
        /// Gets Module by id
        /// </summary>
        /// <param name="Ids">Module Ids</param>
        /// <returns>Module list</returns>
        List<OpModule> GetModule(int[] Ids);

        /// <summary>
        /// Gets Module by id
        /// </summary>
        /// <param name="Id">Module Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Module object</returns>
        OpModule GetModule(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Module by id
        /// </summary>
        /// <param name="Ids">Module Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Module list</returns>
        List<OpModule> GetModule(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the Module object from the database
        /// </summary>
        /// <param name="toBeDeleted">Module to delete</param>
        void DeleteModule(OpModule toBeDeleted);

        /// <summary>
        /// Clears the Module object cache
        /// </summary>
        void ClearModuleCache();
        #endregion

        #region ModuleData

        /// <summary>
        /// Indicates whether the ModuleData is cached.
        /// </summary>
        bool ModuleDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ModuleData objects
        /// </summary>
        List<OpModuleData> GetAllModuleData();


        /// <summary>
        /// Gets ModuleData by id
        /// </summary>
        /// <param name="Id">ModuleData Id</param>
        /// <returns>ModuleData object</returns>
        OpModuleData GetModuleData(long Id);

        /// <summary>
        /// Gets ModuleData by id
        /// </summary>
        /// <param name="Ids">ModuleData Ids</param>
        /// <returns>ModuleData list</returns>
        List<OpModuleData> GetModuleData(long[] Ids);

        /// <summary>
        /// Gets ModuleData by id
        /// </summary>
        /// <param name="Id">ModuleData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ModuleData object</returns>
        OpModuleData GetModuleData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ModuleData by id
        /// </summary>
        /// <param name="Ids">ModuleData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ModuleData list</returns>
        List<OpModuleData> GetModuleData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ModuleData object to the database
        /// </summary>
        /// <param name="toBeSaved">ModuleData to save</param>
        /// <returns>ModuleData Id</returns>
        long SaveModuleData(OpModuleData toBeSaved);

        /// <summary>
        /// Deletes the ModuleData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ModuleData to delete</param>
        void DeleteModuleData(OpModuleData toBeDeleted);

        /// <summary>
        /// Clears the ModuleData object cache
        /// </summary>
        void ClearModuleDataCache();
        #endregion

        #region MonitoringWatcher

        /// <summary>
        /// Indicates whether the MonitoringWatcher is cached.
        /// </summary>
        bool MonitoringWatcherCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcher objects
        /// </summary>
        List<OpMonitoringWatcher> GetAllMonitoringWatcher();


        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Id">MonitoringWatcher Id</param>
        /// <returns>MonitoringWatcher object</returns>
        OpMonitoringWatcher GetMonitoringWatcher(int Id);

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcher Ids</param>
        /// <returns>MonitoringWatcher list</returns>
        List<OpMonitoringWatcher> GetMonitoringWatcher(int[] Ids);

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Id">MonitoringWatcher Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcher object</returns>
        OpMonitoringWatcher GetMonitoringWatcher(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcher Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcher list</returns>
        List<OpMonitoringWatcher> GetMonitoringWatcher(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the MonitoringWatcher object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcher to save</param>
        /// <returns>MonitoringWatcher Id</returns>
        int SaveMonitoringWatcher(OpMonitoringWatcher toBeSaved);

        /// <summary>
        /// Deletes the MonitoringWatcher object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcher to delete</param>
        void DeleteMonitoringWatcher(OpMonitoringWatcher toBeDeleted);

        /// <summary>
        /// Clears the MonitoringWatcher object cache
        /// </summary>
        void ClearMonitoringWatcherCache();
        #endregion

        #region MonitoringWatcherData

        /// <summary>
        /// Indicates whether the MonitoringWatcherData is cached.
        /// </summary>
        bool MonitoringWatcherDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcherData objects
        /// </summary>
        List<OpMonitoringWatcherData> GetAllMonitoringWatcherData();


        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherData Id</param>
        /// <returns>MonitoringWatcherData object</returns>
        OpMonitoringWatcherData GetMonitoringWatcherData(int Id);

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherData Ids</param>
        /// <returns>MonitoringWatcherData list</returns>
        List<OpMonitoringWatcherData> GetMonitoringWatcherData(int[] Ids);

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherData object</returns>
        OpMonitoringWatcherData GetMonitoringWatcherData(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherData list</returns>
        List<OpMonitoringWatcherData> GetMonitoringWatcherData(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the MonitoringWatcherData object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcherData to save</param>
        /// <returns>MonitoringWatcherData Id</returns>
        int SaveMonitoringWatcherData(OpMonitoringWatcherData toBeSaved);

        /// <summary>
        /// Deletes the MonitoringWatcherData object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcherData to delete</param>
        void DeleteMonitoringWatcherData(OpMonitoringWatcherData toBeDeleted);

        /// <summary>
        /// Clears the MonitoringWatcherData object cache
        /// </summary>
        void ClearMonitoringWatcherDataCache();
        #endregion

        #region MonitoringWatcherType

        /// <summary>
        /// Indicates whether the MonitoringWatcherType is cached.
        /// </summary>
        bool MonitoringWatcherTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcherType objects
        /// </summary>
        List<OpMonitoringWatcherType> GetAllMonitoringWatcherType();


        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherType Id</param>
        /// <returns>MonitoringWatcherType object</returns>
        OpMonitoringWatcherType GetMonitoringWatcherType(int Id);

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherType Ids</param>
        /// <returns>MonitoringWatcherType list</returns>
        List<OpMonitoringWatcherType> GetMonitoringWatcherType(int[] Ids);

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherType object</returns>
        OpMonitoringWatcherType GetMonitoringWatcherType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherType list</returns>
        List<OpMonitoringWatcherType> GetMonitoringWatcherType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the MonitoringWatcherType object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcherType to save</param>
        /// <returns>MonitoringWatcherType Id</returns>
        int SaveMonitoringWatcherType(OpMonitoringWatcherType toBeSaved);

        /// <summary>
        /// Deletes the MonitoringWatcherType object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcherType to delete</param>
        void DeleteMonitoringWatcherType(OpMonitoringWatcherType toBeDeleted);

        /// <summary>
        /// Clears the MonitoringWatcherType object cache
        /// </summary>
        void ClearMonitoringWatcherTypeCache();
        #endregion

        #region NotificationDeliveryType

        /// <summary>
        /// Indicates whether the NotificationDeliveryType is cached.
        /// </summary>
        bool NotificationDeliveryTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all NotificationDeliveryType objects
        /// </summary>
        List<OpNotificationDeliveryType> GetAllNotificationDeliveryType();


        /// <summary>
        /// Gets NotificationDeliveryType by id
        /// </summary>
        /// <param name="Id">NotificationDeliveryType Id</param>
        /// <returns>NotificationDeliveryType object</returns>
        OpNotificationDeliveryType GetNotificationDeliveryType(int Id);

        /// <summary>
        /// Gets NotificationDeliveryType by id
        /// </summary>
        /// <param name="Ids">NotificationDeliveryType Ids</param>
        /// <returns>NotificationDeliveryType list</returns>
        List<OpNotificationDeliveryType> GetNotificationDeliveryType(int[] Ids);

        /// <summary>
        /// Gets NotificationDeliveryType by id
        /// </summary>
        /// <param name="Id">NotificationDeliveryType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>NotificationDeliveryType object</returns>
        OpNotificationDeliveryType GetNotificationDeliveryType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets NotificationDeliveryType by id
        /// </summary>
        /// <param name="Ids">NotificationDeliveryType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>NotificationDeliveryType list</returns>
        List<OpNotificationDeliveryType> GetNotificationDeliveryType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the NotificationDeliveryType object to the database
        /// </summary>
        /// <param name="toBeSaved">NotificationDeliveryType to save</param>
        /// <returns>NotificationDeliveryType Id</returns>
        int SaveNotificationDeliveryType(OpNotificationDeliveryType toBeSaved);

        /// <summary>
        /// Deletes the NotificationDeliveryType object from the database
        /// </summary>
        /// <param name="toBeDeleted">NotificationDeliveryType to delete</param>
        void DeleteNotificationDeliveryType(OpNotificationDeliveryType toBeDeleted);

        /// <summary>
        /// Clears the NotificationDeliveryType object cache
        /// </summary>
        void ClearNotificationDeliveryTypeCache();
        #endregion

        #region NotificationType

        /// <summary>
        /// Indicates whether the NotificationType is cached.
        /// </summary>
        bool NotificationTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all NotificationType objects
        /// </summary>
        List<OpNotificationType> GetAllNotificationType();


        /// <summary>
        /// Gets NotificationType by id
        /// </summary>
        /// <param name="Id">NotificationType Id</param>
        /// <returns>NotificationType object</returns>
        OpNotificationType GetNotificationType(int Id);

        /// <summary>
        /// Gets NotificationType by id
        /// </summary>
        /// <param name="Ids">NotificationType Ids</param>
        /// <returns>NotificationType list</returns>
        List<OpNotificationType> GetNotificationType(int[] Ids);

        /// <summary>
        /// Gets NotificationType by id
        /// </summary>
        /// <param name="Id">NotificationType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>NotificationType object</returns>
        OpNotificationType GetNotificationType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets NotificationType by id
        /// </summary>
        /// <param name="Ids">NotificationType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>NotificationType list</returns>
        List<OpNotificationType> GetNotificationType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the NotificationType object to the database
        /// </summary>
        /// <param name="toBeSaved">NotificationType to save</param>
        /// <returns>NotificationType Id</returns>
        int SaveNotificationType(OpNotificationType toBeSaved);

        /// <summary>
        /// Deletes the NotificationType object from the database
        /// </summary>
        /// <param name="toBeDeleted">NotificationType to delete</param>
        void DeleteNotificationType(OpNotificationType toBeDeleted);

        /// <summary>
        /// Clears the NotificationType object cache
        /// </summary>
        void ClearNotificationTypeCache();
        #endregion

        #region Operator

        /// <summary>
        /// Indicates whether the Operator is cached.
        /// </summary>
        bool OperatorCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Operator objects
        /// </summary>
        List<OpOperator> GetAllOperator(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Operator by id
        /// </summary>
        /// <param name="Id">Operator Id</param>
        /// <returns>Operator object</returns>
        OpOperator GetOperator(int Id);

        /// <summary>
        /// Gets Operator by id
        /// </summary>
        /// <param name="Ids">Operator Ids</param>
        /// <returns>Operator list</returns>
        List<OpOperator> GetOperator(int[] Ids);

        /// <summary>
        /// Gets Operator by id
        /// </summary>
        /// <param name="Id">Operator Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Operator object</returns>
        OpOperator GetOperator(int Id, bool queryDatabase);

        /// <summary>
        /// Saves the Operator object to the database
        /// </summary>
        /// <param name="toBeSaved">Operator to save</param>
        /// <returns>Operator Id</returns>
        int SaveOperator(OpOperator toBeSaved);

        /// <summary>
        /// Deletes the Operator object from the database
        /// </summary>
        /// <param name="toBeDeleted">Operator to delete</param>
        void DeleteOperator(OpOperator toBeDeleted);

        /// <summary>
        /// Clears the Operator object cache
        /// </summary>
        void ClearOperatorCache();
        #endregion

        #region OperatorActivity

        /// <summary>
        /// Indicates whether the OperatorActivity is cached.
        /// </summary>
        bool OperatorActivityCacheEnabled { get; set; }

        /// <summary>
        /// Gets all OperatorActivity objects
        /// </summary>
        List<OpOperatorActivity> GetAllOperatorActivity();


        /// <summary>
        /// Gets OperatorActivity by id
        /// </summary>
        /// <param name="Id">OperatorActivity Id</param>
        /// <returns>OperatorActivity object</returns>
        OpOperatorActivity GetOperatorActivity(long Id);

        /// <summary>
        /// Gets OperatorActivity by id
        /// </summary>
        /// <param name="Ids">OperatorActivity Ids</param>
        /// <returns>OperatorActivity list</returns>
        List<OpOperatorActivity> GetOperatorActivity(long[] Ids);

        /// <summary>
        /// Gets OperatorActivity by id
        /// </summary>
        /// <param name="Id">OperatorActivity Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OperatorActivity object</returns>
        OpOperatorActivity GetOperatorActivity(long Id, bool queryDatabase);

        /// <summary>
        /// Gets OperatorActivity by id
        /// </summary>
        /// <param name="Ids">OperatorActivity Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OperatorActivity list</returns>
        List<OpOperatorActivity> GetOperatorActivity(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the OperatorActivity object to the database
        /// </summary>
        /// <param name="toBeSaved">OperatorActivity to save</param>
        /// <returns>OperatorActivity Id</returns>
        long SaveOperatorActivity(OpOperatorActivity toBeSaved);

        /// <summary>
        /// Deletes the OperatorActivity object from the database
        /// </summary>
        /// <param name="toBeDeleted">OperatorActivity to delete</param>
        void DeleteOperatorActivity(OpOperatorActivity toBeDeleted);

        /// <summary>
        /// Clears the OperatorActivity object cache
        /// </summary>
        void ClearOperatorActivityCache();
        #endregion

        #region OperatorData

        /// <summary>
        /// Indicates whether the OperatorData is cached.
        /// </summary>
        bool OperatorDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all OperatorData objects
        /// </summary>
        List<OpOperatorData> GetAllOperatorData();


        /// <summary>
        /// Gets OperatorData by id
        /// </summary>
        /// <param name="Id">OperatorData Id</param>
        /// <returns>OperatorData object</returns>
        OpOperatorData GetOperatorData(long Id);

        /// <summary>
        /// Gets OperatorData by id
        /// </summary>
        /// <param name="Ids">OperatorData Ids</param>
        /// <returns>OperatorData list</returns>
        List<OpOperatorData> GetOperatorData(long[] Ids);

        /// <summary>
        /// Gets OperatorData by id
        /// </summary>
        /// <param name="Id">OperatorData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OperatorData object</returns>
        OpOperatorData GetOperatorData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets OperatorData by id
        /// </summary>
        /// <param name="Ids">OperatorData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OperatorData list</returns>
        List<OpOperatorData> GetOperatorData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the OperatorData object cache
        /// </summary>
        void ClearOperatorDataCache();
        #endregion

        #region Order

        /// <summary>
        /// Indicates whether the Order is cached.
        /// </summary>
        bool OrderCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Order objects
        /// </summary>
        List<OpOrder> GetAllOrder();


        /// <summary>
        /// Gets Order by id
        /// </summary>
        /// <param name="Id">Order Id</param>
        /// <returns>Order object</returns>
        OpOrder GetOrder(int Id);

        /// <summary>
        /// Gets Order by id
        /// </summary>
        /// <param name="Ids">Order Ids</param>
        /// <returns>Order list</returns>
        List<OpOrder> GetOrder(int[] Ids);

        /// <summary>
        /// Gets Order by id
        /// </summary>
        /// <param name="Id">Order Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Order object</returns>
        OpOrder GetOrder(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Order by id
        /// </summary>
        /// <param name="Ids">Order Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Order list</returns>
        List<OpOrder> GetOrder(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Order object to the database
        /// </summary>
        /// <param name="toBeSaved">Order to save</param>
        /// <returns>Order Id</returns>
        int SaveOrder(OpOrder toBeSaved);

        /// <summary>
        /// Deletes the Order object from the database
        /// </summary>
        /// <param name="toBeDeleted">Order to delete</param>
        void DeleteOrder(OpOrder toBeDeleted);

        /// <summary>
        /// Clears the Order object cache
        /// </summary>
        void ClearOrderCache();
        #endregion

        #region OrderData

        /// <summary>
        /// Indicates whether the OrderData is cached.
        /// </summary>
        bool OrderDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all OrderData objects
        /// </summary>
        List<OpOrderData> GetAllOrderData();


        /// <summary>
        /// Gets OrderData by id
        /// </summary>
        /// <param name="Id">OrderData Id</param>
        /// <returns>OrderData object</returns>
        OpOrderData GetOrderData(long Id);

        /// <summary>
        /// Gets OrderData by id
        /// </summary>
        /// <param name="Ids">OrderData Ids</param>
        /// <returns>OrderData list</returns>
        List<OpOrderData> GetOrderData(long[] Ids);

        /// <summary>
        /// Gets OrderData by id
        /// </summary>
        /// <param name="Id">OrderData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OrderData object</returns>
        OpOrderData GetOrderData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets OrderData by id
        /// </summary>
        /// <param name="Ids">OrderData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OrderData list</returns>
        List<OpOrderData> GetOrderData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the OrderData object to the database
        /// </summary>
        /// <param name="toBeSaved">OrderData to save</param>
        /// <returns>OrderData Id</returns>
        long SaveOrderData(OpOrderData toBeSaved);

        /// <summary>
        /// Deletes the OrderData object from the database
        /// </summary>
        /// <param name="toBeDeleted">OrderData to delete</param>
        void DeleteOrderData(OpOrderData toBeDeleted);

        /// <summary>
        /// Clears the OrderData object cache
        /// </summary>
        void ClearOrderDataCache();
        #endregion

        #region OrderStateType

        /// <summary>
        /// Indicates whether the OrderStateType is cached.
        /// </summary>
        bool OrderStateTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all OrderStateType objects
        /// </summary>
        List<OpOrderStateType> GetAllOrderStateType();


        /// <summary>
        /// Gets OrderStateType by id
        /// </summary>
        /// <param name="Id">OrderStateType Id</param>
        /// <returns>OrderStateType object</returns>
        OpOrderStateType GetOrderStateType(int Id);

        /// <summary>
        /// Gets OrderStateType by id
        /// </summary>
        /// <param name="Ids">OrderStateType Ids</param>
        /// <returns>OrderStateType list</returns>
        List<OpOrderStateType> GetOrderStateType(int[] Ids);

        /// <summary>
        /// Gets OrderStateType by id
        /// </summary>
        /// <param name="Id">OrderStateType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OrderStateType object</returns>
        OpOrderStateType GetOrderStateType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets OrderStateType by id
        /// </summary>
        /// <param name="Ids">OrderStateType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OrderStateType list</returns>
        List<OpOrderStateType> GetOrderStateType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the OrderStateType object to the database
        /// </summary>
        /// <param name="toBeSaved">OrderStateType to save</param>
        /// <returns>OrderStateType Id</returns>
        int SaveOrderStateType(OpOrderStateType toBeSaved);

        /// <summary>
        /// Deletes the OrderStateType object from the database
        /// </summary>
        /// <param name="toBeDeleted">OrderStateType to delete</param>
        void DeleteOrderStateType(OpOrderStateType toBeDeleted);

        /// <summary>
        /// Clears the OrderStateType object cache
        /// </summary>
        void ClearOrderStateTypeCache();
        #endregion

        #region Package

        /// <summary>
        /// Indicates whether the Package is cached.
        /// </summary>
        bool PackageCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Package objects
        /// </summary>
        List<OpPackage> GetAllPackage();


        /// <summary>
        /// Gets Package by id
        /// </summary>
        /// <param name="Id">Package Id</param>
        /// <returns>Package object</returns>
        OpPackage GetPackage(int Id);

        /// <summary>
        /// Gets Package by id
        /// </summary>
        /// <param name="Ids">Package Ids</param>
        /// <returns>Package list</returns>
        List<OpPackage> GetPackage(int[] Ids);

        /// <summary>
        /// Gets Package by id
        /// </summary>
        /// <param name="Id">Package Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Package object</returns>
        OpPackage GetPackage(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Package by id
        /// </summary>
        /// <param name="Ids">Package Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Package list</returns>
        List<OpPackage> GetPackage(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Package object to the database
        /// </summary>
        /// <param name="toBeSaved">Package to save</param>
        /// <returns>Package Id</returns>
        int SavePackage(OpPackage toBeSaved);

        /// <summary>
        /// Deletes the Package object from the database
        /// </summary>
        /// <param name="toBeDeleted">Package to delete</param>
        void DeletePackage(OpPackage toBeDeleted);

        /// <summary>
        /// Clears the Package object cache
        /// </summary>
        void ClearPackageCache();
        #endregion

        #region PackageStatus

        /// <summary>
        /// Indicates whether the PackageStatus is cached.
        /// </summary>
        bool PackageStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PackageStatus objects
        /// </summary>
        List<OpPackageStatus> GetAllPackageStatus();


        /// <summary>
        /// Gets PackageStatus by id
        /// </summary>
        /// <param name="Id">PackageStatus Id</param>
        /// <returns>PackageStatus object</returns>
        OpPackageStatus GetPackageStatus(int Id);

        /// <summary>
        /// Gets PackageStatus by id
        /// </summary>
        /// <param name="Ids">PackageStatus Ids</param>
        /// <returns>PackageStatus list</returns>
        List<OpPackageStatus> GetPackageStatus(int[] Ids);

        /// <summary>
        /// Gets PackageStatus by id
        /// </summary>
        /// <param name="Id">PackageStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PackageStatus object</returns>
        OpPackageStatus GetPackageStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets PackageStatus by id
        /// </summary>
        /// <param name="Ids">PackageStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PackageStatus list</returns>
        List<OpPackageStatus> GetPackageStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the PackageStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">PackageStatus to save</param>
        /// <returns>PackageStatus Id</returns>
        int SavePackageStatus(OpPackageStatus toBeSaved);

        /// <summary>
        /// Deletes the PackageStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">PackageStatus to delete</param>
        void DeletePackageStatus(OpPackageStatus toBeDeleted);

        /// <summary>
        /// Clears the PackageStatus object cache
        /// </summary>
        void ClearPackageStatusCache();

        #endregion

        #region PackageStatusHistory

        /// <summary>
        /// Indicates whether the PackageStatusHistory is cached.
        /// </summary>
        bool PackageStatusHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PackageStatusHistory objects
        /// </summary>
        List<OpPackageStatusHistory> GetAllPackageStatusHistory();


        /// <summary>
        /// Gets PackageStatusHistory by id
        /// </summary>
        /// <param name="Id">PackageStatusHistory Id</param>
        /// <returns>PackageStatusHistory object</returns>
        OpPackageStatusHistory GetPackageStatusHistory(int Id);

        /// <summary>
        /// Gets PackageStatusHistory by id
        /// </summary>
        /// <param name="Ids">PackageStatusHistory Ids</param>
        /// <returns>PackageStatusHistory list</returns>
        List<OpPackageStatusHistory> GetPackageStatusHistory(int[] Ids);

        /// <summary>
        /// Gets PackageStatusHistory by id
        /// </summary>
        /// <param name="Id">PackageStatusHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PackageStatusHistory object</returns>
        OpPackageStatusHistory GetPackageStatusHistory(int Id, bool queryDatabase);

        /// <summary>
        /// Gets PackageStatusHistory by id
        /// </summary>
        /// <param name="Ids">PackageStatusHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PackageStatusHistory list</returns>
        List<OpPackageStatusHistory> GetPackageStatusHistory(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the PackageStatusHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">PackageStatusHistory to save</param>
        /// <returns>PackageStatusHistory Id</returns>
        int SavePackageStatusHistory(OpPackageStatusHistory toBeSaved);

        /// <summary>
        /// Deletes the PackageStatusHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">PackageStatusHistory to delete</param>
        void DeletePackageStatusHistory(OpPackageStatusHistory toBeDeleted);

        /// <summary>
        /// Clears the PackageStatusHistory object cache
        /// </summary>
        void ClearPackageStatusHistoryCache();
        #endregion

        #region PackageData

        /// <summary>
        /// Indicates whether the PackageData is cached.
        /// </summary>
        bool PackageDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PackageData objects
        /// </summary>
        List<OpPackageData> GetAllPackageData();


        /// <summary>
        /// Gets PackageData by id
        /// </summary>
        /// <param name="Id">PackageData Id</param>
        /// <returns>PackageData object</returns>
        OpPackageData GetPackageData(long Id);

        /// <summary>
        /// Gets PackageData by id
        /// </summary>
        /// <param name="Ids">PackageData Ids</param>
        /// <returns>PackageData list</returns>
        List<OpPackageData> GetPackageData(long[] Ids);

        /// <summary>
        /// Gets PackageData by id
        /// </summary>
        /// <param name="Id">PackageData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PackageData object</returns>
        OpPackageData GetPackageData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets PackageData by id
        /// </summary>
        /// <param name="Ids">PackageData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PackageData list</returns>
        List<OpPackageData> GetPackageData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the PackageData object to the database
        /// </summary>
        /// <param name="toBeSaved">PackageData to save</param>
        /// <returns>PackageData Id</returns>
        long SavePackageData(OpPackageData toBeSaved);

        /// <summary>
        /// Deletes the PackageData object from the database
        /// </summary>
        /// <param name="toBeDeleted">PackageData to delete</param>
        void DeletePackageData(OpPackageData toBeDeleted);

        /// <summary>
        /// Clears the PackageData object cache
        /// </summary>
        void ClearPackageDataCache();
        #endregion

        #region PaymentModule

        /// <summary>
        /// Indicates whether the PaymentModule is cached.
        /// </summary>
        bool PaymentModuleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PaymentModule objects
        /// </summary>
        List<OpPaymentModule> GetAllPaymentModule();


        /// <summary>
        /// Gets PaymentModule by id
        /// </summary>
        /// <param name="Id">PaymentModule Id</param>
        /// <returns>PaymentModule object</returns>
        OpPaymentModule GetPaymentModule(int Id);

        /// <summary>
        /// Gets PaymentModule by id
        /// </summary>
        /// <param name="Ids">PaymentModule Ids</param>
        /// <returns>PaymentModule list</returns>
        List<OpPaymentModule> GetPaymentModule(int[] Ids);

        /// <summary>
        /// Gets PaymentModule by id
        /// </summary>
        /// <param name="Id">PaymentModule Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PaymentModule object</returns>
        OpPaymentModule GetPaymentModule(int Id, bool queryDatabase);

        /// <summary>
        /// Gets PaymentModule by id
        /// </summary>
        /// <param name="Ids">PaymentModule Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PaymentModule list</returns>
        List<OpPaymentModule> GetPaymentModule(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the PaymentModule object to the database
        /// </summary>
        /// <param name="toBeSaved">PaymentModule to save</param>
        /// <returns>PaymentModule Id</returns>
        int SavePaymentModule(OpPaymentModule toBeSaved);

        /// <summary>
        /// Deletes the PaymentModule object from the database
        /// </summary>
        /// <param name="toBeDeleted">PaymentModule to delete</param>
        void DeletePaymentModule(OpPaymentModule toBeDeleted);

        /// <summary>
        /// Clears the PaymentModule object cache
        /// </summary>
        void ClearPaymentModuleCache();
        #endregion

        #region PaymentModuleData

        /// <summary>
        /// Indicates whether the PaymentModuleData is cached.
        /// </summary>
        bool PaymentModuleDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PaymentModuleData objects
        /// </summary>
        List<OpPaymentModuleData> GetAllPaymentModuleData();


        /// <summary>
        /// Gets PaymentModuleData by id
        /// </summary>
        /// <param name="Id">PaymentModuleData Id</param>
        /// <returns>PaymentModuleData object</returns>
        OpPaymentModuleData GetPaymentModuleData(long Id);

        /// <summary>
        /// Gets PaymentModuleData by id
        /// </summary>
        /// <param name="Ids">PaymentModuleData Ids</param>
        /// <returns>PaymentModuleData list</returns>
        List<OpPaymentModuleData> GetPaymentModuleData(long[] Ids);

        /// <summary>
        /// Gets PaymentModuleData by id
        /// </summary>
        /// <param name="Id">PaymentModuleData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PaymentModuleData object</returns>
        OpPaymentModuleData GetPaymentModuleData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets PaymentModuleData by id
        /// </summary>
        /// <param name="Ids">PaymentModuleData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PaymentModuleData list</returns>
        List<OpPaymentModuleData> GetPaymentModuleData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the PaymentModuleData object to the database
        /// </summary>
        /// <param name="toBeSaved">PaymentModuleData to save</param>
        /// <returns>PaymentModuleData Id</returns>
        long SavePaymentModuleData(OpPaymentModuleData toBeSaved);

        /// <summary>
        /// Deletes the PaymentModuleData object from the database
        /// </summary>
        /// <param name="toBeDeleted">PaymentModuleData to delete</param>
        void DeletePaymentModuleData(OpPaymentModuleData toBeDeleted);

        /// <summary>
        /// Clears the PaymentModuleData object cache
        /// </summary>
        void ClearPaymentModuleDataCache();
        #endregion

        #region PaymentModuleType

        /// <summary>
        /// Indicates whether the PaymentModuleType is cached.
        /// </summary>
        bool PaymentModuleTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PaymentModuleType objects
        /// </summary>
        List<OpPaymentModuleType> GetAllPaymentModuleType();


        /// <summary>
        /// Gets PaymentModuleType by id
        /// </summary>
        /// <param name="Id">PaymentModuleType Id</param>
        /// <returns>PaymentModuleType object</returns>
        OpPaymentModuleType GetPaymentModuleType(int Id);

        /// <summary>
        /// Gets PaymentModuleType by id
        /// </summary>
        /// <param name="Ids">PaymentModuleType Ids</param>
        /// <returns>PaymentModuleType list</returns>
        List<OpPaymentModuleType> GetPaymentModuleType(int[] Ids);

        /// <summary>
        /// Gets PaymentModuleType by id
        /// </summary>
        /// <param name="Id">PaymentModuleType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PaymentModuleType object</returns>
        OpPaymentModuleType GetPaymentModuleType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets PaymentModuleType by id
        /// </summary>
        /// <param name="Ids">PaymentModuleType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PaymentModuleType list</returns>
        List<OpPaymentModuleType> GetPaymentModuleType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the PaymentModuleType object to the database
        /// </summary>
        /// <param name="toBeSaved">PaymentModuleType to save</param>
        /// <returns>PaymentModuleType Id</returns>
        int SavePaymentModuleType(OpPaymentModuleType toBeSaved);

        /// <summary>
        /// Deletes the PaymentModuleType object from the database
        /// </summary>
        /// <param name="toBeDeleted">PaymentModuleType to delete</param>
        void DeletePaymentModuleType(OpPaymentModuleType toBeDeleted);

        /// <summary>
        /// Clears the PaymentModuleType object cache
        /// </summary>
        void ClearPaymentModuleTypeCache();
        #endregion

        #region Priority

        /// <summary>
        /// Indicates whether the Priority is cached.
        /// </summary>
        bool PriorityCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Priority objects
        /// </summary>
        List<OpPriority> GetAllPriority(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Priority by id
        /// </summary>
        /// <param name="Id">Priority Id</param>
        /// <returns>Priority object</returns>
        OpPriority GetPriority(int Id);

        /// <summary>
        /// Gets Priority by id
        /// </summary>
        /// <param name="Ids">Priority Ids</param>
        /// <returns>Priority list</returns>
        List<OpPriority> GetPriority(int[] Ids);

        /// <summary>
        /// Gets Priority by id
        /// </summary>
        /// <param name="Id">Priority Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Priority object</returns>
        OpPriority GetPriority(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Priority by id
        /// </summary>
        /// <param name="Ids">Priority Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Priority list</returns>
        List<OpPriority> GetPriority(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Priority object to the database
        /// </summary>
        /// <param name="toBeSaved">Priority to save</param>
        /// <returns>Priority Id</returns>
        int SavePriority(OpPriority toBeSaved);

        /// <summary>
        /// Deletes the Priority object from the database
        /// </summary>
        /// <param name="toBeDeleted">Priority to delete</param>
        void DeletePriority(OpPriority toBeDeleted);

        /// <summary>
        /// Clears the Priority object cache
        /// </summary>
        void ClearPriorityCache();
        #endregion

        #region ProductCode

        /// <summary>
        /// Indicates whether the ProductCode is cached.
        /// </summary>
        bool ProductCodeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ProductCode objects
        /// </summary>
        List<OpProductCode> GetAllProductCode(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ProductCode by id
        /// </summary>
        /// <param name="Id">ProductCode Id</param>
        /// <returns>ProductCode object</returns>
        OpProductCode GetProductCode(int Id);

        /// <summary>
        /// Gets ProductCode by id
        /// </summary>
        /// <param name="Ids">ProductCode Ids</param>
        /// <returns>ProductCode list</returns>
        List<OpProductCode> GetProductCode(int[] Ids);

        /// <summary>
        /// Gets ProductCode by id
        /// </summary>
        /// <param name="Id">ProductCode Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProductCode object</returns>
        OpProductCode GetProductCode(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ProductCode by id
        /// </summary>
        /// <param name="Ids">ProductCode Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProductCode list</returns>
        List<OpProductCode> GetProductCode(int[] Ids, bool queryDatabase, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        /// <summary>
        /// Saves the ProductCode object to the database
        /// </summary>
        /// <param name="toBeSaved">ProductCode to save</param>
        /// <returns>ProductCode Id</returns>
        int SaveProductCode(OpProductCode toBeSaved);

        /// <summary>
        /// Deletes the ProductCode object from the database
        /// </summary>
        /// <param name="toBeDeleted">ProductCode to delete</param>
        void DeleteProductCode(OpProductCode toBeDeleted);

        /// <summary>
        /// Clears the ProductCode object cache
        /// </summary>
        void ClearProductCodeCache();
        #endregion

        #region Profile

        /// <summary>
        /// Indicates whether the Profile is cached.
        /// </summary>
        bool ProfileCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Profile objects
        /// </summary>
        List<OpProfile> GetAllProfile();


        /// <summary>
        /// Gets Profile by id
        /// </summary>
        /// <param name="Id">Profile Id</param>
        /// <returns>Profile object</returns>
        OpProfile GetProfile(int Id);

        /// <summary>
        /// Gets Profile by id
        /// </summary>
        /// <param name="Ids">Profile Ids</param>
        /// <returns>Profile list</returns>
        List<OpProfile> GetProfile(int[] Ids);

        /// <summary>
        /// Gets Profile by id
        /// </summary>
        /// <param name="Id">Profile Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Profile object</returns>
        OpProfile GetProfile(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Profile by id
        /// </summary>
        /// <param name="Ids">Profile Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Profile list</returns>
        List<OpProfile> GetProfile(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Profile object to the database
        /// </summary>
        /// <param name="toBeSaved">Profile to save</param>
        /// <returns>Profile Id</returns>
        int SaveProfile(OpProfile toBeSaved);

        /// <summary>
        /// Deletes the Profile object from the database
        /// </summary>
        /// <param name="toBeDeleted">Profile to delete</param>
        void DeleteProfile(OpProfile toBeDeleted);

        /// <summary>
        /// Clears the Profile object cache
        /// </summary>
        void ClearProfileCache();
        #endregion

        #region ProfileData

        /// <summary>
        /// Indicates whether the ProfileData is cached.
        /// </summary>
        bool ProfileDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ProfileData objects
        /// </summary>
        List<OpProfileData> GetAllProfileData();


        /// <summary>
        /// Gets ProfileData by id
        /// </summary>
        /// <param name="Id">ProfileData Id</param>
        /// <returns>ProfileData object</returns>
        OpProfileData GetProfileData(long Id);

        /// <summary>
        /// Gets ProfileData by id
        /// </summary>
        /// <param name="Ids">ProfileData Ids</param>
        /// <returns>ProfileData list</returns>
        List<OpProfileData> GetProfileData(long[] Ids);

        /// <summary>
        /// Gets ProfileData by id
        /// </summary>
        /// <param name="Id">ProfileData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProfileData object</returns>
        OpProfileData GetProfileData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ProfileData by id
        /// </summary>
        /// <param name="Ids">ProfileData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProfileData list</returns>
        List<OpProfileData> GetProfileData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ProfileData object to the database
        /// </summary>
        /// <param name="toBeSaved">ProfileData to save</param>
        /// <returns>ProfileData Id</returns>
        long SaveProfileData(OpProfileData toBeSaved);

        /// <summary>
        /// Deletes the ProfileData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ProfileData to delete</param>
        void DeleteProfileData(OpProfileData toBeDeleted);

        /// <summary>
        /// Clears the ProfileData object cache
        /// </summary>
        void ClearProfileDataCache();
        #endregion

        #region Protocol

        /// <summary>
        /// Indicates whether the Protocol is cached.
        /// </summary>
        bool ProtocolCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Protocol objects
        /// </summary>
        List<OpProtocol> GetAllProtocol(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Protocol by id
        /// </summary>
        /// <param name="Id">Protocol Id</param>
        /// <returns>Protocol object</returns>
        OpProtocol GetProtocol(int Id);

        /// <summary>
        /// Gets Protocol by id
        /// </summary>
        /// <param name="Ids">Protocol Ids</param>
        /// <returns>Protocol list</returns>
        List<OpProtocol> GetProtocol(int[] Ids);

        /// <summary>
        /// Gets Protocol by id
        /// </summary>
        /// <param name="Id">Protocol Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Protocol object</returns>
        OpProtocol GetProtocol(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Protocol by id
        /// </summary>
        /// <param name="Ids">Protocol Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Protocol list</returns>
        List<OpProtocol> GetProtocol(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the Protocol object from the database
        /// </summary>
        /// <param name="toBeDeleted">Protocol to delete</param>
        void DeleteProtocol(OpProtocol toBeDeleted);

        /// <summary>
        /// Clears the Protocol object cache
        /// </summary>
        void ClearProtocolCache();
        #endregion

        #region ProtocolDriver

        /// <summary>
        /// Indicates whether the ProtocolDriver is cached.
        /// </summary>
        bool ProtocolDriverCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ProtocolDriver objects
        /// </summary>
        List<OpProtocolDriver> GetAllProtocolDriver(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ProtocolDriver by id
        /// </summary>
        /// <param name="Id">ProtocolDriver Id</param>
        /// <returns>ProtocolDriver object</returns>
        OpProtocolDriver GetProtocolDriver(int Id);

        /// <summary>
        /// Gets ProtocolDriver by id
        /// </summary>
        /// <param name="Ids">ProtocolDriver Ids</param>
        /// <returns>ProtocolDriver list</returns>
        List<OpProtocolDriver> GetProtocolDriver(int[] Ids);

        /// <summary>
        /// Gets ProtocolDriver by id
        /// </summary>
        /// <param name="Id">ProtocolDriver Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProtocolDriver object</returns>
        OpProtocolDriver GetProtocolDriver(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ProtocolDriver by id
        /// </summary>
        /// <param name="Ids">ProtocolDriver Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProtocolDriver list</returns>
        List<OpProtocolDriver> GetProtocolDriver(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Deletes the ProtocolDriver object from the database
        /// </summary>
        /// <param name="toBeDeleted">ProtocolDriver to delete</param>
        void DeleteProtocolDriver(OpProtocolDriver toBeDeleted);

        /// <summary>
        /// Clears the ProtocolDriver object cache
        /// </summary>
        void ClearProtocolDriverCache();
        #endregion

        #region ReferenceType

        /// <summary>
        /// Indicates whether the ReferenceType is cached.
        /// </summary>
        bool ReferenceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReferenceType objects
        /// </summary>
        List<OpReferenceType> GetAllReferenceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ReferenceType by id
        /// </summary>
        /// <param name="Id">ReferenceType Id</param>
        /// <returns>ReferenceType object</returns>
        OpReferenceType GetReferenceType(int Id);

        /// <summary>
        /// Gets ReferenceType by id
        /// </summary>
        /// <param name="Ids">ReferenceType Ids</param>
        /// <returns>ReferenceType list</returns>
        List<OpReferenceType> GetReferenceType(int[] Ids);

        /// <summary>
        /// Gets ReferenceType by id
        /// </summary>
        /// <param name="Id">ReferenceType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReferenceType object</returns>
        OpReferenceType GetReferenceType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ReferenceType by id
        /// </summary>
        /// <param name="Ids">ReferenceType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReferenceType list</returns>
        List<OpReferenceType> GetReferenceType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ReferenceType object to the database
        /// </summary>
        /// <param name="toBeSaved">ReferenceType to save</param>
        /// <returns>ReferenceType Id</returns>
        int SaveReferenceType(OpReferenceType toBeSaved);

        /// <summary>
        /// Deletes the ReferenceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReferenceType to delete</param>
        void DeleteReferenceType(OpReferenceType toBeDeleted);

        /// <summary>
        /// Clears the ReferenceType object cache
        /// </summary>
        void ClearReferenceTypeCache();
        #endregion

        #region Role

        /// <summary>
        /// Indicates whether the Role is cached.
        /// </summary>
        bool RoleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Role objects
        /// </summary>
        List<OpRole> GetAllRole();


        /// <summary>
        /// Gets Role by id
        /// </summary>
        /// <param name="Id">Role Id</param>
        /// <returns>Role object</returns>
        OpRole GetRole(int Id);

        /// <summary>
        /// Gets Role by id
        /// </summary>
        /// <param name="Ids">Role Ids</param>
        /// <returns>Role list</returns>
        List<OpRole> GetRole(int[] Ids);

        /// <summary>
        /// Gets Role by id
        /// </summary>
        /// <param name="Id">Role Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Role object</returns>
        OpRole GetRole(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Role by id
        /// </summary>
        /// <param name="Ids">Role Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Role list</returns>
        List<OpRole> GetRole(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Role object to the database
        /// </summary>
        /// <param name="toBeSaved">Role to save</param>
        /// <returns>Role Id</returns>
        int SaveRole(OpRole toBeSaved);

        /// <summary>
        /// Deletes the Role object from the database
        /// </summary>
        /// <param name="toBeDeleted">Role to delete</param>
        void DeleteRole(OpRole toBeDeleted);

        /// <summary>
        /// Clears the Role object cache
        /// </summary>
        void ClearRoleCache();
        #endregion

        #region RoleActivity

        /// <summary>
        /// Indicates whether the RoleActivity is cached.
        /// </summary>
        bool RoleActivityCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RoleActivity objects
        /// </summary>
        List<OpRoleActivity> GetAllRoleActivity();


        /// <summary>
        /// Gets RoleActivity by id
        /// </summary>
        /// <param name="Id">RoleActivity Id</param>
        /// <returns>RoleActivity object</returns>
        OpRoleActivity GetRoleActivity(long Id);

        /// <summary>
        /// Gets RoleActivity by id
        /// </summary>
        /// <param name="Ids">RoleActivity Ids</param>
        /// <returns>RoleActivity list</returns>
        List<OpRoleActivity> GetRoleActivity(long[] Ids);

        /// <summary>
        /// Gets RoleActivity by id
        /// </summary>
        /// <param name="Id">RoleActivity Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoleActivity object</returns>
        OpRoleActivity GetRoleActivity(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RoleActivity by id
        /// </summary>
        /// <param name="Ids">RoleActivity Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoleActivity list</returns>
        List<OpRoleActivity> GetRoleActivity(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RoleActivity object to the database
        /// </summary>
        /// <param name="toBeSaved">RoleActivity to save</param>
        /// <returns>RoleActivity Id</returns>
        long SaveRoleActivity(OpRoleActivity toBeSaved);

        /// <summary>
        /// Deletes the RoleActivity object from the database
        /// </summary>
        /// <param name="toBeDeleted">RoleActivity to delete</param>
        void DeleteRoleActivity(OpRoleActivity toBeDeleted);

        /// <summary>
        /// Clears the RoleActivity object cache
        /// </summary>
        void ClearRoleActivityCache();
        #endregion

        #region Route

        /// <summary>
        /// Indicates whether the Route is cached.
        /// </summary>
        bool RouteCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Route objects
        /// </summary>
        List<OpRoute> GetAllRoute(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Route by id
        /// </summary>
        /// <param name="Id">Route Id</param>
        /// <returns>Route object</returns>
        OpRoute GetRoute(long Id);

        /// <summary>
        /// Gets Route by id
        /// </summary>
        /// <param name="Ids">Route Ids</param>
        /// <returns>Route list</returns>
        List<OpRoute> GetRoute(long[] Ids);

        /// <summary>
        /// Gets Route by id
        /// </summary>
        /// <param name="Id">Route Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Route object</returns>
        OpRoute GetRoute(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Route by id
        /// </summary>
        /// <param name="Ids">Route Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Route list</returns>
        List<OpRoute> GetRoute(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Route object to the database
        /// </summary>
        /// <param name="toBeSaved">Route to save</param>
        /// <returns>Route Id</returns>
        long SaveRoute(OpRoute toBeSaved);

        /// <summary>
        /// Deletes the Route object from the database
        /// </summary>
        /// <param name="toBeDeleted">Route to delete</param>
        void DeleteRoute(OpRoute toBeDeleted);

        /// <summary>
        /// Clears the Route object cache
        /// </summary>
        void ClearRouteCache();
        #endregion

        #region RouteData

        /// <summary>
        /// Indicates whether the RouteData is cached.
        /// </summary>
        bool RouteDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteData objects
        /// </summary>
        List<OpRouteData> GetAllRouteData();


        /// <summary>
        /// Gets RouteData by id
        /// </summary>
        /// <param name="Id">RouteData Id</param>
        /// <returns>RouteData object</returns>
        OpRouteData GetRouteData(long Id);

        /// <summary>
        /// Gets RouteData by id
        /// </summary>
        /// <param name="Ids">RouteData Ids</param>
        /// <returns>RouteData list</returns>
        List<OpRouteData> GetRouteData(long[] Ids);

        /// <summary>
        /// Gets RouteData by id
        /// </summary>
        /// <param name="Id">RouteData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteData object</returns>
        OpRouteData GetRouteData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteData by id
        /// </summary>
        /// <param name="Ids">RouteData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteData list</returns>
        List<OpRouteData> GetRouteData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteData object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteData to save</param>
        /// <returns>RouteData Id</returns>
        long SaveRouteData(OpRouteData toBeSaved);

        /// <summary>
        /// Deletes the RouteData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteData to delete</param>
        void DeleteRouteData(OpRouteData toBeDeleted);

        /// <summary>
        /// Clears the RouteData object cache
        /// </summary>
        void ClearRouteDataCache();
        #endregion

        #region RouteDef

        /// <summary>
        /// Indicates whether the RouteDef is cached.
        /// </summary>
        bool RouteDefCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteDef objects
        /// </summary>
        List<OpRouteDef> GetAllRouteDef();


        /// <summary>
        /// Gets RouteDef by id
        /// </summary>
        /// <param name="Id">RouteDef Id</param>
        /// <returns>RouteDef object</returns>
        OpRouteDef GetRouteDef(int Id);

        /// <summary>
        /// Gets RouteDef by id
        /// </summary>
        /// <param name="Ids">RouteDef Ids</param>
        /// <returns>RouteDef list</returns>
        List<OpRouteDef> GetRouteDef(int[] Ids);

        /// <summary>
        /// Gets RouteDef by id
        /// </summary>
        /// <param name="Id">RouteDef Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDef object</returns>
        OpRouteDef GetRouteDef(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteDef by id
        /// </summary>
        /// <param name="Ids">RouteDef Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDef list</returns>
        List<OpRouteDef> GetRouteDef(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteDef object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDef to save</param>
        /// <returns>RouteDef Id</returns>
        int SaveRouteDef(OpRouteDef toBeSaved);

        /// <summary>
        /// Deletes the RouteDef object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDef to delete</param>
        void DeleteRouteDef(OpRouteDef toBeDeleted);

        /// <summary>
        /// Clears the RouteDef object cache
        /// </summary>
        void ClearRouteDefCache();
        #endregion

        #region RouteDefData

        /// <summary>
        /// Indicates whether the RouteDefData is cached.
        /// </summary>
        bool RouteDefDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteDefData objects
        /// </summary>
        List<OpRouteDefData> GetAllRouteDefData();


        /// <summary>
        /// Gets RouteDefData by id
        /// </summary>
        /// <param name="Id">RouteDefData Id</param>
        /// <returns>RouteDefData object</returns>
        OpRouteDefData GetRouteDefData(int Id);

        /// <summary>
        /// Gets RouteDefData by id
        /// </summary>
        /// <param name="Ids">RouteDefData Ids</param>
        /// <returns>RouteDefData list</returns>
        List<OpRouteDefData> GetRouteDefData(int[] Ids);

        /// <summary>
        /// Gets RouteDefData by id
        /// </summary>
        /// <param name="Id">RouteDefData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDefData object</returns>
        OpRouteDefData GetRouteDefData(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteDefData by id
        /// </summary>
        /// <param name="Ids">RouteDefData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDefData list</returns>
        List<OpRouteDefData> GetRouteDefData(int[] Ids, bool queryDatabase);

        ///// <summary>
        ///// Saves the RouteDefData object to the database
        ///// </summary>
        ///// <param name="toBeSaved">RouteDefData to save</param>
        ///// <returns>RouteDefData Id</returns>
        //int SaveRouteDefData(OpRouteDefData toBeSaved)
        //{
        //    try
        //    {
        //        if (toBeSaved.DataType == null)
        //            toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
        //        int ret = dbConnectionSaveRouteDefData(toBeSaved, toBeSaved.DataType);
        //        if (RouteDefDataCacheEnabled)
        //        {
        //            RouteDefDataDict[toBeSaved.IdRouteDef] = toBeSaved; //add or update element
        //        }
        //        return ret;
        //    }
        //    catch (Exception ex)
        //    {
        //        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveRouteDefData", ex);
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// Deletes the RouteDefData object from the database
        ///// </summary>
        ///// <param name="toBeDeleted">RouteDefData to delete</param>
        //void DeleteRouteDefData(OpRouteDefData toBeDeleted)
        //{
        //    try
        //    {
        //        dbConnectionDeleteRouteDefData(toBeDeleted);
        //        if (RouteDefDataCacheEnabled)
        //        {
        //            RouteDefDataDict.Remove(toBeDeleted.IdRouteDef);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelRouteDefData", ex);
        //        throw;
        //    }
        //}

        /// <summary>
        /// Clears the RouteDefData object cache
        /// </summary>
        void ClearRouteDefDataCache();
        #endregion

        #region RouteDefHistory

        /// <summary>
        /// Indicates whether the RouteDefHistory is cached.
        /// </summary>
        bool RouteDefHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteDefHistory objects
        /// </summary>
        List<OpRouteDefHistory> GetAllRouteDefHistory();


        /// <summary>
        /// Gets RouteDefHistory by id
        /// </summary>
        /// <param name="Id">RouteDefHistory Id</param>
        /// <returns>RouteDefHistory object</returns>
        OpRouteDefHistory GetRouteDefHistory(long Id);

        /// <summary>
        /// Gets RouteDefHistory by id
        /// </summary>
        /// <param name="Ids">RouteDefHistory Ids</param>
        /// <returns>RouteDefHistory list</returns>
        List<OpRouteDefHistory> GetRouteDefHistory(long[] Ids);

        /// <summary>
        /// Gets RouteDefHistory by id
        /// </summary>
        /// <param name="Id">RouteDefHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDefHistory object</returns>
        OpRouteDefHistory GetRouteDefHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteDefHistory by id
        /// </summary>
        /// <param name="Ids">RouteDefHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDefHistory list</returns>
        List<OpRouteDefHistory> GetRouteDefHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteDefHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDefHistory to save</param>
        /// <returns>RouteDefHistory Id</returns>
        long SaveRouteDefHistory(OpRouteDefHistory toBeSaved);

        /// <summary>
        /// Deletes the RouteDefHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDefHistory to delete</param>
        void DeleteRouteDefHistory(OpRouteDefHistory toBeDeleted);

        /// <summary>
        /// Clears the RouteDefHistory object cache
        /// </summary>
        void ClearRouteDefHistoryCache();
        #endregion

        #region RouteDefLocation

        /// <summary>
        /// Indicates whether the RouteDefLocation is cached.
        /// </summary>
        bool RouteDefLocationCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteDefLocation objects
        /// </summary>
        List<OpRouteDefLocation> GetAllRouteDefLocation();


        /// <summary>
        /// Gets RouteDefLocation by id
        /// </summary>
        /// <param name="Id">RouteDefLocation Id</param>
        /// <returns>RouteDefLocation object</returns>
        OpRouteDefLocation GetRouteDefLocation(long Id);

        /// <summary>
        /// Gets RouteDefLocation by id
        /// </summary>
        /// <param name="Ids">RouteDefLocation Ids</param>
        /// <returns>RouteDefLocation list</returns>
        List<OpRouteDefLocation> GetRouteDefLocation(long[] Ids);

        /// <summary>
        /// Gets RouteDefLocation by id
        /// </summary>
        /// <param name="Id">RouteDefLocation Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDefLocation object</returns>
        OpRouteDefLocation GetRouteDefLocation(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteDefLocation by id
        /// </summary>
        /// <param name="Ids">RouteDefLocation Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDefLocation list</returns>
        List<OpRouteDefLocation> GetRouteDefLocation(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteDefLocation object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDefLocation to save</param>
        /// <returns>RouteDefLocation Id</returns>
        long SaveRouteDefLocation(OpRouteDefLocation toBeSaved);

        /// <summary>
        /// Deletes the RouteDefLocation object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDefLocation to delete</param>
        void DeleteRouteDefLocation(OpRouteDefLocation toBeDeleted);

        /// <summary>
        /// Clears the RouteDefLocation object cache
        /// </summary>
        void ClearRouteDefLocationCache();
        #endregion

        #region RouteDevice

        /// <summary>
        /// Indicates whether the RouteDevice is cached.
        /// </summary>
        bool RouteDeviceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteDevice objects
        /// </summary>
        List<OpRouteDevice> GetAllRouteDevice();


        /// <summary>
        /// Gets RouteDevice by id
        /// </summary>
        /// <param name="Id">RouteDevice Id</param>
        /// <returns>RouteDevice object</returns>
        OpRouteDevice GetRouteDevice(long Id);

        /// <summary>
        /// Gets RouteDevice by id
        /// </summary>
        /// <param name="Ids">RouteDevice Ids</param>
        /// <returns>RouteDevice list</returns>
        List<OpRouteDevice> GetRouteDevice(long[] Ids);

        /// <summary>
        /// Gets RouteDevice by id
        /// </summary>
        /// <param name="Id">RouteDevice Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDevice object</returns>
        OpRouteDevice GetRouteDevice(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteDevice by id
        /// </summary>
        /// <param name="Ids">RouteDevice Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteDevice list</returns>
        List<OpRouteDevice> GetRouteDevice(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteDevice object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDevice to save</param>
        /// <returns>RouteDevice Id</returns>
        long SaveRouteDevice(OpRouteDevice toBeSaved);

        /// <summary>
        /// Deletes the RouteDevice object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDevice to delete</param>
        void DeleteRouteDevice(OpRouteDevice toBeDeleted);

        /// <summary>
        /// Clears the RouteDevice object cache
        /// </summary>
        void ClearRouteDeviceCache();
        #endregion

        #region RouteElementStatus

        /// <summary>
        /// Indicates whether the RouteElementStatus is cached.
        /// </summary>
        bool RouteElementStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteElementStatus objects
        /// </summary>
        List<OpRouteElementStatus> GetAllRouteElementStatus();


        /// <summary>
        /// Gets RouteElementStatus by id
        /// </summary>
        /// <param name="Id">RouteElementStatus Id</param>
        /// <returns>RouteElementStatus object</returns>
        OpRouteElementStatus GetRouteElementStatus(int Id);

        /// <summary>
        /// Gets RouteElementStatus by id
        /// </summary>
        /// <param name="Ids">RouteElementStatus Ids</param>
        /// <returns>RouteElementStatus list</returns>
        List<OpRouteElementStatus> GetRouteElementStatus(int[] Ids);

        /// <summary>
        /// Gets RouteElementStatus by id
        /// </summary>
        /// <param name="Id">RouteElementStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteElementStatus object</returns>
        OpRouteElementStatus GetRouteElementStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteElementStatus by id
        /// </summary>
        /// <param name="Ids">RouteElementStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteElementStatus list</returns>
        List<OpRouteElementStatus> GetRouteElementStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteElementStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteElementStatus to save</param>
        /// <returns>RouteElementStatus Id</returns>
        int SaveRouteElementStatus(OpRouteElementStatus toBeSaved);

        /// <summary>
        /// Deletes the RouteElementStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteElementStatus to delete</param>
        void DeleteRouteElementStatus(OpRouteElementStatus toBeDeleted);

        /// <summary>
        /// Clears the RouteElementStatus object cache
        /// </summary>
        void ClearRouteElementStatusCache();
        #endregion

        #region RoutePoint

        /// <summary>
        /// Indicates whether the RoutePoint is cached.
        /// </summary>
        bool RoutePointCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RoutePoint objects
        /// </summary>
        List<OpRoutePoint> GetAllRoutePoint();


        /// <summary>
        /// Gets RoutePoint by id
        /// </summary>
        /// <param name="Id">RoutePoint Id</param>
        /// <returns>RoutePoint object</returns>
        OpRoutePoint GetRoutePoint(int Id);

        /// <summary>
        /// Gets RoutePoint by id
        /// </summary>
        /// <param name="Ids">RoutePoint Ids</param>
        /// <returns>RoutePoint list</returns>
        List<OpRoutePoint> GetRoutePoint(int[] Ids);

        /// <summary>
        /// Gets RoutePoint by id
        /// </summary>
        /// <param name="Id">RoutePoint Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoutePoint object</returns>
        OpRoutePoint GetRoutePoint(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RoutePoint by id
        /// </summary>
        /// <param name="Ids">RoutePoint Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoutePoint list</returns>
        List<OpRoutePoint> GetRoutePoint(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RoutePoint object to the database
        /// </summary>
        /// <param name="toBeSaved">RoutePoint to save</param>
        /// <returns>RoutePoint Id</returns>
        int SaveRoutePoint(OpRoutePoint toBeSaved);

        /// <summary>
        /// Deletes the RoutePoint object from the database
        /// </summary>
        /// <param name="toBeDeleted">RoutePoint to delete</param>
        void DeleteRoutePoint(OpRoutePoint toBeDeleted);

        /// <summary>
        /// Clears the RoutePoint object cache
        /// </summary>
        void ClearRoutePointCache();
        #endregion

        #region RoutePointData

        /// <summary>
        /// Indicates whether the RoutePointData is cached.
        /// </summary>
        bool RoutePointDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RoutePointData objects
        /// </summary>
        List<OpRoutePointData> GetAllRoutePointData();


        /// <summary>
        /// Gets RoutePointData by id
        /// </summary>
        /// <param name="Id">RoutePointData Id</param>
        /// <returns>RoutePointData object</returns>
        OpRoutePointData GetRoutePointData(long Id);

        /// <summary>
        /// Gets RoutePointData by id
        /// </summary>
        /// <param name="Ids">RoutePointData Ids</param>
        /// <returns>RoutePointData list</returns>
        List<OpRoutePointData> GetRoutePointData(long[] Ids);

        /// <summary>
        /// Gets RoutePointData by id
        /// </summary>
        /// <param name="Id">RoutePointData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoutePointData object</returns>
        OpRoutePointData GetRoutePointData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RoutePointData by id
        /// </summary>
        /// <param name="Ids">RoutePointData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoutePointData list</returns>
        List<OpRoutePointData> GetRoutePointData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RoutePointData object to the database
        /// </summary>
        /// <param name="toBeSaved">RoutePointData to save</param>
        /// <returns>RoutePointData Id</returns>
        long SaveRoutePointData(OpRoutePointData toBeSaved);

        /// <summary>
        /// Deletes the RoutePointData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RoutePointData to delete</param>
        void DeleteRoutePointData(OpRoutePointData toBeDeleted);

        /// <summary>
        /// Clears the RoutePointData object cache
        /// </summary>
        void ClearRoutePointDataCache();
        #endregion

        #region RouteStatus

        /// <summary>
        /// Indicates whether the RouteStatus is cached.
        /// </summary>
        bool RouteStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteStatus objects
        /// </summary>
        List<OpRouteStatus> GetAllRouteStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets RouteStatus by id
        /// </summary>
        /// <param name="Id">RouteStatus Id</param>
        /// <returns>RouteStatus object</returns>
        OpRouteStatus GetRouteStatus(int Id);

        /// <summary>
        /// Gets RouteStatus by id
        /// </summary>
        /// <param name="Ids">RouteStatus Ids</param>
        /// <returns>RouteStatus list</returns>
        List<OpRouteStatus> GetRouteStatus(int[] Ids);

        /// <summary>
        /// Gets RouteStatus by id
        /// </summary>
        /// <param name="Id">RouteStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteStatus object</returns>
        OpRouteStatus GetRouteStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteStatus by id
        /// </summary>
        /// <param name="Ids">RouteStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteStatus list</returns>
        List<OpRouteStatus> GetRouteStatus(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the RouteStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteStatus to save</param>
        /// <returns>RouteStatus Id</returns>
        int SaveRouteStatus(OpRouteStatus toBeSaved);

        /// <summary>
        /// Deletes the RouteStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteStatus to delete</param>
        void DeleteRouteStatus(OpRouteStatus toBeDeleted);

        /// <summary>
        /// Clears the RouteStatus object cache
        /// </summary>
        void ClearRouteStatusCache();
        #endregion

        #region RouteType

        /// <summary>
        /// Indicates whether the RouteType is cached.
        /// </summary>
        bool RouteTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteType objects
        /// </summary>
        List<OpRouteType> GetAllRouteType();


        /// <summary>
        /// Gets RouteType by id
        /// </summary>
        /// <param name="Id">RouteType Id</param>
        /// <returns>RouteType object</returns>
        OpRouteType GetRouteType(int Id);

        /// <summary>
        /// Gets RouteType by id
        /// </summary>
        /// <param name="Ids">RouteType Ids</param>
        /// <returns>RouteType list</returns>
        List<OpRouteType> GetRouteType(int[] Ids);

        /// <summary>
        /// Gets RouteType by id
        /// </summary>
        /// <param name="Id">RouteType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteType object</returns>
        OpRouteType GetRouteType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteType by id
        /// </summary>
        /// <param name="Ids">RouteType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteType list</returns>
        List<OpRouteType> GetRouteType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteType object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteType to save</param>
        /// <returns>RouteType Id</returns>
        int SaveRouteType(OpRouteType toBeSaved);

        /// <summary>
        /// Deletes the RouteType object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteType to delete</param>
        void DeleteRouteType(OpRouteType toBeDeleted);

        /// <summary>
        /// Clears the RouteType object cache
        /// </summary>
        void ClearRouteTypeCache();
        #endregion

        #region RouteTypeActionType

        /// <summary>
        /// Indicates whether the RouteTypeActionType is cached.
        /// </summary>
        bool RouteTypeActionTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteTypeActionType objects
        /// </summary>
        List<OpRouteTypeActionType> GetAllRouteTypeActionType();


        /// <summary>
        /// Gets RouteTypeActionType by id
        /// </summary>
        /// <param name="Id">RouteTypeActionType Id</param>
        /// <returns>RouteTypeActionType object</returns>
        OpRouteTypeActionType GetRouteTypeActionType(int Id);

        /// <summary>
        /// Gets RouteTypeActionType by id
        /// </summary>
        /// <param name="Ids">RouteTypeActionType Ids</param>
        /// <returns>RouteTypeActionType list</returns>
        List<OpRouteTypeActionType> GetRouteTypeActionType(int[] Ids);

        /// <summary>
        /// Gets RouteTypeActionType by id
        /// </summary>
        /// <param name="Id">RouteTypeActionType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTypeActionType object</returns>
        OpRouteTypeActionType GetRouteTypeActionType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteTypeActionType by id
        /// </summary>
        /// <param name="Ids">RouteTypeActionType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTypeActionType list</returns>
        List<OpRouteTypeActionType> GetRouteTypeActionType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteTypeActionType object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteTypeActionType to save</param>
        /// <returns>RouteTypeActionType Id</returns>
        int SaveRouteTypeActionType(OpRouteTypeActionType toBeSaved);

        /// <summary>
        /// Deletes the RouteTypeActionType object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteTypeActionType to delete</param>
        void DeleteRouteTypeActionType(OpRouteTypeActionType toBeDeleted);

        /// <summary>
        /// Clears the RouteTypeActionType object cache
        /// </summary>
        void ClearRouteTypeActionTypeCache();
        #endregion

        #region Schedule

        /// <summary>
        /// Indicates whether the Schedule is cached.
        /// </summary>
        bool ScheduleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Schedule objects
        /// </summary>
        List<OpSchedule> GetAllSchedule();


        /// <summary>
        /// Gets Schedule by id
        /// </summary>
        /// <param name="Id">Schedule Id</param>
        /// <returns>Schedule object</returns>
        OpSchedule GetSchedule(int Id);

        /// <summary>
        /// Gets Schedule by id
        /// </summary>
        /// <param name="Ids">Schedule Ids</param>
        /// <returns>Schedule list</returns>
        List<OpSchedule> GetSchedule(int[] Ids);

        /// <summary>
        /// Gets Schedule by id
        /// </summary>
        /// <param name="Id">Schedule Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Schedule object</returns>
        OpSchedule GetSchedule(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Schedule by id
        /// </summary>
        /// <param name="Ids">Schedule Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Schedule list</returns>
        List<OpSchedule> GetSchedule(int[] Ids, bool queryDatabase);


        /// <summary>
        /// Clears the Schedule object cache
        /// </summary>
        void ClearScheduleCache();
        #endregion

        #region ScheduleType

        /// <summary>
        /// Indicates whether the ScheduleType is cached.
        /// </summary>
        bool ScheduleTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ScheduleType objects
        /// </summary>
        List<OpScheduleType> GetAllScheduleType();


        /// <summary>
        /// Gets ScheduleType by id
        /// </summary>
        /// <param name="Id">ScheduleType Id</param>
        /// <returns>ScheduleType object</returns>
        OpScheduleType GetScheduleType(int Id);

        /// <summary>
        /// Gets ScheduleType by id
        /// </summary>
        /// <param name="Ids">ScheduleType Ids</param>
        /// <returns>ScheduleType list</returns>
        List<OpScheduleType> GetScheduleType(int[] Ids);

        /// <summary>
        /// Gets ScheduleType by id
        /// </summary>
        /// <param name="Id">ScheduleType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ScheduleType object</returns>
        OpScheduleType GetScheduleType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ScheduleType by id
        /// </summary>
        /// <param name="Ids">ScheduleType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ScheduleType list</returns>
        List<OpScheduleType> GetScheduleType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the ScheduleType object cache
        /// </summary>
        void ClearScheduleTypeCache();
        #endregion

        #region Seal

        /// <summary>
        /// Indicates whether the Seal is cached.
        /// </summary>
        bool SealCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Seal objects
        /// </summary>
        List<OpSeal> GetAllSeal();


        /// <summary>
        /// Gets Seal by id
        /// </summary>
        /// <param name="Id">Seal Id</param>
        /// <returns>Seal object</returns>
        OpSeal GetSeal(long Id);

        /// <summary>
        /// Gets Seal by id
        /// </summary>
        /// <param name="Ids">Seal Ids</param>
        /// <returns>Seal list</returns>
        List<OpSeal> GetSeal(long[] Ids);

        /// <summary>
        /// Gets Seal by id
        /// </summary>
        /// <param name="Id">Seal Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Seal object</returns>
        OpSeal GetSeal(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Seal by id
        /// </summary>
        /// <param name="Ids">Seal Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Seal list</returns>
        List<OpSeal> GetSeal(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Seal object to the database
        /// </summary>
        /// <param name="toBeSaved">Seal to save</param>
        /// <returns>Seal Id</returns>
        long SaveSeal(OpSeal toBeSaved);

        /// <summary>
        /// Deletes the Seal object from the database
        /// </summary>
        /// <param name="toBeDeleted">Seal to delete</param>
        void DeleteSeal(OpSeal toBeDeleted);

        /// <summary>
        /// Clears the Seal object cache
        /// </summary>
        void ClearSealCache();
        #endregion

        #region Service

        /// <summary>
        /// Indicates whether the Service is cached.
        /// </summary>
        bool ServiceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Service objects
        /// </summary>
        List<OpService> GetAllService(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Service by id
        /// </summary>
        /// <param name="Id">Service Id</param>
        /// <returns>Service object</returns>
        OpService GetService(int Id);

        /// <summary>
        /// Gets Service by id
        /// </summary>
        /// <param name="Ids">Service Ids</param>
        /// <returns>Service list</returns>
        List<OpService> GetService(int[] Ids);

        /// <summary>
        /// Gets Service by id
        /// </summary>
        /// <param name="Id">Service Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Service object</returns>
        OpService GetService(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Service by id
        /// </summary>
        /// <param name="Ids">Service Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Service list</returns>
        List<OpService> GetService(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Service object to the database
        /// </summary>
        /// <param name="toBeSaved">Service to save</param>
        /// <returns>Service Id</returns>
        int SaveService(OpService toBeSaved);

        /// <summary>
        /// Deletes the Service object from the database
        /// </summary>
        /// <param name="toBeDeleted">Service to delete</param>
        void DeleteService(OpService toBeDeleted);

        /// <summary>
        /// Clears the Service object cache
        /// </summary>
        void ClearServiceCache();
        #endregion

        #region ServiceActionResult

        /// <summary>
        /// Indicates whether the ServiceActionResult is cached.
        /// </summary>
        bool ServiceActionResultCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceActionResult objects
        /// </summary>
        List<OpServiceActionResult> GetAllServiceActionResult(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceActionResult by id
        /// </summary>
        /// <param name="Id">ServiceActionResult Id</param>
        /// <returns>ServiceActionResult object</returns>
        OpServiceActionResult GetServiceActionResult(int Id);

        /// <summary>
        /// Gets ServiceActionResult by id
        /// </summary>
        /// <param name="Ids">ServiceActionResult Ids</param>
        /// <returns>ServiceActionResult list</returns>
        List<OpServiceActionResult> GetServiceActionResult(int[] Ids);

        /// <summary>
        /// Gets ServiceActionResult by id
        /// </summary>
        /// <param name="Id">ServiceActionResult Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceActionResult object</returns>
        OpServiceActionResult GetServiceActionResult(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceActionResult by id
        /// </summary>
        /// <param name="Ids">ServiceActionResult Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceActionResult list</returns>
        List<OpServiceActionResult> GetServiceActionResult(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceActionResult object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceActionResult to save</param>
        /// <returns>ServiceActionResult Id</returns>
        int SaveServiceActionResult(OpServiceActionResult toBeSaved);

        /// <summary>
        /// Deletes the ServiceActionResult object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceActionResult to delete</param>
        void DeleteServiceActionResult(OpServiceActionResult toBeDeleted);

        /// <summary>
        /// Clears the ServiceActionResult object cache
        /// </summary>
        void ClearServiceActionResultCache();
        #endregion

        #region ServiceData

        /// <summary>
        /// Indicates whether the ServiceData is cached.
        /// </summary>
        bool ServiceDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceData objects
        /// </summary>
        List<OpServiceData> GetAllServiceData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceData by id
        /// </summary>
        /// <param name="Id">ServiceData Id</param>
        /// <returns>ServiceData object</returns>
        OpServiceData GetServiceData(long Id);

        /// <summary>
        /// Gets ServiceData by id
        /// </summary>
        /// <param name="Ids">ServiceData Ids</param>
        /// <returns>ServiceData list</returns>
        List<OpServiceData> GetServiceData(long[] Ids);

        /// <summary>
        /// Gets ServiceData by id
        /// </summary>
        /// <param name="Id">ServiceData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceData object</returns>
        OpServiceData GetServiceData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceData by id
        /// </summary>
        /// <param name="Ids">ServiceData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceData list</returns>
        List<OpServiceData> GetServiceData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceData object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceData to save</param>
        /// <returns>ServiceData Id</returns>
        long SaveServiceData(OpServiceData toBeSaved);

        /// <summary>
        /// Deletes the ServiceData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceData to delete</param>
        void DeleteServiceData(OpServiceData toBeDeleted);

        /// <summary>
        /// Clears the ServiceData object cache
        /// </summary>
        void ClearServiceDataCache();
        #endregion

        #region ServiceCustom

        /// <summary>
        /// Indicates whether the ServiceCustom is cached.
        /// </summary>
        bool ServiceCustomCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceCustom objects
        /// </summary>
        List<OpServiceCustom> GetAllServiceCustom(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceCustom by id
        /// </summary>
        /// <param name="Id">ServiceCustom Id</param>
        /// <returns>ServiceCustom object</returns>
        OpServiceCustom GetServiceCustom(long Id);

        /// <summary>
        /// Gets ServiceCustom by id
        /// </summary>
        /// <param name="Ids">ServiceCustom Ids</param>
        /// <returns>ServiceCustom list</returns>
        List<OpServiceCustom> GetServiceCustom(long[] Ids);

        /// <summary>
        /// Gets ServiceCustom by id
        /// </summary>
        /// <param name="Id">ServiceCustom Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceCustom object</returns>
        OpServiceCustom GetServiceCustom(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceCustom by id
        /// </summary>
        /// <param name="Ids">ServiceCustom Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceCustom list</returns>
        List<OpServiceCustom> GetServiceCustom(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceCustom object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceCustom to save</param>
        /// <returns>ServiceCustom Id</returns>
        long SaveServiceCustom(OpServiceCustom toBeSaved);

        /// <summary>
        /// Deletes the ServiceCustom object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceCustom to delete</param>
        void DeleteServiceCustom(OpServiceCustom toBeDeleted);

        /// <summary>
        /// Clears the ServiceCustom object cache
        /// </summary>
        void ClearServiceCustomCache();
        #endregion

        #region ServiceCustomData

        /// <summary>
        /// Indicates whether the ServiceCustomData is cached.
        /// </summary>
        bool ServiceCustomDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceCustomData objects
        /// </summary>
        List<OpServiceCustomData> GetAllServiceCustomData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceCustomData by id
        /// </summary>
        /// <param name="Id">ServiceCustomData Id</param>
        /// <returns>ServiceCustomData object</returns>
        OpServiceCustomData GetServiceCustomData(long Id);

        /// <summary>
        /// Gets ServiceCustomData by id
        /// </summary>
        /// <param name="Ids">ServiceCustomData Ids</param>
        /// <returns>ServiceCustomData list</returns>
        List<OpServiceCustomData> GetServiceCustomData(long[] Ids);

        /// <summary>
        /// Gets ServiceCustomData by id
        /// </summary>
        /// <param name="Id">ServiceCustomData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceCustomData object</returns>
        OpServiceCustomData GetServiceCustomData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceCustomData by id
        /// </summary>
        /// <param name="Ids">ServiceCustomData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceCustomData list</returns>
        List<OpServiceCustomData> GetServiceCustomData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceCustomData object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceCustomData to save</param>
        /// <returns>ServiceCustomData Id</returns>
        long SaveServiceCustomData(OpServiceCustomData toBeSaved);

        /// <summary>
        /// Deletes the ServiceCustomData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceCustomData to delete</param>
        void DeleteServiceCustomData(OpServiceCustomData toBeDeleted);

        /// <summary>
        /// Clears the ServiceCustomData object cache
        /// </summary>
        void ClearServiceCustomDataCache();
        #endregion

        #region ServiceInPackage

        /// <summary>
        /// Indicates whether the ServiceInPackage is cached.
        /// </summary>
        bool ServiceInPackageCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceInPackage objects
        /// </summary>
        List<OpServiceInPackage> GetAllServiceInPackage(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceInPackage by id
        /// </summary>
        /// <param name="Id">ServiceInPackage Id</param>
        /// <returns>ServiceInPackage object</returns>
        OpServiceInPackage GetServiceInPackage(long Id);

        /// <summary>
        /// Gets ServiceInPackage by id
        /// </summary>
        /// <param name="Ids">ServiceInPackage Ids</param>
        /// <returns>ServiceInPackage list</returns>
        List<OpServiceInPackage> GetServiceInPackage(long[] Ids);

        /// <summary>
        /// Gets ServiceInPackage by id
        /// </summary>
        /// <param name="Id">ServiceInPackage Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceInPackage object</returns>
        OpServiceInPackage GetServiceInPackage(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceInPackage by id
        /// </summary>
        /// <param name="Ids">ServiceInPackage Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceInPackage list</returns>
        List<OpServiceInPackage> GetServiceInPackage(long[] Ids, bool queryDatabase,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceInPackage object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceInPackage to save</param>
        /// <returns>ServiceInPackage Id</returns>
        long SaveServiceInPackage(OpServiceInPackage toBeSaved);

        /// <summary>
        /// Deletes the ServiceInPackage object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceInPackage to delete</param>
        void DeleteServiceInPackage(OpServiceInPackage toBeDeleted);

        /// <summary>
        /// Clears the ServiceInPackage object cache
        /// </summary>
        void ClearServiceInPackageCache();
        #endregion

        #region ServiceList

        /// <summary>
        /// Indicates whether the ServiceList is cached.
        /// </summary>
        bool ServiceListCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceList objects
        /// </summary>
        List<OpServiceList> GetAllServiceList(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceList by id
        /// </summary>
        /// <param name="Id">ServiceList Id</param>
        /// <returns>ServiceList object</returns>
        OpServiceList GetServiceList(int Id);

        /// <summary>
        /// Gets ServiceList by id
        /// </summary>
        /// <param name="Ids">ServiceList Ids</param>
        /// <returns>ServiceList list</returns>
        List<OpServiceList> GetServiceList(int[] Ids);

        /// <summary>
        /// Gets ServiceList by id
        /// </summary>
        /// <param name="Id">ServiceList Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceList object</returns>
        OpServiceList GetServiceList(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceList by id
        /// </summary>
        /// <param name="Ids">ServiceList Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceList list</returns>
        List<OpServiceList> GetServiceList(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceList object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceList to save</param>
        /// <returns>ServiceList Id</returns>
        int SaveServiceList(OpServiceList toBeSaved);

        /// <summary>
        /// Deletes the ServiceList object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceList to delete</param>
        void DeleteServiceList(OpServiceList toBeDeleted);

        /// <summary>
        /// Clears the ServiceList object cache
        /// </summary>
        void ClearServiceListCache();

        DataTable GetServicesForDevices(long[] SerialNbr, int IdServiceList);
        #endregion

        #region ServiceListData

        /// <summary>
        /// Indicates whether the ServiceListData is cached.
        /// </summary>
        bool ServiceListDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceListData objects
        /// </summary>
        List<OpServiceListData> GetAllServiceListData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceListData by id
        /// </summary>
        /// <param name="Id">ServiceListData Id</param>
        /// <returns>ServiceListData object</returns>
        OpServiceListData GetServiceListData(long Id);

        /// <summary>
        /// Gets ServiceListData by id
        /// </summary>
        /// <param name="Ids">ServiceListData Ids</param>
        /// <returns>ServiceListData list</returns>
        List<OpServiceListData> GetServiceListData(long[] Ids);

        /// <summary>
        /// Gets ServiceListData by id
        /// </summary>
        /// <param name="Id">ServiceListData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListData object</returns>
        OpServiceListData GetServiceListData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceListData by id
        /// </summary>
        /// <param name="Ids">ServiceListData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListData list</returns>
        List<OpServiceListData> GetServiceListData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceListData object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListData to save</param>
        /// <returns>ServiceListData Id</returns>
        long SaveServiceListData(OpServiceListData toBeSaved);

        /// <summary>
        /// Deletes the ServiceListData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListData to delete</param>
        void DeleteServiceListData(OpServiceListData toBeDeleted);

        /// <summary>
        /// Clears the ServiceListData object cache
        /// </summary>
        void ClearServiceListDataCache();
        #endregion

        #region ServiceListDevice

        /// <summary>
        /// Indicates whether the ServiceListDevice is cached.
        /// </summary>
        bool ServiceListDeviceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceListDevice objects
        /// </summary>
        List<OpServiceListDevice> GetAllServiceListDevice(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        List<OpServiceListDevice> GetAllServiceListDeviceCustom(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceListDevice by id
        /// </summary>
        /// <param name="Id">ServiceListDevice Id</param>
        /// <returns>ServiceListDevice object</returns>
        OpServiceListDevice GetServiceListDevice(Tuple<int, long> Id);

        /// <summary>
        /// Gets ServiceListDevice by id
        /// </summary>
        /// <param name="Ids">ServiceListDevice Ids</param>
        /// <returns>ServiceListDevice list</returns>
        List<OpServiceListDevice> GetServiceListDevice(Tuple<int, long>[] Ids);

        /// <summary>
        /// Gets ServiceListDevice by id
        /// </summary>
        /// <param name="Id">ServiceListDevice Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDevice object</returns>
        OpServiceListDevice GetServiceListDevice(Tuple<int, long> Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceListDevice by id
        /// </summary>
        /// <param name="Ids">ServiceListDevice Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDevice list</returns>
        List<OpServiceListDevice> GetServiceListDevice(Tuple<int, long>[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceListDevice object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDevice to save</param>
        /// <returns>ServiceListDevice Id</returns>
        int SaveServiceListDevice(OpServiceListDevice toBeSaved);

        /// <summary>
        /// Saves the ServiceListDevice object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDevice to save</param>
        /// <returns>ServiceListDevice Id</returns>
        void SaveServiceListDevice(int idServiceList, OpServiceListDevice toBeSaved);

        /// <summary>
        /// Deletes the ServiceListDevice object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListDevice to delete</param>
        void DeleteServiceListDevice(OpServiceListDevice toBeDeleted);

        void DeleteServiceListDevice(int idServiceList, long serialNbr);

        /// <summary>
        /// Clears the ServiceListDevice object cache
        /// </summary>
        void ClearServiceListDeviceCache();
        #endregion

        #region ServiceListDeviceData

        /// <summary>
        /// Indicates whether the ServiceListDeviceData is cached.
        /// </summary>
        bool ServiceListDeviceDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceListDeviceData objects
        /// </summary>
        List<OpServiceListDeviceData> GetAllServiceListDeviceData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceListDeviceData by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceData Id</param>
        /// <returns>ServiceListDeviceData object</returns>
        OpServiceListDeviceData GetServiceListDeviceData(long Id);

        /// <summary>
        /// Gets ServiceListDeviceData by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceData Ids</param>
        /// <returns>ServiceListDeviceData list</returns>
        List<OpServiceListDeviceData> GetServiceListDeviceData(long[] Ids);

        /// <summary>
        /// Gets ServiceListDeviceData by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceData object</returns>
        OpServiceListDeviceData GetServiceListDeviceData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceListDeviceData by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceData list</returns>
        List<OpServiceListDeviceData> GetServiceListDeviceData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceListDeviceData object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDeviceData to save</param>
        /// <returns>ServiceListDeviceData Id</returns>
        long SaveServiceListDeviceData(OpServiceListDeviceData toBeSaved);

        /// <summary>
        /// Deletes the ServiceListDeviceData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListDeviceData to delete</param>
        void DeleteServiceListDeviceData(OpServiceListDeviceData toBeDeleted);

        /// <summary>
        /// Clears the ServiceListDeviceData object cache
        /// </summary>
        void ClearServiceListDeviceDataCache();
        #endregion

        #region ServiceDiagnosticResult

        /// <summary>
        /// Indicates whether the ServiceDiagnosticResult is cached.
        /// </summary>
        bool ServiceDiagnosticResultCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceDiagnosticResult objects
        /// </summary>
        List<OpServiceDiagnosticResult> GetAllServiceDiagnosticResult(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceDiagnosticResult by id
        /// </summary>
        /// <param name="Id">ServiceDiagnosticResult Id</param>
        /// <returns>ServiceDiagnosticResult object</returns>
        OpServiceDiagnosticResult GetServiceDiagnosticResult(int Id);

        /// <summary>
        /// Gets ServiceDiagnosticResult by id
        /// </summary>
        /// <param name="Ids">ServiceDiagnosticResult Ids</param>
        /// <returns>ServiceDiagnosticResult list</returns>
        List<OpServiceDiagnosticResult> GetServiceDiagnosticResult(int[] Ids);

        /// <summary>
        /// Gets ServiceDiagnosticResult by id
        /// </summary>
        /// <param name="Id">ServiceDiagnosticResult Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceDiagnosticResult object</returns>
        OpServiceDiagnosticResult GetServiceDiagnosticResult(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceDiagnosticResult by id
        /// </summary>
        /// <param name="Ids">ServiceDiagnosticResult Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceDiagnosticResult list</returns>
        List<OpServiceDiagnosticResult> GetServiceDiagnosticResult(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceDiagnosticResult object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceDiagnosticResult to save</param>
        /// <returns>ServiceDiagnosticResult Id</returns>
        int SaveServiceDiagnosticResult(OpServiceDiagnosticResult toBeSaved);

        /// <summary>
        /// Deletes the ServiceDiagnosticResult object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceDiagnosticResult to delete</param>
        void DeleteServiceDiagnosticResult(OpServiceDiagnosticResult toBeDeleted);

        /// <summary>
        /// Clears the ServiceDiagnosticResult object cache
        /// </summary>
        void ClearServiceDiagnosticResultCache();
        #endregion

        #region ServiceListDeviceAction

        /// <summary>
        /// Indicates whether the ServiceListDeviceAction is cached.
        /// </summary>
        bool ServiceListDeviceActionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceListDeviceAction objects
        /// </summary>
        List<OpServiceListDeviceAction> GetAllServiceListDeviceAction(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceListDeviceAction by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceAction Id</param>
        /// <returns>ServiceListDeviceAction object</returns>
        OpServiceListDeviceAction GetServiceListDeviceAction(long Id);

        /// <summary>
        /// Gets ServiceListDeviceAction by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceAction Ids</param>
        /// <returns>ServiceListDeviceAction list</returns>
        List<OpServiceListDeviceAction> GetServiceListDeviceAction(long[] Ids);

        /// <summary>
        /// Gets ServiceListDeviceAction by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceAction Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceAction object</returns>
        OpServiceListDeviceAction GetServiceListDeviceAction(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceListDeviceAction by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceAction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceAction list</returns>
        List<OpServiceListDeviceAction> GetServiceListDeviceAction(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceListDeviceAction object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDeviceAction to save</param>
        /// <returns>ServiceListDeviceAction Id</returns>
        long SaveServiceListDeviceAction(OpServiceListDeviceAction toBeSaved);

        /// <summary>
        /// Saves the ServiceListDeviceAction object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDeviceAction to save</param>
        /// <returns>ServiceListDeviceAction Id</returns>
        void SaveServiceListDeviceAction(long serialNbr, int idServiceList, long idServciePackage, long? idServiceCustom);

        /// <summary>
        /// Deletes the ServiceListDeviceAction object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListDeviceAction to delete</param>
        void DeleteServiceListDeviceAction(OpServiceListDeviceAction toBeDeleted);

        /// <summary>
        /// Deletes the ServiceListDeviceAction object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListDeviceAction to delete</param>
        void DeleteServiceListDeviceAction(long serialNbr, int idServiceList, long idServicePackage, long? idServiceCustom);

        /// <summary>
        /// Clears the ServiceListDeviceAction object cache
        /// </summary>
        void ClearServiceListDeviceActionCache();
        #endregion

        #region ServiceListDeviceActionDetails

        /// <summary>
        /// Indicates whether the ServiceListDeviceActionDetails is cached.
        /// </summary>
        bool ServiceListDeviceActionDetailsCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceListDeviceActionDetails objects
        /// </summary>
        List<OpServiceListDeviceActionDetails> GetAllServiceListDeviceActionDetails(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceListDeviceActionDetails by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceActionDetails Id</param>
        /// <returns>ServiceListDeviceActionDetails object</returns>
        OpServiceListDeviceActionDetails GetServiceListDeviceActionDetails(long Id);

        /// <summary>
        /// Gets ServiceListDeviceActionDetails by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceActionDetails Ids</param>
        /// <returns>ServiceListDeviceActionDetails list</returns>
        List<OpServiceListDeviceActionDetails> GetServiceListDeviceActionDetails(long[] Ids);

        /// <summary>
        /// Gets ServiceListDeviceActionDetails by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceActionDetails Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceActionDetails object</returns>
        OpServiceListDeviceActionDetails GetServiceListDeviceActionDetails(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceListDeviceActionDetails by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceActionDetails Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceActionDetails list</returns>
        List<OpServiceListDeviceActionDetails> GetServiceListDeviceActionDetails(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceListDeviceActionDetails object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDeviceActionDetails to save</param>
        /// <returns>ServiceListDeviceActionDetails Id</returns>
        long SaveServiceListDeviceActionDetails(OpServiceListDeviceActionDetails toBeSaved);

        /// <summary>
        /// Deletes the ServiceListDeviceActionDetails object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListDeviceActionDetails to delete</param>
        void DeleteServiceListDeviceActionDetails(OpServiceListDeviceActionDetails toBeDeleted);

        /// <summary>
        /// Clears the ServiceListDeviceActionDetails object cache
        /// </summary>
        void ClearServiceListDeviceActionDetailsCache();
        #endregion

        #region ServiceListDeviceDiagnostic

        /// <summary>
        /// Indicates whether the ServiceListDeviceDiagnostic is cached.
        /// </summary>
        bool ServiceListDeviceDiagnosticCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceListDeviceDiagnostic objects
        /// </summary>
        List<OpServiceListDeviceDiagnostic> GetAllServiceListDeviceDiagnostic(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceListDeviceDiagnostic by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceDiagnostic Id</param>
        /// <returns>ServiceListDeviceDiagnostic object</returns>
        OpServiceListDeviceDiagnostic GetServiceListDeviceDiagnostic(long Id);

        /// <summary>
        /// Gets ServiceListDeviceDiagnostic by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceDiagnostic Ids</param>
        /// <returns>ServiceListDeviceDiagnostic list</returns>
        List<OpServiceListDeviceDiagnostic> GetServiceListDeviceDiagnostic(long[] Ids);

        /// <summary>
        /// Gets ServiceListDeviceDiagnostic by id
        /// </summary>
        /// <param name="Id">ServiceListDeviceDiagnostic Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceDiagnostic object</returns>
        OpServiceListDeviceDiagnostic GetServiceListDeviceDiagnostic(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceListDeviceDiagnostic by id
        /// </summary>
        /// <param name="Ids">ServiceListDeviceDiagnostic Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceListDeviceDiagnostic list</returns>
        List<OpServiceListDeviceDiagnostic> GetServiceListDeviceDiagnostic(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceListDeviceDiagnostic object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDeviceDiagnostic to save</param>
        /// <returns>ServiceListDeviceDiagnostic Id</returns>
        long SaveServiceListDeviceDiagnostic(OpServiceListDeviceDiagnostic toBeSaved);

        /// <summary>
        /// Saves the ServiceListDeviceDiagnostic object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceListDeviceDiagnostic to save</param>
        /// <returns>ServiceListDeviceDiagnostic Id</returns>
        void SaveServiceListDeviceDiagnostic(long serialNbr, int idServiceList, int idDiagnosticAction, string inputText, object damageLevel, int? IdDiagnosticActionResult);

        /// <summary>
        /// Deletes the ServiceListDeviceDiagnostic object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListDeviceDiagnostic to delete</param>
        void DeleteServiceListDeviceDiagnostic(OpServiceListDeviceDiagnostic toBeDeleted);

        /// <summary>
        /// Deletes the ServiceListDeviceDiagnostic object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceListDeviceDiagnostic to delete</param>
        void DeleteServiceListDeviceDiagnostic(long serialNbr, int idServiceList);

        /// <summary>
        /// Clears the ServiceListDeviceDiagnostic object cache
        /// </summary>
        void ClearServiceListDeviceDiagnosticCache();
        #endregion

        #region ServicePackage

        /// <summary>
        /// Indicates whether the ServicePackage is cached.
        /// </summary>
        bool ServicePackageCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServicePackage objects
        /// </summary>
        List<OpServicePackage> GetAllServicePackage(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServicePackage by id
        /// </summary>
        /// <param name="Id">ServicePackage Id</param>
        /// <returns>ServicePackage object</returns>
        OpServicePackage GetServicePackage(long Id);

        /// <summary>
        /// Gets ServicePackage by id
        /// </summary>
        /// <param name="Ids">ServicePackage Ids</param>
        /// <returns>ServicePackage list</returns>
        List<OpServicePackage> GetServicePackage(long[] Ids);

        /// <summary>
        /// Gets ServicePackage by id
        /// </summary>
        /// <param name="Id">ServicePackage Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServicePackage object</returns>
        OpServicePackage GetServicePackage(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServicePackage by id
        /// </summary>
        /// <param name="Ids">ServicePackage Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServicePackage list</returns>
        List<OpServicePackage> GetServicePackage(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServicePackage object to the database
        /// </summary>
        /// <param name="toBeSaved">ServicePackage to save</param>
        /// <returns>ServicePackage Id</returns>
        long SaveServicePackage(OpServicePackage toBeSaved);

        /// <summary>
        /// Deletes the ServicePackage object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServicePackage to delete</param>
        void DeleteServicePackage(OpServicePackage toBeDeleted);

        /// <summary>
        /// Clears the ServicePackage object cache
        /// </summary>
        void ClearServicePackageCache();
        #endregion

        #region ServicePackageData

        /// <summary>
        /// Indicates whether the ServicePackageData is cached.
        /// </summary>
        bool ServicePackageDataCacheEnabled { get; set; }


        /// <summary>
        /// Gets all ServicePackageData objects
        /// </summary>
        List<OpServicePackageData> GetAllServicePackageData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServicePackageData by id
        /// </summary>
        /// <param name="Id">ServicePackageData Id</param>
        /// <returns>ServicePackageData object</returns>
        OpServicePackageData GetServicePackageData(long Id);

        /// <summary>
        /// Gets ServicePackageData by id
        /// </summary>
        /// <param name="Ids">ServicePackageData Ids</param>
        /// <returns>ServicePackageData list</returns>
        List<OpServicePackageData> GetServicePackageData(long[] Ids);

        /// <summary>
        /// Gets ServicePackageData by id
        /// </summary>
        /// <param name="Id">ServicePackageData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServicePackageData object</returns>
        OpServicePackageData GetServicePackageData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServicePackageData by id
        /// </summary>
        /// <param name="Ids">ServicePackageData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServicePackageData list</returns>
        List<OpServicePackageData> GetServicePackageData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServicePackageData object to the database
        /// </summary>
        /// <param name="toBeSaved">ServicePackageData to save</param>
        /// <returns>ServicePackageData Id</returns>
        long SaveServicePackageData(OpServicePackageData toBeSaved);

        /// <summary>
        /// Deletes the ServicePackageData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServicePackageData to delete</param>
        void DeleteServicePackageData(OpServicePackageData toBeDeleted);

        /// <summary>
        /// Clears the ServicePackageData object cache
        /// </summary>
        void ClearServicePackageDataCache();
        #endregion

        #region ServiceReferenceType

        /// <summary>
        /// Indicates whether the ServiceReferenceType is cached.
        /// </summary>
        bool ServiceReferenceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceReferenceType objects
        /// </summary>
        List<OpServiceReferenceType> GetAllServiceReferenceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceReferenceType by id
        /// </summary>
        /// <param name="Id">ServiceReferenceType Id</param>
        /// <returns>ServiceReferenceType object</returns>
        OpServiceReferenceType GetServiceReferenceType(int Id);

        /// <summary>
        /// Gets ServiceReferenceType by id
        /// </summary>
        /// <param name="Ids">ServiceReferenceType Ids</param>
        /// <returns>ServiceReferenceType list</returns>
        List<OpServiceReferenceType> GetServiceReferenceType(int[] Ids);

        /// <summary>
        /// Gets ServiceReferenceType by id
        /// </summary>
        /// <param name="Id">ServiceReferenceType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceReferenceType object</returns>
        OpServiceReferenceType GetServiceReferenceType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceReferenceType by id
        /// </summary>
        /// <param name="Ids">ServiceReferenceType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceReferenceType list</returns>
        List<OpServiceReferenceType> GetServiceReferenceType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceReferenceType object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceReferenceType to save</param>
        /// <returns>ServiceReferenceType Id</returns>
        int SaveServiceReferenceType(OpServiceReferenceType toBeSaved);

        /// <summary>
        /// Deletes the ServiceReferenceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceReferenceType to delete</param>
        void DeleteServiceReferenceType(OpServiceReferenceType toBeDeleted);

        /// <summary>
        /// Clears the ServiceReferenceType object cache
        /// </summary>
        void ClearServiceReferenceTypeCache();
        #endregion

        #region ServiceSummary

        /// <summary>
        /// Indicates whether the ServiceSummary is cached.
        /// </summary>
        bool ServiceSummaryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceSummary objects
        /// </summary>
        List<OpServiceSummary> GetAllServiceSummary(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceSummary by id
        /// </summary>
        /// <param name="Id">ServiceSummary Id</param>
        /// <returns>ServiceSummary object</returns>
        OpServiceSummary GetServiceSummary(int Id);

        /// <summary>
        /// Gets ServiceSummary by id
        /// </summary>
        /// <param name="Ids">ServiceSummary Ids</param>
        /// <returns>ServiceSummary list</returns>
        List<OpServiceSummary> GetServiceSummary(int[] Ids);

        /// <summary>
        /// Gets ServiceSummary by id
        /// </summary>
        /// <param name="Id">ServiceSummary Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceSummary object</returns>
        OpServiceSummary GetServiceSummary(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceSummary by id
        /// </summary>
        /// <param name="Ids">ServiceSummary Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceSummary list</returns>
        List<OpServiceSummary> GetServiceSummary(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceSummary object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceSummary to save</param>
        /// <returns>ServiceSummary Id</returns>
        int SaveServiceSummary(OpServiceSummary toBeSaved);

        /// <summary>
        /// Saves the ServiceSummary object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceSummary to save</param>
        /// <returns>ServiceSummary Id</returns>
        void SaveServiceSummary(int idServiceList, long idServicePackage, int amount, double cost);

        /// <summary>
        /// Deletes the ServiceSummary object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceSummary to delete</param>
        void DeleteServiceSummary(OpServiceSummary toBeDeleted);

        /// <summary>
        /// Deletes the ServiceSummary object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceSummary to delete</param>
        void DeleteServiceSummary(int idServiceList, long idServiCePackage, double? cost);

        /// <summary>
        /// Clears the ServiceSummary object cache
        /// </summary>
        void ClearServiceSummaryCache();
        #endregion

        #region ServiceSummaryData

        /// <summary>
        /// Indicates whether the ServiceSummaryData is cached.
        /// </summary>
        bool ServiceSummaryDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ServiceSummaryData objects
        /// </summary>
        List<OpServiceSummaryData> GetAllServiceSummaryData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ServiceSummaryData by id
        /// </summary>
        /// <param name="Id">ServiceSummaryData Id</param>
        /// <returns>ServiceSummaryData object</returns>
        OpServiceSummaryData GetServiceSummaryData(long Id);

        /// <summary>
        /// Gets ServiceSummaryData by id
        /// </summary>
        /// <param name="Ids">ServiceSummaryData Ids</param>
        /// <returns>ServiceSummaryData list</returns>
        List<OpServiceSummaryData> GetServiceSummaryData(long[] Ids);

        /// <summary>
        /// Gets ServiceSummaryData by id
        /// </summary>
        /// <param name="Id">ServiceSummaryData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceSummaryData object</returns>
        OpServiceSummaryData GetServiceSummaryData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ServiceSummaryData by id
        /// </summary>
        /// <param name="Ids">ServiceSummaryData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ServiceSummaryData list</returns>
        List<OpServiceSummaryData> GetServiceSummaryData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ServiceSummaryData object to the database
        /// </summary>
        /// <param name="toBeSaved">ServiceSummaryData to save</param>
        /// <returns>ServiceSummaryData Id</returns>
        long SaveServiceSummaryData(OpServiceSummaryData toBeSaved);

        /// <summary>
        /// Deletes the ServiceSummaryData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ServiceSummaryData to delete</param>
        void DeleteServiceSummaryData(OpServiceSummaryData toBeDeleted);

        /// <summary>
        /// Clears the ServiceSummaryData object cache
        /// </summary>
        void ClearServiceSummaryDataCache();
        #endregion

        #region Session

        /// <summary>
        /// Indicates whether the Session is cached.
        /// </summary>
        bool SessionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Session objects
        /// </summary>
        List<OpSession> GetAllSession();


        /// <summary>
        /// Gets Session by id
        /// </summary>
        /// <param name="Id">Session Id</param>
        /// <returns>Session object</returns>
        OpSession GetSession(long Id);

        /// <summary>
        /// Gets Session by id
        /// </summary>
        /// <param name="Ids">Session Ids</param>
        /// <returns>Session list</returns>
        List<OpSession> GetSession(long[] Ids);

        /// <summary>
        /// Gets Session by id
        /// </summary>
        /// <param name="Id">Session Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Session object</returns>
        OpSession GetSession(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Session by id
        /// </summary>
        /// <param name="Ids">Session Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Session list</returns>
        List<OpSession> GetSession(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Session object to the database
        /// </summary>
        /// <param name="toBeSaved">Session to save</param>
        /// <returns>Session Id</returns>
        long SaveSession(OpSession toBeSaved);

        /// <summary>
        /// Deletes the Session object from the database
        /// </summary>
        /// <param name="toBeDeleted">Session to delete</param>
        void DeleteSession(OpSession toBeDeleted);

        /// <summary>
        /// Clears the Session object cache
        /// </summary>
        void ClearSessionCache();
        #endregion

        #region SessionData

        /// <summary>
        /// Indicates whether the SessionData is cached.
        /// </summary>
        bool SessionDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SessionData objects
        /// </summary>
        List<OpSessionData> GetAllSessionData();


        /// <summary>
        /// Gets SessionData by id
        /// </summary>
        /// <param name="Id">SessionData Id</param>
        /// <returns>SessionData object</returns>
        OpSessionData GetSessionData(long Id);

        /// <summary>
        /// Gets SessionData by id
        /// </summary>
        /// <param name="Ids">SessionData Ids</param>
        /// <returns>SessionData list</returns>
        List<OpSessionData> GetSessionData(long[] Ids);

        /// <summary>
        /// Gets SessionData by id
        /// </summary>
        /// <param name="Id">SessionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionData object</returns>
        OpSessionData GetSessionData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets SessionData by id
        /// </summary>
        /// <param name="Ids">SessionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionData list</returns>
        List<OpSessionData> GetSessionData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the SessionData object to the database
        /// </summary>
        /// <param name="toBeSaved">SessionData to save</param>
        /// <returns>SessionData Id</returns>
        long SaveSessionData(OpSessionData toBeSaved);

        /// <summary>
        /// Deletes the SessionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">SessionData to delete</param>
        void DeleteSessionData(OpSessionData toBeDeleted);

        /// <summary>
        /// Clears the SessionData object cache
        /// </summary>
        void ClearSessionDataCache();
        #endregion

        #region SessionEvent

        /// <summary>
        /// Indicates whether the SessionEvent is cached.
        /// </summary>
        bool SessionEventCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SessionEvent objects
        /// </summary>
        List<OpSessionEvent> GetAllSessionEvent();


        /// <summary>
        /// Gets SessionEvent by id
        /// </summary>
        /// <param name="Id">SessionEvent Id</param>
        /// <returns>SessionEvent object</returns>
        OpSessionEvent GetSessionEvent(long Id);

        /// <summary>
        /// Gets SessionEvent by id
        /// </summary>
        /// <param name="Ids">SessionEvent Ids</param>
        /// <returns>SessionEvent list</returns>
        List<OpSessionEvent> GetSessionEvent(long[] Ids);

        /// <summary>
        /// Gets SessionEvent by id
        /// </summary>
        /// <param name="Id">SessionEvent Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionEvent object</returns>
        OpSessionEvent GetSessionEvent(long Id, bool queryDatabase);

        /// <summary>
        /// Gets SessionEvent by id
        /// </summary>
        /// <param name="Ids">SessionEvent Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionEvent list</returns>
        List<OpSessionEvent> GetSessionEvent(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the SessionEvent object to the database
        /// </summary>
        /// <param name="toBeSaved">SessionEvent to save</param>
        /// <returns>SessionEvent Id</returns>
        long SaveSessionEvent(OpSessionEvent toBeSaved);

        /// <summary>
        /// Deletes the SessionEvent object from the database
        /// </summary>
        /// <param name="toBeDeleted">SessionEvent to delete</param>
        void DeleteSessionEvent(OpSessionEvent toBeDeleted);

        /// <summary>
        /// Clears the SessionEvent object cache
        /// </summary>
        void ClearSessionEventCache();
        #endregion

        #region SessionEventData

        /// <summary>
        /// Indicates whether the SessionEventData is cached.
        /// </summary>
        bool SessionEventDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SessionEventData objects
        /// </summary>
        List<OpSessionEventData> GetAllSessionEventData();


        /// <summary>
        /// Gets SessionEventData by id
        /// </summary>
        /// <param name="Id">SessionEventData Id</param>
        /// <returns>SessionEventData object</returns>
        OpSessionEventData GetSessionEventData(long Id);

        /// <summary>
        /// Gets SessionEventData by id
        /// </summary>
        /// <param name="Ids">SessionEventData Ids</param>
        /// <returns>SessionEventData list</returns>
        List<OpSessionEventData> GetSessionEventData(long[] Ids);

        /// <summary>
        /// Gets SessionEventData by id
        /// </summary>
        /// <param name="Id">SessionEventData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionEventData object</returns>
        OpSessionEventData GetSessionEventData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets SessionEventData by id
        /// </summary>
        /// <param name="Ids">SessionEventData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionEventData list</returns>
        List<OpSessionEventData> GetSessionEventData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the SessionEventData object to the database
        /// </summary>
        /// <param name="toBeSaved">SessionEventData to save</param>
        /// <returns>SessionEventData Id</returns>
        long SaveSessionEventData(OpSessionEventData toBeSaved);

        /// <summary>
        /// Deletes the SessionEventData object from the database
        /// </summary>
        /// <param name="toBeDeleted">SessionEventData to delete</param>
        void DeleteSessionEventData(OpSessionEventData toBeDeleted);

        /// <summary>
        /// Clears the SessionEventData object cache
        /// </summary>
        void ClearSessionEventDataCache();
        #endregion

        #region SessionStatus

        /// <summary>
        /// Indicates whether the SessionStatus is cached.
        /// </summary>
        bool SessionStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SessionStatus objects
        /// </summary>
        List<OpSessionStatus> GetAllSessionStatus();


        /// <summary>
        /// Gets SessionStatus by id
        /// </summary>
        /// <param name="Id">SessionStatus Id</param>
        /// <returns>SessionStatus object</returns>
        OpSessionStatus GetSessionStatus(int Id);

        /// <summary>
        /// Gets SessionStatus by id
        /// </summary>
        /// <param name="Ids">SessionStatus Ids</param>
        /// <returns>SessionStatus list</returns>
        List<OpSessionStatus> GetSessionStatus(int[] Ids);

        /// <summary>
        /// Gets SessionStatus by id
        /// </summary>
        /// <param name="Id">SessionStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionStatus object</returns>
        OpSessionStatus GetSessionStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets SessionStatus by id
        /// </summary>
        /// <param name="Ids">SessionStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SessionStatus list</returns>
        List<OpSessionStatus> GetSessionStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the SessionStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">SessionStatus to save</param>
        /// <returns>SessionStatus Id</returns>
        int SaveSessionStatus(OpSessionStatus toBeSaved);

        /// <summary>
        /// Deletes the SessionStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">SessionStatus to delete</param>
        void DeleteSessionStatus(OpSessionStatus toBeDeleted);

        /// <summary>
        /// Clears the SessionStatus object cache
        /// </summary>
        void ClearSessionStatusCache();
        #endregion

        #region ShippingList

        /// <summary>
        /// Indicates whether the ShippingList is cached.
        /// </summary>
        bool ShippingListCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ShippingList objects
        /// </summary>
        List<OpShippingList> GetAllShippingList(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ShippingList by id
        /// </summary>
        /// <param name="Id">ShippingList Id</param>
        /// <returns>ShippingList object</returns>
        OpShippingList GetShippingList(int Id);

        /// <summary>
        /// Gets ShippingList by id
        /// </summary>
        /// <param name="Ids">ShippingList Ids</param>
        /// <returns>ShippingList list</returns>
        List<OpShippingList> GetShippingList(int[] Ids);

        /// <summary>
        /// Gets ShippingList by id
        /// </summary>
        /// <param name="Id">ShippingList Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingList object</returns>
        OpShippingList GetShippingList(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ShippingList by id
        /// </summary>
        /// <param name="Ids">ShippingList Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingList list</returns>
        List<OpShippingList> GetShippingList(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ShippingList object to the database
        /// </summary>
        /// <param name="toBeSaved">ShippingList to save</param>
        /// <returns>ShippingList Id</returns>
        int SaveShippingList(OpShippingList toBeSaved);

        /// <summary>
        /// Deletes the ShippingList object from the database
        /// </summary>
        /// <param name="toBeDeleted">ShippingList to delete</param>
        void DeleteShippingList(OpShippingList toBeDeleted);

        /// <summary>
        /// Clears the ShippingList object cache
        /// </summary>
        void ClearShippingListCache();
        #endregion

        #region ShippingListData

        /// <summary>
        /// Indicates whether the ShippingListData is cached.
        /// </summary>
        bool ShippingListDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ShippingListData objects
        /// </summary>
        List<OpShippingListData> GetAllShippingListData();


        /// <summary>
        /// Gets ShippingListData by id
        /// </summary>
        /// <param name="Id">ShippingListData Id</param>
        /// <returns>ShippingListData object</returns>
        OpShippingListData GetShippingListData(long Id);

        /// <summary>
        /// Gets ShippingListData by id
        /// </summary>
        /// <param name="Ids">ShippingListData Ids</param>
        /// <returns>ShippingListData list</returns>
        List<OpShippingListData> GetShippingListData(long[] Ids);

        /// <summary>
        /// Gets ShippingListData by id
        /// </summary>
        /// <param name="Id">ShippingListData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingListData object</returns>
        OpShippingListData GetShippingListData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ShippingListData by id
        /// </summary>
        /// <param name="Ids">ShippingListData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingListData list</returns>
        List<OpShippingListData> GetShippingListData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ShippingListData object to the database
        /// </summary>
        /// <param name="toBeSaved">ShippingListData to save</param>
        /// <returns>ShippingListData Id</returns>
        long SaveShippingListData(OpShippingListData toBeSaved);

        /// <summary>
        /// Deletes the ShippingListData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ShippingListData to delete</param>
        void DeleteShippingListData(OpShippingListData toBeDeleted);

        /// <summary>
        /// Clears the ShippingListData object cache
        /// </summary>
        void ClearShippingListDataCache();
        #endregion

        #region ShippingListDevice

        /// <summary>
        /// Indicates whether the ShippingListDevice is cached.
        /// </summary>
        bool ShippingListDeviceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ShippingListDevice objects
        /// </summary>
        List<OpShippingListDevice> GetAllShippingListDevice();

        List<OpShippingListDevice> GetAllShippingListDeviceCustom();


        /// <summary>
        /// Gets ShippingListDevice by id
        /// </summary>
        /// <param name="Id">ShippingListDevice Id</param>
        /// <returns>ShippingListDevice object</returns>
        OpShippingListDevice GetShippingListDevice(int Id);

        /// <summary>
        /// Gets ShippingListDevice by id
        /// </summary>
        /// <param name="Ids">ShippingListDevice Ids</param>
        /// <returns>ShippingListDevice list</returns>
        List<OpShippingListDevice> GetShippingListDevice(int[] Ids);

        /// <summary>
        /// Gets ShippingListDevice by id
        /// </summary>
        /// <param name="Id">ShippingListDevice Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingListDevice object</returns>
        OpShippingListDevice GetShippingListDevice(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ShippingListDevice by id
        /// </summary>
        /// <param name="Ids">ShippingListDevice Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingListDevice list</returns>
        List<OpShippingListDevice> GetShippingListDevice(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ShippingListDevice object to the database
        /// </summary>
        /// <param name="toBeSaved">ShippingListDevice to save</param>
        /// <returns>ShippingListDevice Id</returns>
        int SaveShippingListDevice(OpShippingListDevice toBeSaved);

        /// <summary>
        /// Deletes the ShippingListDevice object from the database
        /// </summary>
        /// <param name="toBeDeleted">ShippingListDevice to delete</param>
        void DeleteShippingListDevice(OpShippingListDevice toBeDeleted);

        /// <summary>
        /// Clears the ShippingListDevice object cache
        /// </summary>
        void ClearShippingListDeviceCache();
        #endregion

        #region DeviceWarranty

        /// <summary>
        /// Indicates whether the DeviceWarranty is cached.
        /// </summary>
        bool DeviceWarrantyCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceWarranty objects
        /// </summary>
        List<OpDeviceWarranty> GetAllDeviceWarranty(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DeviceWarranty by id
        /// </summary>
        /// <param name="Id">DeviceWarranty Id</param>
        /// <returns>DeviceWarranty object</returns>
        OpDeviceWarranty GetDeviceWarranty(int Id);

        /// <summary>
        /// Gets DeviceWarranty by id
        /// </summary>
        /// <param name="Ids">DeviceWarranty Ids</param>
        /// <returns>DeviceWarranty list</returns>
        List<OpDeviceWarranty> GetDeviceWarranty(int[] Ids);

        /// <summary>
        /// Gets DeviceWarranty by id
        /// </summary>
        /// <param name="Id">DeviceWarranty Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceWarranty object</returns>
        OpDeviceWarranty GetDeviceWarranty(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceWarranty by id
        /// </summary>
        /// <param name="Ids">DeviceWarranty Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceWarranty list</returns>
        List<OpDeviceWarranty> GetDeviceWarranty(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DeviceWarranty object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceWarranty to save</param>
        /// <returns>DeviceWarranty Id</returns>
        int SaveDeviceWarranty(OpDeviceWarranty toBeSaved);

        /// <summary>
        /// Deletes the DeviceWarranty object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceWarranty to delete</param>
        void DeleteDeviceWarranty(OpDeviceWarranty toBeDeleted);

        /// <summary>
        /// Clears the DeviceWarranty object cache
        /// </summary>
        void ClearDeviceWarrantyCache();
        #endregion

        #region ShippingListType

        /// <summary>
        /// Indicates whether the ShippingListType is cached.
        /// </summary>
        bool ShippingListTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ShippingListType objects
        /// </summary>
        List<OpShippingListType> GetAllShippingListType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ShippingListType by id
        /// </summary>
        /// <param name="Id">ShippingListType Id</param>
        /// <returns>ShippingListType object</returns>
        OpShippingListType GetShippingListType(int Id);

        /// <summary>
        /// Gets ShippingListType by id
        /// </summary>
        /// <param name="Ids">ShippingListType Ids</param>
        /// <returns>ShippingListType list</returns>
        List<OpShippingListType> GetShippingListType(int[] Ids);

        /// <summary>
        /// Gets ShippingListType by id
        /// </summary>
        /// <param name="Id">ShippingListType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingListType object</returns>
        OpShippingListType GetShippingListType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ShippingListType by id
        /// </summary>
        /// <param name="Ids">ShippingListType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ShippingListType list</returns>
        List<OpShippingListType> GetShippingListType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ShippingListType object to the database
        /// </summary>
        /// <param name="toBeSaved">ShippingListType to save</param>
        /// <returns>ShippingListType Id</returns>
        int SaveShippingListType(OpShippingListType toBeSaved);

        /// <summary>
        /// Deletes the ShippingListType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ShippingListType to delete</param>
        void DeleteShippingListType(OpShippingListType toBeDeleted);

        /// <summary>
        /// Clears the ShippingListType object cache
        /// </summary>
        void ClearShippingListTypeCache();
        #endregion

        #region SimCard

        /// <summary>
        /// Indicates whether the SimCard is cached.
        /// </summary>
        bool SimCardCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SimCard objects
        /// </summary>
        List<OpSimCard> GetAllSimCard();


        /// <summary>
        /// Gets SimCard by id
        /// </summary>
        /// <param name="Id">SimCard Id</param>
        /// <returns>SimCard object</returns>
        OpSimCard GetSimCard(int Id);

        /// <summary>
        /// Gets SimCard by id
        /// </summary>
        /// <param name="Ids">SimCard Ids</param>
        /// <returns>SimCard list</returns>
        List<OpSimCard> GetSimCard(int[] Ids);

        /// <summary>
        /// Gets SimCard by id
        /// </summary>
        /// <param name="Id">SimCard Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SimCard object</returns>
        OpSimCard GetSimCard(int Id, bool queryDatabase);

        /// <summary>
        /// Gets SimCard by id
        /// </summary>
        /// <param name="Ids">SimCard Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SimCard list</returns>
        List<OpSimCard> GetSimCard(int[] Ids, bool queryDatabase, bool loadCustomData = true);

        /// <summary>
        /// Saves the SimCard object to the database
        /// </summary>
        /// <param name="toBeSaved">SimCard to save</param>
        /// <returns>SimCard Id</returns>
        int SaveSimCard(OpSimCard toBeSaved);

        /// <summary>
        /// Deletes the SimCard object from the database
        /// </summary>
        /// <param name="toBeDeleted">SimCard to delete</param>
        void DeleteSimCard(OpSimCard toBeDeleted);

        /// <summary>
        /// Clears the SimCard object cache
        /// </summary>
        void ClearSimCardCache();
        #endregion

        #region SimCardData

        /// <summary>
        /// Indicates whether the SimCardData is cached.
        /// </summary>
        bool SimCardDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SimCardData objects
        /// </summary>
        List<OpSimCardData> GetAllSimCardData(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets SimCardData by id
        /// </summary>
        /// <param name="Id">SimCardData Id</param>
        /// <returns>SimCardData object</returns>
        OpSimCardData GetSimCardData(long Id);

        /// <summary>
        /// Gets SimCardData by id
        /// </summary>
        /// <param name="Ids">SimCardData Ids</param>
        /// <returns>SimCardData list</returns>
        List<OpSimCardData> GetSimCardData(long[] Ids);

        /// <summary>
        /// Gets SimCardData by id
        /// </summary>
        /// <param name="Id">SimCardData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SimCardData object</returns>
        OpSimCardData GetSimCardData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets SimCardData by id
        /// </summary>
        /// <param name="Ids">SimCardData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SimCardData list</returns>
        List<OpSimCardData> GetSimCardData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the SimCardData object to the database
        /// </summary>
        /// <param name="toBeSaved">SimCardData to save</param>
        /// <returns>SimCardData Id</returns>
        long SaveSimCardData(OpSimCardData toBeSaved);

        /// <summary>
        /// Deletes the SimCardData object from the database
        /// </summary>
        /// <param name="toBeDeleted">SimCardData to delete</param>
        void DeleteSimCardData(OpSimCardData toBeDeleted);

        /// <summary>
        /// Clears the SimCardData object cache
        /// </summary>
        void ClearSimCardDataCache();
        #endregion

        #region Sla

        /// <summary>
        /// Indicates whether the Sla is cached.
        /// </summary>
        bool SlaCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Sla objects
        /// </summary>
        List<OpSla> GetAllSla();


        /// <summary>
        /// Gets Sla by id
        /// </summary>
        /// <param name="Id">Sla Id</param>
        /// <returns>Sla object</returns>
        OpSla GetSla(int Id);

        /// <summary>
        /// Gets Sla by id
        /// </summary>
        /// <param name="Ids">Sla Ids</param>
        /// <returns>Sla list</returns>
        List<OpSla> GetSla(int[] Ids);

        /// <summary>
        /// Gets Sla by id
        /// </summary>
        /// <param name="Id">Sla Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Sla object</returns>
        OpSla GetSla(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Sla by id
        /// </summary>
        /// <param name="Ids">Sla Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Sla list</returns>
        List<OpSla> GetSla(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Sla object to the database
        /// </summary>
        /// <param name="toBeSaved">Sla to save</param>
        /// <returns>Sla Id</returns>
        int SaveSla(OpSla toBeSaved);

        /// <summary>
        /// Deletes the Sla object from the database
        /// </summary>
        /// <param name="toBeDeleted">Sla to delete</param>
        void DeleteSla(OpSla toBeDeleted);

        /// <summary>
        /// Clears the Sla object cache
        /// </summary>
        void ClearSlaCache();
        #endregion

        #region SlotType

        /// <summary>
        /// Indicates whether the SlotType is cached.
        /// </summary>
        bool SlotTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SlotType objects
        /// </summary>
        List<OpSlotType> GetAllSlotType();


        /// <summary>
        /// Gets SlotType by id
        /// </summary>
        /// <param name="Id">SlotType Id</param>
        /// <returns>SlotType object</returns>
        OpSlotType GetSlotType(int Id);

        /// <summary>
        /// Gets SlotType by id
        /// </summary>
        /// <param name="Ids">SlotType Ids</param>
        /// <returns>SlotType list</returns>
        List<OpSlotType> GetSlotType(int[] Ids);

        /// <summary>
        /// Gets SlotType by id
        /// </summary>
        /// <param name="Id">SlotType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SlotType object</returns>
        OpSlotType GetSlotType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets SlotType by id
        /// </summary>
        /// <param name="Ids">SlotType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SlotType list</returns>
        List<OpSlotType> GetSlotType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the SlotType object to the database
        /// </summary>
        /// <param name="toBeSaved">SlotType to save</param>
        /// <returns>SlotType Id</returns>
        int SaveSlotType(OpSlotType toBeSaved);

        /// <summary>
        /// Deletes the SlotType object from the database
        /// </summary>
        /// <param name="toBeDeleted">SlotType to delete</param>
        void DeleteSlotType(OpSlotType toBeDeleted);

        /// <summary>
        /// Clears the SlotType object cache
        /// </summary>
        void ClearSlotTypeCache();
        #endregion

        #region Supplier

        /// <summary>
        /// Indicates whether the Supplier is cached.
        /// </summary>
        bool SupplierCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Supplier objects
        /// </summary>
        List<OpSupplier> GetAllSupplier();


        /// <summary>
        /// Gets Supplier by id
        /// </summary>
        /// <param name="Id">Supplier Id</param>
        /// <returns>Supplier object</returns>
        OpSupplier GetSupplier(int Id);

        /// <summary>
        /// Gets Supplier by id
        /// </summary>
        /// <param name="Ids">Supplier Ids</param>
        /// <returns>Supplier list</returns>
        List<OpSupplier> GetSupplier(int[] Ids);

        /// <summary>
        /// Gets Supplier by id
        /// </summary>
        /// <param name="Id">Supplier Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Supplier object</returns>
        OpSupplier GetSupplier(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Supplier by id
        /// </summary>
        /// <param name="Ids">Supplier Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Supplier list</returns>
        List<OpSupplier> GetSupplier(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Supplier object to the database
        /// </summary>
        /// <param name="toBeSaved">Supplier to save</param>
        /// <returns>Supplier Id</returns>
        int SaveSupplier(OpSupplier toBeSaved);

        /// <summary>
        /// Deletes the Supplier object from the database
        /// </summary>
        /// <param name="toBeDeleted">Supplier to delete</param>
        void DeleteSupplier(OpSupplier toBeDeleted);

        /// <summary>
        /// Clears the Supplier object cache
        /// </summary>
        void ClearSupplierCache();
        #endregion

        #region SystemData

        /// <summary>
        /// Indicates whether the SystemData is cached.
        /// </summary>
        bool SystemDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SystemData objects
        /// </summary>
        List<OpSystemData> GetAllSystemData();


        /// <summary>
        /// Gets SystemData by id
        /// </summary>
        /// <param name="Id">SystemData Id</param>
        /// <returns>SystemData object</returns>
        OpSystemData GetSystemData(long Id);

        /// <summary>
        /// Gets SystemData by id
        /// </summary>
        /// <param name="Ids">SystemData Ids</param>
        /// <returns>SystemData list</returns>
        List<OpSystemData> GetSystemData(long[] Ids);

        /// <summary>
        /// Gets SystemData by id
        /// </summary>
        /// <param name="Id">SystemData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SystemData object</returns>
        OpSystemData GetSystemData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets SystemData by id
        /// </summary>
        /// <param name="Ids">SystemData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SystemData list</returns>
        List<OpSystemData> GetSystemData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the SystemData object to the database
        /// </summary>
        /// <param name="toBeSaved">SystemData to save</param>
        /// <returns>SystemData Id</returns>
        long SaveSystemData(OpSystemData toBeSaved);

        /// <summary>
        /// Deletes the SystemData object from the database
        /// </summary>
        /// <param name="toBeDeleted">SystemData to delete</param>
        void DeleteSystemData(OpSystemData toBeDeleted);

        /// <summary>
        /// Clears the SystemData object cache
        /// </summary>
        void ClearSystemDataCache();
        #endregion

        #region Tariff

        /// <summary>
        /// Indicates whether the Tariff is cached.
        /// </summary>
        bool TariffCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Tariff objects
        /// </summary>
        List<OpTariff> GetAllTariff();

        /// <summary>
        /// Gets Tariff by id
        /// </summary>
        /// <param name="Id">Tariff Id</param>
        /// <returns>Tariff object</returns>
        OpTariff GetTariff(int Id);

        /// <summary>
        /// Gets Tariff by id
        /// </summary>
        /// <param name="Ids">Tariff Ids</param>
        /// <returns>Tariff list</returns>
        List<OpTariff> GetTariff(int[] Ids);

        /// <summary>
        /// Gets Tariff by id
        /// </summary>
        /// <param name="Id">Tariff Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Tariff object</returns>
        OpTariff GetTariff(int Id, bool queryDatabase);

        /// <summary>
        /// Saves the Tariff object to the database
        /// </summary>
        /// <param name="toBeSaved">Tariff to save</param>
        /// <returns>Tariff Id</returns>
        int SaveTariff(OpTariff toBeSaved);

        /// <summary>
        /// Deletes the Tariff object from the database
        /// </summary>
        /// <param name="toBeDeleted">Tariff to delete</param>
        void DeleteTariff(OpTariff toBeDeleted);

        /// <summary>
        /// Clears the Tariff object cache
        /// </summary>
        void ClearTariffCache();
        #endregion

        #region TariffData

        /// <summary>
        /// Indicates whether the TariffData is cached.
        /// </summary>
        bool TariffDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TariffData objects
        /// </summary>
        List<OpTariffData> GetAllTariffData();


        /// <summary>
        /// Gets TariffData by id
        /// </summary>
        /// <param name="Id">TariffData Id</param>
        /// <returns>TariffData object</returns>
        OpTariffData GetTariffData(long Id);

        /// <summary>
        /// Gets TariffData by id
        /// </summary>
        /// <param name="Ids">TariffData Ids</param>
        /// <returns>TariffData list</returns>
        List<OpTariffData> GetTariffData(long[] Ids);

        /// <summary>
        /// Gets TariffData by id
        /// </summary>
        /// <param name="Id">TariffData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TariffData object</returns>
        OpTariffData GetTariffData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets TariffData by id
        /// </summary>
        /// <param name="Ids">TariffData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TariffData list</returns>
        List<OpTariffData> GetTariffData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TariffData object to the database
        /// </summary>
        /// <param name="toBeSaved">TariffData to save</param>
        /// <returns>TariffData Id</returns>
        long SaveTariffData(OpTariffData toBeSaved);

        /// <summary>
        /// Deletes the TariffData object from the database
        /// </summary>
        /// <param name="toBeDeleted">TariffData to delete</param>
        void DeleteTariffData(OpTariffData toBeDeleted);

        /// <summary>
        /// Clears the TariffData object cache
        /// </summary>
        void ClearTariffDataCache();
        #endregion

        #region SettlementDocumentType

        /// <summary>
        /// Indicates whether the SettlementDocumentType is cached.
        /// </summary>
        bool SettlementDocumentTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all SettlementDocumentType objects
        /// </summary>
        List<OpSettlementDocumentType> GetAllSettlementDocumentType();


        /// <summary>
        /// Gets SettlementDocumentType by id
        /// </summary>
        /// <param name="Id">SettlementDocumentType Id</param>
        /// <returns>SettlementDocumentType object</returns>
        OpSettlementDocumentType GetSettlementDocumentType(int Id);

        /// <summary>
        /// Gets SettlementDocumentType by id
        /// </summary>
        /// <param name="Ids">SettlementDocumentType Ids</param>
        /// <returns>SettlementDocumentType list</returns>
        List<OpSettlementDocumentType> GetSettlementDocumentType(int[] Ids);

        /// <summary>
        /// Gets SettlementDocumentType by id
        /// </summary>
        /// <param name="Id">SettlementDocumentType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SettlementDocumentType object</returns>
        OpSettlementDocumentType GetSettlementDocumentType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets SettlementDocumentType by id
        /// </summary>
        /// <param name="Ids">SettlementDocumentType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>SettlementDocumentType list</returns>
        List<OpSettlementDocumentType> GetSettlementDocumentType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the SettlementDocumentType object to the database
        /// </summary>
        /// <param name="toBeSaved">SettlementDocumentType to save</param>
        /// <returns>SettlementDocumentType Id</returns>
        int SaveSettlementDocumentType(OpSettlementDocumentType toBeSaved);

        /// <summary>
        /// Deletes the SettlementDocumentType object from the database
        /// </summary>
        /// <param name="toBeDeleted">SettlementDocumentType to delete</param>
        void DeleteSettlementDocumentType(OpSettlementDocumentType toBeDeleted);

        /// <summary>
        /// Clears the SettlementDocumentType object cache
        /// </summary>
        void ClearSettlementDocumentTypeCache();
        #endregion

        #region TariffSettlementPeriod

        /// <summary>
        /// Indicates whether the TariffSettlementPeriod is cached.
        /// </summary>
        bool TariffSettlementPeriodCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TariffSettlementPeriod objects
        /// </summary>
        List<OpTariffSettlementPeriod> GetAllTariffSettlementPeriod();

        /// <summary>
        /// Gets TariffSettlementPeriod by id
        /// </summary>
        /// <param name="Id">TariffSettlementPeriod Id</param>
        /// <returns>TariffSettlementPeriod object</returns>
        OpTariffSettlementPeriod GetTariffSettlementPeriod(int Id);

        /// <summary>
        /// Gets TariffSettlementPeriod by id
        /// </summary>
        /// <param name="Ids">TariffSettlementPeriod Ids</param>
        /// <returns>TariffSettlementPeriod list</returns>
        List<OpTariffSettlementPeriod> GetTariffSettlementPeriod(int[] Ids);

        /// <summary>
        /// Gets TariffSettlementPeriod by id
        /// </summary>
        /// <param name="Id">TariffSettlementPeriod Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TariffSettlementPeriod object</returns>
        OpTariffSettlementPeriod GetTariffSettlementPeriod(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TariffSettlementPeriod by id
        /// </summary>
        /// <param name="Ids">TariffSettlementPeriod Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TariffSettlementPeriod list</returns>
        List<OpTariffSettlementPeriod> GetTariffSettlementPeriod(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the TariffSettlementPeriod object to the database
        /// </summary>
        /// <param name="toBeSaved">TariffSettlementPeriod to save</param>
        /// <returns>TariffSettlementPeriod Id</returns>
        int SaveTariffSettlementPeriod(OpTariffSettlementPeriod toBeSaved);

        /// <summary>
        /// Deletes the TariffSettlementPeriod object from the database
        /// </summary>
        /// <param name="toBeDeleted">TariffSettlementPeriod to delete</param>
        void DeleteTariffSettlementPeriod(OpTariffSettlementPeriod toBeDeleted);

        /// <summary>
        /// Clears the TariffSettlementPeriod object cache
        /// </summary>
        void ClearTariffSettlementPeriodCache();
        #endregion

        #region Task

        /// <summary>
        /// Indicates whether the Task is cached.
        /// </summary>
        bool TaskCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Task objects
        /// </summary>
        List<OpTask> GetAllTask(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Task by id
        /// </summary>
        /// <param name="Id">Task Id</param>
        /// <returns>Task object</returns>
        OpTask GetTask(int Id);

        /// <summary>
        /// Gets Task by id
        /// </summary>
        /// <param name="Ids">Task Ids</param>
        /// <returns>Task list</returns>
        List<OpTask> GetTask(int[] Ids);

        /// <summary>
        /// Gets Task by id
        /// </summary>
        /// <param name="Id">Task Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Task object</returns>
        OpTask GetTask(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Task by id
        /// </summary>
        /// <param name="Ids">Task Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Task list</returns>
        List<OpTask> GetTask(int[] Ids, bool queryDatabase, bool loadCustomData, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Task object to the database
        /// </summary>
        /// <param name="toBeSaved">Task to save</param>
        /// <returns>Task Id</returns>
        int SaveTask(OpTask toBeSaved);

        /// <summary>
        /// Deletes the Task object from the database
        /// </summary>
        /// <param name="toBeDeleted">Task to delete</param>
        void DeleteTask(OpTask toBeDeleted);

        /// <summary>
        /// Clears the Task object cache
        /// </summary>
        void ClearTaskCache();
        #endregion

        #region TaskData

        /// <summary>
        /// Indicates whether the TaskData is cached.
        /// </summary>
        bool TaskDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TaskData objects
        /// </summary>
        List<OpTaskData> GetAllTaskData();


        /// <summary>
        /// Gets TaskData by id
        /// </summary>
        /// <param name="Id">TaskData Id</param>
        /// <returns>TaskData object</returns>
        OpTaskData GetTaskData(long Id);

        /// <summary>
        /// Gets TaskData by id
        /// </summary>
        /// <param name="Ids">TaskData Ids</param>
        /// <returns>TaskData list</returns>
        List<OpTaskData> GetTaskData(long[] Ids);

        /// <summary>
        /// Gets TaskData by id
        /// </summary>
        /// <param name="Id">TaskData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskData object</returns>
        OpTaskData GetTaskData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets TaskData by id
        /// </summary>
        /// <param name="Ids">TaskData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskData list</returns>
        List<OpTaskData> GetTaskData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TaskData object to the database
        /// </summary>
        /// <param name="toBeSaved">TaskData to save</param>
        /// <returns>TaskData Id</returns>
        long SaveTaskData(OpTaskData toBeSaved);

        /// <summary>
        /// Deletes the TaskData object from the database
        /// </summary>
        /// <param name="toBeDeleted">TaskData to delete</param>
        void DeleteTaskData(OpTaskData toBeDeleted);

        /// <summary>
        /// Clears the TaskData object cache
        /// </summary>
        void ClearTaskDataCache();
        #endregion

        #region TaskGroup

        /// <summary>
        /// Indicates whether the TaskGroup is cached.
        /// </summary>
        bool TaskGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TaskGroup objects
        /// </summary>
        List<OpTaskGroup> GetAllTaskGroup(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets TaskGroup by id
        /// </summary>
        /// <param name="Id">TaskGroup Id</param>
        /// <returns>TaskGroup object</returns>
        OpTaskGroup GetTaskGroup(int Id);

        /// <summary>
        /// Gets TaskGroup by id
        /// </summary>
        /// <param name="Ids">TaskGroup Ids</param>
        /// <returns>TaskGroup list</returns>
        List<OpTaskGroup> GetTaskGroup(int[] Ids);

        /// <summary>
        /// Gets TaskGroup by id
        /// </summary>
        /// <param name="Id">TaskGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskGroup object</returns>
        OpTaskGroup GetTaskGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TaskGroup by id
        /// </summary>
        /// <param name="Ids">TaskGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskGroup list</returns>
        List<OpTaskGroup> GetTaskGroup(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the TaskGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">TaskGroup to save</param>
        /// <returns>TaskGroup Id</returns>
        int SaveTaskGroup(OpTaskGroup toBeSaved);

        /// <summary>
        /// Deletes the TaskGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">TaskGroup to delete</param>
        void DeleteTaskGroup(OpTaskGroup toBeDeleted);

        /// <summary>
        /// Clears the TaskGroup object cache
        /// </summary>
        void ClearTaskGroupCache();
        #endregion

        #region TaskHistory

        /// <summary>
        /// Indicates whether the TaskHistory is cached.
        /// </summary>
        bool TaskHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TaskHistory objects
        /// </summary>
        List<OpTaskHistory> GetAllTaskHistory();


        /// <summary>
        /// Gets TaskHistory by id
        /// </summary>
        /// <param name="Id">TaskHistory Id</param>
        /// <returns>TaskHistory object</returns>
        OpTaskHistory GetTaskHistory(int Id);

        /// <summary>
        /// Gets TaskHistory by id
        /// </summary>
        /// <param name="Ids">TaskHistory Ids</param>
        /// <returns>TaskHistory list</returns>
        List<OpTaskHistory> GetTaskHistory(int[] Ids);

        /// <summary>
        /// Gets TaskHistory by id
        /// </summary>
        /// <param name="Id">TaskHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskHistory object</returns>
        OpTaskHistory GetTaskHistory(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TaskHistory by id
        /// </summary>
        /// <param name="Ids">TaskHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskHistory list</returns>
        List<OpTaskHistory> GetTaskHistory(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TaskHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">TaskHistory to save</param>
        /// <returns>TaskHistory Id</returns>
        int SaveTaskHistory(OpTaskHistory toBeSaved);

        /// <summary>
        /// Deletes the TaskHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">TaskHistory to delete</param>
        void DeleteTaskHistory(OpTaskHistory toBeDeleted);

        /// <summary>
        /// Clears the TaskHistory object cache
        /// </summary>
        void ClearTaskHistoryCache();
        #endregion

        #region TaskStatus

        /// <summary>
        /// Indicates whether the TaskStatus is cached.
        /// </summary>
        bool TaskStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TaskStatus objects
        /// </summary>
        List<OpTaskStatus> GetAllTaskStatus(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets TaskStatus by id
        /// </summary>
        /// <param name="Id">TaskStatus Id</param>
        /// <returns>TaskStatus object</returns>
        OpTaskStatus GetTaskStatus(int Id);

        /// <summary>
        /// Gets TaskStatus by id
        /// </summary>
        /// <param name="Ids">TaskStatus Ids</param>
        /// <returns>TaskStatus list</returns>
        List<OpTaskStatus> GetTaskStatus(int[] Ids);

        /// <summary>
        /// Gets TaskStatus by id
        /// </summary>
        /// <param name="Id">TaskStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskStatus object</returns>
        OpTaskStatus GetTaskStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TaskStatus by id
        /// </summary>
        /// <param name="Ids">TaskStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskStatus list</returns>
        List<OpTaskStatus> GetTaskStatus(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the TaskStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">TaskStatus to save</param>
        /// <returns>TaskStatus Id</returns>
        int SaveTaskStatus(OpTaskStatus toBeSaved);

        /// <summary>
        /// Deletes the TaskStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">TaskStatus to delete</param>
        void DeleteTaskStatus(OpTaskStatus toBeDeleted);

        /// <summary>
        /// Clears the TaskStatus object cache
        /// </summary>
        void ClearTaskStatusCache();
        #endregion

        #region TaskType

        /// <summary>
        /// Indicates whether the TaskType is cached.
        /// </summary>
        bool TaskTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TaskType objects
        /// </summary>
        List<OpTaskType> GetAllTaskType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets TaskType by id
        /// </summary>
        /// <param name="Id">TaskType Id</param>
        /// <returns>TaskType object</returns>
        OpTaskType GetTaskType(int Id);

        /// <summary>
        /// Gets TaskType by id
        /// </summary>
        /// <param name="Ids">TaskType Ids</param>
        /// <returns>TaskType list</returns>
        List<OpTaskType> GetTaskType(int[] Ids);

        /// <summary>
        /// Gets TaskType by id
        /// </summary>
        /// <param name="Id">TaskType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskType object</returns>
        OpTaskType GetTaskType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TaskType by id
        /// </summary>
        /// <param name="Ids">TaskType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskType list</returns>
        List<OpTaskType> GetTaskType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the TaskType object to the database
        /// </summary>
        /// <param name="toBeSaved">TaskType to save</param>
        /// <returns>TaskType Id</returns>
        int SaveTaskType(OpTaskType toBeSaved);

        /// <summary>
        /// Deletes the TaskType object from the database
        /// </summary>
        /// <param name="toBeDeleted">TaskType to delete</param>
        void DeleteTaskType(OpTaskType toBeDeleted);

        /// <summary>
        /// Clears the TaskType object cache
        /// </summary>
        void ClearTaskTypeCache();
        #endregion

        #region ThermalConversion

        #region GetThermalConversionTable
        //private object GetThermalConversionTableTarget(SqlDataReader input)
        //{
        //    List<double[]> RetVal = new List<double[]>();

        //    try
        //    {
        //        while (true)
        //        {
        //            input.Read();

        //            double[] New = new double[3];

        //            New[0] = float.Parse(input["TEMPERATURE"].ToString());

        //            New[1] = float.Parse(input["DENSITY"].ToString());

        //            New[2] = float.Parse(input["VALUE"].ToString());

        //            RetVal.Add(New);
        //        }
        //    }
        //    catch
        //    {
        //        //do nothing
        //    }
        //    finally
        //    {
        //        input.Close();
        //    }

        //    return RetVal;
        //}

        //List<double[]> GetThermalConversionTable()
        //{
        //    DB.AnalyzeDataReader reader = new DB.AnalyzeDataReader(GetThermalConversionTableTarget);

        //    return (List<double[]>)dbConnectionDw.ExecuteProcedure("[dbo].[awsr_GetConversionTable]", reader, null);
        //}

        #endregion

        #endregion

        #region TaskTypeGroup

        /// <summary>
        /// Indicates whether the TaskTypeGroup is cached.
        /// </summary>
        bool TaskTypeGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TaskTypeGroup objects
        /// </summary>
        List<OpTaskTypeGroup> GetAllTaskTypeGroup(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets TaskTypeGroup by id
        /// </summary>
        /// <param name="Id">TaskTypeGroup Id</param>
        /// <returns>TaskTypeGroup object</returns>
        OpTaskTypeGroup GetTaskTypeGroup(int Id);

        /// <summary>
        /// Gets TaskTypeGroup by id
        /// </summary>
        /// <param name="Ids">TaskTypeGroup Ids</param>
        /// <returns>TaskTypeGroup list</returns>
        List<OpTaskTypeGroup> GetTaskTypeGroup(int[] Ids);

        /// <summary>
        /// Gets TaskTypeGroup by id
        /// </summary>
        /// <param name="Id">TaskTypeGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskTypeGroup object</returns>
        OpTaskTypeGroup GetTaskTypeGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TaskTypeGroup by id
        /// </summary>
        /// <param name="Ids">TaskTypeGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TaskTypeGroup list</returns>
        List<OpTaskTypeGroup> GetTaskTypeGroup(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the TaskTypeGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">TaskTypeGroup to save</param>
        /// <returns>TaskTypeGroup Id</returns>
        int SaveTaskTypeGroup(OpTaskTypeGroup toBeSaved);

        /// <summary>
        /// Deletes the TaskTypeGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">TaskTypeGroup to delete</param>
        void DeleteTaskTypeGroup(OpTaskTypeGroup toBeDeleted);

        /// <summary>
        /// Clears the TaskTypeGroup object cache
        /// </summary>
        void ClearTaskTypeGroupCache();
        #endregion

        #region TransmissionStatus

        /// <summary>
        /// Indicates whether the TransmissionStatus is cached.
        /// </summary>
        bool TransmissionStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionStatus objects
        /// </summary>
        List<OpTransmissionStatus> GetAllTransmissionStatus();


        /// <summary>
        /// Gets TransmissionStatus by id
        /// </summary>
        /// <param name="Id">TransmissionStatus Id</param>
        /// <returns>TransmissionStatus object</returns>
        OpTransmissionStatus GetTransmissionStatus(int Id);

        /// <summary>
        /// Gets TransmissionStatus by id
        /// </summary>
        /// <param name="Ids">TransmissionStatus Ids</param>
        /// <returns>TransmissionStatus list</returns>
        List<OpTransmissionStatus> GetTransmissionStatus(int[] Ids);

        /// <summary>
        /// Gets TransmissionStatus by id
        /// </summary>
        /// <param name="Id">TransmissionStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionStatus object</returns>
        OpTransmissionStatus GetTransmissionStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionStatus by id
        /// </summary>
        /// <param name="Ids">TransmissionStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionStatus list</returns>
        List<OpTransmissionStatus> GetTransmissionStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TransmissionStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionStatus to save</param>
        /// <returns>TransmissionStatus Id</returns>
        int SaveTransmissionStatus(OpTransmissionStatus toBeSaved);

        /// <summary>
        /// Deletes the TransmissionStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionStatus to delete</param>
        void DeleteTransmissionStatus(OpTransmissionStatus toBeDeleted);

        /// <summary>
        /// Clears the TransmissionStatus object cache
        /// </summary>
        void ClearTransmissionStatusCache();
        #endregion

        #region TransmissionType

        /// <summary>
        /// Indicates whether the TransmissionType is cached.
        /// </summary>
        bool TransmissionTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionType objects
        /// </summary>
        List<OpTransmissionType> GetAllTransmissionType();


        /// <summary>
        /// Gets TransmissionType by id
        /// </summary>
        /// <param name="Id">TransmissionType Id</param>
        /// <returns>TransmissionType object</returns>
        OpTransmissionType GetTransmissionType(int Id);

        /// <summary>
        /// Gets TransmissionType by id
        /// </summary>
        /// <param name="Ids">TransmissionType Ids</param>
        /// <returns>TransmissionType list</returns>
        List<OpTransmissionType> GetTransmissionType(int[] Ids);

        /// <summary>
        /// Gets TransmissionType by id
        /// </summary>
        /// <param name="Id">TransmissionType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionType object</returns>
        OpTransmissionType GetTransmissionType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionType by id
        /// </summary>
        /// <param name="Ids">TransmissionType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionType list</returns>
        List<OpTransmissionType> GetTransmissionType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TransmissionType object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionType to save</param>
        /// <returns>TransmissionType Id</returns>
        int SaveTransmissionType(OpTransmissionType toBeSaved);

        /// <summary>
        /// Deletes the TransmissionType object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionType to delete</param>
        void DeleteTransmissionType(OpTransmissionType toBeDeleted);

        /// <summary>
        /// Clears the TransmissionType object cache
        /// </summary>
        void ClearTransmissionTypeCache();
        #endregion

        #region UniqueType

        /// <summary>
        /// Indicates whether the UniqueType is cached.
        /// </summary>
        bool UniqueTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all UniqueType objects
        /// </summary>
        List<OpUniqueType> GetAllUniqueType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets UniqueType by id
        /// </summary>
        /// <param name="Id">UniqueType Id</param>
        /// <returns>UniqueType object</returns>
        OpUniqueType GetUniqueType(int Id);

        /// <summary>
        /// Gets UniqueType by id
        /// </summary>
        /// <param name="Ids">UniqueType Ids</param>
        /// <returns>UniqueType list</returns>
        List<OpUniqueType> GetUniqueType(int[] Ids);

        /// <summary>
        /// Gets UniqueType by id
        /// </summary>
        /// <param name="Id">UniqueType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>UniqueType object</returns>
        OpUniqueType GetUniqueType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets UniqueType by id
        /// </summary>
        /// <param name="Ids">UniqueType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>UniqueType list</returns>
        List<OpUniqueType> GetUniqueType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the UniqueType object to the database
        /// </summary>
        /// <param name="toBeSaved">UniqueType to save</param>
        /// <returns>UniqueType Id</returns>
        int SaveUniqueType(OpUniqueType toBeSaved);

        /// <summary>
        /// Deletes the UniqueType object from the database
        /// </summary>
        /// <param name="toBeDeleted">UniqueType to delete</param>
        void DeleteUniqueType(OpUniqueType toBeDeleted);

        /// <summary>
        /// Clears the UniqueType object cache
        /// </summary>
        void ClearUniqueTypeCache();
        #endregion

        #region Unit

        /// <summary>
        /// Indicates whether the Unit is cached.
        /// </summary>
        bool UnitCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Unit objects
        /// </summary>
        List<OpUnit> GetAllUnit(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Unit by id
        /// </summary>
        /// <param name="Id">Unit Id</param>
        /// <returns>Unit object</returns>
        OpUnit GetUnit(int Id);

        /// <summary>
        /// Gets Unit by id
        /// </summary>
        /// <param name="Ids">Unit Ids</param>
        /// <returns>Unit list</returns>
        List<OpUnit> GetUnit(int[] Ids);

        /// <summary>
        /// Gets Unit by id
        /// </summary>
        /// <param name="Id">Unit Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Unit object</returns>
        OpUnit GetUnit(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Unit by id
        /// </summary>
        /// <param name="Ids">Unit Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Unit list</returns>
        List<OpUnit> GetUnit(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Unit object to the database
        /// </summary>
        /// <param name="toBeSaved">Unit to save</param>
        /// <returns>Unit Id</returns>
        int SaveUnit(OpUnit toBeSaved);

        /// <summary>
        /// Deletes the Unit object from the database
        /// </summary>
        /// <param name="toBeDeleted">Unit to delete</param>
        void DeleteUnit(OpUnit toBeDeleted);

        /// <summary>
        /// Clears the Unit object cache
        /// </summary>
        void ClearUnitCache();
        #endregion

        #region Version

        /// <summary>
        /// Indicates whether the Version is cached.
        /// </summary>
        bool VersionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Version objects
        /// </summary>
        List<OpVersion> GetAllVersion();


        /// <summary>
        /// Gets Version by id
        /// </summary>
        /// <param name="Id">Version Id</param>
        /// <returns>Version object</returns>
        OpVersion GetVersion(long Id);

        /// <summary>
        /// Gets Version by id
        /// </summary>
        /// <param name="Ids">Version Ids</param>
        /// <returns>Version list</returns>
        List<OpVersion> GetVersion(long[] Ids);

        /// <summary>
        /// Gets Version by id
        /// </summary>
        /// <param name="Id">Version Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Version object</returns>
        OpVersion GetVersion(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Version by id
        /// </summary>
        /// <param name="Ids">Version Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Version list</returns>
        List<OpVersion> GetVersion(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Version object to the database
        /// </summary>
        /// <param name="toBeSaved">Version to save</param>
        /// <returns>Version Id</returns>
        long SaveVersion(OpVersion toBeSaved);

        /// <summary>
        /// Deletes the Version object from the database
        /// </summary>
        /// <param name="toBeDeleted">Version to delete</param>
        void DeleteVersion(OpVersion toBeDeleted);

        /// <summary>
        /// Clears the Version object cache
        /// </summary>
        void ClearVersionCache();
        #endregion

        #region VersionData

        /// <summary>
        /// Indicates whether the VersionData is cached.
        /// </summary>
        bool VersionDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionData objects
        /// </summary>
        List<OpVersionData> GetAllVersionData();


        /// <summary>
        /// Gets VersionData by id
        /// </summary>
        /// <param name="Id">VersionData Id</param>
        /// <returns>VersionData object</returns>
        OpVersionData GetVersionData(long Id);

        /// <summary>
        /// Gets VersionData by id
        /// </summary>
        /// <param name="Ids">VersionData Ids</param>
        /// <returns>VersionData list</returns>
        List<OpVersionData> GetVersionData(long[] Ids);

        /// <summary>
        /// Gets VersionData by id
        /// </summary>
        /// <param name="Id">VersionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionData object</returns>
        OpVersionData GetVersionData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionData by id
        /// </summary>
        /// <param name="Ids">VersionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionData list</returns>
        List<OpVersionData> GetVersionData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionData object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionData to save</param>
        /// <returns>VersionData Id</returns>
        long SaveVersionData(OpVersionData toBeSaved);

        /// <summary>
        /// Deletes the VersionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionData to delete</param>
        void DeleteVersionData(OpVersionData toBeDeleted);

        /// <summary>
        /// Clears the VersionData object cache
        /// </summary>
        void ClearVersionDataCache();
        #endregion

        #region VersionElement

        /// <summary>
        /// Indicates whether the VersionElement is cached.
        /// </summary>
        bool VersionElementCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionElement objects
        /// </summary>
        List<OpVersionElement> GetAllVersionElement();


        /// <summary>
        /// Gets VersionElement by id
        /// </summary>
        /// <param name="Id">VersionElement Id</param>
        /// <returns>VersionElement object</returns>
        OpVersionElement GetVersionElement(int Id);

        /// <summary>
        /// Gets VersionElement by id
        /// </summary>
        /// <param name="Ids">VersionElement Ids</param>
        /// <returns>VersionElement list</returns>
        List<OpVersionElement> GetVersionElement(int[] Ids);

        /// <summary>
        /// Gets VersionElement by id
        /// </summary>
        /// <param name="Id">VersionElement Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElement object</returns>
        OpVersionElement GetVersionElement(int Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionElement by id
        /// </summary>
        /// <param name="Ids">VersionElement Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElement list</returns>
        List<OpVersionElement> GetVersionElement(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionElement object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionElement to save</param>
        /// <returns>VersionElement Id</returns>
        int SaveVersionElement(OpVersionElement toBeSaved);

        /// <summary>
        /// Deletes the VersionElement object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionElement to delete</param>
        void DeleteVersionElement(OpVersionElement toBeDeleted);

        /// <summary>
        /// Clears the VersionElement object cache
        /// </summary>
        void ClearVersionElementCache();
        #endregion

        #region VersionElementData

        /// <summary>
        /// Indicates whether the VersionElementData is cached.
        /// </summary>
        bool VersionElementDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionElementData objects
        /// </summary>
        List<OpVersionElementData> GetAllVersionElementData();


        /// <summary>
        /// Gets VersionElementData by id
        /// </summary>
        /// <param name="Id">VersionElementData Id</param>
        /// <returns>VersionElementData object</returns>
        OpVersionElementData GetVersionElementData(long Id);

        /// <summary>
        /// Gets VersionElementData by id
        /// </summary>
        /// <param name="Ids">VersionElementData Ids</param>
        /// <returns>VersionElementData list</returns>
        List<OpVersionElementData> GetVersionElementData(long[] Ids);

        /// <summary>
        /// Gets VersionElementData by id
        /// </summary>
        /// <param name="Id">VersionElementData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementData object</returns>
        OpVersionElementData GetVersionElementData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionElementData by id
        /// </summary>
        /// <param name="Ids">VersionElementData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementData list</returns>
        List<OpVersionElementData> GetVersionElementData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionElementData object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionElementData to save</param>
        /// <returns>VersionElementData Id</returns>
        long SaveVersionElementData(OpVersionElementData toBeSaved);

        /// <summary>
        /// Deletes the VersionElementData object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionElementData to delete</param>
        void DeleteVersionElementData(OpVersionElementData toBeDeleted);

        /// <summary>
        /// Clears the VersionElementData object cache
        /// </summary>
        void ClearVersionElementDataCache();
        #endregion

        #region VersionElementHierarchy

        /// <summary>
        /// Indicates whether the VersionElementHierarchy is cached.
        /// </summary>
        bool VersionElementHierarchyCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionElementHierarchy objects
        /// </summary>
        List<OpVersionElementHierarchy> GetAllVersionElementHierarchy();


        /// <summary>
        /// Gets VersionElementHierarchy by id
        /// </summary>
        /// <param name="Id">VersionElementHierarchy Id</param>
        /// <returns>VersionElementHierarchy object</returns>
        OpVersionElementHierarchy GetVersionElementHierarchy(long Id);

        /// <summary>
        /// Gets VersionElementHierarchy by id
        /// </summary>
        /// <param name="Ids">VersionElementHierarchy Ids</param>
        /// <returns>VersionElementHierarchy list</returns>
        List<OpVersionElementHierarchy> GetVersionElementHierarchy(long[] Ids);

        /// <summary>
        /// Gets VersionElementHierarchy by id
        /// </summary>
        /// <param name="Id">VersionElementHierarchy Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementHierarchy object</returns>
        OpVersionElementHierarchy GetVersionElementHierarchy(long Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionElementHierarchy by id
        /// </summary>
        /// <param name="Ids">VersionElementHierarchy Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementHierarchy list</returns>
        List<OpVersionElementHierarchy> GetVersionElementHierarchy(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionElementHierarchy object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionElementHierarchy to save</param>
        /// <returns>VersionElementHierarchy Id</returns>
        long SaveVersionElementHierarchy(OpVersionElementHierarchy toBeSaved);

        /// <summary>
        /// Deletes the VersionElementHierarchy object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionElementHierarchy to delete</param>
        void DeleteVersionElementHierarchy(OpVersionElementHierarchy toBeDeleted);

        /// <summary>
        /// Clears the VersionElementHierarchy object cache
        /// </summary>
        void ClearVersionElementHierarchyCache();
        #endregion

        #region VersionElementHistory

        /// <summary>
        /// Indicates whether the VersionElementHistory is cached.
        /// </summary>
        bool VersionElementHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionElementHistory objects
        /// </summary>
        List<OpVersionElementHistory> GetAllVersionElementHistory();


        /// <summary>
        /// Gets VersionElementHistory by id
        /// </summary>
        /// <param name="Id">VersionElementHistory Id</param>
        /// <returns>VersionElementHistory object</returns>
        OpVersionElementHistory GetVersionElementHistory(long Id);

        /// <summary>
        /// Gets VersionElementHistory by id
        /// </summary>
        /// <param name="Ids">VersionElementHistory Ids</param>
        /// <returns>VersionElementHistory list</returns>
        List<OpVersionElementHistory> GetVersionElementHistory(long[] Ids);

        /// <summary>
        /// Gets VersionElementHistory by id
        /// </summary>
        /// <param name="Id">VersionElementHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementHistory object</returns>
        OpVersionElementHistory GetVersionElementHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionElementHistory by id
        /// </summary>
        /// <param name="Ids">VersionElementHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementHistory list</returns>
        List<OpVersionElementHistory> GetVersionElementHistory(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionElementHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionElementHistory to save</param>
        /// <returns>VersionElementHistory Id</returns>
        long SaveVersionElementHistory(OpVersionElementHistory toBeSaved);

        /// <summary>
        /// Deletes the VersionElementHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionElementHistory to delete</param>
        void DeleteVersionElementHistory(OpVersionElementHistory toBeDeleted);

        /// <summary>
        /// Clears the VersionElementHistory object cache
        /// </summary>
        void ClearVersionElementHistoryCache();
        #endregion

        #region VersionElementType

        /// <summary>
        /// Indicates whether the VersionElementType is cached.
        /// </summary>
        bool VersionElementTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionElementType objects
        /// </summary>
        List<OpVersionElementType> GetAllVersionElementType();


        /// <summary>
        /// Gets VersionElementType by id
        /// </summary>
        /// <param name="Id">VersionElementType Id</param>
        /// <returns>VersionElementType object</returns>
        OpVersionElementType GetVersionElementType(int Id);

        /// <summary>
        /// Gets VersionElementType by id
        /// </summary>
        /// <param name="Ids">VersionElementType Ids</param>
        /// <returns>VersionElementType list</returns>
        List<OpVersionElementType> GetVersionElementType(int[] Ids);

        /// <summary>
        /// Gets VersionElementType by id
        /// </summary>
        /// <param name="Id">VersionElementType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementType object</returns>
        OpVersionElementType GetVersionElementType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionElementType by id
        /// </summary>
        /// <param name="Ids">VersionElementType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementType list</returns>
        List<OpVersionElementType> GetVersionElementType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionElementType object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionElementType to save</param>
        /// <returns>VersionElementType Id</returns>
        int SaveVersionElementType(OpVersionElementType toBeSaved);

        /// <summary>
        /// Deletes the VersionElementType object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionElementType to delete</param>
        void DeleteVersionElementType(OpVersionElementType toBeDeleted);

        /// <summary>
        /// Clears the VersionElementType object cache
        /// </summary>
        void ClearVersionElementTypeCache();
        #endregion

        #region VersionElementTypeData
        List<OpVersionElementTypeData> GetAllVersionElementTypeData();

        /// <summary>
        /// Gets VersionElementTypeData by id
        /// </summary>
        /// <param name="Id">VersionElementTypeData Id</param>
        /// <returns>VersionElementTypeData object</returns>
        OpVersionElementTypeData GetVersionElementTypeData(long Id);

        /// <summary>
        /// Gets VersionElementTypeData by id
        /// </summary>
        /// <param name="Ids">VersionElementTypeData Ids</param>
        /// <returns>VersionElementTypeData list</returns>
        List<OpVersionElementTypeData> GetVersionElementTypeData(long[] Ids);

        /// <summary>
        /// Gets VersionElementTypeData by id
        /// </summary>
        /// <param name="Id">VersionElementTypeData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementTypeData object</returns>
        OpVersionElementTypeData GetVersionElementTypeData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionElementTypeData by id
        /// </summary>
        /// <param name="Ids">VersionElementTypeData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementTypeData list</returns>
        List<OpVersionElementTypeData> GetVersionElementTypeData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionElementTypeData object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionElementTypeData to save</param>
        /// <returns>VersionElementTypeData Id</returns>
        long SaveVersionElementTypeData(OpVersionElementTypeData toBeSaved);

        /// <summary>
        /// Deletes the VersionElementTypeData object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionElementTypeData to delete</param>
        void DeleteVersionElementTypeData(OpVersionElementTypeData toBeDeleted);

        /// <summary>
        /// Clears the VersionElementTypeData object cache
        /// </summary>
        void ClearVersionElementTypeDataCache();
        #endregion
        
        #region VersionElementTypeHierarchy

        /// <summary>
        /// Indicates whether the VersionElementTypeHierarchy is cached.
        /// </summary>
        bool VersionElementTypeHierarchyCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionElementTypeHierarchy objects
        /// </summary>
        List<OpVersionElementTypeHierarchy> GetAllVersionElementTypeHierarchy();


        /// <summary>
        /// Gets VersionElementTypeHierarchy by id
        /// </summary>
        /// <param name="Id">VersionElementTypeHierarchy Id</param>
        /// <returns>VersionElementTypeHierarchy object</returns>
        OpVersionElementTypeHierarchy GetVersionElementTypeHierarchy(int Id);

        /// <summary>
        /// Gets VersionElementTypeHierarchy by id
        /// </summary>
        /// <param name="Ids">VersionElementTypeHierarchy Ids</param>
        /// <returns>VersionElementTypeHierarchy list</returns>
        List<OpVersionElementTypeHierarchy> GetVersionElementTypeHierarchy(int[] Ids);

        /// <summary>
        /// Gets VersionElementTypeHierarchy by id
        /// </summary>
        /// <param name="Id">VersionElementTypeHierarchy Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementTypeHierarchy object</returns>
        OpVersionElementTypeHierarchy GetVersionElementTypeHierarchy(int Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionElementTypeHierarchy by id
        /// </summary>
        /// <param name="Ids">VersionElementTypeHierarchy Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionElementTypeHierarchy list</returns>
        List<OpVersionElementTypeHierarchy> GetVersionElementTypeHierarchy(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionElementTypeHierarchy object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionElementTypeHierarchy to save</param>
        /// <returns>VersionElementTypeHierarchy Id</returns>
        int SaveVersionElementTypeHierarchy(OpVersionElementTypeHierarchy toBeSaved);

        /// <summary>
        /// Deletes the VersionElementTypeHierarchy object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionElementTypeHierarchy to delete</param>
        void DeleteVersionElementTypeHierarchy(OpVersionElementTypeHierarchy toBeDeleted);

        /// <summary>
        /// Clears the VersionElementTypeHierarchy object cache
        /// </summary>
        void ClearVersionElementTypeHierarchyCache();
        #endregion

        #region VersionHistory

        /// <summary>
        /// Indicates whether the VersionHistory is cached.
        /// </summary>
        bool VersionHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionHistory objects
        /// </summary>
        List<OpVersionHistory> GetAllVersionHistory();


        /// <summary>
        /// Gets VersionHistory by id
        /// </summary>
        /// <param name="Id">VersionHistory Id</param>
        /// <returns>VersionHistory object</returns>
        OpVersionHistory GetVersionHistory(long Id);

        /// <summary>
        /// Gets VersionHistory by id
        /// </summary>
        /// <param name="Ids">VersionHistory Ids</param>
        /// <returns>VersionHistory list</returns>
        List<OpVersionHistory> GetVersionHistory(long[] Ids);

        /// <summary>
        /// Gets VersionHistory by id
        /// </summary>
        /// <param name="Id">VersionHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionHistory object</returns>
        OpVersionHistory GetVersionHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionHistory by id
        /// </summary>
        /// <param name="Ids">VersionHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionHistory list</returns>
        List<OpVersionHistory> GetVersionHistory(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionHistory to save</param>
        /// <returns>VersionHistory Id</returns>
        long SaveVersionHistory(OpVersionHistory toBeSaved);

        /// <summary>
        /// Deletes the VersionHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionHistory to delete</param>
        void DeleteVersionHistory(OpVersionHistory toBeDeleted);

        /// <summary>
        /// Clears the VersionHistory object cache
        /// </summary>
        void ClearVersionHistoryCache();
        #endregion

        #region VersionNbrFormat

        /// <summary>
        /// Indicates whether the VersionNbrFormat is cached.
        /// </summary>
        bool VersionNbrFormatCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionNbrFormat objects
        /// </summary>
        List<OpVersionNbrFormat> GetAllVersionNbrFormat();


        /// <summary>
        /// Gets VersionNbrFormat by id
        /// </summary>
        /// <param name="Id">VersionNbrFormat Id</param>
        /// <returns>VersionNbrFormat object</returns>
        OpVersionNbrFormat GetVersionNbrFormat(int Id);

        /// <summary>
        /// Gets VersionNbrFormat by id
        /// </summary>
        /// <param name="Ids">VersionNbrFormat Ids</param>
        /// <returns>VersionNbrFormat list</returns>
        List<OpVersionNbrFormat> GetVersionNbrFormat(int[] Ids);

        /// <summary>
        /// Gets VersionNbrFormat by id
        /// </summary>
        /// <param name="Id">VersionNbrFormat Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionNbrFormat object</returns>
        OpVersionNbrFormat GetVersionNbrFormat(int Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionNbrFormat by id
        /// </summary>
        /// <param name="Ids">VersionNbrFormat Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionNbrFormat list</returns>
        List<OpVersionNbrFormat> GetVersionNbrFormat(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionNbrFormat object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionNbrFormat to save</param>
        /// <returns>VersionNbrFormat Id</returns>
        int SaveVersionNbrFormat(OpVersionNbrFormat toBeSaved);

        /// <summary>
        /// Deletes the VersionNbrFormat object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionNbrFormat to delete</param>
        void DeleteVersionNbrFormat(OpVersionNbrFormat toBeDeleted);

        /// <summary>
        /// Clears the VersionNbrFormat object cache
        /// </summary>
        void ClearVersionNbrFormatCache();
        #endregion

        #region VersionRequirement

        /// <summary>
        /// Indicates whether the VersionRequirement is cached.
        /// </summary>
        bool VersionRequirementCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionRequirement objects
        /// </summary>
        List<OpVersionRequirement> GetAllVersionRequirement();


        /// <summary>
        /// Gets VersionRequirement by id
        /// </summary>
        /// <param name="Id">VersionRequirement Id</param>
        /// <returns>VersionRequirement object</returns>
        OpVersionRequirement GetVersionRequirement(long Id);

        /// <summary>
        /// Gets VersionRequirement by id
        /// </summary>
        /// <param name="Ids">VersionRequirement Ids</param>
        /// <returns>VersionRequirement list</returns>
        List<OpVersionRequirement> GetVersionRequirement(long[] Ids);

        /// <summary>
        /// Gets VersionRequirement by id
        /// </summary>
        /// <param name="Id">VersionRequirement Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionRequirement object</returns>
        OpVersionRequirement GetVersionRequirement(long Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionRequirement by id
        /// </summary>
        /// <param name="Ids">VersionRequirement Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionRequirement list</returns>
        List<OpVersionRequirement> GetVersionRequirement(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionRequirement object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionRequirement to save</param>
        /// <returns>VersionRequirement Id</returns>
        long SaveVersionRequirement(OpVersionRequirement toBeSaved);

        /// <summary>
        /// Deletes the VersionRequirement object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionRequirement to delete</param>
        void DeleteVersionRequirement(OpVersionRequirement toBeDeleted);

        /// <summary>
        /// Clears the VersionRequirement object cache
        /// </summary>
        void ClearVersionRequirementCache();
        #endregion

        #region VersionState

        /// <summary>
        /// Indicates whether the VersionState is cached.
        /// </summary>
        bool VersionStateCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionState objects
        /// </summary>
        List<OpVersionState> GetAllVersionState();


        /// <summary>
        /// Gets VersionState by id
        /// </summary>
        /// <param name="Id">VersionState Id</param>
        /// <returns>VersionState object</returns>
        OpVersionState GetVersionState(int Id);

        /// <summary>
        /// Gets VersionState by id
        /// </summary>
        /// <param name="Ids">VersionState Ids</param>
        /// <returns>VersionState list</returns>
        List<OpVersionState> GetVersionState(int[] Ids);

        /// <summary>
        /// Gets VersionState by id
        /// </summary>
        /// <param name="Id">VersionState Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionState object</returns>
        OpVersionState GetVersionState(int Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionState by id
        /// </summary>
        /// <param name="Ids">VersionState Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionState list</returns>
        List<OpVersionState> GetVersionState(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionState object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionState to save</param>
        /// <returns>VersionState Id</returns>
        int SaveVersionState(OpVersionState toBeSaved);

        /// <summary>
        /// Deletes the VersionState object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionState to delete</param>
        void DeleteVersionState(OpVersionState toBeDeleted);

        /// <summary>
        /// Clears the VersionState object cache
        /// </summary>
        void ClearVersionStateCache();
        #endregion

        #region VersionType

        /// <summary>
        /// Indicates whether the VersionType is cached.
        /// </summary>
        bool VersionTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all VersionType objects
        /// </summary>
        List<OpVersionType> GetAllVersionType();


        /// <summary>
        /// Gets VersionType by id
        /// </summary>
        /// <param name="Id">VersionType Id</param>
        /// <returns>VersionType object</returns>
        OpVersionType GetVersionType(int Id);

        /// <summary>
        /// Gets VersionType by id
        /// </summary>
        /// <param name="Ids">VersionType Ids</param>
        /// <returns>VersionType list</returns>
        List<OpVersionType> GetVersionType(int[] Ids);

        /// <summary>
        /// Gets VersionType by id
        /// </summary>
        /// <param name="Id">VersionType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionType object</returns>
        OpVersionType GetVersionType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets VersionType by id
        /// </summary>
        /// <param name="Ids">VersionType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>VersionType list</returns>
        List<OpVersionType> GetVersionType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the VersionType object to the database
        /// </summary>
        /// <param name="toBeSaved">VersionType to save</param>
        /// <returns>VersionType Id</returns>
        int SaveVersionType(OpVersionType toBeSaved);

        /// <summary>
        /// Deletes the VersionType object from the database
        /// </summary>
        /// <param name="toBeDeleted">VersionType to delete</param>
        void DeleteVersionType(OpVersionType toBeDeleted);

        /// <summary>
        /// Clears the VersionType object cache
        /// </summary>
        void ClearVersionTypeCache();
        #endregion

        #region View

        /// <summary>
        /// Indicates whether the View is cached.
        /// </summary>
        bool ViewCacheEnabled { get; set; }

        /// <summary>
        /// Gets all View objects
        /// </summary>
        List<OpView> GetAllView();


        /// <summary>
        /// Gets View by id
        /// </summary>
        /// <param name="Id">View Id</param>
        /// <returns>View object</returns>
        OpView GetView(int Id);

        /// <summary>
        /// Gets View by id
        /// </summary>
        /// <param name="Ids">View Ids</param>
        /// <returns>View list</returns>
        List<OpView> GetView(int[] Ids);

        /// <summary>
        /// Gets View by id
        /// </summary>
        /// <param name="Id">View Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>View object</returns>
        OpView GetView(int Id, bool queryDatabase);

        /// <summary>
        /// Gets View by id
        /// </summary>
        /// <param name="Ids">View Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>View list</returns>
        List<OpView> GetView(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the View object to the database
        /// </summary>
        /// <param name="toBeSaved">View to save</param>
        /// <returns>View Id</returns>
        int SaveView(OpView toBeSaved);

        /// <summary>
        /// Deletes the View object from the database
        /// </summary>
        /// <param name="toBeDeleted">View to delete</param>
        void DeleteView(OpView toBeDeleted);

        /// <summary>
        /// Clears the View object cache
        /// </summary>
        void ClearViewCache();
        #endregion
        #region ViewData
        /// <summary>
        /// Indicates whether the ViewData is cached.
        /// </summary>
        bool ViewDataCacheEnabled { get; set; }


        /// <summary>
        /// Gets ViewData by id
        /// </summary>
        /// <param name="Id">ViewData Id</param>
        /// <returns>ViewData object</returns>
        OpViewData GetViewData(long Id);

        /// <summary>
        /// Gets ViewData by id
        /// </summary>
        /// <param name="Ids">ViewData Ids</param>
        /// <returns>ViewData list</returns>
        List<OpViewData> GetViewData(long[] Ids);

        /// <summary>
        /// Gets ViewData by id
        /// </summary>
        /// <param name="Id">ViewData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewData object</returns>
        OpViewData GetViewData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ViewData by id
        /// </summary>
        /// <param name="Ids">ViewData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewData list</returns>
        List<OpViewData> GetViewData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ViewData object to the database
        /// </summary>
        /// <param name="toBeSaved">ViewData to save</param>
        /// <returns>ViewData Id</returns>
        long SaveViewData(OpViewData toBeSaved);

        /// <summary>
        /// Deletes the ViewData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ViewData to delete</param>
        void DeleteViewData(OpViewData toBeDeleted);

        /// <summary>
        /// Clears the ViewData object cache
        /// </summary>
        void ClearViewDataCache();
        #endregion
        #region ViewColumn
        /// <summary>
        /// Indicates whether the ViewColumn is cached.
        /// </summary>
        bool ViewColumnCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ViewColumn objects
        /// </summary>
        List<OpViewColumn> GetAllViewColumn();


        /// <summary>
        /// Gets ViewColumn by id
        /// </summary>
        /// <param name="Id">ViewColumn Id</param>
        /// <returns>ViewColumn object</returns>
        OpViewColumn GetViewColumn(int Id);

        /// <summary>
        /// Gets ViewColumn by id
        /// </summary>
        /// <param name="Ids">ViewColumn Ids</param>
        /// <returns>ViewColumn list</returns>
        List<OpViewColumn> GetViewColumn(int[] Ids);

        /// <summary>
        /// Gets ViewColumn by id
        /// </summary>
        /// <param name="Id">ViewColumn Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumn object</returns>
        OpViewColumn GetViewColumn(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ViewColumn by id
        /// </summary>
        /// <param name="Ids">ViewColumn Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumn list</returns>
        List<OpViewColumn> GetViewColumn(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ViewColumn object to the database
        /// </summary>
        /// <param name="toBeSaved">ViewColumn to save</param>
        /// <returns>ViewColumn Id</returns>
        int SaveViewColumn(OpViewColumn toBeSaved);

        /// <summary>
        /// Deletes the ViewColumn object from the database
        /// </summary>
        /// <param name="toBeDeleted">ViewColumn to delete</param>
        void DeleteViewColumn(OpViewColumn toBeDeleted);

        /// <summary>
        /// Clears the ViewColumn object cache
        /// </summary>
        void ClearViewColumnCache();
        #endregion
        #region ViewColumnData
        /// <summary>
        /// Indicates whether the ViewColumnData is cached.
        /// </summary>
        bool ViewColumnDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ViewColumnData objects
        /// </summary>
        List<OpViewColumnData> GetAllViewColumnData();


        /// <summary>
        /// Gets ViewColumnData by id
        /// </summary>
        /// <param name="Id">ViewColumnData Id</param>
        /// <returns>ViewColumnData object</returns>
        OpViewColumnData GetViewColumnData(long Id);

        /// <summary>
        /// Gets ViewColumnData by id
        /// </summary>
        /// <param name="Ids">ViewColumnData Ids</param>
        /// <returns>ViewColumnData list</returns>
        List<OpViewColumnData> GetViewColumnData(long[] Ids);

        /// <summary>
        /// Gets ViewColumnData by id
        /// </summary>
        /// <param name="Id">ViewColumnData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumnData object</returns>
        OpViewColumnData GetViewColumnData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ViewColumnData by id
        /// </summary>
        /// <param name="Ids">ViewColumnData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumnData list</returns>
        List<OpViewColumnData> GetViewColumnData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ViewColumnData object to the database
        /// </summary>
        /// <param name="toBeSaved">ViewColumnData to save</param>
        /// <returns>ViewColumnData Id</returns>
        long SaveViewColumnData(OpViewColumnData toBeSaved);

        /// <summary>
        /// Deletes the ViewColumnData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ViewColumnData to delete</param>
        void DeleteViewColumnData(OpViewColumnData toBeDeleted);

        /// <summary>
        /// Clears the ViewColumnData object cache
        /// </summary>
        void ClearViewColumnDataCache();
        #endregion
        #region ViewColumnSource
        /// <summary>
        /// Indicates whether the ViewColumnSource is cached.
        /// </summary>
        bool ViewColumnSourceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ViewColumnSource objects
        /// </summary>
        List<OpViewColumnSource> GetAllViewColumnSource();


        /// <summary>
        /// Gets ViewColumnSource by id
        /// </summary>
        /// <param name="Id">ViewColumnSource Id</param>
        /// <returns>ViewColumnSource object</returns>
        OpViewColumnSource GetViewColumnSource(int Id);

        /// <summary>
        /// Gets ViewColumnSource by id
        /// </summary>
        /// <param name="Ids">ViewColumnSource Ids</param>
        /// <returns>ViewColumnSource list</returns>
        List<OpViewColumnSource> GetViewColumnSource(int[] Ids);

        /// <summary>
        /// Gets ViewColumnSource by id
        /// </summary>
        /// <param name="Id">ViewColumnSource Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumnSource object</returns>
        OpViewColumnSource GetViewColumnSource(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ViewColumnSource by id
        /// </summary>
        /// <param name="Ids">ViewColumnSource Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumnSource list</returns>
        List<OpViewColumnSource> GetViewColumnSource(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ViewColumnSource object to the database
        /// </summary>
        /// <param name="toBeSaved">ViewColumnSource to save</param>
        /// <returns>ViewColumnSource Id</returns>
        int SaveViewColumnSource(OpViewColumnSource toBeSaved);

        /// <summary>
        /// Deletes the ViewColumnSource object from the database
        /// </summary>
        /// <param name="toBeDeleted">ViewColumnSource to delete</param>
        void DeleteViewColumnSource(OpViewColumnSource toBeDeleted);

        /// <summary>
        /// Clears the ViewColumnSource object cache
        /// </summary>
        void ClearViewColumnSourceCache();
        #endregion
        #region ViewColumnType
        /// <summary>
        /// Indicates whether the ViewColumnType is cached.
        /// </summary>
        bool ViewColumnTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ViewColumnType objects
        /// </summary>
        List<OpViewColumnType> GetAllViewColumnType();


        /// <summary>
        /// Gets ViewColumnType by id
        /// </summary>
        /// <param name="Id">ViewColumnType Id</param>
        /// <returns>ViewColumnType object</returns>
        OpViewColumnType GetViewColumnType(int Id);

        /// <summary>
        /// Gets ViewColumnType by id
        /// </summary>
        /// <param name="Ids">ViewColumnType Ids</param>
        /// <returns>ViewColumnType list</returns>
        List<OpViewColumnType> GetViewColumnType(int[] Ids);

        /// <summary>
        /// Gets ViewColumnType by id
        /// </summary>
        /// <param name="Id">ViewColumnType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumnType object</returns>
        OpViewColumnType GetViewColumnType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ViewColumnType by id
        /// </summary>
        /// <param name="Ids">ViewColumnType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ViewColumnType list</returns>
        List<OpViewColumnType> GetViewColumnType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ViewColumnType object to the database
        /// </summary>
        /// <param name="toBeSaved">ViewColumnType to save</param>
        /// <returns>ViewColumnType Id</returns>
        int SaveViewColumnType(OpViewColumnType toBeSaved);

        /// <summary>
        /// Deletes the ViewColumnType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ViewColumnType to delete</param>
        void DeleteViewColumnType(OpViewColumnType toBeDeleted);

        /// <summary>
        /// Clears the ViewColumnType object cache
        /// </summary>
        void ClearViewColumnTypeCache();
        #endregion

        #region WarrantyContract

        /// <summary>
        /// Indicates whether the WarrantyContract is cached.
        /// </summary>
        bool WarrantyContractCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WarrantyContract objects
        /// </summary>
        List<OpWarrantyContract> GetAllWarrantyContract(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets WarrantyContract by id
        /// </summary>
        /// <param name="Id">WarrantyContract Id</param>
        /// <returns>WarrantyContract object</returns>
        OpWarrantyContract GetWarrantyContract(int Id);

        /// <summary>
        /// Gets WarrantyContract by id
        /// </summary>
        /// <param name="Ids">WarrantyContract Ids</param>
        /// <returns>WarrantyContract list</returns>
        List<OpWarrantyContract> GetWarrantyContract(int[] Ids);

        /// <summary>
        /// Gets WarrantyContract by id
        /// </summary>
        /// <param name="Id">WarrantyContract Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WarrantyContract object</returns>
        OpWarrantyContract GetWarrantyContract(int Id, bool queryDatabase);

        /// <summary>
        /// Gets WarrantyContract by id
        /// </summary>
        /// <param name="Ids">WarrantyContract Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WarrantyContract list</returns>
        List<OpWarrantyContract> GetWarrantyContract(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the WarrantyContract object to the database
        /// </summary>
        /// <param name="toBeSaved">WarrantyContract to save</param>
        /// <returns>WarrantyContract Id</returns>
        int SaveWarrantyContract(OpWarrantyContract toBeSaved);

        /// <summary>
        /// Deletes the WarrantyContract object from the database
        /// </summary>
        /// <param name="toBeDeleted">WarrantyContract to delete</param>
        void DeleteWarrantyContract(OpWarrantyContract toBeDeleted);

        /// <summary>
        /// Clears the WarrantyContract object cache
        /// </summary>
        void ClearWarrantyContractCache();
        #endregion

        #region WmbusTransmissionType

        /// <summary>
        /// Indicates whether the WmbusTransmissionType is cached.
        /// </summary>
        bool WmbusTransmissionTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WmbusTransmissionType objects
        /// </summary>
        List<OpWmbusTransmissionType> GetAllWmbusTransmissionType();


        /// <summary>
        /// Gets WmbusTransmissionType by id
        /// </summary>
        /// <param name="Id">WmbusTransmissionType Id</param>
        /// <returns>WmbusTransmissionType object</returns>
        OpWmbusTransmissionType GetWmbusTransmissionType(long Id);

        /// <summary>
        /// Gets WmbusTransmissionType by id
        /// </summary>
        /// <param name="Ids">WmbusTransmissionType Ids</param>
        /// <returns>WmbusTransmissionType list</returns>
        List<OpWmbusTransmissionType> GetWmbusTransmissionType(long[] Ids);

        /// <summary>
        /// Gets WmbusTransmissionType by id
        /// </summary>
        /// <param name="Id">WmbusTransmissionType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WmbusTransmissionType object</returns>
        OpWmbusTransmissionType GetWmbusTransmissionType(long Id, bool queryDatabase);

        /// <summary>
        /// Gets WmbusTransmissionType by id
        /// </summary>
        /// <param name="Ids">WmbusTransmissionType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WmbusTransmissionType list</returns>
        List<OpWmbusTransmissionType> GetWmbusTransmissionType(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the WmbusTransmissionType object to the database
        /// </summary>
        /// <param name="toBeSaved">WmbusTransmissionType to save</param>
        /// <returns>WmbusTransmissionType Id</returns>
        long SaveWmbusTransmissionType(OpWmbusTransmissionType toBeSaved);

        /// <summary>
        /// Deletes the WmbusTransmissionType object from the database
        /// </summary>
        /// <param name="toBeDeleted">WmbusTransmissionType to delete</param>
        void DeleteWmbusTransmissionType(OpWmbusTransmissionType toBeDeleted);

        /// <summary>
        /// Clears the WmbusTransmissionType object cache
        /// </summary>
        void ClearWmbusTransmissionTypeCache();
        #endregion

        #region Work
        /// <summary>
        /// Indicates whether the Work is cached.
        /// </summary>
        bool WorkCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Work objects
        /// </summary>
        List<OpWork> GetAllWork();

        /// <summary>
        /// Gets Work by id
        /// </summary>
        /// <param name="Id">Work Id</param>
        /// <returns>Work object</returns>
        OpWork GetWork(long Id);

        /// <summary>
        /// Gets Work by id
        /// </summary>
        /// <param name="Ids">Work Ids</param>
        /// <returns>Work list</returns>
        List<OpWork> GetWork(long[] Ids);

        /// <summary>
        /// Gets Work by id
        /// </summary>
        /// <param name="Id">Work Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Work object</returns>
        OpWork GetWork(long Id, bool queryDatabase);

        /// <summary>
        /// Deletes the Work object from the database
        /// </summary>
        /// <param name="toBeDeleted">Work to delete</param>
        void DeleteWork(OpWork toBeDeleted);

        /// <summary>
        /// Clears the Work object cache
        /// </summary>
        void ClearWorkCache();
        #endregion
        #region WorkHistory        
        /// <summary>
        /// Indicates whether the WorkHistory is cached.
        /// </summary>
        bool WorkHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WorkHistory objects
        /// </summary>
        List<OpWorkHistory> GetAllWorkHistory();

        /// <summary>
        /// Gets WorkHistory by id
        /// </summary>
        /// <param name="Id">WorkHistory Id</param>
        /// <returns>WorkHistory object</returns>
        OpWorkHistory GetWorkHistory(long Id);

        /// <summary>
        /// Gets WorkHistory by id
        /// </summary>
        /// <param name="Ids">WorkHistory Ids</param>
        /// <returns>WorkHistory list</returns>
        List<OpWorkHistory> GetWorkHistory(long[] Ids);

        /// <summary>
        /// Gets WorkHistory by id
        /// </summary>
        /// <param name="Id">WorkHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkHistory object</returns>
        OpWorkHistory GetWorkHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets WorkHistory by id
        /// </summary>
        /// <param name="Ids">WorkHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkHistory list</returns>
        List<OpWorkHistory> GetWorkHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the WorkHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">WorkHistory to save</param>
        /// <returns>WorkHistory Id</returns>
        long SaveWorkHistory(OpWorkHistory toBeSaved);

        /// <summary>
        /// Deletes the WorkHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">WorkHistory to delete</param>
        void DeleteWorkHistory(OpWorkHistory toBeDeleted);

        /// <summary>
        /// Clears the WorkHistory object cache
        /// </summary>
        void ClearWorkHistoryCache();
        #endregion
        #region WorkHistoryData
        /// <summary>
        /// Indicates whether the WorkHistoryData is cached.
        /// </summary>
        bool WorkHistoryDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WorkHistoryData objects
        /// </summary>
        List<OpWorkHistoryData> GetAllWorkHistoryData();

        /// <summary>
        /// Gets WorkHistoryData by id
        /// </summary>
        /// <param name="Id">WorkHistoryData Id</param>
        /// <returns>WorkHistoryData</returns>
        OpWorkHistoryData GetWorkHistoryData(long Id);

        /// <summary>
        /// Gets WorkHistoryData by id
        /// </summary>
        /// <param name="Ids">WorkHistoryData Ids</param>
        /// <returns>WorkHistoryData list</returns>
        List<OpWorkHistoryData> GetWorkHistoryData(long[] Ids);

        /// <summary>
        /// Gets WorkHistoryData by id
        /// </summary>
        /// <param name="Id">WorkHistoryData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkHistoryData</returns>
        OpWorkHistoryData GetWorkHistoryData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets WorkHistoryData by id
        /// </summary>
        /// <param name="Ids">WorkHistoryData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkHistoryData list</returns>
        List<OpWorkHistoryData> GetWorkHistoryData(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the WorkHistoryData object to the database
        /// </summary>
        /// <param name="toBeSaved">WorkHistoryData to save</param>
        /// <returns>WorkHistoryData Id</returns>
        long SaveWorkHistoryData(OpWorkHistoryData toBeSaved);

        /// <summary>
        /// Deletes the WorkHistoryData object from the database
        /// </summary>
        /// <param name="toBeDeleted">WorkHistoryData to delete</param>
        void DeleteWorkHistoryData(OpWorkHistoryData toBeDeleted);

        /// <summary>
        /// Clears the WorkHistoryData object cache
        /// </summary>
        void ClearWorkHistoryDataCache();
        #endregion

        #region WorkStatus
        /// <summary>
        /// Indicates whether the WorkStatus is cached.
        /// </summary>
        bool WorkStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WorkStatus objects
        /// </summary>
        List<OpWorkStatus> GetAllWorkStatus();

        /// <summary>
        /// Gets WorkStatus by id
        /// </summary>
        /// <param name="Id">WorkStatus Id</param>
        /// <returns>WorkStatus object</returns>
        OpWorkStatus GetWorkStatus(int Id);

        /// <summary>
        /// Gets WorkStatus by id
        /// </summary>
        /// <param name="Ids">WorkStatus Ids</param>
        /// <returns>WorkStatus list</returns>
        List<OpWorkStatus> GetWorkStatus(int[] Ids);

        /// <summary>
        /// Gets WorkStatus by id
        /// </summary>
        /// <param name="Id">WorkStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkStatus object</returns>
        OpWorkStatus GetWorkStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets WorkStatus by id
        /// </summary>
        /// <param name="Ids">WorkStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkStatus list</returns>
        List<OpWorkStatus> GetWorkStatus(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the WorkStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">WorkStatus to save</param>
        /// <returns>WorkStatus Id</returns>
        int SaveWorkStatus(OpWorkStatus toBeSaved);

        /// <summary>
        /// Deletes the WorkStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">WorkStatus to delete</param>
        void DeleteWorkStatus(OpWorkStatus toBeDeleted);

        /// <summary>
        /// Clears the WorkStatus object cache
        /// </summary>
        void ClearWorkStatusCache();
        #endregion
        #region WorkType
        /// <summary>
        /// Indicates whether the WorkType is cached.
        /// </summary>
        bool WorkTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets WorkType by id
        /// </summary>
        /// <param name="Id">WorkType Id</param>
        /// <returns>WorkType object</returns>
        OpWorkType GetWorkType(long Id);

        /// <summary>
        /// Gets WorkType by id
        /// </summary>
        /// <param name="Ids">WorkType Ids</param>
        /// <returns>WorkType list</returns>
        List<OpWorkType> GetWorkType(long[] Ids);

        /// <summary>
        /// Gets WorkType by id
        /// </summary>
        /// <param name="Id">WorkType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkType object</returns>
        OpWorkType GetWorkType(long Id, bool queryDatabase);

        /// <summary>
        /// Gets WorkType by id
        /// </summary>
        /// <param name="Ids">WorkType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkType list</returns>
        List<OpWorkType> GetWorkType(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the WorkType object to the database
        /// </summary>
        /// <param name="toBeSaved">WorkType to save</param>
        /// <returns>WorkType Id</returns>
        int SaveWorkType(OpWorkType toBeSaved);

        /// <summary>
        /// Deletes the WorkType object from the database
        /// </summary>
        /// <param name="toBeDeleted">WorkType to delete</param>
        void DeleteWorkType(OpWorkType toBeDeleted);

        /// <summary>
        /// Clears the WorkType object cache
        /// </summary>
        void ClearWorkTypeCache();
        #endregion

        #region WorkOrderJci

        /// <summary>
        /// Indicates whether the WorkOrderJci is cached.
        /// </summary>
        bool WorkOrderJciCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WorkOrderJci objects
        /// </summary>
        List<OpWorkOrderJci> GetAllWorkOrderJci(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets WorkOrderJci by id
        /// </summary>
        /// <param name="Id">WorkOrderJci Id</param>
        /// <returns>WorkOrderJci object</returns>
        OpWorkOrderJci GetWorkOrderJci(int Id);

        /// <summary>
        /// Gets WorkOrderJci by id
        /// </summary>
        /// <param name="Ids">WorkOrderJci Ids</param>
        /// <returns>WorkOrderJci list</returns>
        List<OpWorkOrderJci> GetWorkOrderJci(int[] Ids);

        /// <summary>
        /// Gets WorkOrderJci by id
        /// </summary>
        /// <param name="Id">WorkOrderJci Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkOrderJci object</returns>
        OpWorkOrderJci GetWorkOrderJci(int Id, bool queryDatabase);

        /// <summary>
        /// Gets WorkOrderJci by id
        /// </summary>
        /// <param name="Ids">WorkOrderJci Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkOrderJci list</returns>
        List<OpWorkOrderJci> GetWorkOrderJci(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the WorkOrderJci object to the database
        /// </summary>
        /// <param name="toBeSaved">WorkOrderJci to save</param>
        /// <returns>WorkOrderJci Id</returns>
        int SaveWorkOrderJci(OpWorkOrderJci toBeSaved);

        /// <summary>
        /// Deletes the WorkOrderJci object from the database
        /// </summary>
        /// <param name="toBeDeleted">WorkOrderJci to delete</param>
        void DeleteWorkOrderJci(OpWorkOrderJci toBeDeleted);

        /// <summary>
        /// Clears the WorkOrderJci object cache
        /// </summary>
        void ClearWorkOrderJciCache();
        #endregion
#endif
    }
}