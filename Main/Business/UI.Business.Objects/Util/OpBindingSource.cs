﻿using System;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Drawing;
using System.Windows.Forms;

namespace IMR.Suite.UI.Business.Objects
{
#if !USE_UNITED_OBJECTS
    #region OpBindingList<T>
    public class OpBindingList<T> : BindingList<T>, ITypedList
    {
        const int MAX_RECURSION = 5;
        internal List<OpPropertyDescriptor<T>> properties = new List<OpPropertyDescriptor<T>>();

    #region Ctor
        public OpBindingList(IList<T> list)
            : base(list)
        {
            foreach (PropertyDescriptor pd in TypeDescriptor.GetProperties(typeof(T), new Attribute[] { new BrowsableAttribute(true) }))
                properties.Add(new OpPropertyDescriptor<T>(typeof(T), pd));
        }
    #endregion

        public void RefreshAllProperties()
        {
            foreach (PropertyDescriptor pd in this.GetItemProperties(null))
                properties.Add(new OpPropertyDescriptor<T>(typeof(T), pd));
        }

    #region ITypedList Implementation
        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            //return new PropertyDescriptorCollection(properties.ToArray()); ;
            PropertyDescriptorCollection pdc = new PropertyDescriptorCollection(properties.ToArray());

            // we are only concerned with browsable properties
            Attribute[] attributes = new Attribute[] { new BrowsableAttribute(true) };

            // get PropertyDescriptor objects for T and all properties on aggregated objects (recursively)
            foreach (PropertyDescriptor property in GetPropertiesRecursive(typeof(T), null, attributes, 1))
            {
                pdc.Add(property);
            }

            return pdc;
        }
        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return typeof(T).Name;
        }
    #endregion

    #region AddProperty
        public void AddProperty(OpDataProperty opDataProperty)
        {
            OpPropertyDescriptor<T> pd = properties.Find(w => (w is PropertyDescriptor) && (w as PropertyDescriptor).Name == opDataProperty.PropertyName);
            if (pd != null)
                opDataProperty.PropertyName = "mix" + opDataProperty.PropertyName;
            properties.Add(new OpPropertyDescriptor<T>(typeof(T), opDataProperty));
        }
        public void AddProperty(OpDynamicProperty dynamicProperty)
        {
            properties.Add(new OpPropertyDescriptor<T>(typeof(T), dynamicProperty));
        }
    #endregion
    #region ChangeProperty
        public void ChangeProperty(string propertyName, OpDataType opDataType, OpUnit opUnit)
        {
            OpPropertyDescriptor<T> pd = properties.Find(w => (w is PropertyDescriptor) && (w as PropertyDescriptor).Name == propertyName);
            if (pd != null)
                properties[properties.IndexOf(pd)] = new OpPropertyDescriptor<T>(typeof(T), pd.Name, pd, opDataType, opUnit);
        }
    #endregion

    #region GetPropertiesRecursive
        internal IEnumerable<PropertyDescriptor> GetPropertiesRecursive(Type t, PropertyDescriptor parent, Attribute[] attributes, int depth)
        {
            // self-referencing properties can cause infinite recursion, so place a cap on the depth of recursion
            if (depth >= MAX_RECURSION) yield break;

            // get the properties of the current Type
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(t, attributes))
            {
                if (parent == null)
                {
                    // property belongs to root type, return as-is
                    yield return property;
                }
                else
                {
                    // property is on an aggregated object, wrap and return
                    yield return new OpAggregatedPropertyDescriptor(parent, property, attributes);
                }

                // repeat for all properties belonging to this property
                foreach (PropertyDescriptor aggregated in GetPropertiesRecursive(property.PropertyType, parent, attributes, depth + 1))
                {
                    yield return new OpAggregatedPropertyDescriptor(property, aggregated, attributes);
                }
            }
        }
    #endregion

    }
    #endregion

    #region OpBindingSource<T>
    public class OpBindingSource<T> : BindingSource where T : IOpDynamicProperty<T>
    {
        const int MAX_RECURSION = 5;
        internal List<OpPropertyDescriptor<T>> properties = new List<OpPropertyDescriptor<T>>();

    #region Properties
        public T Owner
        {
            get
            {
                if (DataSource == null)
                    return default(T);
                return ((T)DataSource).Owner;
            }
        }
        public OpDynamicPropertyDict DynamicProperties
        {
            get
            {
                if (DataSource == null)
                    return null;
                return ((T)DataSource).DynamicProperties;
            }
        }
    #endregion

    #region Ctor
        public OpBindingSource() : this(default(T)) { }

        public OpBindingSource(T objectValue)
        {
            //foreach (PropertyDescriptor pd in TypeDescriptor.GetProperties(typeof(T), new Attribute[] { new BrowsableAttribute(true) }))
            //    properties.Add(new OpPropertyDescriptor(typeof(T), pd));

            if (objectValue != null)
            {
                foreach (OpDynamicProperty dynamicProperty in objectValue.DynamicProperties.Values)
                {
                    AddProperty(dynamicProperty);
                }

                DataSource = objectValue;
            }
        }
    #endregion

    #region AddProperty
        public void AddProperty(OpDataProperty opDataProperty)
        {
            OpPropertyDescriptor<T> pd = properties.Find(w => (w is PropertyDescriptor) && (w as PropertyDescriptor).Name == opDataProperty.PropertyName);
            if (pd != null)
                opDataProperty.PropertyName = "mix" + opDataProperty.PropertyName;
            properties.Add(new OpPropertyDescriptor<T>(typeof(T), opDataProperty));
        }
        public void AddProperty(OpDynamicProperty dynamicProperty)
        {
            properties.Add(new OpPropertyDescriptor<T>(typeof(T), dynamicProperty));
        }
    #endregion

    #region override GetItemProperties(...)
        public override PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            //return new PropertyDescriptorCollection(properties.ToArray()); ;
            PropertyDescriptorCollection pdc = new PropertyDescriptorCollection(properties.ToArray());

            // we are only concerned with browsable properties
            Attribute[] attributes = new Attribute[] { new BrowsableAttribute(true) };

            // get PropertyDescriptor objects for T and all properties on aggregated objects (recursively)
            foreach (PropertyDescriptor property in GetPropertiesRecursive(typeof(T), null, attributes, 1))
            {
                pdc.Add(property);
            }

            return pdc;
        }
    #endregion
    #region override GetListName(...)
        public override string GetListName(PropertyDescriptor[] listAccessors)
        {
            return GetType().Name;
        }
    #endregion

    #region GetPropertiesRecursive
        internal IEnumerable<PropertyDescriptor> GetPropertiesRecursive(Type t, PropertyDescriptor parent, Attribute[] attributes, int depth)
        {
            // self-referencing properties can cause infinite recursion, so place a cap on the depth of recursion
            if (depth >= MAX_RECURSION) yield break;

            // get the properties of the current Type
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(t, attributes))
            {
                if (parent == null)
                {
                    // property belongs to root type, return as-is
                    yield return property;
                }
                else
                {
                    // property is on an aggregated object, wrap and return
                    yield return new OpAggregatedPropertyDescriptor(parent, property, attributes);
                }

                // repeat for all properties belonging to this property
                foreach (PropertyDescriptor aggregated in GetPropertiesRecursive(property.PropertyType, parent, attributes, depth + 1))
                {
                    yield return new OpAggregatedPropertyDescriptor(property, aggregated, attributes);
                }
            }
        } 
    #endregion
    }
    #endregion

    #region OpAggregatedPropertyDescriptor
    /// <summary>
    /// Describes a property which belongs to another property, and whose value 
    /// is accessed/changed from the context of that property.
    /// </summary>
    public class OpAggregatedPropertyDescriptor : PropertyDescriptor
    {

        /// <summary>
        /// Gets the type of the component this property is bound to.
        /// </summary>
        public override Type ComponentType
        {
            get
            {
                return AggregatedProperty.ComponentType;
            }
        }
        /// <summary>
        /// Gets the PropertyDescriptor object wrapped by this instance.
        /// </summary>
        public PropertyDescriptor AggregatedProperty { get; private set; }
        /// <summary>
        /// Gets a value indicating whether this property is read-only.
        /// </summary>
        public override bool IsReadOnly
        {
            get
            {
                return AggregatedProperty.IsReadOnly;
            }
        }
        /// <summary>
        /// Gets the PropertyDescriptor object which owns this property.
        /// </summary>
        public PropertyDescriptor OwningProperty { get; private set; }
        /// <summary>
        /// Gets the type of the property.
        /// </summary>
        public override Type PropertyType
        {
            get
            {
                return AggregatedProperty.PropertyType;
            }
        }

        /// <summary>
        /// Initializes a new instance of the AggregatedPropertyDescriptor class, 
        /// given the owning property, the PropertyDescriptor instance being 
        /// wrapped and an attribute filter.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="aggregated"></param>
        /// <param name="attributes"></param>
        public OpAggregatedPropertyDescriptor(PropertyDescriptor owner, PropertyDescriptor aggregated, Attribute[] attributes)
            : base(owner.Name + "." + aggregated.Name, attributes)
        {
            OwningProperty = owner;
            AggregatedProperty = aggregated;
        }

        /// <summary>
        /// Returns whether resetting an object changes its value.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public override bool CanResetValue(object component)
        {
            return AggregatedProperty.CanResetValue(component);
        }

        /// <summary>
        /// Gets the current value of the property on a component, redirecting 
        /// the call to the appropriate object.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public override object GetValue(object component)
        {
            return AggregatedProperty.GetValue(OwningProperty.GetValue(component));
        }

        /// <summary>
        /// Resets the value for this property of the component to its default 
        /// value.
        /// </summary>
        /// <param name="component"></param>
        public override void ResetValue(object component)
        {
            AggregatedProperty.ResetValue(component);
        }

        /// <summary>
        /// Sets the value of the component to a different value, redirecting 
        /// the call to the appropriate object.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="value"></param>
        public override void SetValue(object component, object value)
        {
            AggregatedProperty.SetValue(OwningProperty.GetValue(component), value);
        }

        /// <summary>
        /// Determines whether the value of this property should be serialized.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public override bool ShouldSerializeValue(object component)
        {
            return AggregatedProperty.ShouldSerializeValue(component);
        }
    }
    #endregion

    #region OpPropertyDescriptor
    [Serializable]
    public class OpPropertyDescriptor<T> : PropertyDescriptor
    {
    #region class MixedProperty
        internal class MixedProperty
        {
            public string PropertyName;
            public PropertyDescriptor PropertyDescriptor;
            public OpDataType OpDataType;
            public OpUnit OpUnit;

            public MixedProperty(string propertyName, PropertyDescriptor propertyDescriptor, OpDataType opDataType, OpUnit opUnit)
            {
                this.PropertyName = propertyName;
                this.PropertyDescriptor = propertyDescriptor;
                this.OpDataType = opDataType;
                this.OpUnit = opUnit;
            }
        }
    #endregion

        object Property;
        Type _ComponentType;

    #region Ctor
        public OpPropertyDescriptor(Type componentType, PropertyDescriptor propertyDescriptor)
            : base(propertyDescriptor.Name, null)
        {
            this._ComponentType = componentType;
            this.Property = propertyDescriptor;
        }
        public OpPropertyDescriptor(Type componentType, string propertyName, PropertyDescriptor propertyDescriptor, OpDataType opDataType, OpUnit opUnit)
            : base(propertyDescriptor.Name, null)
        {
            this._ComponentType = componentType;
            this.Property = new MixedProperty(propertyName, propertyDescriptor, opDataType, opUnit);
        }
        public OpPropertyDescriptor(Type componentType, OpDataProperty opDataProperty)
            : base(opDataProperty.PropertyName, null)
        {
            this._ComponentType = componentType;
            this.Property = opDataProperty;
        }
        public OpPropertyDescriptor(Type componentType, OpDynamicProperty dynamicProperty)
            : base(dynamicProperty.PropertyName, null)
        {
            this._ComponentType = componentType;
            this.Property = dynamicProperty;
        }
    #endregion

    #region override CanResetValue
        public override bool CanResetValue(object component)
        {
            return false;
        }
    #endregion
    #region override GetValue
        public override object GetValue(object component)
        {
            PropertyInfo propertyInfo = null;

    #region OpDataProperty
            if (Property is OpDataProperty)
            {
                OpDataProperty opDataProperty = Property as OpDataProperty;
                if (component is IOpDataProperty)
                    return (component as IOpDataProperty).GetOpDataPropertyValue(opDataProperty);
                
                return null;
            }
    #endregion
    #region OpDynamicProperty
            if (Property is OpDynamicProperty)
            {
                if (component is IOpDynamicProperty<T>)
                {
                    OpDynamicPropertyDict dynamicProperties = (component as IOpDynamicProperty<T>).DynamicProperties;
                    if (dynamicProperties != null)
                        return dynamicProperties.GetValue((Property as OpDynamicProperty).PropertyName);
                }
                return null;
            }
    #endregion
    #region MixedProperty
            if (Property is MixedProperty)
                propertyInfo = GetLowestProperty(_ComponentType, (Property as MixedProperty).PropertyDescriptor.Name); //_ComponentType.GetProperty((Property as MixedProperty).PropertyDescriptor.Name);
    #endregion
    #region PropertyDescriptor
            if (Property is PropertyDescriptor)
                propertyInfo = GetLowestProperty(_ComponentType, (Property as PropertyDescriptor).Name); //_ComponentType.GetProperty((Property as PropertyDescriptor).Name);
    #endregion

            if (propertyInfo == null)
                return null;

            if (propertyInfo.CanRead)
                return propertyInfo.GetValue(component, null);

            return null;
        }
    #endregion
    #region override SetValue()
        public override void SetValue(object component, object value)
        {
            PropertyInfo propertyInfo = null;

    #region OpDataProperty
            if (Property is OpDataProperty)
            {
                OpDataProperty opDataProperty = Property as OpDataProperty;
                if (component is IOpDataProperty)
                    (component as IOpDataProperty).SetOpDataPropertyValue(opDataProperty, value);   
            }
    #endregion
    #region OpDynamicProperty
            if (Property is OpDynamicProperty)
            {
                if (component is IOpDynamicProperty<T>)
                {
                    OpDynamicPropertyDict dynamicProperties = (component as IOpDynamicProperty<T>).DynamicProperties;
                    if (dynamicProperties != null)
                        dynamicProperties.SetValue((Property as OpDynamicProperty).PropertyName, value);
                }
                return;
            }
    #endregion
    #region MixedProperty
            if (Property is MixedProperty)
                propertyInfo = GetLowestProperty(_ComponentType, (Property as MixedProperty).PropertyDescriptor.Name);  //_ComponentType.GetProperty((Property as MixedProperty).PropertyDescriptor.Name);
    #endregion
    #region PropertyDescriptor
            if (Property is PropertyDescriptor)
                propertyInfo = GetLowestProperty(_ComponentType, (Property as PropertyDescriptor).Name); //_ComponentType.GetProperty((Property as PropertyDescriptor).Name);
    #endregion

            if (propertyInfo == null)
                return;

            if (propertyInfo.CanWrite)
                propertyInfo.SetValue(component, value, null);
        }
    #endregion
    #region override IsReadOnly
        public override bool IsReadOnly
        {
            get
            {
                if (Property is MixedProperty)
                    return !(Property as MixedProperty).OpDataType.IsEditable;

                if (Property is OpDataProperty)
                    return !(Property as OpDataProperty).OpDataType.IsEditable;

                if (Property is OpDynamicProperty)
                    return (Property as OpDynamicProperty).IsReadOnly;

                if (Property is PropertyDescriptor)
                    return (Property as PropertyDescriptor).IsReadOnly;

                return true;
            }
        }
    #endregion
    #region override Name
        public override string Name
        {
            get
            {
                if (Property is OpDataProperty)
                    return (Property as OpDataProperty).PropertyName;
                if (Property is MixedProperty)
                    return (Property as MixedProperty).PropertyName;
                if (Property is OpDynamicProperty)
                    return (Property as OpDynamicProperty).PropertyName;
                if (Property is PropertyDescriptor)
                    return (Property as PropertyDescriptor).Name;

                return "Unknown";
            }
        }
    #endregion
    #region override ComponentType
        public override Type ComponentType { get { return _ComponentType; } }
    #endregion
    #region override PropertyType
        public override Type PropertyType
        {
            get
            {

                if (Property is OpDataProperty)
                {
                    if ((Property as OpDataProperty).PropertyType != null)
                        return (Property as OpDataProperty).PropertyType;
                    else if ((Property as OpDataProperty).ReturnReferencedTypeIfHelper.HasValue)
                    {
                        Type type = Utils.GetHelperType((Property as OpDataProperty).OpDataType, (Property as OpDataProperty).ReturnReferencedTypeIfHelper.Value);
                        if(type != null)
                            return type; 
                    }
                    return DataType.GetSystemType((Property as OpDataProperty).OpDataType.IdDataTypeClass);
                }

                if (Property is MixedProperty)
                    return (Property as MixedProperty).PropertyDescriptor.PropertyType;

                if (Property is OpDynamicProperty)
                    return (Property as OpDynamicProperty).PropertyType;

                if (Property is PropertyDescriptor)
                    return (Property as PropertyDescriptor).PropertyType;

                return typeof(string);
            }
        }
    #endregion
    #region override ResetValue
        public override void ResetValue(object component) { }
    #endregion
    #region override ShouldSerializeValue
        public override bool ShouldSerializeValue(object component) { return true; }
    #endregion

    #region GetLowestProperty
        public static PropertyInfo GetLowestProperty(Type type, string name)
        {
            while (type != null)
            {
                PropertyInfo info = type.GetProperty(name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                if (info != null)
                    return info;

                type = type.BaseType;
            }

            return null;
        }
    #endregion
    }
    #endregion

    #region OpDataProperty
    [Serializable]
    public class OpDataProperty
    {
        public string PropertyName;
        public Type PropertyType;
        public OpDataType OpDataType;
        public OpUnit OpUnit;
        public OpDataTypeFormat OpDataTypeFormat;
        public Enums.ReferenceType ReferenceType;
        /// <summary>
        /// Describes return value. 
        /// If value is true and object has helper datatype, referencedType should be return. 
        /// Otherwise basic type is return 
        /// </summary>
        public bool? ReturnReferencedTypeIfHelper;

        public OpDataProperty(string propertyName, OpDataType opDataType, Enums.ReferenceType referenceType, OpUnit opUnit)
            : this(propertyName, null, opDataType, referenceType, opUnit)
        {}

        public OpDataProperty(string propertyName, Type propertyType, OpDataType opDataType, Enums.ReferenceType referenceType, OpUnit opUnit)
            : this(propertyName, propertyType, opDataType, referenceType, opUnit, null)
        {}

        public OpDataProperty(string propertyName, Type propertyType, OpDataType opDataType, Enums.ReferenceType referenceType, OpUnit opUnit, OpDataTypeFormat opDataTypeFormat)
            : this(propertyName, propertyType, opDataType, referenceType, opUnit, opDataTypeFormat, null)
        {}
        public OpDataProperty(string propertyName, Type propertyType, OpDataType opDataType, Enums.ReferenceType referenceType, OpUnit opUnit, OpDataTypeFormat opDataTypeFormat, bool? returnReferenceTypeIfHelper = null)
        {
            this.PropertyType = propertyType;
            this.PropertyName = propertyName;
            this.OpDataType = opDataType;
            this.OpUnit = opUnit;
            this.OpDataTypeFormat = opDataTypeFormat;
            this.ReferenceType = referenceType;
            this.ReturnReferencedTypeIfHelper = returnReferenceTypeIfHelper;
        }
        public static OpDataProperty Create(string propertyName, OpDataType opDataType)
        {
            return new OpDataProperty(propertyName, opDataType, Enums.ReferenceType.None, null);
        }

        public static OpDataProperty Create(OpDataType opDataType)
        {
            return new OpDataProperty(opDataType.Name, opDataType, Enums.ReferenceType.None, null);
        }
    }
    #endregion

    #region OpDynamicProperty
    [Serializable]
    [System.Diagnostics.DebuggerDisplay("Property:{PropertyName}; Value:{Value}")]
    public class OpDynamicProperty
    {
        public string PropertyName;
        public Type PropertyType;
        public bool IsReadOnly;
        private object _Value;
        public object Value
        {
            get { return this._Value; }
            set
            {
                object newValue = value;
                // wymaga jeszcze poprawy
                //if (PropertyType != null
                //    && newValue != null && newValue is IConvertible)
                //{
                //    try
                //    {
                //        newValue = Convert.ChangeType(newValue, PropertyType);
                //    }
                //    catch
                //    {
                //        try
                //        {
                //            newValue = Convert.ChangeType(newValue, PropertyType, System.Globalization.CultureInfo.InvariantCulture);
                //        }
                //        catch 
                //        {
                //            return;
                //        }
                //    }
                //}
                this._Value = newValue;
            }
        }
        public OpChangeState OpState;

        public OpDynamicProperty(string propertyName, object value)
            : this(propertyName, null, false, value)
        { }
        public OpDynamicProperty(string propertyName, Type propertyType)
            : this(propertyName, propertyType, true, null)
        { }
        public OpDynamicProperty(string propertyName, Type propertyType, object value)
            : this(propertyName, propertyType, true, value)
        { }

        public OpDynamicProperty(string propertyName, Type propertyType, bool isReadOnly, object value)
        {
            this.PropertyName = propertyName;
            this.PropertyType = propertyType;
            this.IsReadOnly = isReadOnly;
            this.OpState = OpChangeState.Loaded;
            // wymaga jeszcze poprawy
            //if (this.PropertyType != null
            //    && value != null && value is IConvertible)
            //{
            //    try
            //    {
            //        value = Convert.ChangeType(value, PropertyType);
            //    }
            //    catch
            //    {
            //        try
            //        {
            //            value = Convert.ChangeType(value, PropertyType, System.Globalization.CultureInfo.InvariantCulture);
            //        }
            //        catch
            //        {
            //            this.OpState = OpChangeState.Incorrect;
            //        }
            //    }
            //}
            this._Value = value;
        }

        public static OpDynamicProperty Create(string propertyName, Type propertyType)
        {
            return new OpDynamicProperty(propertyName, propertyType);
        }
    }
    #endregion

    #region OpDynamicPropertyDict
    [Serializable]
    public class OpDynamicPropertyDict : Dictionary<string, OpDynamicProperty>
    {
    #region Ctor
        public OpDynamicPropertyDict()
        {

        }
        public OpDynamicPropertyDict(OpDynamicPropertyDict dict) : base(dict) { }
    #endregion

    #region AddRange
        public void AddRange(List<OpDynamicProperty> dynamicProperties)
        {
            foreach (OpDynamicProperty dynamicProperty in dynamicProperties)
            {
                Add(dynamicProperty.PropertyName, dynamicProperty);
            }
        }
    #endregion

    #region GetValue
        public object GetValue(string propertyName)
        {
            OpDynamicProperty dynamicProperty = null;
            if (TryGetValue(propertyName, out dynamicProperty))
                return dynamicProperty.Value;

            return null;
        }
    #endregion
    #region GetValue<T>
        public T GetValue<T>(string propertyName) where T : struct, IConvertible
        {
            OpDynamicProperty dynamicProperty = null;
            if (TryGetValue(propertyName, out dynamicProperty))
            {
                if (dynamicProperty.Value != null)
                    return GenericConverter.Parse<T>(dynamicProperty.Value);
            }

            return default(T);
        }
    #endregion
    #region GetNullableValue
        public Nullable<T> GetNullableValue<T>(string propertyName) where T : struct, IConvertible
        {
            OpDynamicProperty dynamicProperty = null;
            if (TryGetValue(propertyName, out dynamicProperty))
            {
                if (dynamicProperty.Value != null)
                    return GenericConverter.Parse<T>(dynamicProperty.Value);
            }

            return null;
        }
    #endregion
    #region SetValue
        public void SetValue(string propertyName, object value)
        {
            if (!ContainsKey(propertyName))
                RaiseKeyNotFoundException();
            base[propertyName].Value = value;
        }
    #endregion

    #region AddProperty
        public void AddProperty(OpDynamicProperty dynamicProperty)
        {
            if (dynamicProperty == null) return;
            Add(dynamicProperty.PropertyName, dynamicProperty);
        }
    #endregion

    #region AddOrUpdateProperty
        public void AddOrUpdateProperty(OpDynamicProperty dynamicProperty)
        {
            if (dynamicProperty == null) return;
            if (ContainsKey(dynamicProperty.PropertyName))
                SetValue(dynamicProperty.PropertyName, dynamicProperty.Value);
            else
                AddProperty(dynamicProperty);
        }
        public void AddOrUpdateProperty(string propertyName, object value)
        {
            AddOrUpdateProperty(propertyName, null, false, value);
        }
        public void AddOrUpdateProperty(string propertyName, Type propertyType)
        {
            AddOrUpdateProperty(propertyName, propertyType, true, null);
        }
        public void AddOrUpdateProperty(string propertyName, Type propertyType, object value)
        {
            AddOrUpdateProperty(propertyName, propertyType, true, value);
        }
        public void AddOrUpdateProperty(string propertyName, Type propertyType, bool isReadOnly, object value)
        {
            if (ContainsKey(propertyName))
                SetValue(propertyName, value);
            else
                AddProperty(new OpDynamicProperty(propertyName, propertyType, isReadOnly, value));
        }
    #endregion

    #region this[string propertyName]
        new public object this[string propertyName]
        {
            get { return GetValue(propertyName); }
            set { SetValue(propertyName, value); }
        }
    #endregion
    #region this[long propertyName]
        public object this[long propertyName]
        {
            get { return GetValue(propertyName.ToString()); }
            set { SetValue(propertyName.ToString(), value); }
        }
    #endregion

    #region RaiseKeyNotFoundException
        private void RaiseKeyNotFoundException()
        {
            throw new KeyNotFoundException("Specified property does't exist");
        }
    #endregion
    }
    #endregion

    #region IOpDynamicProperty<T>
    public interface IOpDynamicProperty<T>
    {
        OpDynamicPropertyDict DynamicProperties { get; set; }
        T Owner { get; }
    }
    #endregion
    #region IOpDataProperty
    public interface IOpDataProperty
    {
        object GetOpDataPropertyValue(OpDataProperty opDataProperty);
        void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value);
        Type GetOpDataPropertyType(OpDataProperty opDataProperty);
    }
    #endregion
#endif
}
