﻿using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Globalization;



namespace IMR.Suite.UI.Business.Objects
{
#if !USE_UNITED_OBJECTS
    #region class IOpDataExtensions
        public static class IOpDataExtensions
        {
            public static T GetValue<T>(this IOpData data) where T : IConvertible
            {
                return data.GetValue<T>(null);
            }
            public static T GetValue<T>(this IOpData data, IFormatProvider provider) where T : IConvertible
            {
                if (data.Value == null)
                    return default(T);
                else
                {
                    if (provider == null)
                        return GenericConverter.Parse<T>(data.Value);
                    else
                        return GenericConverter.Parse<T>(data.Value, provider);
                }
            }
            public static Nullable<T> GetNullableValue<T>(this IOpData data) where T : struct, IConvertible
            {
                if (data.Value == null)
                    return null;
                else
                    return GenericConverter.Parse<T>(data.Value);
            }

            public static T TryGetValue<T>(this IOpData data) where T : IConvertible
            {
                return data.TryGetValue<T>(null);
            }
            public static T TryGetValue<T>(this IOpData data, IFormatProvider provider) where T : IConvertible
            {
                if (data.Value == null)
                    return default(T);
                else
                {
                    if (provider == null)
                        return GenericConverter.Parse<T>(data.Value);
                    else
                        return GenericConverter.Parse<T>(data.Value, provider);
                }
            }
            public static Nullable<T> TryGetNullableValue<T>(this IOpData data) where T : struct, IConvertible
            {
                if (data.Value == null)
                    return null;
                else
                    return GenericConverter.Parse<T>(data.Value);
            }
        }
    #endregion

    #region class OpDataUtil //przeniesione do UI.Business.Objects
        /*
        public static class OpDataUtil
        {
    #region ValueEquals
            public static bool ValueEquals<T>(T firstPrimitive, T secondPrimitive)
            {
                if (firstPrimitive == null && secondPrimitive == null)
                    return true;

                if (firstPrimitive == null && secondPrimitive != null)
                    return false;

                if (firstPrimitive != null && secondPrimitive == null)
                    return false;

                Type[] notHandledTypes = new Type[] { typeof(char), typeof(string), typeof(object) };

                Type firstType = firstPrimitive.GetType();
                Type secondType = secondPrimitive.GetType();

                if ((firstType.IsPrimitive || firstType == typeof(decimal)) && (secondType.IsPrimitive || secondType == typeof(decimal)) && !notHandledTypes.Contains(firstPrimitive.GetType()) && !notHandledTypes.Contains(secondPrimitive.GetType()))
                {
                    if (firstType == typeof(bool))
                        return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    else if (firstType == typeof(byte))
                    {
                        if (new Type[] { typeof(bool) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(sbyte))
                    {
                        if (new Type[] { typeof(bool) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(ushort))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(short))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(uint))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(int))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(ulong))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(long))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(float))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int), typeof(ulong), typeof(long) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(double))
                    {
                        if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int), typeof(ulong), typeof(long), typeof(float) }.Contains(secondType))
                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                        else
                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else if (firstType == typeof(decimal))
                    {
                        return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                    }
                    else
                        return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                }
                else if (firstType == typeof(string) && secondType == typeof(string))
                {
                    string firstString = firstPrimitive.ToString();
                    string secondString = secondPrimitive.ToString();
                    return firstString.Equals(secondString);
                }
                else if (firstType == typeof(byte[]) && secondType == typeof(byte[]))
                {
                    if ((firstPrimitive as byte[]).Length != (secondPrimitive as byte[]).Length)
                        return false;

                    for (int i = 0; i < (firstPrimitive as byte[]).Length; i++)
                    {
                        byte firstByte = (firstPrimitive as byte[])[i];
                        byte secondByte = (secondPrimitive as byte[])[i];
                        if (firstByte != secondByte)
                            return false;
                    }
                    return true;
                }
                else if (notHandledTypes.Contains(firstType) || notHandledTypes.Contains(secondType))
                    return false;
                else if ((firstType.IsArray && !secondType.IsArray) || (!firstType.IsArray && secondType.IsArray)) // dodane, bo gdy np. secondType jest long, a firstType jest byte[] to w else byl blad konwersji
                    return false;
                else
                    return (bool)(firstPrimitive.GetType().InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstPrimitive.GetType()) }));
            }
    #endregion
    #region CorrectValueType
            public static object CorrectValueType(OpDataType dataType, object value)
            {
                if (dataType == null) return value;

                return DataType.CorrectValueType(dataType.IdDataTypeClass, value);
            }
    #endregion
    #region SetNewValue
            public static T SetNewValue<T, U>(T value, T newValue, U item) where U : IOpChangeState
            {
                return SetNewValue(value, newValue, item, null);
            }
            public static T SetNewValue<T, U>(T value, T newValue, U item, OpDataType dataType) where U : IOpChangeState
            {
                if (dataType != null)
                {
                    try
                    {
                        newValue = (T)CorrectValueType(dataType, newValue);
                    }
                    catch
                    {
                        return value;
                    }
                }
                if (!OpDataUtil.ValueEquals<T>(value, newValue))
                {
                    value = newValue;
                    if (item.OpState != OpChangeState.New && item.OpState != OpChangeState.Delete)
                        item.OpState = OpChangeState.Modified;
                }
                return value;
            }
    #endregion
    #region SetNewIndex
            public static int SetNewIndex<U>(int value, int newValue, U item) where U : IOpChangeState
            {
                if (value != newValue)
                {
                    value = newValue;
                    if (item.OpState != OpChangeState.New && item.OpState != OpChangeState.Delete)
                        item.OpState = OpChangeState.Modified;
                }
                return value;
            }
    #endregion
        }
        */
    #endregion

    #region class OpDataUtil

        //konieczne jest pozostawienie tej klasy w tym przypadku, ponieważ w tym trybie nie mam dziedziczenia po klasach UI.Bussiness.Objects
        public static class OpDataUtil
        {
            #region ValueEquals
                                public static bool ValueEquals<T>(T firstPrimitive, T secondPrimitive)
                                {
                                    if (firstPrimitive == null && secondPrimitive == null)
                                        return true;

                                    if (firstPrimitive == null && secondPrimitive != null)
                                        return false;

                                    if (firstPrimitive != null && secondPrimitive == null)
                                        return false;

                                    Type[] notHandledTypes = new Type[] { typeof(char), typeof(string), typeof(object) };

                                    Type firstType = firstPrimitive.GetType();
                                    Type secondType = secondPrimitive.GetType();

                                    if ((firstType.IsPrimitive || firstType == typeof(decimal)) && (secondType.IsPrimitive || secondType == typeof(decimal)) && !notHandledTypes.Contains(firstPrimitive.GetType()) && !notHandledTypes.Contains(secondPrimitive.GetType()))
                                    {
                                        if (firstType == typeof(bool))
                                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        else if (firstType == typeof(byte))
                                        {
                                            if (new Type[] { typeof(bool) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(sbyte))
                                        {
                                            if (new Type[] { typeof(bool) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(ushort))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(short))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(uint))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(int))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(ulong))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(long))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(float))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int), typeof(ulong), typeof(long) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(double))
                                        {
                                            if (new Type[] { typeof(bool), typeof(byte), typeof(sbyte), typeof(ushort), typeof(short), typeof(uint), typeof(int), typeof(ulong), typeof(long), typeof(float) }.Contains(secondType))
                                                return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                            else
                                                return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else if (firstType == typeof(decimal))
                                        {
                                            return (bool)(secondType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, secondPrimitive, new object[] { Convert.ChangeType(firstPrimitive, secondType) }));
                                        }
                                        else
                                            return (bool)(firstType.InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstType) }));
                                    }
                                    else if (firstType == typeof(string) && secondType == typeof(string))
                                    {
                                        string firstString = firstPrimitive.ToString();
                                        string secondString = secondPrimitive.ToString();
                                        return firstString.Equals(secondString);
                                    }
                                    else if (firstType == typeof(byte[]) && secondType == typeof(byte[]))
                                    {
                                        if ((firstPrimitive as byte[]).Length != (secondPrimitive as byte[]).Length)
                                            return false;

                                        for (int i = 0; i < (firstPrimitive as byte[]).Length; i++)
                                        {
                                            byte firstByte = (firstPrimitive as byte[])[i];
                                            byte secondByte = (secondPrimitive as byte[])[i];
                                            if (firstByte != secondByte)
                                                return false;
                                        }
                                        return true;
                                    }
                                    else if (notHandledTypes.Contains(firstType) || notHandledTypes.Contains(secondType))
                                        return false;
                                    else if ((firstType.IsArray && !secondType.IsArray) || (!firstType.IsArray && secondType.IsArray)) // dodane, bo gdy np. secondType jest long, a firstType jest byte[] to w else byl blad konwersji
                                        return false;
                                    else
                                        return (bool)(firstPrimitive.GetType().InvokeMember("Equals", BindingFlags.InvokeMethod, null, firstPrimitive, new object[] { Convert.ChangeType(secondPrimitive, firstPrimitive.GetType()) }));
                                }
            #endregion
            #region CorrectValueType
                                public static object CorrectValueType(OpDataType dataType, object value)
                                {
                                    if (dataType == null) return value;

                                    return DataType.CorrectValueType(dataType.IdDataTypeClass, value);
                                }
            #endregion
            #region SetNewValue
                                public static T SetNewValue<T, U>(T value, T newValue, U item) where U : IOpChangeState
                                {
                                    return SetNewValue(value, newValue, item, null);
                                }
                                public static T SetNewValue<T, U>(T value, T newValue, U item, OpDataType dataType) where U : IOpChangeState
                                {
                                    if (dataType != null)
                                    {
                                        try
                                        {
                                            newValue = (T)CorrectValueType(dataType, newValue);
                                        }
                                        catch
                                        {
                                            return value;
                                        }
                                    }
                                    if (!OpDataUtil.ValueEquals<T>(value, newValue))
                                    {
                                        value = newValue;
                                        if (item.OpState != OpChangeState.New && item.OpState != OpChangeState.Delete)
                                            item.OpState = OpChangeState.Modified;
                                    }
                                    return value;
                                }
            #endregion
            #region SetNewIndex
                                public static int SetNewIndex<U>(int value, int newValue, U item) where U : IOpChangeState
                                {
                                    if (value != newValue)
                                    {
                                        value = newValue;
                                        if (item.OpState != OpChangeState.New && item.OpState != OpChangeState.Delete)
                                            item.OpState = OpChangeState.Modified;
                                    }
                                    return value;
                                }
            #endregion
        }

    #endregion

    #region class OpDataTypeIndexValueComparer<T>
        public class OpDataTypeIndexValueComparer<T> : IEqualityComparer<T> where T : IOpData, new()
        {
            public bool Equals(T x, T y)
            {
                if (x.IdDataType == y.IdDataType & x.Index == y.Index)
                {
                    if (OpDataUtil.ValueEquals(x.Value, y.Value))
                        return true;
                }
                return false;
            }

            public int GetHashCode(T obj)
            {
                return (obj.IdDataType ^ obj.Index).GetHashCode();
            }
        }
    #endregion
    
    #region class OpDataListExtensions
        public static class OpDataListExtensions
        {
            public static OpDataList<T> ToOpDataList<T>(this IEnumerable<T> items) where T : IOpDataProvider, IOpData, new()
            {
                OpDataList<T> result = new OpDataList<T>(); result.AddRange(items);
                return result;
            }

            /// <summary>
            /// Funkcja "merguje" listę OpDataList na podstawie oryginalnej listy oraz listy zmian
            /// </summary>
            /// <typeparam name="T"></typeparam>
            /// <param name="originalList">Oryginalna lista parametrów</param>
            /// <param name="changeList">Lista parametrów ktore zostaly zmienione/dodane/usuniete</param>
            /// <returns></returns>
            public static OpDataList<T> MergeDiff<T>(this OpDataList<T> originalList, OpDataList<T> changeList) where T : IOpDataProvider, IOpData, new()
            {
                foreach (T changedData in changeList.Where(d => d.OpState == OpChangeState.New || d.OpState == OpChangeState.Modified))
                {
                    originalList.SetValue(changedData.IdDataType, changedData.Index, changedData.Value);
                }
                foreach (T changedData in changeList.Where(d => d.OpState == OpChangeState.Delete))
                {
                    originalList.DeleteValue(changedData.IdDataType, changedData.Index);
                }
                return originalList;
            }
        }
    #endregion
    
    #region class OpDataList<TSource>
        [Serializable]
        public class OpDataList<TSource> : List<TSource> where TSource : IOpData, IOpDataProvider, new()
        {
    #region Properties
            public bool HasChanged
            {
                get
                {
                    return this.Count(d => d.OpState != OpChangeState.Loaded && d.OpState != OpChangeState.Undefined) > 0;
                }
            }
    #endregion

            public OpDataList() { }
            public OpDataList(IEnumerable<TSource> collection)
                : base(collection)
            {
            }
    #region override List<TSource> Members
            public new bool Remove(TSource item)
            {
                return this.Remove(item, false);
            }
            public bool Remove(TSource item, bool forceRemove)
            {
                if (forceRemove)
                {
                    base.Remove(item);
                    return true;
                }

                if (item.OpState == OpChangeState.New)
                    return base.Remove(item);

                item.OpState = OpChangeState.Delete;
                return true;
            }
            public new void RemoveAt(int index)
            {
                this.RemoveAt(index, false);
            }
            public void RemoveAt(int index, bool forceRemove)
            {
                if (forceRemove)
                {
                    base.RemoveAt(index);
                    return;
                }

                TSource dataToRemove = default(TSource);
                try
                {
                    dataToRemove = base[index];
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (dataToRemove.OpState == OpChangeState.New)
                        base.RemoveAt(index);
                    else
                        dataToRemove.OpState = OpChangeState.Delete;
                }
            }
    #endregion

            public OpDataList<TSource> Clone(IDataProvider dataProvider)
            {
                OpDataList<TSource> clonedDataList = new OpDataList<TSource>();
                this.ForEach(w => clonedDataList.Add((TSource)w.Clone(dataProvider)));
                return clonedDataList;
            }

            public bool Exists(long idDataType)
            {
                return this.Find(d => d.IdDataType == idDataType) != null;
            }
            public bool Exists(long idDataType, int index)
            {
                return this.Find(d => d.IdDataType == idDataType && d.Index == index) != null;
            }

            public int GetNextIndex(long idDataType)
            {
                List<TSource> dataValues = this.Where(d => d.IdDataType == idDataType).ToList();
                if (dataValues != null && dataValues.Count > 0)
                {
                    return dataValues.Max(d => d.Index) + 1;
                }
                else
                    return 0;
            }
            public int GetFirstFreeIndex(long idDataType)
            {
                HashSet<int> indices = new HashSet<int>(this.Where(d => d.IdDataType == idDataType).Select(d => d.Index));
                int index = 0;
                while (indices.Contains(index))
                    index++;
                return index;
            }

            private T GetValue<T>(long idDataType, int index) where T : IConvertible
            {
                return GetValue<T>(idDataType, index, null);
            }
            private T GetValue<T>(long idDataType, int index, IFormatProvider provider) where T : IConvertible
            {
                object value = this.Find(d => d.IdDataType == idDataType && d.Index == index).Value;
                if (value == null)
                    return default(T);
                else
                {
                    if (provider == null)
                        return GenericConverter.Parse<T>(value);
                    else
                        return GenericConverter.Parse<T>(value, provider);
                }
            }

            private Nullable<T> GetNullableValue<T>(long idDataType, int index) where T : struct, IConvertible
            {
                object value = this.Find(d => d.IdDataType == idDataType && d.Index == index).Value;
                if (value == null)
                    return null;
                else
                    return GenericConverter.Parse<T>(value);
            }

            public T TryGetValue<T>(long idDataType) where T : IConvertible
            {
                return TryGetValue<T>(idDataType, 0, null);
            }
            public T TryGetValue<T>(long idDataType, int index) where T : IConvertible
            {
                return TryGetValue<T>(idDataType, index, null);
            }
            public T TryGetValue<T>(long idDataType, IFormatProvider provider) where T : IConvertible
            {
                return TryGetValue<T>(idDataType, 0, provider);
            }
            public T TryGetValue<T>(long idDataType, int index, IFormatProvider provider) where T : IConvertible
            {
                if (Exists(idDataType, index))
                    return GetValue<T>(idDataType, index, provider);
                else
                    return default(T);
            }

            public Nullable<T> TryGetNullableValue<T>(long idDataType) where T : struct, IConvertible
            {
                return TryGetNullableValue<T>(idDataType, 0);
            }
            public Nullable<T> TryGetNullableValue<T>(long idDataType, int index) where T : struct, IConvertible
            {
                if (Exists(idDataType, index))
                    return GetNullableValue<T>(idDataType, index);
                else
                    return null;
            }

            private void AddValue(long idDataType, int index, object value)
            {
                this.Add(new TSource() { IdDataType = idDataType, Index = index, Value = value, OpState = OpChangeState.New });
            }
            public void SetValue(long idDataType, object value)
            {
                SetValue(idDataType, 0, value);
            }
            public void SetValue(long idDataType, int index, object value)
            {
                TSource item = this.Find(d => d.IdDataType == idDataType && d.Index == index);
                if (item != null)
                {
                    if (!OpDataUtil.ValueEquals(item.Value, value))
                    {
                        item.Value = value;
                        if (item.OpState != OpChangeState.New && item.OpState != OpChangeState.Delete)
                        {
                            item.OpState = OpChangeState.Modified;

                            if (item is OpData && (item as OpData).DataType != null && (item as OpData).DataType.IsRemoteWrite)
                            {
                                (item as OpData).DataStatus &= ~Enums.DataStatus.ActiveInDevice;
                                (item as OpData).DataStatus &= ~Enums.DataStatus.Synchronized;
                            }
                        }
                    }
                }
                else
                    AddValue(idDataType, index, value);
            }

            public void SetStatus(long idDataType, OpChangeState changeState)
            {
                List<TSource> list = this.FindAll(d => d.IdDataType == idDataType);
                foreach (TSource item in list)
                {
                    item.OpState = changeState;
                }
            }

            public void SetStatus(long idDataType, int index, OpChangeState changeState)
            {
                List<TSource> list = this.FindAll(d => d.IdDataType == idDataType && d.Index == index);
                foreach (TSource item in list)
                {
                    item.OpState = changeState;
                }
            }

            public void DeleteValue(long idDataType, bool forceRemove = false)
            {
                List<TSource> list = this.Where(d => d.IdDataType == idDataType).ToList();
                foreach (var item in list)
                    DeleteValue(idDataType, item.Index, forceRemove);
            }
            public void DeleteValue(long idDataType, int index, bool forceRemove = false)
            {
                TSource dataToRemove = this.Find(d => d.IdDataType == idDataType && d.Index == index);
                if (dataToRemove != null)
                    this.Remove(dataToRemove, forceRemove);
            }
            public void DeleteAll()
            {
                List<TSource> newItems = this.Where(w => w.OpState == OpChangeState.New).ToList();
                newItems.ForEach(w => { this.Remove(w); });
                this.ForEach(w => { w.OpState = OpChangeState.Delete; });
            }

            public void AssignReferences(IDataProvider dataprovider)
            {
                this.ForEach(w => { w.AssignReferences(dataprovider); });
            }

        }
    #endregion
#endif
}
