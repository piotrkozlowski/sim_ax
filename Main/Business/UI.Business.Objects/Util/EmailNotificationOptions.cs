﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects
{
#if !USE_UNITED_OBJECTS
    #region EmailNotificationOptions
    [Serializable]
    public class EmailNotificationOptions
    {
    #region Enums
    #region IssueOptions
        public enum IssueOptions
        {
            None = 0,
            All = int.MaxValue,
            Add = 1,
            Change = 1 << 1,
            Delete = 1 << 2,
            Assigned = 1 << 3,
            Status_NEW = 1 << 4,
            Status_ACCEPTED = 1 << 5,
            Status_QUEUED = 1 << 6,
            Status_IN_PROGRESS = 1 << 7,
            Status_WAITING_FOR_RESPONSE = 1 << 8,
            Status_FINISHED = 1 << 9,
            Status_SUSPENDED = 1 << 10,
            Status_REOPENED = 1 << 11,
            Note_Change = 1 << 12
        } 
    #endregion
    #region TaskOptions
        public enum TaskOptions
        {
            None = 0,
            All = int.MaxValue,
            Add = 1,
            Change = 1 << 1,
            Delete = 1 << 2,
            Assigned = 1 << 3,
            Status_Not_Started = 1 << 4,
            Status_In_progress = 1 << 5,
            Status_WaitingForAcceptance = 1 << 6,
            Status_Finished_succesfully = 1 << 7,
            Status_Finished_with_error = 1 << 8,
            Status_Planned = 1 << 9,
            Status_Canceled = 1 << 10,
            Status_Suspended = 1 << 11,
            Status_Verified = 1 << 12,
            Status_RealizationNotPossible = 1 << 13,
            Status_Rejected = 1 << 14

        } 
    #endregion
    #region WorkOrderOptions
        public enum WorkOrderOptions
        {
            None = 0,
            All = int.MaxValue,
            Add = 1,
            Change = 1 << 1,
            Delete = 1 << 2,
            Assigned = 1 << 3,
            Status_Planned = 1 << 4,
            Status_Accepted = 1 << 5,
            Status_InProgress = 1 << 6,
            Status_Finished = 1 << 7
        } 
    #endregion
    #region PackageOptions
        public enum PackageOptions
        {
            None = 0,
            All = int.MaxValue,
            //Add = 1,
            //Change = 1 << 1,
            // Delete = 1 << 2,
            // Assigned = 1 << 3,
            Status_New = 1 << 4,
            Status_Sent = 1 << 5,
            Status_Wrong = 1 << 6,
            Status_Accepted = 1 << 7,
            Status_Distributed = 1 << 8,
            Status_Removed = 1 << 9,
            Status_NewOrder = 1 << 10,
            Status_Ordered = 1 << 11,
            Status_ToCompletion = 1 << 12,
            Status_InCompletion = 1 << 13,
            // Status_Expired = 1 << 9,
        } 
    #endregion
    #region LocationOptions
        public enum LocationOptions
        {
            None = 0,
            All = int.MaxValue,
            Add = 1,
            Change = 1 << 1,
            Delete = 1 << 2,
            //Assigned = 1 << 3,
            State_NEW = 1 << 4,
            State_SCHEDULED = 1 << 5,
            State_INTERVENTION = 1 << 6,
            State_PENDING = 1 << 7,
            State_OPERATIONAL = 1 << 8,
            State_SUSPENDED = 1 << 9,
            State_UNUSED = 1 << 10,
            State_DELETED = 1 << 11,
            State_APPROVED = 1 << 12
        } 
    #endregion

    #region NotificationType
        public enum NotificationType
        {
            Issue = 1,
            Task = 2,
            WorkOrder = 3,
            Package = 4,
            ShippingList = 5,
            Location = 6,
        } 
        #endregion
        #endregion

    #region Members
        private int issueOptions;
        private int taskOptions;
        private int workOrderOptions;
        private int packageOptions;
        private int locationOptions;
        private List<int> shippingListCloseOptions;
        private List<int> taskIdDistributorOptions;
        private List<int> issueIdDistributorOptions;
        private List<int> locationIdDistributorOptions;
        private List<int> issueIdIssueTypeOptions;

        private OpOperator owner;
        #endregion
        #region Ctor
        internal EmailNotificationOptions()
        {
            owner = new OpOperator();

            issueOptions = (int)IssueOptions.None;
            taskOptions = (int)TaskOptions.None;
            workOrderOptions = (int)WorkOrderOptions.None;
            packageOptions = (int)PackageOptions.None;
            locationOptions = (int)LocationOptions.None;
            
            locationIdDistributorOptions = new List<int>();
            taskIdDistributorOptions = new List<int>();
            issueIdDistributorOptions = new List<int>();
            shippingListCloseOptions = new List<int>();
            issueIdIssueTypeOptions = new List<int>();

        }
        internal EmailNotificationOptions(OpOperator op)
        {

            //Musimy zrobić kopie obiektu żeby wywalić z niego powiązanie z SMSNotification inaczej nie da się tych obiektów seializować
            OpOperator operTmp = new OpOperator(op);
            operTmp.DataList = op.DataList;//Chcemy mieć referencję do właściwego DataList
            operTmp.SmsNotificationOptions = null;
            operTmp.EmailNotificationOptions = null;
            owner = operTmp;

            issueOptions = owner.DataList.TryGetNullableValue<int>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE) ?? (int)IssueOptions.None;
            taskOptions = owner.DataList.TryGetNullableValue<int>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK) ?? (int)TaskOptions.None;
            workOrderOptions = owner.DataList.TryGetNullableValue<int>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ROUTE) ?? (int)WorkOrderOptions.None;
            packageOptions = owner.DataList.TryGetNullableValue<int>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_PACKAGE) ?? (int)PackageOptions.None;
            locationOptions = owner.DataList.TryGetNullableValue<int>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION) ?? (int)LocationOptions.None;

            string locationIdDistributor = owner.DataList.TryGetValue<string>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION_ID_DISTRIBUTOR);
            locationIdDistributorOptions = new List<int>();
            if (!String.IsNullOrEmpty(locationIdDistributor))
            {
                locationIdDistributor = locationIdDistributor.Trim();
                foreach (string sItem in locationIdDistributor.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    int idDistributor = -1;
                    if (int.TryParse(sItem, out idDistributor))
                        locationIdDistributorOptions.Add(idDistributor);
                }
            }

            string taskIdDistributor = owner.DataList.TryGetValue<string>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK_ID_DISTRIBUTOR);
            taskIdDistributorOptions = new List<int>();
            if (!String.IsNullOrEmpty(taskIdDistributor))
            {
                taskIdDistributor = taskIdDistributor.Trim();
                foreach (string sItem in taskIdDistributor.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    int idDistributor = -1;
                    if (int.TryParse(sItem, out idDistributor))
                        taskIdDistributorOptions.Add(idDistributor);
                }
            }

            string issueIdDistributor = owner.DataList.TryGetValue<string>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR);
            issueIdDistributorOptions = new List<int>();
            if (!String.IsNullOrEmpty(issueIdDistributor))
            {
                issueIdDistributor = issueIdDistributor.Trim();
                foreach (string sItem in issueIdDistributor.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    int idDistributor = -1;
                    if (int.TryParse(sItem, out idDistributor))
                        issueIdDistributorOptions.Add(idDistributor);
                }
            }

            string shippingListClose = owner.DataList.TryGetValue<string>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_SHIPPING_LIST_CLOSE);
            shippingListCloseOptions = new List<int>();
            if (!String.IsNullOrEmpty(shippingListClose))
            {
                shippingListClose = shippingListClose.Trim();
                foreach (string sItem in shippingListClose.Split(new char[]{ ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    int idDistributor = -1;
                    if (int.TryParse(sItem, out idDistributor))
                        shippingListCloseOptions.Add(idDistributor);
                }
            }

            string issueIdIssueType = owner.DataList.TryGetValue<string>(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE);
            issueIdIssueTypeOptions = new List<int>();
            if (!String.IsNullOrEmpty(issueIdIssueType))
            {
                issueIdIssueType = issueIdIssueType.Trim();
                foreach (string sItem in issueIdIssueType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    int idIssueType = -1;
                    if (int.TryParse(sItem, out idIssueType))
                        issueIdIssueTypeOptions.Add(idIssueType);
                }
            }
        }
    #endregion

    #region get
        public bool GetNotify(LocationOptions notification)
        {
            if (notification == LocationOptions.All)
                return locationOptions == int.MaxValue;
            if (notification == LocationOptions.None)
                return locationOptions == 0;
            return (locationOptions & (int)notification) > 0;
        }

        public bool GetNotify(IssueOptions notification)
        {
            if (notification == IssueOptions.All)
                return issueOptions == int.MaxValue;
            if (notification == IssueOptions.None)
                return issueOptions == 0;
            return (issueOptions & (int)notification) > 0;
        }

        public bool GetNotify(TaskOptions notification)
        {
            if (notification == TaskOptions.All)
                return taskOptions == int.MaxValue;
            if (notification == TaskOptions.None)
                return taskOptions == 0;
            return (taskOptions & (int)notification) > 0;
        }

        public bool GetNotify(WorkOrderOptions notification)
        {
            if (notification == WorkOrderOptions.All)
                return workOrderOptions == int.MaxValue;
            if (notification == WorkOrderOptions.None)
                return workOrderOptions == 0;
            return (workOrderOptions & (int)notification) > 0;
        }

        public bool GetNotify(PackageOptions notification)
        {
            if (notification == PackageOptions.All)
                return packageOptions == int.MaxValue;
            if (notification == PackageOptions.None)
                return packageOptions == 0;
            return (packageOptions & (int)notification) > 0;
        }

        public bool GetNotify(OpDistributor distributor, NotificationType type)
        {
            List<int> idDistributorList = new List<int>();
            if (type == NotificationType.ShippingList)
                idDistributorList = shippingListCloseOptions;
            else if (type == NotificationType.Task)
                idDistributorList = taskIdDistributorOptions;
            else if (type == NotificationType.Issue)
                idDistributorList = issueIdDistributorOptions;
            else if (type == NotificationType.Location)
                idDistributorList = locationIdDistributorOptions;

            if (idDistributorList != null && distributor != null && idDistributorList.Contains(distributor.IdDistributor))
                return true;
            else
                return false;
        }

        public bool GetNotify(OpIssueType issueType, NotificationType type)
        {
            List<int> idIssueTypeList = new List<int>();
            if (type == NotificationType.Issue)
                idIssueTypeList = issueIdIssueTypeOptions;           

            if (idIssueTypeList != null && issueType != null && idIssueTypeList.Contains(issueType.IdIssueType))
                return true;
            else
                return false;
        }
        #endregion
        #region set
        public void SetNotify(LocationOptions notification)
        {
            if (notification == LocationOptions.None)
                locationOptions = 0;
            else
                locationOptions |= (int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION, locationOptions);
            OpOperatorData odItem = owner.DataList.FirstOrDefault(d => d.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION);
            if (odItem != null)
                odItem.Distributor = owner.Distributor;
        }

        public void SetNotify(IssueOptions notification)
        {
            if (notification == IssueOptions.None)
                issueOptions = 0;
            else
                issueOptions |= (int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE, issueOptions);
            OpOperatorData odItem = owner.DataList.FirstOrDefault(d => d.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE);
            if (odItem != null)
                odItem.Distributor = owner.Distributor;
        }

        public void SetNotify(TaskOptions notification)
        {
            if (notification == TaskOptions.None)
                taskOptions = 0;
            else
                taskOptions |= (int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK, taskOptions);
            OpOperatorData odItem = owner.DataList.FirstOrDefault(d => d.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK);
            if (odItem != null)
                odItem.Distributor = owner.Distributor;
        }

        public void SetNotify(WorkOrderOptions notification)
        {
            if (notification == WorkOrderOptions.None)
                workOrderOptions = 0;
            else
                workOrderOptions |= (int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ROUTE, workOrderOptions);
            OpOperatorData odItem = owner.DataList.FirstOrDefault(d => d.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ROUTE);
            if (odItem != null)
                odItem.Distributor = owner.Distributor;
        }

        public void SetNotify(PackageOptions notification)
        {
            if (notification == PackageOptions.None)
                packageOptions = 0;
            else
                packageOptions |= (int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_PACKAGE, packageOptions);
            OpOperatorData odItem = owner.DataList.FirstOrDefault(d => d.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_PACKAGE);
            if (odItem != null)
                odItem.Distributor = owner.Distributor;
        }

        public void SetNotify(List<OpDistributor> distributor, NotificationType type)
        {
            long idDataType = 0;
            if (type == NotificationType.ShippingList)
                idDataType = DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_SHIPPING_LIST_CLOSE;
            else if (type == NotificationType.Task)
                idDataType = DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK_ID_DISTRIBUTOR;
            else if (type == NotificationType.Issue)
                idDataType = DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR;
            else if (type == NotificationType.Location)
                idDataType = DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION_ID_DISTRIBUTOR;
            if (idDataType > 0)
            {
                if (distributor != null && distributor.Count > 0)
                {
                    owner.DataList.SetValue(idDataType, String.Join(",", distributor.Select(d => d.IdDistributor).ToArray()));
                }
                else
                {
                    if (owner.DataList.Exists(d => d.IdDataType == idDataType))
                        owner.DataList.Find(d => d.IdDataType == idDataType).OpState = OpChangeState.Delete;
                }
                OpOperatorData odItem = owner.DataList.FirstOrDefault(d => d.IdDataType == idDataType);
                if (odItem != null)
                    odItem.Distributor = owner.Distributor;
            }
        }

        public void SetNotify(List<OpIssueType> issueType, NotificationType type)
        {
            long idDataType = 0;
            if (type == NotificationType.Issue)
                idDataType = DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE;
            
            if (idDataType > 0)
            {
                if (issueType != null && issueType.Count > 0)
                {
                    owner.DataList.SetValue(idDataType, String.Join(",", issueType.Select(d => d.IdIssueType).ToArray()));
                }
                else
                {
                    if (owner.DataList.Exists(d => d.IdDataType == idDataType))
                        owner.DataList.Find(d => d.IdDataType == idDataType).OpState = OpChangeState.Delete;
                }
                OpOperatorData odItem = owner.DataList.FirstOrDefault(d => d.IdDataType == idDataType);
                if (odItem != null)
                    odItem.Distributor = owner.Distributor;
            }
        }
        #endregion
        #region reset
        public void ResetNotify(LocationOptions notification)
        {
            locationOptions &= ~(int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION, locationOptions);
        }

        public void ResetNotify(IssueOptions notification)
        {
            issueOptions &= ~(int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE, issueOptions);
        }

        public void ResetNotify(TaskOptions notification)
        {
            taskOptions &= ~(int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK, taskOptions);
        }

        public void ResetNotify(WorkOrderOptions notification)
        {
            workOrderOptions &= ~(int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ROUTE, workOrderOptions);
        }

        public void ResetNotify(PackageOptions notification)
        {
            packageOptions &= ~(int)notification;
            owner.DataList.SetValue(DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_PACKAGE, packageOptions);
        }
    #endregion
    #region List options
        private static List<object> locationOptionsList = null;
        public static List<object> GetLocationOptions()
        {
            if (locationOptionsList == null)
            {
                locationOptionsList = new List<object>();
                foreach (LocationOptions opt in Enum.GetValues(typeof(LocationOptions)))
                {
                    locationOptionsList.Add(opt);
                }
            }
            return locationOptionsList;
        }

        private static List<object> issueOptionsList = null;
        public static List<object> GetIssueOptions()
        {
            if (issueOptionsList == null)
            {
                issueOptionsList = new List<object>();
                foreach (IssueOptions opt in Enum.GetValues(typeof(IssueOptions)))
                {
                    issueOptionsList.Add(opt);
                }
            }
            return issueOptionsList;
        }

        private static List<object> taskOptionsList = null;
        public static List<object> GetTaskOptions()
        {
            if (taskOptionsList == null)
            {
                taskOptionsList = new List<object>();
                foreach (TaskOptions opt in Enum.GetValues(typeof(TaskOptions)))
                {
                    taskOptionsList.Add(opt);
                }
            }
            return taskOptionsList;
        }

        private static List<object> workOrderOptionsList = null;
        public static List<object> GetWorkOrderOptions()
        {
            if (workOrderOptionsList == null)
            {
                workOrderOptionsList = new List<object>();
                foreach (WorkOrderOptions opt in Enum.GetValues(typeof(WorkOrderOptions)))
                {
                    workOrderOptionsList.Add(opt);
                }
            }
            return workOrderOptionsList;
        }

        private static List<object> packageList = null;
        public static List<object> GetPackageOptions()
        {
            if (packageList == null)
            {
                packageList = new List<object>();
                foreach (PackageOptions opt in Enum.GetValues(typeof(PackageOptions)))
                {
                    packageList.Add(opt);
                }
            }
            return packageList;
        }

        public List<int> GetShippingListCloseOptions()
        {
            return shippingListCloseOptions;
        }

        public List<int> GetTaskIdDistributorOptions()
        {
            return taskIdDistributorOptions;
        }

        public List<int> GetIssueIdDistributorOptions()
        {
            return issueIdDistributorOptions;
        }

        public List<int> GetLocationIdDistributorOptions()
        {
            return locationIdDistributorOptions;
        }

        public List<int> GetIssueIdIssueTypeOptions()
        {
            return issueIdIssueTypeOptions;
        }

        #endregion
        #region Status
        public static Enums.LocationState OptionsToStatus(LocationOptions opt)
        {
            switch (opt)
            {
                case LocationOptions.State_NEW:
                    return Enums.LocationState.NEW;
                case LocationOptions.State_SCHEDULED:
                    return Enums.LocationState.SCHEDULED;
                case LocationOptions.State_INTERVENTION:
                    return Enums.LocationState.INTERVENTION;
                case LocationOptions.State_PENDING:
                    return Enums.LocationState.PENDING;
                case LocationOptions.State_OPERATIONAL:
                    return Enums.LocationState.OPERATIONAL;
                case LocationOptions.State_SUSPENDED:
                    return Enums.LocationState.SUSPENDED;
                case LocationOptions.State_UNUSED:
                    return Enums.LocationState.UNUSED;
                case LocationOptions.State_DELETED:
                    return Enums.LocationState.DELETED;
                case LocationOptions.State_APPROVED:
                    return Enums.LocationState.APPROVED;
                default:
                    throw new ArgumentException("No OpLocationStateType for LocationOptions: " + opt);
            }
        }

        public static OpIssueStatus.Enum OptionsToStatus(IssueOptions opt)
        {
            switch (opt)
            {
                case IssueOptions.Status_ACCEPTED:
                    return OpIssueStatus.Enum.ACCEPTED;
                case IssueOptions.Status_FINISHED:
                    return OpIssueStatus.Enum.FINISHED;
                case IssueOptions.Status_IN_PROGRESS:
                    return OpIssueStatus.Enum.IN_PROGRESS;
                case IssueOptions.Status_NEW:
                    return OpIssueStatus.Enum.NEW;
                case IssueOptions.Status_QUEUED:
                    return OpIssueStatus.Enum.QUEUED;
                case IssueOptions.Status_WAITING_FOR_RESPONSE:
                    return OpIssueStatus.Enum.WAITING_FOR_RESPONSE;
                case IssueOptions.Status_SUSPENDED:
                    return OpIssueStatus.Enum.SUSPENDED;
                case IssueOptions.Status_REOPENED:
                    return OpIssueStatus.Enum.REOPENED;
                default:
                    throw new ArgumentException("No IssueStatus for IssueOption: " + opt);
            }
        }
        public static OpTaskStatus.Enum OptionsToStatus(TaskOptions opt)
        {
            switch (opt)
            {
                case TaskOptions.Status_Canceled:
                    return OpTaskStatus.Enum.Canceled;
                case TaskOptions.Status_Finished_succesfully:
                    return OpTaskStatus.Enum.Finished_succesfully;
                case TaskOptions.Status_Finished_with_error:
                    return OpTaskStatus.Enum.Finished_with_error;
                case TaskOptions.Status_In_progress:
                    return OpTaskStatus.Enum.In_progress;
                case TaskOptions.Status_Not_Started:
                    return OpTaskStatus.Enum.Not_Started;
                case TaskOptions.Status_Planned:
                    return OpTaskStatus.Enum.Planned;
                case TaskOptions.Status_WaitingForAcceptance:
                    return OpTaskStatus.Enum.WaitingForAcceptance;
                case TaskOptions.Status_Suspended:
                    return OpTaskStatus.Enum.Suspended;
                case TaskOptions.Status_Verified:
                    return OpTaskStatus.Enum.Verified;
                case TaskOptions.Status_RealizationNotPossible:
                    return OpTaskStatus.Enum.RealizationNotPossible;
                case TaskOptions.Status_Rejected:
                    return OpTaskStatus.Enum.Rejected;
                default:
                    throw new ArgumentException("No TaskStatus for TaskOptions: " + opt);
            }
        }
        public static OpRouteStatus.Enum OptionsToStatus(WorkOrderOptions opt)
        {
            switch (opt)
            {
                case WorkOrderOptions.Status_Accepted:
                    return OpRouteStatus.Enum.Accepted;
                case WorkOrderOptions.Status_Finished:
                    return OpRouteStatus.Enum.Finished;
                case WorkOrderOptions.Status_InProgress:
                    return OpRouteStatus.Enum.InProgress;
                case WorkOrderOptions.Status_Planned:
                    return OpRouteStatus.Enum.Planned;
                default:
                    throw new ArgumentException("No RouteStatus for WorkOrderOptions: " + opt);
            }
        }
        public static Enums.PackageStatus OptionsToStatus(PackageOptions opt)
        {
            switch (opt)
            {
                case PackageOptions.Status_New:
                    return Enums.PackageStatus.New;
                case PackageOptions.Status_Sent:
                    return Enums.PackageStatus.Sent;
                case PackageOptions.Status_Wrong:
                    return Enums.PackageStatus.Wrong;
                case PackageOptions.Status_Accepted:
                    return Enums.PackageStatus.Accepted;
                case PackageOptions.Status_Distributed:
                    return Enums.PackageStatus.Distributed;
                case PackageOptions.Status_Removed:
                    return Enums.PackageStatus.Removed;
                case PackageOptions.Status_NewOrder:
                    return Enums.PackageStatus.NewOrder;
                case PackageOptions.Status_Ordered:
                    return Enums.PackageStatus.Ordered;
                case PackageOptions.Status_ToCompletion:
                    return Enums.PackageStatus.ToCompletion;
                case PackageOptions.Status_InCompletion:
                    return Enums.PackageStatus.InCompletion;
                default:
                    throw new ArgumentException("No RouteStatus for WorkOrderOptions: " + opt);
            }
        }
        public static IssueOptions StatusToOptions(OpIssueStatus.Enum stat)
        {
            switch (stat)
            {
                case OpIssueStatus.Enum.ACCEPTED:
                    return IssueOptions.Status_ACCEPTED;
                case OpIssueStatus.Enum.FINISHED:
                    return IssueOptions.Status_FINISHED;
                case OpIssueStatus.Enum.IN_PROGRESS:
                    return IssueOptions.Status_IN_PROGRESS;
                case OpIssueStatus.Enum.NEW:
                    return IssueOptions.Status_NEW;
                case OpIssueStatus.Enum.QUEUED:
                    return IssueOptions.Status_QUEUED;
                case OpIssueStatus.Enum.WAITING_FOR_RESPONSE:
                    return IssueOptions.Status_WAITING_FOR_RESPONSE;
                case OpIssueStatus.Enum.SUSPENDED:
                    return IssueOptions.Status_SUSPENDED;
                case OpIssueStatus.Enum.REOPENED:
                    return IssueOptions.Status_REOPENED;
                default:
                    return (IssueOptions)0;
            }
        }
        public static LocationOptions StatusToOptions(Enums.LocationState stat)
        {
            switch (stat)
            {
                case Enums.LocationState.NEW:
                    return LocationOptions.State_NEW;
                case Enums.LocationState.SCHEDULED:
                    return LocationOptions.State_SCHEDULED;
                case Enums.LocationState.INTERVENTION:
                    return LocationOptions.State_INTERVENTION;
                case Enums.LocationState.PENDING:
                    return LocationOptions.State_PENDING;
                case Enums.LocationState.OPERATIONAL:
                    return LocationOptions.State_OPERATIONAL;
                case Enums.LocationState.SUSPENDED:
                    return LocationOptions.State_SUSPENDED;
                case Enums.LocationState.UNUSED:
                    return LocationOptions.State_UNUSED;
                case Enums.LocationState.DELETED:
                    return LocationOptions.State_DELETED;
                case Enums.LocationState.APPROVED:
                    return LocationOptions.State_APPROVED;
                default:
                    return (LocationOptions)0;
            }
        }
        public static TaskOptions StatusToOptions(OpTaskStatus.Enum stat)
        {
            switch (stat)
            {
                case OpTaskStatus.Enum.Canceled:
                    return TaskOptions.Status_Canceled;
                case OpTaskStatus.Enum.Finished_succesfully:
                    return TaskOptions.Status_Finished_succesfully;
                case OpTaskStatus.Enum.Finished_with_error:
                    return TaskOptions.Status_Finished_with_error;
                case OpTaskStatus.Enum.In_progress:
                    return TaskOptions.Status_In_progress;
                case OpTaskStatus.Enum.Not_Started:
                    return TaskOptions.Status_Not_Started;
                case OpTaskStatus.Enum.Planned:
                    return TaskOptions.Status_Planned;
                case OpTaskStatus.Enum.WaitingForAcceptance:
                    return TaskOptions.Status_WaitingForAcceptance;
                case OpTaskStatus.Enum.Suspended:
                    return TaskOptions.Status_Suspended;
                case OpTaskStatus.Enum.Verified:
                    return TaskOptions.Status_Verified;
                case OpTaskStatus.Enum.RealizationNotPossible:
                    return TaskOptions.Status_RealizationNotPossible;
                case OpTaskStatus.Enum.Rejected:
                    return TaskOptions.Status_Rejected;
                default:
                    return (TaskOptions)0;
            }
        }
        public static WorkOrderOptions StatusToOptions(OpRouteStatus.Enum stat)
        {
            switch (stat)
            {
                case OpRouteStatus.Enum.Accepted:
                    return WorkOrderOptions.Status_Accepted;
                case OpRouteStatus.Enum.Finished:
                    return WorkOrderOptions.Status_Finished;
                case OpRouteStatus.Enum.InProgress:
                    return WorkOrderOptions.Status_InProgress;
                case OpRouteStatus.Enum.Planned:
                    return WorkOrderOptions.Status_Planned;
                default:
                    return (WorkOrderOptions)0;
            }
        }
        public static PackageOptions StatusToOptions(Enums.PackageStatus stat)
        {
            switch (stat)
            {
                case Enums.PackageStatus.New:
                    return PackageOptions.Status_New;
                case Enums.PackageStatus.Sent:
                    return PackageOptions.Status_Sent;
                case Enums.PackageStatus.Wrong:
                    return PackageOptions.Status_Wrong;
                case Enums.PackageStatus.Accepted:
                    return PackageOptions.Status_Accepted;
                case Enums.PackageStatus.Distributed:
                    return PackageOptions.Status_Distributed;
                case Enums.PackageStatus.Removed:
                    return PackageOptions.Status_Removed;
                case Enums.PackageStatus.NewOrder:
                    return PackageOptions.Status_NewOrder;
                case Enums.PackageStatus.Ordered:
                    return PackageOptions.Status_Ordered;
                case Enums.PackageStatus.ToCompletion:
                    return PackageOptions.Status_ToCompletion;
                case Enums.PackageStatus.InCompletion:
                    return PackageOptions.Status_InCompletion;
                default:
                    return (PackageOptions)0;
            }
        }
    #endregion
    }
    #endregion
#endif
}
