﻿using System;
using System.Data;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;
using DW = IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.UI.Business.Objects
{
    public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region GetAlarmFilter
        /// <summary>
        /// Gets Alarm list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarm funcion</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="IdDataTypeAlarm">Specifies filter for ID_DATA_TYPE_ALARM column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="TransmissionType">Specifies filter for TRANSMISSION_TYPE column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Alarm list</returns>
        List<DW.OpAlarm> GetAlarmFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarm = null, long[] IdAlarmEvent = null, int[] IdAlarmDef = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null,
                            int[] IdAlarmType = null, long[] IdDataTypeAlarm = null, int[] IdOperator = null, int[] IdAlarmStatus = null, string TransmissionType = null,
                            int[] IdTransmissionType = null, int[] IdAlarmGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmEventFilterDW

        /// <summary>
        /// Gets AlarmEvent list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmEvent funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="IdDataTypeAlarm">Specifies filter for ID_DATA_TYPE_ALARM column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IsConfirmed">Specifies filter for IS_CONFIRMED column</param>
        /// <param name="ConfirmedBy">Specifies filter for CONFIRMED_BY column</param>
        /// <param name="ConfirmTime">Specifies filter for CONFIRM_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmEventDW list</returns>
        List<DW.OpAlarmEvent> GetAlarmEventFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, long[] IdAlarmDef = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdAlarmType = null,
                            int[] IdDataTypeAlarm = null, TypeDateTimeCode Time = null, long[] IdIssue = null, bool? IsConfirmed = null, int[] ConfirmedBy = null,
                            TypeDateTimeCode ConfirmTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetAlarmHistoryFilter
        /// <summary>
        /// Gets AlarmHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmHistory funcion</param>
        /// <param name="IdAlarmHistory">Specifies filter for ID_ALARM_HISTORY column</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmHistory list</returns>
        List<DW.OpAlarmHistory> GetAlarmHistoryFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmHistory = null, long[] IdAlarm = null, int[] IdAlarmStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmMessageFilter
        /// <summary>
        /// Gets AlarmMessage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmMessage funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="AlarmMessage">Specifies filter for ALARM_MESSAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmMessage list</returns>
        List<DW.OpAlarmMessage> GetAlarmMessageFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, int[] IdLanguage = null, string AlarmMessage = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion

        #region GetAuditFilterDW

        /// <summary>
        /// Gets Audit list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAudit funcion</param>
        /// <param name="IdAudit">Specifies filter for ID_AUDIT column</param>
        /// <param name="BatchId">Specifies filter for BATCH_ID column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="Key1">Specifies filter for KEY1 column</param>
        /// <param name="Key2">Specifies filter for KEY2 column</param>
        /// <param name="Key3">Specifies filter for KEY3 column</param>
        /// <param name="Key4">Specifies filter for KEY4 column</param>
        /// <param name="Key5">Specifies filter for KEY5 column</param>
        /// <param name="Key6">Specifies filter for KEY6 column</param>
        /// <param name="ColumnName">Specifies filter for COLUMN_NAME column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="Host">Specifies filter for HOST column</param>
        /// <param name="Application">Specifies filter for APPLICATION column</param>
        /// <param name="Contextinfo">Specifies filter for CONTEXTINFO column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_AUDIT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Audit list</returns>
        List<OpAudit> GetAuditFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAudit = null, long[] BatchId = null, int[] ChangeType = null, string TableName = null, long[] Key1 = null, long[] Key2 = null,
                            long[] Key3 = null, long[] Key4 = null, long[] Key5 = null, long[] Key6 = null, string ColumnName = null,
                            TypeDateTimeCode Time = null, string User = null, string Host = null, string Application = null, string Contextinfo = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion

        #region GetConsumerTransactionFilter

        /// <summary>
        /// Gets ConsumerTransaction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransaction funcion</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdTransactionType">Specifies filter for ID_TRANSACTION_TYPE column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="Timestamp">Specifies filter for TIMESTAMP column</param>
        /// <param name="ChargePrepaid">Specifies filter for CHARGE_PREPAID column</param>
        /// <param name="ChargeEc">Specifies filter for CHARGE_EC column</param>
        /// <param name="ChargeOwed">Specifies filter for CHARGE_OWED column</param>
        /// <param name="BalancePrepaid">Specifies filter for BALANCE_PREPAID column</param>
        /// <param name="BalanceEc">Specifies filter for BALANCE_EC column</param>
        /// <param name="BalanceOwed">Specifies filter for BALANCE_OWED column</param>
        /// <param name="MeterIndex">Specifies filter for METER_INDEX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransaction list</returns>
        List<DW.OpConsumerTransaction> GetConsumerTransactionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerTransaction = null, int[] IdConsumer = null, int[] IdOperator = null, int[] IdDistributor = null, int[] IdTransactionType = null, int[] IdTariff = null,
                            TypeDateTimeCode Timestamp = null, Double[] ChargePrepaid = null, Double[] ChargeEc = null, Double[] ChargeOwed = null, Double[] BalancePrepaid = null,
                            Double[] BalanceEc = null, Double[] BalanceOwed = null, Double[] MeterIndex = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                            long[] IdConsumerSettlement = null, int[] IdModule = null);
        #endregion
        #region GetConsumerTransactionDataFilter

        /// <summary>
        /// Gets ConsumerTransactionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransactionData funcion</param>
        /// <param name="IdConsumerTransactionData">Specifies filter for ID_CONSUMER_TRANSACTION_DATA column</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransactionData list</returns>
        List<DW.OpConsumerTransactionData> GetConsumerTransactionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTransactionData = null, int[] IdConsumerTransaction = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataSourceFilter

        /// <summary>
        /// Gets DataSource list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataSource funcion</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_SOURCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataSource list</returns>
        List<OpDataSource> GetDataSourceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataSource = null, int[] IdDataSourceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataSourceTypeFilter

        /// <summary>
        /// Gets DataSourceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataSourceType funcion</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_SOURCE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataSourceType list</returns>
        List<OpDataSourceType> GetDataSourceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataSourceType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataTemporalFilter

        /// <summary>
        /// Gets DataTemporal list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTemporal funcion</param>
        /// <param name="IdDataTemporal">Specifies filter for ID_DATA_TEMPORAL column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TEMPORAL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTemporal list</returns>
        List<OpDataTemporal> GetDataTemporalFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataTemporal = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, int[] IdAggregationType = null, long[] IdDataSource = null, int[] IdDataSourceType = null,
                            long[] IdAlarmEvent = null, int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeliveryAdviceFilter

        /// <summary>
        /// Gets DeliveryAdvice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryAdvice funcion</param>
        /// <param name="IdDeliveryAdvice">Specifies filter for ID_DELIVERY_ADVICE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_ADVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryAdvice list</returns>
        List<OpDeliveryAdvice> GetDeliveryAdviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryAdvice = null, string Name = null, long[] IdDescr = null, int[] Priority = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeliveryBatchFilter

        /// <summary>
        /// Gets DeliveryBatch list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryBatch funcion</param>
        /// <param name="IdDeliveryBatch">Specifies filter for ID_DELIVERY_BATCH column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDeliveryBatchStatus">Specifies filter for ID_DELIVERY_BATCH_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="CommitDate">Specifies filter for COMMIT_DATE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="SuccessTotal">Specifies filter for SUCCESS_TOTAL column</param>
        /// <param name="FailedTotal">Specifies filter for FAILED_TOTAL column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_BATCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryBatch list</returns>
        List<OpDeliveryBatch> GetDeliveryBatchFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryBatch = null, TypeDateTimeCode CreationDate = null, string Name = null, int[] IdDeliveryBatchStatus = null, int[] IdOperator = null, TypeDateTimeCode CommitDate = null,
                            int[] IdDistributor = null, int[] SuccessTotal = null, int[] FailedTotal = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeliveryBatchStatusFilter

        /// <summary>
        /// Gets DeliveryBatchStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryBatchStatus funcion</param>
        /// <param name="IdDeliveryBatchStatus">Specifies filter for ID_DELIVERY_BATCH_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_BATCH_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryBatchStatus list</returns>
        List<OpDeliveryBatchStatus> GetDeliveryBatchStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryBatchStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeliveryOrderFilter

        /// <summary>
        /// Gets DeliveryOrder list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryOrder funcion</param>
        /// <param name="IdDeliveryOrder">Specifies filter for ID_DELIVERY_ORDER column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="IdDeliveryBatch">Specifies filter for ID_DELIVERY_BATCH column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDeliveryAdvice">Specifies filter for ID_DELIVERY_ADVICE column</param>
        /// <param name="DeliveryVolume">Specifies filter for DELIVERY_VOLUME column</param>
        /// <param name="IsSuccesfull">Specifies filter for IS_SUCCESFULL column</param>
        /// <param name="OrderNo">Specifies filter for ORDER_NO column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_ORDER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryOrder list</returns>
        List<OpDeliveryOrder> GetDeliveryOrderFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryOrder = null, TypeDateTimeCode CreationDate = null, int[] IdDeliveryBatch = null, long[] IdLocation = null, long[] IdMeter = null, int[] IdOperator = null,
                            int[] IdDeliveryAdvice = null, int[] DeliveryVolume = null, bool? IsSuccesfull = null, string OrderNo = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeliveryOrderHistoryFilter

        /// <summary>
        /// Gets DeliveryOrderHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryOrderHistory funcion</param>
        /// <param name="IdDeliveryOrderHistory">Specifies filter for ID_DELIVERY_ORDER_HISTORY column</param>
        /// <param name="IdDeliveryOrder">Specifies filter for ID_DELIVERY_ORDER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IsSuccesfull">Specifies filter for IS_SUCCESFULL column</param>
        /// <param name="CommitDate">Specifies filter for COMMIT_DATE column</param>
        /// <param name="CommitResult">Specifies filter for COMMIT_RESULT column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_ORDER_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryOrderHistory list</returns>
        List<OpDeliveryOrderHistory> GetDeliveryOrderHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeliveryOrderHistory = null, int[] IdDeliveryOrder = null, int[] IdOperator = null, bool? IsSuccesfull = null, TypeDateTimeCode CommitDate = null, string CommitResult = null,
                            string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceConnectionFilter

        /// <summary>
        /// Gets DeviceConnection list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceConnection funcion</param>
        /// <param name="IdDeviceConnection">Specifies filter for ID_DEVICE_CONNECTION column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="SerialNbrParent">Specifies filter for SERIAL_NBR_PARENT column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_CONNECTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceConnection list</returns>
        List<OpDeviceConnection> GetDeviceConnectionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceConnection = null, long[] SerialNbr = null, long[] SerialNbrParent = null, TypeDateTimeCode Time = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetDeviceScheduleFilter

        /// <summary>
        /// Gets DeviceSchedule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceSchedule funcion</param>
        /// <param name="IdDeviceSchedule">Specifies filter for ID_DEVICE_SCHEDULE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="DayOfMonth">Specifies filter for DAY_OF_MONTH column</param>
        /// <param name="DayOfWeek">Specifies filter for DAY_OF_WEEK column</param>
        /// <param name="HourOfDay">Specifies filter for HOUR_OF_DAY column</param>
        /// <param name="MinuteOfHour">Specifies filter for MINUTE_OF_HOUR column</param>
        /// <param name="CommandCode">Specifies filter for COMMAND_CODE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_SCHEDULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceSchedule list</returns>
        List<OpDeviceSchedule> GetDeviceScheduleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceSchedule = null, long[] SerialNbr = null, int[] DayOfMonth = null, int[] DayOfWeek = null, int[] HourOfDay = null, int[] MinuteOfHour = null,
                            int[] CommandCode = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDistributorPerformanceFilter

        /// <summary>
        /// Gets DistributorPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorPerformance funcion</param>
        /// <param name="IdDistributorPerformance">Specifies filter for ID_DISTRIBUTOR_PERFORMANCE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorPerformance list</returns>
        List<OpDistributorPerformance> GetDistributorPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDistributorPerformance = null, int[] IdDistributor = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetEtlFilter

        /// <summary>
        /// Gets Etl list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllEtl funcion</param>
        /// <param name="IdParam">Specifies filter for ID_PARAM column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PARAM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Etl list</returns>
        //List<DW.OpEtl> GetEtlFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdParam = null, string Code = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        List<OpEtl> GetEtlFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdParam = null, string Code = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetEtlPerformanceFilter

        /// <summary>
        /// Gets EtlPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllEtlPerformance funcion</param>
        /// <param name="IdEtlPerformance">Specifies filter for ID_ETL_PERFORMANCE column</param>
        /// <param name="IdEtl">Specifies filter for ID_ETL column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ETL_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>EtlPerformance list</returns>
        List<OpEtlPerformance> GetEtlPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdEtlPerformance = null, int[] IdEtl = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, int[] IdDataSourceType = null, string Code = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetImrServerPerformanceFilter

        /// <summary>
        /// Gets ImrServerPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllImrServerPerformance funcion</param>
        /// <param name="IdImrServerPerformance">Specifies filter for ID_IMR_SERVER_PERFORMANCE column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_IMR_SERVER_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ImrServerPerformance list</returns>
        List<OpImrServerPerformance> GetImrServerPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdImrServerPerformance = null, int[] IdImrServer = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationKpiFilter

        /// <summary>
        /// Gets LocationKpi list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRefuel funcion</param>
        /// <param name="IdLocationKpi">Specifies filter for ID_LOCATION_KPI column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="New">Specifies filter for NEW column</param>
        /// <param name="Operational">Specifies filter for OPERATIONAL column</param>
        /// <param name="Suspended">Specifies filter for SUSPENDED column</param>
        /// <param name="Pending">Specifies filter for PENDING column</param>
        /// <param name="KpiRelevant">Specifies filter for KPI_RELEVANT column</param>
        /// <param name="KpiIrrelevant">Specifies filter for KPI_IRRELEVANT column</param>
        /// <param name="KpiConforming">Specifies filter for KPI_CONFORMING column</param>
        /// <param name="KpiNonConforming">Specifies filter for KPI_NONCONFORMING column</param>
        /// <param name="SrtConforming">Specifies filter for SRT_CONFORMING column</param>
        /// <param name="IsMantenanceOnly">Specifies filter for IS_MAINTENANCE_ONLY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFUEL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Refuel list</returns>
        List<OpLocationKpi> GetLocationKpiFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false,
            long[] IdLocationKpi = null, int[] IdDistributor = null, TypeDateTimeCode Date = null,
            int[] New = null, int[] Operational = null, int[] Suspended = null, int[] Pending = null,
            int[] KpiRelevant = null, int[] KpiIrrelevant = null, long[] KpiConforming = null, long[] KpiNonConforming = null, int[] SrtConforming = null, bool? IsMantenanceOnly = null,
            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationDistanceFilter

        /// <summary>
        /// Gets LocationDistance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationDistance funcion</param>
        /// <param name="IdLocationOrigin">Specifies filter for ID_LOCATION_ORIGIN column</param>
        /// <param name="IdLocationDest">Specifies filter for ID_LOCATION_DEST column</param>
        /// <param name="DistanceStraight">Specifies filter for DISTANCE_STRAIGHT column</param>
        /// <param name="DistanceFormula">Specifies filter for DISTANCE_FORMULA column</param>
        /// <param name="DistanceGoogle">Specifies filter for DISTANCE_GOOGLE column</param>
        /// <param name="LastGoogleUpdate">Specifies filter for LAST_GOOGLE_UPDATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_ORIGIN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationDistance list</returns>
        List<OpLocationDistance> GetLocationDistanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationOrigin = null, long[] IdLocationDest = null, Double[] DistanceStraight = null, Double[] DistanceFormula = null, Double[] DistanceGoogle = null, TypeDateTimeCode LastGoogleUpdate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMappingFilterDW

        /// <summary>
        /// Gets Mapping list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMapping funcion</param>
        /// <param name="IdMapping">Specifies filter for ID_MAPPING column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdSource">Specifies filter for ID_SOURCE column</param>
        /// <param name="IdDestination">Specifies filter for ID_DESTINATION column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="Timestamp">Specifies filter for TIMESTAMP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MAPPING DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Mapping list</returns>
        List<DW.OpMapping> GetMappingFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMapping = null, int[] IdReferenceType = null, long[] IdSource = null, long[] IdDestination = null, int[] IdImrServer = null, TypeDateTimeCode Timestamp = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetModuleHistoryFilter

        /// <summary>
        /// Gets ModuleHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllModuleHistory funcion</param>
        /// <param name="IdModuleHistory">Specifies filter for ID_MODULE_HISTORY column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="CommonName">Specifies filter for COMMON_NAME column</param>
        /// <param name="Version">Specifies filter for VERSION column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdOperatorUpgrade">Specifies filter for ID_OPERATOR_UPGRADE column</param>
        /// <param name="IdOperatorAccept">Specifies filter for ID_OPERATOR_ACCEPT column</param>
        /// <param name="UpgradeDescription">Specifies filter for UPGRADE_DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MODULE_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ModuleHistory list</returns>
        List<OpModuleHistory> GetModuleHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdModuleHistory = null, int[] IdModule = null, string CommonName = null, string Version = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            int[] IdOperatorUpgrade = null, int[] IdOperatorAccept = null, string UpgradeDescription = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMonitoringWatcherFilterDW

        /// <summary>
        /// Gets MonitoringWatcher list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcher funcion</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcher list</returns>
        List<DW.OpMonitoringWatcher> GetMonitoringWatcherFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcher = null, int[] IdMonitoringWatcherType = null, bool? IsActive = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion        
        #region GetMonitoringWatcherDataFilterDW

        /// <summary>
        /// Gets MonitoringWatcherData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherData funcion</param>
        /// <param name="IdMonitoringWatcherData">Specifies filter for ID_MONITORING_WATCHER_DATA column</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Index">Specifies filter for INDEX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherData list</returns>
        List<DW.OpMonitoringWatcherData> GetMonitoringWatcherDataFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherData = null, int[] IdMonitoringWatcher = null, long[] IdDataType = null, int[] Index = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetMonitoringWatcherTypeFilterDW

        /// <summary>
        /// Gets MonitoringWatcherType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherType funcion</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Plugin">Specifies filter for PLUGIN column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherType list</returns>
        List<DW.OpMonitoringWatcherType> GetMonitoringWatcherTypeFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherType = null, string Name = null, string Plugin = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetProblemClassFilter

        /// <summary>
        /// Gets ProblemClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProblemClass funcion</param>
        /// <param name="IdProblemClass">Specifies filter for ID_PROBLEM_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROBLEM_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProblemClass list</returns>
        List<OpProblemClass> GetProblemClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProblemClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRefuelDataFilter

        /// <summary>
        /// Gets RefuelData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRefuelData funcion</param>
        /// <param name="IdRefuelData">Specifies filter for ID_REFUEL_DATA column</param>
        /// <param name="IdRefuel">Specifies filter for ID_REFUEL column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFUEL_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RefuelData list</returns>
        List<OpRefuelData> GetRefuelDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRefuelData = null, long[] IdRefuel = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetReportFilter

        /// <summary>
        /// Gets Report list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReport funcion</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="IdReportType">Specifies filter for ID_REPORT_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Report list</returns>
        List<OpReport> GetReportFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdReport = null, int[] IdReportType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
            bool loadCustomData = true, List<long> customDataTypes = null);
        #endregion
        #region GetReportDataFilter

        /// <summary>
        /// Gets ReportData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportData funcion</param>
        /// <param name="IdReportData">Specifies filter for ID_REPORT_DATA column</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportData list</returns>
        List<OpReportData> GetReportDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdReportData = null, int[] IdReport = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetReportParamFilter

        /// <summary>
        /// Gets ReportParam list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportParam funcion</param>
        /// <param name="IdReportParam">Specifies filter for ID_REPORT_PARAM column</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="FieldName">Specifies filter for FIELD_NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdReference">Specifies filter for ID_REFERENCE column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="IsInputParam">Specifies filter for IS_INPUT_PARAM column</param>
        /// <param name="IsVisible">Specifies filter for IS_VISIBLE column</param>
        /// <param name="ColorBg">Specifies filter for COLOR_BG column</param>
        /// <param name="Format">Specifies filter for FORMAT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_PARAM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportParam list</returns>
        List<OpReportParam> GetReportParamFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdReportParam = null, int[] IdReport = null, string FieldName = null, long[] IdDescr = null, int[] IdUnit = null, string Name = null,
                            int[] IdReference = null, int[] IdDataTypeClass = null, bool? IsInputParam = null, bool? IsVisible = null, string ColorBg = null,
                            string Format = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetReportPerformanceFilter

        /// <summary>
        /// Gets ReportPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportPerformance funcion</param>
        /// <param name="IdReportPerformance">Specifies filter for ID_REPORT_PERFORMANCE column</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportPerformance list</returns>
        List<OpReportPerformance> GetReportPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdReportPerformance = null, int[] IdReport = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, int[] IdDataSourceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetReportTypeFilter

        /// <summary>
        /// Gets ReportType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportType funcion</param>
        /// <param name="IdReportType">Specifies filter for ID_REPORT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportType list</returns>
        List<OpReportType> GetReportTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdReportType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataMissingAnalogReadoutsFilter

        /// <summary>
        /// Gets DataMissingAnalogReadouts list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataMissingAnalogReadouts funcion</param>
        /// <param name="IdDataMissingAnalogReadouts">Specifies filter for ID_DATA_MISSING_ANALOG_READOUTS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="SerialNbrParent">Specifies filter for SERIAL_NBR_PARENT column</param>
        /// <param name="MissingDataStartTime">Specifies filter for MISSING_DATA_START_TIME column</param>
        /// <param name="PacketsCount">Specifies filter for PACKETS_COUNT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_MISSING_ANALOG_READOUTS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataMissingAnalogReadouts list</returns>
        List<OpDataMissingAnalogReadouts> GetDataMissingAnalogReadoutsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataMissingAnalogReadouts = null, long[] SerialNbr = null, long[] SerialNbrParent = null, TypeDateTimeCode MissingDataStartTime = null, int[] PacketsCount = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPacketHourTransmissionDriverFilter

        /// <summary>
        /// Gets PacketHourTransmissionDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketHourTransmissionDriver funcion</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketHourTransmissionDriver list</returns>
        List<OpPacketHourTransmissionDriver> GetPacketHourTransmissionDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, int[] IdTransmissionType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPacketHourAddressFilter

        /// <summary>
        /// Gets PacketHourAddress list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketHourAddress funcion</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ADDRESS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketHourAddress list</returns>
        List<OpPacketHourAddress> GetPacketHourAddressFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, string Address = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPacketHourDeviceFilter

        /// <summary>
        /// Gets PacketHourDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketHourDevice funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketHourDevice list</returns>
        List<OpPacketHourDevice> GetPacketHourDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPacketTrashHourAddressFilter

        /// <summary>
        /// Gets PacketTrashHourAddress list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketTrashHourAddress funcion</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ADDRESS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketTrashHourAddress list</returns>
        List<OpPacketTrashHourAddress> GetPacketTrashHourAddressFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, string Address = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPacketTrashHourDeviceFilter

        /// <summary>
        /// Gets PacketTrashHourDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketTrashHourDevice funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketTrashHourDevice list</returns>
        List<OpPacketTrashHourDevice> GetPacketTrashHourDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPacketTrashHourTransmissionDriverFilter

        /// <summary>
        /// Gets PacketTrashHourTransmissionDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketTrashHourTransmissionDriver funcion</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketTrashHourTransmissionDriver list</returns>
        List<OpPacketTrashHourTransmissionDriver> GetPacketTrashHourTransmissionDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, int[] IdTransmissionType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTemperatureCoefficientFilter

        /// <summary>
        /// Gets TemperatureCoefficient list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTemperatureCoefficient funcion</param>
        /// <param name="IdTemperatureCoefficient">Specifies filter for ID_TEMPERATURE_COEFFICIENT column</param>
        /// <param name="Temperature">Specifies filter for TEMPERATURE column</param>
        /// <param name="Density">Specifies filter for DENSITY column</param>
        /// <param name="Value">Specifies filter for VALUE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TEMPERATURE_COEFFICIENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TemperatureCoefficient list</returns>
        List<OpTemperatureCoefficient> GetTemperatureCoefficientFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTemperatureCoefficient = null, Double[] Temperature = null, Double[] Density = null, Double[] Value = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetTransmissionDriverPerformanceFilter

        /// <summary>
        /// Gets TransmissionDriverPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionDriverPerformance funcion</param>
        /// <param name="IdTransmissionDriverPerformance">Specifies filter for ID_TRANSMISSION_DRIVER_PERFORMANCE column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionDriverPerformance list</returns>
        List<OpTransmissionDriverPerformance> GetTransmissionDriverPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTransmissionDriverPerformance = null, int[] IdTransmissionDriver = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, int[] IdDataSourceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion

        #region GetReportDataTypeFilter

        /// <summary>
        /// Gets ReportDataType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdReportDataType">Specifies filter for ID_REPORT_DATA_TYPE column</param>
        /// <param name="Signature">Specifies filter for SIGNATURE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="DigitsAfterComma">Specifies filter for DIGITS_AFTER_COMMA column</param>
        /// <param name="SequenceNbr">Specifies filter for SEQUENCE_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_DATA_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportDataType list</returns>
        List<OpReportDataType> GetReportDataTypeFilter(bool loadNavigationProperties = true, long[] IdReportDataType = null, string Signature = null, long[] IdDataType = null, int[] IdUnit = null, int[] DigitsAfterComma = null, int[] SequenceNbr = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSystemDataFilterDW

        /// <summary>
        /// Gets SystemData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdSystemData">Specifies filter for ID_SYSTEM_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SYSTEM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SystemData list</returns>
        List<OpSystemData> GetSystemDataFilterDW(bool loadNavigationProperties = true, long[] IdSystemData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
#endif
    }
}
