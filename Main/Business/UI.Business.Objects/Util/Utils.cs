﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects
{
    public class Utils
    {
        private static CultureInfo _CultureInfo { get; set; }
        public static CultureInfo CultureInfo { get { return _CultureInfo; } set { _CultureInfo = value; ResourcesText.Culture = value; } }
        public static Type GetHelperType(long idDataType, bool returnReferencedType)
        {
            Type type;
            switch (idDataType)
            {
                case DataType.HELPER_ID_METER_TYPE_CLASS:
                    type = returnReferencedType == true ? typeof(OpMeterTypeClass) : typeof(int);
                    break;
                case DataType.HELPER_ID_LOCATION:
                    type = returnReferencedType == true ? typeof(OpLocation) : typeof(long);
                    break;
                case DataType.HELPER_ID_METER:
                    type = returnReferencedType == true ? typeof(OpMeter) : typeof(long);
                    break;
                case DataType.HELPER_SERIAL_NBR:
                    type = typeof(long);
                    break;
                case DataType.HELPER_ID_DISTRIBUTOR:
                    type = returnReferencedType == true ? typeof(OpDistributor) : typeof(int);
                    break;
                case DataType.HELPER_ID_CONSUMER:
                    type = returnReferencedType == true ? typeof(OpConsumer) : typeof(int);
                    break;
                case DataType.HELPER_ID_LOCATION_TYPE:
                    type = returnReferencedType == true ? typeof(OpLocationType) : typeof(int);
                    break;
                case DataType.HELPER_ID_LOCATION_STATE_TYPE:
                    type = returnReferencedType == true ? typeof(OpLocationStateType) : typeof(int);
                    break;
                case DataType.HELPER_LOCATION_IN_KPI:
                    type = typeof(bool);
                    break;
                case DataType.HELPER_ID_DEVICE_TYPE:
                    type = returnReferencedType == true ? typeof(OpDeviceType) : typeof(int);
                    break;
                case DataType.HELPER_ID_DEVICE_STATE_TYPE:
                    type = returnReferencedType == true ? typeof(OpDeviceStateType) : typeof(int);
                    break;
                case DataType.HELPER_ID_DEVICE_ORDER_NUMBER:
                    type = returnReferencedType == true ? typeof(OpDeviceOrderNumber) : typeof(int);
                    break;
                case DataType.HELPER_ID_METER_TYPE:
                    type = returnReferencedType == true ? typeof(OpMeterType) : typeof(int);
                    break;
                case DataType.HELPER_DEVICE_TEMPLATE:
                    type = returnReferencedType == true ? typeof(OpDeviceTemplate) : typeof(int);
                    break;
                case DataType.HELPER_INSTALLATION_DATE:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_LOCATION_PARENT_GROUPS:
                    type = typeof(string);
                    break;
                case DataType.HELPER_ID_TASK:
                    type = returnReferencedType == true ? typeof(OpTask) : typeof(int);
                    break;
                case DataType.HELPER_ID_OPERATOR:
                    type = returnReferencedType == true ? typeof(OpOperator) : typeof(int);
                    break;
                case DataType.HELPER_ID_ISSUE:
                    type = returnReferencedType == true ? typeof(OpIssue) : typeof(int);
                    break;
                case DataType.HELPER_ID_ACTOR:
                    type = returnReferencedType == true ? typeof(OpActor) : typeof(int);
                    break;
                case DataType.HELPER_LOGIN:
                    type = typeof(string);
                    break;
                case DataType.HELPER_PASSWORD:
                    type = typeof(string);
                    break;
                case DataType.HELPER_IS_BLOCKED:
                    type = typeof(bool);
                    break;
                case DataType.HELPER_ID_TASK_GROUP:
                    type = returnReferencedType == true ? typeof(OpTaskGroup) : typeof(int);
                    break;
                case DataType.HELPER_ID_PLANNED_ROUTE:
                    type = typeof(long);
                    break;
                case DataType.HELPER_ID_OPERATOR_REGISTERING:
                    type = typeof(int);
                    break;
                case DataType.HELPER_ID_OPERATOR_PERFORMER:
                    type = typeof(int);
                    break;
                case DataType.HELPER_ID_TASK_STATUS:
                    type = returnReferencedType == true ? typeof(OpTaskStatus) : typeof(int);
                    break;
                case DataType.HELPER_CREATION_DATE:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_ACCEPTED:
                    type = typeof(bool);
                    break;
                case DataType.HELPER_ID_OPERATOR_ACCEPTED:
                    type = typeof(int);
                    break;
                case DataType.HELPER_ACCEPTANCE_DATE:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_NOTES:
                    type = typeof(string);
                    break;
                case DataType.HELPER_DEADLINE:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_TOPIC_NUMBER:
                    type = typeof(string);
                    break;
                case DataType.HELPER_PRIORITY:
                    type = typeof(int);
                    break;
                case DataType.HELPER_OPERATION_CODE:
                    type = typeof(long);
                    break;
                case DataType.HELPER_ID_TASK_TYPE:
                    type = returnReferencedType == true ? typeof(OpTaskType) : typeof(int);
                    break;
                case DataType.HELPER_ID_ISSUE_TYPE:
                    type = returnReferencedType == true ? typeof(OpIssueType) : typeof(int);
                    break;
                case DataType.HELPER_ID_ISSUE_STATUS:
                    type = returnReferencedType == true ? typeof(OpIssueStatus) : typeof(int);
                    break;
                case DataType.HELPER_REALIZATION_DATE:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_SHORT_DESCR:
                    type = typeof(string);
                    break;
                case DataType.HELPER_LONG_DESCR:
                    type = typeof(string);
                    break;
                case DataType.HELPER_NAME:
                    type = typeof(string);
                    break;
                case DataType.HELPER_SURNAME:
                    type = typeof(string);
                    break;
                case DataType.HELPER_CITY:
                    type = typeof(string);
                    break;
                case DataType.HELPER_ADDRESS:
                    type = typeof(string);
                    break;
                case DataType.HELPER_POSTCODE:
                    type = typeof(string);
                    break;
                case DataType.HELPER_EMAIL:
                    type = typeof(string);
                    break;
                case DataType.HELPER_MOBILE:
                    type = typeof(string);
                    break;
                case DataType.HELPER_PHONE:
                    type = typeof(string);
                    break;
                case DataType.HELPER_ID_LANGUAGE:
                    type = returnReferencedType == true ? typeof(OpLanguage) : typeof(int);
                    break;
                case DataType.HELPER_DESCRIPTION:
                    type = typeof(string);
                    break;
                case DataType.HELPER_AVATAR:
                    type = typeof(byte[]);
                    break;
                case DataType.HELPER_ID_DESCR:
                    type = typeof(string);
                    break;
                case DataType.HELPER_ID_IMR_SERVER:
                    type = returnReferencedType == true ? typeof(OpImrServer) : typeof(int);
                    break;
                case DataType.HELPER_ID_ALARM:
                    type = typeof(long);
                    break;
                case DataType.HELPER_ID_ALARM_EVENT:
                    type = typeof(long);
                    break;
                case DataType.HELPER_ID_ALARM_GROUP:
                    type = returnReferencedType == true ? typeof(OpAlarmGroup) : typeof(int);
                    break;
                case DataType.HELPER_ID_ALARM_STATUS:
                    type = returnReferencedType == true ? typeof(OpAlarmStatus) : typeof(int);
                    break;
                case DataType.HELPER_ID_TRANSMISSION_TYPE:
                    type = returnReferencedType == true ? typeof(OpTransmissionType) : typeof(int);
                    break;
                case DataType.HELPER_ID_ROUTE:
                    type = returnReferencedType == true ? typeof(OpRoute) : typeof(long);
                    break;
                case DataType.HELPER_ID_ROUTE_DEF:
                    type = returnReferencedType == true ? typeof(OpRouteDef) : typeof(int);
                    break;
                case DataType.HELPER_YEAR:
                    type = typeof(int);
                    break;
                case DataType.HELPER_MONTH:
                    type = typeof(int);
                    break;
                case DataType.HELPER_WEEK:
                    type = typeof(int);
                    break;
                case DataType.HELPER_ID_OPERATOR_EXECUTOR:
                    type = typeof(int);
                    break;
                case DataType.HELPER_ID_OPERATOR_APPROVED:
                    type = typeof(int);
                    break;
                case DataType.HELPER_DATE_UPLOADED:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_DATE_APPROVED:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_DATE_FINISHED:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_ID_ROUTE_STATUS:
                    type = returnReferencedType == true ? typeof(OpRouteStatus) : typeof(int);
                    break;
                case DataType.HELPER_ID_ROUTE_TYPE:
                    type = returnReferencedType == true ? typeof(OpRouteType) : typeof(int);
                    break;
                case DataType.HELPER_EXPIRATION_DATE:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_ID_ALARM_DEF:
                    type = returnReferencedType == true ? typeof(OpAlarmDef) : typeof(long);
                    break;
                case DataType.HELPER_ID_DATA_ARCH:
                    type = typeof(long);
                    break;
                case DataType.HELPER_IS_ACTIVE:
                    type = returnReferencedType == true ? typeof(OpActivity) : typeof(int);
                    break;
                case DataType.HELPER_ID_ALARM_TYPE:
                    type = returnReferencedType == true ? typeof(OpAlarmType) : typeof(int);
                    break;
                case DataType.HELPER_ID_DATA_TYPE_ALARM:
                    type = returnReferencedType == true ? typeof(OpDataType) : typeof(long);
                    break;
                case DataType.HELPER_SYSTEM_ALARM_VALUE:
                    type = typeof(object);
                    break;
                case DataType.HELPER_ALARM_VALUE:
                    type = typeof(object);
                    break;
                case DataType.HELPER_TRANSMISSION_TYPE:
                    type = returnReferencedType == true ? typeof(OpTransmissionType) : typeof(int);
                    break;
                case DataType.HELPER_DISTRIBUTOR_ID_ROLE:
                    type = returnReferencedType == true ? typeof(OpRole) : typeof(int);
                    break;
                case DataType.HELPER_ID_REPORT:
                    type = returnReferencedType == true ? typeof(OpReport) : typeof(int);
                    break;
                case DataType.HELPER_ID_REPORT_TYPE:
                    type = returnReferencedType == true ? typeof(OpReportType) : typeof(int);
                    break;
                case DataType.HELPER_ID_ACTION:
                    type = returnReferencedType == true ? typeof(OpAction) : typeof(long);
                    break;
                case DataType.HELPER_ID_ACTION_TYPE:
                    type = returnReferencedType == true ? typeof(OpActionType) : typeof(int);
                    break;
                case DataType.HELPER_ID_ACTION_STATUS:
                    type = returnReferencedType == true ? typeof(OpActionStatus) : typeof(int);
                    break;
                case DataType.HELPER_ID_ACTION_DATA:
                    type = returnReferencedType == true ? typeof(OpActionData) : typeof(long);
                    break;
                case DataType.HELPER_ID_ACTION_PARENT:
                    type = returnReferencedType == true ? typeof(OpAction) : typeof(long);
                    break;
                case DataType.HELPER_ID_MODULE:
                    type = returnReferencedType == true ? typeof(OpModule) : typeof(int);
                    break;
                case DataType.HELPER_ID_ACTOR_GROUP:
                    type = returnReferencedType == true ? typeof(OpActorGroup) : typeof(int);
                    break;
                case DataType.HELPER_BUILT_IN:
                    type = typeof(bool);
                    break;
                case DataType.HELPER_ACTION_HIERARCHY:
                    type = typeof(string);
                    break;
                case DataType.HELPER_ACTION_HIERARCHY_LEVEL:
                    type = typeof(long);
                    break;
                case DataType.HELPER_ACTION_HIERARCHY_ROOT:
                    type = typeof(long);
                    break;
                case DataType.HELPER_SERIAL_NBR_PARENT:
                    type = typeof(long);
                    break;
                case DataType.HELPER_TIME:
                    type = typeof(DateTime);
                    break;
                case DataType.HELPER_ALARM_MESSAGE:
                    type = typeof(string);
                    break;
                case DataType.HELPER_ID_SIM_CARD:
                    type = returnReferencedType == true ? typeof(OpSimCard) : typeof(int);
                    break;
                case DataType.HELPER_PIN:
                    type = typeof(string);
                    break;
                case DataType.HELPER_PUK:
                    type = typeof(string);
                    break;
                case DataType.HELPER_MOBILE_NETWORK_CODE:
                    type = typeof(int);
                    break;
                case DataType.HELPER_IP:
                    type = typeof(string);
                    break;
                case DataType.HELPER_APN_LOGIN:
                    type = typeof(string);
                    break;
                case DataType.HELPER_APN_PASSWORD:
                    type = typeof(string);
                    break;
                case DataType.HELPER_SIM_CARD_SERIAL_NBR:
                    type = typeof(long);
                    break;
                default:
                    type = null;
                    break;
            }
            return type;
        }
        public static Type GetHelperType(OpDataType opDataType, bool returnReferencedType)
        {
            if (opDataType == null)
                return null;
            else
                return GetHelperType(opDataType.IdDataType, returnReferencedType);
        }
    }
}
