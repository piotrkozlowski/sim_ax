﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.DAQ;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects
{
    public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region GetActionPacketFilter
        /// <summary>
        /// Gets ActionPacket list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionPacket list</returns>
        List<OpActionPacket> GetActionPacketFilter(bool loadNavigationProperties = true, long[] IdAction = null, long[] IdPacket = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #region GetDataArchFilter
        /// <summary>
        /// Gets DataArch list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="IsAction">Specifies filter for IS_ACTION column</param>
        /// <param name="IsAlarm">Specifies filter for IS_ALARM column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataArch list</returns>
        List<OpDataArch> GetDataArchFilterDAQ(bool loadNavigationProperties = true, long[] IdDataArch = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode Time = null, long[] IdPacket = null,
                            bool? IsAction = null, bool? IsAlarm = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #region GetDataArchTrashFilter
        /// <summary>
        /// Gets DataArchTrash list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdDataArchTrash">Specifies filter for ID_DATA_ARCH_TRASH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="IsAction">Specifies filter for IS_ACTION column</param>
        /// <param name="IsAlarm">Specifies filter for IS_ALARM column</param>
        /// <param name="IdDataArchTrashStatus">Specifies filter for ID_DATA_ARCH_TRASH_STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH_TRASH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataArchTrash list</returns>
        List<OpDataArchTrash> GetDataArchTrashFilter(bool loadNavigationProperties = true, long[] IdDataArchTrash = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode Time = null, long[] IdPacket = null,
                            bool? IsAction = null, bool? IsAlarm = null, int[] IdDataArchTrashStatus = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #region PacketDataArch
        List<OpPacketDataArch> GetPacketDataArch(long serialNbr, long idPacket, bool useDBCollector = false);
        #endregion

        #region GetPacketFilter
        /// <summary>
        /// Gets Packet list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacket funcion</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="TransmittedPackets">Specifies filter for TRANSMITTED_PACKETS column</param>
        /// <param name="Bytes">Specifies filter for BYTES column</param>
        /// <param name="TransmittedBytes">Specifies filter for TRANSMITTED_BYTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKET DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Packet list</returns>
        List<OpPacket> GetPacketFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPacket = null, long[] SerialNbr = null, string Address = null, int[] IdTransmissionDriver = null, bool? IsIncoming = null, int[] IdTransmissionType = null,
                            int[] TransmittedPackets = null, int[] Bytes = null, int[] TransmittedBytes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region GetPacketHistoryFilter
        /// <summary>
        /// Gets PacketHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketHistory funcion</param>
        /// <param name="IdPacketHistory">Specifies filter for ID_PACKET_HISTORY column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdPacketStatus">Specifies filter for ID_PACKET_STATUS column</param>
        /// <param name="SystemTime">Specifies filter for SYSTEM_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKET_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketHistory list</returns>
        List<OpPacketHistory> GetPacketHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPacketHistory = null, long[] IdPacket = null, TypeDateTimeCode Time = null, int[] IdPacketStatus = null, TypeDateTimeCode SystemTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #region SaveRouteTable
        /// <summary>
        /// Saves the RouteTable object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteTable to save</param>
        /// <returns>RouteTable Id</returns>
        int SaveRouteTable(OpRouteTable toBeSaved);
        #endregion

        DataTable GetClusterVeryfication(long[] oko, DateTime startTime, DateTime endTime);

        #region GetTransmissionDriverFilter
        /// <summary>
        /// Gets TransmissionDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionDriver funcion</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="IdTransmissionDriverType">Specifies filter for ID_TRANSMISSION_DRIVER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="RunAtHost">Specifies filter for RUN_AT_HOST column</param>
        /// <param name="ResponseAddress">Specifies filter for RESPONSE_ADDRESS column</param>
        /// <param name="Plugin">Specifies filter for PLUGIN column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="IdTransmissionProtocol">Specifies filter for ID_TRANSMISSION_PROTOCOL column</param>
        /// <param name="UseCache">Specifies filter for USE_CACHE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionDriver list</returns>
        List<OpTransmissionDriver> GetTransmissionDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionDriver = null, int[] IdTransmissionDriverType = null, string Name = null, string RunAtHost = null, string ResponseAddress = null, string Plugin = null,
                            bool? IsActive = null, bool? UseCache = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool loadCustomData = true, bool useDBCollector = false);
        #endregion
#endif
    }
}
