﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects
{
    public interface IExportData
    {
        System.Threading.Tasks.Task ExportTask { get; set; }
        List<Enums.ReferenceType> GetSupportedObjects();

        List<Type> GetSupportedExportTypes();//Lista typów jakie mecchanizm jest wstanie zintepretować
        
        OpExportProgressStep GetCurrentProgressState();

        List<OpDataType> GetRequiredFields(List<Enums.ReferenceType> referenceType);

        void Initialize(object[] parameters);

        void LoadData(object dataToLoad);

        List<OpExportItemSummary> ValidateData();

        List<OpExportItemSummary> GetSummary();
        
        Enums.ExportStatus StartExport();
        void StopExport();       

        byte[] FileByteArray { get; set; }       
    }

    [Serializable, DataContract]
    public class OpExportItemSummary
    {
        [DataMember]
        public object Key { get; set; }
        [DataMember]
        public Enums.ExportStatus ExportStatus { get; set; }
        [DataMember]
        public string Description { get; set; }

        public OpExportItemSummary() { }
        public OpExportItemSummary(object Key, Enums.ExportStatus ExportStatus, string Description)
        {
            this.Key = Key;
            this.ExportStatus = ExportStatus;
            this.Description = Description;
        }
    }

    [Serializable, DataContract]
    public class OpExportProgressStep
    {
        [DataMember]
        public Enums.ExportStatus ExportStatus { get; set; }
        [DataMember]
        public long ItemsCount { get; set; }
        [DataMember]
        public long ProceedItemsCount { get; set; }
        [DataMember]
        public long ProceedSuccessItemsCount { get; set; }
        [DataMember]
        public long ProceedErrorItemsCount { get; set; }
        [DataMember]
        public long ProceedWarningItemsCount { get; set; }

        public OpExportProgressStep() { }
        public OpExportProgressStep(Enums.ExportStatus ExportStatus, long ItemsCount, long ProceedItemsCount, long ProceedSuccessItemsCount, long ProceedErrorItemsCount, long ProceedWarningItemsCount)
        {
            this.ExportStatus = ExportStatus;
            this.ItemsCount = ItemsCount;
            this.ProceedItemsCount = ProceedItemsCount;
            this.ProceedSuccessItemsCount = ProceedSuccessItemsCount;
            this.ProceedErrorItemsCount = ProceedErrorItemsCount;
            this.ProceedWarningItemsCount = ProceedWarningItemsCount;
        }
    }

    [Serializable, DataContract]
    public class OpExportData
    {
        [DataMember]
        public long? IdFile { get; set; }

        [DataMember]
        private byte[] _FileBytes { get; set; }
        [DataMember]
        public byte[] FileBytes
        {
            get
            {
                if (this._FileBytes != null && this._FileBytes.Length > 0)
                    return this._FileBytes;
                else if (this.File != null)
                    return this.File.FileBytes as byte[];
                else
                    return null;
            }
            set
            {
                this._FileBytes = value;
            }
        }

        [DataMember]
        public OpFile File { get; set; }

        [DataMember]
        public string[] Headers { get; set; }

        [DataMember]
        public object[][] Data { get; set; }

        public OpExportData() { }
        public OpExportData(string[] Headers, object[][] Data)
        {
            this.Headers = Headers;
            this.Data = Data;
        }        
        public OpExportData(long IdFile)
        {
            this.IdFile = IdFile;
        }
        public OpExportData(byte[] FileBytes)
        {
            this.FileBytes = FileBytes;
        }
        public OpExportData(OpFile File)
        {
            this.File = File;
        }
    }
}
