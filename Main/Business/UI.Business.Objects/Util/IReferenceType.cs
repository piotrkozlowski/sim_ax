﻿namespace IMR.Suite.UI.Business.Objects
{
#if !USE_UNITED_OBJECTS
    public interface IReferenceType
    {
        object GetReferenceKey();
        object GetReferenceValue();
    }
#endif
}
