﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Windows.Forms;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects
{
#if !USE_UNITED_OBJECTS
    public enum OpChangeState
    {
        Undefined = 0,
        New = 1,
        Modified = 2,
        Loaded = 3,
        Delete = 4,
        Incorrect = 5,
    }

    public interface IOpChangeState
    {
        OpChangeState OpState { get; set; }
    }

    
    #region do usuniecia
    //bblach. tymczasowe miejsce, miałem zablokowany csproj :)
    //klasy do usuniecia, nie należy z nich korzystać. 
    //miejsca gdzie zostały wykorzystane w DIMA bedą przerobione na OpBindingList i OpBindingDictionary (patrz plik OpBindingSource)
    [Obsolete("Interface will be removed, don't use it!", false)]
    public interface IOpDynamic
    {
        object Key { get; }
        Dictionary<string, object> DynamicValues { get; set; }
        object Owner { get; }
    }


    [Obsolete("Class will be removed, don't use it!", false)]
    public class OpDynamic<T> where T : IOpDynamic
    {
        private T ObjectValue;

        public OpDynamic(T objectValue)
            : this(objectValue, null)
        {
        }
        public OpDynamic(T objectValue, Dictionary<string, object> values)
        {
            this.ObjectValue = objectValue;
            if (values != null)
                this.ObjectValue.DynamicValues = values;
            //else
            //    this.ObjectValue.DynamicValues = new Dictionary<string, object>();
        }

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return ObjectValue.Key; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return ObjectValue.DynamicValues; }
        }

        public T Owner
        {
            get { return (T)ObjectValue.Owner; }
        }
        #endregion

        #region GetValue
        public object GetValue(string fieldName)
        {
            return ObjectValue.DynamicValues.TryGetValue(fieldName);
        }
        #endregion
        #region TryGetValue
        public U TryGetValue<U>(string fielName) where U : struct, IConvertible
        {
            object obj;
            if (ObjectValue.DynamicValues.TryGetValue(fielName, out obj))
                return GenericConverter.Parse<U>(obj);

            return default(U);
        }
        #endregion
        #region TryGetNullableValue
        public Nullable<U> TryGetNullableValue<U>(string fielName) where U : struct, IConvertible
        {
            object obj;
            if (ObjectValue.DynamicValues.TryGetValue(fielName, out obj))
                return GenericConverter.Parse<U>(obj);

            return null;
        }
        #endregion

        #region this[string fieldName]
        public object this[string fieldName]
        {
            get { return GetValue(fieldName); }
            set { ObjectValue.DynamicValues[fieldName] = value; }
        }
        #endregion

        #region ToString()
        public override string ToString()
        {
            return String.Format("{0}", Key);
        }
        #endregion

        #region IEquatable<OpDynamic<T>> Members
        public bool Equals(OpDynamic<T> other)
        {
            if (other != null)
                return this.Key.Equals(other.Key);
            else
                return false;
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDynamic<T>)
                return this.Key == ((OpDynamic<T>)obj).Key;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDynamic<T> left, OpDynamic<T> right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDynamic<T> left, OpDynamic<T> right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }
        #endregion
    }

    [Obsolete("Class will be removed, don't use it!", false)]
    public class OpDynamicList<T> : List<OpDynamic<T>> where T : IOpDynamic
    {
        #region Members
        private Dictionary<object, OpDynamic<T>> itemsDict;
        private bool isDictionary = false;
        #endregion

        #region Properties
        public Dictionary<object, OpDynamic<T>> Dictionary
        {
            get
            {
                if (!IsDictionary)
                    RaiseDictionaryException();
                return itemsDict;
            }
        }

        public bool IsDictionary
        {
            get { return isDictionary; }
        }
        #endregion

        #region Ctor
        public OpDynamicList(bool isDictionary)
            : this(1, isDictionary) { }

        public OpDynamicList(int capacity, bool isDictionary)
            : base(capacity)
        {
            this.isDictionary = isDictionary;
            if (isDictionary)
                itemsDict = new Dictionary<object, OpDynamic<T>>();
        }
        #endregion

        #region Implementation of ICollection<OpDynamic<T>>
        new public void Add(OpDynamic<T> item)
        {
            if (isDictionary)
            {
                if (!itemsDict.ContainsKey(item.Key))
                {
                    base.Add(item);
                    itemsDict[item.Key] = item; //dodajemy element do slownika
                }
                else
                    throw new ArgumentException("Item with the same key already exists.");
            }
            else
                base.Add(item);
        }

        new public void Clear()
        {
            base.Clear();
            if (isDictionary)
                itemsDict.Clear();
        }

        new public bool Remove(OpDynamic<T> item)
        {
            if (isDictionary)
                itemsDict.Remove(item.Key);
            return base.Remove(item);
        }

        new public int Count
        {
            get { return base.Count; }
        }
        #endregion
        #region Implementation of IList<OpDynamic<T>>
        new public void Insert(int index, OpDynamic<T> item)
        {
            if (isDictionary)
            {
                if (!itemsDict.ContainsKey(item.Key))
                {
                    base.Insert(index, item);
                    itemsDict.Add(item.Key, item);
                }
                else
                    throw new ArgumentException("Item with the same key already exists.");
            }
            else
                base.Insert(index, item);
        }

        new public void RemoveAt(int index)
        {
            if (index >= 0 && index < base.Count)
            {
                OpDynamic<T> item = base[index];
                if (isDictionary)
                    itemsDict.Remove(item.Key);
                base.RemoveAt(index);
            }
        }

        new public OpDynamic<T> this[int index]
        {
            get { return base[index]; }
            set
            {
                base[index] = value;
                if (isDictionary)
                    itemsDict[value.Key] = value;
            }
        }
        #endregion

        #region ContainsKey
        public bool ContainsKey(object key)
        {
            if (!isDictionary)
                RaiseDictionaryException();
            return itemsDict.ContainsKey(key);
        }
        #endregion
        #region this[object key]
        public OpDynamic<T> this[object key]
        {
            get
            {
                if (!isDictionary)
                    RaiseDictionaryException();
                return itemsDict.TryGetValue(key);
            }
            set
            {
                if (!isDictionary)
                    RaiseDictionaryException();
                itemsDict[key] = value;
            }
        }
        #endregion
        #region AddRange
        public void AddRange(List<OpDynamic<T>> objects)
        {
            if (objects == null)
                return;

            foreach (OpDynamic<T> owner in objects)
            {
                this.Add(owner);
            }

        }
        #endregion

        #region AddRange
        public void AddRange(List<T> objects)
        {
            if (objects == null || objects.Count == 0)
                return;

            foreach (T owner in objects)
            {
                this.Add(new OpDynamic<T>(owner, owner.DynamicValues));
            }

        }
        #endregion

        #region RaiseDictionaryException
        private void RaiseDictionaryException()
        {
            throw new ApplicationException("Not supported operation. Set isDictionary during creation of this object");
        }
        #endregion
    }

    [Obsolete("Class will be removed, don't use it!", false)]
    public class OpMeasure : IOpDynamic
    {
        public DateTime Time { get; set; }
        private Dictionary<string, object> dynamicValues;

        public OpMeasure(DateTime time)
        {
            this.Time = time;
            this.dynamicValues = new Dictionary<string, object>();
        }

        public OpMeasure Clone()
        {
            return new OpMeasure(Time) { DynamicValues = new Dictionary<string, object>(this.DynamicValues) };
        }

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return Time; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
        #endregion
    }

    [Obsolete("Class will be removed, don't use it!", false)]
    public class OpMeasure2 : IOpDynamicProperty<OpMeasure2>
    {
        public DateTime Time { get; set; }
        private OpDynamicPropertyDict dynamicValues;

        public OpMeasure2(DateTime time)
        {
            this.Time = time;
            this.dynamicValues = new OpDynamicPropertyDict();
        }

        public OpMeasure2 Clone()
        {
            return new OpMeasure2(Time) { DynamicProperties = new OpDynamicPropertyDict(this.DynamicProperties) };
        }

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return Time; }
        }

        public OpDynamicPropertyDict DynamicProperties
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public OpMeasure2 Owner
        {
            get { return this; }
        }
        #endregion
    }
    #endregion

#endif
}
