﻿

namespace IMR.Suite.UI.Business.Objects
{
#if !USE_UNITED_OBJECTS
    public interface IOpDataProvider
    {
        object Clone(IDataProvider dataProvider = null);
        void AssignReferences(IDataProvider dataProvider);
    }
#endif
}
