﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace IMR.Suite.UI.Business.Objects
{
    //Rozwiazanie w fazie testów w IMRSC. Powinno pomóc przy ograniczeniu zaciagania nadmiarowych Navigation Properties w obiektach OpXXX
    public class DataLoadOptions
    {
        private Dictionary<MetaPosition, MemberInfo> includes = new Dictionary<MetaPosition, MemberInfo>();

        public void LoadWith<T>(Expression<Func<T, object>> expression)
        {
            MemberInfo memberInfo = GetLoadWithMemberInfo(expression);
            includes.Add(new MetaPosition(memberInfo), memberInfo);
        }

        public bool ContainsMember(Type type, string memberName)
        {
            MemberInfo memberInfo = type.GetMember(memberName).FirstOrDefault();
            if (memberInfo != null)
                return ContainsMember(memberInfo);
            return false;
        }

        public bool ContainsMember(MemberInfo memberInfo)
        {
            return includes.ContainsKey(new MetaPosition(memberInfo));
        }

        private static MemberInfo GetLoadWithMemberInfo(LambdaExpression lambda)
        {
            Expression body = lambda.Body;
            if ((body != null) && ((body.NodeType == ExpressionType.Convert) || (body.NodeType == ExpressionType.ConvertChecked)))
            {
                body = ((UnaryExpression)body).Operand;
            }
            MemberExpression expression2 = body as MemberExpression;
            if ((expression2 == null) || (expression2.Expression.NodeType != ExpressionType.Parameter))
            {
                throw new Exception();
            }
            return expression2.Member;
        }

        #region struct MetaPosition
        public struct MetaPosition : IEqualityComparer<MetaPosition>, IEqualityComparer
        {
            private int metadataToken;
            private Assembly assembly;
            public MetaPosition(MemberInfo mi)
                : this(mi.DeclaringType.Assembly, mi.MetadataToken)
            {
            }

            private MetaPosition(Assembly assembly, int metadataToken)
            {
                this.assembly = assembly;
                this.metadataToken = metadataToken;
            }

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return false;
                }
                if (obj.GetType() != base.GetType())
                {
                    return false;
                }
                return AreEqual(this, (MetaPosition)obj);
            }

            public override int GetHashCode()
            {
                return this.metadataToken;
            }

            public bool Equals(MetaPosition x, MetaPosition y)
            {
                return AreEqual(x, y);
            }

            public int GetHashCode(MetaPosition obj)
            {
                return obj.metadataToken;
            }

            bool IEqualityComparer.Equals(object x, object y)
            {
                return this.Equals((MetaPosition)x, (MetaPosition)y);
            }

            int IEqualityComparer.GetHashCode(object obj)
            {
                return this.GetHashCode((MetaPosition)obj);
            }

            private static bool AreEqual(MetaPosition x, MetaPosition y)
            {
                return ((x.metadataToken == y.metadataToken) && (x.assembly == y.assembly));
            }

            public static bool operator ==(MetaPosition x, MetaPosition y)
            {
                return AreEqual(x, y);
            }

            public static bool operator !=(MetaPosition x, MetaPosition y)
            {
                return !AreEqual(x, y);
            }

            internal static bool AreSameMember(MemberInfo x, MemberInfo y)
            {
                return ((x.MetadataToken == y.MetadataToken) && (x.DeclaringType.Assembly == y.DeclaringType.Assembly));
            }
        } 
        #endregion
    }
}
