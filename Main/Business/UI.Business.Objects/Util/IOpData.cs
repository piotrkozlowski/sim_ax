﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects
{
#if !USE_UNITED_OBJECTS
    public interface IOpData : IOpChangeState
    {
        long IdData { get; set; }
        long IdDataType { get; set; }
        int Index { get; set; }
        object Value { get; set; }
        object ReferencedObject { get; set; }

        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        // object Clone(DataProvider dataProvider = null);//przeniesione do IOpDataProvider w UI.Business
        // void AssignReferences(DataProvider dataProvider);//przeniesione do IOpDataProvider w UI.Business
    }
#endif
}
