﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects
{
    //Interfejs dla standardowych właściwości/pól występujących w obiektach biznesowych
    public interface IOpObject<DLT> where DLT : IOpDataProvider, IOpData, new()
    {
        object IdObject { get; set; }
        OpDataList<DLT> DataList { get; set; }
    }
}
