﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects
{
    public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region InvalidateDataFromDataArch
        bool InvalidateDataFromDataArch(long[] IdDataArch, int? IdImrServer = null, bool useDBCollector = false);
        #endregion

        #region ActionDataSource
        #region GetActionDataSourceFilter
        /// <summary>
        /// Gets ActionDataSource list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionDataSource list</returns>
        List<OpActionDataSource> GetActionDataSourceFilter(bool loadNavigationProperties = true, long[] IdAction = null, long[] IdDataSource = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #endregion

        #region CalibrationSession

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        List<OpCalibrationSession> GetAllCalibrationSessions();

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        List<OpCalibrationSession> GetCalibrationSessions(int[] IdSessions);

        /// <summary>
        /// Deletes the Action object from the database
        /// </summary>
        /// <param name="toBeDeleted">Action to delete</param>
        void DeleteCalibrationSession(OpCalibrationSession toBeDeleted);

        void DeleteCalibrationSessions(List<OpCalibrationSession> toBeDeleted);
        #endregion

        #region CalibrationModules

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        List<OpCalibrationModule> GetAllCalibrationModules();


        int SaveCalibrationSession(IMR.Suite.UI.Business.Objects.DW.OpCalibrationSession Session);

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        List<OpCalibrationModule> GetCalibrationModules(int[] IdModules);
        #endregion

        #region CALIBRATION_TEST

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        List<OpCalibrationTest> GetCalibrationTests(long[] IdTests);

        #endregion

        #region CalibrationData
        void SaveCalibrationData(OpCalibrationData[] toBeSaved, int commandTimeout = 0);

        List<OpCalibrationData> GetCalibrationData(int IdCalibrationSession);

        void DeleteCalibrationDataSession(int toBeDeleted);

        #endregion

        #region CheckIfTableExistsDW

        bool CheckIfTableExistsDW(string name);
 
	    #endregion

        #region DataArch
        #region GetDataArchFilterAggregated
        /// <summary>
        /// Gets DataArch list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataArch funcion</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="AggregationType">Specifies aggregation type criteria</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataArch list</returns>
        List<OpDataArch> GetDataArchFilterAggregated(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                             TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, long[] IdAlarmEvent = null, int[] Status = null, int AggregationType = 0,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataArchFilter

        /// <summary>
        /// Gets DataArch list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataArch funcion</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DataArch list</returns>
        List<OpDataArch> GetDataArchFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, long[] IdAlarmEvent = null, int[] Status = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false, bool loadCustomData = true);
        #endregion
        #region SaveDataArch
        /// <summary>
        /// Saves the DataArch object to the database
        /// </summary>
        /// <param name="toBeSaved">DataArch to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>DataArch Id</returns>
        long SaveDataArch(OpDataArch toBeSaved, bool useDBCollector = false);
        void SaveDataArch(OpDataArch[] toBeSaved, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region DeleteDataArchFilter
        /// <summary>
        /// Deletes the DataArch objects from database
        /// </summary>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        void DeleteDataArchFilter(long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                                  TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, long[] IdAlarmEvent = null, int[] Status = null,
                                  string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #endregion

        #region TransmissionDriverPerformance

        void DeleteEtlPerformance(int[] IdEtl = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0);

        #endregion

        #region GetCurrentDataForDistributor
        /// <summary>
        /// Gets current data for distributor's devices (warning - all devices from specified DEVICE filter!)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdDistributor">Specifies filter for DEVICE.ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceType">Specifies filter for DEVICE.ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceStateType">Specifies filter for DEVICE.ID_DEVICE_STATE_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns></returns>
        List<OpData> GetCurrentDataForDistributor(bool loadNavigationProperties = true, int[] IdDistributor = null, int[] IdDeviceType = null, int[] IdDeviceStateType = null, long[] IdDataType = null, int[] IndexNbr = null, bool useDBCollector = false);
        #endregion

        #region DistributorPerformance

        void DeleteDistributorPerformance(int[] IdDistributor = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0);

        #endregion

        #region ImrServerPerformance

        void DeleteImrServerPerformance(int[] IdImrServer = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0);

        #endregion

        #region GetDataFilterDW
        /// <summary>
        /// Gets Data list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdData">Specifies filter for ID_DATA column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>Data list</returns>
        List<OpData> GetDataFilterDW(bool loadNavigationProperties = true, long[] IdData = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, int[] Status = null, long topCount = 0, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #region SaveData
        /// <summary>
        /// Saves the Data object to the database
        /// </summary>
        /// <param name="toBeSaved">Data to save</param>
        /// <returns>Data Id</returns>
        long SaveData(OpData toBeSaved);
        #endregion
        #region DeleteData
        /// <summary>
        /// Deletes the Data object from the database
        /// </summary>
        /// <param name="toBeDeleted">Data to delete</param>
        void DeleteData(List<OpData> toBeDeleted);
        #endregion
        #region SaveDataSource
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idOperator"></param>
        /// <param name="IdImrServer"></param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns></returns>
        long SaveDataSource(int idOperator, int? IdImrServer = null, bool useDBCollector = false);
        #endregion
        #region ReportDataType

        /// <summary>
        /// Indicates whether the ReportDataType is cached.
        /// </summary>
        bool ReportDataTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportDataType objects
        /// </summary>
        List<OpReportDataType> GetAllReportDataType();


        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Id">ReportDataType Id</param>
        /// <returns>ReportDataType object</returns>
        OpReportDataType GetReportDataType(long Id);

        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Ids">ReportDataType Ids</param>
        /// <returns>ReportDataType list</returns>
        List<OpReportDataType> GetReportDataType(long[] Ids);

        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Id">ReportDataType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportDataType object</returns>
        OpReportDataType GetReportDataType(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Ids">ReportDataType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportDataType list</returns>
        List<OpReportDataType> GetReportDataType(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ReportDataType object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportDataType to save</param>
        /// <returns>ReportDataType Id</returns>
        long SaveReportDataType(OpReportDataType toBeSaved);

        /// <summary>
        /// Deletes the ReportDataType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportDataType to delete</param>
        void DeleteReportDataType(OpReportDataType toBeDeleted);

        /// <summary>
        /// Clears the ReportDataType object cache
        /// </summary>
        void ClearReportDataTypeCache();
        #endregion

        #region DataTemporal

        void DeleteDataTemporal(long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0);


        void SaveDataTemporal(OpDataTemporal[] toBeSaved, int commandTimeout = 0);

        List<OpDataTemporal> GetDataTemporalDevicePerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false,
                            long[] IdDataTemporal = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null,
                            int[] IdAggregationType = null, int[] IdDataSourceType = null,
                            int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        List<OpDataTemporal> GetDataTemporalByDistributor(bool loadNavigationProperties = true,
                            Enums.ReferenceType FilterOption = Enums.ReferenceType.None, int[] IdDistributor = null, long[] IdDataType = null, int[] IdAggregationType = null,
                            string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        #endregion

        #region DataSourceData
       
        /// <summary>
        /// Gets all DataSourceData objects
        /// </summary>
        List<OpDataSourceData> GetAllDataSourceData();


        /// <summary>
        /// Gets DataSourceData by id
        /// </summary>
        /// <param name="Id">DataSourceData Id</param>
        /// <returns>DataSourceData object</returns>
        OpDataSourceData GetDataSourceData(long Id);

        ///// <summary>
        ///// Gets DataSourceData by id
        ///// </summary>
        ///// <param name="Ids">DataSourceData Ids</param>
        ///// <returns>DataSourceData list</returns>
        //List<OpDataSourceData> GetDataSourceData(long[] Ids)
        //{
        //    return GetDataSourceData(Ids, false);
        //}

        ///// <summary>
        ///// Gets DataSourceData by id
        ///// </summary>
        ///// <param name="Id">DataSourceData Id</param>
        ///// <param name="queryDatabase">If True, gets object from database</param>
        ///// <returns>DataSourceData object</returns>
        //OpDataSourceData GetDataSourceData(long Id, bool queryDatabase)
        //{
        //    return GetDataSourceData(new long[] { Id }, queryDatabase).FirstOrDefault();
        //}

        /// <summary>
        /// Gets DataSourceData by id
        /// </summary>
        /// <param name="Ids">DataSourceData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSourceData list</returns>
        List<OpDataSourceData> GetDataSourceData(long[] Ids);

        /// <summary>
        /// Saves the DataSourceData object to the database
        /// </summary>
        /// <param name="toBeSaved">DataSourceData to save</param>
        /// <returns>DataSourceData Id</returns>
        long SaveDataSourceData(OpDataSourceData toBeSaved);

        /// <summary>
        /// Deletes the DataSourceData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataSourceData to delete</param>
        void DeleteDataSourceData(OpDataSourceData toBeDeleted);

        ///// <summary>
        ///// Clears the DataSourceData object cache
        ///// </summary>
        //void ClearDataSourceDataCache()
        //{
        //    DataSourceDataDict.Clear();
        //}

        #region GetDataSourceDataFilter
        /// <summary>
        /// Gets DataSourceData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_SOURCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataSourceData list</returns>
        List<OpDataSourceData> GetDataSourceDataFilter(bool loadNavigationProperties = true, long[] IdDataSource = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #endregion

        #region Device
        #region GetDeviceInvoice

        DataTableCollection GetDeviceInvoice(int IdDistributor, DateTime StartDate, DateTime EndDate);

        #endregion
        #endregion
        #region DeviceSchedule

        #region SaveDeviceSchedule

        void SaveDeviceSchedule(long[] serialNbr, int commandTimeout = 0);
        #endregion

        #endregion

        #region GetFuelNozzlesForMeter

        List<long> GetFuelNozzlesForMeter(long IdMeter);

        #endregion

        #region Refuel
        #region GetRefuelFilter

        /// <summary>
        /// Gets Refuel list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRefuel funcion</param>
        /// <param name="IdRefuel">Specifies filter for ID_REFUEL column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFUEL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Refuel list</returns>
        List<OpRefuel> GetRefuelFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdRefuel = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataSource = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
            long? topCount = null, string customWhereClause = null,
            List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion

        #region GetRefuelDataTemporals
        Dictionary<DateTime, List<OpDataTemporal>> GetRefuelDataTemporals(long[] idLocation, long[] idMeter, DateTime rangeStart, DateTime rangeEnd, string orderNumber, Enums.AggregationType aggrType, bool onlyJoinedRefuels = true, int commandTimeout = 0);
        Dictionary<DateTime, List<OpDataTemporal>> GetRefuelDataTemporals(long? idLocation, long idMeter, DateTime _dataStartTime, DateTime _dataEndTime, Enums.AggregationType aggregationType, bool onlyJoinedRefuels = true, int commandTimeout = 0);
        #endregion

        #endregion

        #region Report

        #region SaveReport

        /// <summary>
        /// Saves the Report object to the database
        /// </summary>
        /// <param name="toBeSaved">Report to save</param>
        /// <returns>Report Id</returns>
        int SaveReport(OpReport toBeSaved, bool useDBCollector = false);

        #endregion

        #region SaveReportData

        /// <summary>
        /// Saves the ReportData object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportData to save</param>
        /// <returns>ReportData Id</returns>
        long SaveReportData(OpReportData toBeSaved, bool useDBCollector = false);

        #endregion

        #region DeleteReport

        /// <summary>
        /// Deletes the Report object from the database
        /// </summary>
        /// <param name="toBeDeleted">Report to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted from connected data base or from DBCollector web service</param>
        void DeleteReport(OpReport toBeDeleted, bool useDBCollector = false);

        #endregion

        #region DeleteReportData
        /// <summary>
        /// Deletes the ReportData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportData to delete</param>
        void DeleteReportData(OpReportData toBeDeleted, bool useDBCollector = false);
        #endregion

        #endregion

        #region ReportPerformance

        void DeleteReportPerformance(int[] IdReport = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0);

        #endregion

        #region TransmissionDriverPerformance

        void DeleteTransmissionDriverPerformance(int[] IdTransmissionDriver = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0);

        #endregion

        #region SaveConsumerTransaction

        /// <summary>
        /// Gets last ConsumerTransaction object from the database, function makes request to DB
        /// </summary>
        /// <param name="idConsumer">Consumer to get data from</param>
        /// <returns>Consumer Transaction Object</returns>
        OpConsumerTransaction GetLastConsumerTransaction(int idConsumer, int? idTransactionType);

        /// <summary>
        /// Gets first ConsumerTransaction object from the database, function makes request to DB
        /// </summary>
        /// <param name="IdConsumer">Consumer to get data from</param>
        /// <returns>Consumer Transaction Object</returns>
        OpConsumerTransaction GetFirstConsumerTransaction(int IdConsumer, int? IdTransactionType, DateTime? startTime = null);
        #endregion

        #region SGM initialize data
        List<OpData> SgmGetDataForLocationFromGivenDistributor(int[] idDistributorArray, long[] idDataTypeArray);

        #endregion
#endif
    }
}
