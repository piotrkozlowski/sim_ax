﻿using System;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DAQ;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Objects
{
    public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region Initialize
        //private void Initialize()
        //{
        //    DataArchCacheEnabled = true;
        //    DataArchTrashCacheEnabled = true;
        //    DataArchTrashStatusCacheEnabled = true;
        //    PacketCacheEnabled = true;
        //    TransmissionDriverCacheEnabled = true;
        //    TransmissionDriverTypeCacheEnabled = true;
        //    TransmissionProtocolCacheEnabled = true;
        //}
        #endregion

        #region Audit

        /// <summary>
        /// Indicates whether the Audit is cached.
        /// </summary>
        bool AuditCacheEnabledDAQ { get; set; }

        /// <summary>
        /// Gets all Audit objects
        /// </summary>
        List<OpAudit> GetAllAuditDAQ();


        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <returns>Audit object</returns>
        OpAudit GetAuditDAQ(long Id);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <returns>Audit list</returns>
        List<OpAudit> GetAuditDAQ(long[] Ids);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit object</returns>
        OpAudit GetAuditDAQ(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit list</returns>
        List<OpAudit> GetAuditDAQ(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Audit object to the database
        /// </summary>
        /// <param name="toBeSaved">Audit to save</param>
        /// <returns>Audit Id</returns>
        long SaveAuditDAQ(OpAudit toBeSaved);

        /// <summary>
        /// Deletes the Audit object from the database
        /// </summary>
        /// <param name="toBeDeleted">Audit to delete</param>
        void DeleteAuditDAQ(OpAudit toBeDeleted);

        /// <summary>
        /// Clears the Audit object cache
        /// </summary>
        void ClearAuditCacheDAQ();
        #endregion

        #region Connection

        /// <summary>
        /// Indicates whether the Connection is cached.
        /// </summary>
        bool ConnectionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Connection objects
        /// </summary>
        List<OpConnection> GetAllConnection();


        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Id">Connection Id</param>
        /// <returns>Connection object</returns>
        OpConnection GetConnection(long Id);

        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Ids">Connection Ids</param>
        /// <returns>Connection list</returns>
        List<OpConnection> GetConnection(long[] Ids);

        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Id">Connection Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Connection object</returns>
        OpConnection GetConnection(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Ids">Connection Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Connection list</returns>
        List<OpConnection> GetConnection(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Connection object to the database
        /// </summary>
        /// <param name="toBeSaved">Connection to save</param>
        /// <returns>Connection Id</returns>
        long SaveConnection(OpConnection toBeSaved);

        /// <summary>
        /// Deletes the Connection object from the database
        /// </summary>
        /// <param name="toBeDeleted">Connection to delete</param>
        void DeleteConnection(OpConnection toBeDeleted);

        /// <summary>
        /// Clears the Connection object cache
        /// </summary>
        void ClearConnectionCache();
        #endregion

        #region DataArchTrash

        #endregion

        #region DataArchTrashStatus

        /// <summary>
        /// Indicates whether the DataArchTrashStatus is cached.
        /// </summary>
        bool DataArchTrashStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataArchTrashStatus objects
        /// </summary>
        List<OpDataArchTrashStatus> GetAllDataArchTrashStatus();


        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Id">DataArchTrashStatus Id</param>
        /// <returns>DataArchTrashStatus object</returns>
        OpDataArchTrashStatus GetDataArchTrashStatus(int Id);

        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Ids">DataArchTrashStatus Ids</param>
        /// <returns>DataArchTrashStatus list</returns>
        List<OpDataArchTrashStatus> GetDataArchTrashStatus(int[] Ids);

        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Id">DataArchTrashStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArchTrashStatus object</returns>
        OpDataArchTrashStatus GetDataArchTrashStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Ids">DataArchTrashStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArchTrashStatus list</returns>
        List<OpDataArchTrashStatus> GetDataArchTrashStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataArchTrashStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">DataArchTrashStatus to save</param>
        /// <returns>DataArchTrashStatus Id</returns>
        int SaveDataArchTrashStatus(OpDataArchTrashStatus toBeSaved);

        /// <summary>
        /// Deletes the DataArchTrashStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataArchTrashStatus to delete</param>
        void DeleteDataArchTrashStatus(OpDataArchTrashStatus toBeDeleted);

        /// <summary>
        /// Clears the DataArchTrashStatus object cache
        /// </summary>
        void ClearDataArchTrashStatusCache();
        #endregion

        #region Packet

        /// <summary>
        /// Indicates whether the Packet is cached.
        /// </summary>
        bool PacketCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Packet objects
        /// </summary>
        List<OpPacket> GetAllPacket();


        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Id">Packet Id</param>
        /// <returns>Packet object</returns>
        OpPacket GetPacket(long Id);

        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Ids">Packet Ids</param>
        /// <returns>Packet list</returns>
        List<OpPacket> GetPacket(long[] Ids);

        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Id">Packet Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Packet object</returns>
        OpPacket GetPacket(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Ids">Packet Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Packet list</returns>
        List<OpPacket> GetPacket(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Packet object to the database
        /// </summary>
        /// <param name="toBeSaved">Packet to save</param>
        /// <returns>Packet Id</returns>
        long SavePacket(OpPacket toBeSaved);

        /// <summary>
        /// Deletes the Packet object from the database
        /// </summary>
        /// <param name="toBeDeleted">Packet to delete</param>
        void DeletePacket(OpPacket toBeDeleted);

        /// <summary>
        /// Clears the Packet object cache
        /// </summary>
        void ClearPacketCache();
        #endregion

        #region PacketHistory

        /// <summary>
        /// Indicates whether the PacketHistory is cached.
        /// </summary>
        bool PacketHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PacketHistory objects
        /// </summary>
        List<OpPacketHistory> GetAllPacketHistory();


        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Id">PacketHistory Id</param>
        /// <returns>PacketHistory object</returns>
        OpPacketHistory GetPacketHistory(long Id);

        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Ids">PacketHistory Ids</param>
        /// <returns>PacketHistory list</returns>
        List<OpPacketHistory> GetPacketHistory(long[] Ids);

        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Id">PacketHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketHistory object</returns>
        OpPacketHistory GetPacketHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Ids">PacketHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketHistory list</returns>
        List<OpPacketHistory> GetPacketHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the PacketHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">PacketHistory to save</param>
        /// <returns>PacketHistory Id</returns>
        long SavePacketHistory(OpPacketHistory toBeSaved);

        /// <summary>
        /// Deletes the PacketHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">PacketHistory to delete</param>
        void DeletePacketHistory(OpPacketHistory toBeDeleted);

        /// <summary>
        /// Clears the PacketHistory object cache
        /// </summary>
        void ClearPacketHistoryCache();
        #endregion

        #region PacketStatus

        /// <summary>
        /// Indicates whether the PacketStatus is cached.
        /// </summary>
        bool PacketStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PacketStatus objects
        /// </summary>
        List<OpPacketStatus> GetAllPacketStatus();


        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Id">PacketStatus Id</param>
        /// <returns>PacketStatus object</returns>
        OpPacketStatus GetPacketStatus(int Id);

        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Ids">PacketStatus Ids</param>
        /// <returns>PacketStatus list</returns>
        List<OpPacketStatus> GetPacketStatus(int[] Ids);

        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Id">PacketStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketStatus object</returns>
        OpPacketStatus GetPacketStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Ids">PacketStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketStatus list</returns>
        List<OpPacketStatus> GetPacketStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the PacketStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">PacketStatus to save</param>
        /// <returns>PacketStatus Id</returns>
        int SavePacketStatus(OpPacketStatus toBeSaved);

        /// <summary>
        /// Deletes the PacketStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">PacketStatus to delete</param>
        void DeletePacketStatus(OpPacketStatus toBeDeleted);

        /// <summary>
        /// Clears the PacketStatus object cache
        /// </summary>
        void ClearPacketStatusCache();
        #endregion

        #region RouteTable

        /// <summary>
        /// Indicates whether the RouteTable is cached.
        /// </summary>
        bool RouteTableCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteTable objects
        /// </summary>
        List<OpRouteTable> GetAllRouteTable();


        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Id">RouteTable Id</param>
        /// <returns>RouteTable object</returns>
        OpRouteTable GetRouteTable(int Id);

        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Ids">RouteTable Ids</param>
        /// <returns>RouteTable list</returns>
        List<OpRouteTable> GetRouteTable(int[] Ids);

        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Id">RouteTable Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTable object</returns>
        OpRouteTable GetRouteTable(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Ids">RouteTable Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTable list</returns>
        List<OpRouteTable> GetRouteTable(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Deletes the RouteTable object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteTable to delete</param>
        void DeleteRouteTable(OpRouteTable toBeDeleted);

        /// <summary>
        /// Clears the RouteTable object cache
        /// </summary>
        void ClearRouteTableCache();
        #endregion

        #region RouteTableBackup

        /// <summary>
        /// Indicates whether the RouteTableBackup is cached.
        /// </summary>
        bool RouteTableBackupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteTableBackup objects
        /// </summary>
        List<OpRouteTableBackup> GetAllRouteTableBackup();


        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Id">RouteTableBackup Id</param>
        /// <returns>RouteTableBackup object</returns>
        OpRouteTableBackup GetRouteTableBackup(int Id);

        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Ids">RouteTableBackup Ids</param>
        /// <returns>RouteTableBackup list</returns>
        List<OpRouteTableBackup> GetRouteTableBackup(int[] Ids);

        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Id">RouteTableBackup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTableBackup object</returns>
        OpRouteTableBackup GetRouteTableBackup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Ids">RouteTableBackup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTableBackup list</returns>
        List<OpRouteTableBackup> GetRouteTableBackup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteTableBackup object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteTableBackup to save</param>
        /// <returns>RouteTableBackup Id</returns>
        int SaveRouteTableBackup(OpRouteTableBackup toBeSaved);

        /// <summary>
        /// Deletes the RouteTableBackup object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteTableBackup to delete</param>
        void DeleteRouteTableBackup(OpRouteTableBackup toBeDeleted);

        /// <summary>
        /// Clears the RouteTableBackup object cache
        /// </summary>
        void ClearRouteTableBackupCache();
        #endregion

        #region TransmissionDriver

        /// <summary>
        /// Indicates whether the TransmissionDriver is cached.
        /// </summary>
        bool TransmissionDriverCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriver objects
        /// </summary>
        List<OpTransmissionDriver> GetAllTransmissionDriver();


        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <returns>TransmissionDriver object</returns>
        OpTransmissionDriver GetTransmissionDriver(int Id);

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <returns>TransmissionDriver list</returns>
        List<OpTransmissionDriver> GetTransmissionDriver(int[] Ids);

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver object</returns>
        OpTransmissionDriver GetTransmissionDriver(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver list</returns>
        List<OpTransmissionDriver> GetTransmissionDriver(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TransmissionDriver object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriver to save</param>
        /// <returns>TransmissionDriver Id</returns>
        int SaveTransmissionDriver(OpTransmissionDriver toBeSaved);

        /// <summary>
        /// Deletes the TransmissionDriver object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriver to delete</param>
        void DeleteTransmissionDriver(OpTransmissionDriver toBeDeleted);

        /// <summary>
        /// Clears the TransmissionDriver object cache
        /// </summary>
        void ClearTransmissionDriverCache();
        #endregion

        #region TransmissionDriverData

        /// <summary>
        /// Indicates whether the TransmissionDriverData is cached.
        /// </summary>
        bool TransmissionDriverDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriverData objects
        /// </summary>
        List<OpTransmissionDriverData> GetAllTransmissionDriverData();


        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Id">TransmissionDriverData Id</param>
        /// <returns>TransmissionDriverData object</returns>
        OpTransmissionDriverData GetTransmissionDriverData(long Id);

        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverData Ids</param>
        /// <returns>TransmissionDriverData list</returns>
        List<OpTransmissionDriverData> GetTransmissionDriverData(long[] Ids);

        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Id">TransmissionDriverData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverData object</returns>
        OpTransmissionDriverData GetTransmissionDriverData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverData list</returns>
        List<OpTransmissionDriverData> GetTransmissionDriverData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TransmissionDriverData object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriverData to save</param>
        /// <returns>TransmissionDriverData Id</returns>
        long SaveTransmissionDriverData(OpTransmissionDriverData toBeSaved);

        /// <summary>
        /// Deletes the TransmissionDriverData object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriverData to delete</param>
        void DeleteTransmissionDriverData(OpTransmissionDriverData toBeDeleted);

        /// <summary>
        /// Clears the TransmissionDriverData object cache
        /// </summary>
        void ClearTransmissionDriverDataCache();
        #endregion

        #region TransmissionDriverType

        /// <summary>
        /// Indicates whether the TransmissionDriverType is cached.
        /// </summary>
        bool TransmissionDriverTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriverType objects
        /// </summary>
        List<OpTransmissionDriverType> GetAllTransmissionDriverType();


        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Id">TransmissionDriverType Id</param>
        /// <returns>TransmissionDriverType object</returns>
        OpTransmissionDriverType GetTransmissionDriverType(int Id);

        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverType Ids</param>
        /// <returns>TransmissionDriverType list</returns>
        List<OpTransmissionDriverType> GetTransmissionDriverType(int[] Ids);

        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Id">TransmissionDriverType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverType object</returns>
        OpTransmissionDriverType GetTransmissionDriverType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverType list</returns>
        List<OpTransmissionDriverType> GetTransmissionDriverType(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TransmissionDriverType object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriverType to save</param>
        /// <returns>TransmissionDriverType Id</returns>
        int SaveTransmissionDriverType(OpTransmissionDriverType toBeSaved);

        /// <summary>
        /// Deletes the TransmissionDriverType object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriverType to delete</param>
        void DeleteTransmissionDriverType(OpTransmissionDriverType toBeDeleted);

        /// <summary>
        /// Clears the TransmissionDriverType object cache
        /// </summary>
        void ClearTransmissionDriverTypeCache();
        #endregion

        #region TransmissionProtocol

        /// <summary>
        /// Indicates whether the TransmissionProtocol is cached.
        /// </summary>
        bool TransmissionProtocolCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionProtocol objects
        /// </summary>
        List<OpTransmissionProtocol> GetAllTransmissionProtocol();


        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Id">TransmissionProtocol Id</param>
        /// <returns>TransmissionProtocol object</returns>
        OpTransmissionProtocol GetTransmissionProtocol(int Id);

        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Ids">TransmissionProtocol Ids</param>
        /// <returns>TransmissionProtocol list</returns>
        List<OpTransmissionProtocol> GetTransmissionProtocol(int[] Ids);

        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Id">TransmissionProtocol Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionProtocol object</returns>
        OpTransmissionProtocol GetTransmissionProtocol(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Ids">TransmissionProtocol Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionProtocol list</returns>
        List<OpTransmissionProtocol> GetTransmissionProtocol(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the TransmissionProtocol object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionProtocol to save</param>
        /// <returns>TransmissionProtocol Id</returns>
        int SaveTransmissionProtocol(OpTransmissionProtocol toBeSaved);

        /// <summary>
        /// Deletes the TransmissionProtocol object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionProtocol to delete</param>
        void DeleteTransmissionProtocol(OpTransmissionProtocol toBeDeleted);

        /// <summary>
        /// Clears the TransmissionProtocol object cache
        /// </summary>
        void ClearTransmissionProtocolCache();
        #endregion
#endif
    }
}
