﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects
{
    public interface IImportData
    {
        System.Threading.Tasks.Task ImportTask { get; set; }
        List<Enums.ReferenceType> GetSupportedObjects();

        List<Type> GetSupportedImportTypes();//Lista typów jakie mecchanizm jest wstanie zintepretować

        Dictionary<Enums.ReferenceType, List<OpDataType>> GetRequiredFields(List<Enums.ReferenceType> referenceType);
        
        OpImportProgressStep GetCurrentProgressState();

        void Initialize(object[] parameters);

        void LoadData(object dataToLoad);

        List<OpImportItemSummary> GetSummary();

        List<OpImportItemSummary> ValidateData();

        Enums.ImportStatus StartImport();
        void StopImport();
    }

    [Serializable, DataContract]
    public class OpImportItemSummary
    {
        [DataMember]
        public object Key { get; set; }
        [DataMember]
        public Enums.ImportStatus ImportStatus { get; set; }
        [DataMember]
        public string Description { get; set; }

        public OpImportItemSummary() { }
        public OpImportItemSummary(object Key, Enums.ImportStatus ImportStatus, string Description)
        {
            this.Key = Key;
            this.ImportStatus = ImportStatus;
            this.Description = Description;
        }
    }

    [Serializable, DataContract]
    public class OpImportProgressStep
    {
        [DataMember]
        public Enums.ImportStatus ImportStatus { get; set; }
        [DataMember]
        public long ItemsCount { get; set; }
        [DataMember]
        public long ProceedItemsCount { get; set; }
        [DataMember]
        public long ProceedSuccessItemsCount { get; set; }
        [DataMember]
        public long ProceedErrorItemsCount { get; set; }
        [DataMember]
        public long ProceedWarningItemsCount { get; set; }

        public OpImportProgressStep() { }
        public OpImportProgressStep(Enums.ImportStatus ImportStatus, long ItemsCount, long ProceedItemsCount, long ProceedSuccessItemsCount, long ProceedErrorItemsCount, long ProceedWarningItemsCount)
        {
            this.ImportStatus = ImportStatus;
            this.ItemsCount = ItemsCount;
            this.ProceedItemsCount = ProceedItemsCount;
            this.ProceedSuccessItemsCount = ProceedSuccessItemsCount;
            this.ProceedErrorItemsCount = ProceedErrorItemsCount;
            this.ProceedWarningItemsCount = ProceedWarningItemsCount;
        }
    }

    [Serializable, DataContract]
    public class OpImportData
    {
        [DataMember]
        public string[] Headers { get; set; }

        [DataMember]
        public object[][] Data { get; set; }
        public OpImportData() { }
        public OpImportData(string[] Headers, object[][] Data)
        {
            this.Headers = Headers;
            this.Data = Data;
        }
    }
}
