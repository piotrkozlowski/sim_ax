﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Data;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects
{
     public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region GetActionFilter

        /// <summary>
        /// Gets Action list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAction funcion</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="IdActionStatus">Specifies filter for ID_ACTION_STATUS column</param>
        /// <param name="IdActionData">Specifies filter for ID_ACTION_DATA column</param>
        /// <param name="IdActionParent">Specifies filter for ID_ACTION_PARENT column</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Action list</returns>
        List<OpAction> GetActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAction = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdActionType = null, int[] IdActionStatus = null,
                            long[] IdActionData = null, long[] IdActionParent = null, long[] IdDataArch = null, int[] IdModule = null, int[] IdOperator = null,
                            TypeDateTimeCode CreationDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActionHistoryDataFilter
        /// <summary>
        /// Gets ActionHistoryData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionHistoryData funcion</param>
        /// <param name="IdActionHistoryData">Specifies filter for ID_ACTION_HISTORY_DATA column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_HISTORY_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionHistoryData list</returns>
        List<OpActionHistoryData> GetActionHistoryDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionHistoryData = null, int[] ArgNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActionSmsTextDataFilter

        /// <summary>
        /// Gets ActionSmsTextData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionSmsTextData funcion</param>
        /// <param name="IdActionSmsText">Specifies filter for ID_ACTION_SMS_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_SMS_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionSmsTextData list</returns>
        List<OpActionSmsTextData> GetActionSmsTextDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionSmsText = null, int[] IdLanguage = null, int[] ArgNbr = null, long[] IdDataType = null, int[] IdUnit = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActionStatusFilter

        /// <summary>
        /// Gets ActionStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionStatus funcion</param>
        /// <param name="IdActionStatus">Specifies filter for ID_ACTION_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionStatus list</returns>
        List<OpActionStatus> GetActionStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActionTypeFilter
        /// <summary>
        /// Gets ActionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionType funcion</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="IdActionTypeClass">Specifies filter for ID_ACTION_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IsActionDataFixed">Specifies filter for IS_ACTION_DATA_FIXED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionType list</returns>
        List<OpActionType> GetActionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionType = null, int[] IdActionTypeClass = null, string Name = null, string PluginName = null, long[] IdDescr = null, bool? IsActionDataFixed = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActionTypeClassFilter

        /// <summary>
        /// Gets ActionTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeClass funcion</param>
        /// <param name="IdActionTypeClass">Specifies filter for ID_ACTION_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionTypeClass list</returns>
        List<OpActionTypeClass> GetActionTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActionTypeGroupTypeFilter

        /// <summary>
        /// Gets ActionTypeGroupType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeGroupType funcion</param>
        /// <param name="IdActionTypeGroupType">Specifies filter for ID_ACTION_TYPE_GROUP_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_GROUP_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionTypeGroupType list</returns>
        List<OpActionTypeGroupType> GetActionTypeGroupTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeGroupType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActivityFilter

        /// <summary>
        /// Gets Activity list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActivity funcion</param>
        /// <param name="IdActivity">Specifies filter for ID_ACTIVITY column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTIVITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Activity list</returns>
        List<OpActivity> GetActivityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActivity = null, string Name = null, long[] IdDescr = null, int[] IdReferenceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActorFilter

        /// <summary>
        /// Gets Actor list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActor funcion</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Surname">Specifies filter for SURNAME column</param>
        /// <param name="City">Specifies filter for CITY column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Postcode">Specifies filter for POSTCODE column</param>
        /// <param name="Email">Specifies filter for EMAIL column</param>
        /// <param name="Mobile">Specifies filter for MOBILE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Actor list</returns>
        List<OpActor> GetActorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActor = null, string Name = null, string Surname = null, string City = null, string Address = null, string Postcode = null,
                           string Email = null, string Mobile = null, string Phone = null, int[] IdLanguage = null, string Description = null,
                           long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActorGroupFilter

        /// <summary>
        /// Gets ActorGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActorGroup funcion</param>
        /// <param name="IdActorGroup">Specifies filter for ID_ACTOR_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="BuiltIn">Specifies filter for BUILT_IN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTOR_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActorGroup list</returns>
        List<OpActorGroup> GetActorGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActorGroup = null, string Name = null, bool? BuiltIn = null, int[] IdDistributor = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetActorInGroupFilter

        /// <summary>
        /// Gets ActorInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActorInGroup funcion</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdActorGroup">Specifies filter for ID_ACTOR_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActorInGroup list</returns>
        List<OpActorInGroup> GetActorInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActor = null, int[] IdActorGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmFilter

        /// <summary>
        /// Gets Alarm list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarm funcion</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Alarm list</returns>
        List<OpAlarm> GetAlarmFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarm = null, long[] IdAlarmEvent = null, int[] IdOperator = null, int[] IdAlarmGroup = null, int[] IdAlarmStatus = null, int[] IdTransmissionType = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmDataFilter

        /// <summary>
        /// Gets AlarmData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmData funcion</param>
        /// <param name="IdAlarmData">Specifies filter for ID_ALARM_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmData list</returns>
        List<OpAlarmData> GetAlarmDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmDefFilter

        /// <summary>
        /// Gets AlarmDef list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmDef funcion</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="IdDataTypeAlarm">Specifies filter for ID_DATA_TYPE_ALARM column</param>
        /// <param name="IdDataTypeConfig">Specifies filter for ID_DATA_TYPE_CONFIG column</param>
        /// <param name="IsEnabled">Specifies filter for IS_ENABLED column</param>
        /// <param name="IdAlarmText">Specifies filter for ID_ALARM_TEXT column</param>
        /// <param name="IdAlarmData">Specifies filter for ID_ALARM_DATA column</param>
        /// <param name="CheckLastAlarm">Specifies filter for CHECK_LAST_ALARM column</param>
        /// <param name="SuspensionTime">Specifies filter for SUSPENSION_TIME column</param>
        /// <param name="IsConfirmable">Specifies filter for IS_CONFIRMABLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmDef list</returns>
        List<OpAlarmDef> GetAlarmDefFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmDef = null, string Name = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdAlarmType = null,
                           long[] IdDataTypeAlarm = null, int[] IndexNbr = null, long[] IdDataTypeConfig = null, bool? IsEnabled = null, long[] IdAlarmText = null, long[] IdAlarmData = null,
                           bool? CheckLastAlarm = null, int[] SuspensionTime = null, bool? IsConfirmable = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmDefGroupFilter

        /// <summary>
        /// Gets AlarmDefGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmDefGroup funcion</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="ConfirmLevel">Specifies filter for CONFIRM_LEVEL column</param>
        /// <param name="ConfirmTimeout">Specifies filter for CONFIRM_TIMEOUT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmDefGroup list</returns>
        List<OpAlarmDefGroup> GetAlarmDefGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmDef = null, int[] IdAlarmGroup = null, int[] IdTransmissionType = null, int[] ConfirmLevel = null, int[] ConfirmTimeout = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmEventFilter

        /// <summary>
        /// Gets AlarmEvent list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmEvent funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmEvent list</returns>
        List<OpAlarmEvent> GetAlarmEventFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, long[] IdAlarmDef = null, long[] IdDataArch = null, bool? IsActive = null, long[] SerialNbr = null, long[] IdMeter = null,
                           long[] IdLocation = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmGroupFilter

        /// <summary>
        /// Gets AlarmGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmGroup funcion</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmGroup list</returns>
        List<OpAlarmGroup> GetAlarmGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmGroup = null, string Name = null, string Description = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmGroupOperatorFilter

        /// <summary>
        /// Gets AlarmGroupOperator list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmGroupOperator funcion</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmGroupOperator list</returns>
        List<OpAlarmGroupOperator> GetAlarmGroupOperatorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmGroup = null, int[] IdOperator = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmHistoryFilter

        /// <summary>
        /// Gets AlarmHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmHistory funcion</param>
        /// <param name="IdAlarmHistory">Specifies filter for ID_ALARM_HISTORY column</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="StartRuleHost">Specifies filter for START_RULE_HOST column</param>
        /// <param name="StartRuleUser">Specifies filter for START_RULE_USER column</param>
        /// <param name="EndRuleHost">Specifies filter for END_RULE_HOST column</param>
        /// <param name="EndRuleUser">Specifies filter for END_RULE_USER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmHistory list</returns>
        List<OpAlarmHistory> GetAlarmHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmHistory = null, long[] IdAlarm = null, int[] IdAlarmStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string StartRuleHost = null,
                           string StartRuleUser = null, string EndRuleHost = null, string EndRuleUser = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmMessageFilter

        /// <summary>
        /// Gets AlarmMessage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmMessage funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="AlarmMessage">Specifies filter for ALARM_MESSAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmMessage list</returns>
        List<OpAlarmMessage> GetAlarmMessageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, int[] IdLanguage = null, string AlarmMessage = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmStatusFilter

        /// <summary>
        /// Gets AlarmStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmStatus funcion</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmStatus list</returns>
        List<OpAlarmStatus> GetAlarmStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmTextFilter

        /// <summary>
        /// Gets AlarmText list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmText funcion</param>
        /// <param name="IdAlarmText">Specifies filter for ID_ALARM_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="AlarmText">Specifies filter for ALARM_TEXT column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmText list</returns>
        List<OpAlarmText> GetAlarmTextFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmText = null, int[] IdLanguage = null, string Name = null, string AlarmText = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmTextDataTypeFilter

        /// <summary>
        /// Gets AlarmTextDataType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmTextDataType funcion</param>
        /// <param name="IdAlarmText">Specifies filter for ID_ALARM_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmTextDataType list</returns>
        List<OpAlarmTextDataType> GetAlarmTextDataTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmText = null, int[] IdLanguage = null, int[] ArgNbr = null, long[] IdDataType = null, int[] IdUnit = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAlarmTypeFilter

        /// <summary>
        /// Gets AlarmType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmType funcion</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="Symbol">Specifies filter for SYMBOL column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmType list</returns>
         List<OpAlarmType> GetAlarmTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmType = null, string Symbol = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetArticleFilter

        /// <summary>
        /// Gets Article list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllArticle funcion</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="ExternalId">Specifies filter for EXTERNAL_ID column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IsUnique">Specifies filter for IS_UNIQUE column</param>
        /// <param name="Visible">Specifies filter for VISIBLE column</param>
        /// <param name="IsAiut">Specifies filter for IS_AIUT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ARTICLE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Article list</returns>
        List<OpArticle> GetArticleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdArticle = null, long[] ExternalId = null, string Name = null, string Description = null, bool? IsUnique = null, bool? Visible = null,
                           bool? IsAiut = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetArticleDataFilter

        /// <summary>
        /// Gets ArticleData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllArticleData funcion</param>
        /// <param name="IdArticleData">Specifies filter for ID_ARTICLE_DATA column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ARTICLE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ArticleData list</returns>
        List<OpArticleData> GetArticleDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdArticleData = null, long[] IdArticle = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetAtgTypeFilter

        /// <summary>
        /// Gets AtgType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAtgType funcion</param>
        /// <param name="IdAtgType">Specifies filter for ID_ATG_TYPE column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ATG_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AtgType list</returns>
        List<OpAtgType> GetAtgTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAtgType = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetAuditFilter

        /// <summary>
        /// Gets Audit list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAudit funcion</param>
        /// <param name="IdAudit">Specifies filter for ID_AUDIT column</param>
        /// <param name="BatchId">Specifies filter for BATCH_ID column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="Key1">Specifies filter for KEY1 column</param>
        /// <param name="Key2">Specifies filter for KEY2 column</param>
        /// <param name="Key3">Specifies filter for KEY3 column</param>
        /// <param name="Key4">Specifies filter for KEY4 column</param>
        /// <param name="Key5">Specifies filter for KEY5 column</param>
        /// <param name="Key6">Specifies filter for KEY6 column</param>
        /// <param name="ColumnName">Specifies filter for COLUMN_NAME column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="Host">Specifies filter for HOST column</param>
        /// <param name="Application">Specifies filter for APPLICATION column</param>
        /// <param name="Contextinfo">Specifies filter for CONTEXTINFO column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_AUDIT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Audit list</returns>
        List<OpAudit> GetAuditFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAudit = null, long[] BatchId = null, int[] ChangeType = null, string TableName = null, long[] Key1 = null, long[] Key2 = null,
                           long[] Key3 = null, long[] Key4 = null, long[] Key5 = null, long[] Key6 = null, string ColumnName = null,
                           TypeDateTimeCode Time = null, string User = null, string Host = null, string Application = null, string Contextinfo = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetCodeFilter

        /// <summary>
        /// Gets Code list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCode funcion</param>
        /// <param name="IdCode">Specifies filter for ID_CODE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdCodeType">Specifies filter for ID_CODE_TYPE column</param>
        /// <param name="CodeNbr">Specifies filter for CODE_NBR column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="ActivationTime">Specifies filter for ACTIVATION_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Code list</returns>
        List<OpCode> GetCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdCode = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdCodeType = null, int[] CodeNbr = null,
                           string Code = null, bool? IsActive = null, TypeDateTimeCode ActivationTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetCodeTypeFilter

        /// <summary>
        /// Gets CodeType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCodeType funcion</param>
        /// <param name="IdCodeType">Specifies filter for ID_CODE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdDataTypeFormat">Specifies filter for ID_DATA_TYPE_FORMAT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CODE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>CodeType list</returns>
        List<OpCodeType> GetCodeTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdCodeType = null, string Name = null, long[] IdDescr = null, int[] IdDataTypeFormat = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetCommandCodeParamMapFilter

        /// <summary>
        /// Gets CommandCodeParamMap list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCommandCodeParamMap funcion</param>
        /// <param name="IdCommandCodeParamMap">Specifies filter for ID_COMMAND_CODE_PARAM_MAP column</param>
        /// <param name="IdCommandCode">Specifies filter for ID_COMMAND_CODE column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="ByteOffset">Specifies filter for BYTE_OFFSET column</param>
        /// <param name="BitOffset">Specifies filter for BIT_OFFSET column</param>
        /// <param name="Length">Specifies filter for LENGTH column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdProtocol">Specifies filter for ID_PROTOCOL column</param>
        /// <param name="IsRequired">Specifies filter for IS_REQUIRED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_COMMAND_CODE_PARAM_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>CommandCodeParamMap list</returns>
        List<OpCommandCodeParamMap> GetCommandCodeParamMapFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdCommandCodeParamMap = null, long[] IdCommandCode = null, long[] ArgNbr = null, string Name = null, int[] ByteOffset = null, int[] BitOffset = null,
                           int[] Length = null, int[] IdDataTypeClass = null, int[] IdUnit = null, long[] IdDescr = null, int[] IdProtocol = null, bool? IsRequired = null, long ? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetCommandCodeTypeFilter

        /// <summary>
        /// Gets CommandCodeType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCommandCodeType funcion</param>
        /// <param name="IdCommandCode">Specifies filter for ID_COMMAND_CODE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_COMMAND_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>CommandCodeType list</returns>
        List<OpCommandCodeType> GetCommandCodeTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdCommandCode = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetComponentFilter

        /// <summary>
        /// Gets Component list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllComponent funcion</param>
        /// <param name="IdComponent">Specifies filter for ID_COMPONENT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_COMPONENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Component list</returns>
        List<OpComponent> GetComponentFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdComponent = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConfigurationProfileFilter

        /// <summary>
        /// Gets ConfigurationProfile list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfile funcion</param>
        /// <param name="IdConfigurationProfile">Specifies filter for ID_CONFIGURATION_PROFILE column</param>
        /// <param name="IdConfigurationProfileType">Specifies filter for ID_CONFIGURATION_PROFILE_TYPE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdOperatorCreated">Specifies filter for ID_OPERATOR_CREATED column</param>
        /// <param name="CreatedTime">Specifies filter for CREATED_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfile list</returns>
        List<OpConfigurationProfile> GetConfigurationProfileFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConfigurationProfile = null, int[] IdConfigurationProfileType = null, int[] IdDistributor = null, string Name = null, long[] IdDescr = null, int[] IdOperatorCreated = null,
                   TypeDateTimeCode CreatedTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConfigurationProfileDataFilter

        /// <summary>
        /// Gets ConfigurationProfileData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileData funcion</param>
        /// <param name="IdConfigurationProfileData">Specifies filter for ID_CONFIGURATION_PROFILE_DATA column</param>
        /// <param name="IdConfigurationProfile">Specifies filter for ID_CONFIGURATION_PROFILE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileData list</returns>
        List<OpConfigurationProfileData> GetConfigurationProfileDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConfigurationProfileData = null, long[] IdConfigurationProfile = null, int[] IdDeviceType = null, int[] IdDeviceTypeProfileStep = null, long[] IdDataType = null, int[] IndexNbr = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConfigurationProfileDeviceTypeFilter

        /// <summary>
        /// Gets ConfigurationProfileDeviceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileDeviceType funcion</param>
        /// <param name="IdConfigurationProfile">Specifies filter for ID_CONFIGURATION_PROFILE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileDeviceType list</returns>
        List<OpConfigurationProfileDeviceType> GetConfigurationProfileDeviceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConfigurationProfile = null, int[] IdDeviceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConfigurationProfileTypeFilter

        /// <summary>
        /// Gets ConfigurationProfileType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileType funcion</param>
        /// <param name="IdConfigurationProfileType">Specifies filter for ID_CONFIGURATION_PROFILE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileType list</returns>
        List<OpConfigurationProfileType> GetConfigurationProfileTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConfigurationProfileType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConfigurationProfileTypeStepFilter

        /// <summary>
        /// Gets ConfigurationProfileTypeStep list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileTypeStep funcion</param>
        /// <param name="IdConfigurationProfileType">Specifies filter for ID_CONFIGURATION_PROFILE_TYPE column</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileTypeStep list</returns>
        List<OpConfigurationProfileTypeStep> GetConfigurationProfileTypeStepFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConfigurationProfileType = null, int[] IdDeviceTypeProfileStep = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerFilter

        /// <summary>
        /// Gets Consumer list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumer funcion</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdConsumerType">Specifies filter for ID_CONSUMER_TYPE column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IsBlocked">Specifies filter for IS_BLOCKED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Consumer list</returns>
        List<OpConsumer> GetConsumerFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumer = null, int[] IdConsumerType = null, int[] IdActor = null, int[] IdDistributor = null, string Description = null, bool IsBlocked = false, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerDataFilter

        /// <summary>
        /// Gets ConsumerData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerData funcion</param>
        /// <param name="IdConsumerData">Specifies filter for ID_CONSUMER_DATA column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerData list</returns>
        List<OpConsumerData> GetConsumerDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerData = null, int[] IdConsumer = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerNotificationFilter

        /// <summary>
        /// Gets ConsumerNotification list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerNotification funcion</param>
        /// <param name="IdConsumerNotification">Specifies filter for ID_CONSUMER_NOTIFICATION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdNotificationDeliveryType">Specifies filter for ID_NOTIFICATION_DELIVERY_TYPE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_NOTIFICATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerNotification list</returns>
        List<OpConsumerNotification> GetConsumerNotificationFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerNotification = null, int[] IdConsumer = null, int[] IdNotificationDeliveryType = null, int[] IdOperator = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadCustomData = true);
        #endregion
        #region GetConsumerNotificationHistoryFilter
        /// <summary>
        /// Gets ConsumerNotificationHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerNotificationHistory funcion</param>
        /// <param name="IdConsumerNotificationHistory">Specifies filter for ID_CONSUMER_NOTIFICATION_HISTORY column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdNotificationDeliveryType">Specifies filter for ID_NOTIFICATION_DELIVERY_TYPE column</param>
        /// <param name="IdNotificationType">Specifies filter for ID_NOTIFICATION_TYPE column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="Body">Specifies filter for BODY column</param>
        /// <param name="DeliveryAddress">Specifies filter for DELIVERY_ADDRESS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_NOTIFICATION_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerNotificationHistory list</returns>
        List<OpConsumerNotificationHistory> GetConsumerNotificationHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerNotificationHistory = null, int[] IdConsumer = null, int[] IdNotificationDeliveryType = null, int[] IdNotificationType = null, TypeDateTimeCode Time = null, string Body = null,
                           string DeliveryAddress = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetConsumerPaymentModuleFilter

        /// <summary>
        /// Gets ConsumerPaymentModule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerPaymentModule funcion</param>
        /// <param name="IdConsumerPaymentModule">Specifies filter for ID_CONSUMER_PAYMENT_MODULE column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="PaymentSystemNumber">Specifies filter for PAYMENT_SYSTEM_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_PAYMENT_MODULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerPaymentModule list</returns>
        List<OpConsumerPaymentModule> GetConsumerPaymentModuleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerPaymentModule = null, int[] IdConsumer = null, int[] IdPaymentModule = null, string PaymentSystemNumber = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetConsumerTransactionFilter
        /// <summary>
        /// Gets ConsumerTransaction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransaction funcion</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdConsumerTransactionType">Specifies filter for ID_CONSUMER_TRANSACTION_TYPE column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="TransactionGuid">Specifies filter for TRANSACTION_GUID column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="IdPrevious consumerTransaction">Specifies filter for ID_PREVIOUS_ CONSUMER_TRANSACTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransaction list</returns>
        List<OpConsumerTransaction> GetConsumerTransactionFilterCORE(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTransaction = null, int[] IdConsumer = null, int[] IdOperator = null, int[] IdDistributor = null, int[] IdConsumerTransactionType = null, int[] IdTariff = null,
                           TypeDateTimeCode Time = null, long[] IdConsumerSettlement = null, int[] IdPaymentModule = null, string TransactionGuid = null, long[] IdPacket = null,
                           long[] IdAction = null, long[] IdPreviousConsumerTransaction = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion

        #region GetLastConsumerTransactionCORE

        /// <summary>
        /// Gets last ConsumerTransaction object from the database, function makes request to DB
        /// </summary>
        /// <param name="idConsumer">Consumer to get data from</param>
        /// <param name="idTransactionType"></param>
        /// <returns>Consumer Transaction Object</returns>
        OpConsumerTransaction GetLastConsumerTransactionCORE(int idConsumer, int? idTransactionType);

        #endregion GetLastConsumerTransactionCORE

        #region GetConsumerTransactionDataFilter

        /// <summary>
        /// Gets ConsumerTransactionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransactionData funcion</param>
        /// <param name="IdConsumerTransactionData">Specifies filter for ID_CONSUMER_TRANSACTION_DATA column</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransactionData list</returns>
        List<OpConsumerTransactionData> GetConsumerTransactionDataFilterCORE(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTransactionData = null, long[] IdConsumerTransaction = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetConsumerTransactionTypeFilter

        /// <summary>
        /// Gets ConsumerTransactionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransactionType funcion</param>
        /// <param name="IdConsumerTransactionType">Specifies filter for ID_CONSUMER_TRANSACTION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransactionType list</returns>
        List<OpConsumerTransactionType> GetConsumerTransactionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerTransactionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetConsumerTypeFilter

        /// <summary>
        /// Gets ConsumerType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerType funcion</param>
        /// <param name="IdConsumerType">Specifies filter for ID_CONSUMER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerType list</returns>
        List<OpConsumerType> GetConsumerTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerType = null, string Name = null, long[] IdDescr = null, long? topCount = null
           , int[] IdDistributor = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerTypeDataFilter

        /// <summary>
        /// Gets ConsumerTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTypeData funcion</param>
        /// <param name="IdConsumerTypeData">Specifies filter for ID_CONSUMER_TYPE_DATA column</param>
        /// <param name="IdConsumerType">Specifies filter for ID_CONSUMER_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTypeData list</returns>
        List<OpConsumerTypeData> GetConsumerTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTypeData = null, int[] IdConsumerType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerSettlementFilter

        /// <summary>
        /// Gets ConsumerSettlement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlement funcion</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdTariffSettlementPeriod">Specifies filter for ID_TARIFF_SETTLEMENT_PERIOD column</param>
        /// <param name="SettlementTime">Specifies filter for SETTLEMENT_TIME column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdConsumerSettlementReason">Specifies filter for ID_CONSUMER_SETTLEMENT_REASON column</param>
        /// <param name="IdConsumerSettlementStatus">Specifies filter for ID_CONSUMER_SETTLEMENT_STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlement list</returns>
        List<OpConsumerSettlement> GetConsumerSettlementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerSettlement = null, int[] IdConsumer = null, int[] IdTariffSettlementPeriod = null, TypeDateTimeCode SettlementTime = null, int[] IdModule = null, int[] IdOperator = null,
                           int[] IdConsumerSettlementReason = null, int[] IdConsumerSettlementStatus = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerSettlementDocumentFilter

        /// <summary>
        /// Gets ConsumerSettlementDocument list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementDocument funcion</param>
        /// <param name="IdConsumerSettlementDocument">Specifies filter for ID_CONSUMER_SETTLEMENT_DOCUMENT column</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdSettlementDocumentType">Specifies filter for ID_SETTLEMENT_DOCUMENT_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_DOCUMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementDocument list</returns>
        List<OpConsumerSettlementDocument> GetConsumerSettlementDocumentFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerSettlementDocument = null, long[] IdConsumerSettlement = null, int[] IdSettlementDocumentType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerSettlementErrorFilter

        /// <summary>
        /// Gets ConsumerSettlementError list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementError funcion</param>
        /// <param name="IdConsumerSettlementError">Specifies filter for ID_CONSUMER_SETTLEMENT_ERROR column</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdConsumerSettlementErrorType">Specifies filter for ID_CONSUMER_SETTLEMENT_ERROR_TYPE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_ERROR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementError list</returns>
        List<OpConsumerSettlementError> GetConsumerSettlementErrorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementError = null, long[] IdConsumerSettlement = null, int[] IdConsumerSettlementErrorType = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerSettlementErrorTypeFilter

        /// <summary>
        /// Gets ConsumerSettlementErrorType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementErrorType funcion</param>
        /// <param name="IdConsumerSettlementErrorType">Specifies filter for ID_CONSUMER_SETTLEMENT_ERROR_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_ERROR_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementErrorType list</returns>
        List<OpConsumerSettlementErrorType> GetConsumerSettlementErrorTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementErrorType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerSettlementReasonFilter

        /// <summary>
        /// Gets ConsumerSettlementReason list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementReason funcion</param>
        /// <param name="IdConsumerSettlementReason">Specifies filter for ID_CONSUMER_SETTLEMENT_REASON column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_REASON DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementReason list</returns>
        List<OpConsumerSettlementReason> GetConsumerSettlementReasonFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementReason = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConsumerSettlementStatusFilter

        /// <summary>
        /// Gets ConsumerSettlementStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementStatus funcion</param>
        /// <param name="IdConsumerSettlementStatus">Specifies filter for ID_CONSUMER_SETTLEMENT_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IsError">Specifies filter for IS_ERROR column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementStatus list</returns>
        List<OpConsumerSettlementStatus> GetConsumerSettlementStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementStatus = null, string Name = null, bool? IsError = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetContentFilter

        /// <summary>
        /// Gets Content list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllContent funcion</param>
        /// <param name="IdContent">Specifies filter for ID_CONTENT column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONTENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Content list</returns>
        List<OpContent> GetContentFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdContent = null, int[] IdModule = null, string Name = null, string Description = null, int[] IdLanguage = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetContractFilter

        /// <summary>
        /// Gets Contract list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllContract funcion</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONTRACT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Contract list</returns>
        List<OpContract> GetContractFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdContract = null, string Name = null, string Descr = null, int[] IdDistributor = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetContractDataFilter

        /// <summary>
        /// Gets ContractData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllContractData funcion</param>
        /// <param name="IdContractData">Specifies filter for ID_CONTRACT_DATA column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONTRACT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ContractData list</returns>
        List<OpContractData> GetContractDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdContractData = null, int[] IdContract = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetConversionFilter

        /// <summary>
        /// Gets Conversion list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConversion funcion</param>
        /// <param name="IdConversion">Specifies filter for ID_CONVERSION column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="ReferenceValue">Specifies filter for REFERENCE_VALUE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Slope">Specifies filter for SLOPE column</param>
        /// <param name="Bias">Specifies filter for BIAS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONVERSION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Conversion list</returns>
        List<OpConversion> GetConversionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConversion = null, int[] IdReferenceType = null, long[] ReferenceValue = null, long[] IdDataType = null, int[] IndexNbr = null, Double[] Slope = null,
                           Double[] Bias = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetCurrencyFilter

        /// <summary>
        /// Gets Currency list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCurrency funcion</param>
        /// <param name="IdCurrency">Specifies filter for ID_CURRENCY column</param>
        /// <param name="Symbol">Specifies filter for SYMBOL column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CURRENCY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Currency list</returns>
        List<OpCurrency> GetCurrencyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdCurrency = null, string Symbol = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataArchFilter

        /// <summary>
        /// Gets DataArch list filtered CORE (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataArch funcion</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IsAction">Specifies filter for IS_ACTION column</param>
        /// <param name="IsAlarm">Specifies filter for IS_ALARM column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataArch list</returns>
        List<OpDataArch> GetDataArchFilter(bool loadNavigationProperties = true, long[] IdDataArch = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode Time = null, bool? IsAction = null,
                           bool? IsAlarm = null, long[] IdPacket = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataChangeFilter

        /// <summary>
        /// Gets DataChange list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataChange funcion</param>
        /// <param name="IdDataChange">Specifies filter for ID_DATA_CHANGE column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="ChangedId">Specifies filter for CHANGED_ID column</param>
        /// <param name="ChangedId2">Specifies filter for CHANGED_ID2 column</param>
        /// <param name="ChangedId3">Specifies filter for CHANGED_ID3 column</param>
        /// <param name="ChangedId4">Specifies filter for CHANGED_ID4 column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_CHANGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataChange list</returns>
        List<OpDataChange> GetDataChangeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataChange = null, string TableName = null, long[] ChangedId = null, long[] ChangedId2 = null, long[] ChangedId3 = null, long[] ChangedId4 = null,
                           int[] IndexNbr = null, int[] ChangeType = null, TypeDateTimeCode Time = null, string User = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataFormatGroupFilter

        /// <summary>
        /// Gets DataFormatGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataFormatGroup funcion</param>
        /// <param name="IdDataFormatGroup">Specifies filter for ID_DATA_FORMAT_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_FORMAT_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataFormatGroup list</returns>
        List<OpDataFormatGroup> GetDataFormatGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataFormatGroup = null, string Name = null, int[] IdModule = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataFormatGroupDetailsFilter

        /// <summary>
        /// Gets DataFormatGroupDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataFormatGroupDetails funcion</param>
        /// <param name="IdDataFormatGroup">Specifies filter for ID_DATA_FORMAT_GROUP column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdDataTypeFormatIn">Specifies filter for ID_DATA_TYPE_FORMAT_IN column</param>
        /// <param name="IdDataTypeFormatOut">Specifies filter for ID_DATA_TYPE_FORMAT_OUT column</param>
        /// <param name="IdUnitIn">Specifies filter for ID_UNIT_IN column</param>
        /// <param name="IdUnitOut">Specifies filter for ID_UNIT_OUT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_FORMAT_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataFormatGroupDetails list</returns>
        List<OpDataFormatGroupDetails> GetDataFormatGroupDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataFormatGroup = null, long[] IdDataType = null, int[] IdDataTypeFormatIn = null, int[] IdDataTypeFormatOut = null, int[] IdUnitIn = null, int[] IdUnitOut = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataTypeFilter

        /// <summary>
        /// Gets DataType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataType funcion</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdDataTypeFormat">Specifies filter for ID_DATA_TYPE_FORMAT column</param>
        /// <param name="IsArchiveOnly">Specifies filter for IS_ARCHIVE_ONLY column</param>
        /// <param name="IsRemoteRead">Specifies filter for IS_REMOTE_READ column</param>
        /// <param name="IsRemoteWrite">Specifies filter for IS_REMOTE_WRITE column</param>
        /// <param name="IsEditable">Specifies filter for IS_EDITABLE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataType list</returns>
        List<OpDataType> GetDataTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataType = null, string Name = null, long[] IdDescr = null, int[] IdDataTypeClass = null, int[] IdReferenceType = null, int[] IdDataTypeFormat = null,
                           bool? IsArchiveOnly = null, bool? IsRemoteRead = null, bool? IsRemoteWrite = null, bool? IsEditable = null, int[] IdUnit = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataTypeClassFilter

        /// <summary>
        /// Gets DataTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeClass funcion</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeClass list</returns>
        List<OpDataTypeClass> GetDataTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataTypeFormatFilter

        /// <summary>
        /// Gets DataTypeFormat list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeFormat funcion</param>
        /// <param name="IdDataTypeFormat">Specifies filter for ID_DATA_TYPE_FORMAT column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="TextMinLength">Specifies filter for TEXT_MIN_LENGTH column</param>
        /// <param name="TextMaxLength">Specifies filter for TEXT_MAX_LENGTH column</param>
        /// <param name="NumberMinPrecision">Specifies filter for NUMBER_MIN_PRECISION column</param>
        /// <param name="NumberMaxPrecision">Specifies filter for NUMBER_MAX_PRECISION column</param>
        /// <param name="NumberMinScale">Specifies filter for NUMBER_MIN_SCALE column</param>
        /// <param name="NumberMaxScale">Specifies filter for NUMBER_MAX_SCALE column</param>
        /// <param name="NumberMinValue">Specifies filter for NUMBER_MIN_VALUE column</param>
        /// <param name="NumberMaxValue">Specifies filter for NUMBER_MAX_VALUE column</param>
        /// <param name="DatetimeFormat">Specifies filter for DATETIME_FORMAT column</param>
        /// <param name="RegularExpression">Specifies filter for REGULAR_EXPRESSION column</param>
        /// <param name="IsRequired">Specifies filter for IS_REQUIRED column</param>
        /// <param name="IdUniqueType">Specifies filter for ID_UNIQUE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_FORMAT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeFormat list</returns>
        List<OpDataTypeFormat> GetDataTypeFormatFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataTypeFormat = null, long[] IdDescr = null, int[] TextMinLength = null, int[] TextMaxLength = null, int[] NumberMinPrecision = null, int[] NumberMaxPrecision = null,
                           int[] NumberMinScale = null, int[] NumberMaxScale = null, Double[] NumberMinValue = null, Double[] NumberMaxValue = null, string DatetimeFormat = null,
                           string RegularExpression = null, bool? IsRequired = null, int[] IdUniqueType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataTypeGroupFilter

        /// <summary>
        /// Gets DataTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeGroup funcion</param>
        /// <param name="IdDataTypeGroup">Specifies filter for ID_DATA_TYPE_GROUP column</param>
        /// <param name="IdDataTypeGroupType">Specifies filter for ID_DATA_TYPE_GROUP_TYPE column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdParentGroup">Specifies filter for ID_PARENT_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeGroup list</returns>
        List<OpDataTypeGroup> GetDataTypeGroupFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdDataTypeGroup = null, int[] IdDataTypeGroupType = null, int[] IdReferenceType = null, string Name = null, int[] IdParentGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataTypeGroupTypeFilter

        /// <summary>
        /// Gets DataTypeGroupType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeGroupType funcion</param>
        /// <param name="IdDataTypeGroupType">Specifies filter for ID_DATA_TYPE_GROUP_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_GROUP_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeGroupType list</returns>
        List<OpDataTypeGroupType> GetDataTypeGroupTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataTypeGroupType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDataTransferFilter

        /// <summary>
        /// Gets DataTransfer list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTransfer funcion</param>
        /// <param name="IdDataTransfer">Specifies filter for ID_DATA_TRANSFER column</param>
        /// <param name="BatchId">Specifies filter for BATCH_ID column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="ChangeDirection">Specifies filter for CHANGE_DIRECTION column</param>
        /// <param name="IdSourceServer">Specifies filter for ID_SOURCE_SERVER column</param>
        /// <param name="IdDestinationServer">Specifies filter for ID_DESTINATION_SERVER column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="Key1">Specifies filter for KEY1 column</param>
        /// <param name="Key2">Specifies filter for KEY2 column</param>
        /// <param name="Key3">Specifies filter for KEY3 column</param>
        /// <param name="Key4">Specifies filter for KEY4 column</param>
        /// <param name="Key5">Specifies filter for KEY5 column</param>
        /// <param name="Key6">Specifies filter for KEY6 column</param>
        /// <param name="ColumnName">Specifies filter for COLUMN_NAME column</param>
        /// <param name="InsertTime">Specifies filter for INSERT_TIME column</param>
        /// <param name="ProceedTime">Specifies filter for PROCEED_TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="Host">Specifies filter for HOST column</param>
        /// <param name="Application">Specifies filter for APPLICATION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TRANSFER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTransfer list</returns>
        List<OpDataTransfer> GetDataTransferFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataTransfer = null, long[] BatchId = null, int[] ChangeType = null, int[] ChangeDirection = null, int[] IdSourceServer = null, int[] IdDestinationServer = null,
                           string TableName = null, long[] Key1 = null, long[] Key2 = null, long[] Key3 = null, long[] Key4 = null,
                           long[] Key5 = null, long[] Key6 = null, string ColumnName = null, TypeDateTimeCode InsertTime = null, TypeDateTimeCode ProceedTime = null,
                           string User = null, string Host = null, string Application = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetDaylightSavingTimeFilter

        /// <summary>
        /// Gets DaylightSavingTime list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDaylightSavingTime funcion</param>
        /// <param name="IdDaylightSaving">Specifies filter for ID_DAYLIGHT_SAVING column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DAYLIGHT_SAVING DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DaylightSavingTime list</returns>
        List<OpDaylightSavingTime> GetDaylightSavingTimeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDaylightSaving = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetDeliveryFilter

        /// <summary>
        /// Gets Delivery list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDelivery funcion</param>
        /// <param name="IdDelivery">Specifies filter for ID_DELIVERY column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="UploadedFile">Specifies filter for UPLOADED_FILE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Delivery list</returns>
        List<OpDelivery> GetDeliveryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDelivery = null, string Name = null, string Description = null, string UploadedFile = null, TypeDateTimeCode CreationDate = null, int[] IdShippingList = null,
                           int[] IdOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDepositoryElementFilter

        /// <summary>
        /// Gets DepositoryElement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDepositoryElement funcion</param>
        /// <param name="IdDepositoryElement">Specifies filter for ID_DEPOSITORY_ELEMENT column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="IdDeviceTypeClass">Specifies filter for ID_DEVICE_TYPE_CLASS column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Removed">Specifies filter for REMOVED column</param>
        /// <param name="Ordered">Specifies filter for ORDERED column</param>
        /// <param name="Accepted">Specifies filter for ACCEPTED column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="ExternalSn">Specifies filter for EXTERNAL_SN column</param>
        /// <param name="IdReference">Specifies filter for ID_REFERENCE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEPOSITORY_ELEMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DepositoryElement list</returns>
        List<OpDepositoryElement> GetDepositoryElementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDepositoryElement = null, long[] IdLocation = null, int[] IdTask = null, int[] IdPackage = null, long[] IdArticle = null, int[] IdDeviceTypeClass = null,
                           int[] IdDeviceStateType = null, long[] SerialNbr = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, bool? Removed = null,
                           bool? Ordered = null, bool? Accepted = null, string Notes = null, string ExternalSn = null, int[] IdReference = null,
                           long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                           bool loadDeviceDetails = false);
        #endregion
        #region GetDepositoryElementDataFilter

        /// <summary>
        /// Gets DepositoryElementData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDepositoryElementData funcion</param>
        /// <param name="IdDepositoryElementData">Specifies filter for ID_DEPOSITORY_ELEMENT_DATA column</param>
        /// <param name="IdDepositoryElement">Specifies filter for ID_DEPOSITORY_ELEMENT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEPOSITORY_ELEMENT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DepositoryElementData list</returns>
        List<OpDepositoryElementData> GetDepositoryElementDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDepositoryElementData = null, int[] IdDepositoryElement = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDepotFilter

        /// <summary>
        /// Gets Depot list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDepot funcion</param>
        /// <param name="IdDepot">Specifies filter for ID_DEPOT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEPOT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Depot list</returns>
        List<OpDepot> GetDepotFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDepot = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceDetailsFilter

        /// <summary>
        /// Gets DeviceDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDetails funcion</param>
        /// <param name="IdDeviceDetails">Specifies filter for ID_DEVICE_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="FactoryNbr">Specifies filter for FACTORY_NBR column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="WarrantyDate">Specifies filter for WARRANTY_DATE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDetails list</returns>
        List<OpDeviceDetails> GetDeviceDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDetails = null, long[] SerialNbr = null, long[] IdLocation = null, string FactoryNbr = null, TypeDateTimeCode ShippingDate = null, TypeDateTimeCode WarrantyDate = null,
                           string Phone = null, int[] IdDeviceType = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, int[] IdDeviceStateType = null,
                           string ProductionDeviceOrderNumber = null, string DeviceBatchNumber = null, int[] IdDistributorOwner = null,
                           long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceDistributorHistoryFilter

        /// <summary>
        /// Gets DeviceDistributorHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDistributorHistory funcion</param>
        /// <param name="IdDeviceDistributorHistory">Specifies filter for ID_DEVICE_DISTRIBUTOR_HISTORY column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDistributorOwner">Specifies filter for ID_DISTRIBUTOR_OWNER column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DISTRIBUTOR_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDistributorHistory list</returns>
        List<OpDeviceDistributorHistory> GetDeviceDistributorHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDistributorHistory = null, long[] SerialNbr = null, int[] IdDistributor = null, int[] IdDistributorOwner = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                           string Notes = null, int[] IdOperator = null, int[] IdServiceReferenceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceDriverFilter

        /// <summary>
        /// Gets DeviceDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriver funcion</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="AutoUpdate">Specifies filter for AUTO_UPDATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDriver list</returns>
        List<OpDeviceDriver> GetDeviceDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceDriver = null, string Name = null, long[] IdDescr = null, string PluginName = null,
                           bool? AutoUpdate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceDriverDataFilter

        /// <summary>
        /// Gets DeviceDriverData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriverData funcion</param>
        /// <param name="IdDeviceDriverData">Specifies filter for ID_DEVICE_DRIVER_DATA column</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDriverData list</returns>
        List<OpDeviceDriverData> GetDeviceDriverDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDriverData = null, int[] IdDeviceDriver = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceHierarchyFilter

        /// <summary>
        /// Gets DeviceHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchy funcion</param>
        /// <param name="IdDeviceHierarchy">Specifies filter for ID_DEVICE_HIERARCHY column</param>
        /// <param name="SerialNbrParent">Specifies filter for SERIAL_NBR_PARENT column</param>
        /// <param name="IdSlotType">Specifies filter for ID_SLOT_TYPE column</param>
        /// <param name="SlotNbr">Specifies filter for SLOT_NBR column</param>
        /// <param name="IdProtocolIn">Specifies filter for ID_PROTOCOL_IN column</param>
        /// <param name="IdProtocolOut">Specifies filter for ID_PROTOCOL_OUT column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchy list</returns>
        List<OpDeviceHierarchy> GetDeviceHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchy = null, long[] SerialNbrParent = null, int[] IdSlotType = null, int[] SlotNbr = null, int[] IdProtocolIn = null, int[] IdProtocolOut = null,
                           long[] SerialNbr = null, bool? IsActive = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceHierarchyGroupFilter

        /// <summary>
        /// Gets DeviceHierarchyGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchyGroup funcion</param>
        /// <param name="IdDeviceHierarchyGroup">Specifies filter for ID_DEVICE_HIERARCHY_GROUP column</param>
        /// <param name="DeviceHierarchySerialNbr">Specifies filter for DEVICE_HIERARCHY_SERIAL_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDepositoryLocation">Specifies filter for ID_DEPOSITORY_LOCATION column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="FinishDate">Specifies filter for FINISH_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchyGroup list</returns>
        List<OpDeviceHierarchyGroup> GetDeviceHierarchyGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchyGroup = null, string DeviceHierarchySerialNbr = null, int[] IdDistributor = null, long[] IdDepositoryLocation = null, TypeDateTimeCode CreationDate = null, TypeDateTimeCode FinishDate = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceHierarchyInGroupFilter

        /// <summary>
        /// Gets DeviceHierarchyInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchyInGroup funcion</param>
        /// <param name="IdDeviceHierarchyGroup">Specifies filter for ID_DEVICE_HIERARCHY_GROUP column</param>
        /// <param name="IdDeviceHierarchy">Specifies filter for ID_DEVICE_HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchyInGroup list</returns>
        List<OpDeviceHierarchyInGroup> GetDeviceHierarchyInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchyGroup = null, long[] IdDeviceHierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceHierarchyPatternFilter

        /// <summary>
        /// Gets DeviceHierarchyPattern list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchyPattern funcion</param>
        /// <param name="IdDeviceHierarchyPattern">Specifies filter for ID_DEVICE_HIERARCHY_PATTERN column</param>
        /// <param name="IdDeviceTypeParent">Specifies filter for ID_DEVICE_TYPE_PARENT column</param>
        /// <param name="IdSlotType">Specifies filter for ID_SLOT_TYPE column</param>
        /// <param name="SlotNbrMin">Specifies filter for SLOT_NBR_MIN column</param>
        /// <param name="SlotNbrMax">Specifies filter for SLOT_NBR_MAX column</param>
        /// <param name="IdProtocolIn">Specifies filter for ID_PROTOCOL_IN column</param>
        /// <param name="IdProtocolOut">Specifies filter for ID_PROTOCOL_OUT column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceTypeGroup">Specifies filter for ID_DEVICE_TYPE_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY_PATTERN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchyPattern list</returns>
        List<OpDeviceHierarchyPattern> GetDeviceHierarchyPatternFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchyPattern = null, int[] IdDeviceTypeParent = null, int[] IdSlotType = null, int[] SlotNbrMin = null, int[] SlotNbrMax = null, int[] IdProtocolIn = null,
                           int[] IdProtocolOut = null, int[] IdDeviceType = null, int[] IdDeviceTypeGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceOrderNumberFilter

        /// <summary>
        /// Gets DeviceOrderNumber list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceOrderNumber funcion</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="FirstQuarter">Specifies filter for FIRST_QUARTER column</param>
        /// <param name="SecondQuarter">Specifies filter for SECOND_QUARTER column</param>
        /// <param name="ThirdQuarter">Specifies filter for THIRD_QUARTER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceOrderNumber list</returns>
        List<OpDeviceOrderNumber> GetDeviceOrderNumberFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceOrderNumber = null, string Name = null, string FirstQuarter = null, string SecondQuarter = null, string ThirdQuarter = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool loadCustomData = true);
        #endregion
        #region GetDeviceOrderNumberDataFilter

        /// <summary>
        /// Gets DeviceOrderNumberData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceOrderNumberData funcion</param>
        /// <param name="IdDeviceOrderNumberData">Specifies filter for ID_DEVICE_ORDER_NUMBER_DATA column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceOrderNumberData list</returns>
        List<OpDeviceOrderNumberData> GetDeviceOrderNumberDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceOrderNumberData = null, int[] IdDeviceOrderNumber = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceOrderNumberInArticleFilter

        /// <summary>
        /// Gets DeviceOrderNumberInArticle list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceOrderNumberInArticle funcion</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceOrderNumberInArticle list</returns>
        List<OpDeviceOrderNumberInArticle> GetDeviceOrderNumberInArticleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceOrderNumber = null, long[] IdArticle = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDevicePatternDataFilter

        /// <summary>
        /// Gets DevicePatternData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDevicePatternData funcion</param>
        /// <param name="IdPattern">Specifies filter for ID_PATTERN column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PATTERN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DevicePatternData list</returns>
        List<OpDevicePatternData> GetDevicePatternDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPattern = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceSimCardHistoryFilter

        /// <summary>
        /// Gets DeviceSimCardHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceSimCardHistory funcion</param>
        /// <param name="IdDeviceSimCardHistory">Specifies filter for ID_DEVICE_SIM_CARD_HISTORY column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdSimCard">Specifies filter for ID_SIM_CARD column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_SIM_CARD_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceSimCardHistory list</returns>
        List<OpDeviceSimCardHistory> GetDeviceSimCardHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceSimCardHistory = null, long[] SerialNbr = null, int[] IdSimCard = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTemplateFilter

        /// <summary>
        /// Gets DeviceTemplate list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTemplate funcion</param>
        /// <param name="IdTemplate">Specifies filter for ID_TEMPLATE column</param>
        /// <param name="IdTemplatePattern">Specifies filter for ID_TEMPLATE_PATTERN column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="FirmwareVersion">Specifies filter for FIRMWARE_VERSION column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CreatedBy">Specifies filter for CREATED_BY column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ConfirmedBy">Specifies filter for CONFIRMED_BY column</param>
        /// <param name="ConfirmDate">Specifies filter for CONFIRM_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TEMPLATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTemplate list</returns>
        List<OpDeviceTemplate> GetDeviceTemplateFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTemplate = null, int[] IdTemplatePattern = null, int[] IdDeviceType = null, string FirmwareVersion = null, string Name = null, int[] CreatedBy = null,
                           TypeDateTimeCode CreationDate = null, int[] ConfirmedBy = null, TypeDateTimeCode ConfirmDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTemplateDataFilter

        /// <summary>
        /// Gets DeviceTemplateData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTemplateData funcion</param>
        /// <param name="IdTemplate">Specifies filter for ID_TEMPLATE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TEMPLATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTemplateData list</returns>
        List<OpDeviceTemplateData> GetDeviceTemplateDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTemplate = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceStateTypeFilter

        /// <summary>
        /// Gets DeviceStateType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceStateType funcion</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_STATE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceStateType list</returns>
        List<OpDeviceStateType> GetDeviceStateTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceStateType = null, string Name = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeFilter

        /// <summary>
        /// Gets DeviceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceType funcion</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDeviceTypeClass">Specifies filter for ID_DEVICE_TYPE_CLASS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="TwoWayTransAvailable">Specifies filter for TWO_WAY_TRANS_AVAILABLE column</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="IdProtocol">Specifies filter for ID_PROTOCOL column</param>
        /// <param name="DefaultIdDeviceOderNumber">Specifies filter for DEFAULT_ID_DEVICE_ODER_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceType list</returns>
        List<OpDeviceType> GetDeviceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceType = null, string Name = null, int[] IdDeviceTypeClass = null, long[] IdDescr = null, bool? TwoWayTransAvailable = null, int[] IdDeviceDriver = null,
                           int[] IdProtocol = null, int[] DefaultIdDeviceOderNumber = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeClassFilter

        /// <summary>
        /// Gets DeviceTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeClass funcion</param>
        /// <param name="IdDeviceTypeClass">Specifies filter for ID_DEVICE_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeClass list</returns>
        List<OpDeviceTypeClass> GetDeviceTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeDataFilter

        /// <summary>
        /// Gets DeviceTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeData funcion</param>
        /// <param name="IdDeviceTypeData">Specifies filter for ID_DEVICE_TYPE_DATA column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeData list</returns>
        List<OpDeviceTypeData> GetDeviceTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceTypeData = null, int[] IdDeviceType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeGroupFilter

        /// <summary>
        /// Gets DeviceTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeGroup funcion</param>
        /// <param name="IdDeviceTypeGroup">Specifies filter for ID_DEVICE_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeGroup list</returns>
        List<OpDeviceTypeGroup> GetDeviceTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeGroup = null, string Name = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeProfileMapFilter

        /// <summary>
        /// Gets DeviceTypeProfileMap list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileMap funcion</param>
        /// <param name="IdDeviceTypeProfileMap">Specifies filter for ID_DEVICE_TYPE_PROFILE_MAP column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdOperatorCreated">Specifies filter for ID_OPERATOR_CREATED column</param>
        /// <param name="CreatedTime">Specifies filter for CREATED_TIME column</param>
        /// <param name="IdOperatorModified">Specifies filter for ID_OPERATOR_MODIFIED column</param>
        /// <param name="ModifiedTime">Specifies filter for MODIFIED_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileMap list</returns>
        List<OpDeviceTypeProfileMap> GetDeviceTypeProfileMapFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileMap = null, int[] IdDeviceType = null, int[] IdOperatorCreated = null, TypeDateTimeCode CreatedTime = null, int[] IdOperatorModified = null, TypeDateTimeCode ModifiedTime = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeProfileMapConfigFilter

        /// <summary>
        /// Gets DeviceTypeProfileMapConfig list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileMapConfig funcion</param>
        /// <param name="IdDeviceTypeProfileMap">Specifies filter for ID_DEVICE_TYPE_PROFILE_MAP column</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="IdDeviceTypeProfileStepKind">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP_KIND column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileMapConfig list</returns>
        List<OpDeviceTypeProfileMapConfig> GetDeviceTypeProfileMapConfigFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileMap = null, int[] IdDeviceTypeProfileStep = null, int[] IdDeviceTypeProfileStepKind = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeProfileStepFilter

        /// <summary>
        /// Gets DeviceTypeProfileStep list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileStep funcion</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_STEP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileStep list</returns>
        List<OpDeviceTypeProfileStep> GetDeviceTypeProfileStepFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileStep = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceTypeProfileStepKindFilter

        /// <summary>
        /// Gets DeviceTypeProfileStepKind list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileStepKind funcion</param>
        /// <param name="IdDeviceTypeProfileStepKind">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP_KIND column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_STEP_KIND DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileStepKind list</returns>
        List<OpDeviceTypeProfileStepKind> GetDeviceTypeProfileStepKindFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileStepKind = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceWarrantyFilter

        /// <summary>
        /// Gets DeviceWarranty list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceWarranty funcion</param>
        /// <param name="IdDeviceWarranty">Specifies filter for ID_DEVICE_WARRANTY column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdComponent">Specifies filter for ID_COMPONENT column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="WarrantyLength">Specifies filter for WARRANTY_LENGTH column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="InstallationDate">Specifies filter for INSTALLATION_DATE column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_WARRANTY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceWarranty list</returns>
        List<OpDeviceWarranty> GetDeviceWarrantyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceWarranty = null, int[] IdShippingList = null, long[] SerialNbr = null, int[] IdComponent = null, int[] IdContract = null, int[] WarrantyLength = null,
                           TypeDateTimeCode ShippingDate = null, TypeDateTimeCode InstallationDate = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDiagnosticActionFilter

        /// <summary>
        /// Gets DiagnosticAction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticAction funcion</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DIAGNOSTIC_ACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticAction list</returns>
        List<OpDiagnosticAction> GetDiagnosticActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDiagnosticAction = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, string Name = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                           long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDiagnosticActionDataFilter

        /// <summary>
        /// Gets DiagnosticActionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticActionData funcion</param>
        /// <param name="IdDiagnosticActionData">Specifies filter for ID_DIAGNOSTIC_ACTION_DATA column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DIAGNOSTIC_ACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticActionData list</returns>
        List<OpDiagnosticActionData> GetDiagnosticActionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDiagnosticActionData = null, int[] IdDiagnosticAction = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDiagnosticActionInServiceFilter

        /// <summary>
        /// Gets DiagnosticActionInService list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticActionInService funcion</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticActionInService list</returns>
        List<OpDiagnosticActionInService> GetDiagnosticActionInServiceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdService = null, int[] IdDiagnosticAction = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDiagnosticActionResultFilter

        /// <summary>
        /// Gets DiagnosticActionResult list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticActionResult funcion</param>
        /// <param name="IdDiagnosticActionResult">Specifies filter for ID_DIAGNOSTIC_ACTION_RESULT column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="AllowInputText">Specifies filter for ALLOW_INPUT_TEXT column</param>
        /// <param name="DamageLevel">Specifies filter for DAMAGE_LEVEL column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DIAGNOSTIC_ACTION_RESULT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticActionResult list</returns>
        List<OpDiagnosticActionResult> GetDiagnosticActionResultFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDiagnosticActionResult = null, int[] IdDiagnosticAction = null, string Name = null, bool? AllowInputText = null, int[] DamageLevel = null, long[] IdDescr = null,
                           int[] IdServiceReferenceType = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDistributorFilter

        /// <summary>
        /// Gets Distributor list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributor funcion</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="City">Specifies filter for CITY column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Distributor list</returns>
        List<OpDistributor> GetDistributorFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdDistributor = null, string Name = null, string City = null, string Address = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDistributorDataFilter

        /// <summary>
        /// Gets DistributorData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorData funcion</param>
        /// <param name="IdDistributorData">Specifies filter for ID_DISTRIBUTOR_DATA column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorData list</returns>
        List<OpDistributorData> GetDistributorDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDistributorData = null, int[] IdDistributor = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDistributorHierarchyFilter

        /// <summary>
        /// Gets DistributorHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorHierarchy funcion</param>
        /// <param name="IdDistributorHierarchy">Specifies filter for ID_DISTRIBUTOR_HIERARCHY column</param>
        /// <param name="IdDistributorHierarchyParent">Specifies filter for ID_DISTRIBUTOR_HIERARCHY_PARENT column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorHierarchy list</returns>
        List<OpDistributorHierarchy> GetDistributorHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDistributorHierarchy = null, int[] IdDistributorHierarchyParent = null, int[] IdDistributor = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDistributorSupplierFilter

        /// <summary>
        /// Gets DistributorSupplier list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorSupplier funcion</param>
        /// <param name="IdDistributorSupplier">Specifies filter for ID_DISTRIBUTOR_SUPPLIER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdSupplier">Specifies filter for ID_SUPPLIER column</param>
        /// <param name="IdProductCode">Specifies filter for ID_PRODUCT_CODE column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_SUPPLIER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorSupplier list</returns>
        List<OpDistributorSupplier> GetDistributorSupplierFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDistributorSupplier = null, int[] IdDistributor = null, int[] IdSupplier = null, int[] IdProductCode = null, int[] IdActor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetErrorLogFilter

        /// <summary>
        /// Gets ErrorLog list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllErrorLog funcion</param>
        /// <param name="IdErrorLog">Specifies filter for ID_ERROR_LOG column</param>
        /// <param name="ApplicationId">Specifies filter for APPLICATION_ID column</param>
        /// <param name="ErrorDate">Specifies filter for ERROR_DATE column</param>
        /// <param name="ClassName">Specifies filter for CLASS_NAME column</param>
        /// <param name="FunctionName">Specifies filter for FUNCTION_NAME column</param>
        /// <param name="Source">Specifies filter for SOURCE column</param>
        /// <param name="Message">Specifies filter for MESSAGE column</param>
        /// <param name="StackTrace">Specifies filter for STACK_TRACE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="Severity">Specifies filter for SEVERITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ERROR_LOG DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ErrorLog list</returns>
        List<OpErrorLog> GetErrorLogFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdErrorLog = null, int[] ApplicationId = null, TypeDateTimeCode ErrorDate = null, string ClassName = null, string FunctionName = null, string Source = null,
                           string Message = null, string StackTrace = null, string Description = null, int[] Severity = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetEtlFilter

        /// <summary>
        /// Gets Etl list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllEtl funcion</param>
        /// <param name="IdParam">Specifies filter for ID_PARAM column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PARAM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Etl list</returns>
        List<OpEtl> GetEtlFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdParam = null, string Code = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetFaultCodeFilter

        /// <summary>
        /// Gets FaultCode list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCode funcion</param>
        /// <param name="IdFaultCode">Specifies filter for ID_FAULT_CODE column</param>
        /// <param name="IdFaultCodeType">Specifies filter for ID_FAULT_CODE_TYPE column</param>
        /// <param name="IdFaultCodeParent">Specifies filter for ID_FAULT_CODE_PARENT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdClientDescr">Specifies filter for ID_CLIENT_DESCR column</param>
        /// <param name="IdCompanyDescr">Specifies filter for ID_COMPANY_DESCR column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="FaultCodeHierarchy">Specifies filter for FAULT_CODE_HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCode list</returns>
        List<OpFaultCode> GetFaultCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdFaultCode = null, int[] IdFaultCodeType = null, int[] IdFaultCodeParent = null, string Name = null, long[] IdClientDescr = null, long[] IdCompanyDescr = null,
                           int[] Priority = null, string FaultCodeHierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetFaultCodeDataFilter

        /// <summary>
        /// Gets FaultCodeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCodeData funcion</param>
        /// <param name="IdFaultCodeData">Specifies filter for ID_FAULT_CODE_DATA column</param>
        /// <param name="IdFaultCode">Specifies filter for ID_FAULT_CODE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCodeData list</returns>
        List<OpFaultCodeData> GetFaultCodeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdFaultCodeData = null, int[] IdFaultCode = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetFaultCodeSummaryFilter

        /// <summary>
        /// Gets FaultCodeSummary list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCodeSummary funcion</param>
        /// <param name="IdFaultCodeSummary">Specifies filter for ID_FAULT_CODE_SUMMARY column</param>
        /// <param name="IdFaultCode">Specifies filter for ID_FAULT_CODE column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="ReferenceValue">Specifies filter for REFERENCE_VALUE column</param>
        /// <param name="FaultsCount">Specifies filter for FAULTS_COUNT column</param>
        /// <param name="TotalCount">Specifies filter for TOTAL_COUNT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE_SUMMARY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCodeSummary list</returns>
        List<OpFaultCodeSummary> GetFaultCodeSummaryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdFaultCodeSummary = null, int[] IdFaultCode = null, int[] IdServiceReferenceType = null, int[] FaultsCount = null, int[] TotalCount = null, int[] IdFaultCodeSummaryType = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetFaultCodeTypeFilter

        /// <summary>
        /// Gets FaultCodeType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCodeType funcion</param>
        /// <param name="IdFaultCodeType">Specifies filter for ID_FAULT_CODE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCodeType list</returns>
        List<OpFaultCodeType> GetFaultCodeTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdFaultCodeType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetHolidayDateFilter

        /// <summary>
        /// Gets HolidayDate list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllHolidayDate funcion</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="CountryCode">Specifies filter for COUNTRY_CODE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY DATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>HolidayDate list</returns>
        List<OpHolidayDate> GetHolidayDateFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, TypeDateTimeCode Date = null, string CountryCode = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetImrServerFilter

        /// <summary>
        /// Gets ImrServer list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllImrServer funcion</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="IpAddress">Specifies filter for IP_ADDRESS column</param>
        /// <param name="DnsName">Specifies filter for DNS_NAME column</param>
        /// <param name="WebserviceAddress">Specifies filter for WEBSERVICE_ADDRESS column</param>
        /// <param name="WebserviceTimeout">Specifies filter for WEBSERVICE_TIMEOUT column</param>
        /// <param name="Login">Specifies filter for LOGIN column</param>
        /// <param name="Password">Specifies filter for PASSWORD column</param>
        /// <param name="StandardDescription">Specifies filter for STANDARD_DESCRIPTION column</param>
        /// <param name="IsGood">Specifies filter for IS_GOOD column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="UtdcSsecUser">Specifies filter for UTDC_SSEC_USER column</param>
        /// <param name="UtdcSsecPass">Specifies filter for UTDC_SSEC_PASS column</param>
        /// <param name="Collation">Specifies filter for COLLATION column</param>
        /// <param name="DbName">Specifies filter for DB_NAME column</param>
        /// <param name="ImrServerVersion">Specifies filter for IMR_SERVER_VERSION column</param>
        /// <param name="IsImrlt">Specifies filter for IS_IMRLT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_IMR_SERVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ImrServer list</returns>
        List<OpImrServer> GetImrServerFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdImrServer = null, string IpAddress = null, string DnsName = null, string WebserviceAddress = null, int[] WebserviceTimeout = null, string Login = null,
                           string Password = null, string StandardDescription = null, bool? IsGood = null, TypeDateTimeCode CreationDate = null, string UtdcSsecUser = null,
                           string UtdcSsecPass = null, string Collation = null, string DbName = null, int[] ImrServerVersion = null, bool? IsImrlt = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetImrServerDataFilter

        /// <summary>
        /// Gets ImrServerData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllImrServerData funcion</param>
        /// <param name="IdImrServerData">Specifies filter for ID_IMR_SERVER_DATA column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_IMR_SERVER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ImrServerData list</returns>
        List<OpImrServerData> GetImrServerDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdImrServerData = null, int[] IdImrServer = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetIssueFilter

        /// <summary>
        /// Gets Issue list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssue funcion</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdOperatorRegistering">Specifies filter for ID_OPERATOR_REGISTERING column</param>
        /// <param name="IdOperatorPerformer">Specifies filter for ID_OPERATOR_PERFORMER column</param>
        /// <param name="IdIssueType">Specifies filter for ID_ISSUE_TYPE column</param>
        /// <param name="IdIssueStatus">Specifies filter for ID_ISSUE_STATUS column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="RealizationDate">Specifies filter for REALIZATION_DATE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ShortDescr">Specifies filter for SHORT_DESCR column</param>
        /// <param name="LongDescr">Specifies filter for LONG_DESCR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Issue list</returns>
        List<OpIssue> GetIssueFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssue = null, int[] IdActor = null, int[] IdOperatorRegistering = null, int[] IdOperatorPerformer = null, int[] IdIssueType = null, int[] IdIssueStatus = null,
                           int[] IdDistributor = null, TypeDateTimeCode RealizationDate = null, TypeDateTimeCode CreationDate = null, string ShortDescr = null, string LongDescr = null,
                           long[] IdLocation = null, TypeDateTimeCode Deadline = null, int[] IdPriority = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool loadIssueHistory = false);
        #endregion
        #region GetIssueDataFilter

        /// <summary>
        /// Gets IssueData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueData funcion</param>
        /// <param name="IdIssueData">Specifies filter for ID_ISSUE_DATA column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueData list</returns>
        List<OpIssueData> GetIssueDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdIssueData = null, int[] IdIssue = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetIssueGroupFilter
        /// <summary>
        /// Gets IssueGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueGroup funcion</param>
        /// <param name="IdIssueGroup">Specifies filter for ID_ISSUE_GROUP column</param>
        /// <param name="IdParentGroup">Specifies filter for ID_PARENT_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="DateCreated">Specifies filter for DATE_CREATED column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueGroup list</returns>
        List<OpIssueGroup> GetIssueGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueGroup = null, int[] IdParentGroup = null, string Name = null, TypeDateTimeCode DateCreated = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetIssueHistoryFilter

        /// <summary>
        /// Gets IssueHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueHistory funcion</param>
        /// <param name="IdIssueHistory">Specifies filter for ID_ISSUE_HISTORY column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdIssueStatus">Specifies filter for ID_ISSUE_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="ShortDescr">Specifies filter for SHORT_DESCR column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueHistory list</returns>
        List<OpIssueHistory> GetIssueHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueHistory = null, int[] IdIssue = null, int[] IdIssueStatus = null, int[] IdOperator = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                           string Notes = null, string ShortDescr = null, TypeDateTimeCode Deadline = null, int[] IdPriority = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetIssueStatusFilter

        /// <summary>
        /// Gets IssueStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueStatus funcion</param>
        /// <param name="IdIssueStatus">Specifies filter for ID_ISSUE_STATUS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueStatus list</returns>
        List<OpIssueStatus> GetIssueStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueStatus = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetIssueTypeFilter

        /// <summary>
        /// Gets IssueType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueType funcion</param>
        /// <param name="IdIssueType">Specifies filter for ID_ISSUE_TYPE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueType list</returns>
        List<OpIssueType> GetIssueTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueType = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLanguageDataFilter

        /// <summary>
        /// Gets LanguageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLanguageData funcion</param>
        /// <param name="IdLanguageData">Specifies filter for ID_LANGUAGE_DATA column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LANGUAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LanguageData list</returns>
        List<OpLanguageData> GetLanguageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLanguageData = null, int[] IdLanguage = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetLanguageFilter

        /// <summary>
        /// Gets Language list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLanguage funcion</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CultureCode">Specifies filter for CULTURE_CODE column</param>
        /// <param name="CultureLangid">Specifies filter for CULTURE_LANGID column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LANGUAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Language list</returns>
        List<OpLanguage> GetLanguageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLanguage = null, string Name = null, string CultureCode = null, int[] CultureLangid = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetLocationConsumerHistoryFilter

        /// <summary>
        /// Gets LocationConsumerHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationConsumerHistory funcion</param>
        /// <param name="IdLocationConsumerHistory">Specifies filter for ID_LOCATION_CONSUMER_HISTORY column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_CONSUMER_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationConsumerHistory list</returns>
        List<OpLocationConsumerHistory> GetLocationConsumerHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationConsumerHistory = null, long[] IdLocation = null, int[] IdConsumer = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationDetailsFilter

        /// <summary>
        /// Gets LocationDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationDetails funcion</param>
        /// <param name="IdLocationDetails">Specifies filter for ID_LOCATION_DETAILS column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="Cid">Specifies filter for CID column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="City">Specifies filter for CITY column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Zip">Specifies filter for ZIP column</param>
        /// <param name="Country">Specifies filter for COUNTRY column</param>
        /// <param name="Latitude">Specifies filter for LATITUDE column</param>
        /// <param name="Longitude">Specifies filter for LONGITUDE column</param>
        /// <param name="ImrServer">Specifies filter for IMR_SERVER column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="LocationMemo">Specifies filter for LOCATION_MEMO column</param>
        /// <param name="LocationSourceServerId">Specifies filter for LOCATION_SOURCE_SERVER_ID column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdLocationType">Specifies filter for ID_LOCATION_TYPE column</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="InKpi">Specifies filter for IN_KPI column</param>
        /// <param name="AllowGrouping">Specifies filter for ALLOW_GROUPING column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>LocationDetails list</returns>
        List<OpLocationDetails> GetLocationDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationDetails = null, long[] IdLocation = null, string Cid = null, string Name = null, string City = null, string Address = null,
                           string Zip = null, string Country = null, string Latitude = null, string Longitude = null, int[] ImrServer = null,
                           TypeDateTimeCode CreationDate = null, string LocationMemo = null, long[] LocationSourceServerId = null, int[] IdDistributor = null, int[] IdLocationType = null,
                           int[] IdLocationStateType = null, bool? InKpi = null, bool? AllowGrouping = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool loadCustomData = true);
        #endregion
        #region GetLocationHierarchyFilter

        /// <summary>
        /// Gets LocationHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationHierarchy funcion</param>
        /// <param name="IdLocationHierarchy">Specifies filter for ID_LOCATION_HIERARCHY column</param>
        /// <param name="IdLocationHierarchyParent">Specifies filter for ID_LOCATION_HIERARCHY_PARENT column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationHierarchy list</returns>
        List<OpLocationHierarchy> GetLocationHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationHierarchy = null, long[] IdLocationHierarchyParent = null, long[] IdLocation = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationSealFilter

        /// <summary>
        /// Gets LocationSeal list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationSeal funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdSeal">Specifies filter for ID_SEAL column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationSeal list</returns>
        List<OpLocationSeal> GetLocationSealFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocation = null, long[] IdSeal = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationStateHistoryFilter

        /// <summary>
        /// Gets LocationStateHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationStateHistory funcion</param>
        /// <param name="IdLocationState">Specifies filter for ID_LOCATION_STATE column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="InKpi">Specifies filter for IN_KPI column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_STATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationStateHistory list</returns>
        List<OpLocationStateHistory> GetLocationStateHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationState = null, long[] IdLocation = null, int[] IdLocationStateType = null, bool? InKpi = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                           string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationStateTypeFilter

        /// <summary>
        /// Gets LocationStateType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationStateType funcion</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_STATE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationStateType list</returns>
        List<OpLocationStateType> GetLocationStateTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationStateType = null, string Name = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationSupplierFilter

        /// <summary>
        /// Gets LocationSupplier list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationSupplier funcion</param>
        /// <param name="IdLocationSupplier">Specifies filter for ID_LOCATION_SUPPLIER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDistributorSupplier">Specifies filter for ID_DISTRIBUTOR_SUPPLIER column</param>
        /// <param name="AccountNo">Specifies filter for ACCOUNT_NO column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_SUPPLIER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationSupplier list</returns>
        List<OpLocationSupplier> GetLocationSupplierFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationSupplier = null, long[] IdLocation = null, int[] IdDistributorSupplier = null, int[] AccountNo = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                           string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationTariffFilter

        /// <summary>
        /// Gets LocationTariff list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationTariff funcion</param>
        /// <param name="IdLocationTariff">Specifies filter for ID_LOCATION_TARIFF column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_TARIFF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationTariff list</returns>
        List<OpLocationTariff> GetLocationTariffFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationTariff = null, long[] IdLocation = null, int[] IdTariff = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationTypeFilter

        /// <summary>
        /// Gets LocationType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationType funcion</param>
        /// <param name="IdLocationType">Specifies filter for ID_LOCATION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationType list</returns>
        List<OpLocationType> GetLocationTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationEntranceFilter

        /// <summary>
        /// Gets LocationEntrance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationEntrance funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="EntranceIndexNbr">Specifies filter for ENTRANCE_INDEX_NBR column</param>
        /// <param name="SectionCount">Specifies filter for SECTION_COUNT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationEntrance list</returns>
        List<OpLocationEntrance> GetLocationEntranceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocation = null, int[] EntranceIndexNbr = null, int[] SectionCount = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetLocationEntranceFloorFilter

        /// <summary>
        /// Gets LocationEntranceFloor list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationEntranceFloor funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="EntranceIndexNbr">Specifies filter for ENTRANCE_INDEX_NBR column</param>
        /// <param name="FloorIndexNbr">Specifies filter for FLOOR_INDEX_NBR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationEntranceFloor list</returns>
        List<OpLocationEntranceFloor> GetLocationEntranceFloorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocation = null, int[] EntranceIndexNbr = null, int[] FloorIndexNbr = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMappingFilter

        /// <summary>
        /// Gets Mapping list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMapping funcion</param>
        /// <param name="IdMapping">Specifies filter for ID_MAPPING column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdSource">Specifies filter for ID_SOURCE column</param>
        /// <param name="IdDestination">Specifies filter for ID_DESTINATION column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="Timestamp">Specifies filter for TIMESTAMP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MAPPING DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Mapping list</returns>
        List<OpMapping> GetMappingFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMapping = null, int[] IdReferenceType = null, long[] IdSource = null, long[] IdDestination = null, int[] IdImrServer = null, TypeDateTimeCode Timestamp = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetMessageFilter

        /// <summary>
        /// Gets Message list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMessage funcion</param>
        /// <param name="IdMessage">Specifies filter for ID_MESSAGE column</param>
        /// <param name="IdMessageType">Specifies filter for ID_MESSAGE_TYPE column</param>
        /// <param name="IdMessageParent">Specifies filter for ID_MESSAGE_PARENT column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="MessageContent">Specifies filter for MESSAGE_CONTENT column</param>
        /// <param name="HasAttachment">Specifies filter for HAS_ATTACHMENT column</param>
        /// <param name="Points">Specifies filter for POINTS column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MESSAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Message list</returns>
        List<OpMessage> GetMessageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMessage = null, int[] IdMessageType = null, long[] IdMessageParent = null, int[] IdOperator = null, string MessageContent = null, bool? HasAttachment = null,
                           int[] Points = null, string Hierarchy = null, TypeDateTimeCode InsertDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMessageDataFilter

        /// <summary>
        /// Gets MessageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMessageData funcion</param>
        /// <param name="IdMessageData">Specifies filter for ID_MESSAGE_DATA column</param>
        /// <param name="IdMessage">Specifies filter for ID_MESSAGE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MESSAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MessageData list</returns>
        List<OpMessageData> GetMessageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMessageData = null, long[] IdMessage = null, int[] IndexNbr = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMeterFilter

        /// <summary>
        /// Gets Meter list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeter funcion</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdMeterPattern">Specifies filter for ID_METER_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Meter list</returns>
        List<OpMeter> GetMeterFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMeter = null, int[] IdMeterType = null, long[] IdMeterPattern = null, long[] IdDescrPattern = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMeterTypeFilter

        /// <summary>
        /// Gets MeterType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterType funcion</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdMeterTypeClass">Specifies filter for ID_METER_TYPE_CLASS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterType list</returns>
        List<OpMeterType> GetMeterTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterType = null, string Name = null, long[] IdDescr = null, int[] IdMeterTypeClass = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMeterTypeClassFilter

        /// <summary>
        /// Gets MeterTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeClass funcion</param>
        /// <param name="IdMeterTypeClass">Specifies filter for ID_METER_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeClass list</returns>
        List<OpMeterTypeClass> GetMeterTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMeterTypeDataFilter

        /// <summary>
        /// Gets MeterTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeData funcion</param>
        /// <param name="IdMeterTypeData">Specifies filter for ID_METER_TYPE_DATA column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeData list</returns>
        List<OpMeterTypeData> GetMeterTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMeterTypeData = null, int[] IdMeterType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMeterTypeGroupFilter
        /// <summary>
        /// Gets MeterTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeGroup funcion</param>
        /// <param name="IdMeterTypeGroup">Specifies filter for ID_METER_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeGroup list</returns>
        List<OpMeterTypeGroup> GetMeterTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterTypeGroup = null, string Name = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMeterTypeInGroupFilter
        /// <summary>
        /// Gets MeterTypeInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeInGroup funcion</param>
        /// <param name="IdMeterTypeGroup">Specifies filter for ID_METER_TYPE_GROUP column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeInGroup list</returns>
        List<OpMeterTypeInGroup> GetMeterTypeInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterTypeGroup = null, int[] IdMeterType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMeterTypeDeviceTypeFilter

        /// <summary>
        /// Gets MeterTypeDeviceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeDeviceType funcion</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeDeviceType list</returns>
        List<OpMeterTypeDeviceType> GetMeterTypeDeviceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterType = null, int[] IdDeviceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMobileNetworkCodeFilter

        /// <summary>
        /// Gets MobileNetworkCode list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMobileNetworkCode funcion</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="Mmc">Specifies filter for MMC column</param>
        /// <param name="Mnc">Specifies filter for MNC column</param>
        /// <param name="Brand">Specifies filter for BRAND column</param>
        /// <param name="Operator">Specifies filter for OPERATOR column</param>
        /// <param name="Bands">Specifies filter for BANDS column</param>
        /// <param name="SmsServiceCenter">Specifies filter for SMS_SERVICE_CENTER column</param>
        /// <param name="ApnAddress">Specifies filter for APN_ADDRESS column</param>
        /// <param name="ApnLogin">Specifies filter for APN_LOGIN column</param>
        /// <param name="ApnPassword">Specifies filter for APN_PASSWORD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MobileNetworkCode list</returns>
        List<OpMobileNetworkCode> GetMobileNetworkCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] Code = null, string Mmc = null, string Mnc = null, string Brand = null, string Operator = null, string Bands = null,
                           string SmsServiceCenter = null, string ApnAddress = null, string ApnLogin = null, string ApnPassword = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetModuleFilter

        /// <summary>
        /// Gets Module list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllModule funcion</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MODULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Module list</returns>
        List<OpModule> GetModuleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdModule = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetModuleDataFilter

        /// <summary>
        /// Gets ModuleData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllModuleData funcion</param>
        /// <param name="IdModuleData">Specifies filter for ID_MODULE_DATA column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MODULE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ModuleData list</returns>
        List<OpModuleData> GetModuleDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdModuleData = null, int[] IdModule = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetMonitoringWatcherFilter

        /// <summary>
        /// Gets MonitoringWatcher list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcher funcion</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcher list</returns>
        List<OpMonitoringWatcher> GetMonitoringWatcherFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcher = null, int[] IdMonitoringWatcherType = null, bool? IsActive = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion        
        #region GetMonitoringWatcherDataFilter

        /// <summary>
        /// Gets MonitoringWatcherData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherData funcion</param>
        /// <param name="IdMonitoringWatcherData">Specifies filter for ID_MONITORING_WATCHER_DATA column</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Index">Specifies filter for INDEX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherData list</returns>
        List<OpMonitoringWatcherData> GetMonitoringWatcherDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherData = null, int[] IdMonitoringWatcher = null, long[] IdDataType = null, int[] Index = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetMonitoringWatcherTypeFilter

        /// <summary>
        /// Gets MonitoringWatcherType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherType funcion</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Plugin">Specifies filter for PLUGIN column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherType list</returns>
        List<OpMonitoringWatcherType> GetMonitoringWatcherTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherType = null, string Name = null, string Plugin = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetNotificationDeliveryTypeFilter

        /// <summary>
        /// Gets NotificationDeliveryType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllNotificationDeliveryType funcion</param>
        /// <param name="IdNotificationDeliveryType">Specifies filter for ID_NOTIFICATION_DELIVERY_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_NOTIFICATION_DELIVERY_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>NotificationDeliveryType list</returns>
        List<OpNotificationDeliveryType> GetNotificationDeliveryTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdNotificationDeliveryType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetNotificationTypeFilter

        /// <summary>
        /// Gets NotificationType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllNotificationType funcion</param>
        /// <param name="IdNotificationType">Specifies filter for ID_NOTIFICATION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_NOTIFICATION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>NotificationType list</returns>
        List<OpNotificationType> GetNotificationTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdNotificationType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetOperatorActivityFilter

        /// <summary>
        /// Gets OperatorActivity list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperatorActivity funcion</param>
        /// <param name="IdOperatorActivity">Specifies filter for ID_OPERATOR_ACTIVITY column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdActivity">Specifies filter for ID_ACTIVITY column</param>
        /// <param name="Deny">Specifies filter for DENY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR_ACTIVITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OperatorActivity list</returns>
        List<OpOperatorActivity> GetOperatorActivityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdOperatorActivity = null, int[] IdOperator = null, int[] IdActivity = null, int[] IdModule = null, bool? Deny = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetOperatorDataFilter

        /// <summary>
        /// Gets OperatorData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperatorData funcion</param>
        /// <param name="IdOperatorData">Specifies filter for ID_OPERATOR_DATA column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OperatorData list</returns>
        List<OpOperatorData> GetOperatorDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdOperatorData = null, int[] IdOperator = null, long[] IdDataType = null, int[] IndexNbr = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetOperatorRoleFilter

        /// <summary>
        /// Gets OperatorRole list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperatorRole funcion</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OperatorRole list</returns>
        List<OpOperatorRole> GetOperatorRoleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOperator = null, int[] IdRole = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetOrderFilter

        /// <summary>
        /// Gets Order list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOrder funcion</param>
        /// <param name="IdOrder">Specifies filter for ID_ORDER column</param>
        /// <param name="IdOrderStateType">Specifies filter for ID_ORDER_STATE_TYPE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ORDER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Order list</returns>
        List<OpOrder> GetOrderFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOrder = null, int[] IdOrderStateType = null, int[] IdOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetOrderDataFilter

        /// <summary>
        /// Gets OrderData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOrderData funcion</param>
        /// <param name="IdOrderData">Specifies filter for ID_ORDER_DATA column</param>
        /// <param name="IdOrder">Specifies filter for ID_ORDER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ORDER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OrderData list</returns>
        List<OpOrderData> GetOrderDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdOrderData = null, int[] IdOrder = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetOrderStateTypeFilter

        /// <summary>
        /// Gets OrderStateType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOrderStateType funcion</param>
        /// <param name="IdOrderStateType">Specifies filter for ID_ORDER_STATE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ORDER_STATE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OrderStateType list</returns>
        List<OpOrderStateType> GetOrderStateTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOrderStateType = null, string Name = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPackageFilter

        /// <summary>
        /// Gets Package list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackage funcion</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="SendDate">Specifies filter for SEND_DATE column</param>
        /// <param name="IdOperatorCreator">Specifies filter for ID_OPERATOR_CREATOR column</param>
        /// <param name="IdOperatorReceiver">Specifies filter for ID_OPERATOR_RECEIVER column</param>
        /// <param name="Received">Specifies filter for RECEIVED column</param>
        /// <param name="ReceiveDate">Specifies filter for RECEIVE_DATE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdPackageStatus">Specifies filter for ID_PACKAGE_STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Package list</returns>
        List<OpPackage> GetPackageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPackage = null, string Name = null, TypeDateTimeCode CreationDate = null, string Code = null, TypeDateTimeCode SendDate = null, int[] IdOperatorCreator = null,
                           int[] IdOperatorReceiver = null, bool? Received = null, TypeDateTimeCode ReceiveDate = null, int[] IdDistributor = null, int[] IdPackageStatus = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPackageStatusFilter

        /// <summary>
        /// Gets PackageStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackageStatus funcion</param>
        /// <param name="IdPackageStatus">Specifies filter for ID_PACKAGE_STATUS column</param>
        /// <param name="PackageStatus">Specifies filter for PACKAGE_STATUS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PackageStatus list</returns>
        List<OpPackageStatus> GetPackageStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPackageStatus = null, string PackageStatus = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPackageStatusHistoryFilter

        /// <summary>
        /// Gets PackageStatusHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackageStatusHistory funcion</param>
        /// <param name="IdPackageStatusHistory">Specifies filter for ID_PACKAGE_STATUS_HISTORY column</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="IdPackageStatus">Specifies filter for ID_PACKAGE_STATUS column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE_STATUS_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PackageStatusHistory list</returns>
        List<OpPackageStatusHistory> GetPackageStatusHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPackageStatusHistory = null, int[] IdPackage = null, int[] IdPackageStatus = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPackageDataFilter

        /// <summary>
        /// Gets PackageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackageData funcion</param>
        /// <param name="IdPackageData">Specifies filter for ID_PACKAGE_DATA column</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PackageData list</returns>
        List<OpPackageData> GetPackageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPackageData = null, int[] IdPackage = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetPaymentModuleFilter

        /// <summary>
        /// Gets PaymentModule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPaymentModule funcion</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdPaymentModuleType">Specifies filter for ID_PAYMENT_MODULE_TYPE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PAYMENT_MODULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PaymentModule list</returns>
        List<OpPaymentModule> GetPaymentModuleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPaymentModule = null, string Name = null, int[] IdPaymentModuleType = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetPaymentModuleDataFilter

        /// <summary>
        /// Gets PaymentModuleData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPaymentModuleData funcion</param>
        /// <param name="IdPaymentModuleData">Specifies filter for ID_PAYMENT_MODULE_DATA column</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PAYMENT_MODULE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PaymentModuleData list</returns>
        List<OpPaymentModuleData> GetPaymentModuleDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPaymentModuleData = null, int[] IdPaymentModule = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetPaymentModuleTypeFilter

        /// <summary>
        /// Gets PaymentModuleType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPaymentModuleType funcion</param>
        /// <param name="IdPaymentModuleType">Specifies filter for ID_PAYMENT_MODULE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PAYMENT_MODULE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PaymentModuleType list</returns>
        List<OpPaymentModuleType> GetPaymentModuleTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPaymentModuleType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetPriorityFilter

        /// <summary>
        /// Gets Priority list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPriority funcion</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Value">Specifies filter for VALUE column</param>
        /// <param name="RealizationTime">Specifies filter for REALIZATION_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PRIORITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Priority list</returns>
        List<OpPriority> GetPriorityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPriority = null, long[] IdDescr = null, int[] Value = null, int[] RealizationTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetProductCodeFilter

        /// <summary>
        /// Gets ProductCode list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProductCode funcion</param>
        /// <param name="IdProductCode">Specifies filter for ID_PRODUCT_CODE column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Density">Specifies filter for DENSITY column</param>
        /// <param name="Alpha">Specifies filter for ALPHA column</param>
        /// <param name="CalorificValue">Specifies filter for CALORIFIC_VALUE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PRODUCT_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProductCode list</returns>
        List<OpProductCode> GetProductCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProductCode = null, string Code = null, long[] IdDescr = null, int[] Density = null, Double[] Alpha = null, Double[] CalorificValue = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetProfileFilter

        /// <summary>
        /// Gets Profile list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProfile funcion</param>
        /// <param name="IdProfile">Specifies filter for ID_PROFILE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROFILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Profile list</returns>
        List<OpProfile> GetProfileFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProfile = null, string Name = null, long[] IdDescr = null, int[] IdModule = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region GetProfileDataFilter

        /// <summary>
        /// Gets ProfileData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProfileData funcion</param>
        /// <param name="IdProfileData">Specifies filter for ID_PROFILE_DATA column</param>
        /// <param name="IdProfile">Specifies filter for ID_PROFILE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROFILE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProfileData list</returns>
        List<OpProfileData> GetProfileDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdProfileData = null, int[] IdProfile = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region GetProtocolFilter

        /// <summary>
        /// Gets Protocol list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProtocol funcion</param>
        /// <param name="IdProtocol">Specifies filter for ID_PROTOCOL column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdProtocolDriver">Specifies filter for ID_PROTOCOL_DRIVER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROTOCOL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Protocol list</returns>
        List<OpProtocol> GetProtocolFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProtocol = null, string Name = null, int[] IdProtocolDriver = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetProtocolDriverFilter

        /// <summary>
        /// Gets ProtocolDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProtocolDriver funcion</param>
        /// <param name="IdProtocolDriver">Specifies filter for ID_PROTOCOL_DRIVER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROTOCOL_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProtocolDriver list</returns>
        List<OpProtocolDriver> GetProtocolDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProtocolDriver = null, string Name = null, long[] IdDescr = null, string PluginName = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetReferenceTypeFilter

        /// <summary>
        /// Gets ReferenceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReferenceType funcion</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFERENCE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReferenceType list</returns>
        List<OpReferenceType> GetReferenceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdReferenceType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRoleFilter

        /// <summary>
        /// Gets Role list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRole funcion</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROLE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Role list</returns>
        List<OpRole> GetRoleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRole = null, string Name = null, int[] IdModule = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRoleActivityFilter

        /// <summary>
        /// Gets RoleActivity list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoleActivity funcion</param>
        /// <param name="IdRoleActivity">Specifies filter for ID_ROLE_ACTIVITY column</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="IdActivity">Specifies filter for ID_ACTIVITY column</param>
        /// <param name="Deny">Specifies filter for DENY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROLE_ACTIVITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoleActivity list</returns>
        List<OpRoleActivity> GetRoleActivityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRoleActivity = null, int[] IdRole = null, int[] IdActivity = null, bool? Deny = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRoleGroupFilter

        /// <summary>
        /// Gets RoleGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoleGroup funcion</param>
        /// <param name="IdRoleParent">Specifies filter for ID_ROLE_PARENT column</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROLE_PARENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoleGroup list</returns>
        List<OpRoleGroup> GetRoleGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRoleParent = null, int[] IdRole = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteFilter

        /// <summary>
        /// Gets Route list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoute funcion</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="Year">Specifies filter for YEAR column</param>
        /// <param name="Month">Specifies filter for MONTH column</param>
        /// <param name="Week">Specifies filter for WEEK column</param>
        /// <param name="IdOperatorExecutor">Specifies filter for ID_OPERATOR_EXECUTOR column</param>
        /// <param name="IdOperatorApproved">Specifies filter for ID_OPERATOR_APPROVED column</param>
        /// <param name="DateUploaded">Specifies filter for DATE_UPLOADED column</param>
        /// <param name="DateApproved">Specifies filter for DATE_APPROVED column</param>
        /// <param name="DateFinished">Specifies filter for DATE_FINISHED column</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ExpirationDate">Specifies filter for EXPIRATION_DATE column</param>
        /// <param name="ExpirationDate">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Route list</returns>
        List<OpRoute> GetRouteFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRoute = null, int[] IdRouteDef = null, int[] Year = null, int[] Month = null, int[] Week = null, int[] IdOperatorExecutor = null,
                           int[] IdOperatorApproved = null, TypeDateTimeCode DateUploaded = null, TypeDateTimeCode DateApproved = null, TypeDateTimeCode DateFinished = null, int[] IdRouteStatus = null,
                           int[] IdDistributor = null, int[] IdRouteType = null, TypeDateTimeCode CreationDate = null, TypeDateTimeCode ExpirationDate = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteDataFilter

        /// <summary>
        /// Gets RouteData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteData funcion</param>
        /// <param name="IdRouteData">Specifies filter for ID_ROUTE_DATA column</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteData list</returns>
        List<OpRouteData> GetRouteDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteData = null, long[] IdRoute = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteDefFilter


        /// <summary>
        /// Gets RouteDef list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDef funcion</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ExpirationDate">Specifies filter for EXPIRATION_DATE column</param>
        /// <param name="DeleteDate">Specifies filter for DELETE_DATE column</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="AdHoc">Specifies filter for AD_HOC column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDef list</returns>
        List<OpRouteDef> GetRouteDefFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteDef = null, string Name = null, string Description = null, TypeDateTimeCode CreationDate = null, TypeDateTimeCode ExpirationDate = null, TypeDateTimeCode DeleteDate = null,
                           int[] IdRouteType = null, int[] IdRouteStatus = null, int[] IdOperator = null, bool? AdHoc = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteDefDataFilter

        /// <summary>
        /// Gets RouteDefData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefData funcion</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefData list</returns>
        List<OpRouteDefData> GetRouteDefDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteDef = null, long[] IdDataType = null, int[] IndexNbr = null
           , long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteDefHistoryFilter

        /// <summary>
        /// Gets RouteDefHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefHistory funcion</param>
        /// <param name="IdRouteDefHistory">Specifies filter for ID_ROUTE_DEF_HISTORY column</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefHistory list</returns>
        List<OpRouteDefHistory> GetRouteDefHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDefHistory = null, int[] IdRouteDef = null, int[] IdRouteStatus = null, int[] IdOperator = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteDefLocationFilter

        /// <summary>
        /// Gets RouteDefLocation list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefLocation funcion</param>
        /// <param name="IdRouteDefLocation">Specifies filter for ID_ROUTE_DEF_LOCATION column</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="OrderNbr">Specifies filter for ORDER_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefLocation list</returns>
        List<OpRouteDefLocation> GetRouteDefLocationFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDefLocation = null, int[] IdRouteDef = null, long[] IdLocation = null, int[] OrderNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteDeviceFilter

        /// <summary>
        /// Gets RouteDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDevice funcion</param>
        /// <param name="IdRouteDevice">Specifies filter for ID_ROUTE_DEVICE column</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDevice list</returns>
        List<OpRouteDevice> GetRouteDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDevice = null, long[] IdRoute = null, long[] SerialNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteElementStatusFilter

        /// <summary>
        /// Gets RouteElementStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteElementStatus funcion</param>
        /// <param name="IdRouteElementStatus">Specifies filter for ID_ROUTE_ELEMENT_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_ELEMENT_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteElementStatus list</returns>
        List<OpRouteElementStatus> GetRouteElementStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteElementStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRoutePointFilter

        /// <summary>
        /// Gets RoutePoint list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoutePoint funcion</param>
        /// <param name="IdRoutePoint">Specifies filter for ID_ROUTE_POINT column</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="Type">Specifies filter for TYPE column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="StartDateTime">Specifies filter for START_DATE_TIME column</param>
        /// <param name="EndDateTime">Specifies filter for END_DATE_TIME column</param>
        /// <param name="Latitude">Specifies filter for LATITUDE column</param>
        /// <param name="Longitude">Specifies filter for LONGITUDE column</param>
        /// <param name="KmCounter">Specifies filter for KM_COUNTER column</param>
        /// <param name="InvoiceNumber">Specifies filter for INVOICE_NUMBER column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_POINT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoutePoint list</returns>
        List<OpRoutePoint> GetRoutePointFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRoutePoint = null, long[] IdRoute = null, int[] Type = null, int[] IdTask = null, TypeDateTimeCode StartDateTime = null, TypeDateTimeCode EndDateTime = null,
                           Double[] Latitude = null, Double[] Longitude = null, int[] KmCounter = null, string InvoiceNumber = null, string Notes = null,
                           int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool loadCustomData = true);
        #endregion
        #region GetRoutePointDataFilter

        /// <summary>
        /// Gets RoutePointData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoutePointData funcion</param>
        /// <param name="IdRoutePointData">Specifies filter for ID_ROUTE_POINT_DATA column</param>
        /// <param name="IdRoutePoint">Specifies filter for ID_ROUTE_POINT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_POINT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoutePointData list</returns>
        List<OpRoutePointData> GetRoutePointDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRoutePointData = null, int[] IdRoutePoint = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteStatusFilter

        /// <summary>
        /// Gets RouteStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteStatus funcion</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteStatus list</returns>
        List<OpRouteStatus> GetRouteStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteTypeFilter

        /// <summary>
        /// Gets RouteType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteType funcion</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteType list</returns>
        List<OpRouteType> GetRouteTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetRouteTypeActionTypeFilter

        /// <summary>
        /// Gets RouteTypeActionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteTypeActionType funcion</param>
        /// <param name="IdRouteTypeActionType">Specifies filter for ID_ROUTE_TYPE_ACTION_TYPE column</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="Default">Specifies filter for DEFAULT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_TYPE_ACTION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteTypeActionType list</returns>
        List<OpRouteTypeActionType> GetRouteTypeActionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteTypeActionType = null, int[] IdRouteType = null, int[] IdActionType = null, bool? Default = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetScheduleTypeFilter

        /// <summary>
        /// Gets ScheduleType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllScheduleType funcion</param>
        /// <param name="IdScheduleType">Specifies filter for ID_SCHEDULE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SCHEDULE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ScheduleType list</returns>
        List<OpScheduleType> GetScheduleTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdScheduleType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSealFilter

        /// <summary>
        /// Gets Seal list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSeal funcion</param>
        /// <param name="IdSeal">Specifies filter for ID_SEAL column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SEAL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Seal list</returns>
        List<OpSeal> GetSealFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSeal = null, string SerialNbr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSettlementDocumentTypeFilter

        /// <summary>
        /// Gets SettlementDocumentType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSettlementDocumentType funcion</param>
        /// <param name="IdSettlementDocumentType">Specifies filter for ID_SETTLEMENT_DOCUMENT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="Active">Specifies filter for ACTIVE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SETTLEMENT_DOCUMENT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SettlementDocumentType list</returns>
        List<OpSettlementDocumentType> GetSettlementDocumentTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSettlementDocumentType = null, string Name = null, string Description = null, bool? Active = null, int[] IdDistributor = null, long[] IdDescr = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceFilter

        /// <summary>
        /// Gets Service list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllService funcion</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="DeleteDate">Specifies filter for DELETE_DATE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Service list</returns>
        List<OpService> GetServiceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdService = null, string Name = null, int[] IdDeviceOrderNumber = null, TypeDateTimeCode DeleteDate = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceActionResultFilter

        /// <summary>
        /// Gets ServiceActionResult list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceActionResult funcion</param>
        /// <param name="IdServiceActionResult">Specifies filter for ID_SERVICE_ACTION_RESULT column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="AllowInputText">Specifies filter for ALLOW_INPUT_TEXT column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_ACTION_RESULT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceActionResult list</returns>
        List<OpServiceActionResult> GetServiceActionResultFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceActionResult = null, int[] IdService = null, string Name = null, bool? AllowInputText = null, long[] IdDescr = null, int[] IdServiceReferenceType = null,
                           TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceDataFilter

        /// <summary>
        /// Gets ServiceData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceData funcion</param>
        /// <param name="IdServiceData">Specifies filter for ID_SERVICE_DATA column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceData list</returns>
        List<OpServiceData> GetServiceDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceData = null, int[] IdService = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceCustomFilter

        /// <summary>
        /// Gets ServiceCustom list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceCustom funcion</param>
        /// <param name="IdServiceCustom">Specifies filter for ID_SERVICE_CUSTOM column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IndirectCost">Specifies filter for INDIRECT_COST column</param>
        /// <param name="IdFile">Specifies filter for ID_FILE column</param>
        /// <param name="CustomValue">Specifies filter for CUSTOM_VALUE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_CUSTOM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceCustom list</returns>
        List<OpServiceCustom> GetServiceCustomFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceCustom = null, int[] IdServiceList = null, int[] IndirectCost = null, long[] IdFile = null, bool? CustomValue = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceCustomDataFilter

        /// <summary>
        /// Gets ServiceCustomData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceCustomData funcion</param>
        /// <param name="IdServiceCustomData">Specifies filter for ID_SERVICE_CUSTOM_DATA column</param>
        /// <param name="IdServiceCustom">Specifies filter for ID_SERVICE_CUSTOM column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_CUSTOM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceCustomData list</returns>
        List<OpServiceCustomData> GetServiceCustomDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceCustomData = null, long[] IdServiceCustom = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceDiagnosticResultFilter

        /// <summary>
        /// Gets ServiceDiagnosticResult list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceDiagnosticResult funcion</param>
        /// <param name="IdServiceDiagnosticResult">Specifies filter for ID_SERVICE_DIAGNOSTIC_RESULT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IsServiceResult">Specifies filter for IS_SERVICE_RESULT column</param>
        /// <param name="IsDiagnosticResult">Specifies filter for IS_DIAGNOSTIC_RESULT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_DIAGNOSTIC_RESULT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceDiagnosticResult list</returns>
        List<OpServiceDiagnosticResult> GetServiceDiagnosticResultFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceDiagnosticResult = null, string Name = null, long[] IdDescr = null, bool? IsServiceResult = null, bool? IsDiagnosticResult = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceInPackageFilter

        /// <summary>
        /// Gets ServiceInPackage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceInPackage funcion</param>
        /// <param name="IdServiceInPackage">Specifies filter for ID_SERVICE_IN_PACKAGE column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_IN_PACKAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceInPackage list</returns>
        List<OpServiceInPackage> GetServiceInPackageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceInPackage = null, long[] IdServicePackage = null, int[] IdService = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListFilter

        /// <summary>
        /// Gets ServiceList list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceList funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="IdOperatorReceiver">Specifies filter for ID_OPERATOR_RECEIVER column</param>
        /// <param name="IdOperatorServicer">Specifies filter for ID_OPERATOR_SERVICER column</param>
        /// <param name="CourierName">Specifies filter for COURIER_NAME column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="ShippingLetterNbr">Specifies filter for SHIPPING_LETTER_NBR column</param>
        /// <param name="ExpectedFinishDate">Specifies filter for EXPECTED_FINISH_DATE column</param>
        /// <param name="ServiceName">Specifies filter for SERVICE_NAME column</param>
        /// <param name="IdDocumentPackage">Specifies filter for ID_DOCUMENT_PACKAGE column</param>
        /// <param name="Confirmed">Specifies filter for CONFIRMED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceList list</returns>
        List<OpServiceList> GetServiceListFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, int[] IdDistributor = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, int[] IdOperatorReceiver = null, int[] IdOperatorServicer = null,
                           string CourierName = null, int[] Priority = null, string ShippingLetterNbr = null, TypeDateTimeCode ExpectedFinishDate = null, string ServiceName = null,
                           int[] IdDocumentPackage = null, bool? Confirmed = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListDataFilter

        /// <summary>
        /// Gets ServiceListData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListData funcion</param>
        /// <param name="IdServiceListData">Specifies filter for ID_SERVICE_LIST_DATA column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListData list</returns>
        List<OpServiceListData> GetServiceListDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceListData = null, int[] IdServiceList = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListDeviceFilter

        /// <summary>
        /// Gets ServiceListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDevice funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="FailureDescription">Specifies filter for FAILURE_DESCRIPTION column</param>
        /// <param name="FailureSuggestion">Specifies filter for FAILURE_SUGGESTION column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="Photo">Specifies filter for PHOTO column</param>
        /// <param name="IdOperatorServicer">Specifies filter for ID_OPERATOR_SERVICER column</param>
        /// <param name="ServiceShortDescr">Specifies filter for SERVICE_SHORT_DESCR column</param>
        /// <param name="ServiceLongDescr">Specifies filter for SERVICE_LONG_DESCR column</param>
        /// <param name="PhotoServiced">Specifies filter for PHOTO_SERVICED column</param>
        /// <param name="AtexOk">Specifies filter for ATEX_OK column</param>
        /// <param name="PayForService">Specifies filter for PAY_FOR_SERVICE column</param>
        /// <param name="IdServiceStatus">Specifies filter for ID_SERVICE_STATUS column</param>
        /// <param name="IdDiagnosticStatus">Specifies filter for ID_DIAGNOSTIC_STATUS column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDevice list</returns>
        List<OpServiceListDevice> GetServiceListDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, long[] SerialNbr = null, string FailureDescription = null, string FailureSuggestion = null, string Notes = null, long[] Photo = null,
                           int[] IdOperatorServicer = null, string ServiceShortDescr = null, string ServiceLongDescr = null, long[] PhotoServiced = null, bool? AtexOk = null,
                           bool? PayForService = null, int[] IdServiceStatus = null, int[] IdDiagnosticStatus = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListDeviceDataFilter

        /// <summary>
        /// Gets ServiceListDeviceData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceData funcion</param>
        /// <param name="IdServiceListDeviceData">Specifies filter for ID_SERVICE_LIST_DEVICE_DATA column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST_DEVICE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceData list</returns>
        List<OpServiceListDeviceData> GetServiceListDeviceDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceListDeviceData = null, int[] IdServiceList = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListDeviceActionFilter

        /// <summary>
        /// Gets ServiceListDeviceAction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceAction funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdServiceCustom">Specifies filter for ID_SERVICE_CUSTOM column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceAction list</returns>
        List<OpServiceListDeviceAction> GetServiceListDeviceActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdServiceList = null, long[] IdServicePackage = null, long[] IdServiceCustom = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListDeviceActionDetailsFilter

        /// <summary>
        /// Gets ServiceListDeviceActionDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceActionDetails funcion</param>
        /// <param name="IdServiceListDeviceActionDetails">Specifies filter for ID_SERVICE_LIST_DEVICE_ACTION_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="InputText">Specifies filter for INPUT_TEXT column</param>
        /// <param name="IdServiceActionResult">Specifies filter for ID_SERVICE_ACTION_RESULT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST_DEVICE_ACTION_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceActionDetails list</returns>
        List<OpServiceListDeviceActionDetails> GetServiceListDeviceActionDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceListDeviceActionDetails = null, long[] SerialNbr = null, int[] IdServiceList = null, long[] IdServicePackage = null, int[] IdService = null, string InputText = null,
                           int[] IdServiceActionResult = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListDeviceDiagnosticFilter

        /// <summary>
        /// Gets ServiceListDeviceDiagnostic list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceDiagnostic funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IdDiagnosticActionResult">Specifies filter for ID_DIAGNOSTIC_ACTION_RESULT column</param>
        /// <param name="InputText">Specifies filter for INPUT_TEXT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceDiagnostic list</returns>
        List<OpServiceListDeviceDiagnostic> GetServiceListDeviceDiagnosticFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdServiceList = null, int[] IdDiagnosticAction = null, int[] IdDiagnosticActionResult = null, string InputText = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServicePackageFilter

        /// <summary>
        /// Gets ServicePackage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServicePackage funcion</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="IsSingle">Specifies filter for IS_SINGLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_PACKAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServicePackage list</returns>
        List<OpServicePackage> GetServicePackageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServicePackage = null, int[] IdContract = null, string Name = null, string Descr = null, bool? IsSingle = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServicePackageDataFilter

        /// <summary>
        /// Gets ServicePackageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServicePackageData funcion</param>
        /// <param name="IdServicePackageData">Specifies filter for ID_SERVICE_PACKAGE_DATA column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Min">Specifies filter for MIN column</param>
        /// <param name="Max">Specifies filter for MAX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_PACKAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServicePackageData list</returns>
        List<OpServicePackageData> GetServicePackageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServicePackageData = null, long[] IdServicePackage = null, long[] IdDataType = null, int[] IndexNbr = null, int[] Min = null, int[] Max = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceReferenceTypeFilter

        /// <summary>
        /// Gets ServiceReferenceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceReferenceType funcion</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_REFERENCE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceReferenceType list</returns>
        List<OpServiceReferenceType> GetServiceReferenceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceReferenceType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceSummaryFilter

        /// <summary>
        /// Gets ServiceSummary list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceSummary funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="Amount">Specifies filter for AMOUNT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceSummary list</returns>
        List<OpServiceSummary> GetServiceSummaryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, long[] IdServicePackage = null, int[] Amount = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceSummaryDataFilter

        /// <summary>
        /// Gets ServiceSummaryData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceSummaryData funcion</param>
        /// <param name="IdServiceSummaryData">Specifies filter for ID_SERVICE_SUMMARY_DATA column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_SUMMARY_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceSummaryData list</returns>
        List<OpServiceSummaryData> GetServiceSummaryDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceSummaryData = null, int[] IdServiceList = null, long[] IdServicePackage = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSessionFilter

        /// <summary>
        /// Gets Session list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSession funcion</param>
        /// <param name="IdSession">Specifies filter for ID_SESSION column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdSessionStatus">Specifies filter for ID_SESSION_STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Session list</returns>
        List<OpSession> GetSessionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSession = null, int[] IdModule = null, int[] IdOperator = null, int[] IdSessionStatus = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSessionDataFilter

        /// <summary>
        /// Gets SessionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionData funcion</param>
        /// <param name="IdSessionData">Specifies filter for ID_SESSION_DATA column</param>
        /// <param name="IdSession">Specifies filter for ID_SESSION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionData list</returns>
        List<OpSessionData> GetSessionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSessionData = null, long[] IdSession = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSessionEventFilter

        /// <summary>
        /// Gets SessionEvent list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionEvent funcion</param>
        /// <param name="IdSessionEvent">Specifies filter for ID_SESSION_EVENT column</param>
        /// <param name="IdSession">Specifies filter for ID_SESSION column</param>
        /// <param name="IdEvent">Specifies filter for ID_EVENT column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionEvent list</returns>
        List<OpSessionEvent> GetSessionEventFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSessionEvent = null, long[] IdSession = null, int[] IdEvent = null, TypeDateTimeCode Time = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSessionEventDataFilter

        /// <summary>
        /// Gets SessionEventData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionEventData funcion</param>
        /// <param name="IdSessionEventData">Specifies filter for ID_SESSION_EVENT_DATA column</param>
        /// <param name="IdSessionEvent">Specifies filter for ID_SESSION_EVENT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_EVENT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionEventData list</returns>
        List<OpSessionEventData> GetSessionEventDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSessionEventData = null, long[] IdSessionEvent = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSessionStatusFilter

        /// <summary>
        /// Gets SessionStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionStatus funcion</param>
        /// <param name="IdSessionStatus">Specifies filter for ID_SESSION_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionStatus list</returns>
        List<OpSessionStatus> GetSessionStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSessionStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetShippingListFilter

        /// <summary>
        /// Gets ShippingList list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingList funcion</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="ContractNbr">Specifies filter for CONTRACT_NBR column</param>
        /// <param name="ShippingListNbr">Specifies filter for SHIPPING_LIST_NBR column</param>
        /// <param name="IdOperatorCreator">Specifies filter for ID_OPERATOR_CREATOR column</param>
        /// <param name="IdOperatorContact">Specifies filter for ID_OPERATOR_CONTACT column</param>
        /// <param name="IdOperatorQuality">Specifies filter for ID_OPERATOR_QUALITY column</param>
        /// <param name="IdOperatorProtocol">Specifies filter for ID_OPERATOR_PROTOCOL column</param>
        /// <param name="ShippingLetterNbr">Specifies filter for SHIPPING_LETTER_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdShippingListType">Specifies filter for ID_SHIPPING_LIST_TYPE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="FinishDate">Specifies filter for FINISH_DATE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingList list</returns>
        List<OpShippingList> GetShippingListFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingList = null, string ContractNbr = null, string ShippingListNbr = null, int[] IdOperatorCreator = null, int[] IdOperatorContact = null, int[] IdOperatorQuality = null,
                           int[] IdOperatorProtocol = null, string ShippingLetterNbr = null, long[] IdLocation = null, int[] IdShippingListType = null, TypeDateTimeCode CreationDate = null,
                           TypeDateTimeCode FinishDate = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetShippingListDataFilter

        /// <summary>
        /// Gets ShippingListData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListData funcion</param>
        /// <param name="IdShippingListData">Specifies filter for ID_SHIPPING_LIST_DATA column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListData list</returns>
        List<OpShippingListData> GetShippingListDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdShippingListData = null, int[] IdShippingList = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetShippingListDeviceFilter

        /// <summary>
        /// Gets ShippingListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListDevice funcion</param>
        /// <param name="IdShippingListDevice">Specifies filter for ID_SHIPPING_LIST_DEVICE column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_DEVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListDevice list</returns>
        List<OpShippingListDevice> GetShippingListDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingListDevice = null, int[] IdShippingList = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdArticle = null, TypeDateTimeCode InsertDate = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetShippingListTypeFilter

        /// <summary>
        /// Gets ShippingListType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListType funcion</param>
        /// <param name="IdShippingListType">Specifies filter for ID_SHIPPING_LIST_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListType list</returns>
        List<OpShippingListType> GetShippingListTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingListType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSimCardFilter

        /// <summary>
        /// Gets SimCard list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSimCard funcion</param>
        /// <param name="IdSimCard">Specifies filter for ID_SIM_CARD column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="Pin">Specifies filter for PIN column</param>
        /// <param name="Puk">Specifies filter for PUK column</param>
        /// <param name="MobileNetworkCode">Specifies filter for MOBILE_NETWORK_CODE column</param>
        /// <param name="Ip">Specifies filter for IP column</param>
        /// <param name="ApnLogin">Specifies filter for APN_LOGIN column</param>
        /// <param name="ApnPassword">Specifies filter for APN_PASSWORD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SIM_CARD DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SimCard list</returns>
        List<OpSimCard> GetSimCardFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdSimCard = null, int[] IdDistributor = null, string SerialNbr = null, string Phone = null, string Pin = null, string Puk = null,
                           int[] MobileNetworkCode = null, string Ip = null, string ApnLogin = null, string ApnPassword = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSimCardDataFilter

        /// <summary>
        /// Gets SimCardData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSimCardData funcion</param>
        /// <param name="IdSimCardData">Specifies filter for ID_SIM_CARD_DATA column</param>
        /// <param name="IdSimCard">Specifies filter for ID_SIM_CARD column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SIM_CARD_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SimCardData list</returns>
        List<OpSimCardData> GetSimCardDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSimCardData = null, int[] IdSimCard = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSlaFilter

        /// <summary>
        /// Gets Sla list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSla funcion</param>
        /// <param name="IdSla">Specifies filter for ID_SLA column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IssueReceiveWd">Specifies filter for ISSUE_RECEIVE_WD column</param>
        /// <param name="IssueReceiveFd">Specifies filter for ISSUE_RECEIVE_FD column</param>
        /// <param name="TechnicalSupportWd">Specifies filter for TECHNICAL_SUPPORT_WD column</param>
        /// <param name="TechnicalSupportFd">Specifies filter for TECHNICAL_SUPPORT_FD column</param>
        /// <param name="IssueResponseWd">Specifies filter for ISSUE_RESPONSE_WD column</param>
        /// <param name="IssueResponseFd">Specifies filter for ISSUE_RESPONSE_FD column</param>
        /// <param name="SystemFailureRemoveWd">Specifies filter for SYSTEM_FAILURE_REMOVE_WD column</param>
        /// <param name="SystemFailureRemoveFd">Specifies filter for SYSTEM_FAILURE_REMOVE_FD column</param>
        /// <param name="ObjectFailureRemoveWd">Specifies filter for OBJECT_FAILURE_REMOVE_WD column</param>
        /// <param name="ObjectFailureRemoveFd">Specifies filter for OBJECT_FAILURE_REMOVE_FD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SLA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Sla list</returns>
        List<OpSla> GetSlaFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSla = null, int[] IdDescr = null, string Description = null, string IssueReceiveWd = null, string IssueReceiveFd = null, string TechnicalSupportWd = null,
                           string TechnicalSupportFd = null, int[] IssueResponseWd = null, int[] IssueResponseFd = null, int[] SystemFailureRemoveWd = null, int[] SystemFailureRemoveFd = null,
                           int[] ObjectFailureRemoveWd = null, int[] ObjectFailureRemoveFd = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSlotTypeFilter

        /// <summary>
        /// Gets SlotType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSlotType funcion</param>
        /// <param name="IdSlotType">Specifies filter for ID_SLOT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SLOT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SlotType list</returns>
        List<OpSlotType> GetSlotTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSlotType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSupplierFilter

        /// <summary>
        /// Gets Supplier list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSupplier funcion</param>
        /// <param name="IdSupplier">Specifies filter for ID_SUPPLIER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SUPPLIER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Supplier list</returns>
        List<OpSupplier> GetSupplierFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSupplier = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetSystemDataFilter

        /// <summary>
        /// Gets SystemData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSystemData funcion</param>
        /// <param name="IdSystemData">Specifies filter for ID_SYSTEM_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SYSTEM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SystemData list</returns>
        List<OpSystemData> GetSystemDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSystemData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTariffFilter

        /// <summary>
        /// Gets Tariff list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTariff funcion</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TARIFF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Tariff list</returns>
        List<OpTariff> GetTariffFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTariff = null, int[] IdDistributor = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTariffDataFilter

        /// <summary>
        /// Gets TariffData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTariffData funcion</param>
        /// <param name="IdTariffData">Specifies filter for ID_TARIFF_DATA column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TARIFF_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TariffData list</returns>
        List<OpTariffData> GetTariffDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTariffData = null, int[] IdTariff = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTariffSettlementPeriodFilter

        /// <summary>
        /// Gets TariffSettlementPeriod list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTariffSettlementPeriod funcion</param>
        /// <param name="IdTariffSettlementPeriod">Specifies filter for ID_TARIFF_SETTLEMENT_PERIOD column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TARIFF_SETTLEMENT_PERIOD DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TariffSettlementPeriod list</returns>
        List<OpTariffSettlementPeriod> GetTariffSettlementPeriodFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTariffSettlementPeriod = null, int[] IdTariff = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetTaskFilter

        /// <summary>
        /// Gets Task list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTask funcion</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdTaskType">Specifies filter for ID_TASK_TYPE column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdTaskGroup">Specifies filter for ID_TASK_GROUP column</param>
        /// <param name="IdPlannedRoute">Specifies filter for ID_PLANNED_ROUTE column</param>
        /// <param name="IdOperatorRegistering">Specifies filter for ID_OPERATOR_REGISTERING column</param>
        /// <param name="IdOperatorPerformer">Specifies filter for ID_OPERATOR_PERFORMER column</param>
        /// <param name="IdTaskStatus">Specifies filter for ID_TASK_STATUS column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Accepted">Specifies filter for ACCEPTED column</param>
        /// <param name="IdOperatorAccepted">Specifies filter for ID_OPERATOR_ACCEPTED column</param>
        /// <param name="AcceptanceDate">Specifies filter for ACCEPTANCE_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="TopicNumber">Specifies filter for TOPIC_NUMBER column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="OperationCode">Specifies filter for OPERATION_CODE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Task list</returns>
        List<OpTask> GetTaskFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTask = null, int[] IdTaskType = null, int[] IdIssue = null, int[] IdTaskGroup = null, long[] IdPlannedRoute = null, int[] IdOperatorRegistering = null,
                           int[] IdOperatorPerformer = null, int[] IdTaskStatus = null, TypeDateTimeCode CreationDate = null, bool? Accepted = null, int[] IdOperatorAccepted = null,
                           TypeDateTimeCode AcceptanceDate = null, string Notes = null, TypeDateTimeCode Deadline = null, long[] IdLocation = null, int[] IdActor = null,
                           string TopicNumber = null, int[] Priority = null, long[] OperationCode = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                           bool loadTaskHistory = false, bool loadCustomData = true, List<long> customDataTypes = null);
        #endregion
        #region GetTaskDataFilter

        /// <summary>
        /// Gets TaskData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskData funcion</param>
        /// <param name="IdTaskData">Specifies filter for ID_TASK_DATA column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskData list</returns>
        List<OpTaskData> GetTaskDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTaskData = null, int[] IdTask = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTaskGroupFilter

        /// <summary>
        /// Gets TaskGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskGroup funcion</param>
        /// <param name="IdTaskGroup">Specifies filter for ID_TASK_GROUP column</param>
        /// <param name="IdParrentGroup">Specifies filter for ID_PARRENT_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="DateCreated">Specifies filter for DATE_CREATED column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskGroup list</returns>
        List<OpTaskGroup> GetTaskGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskGroup = null, int[] IdParrentGroup = null, string Name = null, TypeDateTimeCode DateCreated = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTaskHistoryFilter

        /// <summary>
        /// Gets TaskHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskHistory funcion</param>
        /// <param name="IdTaskHistory">Specifies filter for ID_TASK_HISTORY column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdTaskStatus">Specifies filter for ID_TASK_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskHistory list</returns>
        List<OpTaskHistory> GetTaskHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskHistory = null, int[] IdTask = null, int[] IdTaskStatus = null, int[] IdOperator = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                           string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTaskStatusFilter

        /// <summary>
        /// Gets TaskStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskStatus funcion</param>
        /// <param name="IdTaskStatus">Specifies filter for ID_TASK_STATUS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IsFinished">Specifies filter for IS_FINISHED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskStatus list</returns>
        List<OpTaskStatus> GetTaskStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskStatus = null, long[] IdDescr = null, string Description = null, bool? IsFinished = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTaskTypeFilter

        /// <summary>
        /// Gets TaskType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskType funcion</param>
        /// <param name="IdTaskType">Specifies filter for ID_TASK_TYPE column</param>
        /// <param name="IdTaskTypeGroup">Specifies filter for ID_TASK_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="Remote">Specifies filter for REMOTE column</param>
        /// <param name="DurationEstimate">Specifies filter for DURATION_ESTIMATE column</param>
        /// <param name="IdOperatorCreator">Specifies filter for ID_OPERATOR_CREATOR column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Authorized">Specifies filter for AUTHORIZED column</param>
        /// <param name="ImrOperator">Specifies filter for IMR_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskType list</returns>
        List<OpTaskType> GetTaskTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskType = null, int[] IdTaskTypeGroup = null, string Name = null, string Descr = null, bool? Remote = null, int[] DurationEstimate = null,
                           int[] IdOperatorCreator = null, TypeDateTimeCode CreationDate = null, bool? Authorized = null, bool? ImrOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTaskTypeGroupFilter

        /// <summary>
        /// Gets TaskTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskTypeGroup funcion</param>
        /// <param name="IdTaskTypeGroup">Specifies filter for ID_TASK_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="IdTaskTypeGroupParent">Specifies filter for ID_TASK_TYPE_GROUP_PARENT column</param>
        /// <param name="ILeft">Specifies filter for I_LEFT column</param>
        /// <param name="IRight">Specifies filter for I_RIGHT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskTypeGroup list</returns>
        List<OpTaskTypeGroup> GetTaskTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskTypeGroup = null, string Name = null, string Descr = null, int[] IdTaskTypeGroupParent = null, int[] ILeft = null, int[] IRight = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTransmissionStatusFilter

        /// <summary>
        /// Gets TransmissionStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionStatus funcion</param>
        /// <param name="IdTransmissionStatus">Specifies filter for ID_TRANSMISSION_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionStatus list</returns>
        List<OpTransmissionStatus> GetTransmissionStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetTransmissionTypeFilter

        /// <summary>
        /// Gets TransmissionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionType funcion</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionType list</returns>
        List<OpTransmissionType> GetTransmissionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetUniqueTypeFilter

        /// <summary>
        /// Gets UniqueType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllUniqueType funcion</param>
        /// <param name="IdUniqueType">Specifies filter for ID_UNIQUE_TYPE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="ProcName">Specifies filter for PROC_NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_UNIQUE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>UniqueType list</returns>
        List<OpUniqueType> GetUniqueTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdUniqueType = null, long[] IdDescr = null, string ProcName = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetUnitFilter

        /// <summary>
        /// Gets Unit list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllUnit funcion</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="IdUnitBase">Specifies filter for ID_UNIT_BASE column</param>
        /// <param name="Scale">Specifies filter for SCALE column</param>
        /// <param name="Bias">Specifies filter for BIAS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_UNIT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Unit list</returns>
        List<OpUnit> GetUnitFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdUnit = null, int[] IdUnitBase = null, Double[] Scale = null, Double[] Bias = null, string Name = null, long[] IdDescr = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetVersionTypeFilter
        /// <summary>
        /// Gets VersionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionType funcion</param>
        /// <param name="IdVersionType">Specifies filter for ID_VERSION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionType list</returns>
        List<OpVersionType> GetVersionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionStateFilter
        /// <summary>
        /// Gets VersionState list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionState funcion</param>
        /// <param name="IdVersionState">Specifies filter for ID_VERSION_STATE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_STATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionState list</returns>
        List<OpVersionState> GetVersionStateFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionState = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionRequirementFilter
        /// <summary>
        /// Gets VersionRequirement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionRequirement funcion</param>
        /// <param name="IdVersionRequirement">Specifies filter for ID_VERSION_REQUIREMENT column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdVersionRequired">Specifies filter for ID_VERSION_REQUIRED column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_REQUIREMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionRequirement list</returns>
        List<OpVersionRequirement> GetVersionRequirementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionRequirement = null, long[] IdVersion = null, long[] IdVersionRequired = null, int[] IdOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionNbrFormatFilter
        /// <summary>
        /// Gets VersionNbrFormat list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionNbrFormat funcion</param>
        /// <param name="IdVersionNbrFormat">Specifies filter for ID_VERSION_NBR_FORMAT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="RegularExpression">Specifies filter for REGULAR_EXPRESSION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_NBR_FORMAT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionNbrFormat list</returns>
        List<OpVersionNbrFormat> GetVersionNbrFormatFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionNbrFormat = null, string Name = null, long[] IdDescr = null, int[] IdDataTypeClass = null, string RegularExpression = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionHistoryFilter
        /// <summary>
        /// Gets VersionHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionHistory funcion</param>
        /// <param name="IdVersionHistory">Specifies filter for ID_VERSION_HISTORY column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdVersionState">Specifies filter for ID_VERSION_STATE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionHistory list</returns>
        List<OpVersionHistory> GetVersionHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionHistory = null, long[] IdVersion = null, int[] IdVersionState = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, int[] IdOperator = null,
                           string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionElementTypeDataFilter

        /// <summary>
        /// Gets VersionElementTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementTypeData funcion</param>
        /// <param name="IdVersionElementTypeData">Specifies filter for ID_VERSION_ELEMENT_TYPE_DATA column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementTypeData list</returns>
        List<OpVersionElementTypeData> GetVersionElementTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementTypeData = null, int[] IdVersionElementType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        #endregion
        #region GetVersionElementTypeHierarchyFilter
        /// <summary>
        /// Gets VersionElementTypeHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementTypeHierarchy funcion</param>
        /// <param name="IdVersionElementTypeHierarchy">Specifies filter for ID_VERSION_ELEMENT_TYPE_HIERARCHY column</param>
        /// <param name="IdVersionElementTypeHierarchyParent">Specifies filter for ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="Required">Specifies filter for REQUIRED column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_TYPE_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementTypeHierarchy list</returns>
        List<OpVersionElementTypeHierarchy> GetVersionElementTypeHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionElementTypeHierarchy = null, int[] IdVersionElementTypeHierarchyParent = null, int[] IdVersionElementType = null, bool? Required = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionElementTypeFilter
        /// <summary>
        /// Gets VersionElementType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementType funcion</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementType list</returns>
        List<OpVersionElementType> GetVersionElementTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionElementType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionElementHistoryFilter
        /// <summary>
        /// Gets VersionElementHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementHistory funcion</param>
        /// <param name="IdVersionElementHistory">Specifies filter for ID_VERSION_ELEMENT_HISTORY column</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementHistory list</returns>
        List<OpVersionElementHistory> GetVersionElementHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementHistory = null, int[] IdVersionElement = null, long[] IdVersion = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, int[] IdOperator = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionElementHierarchyFilter
        /// <summary>
        /// Gets VersionElementHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementHierarchy funcion</param>
        /// <param name="IdVersionElementHierarchy">Specifies filter for ID_VERSION_ELEMENT_HIERARCHY column</param>
        /// <param name="IdVersionElementHierarchyParent">Specifies filter for ID_VERSION_ELEMENT_HIERARCHY_PARENT column</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementHierarchy list</returns>
        List<OpVersionElementHierarchy> GetVersionElementHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementHierarchy = null, long[] IdVersionElementHierarchyParent = null, int[] IdVersionElement = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionElementDataFilter
        /// <summary>
        /// Gets VersionElementData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementData funcion</param>
        /// <param name="IdVersionElementData">Specifies filter for ID_VERSION_ELEMENT_DATA column</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementData list</returns>
        List<OpVersionElementData> GetVersionElementDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementData = null, int[] IdVersionElement = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionElementFilter
        /// <summary>
        /// Gets VersionElement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElement funcion</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="IdVersionNbrFormat">Specifies filter for ID_VERSION_NBR_FORMAT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="ReferenceType">Specifies filter for REFERENCE_TYPE column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElement list</returns>
        List<OpVersionElement> GetVersionElementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionElement = null, int[] IdVersionElementType = null, int[] IdVersionNbrFormat = null, string Name = null, long[] IdDescr = null, int[] ReferenceType = null,
                           long[] IdVersion = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionDataFilter
        /// <summary>
        /// Gets VersionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionData funcion</param>
        /// <param name="IdVersionData">Specifies filter for ID_VERSION_DATA column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionData list</returns>
        List<OpVersionData> GetVersionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionData = null, long[] IdVersion = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetVersionFilter
        /// <summary>
        /// Gets Version list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersion funcion</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdVersionState">Specifies filter for ID_VERSION_STATE column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="IdVersionType">Specifies filter for ID_VERSION_TYPE column</param>
        /// <param name="VersionNbr">Specifies filter for VERSION_NBR column</param>
        /// <param name="VersionRaw">Specifies filter for VERSION_RAW column</param>
        /// <param name="ReleaseDate">Specifies filter for RELEASE_DATE column</param>
        /// <param name="IdOperatorPublisher">Specifies filter for ID_OPERATOR_PUBLISHER column</param>
        /// <param name="NotificationDate">Specifies filter for NOTIFICATION_DATE column</param>
        /// <param name="IdOperatorNotifier">Specifies filter for ID_OPERATOR_NOTIFIER column</param>
        /// <param name="ConfirmDate">Specifies filter for CONFIRM_DATE column</param>
        /// <param name="IdOperatorCofirming">Specifies filter for ID_OPERATOR_COFIRMING column</param>
        /// <param name="ReleaseNotes">Specifies filter for RELEASE_NOTES column</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Version list</returns>
        List<OpVersion> GetVersionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersion = null, int[] IdVersionState = null, int[] IdVersionElementType = null, int[] IdVersionType = null, string VersionNbr = null, long[] VersionRaw = null, TypeDateTimeCode ReleaseDate = null, int[] IdOperatorPublisher = null, TypeDateTimeCode NotificationDate = null, int[] IdOperatorNotifier = null, TypeDateTimeCode ConfirmDate = null,
                                               int[] IdOperatorCofirming = null, string ReleaseNotes = null, int[] IdPriority = null, TypeDateTimeCode Deadline = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                                               long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetViewFilter

        /// <summary>
        /// Gets View list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllView funcion</param>
        /// <param name="IdView">Specifies filter for ID_VIEW column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>View list</returns>
        List<OpView> GetViewFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdView = null, string Name = null, int[] IdReferenceType = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetViewDataFilter

        /// <summary>
        /// Gets ViewData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewData funcion</param>
        /// <param name="IdViewData">Specifies filter for ID_VIEW_DATA column</param>
        /// <param name="IdView">Specifies filter for ID_VIEW column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewData list</returns>
        List<OpViewData> GetViewDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdViewData = null, int[] IdView = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetViewColumnFilter

        /// <summary>
        /// Gets ViewColumn list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumn funcion</param>
        /// <param name="IdViewColumn">Specifies filter for ID_VIEW_COLUMN column</param>
        /// <param name="IdView">Specifies filter for ID_VIEW column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Table">Specifies filter for TABLE column</param>
        /// <param name="PrimaryKey">Specifies filter for PRIMARY_KEY column</param>
        /// <param name="Size">Specifies filter for SIZE column</param>
        /// <param name="IdViewColumnSource">Specifies filter for ID_VIEW_COLUMN_SOURCE column</param>
        /// <param name="IdViewColumnType">Specifies filter for ID_VIEW_COLUMN_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumn list</returns>
        List<OpViewColumn> GetViewColumnFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdViewColumn = null, int[] IdView = null, long[] IdDataType = null, string Name = null, string Table = null, bool? PrimaryKey = null,
                            long[] Size = null, int[] IdViewColumnSource = null, int[] IdViewColumnType = null, bool? Key = null, int[] IdKeyViewColumn = null,
                            long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetViewColumnDataFilter

        /// <summary>
        /// Gets ViewColumnData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumnData funcion</param>
        /// <param name="IdViewColumnData">Specifies filter for ID_VIEW_COLUMN_DATA column</param>
        /// <param name="IdViewColumn">Specifies filter for ID_VIEW_COLUMN column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumnData list</returns>
        List<OpViewColumnData> GetViewColumnDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdViewColumnData = null, int[] IdViewColumn = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetViewColumnSourceFilter

        /// <summary>
        /// Gets ViewColumnSource list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumnSource funcion</param>
        /// <param name="IdViewColumnSource">Specifies filter for ID_VIEW_COLUMN_SOURCE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN_SOURCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumnSource list</returns>
        List<OpViewColumnSource> GetViewColumnSourceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdViewColumnSource = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetViewColumnTypeFilter

        /// <summary>
        /// Gets ViewColumnType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumnType funcion</param>
        /// <param name="IdViewColumnType">Specifies filter for ID_VIEW_COLUMN_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumnType list</returns>
        List<OpViewColumnType> GetViewColumnTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdViewColumnType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetWarrantyContractFilter

        /// <summary>
        /// Gets WarrantyContract list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWarrantyContract funcion</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdComponent">Specifies filter for ID_COMPONENT column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="WarrantyTime">Specifies filter for WARRANTY_TIME column</param>
        /// <param name="IdWarrantyStartDateCalculationMethod">Specifies filter for ID_WARRANTY_START_DATE_CALCULATION_METHOD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WarrantyContract list</returns>
        List<OpWarrantyContract> GetWarrantyContractFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceOrderNumber = null, int[] IdComponent = null, int[] IdContract = null, int[] WarrantyTime = null, int[] IdWarrantyStartDateCalculationMethod = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetWmbusTransmissionTypeFilter

        /// <summary>
        /// Gets WmbusTransmissionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWmbusTransmissionType funcion</param>
        /// <param name="IdWmbusTransmissionType">Specifies filter for ID_WMBUS_TRANSMISSION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WMBUS_TRANSMISSION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WmbusTransmissionType list</returns>
        List<OpWmbusTransmissionType> GetWmbusTransmissionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWmbusTransmissionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetWorkFilter
        /// <summary>
        /// Gets Work list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWork funcion</param>
        /// <param name="IdWork">Specifies filter for ID_WORKcolumn</param>
        /// <param name="IdWorkType">Specifies filter for ID_WORK_TYPE column</param>
        /// <param name="IdWorkStatus">Specifies filter for ID_WORK_STATUS column</param>
        /// <param name="IdWorkData">Specifies filter for ID_WORK_DATA column</param>
        /// <param name="IdWorkParent">Specifies filter for ID_WORK_PARENT column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Work list</returns>
        List<OpWork> GetWorkFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWork = null, long[] IdWorkType = null, int[] IdWorkStatus = null,
                            long[] IdWorkData = null, long[] IdWorkParent = null, int[] IdModule = null, int[] IdOperator = null,
                            TypeDateTimeCode CreationDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetWorkDataFilter
        /// <summary>
        /// Gets WorkData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdWorkData">Specifies filter for ID_WORK_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>WorkData list</returns>
        List<OpWorkData> GetWorkDataFilter(bool loadNavigationProperties = true, long[] IdWorkData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region GetWorkHistoryFilter
        /// <summary>
        /// Gets WorkHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkHistory funcion</param>
        /// <param name="IdWorkHistory">Specifies filter for ID_WORK_HISTORY column</param>
        /// <param name="IdWork">Specifies filter for ID_WORK column</param>
        /// <param name="IdWorkStatus">Specifies filter for ID_WORK_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Assembly">Specifies filter for ASSEMBLY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether objects should be loaded from connected database or from DBCollector web service</param>
        /// <returns>WorkHistory list</returns>
        List<OpWorkHistory> GetWorkHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWorkHistory = null, long[] IdWork = null, int[] IdWorkStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long[] IdDescr = null,
                                                 int[] IdModule = null, string Assembly = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region GetWorkHistoryDataFilter
        /// <summary>
        /// Gets WorkHistoryData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkHistoryData funcion</param>
        /// <param name="IdWorkHistoryData">Specifies filter for ID_WORK_HISTORY_DATA column</param>
        /// <param name="IdWorkHistory">Specifies filter for ID_WORK_HISTORY column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_HISTORY_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkHistoryData list</returns>
        List<OpWorkHistoryData> GetWorkHistoryDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWorkHistoryData = null, long[] IdWorkHistory = null, int[] ArgNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetWorkStatusFilter
        /// <summary>
        /// Gets WorkStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkStatus funcion</param>
        /// <param name="IdWorkStatus">Specifies filter for ID_WORK_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkStatus list</returns>
        List<OpWorkStatus> GetWorkStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdWorkStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetWorkTypeFilter
        /// <summary>
        /// Gets WorkType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkType funcion</param>
        /// <param name="IdWorkType">Specifies filter for ID_WORK_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkType list</returns>
        List<OpWorkType> GetWorkTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdWorkType = null, string Name = null, string PluginName = null, long[] IdDescr = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetWorkOrderJciFilter

        /// <summary>
        /// Gets WorkOrderJci list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkOrderJci funcion</param>
        /// <param name="IdWorkOrderJci">Specifies filter for ID_WORK_ORDER_JCI column</param>
        /// <param name="Wonum">Specifies filter for WONUM column</param>
        /// <param name="Sentdatetime">Specifies filter for SENTDATETIME column</param>
        /// <param name="Wodesc">Specifies filter for WODESC column</param>
        /// <param name="Ivrcode">Specifies filter for IVRCODE column</param>
        /// <param name="Subcasdesc">Specifies filter for SUBCASDESC column</param>
        /// <param name="Problemdesc">Specifies filter for PROBLEMDESC column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="Typedesc">Specifies filter for TYPEDESC column</param>
        /// <param name="Statusdesc">Specifies filter for STATUSDESC column</param>
        /// <param name="Reportdatetime">Specifies filter for REPORTDATETIME column</param>
        /// <param name="Rfdatetime">Specifies filter for RFDATETIME column</param>
        /// <param name="Ponumber">Specifies filter for PONUMBER column</param>
        /// <param name="Supplierid">Specifies filter for SUPPLIERID column</param>
        /// <param name="Suppliername">Specifies filter for SUPPLIERNAME column</param>
        /// <param name="Siteid">Specifies filter for SITEID column</param>
        /// <param name="Sitename">Specifies filter for SITENAME column</param>
        /// <param name="Sitephone">Specifies filter for SITEPHONE column</param>
        /// <param name="Equipid">Specifies filter for EQUIPID column</param>
        /// <param name="Equipdesc">Specifies filter for EQUIPDESC column</param>
        /// <param name="Downtime">Specifies filter for DOWNTIME column</param>
        /// <param name="Lumpsum">Specifies filter for LUMPSUM column</param>
        /// <param name="Disclaimertext">Specifies filter for DISCLAIMER_TEXT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_ORDER_JCI DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkOrderJci list</returns>
        List<OpWorkOrderJci> GetWorkOrderJciFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdWorkOrderJci = null, string Wonum = null, TypeDateTimeCode Sentdatetime = null, string Wodesc = null, string Ivrcode = null, string Subcasdesc = null,
                           string Problemdesc = null, int[] Priority = null, string Typedesc = null, string Statusdesc = null, TypeDateTimeCode Reportdatetime = null,
                           TypeDateTimeCode Rfdatetime = null, string Ponumber = null, string Supplierid = null, string Suppliername = null, string Siteid = null,
                           string Sitename = null, string Sitephone = null, string Equipid = null, string Equipdesc = null, string Downtime = null,
                           string Lumpsum = null, string Disclaimertext = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
 #endif
    }
}
