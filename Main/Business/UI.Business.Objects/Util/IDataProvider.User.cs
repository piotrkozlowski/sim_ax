﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;
using IMR.Suite.UI.Business.Objects.Custom;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Objects
{
    public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region Members

        DBCommonCORE dbConnectionCore { get; }
        DBCommonDAQ dbConnectionDaq { get; }
        DBCommonDW dbConnectionDw { get; }

        #endregion
        #region Properties
        #region UserLanguage
        // default language: English
        Enums.Language UserLanguage { get; set; }
        #endregion
        #region TimeZoneInfo
         TimeZoneInfo TimeZoneInfo { get; set; }
        #endregion
        #region DateTimeNow
        DateTime DateTimeNow { get; }
        #endregion
        #region DateTimeUtcNow
        DateTime DateTimeUtcNow { get; }
        #endregion
        #region ConvertUtcTimeToTimeZone
        DateTime? ConvertUtcTimeToTimeZone(DateTime? dateTime);
        DateTime ConvertUtcTimeToTimeZone(DateTime dateTime);
        #endregion
        #region ConvertTimeZoneToUtcTime
        DateTime? ConvertTimeZoneToUtcTime(DateTime? dateTime);
        DateTime ConvertTimeZoneToUtcTime(DateTime dateTime);
        #endregion

        #region Filter
        int[] DistributorFilter { get; set; }
        long[] LocationFilter { get; set; }
        long[] DeviceFilter { get; set; }
        long[] MeterFilter { get; set; }

        Dictionary<int, int> DistributorFilterDict { get; set; }
        Dictionary<long, long> LocationFilterDict { get; set; }
        Dictionary<long, long> DeviceFilterDict { get; set; }
        Dictionary<long, long> MeterFilterDict { get; set; }
        Dictionary<int, int> TaskGroupFilterDict { get; set; }

        #endregion

        List<long> IArticleDataTypes { get; set; }
        List<long> IIssueDataTypes { get; set; }
        List<long> IDeviceDataTypes { get; set; }
        List<long> IMeterDataTypes { get; set; }
        List<long> IMeterTypeDataTypes { get; set; }
        List<long> ILocationDataTypes { get; set; }
        List<long> ITaskDataTypes { get; set; }
        List<long> IRouteDataTypes { get; set; }
        List<long> IRoutePointDataTypes { get; set; }
        List<long> IOperatorDataTypes { get; set; }
        List<long> IDistributorDataTypes { get; set; }
        List<long> ISystemDataTypes { get; set; }
        List<long> IPackageDataTypes { get; set; }
        List<long> IDepositoryElementDataTypes { get; set; }
        List<long> IReportDataTypes { get; set; }
        List<long> IConsumerDataTypes { get; set; }
        List<long> IConsumerTypeDataTypes { get; set; }
        List<long> ITariffDataTypes { get; set; }
        List<long> IRefuelDataTypes { get; set; }
        List<long> IDeviceDriverDataTypes { get; set; }
        List<long> ITransmissionDriverDataTypes { get; set; }
        List<long> IContractDataTypes { get; set; }
        List<long> IServicePackageDataTypes { get; set; }
        List<long> IDiagnosticActionDataTypes { get; set; }
        List<long> IDeviceInServiceListDataTypes { get; set; }
        List<long> IDeviceOrderNumberDataTypes { get; set; }
        List<long> IDeviceDetailsDataTypes { get; set; }
        List<long> IMessageDataTypes { get; set; }
        List<long> IServiceListDataTypes { get; set; }
        List<long> IImrSeverDataTypes { get; set; }
        List<long> IServiceDataTypes { get; set; }
        List<long> IShippingListDataTypes { get; set; }
        List<long> ISimCardDataTypes { get; set; }
        List<long> IFaultCodeDataTypes { get; set; }
        List<long> ILanguageDataTypes { get; set; }
        List<long> IViewDataTypes { get; set; }
        List<long> IViewColumnDataTypes { get; set; }

        #region SystemOwner
        /// <summary>
        /// Returns System Owner Distributor item if exists
        /// </summary>
        /// <returns>OpDistributor instance</returns>
        OpDistributor SystemOwner { get; }
        #endregion

        DataLoadOptions LoadOptions { get; }
        bool LoadOptionsEnabled { get; }

        #endregion

        #region Close
        void Close();
        #endregion
        #region ClearCache
        void ClearCache();
        #endregion

        #region InitializeDBCollector
        bool InitializeDBCollector(string Binding, string Endpoint, Enums.Module Module, string Login, string Password, string UserCulture, int[] DistributorFilter = null);
        #endregion

        #region DBCollector
        
        #region IsDBCollectorEnabled
        bool? IsDBCollectorEnabled(bool useDBCollector, bool tryReconnecting = true);
        #endregion

        #endregion

        #region Actor
        /// <summary>
        /// Gets Actor by id
        /// </summary>
        /// <param name="Ids">Actor Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Actor list</returns>
        List<OpActor> GetActor(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);

        void SaveActorDetails(int? changeFromLastMinutes, long[] idActor, long[] idDataType);

        void DeleteActorDetails(int idActor);

        #endregion

        #region ActorInGroup
        /// <summary>
        /// Indicates whether the ActorInGroup is cached.
        /// </summary>
        bool ActorInGroupCacheEnabled { get; set; }


        /// <summary>
        /// Gets all ActorInGroup objects
        /// </summary>
        List<OpActorInGroup> GetAllActorInGroup();


        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Id">ActorInGroup Id</param>
        /// <returns>ActorInGroup object</returns>
        OpActorInGroup GetActorInGroup(int Id);

        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Ids">ActorInGroup Ids</param>
        /// <returns>ActorInGroup list</returns>
        List<OpActorInGroup> GetActorInGroup(int[] Ids);

        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Id">ActorInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActorInGroup object</returns>
        OpActorInGroup GetActorInGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Ids">ActorInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActorInGroup list</returns>
        List<OpActorInGroup> GetActorInGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActorInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">ActorInGroup to save</param>
        /// <returns>ActorInGroup Id</returns>
        int SaveActorInGroup(OpActorInGroup toBeSaved);

        /// <summary>
        /// Deletes the ActorInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActorInGroup to delete</param>
        void DeleteActorInGroup(OpActorInGroup toBeDeleted);

        /// <summary>
        /// Clears the ActorInGroup object cache
        /// </summary>
        void ClearActorInGroupCache();
        #endregion

        #region Action
        #region GetAction
        /// <summary>
        /// Gets Action by id
        /// </summary>
        /// <param name="Ids">Action Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Action list</returns>
        List<OpAction> GetAction(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);
        #endregion
        #region GetActionFilter
        /// <summary>
        /// Gets Action list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">If True, load navigation properties</param>
        /// <param name="mergeIntoCache">If True, query data is merged into cache and available through GetAllAction funcion</param>
        /// <returns>Action list</returns>
        List<OpAction> GetActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAction = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdActionType = null, int[] IdActionStatus = null,
                           long[] IdActionData = null, long[] IdActionParent = null, long[] IdDataArch = null, int[] IdModule = null, int[] IdOperator = null,
                           int[] IdDistributor = null, IMR.Suite.Data.DB.TypeDateTimeCode StartDateFrom = null, IMR.Suite.Data.DB.TypeDateTimeCode StartDateTo = null,
                           bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                           bool useDBCollector = false);
        #endregion
        #region SaveAction
        /// <summary>
        /// Saves the Action object to the database
        /// </summary>
        /// <param name="toBeSaved">Action to save</param>
        /// <returns>Action Id</returns>
        long SaveAction(OpAction toBeSaved);

        /// <summary>
        /// Saves the Action object to the database
        /// </summary>
        /// <param name="toBeSaved">Action to save</param>
        /// <param name="useDBCollector"></param>
        /// <returns>Action object</returns>
        OpAction SaveAction(OpAction toBeSaved, bool useDBCollector = false);
        #endregion
        #endregion

        #region ActionData

        /// <summary>
        /// Indicates whether the ActionData is cached.
        /// </summary>
        bool ActionDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionData objects
        /// </summary>
        List<OpActionData> GetAllActionData();


        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Id">ActionData Id</param>
        /// <returns>ActionData object</returns>
        OpActionData GetActionData(long Id);

        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Ids">ActionData Ids</param>
        /// <returns>ActionData list</returns>
        List<OpActionData> GetActionData(long[] Ids);

        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Id">ActionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionData object</returns>
        OpActionData GetActionData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Ids">ActionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionData list</returns>
        List<OpActionData> GetActionData(long[] Ids, bool queryDatabase);

        #region GetActionDataFilter

        /// <summary>
        /// Gets ActionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdActionData">Specifies filter for ID_ACTION_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>ActionData list</returns>
        List<OpActionData> GetActionDataFilter(bool loadNavigationProperties = true, long[] IdActionData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        /// <summary>
        /// Saves the ActionData object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionData to save</param>
        /// <returns>ActionData Id</returns>
        long SaveActionData(OpActionData toBeSaved, bool useDBCollector = false);
        long SaveActionData(long IdDataType, int IndexNbr, object Value);

        /// <summary>
        /// Deletes the ActionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionData to delete</param>
        void DeleteActionData(OpActionData toBeDeleted, bool useDBCollector = false);

        /// <summary>
        /// Clears the ActionData object cache
        /// </summary>
        void ClearActionDataCache();
        #endregion

        #region ActionDef

        #region GetActionDefFilter

        /// <summary>
        /// Gets ActionDef list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionDef funcion</param>
        /// <param name="IdActionDef">Specifies filter for ID_ACTION_DEF column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="IdActionData">Specifies filter for ID_ACTION_DATA column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionDef list</returns>
        List<OpActionDef> GetActionDefFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionDef = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IdActionType = null,
                           long[] IdActionData = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #region DeleteActionDef

        /// <summary>
        /// Deletes the ActionDef object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionDef to delete</param>
        void DeleteActionDef(OpActionDef toBeDeleted, bool useDBCollector = false);

        #endregion

        #endregion

        #region ActionHistory
        #region GetActionHistoryFilter
        /// <summary>
        /// Gets ActionHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionHistory funcion</param>
        /// <param name="IdActionHistory">Specifies filter for ID_ACTION_HISTORY column</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="IdActionStatus">Specifies filter for ID_ACTION_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdActionHistoryData">Specifies filter for ID_ACTION_HISTORY_DATA column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Assembly">Specifies filter for ASSEMBLY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether objects should be loaded from connected database or from DBCollector web service</param>
        /// <returns>ActionHistory list</returns>
        List<OpActionHistory> GetActionHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionHistory = null, long[] IdAction = null, int[] IdActionStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long[] IdDescr = null,
                           long[] IdActionHistoryData = null, int[] IdModule = null, string Assembly = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #endregion

        #region ActionHistoryData

        /// <summary>
        /// Indicates whether the ActionHistoryData is cached.
        /// </summary>
        bool ActionHistoryDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionHistoryData objects
        /// </summary>
        List<OpActionHistoryData> GetAllActionHistoryData();


        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Id">ActionHistoryData Id</param>
        /// <returns>ActionHistoryData list</returns>
        List<OpActionHistoryData> GetActionHistoryData(long Id);

        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Ids">ActionHistoryData Ids</param>
        /// <returns>ActionHistoryData list</returns>
        List<OpActionHistoryData> GetActionHistoryData(long[] Ids);

        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Id">ActionHistoryData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionHistoryData list</returns>
        List<OpActionHistoryData> GetActionHistoryData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Ids">ActionHistoryData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionHistoryData list</returns>
        List<OpActionHistoryData> GetActionHistoryData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ActionHistoryData object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionHistoryData to save</param>
        /// <returns>ActionHistoryData Id</returns>
        long SaveActionHistoryData(OpActionHistoryData toBeSaved);

        /// <summary>
        /// Deletes the ActionHistoryData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionHistoryData to delete</param>
        void DeleteActionHistoryData(OpActionHistoryData toBeDeleted);

        /// <summary>
        /// Clears the ActionHistoryData object cache
        /// </summary>
        void ClearActionHistoryDataCache();
        #endregion

        #region ActionSmsMessage
        #region GetActionSmsMessageFilter
        /// <summary>
        /// Gets ActionSmsMessage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionSmsMessage funcion</param>
        /// <param name="IdActionSmsMessage">Specifies filter for ID_ACTION_SMS_MESSAGE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="IdTransmissionStatus">Specifies filter for ID_TRANSMISSION_STATUS column</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="Message">Specifies filter for MESSAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_SMS_MESSAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionSmsMessage list</returns>
        List<OpActionSmsMessage> GetActionSmsMessageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionSmsMessage = null, long[] SerialNbr = null, TypeDateTimeCode Time = null, bool? IsIncoming = null, int[] IdTransmissionStatus = null, long[] IdAction = null,
                           string Message = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #endregion

        #region ActionSmsText

        #region GetActionSmsTextFilter

        /// <summary>
        /// Gets ActionSmsText list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionSmsText funcion</param>
        /// <param name="IdActionSmsText">Specifies filter for ID_ACTION_SMS_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="SmsText">Specifies filter for SMS_TEXT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_SMS_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionSmsText list</returns>
        List<OpActionSmsText> GetActionSmsTextFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionSmsText = null, int[] IdLanguage = null, string SmsText = null, long[] IdDataType = null, bool? IsIncoming = null, int[] IdTransmissionType = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #endregion

        #region ActionType

        /// <summary>
        /// Gets all ActionType objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        List<OpActionType> GetAllActionType(bool useDBCollector = false, bool loadNavigationProperties = true, bool loadCustomData = true);

        #endregion

        #region ActionTypeGroupFilter

        #region GetActionTypeGroupFilter

        /// <summary>
        /// Gets ActionTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeGroup funcion</param>
        /// <param name="IdActionTypeGroup">Specifies filter for ID_ACTION_TYPE_GROUP column</param>
        /// <param name="IdActionTypeGroupType">Specifies filter for ID_ACTION_TYPE_GROUP_TYPE column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdParentGroup">Specifies filter for ID_PARENT_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>ActionTypeGroup list</returns>
        List<OpActionTypeGroup> GetActionTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeGroup = null, int[] IdActionTypeGroupType = null, int[] IdReferenceType = null, int[] IdParentGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #endregion

        #region ActionTypeInGroup

        #region GetActionTypeInGroupFilter

        /// <summary>
        /// Gets ActionTypeInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeInGroup funcion</param>
        /// <param name="IdActionTypeGroup">Specifies filter for ID_ACTION_TYPE_GROUP column</param>
        /// <param name="ActionTypeName">Specifies filter for ACTION_TYPE_NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>ActionTypeInGroup list</returns>
        List<OpActionTypeInGroup> GetActionTypeInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeGroup = null, string ActionTypeName = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #endregion

        #region Activity
        /// <summary>
        /// Saves the Activity object to the database
        /// </summary>
        /// <param name="toBeSaved">Activity to save</param>
        /// <returns>Activity Id</returns>
        int SaveActivity(OpActivity toBeSaved);
        #endregion

        #region Alarm

        DataTable GetIMRSCAlarms(int IdLanguage, string AlarmModule);

        #endregion

        #region Audit

        List<long> GetAuditBatchFromTimePeriod(DateTime StartTime, DateTime EndTime, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        #endregion

        #region BillingConsumer
        List<OpBillingConsumer> GetBillingConsumer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        OpBillingConsumer GetBillingConsumer(int id, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        List<OpBillingConsumer> GetBillingConsumer(int[] ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion

        #region ConsumerType

        /// <summary>
        /// Gets ConsumerType by id
        /// </summary>
        /// <param name="Ids">ConsumerType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerType list</returns>
        List<OpConsumerType> GetConsumerType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);

        #endregion

        #region Consumer notification
        /// <summary>
        /// Gets ConsumerNotification by id
        /// </summary>
        /// <param name="Ids">ConsumerNotification Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotification list</returns>
        List<OpConsumerNotification> GetConsumerNotification(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);
        #endregion

        #region ConsumerNotificationHistory
        /// <summary>
        /// Gets ConsumerNotificationHistory by id
        /// </summary>
        /// <param name="Ids">ConsumerNotificationHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotificationHistory list</returns>
        List<OpConsumerNotificationHistory> GetConsumerNotificationHistory(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);
        #endregion

        #region ConsumerNotificationTypeData

        bool SaveConsumerPeriodCreditStatusNotification(int consumerId, int timePeriodType, DateTime startTime, DateTime executeTime, int? customInterval);

        bool DeleteConsumerPeriodCreditStatusNotification(int consumerId);

        #endregion ConsumerNotificationTypeData

        #region ConsumerPaymentModule
        /// <summary>
        /// Gets all ConsumerPaymentModule objects
        /// </summary>
        List<OpConsumerPaymentModule> GetAllConsumerPaymentModule(bool loadNavigationProperties = true);

        /// <summary>
        /// Gets ConsumerPaymentModule by id
        /// </summary>
        /// <param name="Ids">ConsumerPaymentModule Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerPaymentModule list</returns>
        List<OpConsumerPaymentModule> GetConsumerPaymentModule(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);
        #endregion

        #region ConsumerTransaction

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Ids">ConsumerTransaction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransaction list</returns>
        List<OpConsumerTransaction> GetConsumerTransactionCORE(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);

        #endregion

        #region CommandCodeParamMap

        /// <summary>
        /// Gets all CommandCodeParamMap objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// </summary>
        List<OpCommandCodeParamMap> GetAllCommandCodeParamMap(bool useDBCollector = false);

        #endregion

        #region CommandCodeType

        /// <summary>
        /// Gets all CommandCodeType objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// </summary>
        List<OpCommandCodeType> GetAllCommandCodeType(bool useDBCollector = false);

        #endregion

        #region Data
        #region GetDataDwFilterDW

        List<OpDataDw> GetDataDwFilterDW(bool loadNavigationProperties = true, long[] IdData = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, int[] Status = null, long topCount = 0, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
 
	    #endregion

        #region GetDataFilter
        /// <summary>
        /// Gets Data list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllData funcion</param>
        /// <param name="IdData">Specifies filter for ID_DATA column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>Data list</returns>
        List<OpData> GetDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdData = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                           int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region SaveData
        /// <summary>
        /// Saves the Data object to the database
        /// </summary>
        /// <param name="toBeSaved">Data to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>Data Id</returns>
        long SaveData(OpData toBeSaved, bool useDBCollector = false);
        #endregion

        #region DeleteData

        /// <summary>
        /// Deletes the Data object from the database
        /// </summary>
        /// <param name="toBeDeleted">Data to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted in connected data base or in DBCollector web service</param>
        void DeleteData(IMR.Suite.UI.Business.Objects.CORE.OpData toBeDeleted, bool useDBCollector = false);

        #endregion

        #endregion

        #region DataFormatGroupDetails

        /// <summary>
        /// Indicates whether the DataFormatGroupDetails is cached.
        /// </summary>
        bool DataFormatGroupDetailsCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataFormatGroupDetails objects
        /// </summary>
        List<OpDataFormatGroupDetails> GetAllDataFormatGroupDetails();


        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Id">DataFormatGroupDetails Id</param>
        /// <returns>DataFormatGroupDetails object</returns>
        OpDataFormatGroupDetails GetDataFormatGroupDetails(int Id);

        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Ids">DataFormatGroupDetails Ids</param>
        /// <returns>DataFormatGroupDetails list</returns>
        List<OpDataFormatGroupDetails> GetDataFormatGroupDetails(int[] Ids);

        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Id">DataFormatGroupDetails Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataFormatGroupDetails object</returns>
        OpDataFormatGroupDetails GetDataFormatGroupDetails(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Ids">DataFormatGroupDetails Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataFormatGroupDetails list</returns>
        List<OpDataFormatGroupDetails> GetDataFormatGroupDetails(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataFormatGroupDetails object to the database
        /// </summary>
        /// <param name="toBeSaved">DataFormatGroupDetails to save</param>
        /// <returns>DataFormatGroupDetails Id</returns>
        int SaveDataFormatGroupDetails(OpDataFormatGroupDetails toBeSaved);

        /// <summary>
        /// Deletes the DataFormatGroupDetails object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataFormatGroupDetails to delete</param>
        void DeleteDataFormatGroupDetails(OpDataFormatGroupDetails toBeDeleted);

        List<OpDataFormatGroupDetails> GetHierarchicalDataFormatGroupDetails(int IdOperator, int IdDistributor, int IdModule, bool loadNavigationProperties = true);

        /// <summary>
        /// Clears the DataFormatGroupDetails object cache
        /// </summary>
        void ClearDataFormatGroupDetailsCache();
        #endregion

        #region DataTransfer

        #region SaveDataTransfer
        /// <summary>
        /// Saves the DataTransfer object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTransfer to save</param>
        /// <returns>DataTransfer Id</returns>
        long SaveDataTransfer(OpDataTransfer toBeSaved);


        void SaveDataTransfer(OpDataTransfer[] toBeSaved, int commandTimeout = 0);

        #endregion

        #region DeleteDataTransfer

        void DeleteDataTransfer(long[] BatchId = null,
           string customWhereClause = null, int commandTimeout = 0);

        #endregion

        #endregion
        
        #region DeleteMeterDetails
        /// <summary>
        /// Deletes the MeterDetails by IdMeter.
        /// </summary>
        /// <param name="idMeter"></param>
        void DeleteMeterDetails(long idMeter);
        #endregion

        #region Descr

        /// <summary>
        /// Indicates whether the Descr is cached.
        /// </summary>
        bool DescrCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Descr objects
        /// </summary>
        List<OpDescr> GetAllDescr(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <returns>Descr object</returns>
        OpDescr GetDescr(long Id);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <returns>Descr list</returns>
        List<OpDescr> GetDescr(long[] Ids);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="language">Language</param>
        /// <returns>Descr object</returns>
        OpDescr GetDescr(long Id, Enums.Language language);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <param name="language">Language</param>
        /// <returns>Descr list</returns>
        List<OpDescr> GetDescr(long[] Ids, Enums.Language language);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr object</returns>
        OpDescr GetDescr(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr object</returns>
        OpDescr GetDescr(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr object</returns>
        OpDescr GetDescr(long Id, bool queryDatabase, Enums.Language language);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr list</returns>
        List<OpDescr> GetDescr(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr list</returns>
        List<OpDescr> GetDescr(long[] Ids, bool queryDatabase, Enums.Language language, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Descr object to the database
        /// </summary>
        /// <param name="toBeSaved">Descr to save</param>
        /// <returns>Descr Id</returns>
        long SaveDescr(OpDescr toBeSaved, long idDescrRangeStart = 0, long idDescrRangeStop = 0, bool useDBCollector = false);

        /// <summary>
        /// Deletes the Descr
        /// </summary>
        /// <param name="toBeDeleted">Descr to delete</param>
        void DeleteDescr(OpDescr toBeDeleted, bool useDBCollector = false);

        void ClearDescrCache();

        #region GetDescrFilter

        /// <summary>
        /// Gets Descr list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDescr funcion</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="ExtendedDescription">Specifies filter for EXTENDED_DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DESCR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Descr list</returns>
        List<OpDescr> GetDescrFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDescr = null, int[] IdLanguage = null, string Description = null, string ExtendedDescription = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #endregion

        #region DataTypeInGroup

        /// <summary>
        /// Gets all DataTypeInGroup objects
        /// </summary>
        List<OpDataTypeInGroup> GetAllDataTypeInGroup();

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Id">DataTypeInGroup Id</param>
        /// <returns>DataTypeInGroup list</returns>
        List<OpDataTypeInGroup> GetDataTypeInGroup(int Id);

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DataTypeInGroup Ids</param>
        /// <returns>DataTypeInGroup list</returns>
        List<OpDataTypeInGroup> GetDataTypeInGroup(int[] Ids);

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Id">DataTypeInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeInGroup list</returns>
        List<OpDataTypeInGroup> GetDataTypeInGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DataTypeInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeInGroup object</returns>
        List<OpDataTypeInGroup> GetDataTypeInGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataTypeInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTypeInGroup to save</param>
        /// <returns>DataTypeInGroup Id</returns>
        int SaveDataTypeInGroup(OpDataTypeInGroup toBeSaved);

        /// <summary>
        /// Deletes the DataTypeInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTypeInGroup to delete</param>
        void DeleteDataTypeInGroup(OpDataTypeInGroup toBeDeleted);

        /// <summary>
        /// Clears the DataTypeInGroup object cache
        /// </summary>
        void ClearDataTypeInGroupCache();
        #endregion

        #region DepositoryElement

        void SaveMultipleDepositoryElement(List<OpDepositoryElement> toBeSaved);

        List<OpDepositoryElement> GetTaskPackageDataForDistributor(List<int> IdDistributor, bool loadDeviceDetails = true, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        DataTable GetMountedDevicesFromServicePool(int idDistributor, long serialNbrService);

        #endregion

        #region Device

        #region CheckDeviceState
        /// <summary>
        /// CheckDeviceState cheks if device has correct ID_DEVICE_STATE
        /// if has TESTED_OK on RadioDeviceTester
        /// </summary>
        /// <param name="serialNbr">Device serial number</param>
        /// <returns>1- device has correct params, 2 - device doesn't exists, 3 - wrong state type, 
        /// 4 - device doesn't exists on RadioDeviceTester, 5 - device has TESTED_OK = 0
        /// </returns>
        int CheckDeviceState(string serialNbr, int statusValidateOption = 0, object statusValidateParam = null);
        #endregion

        #region FindDevice

        bool FindDevice(long serialNbr);

        #endregion

        #region CheckIfDeviceHasShippingList

        bool CheckIfDeviceHasShippingList(long serialNbr);

        #endregion

        DataTable GetMirroringDepositoryElements();

        #region CheckDataStatusForDevice
        void CheckDataStatusForDevice(ulong? serialNbr);
        #endregion

        #region GetDeviceBatchNumbers

        List<string> GetDeviceBatchNumbers(bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        #endregion

        #region GetDeviceManufacturingBatchNumbers

        List<string> GetDeviceManufacturingBatchNumbers(bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        #endregion

        #region SaveDevice
        /// <summary>
        /// Saves the Device object to the database
        /// </summary>
        /// <param name="toBeSaved">Device to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>Device Id</returns>
        long SaveDevice(OpDevice toBeSaved, bool useDBCollector = false);
        #endregion
        #endregion

        #region DeviceDetails

        #region GetDeviceDetailsFilter

        /// <summary>
        /// Gets DeviceDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDetails funcion</param>
        /// <param name="IdDeviceDetails">Specifies filter for ID_DEVICE_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="FactoryNbr">Specifies filter for FACTORY_NBR column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="WarrantyDate">Specifies filter for WARRANTY_DATE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="ProductionDeviceOrderNumber">Specifies filter for PRODUCTION_DEVICE_ORDER_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDetails list</returns>
        List<OpDeviceDetails> GetDeviceDetailsFilterUser(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDetails = null, long[] SerialNbr = null, long[] IdLocation = null, string FactoryNbr = null, TypeDateTimeCode ShippingDate = null, TypeDateTimeCode WarrantyDate = null,
                   string Phone = null, int[] IdDeviceType = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, int[] IdDeviceStateType = null,
                   string ProductionDeviceOrderNumber = null, string DeviceBatchNumber = null, int[] IdDistributorOwner = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetDeviceDetailsFilterCustom
        /// <summary>
        /// Gets DeviceDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDetails funcion</param>
        /// <param name="IdDeviceDetails">Specifies filter for ID_DEVICE_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="FactoryNbr">Specifies filter for FACTORY_NBR column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="WarrantyDate">Specifies filter for WARRANTY_DATE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="ProductionDeviceOrderNumber">Specifies filter for PRODUCTION_DEVICE_ORDER_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDetails list</returns>
        List<OpDeviceDetails> GetDeviceDetailsFilterCustom(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDetails = null, long[] SerialNbr = null, long[] IdLocation = null, string FactoryNbr = null, TypeDateTimeCode ShippingDate = null, TypeDateTimeCode WarrantyDate = null,
                   string Phone = null, int[] IdDeviceType = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, int[] IdDeviceStateType = null,
                   string ProductionDeviceOrderNumber = null, string DeviceBatchNumber = null, int[] IdDistributorOwner = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion

        #region DevicesInvoice

        DataTable DevicesInvoice(int IdDistributor, DateTime? ReferenceDate = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion

        #region GetDistributorAllowedDeviceAttributes

        Tuple<List<OpDeviceType>, List<OpDeviceOrderNumber>, List<string>, List<OpFaultCode>, List<OpShippingList>, List<string>> GetDistributorAllowedDeviceAttributes(int[] IdDistributor = null, bool loadNavigationProperties = true, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion

        #region SaveDeviceDetails

        void SaveDeviceDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType);

        #endregion

        #endregion

        #region DeviceDriverMemoryMap

        #region GetDeviceDriverMemoryMapFilter

        /// <summary>
        /// Gets DeviceDriverMemoryMap list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriverMemoryMap funcion</param>
        /// <param name="IdDeviceDriverMemoryMap">Specifies filter for ID_DEVICE_DRIVER_MEMORY_MAP column</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="FileName">Specifies filter for FILE_NAME column</param>
        /// <param name="FileVersion">Specifies filter for FILE_VERSION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER_MEMORY_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DeviceDriverMemoryMap list</returns>
        List<OpDeviceDriverMemoryMap> GetDeviceDriverMemoryMapFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceDriverMemoryMap = null, int[] IdDeviceDriver = null, string FileName = null, string FileVersion = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
        bool useDBCollector = false);
        #endregion
        #region GetDeviceDriverMemoryMapDataFilter

        /// <summary>
        /// Gets DeviceDriverMemoryMapData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriverMemoryMapData funcion</param>
        /// <param name="IdDeviceDriverMemoryMapData">Specifies filter for ID_DEVICE_DRIVER_MEMORY_MAP_DATA column</param>
        /// <param name="IdDdmmdParent">Specifies filter for ID_DDMMD_PARENT column</param>
        /// <param name="IdDeviceDriverMemoryMap">Specifies filter for ID_DEVICE_DRIVER_MEMORY_MAP column</param>
        /// <param name="IdOmb">Specifies filter for ID_OMB column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="IsRead">Specifies filter for IS_READ column</param>
        /// <param name="IsWrite">Specifies filter for IS_WRITE column</param>
        /// <param name="IsPutDataOmb">Specifies filter for IS_PUT_DATA_OMB column</param>
        /// <param name="IdDdmmdPutData">Specifies filter for ID_DDMMD_PUT_DATA column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER_MEMORY_MAP_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DeviceDriverMemoryMapData list</returns>
        List<OpDeviceDriverMemoryMapData> GetDeviceDriverMemoryMapDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDriverMemoryMapData = null, long[] IdDdmmdParent = null, int[] IdDeviceDriverMemoryMap = null, int[] IdOmb = null, string Name = null, long[] IdDataType = null,
                           int[] IndexNbr = null, bool? IsRead = null, bool? IsWrite = null, bool? IsPutDataOmb = null, long[] IdDdmmdPutData = null,
                   long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                   bool useDBCollector = false);
        #endregion

        #endregion

        #region DeviceHierarchy

        #region SaveDeviceHierarchy
        /// <summary>
        /// Saves the DeviceHierarchy object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceHierarchy to save</param>
        /// <returns>DeviceHierarchy Id</returns>
        long SaveDeviceHierarchy(OpDeviceHierarchy toBeSaved, bool useDBCollector = false);
        #endregion

        #endregion

        #region DevicePattern

        Dictionary<long, long> GetDevicesPattern();

        #region GetDevicePattern

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Id">DevicePattern Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern object</returns>
        OpDevicePattern GetDevicePattern(long Id, bool useDBCollector = false);

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Ids">DevicePattern Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern list</returns>
        List<OpDevicePattern> GetDevicePattern(long[] Ids, bool useDBCollector = false);

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Id">DevicePattern Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern object</returns>
        OpDevicePattern GetDevicePattern(long Id, bool queryDatabase, bool useDBCollector = false);

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Ids">DevicePattern Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern list</returns>
        List<OpDevicePattern> GetDevicePattern(long[] Ids, bool queryDatabase, bool useDBCollector = false);

        #endregion

        #region GetDevicePatternData

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Id">DevicePatternData Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData object</returns>
        List<OpDevicePatternData> GetDevicePatternData(long Id, bool useDBCollector = false);

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Ids">DevicePatternData Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData list</returns>
        List<OpDevicePatternData> GetDevicePatternData(long[] Ids, bool useDBCollector = false);

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Id">DevicePatternData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData object</returns>
        List<OpDevicePatternData> GetDevicePatternData(long Id, bool queryDatabase, bool useDBCollector = false);

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Ids">DevicePatternData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData list</returns>
        List<OpDevicePatternData> GetDevicePatternData(long[] Ids, bool queryDatabase, bool useDBCollector = false);

        #endregion

        #region GetDevicePatternFilter

        /// <summary>
        /// Gets DevicePattern list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDevicePattern funcion</param>
        /// <param name="IdPattern">Specifies filter for ID_PATTERN column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="CreatedBy">Specifies filter for CREATED_BY column</param>
        /// <param name="IdTemplate">Specifies filter for ID_TEMPLATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PATTERN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern list</returns>
        List<OpDevicePattern> GetDevicePatternFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPattern = null, long[] SerialNbr = null, int[] IdDistributor = null, string Name = null, TypeDateTimeCode CreationDate = null, int[] CreatedBy = null,
                           int[] IdTemplate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                           bool useDBCollector = false);
        #endregion

        #endregion

        #region DeviceStateHistory
        #region GetDeviceStateHistoryFilter
        /// <summary>
        /// Gets DeviceStateHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceStateHistory funcion</param>
        /// <param name="IdDeviceState">Specifies filter for ID_DEVICE_STATE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_STATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceStateHistory list</returns>
        List<OpDeviceStateHistory> GetDeviceStateHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceState = null, long[] SerialNbr = null, int[] IdDeviceStateType = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region GetDeviceStateHistoryByDistributor

        List<OpDeviceStateHistory> GetDeviceStateHistoryByDistributor(int[] IdDistributor,
                                                                            bool IncludeShipping = true,
                                                                            bool IncludeService = true,
                                                                            bool loadNavigationProperties = false);
        #endregion
        #region SaveDeviceStateHistory
        /// <summary>
        /// Saves the DeviceStateHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceStateHistory to save</param>
        /// <returns>DeviceStateHistory Id</returns>
        long SaveDeviceStateHistory(OpDeviceStateHistory toBeSaved, bool useDBCollector = false);
        #endregion

        #region DeleteDeviceStateHistory

        /// <summary>
        /// Deletes the DeviceStateHistory object from the database
        /// </summary>
        /// <param name="useDBCollector">Indicates whether data should be deleted from connected data base or from DBCollector web service</param>
        /// <param name="toBeDeleted">DeviceStateHistory to delete</param>
        void DeleteDeviceStateHistory(OpDeviceStateHistory toBeDeleted, bool useDBCollector = false);

        #endregion

        #endregion

        #region DeviceStateType
        /// <summary>
        /// Saves or adds the DeviceStateType object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceStateType to save</param>
        /// <returns>DeviceStateType Id</returns>
        int SaveDeviceStateType(OpDeviceStateType toBeSaved);
        #endregion

        #region DeviceTemplate
        #region DeviceTemplate
        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Id">DeviceTemplate Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate object</returns>
        OpDeviceTemplate GetDeviceTemplate(int Id, bool useDBCollector = false);

        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Ids">DeviceTemplate Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate list</returns>
        List<OpDeviceTemplate> GetDeviceTemplate(int[] Ids, bool useDBCollector = false);

        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Id">DeviceTemplate Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate object</returns>
        OpDeviceTemplate GetDeviceTemplate(int Id, bool queryDatabase, bool useDBCollector = false);

        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Ids">DeviceTemplate Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate list</returns>
        List<OpDeviceTemplate> GetDeviceTemplate(int[] Ids, bool queryDatabase, bool useDBCollector = false);
        #endregion
        #region DeviceTemplateData
        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Id">DeviceTemplateData Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData object</returns>
        List<OpDeviceTemplateData> GetDeviceTemplateData(int Id, bool useDBCollector = false);

        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Ids">DeviceTemplateData Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData list</returns>
        List<OpDeviceTemplateData> GetDeviceTemplateData(int[] Ids, bool useDBCollector = false);

        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Id">DeviceTemplateData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData object</returns>
        List<OpDeviceTemplateData> GetDeviceTemplateData(int Id, bool queryDatabase, bool useDBCollector = false);

        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Ids">DeviceTemplateData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData list</returns>
        List<OpDeviceTemplateData> GetDeviceTemplateData(int[] Ids, bool queryDatabase, bool useDBCollector = false);
        #endregion
        #endregion

        #region DeviceTypeInGroup

        /// <summary>
        /// Indicates whether the DeviceTypeInGroup is cached.
        /// </summary>
        bool DeviceTypeInGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeInGroup objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        List<OpDeviceTypeInGroup> GetAllDeviceTypeInGroup(bool useDBCollector = false);

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Id">DeviceTypeInGroup Id</param>
        /// <returns>DeviceTypeInGroup list</returns>
        List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int Id);

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DeviceTypeInGroup Ids</param>
        /// <returns>DeviceTypeInGroup list</returns>
        List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int[] Ids);

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Id">DeviceTypeInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeInGroup list</returns>
        List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DeviceTypeInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeInGroup list</returns>
        List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceTypeInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeInGroup to save</param>
        /// <returns>DeviceTypeInGroup Id</returns>
        int SaveDeviceTypeInGroup(OpDeviceTypeInGroup toBeSaved);

        /// <summary>
        /// Deletes the DeviceTypeInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeInGroup to delete</param>
        void DeleteDeviceTypeInGroup(OpDeviceTypeInGroup toBeDeleted);

        /// <summary>
        /// Clears the DeviceTypeInGroup object cache
        /// </summary>
        void ClearDeviceTypeInGroupCache();
        #endregion

        #region DeviceHierarchyPattern

        #region GetAllDeviceHierarchyPattern
        /// <summary>
        /// Gets all DeviceHierarchyPattern objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        List<OpDeviceHierarchyPattern> GetAllDeviceHierarchyPattern(bool useDBCollector = false);
        #endregion

        #endregion

        #region Distributor

        /// <summary>
        /// Indicates whether the Distributor is cached.
        /// </summary>
        bool DistributorCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Distributor objects
        /// </summary>
        List<OpDistributor> GetAllDistributor(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Id">Distributor Id</param>
        /// <returns>Distributor object</returns>
        OpDistributor GetDistributor(int Id);

        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Ids">Distributor Ids</param>
        /// <returns>Distributor list</returns>
        List<OpDistributor> GetDistributor(int[] Ids);

        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Id">Distributor Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Distributor object</returns>
        OpDistributor GetDistributor(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Ids">Distributor Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Distributor list</returns>
        List<OpDistributor> GetDistributor(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Distributor object to the database
        /// </summary>
        /// <param name="toBeSaved">Distributor to save</param>
        /// <returns>Distributor Id</returns>
        int SaveDistributor(OpDistributor toBeSaved);

        /// <summary>
        /// Deletes the Distributor object from the database
        /// </summary>
        /// <param name="toBeDeleted">Distributor to delete</param>
        void DeleteDistributor(OpDistributor toBeDeleted);

        /// <summary>
        /// Clears the Distributor object cache
        /// </summary>
        void ClearDistributorCache();
        #endregion

        #region File

        /// <summary>
        /// Indicates whether the File is cached.
        /// </summary>
        bool FileCacheEnabled { get; set; }

        /// <summary>
        /// Gets all File objects
        /// </summary>
        List<OpFile> GetAllFile(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Id">File Id</param>
        /// <returns>File object</returns>
        OpFile GetFile(long Id);

        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Ids">File Ids</param>
        /// <returns>File list</returns>
        List<OpFile> GetFile(long[] Ids);

        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Id">File Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>File object</returns>
        OpFile GetFile(long Id, bool queryDatabase);

        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Ids">File Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>File list</returns>
        List<OpFile> GetFile(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Gets File list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFile funcion</param>
        /// <param name="IdFile">Specifies filter for ID_FILE column</param>
        /// <param name="PhysicalFileName">Specifies filter for PHYSICAL_FILE_NAME column</param>
        /// <param name="Size">Specifies filter for SIZE column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="Version">Specifies filter for VERSION column</param>
        /// <param name="LastDownloaded">Specifies filter for LAST_DOWNLOADED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>File list</returns>
        List<OpFile> GetFileFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdFile = null, string PhysicalFileName = null, long[] Size = null, TypeDateTimeCode InsertDate = null, string Description = null, string Version = null,
                           TypeDateTimeCode LastDownloaded = null, bool? IsBlocked = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        /// <summary>
        /// Saves the File object to the database
        /// </summary>
        /// <param name="toBeSaved">File to save</param>
        /// <returns>File Id</returns>
        long SaveFile(OpFile toBeSaved);

        /// <summary>
        /// Deletes the File object from the database
        /// </summary>
        /// <param name="toBeDeleted">File to delete</param>
        void DeleteFile(OpFile toBeDeleted);

        /// <summary>
        /// Clears the File object cache
        /// </summary>
        void ClearFileCache();


        /// <summary>
        /// Gets File content
        /// </summary>
        /// <param name="file">File object for which to load file content</param>
        /// <returns>void</returns>
        void GetFileContent(OpFile file);

        /// <summary>
        /// Saves the content of the File object to the database, this function does not save standart file properties
        /// </summary>
        /// <param name="toBeSaved">File to save</param>
        /// <returns>File Id</returns>
        void SaveFileContent(OpFile toBeSaved);

        #endregion

        #region GetDevice
        /// <summary>
        /// Gets Device by id
        /// </summary>
        /// <param name="Ids">Device Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>Device list</returns>
        List<OpDevice> GetDevice(long[] Ids, bool queryDatabase, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetLocation
        /// <summary>
        /// Gets Location by id
        /// </summary>
        /// <param name="Ids">Location Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>Location list</returns>
        List<OpLocation> GetLocation(long[] Ids, bool queryDatabase, bool loadNavigationProperties = true, bool loadCustomData = true,
           bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion
        #region GetMeter
        /// <summary>
        /// Gets Meter by id
        /// </summary>
        /// <param name="Ids">Meter Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>Meter list</returns>
        List<OpMeter> GetMeter(long[] Ids, bool queryDatabase, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion

        #region ImrServer
        /// <summary>
        /// Saves the ImrServer object to the database
        /// </summary>
        /// <param name="toBeSaved">ImrServer to save</param>
        /// <returns>ImrServer Id</returns>
        int SaveImrServer(OpImrServer toBeSaved);
        #endregion

        #region Language
        /// <summary>
        /// Saves the Language object to the database
        /// </summary>
        /// <param name="toBeSaved">Language to save</param>
        /// <returns>Language Id</returns>
        int SaveLanguage(OpLanguage toBeSaved);
        #endregion

        #region Location

        List<OpLocation> GetLocationMagazineDistinct(int[] IdLocationType, int[] IdDistributor, bool AllowBlankCid,
           bool loadNavigationProperties = true, bool mergeIntoCache = false,
           bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        /// <summary>
        /// Saves the Location object to the database
        /// </summary>
        /// <param name="toBeSaved">Location to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>Location Id</returns>
        long SaveLocation(OpLocation toBeSaved, bool useDBCollector = false);

        List<OpLocation> GetAll(bool? exludeDeletedAndUnused = null);

        void SaveLocationDetails(int? changeFromLastMinutes, long[] idLocation, long[] idDataType);

        #endregion

        #region LocationStateType
        /// <summary>
        /// Saves the LocationStateType object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationStateType to save</param>
        /// <returns>LocationStateType Id</returns>
        int SaveLocationStateType(OpLocationStateType toBeSaved);
        #endregion

        #region LocationType
        /// <summary>
        /// Saves the LocationType object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationType to save</param>
        /// <returns>LocationType Id</returns>
        int SaveLocationType(OpLocationType toBeSaved);
        #endregion

        #region LocationEquipment

        /// <summary>
        /// Saves the LocationEquipment object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationEquipment to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>LocationEquipment Id</returns>
        long SaveLocationEquipment(OpLocationEquipment toBeSaved, bool useDBCollector = false);
        #endregion

        #region LocationHierarchy

        #region SaveLocationHierarchy

        /// <summary>
        /// Saves the LocationHierarchy object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationHierarchy to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>LocationHierarchy Id</returns>
        long SaveLocationHierarchy(OpLocationHierarchy toBeSaved, bool useDBCollector = false);

        #endregion

        #region DeleteLocationHierarchy

        /// <summary>
        /// Deletes the LocationHierarchy object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationHierarchy to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted in connected data base or in DBCollector web service</param>
        void DeleteLocationHierarchy(OpLocationHierarchy toBeDeleted, bool useDBCollector = false);

        #endregion

        #endregion

        #region LocationStateHistory
        /// <summary>
        /// Saves the LocationStateHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationStateHistory to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>LocationStateHistory Id</returns>
        long SaveLocationStateHistory(OpLocationStateHistory toBeSaved, bool useDBCollector = false);

        /// <summary>
        /// Deletes the LocationStateHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationStateHistory to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted in connected data base or in DBCollector web service</param>
        void DeleteLocationStateHistory(OpLocationStateHistory toBeDeleted, bool useDBCollector = false);
        #endregion

        #region MeterType
        /// <summary>
        /// Saves the MeterType object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterType to save</param>
        /// <returns>MeterType Id</returns>
        int SaveMeterType(OpMeterType toBeSaved);
        #endregion

        #region MeterTypeClass
        /// <summary>
        /// Saves the MeterTypeClass object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeClass to save</param>
        /// <returns>MeterTypeClass Id</returns>
        int SaveMeterTypeClass(OpMeterTypeClass toBeSaved);
        #endregion

        #region MeterTypeDeviceType

        /// <summary>
        /// Indicates whether the MeterTypeDeviceType is cached.
        /// </summary>
        bool MeterTypeDeviceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterTypeDeviceType objects
        /// </summary>
        List<OpMeterTypeDeviceType> GetAllMeterTypeDeviceType();


        /// <summary>
        /// Gets MeterTypeDeviceType by IdMeterType and IdDeviceType
        /// </summary>
        /// <param name="IdMeterType"></param>
        /// <param name="IdDeviceType"></param>
        /// <returns>MeterTypeDeviceType object</returns>
        OpMeterTypeDeviceType GetMeterTypeDeviceType(int idMeterType, int idDeviceType);

        /// <summary>
        /// Gets MeterTypeDeviceType by IdMeterType or IdDeviceType
        /// </summary>
        /// <param name="IdMeterType"></param>
        /// <param name="IdDeviceType"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeDeviceType list</returns>
        List<OpMeterTypeDeviceType> GetMeterTypeDeviceType(int? idMeterType, int? idDeviceType);

        /// <summary>
        /// Gets MeterTypeDeviceType by IdMeterType or IdDeviceType
        /// </summary>
        /// <param name="IdMeterType"></param>
        /// <param name="IdDeviceType"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeDeviceType list</returns>
        List<OpMeterTypeDeviceType> GetMeterTypeDeviceType(int? idMeterType, int? idDeviceType, bool queryDatabase);

        /// <summary>
        /// Saves the MeterTypeDeviceType object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeDeviceType to save</param>
        void SaveMeterTypeDeviceType(OpMeterTypeDeviceType toBeSaved);

        /// <summary>
        /// Deletes the MeterTypeDeviceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterTypeDeviceType to delete</param>
        void DeleteMeterTypeDeviceType(OpMeterTypeDeviceType toBeDeleted);

        /// <summary>
        /// Clears the MeterTypeDeviceType object cache
        /// </summary>
        void ClearMeterTypeDeviceTypeCache();
        #endregion

        #region MeterTypeInGroup

        /// <summary>
        /// Indicates whether the MeterTypeInGroup is cached.
        /// </summary>
        bool MeterTypeInGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterTypeInGroup objects
        /// </summary>
        List<OpMeterTypeInGroup> GetAllMeterTypeInGroup();


        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Id">MeterTypeInGroup Id</param>
        /// <returns>MeterTypeInGroup object</returns>
        List<OpMeterTypeInGroup> GetMeterTypeInGroup(int Id);

        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Ids">MeterTypeInGroup Ids</param>
        /// <returns>MeterTypeInGroup list</returns>
        List<OpMeterTypeInGroup> GetMeterTypeInGroup(int[] Ids);

        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Id">MeterTypeInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeInGroup object</returns>
        List<OpMeterTypeInGroup> GetMeterTypeInGroup(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Ids">MeterTypeInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeInGroup list</returns>
        List<OpMeterTypeInGroup> GetMeterTypeInGroup(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the MeterTypeInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeInGroup to save</param>
        /// <returns>MeterTypeInGroup Id</returns>
        int SaveMeterTypeInGroup(OpMeterTypeInGroup toBeSaved);

        /// <summary>
        /// Deletes the MeterTypeInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterTypeInGroup to delete</param>
        void DeleteMeterTypeInGroup(OpMeterTypeInGroup toBeDeleted);

        /// <summary>
        /// Clears the MeterTypeInGroup object cache
        /// </summary>
        void ClearMeterTypeInGroupCache();
        #endregion

        #region Module
        /// <summary>
        /// Saves the Module object to the database
        /// </summary>
        /// <param name="toBeSaved">Module to save</param>
        /// <returns>Module Id</returns>
        int SaveModule(OpModule toBeSaved);
        #endregion

        #region Operator
        /// <summary>
        /// Gets Operator by id
        /// </summary>
        /// <param name="Ids">Operator Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Operator list</returns>
        List<OpOperator> GetOperator(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);
        #region GetOperatorFilter 

        // List<OpOperator> GetOperatorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOperator = null, int[] IdActor = null, int[] IdDistributor = null, long[] SerialNbr = null, string Login = null, string Password = null,
        //                    bool? IsBlocked = null, string Description = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        //{
        //    return GetOperatorFilter(loadNavigationProperties: loadNavigationProperties, loadCustomData: true, mergeIntoCache: mergeIntoCache,
        //    IdOperator: IdOperator, IdActor: IdActor, 
        //}
        /// <summary>
        /// Gets Operator list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperator funcion</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Login">Specifies filter for LOGIN column</param>
        /// <param name="Password">Specifies filter for PASSWORD column</param>
        /// <param name="IsBlocked">Specifies filter for IS_BLOCKED column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Operator list</returns>
        List<OpOperator> GetOperatorFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdOperator = null, int[] IdActor = null, int[] IdDistributor = null, long[] SerialNbr = null, string Login = null, string Password = null,
                           bool? IsBlocked = null, string Description = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetOperatorShippingListMailingList
        List<OpOperator> GetOperatorShippingListMailingList(int IdDistributor, int OperationType);
        #endregion
        #region DeleteOperatorData
        /// <summary>
        /// Deletes the OperatorData object from the database
        /// </summary>
        /// <param name="toBeDeleted">OperatorData to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted from connected data base or from DBCollector web service</param>
        void DeleteOperatorData(OpOperatorData toBeDeleted, bool useDBCollector = false);
        #endregion
        /// <summary>
        /// Saves the OperatorData object to the database
        /// </summary>
        /// <param name="toBeSaved">OperatorData to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>OperatorData Id</returns>
        long SaveOperatorData(OpOperatorData toBeSaved, bool useDBCollector = false);

        /// <summary>
        /// Deletes the OperatorDetails by IdOperator.
        /// </summary>
        /// <param name="idOperator"></param>
        void DeleteOperatorDetails(int idOperator);

        void SaveOperatorDetails(int? changeFromLastMinutes, long[] idOperator, long[] idDataType);
        #endregion

        #region OperatorRole

        /// <summary>
        /// Indicates whether the Activity is cached.
        /// </summary>
        bool OperatorRoleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all OperatorRole objects
        /// </summary>
        List<OpOperatorRole> GetAllOperatorRole();

        /// <summary>
        /// Gets OperatorRole by IdOperator and IdRole
        /// </summary>
        /// <param name="IdOperator"></param>
        /// <param name="IdRole"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OperatorRole object</returns>
        List<OpOperatorRole> GetOperatorRole(int? IdOperator, int? IdRole, bool queryDatabase = false);

        void SaveOperatorRole(OpOperatorRole toBeSaved);

        void DeleteOperatorRole(OpOperatorRole toBeDeleted);

        void DeleteOperatorRoleByOperatorAndRole(int idOperator, int idRole);

        void ClearOperatorRoleCache();
        #endregion

        #region Protocol
        /// <summary>
        /// Saves the Protocol object to the database
        /// </summary>
        /// <param name="toBeSaved">Protocol to save</param>
        /// <returns>Protocol Id</returns>
        int SaveProtocol(OpProtocol toBeSaved);
        #endregion

        #region ReferenceType

        object GetReferenceObject(Enums.ReferenceType referenceType, object referenceValue, bool loadNavigationProperties = true, bool loadCustomData = true);

        #endregion

        #region RoleGroup

        /// <summary>
        /// Indicates whether the Activity is cached.
        /// </summary>
        bool RoleGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RoleGroup objects
        /// </summary>
        List<OpRoleGroup> GetAllRoleGroup();

        /// <summary>
        /// Gets RoleGroup by IdOperator and IdRole
        /// </summary>
        /// <param name="IdParentRole"></param>
        /// <param name="IdRole"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoleGroup object</returns>
        List<OpRoleGroup> GetRoleGroup(int? IdParentRole, int? IdRole, bool queryDatabase = false);

        void SaveRoleGroup(OpRoleGroup toBeSaved);

        void DeleteRoleGroup(OpRoleGroup toBeDeleted);

        void ClearRoleGroupCache();
        #endregion

        #region Route

        void SaveRouteDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType);
        void DeleteRouteDetails(long idRoute);

        #endregion

        #region RouteDefData
        /// <summary>
        /// Saves the RouteDefData object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDefData to save</param>
        /// <returns>RouteDefData Id</returns>
        void SaveRouteDefData(OpRouteDefData toBeSaved);

        /// <summary>
        /// Deletes the RouteDefData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDefData to delete</param>
        void DeleteRouteDefData(OpRouteDefData toBeDeleted);
        #endregion

        #region RouteDefLocationData

        /// <summary>
        /// Gets all RouteDefLocationData objects (Cache disabled)
        /// </summary>
        List<OpRouteDefLocationData> GetAllRouteDefLocationData();


        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Id">RouteDefLocationData Id</param>
        /// <returns>RouteDefLocationData object</returns>
        OpRouteDefLocationData GetRouteDefLocationData(long Id);

        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Ids">RouteDefLocationData Ids</param>
        /// <returns>RouteDefLocationData list</returns>
        List<OpRouteDefLocationData> GetRouteDefLocationData(long[] Ids);

        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Id">RouteDefLocationData Id</param>
        /// <param name="queryDatabase">If True, gets object from database (Cache disabled)</param>
        /// <returns>RouteDefLocationData object</returns>
        OpRouteDefLocationData GetRouteDefLocationData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Ids">RouteDefLocationData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database (Cache disabled)</param>
        /// <returns>RouteDefLocationData list</returns>
        List<OpRouteDefLocationData> GetRouteDefLocationData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RouteDefLocationData object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDefLocationData to save</param>
        /// <returns>RouteDefLocationData Id</returns>
        void SaveRouteDefLocationData(OpRouteDefLocationData toBeSaved);

        /// <summary>
        /// Deletes the RouteDefLocationData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDefLocationData to delete</param>
        void DeleteRouteDefLocationData(OpRouteDefLocationData toBeDeleted);

        ///// <summary>
        ///// Clears the RouteDefLocationData object cache
        ///// </summary>
        // void ClearRouteDefLocationDataCache()
        //{
        //    RouteDefLocationDataDict.Clear();
        //    RouteDefLocationDataIsFullCache = false;
        //}
        #endregion

        #region HolidayDate
        DateTime GetWorkingDate(DateTime startDate, int days, string countryCode);
        #endregion

        #region Schedule

        #region DeleteSchedule

        /// <summary>
        /// Deletes the Schedule object from the database
        /// </summary>
        /// <param name="toBeDeleted">Schedule to delete</param>
        void DeleteSchedule(OpSchedule toBeDeleted, bool useDBCollector = false);

        #endregion

        #region SaveSchedule

        /// <summary>
        /// Saves the Schedule object to the database
        /// </summary>
        /// <param name="toBeSaved">Schedule to save</param>
        /// <returns>Schedule Id</returns>
        int SaveSchedule(OpSchedule toBeSaved, bool useDBCollector = false);

        #endregion

        #region GetScheduleFilter
        /// <summary>
        /// Gets Schedule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSchedule funcion</param>
        /// <param name="IdSchedule">Specifies filter for ID_SCHEDULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IdScheduleType">Specifies filter for ID_SCHEDULE_TYPE column</param>
        /// <param name="ScheduleInterval">Specifies filter for SCHEDULE_INTERVAL column</param>
        /// <param name="SubdayType">Specifies filter for SUBDAY_TYPE column</param>
        /// <param name="SubdayInterval">Specifies filter for SUBDAY_INTERVAL column</param>
        /// <param name="RelativeInterval">Specifies filter for RELATIVE_INTERVAL column</param>
        /// <param name="RecurrenceFactor">Specifies filter for RECURRENCE_FACTOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="TimesInUtc">Specifies filter for TIMES_IN_UTC column</param>
        /// <param name="LastRunDate">Specifies filter for LAST_RUN_DATE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SCHEDULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Schedule list</returns>
        List<OpSchedule> GetScheduleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSchedule = null, string Name = null, string Description = null, int[] IdScheduleType = null, int[] ScheduleInterval = null, int[] SubdayType = null,
                           int[] SubdayInterval = null, int[] RelativeInterval = null, int[] RecurrenceFactor = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                           TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, bool? TimesInUtc = null, TypeDateTimeCode LastRunDate = null, bool? IsActive = null,
                               long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #endregion

        #region ServiceListDevice

        #region GetServiceDeviceDiagnosticActions

        DataTable GetServiceDeviceDiagnosticActions(long[] SerialNbr, int IdLanguage);

        #endregion

        #region GetServiceDeviceServiceActions

        DataTable GetServiceDeviceServiceActions(long[] SerialNbr, int IdLanguage);

        #endregion

        #endregion

        #region Tariff

        /// <summary>
        /// Gets Tariff by id
        /// </summary>
        /// <param name="Ids">Tariff Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Tariff list</returns>
        List<OpTariff> GetTariff(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);

        #endregion

        #region Task

        List<OpTaskStatistics> GetTaskStatistics(int[] IdOperatorPerformer, bool loadNavigationProperties = true);

        #endregion

        #region TaskData

        List<Tuple<OpTaskData, string>> GetTaskAssignedOperations(int[] IdTask, int IdLaguage);

        Dictionary<int, List<string>> GetTaskAssignedOperations2(int[] IdTask, int IdLaguage, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        #endregion

        #region WarrantyContract

        void SaveWarrantyContract(int idDeviceOrderNumber, int idContract, int idComponent, int months, int idWarrantyStartDateCalcMethod);

        void DeleteWarrantyContract(int idDeviceOrderNumber, int idContract, int idComponent);

        #endregion

        #region Work
        #region GetWork
        /// <summary>
        /// Gets Work by id
        /// </summary>
        /// <param name="Ids">Work Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Work list</returns>
        List<OpWork> GetWork(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true);
        #endregion
        #region SaveWork
        /// <summary>
        /// Saves the Work object to the database
        /// </summary>
        /// <param name="toBeSaved">Work to save</param>
        /// <returns>Work Id</returns>
        long SaveWork(OpWork toBeSaved);
        /// <summary>
        /// Saves the Work object to the database
        /// </summary>
        /// <param name="toBeSaved">Work to save</param>
        /// <param name="useDBCollector"></param>
        /// <returns>Work object</returns>
        OpWork SaveWork(OpWork toBeSaved, bool useDBCollector = false);
        /// <summary>
        /// Bulk Insert/Update Workobjects to the database
        /// </summary>
        /// <param name="toBeSaved">Work's to save</param>
        void SaveWork(OpWork[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region ChangeWorkStatus
        /// <summary>
        /// Bulk Update Status of Work objects in database
        /// </summary>
        /// <param name="toBeChanged">Work's to change</param>
        void ChangeWorkStatus(OpWork[] toBeChanged, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #endregion
        #region WorkData
        /// <summary>
        /// Indicates whether the WorkData is cached.
        /// </summary>
        bool WorkDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WorkData objects
        /// </summary>
        List<OpWorkData> GetAllWorkData();

        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Id">WorkData Id</param>
        /// <returns>WorkData object</returns>
        OpWorkData GetWorkData(long Id);

        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Ids">WorkData Ids</param>
        /// <returns>WorkData list</returns>
        List<OpWorkData> GetWorkData(long[] Ids);

        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Id">WorkData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkData object</returns>
        OpWorkData GetWorkData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Ids">WorkData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkData list</returns>
        List<OpWorkData> GetWorkData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the WorkData object to the database
        /// </summary>
        /// <param name="toBeSaved">WorkData to save</param>
        /// <returns>WorkData Id</returns>
        long SaveWorkData(OpWorkData toBeSaved, bool useDBCollector = false);
        /// <summary>
        /// Bulk Insert/Update WorkData objects to the database
        /// </summary>
        /// <param name="toBeSaved">WorkData's to save</param>
        void SaveWorkData(OpWorkData[] toBeSaved, int commandTimeout = 0, bool useDBCollector = false);

        /// <summary>
        /// Deletes the WorkData object from the database
        /// </summary>
        /// <param name="toBeDeleted">WorkData to delete</param>
        void DeleteWorkData(OpWorkData toBeDeleted, bool useDBCollector = false);

        /// <summary>
        /// Clears the WorkData object cache
        /// </summary>
        void ClearWorkDataCache();
        #endregion
        #region WorkHistory
        #region SaveWorkHistory
        /// <summary>
        /// Bulk Insert/Update WorkHistory objects to the database
        /// </summary>
        /// <param name="toBeSaved">WorkHistory's to save</param>
        void SaveWorkHistory(OpWorkHistory[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #endregion
        #region WorkHistoryData
        #region SaveWorkHistoryData
        /// <summary>
        /// Bulk Insert/Update WorkHistoryData objects to the database
        /// </summary>
        /// <param name="toBeSaved">WorkHistoryData's to save</param>
        void SaveWorkHistoryData(OpWorkHistoryData[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false);

        #endregion
        #endregion
        #region WorkType
        /// <summary>
        /// Gets all WorkType objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        List<OpWorkType> GetAllWorkType(bool useDBCollector = false, bool loadNavigationProperties = true, bool loadCustomData = true);
        #endregion

        #region View

        void ProceedViewOperation(List<OpViewOperation> ViewOperations);
        void ProceedViewChange(OpView view, Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>[] viewChanges);
        void ProceedViewCreation(OpView view);
        Tuple<long, List<Tuple<long, Dictionary<string, object>>>> GetFilterFlexView(int PageNbr, int PageSize, OpView View, string customSelectClause, string customWhereClause, string customOrderClause);

        #endregion

        #region AddActionWithActionDataListToQueue
        void AddActionWithActionDataListToQueue(long? IdAction, long IdActionType, int IdActionStatus, long? IdActionData,
            long? SerialNbr, long? IdMeter, long? IdLocation, int IdModule, long? IdOperator, string XmlActionData, string QueuePath, int? IdServer,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion

        #region CheckIfDataBaseExists
        bool? CheckIfDataBaseExists(string name);
        #endregion

        #region CheckIfTableExists
        bool CheckIfTableExists(string name);
        #endregion

        #region CheckIfLinkedServerExists
        bool CheckIfLinkedServerExists(string name);
        #endregion

        #region CheckIfStoredProcedureExists
        bool CheckIfStoredProcedureExists(string storedProcedure, Enums.DatabaseType databaseType, int? idServer, bool useDBCollector = false);
        #endregion

        #region ManageDistributorCORE
        void ManageDistributorCORE(OpDistributor toBeSaved);
        #endregion

        #region AddDevice
        void AddDevice(OpDevice toBeSaved, bool useDBCollector = false);
        #endregion

        #region AddDeviceType
        void AddDeviceType(OpDeviceType toBeSaved);
        #endregion

        #region DataTypeValue
        DB.InParameter DataTypeValue(OpDataTypeClass.Enum dataTypeClass, object Value, string ParamName);
        #endregion

        #region GetTriadaData
        System.Tuple<List<OpLocation>, List<OpMeter>, List<OpDevice>> GetTriadaData(List<OpLocation> locations, List<OpDataType> locationDataTypes, List<OpMeter> meters, List<OpDataType> meterDataTypes, List<OpDevice> devices, List<OpDataType> deviceDataTypes);
        System.Tuple<List<OpLocation>, List<OpMeter>, List<OpDevice>> GetTriadaData(List<OpLocation> locations, List<long> locationDataTypes, List<OpMeter> meters, List<long> meterDataTypes, List<OpDevice> devices, List<long> deviceDataTypes);
        #endregion
        #region GetAdditionalInfoForLocations
        void GetAdditionalInfoForLocations(List<OpLocation> locations);
        #endregion

        #region Area

        int AreaCount { get; }
        List<OpArea> Area { get; }

        List<OpArea> GetAllArea();

        void ClearAreaCache();
        #endregion

        #region SaveProtocolDriver
        /// <summary>
        /// Saves the ProtocolDriver object to the database
        /// </summary>
        /// <param name="toBeSaved">ProtocolDriver to save</param>
        /// <returns>ProtocolDriver Id</returns>
        int SaveProtocolDriver(OpProtocolDriver toBeSaved);
        #endregion

        #region SaveDeviceTypeGroup
        /// <summary>
        /// Saves the DeviceTypeGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeGroup to save</param>
        /// <returns>DeviceTypeGroup Id</returns>
        int SaveDeviceTypeGroup(OpDeviceTypeGroup toBeSaved);
        #endregion


        #region SaveActionDef
        /// <summary>
        /// Saves the ActionDef object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionDef to save</param>
        /// <returns>ActionDef Id</returns>
        long SaveActionDef(OpActionDef toBeSaved, bool useDBCollector = false);
        #endregion

        #region ScheduleAction

        /// <summary>
        /// Indicates whether the ScheduleAction is cached.
        /// </summary>
        bool ScheduleActionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ScheduleAction objects (cache disabled)
        /// </summary>
        List<OpScheduleAction> GetAllScheduleAction();


        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Id">ScheduleAction Id</param>
        /// <returns>ScheduleAction object</returns>
        OpScheduleAction GetScheduleAction(long Id);

        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Ids">ScheduleAction Ids</param>
        /// <returns>ScheduleAction list</returns>
        List<OpScheduleAction> GetScheduleAction(long[] Ids);

        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Id">ScheduleAction Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ScheduleAction object</returns>
        OpScheduleAction GetScheduleAction(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Ids">ScheduleAction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ScheduleAction list</returns>
        List<OpScheduleAction> GetScheduleAction(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ScheduleAction object to the database
        /// </summary>
        /// <param name="toBeSaved">ScheduleAction to save</param>
        /// <returns>ScheduleAction Id</returns>
        int SaveScheduleAction(OpScheduleAction toBeSaved, bool useDBCollector = false);

        /// <summary>
        /// Deletes the ScheduleAction object from the database
        /// </summary>
        /// <param name="toBeDeleted">ScheduleAction to delete</param>
        void DeleteScheduleAction(OpScheduleAction toBeDeleted, bool useDBCollector = false);

        /// <summary>
        /// Clears the ScheduleAction object cache
        /// </summary>
        void ClearScheduleActionCache();
        #endregion

        #region SimCard

        #region GetSimCardDataBySerialNbr

        List<OpDeviceSimCard> GetSimCardDataBySerialNbr(long[] SerialNbr, int[] IdDistributor, bool loadNavigationProperties = true);
        
        #endregion

        #region GetSimCardBySerialNbr

        List<OpDeviceSimCard> GetSimCardBySerialNbr(long[] SerialNbr, int[] IdDistributor, bool loadNavigationProperties = true);

        #endregion

        #region SaveSimCard

        void SaveSimCard(OpSimCard[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false);

        #endregion

        #region SaveSimCardData

        void SaveSimCardData(OpSimCardData[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false);

        #endregion

        #region SaveSimCardDetails

        void SaveSimCardDetails(int? changeFromLastMinutes, long[] idSimCard, long[] idDataType);
        
        #endregion

        #region DeleteSimCardDetails

        void DeleteSimCardDetails(int idSimCard);
        
        #endregion

        #endregion

        #region GetHierarchicalProfiles
        /// <summary>
        /// Gets all profiles for provided elements, excluding Distributor profiles
        /// </summary>
        /// <returns>Dictionary with IdProfile as keys and array of { SerialNbr, IdMeter, IdLocation, profile parent SerialNbr, profile parent IdMeter, profile parent IdLocation } as values</returns>
        Dictionary<int, List<long[]>> GetHierarchicalProfiles(IEnumerable<OpDevice> devices = null, IEnumerable<OpMeter> meters = null, IEnumerable<OpLocation> locations = null, IEnumerable<OpDistributor> distributors = null,
                                                                    int[] idProfile = null, long idDataType = IMR.Suite.Common.DataType.PROFILE_ID);
        /// <summary>
        /// Gets all profiles for provided elements, excluding Distributor profiles
        /// </summary>
        /// <returns>Dictionary with IdProfile as keys and array of { SerialNbr, IdMeter, IdLocation, profile parent SerialNbr, profile parent IdMeter, profile parent IdLocation } as values</returns>
        Dictionary<int, List<long[]>> GetHierarchicalProfiles(List<long> devices = null, List<long> meters = null, List<long> locations = null, List<int> distributors = null,
                                                                    int[] idProfile = null, long idDataType = IMR.Suite.Common.DataType.PROFILE_ID);
        #endregion

        #region GetShippingListDeviceWarranty

        List<OpShippingListDeviceWarrantyUser> GetShippingListDeviceWarrantyByDistributor(int idDistributor);

        #endregion

        #region GetDeviceShippingAndServiceHistory

        List<OpDeviceShippingAndServiceHistoryUser> GetDeviceShippingAndServiceHistory(long serialNbr);

        #endregion

        #region GetRouteDefByDistributor

        List<OpRouteDef> GetRouteDefByDistributor(int[] IdDistributor);

        #endregion

        // User Filter
        #region GetDeviceFilter
        /// <summary>
        /// Gets Device list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDevice funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="SerialNbrPattern">Specifies filter for SERIAL_NBR_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Device list</returns>
        List<OpDevice> GetDeviceFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdDeviceType = null, long[] SerialNbrPattern = null, long[] IdDescrPattern = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null,
                           int[] IdDeviceStateType = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);

        /*
         DataTable GetDeviceFilter(int[] IdDeviceType = null, int[] IdDistributor = null, int IdLanguage = 2,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {

            try
            {
                return dbConnectionCore.GetDeviceFilter(IdDeviceType, IdDistributor, IdLanguage, autoTransaction, transactionLevel, commandTimeout);

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceFilter", ex);
                throw;
            }

        }*/
        #endregion
        #region GetLocationFilter
        /// <summary>
        /// Gets Location list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocation funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdLocationType">Specifies filter for ID_LOCATION_TYPE column</param>
        /// <param name="IdLocationPattern">Specifies filter for ID_LOCATION_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="InKpi">Specifies filter for IN_KPI column</param>
        /// <param name="AllowGrouping">Specifies filter for ALLOW_GROUPING column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>Location list</returns>
        List<OpLocation> GetLocationFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdLocation = null, int[] IdLocationType = null, long[] IdLocationPattern = null, long[] IdDescrPattern = null, int[] IdDistributor = null, int[] IdConsumer = null,
                           int[] IdLocationStateType = null, bool? InKpi = null, bool? AllowGrouping = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                           bool useDBCollector = false);
        #endregion
        #region GetLocationEquipmentFilter
        /// <summary>
        /// Gets LocationEquipment list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationEquipment funcion</param>
        /// <param name="IdLocationEquipment">Specifies filter for ID_LOCATION_EQUIPMENT column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_EQUIPMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>LocationEquipment list</returns>
        List<OpLocationEquipment> GetLocationEquipmentFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdLocationEquipment = null, long[] IdLocation = null, long[] IdMeter = null, long[] SerialNbr = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                            bool useDBCollector = false);
        #endregion
        #region GetMeterFilter
        /// <summary>
        /// Gets Meter list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeter funcion</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdMeterPattern">Specifies filter for ID_METER_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Meter list</returns>
        List<OpMeter> GetMeterFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdMeter = null, int[] IdMeterType = null, long[] IdMeterPattern = null, long[] IdDescrPattern = null, int[] IdDistributor = null,
                                           long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetScheduleActionFilter

        /// <summary>
        /// Gets ScheduleAction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllScheduleAction funcion</param>
        /// <param name="IdSchedule">Specifies filter for ID_SCHEDULE column</param>
        /// <param name="IdActionDef">Specifies filter for ID_ACTION_DEF column</param>
        /// <param name="IsEnabled">Specifies filter for IS_ENABLED column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SCHEDULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ScheduleAction list</returns>
        List<OpScheduleAction> GetScheduleActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdScheduleAction = null, int[] IdSchedule = null, long[] IdActionDef = null, bool? IsEnabled = null, int[] IdOperator = null, int[] IdProfile = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false);
        #endregion
        #region GetRouteDefLocationDataFilter
        /// <summary>
        /// Gets RouteDefLocationData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefLocationData funcion (Cache disabled)</param>
        /// <param name="IdRouteDefLocation">Specifies filter for ID_ROUTE_DEF_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefLocationData list</returns>
        List<OpRouteDefLocationData> GetRouteDefLocationDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDefLocation = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetShippingListDeviceFilterCustom

        /// <summary>
        /// Gets ShippingListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListDevice funcion</param>
        /// <param name="IdShippingListDevice">Specifies filter for ID_SHIPPING_LIST_DEVICE column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_DEVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListDevice list</returns>
        List<OpShippingListDevice> GetShippingListDeviceFilterCustom(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingListDevice = null, int[] IdShippingList = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdArticle = null, TypeDateTimeCode InsertDate = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion
        #region GetServiceListDeviceFilterCustom

        /// <summary>
        /// Gets ServiceListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDevice funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="FailureDescription">Specifies filter for FAILURE_DESCRIPTION column</param>
        /// <param name="FailureSuggestion">Specifies filter for FAILURE_SUGGESTION column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="Photo">Specifies filter for PHOTO column</param>
        /// <param name="IdOperatorServicer">Specifies filter for ID_OPERATOR_SERVICER column</param>
        /// <param name="ServiceShortDescr">Specifies filter for SERVICE_SHORT_DESCR column</param>
        /// <param name="ServiceLongDescr">Specifies filter for SERVICE_LONG_DESCR column</param>
        /// <param name="PhotoServiced">Specifies filter for PHOTO_SERVICED column</param>
        /// <param name="AtexOk">Specifies filter for ATEX_OK column</param>
        /// <param name="PayForService">Specifies filter for PAY_FOR_SERVICE column</param>
        /// <param name="IdServiceStatus">Specifies filter for ID_SERVICE_STATUS column</param>
        /// <param name="IdDiagnosticStatus">Specifies filter for ID_DIAGNOSTIC_STATUS column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDevice list</returns>
        List<OpServiceListDevice> GetServiceListDeviceFilterCustom(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, long[] SerialNbr = null, string FailureDescription = null, string FailureSuggestion = null, string Notes = null, long[] Photo = null,
                           int[] IdOperatorServicer = null, string ServiceShortDescr = null, string ServiceLongDescr = null, long[] PhotoServiced = null, bool? AtexOk = null,
                           bool? PayForService = null, int[] IdServiceStatus = null, int[] IdDiagnosticStatus = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0);
        #endregion

        #region DevicePatternConfig
        /// <summary>
        /// Gets all DevicePatternConfig objects
        /// </summary>
        List<OpDevicePatternData> GetAllDevicePatternConfig();

        /// <summary>
        /// Gets DevicePatternConfig by id
        /// </summary>
        /// <param name="Ids">DevicePatternConfig Ids</param>
        /// <returns>DevicePatternConfig list</returns>
        List<OpDevicePatternData> GetDevicePatternConfig(long[] Ids);

        /// <summary>
        /// Saves the DevicePatternConfig object to the database
        /// </summary>
        /// <param name="toBeSaved">DevicePatternConfig to save</param>
        /// <returns>DevicePatternConfig Id</returns>
        void SaveDevicePatternConfig(OpDevicePatternData toBeSaved);

        /// <summary>
        /// Deletes the DevicePatternConfig object from the database
        /// </summary>
        /// <param name="toBeDeleted">DevicePatternConfig to delete</param>
        void DeleteDevicePatternConfig(OpDevicePatternData toBeDeleted);
        #endregion

        #region Testowe
        void GetStatusForLocations(List<OpLocation> locations);

        List<OpOperator> RPGetOperatorsSOT();
        #endregion

        #region GetShippingListDevices
        DataTable GetShippingListDevices(int idShippingList);
        #endregion
        #region GetDashboardData
        DataSet GetDashboardData(string Procedure, Enums.DashboardItemClass DashboardItemClass, long[] IdType, int IdOperator,
            Enums.Language Language = Enums.Language.Default,
            int[] IdDistributor = null, long[] IdLocation = null, long[] IdMeter = null, long[] SerialNbr = null,
            int CommandTimeout = 0);
        #endregion

        #region GetShippingListDeviceRadioDeviceTesterData
        DataTable GetShippingListDeviceRadioDeviceTesterData(int idShippingList);
        #endregion

        #region GetDeadlineDate

        DateTime? GetIssueDeadlineDate(int idIssue, DateTime? startDate = null);

        DateTime? GetTaskDeadlineDate(int idTask, DateTime? startDate = null);

        DateTime? GetDeadlineDate(DateTime? startDate = null, bool? calendarDays = null, int? days = null, string countryCode = null, int? idIssue = null, int? idTask = null);

        #endregion


        #region other

        #region CheckSimForDevicesForShipping
        int CheckSimForDevicesForShipping(int id);
        #endregion

        #region GetObjectIdServer
        int? GetObjectIdServer(long idObject, Enums.ReferenceType referenceType, bool useDBCollector = false);
        #endregion

        #endregion
#endif
    }
}
