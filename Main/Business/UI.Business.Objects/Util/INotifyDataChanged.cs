﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects
{
    #if !USE_UNITED_OBJECTS
    public enum NotifyType
    {
        Unknown = 0,
        Insert = 1,
        Update = 2,
        Delete = 3,
    }
    public interface INotifyDataChanged
    {
        event DataChangedEventHandler DataChanged;
    }

    public delegate void DataChangedEventHandler(object sender, DataChangedEventArgs e);

    public class DataChangedEventArgs : EventArgs
    {
        public NotifyType NotifyType { get; set; }
        public Type ChangedType { get; private set; }
        public object ChangedObject { get; private set; }

        public Guid? SourceGuid { get; private set; }

        public DataChangedEventArgs(object changedObject) :  this(changedObject, (changedObject != null ? changedObject.GetType() : null), NotifyType.Unknown, null)
        {
            
        }
        public DataChangedEventArgs(object changedObject, Type changedType, NotifyType notifyType, Guid? sourceGuid)
        {
            this.ChangedObject = changedObject;
            this.ChangedType = changedType;
            this.NotifyType = notifyType;
            this.SourceGuid = sourceGuid;
        }
    }
    #endif
}
