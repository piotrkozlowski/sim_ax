﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CORE = IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Data.DB;
using System.Data;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects
{
    public partial interface IDataProvider
    {
#if !USE_UNITED_OBJECTS
        #region Alarm

        /// <summary>
        /// Indicates whether the Alarm is cached.
        /// </summary>
        bool AlarmCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all Alarm objects
        /// </summary>
        List<OpAlarm> GetAllAlarmDW();


        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Id">Alarm Id</param>
        /// <returns>Alarm object</returns>
        OpAlarm GetAlarmDW(long Id);

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Ids">Alarm Ids</param>
        /// <returns>Alarm list</returns>
        List<OpAlarm> GetAlarmDW(long[] Ids);

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Id">Alarm Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Alarm object</returns>
        OpAlarm GetAlarmDW(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Ids">Alarm Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Alarm list</returns>
        List<OpAlarm> GetAlarmDW(long[] Ids, bool queryDatabase);

        long SaveAlarmDw(OpAlarm toBeSaved);

        void DeleteAlarmDw(OpAlarm toBeDeleted);

        /// <summary>
        /// Clears the Alarm object cache
        /// </summary>
        void ClearAlarmCacheDW();
        #endregion

        #region AlarmEventDW

        /// <summary>
        /// Indicates whether the AlarmEventDW is cached.
        /// </summary>
        bool AlarmEventCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all AlarmEventDW objects
        /// </summary>
        List<OpAlarmEvent> GetAllAlarmEventDW();


        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Id">AlarmEventDW Id</param>
        /// <returns>AlarmEventDW object</returns>
        OpAlarmEvent GetAlarmEventDW(long Id);

        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Ids">AlarmEventDW Ids</param>
        /// <returns>AlarmEventDW list</returns>
        List<OpAlarmEvent> GetAlarmEventDW(long[] Ids);

        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Id">AlarmEventDW Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmEventDW object</returns>
        OpAlarmEvent GetAlarmEventDW(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Ids">AlarmEventDW Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmEventDW list</returns>
        List<OpAlarmEvent> GetAlarmEventDW(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the AlarmEventDW object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmEventDW to save</param>
        /// <returns>AlarmEventDW Id</returns>
        long SaveAlarmEventDW(OpAlarmEvent toBeSaved);

        /// <summary>
        /// Deletes the AlarmEvent object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmEvent to delete</param>
        void DeleteAlarmEventDW(OpAlarmEvent toBeDeleted);

        /// <summary>
        /// Clears the AlarmEvent object cache
        /// </summary>
        void ClearAlarmEventCacheDW();
        #endregion

        #region AlarmHistory

        /// <summary>
        /// Indicates whether the AlarmHistory is cached.
        /// </summary>
        bool AlarmHistoryCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all AlarmHistory objects
        /// </summary>
        List<OpAlarmHistory> GetAllAlarmHistoryDW();


        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Id">AlarmHistory Id</param>
        /// <returns>AlarmHistory object</returns>
        OpAlarmHistory GetAlarmHistoryDW(long Id);

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Ids">AlarmHistory Ids</param>
        /// <returns>AlarmHistory list</returns>
        List<OpAlarmHistory> GetAlarmHistoryDW(long[] Ids);

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Id">AlarmHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmHistory object</returns>
        OpAlarmHistory GetAlarmHistoryDW(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Ids">AlarmHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmHistory list</returns>
        List<OpAlarmHistory> GetAlarmHistoryDW(long[] Ids, bool queryDatabase);
        /// <summary>
        /// Clears the AlarmHistory object cache
        /// </summary>
        void ClearAlarmHistoryCacheDW();
        #endregion

        #region AlarmMessage

        /// <summary>
        /// Indicates whether the AlarmMessage is cached.
        /// </summary>
        bool AlarmMessageCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all AlarmMessage objects
        /// </summary>
        List<OpAlarmMessage> GetAllAlarmMessageDW();


        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Id">AlarmMessage Id</param>
        /// <returns>AlarmMessage object</returns>
        OpAlarmMessage GetAlarmMessageDW(long Id);

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Ids">AlarmMessage Ids</param>
        /// <returns>AlarmMessage list</returns>
        List<OpAlarmMessage> GetAlarmMessageDW(long[] Ids);

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Id">AlarmMessage Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmMessage object</returns>
        OpAlarmMessage GetAlarmMessageDW(long Id, bool queryDatabase);

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Ids">AlarmMessage Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmMessage list</returns>
        List<OpAlarmMessage> GetAlarmMessageDW(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the AlarmMessage object cache
        /// </summary>
        void ClearAlarmMessageCacheDW();
        #endregion

        #region AuditDW

        /// <summary>
        /// Indicates whether the Audit is cached.
        /// </summary>
        bool AuditCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all Audit objects
        /// </summary>
        List<CORE.OpAudit> GetAllAuditDW();


        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <returns>Audit object</returns>
        CORE.OpAudit GetAuditDW(long Id);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <returns>Audit list</returns>
        List<CORE.OpAudit> GetAuditDW(long[] Ids);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit object</returns>
        CORE.OpAudit GetAuditDW(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit list</returns>
        List<CORE.OpAudit> GetAuditDW(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Audit object to the database
        /// </summary>
        /// <param name="toBeSaved">Audit to save</param>
        /// <returns>Audit Id</returns>
        long SaveAuditDW(CORE.OpAudit toBeSaved);

        /// <summary>
        /// Deletes the Audit object from the database
        /// </summary>
        /// <param name="toBeDeleted">Audit to delete</param>
        void DeleteAuditDW(CORE.OpAudit toBeDeleted);

        /// <summary>
        /// Clears the Audit object cache
        /// </summary>
        void ClearAuditCacheDW();
        #endregion

        #region ConsumerTransaction

        /// <summary>
        /// Indicates whether the ConsumerTransaction is cached.
        /// </summary>
        bool ConsumerTransactionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerTransaction objects
        /// </summary>
        List<OpConsumerTransaction> GetAllConsumerTransaction();


        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Id">ConsumerTransaction Id</param>
        /// <returns>ConsumerTransaction object</returns>
        OpConsumerTransaction GetConsumerTransaction(int Id);

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Ids">ConsumerTransaction Ids</param>
        /// <returns>ConsumerTransaction list</returns>
        List<OpConsumerTransaction> GetConsumerTransaction(int[] Ids);

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Id">ConsumerTransaction Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransaction object</returns>
        OpConsumerTransaction GetConsumerTransaction(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Ids">ConsumerTransaction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransaction list</returns>
        List<OpConsumerTransaction> GetConsumerTransaction(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerTransaction object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTransaction to save</param>
        /// <returns>ConsumerTransaction Id</returns>
        int SaveConsumerTransaction(OpConsumerTransaction toBeSaved);

        /// <summary>
        /// Deletes the ConsumerTransaction object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTransaction to delete</param>
        void DeleteConsumerTransaction(OpConsumerTransaction toBeDeleted);

        /// <summary>
        /// Clears the ConsumerTransaction object cache
        /// </summary>
        void ClearConsumerTransactionCache();
        #endregion

        #region ConsumerTransactionData

        /// <summary>
        /// Indicates whether the ConsumerTransactionData is cached.
        /// </summary>
        bool ConsumerTransactionDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerTransactionData objects
        /// </summary>
        List<OpConsumerTransactionData> GetAllConsumerTransactionData();


        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionData Id</param>
        /// <returns>ConsumerTransactionData object</returns>
        OpConsumerTransactionData GetConsumerTransactionData(long Id);

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionData Ids</param>
        /// <returns>ConsumerTransactionData list</returns>
        List<OpConsumerTransactionData> GetConsumerTransactionData(long[] Ids);

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionData object</returns>
        OpConsumerTransactionData GetConsumerTransactionData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionData list</returns>
        List<OpConsumerTransactionData> GetConsumerTransactionData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ConsumerTransactionData object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTransactionData to save</param>
        /// <returns>ConsumerTransactionData Id</returns>
        long SaveConsumerTransactionData(OpConsumerTransactionData toBeSaved);

        /// <summary>
        /// Deletes the ConsumerTransactionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTransactionData to delete</param>
        void DeleteConsumerTransactionData(OpConsumerTransactionData toBeDeleted);

        /// <summary>
        /// Clears the ConsumerTransactionData object cache
        /// </summary>
        void ClearConsumerTransactionDataCache();
        #endregion

        #region DataTemporal

        /// <summary>
        /// Indicates whether the DataTemporal is cached.
        /// </summary>
        bool DataTemporalCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataTemporal objects
        /// </summary>
        List<OpDataTemporal> GetAllDataTemporal();


        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Id">DataTemporal Id</param>
        /// <returns>DataTemporal object</returns>
        OpDataTemporal GetDataTemporal(long Id);

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Ids">DataTemporal Ids</param>
        /// <returns>DataTemporal list</returns>
        List<OpDataTemporal> GetDataTemporal(long[] Ids);

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Id">DataTemporal Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTemporal object</returns>
        OpDataTemporal GetDataTemporal(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Ids">DataTemporal Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTemporal list</returns>
        List<OpDataTemporal> GetDataTemporal(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataTemporal object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTemporal to save</param>
        /// <returns>DataTemporal Id</returns>
        long SaveDataTemporal(OpDataTemporal toBeSaved);

        /// <summary>
        /// Deletes the DataTemporal object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTemporal to delete</param>
        void DeleteDataTemporal(OpDataTemporal toBeDeleted);

        /// <summary>
        /// Clears the DataTemporal object cache
        /// </summary>
        void ClearDataTemporalCache();
        #endregion

        #region DeliveryAdvice

        /// <summary>
        /// Indicates whether the DeliveryAdvice is cached.
        /// </summary>
        bool DeliveryAdviceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryAdvice objects
        /// </summary>
        List<OpDeliveryAdvice> GetAllDeliveryAdvice();


        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Id">DeliveryAdvice Id</param>
        /// <returns>DeliveryAdvice object</returns>
        OpDeliveryAdvice GetDeliveryAdvice(int Id);

        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Ids">DeliveryAdvice Ids</param>
        /// <returns>DeliveryAdvice list</returns>
        List<OpDeliveryAdvice> GetDeliveryAdvice(int[] Ids);

        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Id">DeliveryAdvice Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryAdvice object</returns>
        OpDeliveryAdvice GetDeliveryAdvice(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Ids">DeliveryAdvice Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryAdvice list</returns>
        List<OpDeliveryAdvice> GetDeliveryAdvice(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeliveryAdvice object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryAdvice to save</param>
        /// <returns>DeliveryAdvice Id</returns>
        int SaveDeliveryAdvice(OpDeliveryAdvice toBeSaved);

        /// <summary>
        /// Deletes the DeliveryAdvice object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryAdvice to delete</param>
        void DeleteDeliveryAdvice(OpDeliveryAdvice toBeDeleted);

        /// <summary>
        /// Clears the DeliveryAdvice object cache
        /// </summary>
        void ClearDeliveryAdviceCache();
        #endregion

        #region DeliveryBatch

        /// <summary>
        /// Indicates whether the DeliveryBatch is cached.
        /// </summary>
        bool DeliveryBatchCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryBatch objects
        /// </summary>
        List<OpDeliveryBatch> GetAllDeliveryBatch();


        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Id">DeliveryBatch Id</param>
        /// <returns>DeliveryBatch object</returns>
        OpDeliveryBatch GetDeliveryBatch(int Id);

        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Ids">DeliveryBatch Ids</param>
        /// <returns>DeliveryBatch list</returns>
        List<OpDeliveryBatch> GetDeliveryBatch(int[] Ids);

        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Id">DeliveryBatch Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatch object</returns>
        OpDeliveryBatch GetDeliveryBatch(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Ids">DeliveryBatch Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatch list</returns>
        List<OpDeliveryBatch> GetDeliveryBatch(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeliveryBatch object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryBatch to save</param>
        /// <returns>DeliveryBatch Id</returns>
        int SaveDeliveryBatch(OpDeliveryBatch toBeSaved);

        /// <summary>
        /// Deletes the DeliveryBatch object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryBatch to delete</param>
        void DeleteDeliveryBatch(OpDeliveryBatch toBeDeleted);

        /// <summary>
        /// Clears the DeliveryBatch object cache
        /// </summary>
        void ClearDeliveryBatchCache();
        #endregion

        #region DeliveryBatchStatus

        /// <summary>
        /// Indicates whether the DeliveryBatchStatus is cached.
        /// </summary>
        bool DeliveryBatchStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryBatchStatus objects
        /// </summary>
        List<OpDeliveryBatchStatus> GetAllDeliveryBatchStatus();


        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Id">DeliveryBatchStatus Id</param>
        /// <returns>DeliveryBatchStatus object</returns>
        OpDeliveryBatchStatus GetDeliveryBatchStatus(int Id);

        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Ids">DeliveryBatchStatus Ids</param>
        /// <returns>DeliveryBatchStatus list</returns>
        List<OpDeliveryBatchStatus> GetDeliveryBatchStatus(int[] Ids);

        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Id">DeliveryBatchStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatchStatus object</returns>
        OpDeliveryBatchStatus GetDeliveryBatchStatus(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Ids">DeliveryBatchStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatchStatus list</returns>
        List<OpDeliveryBatchStatus> GetDeliveryBatchStatus(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeliveryBatchStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryBatchStatus to save</param>
        /// <returns>DeliveryBatchStatus Id</returns>
        int SaveDeliveryBatchStatus(OpDeliveryBatchStatus toBeSaved);

        /// <summary>
        /// Deletes the DeliveryBatchStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryBatchStatus to delete</param>
        void DeleteDeliveryBatchStatus(OpDeliveryBatchStatus toBeDeleted);

        /// <summary>
        /// Clears the DeliveryBatchStatus object cache
        /// </summary>
        void ClearDeliveryBatchStatusCache();
        #endregion

        #region DeliveryOrder

        /// <summary>
        /// Indicates whether the DeliveryOrder is cached.
        /// </summary>
        bool DeliveryOrderCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryOrder objects
        /// </summary>
        List<OpDeliveryOrder> GetAllDeliveryOrder();


        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Id">DeliveryOrder Id</param>
        /// <returns>DeliveryOrder object</returns>
        OpDeliveryOrder GetDeliveryOrder(int Id);

        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Ids">DeliveryOrder Ids</param>
        /// <returns>DeliveryOrder list</returns>
        List<OpDeliveryOrder> GetDeliveryOrder(int[] Ids);

        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Id">DeliveryOrder Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrder object</returns>
        OpDeliveryOrder GetDeliveryOrder(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Ids">DeliveryOrder Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrder list</returns>
        List<OpDeliveryOrder> GetDeliveryOrder(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeliveryOrder object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryOrder to save</param>
        /// <returns>DeliveryOrder Id</returns>
        int SaveDeliveryOrder(OpDeliveryOrder toBeSaved);

        /// <summary>
        /// Deletes the DeliveryOrder object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryOrder to delete</param>
        void DeleteDeliveryOrder(OpDeliveryOrder toBeDeleted);

        /// <summary>
        /// Clears the DeliveryOrder object cache
        /// </summary>
        void ClearDeliveryOrderCache();
        #endregion

        #region DeliveryOrderHistory

        /// <summary>
        /// Indicates whether the DeliveryOrderHistory is cached.
        /// </summary>
        bool DeliveryOrderHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryOrderHistory objects
        /// </summary>
        List<OpDeliveryOrderHistory> GetAllDeliveryOrderHistory();


        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Id">DeliveryOrderHistory Id</param>
        /// <returns>DeliveryOrderHistory object</returns>
        OpDeliveryOrderHistory GetDeliveryOrderHistory(long Id);

        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Ids">DeliveryOrderHistory Ids</param>
        /// <returns>DeliveryOrderHistory list</returns>
        List<OpDeliveryOrderHistory> GetDeliveryOrderHistory(long[] Ids);

        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Id">DeliveryOrderHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrderHistory object</returns>
        OpDeliveryOrderHistory GetDeliveryOrderHistory(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Ids">DeliveryOrderHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrderHistory list</returns>
        List<OpDeliveryOrderHistory> GetDeliveryOrderHistory(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeliveryOrderHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryOrderHistory to save</param>
        /// <returns>DeliveryOrderHistory Id</returns>
        long SaveDeliveryOrderHistory(OpDeliveryOrderHistory toBeSaved);

        /// <summary>
        /// Deletes the DeliveryOrderHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryOrderHistory to delete</param>
        void DeleteDeliveryOrderHistory(OpDeliveryOrderHistory toBeDeleted);

        /// <summary>
        /// Clears the DeliveryOrderHistory object cache
        /// </summary>
        void ClearDeliveryOrderHistoryCache();
        #endregion

        #region DeviceConnection

        /// <summary>
        /// Indicates whether the DeviceConnection is cached.
        /// </summary>
        bool DeviceConnectionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceConnection objects
        /// </summary>
        List<OpDeviceConnection> DeviceConnection { get; }

        /// <summary>
        /// Gets all DeviceConnection objects
        /// </summary>
        List<OpDeviceConnection> GetAllDeviceConnection();


        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Id">DeviceConnection Id</param>
        /// <returns>DeviceConnection object</returns>
        OpDeviceConnection GetDeviceConnection(long Id);

        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Ids">DeviceConnection Ids</param>
        /// <returns>DeviceConnection list</returns>
        List<OpDeviceConnection> GetDeviceConnection(long[] Ids);

        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Id">DeviceConnection Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceConnection object</returns>
        OpDeviceConnection GetDeviceConnection(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Ids">DeviceConnection Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceConnection list</returns>
        List<OpDeviceConnection> GetDeviceConnection(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DeviceConnection object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceConnection to save</param>
        /// <returns>DeviceConnection Id</returns>
        long SaveDeviceConnection(OpDeviceConnection toBeSaved);

        /// <summary>
        /// Deletes the DeviceConnection object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceConnection to delete</param>
        void DeleteDeviceConnection(OpDeviceConnection toBeDeleted);

        /// <summary>
        /// Clears the DeviceConnection object cache
        /// </summary>
        void ClearDeviceConnectionCache();
        #endregion

        #region DeviceSchedule

        /// <summary>
        /// Indicates whether the DeviceSchedule is cached.
        /// </summary>
        bool DeviceScheduleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceSchedule objects
        /// </summary>
        List<OpDeviceSchedule> GetAllDeviceSchedule();


        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Id">DeviceSchedule Id</param>
        /// <returns>DeviceSchedule object</returns>
        OpDeviceSchedule GetDeviceSchedule(int Id);

        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Ids">DeviceSchedule Ids</param>
        /// <returns>DeviceSchedule list</returns>
        List<OpDeviceSchedule> GetDeviceSchedule(int[] Ids);

        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Id">DeviceSchedule Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceSchedule object</returns>
        OpDeviceSchedule GetDeviceSchedule(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Ids">DeviceSchedule Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceSchedule list</returns>
        List<OpDeviceSchedule> GetDeviceSchedule(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DeviceSchedule object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceSchedule to save</param>
        /// <returns>DeviceSchedule Id</returns>
        int SaveDeviceSchedule(OpDeviceSchedule toBeSaved);

        /// <summary>
        /// Deletes the DeviceSchedule object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceSchedule to delete</param>
        void DeleteDeviceSchedule(OpDeviceSchedule toBeDeleted);

        /// <summary>
        /// Clears the DeviceSchedule object cache
        /// </summary>
        void ClearDeviceScheduleCache();
        #endregion

        #region DistributorPerformance

        /// <summary>
        /// Indicates whether the DistributorPerformance is cached.
        /// </summary>
        bool DistributorPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DistributorPerformance objects
        /// </summary>
        List<OpDistributorPerformance> GetAllDistributorPerformance();


        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Id">DistributorPerformance Id</param>
        /// <returns>DistributorPerformance object</returns>
        OpDistributorPerformance GetDistributorPerformance(long Id);

        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Ids">DistributorPerformance Ids</param>
        /// <returns>DistributorPerformance list</returns>
        List<OpDistributorPerformance> GetDistributorPerformance(long[] Ids);

        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Id">DistributorPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorPerformance object</returns>
        OpDistributorPerformance GetDistributorPerformance(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Ids">DistributorPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorPerformance list</returns>
        List<OpDistributorPerformance> GetDistributorPerformance(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DistributorPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">DistributorPerformance to save</param>
        /// <returns>DistributorPerformance Id</returns>
        long SaveDistributorPerformance(OpDistributorPerformance toBeSaved);

        /// <summary>
        /// Deletes the DistributorPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">DistributorPerformance to delete</param>
        void DeleteDistributorPerformance(OpDistributorPerformance toBeDeleted);

        /// <summary>
        /// Clears the DistributorPerformance object cache
        /// </summary>
        void ClearDistributorPerformanceCache();
        #endregion

        #region EtlDW

        ///// <summary>
        ///// Indicates whether the Etl is cached.
        ///// </summary>
        //bool EtlCacheEnabledDW { get; set; }


        ///// <summary>
        ///// Gets all Etl objects
        ///// </summary>
        //List<DW.OpEtl> GetAllEtlDW();


        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Id">Etl Id</param>
        ///// <returns>Etl object</returns>
        //DW.OpEtl GetEtlDW(int Id);

        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Ids">Etl Ids</param>
        ///// <returns>Etl list</returns>
        //List<DW.OpEtl> GetEtlDW(int[] Ids);

        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Id">Etl Id</param>
        ///// <param name="queryDatabase">If True, gets object from database</param>
        ///// <returns>Etl object</returns>
        //DW.OpEtl GetEtlDW(int Id, bool queryDatabase);

        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Ids">Etl Ids</param>
        ///// <param name="queryDatabase">If True, gets object from database</param>
        ///// <returns>Etl list</returns>
        //List<DW.OpEtl> GetEtlDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        ///// <summary>
        ///// Saves the Etl object to the database
        ///// </summary>
        ///// <param name="toBeSaved">Etl to save</param>
        ///// <returns>Etl Id</returns>
        //int SaveEtlDW(DW.OpEtl toBeSaved);

        ///// <summary>
        ///// Deletes the Etl object from the database
        ///// </summary>
        ///// <param name="toBeDeleted">Etl to delete</param>
        //void DeleteEtlDW(DW.OpEtl toBeDeleted);

        ///// <summary>
        ///// Clears the Etl object cache
        ///// </summary>
        //void ClearEtlCacheDW();


        /// <summary>
        /// Indicates whether the Etl is cached.
        /// </summary>
        bool EtlCacheEnabledDW { get; set; }


        /// <summary>
        /// Gets all Etl objects
        /// </summary>
        List<CORE.OpEtl> GetAllEtlDW();


        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Id">Etl Id</param>
        /// <returns>Etl object</returns>
        CORE.OpEtl GetEtlDW(int Id);

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Ids">Etl Ids</param>
        /// <returns>Etl list</returns>
        List<CORE.OpEtl> GetEtlDW(int[] Ids);

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Id">Etl Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Etl object</returns>
        CORE.OpEtl GetEtlDW(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Ids">Etl Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Etl list</returns>
        List<CORE.OpEtl> GetEtlDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Etl object to the database
        /// </summary>
        /// <param name="toBeSaved">Etl to save</param>
        /// <returns>Etl Id</returns>
        int SaveEtlDW(CORE.OpEtl toBeSaved);

        /// <summary>
        /// Deletes the Etl object from the database
        /// </summary>
        /// <param name="toBeDeleted">Etl to delete</param>
        void DeleteEtlDW(CORE.OpEtl toBeDeleted);

        /// <summary>
        /// Clears the Etl object cache
        /// </summary>
        void ClearEtlCacheDW();
        #endregion

        #region EtlPerformance
       
        /// <summary>
        /// Indicates whether the EtlPerformance is cached.
        /// </summary>
        bool EtlPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all EtlPerformance objects
        /// </summary>
        List<OpEtlPerformance> GetAllEtlPerformance();


        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Id">EtlPerformance Id</param>
        /// <returns>EtlPerformance object</returns>
        OpEtlPerformance GetEtlPerformance(long Id);

        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Ids">EtlPerformance Ids</param>
        /// <returns>EtlPerformance list</returns>
        List<OpEtlPerformance> GetEtlPerformance(long[] Ids);

        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Id">EtlPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>EtlPerformance object</returns>
        OpEtlPerformance GetEtlPerformance(long Id, bool queryDatabase);

        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Ids">EtlPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>EtlPerformance list</returns>
        List<OpEtlPerformance> GetEtlPerformance(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the EtlPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">EtlPerformance to save</param>
        /// <returns>EtlPerformance Id</returns>
        long SaveEtlPerformance(OpEtlPerformance toBeSaved);

        /// <summary>
        /// Deletes the EtlPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">EtlPerformance to delete</param>
        void DeleteEtlPerformance(OpEtlPerformance toBeDeleted);

        /// <summary>
        /// Clears the EtlPerformance object cache
        /// </summary>
        void ClearEtlPerformanceCache();
        #endregion

        #region ImrServerPerformance
        /// <summary>
        /// Indicates whether the ImrServerPerformance is cached.
        /// </summary>
        bool ImrServerPerformanceCacheEnabled { get; set; }


        /// <summary>
        /// Gets all ImrServerPerformance objects
        /// </summary>
        List<OpImrServerPerformance> GetAllImrServerPerformance();


        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Id">ImrServerPerformance Id</param>
        /// <returns>ImrServerPerformance object</returns>
        OpImrServerPerformance GetImrServerPerformance(long Id);

        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Ids">ImrServerPerformance Ids</param>
        /// <returns>ImrServerPerformance list</returns>
        List<OpImrServerPerformance> GetImrServerPerformance(long[] Ids);

        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Id">ImrServerPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServerPerformance object</returns>
        OpImrServerPerformance GetImrServerPerformance(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Ids">ImrServerPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServerPerformance list</returns>
        List<OpImrServerPerformance> GetImrServerPerformance(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ImrServerPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">ImrServerPerformance to save</param>
        /// <returns>ImrServerPerformance Id</returns>
        long SaveImrServerPerformance(OpImrServerPerformance toBeSaved);

        /// <summary>
        /// Deletes the ImrServerPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">ImrServerPerformance to delete</param>
        void DeleteImrServerPerformance(OpImrServerPerformance toBeDeleted);

        /// <summary>
        /// Clears the ImrServerPerformance object cache
        /// </summary>
        void ClearImrServerPerformanceCache();
        #endregion

        #region LocationKpi

        /// <summary>
        /// Indicates whether the LocationKpi is cached.
        /// </summary>
        bool LocationKpiCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationKpi objects
        /// </summary>
        List<OpLocationKpi> GetAllLocationKpi();


        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Id">LocationKpi Id</param>
        /// <returns>LocationKpi object</returns>
        OpLocationKpi GetLocationKpi(long Id);

        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Ids">LocationKpi Ids</param>
        /// <returns>Refuel list</returns>
        List<OpLocationKpi> GetLocationKpi(long[] Ids);

        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Id">LocationKpi Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationKpi object</returns>
        OpLocationKpi GetLocationKpi(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Ids">LocationKpi Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationKpi list</returns>
        List<OpLocationKpi> GetLocationKpi(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationKpi object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationKpi to save</param>
        /// <returns>LocationKpi Id</returns>
        long SaveLocationKpi(OpLocationKpi toBeSaved);

        /// <summary>
        /// Deletes the LocationKpi object from the database
        /// </summary>
        /// <param name="toBeDeleted">Refuel to delete</param>
        void DeleteLocationKpi(OpLocationKpi toBeDeleted);

        /// <summary>
        /// Clears the LocationKpi object cache
        /// </summary>
        void ClearLocationKpi();
        #endregion

        #region LocationDistance

        /// <summary>
        /// Gets all LocationDistance objects
        /// </summary>
        List<OpLocationDistance> GetAllLocationDistance();


        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Id">LocationDistance Id</param>
        /// <returns>LocationDistance object</returns>
        OpLocationDistance GetLocationDistance(long Id);

        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Ids">LocationDistance Ids</param>
        /// <returns>LocationDistance list</returns>
        List<OpLocationDistance> GetLocationDistance(long[] Ids);

        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Id">LocationDistance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationDistance object</returns>
        OpLocationDistance GetLocationDistance(long Id, bool queryDatabase);

        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Ids">LocationDistance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationDistance list</returns>
        List<OpLocationDistance> GetLocationDistance(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the LocationDistance object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationDistance to save</param>
        /// <returns>LocationDistance Id</returns>
        long SaveLocationDistance(OpLocationDistance toBeSaved);

        /// <summary>
        /// Deletes the LocationDistance object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationDistance to delete</param>
        void DeleteLocationDistance(OpLocationDistance toBeDeleted);

        /// <summary>
        /// Clears the LocationDistance object cache
        /// </summary>
        void ClearLocationDistanceCache();
        #endregion

        #region MappingDW

        /// <summary>
        /// Indicates whether the Mapping is cached.
        /// </summary>
        bool MappingCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all Mapping objects
        /// </summary>
        List<OpMapping> GetAllMappingDW();


        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Id">Mapping Id</param>
        /// <returns>Mapping object</returns>
        OpMapping GetMappingDW(long Id);

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Ids">Mapping Ids</param>
        /// <returns>Mapping list</returns>
        List<OpMapping> GetMappingDW(long[] Ids);

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Id">Mapping Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Mapping object</returns>
        OpMapping GetMappingDW(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Ids">Mapping Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Mapping list</returns>
        List<OpMapping> GetMappingDW(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the Mapping object to the database
        /// </summary>
        /// <param name="toBeSaved">Mapping to save</param>
        /// <returns>Mapping Id</returns>
        long SaveMappingDW(OpMapping toBeSaved);

        /// <summary>
        /// Deletes the Mapping object from the database
        /// </summary>
        /// <param name="toBeDeleted">Mapping to delete</param>
        void DeleteMappingDW(OpMapping toBeDeleted);

        /// <summary>
        /// Clears the Mapping object cache
        /// </summary>
        void ClearMappingCacheDW();
        #endregion

        #region ModuleHistory

        /// <summary>
        /// Indicates whether the ModuleHistory is cached.
        /// </summary>
        bool ModuleHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ModuleHistory objects
        /// </summary>
        List<OpModuleHistory> GetAllModuleHistory();


        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Id">ModuleHistory Id</param>
        /// <returns>ModuleHistory object</returns>
        OpModuleHistory GetModuleHistory(int Id);

        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Ids">ModuleHistory Ids</param>
        /// <returns>ModuleHistory list</returns>
        List<OpModuleHistory> GetModuleHistory(int[] Ids);

        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Id">ModuleHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ModuleHistory object</returns>
        OpModuleHistory GetModuleHistory(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Ids">ModuleHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ModuleHistory list</returns>
        List<OpModuleHistory> GetModuleHistory(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ModuleHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">ModuleHistory to save</param>
        /// <returns>ModuleHistory Id</returns>
        int SaveModuleHistory(OpModuleHistory toBeSaved);

        /// <summary>
        /// Deletes the ModuleHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">ModuleHistory to delete</param>
        void DeleteModuleHistory(OpModuleHistory toBeDeleted);

        /// <summary>
        /// Clears the ModuleHistory object cache
        /// </summary>
        void ClearModuleHistoryCache();
        #endregion

        #region MonitoringWatcherDW

        /// <summary>
        /// Indicates whether the MonitoringWatcher is cached.
        /// </summary>
        bool MonitoringWatcherCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcher objects
        /// </summary>
        List<OpMonitoringWatcher> GetAllMonitoringWatcherDW();


        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Id">MonitoringWatcher Id</param>
        /// <returns>MonitoringWatcher object</returns>
        OpMonitoringWatcher GetMonitoringWatcherDW(int Id);

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcher Ids</param>
        /// <returns>MonitoringWatcher list</returns>
        List<OpMonitoringWatcher> GetMonitoringWatcherDW(int[] Ids);

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Id">MonitoringWatcher Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcher object</returns>
        OpMonitoringWatcher GetMonitoringWatcherDW(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcher Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcher list</returns>
        List<OpMonitoringWatcher> GetMonitoringWatcherDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the MonitoringWatcher object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcher to save</param>
        /// <returns>MonitoringWatcher Id</returns>
        int SaveMonitoringWatcherDW(OpMonitoringWatcher toBeSaved);

        /// <summary>
        /// Deletes the MonitoringWatcher object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcher to delete</param>
        void DeleteMonitoringWatcherDW(OpMonitoringWatcher toBeDeleted);

        /// <summary>
        /// Clears the MonitoringWatcher object cache
        /// </summary>
        void ClearMonitoringWatcherCacheDW();
        #endregion

        #region MonitoringWatcherDataDW

        /// <summary>
        /// Indicates whether the MonitoringWatcherData is cached.
        /// </summary>
        bool MonitoringWatcherDataCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcherData objects
        /// </summary>
        List<OpMonitoringWatcherData> GetAllMonitoringWatcherDataDW();


        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherData Id</param>
        /// <returns>MonitoringWatcherData object</returns>
        OpMonitoringWatcherData GetMonitoringWatcherDataDW(int Id);

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherData Ids</param>
        /// <returns>MonitoringWatcherData list</returns>
        List<OpMonitoringWatcherData> GetMonitoringWatcherDataDW(int[] Ids);

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherData object</returns>
        OpMonitoringWatcherData GetMonitoringWatcherDataDW(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherData list</returns>
        List<OpMonitoringWatcherData> GetMonitoringWatcherDataDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the MonitoringWatcherData object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcherData to save</param>
        /// <returns>MonitoringWatcherData Id</returns>
        int SaveMonitoringWatcherDataDW(OpMonitoringWatcherData toBeSaved);

        /// <summary>
        /// Deletes the MonitoringWatcherData object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcherData to delete</param>
        void DeleteMonitoringWatcherDataDW(OpMonitoringWatcherData toBeDeleted);

        /// <summary>
        /// Clears the MonitoringWatcherData object cache
        /// </summary>
        void ClearMonitoringWatcherDataCacheDW();
        #endregion

        #region MonitoringWatcherTypeDW

        /// <summary>
        /// Indicates whether the MonitoringWatcherType is cached.
        /// </summary>
        bool MonitoringWatcherTypeCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcherType objects
        /// </summary>
        List<OpMonitoringWatcherType> GetAllMonitoringWatcherTypeDW();


        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherType Id</param>
        /// <returns>MonitoringWatcherType object</returns>
        OpMonitoringWatcherType GetMonitoringWatcherTypeDW(int Id);

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherType Ids</param>
        /// <returns>MonitoringWatcherType list</returns>
        List<OpMonitoringWatcherType> GetMonitoringWatcherTypeDW(int[] Ids);

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherType object</returns>
        OpMonitoringWatcherType GetMonitoringWatcherTypeDW(int Id, bool queryDatabase);

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherType list</returns>
        List<OpMonitoringWatcherType> GetMonitoringWatcherTypeDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the MonitoringWatcherType object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcherType to save</param>
        /// <returns>MonitoringWatcherType Id</returns>
        int SaveMonitoringWatcherTypeDW(OpMonitoringWatcherType toBeSaved);

        /// <summary>
        /// Deletes the MonitoringWatcherType object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcherType to delete</param>
        void DeleteMonitoringWatcherTypeDW(OpMonitoringWatcherType toBeDeleted);

        /// <summary>
        /// Clears the MonitoringWatcherType object cache
        /// </summary>
        void ClearMonitoringWatcherTypeCacheDW();
        #endregion

        #region ProblemClass
        /// <summary>
        /// Indicates whether the ProblemClass is cached.
        /// </summary>
        bool ProblemClassCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ProblemClass objects
        /// </summary>
        List<OpProblemClass> GetAllProblemClass();


        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Id">ProblemClass Id</param>
        /// <returns>ProblemClass object</returns>
        OpProblemClass GetProblemClass(int Id);

        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Ids">ProblemClass Ids</param>
        /// <returns>ProblemClass list</returns>
        List<OpProblemClass> GetProblemClass(int[] Ids);

        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Id">ProblemClass Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProblemClass object</returns>
        OpProblemClass GetProblemClass(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Ids">ProblemClass Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProblemClass list</returns>
        List<OpProblemClass> GetProblemClass(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ProblemClass object to the database
        /// </summary>
        /// <param name="toBeSaved">ProblemClass to save</param>
        /// <returns>ProblemClass Id</returns>
        int SaveProblemClass(OpProblemClass toBeSaved);

        /// <summary>
        /// Deletes the ProblemClass object from the database
        /// </summary>
        /// <param name="toBeDeleted">ProblemClass to delete</param>
        void DeleteProblemClass(OpProblemClass toBeDeleted);

        /// <summary>
        /// Clears the ProblemClass object cache
        /// </summary>
        void ClearProblemClassCache();
        #endregion

        #region Refuel
        /// <summary>
        /// Indicates whether the Refuel is cached.
        /// </summary>
        bool RefuelCacheEnabled { get; set; }


        /// <summary>
        /// Gets all Refuel objects
        /// </summary>
        List<OpRefuel> GetAllRefuel();


        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Id">Refuel Id</param>
        /// <returns>Refuel object</returns>
        OpRefuel GetRefuel(long Id);

        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Ids">Refuel Ids</param>
        /// <returns>Refuel list</returns>
        List<OpRefuel> GetRefuel(long[] Ids);

        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Id">Refuel Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Refuel object</returns>
        OpRefuel GetRefuel(long Id, bool queryDatabase);

        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Ids">Refuel Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Refuel list</returns>
        List<OpRefuel> GetRefuel(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the Refuel object to the database
        /// </summary>
        /// <param name="toBeSaved">Refuel to save</param>
        /// <returns>Refuel Id</returns>
        long SaveRefuel(OpRefuel toBeSaved);

        /// <summary>
        /// Deletes the Refuel object from the database
        /// </summary>
        /// <param name="toBeDeleted">Refuel to delete</param>
        void DeleteRefuel(OpRefuel toBeDeleted);

        /// <summary>
        /// Clears the Refuel object cache
        /// </summary>
        void ClearRefuelCache();
        #endregion

        #region RefuelData

        /// <summary>
        /// Indicates whether the RefuelData is cached.
        /// </summary>
        bool RefuelDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RefuelData objects
        /// </summary>
        List<OpRefuelData> GetAllRefuelData();


        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Id">RefuelData Id</param>
        /// <returns>RefuelData object</returns>
        OpRefuelData GetRefuelData(long Id);

        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Ids">RefuelData Ids</param>
        /// <returns>RefuelData list</returns>
        List<OpRefuelData> GetRefuelData(long[] Ids);

        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Id">RefuelData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RefuelData object</returns>
        OpRefuelData GetRefuelData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Ids">RefuelData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RefuelData list</returns>
        List<OpRefuelData> GetRefuelData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the RefuelData object to the database
        /// </summary>
        /// <param name="toBeSaved">RefuelData to save</param>
        /// <returns>RefuelData Id</returns>
        long SaveRefuelData(OpRefuelData toBeSaved);

        /// <summary>
        /// Deletes the RefuelData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RefuelData to delete</param>
        void DeleteRefuelData(OpRefuelData toBeDeleted);

        /// <summary>
        /// Clears the RefuelData object cache
        /// </summary>
        void ClearRefuelDataCache();
        #endregion

        #region Report

        /// <summary>
        /// Indicates whether the Report is cached.
        /// </summary>
        bool ReportCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Report objects
        /// </summary>
        List<OpReport> GetAllReport(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Id">Report Id</param>
        /// <returns>Report object</returns>
        OpReport GetReport(int Id);

        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Ids">Report Ids</param>
        /// <returns>Report list</returns>
        List<OpReport> GetReport(int[] Ids);

        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Id">Report Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Report object</returns>
        OpReport GetReport(int Id, bool queryDatabase);

        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Ids">Report Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Report list</returns>
        List<OpReport> GetReport(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Clears the Report object cache
        /// </summary>
        void ClearReportCache();
        #endregion

        #region ReportData

        /// <summary>
        /// Indicates whether the ReportData is cached.
        /// </summary>
        bool ReportDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportData objects
        /// </summary>
        List<OpReportData> GetAllReportData();


        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Id">ReportData Id</param>
        /// <returns>ReportData object</returns>
        OpReportData GetReportData(long Id);

        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Ids">ReportData Ids</param>
        /// <returns>ReportData list</returns>
        List<OpReportData> GetReportData(long[] Ids);

        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Id">ReportData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportData object</returns>
        OpReportData GetReportData(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Ids">ReportData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportData list</returns>
        List<OpReportData> GetReportData(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Clears the ReportData object cache
        /// </summary>
        void ClearReportDataCache();
        #endregion

        #region ReportParam

        /// <summary>
        /// Indicates whether the ReportParam is cached.
        /// </summary>
        bool ReportParamCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportParam objects
        /// </summary>
        List<OpReportParam> GetAllReportParam();


        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Id">ReportParam Id</param>
        /// <returns>ReportParam object</returns>
        OpReportParam GetReportParam(int Id);

        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Ids">ReportParam Ids</param>
        /// <returns>ReportParam list</returns>
        List<OpReportParam> GetReportParam(int[] Ids);

        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Id">ReportParam Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportParam object</returns>
        OpReportParam GetReportParam(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Ids">ReportParam Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportParam list</returns>
        List<OpReportParam> GetReportParam(int[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ReportParam object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportParam to save</param>
        /// <returns>ReportParam Id</returns>
        int SaveReportParam(OpReportParam toBeSaved);

        /// <summary>
        /// Deletes the ReportParam object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportParam to delete</param>
        void DeleteReportParam(OpReportParam toBeDeleted);

        /// <summary>
        /// Clears the ReportParam object cache
        /// </summary>
        void ClearReportParamCache();
        #endregion

        #region ReportPerformance

        /// <summary>
        /// Indicates whether the ReportPerformance is cached.
        /// </summary>
        bool ReportPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportPerformance objects
        /// </summary>
        List<OpReportPerformance> GetAllReportPerformance();


        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Id">ReportPerformance Id</param>
        /// <returns>ReportPerformance object</returns>
        OpReportPerformance GetReportPerformance(long Id);

        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Ids">ReportPerformance Ids</param>
        /// <returns>ReportPerformance list</returns>
        List<OpReportPerformance> GetReportPerformance(long[] Ids);

        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Id">ReportPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportPerformance object</returns>
        OpReportPerformance GetReportPerformance(long Id, bool queryDatabase);

        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Ids">ReportPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportPerformance list</returns>
        List<OpReportPerformance> GetReportPerformance(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the ReportPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportPerformance to save</param>
        /// <returns>ReportPerformance Id</returns>
        long SaveReportPerformance(OpReportPerformance toBeSaved);

        /// <summary>
        /// Deletes the ReportPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportPerformance to delete</param>
        void DeleteReportPerformance(OpReportPerformance toBeDeleted);

        /// <summary>
        /// Clears the ReportPerformance object cache
        /// </summary>
        void ClearReportPerformanceCache();
        #endregion

        #region ReportType

        /// <summary>
        /// Indicates whether the ReportType is cached.
        /// </summary>
        bool ReportTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportType objects
        /// </summary>
        List<OpReportType> GetAllReportType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Id">ReportType Id</param>
        /// <returns>ReportType object</returns>
        OpReportType GetReportType(int Id);

        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Ids">ReportType Ids</param>
        /// <returns>ReportType list</returns>
        List<OpReportType> GetReportType(int[] Ids);

        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Id">ReportType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportType object</returns>
        OpReportType GetReportType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Ids">ReportType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportType list</returns>
        List<OpReportType> GetReportType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the ReportType object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportType to save</param>
        /// <returns>ReportType Id</returns>
        int SaveReportType(OpReportType toBeSaved);

        /// <summary>
        /// Deletes the ReportType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportType to delete</param>
        void DeleteReportType(OpReportType toBeDeleted);

        /// <summary>
        /// Clears the ReportType object cache
        /// </summary>
        void ClearReportTypeCache();
        #endregion

        #region DataArch

        /// <summary>
        /// Indicates whether the DataArch is cached.
        /// </summary>
        bool DataArchCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataArch objects
        /// </summary>
        List<OpDataArch> GetAllDataArch();


        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Id">DataArch Id</param>
        /// <returns>DataArch object</returns>
        OpDataArch GetDataArch(long Id);

        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Ids">DataArch Ids</param>
        /// <returns>DataArch list</returns>
        List<OpDataArch> GetDataArch(long[] Ids);

        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Id">DataArch Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArch object</returns>
        OpDataArch GetDataArch(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Ids">DataArch Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArch list</returns>
        List<OpDataArch> GetDataArch(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Deletes the DataArch object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataArch to delete</param>
        void DeleteDataArch(OpDataArch toBeDeleted);
        /// <summary>
        /// Clears the DataArch object cache
        /// </summary>
        void ClearDataArchCache();
        #endregion

        #region DataSource

        /// <summary>
        /// Indicates whether the DataSource is cached.
        /// </summary>
        bool DataSourceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataSource objects
        /// </summary>
        List<OpDataSource> GetAllDataSource();


        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Id">DataSource Id</param>
        /// <returns>DataSource object</returns>
        OpDataSource GetDataSource(long Id);

        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Ids">DataSource Ids</param>
        /// <returns>DataSource list</returns>
        List<OpDataSource> GetDataSource(long[] Ids);

        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Id">DataSource Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSource object</returns>
        OpDataSource GetDataSource(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Ids">DataSource Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSource list</returns>
        List<OpDataSource> GetDataSource(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataSource object to the database
        /// </summary>
        /// <param name="toBeSaved">DataSource to save</param>
        /// <returns>DataSource Id</returns>
        long SaveDataSource(OpDataSource toBeSaved);

        /// <summary>
        /// Deletes the DataSource object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataSource to delete</param>
        void DeleteDataSource(OpDataSource toBeDeleted);

        /// <summary>
        /// Clears the DataSource object cache
        /// </summary>
        void ClearDataSourceCache();
        #endregion

        #region DataSourceType

        /// <summary>
        /// Indicates whether the DataSourceType is cached.
        /// </summary>
        bool DataSourceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataSourceType objects
        /// </summary>
        List<OpDataSourceType> GetAllDataSourceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);


        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Id">DataSourceType Id</param>
        /// <returns>DataSourceType object</returns>
        OpDataSourceType GetDataSourceType(int Id);

        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Ids">DataSourceType Ids</param>
        /// <returns>DataSourceType list</returns>
        List<OpDataSourceType> GetDataSourceType(int[] Ids);

        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Id">DataSourceType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSourceType object</returns>
        OpDataSourceType GetDataSourceType(int Id, bool queryDatabase);

        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Ids">DataSourceType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSourceType list</returns>
        List<OpDataSourceType> GetDataSourceType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the DataSourceType object to the database
        /// </summary>
        /// <param name="toBeSaved">DataSourceType to save</param>
        /// <returns>DataSourceType Id</returns>
        int SaveDataSourceType(OpDataSourceType toBeSaved);

        /// <summary>
        /// Deletes the DataSourceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataSourceType to delete</param>
        void DeleteDataSourceType(OpDataSourceType toBeDeleted);

        /// <summary>
        /// Clears the DataSourceType object cache
        /// </summary>
        void ClearDataSourceTypeCache();
        #endregion

        #region DataMissingAnalogReadouts

        /// <summary>
        /// Indicates whether the DataMissingAnalogReadouts is cached.
        /// </summary>
        bool DataMissingAnalogReadoutsCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataMissingAnalogReadouts objects
        /// </summary>
        List<OpDataMissingAnalogReadouts> GetAllDataMissingAnalogReadouts();


        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Id">DataMissingAnalogReadouts Id</param>
        /// <returns>DataMissingAnalogReadouts object</returns>
        OpDataMissingAnalogReadouts GetDataMissingAnalogReadouts(long Id);

        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Ids">DataMissingAnalogReadouts Ids</param>
        /// <returns>DataMissingAnalogReadouts list</returns>
        List<OpDataMissingAnalogReadouts> GetDataMissingAnalogReadouts(long[] Ids);

        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Id">DataMissingAnalogReadouts Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataMissingAnalogReadouts object</returns>
        OpDataMissingAnalogReadouts GetDataMissingAnalogReadouts(long Id, bool queryDatabase);

        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Ids">DataMissingAnalogReadouts Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataMissingAnalogReadouts list</returns>
        List<OpDataMissingAnalogReadouts> GetDataMissingAnalogReadouts(long[] Ids, bool queryDatabase);

        /// <summary>
        /// Saves the DataMissingAnalogReadouts object to the database
        /// </summary>
        /// <param name="toBeSaved">DataMissingAnalogReadouts to save</param>
        /// <returns>DataMissingAnalogReadouts Id</returns>
        long SaveDataMissingAnalogReadouts(OpDataMissingAnalogReadouts toBeSaved);

        /// <summary>
        /// Deletes the DataMissingAnalogReadouts object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataMissingAnalogReadouts to delete</param>
        void DeleteDataMissingAnalogReadouts(OpDataMissingAnalogReadouts toBeDeleted);

        /// <summary>
        /// Clears the DataMissingAnalogReadouts object cache
        /// </summary>
        void ClearDataMissingAnalogReadoutsCache();
        #endregion

        #region TemperatureCoefficient
        /// <summary>
        /// Indicates whether the TemperatureCoefficient is cached.
        /// </summary>
        bool TemperatureCoefficientCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TemperatureCoefficient objects
        /// </summary>
        List<OpTemperatureCoefficient> GetAllTemperatureCoefficient();

        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Id">TemperatureCoefficient Id</param>
        /// <returns>TemperatureCoefficient object</returns>
        OpTemperatureCoefficient GetTemperatureCoefficient(long Id);

        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Ids">TemperatureCoefficient Ids</param>
        /// <returns>TemperatureCoefficient list</returns>
        List<OpTemperatureCoefficient> GetTemperatureCoefficient(long[] Ids);

        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Id">TemperatureCoefficient Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TemperatureCoefficient object</returns>
        OpTemperatureCoefficient GetTemperatureCoefficient(long Id, bool queryDatabase);

        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Ids">TemperatureCoefficient Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TemperatureCoefficient list</returns>
        List<OpTemperatureCoefficient> GetTemperatureCoefficient(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the TemperatureCoefficient object to the database
        /// </summary>
        /// <param name="toBeSaved">TemperatureCoefficient to save</param>
        /// <returns>TemperatureCoefficient Id</returns>
        long SaveTemperatureCoefficient(OpTemperatureCoefficient toBeSaved);

        /// <summary>
        /// Deletes the TemperatureCoefficient object from the database
        /// </summary>
        /// <param name="toBeDeleted">TemperatureCoefficient to delete</param>
        void DeleteTemperatureCoefficient(OpTemperatureCoefficient toBeDeleted);

        /// <summary>
        /// Clears the TemperatureCoefficient object cache
        /// </summary>
        void ClearTemperatureCoefficientCache();
        #endregion

        #region TransmissionDriver

        /// <summary>
        /// Indicates whether the TransmissionDriver is cached.
        /// </summary>
        bool TransmissionDriverCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all TransmissionDriver objects
        /// </summary>
        List<OpTransmissionDriver> GetAllTransmissionDriverDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <returns>TransmissionDriver object</returns>
        OpTransmissionDriver GetTransmissionDriverDW(int Id);

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <returns>TransmissionDriver list</returns>
        List<OpTransmissionDriver> GetTransmissionDriverDW(int[] Ids);

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver object</returns>
        OpTransmissionDriver GetTransmissionDriverDW(int Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver list</returns>
        List<OpTransmissionDriver> GetTransmissionDriverDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);
        #endregion

        #region TransmissionDriverPerformance

        /// <summary>
        /// Indicates whether the TransmissionDriverPerformance is cached.
        /// </summary>
        bool TransmissionDriverPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriverPerformance objects
        /// </summary>
        List<OpTransmissionDriverPerformance> GetAllTransmissionDriverPerformance();


        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Id">TransmissionDriverPerformance Id</param>
        /// <returns>TransmissionDriverPerformance object</returns>
        OpTransmissionDriverPerformance GetTransmissionDriverPerformance(long Id);

        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverPerformance Ids</param>
        /// <returns>TransmissionDriverPerformance list</returns>
        List<OpTransmissionDriverPerformance> GetTransmissionDriverPerformance(long[] Ids);

        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Id">TransmissionDriverPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverPerformance object</returns>
        OpTransmissionDriverPerformance GetTransmissionDriverPerformance(long Id, bool queryDatabase);

        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverPerformance list</returns>
        List<OpTransmissionDriverPerformance> GetTransmissionDriverPerformance(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0);

        /// <summary>
        /// Saves the TransmissionDriverPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriverPerformance to save</param>
        /// <returns>TransmissionDriverPerformance Id</returns>
        long SaveTransmissionDriverPerformance(OpTransmissionDriverPerformance toBeSaved);

        /// <summary>
        /// Deletes the TransmissionDriverPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriverPerformance to delete</param>
        void DeleteTransmissionDriverPerformance(OpTransmissionDriverPerformance toBeDeleted);

        /// <summary>
        /// Clears the TransmissionDriverPerformance object cache
        /// </summary>
        void ClearTransmissionDriverPerformanceCache();
        #endregion

        #region PacketHourTransmissionDriver

        /// <summary>
        /// Indicates whether the PacketHourTransmissionDriver is cached.
        /// </summary>
        bool PacketHourTransmissionDriverCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketHourTransmissionDriver object cache
        /// </summary>
        void ClearPacketHourTransmissionDriverCache();
        #endregion

        #region PacketHourAddress

        /// <summary>
        /// Indicates whether the PacketHourAddress is cached.
        /// </summary>
        bool PacketHourAddressCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketHourAddress object cache
        /// </summary>
        void ClearPacketHourAddressCache();
        #endregion

        #region PacketHourDevice

        /// <summary>
        /// Indicates whether the PacketHourDevice is cached.
        /// </summary>
        bool PacketHourDeviceCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketHourDevice object cache
        /// </summary>
        void ClearPacketHourDeviceCache();
        #endregion

        #region PacketTrashHourAddress

        /// <summary>
        /// Indicates whether the PacketTrashHourAddress is cached.
        /// </summary>
        bool PacketTrashHourAddressCacheEnabled { get; set; }
        /// <summary>
        /// Clears the PacketTrashHourAddress object cache
        /// </summary>
        void ClearPacketTrashHourAddressCache();
        #endregion

        #region PacketTrashHourDevice

        /// <summary>
        /// Indicates whether the PacketTrashHourDevice is cached.
        /// </summary>
        bool PacketTrashHourDeviceCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketTrashHourDevice object cache
        /// </summary>
        void ClearPacketTrashHourDeviceCache();
        #endregion

        #region PacketTrashHourTransmissionDriver
        /// <summary>
        /// Indicates whether the PacketTrashHourTransmissionDriver is cached.
        /// </summary>
        bool PacketTrashHourTransmissionDriverCacheEnabled { get; set; }


        /// <summary>
        /// Clears the PacketTrashHourTransmissionDriver object cache
        /// </summary>
        void ClearPacketTrashHourTransmissionDriverCache();
        #endregion
#endif
    }
}
