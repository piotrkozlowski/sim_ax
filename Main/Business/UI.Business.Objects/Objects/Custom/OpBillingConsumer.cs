﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS

    public class OpBillingConsumer
    {
        #region Properties

        public int IdConsumer { get; private set; }

        public int IdActor { get; private set; }

        public int IdDistributor { get; private set; }

        public int IdConsumerType { get; private set; }

        public int IdTariff { get; private set; }

        public int IdDeviceType { get; private set; }

        public long SerialNbr { get; private set; }

        public string DevicePhoneNbr { get; private set; }

        public long IdLocation { get; private set; }

        #endregion Properties

        #region .Ctor

        public OpBillingConsumer()
        {
        }

        public OpBillingConsumer(DB_BILLING_CONSUMER billingConsumer)
        {
            IdConsumer = billingConsumer.ID_CONSUMER;
            IdActor = billingConsumer.ID_ACTOR;
            IdDistributor = billingConsumer.ID_DISTRIBUTOR;
            IdConsumerType = billingConsumer.ID_CONSUMER_TYPE;
            IdTariff = billingConsumer.ID_TARIFF;
            IdDeviceType = billingConsumer.ID_DEVICE_TYPE;
            IdLocation = billingConsumer.ID_LOCATION;
            SerialNbr = billingConsumer.SERIAL_NBR;
            //moduleBillingNbrData = consumerCoreData.MODULE_BILLING_NBR_DATA;
            DevicePhoneNbr = billingConsumer.DEVICE_PHONE_NBR;
            //NotificationData = consumerCoreData.NOTIFICATION_DATA;
        }

        #endregion .Ctor
    }

#endif
}
