﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    public class OpReconciliation
    {
        public long? IdMeter { get; set; }
        public long? IdLocation { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int? IdAggregationType { get; set; }
        public long? IdDataSourceType { get; set; }
        public DateTime Day { get; set; }
        public Common.Enums.DataStatus Status { get; set; }
        public double? SalesDispencedGross { get; set; }
        public double? SalesDispencedNet { get; set; }
        public double? FlowDispencedGross { get; set; }
        public double? FlowDispencedNet { get; set; }
        public double? OutcomeGross { get; set; }
        public double? OutcomeNet { get; set; }
        public double? DeliveryManualGross { get; set; }
        public double? DeliveryManualNet { get; set; }
        public double? DeliveryCalculatedGross { get; set; }
        public double? DeliveryCalculatedNet { get; set; }
        public double? DeliveryMixedGross { get; set; }
        public double? DeliveryMixedNet { get; set; }
        public double? StartVolumeGross { get; set; }
        public double? StartVolumeNet { get; set; }
        public double? EndVolumeGross { get; set; }
        public double? EndVolumeNet { get; set; }
        public double? EndHeight { get; set; }
        public double? EndTemperature { get; set; }        
        public double? FuelDepth { get; set; }
        public double? FuelTemperature { get; set; }
    }   
#endif
}
