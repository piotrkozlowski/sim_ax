﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    public class OpTaskWizard
    {
        public int WizardVersion;

        public string WizardName;

        public string WizardDescription;

        public List<OpTaskWizardStep> TaskWizardSteps;

        public List<OpDictItem> WizardParameters;

        public List<object> WizardRestrictions;
    }
#endif
}
