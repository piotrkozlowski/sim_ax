﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    public class OpTaskWizardStep
    {
        public int IdStep;

        public int IdNextStepYes;

        public int IdNextStepNo;

        public int IdPreviousStep;

        public IMR.Suite.Common.Enums.TaskWizardStepType StepType;

        public int StepParts;

        public int CurrentPart;

        public bool CorrectenessRequired;

        public List<OpControlValue> ControlValue;
    }
#endif
}
