﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [DataContract]
    public class OpEnumObject : IEquatable<OpEnumObject>, IComparable
    {
        //sposób wyciągania tekstu z Resource <NazwaEnuma>_<NazwaPozycjiEnum>_Enums
        private static System.Resources.ResourceManager ResourcesManager = new System.Resources.ResourceManager(typeof(ResourcesText));

        [DataMember]
        private string EnumId;

        [DataMember]
        public string EnumTypeName { get; set; }
        public Type EnumType { get { return String.IsNullOrEmpty(this.EnumTypeName) ? null : System.Type.GetType(EnumTypeName); } }

        [DataMember]
        public string EnumName { get; private set; }
        [DataMember]
        public string EnumText { get; private set; }
        
        public Enum EnumValue { get; private set; }
        [DataMember]
        public object EnumKey { get; private set; }

        [DataMember]
        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }


        public OpEnumObject()
        {
            
        }

        public OpEnumObject(Enum enumValue)
        {
            this.EnumTypeName = enumValue.GetType().AssemblyQualifiedName;
           
            this.EnumValue = enumValue;
            this.EnumText = Enum.GetName(EnumType, enumValue);
            this.EnumName = EnumType.Name;
            try
            {
                if (this.EnumValue != null)
                    this.EnumKey = Convert.ChangeType(this.EnumValue, Type.GetTypeCode(this.EnumType));
            }
            catch { }

            try
            {
                string retVal = ResourcesManager.GetString(String.Format("{0}_{1}_Enums", EnumName, EnumText), Utils.CultureInfo);
                if (String.IsNullOrEmpty(retVal))
                    retVal = EnumText;
                Description = retVal;
            }
            catch
            {
                Description = EnumText;
            }

            EnumId = String.Format("{0}.{1}.{2}", EnumName, EnumValue, EnumKey);
        }
        
        public override string ToString()
        {
            return Description;
        }

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpEnumObject)
                return Equals(obj as OpEnumObject);
            else
                return base.Equals(obj);
        }
    #endregion

    #region IEquatable<OpEnumObject>

        public bool Equals(OpEnumObject obj)
        {
            if (obj != null)
                return obj.EnumId.Equals(this.EnumId);
            else
                return base.Equals(obj);
        }

    #endregion

    #region IComparable

        public int CompareTo(object obj)
        {
            if (obj is OpEnumObject)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpEnumObject).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }

    #endregion

    #region Operators

        public static bool operator ==(OpEnumObject left, OpEnumObject right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpEnumObject left, OpEnumObject right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

    #endregion

        public override int GetHashCode()
        {
            return String.Format("{0}.{1}", this.EnumName, this.EnumText).GetHashCode();
        }
    }
#endif
}
