﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTaskStatistics : DB_TASK_STATISTICS
    {
    #region Properties
        public int IdOperator
        {
            get { return this.ID_OPERATOR; }
            set { this.ID_OPERATOR = value; }
        }

        public int TotalNo
        {
            get { return this.TOTAL_NO; }
            set { this.TOTAL_NO = value; }
        }

        public int FinishedNo
        {
            get { return this.FINISHED_NO; }
            set { this.FINISHED_NO = value; }
        }

        public int PlannedNo
        {
            get { return this.PLANNED_NO; }
            set { this.PLANNED_NO = value; }
        }

        public int InProgressNo
        {
            get { return this.IN_PROGRESS_NO; }
            set { this.IN_PROGRESS_NO = value; }
        }

        public int WaitingForAcceptanceNo
        {
            get { return this.WAITING_FOR_ACCEPTANCE_NO; }
            set { this.WAITING_FOR_ACCEPTANCE_NO = value; }
        }

        public int NotPaidNo
        {
            get { return this.NOT_PAID_NO; }
            set { this.NOT_PAID_NO = value; }
        }
    #endregion

    #region	Ctor
        public OpTaskStatistics()
            : base()
        {
        }

        public OpTaskStatistics(DB_TASK_STATISTICS clone)
            : base(clone)
        {
        }

    #endregion

    #region	ConvertList
        public static List<OpTaskStatistics> ConvertList(DB_TASK_STATISTICS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTaskStatistics> ConvertList(DB_TASK_STATISTICS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpTaskStatistics> ret = new List<OpTaskStatistics>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTaskStatistics(db_object)));
            return ret;
        }
    #endregion
    }
#endif
}
