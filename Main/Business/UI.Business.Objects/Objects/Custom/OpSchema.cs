﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    #region OpSchema

    [DataContract, Serializable]
    [KnownType(typeof(OpTankSchema))]
    [KnownType(typeof(OpACOSchema))]
    [KnownType(typeof(OpGasMeterSchema))]
    public class OpSchema
    {
        [DataMember, XmlIgnore]
        public byte[] Schema { get; set; }
        [DataMember, XmlIgnore]
        public object IdObject { get; set; }
        [DataMember, XmlIgnore]
        public Enums.ReferenceType ReferenceType { get; set; }
        [DataMember]
        public List<OpSchemaElement> SchemaConfiguration { get; set; }

        public OpSchema()
        {
        }
        public OpSchema(object IdObject, Enums.ReferenceType ReferenceType) : this(IdObject, ReferenceType, null, null)
        {

        }
        public OpSchema(object IdObject, Enums.ReferenceType ReferenceType, byte[] Schema, List<OpSchemaElement> SchemaConfiguration)
        {
            this.IdObject = IdObject;
            this.ReferenceType = ReferenceType;
            this.Schema = Schema;
            this.SchemaConfiguration = SchemaConfiguration;
        }

        public void SetSchemaConfiguration(byte[] schemaConfiguration)
        {
            if (schemaConfiguration != null)
            {
                using (System.IO.Stream stream = new System.IO.MemoryStream(schemaConfiguration))
                {
                    System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(OpSchema));

                    OpSchema sItem = xs.Deserialize(stream) as OpSchema;
                    stream.Close();
                    if (sItem != null)
                        this.SchemaConfiguration = sItem.SchemaConfiguration;
                }
            }
        }
    }

    public class OpTankSchema : OpSchema
    {
        public static Dictionary<string, byte[]> TankFile = new Dictionary<string, byte[]>();

        #region Horizontal
        public static string TankHorizontalSchemaFileName = null;
        public static string TankHorizontalMaskFileName = null;
        public static string TankHorizontalContourMaskFileName = null;
        public static string TankHorizontalConfigurationFileName = null;

        public static string TankHorizontalUndergroundSchemaFileName = null;
        public static string TankHorizontalUndergroundMaskFileName = null;
        public static string TankHorizontalUndergroundContourMaskFileName = null;
        public static string TankHorizontalUndergroundConfigurationFileName = null;

        public static string TankHorizontalSquareSchemaFileName = null;
        public static string TankHorizontalSquareMaskFileName = null;
        public static string TankHorizontalSquareContourMaskFileName = null;
        public static string TankHorizontalSquareConfigurationFileName = null;

        public static string TankHorizontalUndergroundSquareSchemaFileName = null;
        public static string TankHorizontalUndergroundSquareMaskFileName = null;
        public static string TankHorizontalUndergroundSquareContourMaskFileName = null;
        public static string TankHorizontalUndergroundSquareConfigurationFileName = null;
        #endregion
        #region Vertical
        public static string TankVerticalSchemaFileName = null;
        public static string TankVerticalMaskFileName = null;
        public static string TankVerticalContourMaskFileName = null;
        public static string TankVerticalConfigurationFileName = null;

        public static string TankVerticalUndergroundSchemaFileName = null;
        public static string TankVerticalUndergroundMaskFileName = null;
        public static string TankVerticalUndergroundContourMaskFileName = null;
        public static string TankVerticalUndergroundConfigurationFileName = null;

        public static string TankVerticalSquareSchemaFileName = null;
        public static string TankVerticalSquareMaskFileName = null;
        public static string TankVerticalSquareContourMaskFileName = null;
        public static string TankVerticalSquareConfigurationFileName = null;

        public static string TankVerticalUndergroundSquareSchemaFileName = null;
        public static string TankVerticalUndergroundSquareMaskFileName = null;
        public static string TankVerticalUndergroundSquareContourMaskFileName = null;
        public static string TankVerticalUndergroundSquareConfigurationFileName = null;
        #endregion

        [DataMember]
        public int Level { get; set; }

        public int? HiLevel { get; set; }
        public int? HiHiLevel { get; set; }
        public int? LoLevel { get; set; }
        public int? LoLoLevel { get; set; }

        public bool IsVertical { get; set; }
        public bool IsUnderground { get; set; }
        public bool IsSquareShape { get; set; }

        public enum TankFileType
        {
            Schema = 1,
            Mask = 2,
            Contour = 3,
            Configuration = 4
        }

        public OpTankSchema()
        {
        }
        public OpTankSchema(object IdObject, Enums.ReferenceType ReferenceType, int Level, bool IsVertical, bool IsUnderground, bool IsSquareShape, List<OpSchemaElement> SchemaConfiguration)
            : this(IdObject, ReferenceType, Level, SchemaConfiguration, IsVertical, IsUnderground, IsSquareShape, null, null, null)
        {
        }
        public OpTankSchema(object IdObject, Enums.ReferenceType ReferenceType, int Level, List<OpSchemaElement> SchemaConfiguration,
            bool IsVertical, bool IsUnderground, bool IsSquareShape,
            byte[] Schema, byte[] Mask, byte[] Contour)
            : base(IdObject, ReferenceType)
        {
            this.Level = Level;
            this.SchemaConfiguration = SchemaConfiguration;
            this.IsVertical = IsVertical;
            this.IsUnderground = IsUnderground;
            this.IsSquareShape = IsSquareShape;

            if (Schema != null)
                TankFile[GetFileName(TankFileType.Schema, IsVertical, IsUnderground, IsSquareShape)] = Schema;
            if (Mask != null)
                TankFile[GetFileName(TankFileType.Mask, IsVertical, IsUnderground, IsSquareShape)] = Mask;
            if (Contour != null)
                TankFile[GetFileName(TankFileType.Contour, IsVertical, IsUnderground, IsSquareShape)] = Contour;

            byte[] confFile = TankFile.TryGetValue(GetFileName(TankFileType.Configuration, IsVertical, IsUnderground, IsSquareShape));
            this.SetSchemaConfiguration(confFile);

            this.Schema = Schema;
        }

        public string GetFileName(TankFileType fileType, bool isVertical, bool isUnderground, bool isSquareShape)
        {
            if (isUnderground == false && isVertical == false)
            {
                if (isSquareShape)
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankHorizontalSquareContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankHorizontalSquareMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankHorizontalSquareSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankHorizontalSquareConfigurationFileName;
                    }
                }
                else
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankHorizontalContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankHorizontalMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankHorizontalSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankHorizontalConfigurationFileName;
                    }
                }
            }
            else if (isUnderground == true && isVertical == true)
            {
                if (isSquareShape)
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankVerticalUndergroundSquareContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankVerticalUndergroundSquareMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankVerticalUndergroundSquareSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankVerticalUndergroundSquareConfigurationFileName;
                    }
                }
                else
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankVerticalUndergroundContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankVerticalUndergroundMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankVerticalUndergroundSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankVerticalUndergroundConfigurationFileName;
                    }
                }
            }
            else if (isUnderground == true && isVertical == false)
            {
                if (isSquareShape)
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankHorizontalUndergroundSquareContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankHorizontalUndergroundSquareMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankHorizontalUndergroundSquareSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankHorizontalUndergroundSquareConfigurationFileName;
                    }
                }
                else
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankHorizontalUndergroundContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankHorizontalUndergroundMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankHorizontalUndergroundSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankHorizontalUndergroundConfigurationFileName;
                    }
                }
            }
            if (isUnderground == false && isVertical == true)
            {
                if (isSquareShape)
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankVerticalSquareContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankVerticalSquareMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankVerticalSquareSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankVerticalSquareConfigurationFileName;
                    }
                }
                else
                {
                    switch (fileType)
                    {
                        case TankFileType.Contour: return OpTankSchema.TankVerticalContourMaskFileName;
                        case TankFileType.Mask: return OpTankSchema.TankVerticalMaskFileName;
                        case TankFileType.Schema: return OpTankSchema.TankVerticalSchemaFileName;
                        case TankFileType.Configuration: return OpTankSchema.TankVerticalConfigurationFileName;
                    }
                }
            }
            throw new Exception(String.Format("Unsupported tank type. FileType: {0}, IsUnderground: {1}, IsVertical: {2}, IsSquareShape: {3}.", fileType, isUnderground, isVertical, isSquareShape));
        }

        public void DrawSchema(int? level = null, int? levelWidth = null, int? levelHeight = null, int? levelX = null, int? levelY = null)
        {
            int red = 255;
            int green = 177;
            int blue = 27;
            if (!level.HasValue)
                level = this.Level;
            else
                this.Level = level.Value;

            //List<MeterModel> meterList = new List<MeterModel>();
            //Webservice.GetMeters(ref meterList);
            //meterList = meterList.Where(m => m.IdMeter == meterId).ToList();
            //Webservice.GetExtendedDataForMeters(ref meterList);
            //MeterModel meter = meterList.First();
            Color levelColor = Color.FromArgb(red, green, blue);

            // w zaleznosci od rodzju zbiornika pobieramy odpowiednią grafike                
            string schemaFile = GetFileName(TankFileType.Schema, this.IsVertical, this.IsUnderground, this.IsSquareShape);
            string maskFile = GetFileName(TankFileType.Mask, this.IsVertical, this.IsUnderground, this.IsSquareShape);
            string contourFile = GetFileName(TankFileType.Contour, this.IsVertical, this.IsUnderground, this.IsSquareShape);

            Image imageTank = null;
            Image imageMask = null;
            Image imageMaskCountour = null;
            using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(TankFile[schemaFile]))
            {
                imageTank = Image.FromStream(msItem);
            }
            using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(TankFile[maskFile]))
            {
                imageMask = Image.FromStream(msItem);
            }
            using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(TankFile[contourFile]))
            {
                imageMaskCountour = Image.FromStream(msItem);
            }
            bool skipFindMaaskLevels = levelWidth.HasValue && levelHeight.HasValue && levelX.HasValue && levelY.HasValue;

            if (!levelWidth.HasValue)
                levelWidth = 180;
            if (!levelHeight.HasValue)
                levelHeight = 56;
            if (!levelX.HasValue)
                levelX = 10;
            if (!levelY.HasValue)
                levelY = 76;
            bool levelsFromMaskFound = false;
            #region FindMaskLevels
            if (skipFindMaaskLevels)
                levelsFromMaskFound = true;
            else
            {
                DateTime DT11 = DateTime.Now;
                //Dictionary<Color, int> foundedColors = new Dictionary<Color, int>();
                Color refColor = Color.FromArgb(0, 0, 0, 0);
                using (Bitmap maskBmpTemp = new Bitmap(imageMask))
                {                    
                    int levelWidthTmp = 0;
                    int levelHeightTmp = 0;
                    int levelxTmp = int.MaxValue;
                    int levelyTmp = int.MaxValue;

                    Size s = maskBmpTemp.Size;
                    PixelFormat fmt = maskBmpTemp.PixelFormat;
                    Rectangle rect = new Rectangle(Point.Empty, s);
                    BitmapData bmpData = maskBmpTemp.LockBits(rect, ImageLockMode.ReadOnly, fmt);
                    int size1 = bmpData.Stride * bmpData.Height;
                    byte[] data = new byte[size1];
                    System.Runtime.InteropServices.Marshal.Copy(bmpData.Scan0, data, 0, size1);

                    for (int x = 0; x < s.Width; x++)
                    {
                        int levelHeightIter = 0;
                        for (int y = 0; y < s.Height; y++)
                        {
                            int index = y * bmpData.Stride + x * 4;
                            if (data[index + 3] == refColor.A && data[index + 2] == refColor.R && data[index + 1] == refColor.G && data[index] == refColor.B)
                            {
                                if (levelxTmp > x)
                                    levelxTmp = x;
                                if (levelyTmp > y)
                                    levelyTmp = y;
                                levelHeightIter++;
                            }                                
                        }
                        if (levelHeightIter > levelHeightTmp)
                            levelHeightTmp = levelHeightIter;
                        if (levelHeightIter > 0)
                            levelWidthTmp++;
                    }
                    System.Runtime.InteropServices.Marshal.Copy(data, 0, bmpData.Scan0, data.Length);
                    maskBmpTemp.UnlockBits(bmpData);

                    if (levelxTmp < int.MaxValue && levelyTmp < int.MaxValue && levelHeightTmp > 0 && levelWidthTmp > 0)
                    {
                        levelWidth = levelWidthTmp;
                        levelHeight = levelHeightTmp;
                        levelX = levelxTmp;
                        levelY = levelyTmp;
                        levelsFromMaskFound = true;
                    }
                }
                TimeSpan ts = (DateTime.Now - DT11);
            }

            if (!levelsFromMaskFound)
            {
                if (this.IsUnderground == false && this.IsVertical == false)
                {
                    levelWidth = 180;
                    levelHeight = 56;
                    levelX = 10;
                    levelY = 76;
                }
                if (this.IsUnderground == true && this.IsVertical == true)
                {
                    levelWidth = 77;
                    levelHeight = 111;
                    levelX = 5;
                    levelY = 30;
                }
                else if (this.IsUnderground == true && this.IsVertical == false)
                {
                    levelWidth = 180;
                    levelHeight = 56;
                    levelX = 10;
                    levelY = 76;
                }
                if (this.IsUnderground == false && this.IsVertical == true)
                {
                    levelWidth = 77;
                    levelHeight = 111;
                    levelX = 5;
                    levelY = 30;
                }
            }

            #endregion

            DateTime DT12 = DateTime.Now;
            SolidBrush contourBrush = new SolidBrush(Color.Gray);

            if (level < 0 && level > 100)
            {
                level = 100;
                levelColor = Color.Red;
            }

            decimal d = (decimal)level;
            decimal h = (decimal)levelHeight;
            int valproc = (int)((d / 100) * h);

            // Naniesienie obszaru z poziomem
            Bitmap bmp = new Bitmap(imageMask.Width, imageMask.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            Bitmap bmpLevel = new Bitmap(imageMask.Width, imageMask.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(bmpLevel))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                int rectX = levelX.Value;
                int rectY = levelY.Value + levelHeight.Value - valproc;
                int rectWidth = levelWidth.Value;
                int rectHeight = valproc;
                g.FillRectangle(new SolidBrush(levelColor), rectX, rectY, rectWidth, rectHeight);
                g.DrawImage(imageMask, new Rectangle(0, 0, bmpLevel.Width, bmpLevel.Height), new Rectangle(0, 0, imageMask.Width, imageMask.Height), GraphicsUnit.Pixel);
            }
            bmpLevel.MakeTransparent(Color.Black);

            // Kolor konturu jesli brak urządzenia
            Bitmap contour = new Bitmap(imageMaskCountour.Width, imageMaskCountour.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            using (Graphics g = Graphics.FromImage(contour))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                g.FillRectangle(contourBrush, 0, 0, contour.Width, contour.Height);
                g.DrawImage(imageMaskCountour, new Rectangle(0, 0, contour.Width, contour.Height), new Rectangle(0, 0, imageMaskCountour.Width, imageMaskCountour.Height), GraphicsUnit.Pixel);
            }
            contour.MakeTransparent(Color.Black);

            #region AlarmLevels
            // dodnie lini alarmu
            if (this.HiHiLevel.HasValue)
            {
                using (Graphics g = Graphics.FromImage(bmpLevel))
                {
                    // oblicznia
                    int alarmlevel = (int)(((double)this.HiHiLevel.Value / 100.0) * (double)levelHeight);
                    int x = levelX.Value;
                    int y = levelY.Value + levelHeight.Value - alarmlevel;
                    Pen drawPen = new Pen(Color.Red, 2);
                    g.DrawLine(drawPen, new Point(x, y + 5), new Point(x + levelWidth.Value, y + 5));
                    drawPen.Dispose();
                }
            }
            if (this.HiLevel.HasValue)
            {
                using (Graphics g = Graphics.FromImage(bmpLevel))
                {
                    // oblicznia
                    int alarmlevel = (int)(((double)this.HiLevel.Value / 100.0) * (double)levelHeight);

                    int x = levelX.Value;
                    int y = levelY.Value + levelHeight.Value - alarmlevel;
                    Pen drawPen = new Pen(Color.Yellow, 2);
                    g.DrawLine(drawPen, new Point(x, y + 5), new Point(x + levelWidth.Value, y + 5));
                    drawPen.Dispose();
                }
            }
            if (this.LoLevel.HasValue)
            {
                using (Graphics g = Graphics.FromImage(bmpLevel))
                {
                    // oblicznia
                    int alarmlevel = (int)(((double)this.LoLevel.Value / 100.0) * (double)levelHeight);

                    int x = levelX.Value;
                    int y = levelY.Value + levelHeight.Value - alarmlevel;
                    Pen drawPen = new Pen(Color.Yellow, 2);
                    g.DrawLine(drawPen, new Point(x, y + 5), new Point(x + levelWidth.Value, y + 5));
                    drawPen.Dispose();
                }
            }
            if (this.LoLoLevel.HasValue)
            {
                using (Graphics g = Graphics.FromImage(bmpLevel))
                {
                    // oblicznia
                    int alarmlevel = (int)(((double)this.LoLoLevel.Value / 100.0) * (double)levelHeight);

                    int x = levelX.Value;
                    int y = levelY.Value + levelHeight.Value - alarmlevel;
                    Pen drawPen = new Pen(Color.Red, 2);
                    g.DrawLine(drawPen, new Point(x, y + 5), new Point(x + levelWidth.Value, y + 5));
                    drawPen.Dispose();
                }
            }
            #endregion

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
                g.DrawImage(bmpLevel, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, bmpLevel.Width, bmpLevel.Height), GraphicsUnit.Pixel);
                g.DrawImage(contour, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, contour.Width, contour.Height), GraphicsUnit.Pixel);
            }

            using (System.IO.MemoryStream msItem = new System.IO.MemoryStream())
            {
                bmp.Save(msItem, System.Drawing.Imaging.ImageFormat.Png);
                Schema = msItem.ToArray();
            }

            contourBrush.Dispose();
            imageTank.Dispose();
            imageMask.Dispose();
            imageMaskCountour.Dispose();
            bmp.Dispose();
            contour.Dispose();
            bmpLevel.Dispose();

            TimeSpan ts2 = (DateTime.Now - DT12);
        }

        public void LoadConfiguration()
        {
            string configurationFileName = this.GetFileName(TankFileType.Configuration, this.IsVertical, this.IsUnderground, this.IsSquareShape);
            if (String.IsNullOrEmpty(configurationFileName))
                throw new Exception("Configuration file name not found");
            byte[] configurationFile = TankFile.TryGetValue(configurationFileName);
            if (configurationFile != null)
            {
                this.SetSchemaConfiguration(configurationFile);
            }
        }
    }

    public class OpACOSchema : OpSchema
    {
        [DataMember]
        public bool Left { get; set; }
        [DataMember]
        public bool Right { get; set; }
        [DataMember]
        public bool Reserve { get; set; }

        public static byte[] AcoSchema = null;
        public static string AcoSchemaFileName = null;
        public static byte[] AcoRightMask = null;
        public static string AcoRightMaskFileName = null;
        public static byte[] AcoLeftMask = null;
        public static string AcoLeftMaskFileName = null;
        public static byte[] AcoRightReserveMask = null;
        public static string AcoRightReserveFileName = null;
        public static byte[] AcoLeftReserveMask = null;
        public static string AcoLeftReserveFileName = null;

        public OpACOSchema()
        {
        }
        public OpACOSchema(object IdObject, Enums.ReferenceType ReferenceType, bool Left, bool Right, bool Reserve, List<OpSchemaElement> SchemaConfiguration)
        {
        }
        public OpACOSchema(object IdObject, Enums.ReferenceType ReferenceType, bool Left, bool Right, bool Reserve, List<OpSchemaElement> SchemaConfiguration,
            byte[] Aco, byte[] AcoRight, byte[] AcoLeft, byte[] AcoRightReserve, byte[] AcoLeftReserve)
        {
            this.Left = Left;
            this.Right = Right;
            this.Reserve = Reserve;
            this.SchemaConfiguration = SchemaConfiguration;
            if (Aco != null)
                AcoSchema = Aco;
            if (AcoRight != null)
                AcoRightMask = AcoRight;
            if (AcoLeft != null)
                AcoLeftMask = AcoLeft;
            if (AcoRightReserve != null)
                AcoRightReserveMask = AcoRightReserve;
            if (AcoLeftReserve != null)
                AcoLeftReserveMask = AcoLeftReserve;

            this.Schema = AcoSchema;



        }

        public void DrawSchema(bool? left = null, bool? right = null, bool? reserve = null)
        {
            if (!left.HasValue)
                left = this.Left;
            else
                this.Left = left.Value;
            if (!right.HasValue)
                right = this.Right;
            else
                this.Right = right.Value;
            if (!reserve.HasValue)
                reserve = this.Reserve;
            else
                this.Reserve = reserve.Value;

            int red = 255;
            int green = 177;
            int blue = 27;

            Image imageACO = null;
            using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(AcoLeftMask))
            {
                imageACO = Image.FromStream(msItem);
            }
            //var pathACO = Server.MapPath("~/Images/graphs/aco_left.png");

            Bitmap bmp = new Bitmap(360, 314, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            bmp.MakeTransparent();
            bmp.MakeTransparent(Color.Black);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                if (this.Right)
                {
                    if (this.Reserve)
                    {
                        using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(AcoRightReserveMask))
                        {
                            imageACO = Image.FromStream(msItem);
                        }
                        // pathACO = Server.MapPath("~/Images/graphs/aco_right_reserve.png");
                        //  imageACO = Image.FromFile(pathACO);
                        g.DrawImage(imageACO, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, imageACO.Width, imageACO.Height), GraphicsUnit.Pixel);
                    }
                    else
                    {
                        using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(AcoRightMask))
                        {
                            imageACO = Image.FromStream(msItem);
                        }
                        // pathACO = Server.MapPath("~/Images/graphs/aco_right.png");
                        //  imageACO = Image.FromFile(pathACO);
                        g.DrawImage(imageACO, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, imageACO.Width, imageACO.Height), GraphicsUnit.Pixel);
                    }
                }

                if (this.Left)
                {
                    if (this.Reserve)
                    {
                        using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(AcoLeftReserveMask))
                        {
                            imageACO = Image.FromStream(msItem);
                        }
                        // pathACO = Server.MapPath("~/Images/graphs/aco_left_reserve.png");
                        // imageACO = Image.FromFile(pathACO);
                        g.DrawImage(imageACO, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, imageACO.Width, imageACO.Height), GraphicsUnit.Pixel);
                    }
                    else
                    {
                        using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(AcoLeftMask))
                        {
                            imageACO = Image.FromStream(msItem);
                        }
                        //pathACO = Server.MapPath("~/Images/graphs/aco_left.png");
                        //imageACO = Image.FromFile(pathACO);
                        g.DrawImage(imageACO, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, imageACO.Width, imageACO.Height), GraphicsUnit.Pixel);
                    }
                }

                if (!this.Right && !this.Left)
                {
                    using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(AcoSchema))
                    {
                        imageACO = Image.FromStream(msItem);
                    }
                    //pathACO = Server.MapPath("~/Images/graphs/aco.png");
                    //imageACO = Image.FromFile(pathACO);
                    g.DrawImage(imageACO, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, imageACO.Width, imageACO.Height), GraphicsUnit.Pixel);
                }
            }
            using (System.IO.MemoryStream msItem = new System.IO.MemoryStream())
            {
                bmp.Save(msItem, System.Drawing.Imaging.ImageFormat.Png);
                Schema = msItem.ToArray();
            }
        }
    }

    public class OpGasMeterSchema : OpSchema
    {
        public static Dictionary<string, byte[]> GasMeterFile = new Dictionary<string, byte[]>();

        public static string GasMeterSchemaFileName = null;
        public static string GasMeterMaskFileName = null;
        public static string GasMeterContourMaskFileName = null;
        public static string GasMeterConfigurationFileName = null;

        public enum GasMeterFileType
        {
            Schema = 1,
            Mask = 2,
            Contour = 3,
            Configuration = 4
        }

        public double UsageValue { get; set; }

        public OpGasMeterSchema()
        {
        }
        public OpGasMeterSchema(object IdObject, Enums.ReferenceType ReferenceType, List<OpSchemaElement> SchemaConfiguration)
            : this(IdObject, ReferenceType, null, null, null, SchemaConfiguration)
        {
        }
        public OpGasMeterSchema(object IdObject, Enums.ReferenceType ReferenceType, byte[] Schema, byte[] Mask, byte[] Contour, List<OpSchemaElement> SchemaConfiguration)
            : base(IdObject, ReferenceType)
        {
            this.SchemaConfiguration = SchemaConfiguration;

            if (Schema != null)
                GasMeterFile[GetFileName(GasMeterFileType.Schema)] = Schema;
            if (Mask != null)
                GasMeterFile[GetFileName(GasMeterFileType.Mask)] = Mask;
            if (Contour != null)
                GasMeterFile[GetFileName(GasMeterFileType.Contour)] = Contour;

            byte[] confFile = GasMeterFile.TryGetValue(GetFileName(GasMeterFileType.Configuration));
            this.SetSchemaConfiguration(confFile);

            this.Schema = Schema;
        }

        public string GetFileName(GasMeterFileType fileType)
        {
            switch (fileType)
            {
                case GasMeterFileType.Contour: return OpGasMeterSchema.GasMeterContourMaskFileName;
                case GasMeterFileType.Mask: return OpGasMeterSchema.GasMeterMaskFileName;
                case GasMeterFileType.Schema: return OpGasMeterSchema.GasMeterSchemaFileName;
                case GasMeterFileType.Configuration: return OpGasMeterSchema.GasMeterConfigurationFileName;
            }

            throw new Exception(String.Format("Unsupported tank type. FileType: {0}.", fileType));
        }
        public static List<string> GetAllFileName()
        {
            List<string> fileNameList = new List<string>();

            if (!String.IsNullOrEmpty(GasMeterSchemaFileName))
                fileNameList.Add(GasMeterSchemaFileName);
            if (!String.IsNullOrEmpty(GasMeterMaskFileName))
                fileNameList.Add(GasMeterMaskFileName);
            if (!String.IsNullOrEmpty(GasMeterContourMaskFileName))
                fileNameList.Add(GasMeterContourMaskFileName);
            if (!String.IsNullOrEmpty(GasMeterConfigurationFileName))
                fileNameList.Add(GasMeterConfigurationFileName);
            return fileNameList;
        }

        public void DrawSchema(double? usageValue, int x, int y)
        {
           /* if (!usageValue.HasValue)
                usageValue = this.UsageValue;
            else
                this.UsageValue = usageValue.Value;

            string schemaFile = GetFileName(GasMeterFileType.Schema);

            Image imageGasMeter = null;

            using (System.IO.MemoryStream msItem = new System.IO.MemoryStream(GasMeterFile[schemaFile]))
            {
                imageGasMeter = Image.FromStream(msItem);
            }*/
        }

        public void LoadConfiguration()
        {
            string configurationFileName = this.GetFileName(GasMeterFileType.Configuration);
            if (String.IsNullOrEmpty(configurationFileName))
                throw new Exception("Configuration file name not found");
            byte[] configurationFile = GasMeterFile.TryGetValue(configurationFileName);
            if (configurationFile != null)
            {
                this.SetSchemaConfiguration(configurationFile);
            }
        }
    }

    #endregion

    #region OpSchemaElement
    [Serializable, DataContract]
    public class OpSchemaElement
    {
        [DataMember]
        public int X { get; set; }
        [DataMember]
        public int Y { get; set; }

        [DataMember]
        public int? Width { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int? Height { get; set; }

        [DataMember]
        public long IdDataType { get; set; }

        private OpDataType _DataType;
        public OpDataType DataType { get { return _DataType; } set { _DataType = value; IdDataType = value == null ? 0 : value.IdDataType; } }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public object Value { get; set; }

        [DataMember]
        public bool IsEditable { get; set; }//nadrzędnym obostrzeniem zawsze flaga IsEditable wynikająca z DataType

        [DataMember]
        public bool DescriptionVisible { get; set; }

        [DataMember]
        public bool UnitVisible { get; set; }

        [DataMember]
        public List<OpSchemaElementApperance> SchemaElementApperance { get; set; }

        public OpSchemaElement()
        {
            this.SchemaElementApperance = new List<OpSchemaElementApperance>();
        }

        public OpSchemaElement(int X, int Y, int? Width, int? Height, OpDataType DataType, bool IsEditable,
            List<OpSchemaElementApperance> SchemaElementApperance)
        {
            this.SchemaElementApperance = new List<OpSchemaElementApperance>();
            if (SchemaElementApperance != null)
                this.SchemaElementApperance.AddRange(SchemaElementApperance);
            this.X = X;
            this.Y = Y;
            this.Width = Width;
            this.Height = Height;
            this.DataType = DataType;
            this.IsEditable = IsEditable;

            this.DescriptionVisible = true;
            this.UnitVisible = true;
        }

    }

    #endregion

    #region OpSchemaElementApperance
    [Serializable, DataContract]
    public class OpSchemaElementApperance
    {
        [DataMember]
        public Color? Color { get; set; }
        [DataMember]
        public byte[] Image { get; set; }

        // <, >, <=, >=, ==, !=
        [DataMember]
        public string Condition { get; set; }

        //Typ zmiennych obostrzeń wynika z klasy data typu w kontekście którego definiowany jest wygląd
        [DataMember]
        public object Constraint1 { get; set; }
        [DataMember]
        public object Constraint2 { get; set; }

        public OpSchemaElementApperance()
        {

        }
        public OpSchemaElementApperance(byte[] Image, Color? Color, string Condition, object Constraint1, object Constraint2)
        {
            this.Color = Color;
            this.Image = Image;
            this.Condition = Condition;

            this.Constraint1 = Constraint1;
            this.Constraint2 = Constraint2;
        }
    }

    #endregion
}
