﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpSchemaControl : OpDashboardControl
    {
        [DataMember]
        public List<OpSchema> Schemas { get; set; }

        public OpSchemaControl(OpDashboardControl DashboardControl)
            : this(DashboardControl, null)
        {

        }
        public OpSchemaControl(OpDashboardControl DashboardControl, List<OpSchema> Schemas)
            : base(DashboardControl.Type, Enums.DashboardItemClass.SCHEMA, DashboardControl.Procedure, null, DashboardControl.Group, DashboardControl.IndexNbr, ReferenceData: DashboardControl.ReferenceData)
        {
            base.Width = DashboardControl.Width;

            this.Schemas = Schemas;
            if(Schemas == null)
                this.Schemas = new List<OpSchema>();
        }
    }
}
