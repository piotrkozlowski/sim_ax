﻿namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    public class OpVariance
    {
        public double? FlowErrorManualGross { get; set; }
        public double? FlowErrorManualNet { get; set; }
        public double? FlowErrorCalculatedGross { get; set; }
        public double? FlowErrorCalculatedNet { get; set; }
        public double? FlowErrorMixedGross { get; set; }
        public double? FlowErrorMixedNet { get; set; }
        public double? SalesErrorManualGross { get; set; }
        public double? SalesErrorManualNet { get; set; }
        public double? SalesErrorCalculatedGross { get; set; }
        public double? SalesErrorCalculatedNet { get; set; }
        public double? SalesErrorMixedGross { get; set; }
        public double? SalesErrorMixedNet { get; set; }
    }
#endif
}
