﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    public class OpViewOperation
    {
        public Enums.ViewOperation ChangeAction { get; set; }
        public List<OpViewOperationCellValue> CellValues { get; set; }
        
        public object ObjectId { get; set; }
        public string KeyColumnName { get; set; }

        public int IdImrServer { get; set; }
        public int IdView { get; set; }

        public long ObjectIdIdDataType { get; set; }

        #region NavigationProperties

        private OpImrServer _imrServer;
        public OpImrServer ImrServer { get { return _imrServer; } set { _imrServer = value; IdImrServer = (value == null) ? 0 : value.IdImrServer; } }
        private OpView _view;
        public OpView View { get { return _view; } set { _view = value; IdView = (value == null) ? 0 : value.IdView; } }

        private OpDataType _objectIdDataType;
        public OpDataType ObjectIdDataType { get { return _objectIdDataType; } set { _objectIdDataType = value; ObjectIdIdDataType = (value == null) ? 0 : value.IdDataType; } }

        #endregion

        public string Statement { get; set; }

        public List<object[]> ColumnCustomWhereClauses { get; set; }

        public OpViewOperation()
        {
            this.ColumnCustomWhereClauses = new List<object[]>();
            this.CellValues = new List<OpViewOperationCellValue>();
        }

        public OpViewOperation(object ObjectId, OpDataType ObjectIdDataType, int IdImrServer, int IdView, Enums.ViewOperation ChangeAction,
            List<OpViewOperationCellValue> CellValues)
        {
            this.ObjectId = ObjectId;
            this.ObjectIdDataType = ObjectIdDataType;
            this.IdImrServer = IdImrServer;
            this.IdView = IdView;
            this.ChangeAction = ChangeAction;
            if (CellValues != null)
                this.CellValues = CellValues;
            else
                this.CellValues = new List<OpViewOperationCellValue>();

            this.ColumnCustomWhereClauses = new List<object[]>();
        }

        public OpViewOperation(object ObjectId, OpDataType ObjectIdDataType, int IdImrServer, int IdView, Enums.ViewOperation ChangeAction,
            List<Tuple<string, object, OpDataType, int>> CellValues)
            : this(ObjectId, ObjectIdDataType, IdImrServer, IdView, ChangeAction, CellValues.Select(x => new OpViewOperationCellValue(x.Item1, x.Item2, x.Item3, x.Item4)).ToList())
        {
        }

        public OpViewOperation(object ObjectId, OpDataType ObjectIdDataType, OpImrServer ImrServer, OpView View, Enums.ViewOperation ChangeAction, 
            List<Tuple<string, object, OpDataType, int>> CellValues)
            : this(ObjectId, ObjectIdDataType, ImrServer, View, ChangeAction, CellValues.Select(x => new OpViewOperationCellValue(x.Item1, x.Item2, x.Item3, x.Item4)).ToList())
        {
        }

        public OpViewOperation(object ObjectId, OpDataType ObjectIdDataType, OpImrServer ImrServer, OpView View, Enums.ViewOperation ChangeAction,
            List<OpViewOperationCellValue> CellValues)
        {
            this.ObjectId = ObjectId;
            this.ObjectIdDataType = ObjectIdDataType;
            this.ImrServer = ImrServer;
            this.View = View;
            this.ChangeAction = ChangeAction;
            if (CellValues != null)
                this.CellValues = CellValues;
            else
                this.CellValues = new List<OpViewOperationCellValue>();

            this.ColumnCustomWhereClauses = new List<object[]>();
        }
        
        public class OpViewOperationCellValue
        {
            public string ColumnName { get; set; }
            public object Value { get; set; }
            public long IdDataType { get; set; }
            public int Index { get; set; }

            private OpDataType _dataType;
            public OpDataType DataType { get { return _dataType; } set { _dataType = value; IdDataType = (value == null) ? 0 : (long)value.IdDataType; } }

            public OpViewOperationCellValue()
	        {

	        }

            public OpViewOperationCellValue(string columnName, object value, OpDataType dataType, int index)
            {
                ColumnName = columnName;
                Value = value;
                DataType = dataType;
                Index = index;
            }

            public OpViewOperationCellValue(string columnName, object value, long idDataType, int index)
	        {
                ColumnName = columnName;
                Value = value;
                IdDataType = idDataType;
                Index = index;
	        }
        }
    }
}
