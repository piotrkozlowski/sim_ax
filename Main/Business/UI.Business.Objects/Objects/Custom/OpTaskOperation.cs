﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    public class OpTaskOperation
    {
        public long IdDescr { get; private set; }
        public string Descr { get; set; }
        public List<IMR.Suite.Common.Enums.MeterTypeClass> AllowedLocationTypes { get; set; }

        public OpTaskOperation(long IdDescr, string Descr, List<IMR.Suite.Common.Enums.MeterTypeClass> AllowedLocationTypes)
        {
            this.IdDescr = IdDescr;
            this.Descr = Descr;
            if (AllowedLocationTypes == null)
                this.AllowedLocationTypes = new List<IMR.Suite.Common.Enums.MeterTypeClass>();
            else
                this.AllowedLocationTypes = AllowedLocationTypes;
        }

        public override string ToString()
        {
            return this.Descr;
        }
    }
#endif
}
