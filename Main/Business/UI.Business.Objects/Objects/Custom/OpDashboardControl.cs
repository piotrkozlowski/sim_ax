﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using DW = IMR.Suite.UI.Business.Objects.DW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    //Klasy dziedziczace
    [KnownType(typeof(OpTileControl))]
    [KnownType(typeof(OpGaugeControl))]
    [KnownType(typeof(OpChartControl))]
    [KnownType(typeof(OpGridControl))]
    [KnownType(typeof(OpMapControl))]
    [KnownType(typeof(OpSchemaControl))]
    //Zawartość ReferenceData
    [KnownType(typeof(DW.OpAlarmEvent))]
    [DataContract]
    public class OpDashboardControl
    {
        public long IdType { get { return this.Type == null ? 0 : this.Type.IdDescr; } }
        [DataMember]
        public OpDescr Type { get; private set; }
        [DataMember]
        public Enums.DashboardItemClass Class { get; private set; }
        [DataMember]
        public string Procedure { get; private set; }
        [DataMember]
        public Enums.ReferenceType? ReferenceType { get; private set; }
        [DataMember]
        public OpDashboardGroup Group { get; private set; }
        [DataMember]
        public int IndexNbr { get; private set; }
        [DataMember]
        public int Width { get; set; }        
        [DataMember]
        public bool IsAlarm { get; set; }
        [DataMember]
        public Enums.Module Module { get; set; }
        [DataMember]
        public List<object> ReferenceData { get; set; }

        public OpDashboardControl(OpDescr Type, Enums.DashboardItemClass Class, string Procedure, Enums.ReferenceType? ReferenceType, OpDashboardGroup Group, int IndexNbr,
            List<object> ReferenceData = null, bool IsAlarm = false, Enums.Module Module = Enums.Module.Unknown)
        {
            this.Type = Type;
            this.Class = Class;
            this.Procedure = Procedure;
            this.ReferenceType = ReferenceType;
            this.Group = Group;
            this.IndexNbr = IndexNbr;
            this.Module = Module;
            this.ReferenceData = ReferenceData;
            this.IsAlarm = IsAlarm;
        }

        public OpDashboardControl()
        {
        }
    }
}
