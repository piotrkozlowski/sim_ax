﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    public class OpControlValue
    {
        public string ControlName;

        public Enums.DataTypeClass Class;

        public object Value;

        public string Tag;
    }
#endif
}
