﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using DW = IMR.Suite.UI.Business.Objects.DW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpTileControl : OpDashboardControl
    {
        [DataMember]
        public string TextHeader { get; set; }
        [DataMember]
        public long Amount { get; set; }

        [DataMember]
        public OpFile Icon { get; set; }

        [DataMember]
        public System.Drawing.Color? BackgroundColor { get; set; }
        [DataMember]
        public OpFile BackgroundImage { get; set; }
        [DataMember]
        public System.Drawing.Color? HoverColor { get; set; }
        [DataMember]
        public System.Drawing.Color? SelectColor { get; set; }

        [DataMember]
        public bool AmountVisible { get; set; }
        [DataMember]
        public bool TexHeaderVisible { get; set; }
        [DataMember]
        public bool IconVisible { get; set; }

        [DataMember]
        //TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT
        public List<int> BorderRadius { get; set; }
        [DataMember]
        public int? BorderWidth { get; set; }
        [DataMember]
        public System.Drawing.Color? BorderColor { get; set; }

        [DataMember]
        public int? ShadowHorizontalLenght { get; set; }
        [DataMember]
        public int? ShadowVerticalLenght { get; set; }
        [DataMember]
        public int? ShadowBlurRadius { get; set; }
        [DataMember]
        public int? ShadowSpreadRadius { get; set; }
        [DataMember]
        public System.Drawing.Color? ShadowColor { get; set; }

        public OpTileControl(string TextHeader, long Amount, OpDashboardControl DashboardControl)
            : base(DashboardControl.Type, Enums.DashboardItemClass.TILE, DashboardControl.Procedure, DashboardControl.ReferenceType, DashboardControl.Group, DashboardControl.IndexNbr, ReferenceData: DashboardControl.ReferenceData, IsAlarm: DashboardControl.IsAlarm, Module: DashboardControl.Module)
        {
            base.Width = DashboardControl.Width;

            this.TextHeader = TextHeader;
            this.Amount = Amount;

            this.AmountVisible = true;
            this.TexHeaderVisible = true;
            this.IconVisible = false;

            this.BorderRadius = new List<int>();
        }
        public OpTileControl(string TextHeader, long Amount, OpDescr Type, string Procedure, OpDashboardGroup Group, int IndexNbr, List<Object> ReferenceData = null)
            : base(Type, Enums.DashboardItemClass.TILE, Procedure, null, Group, IndexNbr, ReferenceData: ReferenceData)
        {
            this.TextHeader = TextHeader;
            this.Amount = Amount;
        }
    }
}
