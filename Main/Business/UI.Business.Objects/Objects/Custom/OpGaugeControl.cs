﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpGaugeControl : OpDashboardControl
    {
        [DataMember]
        public List<OpGaugeRange> Ranges { get; set; }

        [DataMember]
        public double MinValue { get; private set; }
        [DataMember]
        public double MaxValue { get; private set; }
        [DataMember]
        public string Label { get; private set; }
        [DataMember]
        public object Value { get; private set; }

        public OpGaugeControl(double MinValue, double MaxValue, string Label, object Value, OpDashboardControl DashboardControl)
            : base(DashboardControl.Type, Enums.DashboardItemClass.GAUGE, DashboardControl.Procedure, null, DashboardControl.Group, DashboardControl.IndexNbr, ReferenceData: DashboardControl.ReferenceData)
        {
            base.Width = DashboardControl.Width;

            this.MinValue = MinValue;
            this.MaxValue = MaxValue;
            this.Label = Label;
            this.Value = Value;

            this.Ranges = new List<OpGaugeRange>();
        }

        [DataContract]
        public class OpGaugeRange
        {
            [DataMember]
            public double MinValue { get; private set; }
            [DataMember]
            public double MaxValue { get; private set; }

            [DataMember]
            public string Name { get; set; }
            [DataMember]
            public System.Drawing.Color Color { get; set; }

            public OpGaugeRange(double MinValue, double MaxValue)
            {
                this.MinValue = MinValue;
                this.MaxValue = MaxValue;
            }
        }
    }
}
