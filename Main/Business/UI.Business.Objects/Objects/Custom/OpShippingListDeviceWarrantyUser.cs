﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpShippingListDeviceWarrantyUser : DB_SHIPPING_LIST_DEVICES_WARRANTY_User
    {
    #region Properties
        public int IdShippingList
        {
            get { return this.ID_SHIPPING_LIST; }
            set { this.ID_SHIPPING_LIST = value; }
        }

        public string ContractNbr
        {
            get { return this.CONTRACT_NBR; }
            set { this.CONTRACT_NBR = value; }
        }

        public string ShippingListNbr
        {
            get { return this.SHIPPING_LIST_NBR; }
            set { this.SHIPPING_LIST_NBR = value; }
        }

        public DateTime CreationDate
        {
            get { return this.CREATION_DATE; }
            set { this.CREATION_DATE = value; }
        }

        public long SerialNbr
        {
            get { return this.SERIAL_NBR; }
            set { this.SERIAL_NBR = value; }
        }

        public string DeviceOrderNumber
        {
            get { return this.DEVICE_ORDER_NUMBER; }
            set { this.DEVICE_ORDER_NUMBER = value; }
        }

        public int IdComponent
        {
            get { return this.ID_COMPONENT; }
            set { this.ID_COMPONENT = value; }
        }

        public DateTime? WarrantyDate
        {
            get { return this.WARRANTY_DATE; }
            set { this.WARRANTY_DATE = value; }
        }
    #endregion

    #region	Ctor
        public OpShippingListDeviceWarrantyUser()
            : base() 
        {
        }

        public OpShippingListDeviceWarrantyUser(DB_SHIPPING_LIST_DEVICES_WARRANTY_User clone)
            : base(clone)
        {
        }
    #endregion

    #region	ConvertList
        public static List<OpShippingListDeviceWarrantyUser> ConvertList(DB_SHIPPING_LIST_DEVICES_WARRANTY_User[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpShippingListDeviceWarrantyUser> ConvertList(DB_SHIPPING_LIST_DEVICES_WARRANTY_User[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpShippingListDeviceWarrantyUser> ret = new List<OpShippingListDeviceWarrantyUser>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpShippingListDeviceWarrantyUser(db_object)));
            return ret;
        }
    #endregion
    }
#endif
}
