﻿namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    public class OpCumulativeVariance
    {
        public double FlowManualGross { get; set; }
        public double FlowManualNet { get; set; }
        public double FlowCalculatedGross { get; set; }
        public double FlowCalculatedNet { get; set; }
        public double FlowMixedGross { get; set; }
        public double FlowMixedNet { get; set; }
        public double SalesManualGross { get; set; }
        public double SalesManualNet { get; set; }
        public double SalesCalculatedGross { get; set; }
        public double SalesCalcuatedNet { get; set; }
        public double SalesMixedGross { get; set; }
        public double SalesMixedNet { get; set; }
    }
#endif
}
