﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpGridControl : OpDashboardControl
    {
        [DataMember]
        public List<OpGridColumn> Columns { get; set; }
        [DataMember]
        public List<OpGridRow> DataSource { get; set; }

        public OpGridControl(OpDashboardControl DashboardControl)
            : base(DashboardControl.Type, Enums.DashboardItemClass.GRID, DashboardControl.Procedure, null, DashboardControl.Group, DashboardControl.IndexNbr, ReferenceData: DashboardControl.ReferenceData)
        {
            base.Width = DashboardControl.Width;

            this.Columns = new List<OpGridColumn>();
            this.DataSource = new List<OpGridRow>();
        }

        [DataContract]
        public class OpGridColumn
        {
            [DataMember]
            public string Name { get; private set; }
            [DataMember]
            public string Caption { get; private set; }
            [DataMember]
            public string TypeName { get; set; }
            public Type Type { get { return String.IsNullOrEmpty(this.TypeName) ? null : System.Type.GetType(TypeName); } }

            public OpGridColumn()
            {
                this.Name = null;
                this.Caption = null;
                this.TypeName = null;
            }
            public OpGridColumn(string Name, string Caption, string TypeName)
            {
                this.Name = Name;
                this.Caption = Caption;
                this.TypeName = TypeName;
            }
        }
    }
}
