﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [DataContract]
    public class OpActionResult
    {
        [DataMember]
        public long ReferenceValue { get; set; }
        [DataMember]
        public long IdAction { get; set; }
        [DataMember]
        public Enums.RunActionResult RunActionResult { get; set; }
        [DataMember]
        public string ExceptionMessage { get; set; }

        public OpActionResult(long ReferenceValue, long IdAction, Enums.RunActionResult RunActionResult, string ExceptionMessage = null)
        {
            this.ReferenceValue = ReferenceValue;
            this.IdAction = IdAction;
            this.RunActionResult = RunActionResult;
            this.ExceptionMessage = ExceptionMessage;
        }        

        public OpActionResult()
        {
        }

        public override string ToString()
        {          
            return this.RunActionResult.ToString();
        }
    }
#endif
}
