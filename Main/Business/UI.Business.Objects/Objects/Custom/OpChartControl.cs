﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpChartControl : OpDashboardControl
    {
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public List<OpChartSeries> SeriesList { get; set; }

        public OpChartControl(string Title, OpDashboardControl DashboardControl)
            : base(DashboardControl.Type, Enums.DashboardItemClass.CHART, DashboardControl.Procedure, null, DashboardControl.Group, DashboardControl.IndexNbr, ReferenceData: DashboardControl.ReferenceData)
        {
            base.Width = DashboardControl.Width;

            this.Title = Title;
            this.SeriesList = new List<OpChartSeries>();
        }

        [DataContract]
        public class OpChartSeries
        {
            [DataMember]
            public string XName { get; set; }
            [DataMember]
            public string YName { get; set; }
            [DataMember]
            public string XCaption { get; set; }
            [DataMember]
            public string YCaption { get; set; }
            [DataMember]
            public string SeriesName { get; set; }
            [DataMember]
            public string SeriesType { get; set; }
            [DataMember]
            public List<OpChartSeriesPoint> SeriesPoints { get; set; }
            [DataMember]
            public string XTypeName { get; set; }
            public Type XType { get { return String.IsNullOrEmpty(this.XTypeName) ? null : System.Type.GetType(XTypeName); } }
             [DataMember]
            public string YTypeName { get; set; }
            public Type YType { get { return String.IsNullOrEmpty(this.YTypeName) ? null : System.Type.GetType(YTypeName); } }

            public OpChartSeries()
            {
                this.SeriesPoints = new List<OpChartSeriesPoint>();
            }
        }

        [DataContract]
        public class OpChartSeriesPoint
        {
            [DataMember]
            public object XValue { get; private set; }
            [DataMember]
            public object YValue { get; private set; }

            public OpChartSeriesPoint(object XValue, object YValue)
            {
                this.XValue = XValue;
                this.YValue = YValue;
            }
        }
    }
}
