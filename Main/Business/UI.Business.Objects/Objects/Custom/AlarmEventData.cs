﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    //Klasa data nie posiadająca narazie odwzorowania w bazie danych, stąd brak dziedziczenie po DB_ALARM_DATA i wszelkich funkcjonalności z tym dziedziczeniem powiązanych
    //Stworzona na potrzeby utworzenia DataList w klasie OpActor w celu wykorzystania DataType HELPER_XXX
    //W momencie potrzeby powstania tabeli ALARM_DATA przenieść tą klasę do odpowiedniego namespace i odkomentować standardową część jej obsługi
    public class OpAlarmEventData : /*DB_ALARM_EVENT_DATA,*/ IComparable, IEquatable<OpAlarmEventData>, IOpData, IOpDataProvider
    {
        #region Properties
        [DataMember]
        public long IdAlarmEventData { get; set; }//{ get { return this.ID_ALARM_EVENT_DATA; } set { this.ID_ALARM_EVENT_DATA = value; } }
        [DataMember]
        public long IdDataType { get; set; }//{ get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [DataMember]
        public int IndexNbr { get; set; }//{ get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [DataMember]
        public object Value { get; set; }//{ get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.IdDataType = (value == null) ? 0 : (long)value.IdDataType; } }
        #endregion

        #region	Custom Properties

        [DataMember]
        public OpChangeState OpState { get; set; }
        public long IdData { get { return this.IdAlarmEventData; } set { this.IdAlarmEventData = value; } }
        public int Index { get { return this.IndexNbr; } set { this.IndexNbr = value; } }
        public object ReferencedObject { get; set; }
        #endregion

        #region	Ctor
        public OpAlarmEventData()
            : base() { this.OpState = OpChangeState.New; }

        //public OpAlarmData(DB_ALARM_DATA clone)
        //    : base(clone) { this.OpState = OpChangeState.Loaded; }
        #endregion

        #region	ToString
        public override string ToString()
        {
            //return "AlarmData [" + IdAlarmData.ToString() + "]";
            return "AlarmEventData [" + Value.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        //public static List<OpAlarmData> ConvertList(DB_ALARM_DATA[] db_objects, IDataProvider dataProvider)
        //{
        //    return ConvertList(db_objects, dataProvider, true);
        //}

        //public static List<OpAlarmData> ConvertList(DB_ALARM_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        //{
        //    List<OpAlarmData> ret = new List<OpAlarmData>(db_objects.Length);
        //    db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmData(db_object)));

        //    if (loadNavigationProperties)
        //        LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

        //    LoadCustomData(ref ret, dataProvider); // Loads user custom data
        //    return ret;
        //}
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmEventData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.IdDataType).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.IdDataType);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmEventData> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmEventData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmEventData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpAlarmData> Members
        public bool Equals(OpAlarmEventData other)
        {
            if (other == null)
                return false;
            return this.IdAlarmEventData.Equals(other.IdAlarmEventData);
        }
        #endregion

        #region IOpDataProvider

        #region Clone
        public object Clone(IDataProvider dataProvider)
        {
            OpAlarmEventData clone = new OpAlarmEventData();
            clone.IdAlarmEventData = this.IdAlarmEventData;
            clone.IdDataType = this.IdDataType;
            clone.IndexNbr = this.IndexNbr;
            clone.Value = this.Value;
            clone.IdData = this.IdData;
            clone.Index = this.Index;


            //if (dataProvider != null)
            //    clone = ConvertList(new DB_ALARM_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
        #endregion

        #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.IdDataType);
        }
        #endregion

        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmEventData)
                return this.IdAlarmEventData == ((OpAlarmEventData)obj).IdAlarmEventData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmEventData left, OpAlarmEventData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmEventData left, OpAlarmEventData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmEventData.GetHashCode();
        }
        #endregion
    }
#endif
}