﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpDashboard
    {
        [DataMember]
        public List<OpDashboardControl> Controls { get; set; }

        [DataMember]
        public bool FitToScreen { get; set; }//dopasowanie zawartości do ekranu - widok bez scroll bar

        [DataMember]
        public bool UseMargin { get; set; }

        [DataMember]
        public bool UsePadding { get; set; }


        [DataMember]
        List<int> Padding { get; set; }//top, right, bottom, left
        [DataMember]
        List<int> Margin { get; set; }//top, right, bottom, left


        [DataMember]
        public bool KeepAspectRatio { get; set; }//Zachowaj proporcje obrazu

        [DataMember]
        public int? Width { get; set; }
        [DataMember]
        public int? MaxWidth { get; set; }
        [DataMember]
        public int? MaxHeight { get; set; }

        public OpDashboard()
        {
            this.Controls = new List<OpDashboardControl>();
        }
    }
}
