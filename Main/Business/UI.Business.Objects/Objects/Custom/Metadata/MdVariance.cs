﻿namespace IMR.Suite.UI.Business.Objects.Custom.Metadata
{
	/// <summary>
	/// OpVariance Metadata
	/// </summary>
	public class MdVariance
	{
		public const string FlowErrorManualGross = "FlowErrorManualGross";
		public const string FlowErrorManualNet = "FlowErrorManualNet";
		public const string FlowErrorCalculatedGross = "FlowErrorCalculatedGross";
		public const string FlowErrorCalculatedNet = "FlowErrorCalculatedNet";
		public const string FlowErrorMixedGross = "FlowErrorMixedGross";
		public const string FlowErrorMixedNet = "FlowErrorMixedNet";
		public const string SalesErrorManualGross = "SalesErrorManualGross";
		public const string SalesErrorManualNet = "SalesErrorManualNet";
		public const string SalesErrorCalculatedGross = "SalesErrorCalculatedGross";
		public const string SalesErrorCalculatedNet = "SalesErrorCalculatedNet";
		public const string SalesErrorMixedGross = "SalesErrorMixedGross";
		public const string SalesErrorMixedNet = "SalesErrorMixedNet";
	
	}
}
	