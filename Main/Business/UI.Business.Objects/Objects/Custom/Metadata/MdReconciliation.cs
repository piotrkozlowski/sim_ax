namespace IMR.Suite.UI.Business.Objects.Custom.Metadata
{
	/// <summary>
	/// OpReconciliation Metadata
	/// </summary>
	public class MdReconciliation
	{
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string IdAggregationType = "IdAggregationType";
		public const string IdDataSourceType = "IdDataSourceType";
		public const string Day = "Day";
		public const string Status = "Status";
		public const string SalesDispencedGross = "SalesDispencedGross";
		public const string SalesDispencedNet = "SalesDispencedNet";
		public const string FlowDispencedGross = "FlowDispencedGross";
		public const string FlowDispencedNet = "FlowDispencedNet";
		public const string OutcomeGross = "OutcomeGross";
		public const string OutcomeNet = "OutcomeNet";
		public const string DeliveryManualGross = "DeliveryManualGross";
		public const string DeliveryManualNet = "DeliveryManualNet";
		public const string DeliveryCalculatedGross = "DeliveryCalculatedGross";
		public const string DeliveryCalculatedNet = "DeliveryCalculatedNet";
		public const string DeliveryMixedGross = "DeliveryMixedGross";
		public const string DeliveryMixedNet = "DeliveryMixedNet";
		public const string StartVolumeGross = "StartVolumeGross";
		public const string StartVolumeNet = "StartVolumeNet";
		public const string EndVolumeGross = "EndVolumeGross";
		public const string EndVolumeNet = "EndVolumeNet";
		public const string EndHeight = "EndHeight";
		public const string EndTemperature = "EndTemperature";
		public const string FuelDepth = "FuelDepth";
		public const string FuelTemperature = "FuelTemperature";
	
	}
}
	