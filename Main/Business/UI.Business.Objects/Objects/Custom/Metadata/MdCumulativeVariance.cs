﻿namespace IMR.Suite.UI.Business.Objects.Custom.Metadata
{
	/// <summary>
	/// OpCumulativeVariance Metadata
	/// </summary>
	public class MdCumulativeVariance
	{
		public const string FlowManualGross = "FlowManualGross";
		public const string FlowManualNet = "FlowManualNet";
		public const string FlowCalculatedGross = "FlowCalculatedGross";
		public const string FlowCalculatedNet = "FlowCalculatedNet";
		public const string FlowMixedGross = "FlowMixedGross";
		public const string FlowMixedNet = "FlowMixedNet";
		public const string SalesManualGross = "SalesManualGross";
		public const string SalesManualNet = "SalesManualNet";
		public const string SalesCalculatedGross = "SalesCalculatedGross";
		public const string SalesCalcuatedNet = "SalesCalcuatedNet";
		public const string SalesMixedGross = "SalesMixedGross";
		public const string SalesMixedNet = "SalesMixedNet";
	
	}
}
	