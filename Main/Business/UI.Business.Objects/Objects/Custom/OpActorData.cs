﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    //Klasa data nie posiadająca narazie odwzorowania w bazie danych, stąd brak dziedziczenie po DB_ACTOR_DATA i wszelkich funkcjonalności z tym dziedziczeniem powiązanych
    //Stworzona na potrzeby utworzenia DataList w klasie OpActor w celu wykorzystania DataType HELPER_XXX
    //W momencie potrzeby powstania tabeli ACTOR_DATA przenieść tą klasę do odpowiedniego namespace i odkomentować standardową część jej obsługi
    [Serializable, DataContract]
    public class OpActorData : /*DB_ACTOR_DATA,*/ IComparable, IEquatable<OpActorData>, IOpData, IOpDataProvider
    {
        #region Properties
        [DataMember]
        public long IdActorData { get; set; }//{ get { return this.ID_ACTOR_DATA; } set { this.ID_ACTOR_DATA = value; } }
        [DataMember]
        public long IdActor { get; set; }//{ get { return this.ID_ACTOR; } set { this.ID_ACTOR = value; } }
        [DataMember]
        public long IdDataType { get; set; }//{ get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [DataMember]
        public int IndexNbr { get; set; }//{ get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        [DataMember]
        public object Value { get; set; }//{ get { return this.VALUE; } set { this.VALUE = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpActor _Actor;
        public OpActor Actor { get { return this._Actor; } set { this._Actor = value; this.IdActor = (value == null) ? 0 : (long)value.IdActor; } }

        [DataMember]
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.IdDataType = (value == null) ? 0 : (long)value.IdDataType; } }
        #endregion

        #region	Custom Properties
        public object ReferencedObject { get; set; }
        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Ctor
        public OpActorData()
            : base() { }

        //public OpActorData(DB_ACTOR_DATA clone)
        //    : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "ActorData [" + IdActorData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        //public static List<OpActorData> ConvertList(DB_ACTOR_DATA[] db_objects, IDataProvider dataProvider)
        //{
        //    return ConvertList(db_objects, dataProvider, true);
        //}

        //public static List<OpActorData> ConvertList(DB_ACTOR_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        //{
        //    List<OpActorData> ret = new List<OpActorData>(db_objects.Length);
        //    db_objects.ToList().ForEach(db_object => ret.Add(new OpActorData(db_object)));

        //    if (loadNavigationProperties)
        //        LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
        //    if (loadCustomData)
        //        LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
        //    return ret;
        //}
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActorData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.IdDataType).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.IdDataType);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpActorData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActorData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActorData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpActorData> Members
        public bool Equals(OpActorData other)
        {
            if (other == null)
                return false;
            return this.IdActorData.Equals(other.IdActorData);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActorData)
                return this.IdActorData == ((OpActorData)obj).IdActorData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActorData left, OpActorData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActorData left, OpActorData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActorData.GetHashCode();
        }
        #endregion

        #region IOpData

        public long IdData { get { return IdActorData; } set { IdActorData = value; } }

        public int Index { get { return this.IndexNbr; } set { this.IndexNbr = OpDataUtil.SetNewIndex(this.IndexNbr, value, this); } }

        #endregion

        #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpActorData clone = new OpActorData();
            clone.IdData = this.IdData;
            clone.IdActor = this.IdActor;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            //if (dataProvider != null)
            //    clone = ConvertList(new DB_ARTICLE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.IdDataType);
        }


        #endregion
    }
}
