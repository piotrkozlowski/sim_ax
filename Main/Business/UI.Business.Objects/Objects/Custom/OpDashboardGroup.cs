﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpDashboardGroup
    {
        [DataMember]
        public int IdGroup { get; private set; }
        [DataMember]
        public string Name { get; private set; }
        [DataMember]
        public OpDescr Descr { get; private set; }
        [DataMember]
        public bool BorderVisible { get; private set; }
        [DataMember]
        ///Width in [%]
        public int Width { get; set; }
        [DataMember]
        ///Refresh period in [s]
        public int RefreshPeriod { get; set; }
        [DataMember]
        public Enums.Module Module { get; set; }

        public OpDashboardGroup()
        {

        }

        public OpDashboardGroup(int IdGroup, string Name, OpDescr Descr, bool BorderVisible, Enums.Module Module = Enums.Module.Unknown)
        {
            this.IdGroup = IdGroup;
            this.Name = Name;
            this.Descr = Descr;
            this.BorderVisible = BorderVisible;
            this.Module = Module;
        }

        public override string ToString()
        {
            if (this.Descr != null && this.Descr.Description != null)
                return this.Descr.Description;
            else
                return this.Name;
        }
    }
}
