﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
#if DEBUG
    [System.Diagnostics.DebuggerDisplay("Time:{Time}; Value:{Value}; DynamicProperties.Count:{DynamicProperties.Count}")]
#endif
    public class OpDynamicMeasure : IOpDynamicProperty<OpDynamicMeasure>
    {
    #region Members
        private DateTime time;
        private object value;
        private OpDynamicPropertyDict dynamicProperties;
    #endregion

    #region Properties
        public DateTime Time { get { return this.time; } set { this.time = value; } }
        public object Value { get { return this.value; } set { this.value = value; } }
    #endregion

    #region Ctor
        public OpDynamicMeasure()
        {
            this.dynamicProperties = new OpDynamicPropertyDict();
        }
    #endregion

    #region this[propertyName]
        public OpDynamicProperty this[string propertyName]
        {
            set
            {
                if (value != null)
                {
                    if (this.dynamicProperties.ContainsKey(propertyName))
                        this.dynamicProperties[propertyName] = value.Value;
                    else
                        this.dynamicProperties.AddProperty(value);
                }
            }
        }
        #endregion

        #region AddOrUpdateProperty
        public void AddOrUpdateProperty(OpDynamicProperty dynamicProperty)
        {
            dynamicProperties.AddOrUpdateProperty(dynamicProperty);
        }
        public void AddOrUpdateProperty(string propertyName, object value)
        {
            dynamicProperties.AddOrUpdateProperty(propertyName, value);
        }
        public void AddOrUpdateProperty(string propertyName, Type propertyType)
        {
            dynamicProperties.AddOrUpdateProperty(propertyName, propertyType);
        }
        public void AddOrUpdateProperty(string propertyName, Type propertyType, object value)
        {
            dynamicProperties.AddOrUpdateProperty(propertyName, propertyType, value);
        }
        public void AddOrUpdateProperty(string propertyName, Type propertyType, bool isReadOnly, object value)
        {
            dynamicProperties.AddOrUpdateProperty(propertyName, propertyType, isReadOnly, value);
        }
        #endregion

        #region ContainsPropertyKey
        public bool ContainsPropertyKey(string propertyName)
        {
            return this.dynamicProperties.ContainsKey(propertyName);
        }
        #endregion

        #region ContainsNotNullProperty
        public bool ContainsNotNullProperty(string propertyName)
        {
            if (this.dynamicProperties.ContainsKey(propertyName) && this.dynamicProperties[propertyName] != null)
                return true;
            return false;
        }
    #endregion

    #region GetPropertyValue
        public object GetPropertyValue(string propertyName)
        {
            return this.dynamicProperties.GetValue(propertyName);
        }
    #endregion
    #region GetPropertyValue<T>
        public T GetPropertyValue<T>(string propertyName) where T : struct, IConvertible
        {
            return this.dynamicProperties.GetValue<T>(propertyName);
        }
    #endregion
    #region GetNullablePropertyValue<T>
        public Nullable<T> GetNullablePropertyValue<T>(string propertyName) where T : struct, IConvertible
        {
            return this.dynamicProperties.GetNullableValue<T>(propertyName);
        }
    #endregion

    #region Implementation of IOpDynamic
        public OpDynamicPropertyDict DynamicProperties
        {
            get { if (this.dynamicProperties == null) this.dynamicProperties = new OpDynamicPropertyDict(); return this.dynamicProperties; }
            set { if (value == null) this.dynamicProperties = new OpDynamicPropertyDict(); else this.dynamicProperties = value; }
        }
        OpDynamicMeasure IOpDynamicProperty<OpDynamicMeasure>.Owner
        {
            get { return this; }
        }
    #endregion
    }
#endif
}
