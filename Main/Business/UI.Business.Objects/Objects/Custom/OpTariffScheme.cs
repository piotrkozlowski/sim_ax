﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTariffScheme
    {
        public const int OptionsCount = 3;

        public bool IsActive
        {
            get { return TariffOptionsData.TryGetValue<bool>(DataType.TARIFF_IS_ACTIVE, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_IS_ACTIVE, OrderNo, value); }
        }
        public int OrderNo { get; set; }

        public string Name
        {
            get { return TariffOptionsData.TryGetValue<string>(DataType.TARIFF_SCHEME_NAME, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_SCHEME_NAME, OrderNo, value); }
        }

        public int StartDayOfWeek
        {
            get { return TariffOptionsData.TryGetValue<int>(DataType.TARIFF_SCHEME_START_DAY_OF_WEEK, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_SCHEME_START_DAY_OF_WEEK, OrderNo, value); }
        }

        public int StartHour
        {
            get { return TariffOptionsData.TryGetValue<int>(DataType.TARIFF_SCHEME_START_HOUR, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_SCHEME_START_HOUR, OrderNo, value); }
        }

        public int StopDayOfWeek
        {
            get { return TariffOptionsData.TryGetValue<int>(DataType.TARIFF_SCHEME_END_DAY_OF_WEEK, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_SCHEME_END_DAY_OF_WEEK, OrderNo, value); }
        }

        public int StopHour
        {
            get { return TariffOptionsData.TryGetValue<int>(DataType.TARIFF_SCHEME_END_HOUR, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_SCHEME_END_HOUR, OrderNo, value); }
        }

        public double PricePrepaid
        {
            get { return TariffOptionsData.TryGetValue<double>(DataType.TARIFF_SCHEME_PRICE_PREPAID_VARIABLE, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_SCHEME_PRICE_PREPAID_VARIABLE, OrderNo, value); }
        }

        public double PricePostpaid
        {
            get { return TariffOptionsData.TryGetValue<double>(DataType.TARIFF_SCHEME_PRICE_POSTPAID_VARIABLE, OrderNo); }
            set { TariffOptionsData.SetValue(DataType.TARIFF_SCHEME_PRICE_POSTPAID_VARIABLE, OrderNo, value); }
        }

        public int IdTariff;
        public OpDataList<OpTariffData> TariffOptionsData;

        public OpTariffScheme()
            : this(null, 0)
        {
        }

        public OpTariffScheme(OpDataList<OpTariffData> list, int Index)
        {
            this.OrderNo = Index;
            TariffOptionsData = new OpDataList<OpTariffData>();
            if (list != null)
            {
                CreateOption(list, Index);
            }
        }

        public override string ToString()
        {
            string S = Name + ", ";

            S += IsActive ? "Active; " : "Inactive; ";

            S += String.Format("{0}:00 - {1}:00", StartHour, StopHour);

            return S;
        }

        public void CreateOption(OpDataList<OpTariffData> list, int Index)
        {
            //TariffOptionsData = new OpDataList<OpTariffData>();
            TariffOptionsData.AddRange(list.FindAll(d => d.Index == Index));
            /*
            OrderNo = Index;
            foreach (OpTariffData loop in list)
            {
                if (loop.Index == Index)
                {
                    bool IsDTRelevant = true;
                    switch (loop.IdDataType)
                    {
                        case DataType.TARIFF_SCHEME_START_HOUR:
                            StartHour = Convert.ToInt32(loop.Value);
                            break;

                        case DataType.TARIFF_SCHEME_END_HOUR:
                            StopHour = Convert.ToInt32(loop.Value);
                            break;

                        case DataType.TARIFF_SCHEME_START_DAY_OF_WEEK:
                            StartDayOfWeek = Convert.ToInt32(loop.Value);
                            break;

                        case DataType.TARIFF_SCHEME_END_DAY_OF_WEEK:
                            StopDayOfWeek = Convert.ToInt32(loop.Value);
                            break;

                        case DataType.TARIFF_SCHEME_NAME:
                            Name = Convert.ToString(loop.Value);
                            break;

                        case DataType.TARIFF_SCHEME_PRICE_PREPAID_VARIABLE:
                            PricePrepaid = Convert.ToDouble(loop.Value);
                            break;

                        case DataType.TARIFF_SCHEME_PRICE_POSTPAID_VARIABLE:
                            PricePostpaid = Convert.ToDouble(loop.Value);
                            break;

                            
                        case DataType.TARIFF_IS_ACTIVE:
                            IsActive = Convert.ToBoolean(loop.Value);
                            //TariffOptionsData.Add(loop);
                            break;

                        default:
                            IsDTRelevant = false;
                            break;
                    }

                    if (IsDTRelevant)
                        TariffOptionsData.Add(loop);
                }
            }
             * */
        }
    }
#endif
}
