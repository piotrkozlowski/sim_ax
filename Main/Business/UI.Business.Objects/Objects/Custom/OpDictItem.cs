﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [DataContract, Serializable]
    public class OpDictItem
    {
        [DataMember]
        public string Key { get; set; }
        [DataMember]
        public object Value { get; set; }

        public OpDictItem() : base()
        {
        }
    }
#endif
}
