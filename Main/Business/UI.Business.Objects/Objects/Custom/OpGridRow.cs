﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    //tymczasowe miejsce na tą klase. Testuje rozwiazanie. Jeśli sie sprawdzi przejdzie do UI.Business
    //Nazwa klasy też jeszcze nie jest do końca chyba trafna :)

    [DataContract]
    public class OpGridRow : IEquatable<OpGridRow>
    {
        #region Members
        /// <summary>
        /// Key/Value pair (FielName/FieldValue)
        /// </summary>
        [DataMember]
        public Dictionary<string, object> Values { get; set; }
        #endregion

        #region Properties - Key
        [DataMember]
        public object Key { get; set; }
    #endregion

    #region Ctor
        public OpGridRow(object key)
        {
            this.Key = key;
            Values = new Dictionary<string, object>();
        }

        public OpGridRow(object key, Dictionary<string, object> values)
            : this(key)
        {
            if (values != null)
            {
                Values = values;
            }
        }
    #endregion

    #region GetValue
        public object GetValue(string fieldName)
        {
            return Values.TryGetValue(fieldName);
        }
    #endregion
    #region TryGetValue
        public T TryGetValue<T>(long fielName) where T : struct, IConvertible
        {
            return TryGetValue<T>(fielName.ToString());
        }
        public T TryGetValue<T>(string fielName) where T : struct, IConvertible
        {
            object obj;
            if (Values.TryGetValue(fielName, out obj))
                return GenericConverter.Parse<T>(obj);

            return default(T);
        }
    #endregion
    #region TryGetNullableValue
        public Nullable<T> TryGetNullableValue<T>(long fielName) where T : struct, IConvertible
        {
            return TryGetNullableValue<T>(fielName.ToString());
        }
        public Nullable<T> TryGetNullableValue<T>(string fielName) where T : struct, IConvertible
        {
            object obj;
            if (Values.TryGetValue(fielName, out obj))
                return GenericConverter.Parse<T>(obj);

            return null;
        }
    #endregion

    #region this[string fieldName]
        public object this[string fieldName]
        {
            get { return GetValue(fieldName); }
            set { Values[fieldName] = value; }
        }
    #endregion
    #region this[long idDataType]
        public object this[long idDataType]
        {
            get { return GetValue(idDataType.ToString()); }
            set { Values[idDataType.ToString()] = value; }
        }
    #endregion

    #region ToString()
        public override string ToString()
        {
            return String.Format("{0}", Key);
        }
    #endregion

    #region IEquatable<OpGridRow> Members
        public bool Equals(OpGridRow other)
        {
            if (other != null)
                return this.Key.Equals(other.Key);
            else
                return false;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpGridRow)
                return this.Key == ((OpGridRow)obj).Key;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpGridRow left, OpGridRow right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpGridRow left, OpGridRow right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }
    #endregion
    }

    public class OpGridRowList : List<OpGridRow>
    {
        private Dictionary<object, OpGridRow> itemsDict;

    #region Properties
        public Dictionary<object, OpGridRow> ItemsDict
        {
            get { return itemsDict; }
        }
    #endregion

    #region Ctor
        public OpGridRowList()
        {
            itemsDict = new Dictionary<object, OpGridRow>();
        }
    #endregion

    #region Implementation of ICollection<OpGridRow>
        new public void Add(OpGridRow item)
        {
            if (!itemsDict.ContainsKey(item.Key))
            {
                base.Add(item);
                itemsDict[item.Key] = item; //dodajemy element do slownika
            }
            else
                throw new ArgumentException("Item with the same key already exists.");
        }

        new public void Clear()
        {
            base.Clear();
            itemsDict.Clear();
        }

        new public bool Contains(OpGridRow item)
        {
            return itemsDict.ContainsKey(item.Key);
        }

        new public void CopyTo(OpGridRow[] array, int arrayIndex)
        {
            base.CopyTo(array, arrayIndex);
        }

        new public bool Remove(OpGridRow item)
        {
            itemsDict.Remove(item.Key);
            return base.Remove(item);
        }

        new public int Count
        {
            get { return base.Count; }
        }
    #endregion
    #region Implementation of IList<OpGridRow>
        new public int IndexOf(OpGridRow item)
        {
            return base.IndexOf(item);
        }

        new public void Insert(int index, OpGridRow item)
        {
            if (!itemsDict.ContainsKey(item.Key))
            {
                base.Insert(index, item);
                itemsDict.Add(item.Key, item);
            }
            else
                throw new ArgumentException("Item with the same key already exists.");
        }

        new public void RemoveAt(int index)
        {
            if (index >= 0 && index < base.Count)
            {
                OpGridRow item = base[index];
                itemsDict.Remove(item.Key);
                base.RemoveAt(index);
            }
        }

        new public OpGridRow this[int index]
        {
            get { return base[index]; }
            set
            {
                base[index] = value;
                itemsDict[value.Key] = value;
            }
        }
    #endregion

    #region ContainsKey
        public bool ContainsKey(object key)
        {
            return itemsDict.ContainsKey(key);
        }
    #endregion
    #region this[object key]
        public OpGridRow this[object key]
        {
            get { return itemsDict.TryGetValue(key); }
            set { itemsDict[key] = value; }
        }
    #endregion
    }
#endif
}
