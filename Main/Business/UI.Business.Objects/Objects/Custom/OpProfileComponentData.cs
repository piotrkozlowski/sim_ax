﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [Serializable, DataContract]
    public class OpProfileComponentData : IEquatable<OpProfileComponentData>
    {
        #region Properties
        [DataMember]
        public long? IdMeter { get; set; }
        [DataMember]
        public long? IdLocation { get; set; }
        [DataMember]
        public int? IdDistributor { get; set; }
        [DataMember]
        public long IdDataType { get; set; }
        [DataMember]
        public int? Index { get; set; }
        [DataMember]
        public object Value { get; set; }
        [DataMember]
        public object DefaultValue { get; set; }
        [DataMember]
        public List<Enums.Module> Modules { get; set; }
        #endregion

        #region	Ctor
        public OpProfileComponentData(long? idMeter, long? idLocation, int? idDistributor, long idDataType, int? index, object value, object defaultValue = null)
        {
            IdMeter = idMeter;
            IdLocation = idLocation;
            IdDistributor = idDistributor;
            IdDataType = idDataType;
            Index = index;
            Value = value;
            DefaultValue = defaultValue;
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "ProfileComponentData [IdDataType=" + IdDataType.ToString() + "]";
        }
        #endregion
        #region Equality members
        public bool Equals(OpProfileComponentData other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;
            return IdMeter == other.IdMeter && IdLocation == other.IdLocation && IdDistributor == other.IdDistributor && IdDataType == other.IdDataType && Index == other.Index && GetModulesHashCode() == other.GetModulesHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj is OpProfileComponentData)
                return Equals((OpProfileComponentData)obj);
            else
                return base.Equals(obj);

        }
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = IdMeter.GetHashCode();
                hashCode = (hashCode * 397) ^ IdLocation.GetHashCode();
                hashCode = (hashCode * 397) ^ IdDistributor.GetHashCode();
                hashCode = (hashCode * 397) ^ IdDataType.GetHashCode();
                hashCode = (hashCode * 397) ^ Index.GetHashCode();
                hashCode = (hashCode * 397) ^ GetModulesHashCode();
                return hashCode;
            }
        }
        public static bool operator ==(OpProfileComponentData left, OpProfileComponentData right)
        {
            return Equals(left, right);
        }
        public static bool operator !=(OpProfileComponentData left, OpProfileComponentData right)
        {
            return !Equals(left, right);
        }
        private int GetModulesHashCode()
        {
            unchecked
            {
                if (Modules == null)
                    return 0;
                int hashCode = 17;
                Modules.ForEach(m => hashCode = (hashCode * 397) ^ m.GetHashCode());
                return hashCode;
            }
        }
        #endregion

    }
}
