﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceSimCard : DB_DEVICE_SIM_CARD
    {
    #region Properties
        public int? IdSimCard
        {
            get { return this.ID_SIM_CARD; }
            set { this.ID_SIM_CARD = value; }
        }

        public long DeviceSerialNbr
        {
            get { return this.DEVICE_SERIAL_NBR; }
            set { this.DEVICE_SERIAL_NBR = value; }
        }

        public int IdDistributor
        {
            get { return this.ID_DISTRIBUTOR; }
            set { this.ID_DISTRIBUTOR = value; }
        }

        public string Phone
        {
            get { return this.PHONE; }
            set { this.PHONE = value; }
        }

        public int? IdTariff
        {
            get { return this.ID_TARIFF; }
            set { this.ID_TARIFF = value; }
        }

        public string TariffName
        {
            get { return this.TARIFF_NAME; }
            set { this.TARIFF_NAME = value; }
        }

        public float? TariffTax
        {
            get { return this.TARIFF_TAX; }
            set { this.TARIFF_TAX = value; }
        }
    #endregion

    #region	Ctor
        public OpDeviceSimCard()
            : base() 
        {
        }

        public OpDeviceSimCard(DB_DEVICE_SIM_CARD clone)
            : base(clone)
        {
        }

    #endregion

    #region	ConvertList
        public static List<OpDeviceSimCard> ConvertList(DB_DEVICE_SIM_CARD[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceSimCard> ConvertList(DB_DEVICE_SIM_CARD[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceSimCard> ret = new List<OpDeviceSimCard>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceSimCard(db_object)));
            return ret;
        }
    #endregion
    }
#endif
}
