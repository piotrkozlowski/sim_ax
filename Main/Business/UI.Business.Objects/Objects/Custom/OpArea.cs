﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using IMR.Suite.UI.Business.Components.CORE;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;



namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    //Unable to derive location - would create duplicated locations.
    [Serializable]
    public class OpArea : IComparable, IEquatable<OpArea>
    {
        public OpLocation Location { get; set; }
        public int NoOfLocations { get { return Locations.Count; } }
        public int NoOfDevices { get { return Devices.Count; } }
        public string DevicesString { get { String s = ""; if (Devices.Count == 0) return " - "; foreach (var loop in Devices) { s += " " + loop.ToString(); } return s; } }
        public OpDistributorSupplier CurrentSupplier { get; set; }
        //public OpProductCode()

        public List<OpDevice> Devices;
        public List<OpLocation> Locations;

        public string Status
        {
            get { return "Operational"; }
        }

        public OpArea()
        {
            Devices = new List<OpDevice>();
            Locations = new List<OpLocation>();
        }

        public override string ToString()
        {
            if (Location == null)
                return "[empty area]";

            return Location.ToString();
        }

        public static List<OpArea> CreateAreas(IDataProvider dataProvider)
        {
            Dictionary<long, OpArea> Dict = new Dictionary<long, OpArea>();

            if (dataProvider.GetAll(exludeDeletedAndUnused: true).Count == 0)
                return Dict.Select(t => t.Value).ToList();
            
            Dict = dataProvider.GetAll(exludeDeletedAndUnused: true)
                .Where(t => t.IdLocationType.In((int)Enums.LocationType.PrepaidAreaLocation, (int)Enums.LocationType.SgmGroupingLocationType1, (int)Enums.LocationType.SgmGroupingLocationType2
                    , (int)Enums.LocationType.SgmGroupingLocationType3, (int)Enums.LocationType.SgmGroupingLocationType4, (int)Enums.LocationType.SgmGroupingLocationType5))
                .Select(t => 
                    {
                        if (dataProvider.AreaCount > 0)
                        {
                            var areaLocation = dataProvider.Area.Find(a => a.Location.IdLocation == t.IdLocation);
                            if (areaLocation != null)
                            {
                                areaLocation.Locations.Clear();
                                return areaLocation;
                            }
                        }
                        return new OpArea() { Location = t };
                        
                    }).ToDictionary(t => t.Location.IdLocation);



            foreach (var loop in dataProvider.GetAll(exludeDeletedAndUnused: true)) loop.Tag = null;


            foreach (var loop in dataProvider.GetAllLocationHierarchy())
            {
                if (loop.LocationHierarchyParent != null && loop.Location != null)
                {
                    loop.Location.Tag = loop.LocationHierarchyParent.Location;
                    if (Dict.ContainsKey(loop.LocationHierarchyParent.IdLocation))
                    {
                        if (Dict[loop.LocationHierarchyParent.IdLocation].Location.IdLocationType == (int)Enums.LocationType.PrepaidAreaLocation)
                        {
                            dataProvider.GetLocation(loop.IdLocation).CurrentArea = Dict[loop.LocationHierarchyParent.IdLocation];
                        }
                        Dict[loop.LocationHierarchyParent.IdLocation].Locations.Add(dataProvider.GetLocation(loop.IdLocation));
                    }
                }
            }

            //Assign devices to areas
            List<OpLocationEquipment> LE = dataProvider.GetAllLocationEquipment();
            //OpLocationEquipment.LoadNavigationProperties(ref LE, dataProvider, false);
            foreach (var loop in LE)
            {
                if (loop.IdLocation.HasValue)
                {
                    OpLocation loc = dataProvider.GetLocation(loop.IdLocation.Value);
                    if (loc != null && loc.CurrentArea != null && loop.Device != null && loop.EndTime == null)
                    {
                        if (!loc.CurrentArea.Devices.Contains(loop.Device))
                            loc.CurrentArea.Devices.Add(loop.Device);
                    }
                }
            }

            return Dict.Select(t => t.Value).ToList();
        }



        public int CompareTo(object obj)
        {
            if (this.Location == null || !(obj is OpArea) || (obj as OpArea).Location == null)
                return 0;

            return this.Location.IdLocation.CompareTo((obj as OpArea).Location.IdLocation);
        }

        public bool Equals(OpArea other)
        {
            if (this.Location == null || other.Location == null)
                return false;
            return (this.Location.IdLocation == other.Location.IdLocation);
        }
    }
#endif
}
