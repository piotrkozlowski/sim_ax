﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Data;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    //Klasa data nie posiadająca narazie odwzorowania w bazie danych, stąd brak dziedziczenie po DB_DEVICE_CONNECTION_DATA i wszelkich funkcjonalności z tym dziedziczeniem powiązanych
    //Stworzona na potrzeby utworzenia DataList w klasie OpDeviceConnection w celu wykorzystania DataType HELPER_XXX
    //W momencie potrzeby powstania tabeli DB_DEVICE_CONNECTION_DATA przenieść tą klasę do odpowiedniego namespace i odkomentować standardową część jej obsługi
    public class OpDeviceConnectionData : /*DB_DEVICE_CONNECTION_DATA,*/ IComparable, IEquatable<OpDeviceConnectionData>, IOpData, IOpDataProvider
    {
                #region Properties
        [DataMember]
        public long IdDeviceConnectionData { get; set; }//{ get { return this.ID_DEVICE_CONNECTION_DATA; } set { this.ID_DEVICE_CONNECTION_DATA = value; } }
        [DataMember]
        public long IdDeviceConnection { get; set; }//{ get { return this.ID_DEVICE_CONNECTION; } set { this.ID_DEVICE_CONNECTION = value; } }
        [DataMember]
        public long IdDataType { get; set; }//{ get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [DataMember]
        public int IndexNbr { get; set; }//{ get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        [DataMember]
        public object Value { get; set; }//{ get { return this.VALUE; } set { this.VALUE = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpDeviceConnection _DeviceConnection;
        public OpDeviceConnection DeviceConnection { get { return this._DeviceConnection; } set { this._DeviceConnection = value; this.IdDeviceConnection = (value == null) ? 0 : (long)value.IdDeviceConnection; } }

        [DataMember]
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.IdDataType = (value == null) ? 0 : (long)value.IdDataType; } }
        #endregion

        #region	Custom Properties
        public object ReferencedObject { get; set; }
        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Ctor
        public OpDeviceConnectionData()
            : base() { }

        //public OpDeviceConnectionData(DB_DEVICE_CONNECTION_DATA clone)
        //    : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "DeviceConnectionData [" + IdDeviceConnectionData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        //public static List<OpDeviceConnectionData> ConvertList(DB_DEVICE_CONNECTION_DATA[] db_objects, IDataProvider dataProvider)
        //{
        //    return ConvertList(db_objects, dataProvider, true);
        //}

        //public static List<OpDeviceConnectionData> ConvertList(DB_DEVICE_CONNECTION_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        //{
        //    List<OpDeviceConnectionData> ret = new List<OpDeviceConnectionData>(db_objects.Length);
        //    db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceConnectionData(db_object)));

        //    if (loadNavigationProperties)
        //        LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
        //    if (loadCustomData)
        //        LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
        //    return ret;
        //}
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceConnectionData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.IdDataType).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.IdDataType);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceConnectionData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceConnectionData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceConnectionData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpDeviceConnectionData> Members
        public bool Equals(OpDeviceConnectionData other)
        {
            if (other == null)
                return false;
            return this.IdDeviceConnectionData.Equals(other.IdDeviceConnectionData);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceConnectionData)
                return this.IdDeviceConnectionData == ((OpDeviceConnectionData)obj).IdDeviceConnectionData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceConnectionData left, OpDeviceConnectionData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceConnectionData left, OpDeviceConnectionData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceConnectionData.GetHashCode();
        }
        #endregion

        #region IOpData

        public long IdData { get { return IdDeviceConnectionData; } set { IdDeviceConnectionData = value; } }

        public int Index { get { return this.IndexNbr; } set { this.IndexNbr = OpDataUtil.SetNewIndex(this.IndexNbr, value, this); } }

        #endregion

        #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpDeviceConnectionData clone = new OpDeviceConnectionData();
            clone.IdData = this.IdData;
            clone.IdDeviceConnection = this.IdDeviceConnection;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            //if (dataProvider != null)
            //    clone = ConvertList(new DB_DEVICE_CONNECTION_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.IdDataType);
        }


        #endregion
    }
}
