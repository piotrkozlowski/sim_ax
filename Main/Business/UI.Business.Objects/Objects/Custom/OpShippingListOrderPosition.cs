﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    public class OpShippingListOrderPosition
    {
        public string Name { get; set; }
        
        public int Count { get; set; }
        
        public float Price { get; set; }
        
        public float Sum
        {
            get
            {
                return this.Count * this.Price;
            }
        }
    }
}
