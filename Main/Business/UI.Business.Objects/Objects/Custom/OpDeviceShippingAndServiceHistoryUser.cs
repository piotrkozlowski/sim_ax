﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.Custom
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceShippingAndServiceHistoryUser : DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User
    {
    #region Properties
        public RowTypeEnum RowType
        {
            get { return (RowTypeEnum)this.ROW_TYPE; }
            set { this.ROW_TYPE = (int)value; }
        }

        public DateTime EventDate
        {
            get { return this.EVENT_DATE; }
            set { this.EVENT_DATE = value; }
        }

        public string Descr
        {
            get { return this.DESCR; }
            set { this.DESCR = value; }
        }

        public string LongDescr
        {
            get { return this.LONG_DESCR; }
            set { this.LONG_DESCR = value; }
        }

        public DateTime? WarrantyDate
        {
            get { return this.WARRANTY_DATE; }
            set { this.WARRANTY_DATE = value; }
        }

    #endregion

        public enum RowTypeEnum
        {
            ShippedToClient = 1,
            ApprovedForService = 2,
            ServicePerformed = 3,
            MountedInLocation = 4,
            RemoveFromLocation = 5
        }

    #region	Ctor
        public OpDeviceShippingAndServiceHistoryUser()
            : base() 
        {
        }

        public OpDeviceShippingAndServiceHistoryUser(DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User clone)
            : base(clone)
        {
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceShippingAndServiceHistoryUser> ConvertList(DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceShippingAndServiceHistoryUser> ConvertList(DB_DEVICE_SHIPPING_AND_SERVICE_HISTORY_User[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceShippingAndServiceHistoryUser> ret = new List<OpDeviceShippingAndServiceHistoryUser>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceShippingAndServiceHistoryUser(db_object)));
            return ret;
        }
    #endregion
    }
#endif
}
