﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpMapControl : OpDashboardControl
    {
        [DataMember]
        public List<OpMarker> Markers { get; set; }
        [DataMember]
        public List<OpRange> Ranges { get; set; }
        [DataMember]
        public List<OpPolygon> Polygons { get; set; }
        [DataMember]
        public Dictionary<long, byte[]> IconDict { get; set; }

        public OpMapControl(OpDashboardControl DashboardControl)
            : base(DashboardControl.Type, Enums.DashboardItemClass.MAP, DashboardControl.Procedure, null, DashboardControl.Group, DashboardControl.IndexNbr, ReferenceData: DashboardControl.ReferenceData)
        {
            base.Width = DashboardControl.Width;

            this.Markers = new List<OpMarker>();
            this.Ranges = new List<OpRange>();
            this.Polygons = new List<OpPolygon>();
            this.IconDict = new Dictionary<long, byte[]>();
        }

        [DataContract]
        public class OpMarker
        {
            [DataMember]
            public OpCoord Coords { get; private set; }
            [DataMember]
            public string Tooltip { get; set; }
            [DataMember]
            public Color? Color { get; set; }
            [DataMember]
            public byte[] Icon { get; set; }
            [DataMember]
            public string IconUrl { get; set; }
            [DataMember]
            public long? IconIndex { get; set; }
            [DataMember]
            public int Id { get; set; }

            public OpMarker()
            {
            }
            public OpMarker(OpCoord Coords)
            {
                this.Coords = Coords;
            }
            public OpMarker(double Latitude, double Longitude)
            {
                this.Coords = new OpCoord(Latitude, Longitude);
            }
        }

        [DataContract]
        public class OpRange
        {
            [DataMember]
            public OpCoord Coords { get; private set; }
            [DataMember]
            public double Radius { get; private set; }
            [DataMember]
            public string Tooltip { get; set; }
            [DataMember]
            public Color? LineColor { get; set; }
            [DataMember]
            public Color? FillColor { get; set; }
            [DataMember]
            public int Id { get; set; }
            public OpRange()
            {
            }
            public OpRange(OpCoord Coords, double Radius)
            {
                this.Coords = Coords;
                this.Radius = Radius;
            }
            public OpRange(double Latitude, double Longitude, double Radius)
            {
                this.Coords = new OpCoord(Latitude, Longitude);
                this.Radius = Radius;
            }
        }

        [DataContract]
        public class OpPolygon
        {
            [DataMember]
            public List<OpCoord> Coords { get; set; }
            [DataMember]
            public string Tooltip { get; set; }
            [DataMember]
            public Color? LineColor { get; set; }
            [DataMember]
            public Color? FillColor { get; set; }
            [DataMember]
            public int Id { get; set; }

            public OpPolygon()
            {
                this.Coords = new List<OpCoord>();
            }
        }

        [DataContract]
        public class OpCoord
        {
            [DataMember]
            public double Latitude { get; private set; }
            [DataMember]
            public double Longitude { get; private set; }
            public OpCoord()
            {
            }
            public OpCoord(double Latitude, double Longitude)
            {
                this.Latitude = Latitude;
                this.Longitude = Longitude;
            }
        }
    }
}
