﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace IMR.Suite.UI.Business.Objects.Custom
{
    [DataContract]
    public class OpHistory
    {
        [DataMember]
        public DateTime EventDate { get; private set; }
        [DataMember]
        private Enums.ObjectHistoryEventType ObjectHistoryEventType { get; set; }
        [DataMember]
        public OpEnumObject EventType { get; private set; }

        [DataMember]
        public string Descr { get; private set; }
        [DataMember]
        public string LongDescr { get; private set; }

        public object EventObject { get; set; }

        public OpHistory(DateTime EventDate, Enums.ObjectHistoryEventType ObjectHistoryEventType,
            string Descr, string LongDescr)
        {
            this.EventDate = EventDate;
            this.ObjectHistoryEventType = ObjectHistoryEventType;
            this.EventType = new OpEnumObject(ObjectHistoryEventType);

            this.Descr = Descr;
            this.LongDescr = LongDescr;
        }

    }
    
}
