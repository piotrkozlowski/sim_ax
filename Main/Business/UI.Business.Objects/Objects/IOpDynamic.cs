﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects;

namespace UI.Business.Objects
{
    [Obsolete("Interface will be removed, don't use it!", false)]
    public interface IOpDynamic
    {
        object Key { get; }
        Dictionary<string, object> DynamicValues { get; set; }
        object Owner { get; }
    }

    public interface IOpDynamicProperty<T>
    {
        OpDynamicPropertyDict DynamicProperties { get; set; }
        T Owner { get; }
    }
}
