using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DAQ;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataArchTrash : DB_DATA_ARCH_TRASH, IComparable, IEquatable<OpDataArchTrash>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdDataArchTrash { get { return this.ID_DATA_ARCH_TRASH; } set { this.ID_DATA_ARCH_TRASH = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int? IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public long IdPacket { get { return this.ID_PACKET; } set { this.ID_PACKET = value; } }
        public bool IsAction { get { return this.IS_ACTION; } set { this.IS_ACTION = value; } }
        public bool IsAlarm { get { return this.IS_ALARM; } set { this.IS_ALARM = value; } }
        public int? IdDataArchTrashStatus { get { return this.ID_DATA_ARCH_TRASH_STATUS; } set { this.ID_DATA_ARCH_TRASH_STATUS = value; } }
    #endregion

    #region	Navigation Properties
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpPacket _Packet;
        public OpPacket Packet { get { return this._Packet; } set { this._Packet = value; this.ID_PACKET = (value == null) ? 0 : (long)value.ID_PACKET; } }
        private OpDataArchTrashStatus _DataArchTrashStatus;
        public OpDataArchTrashStatus DataArchTrashStatus { get { return this._DataArchTrashStatus; } set { this._DataArchTrashStatus = value; this.ID_DATA_ARCH_TRASH_STATUS = (value == null) ? null : (int?)value.ID_DATA_ARCH_TRASH_STATUS; } }
    #endregion

    #region	Custom Properties
        public long IdData { get { return IdDataArchTrash; } set { IdDataArchTrash = value; } }
        public int Index { get; set; }
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpDataArchTrash()
            : base() { }

        public OpDataArchTrash(DB_DATA_ARCH_TRASH clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DataArchTrash [" + IdDataArchTrash.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDataArchTrash> ConvertList(DB_DATA_ARCH_TRASH[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataArchTrash> ConvertList(DB_DATA_ARCH_TRASH[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDataArchTrash> ret = new List<OpDataArchTrash>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataArchTrash(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataArchTrash> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<long, OpPacket> PacketDict = dataProvider.GetPacket(list.Select(l => l.ID_PACKET).Distinct().ToArray()).ToDictionary(l => l.ID_PACKET);
                Dictionary<int, OpDataArchTrashStatus> DataArchTrashStatusDict = dataProvider.GetDataArchTrashStatus(list.Where(l => l.ID_DATA_ARCH_TRASH_STATUS.HasValue).Select(l => l.ID_DATA_ARCH_TRASH_STATUS.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_ARCH_TRASH_STATUS);
                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    loop.Packet = PacketDict.TryGetValue(loop.ID_PACKET);
                    if (loop.ID_DATA_ARCH_TRASH_STATUS.HasValue)
                        loop.DataArchTrashStatus = DataArchTrashStatusDict.TryGetValue(loop.ID_DATA_ARCH_TRASH_STATUS.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataArchTrash> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataArchTrash)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataArchTrash).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataArchTrash> Members
        public bool Equals(OpDataArchTrash other)
        {
            if (other == null)
                return false;
            return this.IdDataArchTrash.Equals(other.IdDataArchTrash);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataArchTrash)
                return this.IdDataArchTrash == ((OpDataArchTrash)obj).IdDataArchTrash;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataArchTrash left, OpDataArchTrash right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataArchTrash left, OpDataArchTrash right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataArchTrash.GetHashCode();
        }
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDataArchTrash clone = new OpDataArchTrash();
            clone.IdData = this.IdData;
            clone.SerialNbr = this.SerialNbr;
            clone.IdDataType = this.IdDataType;
            clone.IndexNbr = this.IndexNbr;
            clone.Value = this.Value;
            clone.Time = this.Time;
            clone.IdPacket = this.IdPacket;
            clone.IsAction = this.IsAction;
            clone.IsAlarm = this.IsAlarm;
            clone.IdDataArchTrashStatus = this.IdDataArchTrashStatus;
            clone.ReferencedObject = this.ReferencedObject;

            if (dataProvider != null)
                clone = ConvertList(new DB_DATA_ARCH_TRASH[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion
    }
#endif
}