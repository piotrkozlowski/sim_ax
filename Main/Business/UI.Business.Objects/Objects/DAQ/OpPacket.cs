using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DAQ;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPacket : DB_PACKET, IComparable, IEquatable<OpPacket>
    {
    #region Properties
        public long IdPacket { get { return this.ID_PACKET; } set { this.ID_PACKET = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public string Address { get { return this.ADDRESS; } set { this.ADDRESS = value; } }
        public int IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = value; } }
        public bool IsIncoming { get { return this.IS_INCOMING; } set { this.IS_INCOMING = value; } }
        public int IdTransmissionType { get { return this.ID_TRANSMISSION_TYPE; } set { this.ID_TRANSMISSION_TYPE = value; } }
        public int TransmittedPackets { get { return this.TRANSMITTED_PACKETS; } set { this.TRANSMITTED_PACKETS = value; } }
        public int Bytes { get { return this.BYTES; } set { this.BYTES = value; } }
        public int TransmittedBytes { get { return this.TRANSMITTED_BYTES; } set { this.TRANSMITTED_BYTES = value; } }
        public byte[] Body { get { return this.BODY; } set { this.BODY = value; } }
    #endregion

    #region	Navigation Properties
        private OpTransmissionDriver _TransmissionDriver;
        public OpTransmissionDriver TransmissionDriver { get { return this._TransmissionDriver; } set { this._TransmissionDriver = value; this.ID_TRANSMISSION_DRIVER = (value == null) ? 0 : (int)value.ID_TRANSMISSION_DRIVER; } }
        private OpTransmissionType _TransmissionType;
        public OpTransmissionType TransmissionType { get { return this._TransmissionType; } set { this._TransmissionType = value; this.ID_TRANSMISSION_TYPE = (value == null) ? 0 : (int)value.ID_TRANSMISSION_TYPE; } }
    #endregion

    #region	Custom Properties
        public DateTime? LastUpdate
        {
            get
            {
                if (PacketHistory.Count == 0)
                    return null;
                return PacketHistory.Max().Time;
            }
        }
        public OpPacketStatus PacketStatus
        {
            get
            {
                if (PacketHistory.Count == 0)
                    return null;
                return PacketHistory.Max().PacketStatus;
            }
        }
        public string BodyHex
        {
            get
            {
                string result = "";
                foreach (Byte b in Body)
                {
                    result += b.ToString("X2"); // convert byte to hex
                }
                result = "0x" + result.ToUpper();
                return result;
            }
        }

        public List<OpPacketHistory> PacketHistory = new List<OpPacketHistory>();
    #endregion

    #region	Ctor
        public OpPacket()
            : base() { }

        public OpPacket(DB_PACKET clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.ADDRESS;
        }
    #endregion

    #region	ConvertList
        public static List<OpPacket> ConvertList(DB_PACKET[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPacket> ConvertList(DB_PACKET[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool useDBCollector = false)
        {
            List<OpPacket> ret = new List<OpPacket>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPacket(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, useDBCollector); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPacket> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionDriver> TransmissionDriverDict = dataProvider.GetTransmissionDriver(list.Select(l => l.ID_TRANSMISSION_DRIVER).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER);
                Dictionary<int, OpTransmissionType> TransmissionTypeDict = dataProvider.GetTransmissionType(list.Select(l => l.ID_TRANSMISSION_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_TYPE);

                foreach (var loop in list)
                {
                    loop.TransmissionDriver = TransmissionDriverDict.TryGetValue(loop.ID_TRANSMISSION_DRIVER);
                    loop.TransmissionType = TransmissionTypeDict.TryGetValue(loop.ID_TRANSMISSION_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPacket> list, IDataProvider dataProvider, bool useDBCollector = false)
        {
            if (list != null && list.Count > 0)
            {
                List<long> idPackets = list.Select(l => l.IdPacket).ToList();

                List<OpPacketHistory> data = dataProvider.GetPacketHistoryFilter(IdPacket: idPackets.ToArray(), useDBCollector: useDBCollector);
                if (data != null && data.Count > 0)
                {
                    if (!useDBCollector)
                    {
                        Dictionary<long, OpPacket> packetDict = list.ToDictionary<OpPacket, long>(l => l.IdPacket);
                        foreach (OpPacketHistory packetHistory in data)
                        {
                            packetDict[packetHistory.IdPacket].PacketHistory.Add(packetHistory);
                        }
                    }
                    else
                    {
                        foreach (var serverGroup in list.GroupBy(q => q.ID_SERVER))
                        {
                            Dictionary<long, OpPacket> packetDict = serverGroup.ToDictionary<OpPacket, long>(l => l.IdPacket);
                            foreach (OpPacketHistory packetHistory in data.Where(q => q.ID_SERVER == serverGroup.Key))
                            {
                                packetDict[packetHistory.IdPacket].PacketHistory.Add(packetHistory);
                            }
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPacket)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPacket).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPacket> Members
        public bool Equals(OpPacket other)
        {
            if (other == null)
                return false;
            return this.IdPacket.Equals(other.IdPacket);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPacket)
                return this.IdPacket == ((OpPacket)obj).IdPacket;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPacket left, OpPacket right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPacket left, OpPacket right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPacket.GetHashCode();
        }
    #endregion
    }
#endif
}