using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DAQ;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTransmissionProtocol : DB_TRANSMISSION_PROTOCOL, IComparable, IEquatable<OpTransmissionProtocol>, IOpChangeState
    {
    #region Properties
        public int IdTransmissionProtocol { get { return this.ID_TRANSMISSION_PROTOCOL; } set { this.ID_TRANSMISSION_PROTOCOL = OpDataUtil.SetNewIndex(this.ID_TRANSMISSION_PROTOCOL, value, this); } }
        public int IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = OpDataUtil.SetNewIndex(this.ID_TRANSMISSION_DRIVER, value, this); } }
        public int Order { get { return this.ORDER; } set { this.ORDER = OpDataUtil.SetNewIndex(this.ORDER, value, this); } }
        public int IdProtocolDriver { get { return this.ID_PROTOCOL_DRIVER; } set { this.ID_PROTOCOL_DRIVER = OpDataUtil.SetNewIndex(this.ID_PROTOCOL_DRIVER, value, this); } }
    #endregion

    #region	Navigation Properties
        private OpTransmissionDriver _TransmissionDriver;
        public OpTransmissionDriver TransmissionDriver { get { return this._TransmissionDriver; } set { this._TransmissionDriver = value; this.IdTransmissionDriver = (value == null) ? 0 : (int)value.ID_TRANSMISSION_DRIVER; } }
        private OpProtocolDriver _ProtocolDriver;
        public OpProtocolDriver ProtocolDriver { get { return this._ProtocolDriver; } set { this._ProtocolDriver = value; this.IdProtocolDriver = (value == null) ? 0 : (int)value.ID_PROTOCOL_DRIVER; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpTransmissionProtocol()
            : base() 
        {
            OpState = OpChangeState.New;
        }

        public OpTransmissionProtocol(DB_TRANSMISSION_PROTOCOL clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "TransmissionProtocol [" + IdTransmissionProtocol.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpTransmissionProtocol> ConvertList(DB_TRANSMISSION_PROTOCOL[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTransmissionProtocol> ConvertList(DB_TRANSMISSION_PROTOCOL[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpTransmissionProtocol> ret = new List<OpTransmissionProtocol>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTransmissionProtocol(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTransmissionProtocol> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionDriver> TransmissionDriverDict = dataProvider.GetTransmissionDriver(list.Select(l => l.ID_TRANSMISSION_DRIVER).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER);
                Dictionary<int, OpProtocolDriver> ProtocolDriverDict = dataProvider.GetProtocolDriver(list.Select(l => l.ID_PROTOCOL_DRIVER).Distinct().ToArray()).ToDictionary(l => l.ID_PROTOCOL_DRIVER);
                foreach (var loop in list)
                {
                    loop.TransmissionDriver = TransmissionDriverDict.TryGetValue(loop.ID_TRANSMISSION_DRIVER);
                    loop.ProtocolDriver = ProtocolDriverDict.TryGetValue(loop.ID_PROTOCOL_DRIVER);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTransmissionProtocol> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTransmissionProtocol)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTransmissionProtocol).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpTransmissionProtocol> Members
        public bool Equals(OpTransmissionProtocol other)
        {
            if (other == null)
                return false;
            return this.IdTransmissionProtocol.Equals(other.IdTransmissionProtocol);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTransmissionProtocol)
                return this.IdTransmissionProtocol == ((OpTransmissionProtocol)obj).IdTransmissionProtocol;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTransmissionProtocol left, OpTransmissionProtocol right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTransmissionProtocol left, OpTransmissionProtocol right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTransmissionProtocol.GetHashCode();
        }
    #endregion
    }
#endif
}