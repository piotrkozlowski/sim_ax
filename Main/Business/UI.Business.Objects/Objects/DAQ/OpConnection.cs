using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpConnection : DB_CONNECTION, IComparable, IEquatable<OpConnection>
    {
    #region Properties
        public long Id { get { return this.ID; } set { this.ID = value; } }
        public int IdConnection { get { return this.ID_CONNECTION; } set { if (this.ID_CONNECTION != value) { this.ID_CONNECTION = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int Order { get { return this.ORDER; } set { if (this.ORDER != value) { this.ORDER = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int LinkTypeOut { get { return this.LINK_TYPE_OUT; } set { if (this.LINK_TYPE_OUT != value) { this.LINK_TYPE_OUT = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int LinkTypeIn { get { return this.LINK_TYPE_IN; } set { if (this.LINK_TYPE_IN != value) { this.LINK_TYPE_IN = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int RetryAttempt { get { return this.RETRY_ATTEMPT; } set { if (this.RETRY_ATTEMPT != value) { this.RETRY_ATTEMPT = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int TimeAttempt { get { return this.TIME_ATTEMPT; } set { if (this.TIME_ATTEMPT != value) { this.TIME_ATTEMPT = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpTransmissionDriverType _TransmissionDriverTypeIn;
        public OpTransmissionDriverType TransmissionDriverTypeIn { get { return _TransmissionDriverTypeIn; } set { _TransmissionDriverTypeIn = value; this.LinkTypeIn = (value == null) ? 0 : value.ID_TRANSMISSION_DRIVER_TYPE; } }
        private OpTransmissionDriverType _TransmissionDriverTypeOut;
        public OpTransmissionDriverType TransmissionDriverTypeOut { get { return _TransmissionDriverTypeOut; } set { _TransmissionDriverTypeOut = value; this.LinkTypeOut = (value == null) ? 0 : value.ID_TRANSMISSION_DRIVER_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpConnection()
            : base() 
        {
            OpState = OpChangeState.New;
        }

        public OpConnection(DB_CONNECTION clone)
            : base(clone) 
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpConnection> ConvertList(DB_CONNECTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpConnection> ConvertList(DB_CONNECTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpConnection> ret = new List<OpConnection>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpConnection(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConnection> list, IDataProvider dataProvider)
        {
            
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionDriverType> LinkDict = dataProvider.GetTransmissionDriverType(list.Select(l => l.LINK_TYPE_IN).Concat(list.Select(l => l.LINK_TYPE_OUT)).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER_TYPE);

                foreach (var loop in list)
                {
                    loop.TransmissionDriverTypeIn = LinkDict.TryGetValue(loop.LINK_TYPE_IN);
                    loop.TransmissionDriverTypeOut = LinkDict.TryGetValue(loop.LINK_TYPE_OUT);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConnection> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConnection)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConnection).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpConnection> Members
        public bool Equals(OpConnection other)
        {
            if (other == null)
                return false;
            return this.Id.Equals(other.Id);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpConnection)
                return this.Id == ((OpConnection)obj).Id;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpConnection left, OpConnection right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpConnection left, OpConnection right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    #endregion
    }
#endif
}