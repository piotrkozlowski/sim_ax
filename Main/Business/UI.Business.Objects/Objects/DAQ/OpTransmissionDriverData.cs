using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTransmissionDriverData : DB_TRANSMISSION_DRIVER_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpTransmissionDriverData>
    {
    #region Properties
        [XmlIgnore]
        public long IdTransmissionDriverData { get { return this.ID_TRANSMISSION_DRIVER_DATA; } set { this.ID_TRANSMISSION_DRIVER_DATA = value; } }
        [XmlIgnore]
        public int IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = OpDataUtil.SetNewValue(this.ID_TRANSMISSION_DRIVER, value, this); } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpTransmissionDriver _TransmissionDriver;
		public OpTransmissionDriver TransmissionDriver { get { return this._TransmissionDriver; } set { this._TransmissionDriver = value; this.ID_TRANSMISSION_DRIVER = (value == null)? 0 : (int)value.ID_TRANSMISSION_DRIVER; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
        [XmlIgnore]
        public long IdData { get { return this.IdTransmissionDriverData; } set { this.IdTransmissionDriverData = value; } }
        public object ReferencedObject { get; set; }
    #endregion
		
    #region Ctor
		public OpTransmissionDriverData()
            : base() { this.OpState = OpChangeState.New; }
		
		public OpTransmissionDriverData(DB_TRANSMISSION_DRIVER_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion
		
    #region ToString
		public override string ToString()
		{
				return "TransmissionDriverData [" + IdTransmissionDriverData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpTransmissionDriverData> ConvertList(DB_TRANSMISSION_DRIVER_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpTransmissionDriverData> ConvertList(DB_TRANSMISSION_DRIVER_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpTransmissionDriverData> ret = new List<OpTransmissionDriverData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpTransmissionDriverData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpTransmissionDriverData> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
                //Dictionary<int, OpTransmissionDriver> TransmissionDriverDict = dataProvider.GetTransmissionDriver(list.Select(l => l.ID_TRANSMISSION_DRIVER).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
                    //loop.TransmissionDriver = TransmissionDriverDict.TryGetValue(loop.ID_TRANSMISSION_DRIVER);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpTransmissionDriverData> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTransmissionDriverData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTransmissionDriverData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpTransmissionDriverData> Members
        public bool Equals(OpTransmissionDriverData other)
        {
            if (other == null)
				return false;
			return this.IdTransmissionDriverData.Equals(other.IdTransmissionDriverData);
        }
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpTransmissionDriverData clone = new OpTransmissionDriverData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_TRANSMISSION_DRIVER_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.TransmissionDriver = dataProvider.GetTransmissionDriver(this.ID_TRANSMISSION_DRIVER);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpTransmissionDriverData)
				return this.IdTransmissionDriverData == ((OpTransmissionDriverData)obj).IdTransmissionDriverData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpTransmissionDriverData left, OpTransmissionDriverData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpTransmissionDriverData left, OpTransmissionDriverData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdTransmissionDriverData.GetHashCode();
		}
    #endregion
    }
#endif
}