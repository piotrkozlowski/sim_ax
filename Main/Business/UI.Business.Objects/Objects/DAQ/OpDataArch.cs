using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DAQ;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataArch : DB_DATA_ARCH, IComparable, IEquatable<OpDataArch>, IOpData, IOpDataProvider
    {
        #region Properties
        public long IdDataArch { get { return this.ID_DATA_ARCH; } set { this.ID_DATA_ARCH = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int? IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public long IdPacket { get { return this.ID_PACKET; } set { this.ID_PACKET = value; } }
        public bool IsAction { get { return this.IS_ACTION; } set { this.IS_ACTION = value; } }
        public bool IsAlarm { get { return this.IS_ALARM; } set { this.IS_ALARM = value; } }
        #endregion

        #region	Navigation Properties
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpPacket _Packet;
        public OpPacket Packet { get { return this._Packet; } set { this._Packet = value; this.ID_PACKET = (value == null) ? 0 : (long)value.ID_PACKET; } }
        #endregion

        #region	Custom Properties
        public long IdData { get { return IdDataArch; } set { IdDataArch = value; } }
        public int Index { get; set; }
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }

        //wykorzystywany do zmiany jednostki bazowej dla wybranego pomiaru
        public OpUnit Unit { get; set; }

        public string ValueUnit
        {
            get
            {
                if (DataType != null && Unit != null && !string.IsNullOrEmpty(Unit.Name))
                    return String.Format("{0} [{1}]", Value, Unit.Name);
                return (Value == null) ? null : Value.ToString();
            }
        }
        public decimal? ValueDecimal
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDecimal(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public double? ValueDouble
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDouble(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public long? ValueLong
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToInt64(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public DateTime? ValueDateTime
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is DateTime)
                        return Convert.ToDateTime(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public bool? ValueBool
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is bool)
                        return Convert.ToBoolean(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public string ValueString
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Value.ToString();
                }
                catch
                {
                }
                return null;
            }
        }
        #endregion

        #region	Ctor
        public OpDataArch()
            : base() { }

        public OpDataArch(DB_DATA_ARCH clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "DataArch [" + IdDataArch.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpDataArch> ConvertList(DB_DATA_ARCH[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataArch> ConvertList(DB_DATA_ARCH[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDataArch> ret = new List<OpDataArch>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataArch(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataArch> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<long, OpPacket> PacketDict = dataProvider.GetPacket(list.Select(l => l.ID_PACKET).Distinct().ToArray()).ToDictionary(l => l.ID_PACKET);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    loop.Packet = PacketDict.TryGetValue(loop.ID_PACKET);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataArch> list, IDataProvider dataProvider)
        {
            foreach (OpDataArch loop in list)
            {
                if (loop.DataType != null && loop.DataType.Unit != null)
                    loop.Unit = (OpUnit)loop.DataType.Unit.Clone(dataProvider);
            }
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataArch)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataArch).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpDataArch> Members
        public bool Equals(OpDataArch other)
        {
            if (other == null)
                return false;
            return this.IdDataArch.Equals(other.IdDataArch);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataArch)
                return this.IdDataArch == ((OpDataArch)obj).IdDataArch;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataArch left, OpDataArch right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataArch left, OpDataArch right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataArch.GetHashCode();
        }
        #endregion

        #region IOpDataProvider

        #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
        #endregion

        #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDataArch clone = new OpDataArch();
            clone.IdData = this.IdData;
            clone.SerialNbr = this.SerialNbr;
            clone.IdDataType = this.IdDataType;
            clone.IndexNbr = this.IndexNbr;
            clone.Value = this.Value;
            clone.Time = this.Time;
            clone.IdPacket = this.IdPacket;
            clone.IsAction = this.IsAction;
            clone.IsAlarm = this.IsAlarm;
            clone.ReferencedObject = this.ReferencedObject;

            if (dataProvider != null)
                clone = ConvertList(new DB_DATA_ARCH[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
        #endregion

        #endregion

        #region ChangeUnit - List of DAQ.OpDataArch
        public static List<Objects.DAQ.OpDataArch> ChangeUnit(List<Objects.DAQ.OpDataArch> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit newUnit = dataTypeUnitDict[dataType.IdDataType];
                            if (newUnit != null && dataType.Unit != null && dataType.Unit.IdUnitBase == newUnit.IdUnitBase && newUnit.IdUnit != dataType.Unit.IdUnit && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType).ToList().ForEach(d => d.Unit = newUnit);
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (dataType.Unit.Scale * Convert.ToDouble(d.Value) + dataType.Unit.Bias - newUnit.Bias) / newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (dataType.Unit.Scale * Convert.ToDouble(d.Value) + dataType.Unit.Bias - newUnit.Bias) / newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = ((decimal)dataType.Unit.Scale * Convert.ToDecimal(d.Value) + (decimal)dataType.Unit.Bias - (decimal)newUnit.Bias) / (decimal)newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDecimal(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                }

                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        public static List<Objects.DAQ.OpDataArch> ChangeUnit(List<Objects.DAQ.OpDataArch> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (fromDataTypeUnitDict.Keys.Count > 0 && toDataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && fromDataTypeUnitDict.ContainsKey(dataType.IdDataType) && toDataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit fromUnit = fromDataTypeUnitDict[dataType.IdDataType];
                            OpUnit toUnit = toDataTypeUnitDict[dataType.IdDataType];
                            if (fromUnit != null && toUnit != null && fromUnit.IdUnitBase == toUnit.IdUnitBase && fromUnit.IdUnit != toUnit.IdUnit && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType).ToList().ForEach(d => d.Unit = toUnit);
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = ((decimal)fromUnit.Scale * Convert.ToDecimal(d.Value) + (decimal)fromUnit.Bias - (decimal)toUnit.Bias) / (decimal)toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDecimal(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                }

                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
    }
#endif
}