using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DAQ;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpActionPacket : DB_ACTION_PACKET, IComparable, IEquatable<OpActionPacket>
    {
    #region Properties
            public long IdAction { get { return this.ID_ACTION;} set {this.ID_ACTION = value;}}
                public long? IdPacket { get { return this.ID_PACKET;} set {this.ID_PACKET = value;}}
    #endregion

    #region	Navigation Properties
		private OpPacket _Packet;
				public OpPacket Packet { get { return this._Packet; } set { this._Packet = value; this.ID_PACKET = (value == null)? null : (long?)value.ID_PACKET; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpActionPacket()
			:base() {}
		
		public OpActionPacket(DB_ACTION_PACKET clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ActionPacket [" + IdAction.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpActionPacket> ConvertList(DB_ACTION_PACKET[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpActionPacket> ConvertList(DB_ACTION_PACKET[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpActionPacket> ret = new List<OpActionPacket>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpActionPacket(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpActionPacket> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpPacket> PacketDict = dataProvider.GetPacket(list.Where(l => l.ID_PACKET.HasValue).Select(l => l.ID_PACKET.Value).Distinct().ToArray()).ToDictionary(l => l.ID_PACKET);			
				foreach (var loop in list)
				{
						if (loop.ID_PACKET.HasValue)
						loop.Packet = PacketDict.TryGetValue(loop.ID_PACKET.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpActionPacket> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionPacket)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionPacket).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpActionPacket> Members
        public bool Equals(OpActionPacket other)
        {
            if (other == null)
				return false;
			return this.IdAction.Equals(other.IdAction);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpActionPacket)
				return this.IdAction == ((OpActionPacket)obj).IdAction;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpActionPacket left, OpActionPacket right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpActionPacket left, OpActionPacket right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdAction.GetHashCode();
		}
    #endregion
    }
#endif
}