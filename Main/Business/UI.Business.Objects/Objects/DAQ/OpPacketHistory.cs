using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DAQ;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPacketHistory : DB_PACKET_HISTORY, IComparable, IEquatable<OpPacketHistory>
    {
    #region Properties
        public long IdPacketHistory { get { return this.ID_PACKET_HISTORY; } set { this.ID_PACKET_HISTORY = value; } }
        public long IdPacket { get { return this.ID_PACKET; } set { this.ID_PACKET = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public int IdPacketStatus { get { return this.ID_PACKET_STATUS; } set { this.ID_PACKET_STATUS = value; } }
        public DateTime? SystemTime { get { return this.SYSTEM_TIME; } set { this.SYSTEM_TIME = value; } }
    #endregion

    #region	Navigation Properties
        //private OpPacket _Packet;
        //public OpPacket Packet { get { return this._Packet; } set { this._Packet = value; this.ID_PACKET = (value == null) ? 0 : (long)value.ID_PACKET; } }
        private OpPacketStatus _PacketStatus;
        public OpPacketStatus PacketStatus { get { return this._PacketStatus; } set { this._PacketStatus = value; this.ID_PACKET_STATUS = (value == null) ? 0 : (int)value.ID_PACKET_STATUS; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpPacketHistory()
            : base() { }

        public OpPacketHistory(DB_PACKET_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "PacketHistory [" + IdPacketHistory.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpPacketHistory> ConvertList(DB_PACKET_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPacketHistory> ConvertList(DB_PACKET_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpPacketHistory> ret = new List<OpPacketHistory>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPacketHistory(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPacketHistory> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<long, OpPacket> PacketDict = dataProvider.GetPacket(list.Select(l => l.ID_PACKET).Distinct().ToArray()).ToDictionary(l => l.ID_PACKET);
                Dictionary<int, OpPacketStatus> PacketStatusDict = dataProvider.GetPacketStatus(list.Select(l => l.ID_PACKET_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_PACKET_STATUS);

                foreach (var loop in list)
                {
                    //loop.Packet = PacketDict.TryGetValue(loop.ID_PACKET);
                    loop.PacketStatus = PacketStatusDict.TryGetValue(loop.ID_PACKET_STATUS);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPacketHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPacketHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPacketHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPacketHistory> Members
        public bool Equals(OpPacketHistory other)
        {
            if (other == null)
                return false;
            return this.IdPacketHistory.Equals(other.IdPacketHistory);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPacketHistory)
                return this.IdPacketHistory == ((OpPacketHistory)obj).IdPacketHistory;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPacketHistory left, OpPacketHistory right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPacketHistory left, OpPacketHistory right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPacketHistory.GetHashCode();
        }
    #endregion
    }
#endif
}