using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using System.Xml.Serialization;
using IMR.Suite.Data.DB.DAQ;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTransmissionDriver : DB_TRANSMISSION_DRIVER, IComparable, IEquatable<OpTransmissionDriver>
    {
    #region Properties
        [XmlIgnore]
        public int IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = value; } }
        [XmlIgnore]
        public int IdTransmissionDriverType { get { return this.ID_TRANSMISSION_DRIVER_TYPE; } set { this.ID_TRANSMISSION_DRIVER_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public string RunAtHost { get { return this.RUN_AT_HOST; } set { this.RUN_AT_HOST = value; } }
        [XmlIgnore]
        public string ResponseAddress { get { return this.RESPONSE_ADDRESS; } set { this.RESPONSE_ADDRESS = value; } }
        [XmlIgnore]
        public string Plugin { get { return this.PLUGIN; } set { this.PLUGIN = value; } }
        [XmlIgnore]
        public bool IsActive { get { return this.IS_ACTIVE; } set { this.IS_ACTIVE = value; } }
        [XmlIgnore]
        public bool UseCache { get { return this.USE_CACHE; } set { this.USE_CACHE = value; } }
    #endregion

    #region	Navigation Properties
        private OpTransmissionDriverType _TransmissionDriverType;
        public OpTransmissionDriverType TransmissionDriverType { get { return this._TransmissionDriverType; } set { this._TransmissionDriverType = value; this.ID_TRANSMISSION_DRIVER_TYPE = (value == null) ? 0 : (int)value.ID_TRANSMISSION_DRIVER_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpTransmissionDriverData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpTransmissionDriver()
            : base() 
        {
            this.DataList = new OpDataList<OpTransmissionDriverData>();
        }

        public OpTransmissionDriver(DB_TRANSMISSION_DRIVER clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpTransmissionDriverData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpTransmissionDriver> ConvertList(DB_TRANSMISSION_DRIVER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true, true);
        }

        public static List<OpTransmissionDriver> ConvertList(DB_TRANSMISSION_DRIVER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            return ConvertList(db_objects, dataProvider, loadNavigationProperties, true);
        }

        public static List<OpTransmissionDriver> ConvertList(DB_TRANSMISSION_DRIVER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData, bool useDBCollector = false)
        {
            List<OpTransmissionDriver> ret = new List<OpTransmissionDriver>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTransmissionDriver(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, useDBCollector); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTransmissionDriver> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionDriverType> TransmissionDriverTypeDict = dataProvider.GetTransmissionDriverType(list.Select(l => l.ID_TRANSMISSION_DRIVER_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER_TYPE);
                //Dictionary<int, OpTransmissionProtocol> TransmissionProtocolDict = dataProvider.GetTransmissionProtocol(list.Where(l => l.ID_TRANSMISSION_PROTOCOL.HasValue).Select(l => l.ID_TRANSMISSION_PROTOCOL.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_PROTOCOL);

                foreach (var loop in list)
                {
                    loop.TransmissionDriverType = TransmissionDriverTypeDict.TryGetValue(loop.ID_TRANSMISSION_DRIVER_TYPE);
                    //if (loop.ID_TRANSMISSION_PROTOCOL.HasValue)
                    //    loop.TransmissionProtocol = TransmissionProtocolDict.TryGetValue(loop.ID_TRANSMISSION_PROTOCOL.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTransmissionDriver> list, IDataProvider dataProvider, bool useDBCollector = false)
        {
            if (list != null && list.Count > 0)
            {
                List<int> ids = list.Select(l => l.IdTransmissionDriver).ToList();

                List<OpTransmissionDriverData> data = dataProvider.GetTransmissionDriverDataFilter(IdTransmissionDriver: ids.ToArray(), IdDataType: dataProvider.ITransmissionDriverDataTypes.ToArray());
                if (data != null && data.Count > 0)
                {
                    if (!useDBCollector)
                    {
                        Dictionary<int, OpTransmissionDriver> TDriversDict = list.ToDictionary<OpTransmissionDriver, int>(l => l.IdTransmissionDriver);
                        foreach (var dataValue in data)
                        {
                            TDriversDict[dataValue.IdTransmissionDriver].DataList.Add(dataValue);
                        }
                    }
                    else
                    {
                        foreach (var serverGroup in list.GroupBy(q => q.ID_SERVER))
                        {
                            Dictionary<int, OpTransmissionDriver> TDriversDict = serverGroup.ToDictionary<OpTransmissionDriver, int>(l => l.IdTransmissionDriver);
                            foreach (var dataValue in data.Where(q => q.ID_SERVER == serverGroup.Key))
                            {
                                TDriversDict[dataValue.IdTransmissionDriver].DataList.Add(dataValue);
                            }
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTransmissionDriver)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTransmissionDriver).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpTransmissionDriver> Members
        public bool Equals(OpTransmissionDriver other)
        {
            if (other == null)
                return false;
            return this.IdTransmissionDriver.Equals(other.IdTransmissionDriver);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTransmissionDriver)
                return this.IdTransmissionDriver == ((OpTransmissionDriver)obj).IdTransmissionDriver;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTransmissionDriver left, OpTransmissionDriver right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTransmissionDriver left, OpTransmissionDriver right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTransmissionDriver.GetHashCode();
        }
    #endregion
    }
#endif
}