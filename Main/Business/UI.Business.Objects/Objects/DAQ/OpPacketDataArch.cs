﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DAQ;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPacketDataArch : DB_PACKET_DATA_ARCH, IComparable, IEquatable<OpPacketDataArch>, IOpData, IOpDataProvider
    {
    #region Properties
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long IdAction { get { return this.ID_ACTION; } set { this.ID_ACTION = value; } }
        public long IdPacket { get { return this.ID_PACKET; } set { this.ID_PACKET = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int? IndexNBR { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public string Unit { get { return this.UNIT; } set { this.UNIT = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public string Status { get { return this.STATUS; } set { this.STATUS = value; } }
    #endregion
    #region Custom properties
        public long IdData { get; set; }
        public int Index { get; set; }
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion


    #region	Ctor
        public OpPacketDataArch()
            : base() { }

        public OpPacketDataArch(DB_PACKET_DATA_ARCH clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.Name;
        }
    #endregion

    #region	ConvertList
        
        public static List<OpPacketDataArch> ConvertList(DB_PACKET_DATA_ARCH[] db_objects)
        {
            if (db_objects == null) return null;
            List<OpPacketDataArch> ret = new List<OpPacketDataArch>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPacketDataArch(db_object)));

            
            return ret;
        }
    #endregion


    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPacketDataArch)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPacketDataArch).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPacketDataArch> Members
        public bool Equals(OpPacketDataArch other)
        {
            if (other == null)
                return false;
            return this.IdDataType.Equals(other.IdDataType);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPacketDataArch)
                return this.IdDataType == ((OpPacketDataArch)obj).IdDataType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPacketDataArch left, OpPacketDataArch right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPacketDataArch left, OpPacketDataArch right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPacket.GetHashCode();
        }
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpPacketDataArch clone = new OpPacketDataArch();
            clone.SerialNbr = this.SerialNbr;
            clone.IdAction = this.IdAction;
            clone.IdPacket = this.IdPacket;
            clone.IdDataType = this.IdDataType;
            clone.Name = this.Name;
            clone.IndexNBR = this.IndexNBR;
            clone.Value = this.Value;
            clone.Unit = this.Unit;
            clone.Time = this.Time;
            clone.Status = this.Status;
            return clone;
        }
    #endregion

    #endregion
    }
#endif
}