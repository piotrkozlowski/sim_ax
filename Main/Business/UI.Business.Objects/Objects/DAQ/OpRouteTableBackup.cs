﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpRouteTableBackup : DB_ROUTE_TABLE_BACKUP, IComparable, IEquatable<OpRouteTableBackup>
    {
    #region Properties
        public int Id { get { return this.ID; } set { this.ID = value; } }
        public int IdRoute { get { return this.ID_ROUTE; } set { this.ID_ROUTE = value; } }
        public int IdRouteBackup { get { return this.ID_ROUTE_BACKUP; } set { this.ID_ROUTE_BACKUP = value; } }
        public int Order { get { return this.ORDER; } set { this.ORDER = value; } }
    #endregion

    #region	Navigation Properties
        private OpRouteTable _Route;
        public OpRouteTable Route { get { return this._Route; } set { this._Route = value; this.ID_ROUTE = (value == null) ? 0 : (int)value.ID_ROUTE; } }
        private OpRouteTable _RouteBackup;
        public OpRouteTable RouteBackup { get { return this._RouteBackup; } set { this._RouteBackup = value; this.ID_ROUTE_BACKUP = (value == null) ? 0 : (int)value.ID_ROUTE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpRouteTableBackup()
            : base() 
        {
            this.OpState = OpChangeState.New;
        }

        public OpRouteTableBackup(DB_ROUTE_TABLE_BACKUP clone)
            : base(clone) 
        {
            this.OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "RouteTableBackup [" + Id.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpRouteTableBackup> ConvertList(DB_ROUTE_TABLE_BACKUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRouteTableBackup> ConvertList(DB_ROUTE_TABLE_BACKUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpRouteTableBackup> ret = new List<OpRouteTableBackup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRouteTableBackup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRouteTableBackup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                //List<int> RouteIds = new List<int>();
                //RouteIds.AddRange(list.Select(q => q.ID_ROUTE));
                //RouteIds.AddRange(list.Select(q => q.ID_ROUTE_BACKUP));
                //Dictionary<int, OpRouteTable> RouteTableDict = dataProvider.GetRouteTableFilter(IdRoute: RouteIds.Distinct().ToArray()).ToDictionary(q => q.ID_ROUTE);

                foreach (var loop in list)
                {
                    //loop.Route = RouteTableDict.TryGetValue(loop.ID_ROUTE);
                    //loop.RouteBackup = RouteTableDict.TryGetValue(loop.ID_ROUTE_BACKUP);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRouteTableBackup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteTableBackup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteTableBackup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpRouteTableBackup> Members
        public bool Equals(OpRouteTableBackup other)
        {
            if (other == null)
                return false;
            return this.Id.Equals(other.Id);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRouteTableBackup)
                return this.Id == ((OpRouteTableBackup)obj).Id;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRouteTableBackup left, OpRouteTableBackup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRouteTableBackup left, OpRouteTableBackup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    #endregion
    }
#endif
}