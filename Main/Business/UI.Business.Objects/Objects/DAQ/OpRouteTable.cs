using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.DAQ
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpRouteTable : DB_ROUTE_TABLE, IComparable, IEquatable<OpRouteTable>, IOpChangeState
    {
    #region Properties
        public int IdRoute { get { return this.ID_ROUTE; } set { if (this.ID_ROUTE != value) { this.ID_ROUTE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string Address { get { return this.ADDRESS; } set { if (this.ADDRESS != value) { this.ADDRESS = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int Prefix { get { return this.PREFIX; } set { if (this.PREFIX != value) { this.PREFIX = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string Hop { get { return this.HOP; } set { if (this.HOP != value) { this.HOP = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int CostOut { get { return this.COST_OUT; } set { if (this.COST_OUT != value) { this.COST_OUT = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int CostIn { get { return this.COST_IN; } set { if (this.COST_IN != value) { this.COST_IN = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
        public List<OpRouteTableBackup> RouteBackupList { get; set; }
    #endregion
		
    #region	Ctor
		public OpRouteTable()
			:base() 
        {
            this.OpState = OpChangeState.New;
            this.RouteBackupList = new List<OpRouteTableBackup>();
        }
		
		public OpRouteTable(DB_ROUTE_TABLE clone)
			:base(clone) 
        {
            this.OpState = OpChangeState.Loaded;
            this.RouteBackupList = new List<OpRouteTableBackup>();
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpRouteTable> ConvertList(DB_ROUTE_TABLE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true, true);
        }
		
		public static List<OpRouteTable> ConvertList(DB_ROUTE_TABLE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData)
		{
			List<OpRouteTable> ret = new List<OpRouteTable>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpRouteTable(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpRouteTable> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRouteTable> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, List<OpRouteTableBackup>> RouteBackupDict = new Dictionary<int, List<OpRouteTableBackup>>();
                foreach (OpRouteTableBackup backup in dataProvider.GetRouteTableBackupFilter(IdRoute: list.Select(q => q.ID_ROUTE).Distinct().ToArray()))
                {
                    if (!RouteBackupDict.ContainsKey(backup.ID_ROUTE) || RouteBackupDict[backup.ID_ROUTE] == null)
                        RouteBackupDict[backup.ID_ROUTE] = new List<OpRouteTableBackup>();
                    RouteBackupDict[backup.ID_ROUTE].Add(backup);
                }
                foreach (OpRouteTable loop in list)
                {
                    if (RouteBackupDict.ContainsKey(loop.ID_ROUTE))
                        loop.RouteBackupList = RouteBackupDict[loop.ID_ROUTE];
                }
            }
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteTable)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteTable).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpRouteTable> Members
        public bool Equals(OpRouteTable other)
        {
            if (other == null)
				return false;
			return this.IdRoute.Equals(other.IdRoute);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpRouteTable)
				return this.IdRoute == ((OpRouteTable)obj).IdRoute;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpRouteTable left, OpRouteTable right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpRouteTable left, OpRouteTable right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdRoute.GetHashCode();
		}
    #endregion
    }
#endif
}