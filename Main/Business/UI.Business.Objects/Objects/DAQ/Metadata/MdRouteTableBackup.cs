namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpRouteTableBackup Metadata
	/// </summary>
	public class MdRouteTableBackup
	{
		public const string Id = "Id";
		public const string IdRoute = "IdRoute";
		public const string IdRouteBackup = "IdRouteBackup";
		public const string Order = "Order";
		public const string Route = "Route";
		public const string RouteBackup = "RouteBackup";
	
	}
}
	