namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpConnection Metadata
	/// </summary>
	public class MdConnection
	{
		public const string Id = "Id";
		public const string IdConnection = "IdConnection";
		public const string Order = "Order";
		public const string LinkTypeOut = "LinkTypeOut";
		public const string LinkTypeIn = "LinkTypeIn";
		public const string RetryAttempt = "RetryAttempt";
		public const string TimeAttempt = "TimeAttempt";
		public const string Name = "Name";
		public const string TransmissionDriverTypeIn = "TransmissionDriverTypeIn";
		public const string TransmissionDriverTypeOut = "TransmissionDriverTypeOut";
		public const string OpState = "OpState";
	
	}
}
	