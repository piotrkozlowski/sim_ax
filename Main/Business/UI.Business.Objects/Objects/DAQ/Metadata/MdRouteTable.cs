namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpRouteTable Metadata
	/// </summary>
	public class MdRouteTable
	{
		public const string IdRoute = "IdRoute";
		public const string Address = "Address";
		public const string Prefix = "Prefix";
		public const string Hop = "Hop";
		public const string CostOut = "CostOut";
		public const string CostIn = "CostIn";
		public const string Name = "Name";
		public const string OpState = "OpState";
	
	}
}
	