namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpDataArch Metadata
	/// </summary>
	public class MdDataArch
	{
		public const string IdDataArch = "IdDataArch";
		public const string SerialNbr = "SerialNbr";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string Time = "Time";
		public const string IdPacket = "IdPacket";
		public const string IsAction = "IsAction";
		public const string IsAlarm = "IsAlarm";
		public const string DataType = "DataType";
		public const string Packet = "Packet";
		public const string Unit = "Unit";
		public const string ValueUnit = "ValueUnit";
		public const string ValueDecimal = "ValueDecimal";
		public const string ValueDouble = "ValueDouble";
		public const string ValueLong = "ValueLong";
		public const string ValueDateTime = "ValueDateTime";
		public const string ValueBool = "ValueBool";
		public const string ValueString = "ValueString";
	
	}
}
	