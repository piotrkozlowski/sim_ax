namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpTransmissionDriver Metadata
	/// </summary>
	public class MdTransmissionDriver
	{
		public const string IdTransmissionDriver = "IdTransmissionDriver";
		public const string IdTransmissionDriverType = "IdTransmissionDriverType";
		public const string Name = "Name";
		public const string RunAtHost = "RunAtHost";
		public const string ResponseAddress = "ResponseAddress";
		public const string Plugin = "Plugin";
		public const string IsActive = "IsActive";
		public const string UseCache = "UseCache";
		public const string TransmissionDriverType = "TransmissionDriverType";
		public const string DataList = "DataList";
	
	}
}
	