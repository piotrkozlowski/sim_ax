namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpPacket Metadata
	/// </summary>
	public class MdPacket
	{
		public const string IdPacket = "IdPacket";
		public const string SerialNbr = "SerialNbr";
		public const string Address = "Address";
		public const string IdTransmissionDriver = "IdTransmissionDriver";
		public const string IsIncoming = "IsIncoming";
		public const string IdTransmissionType = "IdTransmissionType";
		public const string TransmittedPackets = "TransmittedPackets";
		public const string Bytes = "Bytes";
		public const string TransmittedBytes = "TransmittedBytes";
		public const string Body = "Body";
		public const string TransmissionDriver = "TransmissionDriver";
		public const string TransmissionType = "TransmissionType";
		public const string LastUpdate = "LastUpdate";
		public const string PacketStatus = "PacketStatus";
		public const string BodyHex = "BodyHex";
	
	}
}
	