namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpTransmissionProtocol Metadata
	/// </summary>
	public class MdTransmissionProtocol
	{
		public const string IdTransmissionProtocol = "IdTransmissionProtocol";
		public const string IdTransmissionDriver = "IdTransmissionDriver";
		public const string Order = "Order";
		public const string IdProtocolDriver = "IdProtocolDriver";
		public const string TransmissionDriver = "TransmissionDriver";
		public const string ProtocolDriver = "ProtocolDriver";
		public const string OpState = "OpState";
	
	}
}
	