namespace IMR.Suite.UI.Business.Objects.DAQ.Metadata
{
	/// <summary>
	/// OpTransmissionDriverType Metadata
	/// </summary>
	public class MdTransmissionDriverType
	{
		public const string IdTransmissionDriverType = "IdTransmissionDriverType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	