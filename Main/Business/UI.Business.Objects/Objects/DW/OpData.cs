using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpData : DB_DATA, IComparable, IEquatable<OpData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdData { get { return this.ID_DATA; } set { this.ID_DATA = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        public Enums.DataStatus DataStatus { get { return this.STATUS.HasValue ? (Enums.DataStatus) this.STATUS : Enums.DataStatus.Unknown; } set { this.STATUS = (int)value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public long? IdDataSource { get { return this.ID_DATA_SOURCE; } set { this.ID_DATA_SOURCE = value; } }
        public int? IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
    #endregion

    #region	Navigation Properties
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpDataSource _DataSource;
        public OpDataSource DataSource { get { return this._DataSource; } set { this._DataSource = value; this.ID_DATA_SOURCE = (value == null) ? null : (long?)value.ID_DATA_SOURCE; } }
        private OpDataSourceType _DataSourceType;
        public OpDataSourceType DataSourceType { get { return this._DataSourceType; } set { this._DataSourceType = value; this.ID_DATA_SOURCE_TYPE = (value == null) ? null : (int?)value.ID_DATA_SOURCE_TYPE; } }
    #endregion

    #region	Custom Properties
        //wykorzystywany do zmiany jednostki bazowej dla wybranego pomiaru
        public OpUnit Unit { get; set; }
        public object ReferencedObject { get; set; }

        public string ValueUnit
        {
            get
            {
                if (DataType != null && Unit != null && !string.IsNullOrEmpty(Unit.Name) && Value != null)
                    return String.Format("{0} [{1}]", Value, Unit);
                return (Value == null) ? null : Value.ToString();
            }
        }
        public decimal? ValueDecimal
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDecimal(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public double? ValueDouble
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDouble(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public long? ValueLong
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToInt64(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public DateTime? ValueDateTime
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is DateTime)
                        return Convert.ToDateTime(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public bool? ValueBool
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is bool)
                        return Convert.ToBoolean(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public string ValueString
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Value.ToString();
                }
                catch
                {
                }
                return null;
            }
        }
    #endregion

    #region	Ctor
        public OpData()
            : base() { this.DataStatus = Enums.DataStatus.Normal; }

        public OpData(DB_DATA clone)
            : base(clone) { /*this.DataStatus = Enums.DataStatus.Normal; tego tu nie moze by� !!!*/ }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "Data [" + IdData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpData> ConvertList(DB_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpData> ConvertList(DB_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpData> ret = new List<OpData>(db_objects.Length);

            Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(db_objects.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, loadNavigationProperties: loadNavigationProperties).ToDictionary(l => l.ID_DATA_TYPE);
            db_objects.ToList().ForEach(db_object =>
            {
                OpData newObject = new OpData(db_object);
                newObject.DataType = DataTypeDict.TryGetValue(newObject.ID_DATA_TYPE);
                try
                {
                    // sdudzik - probujemy poprawic wartosc, zeby byla wlasciwego typu
                    // CELOWO (!) uzywamy VALUE, a nie Value - nie zmieniac tego!
                    // VALUE uzywane do tego, zeby raz poprawic typ rzeczywistej wartosci, Value to tylko get/set
                    newObject.VALUE = OpDataUtil.CorrectValueType(newObject.DataType, newObject.VALUE);
                }
                catch
                {
                    // korekcja sie nie udala, oznaczamy wartosc jako bledna
                    newObject.OpState = OpChangeState.Incorrect;
                }
                ret.Add(newObject);
            });

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<long, OpDataSource> DataSourceDict = dataProvider.GetDataSource(list.Where(l => l.ID_DATA_SOURCE.HasValue).Select(l => l.ID_DATA_SOURCE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE);
                Dictionary<int, OpDataSourceType> DataSourceTypeDict = dataProvider.GetDataSourceType(list.Where(l => l.ID_DATA_SOURCE_TYPE.HasValue).Select(l => l.ID_DATA_SOURCE_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE_TYPE);

                foreach (var loop in list)
                {
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DATA_SOURCE.HasValue)
                        loop.DataSource = DataSourceDict.TryGetValue(loop.ID_DATA_SOURCE.Value);
                    if (loop.ID_DATA_SOURCE_TYPE.HasValue)
                        loop.DataSourceType = DataSourceTypeDict.TryGetValue(loop.ID_DATA_SOURCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpData> list, IDataProvider dataProvider)
        {
            foreach (OpData loop in list)
            {
                if (loop.DataType != null && loop.DataType.Unit != null)
                    loop.Unit = (OpUnit)loop.DataType.Unit.Clone(dataProvider);
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpData> Members
        public bool Equals(OpData other)
        {
            if (other == null)
                return false;
            return this.IdData.Equals(other.IdData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpData)
                return this.IdData == ((OpData)obj).IdData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpData left, OpData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpData left, OpData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdData.GetHashCode();
        }
    #endregion

    #region IOpDataProvider

    #region Clone
        public object Clone(IDataProvider dataProvider = null)
        {
            OpData clone = new OpData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.SerialNbr = this.SerialNbr;
            clone.IdLocation = this.IdLocation;
            clone.IdMeter = this.IdMeter;
            clone.DataStatus = this.DataStatus;

            if (dataProvider != null)
                clone = ConvertList(new DB_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        } 
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            if (this.ID_METER.HasValue)
                this.Meter = dataProvider.GetMeter(this.ID_METER.Value);
            if (this.ID_LOCATION.HasValue)
                this.Location = dataProvider.GetLocation(this.ID_LOCATION.Value);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        } 
    #endregion

    #endregion

    #region Implementation of IOpChangeState
        public OpChangeState OpState { get; set; }
        #endregion

        #region ChangeUnit - List of DW.OpData
        public static List<Objects.DW.OpData> ChangeUnit(List<Objects.DW.OpData> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit newUnit = dataTypeUnitDict[dataType.IdDataType];
                            if (newUnit != null && dataType.Unit != null && dataType.Unit.IdUnitBase == newUnit.IdUnitBase && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                {
                                    d.Unit = newUnit;

                                    if (d.Value != null)
                                        d.Value = OpUnit.ChangeUnit(d.Value, d.DataType, dataType.Unit, newUnit,
                                            dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType) ? dataTypeFractionalDigitsDict[dataType.IdDataType] : null);
                                });
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        public static List<Objects.DW.OpData> ChangeUnit(List<Objects.DW.OpData> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (fromDataTypeUnitDict.Keys.Count > 0 && toDataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && fromDataTypeUnitDict.ContainsKey(dataType.IdDataType) && toDataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit fromUnit = fromDataTypeUnitDict[dataType.IdDataType];
                            OpUnit toUnit = toDataTypeUnitDict[dataType.IdDataType];
                            if (fromUnit != null && toUnit != null && fromUnit.IdUnitBase == toUnit.IdUnitBase && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                {
                                    d.Unit = toUnit;

                                    if (d.Value != null)
                                        d.Value = OpUnit.ChangeUnit(d.Value, d.DataType, fromUnit, toUnit,
                                            dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType) ? dataTypeFractionalDigitsDict[dataType.IdDataType] : null);
                                });
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
    }
#endif
}