using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataSource : DB_DATA_SOURCE, IComparable, IEquatable<OpDataSource>
    {
    #region Properties
        public long IdDataSource { get { return this.ID_DATA_SOURCE; } set { this.ID_DATA_SOURCE = value; } }
        public int IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDataSourceType _DataSourceType;
        public OpDataSourceType DataSourceType { get { return this._DataSourceType; } set { this._DataSourceType = value; this.ID_DATA_SOURCE_TYPE = (value == null) ? 0 : (int)value.ID_DATA_SOURCE_TYPE; } }
    #endregion

    #region	Custom Properties
        public long? IdPacket { get; set; }
    #endregion

    #region	Ctor
        public OpDataSource()
            : base() { }

        public OpDataSource(DB_DATA_SOURCE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DataSource [" + IdDataSource.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDataSource> ConvertList(DB_DATA_SOURCE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDataSource> ConvertList(DB_DATA_SOURCE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDataSource> ret = new List<OpDataSource>(list.Length);
            foreach (var loop in list)
            {
                OpDataSource insert = new OpDataSource(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataSource> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDataSourceType> DataSourceTypeDict = dataProvider.GetDataSourceType(list.Select(l => l.ID_DATA_SOURCE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE_TYPE);
                foreach (var loop in list)
                {
                    loop.DataSourceType = DataSourceTypeDict.TryGetValue(loop.ID_DATA_SOURCE_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataSource> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                List<long> IdDataSource = list.Select(l => l.IdDataSource).Distinct().ToList();

                if (IdDataSource.Count > 0)
                {
                    Dictionary<long, OpDataSourceData> data = dataProvider.GetDataSourceDataFilter(IdDataSource: IdDataSource.ToArray(), IdDataType: new long[] { DataType.DATA_SOURCE_ID_PACKET }).Distinct(d => d.IdDataSource).ToDictionary(d => d.IdDataSource); //TS: dodano distinct, zeby nie wywalalo bledu jak sa te same pomiary wiele razy przetworzone
                    foreach (var datasource in list)
                    {
                        OpDataSourceData dataSourceData = data.TryGetValue(datasource.IdDataSource);
                        if (dataSourceData != null)
                            datasource.IdPacket = Convert.ToInt64(dataSourceData.Value);
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataSource)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataSource).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataSource> Members
        public bool Equals(OpDataSource other)
        {
            if (other == null)
                return false;
            return this.IdDataSource.Equals(other.IdDataSource);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataSource)
                return this.IdDataSource == ((OpDataSource)obj).IdDataSource;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataSource left, OpDataSource right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataSource left, OpDataSource right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataSource.GetHashCode();
        }
    #endregion
    }
#endif
}