using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.DW
{
    [Serializable]
	public class OpTemperatureCoefficient : DB_TEMPERATURE_COEFFICIENT, IComparable, IEquatable<OpTemperatureCoefficient>
    {
		#region Properties
        public long IdTemperatureCoefficient { get { return this.ID_TEMPERATURE_COEFFICIENT;} set {this.ID_TEMPERATURE_COEFFICIENT = value;}}
        public Double Temperature { get { return this.TEMPERATURE;} set {this.TEMPERATURE = value;}}
        public Double Density { get { return this.DENSITY;} set {this.DENSITY = value;}}
        public Double Value { get { return this.VALUE;} set {this.VALUE = value;}}
		#endregion

		#region	Navigation Properties
		#endregion

		#region	Custom Properties
		#endregion
		
		#region	Ctor
		public OpTemperatureCoefficient()
			:base() {}
		
		public OpTemperatureCoefficient(DB_TEMPERATURE_COEFFICIENT clone)
			:base(clone) {}
		#endregion
		
		#region	ToString
		public override string ToString()
		{
            return String.Format(@"[{0} �C, {1} kg/m�] = {2}", Temperature, Density, Value);
		}
		#endregion

		#region	ConvertList
		public static List<OpTemperatureCoefficient> ConvertList(DB_TEMPERATURE_COEFFICIENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpTemperatureCoefficient> ConvertList(DB_TEMPERATURE_COEFFICIENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpTemperatureCoefficient> ret = new List<OpTemperatureCoefficient>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpTemperatureCoefficient(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
		#endregion
		
		#region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpTemperatureCoefficient> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            //if (list != null && list.Count > 0)
            //{	
            //    foreach (var loop in list)
            //    {
            //    }
            //}
		}
		#endregion
		
		#region LoadCustomData
		private static void LoadCustomData(ref List<OpTemperatureCoefficient> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
		#endregion
		
		#region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTemperatureCoefficient)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTemperatureCoefficient).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
		#endregion
		
		#region IEquatable<OpTemperatureCoefficient> Members
        public bool Equals(OpTemperatureCoefficient other)
        {
            if (other == null)
				return false;
			return this.IdTemperatureCoefficient.Equals(other.IdTemperatureCoefficient);
        }
        #endregion

		#region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpTemperatureCoefficient)
				return this.IdTemperatureCoefficient == ((OpTemperatureCoefficient)obj).IdTemperatureCoefficient;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpTemperatureCoefficient left, OpTemperatureCoefficient right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpTemperatureCoefficient left, OpTemperatureCoefficient right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdTemperatureCoefficient.GetHashCode();
		}
		#endregion
    }
}