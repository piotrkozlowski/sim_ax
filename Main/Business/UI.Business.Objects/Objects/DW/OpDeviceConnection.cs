using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpDeviceConnection : DB_DEVICE_CONNECTION, IComparable, IEquatable<OpDeviceConnection>, IOpChangeState, IOpDynamic, IOpDynamicProperty<OpDeviceConnection>, IReferenceType, IOpObject<OpDeviceConnectionData>
    {
		#region Properties
        public long IdDeviceConnection { get { return this.ID_DEVICE_CONNECTION;} set {this.ID_DEVICE_CONNECTION = value;}}
        public long SerialNbr { get { return this.SERIAL_NBR;} set {this.SERIAL_NBR = value;}}
        public long SerialNbrParent { get { return this.SERIAL_NBR_PARENT;} set {this.SERIAL_NBR_PARENT = value;}}
        public DateTime Time { get { return this.TIME;} set {this.TIME = value;}}
        #endregion

        #region	Navigation Properties
        #endregion

        #region	Custom Properties
        [DataMember]
        public OpChangeState OpState { get; set; }
        [DataMember]
        private Dictionary<string, object> dynamicValues;
        [DataMember]
        private OpDynamicPropertyDict dynamicProperties;
        #endregion

        #region	Ctor
        public OpDeviceConnection()
			:base()
        {
            this.OpState = OpChangeState.New;
            this.DynamicValues = new Dictionary<string, object>();
            this.DataList = new OpDataList<OpDeviceConnectionData>();
        }
		
		public OpDeviceConnection(DB_DEVICE_CONNECTION clone)
			:base(clone)
        {
            this.OpState = OpChangeState.Loaded;
            this.DynamicValues = new Dictionary<string, object>();
            this.DataList = new OpDataList<OpDeviceConnectionData>();
        }
		#endregion
		
		#region	ToString
		public override string ToString()
		{
				return "DeviceConnection [" + IdDeviceConnection.ToString() + "]";
		}
		#endregion

		#region	ConvertList
		public static List<OpDeviceConnection> ConvertList(DB_DEVICE_CONNECTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDeviceConnection> ConvertList(DB_DEVICE_CONNECTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpDeviceConnection> ret = new List<OpDeviceConnection>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceConnection(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
		#endregion
		
		#region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDeviceConnection> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
		#endregion
		
		#region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceConnection> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
		#endregion
		
		#region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceConnection)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceConnection).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
		#endregion
		
		#region IEquatable<OpDeviceConnection> Members
        public bool Equals(OpDeviceConnection other)
        {
            if (other == null)
				return false;
			return this.IdDeviceConnection.Equals(other.IdDeviceConnection);
        }
        #endregion

		#region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDeviceConnection)
				return this.IdDeviceConnection == ((OpDeviceConnection)obj).IdDeviceConnection;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceConnection left, OpDeviceConnection right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceConnection left, OpDeviceConnection right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDeviceConnection.GetHashCode();
		}
        #endregion

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdDeviceConnection; }
        }

        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpDeviceConnection IOpDynamicProperty<OpDeviceConnection>.Owner
        {
            get { return this; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
        #endregion

        #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdDeviceConnection;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion

        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdDeviceConnection; }
            set { IdDeviceConnection = Convert.ToInt64(value); }
        }

        [DataMember]
        public OpDataList<OpDeviceConnectionData> DataList { get; set; }
        #endregion
    }
#endif
}