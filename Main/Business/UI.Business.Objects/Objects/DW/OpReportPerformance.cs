﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpReportPerformance : DB_REPORT_PERFORMANCE, IComparable, IEquatable<OpReportPerformance>
    {
    #region Properties
        public long IdReportPerformance { get { return this.ID_REPORT_PERFORMANCE; } set { this.ID_REPORT_PERFORMANCE = value; } }
        public int IdReport { get { return this.ID_REPORT; } set { this.ID_REPORT = value; } }
        public int IdAggregationType { get { return this.ID_AGGREGATION_TYPE; } set { this.ID_AGGREGATION_TYPE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public int? IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
    #endregion

    #region	Navigation Properties
        private OpReport _Report;
        public OpReport Report { get { return this._Report; } set { this._Report = value; this.ID_REPORT = (value == null) ? 0 : (int)value.ID_REPORT; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpDataSourceType _DataSourceType;
        public OpDataSourceType DataSourceType { get { return this._DataSourceType; } set { this._DataSourceType = value; this.ID_DATA_SOURCE_TYPE = (value == null) ? null : (int?)value.ID_DATA_SOURCE_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpReportPerformance()
            : base() { }

        public OpReportPerformance(DB_REPORT_PERFORMANCE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ReportPerformance [" + IdReportPerformance.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpReportPerformance> ConvertList(DB_REPORT_PERFORMANCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpReportPerformance> ConvertList(DB_REPORT_PERFORMANCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpReportPerformance> ret = new List<OpReportPerformance>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpReportPerformance(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpReportPerformance> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpReport> ReportDict = dataProvider.GetReport(list.Select(l => l.ID_REPORT).Distinct().ToArray()).ToDictionary(l => l.ID_REPORT);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpDataSourceType> DataSourceTypeDict = dataProvider.GetDataSourceType(list.Where(l => l.ID_DATA_SOURCE_TYPE.HasValue).Select(l => l.ID_DATA_SOURCE_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE_TYPE);

                foreach (var loop in list)
                {
                    loop.Report = ReportDict.TryGetValue(loop.ID_REPORT);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DATA_SOURCE_TYPE.HasValue)
                        loop.DataSourceType = DataSourceTypeDict.TryGetValue(loop.ID_DATA_SOURCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpReportPerformance> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpReportPerformance)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpReportPerformance).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpReportPerformance> Members
        public bool Equals(OpReportPerformance other)
        {
            if (other == null)
                return false;
            return this.IdReportPerformance.Equals(other.IdReportPerformance);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpReportPerformance)
                return this.IdReportPerformance == ((OpReportPerformance)obj).IdReportPerformance;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpReportPerformance left, OpReportPerformance right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpReportPerformance left, OpReportPerformance right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdReportPerformance.GetHashCode();
        }
    #endregion
    }
#endif
}