using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpConsumerTransaction : DB_CONSUMER_TRANSACTION, IComparable, IEquatable<OpConsumerTransaction>
    {
    #region Properties
        public int IdConsumerTransaction { get { return this.ID_CONSUMER_TRANSACTION; } set { this.ID_CONSUMER_TRANSACTION = value; } }
        public int IdConsumer { get { return this.ID_CONSUMER; } set { this.ID_CONSUMER = value; } }
        public int? IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public int IdTransactionType { get { return this.ID_TRANSACTION_TYPE; } set { this.ID_TRANSACTION_TYPE = value; } }
        public int IdTariff { get { return this.ID_TARIFF; } set { this.ID_TARIFF = value; } }
        public DateTime Timestamp { get { return this.TIMESTAMP; } set { this.TIMESTAMP = value; } }
        public Double ChargePrepaid { get { return this.CHARGE_PREPAID; } set { this.CHARGE_PREPAID = value; } }
        public Double ChargeEc { get { return this.CHARGE_EC; } set { this.CHARGE_EC = value; } }
        public Double ChargeOwed { get { return this.CHARGE_OWED; } set { this.CHARGE_OWED = value; } }
        public Double BalancePrepaid { get { return this.BALANCE_PREPAID; } set { this.BALANCE_PREPAID = value; } }
        public Double BalanceEc { get { return this.BALANCE_EC; } set { this.BALANCE_EC = value; } }
        public Double BalanceOwed { get { return this.BALANCE_OWED; } set { this.BALANCE_OWED = value; } }
        public Double? MeterIndex { get { return this.METER_INDEX; } set { this.METER_INDEX = value; } }
        public long? IdConsumerSettlement { get { return this.ID_CONSUMER_SETTLEMENT; } set { this.ID_CONSUMER_SETTLEMENT = value; } }
        public int? IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        public string TransactionGuid { get { return this.TRANSACTION_GUID; } set { this.TRANSACTION_GUID = value; } }
    #endregion

    #region	Navigation Properties
        private OpConsumer _Consumer;
        public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null) ? 0 : (int)value.ID_CONSUMER; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpTariff _Tariff;
        public OpTariff Tariff
        {
            get { return this._Tariff; }
            set { this._Tariff = value;
                this.ID_TARIFF = (value == null) ? 0 : (int) value.ID_TARIFF;
            }
        }
        private OpConsumerTransactionType _ConsumerTransactionType;
        public OpConsumerTransactionType ConsumerTransactionType{ get { return _ConsumerTransactionType; } set { _ConsumerTransactionType = value; IdTransactionType = (value == null) ? 0 : value.IdConsumerTransactionType; } }
        private OpConsumerSettlement _ConsumerSettlement;
        public OpConsumerSettlement ConsumerSettlement { get { return this._ConsumerSettlement; } set { this._ConsumerSettlement = value; this.ID_CONSUMER_SETTLEMENT = value == null ? 0 : value.ID_CONSUMER_SETTLEMENT; } }


    #endregion

    #region	Custom Properties 
        public Double TotalPaid
        {
            get
            {
                return ChargeOwed + ChargePrepaid;
            }
        }
        public Double BalancePrepaidBeforeTransaction
        {
            get { return this.BalancePrepaid - this.ChargePrepaid; }
        }
        public Double Debt
        {
            get { return BalanceEc + BalanceOwed; }
        }

        public Double Balance
        {
            get { return this.BalancePrepaid - this.BalanceEc; }
        }

        public Double PositiveChargePrepaid { get { return -ChargePrepaid; } }
        public Double PositiveChargeEc { get { return -ChargeEc; } }
        public Double PositiveChargeOwed { get { return -ChargeOwed; } }

        public Double? MeterIndexM3 { get { if (MeterIndex.HasValue) return MeterIndex / 1000.0; else return null; } }

    #endregion

    #region	Ctor
        public OpConsumerTransaction()
            : base() { }

        public OpConsumerTransaction(DB_CONSUMER_TRANSACTION clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ConsumerTransaction [" + IdConsumerTransaction.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpConsumerTransaction> ConvertList(DB_CONSUMER_TRANSACTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpConsumerTransaction> ConvertList(DB_CONSUMER_TRANSACTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpConsumerTransaction> ret = new List<OpConsumerTransaction>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerTransaction(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConsumerTransaction> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Select(l => l.ID_CONSUMER).Distinct().ToArray()).ToDictionary(l => l.ID_CONSUMER);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpTariff> TariffDict = dataProvider.GetTariff(list.Select(l => l.ID_TARIFF).Distinct().ToArray()).ToDictionary(l => l.ID_TARIFF);
                Dictionary<int, OpConsumerTransactionType> TypeDict = dataProvider.GetConsumerTransactionType(list.Select(l => l.ID_TRANSACTION_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_CONSUMER_TRANSACTION_TYPE);

                foreach (var loop in list)
                {
                    loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER);
                    if (loop.ID_OPERATOR.HasValue)
                        loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.Tariff = TariffDict.TryGetValue(loop.ID_TARIFF);
                    loop.ConsumerTransactionType = TypeDict.TryGetValue(loop.ID_TRANSACTION_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConsumerTransaction> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerTransaction)
            {
                return this.Timestamp.CompareTo((obj as OpConsumerTransaction).Timestamp);
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpConsumerTransaction> Members
        public bool Equals(OpConsumerTransaction other)
        {
            if (other == null)
                return false;
            return this.IdConsumerTransaction.Equals(other.IdConsumerTransaction);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpConsumerTransaction)
                return this.IdConsumerTransaction == ((OpConsumerTransaction)obj).IdConsumerTransaction;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpConsumerTransaction left, OpConsumerTransaction right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpConsumerTransaction left, OpConsumerTransaction right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdConsumerTransaction.GetHashCode();
        }
    #endregion
    }
#endif
}