using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpCalibrationSession : DB_CALIBRATION_SESSION, IComparable, IEquatable<OpCalibrationSession>
    {
    #region Properties
        public int IdCalibrationSession { get { return this.ID_CALIBRATION_SESSION; } set { this.ID_CALIBRATION_SESSION = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public string ReporterName { get { return this.REPORTER_NAME; } set { this.REPORTER_NAME = value; } }
        public Enums.CalibrationStatus Status { get { return (Enums.CalibrationStatus)this.STATUS; } set { this.STATUS = (int)value; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpCalibrationSession()
            : base() { }

        public OpCalibrationSession(DB_CALIBRATION_SESSION clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpCalibrationSession> ConvertList(DB_CALIBRATION_SESSION[] db_objects)
        {
            List<OpCalibrationSession> ret = new List<OpCalibrationSession>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpCalibrationSession(db_object)));

            return ret;
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpCalibrationSession)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpCalibrationSession).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpCalibrationSession> Members
        public bool Equals(OpCalibrationSession other)
        {
            if (other == null)
                return false;
            return this.IdCalibrationSession.Equals(other.IdCalibrationSession);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpCalibrationSession)
                return this.IdCalibrationSession == ((OpCalibrationSession)obj).IdCalibrationSession;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpCalibrationSession left, OpCalibrationSession right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpCalibrationSession left, OpCalibrationSession right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdCalibrationSession.GetHashCode();
        }
    #endregion
    }
#endif
}