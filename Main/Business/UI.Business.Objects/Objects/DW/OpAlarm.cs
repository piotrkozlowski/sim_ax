using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
	public class OpAlarm : DB_ALARM, IComparable, IEquatable<OpAlarm>, IOpChangeState, IOpDynamic, IOpDynamicProperty<OpAlarm>, IOpObject<Custom.OpAlarmData>
    {
    #region Properties
        public long IdAlarm { get { return this.ID_ALARM;} set {this.ID_ALARM = value;}}
        public long IdAlarmEvent { get { return this.ID_ALARM_EVENT;} set {this.ID_ALARM_EVENT = value;}}
        public int? IdAlarmDef { get { return this.ID_ALARM_DEF;} set {this.ID_ALARM_DEF = value;}}
        public long? SerialNbr { get { return this.SERIAL_NBR;} set {this.SERIAL_NBR = value;}}
        public long? IdMeter { get { return this.ID_METER;} set {this.ID_METER = value;}}
        public long? IdLocation { get { return this.ID_LOCATION;} set {this.ID_LOCATION = value;}}
        public int IdAlarmType { get { return this.ID_ALARM_TYPE;} set {this.ID_ALARM_TYPE = value;}}
        public long IdDataTypeAlarm { get { return this.ID_DATA_TYPE_ALARM;} set {this.ID_DATA_TYPE_ALARM = value;}}
        public object SystemAlarmValue { get { return this.SYSTEM_ALARM_VALUE;} set {this.SYSTEM_ALARM_VALUE = value;}}
        public object AlarmValue { get { return this.ALARM_VALUE;} set {this.ALARM_VALUE = value;}}
        public int IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
        public int IdAlarmStatus { get { return this.ID_ALARM_STATUS;} set {this.ID_ALARM_STATUS = value;}}
        public int? IdTransmissionType { get { return this.ID_TRANSMISSION_TYPE;} set {this.ID_TRANSMISSION_TYPE = value;}}
        public int? IdAlarmGroup { get { return this.ID_ALARM_GROUP;} set {this.ID_ALARM_GROUP = value;}}
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpMeter _Meter;
		public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null)? null : (long?)value.ID_METER; } }

        [DataMember]
        private OpLocation _Location;
		public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null)? null : (long?)value.ID_LOCATION; } }

        [DataMember]
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }

        [DataMember]
        private OpAlarmType _AlarmType;
		public OpAlarmType AlarmType { get { return this._AlarmType; } set { this._AlarmType = value; this.ID_ALARM_TYPE = (value == null)? 0 : (int)value.ID_ALARM_TYPE; } }

        [DataMember]
        private OpOperator _Operator;
		public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }

        [DataMember]
        private OpAlarmStatus _AlarmStatus;
		public OpAlarmStatus AlarmStatus { get { return this._AlarmStatus; } set { this._AlarmStatus = value; this.ID_ALARM_STATUS = (value == null)? 0 : (int)value.ID_ALARM_STATUS; } }

        [DataMember]
        private OpTransmissionType _TransmissionType;
        public OpTransmissionType TransmissionType { get { return this._TransmissionType; } set { this._TransmissionType = value; this.ID_TRANSMISSION_TYPE = (value == null) ? 0 : (int)value.ID_TRANSMISSION_TYPE; } }
        #endregion

        #region	Custom Properties
        [DataMember]
        private Dictionary<string, object> dynamicValues;

        private OpDynamicPropertyDict dynamicProperties;

        [DataMember]
        public OpChangeState OpState { get; set; }

        #endregion

        #region	Ctor
        public OpAlarm()
			:base()
        {
            this.DataList = new OpDataList<Custom.OpAlarmData>();
        }
		
		public OpAlarm(DB_ALARM clone)
			:base(clone)
        {
            this.DataList = new OpDataList<Custom.OpAlarmData>();
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.TRANSMISSION_TYPE;
		}
    #endregion

    #region	ConvertList
		public static List<OpAlarm> ConvertList(DB_ALARM[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpAlarm> ConvertList(DB_ALARM[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpAlarm> ret = new List<OpAlarm>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarm(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpAlarm> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Where(l => l.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
	            Dictionary<long,OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
	            Dictionary<int,OpAlarmType> AlarmTypeDict = dataProvider.GetAlarmType(list.Select(l => l.ID_ALARM_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_TYPE);
	            Dictionary<int,OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
	            Dictionary<int,OpAlarmStatus> AlarmStatusDict = dataProvider.GetAlarmStatus(list.Select(l => l.ID_ALARM_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_STATUS);
                Dictionary<int, OpTransmissionType> TransmissionTypeDict = dataProvider.GetTransmissionType(list.Where(w => w.ID_TRANSMISSION_TYPE.HasValue).Select(l => l.ID_TRANSMISSION_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_TYPE);
			
				foreach (var loop in list)
				{
                    if (loop.SERIAL_NBR.HasValue)
                        loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR.Value);
                    if (loop.ID_METER.HasValue)
				    	loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value); 
		            if (loop.ID_LOCATION.HasValue)
						loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value); 
		            loop.AlarmType = AlarmTypeDict.TryGetValue(loop.ID_ALARM_TYPE); 
		            loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR); 
		            loop.AlarmStatus = AlarmStatusDict.TryGetValue(loop.ID_ALARM_STATUS);
                    if (loop.ID_TRANSMISSION_TYPE.HasValue)
                        loop.TransmissionType = TransmissionTypeDict.TryGetValue(loop.ID_TRANSMISSION_TYPE.Value);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpAlarm> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarm)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarm).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpAlarm> Members
        public bool Equals(OpAlarm other)
        {
            if (other == null)
				return false;
			return this.IdAlarm.Equals(other.IdAlarm);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpAlarm)
				return this.IdAlarm == ((OpAlarm)obj).IdAlarm;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpAlarm left, OpAlarm right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpAlarm left, OpAlarm right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdAlarm.GetHashCode();
		}
        #endregion

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdAlarm; }
        }

        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpAlarm IOpDynamicProperty<OpAlarm>.Owner
        {
            get { return this; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdAlarm; }
            set { IdAlarm = Convert.ToInt64(value); }
        }

        [DataMember]
        public OpDataList<Custom.OpAlarmData> DataList { get; set; }
        #endregion

    }
#endif
}