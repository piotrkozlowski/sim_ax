using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMonitoringWatcherType : DB_MONITORING_WATCHER_TYPE, IComparable, IEquatable<OpMonitoringWatcherType>
    {
        #region Properties
        public int IdMonitoringWatcherType { get { return this.ID_MONITORING_WATCHER_TYPE; } set { this.ID_MONITORING_WATCHER_TYPE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Plugin { get { return this.PLUGIN; } set { this.PLUGIN = value; } }
        #endregion

        #region	Navigation Properties
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpMonitoringWatcherType()
            : base() { }

        public OpMonitoringWatcherType(DB_MONITORING_WATCHER_TYPE clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpMonitoringWatcherType> ConvertList(DB_MONITORING_WATCHER_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMonitoringWatcherType> ConvertList(DB_MONITORING_WATCHER_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMonitoringWatcherType> ret = new List<OpMonitoringWatcherType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMonitoringWatcherType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMonitoringWatcherType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpMonitoringWatcherType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMonitoringWatcherType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMonitoringWatcherType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpMonitoringWatcherType> Members
        public bool Equals(OpMonitoringWatcherType other)
        {
            if (other == null)
                return false;
            return this.IdMonitoringWatcherType.Equals(other.IdMonitoringWatcherType);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMonitoringWatcherType)
                return this.IdMonitoringWatcherType == ((OpMonitoringWatcherType)obj).IdMonitoringWatcherType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMonitoringWatcherType left, OpMonitoringWatcherType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMonitoringWatcherType left, OpMonitoringWatcherType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMonitoringWatcherType.GetHashCode();
        }
        #endregion
    }
#endif
}