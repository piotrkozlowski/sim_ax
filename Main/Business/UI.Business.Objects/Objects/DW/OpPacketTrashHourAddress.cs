﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPacketTrashHourAddress : DB_PACKET_TRASH_HOUR_ADDRESS, IComparable, IEquatable<OpPacketTrashHourAddress>
    {
    #region Properties
        public string Address { get { return this.ADDRESS; } set { this.ADDRESS = value; } }
        public DateTime Date { get { return this.DATE; } set { this.DATE = value; } }
        public bool IsIncoming { get { return this.IS_INCOMING; } set { this.IS_INCOMING = value; } }
        public int Packets { get { return this.PACKETS; } set { this.PACKETS = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpPacketTrashHourAddress()
            : base() { }

        public OpPacketTrashHourAddress(DB_PACKET_TRASH_HOUR_ADDRESS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.ADDRESS;
        }
    #endregion

    #region	ConvertList
        public static List<OpPacketTrashHourAddress> ConvertList(DB_PACKET_TRASH_HOUR_ADDRESS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPacketTrashHourAddress> ConvertList(DB_PACKET_TRASH_HOUR_ADDRESS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpPacketTrashHourAddress> ret = new List<OpPacketTrashHourAddress>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPacketTrashHourAddress(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPacketTrashHourAddress> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPacketTrashHourAddress> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPacketTrashHourAddress)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPacketTrashHourAddress).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPacketTrashHourAddress> Members
        public bool Equals(OpPacketTrashHourAddress other)
        {
            if (other == null)
                return false;
            return this.Address.Equals(other.Address);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPacketTrashHourAddress)
                return this.Address == ((OpPacketTrashHourAddress)obj).Address;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPacketTrashHourAddress left, OpPacketTrashHourAddress right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPacketTrashHourAddress left, OpPacketTrashHourAddress right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return Address.GetHashCode();
        }
    #endregion
    }
#endif
}