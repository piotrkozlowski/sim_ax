using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpRefuelData : DB_REFUEL_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpRefuelData>
    {
    #region Properties
        public long IdRefuelData { get { return this.ID_REFUEL_DATA; } set { this.ID_REFUEL_DATA = value; } }
        public long IdRefuel { get { return this.ID_REFUEL; } set { this.ID_REFUEL = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        public DateTime? StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public int? Status { get { return this.STATUS; } set { this.STATUS = value; } }
    #endregion

    #region	Navigation Properties
        private OpRefuel _Refuel;
        public OpRefuel Refuel { get { return this._Refuel; } set { this._Refuel = value; this.ID_REFUEL = (value == null) ? 0 : (long)value.ID_REFUEL; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public long IdData { get { return IdRefuelData; } set { IdRefuelData = value; } }
        public OpChangeState OpState { get; set; }
        //wykorzystywany do zmiany jednostki bazowej dla wybranego pomiaru
        public OpUnit Unit { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpRefuelData()
            : base() { }

        public OpRefuelData(DB_REFUEL_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "RefuelData [" + IdRefuelData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpRefuelData> ConvertList(DB_REFUEL_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRefuelData> ConvertList(DB_REFUEL_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpRefuelData> ret = new List<OpRefuelData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRefuelData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRefuelData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<long, OpRefuel> RefuelDict = dataProvider.GetRefuel(list.Select(l => l.ID_REFUEL).Distinct().ToArray()).ToDictionary(l => l.ID_REFUEL);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    //loop.Refuel = RefuelDict.TryGetValue(loop.ID_REFUEL);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRefuelData> list, IDataProvider dataProvider)
        {
            foreach (OpRefuelData loop in list)
            {
                if (loop.DataType != null && loop.DataType.Unit != null)
                    loop.Unit = (OpUnit)loop.DataType.Unit.Clone(dataProvider);
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRefuelData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRefuelData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpRefuelData> Members
        public bool Equals(OpRefuelData other)
        {
            if (other == null)
                return false;
            return this.IdRefuelData.Equals(other.IdRefuelData);
        }
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpRefuelData clone = new OpRefuelData();
            clone.IdData = this.IdData;
            clone.IdRefuel = this.IdRefuel;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_REFUEL_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.Task = dataProvider.GetTask(this.ID_TASK);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRefuelData)
                return this.IdRefuelData == ((OpRefuelData)obj).IdRefuelData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRefuelData left, OpRefuelData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRefuelData left, OpRefuelData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRefuelData.GetHashCode();
        }
        #endregion

        #region ChangeUnit - List of DW.OpRefuelData
        public static List<Objects.DW.OpRefuelData> ChangeUnit(List<Objects.DW.OpRefuelData> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit newUnit = dataTypeUnitDict[dataType.IdDataType];
                            if (newUnit != null && dataType.Unit != null && dataType.Unit.IdUnitBase == newUnit.IdUnitBase && newUnit.IdUnit != dataType.Unit.IdUnit && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType).ToList().ForEach(d => d.Unit = newUnit);
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (dataType.Unit.Scale * Convert.ToDouble(d.Value) + dataType.Unit.Bias - newUnit.Bias) / newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (dataType.Unit.Scale * Convert.ToDouble(d.Value) + dataType.Unit.Bias - newUnit.Bias) / newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = ((decimal)dataType.Unit.Scale * Convert.ToDecimal(d.Value) + (decimal)dataType.Unit.Bias - (decimal)newUnit.Bias) / (decimal)newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDecimal(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                }

                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        public static List<Objects.DW.OpRefuelData> ChangeUnit(List<Objects.DW.OpRefuelData> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (fromDataTypeUnitDict.Keys.Count > 0 && toDataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && fromDataTypeUnitDict.ContainsKey(dataType.IdDataType) && toDataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit fromUnit = fromDataTypeUnitDict[dataType.IdDataType];
                            OpUnit toUnit = toDataTypeUnitDict[dataType.IdDataType];
                            if (fromUnit != null && toUnit != null && fromUnit.IdUnitBase == toUnit.IdUnitBase && fromUnit.IdUnit != toUnit.IdUnit && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType).ToList().ForEach(d => d.Unit = toUnit);
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });

                                        break;
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });

                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = ((decimal)fromUnit.Scale * Convert.ToDecimal(d.Value) + (decimal)fromUnit.Bias - (decimal)toUnit.Bias) / (decimal)toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDecimal(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });

                                        break;
                                }

                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
    }
#endif
}