using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpLocationDistance : DB_LOCATION_DISTANCE, IComparable, IEquatable<OpLocationDistance>
    {
    #region Properties
        public long IdLocationOrigin { get { return this.ID_LOCATION_ORIGIN;} set {this.ID_LOCATION_ORIGIN = value;}}
        public long IdLocationDest { get { return this.ID_LOCATION_DEST;} set {this.ID_LOCATION_DEST = value;}}
        public Double? DistanceStraight { get { return this.DISTANCE_STRAIGHT;} set {this.DISTANCE_STRAIGHT = value;}}
        public Double? DistanceFormula { get { return this.DISTANCE_FORMULA;} set {this.DISTANCE_FORMULA = value;}}
        public Double? DistanceGoogle { get { return this.DISTANCE_GOOGLE;} set {this.DISTANCE_GOOGLE = value;}}
        public DateTime? LastGoogleUpdate { get { return this.LAST_GOOGLE_UPDATE;} set {this.LAST_GOOGLE_UPDATE = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpLocationDistance()
			:base() {}
		
		public OpLocationDistance(DB_LOCATION_DISTANCE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "LocationDistance [" + IdLocationOrigin.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpLocationDistance> ConvertList(DB_LOCATION_DISTANCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpLocationDistance> ConvertList(DB_LOCATION_DISTANCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpLocationDistance> ret = new List<OpLocationDistance>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpLocationDistance(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpLocationDistance> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpLocationDistance> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationDistance)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationDistance).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpLocationDistance> Members
        public bool Equals(OpLocationDistance other)
        {
            if (other == null)
				return false;
			return this.IdLocationOrigin.Equals(other.IdLocationOrigin);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpLocationDistance)
				return this.IdLocationOrigin == ((OpLocationDistance)obj).IdLocationOrigin;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpLocationDistance left, OpLocationDistance right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpLocationDistance left, OpLocationDistance right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdLocationOrigin.GetHashCode();
		}
    #endregion
    }
#endif
}