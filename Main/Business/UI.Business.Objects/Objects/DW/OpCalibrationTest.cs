using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpCalibrationTest : DB_CALIBRATION_TEST, IComparable, IEquatable<OpCalibrationTest>
    {
    #region Properties
        public long IdCalibrationTest { get { return this.ID_CALIBRATION_TEST; } set { this.ID_CALIBRATION_TEST = value; } }
        public int? IdCalibrationModule { get { return this.ID_CALIBRATION_MODULE; } set { this.ID_CALIBRATION_MODULE = value; } }
        public int IdCalibrationSession { get { return this.ID_CALIBRATION_SESSION; } set { this.ID_CALIBRATION_SESSION = value; } }
        public int IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public int IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpCalibrationTest()
            : base() { }

        public OpCalibrationTest(DB_CALIBRATION_TEST clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpCalibrationTest> ConvertList(DB_CALIBRATION_TEST[] db_objects)
        {
            List<OpCalibrationTest> ret = new List<OpCalibrationTest>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpCalibrationTest(db_object)));

            return ret;
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpCalibrationTest)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpCalibrationTest).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpCalibrationSession> Members
        public bool Equals(OpCalibrationTest other)
        {
            if (other == null)
                return false;
            return this.IdCalibrationTest.Equals(other.IdCalibrationTest);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpCalibrationTest)
                return this.IdCalibrationTest == ((OpCalibrationTest)obj).IdCalibrationTest;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpCalibrationTest left, OpCalibrationTest right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpCalibrationTest left, OpCalibrationTest right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdCalibrationTest.GetHashCode();
        }
    #endregion
    }
#endif
}