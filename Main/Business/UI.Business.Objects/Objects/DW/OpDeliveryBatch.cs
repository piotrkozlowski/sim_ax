using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeliveryBatch : DB_DELIVERY_BATCH, IComparable, IEquatable<OpDeliveryBatch>
    {
    #region Properties
        public int IdDeliveryBatch { get { return this.ID_DELIVERY_BATCH; } set { this.ID_DELIVERY_BATCH = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int IdDeliveryBatchStatus { get { return this.ID_DELIVERY_BATCH_STATUS; } set { this.ID_DELIVERY_BATCH_STATUS = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public DateTime? CommitDate { get { return this.COMMIT_DATE; } set { this.COMMIT_DATE = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public int SuccessTotal { get { return this.SUCCESS_TOTAL; } set { this.SUCCESS_TOTAL = value; } }
        public int FailedTotal { get { return this.FAILED_TOTAL; } set { this.FAILED_TOTAL = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeliveryBatchStatus _DeliveryBatchStatus;
        public OpDeliveryBatchStatus DeliveryBatchStatus { get { return this._DeliveryBatchStatus; } set { this._DeliveryBatchStatus = value; this.ID_DELIVERY_BATCH_STATUS = (value == null) ? 0 : (int)value.ID_DELIVERY_BATCH_STATUS; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeliveryBatch()
            : base() { }

        public OpDeliveryBatch(DB_DELIVERY_BATCH clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeliveryBatch> ConvertList(DB_DELIVERY_BATCH[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeliveryBatch> ConvertList(DB_DELIVERY_BATCH[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeliveryBatch> ret = new List<OpDeliveryBatch>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeliveryBatch(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeliveryBatch> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeliveryBatchStatus> DeliveryBatchStatusDict = dataProvider.GetDeliveryBatchStatus(list.Select(l => l.ID_DELIVERY_BATCH_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_DELIVERY_BATCH_STATUS);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                foreach (var loop in list)
                {
                    loop.DeliveryBatchStatus = DeliveryBatchStatusDict.TryGetValue(loop.ID_DELIVERY_BATCH_STATUS);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeliveryBatch> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeliveryBatch)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeliveryBatch).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeliveryBatch> Members
        public bool Equals(OpDeliveryBatch other)
        {
            if (other == null)
                return false;
            return this.IdDeliveryBatch.Equals(other.IdDeliveryBatch);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeliveryBatch)
                return this.IdDeliveryBatch == ((OpDeliveryBatch)obj).IdDeliveryBatch;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeliveryBatch left, OpDeliveryBatch right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeliveryBatch left, OpDeliveryBatch right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeliveryBatch.GetHashCode();
        }
    #endregion
    }
#endif
}