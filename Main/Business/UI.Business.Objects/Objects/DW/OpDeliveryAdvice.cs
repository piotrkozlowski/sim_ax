using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeliveryAdvice : DB_DELIVERY_ADVICE, IComparable, IEquatable<OpDeliveryAdvice>, IReferenceType
    {
    #region Properties
        public int IdDeliveryAdvice { get { return this.ID_DELIVERY_ADVICE; } set { this.ID_DELIVERY_ADVICE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public int Priority { get { return this.PRIORITY; } set { this.PRIORITY = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        public OpDeliveryAdvice PreviousAdvice { get; set; }
    #endregion

    #region	Ctor
        public OpDeliveryAdvice()
            : base() { }

        public OpDeliveryAdvice(DB_DELIVERY_ADVICE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            string result = Descr != null ? Descr.Description : Name;
            if (IdDeliveryAdvice == (int)Enums.DeliveryAdvice.Planned && PreviousAdvice != null)
                return String.Format("{0} ({1})", result, PreviousAdvice);
            return result;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeliveryAdvice> ConvertList(DB_DELIVERY_ADVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeliveryAdvice> ConvertList(DB_DELIVERY_ADVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeliveryAdvice> ret = new List<OpDeliveryAdvice>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeliveryAdvice(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeliveryAdvice> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeliveryAdvice> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeliveryAdvice)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeliveryAdvice).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeliveryAdvice> Members
        public bool Equals(OpDeliveryAdvice other)
        {
            if (other == null)
                return false;
            return this.IdDeliveryAdvice.Equals(other.IdDeliveryAdvice);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeliveryAdvice)
                return this.IdDeliveryAdvice == ((OpDeliveryAdvice)obj).IdDeliveryAdvice;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeliveryAdvice left, OpDeliveryAdvice right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeliveryAdvice left, OpDeliveryAdvice right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeliveryAdvice.GetHashCode();
        }
    #endregion

    #region IReferenceType
        public object GetReferenceKey()
        {
            return this.ID_DELIVERY_ADVICE;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}