using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using System.Runtime.Serialization;
using IMR.Suite.UI.Business.Objects.CORE;
using Custom = IMR.Suite.UI.Business.Objects.Custom;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpAlarmEvent : DB_ALARM_EVENT, IComparable, IEquatable<OpAlarmEvent>, IReferenceType, IOpChangeState, IOpDynamic, IOpDynamicProperty<OpAlarmEvent>, IOpObject<Custom.OpAlarmEventData>
    {
        #region Properties
        public long IdAlarmEvent { get { return this.ID_ALARM_EVENT; } set { this.ID_ALARM_EVENT = value; } }
        public long IdAlarmDef { get { return this.ID_ALARM_DEF; } set { this.ID_ALARM_DEF = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public int IdAlarmType { get { return this.ID_ALARM_TYPE; } set { this.ID_ALARM_TYPE = value; } }
        public int IdDataTypeAlarm { get { return this.ID_DATA_TYPE_ALARM; } set { this.ID_DATA_TYPE_ALARM = value; } }
        public object SystemAlarmValue { get { return this.SYSTEM_ALARM_VALUE; } set { this.SYSTEM_ALARM_VALUE = value; } }
        public object AlarmValue { get { return this.ALARM_VALUE; } set { this.ALARM_VALUE = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public long? IdIssue { get { return this.ID_ISSUE; } set { this.ID_ISSUE = value; } }
        public bool IsConfirmed { get { return this.IS_CONFIRMED; } set { this.IS_CONFIRMED = value; } }
        public int? ConfirmedBy { get { return this.CONFIRMED_BY; } set { this.CONFIRMED_BY = value; } }
        public DateTime? ConfirmTime { get { return this.CONFIRM_TIME; } set { this.CONFIRM_TIME = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        [DataMember]
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        [DataMember]
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }
        [DataMember]
        private OpAlarmType _AlarmType;
        public OpAlarmType AlarmType { get { return this._AlarmType; } set { this._AlarmType = value; this.ID_ALARM_TYPE = (value == null) ? 0 : (int)value.ID_ALARM_TYPE; } }
        [DataMember]
        private OpAlarmEvent _AlarmEvent;
        public OpAlarmEvent AlarmEvent { get { return this._AlarmEvent; } set { this._AlarmEvent = value; this.ID_ALARM_EVENT = (value == null) ? 0 : (int)value.ID_ALARM_EVENT; } }
        #endregion

        #region	Custom Properties
        [DataMember]
        private Dictionary<string, object> dynamicValues;
        
        private OpDynamicPropertyDict dynamicProperties;

        [DataMember]
        public OpChangeState OpState { get; set; }

        [DataMember]
        public OpAlarmMessage AlarmMessage { get; set; }
        #endregion

        #region	Ctor
        public OpAlarmEvent()
            : base()
        {
            this.OpState = OpChangeState.New;
            this.DynamicValues = new Dictionary<string, object>();
            this.DataList = new OpDataList<Custom.OpAlarmEventData>();
        }

        public OpAlarmEvent(DB_ALARM_EVENT clone)
            : base(clone)
        {
            this.OpState = OpChangeState.Loaded;
            this.DynamicValues = new Dictionary<string, object>();
            this.DataList = new OpDataList<Custom.OpAlarmEventData>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            if (this.AlarmMessage != null)
                return this.AlarmMessage.ToString();
            if (this.Location != null)
                return this.Location.ToString();
            return "AlarmEvent [" + IdAlarmEvent.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpAlarmEvent> ConvertList(DB_ALARM_EVENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmEvent> ConvertList(DB_ALARM_EVENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpAlarmEvent> ret = new List<OpAlarmEvent>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmEvent(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmEvent> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpAlarmType> AlarmTypeDict = dataProvider.GetAlarmType(list.Select(l => l.ID_ALARM_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_TYPE);

                foreach (var loop in list)
                {
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.AlarmType = AlarmTypeDict.TryGetValue(loop.ID_ALARM_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmEvent> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmEvent)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmEvent).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpAlarmEvent> Members
        public bool Equals(OpAlarmEvent other)
        {
            if (other == null)
                return false;
            return this.IdAlarmEvent.Equals(other.IdAlarmEvent);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmEvent)
                return this.IdAlarmEvent == ((OpAlarmEvent)obj).IdAlarmEvent;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmEvent left, OpAlarmEvent right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmEvent left, OpAlarmEvent right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmEvent.GetHashCode();
        }

        #endregion

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdAlarmEvent; }
        }

        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpAlarmEvent IOpDynamicProperty<OpAlarmEvent>.Owner
        {
            get { return this; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdAlarmEvent; }
            set { IdAlarmEvent = Convert.ToInt64(value); }
        }

        [DataMember]
        public OpDataList<Custom.OpAlarmEventData> DataList { get; set; }
        #endregion
        #region Implementation of IOpObject
        public object GetReferenceKey()
        {
            return this.IdAlarmEvent;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion
    }
#endif
}