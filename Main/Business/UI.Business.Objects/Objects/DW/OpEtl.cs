using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    //[Serializable]
    //public class OpEtl : DB_ETL, IComparable, IEquatable<OpEtl>
    //{
    //    #region Properties
    //    public int IdParam { get { return this.ID_PARAM;} set {this.ID_PARAM = value;}}
    //    public string Code { get { return this.CODE;} set {this.CODE = value;}}
    //    public string Descr { get { return this.DESCR;} set {this.DESCR = value;}}
    //    public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
    //    #endregion

    //    #region	Navigation Properties
    //    #endregion

    //    #region	Custom Properties
    //    #endregion
		
    //    #region	Ctor
    //    public OpEtl()
    //        :base() {}
		
    //    public OpEtl(DB_ETL clone)
    //        :base(clone) {}
    //    #endregion
		
    //    #region	ToString
    //    public override string ToString()
    //    {
    //            return this.CODE;
    //    }
    //    #endregion

    //    #region	ConvertList
    //    public static List<OpEtl> ConvertList(DB_ETL[] db_objects, IDataProvider dataProvider)
    //    {
    //        return ConvertList(db_objects, dataProvider, true);
    //    }
		
    //    public static List<OpEtl> ConvertList(DB_ETL[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
    //    {
    //        List<OpEtl> ret = new List<OpEtl>(db_objects.Length);
    //        db_objects.ToList().ForEach(db_object => ret.Add(new OpEtl(db_object)));
			
    //        if(loadNavigationProperties)
    //            LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
    //        if(loadCustomData)
    //            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
    //        return ret;
    //    }
    //    #endregion
		
    //    #region LoadNavigationProperties
    //    public static void LoadNavigationProperties(ref List<OpEtl> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
    //    {
    //        if (list != null && list.Count > 0)
    //        {
					
    //            foreach (var loop in list)
    //            {
    //                            }
    //        }
    //    }
    //    #endregion
		
    //    #region LoadCustomData
    //    private static void LoadCustomData(ref List<OpEtl> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
    //    {
    //    }
    //    #endregion
		
    //    #region	IComparable Members
    //    //Used for XtraGridControl column sort feature
    //    public int CompareTo(object obj)
    //    {
    //        if (obj is OpEtl)
    //        {
    //            if (this.ToString() != null)
    //                return this.ToString().CompareTo((obj as OpEtl).ToString());
    //            else
    //                return 0;
    //        }
    //        else
    //            return 0;
    //    }
    //    #endregion
		
    //    #region IEquatable<OpEtl> Members
    //    public bool Equals(OpEtl other)
    //    {
    //        if (other == null)
    //            return false;
    //        return this.IdParam.Equals(other.IdParam);
    //    }
    //    #endregion

    //    #region override Equals
    //    public override bool Equals(object obj)
    //    {
    //        if (obj is OpEtl)
    //            return this.IdParam == ((OpEtl)obj).IdParam;
    //        else
    //            return base.Equals(obj);
    //    }
    //    public static bool operator ==(OpEtl left, OpEtl right)
    //    {
    //        if ((object)left == null)
    //            return ((object)right == null);
    //        return left.Equals(right);
    //    }
    //    public static bool operator !=(OpEtl left, OpEtl right)
    //    {
    //        if ((object)left == null)
    //            return ((object)right != null);
    //        return !left.Equals(right);
    //    }

    //    public override int GetHashCode()
    //    {
    //        return IdParam.GetHashCode();
    //    }
    //    #endregion
    //}
#endif
}