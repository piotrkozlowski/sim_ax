using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeliveryBatchStatus : DB_DELIVERY_BATCH_STATUS, IComparable, IEquatable<OpDeliveryBatchStatus>
    {
    #region Properties
        public int IdDeliveryBatchStatus { get { return this.ID_DELIVERY_BATCH_STATUS; } set { this.ID_DELIVERY_BATCH_STATUS = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpDeliveryBatchStatus()
            : base() { }

        public OpDeliveryBatchStatus(DB_DELIVERY_BATCH_STATUS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeliveryBatchStatus> ConvertList(DB_DELIVERY_BATCH_STATUS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeliveryBatchStatus> ConvertList(DB_DELIVERY_BATCH_STATUS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeliveryBatchStatus> ret = new List<OpDeliveryBatchStatus>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeliveryBatchStatus(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeliveryBatchStatus> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeliveryBatchStatus> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeliveryBatchStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeliveryBatchStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeliveryBatchStatus> Members
        public bool Equals(OpDeliveryBatchStatus other)
        {
            if (other == null)
                return false;
            return this.IdDeliveryBatchStatus.Equals(other.IdDeliveryBatchStatus);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeliveryBatchStatus)
                return this.IdDeliveryBatchStatus == ((OpDeliveryBatchStatus)obj).IdDeliveryBatchStatus;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeliveryBatchStatus left, OpDeliveryBatchStatus right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeliveryBatchStatus left, OpDeliveryBatchStatus right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeliveryBatchStatus.GetHashCode();
        }
    #endregion
    }
#endif
}