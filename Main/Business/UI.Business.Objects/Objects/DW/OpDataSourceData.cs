using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDataSourceData : DB_DATA_SOURCE_DATA, IComparable, IEquatable<OpDataSourceData>
    {
    #region Properties
            public long IdDataSource { get { return this.ID_DATA_SOURCE;} set {this.ID_DATA_SOURCE = value;}}
                public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
                public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
    #endregion

    #region	Navigation Properties
		private OpDataSource _DataSource;
				public OpDataSource DataSource { get { return this._DataSource; } set { this._DataSource = value; this.ID_DATA_SOURCE = (value == null)? 0 : (long)value.ID_DATA_SOURCE; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDataSourceData()
			:base() {}
		
		public OpDataSourceData(DB_DATA_SOURCE_DATA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "DataSourceData [" + IdDataSource.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpDataSourceData> ConvertList(DB_DATA_SOURCE_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpDataSourceData> ConvertList(DB_DATA_SOURCE_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDataSourceData> ret = new List<OpDataSourceData>(list.Length);
			foreach (var loop in list)
			{
				OpDataSourceData insert = new OpDataSourceData(loop);
				ret.Add(insert);
			}
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDataSourceData> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
						loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDataSourceData> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataSourceData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataSourceData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDataSourceData> Members
        public bool Equals(OpDataSourceData other)
        {
            if (other == null)
				return false;
			return this.IdDataSource.Equals(other.IdDataSource);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDataSourceData)
				return this.IdDataSource == ((OpDataSourceData)obj).IdDataSource;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDataSourceData left, OpDataSourceData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDataSourceData left, OpDataSourceData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDataSource.GetHashCode();
		}
    #endregion
    }
#endif
}