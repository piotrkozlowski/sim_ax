﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpImrServerPerformance : DB_IMR_SERVER_PERFORMANCE, IComparable, IEquatable<OpImrServerPerformance>
    {
    #region Properties
        public long IdImrServerPerformance { get { return this.ID_IMR_SERVER_PERFORMANCE; } set { this.ID_IMR_SERVER_PERFORMANCE = value; } }
        public int IdImrServer { get { return this.ID_IMR_SERVER; } set { this.ID_IMR_SERVER = value; } }
        public int IdAggregationType { get { return this.ID_AGGREGATION_TYPE; } set { this.ID_AGGREGATION_TYPE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpImrServerPerformance()
            : base() { }

        public OpImrServerPerformance(DB_IMR_SERVER_PERFORMANCE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ImrServerPerformance [" + IdImrServerPerformance.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpImrServerPerformance> ConvertList(DB_IMR_SERVER_PERFORMANCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpImrServerPerformance> ConvertList(DB_IMR_SERVER_PERFORMANCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpImrServerPerformance> ret = new List<OpImrServerPerformance>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpImrServerPerformance(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpImrServerPerformance> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpImrServerPerformance> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpImrServerPerformance)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpImrServerPerformance).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpImrServerPerformance> Members
        public bool Equals(OpImrServerPerformance other)
        {
            if (other == null)
                return false;
            return this.IdImrServerPerformance.Equals(other.IdImrServerPerformance);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpImrServerPerformance)
                return this.IdImrServerPerformance == ((OpImrServerPerformance)obj).IdImrServerPerformance;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpImrServerPerformance left, OpImrServerPerformance right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpImrServerPerformance left, OpImrServerPerformance right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdImrServerPerformance.GetHashCode();
        }
    #endregion
    }
#endif
}