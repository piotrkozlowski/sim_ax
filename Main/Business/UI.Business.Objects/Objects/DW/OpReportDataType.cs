using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpReportDataType : DB_REPORT_DATA_TYPE, IComparable, IEquatable<OpReportDataType>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public long IdReportDataType { get { return this.ID_REPORT_DATA_TYPE; } set { if (this.ID_REPORT_DATA_TYPE != value) { this.ID_REPORT_DATA_TYPE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Signature { get { return this.SIGNATURE; } set { if (this.SIGNATURE != value) { this.SIGNATURE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { if (this.ID_DATA_TYPE != value) { this.OldIdDataType = this.ID_DATA_TYPE; this.ID_DATA_TYPE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdUnit { get { return this.ID_UNIT; } set { if (this.ID_UNIT != value) { this.ID_UNIT = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? DigitsAfterComma { get { return this.DIGITS_AFTER_COMMA; } set { if (this.DIGITS_AFTER_COMMA != value) { this.DIGITS_AFTER_COMMA = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? SequenceNbr { get { return this.SEQUENCE_NBR; } set { if (this.SEQUENCE_NBR != value) { this.SEQUENCE_NBR = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { if (this._DataType != value) { this.OldIdDataType = this.ID_DATA_TYPE; this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        private OpUnit _Unit;
        [XmlIgnore]
        public OpUnit Unit { get { return this._Unit; } set { if (this._Unit != value) { this._Unit = value; this.ID_UNIT = (value == null) ? 0 : (int)value.ID_UNIT; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Custom Properties
        public long? IdReportDataTypeParent { get; set; }
        public long IdReportDataTypeChild { get; set; }
        public OpChangeState OpState { get; set; }
        public long OldIdDataType { get; set; }
    #endregion

    #region	Ctor
        public OpReportDataType()
            : base() { }

        public OpReportDataType(DB_REPORT_DATA_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.IdReportDataType.ToString();
        }
    #endregion

    #region	ConvertList
        public static List<OpReportDataType> ConvertList(DB_REPORT_DATA_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpReportDataType> ConvertList(DB_REPORT_DATA_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpReportDataType> ret = new List<OpReportDataType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpReportDataType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpReportDataType> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpUnit> UnitDict = dataProvider.GetUnit(list.Select(l => l.ID_UNIT).Distinct().ToArray()).ToDictionary(l => l.ID_UNIT);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    loop.Unit = UnitDict.TryGetValue(loop.ID_UNIT);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpReportDataType> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpReportDataType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpReportDataType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpReportDataType> Members
        public bool Equals(OpReportDataType other)
        {
            if (other == null)
                return false;
            return this.IdReportDataType.Equals(other.IdReportDataType) && this.IdDataType.Equals(other.IdDataType);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpReportDataType)
                return this.IdReportDataType == ((OpReportDataType)obj).IdReportDataType && this.IdDataType == ((OpReportDataType)obj).IdDataType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpReportDataType left, OpReportDataType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpReportDataType left, OpReportDataType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdReportDataType.GetHashCode() ^ IdDataType.GetHashCode();
        }
    #endregion

    #region IReferenceType members
        public object GetReferenceKey()
        {
            return this.IdReportDataType;
        }

        public object GetReferenceValue()
        {
            return this;
        } 
    #endregion
    }
#endif
}