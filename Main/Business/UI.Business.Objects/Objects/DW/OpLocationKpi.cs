﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLocationKpi : DB_LOCATION_KPI, IComparable, IEquatable<OpLocationKpi>
    {
    #region Properties
        public long IdLocationKpi { get { return this.ID_LOCATION_KPI; } set { this.ID_LOCATION_KPI = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public DateTime Date { get { return this.DATE; } set { this.DATE = value; } }
        public double Kpi { get { return this.KPI; } set { this.KPI = value; } }
        public int? New { get { return this.NEW; } set { this.NEW = value; } }
        public int? Operational { get { return this.OPERATIONAL; } set { this.OPERATIONAL = value; } }
        public int? Suspended { get { return this.SUSPENDED; } set { this.SUSPENDED = value; } }
        public int? Pending { get { return this.PENDING; } set { this.PENDING = value; } }
        public int? KpiRelevant { get { return this.KPI_RELEVANT; } set { this.KPI_RELEVANT = value; } }
        public int? KpiIrrelevant { get { return this.KPI_IRRELEVANT; } set { this.KPI_IRRELEVANT = value; } }
        public int? KpiConforming { get { return this.KPI_CONFORMING; } set { this.KPI_CONFORMING = value; } }
        public int? KpiNonConforming { get { return this.KPI_NONCONFORMING; } set { this.KPI_NONCONFORMING = value; } }
        public int? SrtConforming { get { return this.SRT_CONFORMING; } set { this.SRT_CONFORMING = value; } }
        public bool IsMantenanceOnly { get { return this.IS_MAINTENANCE_ONLY; } set { this.IS_MAINTENANCE_ONLY = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpLocationKpi()
            : base()
        {
        }

        public OpLocationKpi(DB_LOCATION_KPI clone)
            : base(clone)
        {
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "LocationKpi [" + IdLocationKpi.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationKpi> ConvertList(DB_LOCATION_KPI[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpLocationKpi> ConvertList(DB_LOCATION_KPI[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpLocationKpi> ret = new List<OpLocationKpi>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpLocationKpi(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpLocationKpi> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationKpi> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationKpi)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationKpi).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpLocationKpi> Members
        public bool Equals(OpLocationKpi other)
        {
            if (other == null)
                return false;
            return this.IdLocationKpi.Equals(other.IdLocationKpi);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpLocationKpi)
                return this.IdLocationKpi == ((OpLocationKpi)obj).IdLocationKpi;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpLocationKpi left, OpLocationKpi right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpLocationKpi left, OpLocationKpi right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdLocationKpi.GetHashCode();
        }
    #endregion
    }
#endif
}