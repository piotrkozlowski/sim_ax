using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpReportType : DB_REPORT_TYPE, IComparable, IEquatable<OpReportType>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdReportType { get { return this.ID_REPORT_TYPE; } set { this.ID_REPORT_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpReportType()
            : base() { }

        public OpReportType(DB_REPORT_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpReportType> ConvertList(DB_REPORT_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpReportType> ConvertList(DB_REPORT_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpReportType> ret = new List<OpReportType>(list.Length);
            foreach (var loop in list)
            {
                OpReportType insert = new OpReportType(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[]{ loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpReportType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpReportType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpReportType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpReportType> Members
        public bool Equals(OpReportType other)
        {
            if (other == null)
                return false;
            return this.IdReportType.Equals(other.IdReportType);
        }
        #endregion

    #region Implementation of IReferenceType
    public object GetReferenceKey()
    {
        return IdReportType;
    }

    public object GetReferenceValue()
    {
        return this;
    }
    #endregion

        #region Enum
        public enum Enum
        {
            /// <summary>
            /// Measures report
            /// </summary>
            Measure = 1,
            /// <summary>
            /// Predefined report based on stored procedure
            /// </summary>
            Predefined = 2
        }
    #endregion
    }
#endif
}