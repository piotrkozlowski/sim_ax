using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using CORE = IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpEtlPerformance : DB_ETL_PERFORMANCE, IComparable, IEquatable<OpEtlPerformance>
    {
        #region Properties
        public long IdEtlPerformance { get { return this.ID_ETL_PERFORMANCE; } set { this.ID_ETL_PERFORMANCE = value; } }
        public int IdEtl { get { return this.ID_ETL; } set { this.ID_ETL = value; } }
        public int IdAggregationType { get { return this.ID_AGGREGATION_TYPE; } set { this.ID_AGGREGATION_TYPE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public int? IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
        public string Code { get { return this.CODE; } set { this.CODE = value; } }
        #endregion

        #region	Navigation Properties
        private OpEtl _Etl;
        public OpEtl Etl { get { return this._Etl; } set { this._Etl = value; this.ID_ETL = (value == null) ? 0 : (int)value.ID_PARAM; } }
        private CORE.OpDataType _DataType;
        public CORE.OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpDataSourceType _DataSourceType;
        public OpDataSourceType DataSourceType { get { return this._DataSourceType; } set { this._DataSourceType = value; this.ID_DATA_SOURCE_TYPE = (value == null) ? null : (int?)value.ID_DATA_SOURCE_TYPE; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpEtlPerformance()
            : base() { }

        public OpEtlPerformance(DB_ETL_PERFORMANCE clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "EtlPerformance [" + this.CODE + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpEtlPerformance> ConvertList(DB_ETL_PERFORMANCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpEtlPerformance> ConvertList(DB_ETL_PERFORMANCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpEtlPerformance> ret = new List<OpEtlPerformance>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpEtlPerformance(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpEtlPerformance> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<int, OpEtl> EtlDict = dataProvider.GetEtlDW(list.Select(l => l.ID_ETL).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PARAM);
                Dictionary<int, OpEtl> EtlDict = dataProvider.GetEtl(list.Select(l => l.ID_ETL).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PARAM);
                Dictionary<long, CORE.OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpDataSourceType> DataSourceTypeDict = dataProvider.GetDataSourceType(list.Where(l => l.ID_DATA_SOURCE_TYPE.HasValue).Select(l => l.ID_DATA_SOURCE_TYPE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_SOURCE_TYPE);

                foreach (var loop in list)
                {
                    loop.Etl = EtlDict.TryGetValue(loop.ID_ETL);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DATA_SOURCE_TYPE.HasValue)
                        loop.DataSourceType = DataSourceTypeDict.TryGetValue(loop.ID_DATA_SOURCE_TYPE.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpEtlPerformance> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpEtlPerformance)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpEtlPerformance).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpEtlPerformance> Members
        public bool Equals(OpEtlPerformance other)
        {
            if (other == null)
                return false;
            return this.IdEtlPerformance.Equals(other.IdEtlPerformance);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpEtlPerformance)
                return this.IdEtlPerformance == ((OpEtlPerformance)obj).IdEtlPerformance;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpEtlPerformance left, OpEtlPerformance right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpEtlPerformance left, OpEtlPerformance right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdEtlPerformance.GetHashCode();
        }
        #endregion
    }
#endif
}