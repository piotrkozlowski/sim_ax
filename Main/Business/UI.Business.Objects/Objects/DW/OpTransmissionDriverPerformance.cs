﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTransmissionDriverPerformance : DB_TRANSMISSION_DRIVER_PERFORMANCE, IComparable, IEquatable<OpTransmissionDriverPerformance>
    {
    #region Properties
        public long IdTransmissionDriverPerformance { get { return this.ID_TRANSMISSION_DRIVER_PERFORMANCE; } set { this.ID_TRANSMISSION_DRIVER_PERFORMANCE = value; } }
        public int IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = value; } }
        public int IdAggregationType { get { return this.ID_AGGREGATION_TYPE; } set { this.ID_AGGREGATION_TYPE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public int? IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
    #endregion

    #region	Navigation Properties
        private OpTransmissionDriver _TransmissionDriver;
        public OpTransmissionDriver TransmissionDriver { get { return this._TransmissionDriver; } set { this._TransmissionDriver = value; this.ID_TRANSMISSION_DRIVER = (value == null) ? 0 : (int)value.ID_TRANSMISSION_DRIVER; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpDataSourceType _DataSourceType;
        public OpDataSourceType DataSourceType { get { return this._DataSourceType; } set { this._DataSourceType = value; this.ID_DATA_SOURCE_TYPE = (value == null) ? null : (int?)value.ID_DATA_SOURCE_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpTransmissionDriverPerformance()
            : base() { }

        public OpTransmissionDriverPerformance(DB_TRANSMISSION_DRIVER_PERFORMANCE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "TransmissionDriverPerformance [" + IdTransmissionDriverPerformance.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpTransmissionDriverPerformance> ConvertList(DB_TRANSMISSION_DRIVER_PERFORMANCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTransmissionDriverPerformance> ConvertList(DB_TRANSMISSION_DRIVER_PERFORMANCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTransmissionDriverPerformance> ret = new List<OpTransmissionDriverPerformance>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTransmissionDriverPerformance(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTransmissionDriverPerformance> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionDriver> TransmissionDriverDict = dataProvider.GetTransmissionDriverDW(list.Select(l => l.ID_TRANSMISSION_DRIVER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_TRANSMISSION_DRIVER);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpDataSourceType> DataSourceTypeDict = dataProvider.GetDataSourceType(list.Where(l => l.ID_DATA_SOURCE_TYPE.HasValue).Select(l => l.ID_DATA_SOURCE_TYPE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_SOURCE_TYPE);
                foreach (var loop in list)
                {
                    loop.TransmissionDriver = TransmissionDriverDict.TryGetValue(loop.ID_TRANSMISSION_DRIVER);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DATA_SOURCE_TYPE.HasValue)
                        loop.DataSourceType = DataSourceTypeDict.TryGetValue(loop.ID_DATA_SOURCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTransmissionDriverPerformance> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTransmissionDriverPerformance)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTransmissionDriverPerformance).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpTransmissionDriverPerformance> Members
        public bool Equals(OpTransmissionDriverPerformance other)
        {
            if (other == null)
                return false;
            return this.IdTransmissionDriverPerformance.Equals(other.IdTransmissionDriverPerformance);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTransmissionDriverPerformance)
                return this.IdTransmissionDriverPerformance == ((OpTransmissionDriverPerformance)obj).IdTransmissionDriverPerformance;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTransmissionDriverPerformance left, OpTransmissionDriverPerformance right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTransmissionDriverPerformance left, OpTransmissionDriverPerformance right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTransmissionDriverPerformance.GetHashCode();
        }
    #endregion
    }
#endif
}