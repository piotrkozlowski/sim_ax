using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataSourceType : DB_DATA_SOURCE_TYPE, IComparable, IEquatable<OpDataSourceType>, IReferenceType
    {
    #region Properties
        public int IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDataSourceType()
            : base() { }

        public OpDataSourceType(DB_DATA_SOURCE_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.ToString();
            return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpDataSourceType> ConvertList(DB_DATA_SOURCE_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDataSourceType> ConvertList(DB_DATA_SOURCE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDataSourceType> ret = new List<OpDataSourceType>(list.Length);
            foreach (var loop in list)
            {
                OpDataSourceType insert = new OpDataSourceType(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataSourceType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataSourceType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataSourceType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataSourceType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataSourceType> Members
        public bool Equals(OpDataSourceType other)
        {
            if (other == null)
                return false;
            return this.IdDataSourceType.Equals(other.IdDataSourceType);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataSourceType)
                return this.IdDataSourceType == ((OpDataSourceType)obj).IdDataSourceType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataSourceType left, OpDataSourceType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataSourceType left, OpDataSourceType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataSourceType.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdDataSourceType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}