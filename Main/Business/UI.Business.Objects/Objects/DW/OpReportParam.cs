using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpReportParam : DB_REPORT_PARAM, IComparable, IEquatable<OpReportParam>
    {
    #region Properties
        public int IdReportParam { get { return this.ID_REPORT_PARAM; } set { this.ID_REPORT_PARAM = value; } }
        public int IdReport { get { return this.ID_REPORT; } set { this.ID_REPORT = value; } }
        public string FieldName { get { return this.FIELD_NAME; } set { this.FIELD_NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public int IdUnit { get { return this.ID_UNIT; } set { this.ID_UNIT = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int? IdReference { get { return this.ID_REFERENCE; } set { this.ID_REFERENCE = value; } }
        public int IdDataTypeClass { get { return this.ID_DATA_TYPE_CLASS; } set { this.ID_DATA_TYPE_CLASS = value; } }
        public bool IsInputParam { get { return this.IS_INPUT_PARAM; } set { this.IS_INPUT_PARAM = value; } }
        public bool IsVisible { get { return this.IS_VISIBLE; } set { this.IS_VISIBLE = value; } }
        public object DefaultValue { get { return this.DEFAULT_VALUE; } set { this.DEFAULT_VALUE = value; } }
        public string ColorBg { get { return this.COLOR_BG; } set { this.COLOR_BG = value; } }
        public string Format { get { return this.FORMAT; } set { this.FORMAT = value; } }
    #endregion

    #region	Navigation Properties
        [DataMember]
        private OpReport _Report;
        public OpReport Report { get { return this._Report; } set { this._Report = value; this.ID_REPORT = (value == null) ? 0 : (int)value.ID_REPORT; } }
        private OpDescr _Descr;
        [DataMember]
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        [DataMember]
        private OpUnit _Unit;
        public OpUnit Unit { get { return this._Unit; } set { this._Unit = value; this.ID_UNIT = (value == null) ? 0 : (int)value.ID_UNIT; } }
        [DataMember]
        private OpReferenceType _Reference;
        public OpReferenceType Reference { get { return this._Reference; } set { this._Reference = value; this.ID_REFERENCE = (value == null) ? null : (int?)value.ID_REFERENCE_TYPE; } }
        [DataMember]
        private OpDataTypeClass _DataTypeClass;
        public OpDataTypeClass DataTypeClass { get { return this._DataTypeClass; } set { this._DataTypeClass = value; this.ID_DATA_TYPE_CLASS = (value == null) ? 0 : (int)value.ID_DATA_TYPE_CLASS; } }
    #endregion

    #region	Custom Properties
        public object Value { get; set; }
        [DataMember]
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpReportParam()
            : base() 
        {
            this.OpState = OpChangeState.New;
        }

        public OpReportParam(DB_REPORT_PARAM clone)
            : base(clone) 
        {
            this.OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpReportParam> ConvertList(DB_REPORT_PARAM[] list, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpReportParam> ret = new List<OpReportParam>(list.Length);
            foreach (var loop in list)
            {
                OpReportParam insert = new OpReportParam(loop);

                if (loadNavigationProperties)
                {
                    insert.Report = dataProvider.GetReport(loop.ID_REPORT);

                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);

                    insert.Unit = dataProvider.GetUnit(loop.ID_UNIT);
                    insert.DataTypeClass = dataProvider.GetDataTypeClass(loop.ID_DATA_TYPE_CLASS);
                    if (loop.ID_REFERENCE.HasValue)
                        insert.Reference = dataProvider.GetReferenceType(loop.ID_REFERENCE.Value);
                }

                ret.Add(insert);
            }

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data

            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpReportParam> list, IDataProvider dataProvider)
        {
            foreach (var loop in list)
            {
    #region Value
                loop.Value = loop.DEFAULT_VALUE; //przypisujemy na wst�pie domysln� warto��, jesli jest podany IdReference to pr�bujemy wyciagnac wlasciwy obiekt poni�ej

                //je�li parametrem raportu jest IdLanguage a nie podano warto�ci domy�lnej wstawiamy domy�lna z kodu
                if (loop.ID_REFERENCE.HasValue && loop.ID_REFERENCE == (int)Enums.ReferenceType.IdLanguage && loop.DEFAULT_VALUE == null)
                    loop.Value = dataProvider.GetLanguage((int)Enums.Language.Default);
                else if (loop.ID_REFERENCE.HasValue && loop.DEFAULT_VALUE != null)
                {
                    try
                    {
                        loop.Value = dataProvider.GetReferenceObject((Enums.ReferenceType)loop.ID_REFERENCE.Value, loop.DEFAULT_VALUE);
                    }
                    catch
                    {
                    }
                }
                else
                {
                    if (loop.DEFAULT_VALUE != null)
                    {
                        switch (loop.DEFAULT_VALUE.ToString())
                        {
                            case "{NOW}":
                                loop.Value = dataProvider.DateTimeNow;
                                break;
                            case "{YESTERDAY}":
                                loop.Value = dataProvider.DateTimeNow - new TimeSpan(1, 0, 0, 0);
                                break;
                            case "{LAST_WEEK}":
                                loop.Value = dataProvider.DateTimeNow - new TimeSpan(7, 0, 0, 0); ;
                                break;
                            case "{LAST_MONTH}":
                                loop.Value = dataProvider.DateTimeNow - new TimeSpan(30, 0, 0, 0); ;
                                break;
                            default:
                                break;
                        }
                    }
                }
    #endregion
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpReportParam)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpReportParam).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpReportParam> Members
        public bool Equals(OpReportParam other)
        {
            if (other == null)
                return false;
            return this.IdReportParam.Equals(other.IdReportParam);
        }
    #endregion
    
    }
#endif
}