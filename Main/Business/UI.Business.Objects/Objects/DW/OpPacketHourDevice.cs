﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.Objects.DW;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPacketHourDevice : DB_PACKET_HOUR_DEVICE, IComparable, IEquatable<OpPacketHourDevice>
    {
    #region Properties
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public int? IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = value; } }
        public DateTime Date { get { return this.DATE; } set { this.DATE = value; }}
        public bool IsIncoming { get { return this.IS_INCOMING; } set { this.IS_INCOMING = value; }}
        public int Packets { get { return this.PACKETS; } set { this.PACKETS = value; }}
    #endregion

    #region	Navigation Properties
        private OpTransmissionDriver _TransmissionDriver;
        public OpTransmissionDriver TransmissionDriver { get { return this._TransmissionDriver; } set { this._TransmissionDriver = value; this.ID_TRANSMISSION_DRIVER = (value == null) ? null : (int?)value.ID_TRANSMISSION_DRIVER; } }
    #endregion

    #region	Custom Properties
        public string DispTransmissionDriver
        {
            get
            {
                if (TransmissionDriver != null)
                    return TransmissionDriver.ToString();
                else
                    return "";
            }
        }
    #endregion

    #region	Ctor
        public OpPacketHourDevice()
            : base() { }

        public OpPacketHourDevice(DB_PACKET_HOUR_DEVICE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return Convert.ToString(this.SERIAL_NBR);
        }
    #endregion

    #region	ConvertList
        public static List<OpPacketHourDevice> ConvertList(DB_PACKET_HOUR_DEVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPacketHourDevice> ConvertList(DB_PACKET_HOUR_DEVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpPacketHourDevice> ret = new List<OpPacketHourDevice>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPacketHourDevice(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPacketHourDevice> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionDriver> TransmissionDriverDict = dataProvider.GetTransmissionDriverDW(list.Where(l => l.ID_TRANSMISSION_DRIVER.HasValue).Select(l => l.ID_TRANSMISSION_DRIVER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER);
                foreach (var loop in list)
                {
                    if (loop.ID_TRANSMISSION_DRIVER.HasValue)
                        loop.TransmissionDriver = TransmissionDriverDict.TryGetValue(loop.ID_TRANSMISSION_DRIVER.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPacketHourDevice> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPacketHourDevice)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPacketHourDevice).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPacketHourDevice> Members
        public bool Equals(OpPacketHourDevice other)
        {
            if (other == null)
                return false;
            return this.SerialNbr.Equals(other.SerialNbr);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPacketHourDevice)
                return this.SerialNbr == ((OpPacketHourDevice)obj).SerialNbr;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPacketHourDevice left, OpPacketHourDevice right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPacketHourDevice left, OpPacketHourDevice right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return SerialNbr.GetHashCode();
        }
    #endregion
    }
#endif
}
