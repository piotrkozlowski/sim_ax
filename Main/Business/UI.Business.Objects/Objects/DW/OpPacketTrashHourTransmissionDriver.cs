﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.DAQ;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPacketTrashHourTransmissionDriver : DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER, IComparable, IEquatable<OpPacketTrashHourTransmissionDriver>
    {
    #region Properties
        public int? IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = value; } }
        public DateTime Date { get { return this.DATE; } set { this.DATE = value; } }
        public bool IsIncoming { get { return this.IS_INCOMING; } set { this.IS_INCOMING = value; } }
        public int Packets { get { return this.PACKETS; } set { this.PACKETS = value; } }
        public int? IdTransmissionType { get { return this.ID_TRANSMISSION_TYPE; } set { this.ID_TRANSMISSION_TYPE = value; } }
    #endregion

    #region	Navigation Properties
        private OpTransmissionDriver _TransmissionDriver;
        public OpTransmissionDriver TransmissionDriver { get { return this._TransmissionDriver; } set { this._TransmissionDriver = value; this.ID_TRANSMISSION_DRIVER = (value == null) ? 0 : (int)value.ID_TRANSMISSION_DRIVER; } }
        private OpTransmissionDriverType _TransmissionDriverType;
        public OpTransmissionDriverType TransmissionDriverType { get { return this._TransmissionDriverType; } set { this._TransmissionDriverType = value; this.ID_TRANSMISSION_TYPE = (value == null) ? 0 : (int)value.ID_TRANSMISSION_DRIVER_TYPE; } }
    #endregion

    #region	Custom Properties
        public string DispTransmissionDriver
        {
            get
            {
                if (TransmissionDriver != null)
                    return TransmissionDriver.ToString();
                else
                    return "";
            }
        }

        public string DispTransmissionType
        {
            get
            {
                if (TransmissionDriverType != null)
                    return TransmissionDriverType.ToString();
                else
                    return "";
            }
        }
    #endregion

    #region	Ctor
        public OpPacketTrashHourTransmissionDriver()
            : base() { }

        public OpPacketTrashHourTransmissionDriver(DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "PacketTrashHourTransmissionDriver [" + IdTransmissionDriver.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpPacketTrashHourTransmissionDriver> ConvertList(DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPacketTrashHourTransmissionDriver> ConvertList(DB_PACKET_TRASH_HOUR_TRANSMISSION_DRIVER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpPacketTrashHourTransmissionDriver> ret = new List<OpPacketTrashHourTransmissionDriver>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPacketTrashHourTransmissionDriver(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPacketTrashHourTransmissionDriver> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionDriver> TransmissionDriverDict = dataProvider.GetTransmissionDriverDW(list.Where(l => l.ID_TRANSMISSION_DRIVER.HasValue).Select(l => l.ID_TRANSMISSION_DRIVER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER);
                Dictionary<int, OpTransmissionDriverType> TransmissionTypeDict = dataProvider.GetTransmissionDriverType(list.Where(l => l.ID_TRANSMISSION_TYPE.HasValue).Select(l => l.ID_TRANSMISSION_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_DRIVER_TYPE);
                foreach (var loop in list)
                {
                    if (loop.ID_TRANSMISSION_DRIVER.HasValue)
                        loop.TransmissionDriver = TransmissionDriverDict.TryGetValue(loop.ID_TRANSMISSION_DRIVER.Value);
                    if (loop.ID_TRANSMISSION_TYPE.HasValue)
                        loop.TransmissionDriverType = TransmissionTypeDict.TryGetValue(loop.ID_TRANSMISSION_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPacketTrashHourTransmissionDriver> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPacketTrashHourTransmissionDriver)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPacketTrashHourTransmissionDriver).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPacketTrashHourTransmissionDriver> Members
        public bool Equals(OpPacketTrashHourTransmissionDriver other)
        {
            if (other == null)
                return false;
            return this.IdTransmissionDriver.Equals(other.IdTransmissionDriver);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPacketTrashHourTransmissionDriver)
                return this.IdTransmissionDriver == ((OpPacketTrashHourTransmissionDriver)obj).IdTransmissionDriver;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPacketTrashHourTransmissionDriver left, OpPacketTrashHourTransmissionDriver right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPacketTrashHourTransmissionDriver left, OpPacketTrashHourTransmissionDriver right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTransmissionDriver.GetHashCode();
        }
    #endregion
    }
#endif
}