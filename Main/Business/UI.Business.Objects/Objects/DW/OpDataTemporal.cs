using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataTemporal : DB_DATA_TEMPORAL, IComparable, IEquatable<OpDataTemporal>
    {
    #region Properties
        public long IdDataTemporal { get { return this.ID_DATA_TEMPORAL; } set { this.ID_DATA_TEMPORAL = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public DateTime? StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public int? IdAggregationType { get { return this.ID_AGGREGATION_TYPE; } set { this.ID_AGGREGATION_TYPE = value; } }
        public long? IdDataSource { get { return this.ID_DATA_SOURCE; } set { this.ID_DATA_SOURCE = value; } }
        public int? IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
        public long? IdAlarmEvent { get { return this.ID_ALARM_EVENT; } set { this.ID_ALARM_EVENT = value; } }
        public Enums.DataStatus DataStatus { get { return (Enums.DataStatus)this.STATUS; } set { this.STATUS = (int)value; } }
    #endregion

    #region	Navigation Properties
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpDataSource _DataSource;
        public OpDataSource DataSource { get { return this._DataSource; } set { this._DataSource = value; this.ID_DATA_SOURCE = (value == null) ? null : (long?)value.ID_DATA_SOURCE; } }
        private OpDataSourceType _DataSourceType;
        public OpDataSourceType DataSourceType { get { return this._DataSourceType; } set { this._DataSourceType = value; this.ID_DATA_SOURCE_TYPE = (value == null) ? null : (int?)value.ID_DATA_SOURCE_TYPE; } }
    #endregion

    #region	Custom Properties
        
        public OpChangeState OpState { get; set; }
        //wykorzystywany do zmiany jednostki bazowej dla wybranego pomiaru
        public OpUnit Unit { get; set; }

        public string ValueUnit
        {
            get
            {
                if (DataType != null && Unit != null && !string.IsNullOrEmpty(Unit.Name))
                    return String.Format("{0} [{1}]", Value, Unit.Name);
                return (Value == null) ? null : Value.ToString();
            }
        }
        public decimal? ValueDecimal
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDecimal(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public double? ValueDouble
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDouble(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public long? ValueLong
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToInt64(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public DateTime? ValueDateTime
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is DateTime)
                        return Convert.ToDateTime(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public bool? ValueBool
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is bool)
                        return Convert.ToBoolean(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public string ValueString
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Value.ToString();
                }
                catch
                {
                }
                return null;
            }
        }

        public int? ValueInt
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToInt32(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
    #endregion

    #region	Ctor
        public OpDataTemporal()
            : base() { this.DataStatus = Enums.DataStatus.Normal; }

        public OpDataTemporal(DB_DATA_TEMPORAL clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DataTemporal [" + IdDataTemporal.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDataTemporal> ConvertList(DB_DATA_TEMPORAL[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true, true);
        }

        public static List<OpDataTemporal> ConvertList(DB_DATA_TEMPORAL[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true)
        {
            List<OpDataTemporal> ret = new List<OpDataTemporal>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataTemporal(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
            if(loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataTemporal> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<long, OpDataSource> DataSourceDict = dataProvider.GetDataSource(list.Where(l => l.ID_DATA_SOURCE.HasValue).Select(l => l.ID_DATA_SOURCE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE);
                Dictionary<int, OpDataSourceType> DataSourceTypeDict = dataProvider.GetDataSourceType(list.Where(l => l.ID_DATA_SOURCE_TYPE.HasValue).Select(l => l.ID_DATA_SOURCE_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE_TYPE);

                foreach (var loop in list)
                {
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DATA_SOURCE.HasValue)
                        loop.DataSource = DataSourceDict.TryGetValue(loop.ID_DATA_SOURCE.Value);
                    if (loop.ID_DATA_SOURCE_TYPE.HasValue)
                        loop.DataSourceType = DataSourceTypeDict.TryGetValue(loop.ID_DATA_SOURCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataTemporal> list, IDataProvider dataProvider)
        {
            foreach (OpDataTemporal loop in list)
            {
                if (loop.DataType != null && loop.DataType.Unit != null)
                    loop.Unit = (OpUnit)loop.DataType.Unit.Clone(dataProvider);
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataTemporal)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataTemporal).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataTemporal> Members
        public bool Equals(OpDataTemporal other)
        {
            if (other == null)
                return false;
            return this.IdDataTemporal.Equals(other.IdDataTemporal);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataTemporal)
                return this.IdDataTemporal == ((OpDataTemporal)obj).IdDataTemporal;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataTemporal left, OpDataTemporal right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataTemporal left, OpDataTemporal right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataTemporal.GetHashCode();
        }
        #endregion

        #region ChangeUnit - List of DW.OpDataTemporal
        public static List<Objects.DW.OpDataTemporal> ChangeUnit(List<Objects.DW.OpDataTemporal> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit newUnit = dataTypeUnitDict[dataType.IdDataType];
                            if (newUnit != null && dataType.Unit != null && dataType.Unit.IdUnitBase == newUnit.IdUnitBase && newUnit.IdUnit != dataType.Unit.IdUnit && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType).ToList().ForEach(d => d.Unit = newUnit);
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (dataType.Unit.Scale * Convert.ToDouble(d.Value) + dataType.Unit.Bias - newUnit.Bias) / newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (dataType.Unit.Scale * Convert.ToDouble(d.Value) + dataType.Unit.Bias - newUnit.Bias) / newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = ((decimal)dataType.Unit.Scale * Convert.ToDecimal(d.Value) + (decimal)dataType.Unit.Bias - (decimal)newUnit.Bias) / (decimal)newUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDecimal(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                }

                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        public static List<Objects.DW.OpDataTemporal> ChangeUnit(List<Objects.DW.OpDataTemporal> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (fromDataTypeUnitDict.Keys.Count > 0 && toDataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && fromDataTypeUnitDict.ContainsKey(dataType.IdDataType) && toDataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit fromUnit = fromDataTypeUnitDict[dataType.IdDataType];
                            OpUnit toUnit = toDataTypeUnitDict[dataType.IdDataType];
                            if (fromUnit != null && toUnit != null && fromUnit.IdUnitBase == toUnit.IdUnitBase && fromUnit.IdUnit != toUnit.IdUnit && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType).ToList().ForEach(d => d.Unit = toUnit);
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = ((decimal)fromUnit.Scale * Convert.ToDecimal(d.Value) + (decimal)fromUnit.Bias - (decimal)toUnit.Bias) / (decimal)toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDecimal(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                }

                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
    }
#endif
}