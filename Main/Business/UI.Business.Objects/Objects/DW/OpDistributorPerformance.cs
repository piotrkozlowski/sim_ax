﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDistributorPerformance : DB_DISTRIBUTOR_PERFORMANCE, IComparable, IEquatable<OpDistributorPerformance>
    {
    #region Properties
        public long IdDistributorPerformance { get { return this.ID_DISTRIBUTOR_PERFORMANCE; } set { this.ID_DISTRIBUTOR_PERFORMANCE = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public int IdAggregationType { get { return this.ID_AGGREGATION_TYPE; } set { this.ID_AGGREGATION_TYPE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDistributorPerformance()
            : base() { }

        public OpDistributorPerformance(DB_DISTRIBUTOR_PERFORMANCE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DistributorPerformance [" + IdDistributorPerformance.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDistributorPerformance> ConvertList(DB_DISTRIBUTOR_PERFORMANCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDistributorPerformance> ConvertList(DB_DISTRIBUTOR_PERFORMANCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDistributorPerformance> ret = new List<OpDistributorPerformance>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDistributorPerformance(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDistributorPerformance> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDistributorPerformance> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDistributorPerformance)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDistributorPerformance).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDistributorPerformance> Members
        public bool Equals(OpDistributorPerformance other)
        {
            if (other == null)
                return false;
            return this.IdDistributorPerformance.Equals(other.IdDistributorPerformance);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDistributorPerformance)
                return this.IdDistributorPerformance == ((OpDistributorPerformance)obj).IdDistributorPerformance;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDistributorPerformance left, OpDistributorPerformance right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDistributorPerformance left, OpDistributorPerformance right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDistributorPerformance.GetHashCode();
        }
    #endregion
    }
#endif
}