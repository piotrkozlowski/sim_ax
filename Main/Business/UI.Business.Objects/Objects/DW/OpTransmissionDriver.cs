﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.Objects.DW;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTransmissionDriver : DB_TRANSMISSION_DRIVER, IComparable, IEquatable<OpTransmissionDriver>, IReferenceType
    {
    #region Properties
        public int IdTransmissionDriver { get { return this.ID_TRANSMISSION_DRIVER; } set { this.ID_TRANSMISSION_DRIVER = value; } }
        public int IdTransmissionDriverType { get { return this.ID_TRANSMISSION_DRIVER_TYPE; } set { this.ID_TRANSMISSION_DRIVER_TYPE = value; } }
        public string TransmissionDriverTypeName { get { return this.TRANSMISSION_DRIVER_TYPE_NAME; } set { this.TRANSMISSION_DRIVER_TYPE_NAME = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string ResponseAddress { get { return this.RESPONSE_ADDRESS; } set { this.RESPONSE_ADDRESS = value; }}
        public string Plugin { get { return this.PLUGIN; } set { this.PLUGIN = value; }}
        public bool? IsActive { get { return this.IS_ACTIVE; } set { this.IS_ACTIVE = value; } }
        public int? IdTransmissionDriverDAQ { get { return this.ID_TRANSMISSION_DRIVER_DAQ; } set { this.ID_TRANSMISSION_DRIVER_DAQ = value; } }
        #endregion

        #region	Navigation Properties
        #endregion

        #region	Custom Properties
        public OpDataList<OpTransmissionDriverData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpTransmissionDriver()
            : base()
        {
            this.DataList = new OpDataList<OpTransmissionDriverData>(); 
        }

        public OpTransmissionDriver(DB_TRANSMISSION_DRIVER clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpTransmissionDriverData>(); 
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpTransmissionDriver> ConvertList(DB_TRANSMISSION_DRIVER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTransmissionDriver> ConvertList(DB_TRANSMISSION_DRIVER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTransmissionDriver> ret = new List<OpTransmissionDriver>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTransmissionDriver(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTransmissionDriver> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTransmissionDriver> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTransmissionDriver)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTransmissionDriver).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpTransmissionDriver> Members
        public bool Equals(OpTransmissionDriver other)
        {
            if (other == null)
                return false;
            return this.IdTransmissionDriver.Equals(other.IdTransmissionDriver);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTransmissionDriver)
                return this.IdTransmissionDriver == ((OpTransmissionDriver)obj).IdTransmissionDriver;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTransmissionDriver left, OpTransmissionDriver right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTransmissionDriver left, OpTransmissionDriver right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTransmissionDriver.GetHashCode();
        }
    #endregion

    #region IReferenceType

        public object GetReferenceKey()
        {
            return this.IdTransmissionDriver;
        }

        public object GetReferenceValue()
        {
            return this;
        }

    #endregion
    }
#endif
}
