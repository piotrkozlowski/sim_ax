using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpDataArch : DB_DATA_ARCH, IComparable, IEquatable<OpDataArch>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdDataArch { get { return this.ID_DATA_ARCH; } set { this.ID_DATA_ARCH = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public long? IdDataSource { get { return this.ID_DATA_SOURCE; } set { this.ID_DATA_SOURCE = value; } }
        public int? IdDataSourceType { get { return this.ID_DATA_SOURCE_TYPE; } set { this.ID_DATA_SOURCE_TYPE = value; } }
        public long? IdAlarmEvent { get { return this.ID_ALARM_EVENT; } set { this.ID_ALARM_EVENT = value; } }
        //public int Status { get { return this.STATUS; } set { this.STATUS = value; } }
        public Enums.DataStatus DataStatus { get { return (Enums.DataStatus)this.STATUS; } set { this.STATUS = (long)value; } }
    #endregion

    #region	Navigation Properties
        [DataMember]
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        [DataMember]
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        [DataMember]
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        [DataMember]
        private OpDataSource _DataSource;
        public OpDataSource DataSource { get { return this._DataSource; } set { this._DataSource = value; this.ID_DATA_SOURCE = (value == null) ? null : (long?)value.ID_DATA_SOURCE; } }
        [DataMember]
        private OpDataSourceType _DataSourceType;
        public OpDataSourceType DataSourceType { get { return this._DataSourceType; } set { this._DataSourceType = value; this.ID_DATA_SOURCE_TYPE = (value == null) ? null : (int?)value.ID_DATA_SOURCE_TYPE; } }
    #endregion

    #region	Custom Properties
        public long IdData { get { return IdDataArch; } set { IdDataArch = value; } }
        public int Index { get { return IndexNbr; } set { IndexNbr = value; } }
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }

        //wykorzystywany do zmiany jednostki bazowej dla wybranego pomiaru
        public OpUnit Unit { get; set; }
        
        public string ValueUnit
        {
            get
            {
                if (DataType != null && Unit != null && !string.IsNullOrEmpty(Unit.Name) && Value != null)
                    return String.Format("{0} [{1}]", Value, Unit);
                return (Value == null) ? null : Value.ToString();
            }
        }
        public decimal? ValueDecimal
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDecimal(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public double? ValueDouble
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToDouble(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public long? ValueLong
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Convert.ToInt64(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public DateTime? ValueDateTime
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is DateTime)
                        return Convert.ToDateTime(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public bool? ValueBool
        {
            get
            {
                try
                {
                    if (this.Value != null && this.Value is bool)
                        return Convert.ToBoolean(this.Value);
                }
                catch
                {
                }
                return null;
            }
        }
        public string ValueString
        {
            get
            {
                try
                {
                    if (this.Value != null)
                        return Value.ToString();
                }
                catch
                {
                }
                return null;
            }
        }
    #endregion

    #region	Ctor
        public OpDataArch()
            : base() { }

        public OpDataArch(DB_DATA_ARCH clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DataArch [" + IdDataArch.ToString() + "]";
        }
    #endregion

    #region ParseValue
        public void ParseValue(object value, OpUnit U)
        {
            OpDataType DT = this.DataType;
            string SValue = value.ToString();
            System.Globalization.NumberStyles NS = System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.AllowLeadingSign;

            if (DT.IdDataTypeClass == (int)Enums.DataTypeClass.Real)
            {
                value = (U == null) ? Double.Parse(SValue, NS) : Double.Parse(SValue, NS) * U.Scale + U.Bias;
            }

            if (DT.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal)
            {
                value = (U == null) ? Decimal.Parse(SValue, NS) : Decimal.Parse(SValue, NS) * (decimal)U.Scale + (decimal)U.Bias;
            }

            if (DT.IdDataTypeClass == (int)Enums.DataTypeClass.Integer)
            {
                value = (U == null) ? int.Parse(SValue, NS) : int.Parse(SValue, NS) * U.Scale + U.Bias;
            }

            if (DT.IdDataTypeClass == (int)Enums.DataTypeClass.Text)
            {
                value = SValue;
            }

            if (DT.IdDataTypeClass == (int)Enums.DataTypeClass.Boolean)
            {
                value = bool.Parse(SValue);
            }

            if (DT.IdDataTypeClass == (int)Enums.DataTypeClass.Time
                || DT.IdDataTypeClass == (int)Enums.DataTypeClass.Datetime
                || DT.IdDataTypeClass == (int)Enums.DataTypeClass.Date)
            {
                value = DateTime.Parse(SValue);
            }

            this.Value = value;
        }
    #endregion

    #region	ConvertList
        public static List<OpDataArch> ConvertList(DB_DATA_ARCH[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true, loadCustomData: true);
        }

        public static List<OpDataArch> ConvertList(DB_DATA_ARCH[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true)
        {
            List<OpDataArch> ret = new List<OpDataArch>(list.Length);
            foreach (var loop in list)
            {
                OpDataArch insert = new OpDataArch(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
            if(loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataArch> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<long, OpDataSource> DataSourceDict = dataProvider.GetDataSource(list.Where(l => l.ID_DATA_SOURCE.HasValue).Select(l => l.ID_DATA_SOURCE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE);
                Dictionary<int, OpDataSourceType> DataSourceTypeDict = dataProvider.GetDataSourceType(list.Where(l => l.ID_DATA_SOURCE_TYPE.HasValue).Select(l => l.ID_DATA_SOURCE_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE_TYPE);

                foreach (var loop in list)
                {
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DATA_SOURCE.HasValue)
                        loop.DataSource = DataSourceDict.TryGetValue(loop.ID_DATA_SOURCE.Value);
                    if (loop.ID_DATA_SOURCE_TYPE.HasValue)
                        loop.DataSourceType = DataSourceTypeDict.TryGetValue(loop.ID_DATA_SOURCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpDataArch> list, IDataProvider dataProvider)
        {
            foreach (OpDataArch loop in list)
            {
                if (loop.DataType != null && loop.DataType.Unit != null)
                    loop.Unit = (OpUnit)loop.DataType.Unit.Clone(dataProvider);
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataArch)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataArch).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataArch> Members
        public bool Equals(OpDataArch other)
        {
            if (other == null)
                return false;
            return this.IdDataArch.Equals(other.IdDataArch);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataArch)
                return this.IdDataArch == ((OpDataArch)obj).IdDataArch;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataArch left, OpDataArch right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataArch left, OpDataArch right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataArch.GetHashCode();
        }
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.Distributor = dataProvider.GetDistributor(this.ID_DISTRIBUTOR);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDataArch clone = new OpDataArch();
            clone.IdData = this.IdData;
            clone.SerialNbr = this.SerialNbr;
            clone.IdMeter = this.IdMeter;
            clone.IdLocation = this.IdLocation;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.Time = this.Time;
            clone.IdDataSource = this.IdDataSource;
            clone.IdDataSourceType = this.IdDataSourceType;
            clone.IdAlarmEvent = this.IdAlarmEvent;
            clone.DataStatus = this.DataStatus;

            if (dataProvider != null)
                clone = ConvertList(new DB_DATA_ARCH[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
        #endregion

        #endregion

        #region ChangeUnit
        public static List<Objects.DW.OpDataArch> ChangeUnit(List<Objects.DW.OpDataArch> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Where(d => d.DataType != null).Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit newUnit = dataTypeUnitDict[dataType.IdDataType];
                            if (newUnit != null && dataType.Unit != null && dataType.Unit.IdUnitBase == newUnit.IdUnitBase && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                {
                                    d.Unit = newUnit;

                                    if (d.Value != null)
                                        d.Value = OpUnit.ChangeUnit(d.Value, d.DataType, dataType.Unit, newUnit,
                                            dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType) ? dataTypeFractionalDigitsDict[dataType.IdDataType] : null);
                                });
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        public static List<Objects.DW.OpDataArch> ChangeUnit(List<Objects.DW.OpDataArch> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (fromDataTypeUnitDict.Keys.Count > 0 && toDataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Where(d => d.DataType != null).Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && fromDataTypeUnitDict.ContainsKey(dataType.IdDataType) && toDataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit fromUnit = fromDataTypeUnitDict[dataType.IdDataType];
                            OpUnit toUnit = toDataTypeUnitDict[dataType.IdDataType];
                            if (fromUnit != null && toUnit != null && fromUnit.IdUnitBase == toUnit.IdUnitBase && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                {
                                    d.Unit = toUnit;

                                    if (d.Value != null)
                                        d.Value = OpUnit.ChangeUnit(d.Value, d.DataType, fromUnit, toUnit,
                                            dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType) ? dataTypeFractionalDigitsDict[dataType.IdDataType] : null);
                                });
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
    }
#endif
}