using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceSchedule : DB_DEVICE_SCHEDULE, IComparable, IEquatable<OpDeviceSchedule>
    {
    #region Properties
        public int IdDeviceSchedule { get { return this.ID_DEVICE_SCHEDULE; } set { this.ID_DEVICE_SCHEDULE = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public int DayOfMonth { get { return this.DAY_OF_MONTH; } set { this.DAY_OF_MONTH = value; } }
        public int DayOfWeek { get { return this.DAY_OF_WEEK; } set { this.DAY_OF_WEEK = value; } }
        public int HourOfDay { get { return this.HOUR_OF_DAY; } set { this.HOUR_OF_DAY = value; } }
        public int MinuteOfHour { get { return this.MINUTE_OF_HOUR; } set { this.MINUTE_OF_HOUR = value; } }
        public int CommandCode { get { return this.COMMAND_CODE; } set { this.COMMAND_CODE = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceSchedule()
            : base() { }

        public OpDeviceSchedule(DB_DEVICE_SCHEDULE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeviceSchedule [" + IdDeviceSchedule.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceSchedule> ConvertList(DB_DEVICE_SCHEDULE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceSchedule> ConvertList(DB_DEVICE_SCHEDULE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceSchedule> ret = new List<OpDeviceSchedule>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceSchedule(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceSchedule> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceSchedule> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceSchedule)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceSchedule).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceSchedule> Members
        public bool Equals(OpDeviceSchedule other)
        {
            if (other == null)
                return false;
            return this.IdDeviceSchedule.Equals(other.IdDeviceSchedule);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceSchedule)
                return this.IdDeviceSchedule == ((OpDeviceSchedule)obj).IdDeviceSchedule;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceSchedule left, OpDeviceSchedule right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceSchedule left, OpDeviceSchedule right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceSchedule.GetHashCode();
        }
    #endregion
    }
#endif
}