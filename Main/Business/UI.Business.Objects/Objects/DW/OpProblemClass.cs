using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpProblemClass : DB_PROBLEM_CLASS, IComparable, IEquatable<OpProblemClass>
    {
    #region Properties
            public int IdProblemClass { get { return this.ID_PROBLEM_CLASS;} set {this.ID_PROBLEM_CLASS = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpProblemClass()
			:base() {}
		
		public OpProblemClass(DB_PROBLEM_CLASS clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            if (Descr != null)
                return Descr.Description;
		    return Name;
		}
    #endregion

    #region	ConvertList
		public static List<OpProblemClass> ConvertList(DB_PROBLEM_CLASS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpProblemClass> ConvertList(DB_PROBLEM_CLASS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpProblemClass> ret = new List<OpProblemClass>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpProblemClass(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpProblemClass> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpProblemClass> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpProblemClass)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpProblemClass).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpProblemClass> Members
        public bool Equals(OpProblemClass other)
        {
            if (other == null)
				return false;
			return this.IdProblemClass.Equals(other.IdProblemClass);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpProblemClass)
				return this.IdProblemClass == ((OpProblemClass)obj).IdProblemClass;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpProblemClass left, OpProblemClass right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpProblemClass left, OpProblemClass right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdProblemClass.GetHashCode();
		}
    #endregion
    }
#endif
}