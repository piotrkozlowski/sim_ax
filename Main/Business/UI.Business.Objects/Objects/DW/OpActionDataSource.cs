using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionDataSource : DB_ACTION_DATA_SOURCE, IComparable, IEquatable<OpActionDataSource>
    {
    #region Properties
        public long IdAction { get { return this.ID_ACTION; } set { this.ID_ACTION = value; } }
        public long IdDataSource { get { return this.ID_DATA_SOURCE; } set { this.ID_DATA_SOURCE = value; } }
    #endregion

    #region	Navigation Properties
        private OpAction _Action;
        public OpAction Action { get { return this._Action; } set { this._Action = value; this.ID_ACTION = (value == null) ? 0 : (long)value.ID_ACTION; } }
        private OpDataSource _DataSource;
        public OpDataSource DataSource { get { return this._DataSource; } set { this._DataSource = value; this.ID_DATA_SOURCE = (value == null) ? 0 : (long)value.ID_DATA_SOURCE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpActionDataSource()
            : base() { }

        public OpActionDataSource(DB_ACTION_DATA_SOURCE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ActionDataSource [" + IdAction.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpActionDataSource> ConvertList(DB_ACTION_DATA_SOURCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActionDataSource> ConvertList(DB_ACTION_DATA_SOURCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActionDataSource> ret = new List<OpActionDataSource>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActionDataSource(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionDataSource> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataSource> DataSourceDict = dataProvider.GetDataSource(list.Select(l => l.ID_DATA_SOURCE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE);
                foreach (var loop in list)
                {
                    loop.DataSource = DataSourceDict.TryGetValue(loop.ID_DATA_SOURCE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionDataSource> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionDataSource)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionDataSource).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionDataSource> Members
        public bool Equals(OpActionDataSource other)
        {
            if (other == null)
                return false;
            return this.IdAction.Equals(other.IdAction);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActionDataSource)
                return this.IdAction == ((OpActionDataSource)obj).IdAction;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActionDataSource left, OpActionDataSource right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionDataSource left, OpActionDataSource right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAction.GetHashCode();
        }
    #endregion
    }
#endif
}