using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpModuleHistory : DB_MODULE_HISTORY, IComparable, IEquatable<OpModuleHistory>
    {
    #region Properties
        public int IdModuleHistory { get { return this.ID_MODULE_HISTORY; } set { this.ID_MODULE_HISTORY = value; } }
        public int IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        public string CommonName { get { return this.COMMON_NAME; } set { this.COMMON_NAME = value; } }
        public string Version { get { return this.VERSION; } set { this.VERSION = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public int IdOperatorUpgrade { get { return this.ID_OPERATOR_UPGRADE; } set { this.ID_OPERATOR_UPGRADE = value; } }
        public int IdOperatorAccept { get { return this.ID_OPERATOR_ACCEPT; } set { this.ID_OPERATOR_ACCEPT = value; } }
        public string UpgradeDescription { get { return this.UPGRADE_DESCRIPTION; } set { this.UPGRADE_DESCRIPTION = value; } }
    #endregion

    #region	Navigation Properties
        private OpModule _Module;
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null) ? 0 : (int)value.ID_MODULE; } }
        private OpOperator _UpgradedBy;
        public OpOperator UpgradedBy { get { return this._UpgradedBy; } set { this._UpgradedBy = value; this.ID_OPERATOR_UPGRADE = (value == null) ? 0 : value.ID_OPERATOR; } }
        private OpOperator _AcceptedBy;
        public OpOperator AcceptedBy { get { return this._AcceptedBy; } set { this._AcceptedBy = value; this.ID_OPERATOR_ACCEPT = (value == null) ? 0 : value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpModuleHistory()
            : base()
        {
        }

        public OpModuleHistory(DB_MODULE_HISTORY clone)
            : base(clone)
        {
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.COMMON_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpModuleHistory> ConvertList(DB_MODULE_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpModuleHistory> ConvertList(DB_MODULE_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpModuleHistory> ret = new List<OpModuleHistory>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpModuleHistory(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpModuleHistory> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Select(l => l.ID_MODULE).Distinct().ToArray()).ToDictionary(l => l.ID_MODULE);
                Dictionary<int, OpOperator> OperatorDictAccept = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_ACCEPT).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorDictUpgrade = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_UPGRADE).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE);
                    loop.UpgradedBy = OperatorDictUpgrade.TryGetValue(loop.ID_OPERATOR_UPGRADE);
                    loop.AcceptedBy = OperatorDictAccept.TryGetValue(loop.ID_OPERATOR_ACCEPT);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpModuleHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpModuleHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpModuleHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpModuleHistory> Members
        public bool Equals(OpModuleHistory other)
        {
            if (other == null)
                return false;
            return this.IdModuleHistory.Equals(other.IdModuleHistory);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpModuleHistory)
                return this.IdModuleHistory == ((OpModuleHistory)obj).IdModuleHistory;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpModuleHistory left, OpModuleHistory right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpModuleHistory left, OpModuleHistory right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdModuleHistory.GetHashCode();
        }
    #endregion

    }
#endif
}