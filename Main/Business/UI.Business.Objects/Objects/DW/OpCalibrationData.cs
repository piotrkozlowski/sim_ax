using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpCalibrationData : DB_CALIBRATION_DATA, IComparable, IEquatable<OpCalibrationData>
    {
    #region Properties
        public long IdCalibrationData { get { return this.ID_CALIBRATION_DATA; } set { this.ID_CALIBRATION_DATA = value; } }
        public int IdCalibrationSession { get { return this.ID_CALIBRATION_SESSION; } set { this.ID_CALIBRATION_SESSION = value; } }
        public int? IdCalibrationModule { get { return this.ID_CALIBRATION_MODULE; } set { this.ID_CALIBRATION_MODULE = value; } }
        public long? IdCalibrationTest { get { return this.ID_CALIBRATION_TEST; } set { this.ID_CALIBRATION_TEST = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public int Status { get { return this.STATUS; } set { this.STATUS = value; } }
    #endregion

    #region	Navigation Properties
        private OpCalibrationSession _CalibrationSession;
        public OpCalibrationSession CalibrationSession { get { return this._CalibrationSession; } set { this._CalibrationSession = value; this.ID_CALIBRATION_SESSION = (value == null) ? 0 : (int)value.ID_CALIBRATION_SESSION; } }
        private OpCalibrationModule _CalibrationModule;
        public OpCalibrationModule CalibrationModule { get { return this._CalibrationModule; } set { this._CalibrationModule = value; this.ID_CALIBRATION_MODULE = (value == null) ? null : (int?)value.ID_CALIBRATION_MODULE; } }
        private OpCalibrationTest _CalibrationTest;
        public OpCalibrationTest CalibrationTest { get { return this._CalibrationTest; } set { this._CalibrationTest = value; this.ID_CALIBRATION_TEST = (value == null) ? null : (int?)value.ID_CALIBRATION_TEST; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpCalibrationData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpCalibrationSession> SessionDict = dataProvider.GetCalibrationSessions(list.Select(l => l.ID_CALIBRATION_SESSION).Distinct().ToArray()).ToDictionary(l => l.ID_CALIBRATION_SESSION);
                Dictionary<int, OpCalibrationModule> ModuleDict = dataProvider.GetCalibrationModules(list.Where(l => l.ID_CALIBRATION_MODULE.HasValue).Select(l => l.ID_CALIBRATION_MODULE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_CALIBRATION_MODULE);
                Dictionary<long, OpCalibrationTest> TestDict = dataProvider.GetCalibrationTests(list.Where(l => l.ID_CALIBRATION_TEST.HasValue).Select(l => l.ID_CALIBRATION_TEST.Value).Distinct().ToArray()).ToDictionary(l => l.ID_CALIBRATION_TEST);

                foreach (var loop in list)
                {
                    loop.CalibrationSession = SessionDict.TryGetValue(loop.ID_CALIBRATION_SESSION);
                    if (loop.ID_CALIBRATION_MODULE.HasValue)
                        loop.CalibrationModule = ModuleDict.TryGetValue(loop.ID_CALIBRATION_MODULE.Value);
                    if (loop.ID_CALIBRATION_TEST.HasValue)
                        loop.CalibrationTest = TestDict.TryGetValue(loop.ID_CALIBRATION_TEST.Value);
                }
            }
        }
    #endregion

    #region	Ctor
        public OpCalibrationData()
            : base() { }

        public OpCalibrationData(DB_CALIBRATION_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.IdDataType.ToString() + ": " + Value.ToString();
        }
    #endregion

    #region	ConvertList
        public static List<OpCalibrationData> ConvertList(DB_CALIBRATION_DATA[] db_objects)
        {
            List<OpCalibrationData> ret = new List<OpCalibrationData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpCalibrationData(db_object)));

            return ret;
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpCalibrationData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpCalibrationData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpCalibrationSession> Members
        public bool Equals(OpCalibrationData other)
        {
            if (other == null)
                return false;
            return this.IdCalibrationData.Equals(other.IdCalibrationData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpCalibrationData)
                return this.IdCalibrationData == ((OpCalibrationData)obj).IdCalibrationData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpCalibrationData left, OpCalibrationData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpCalibrationData left, OpCalibrationData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdCalibrationData.GetHashCode();
        }
    #endregion
    }
#endif
}