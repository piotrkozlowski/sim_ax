using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpRefuel : DB_REFUEL, IComparable, IEquatable<OpRefuel>, IReferenceType, IOpDynamicProperty<OpRefuel>
    {
        #region Properties
        public long IdRefuel { get { return this.ID_REFUEL; } set { this.ID_REFUEL = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public long IdDataSource { get { return this.ID_DATA_SOURCE; } set { this.ID_DATA_SOURCE = value; } }
        public DateTime? StartTime { get { return this.START_TIME; } set { this.START_TIME = value; StartTimeDataList = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; EndTimeDataList = value; } }
        #endregion

        #region	Navigation Properties
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpDataSource _DataSource;
        public OpDataSource DataSource { get { return this._DataSource; } set { this._DataSource = value; this.ID_DATA_SOURCE = (value == null) ? 0 : (long)value.ID_DATA_SOURCE; } }
        #endregion

        #region	Custom Properties
        public DateTime? StartTimeDataList
        {
            get
            {
                if (DataProvider != null)
                    return DataProvider.ConvertUtcTimeToTimeZone(DataList.TryGetNullableValue<DateTime>(DataType.REFUEL_START_TIME));
                else
                    return DataList.TryGetNullableValue<DateTime>(DataType.REFUEL_START_TIME).Value.ToLocalTime();
            }
            set
            {
                if (DataProvider != null)
                    DataList.SetValue(DataType.REFUEL_START_TIME, DataProvider.ConvertTimeZoneToUtcTime(value));
                else
                    DataList.SetValue(DataType.REFUEL_START_TIME, value.Value.ToUniversalTime());
            }
        }
        public DateTime? EndTimeDataList
        {
            get
            {
                if (DataProvider != null)
                    return DataProvider.ConvertUtcTimeToTimeZone(DataList.TryGetNullableValue<DateTime>(DataType.REFUEL_END_TIME));
                else
                    return DataList.TryGetNullableValue<DateTime>(DataType.REFUEL_END_TIME).Value.ToLocalTime();
            }
            set
            {
                if (DataProvider != null)
                    DataList.SetValue(DataType.REFUEL_END_TIME, DataProvider.ConvertTimeZoneToUtcTime(value));
                else
                    DataList.SetValue(DataType.REFUEL_END_TIME, value.Value.ToUniversalTime());
            }
        }
        public int TotalVolume
        {
            get
            {
                return Convert.ToInt32(DataList.TryGetValue<double>(DataType.REFUEL_TOTAL_VOLUME) * 1000);
            }
            set
            {
                DataList.SetValue(DataType.REFUEL_TOTAL_VOLUME, value / 1000);
            }
        }

        public OpDataList<OpRefuelData> DataList { get; set; }
        #endregion

        #region	Ctor
        public OpRefuel()
            : base()
        {
            DataList = new OpDataList<OpRefuelData>();
            dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpRefuel(DB_REFUEL clone)
            : base(clone)
        {
            DataList = new OpDataList<OpRefuelData>();
            dynamicProperties = new OpDynamicPropertyDict();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "Refuel [" + IdRefuel.ToString() + "]";
        }
        #endregion

        #region Private members
        private static IDataProvider DataProvider = null;
        #endregion
        #region	ConvertList
        public static List<OpRefuel> ConvertList(DB_REFUEL[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRefuel> ConvertList(DB_REFUEL[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            return ConvertList(db_objects, dataProvider, loadNavigationProperties, true);
        }

        public static List<OpRefuel> ConvertList(DB_REFUEL[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData, List<long> customDataTypes = null)
        {
            DataProvider = dataProvider;

            List<OpRefuel> ret = new List<OpRefuel>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRefuel(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRefuel> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<long, OpDataSource> DataSourceDict = dataProvider.GetDataSource(list.Select(l => l.ID_DATA_SOURCE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_SOURCE);
                foreach (var loop in list)
                {
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.DataSource = DataSourceDict.TryGetValue(loop.ID_DATA_SOURCE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpRefuel> list, IDataProvider dataProvider, List<long> customDataTypes = null)
        {
            if (list != null && list.Count > 0 
                && ((dataProvider.IRefuelDataTypes != null && dataProvider.IRefuelDataTypes.Count > 0)
                    || (customDataTypes != null && customDataTypes.Count > 0)))
            {
                List<long> idRefuel = list.Select(l => l.IdRefuel).ToList();
                List<OpRefuelData> data = new List<OpRefuelData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetRefuelDataFilter(IdRefuel: idRefuel.ToArray(), IdDataType: customDataTypes.ToArray());
                else if (dataProvider.IRefuelDataTypes != null && dataProvider.IRefuelDataTypes.Count > 0)
                    data = dataProvider.GetRefuelDataFilter(IdRefuel: idRefuel.ToArray(), IdDataType: dataProvider.IRefuelDataTypes.ToArray());
                
                if (data != null && data.Count > 0)
                {
                    Dictionary<long, OpRefuel> refuelsDict = list.ToDictionary<OpRefuel, long>(l => l.IdRefuel);
                    foreach (var dataValue in data)
                    {
                        refuelsDict[dataValue.IdRefuel].DataList.Add(dataValue);
                    }
                }
            }
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRefuel)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRefuel).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpRefuel> Members
        public bool Equals(OpRefuel other)
        {
            if (other == null)
                return false;
            return this.IdRefuel.Equals(other.IdRefuel);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRefuel)
                return this.IdRefuel == ((OpRefuel)obj).IdRefuel;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRefuel left, OpRefuel right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRefuel left, OpRefuel right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRefuel.GetHashCode();
        }
        #endregion

        #region IReferenceType
        public object GetReferenceKey()
        {
            return ID_REFUEL;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion
        #region IOpDynamicProperty
        private OpDynamicPropertyDict dynamicProperties;
        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return dynamicProperties; }
            set { if (value != null) dynamicProperties = value; }
        }
        [XmlIgnore]
        public OpRefuel Owner
        {
            get { return this; }
        }
        #endregion
    }
#endif
}