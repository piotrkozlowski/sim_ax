using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeliveryOrderHistory : DB_DELIVERY_ORDER_HISTORY, IComparable, IEquatable<OpDeliveryOrderHistory>
    {
    #region Properties
        public long IdDeliveryOrderHistory { get { return this.ID_DELIVERY_ORDER_HISTORY; } set { this.ID_DELIVERY_ORDER_HISTORY = value; } }
        public int IdDeliveryOrder { get { return this.ID_DELIVERY_ORDER; } set { this.ID_DELIVERY_ORDER = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public bool IsSuccesfull { get { return this.IS_SUCCESFULL; } set { this.IS_SUCCESFULL = value; } }
        public DateTime CommitDate { get { return this.COMMIT_DATE; } set { this.COMMIT_DATE = value; } }
        public string CommitResult { get { return this.COMMIT_RESULT; } set { this.COMMIT_RESULT = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeliveryOrder _DeliveryOrder;
        public OpDeliveryOrder DeliveryOrder { get { return this._DeliveryOrder; } set { this._DeliveryOrder = value; this.ID_DELIVERY_ORDER = (value == null) ? 0 : (int)value.ID_DELIVERY_ORDER; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeliveryOrderHistory()
            : base() { }

        public OpDeliveryOrderHistory(DB_DELIVERY_ORDER_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.COMMIT_RESULT;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeliveryOrderHistory> ConvertList(DB_DELIVERY_ORDER_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeliveryOrderHistory> ConvertList(DB_DELIVERY_ORDER_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeliveryOrderHistory> ret = new List<OpDeliveryOrderHistory>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeliveryOrderHistory(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeliveryOrderHistory> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeliveryOrder> DeliveryOrderDict = dataProvider.GetDeliveryOrder(list.Select(l => l.ID_DELIVERY_ORDER).Distinct().ToArray()).ToDictionary(l => l.ID_DELIVERY_ORDER);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.DeliveryOrder = DeliveryOrderDict.TryGetValue(loop.ID_DELIVERY_ORDER);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeliveryOrderHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeliveryOrderHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeliveryOrderHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeliveryOrderHistory> Members
        public bool Equals(OpDeliveryOrderHistory other)
        {
            if (other == null)
                return false;
            return this.IdDeliveryOrderHistory.Equals(other.IdDeliveryOrderHistory);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeliveryOrderHistory)
                return this.IdDeliveryOrderHistory == ((OpDeliveryOrderHistory)obj).IdDeliveryOrderHistory;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeliveryOrderHistory left, OpDeliveryOrderHistory right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeliveryOrderHistory left, OpDeliveryOrderHistory right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeliveryOrderHistory.GetHashCode();
        }
    #endregion
    }
#endif
}