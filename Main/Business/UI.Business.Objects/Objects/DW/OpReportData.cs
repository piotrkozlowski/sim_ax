using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpReportData : DB_REPORT_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpReportData>, IOpChangeState
    {
    #region Properties
        [XmlIgnore]
        public long IdReportData { get { return this.ID_REPORT_DATA; } set { this.ID_REPORT_DATA = value; } }
        [XmlIgnore]
        public int IdReport { get { return this.ID_REPORT; } set { this.ID_REPORT = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        [XmlIgnore]
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
       
        #endregion

        #region Navigation Properties
        [DataMember]
		private OpReport _Report;
        [XmlIgnore]
        public OpReport Report { get { return this._Report; } set { this._Report = value; this.ID_REPORT = (value == null) ? 0 : (int)value.ID_REPORT; } }
        [DataMember]
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdReportData; } set { IdReportData = value; } }
        public int Index { get; set; }
        [XmlIgnore, DataMember]
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
        public OpReportData()
			: base() { this.OpState = OpChangeState.New; }

        public OpReportData(DB_REPORT_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
        public override string ToString()
        {
            return "ReportData [" + IdReportData.ToString() + "]";
        }
    #endregion

    #region ConvertList
        public static List<OpReportData> ConvertList(DB_REPORT_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpReportData> ConvertList(DB_REPORT_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpReportData> ret = new List<OpReportData>(list.Length);
            foreach (var loop in list)
            {
                OpReportData insert = new OpReportData(loop);
                if (loadNavigationProperties)
                {
                    //insert.Report = dataProvider.GetReport(loop.ID_REPORT);
                    insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpReportData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpReportData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpReportData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpReportData> Members
        public bool Equals(OpReportData other)
        {
            if (other == null)
                return false;
            return this.IdReportData.Equals(other.IdReportData);
        }
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public object Clone(IDataProvider dataProvider)
		{
			OpReportData clone = new OpReportData();
			clone.IdData = this.IdData;
			clone.IdReport = this.IdReport;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_REPORT_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.Report = dataProvider.GetReport(this.ID_REPORT);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpReportData)
				return this.IdData == ((OpReportData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpReportData left, OpReportData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpReportData left, OpReportData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
    }
#endif
}