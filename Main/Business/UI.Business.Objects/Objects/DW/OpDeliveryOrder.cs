using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeliveryOrder : DB_DELIVERY_ORDER, IComparable, IEquatable<OpDeliveryOrder>, IOpDynamicProperty<OpDeliveryOrder>
    {
    #region Properties
        public int IdDeliveryOrder { get { return this.ID_DELIVERY_ORDER; } set { this.ID_DELIVERY_ORDER = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public int IdDeliveryBatch { get { return this.ID_DELIVERY_BATCH; } set { this.ID_DELIVERY_BATCH = value; } }
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public int IdDeliveryAdvice { get { return this.ID_DELIVERY_ADVICE; } set { this.ID_DELIVERY_ADVICE = value; } }
        public int? DeliveryVolume { get { return this.DELIVERY_VOLUME; } set { this.DELIVERY_VOLUME = value; } }
        public bool? IsSuccesfull { get { return this.IS_SUCCESFULL; } set { this.IS_SUCCESFULL = value; } }
        public string OrderNo { get { return this.ORDER_NO; } set { this.ORDER_NO = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeliveryBatch _DeliveryBatch;
        public OpDeliveryBatch DeliveryBatch { get { return this._DeliveryBatch; } set { this._DeliveryBatch = value; this.ID_DELIVERY_BATCH = (value == null) ? 0 : (int)value.ID_DELIVERY_BATCH; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? 0 : (long)value.ID_LOCATION; } }
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpDeliveryAdvice _DeliveryAdvice;
        public OpDeliveryAdvice DeliveryAdvice { get { return this._DeliveryAdvice; } set { this._DeliveryAdvice = value; this.ID_DELIVERY_ADVICE = (value == null) ? 0 : (int)value.ID_DELIVERY_ADVICE; } }
    #endregion

    #region	Custom Properties
        private OpDynamicPropertyDict dynamicProperties;
    #endregion

    #region	Ctor
        public OpDeliveryOrder()
            : base() 
        {
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpDeliveryOrder(DB_DELIVERY_ORDER clone)
            : base(clone) 
        {
            this.dynamicProperties = new OpDynamicPropertyDict();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeliveryOrder [" + IdDeliveryOrder.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeliveryOrder> ConvertList(DB_DELIVERY_ORDER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeliveryOrder> ConvertList(DB_DELIVERY_ORDER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeliveryOrder> ret = new List<OpDeliveryOrder>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeliveryOrder(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeliveryOrder> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeliveryBatch> DeliveryBatchDict = dataProvider.GetDeliveryBatch(list.Select(l => l.ID_DELIVERY_BATCH).Distinct().ToArray()).ToDictionary(l => l.ID_DELIVERY_BATCH);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Select(l => l.ID_LOCATION).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpDeliveryAdvice> DeliveryAdviceDict = dataProvider.GetDeliveryAdvice(list.Select(l => l.ID_DELIVERY_ADVICE).Distinct().ToArray()).ToDictionary(l => l.ID_DELIVERY_ADVICE);

                foreach (var loop in list)
                {
                    loop.DeliveryBatch = DeliveryBatchDict.TryGetValue(loop.ID_DELIVERY_BATCH);
                    loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION);
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                    loop.DeliveryAdvice = DeliveryAdviceDict.TryGetValue(loop.ID_DELIVERY_ADVICE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeliveryOrder> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeliveryOrder)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeliveryOrder).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeliveryOrder> Members
        public bool Equals(OpDeliveryOrder other)
        {
            if (other == null)
                return false;
            return this.IdDeliveryOrder.Equals(other.IdDeliveryOrder);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeliveryOrder)
                return this.IdDeliveryOrder == ((OpDeliveryOrder)obj).IdDeliveryOrder;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeliveryOrder left, OpDeliveryOrder right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeliveryOrder left, OpDeliveryOrder right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeliveryOrder.GetHashCode();
        }
    #endregion

    #region Implementation of IOpDynamicProperty
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpDeliveryOrder IOpDynamicProperty<OpDeliveryOrder>.Owner
        {
            get { return this; }
        }
    #endregion
    }
#endif
}