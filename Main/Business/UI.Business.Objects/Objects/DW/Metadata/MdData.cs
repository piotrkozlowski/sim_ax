namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpData Metadata
	/// </summary>
	public class MdData
	{
		public const string IdData = "IdData";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string DataStatus = "DataStatus";
		public const string Time = "Time";
		public const string IdDataSource = "IdDataSource";
		public const string IdDataSourceType = "IdDataSourceType";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string DataType = "DataType";
		public const string DataSource = "DataSource";
		public const string DataSourceType = "DataSourceType";
		public const string Unit = "Unit";
		public const string ValueUnit = "ValueUnit";
		public const string ValueDecimal = "ValueDecimal";
		public const string ValueDouble = "ValueDouble";
		public const string ValueLong = "ValueLong";
		public const string ValueDateTime = "ValueDateTime";
		public const string ValueBool = "ValueBool";
		public const string ValueString = "ValueString";
		public const string OpState = "OpState";
	
	}
}
	