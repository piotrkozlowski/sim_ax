namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDeliveryBatchStatus Metadata
	/// </summary>
	public class MdDeliveryBatchStatus
	{
		public const string IdDeliveryBatchStatus = "IdDeliveryBatchStatus";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	