namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDataTemporal Metadata
	/// </summary>
	public class MdDataTemporal
	{
		public const string IdDataTemporal = "IdDataTemporal";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string IdAggregationType = "IdAggregationType";
		public const string IdDataSource = "IdDataSource";
		public const string IdDataSourceType = "IdDataSourceType";
		public const string IdAlarmEvent = "IdAlarmEvent";
		public const string DataStatus = "DataStatus";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string DataType = "DataType";
		public const string DataSource = "DataSource";
		public const string DataSourceType = "DataSourceType";
		public const string OpState = "OpState";
		public const string Unit = "Unit";
		public const string ValueUnit = "ValueUnit";
		public const string ValueDecimal = "ValueDecimal";
		public const string ValueDouble = "ValueDouble";
		public const string ValueLong = "ValueLong";
		public const string ValueDateTime = "ValueDateTime";
		public const string ValueBool = "ValueBool";
		public const string ValueString = "ValueString";
	
	}
}
	