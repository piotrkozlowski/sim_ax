namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDataSource Metadata
	/// </summary>
	public class MdDataSource
	{
		public const string IdDataSource = "IdDataSource";
		public const string IdDataSourceType = "IdDataSourceType";
		public const string DataSourceType = "DataSourceType";
		public const string IdPacket = "IdPacket";
	
	}
}
	