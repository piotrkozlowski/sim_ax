namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDataSourceType Metadata
	/// </summary>
	public class MdDataSourceType
	{
		public const string IdDataSourceType = "IdDataSourceType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	