namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpRefuel Metadata
	/// </summary>
	public class MdRefuel
	{
		public const string IdRefuel = "IdRefuel";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdDataSource = "IdDataSource";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string DataSource = "DataSource";
		public const string StartTimeDataList = "StartTimeDataList";
		public const string EndTimeDataList = "EndTimeDataList";
		public const string TotalVolume = "TotalVolume";
		public const string DataList = "DataList";
	
	}
}
	