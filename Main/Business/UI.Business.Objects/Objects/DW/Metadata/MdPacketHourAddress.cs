namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpPacketHourAddress Metadata
	/// </summary>
	public class MdPacketHourAddress
	{
		public const string Address = "Address";
		public const string Date = "Date";
		public const string IsIncoming = "IsIncoming";
		public const string Packets = "Packets";
	
	}
}
	