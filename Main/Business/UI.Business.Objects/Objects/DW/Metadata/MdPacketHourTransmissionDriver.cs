namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpPacketHourTransmissionDriver Metadata
	/// </summary>
	public class MdPacketHourTransmissionDriver
	{
		public const string IdTransmissionDriver = "IdTransmissionDriver";
		public const string Date = "Date";
		public const string IsIncoming = "IsIncoming";
		public const string Packets = "Packets";
		public const string IdTransmissionType = "IdTransmissionType";
		public const string TransmissionDriver = "TransmissionDriver";
		public const string TransmissionDriverType = "TransmissionDriverType";
		public const string DispTransmissionDriver = "DispTransmissionDriver";
		public const string DispTransmissionType = "DispTransmissionType";
	
	}
}
	