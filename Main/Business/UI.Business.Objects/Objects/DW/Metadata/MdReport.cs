namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpReport Metadata
	/// </summary>
	public class MdReport
	{
		public const string IdReport = "IdReport";
		public const string IdReportType = "IdReportType";
		public const string Layout = "Layout";
		public const string ReportType = "ReportType";
		public const string Name = "Name";
		public const string Description = "Description";
		public const string StoredProcedure = "StoredProcedure";
		public const string DataList = "DataList";
        public const string UseCOREDatabase = "UseCOREDatabase";


    }
}
	