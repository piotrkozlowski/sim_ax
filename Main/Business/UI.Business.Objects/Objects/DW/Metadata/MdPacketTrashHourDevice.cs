namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpPacketTrashHourDevice Metadata
	/// </summary>
	public class MdPacketTrashHourDevice
	{
		public const string SerialNbr = "SerialNbr";
		public const string IdTransmissionDriver = "IdTransmissionDriver";
		public const string Date = "Date";
		public const string IsIncoming = "IsIncoming";
		public const string Packets = "Packets";
		public const string TransmissionDriver = "TransmissionDriver";
		public const string DispTransmissionDriver = "DispTransmissionDriver";
	
	}
}
	