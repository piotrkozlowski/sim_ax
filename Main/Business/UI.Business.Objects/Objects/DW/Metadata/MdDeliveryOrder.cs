namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDeliveryOrder Metadata
	/// </summary>
	public class MdDeliveryOrder
	{
		public const string IdDeliveryOrder = "IdDeliveryOrder";
		public const string CreationDate = "CreationDate";
		public const string IdDeliveryBatch = "IdDeliveryBatch";
		public const string IdLocation = "IdLocation";
		public const string IdMeter = "IdMeter";
		public const string IdOperator = "IdOperator";
		public const string IdDeliveryAdvice = "IdDeliveryAdvice";
		public const string DeliveryVolume = "DeliveryVolume";
		public const string IsSuccesfull = "IsSuccesfull";
		public const string OrderNo = "OrderNo";
		public const string DeliveryBatch = "DeliveryBatch";
		public const string Location = "Location";
		public const string Meter = "Meter";
		public const string Operator = "Operator";
		public const string DeliveryAdvice = "DeliveryAdvice";
	
	}
}
	