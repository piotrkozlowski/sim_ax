namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpReportDataType Metadata
	/// </summary>
	public class MdReportDataType
	{
		public const string IdReportDataType = "IdReportDataType";
		public const string Signature = "Signature";
		public const string IdDataType = "IdDataType";
		public const string IdUnit = "IdUnit";
		public const string DigitsAfterComma = "DigitsAfterComma";
		public const string SequenceNbr = "SequenceNbr";
		public const string DataType = "DataType";
		public const string Unit = "Unit";
        public const string IdReportDataTypeParent = "IdReportDataTypeParent";
        public const string IdReportDataTypeChild = "IdReportDataTypeChild";
        public const string OpState = "OpState";
	
	}
}
	