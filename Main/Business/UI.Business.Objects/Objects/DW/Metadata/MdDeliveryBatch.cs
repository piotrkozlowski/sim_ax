namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDeliveryBatch Metadata
	/// </summary>
	public class MdDeliveryBatch
	{
		public const string IdDeliveryBatch = "IdDeliveryBatch";
		public const string CreationDate = "CreationDate";
		public const string Name = "Name";
		public const string IdDeliveryBatchStatus = "IdDeliveryBatchStatus";
		public const string IdOperator = "IdOperator";
		public const string CommitDate = "CommitDate";
		public const string IdDistributor = "IdDistributor";
		public const string SuccessTotal = "SuccessTotal";
		public const string FailedTotal = "FailedTotal";
		public const string DeliveryBatchStatus = "DeliveryBatchStatus";
		public const string Operator = "Operator";
		public const string Distributor = "Distributor";
	
	}
}
	