namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpReportParam Metadata
	/// </summary>
	public class MdReportParam
	{
		public const string IdReportParam = "IdReportParam";
		public const string IdReport = "IdReport";
		public const string FieldName = "FieldName";
		public const string IdDescr = "IdDescr";
		public const string IdUnit = "IdUnit";
		public const string Name = "Name";
		public const string IdReference = "IdReference";
		public const string IdDataTypeClass = "IdDataTypeClass";
		public const string IsInputParam = "IsInputParam";
		public const string IsVisible = "IsVisible";
		public const string DefaultValue = "DefaultValue";
		public const string ColorBg = "ColorBg";
		public const string Format = "Format";
		public const string Report = "Report";
		public const string Descr = "Descr";
		public const string Unit = "Unit";
		public const string DataTypeClass = "DataTypeClass";
        public const string Reference = "Reference";
		public const string Value = "Value";
	
	}
}
	