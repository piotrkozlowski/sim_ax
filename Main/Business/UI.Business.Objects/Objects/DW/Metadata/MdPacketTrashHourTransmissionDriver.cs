namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpPacketTrashHourTransmissionDriver Metadata
	/// </summary>
	public class MdPacketTrashHourTransmissionDriver
	{
		public const string IdTransmissionDriver = "IdTransmissionDriver";
		public const string Date = "Date";
		public const string IsIncoming = "IsIncoming";
		public const string Packets = "Packets";
		public const string IdTransmissionType = "IdTransmissionType";
		public const string TransmissionDriver = "TransmissionDriver";
		public const string TransmissionDriverType = "TransmissionDriverType";
		public const string DispTransmissionDriver = "DispTransmissionDriver";
		public const string DispTransmissionType = "DispTransmissionType";
	
	}
}
	