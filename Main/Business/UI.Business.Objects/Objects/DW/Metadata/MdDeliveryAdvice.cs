namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDeliveryAdvice Metadata
	/// </summary>
	public class MdDeliveryAdvice
	{
		public const string IdDeliveryAdvice = "IdDeliveryAdvice";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Priority = "Priority";
		public const string Descr = "Descr";
		public const string PreviousAdvice = "PreviousAdvice";
	
	}
}
	