namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpConsumerTransaction Metadata
	/// </summary>
	public class MdConsumerTransaction
	{
		public const string IdConsumerTransaction = "IdConsumerTransaction";
		public const string IdConsumer = "IdConsumer";
		public const string IdOperator = "IdOperator";
		public const string IdDistributor = "IdDistributor";
		public const string IdTransactionType = "IdTransactionType";
		public const string IdTariff = "IdTariff";
		public const string Timestamp = "Timestamp";
		public const string ChargePrepaid = "ChargePrepaid";
		public const string ChargeEc = "ChargeEc";
		public const string ChargeOwed = "ChargeOwed";
		public const string BalancePrepaid = "BalancePrepaid";
		public const string BalanceEc = "BalanceEc";
		public const string BalanceOwed = "BalanceOwed";
		public const string MeterIndex = "MeterIndex";
		public const string IdConsumerSettlement = "IdConsumerSettlement";
		public const string IdModule = "IdModule";
		public const string TransactionGuid = "TransactionGuid";
		public const string Consumer = "Consumer";
		public const string Operator = "Operator";
		public const string Distributor = "Distributor";
		public const string Tariff = "Tariff";
		public const string ConsumerTransactionType = "ConsumerTransactionType";
		public const string ConsumerSettlement = "ConsumerSettlement";
		public const string TotalPaid = "TotalPaid";
		public const string BalancePrepaidBeforeTransaction = "BalancePrepaidBeforeTransaction";
		public const string Debt = "Debt";
		public const string Balance = "Balance";
		public const string PositiveChargePrepaid = "PositiveChargePrepaid";
		public const string PositiveChargeEc = "PositiveChargeEc";
		public const string PositiveChargeOwed = "PositiveChargeOwed";
		public const string MeterIndexM3 = "MeterIndexM3";
	
	}
}
	