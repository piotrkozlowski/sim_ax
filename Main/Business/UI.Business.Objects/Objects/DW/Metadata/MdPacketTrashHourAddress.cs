namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpPacketTrashHourAddress Metadata
	/// </summary>
	public class MdPacketTrashHourAddress
	{
		public const string Address = "Address";
		public const string Date = "Date";
		public const string IsIncoming = "IsIncoming";
		public const string Packets = "Packets";
	
	}
}
	