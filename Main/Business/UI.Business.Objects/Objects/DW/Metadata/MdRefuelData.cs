namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpRefuelData Metadata
	/// </summary>
	public class MdRefuelData
	{
		public const string IdRefuelData = "IdRefuelData";
		public const string IdRefuel = "IdRefuel";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Status = "Status";
		public const string Refuel = "Refuel";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string OpState = "OpState";
		public const string Unit = "Unit";
	
	}
}
	