namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpDeliveryOrderHistory Metadata
	/// </summary>
	public class MdDeliveryOrderHistory
	{
		public const string IdDeliveryOrderHistory = "IdDeliveryOrderHistory";
		public const string IdDeliveryOrder = "IdDeliveryOrder";
		public const string IdOperator = "IdOperator";
		public const string IsSuccesfull = "IsSuccesfull";
		public const string CommitDate = "CommitDate";
		public const string CommitResult = "CommitResult";
		public const string Notes = "Notes";
		public const string DeliveryOrder = "DeliveryOrder";
		public const string Operator = "Operator";
	
	}
}
	