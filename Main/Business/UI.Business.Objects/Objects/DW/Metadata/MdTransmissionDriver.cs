namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpTransmissionDriver Metadata
	/// </summary>
	public class MdTransmissionDriver
	{
		public const string IdTransmissionDriver = "IdTransmissionDriver";
		public const string IdTransmissionDriverType = "IdTransmissionDriverType";
		public const string TransmissionDriverTypeName = "TransmissionDriverTypeName";
		public const string Name = "Name";
		public const string ResponseAddress = "ResponseAddress";
		public const string Plugin = "Plugin";
	
	}
}

	