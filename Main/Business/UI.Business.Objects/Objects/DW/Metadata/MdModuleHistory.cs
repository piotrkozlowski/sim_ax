namespace IMR.Suite.UI.Business.Objects.DW.Metadata
{
	/// <summary>
	/// OpModuleHistory Metadata
	/// </summary>
	public class MdModuleHistory
	{
		public const string IdModuleHistory = "IdModuleHistory";
		public const string IdModule = "IdModule";
		public const string CommonName = "CommonName";
		public const string Version = "Version";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string IdOperatorUpgrade = "IdOperatorUpgrade";
		public const string IdOperatorAccept = "IdOperatorAccept";
		public const string UpgradeDescription = "UpgradeDescription";
		public const string Module = "Module";
		public const string UpgradedBy = "UpgradedBy";
		public const string AcceptedBy = "AcceptedBy";
	
	}
}
	