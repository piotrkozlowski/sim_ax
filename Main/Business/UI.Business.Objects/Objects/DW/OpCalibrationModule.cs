using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.DW;
using IMR.Suite.UI.Business.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpCalibrationModule : DB_CALIBRATION_MODULE, IComparable, IEquatable<OpCalibrationModule>
    {
    #region Properties
        public int IdCalibrationModule { get { return this.ID_CALIBRATION_MODULE; } set { this.ID_CALIBRATION_MODULE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }

    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpCalibrationModule()
            : base() { }

        public OpCalibrationModule(DB_CALIBRATION_MODULE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpCalibrationModule> ConvertList(DB_CALIBRATION_MODULE[] db_objects)
        {
            List<OpCalibrationModule> ret = new List<OpCalibrationModule>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpCalibrationModule(db_object)));

            return ret;
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpCalibrationModule)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpCalibrationModule).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpAlarm> Members
        public bool Equals(OpCalibrationModule other)
        {
            if (other == null)
                return false;
            return this.IdCalibrationModule.Equals(other.IdCalibrationModule);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpCalibrationModule)
                return this.IdCalibrationModule == ((OpCalibrationModule)obj).IdCalibrationModule;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpCalibrationModule left, OpCalibrationModule right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpCalibrationModule left, OpCalibrationModule right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdCalibrationModule.GetHashCode();
        }
    #endregion
    }
#endif
}