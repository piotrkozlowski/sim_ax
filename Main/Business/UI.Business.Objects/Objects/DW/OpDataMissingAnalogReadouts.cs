using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDataMissingAnalogReadouts : DB_DATA_MISSING_ANALOG_READOUTS, IComparable, IEquatable<OpDataMissingAnalogReadouts>
    {
    #region Properties
            public long IdDataMissingAnalogReadouts { get { return this.ID_DATA_MISSING_ANALOG_READOUTS;} set {this.ID_DATA_MISSING_ANALOG_READOUTS = value;}}
                public long SerialNbr { get { return this.SERIAL_NBR;} set {this.SERIAL_NBR = value;}}
                public long? SerialNbrParent { get { return this.SERIAL_NBR_PARENT;} set {this.SERIAL_NBR_PARENT = value;}}
                    public DateTime MissingDataStartTime { get { return this.MISSING_DATA_START_TIME;} set {this.MISSING_DATA_START_TIME = value; } }
                    public int PacketsCount { get { return this.PACKETS_COUNT;} set {this.PACKETS_COUNT = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDataMissingAnalogReadouts()
			:base() {}
		
		public OpDataMissingAnalogReadouts(DB_DATA_MISSING_ANALOG_READOUTS clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "DataMissingAnalogReadouts [" + IdDataMissingAnalogReadouts.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpDataMissingAnalogReadouts> ConvertList(DB_DATA_MISSING_ANALOG_READOUTS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDataMissingAnalogReadouts> ConvertList(DB_DATA_MISSING_ANALOG_READOUTS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDataMissingAnalogReadouts> ret = new List<OpDataMissingAnalogReadouts>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDataMissingAnalogReadouts(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDataMissingAnalogReadouts> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDataMissingAnalogReadouts> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataMissingAnalogReadouts)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataMissingAnalogReadouts).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDataMissingAnalogReadouts> Members
        public bool Equals(OpDataMissingAnalogReadouts other)
        {
            if (other == null)
				return false;
			return this.IdDataMissingAnalogReadouts.Equals(other.IdDataMissingAnalogReadouts);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDataMissingAnalogReadouts)
				return this.IdDataMissingAnalogReadouts == ((OpDataMissingAnalogReadouts)obj).IdDataMissingAnalogReadouts;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDataMissingAnalogReadouts left, OpDataMissingAnalogReadouts right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDataMissingAnalogReadouts left, OpDataMissingAnalogReadouts right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDataMissingAnalogReadouts.GetHashCode();
		}
    #endregion
    }
#endif
}