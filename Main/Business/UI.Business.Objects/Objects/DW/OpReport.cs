using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.Data;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.DW
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpReport : DB_REPORT, IComparable, IEquatable<OpReport>, IReferenceType, IOpDynamic, IOpChangeState, IOpObject<OpReportData>
    {
        #region Properties
        [XmlIgnore]
        public int IdReport { get { return this.ID_REPORT; } set { this.ID_REPORT = value; } }
        [XmlIgnore]
        public int IdReportType { get { return this.ID_REPORT_TYPE; } set { this.ID_REPORT_TYPE = value; } }
        [XmlIgnore]
        public object Layout { get { return this.LAYOUT; } set { this.LAYOUT = value; } }
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpReportType _ReportType;
        public OpReportType ReportType { get { return this._ReportType; } set { this._ReportType = value; this.ID_REPORT_TYPE = (value == null) ? 0 : (int)value.ID_REPORT_TYPE; } }
        #endregion

        #region	Custom Properties
        [XmlIgnore]
        public string Name
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.REPORT_NAME);
            }
            set
            {
                DataList.SetValue(DataType.REPORT_NAME, value);
            }
        }
        [XmlIgnore]
        public string Description
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.REPORT_DESCRIPTION);
            }
            set
            {
                DataList.SetValue(DataType.REPORT_DESCRIPTION, value);
            }
        }
        [XmlIgnore]
        public string StoredProcedure
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.REPORT_STORED_PROCEDURE);
            }
            set
            {
                DataList.SetValue(DataType.REPORT_STORED_PROCEDURE, value);
            }
        }
        [XmlIgnore]
        public bool UseCOREDatabase
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.REPORT_USE_CORE_DATABASE);
            }
            set
            {
                DataList.SetValue(DataType.REPORT_USE_CORE_DATABASE, value);
            }
        }
        [DataMember]
        public OpDataList<OpReportData> DataList { get; set; }
        [DataMember]
        public List<OpReportParam> ParamsList { get; set; }

        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;

        [DataMember]
        private Dictionary<string, object> dynamicValues;
        #endregion

        #region	Ctor
        public OpReport()
            : base()
        {
            DataList = new OpDataList<OpReportData>();
            ParamsList = new List<OpReportParam>();
            this.OpState = OpChangeState.New;
            this.DynamicValues = new Dictionary<string, object>();
        }

        public OpReport(DB_REPORT clone)
            : base(clone)
        {
            DataList = new OpDataList<OpReportData>();
            ParamsList = new List<OpReportParam>();
            this.OpState = OpChangeState.Loaded;
            this.DynamicValues = new Dictionary<string, object>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return Name;
        }
        #endregion

        #region	ConvertList
        public static List<OpReport> ConvertList(DB_REPORT[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpReport> ConvertList(DB_REPORT[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpReport> ret = new List<OpReport>(list.Length);
            list.ToList().ForEach(db_object => ret.Add(new OpReport(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpReport> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpReportType> ReportTypeDict = dataProvider.GetReportType(list.Select(l => l.ID_REPORT_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_REPORT_TYPE);
                foreach (var loop in list)
                {
                    loop.ReportType = ReportTypeDict.TryGetValue(loop.ID_REPORT_TYPE);
                }
            }
        }
        #endregion
        #region LoadCustomData
        public static void LoadCustomData(ref List<OpReport> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IReportDataTypes != null)
            {
                List<int> idReport = list.Select(l => l.IdReport).ToList();

                List<OpReportData> data = new List<OpReportData>();
                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetReportDataFilter(IdReport: idReport.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.IReportDataTypes != null && dataProvider.IReportDataTypes.Count > 0)
                    data = dataProvider.GetReportDataFilter(IdReport: idReport.ToArray(), IdDataType: dataProvider.IReportDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data != null && data.Count > 0)
                {
                    foreach (var report in list)
                    {
                        report.DataList.AddRange(data.Where(d => d.IdReport == report.IdReport));
                    }
                }
            }
        }
        #endregion

        #region LoadReportParams
        public static void LoadReportParams(ref List<OpReport> list, IDataProvider dataProvider,
            bool loadCustomData = true, bool loadNavigationProperties = true,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> idReport = list.Select(l => l.IdReport).ToList();

                List<OpReportParam> reportParams = dataProvider.GetReportParamFilter(loadCustomData: loadCustomData, loadNavigationProperties: loadNavigationProperties,
                    autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout,
                    IdReport: idReport.ToArray());



                if (reportParams != null && reportParams.Count > 0)
                {
                    foreach (var report in list)
                    {
                        report.ParamsList.AddRange(reportParams.Where(d => d.IdReport == report.IdReport));
                    }
                }
            }
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpReport)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpReport).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpReport> Members
        public bool Equals(OpReport other)
        {
            if (other == null)
                return false;
            return this.IdReport.Equals(other.IdReport);
        }
        #endregion

        #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdReport;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion

        #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdReport; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
        #endregion

        #region Implementation of IOpObject
        public object IdObject
        {
            get { return IdReport; }
            set { IdReport = Convert.ToInt32(value); }
        }
        #endregion
    }
#endif
}