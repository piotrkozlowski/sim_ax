using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLocationSupplier : DB_LOCATION_SUPPLIER, IComparable, IEquatable<OpLocationSupplier>
    {
    #region Properties
        public int IdLocationSupplier { get { return this.ID_LOCATION_SUPPLIER; } set { this.ID_LOCATION_SUPPLIER = value; } }
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public int IdDistributorSupplier { get { return this.ID_DISTRIBUTOR_SUPPLIER; } set { this.ID_DISTRIBUTOR_SUPPLIER = value; } }
        public int AccountNo { get { return this.ACCOUNT_NO; } set { this.ACCOUNT_NO = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
    #endregion

    #region	Navigation Properties
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? 0 : (long)value.ID_LOCATION; } }
        private OpDistributorSupplier _DistributorSupplier;
        public OpDistributorSupplier DistributorSupplier { get { return this._DistributorSupplier; } set { this._DistributorSupplier = value; this.ID_DISTRIBUTOR_SUPPLIER = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR_SUPPLIER; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpLocationSupplier()
            : base() { }

        public OpLocationSupplier(DB_LOCATION_SUPPLIER clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationSupplier> ConvertList(DB_LOCATION_SUPPLIER[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLocationSupplier> ConvertList(DB_LOCATION_SUPPLIER[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpLocationSupplier> ret = new List<OpLocationSupplier>(list.Length);
            foreach (var loop in list)
            {
                OpLocationSupplier insert = new OpLocationSupplier(loop);
                if (loadNavigationProperties)
                {
                    insert.Location = dataProvider.GetLocation(loop.ID_LOCATION);
                    insert.DistributorSupplier = dataProvider.GetDistributorSupplier(loop.ID_DISTRIBUTOR_SUPPLIER);

                    if (insert.Location != null && insert.EndTime == null)
                        insert.Location.CurrentSupplier = insert.DistributorSupplier;
                }
                ret.Add(insert);
            }
            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationSupplier> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationSupplier)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationSupplier).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpLocationSupplier> Members
        public bool Equals(OpLocationSupplier other)
        {
            if (other == null)
                return false;
            return this.IdLocationSupplier.Equals(other.IdLocationSupplier);
        }
    #endregion
    }
#endif
}