using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpAlarmStatus : DB_ALARM_STATUS, IComparable, IReferenceType, IEquatable<OpAlarmStatus>
    {
        #region Properties
        public int IdAlarmStatus { get { return this.ID_ALARM_STATUS; } set { this.ID_ALARM_STATUS = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpAlarmStatus()
            : base() { }

        public OpAlarmStatus(DB_ALARM_STATUS clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            if (this.Descr != null)
                return this.Descr.ToString();
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpAlarmStatus> ConvertList(DB_ALARM_STATUS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmStatus> ConvertList(DB_ALARM_STATUS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpAlarmStatus> ret = new List<OpAlarmStatus>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmStatus(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmStatus> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmStatus> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpAlarmStatus> Members
        public bool Equals(OpAlarmStatus other)
        {
            if (other == null)
                return false;
            return this.IdAlarmStatus.Equals(other.IdAlarmStatus);
        }
        #endregion

        #region Implementation of IReferenceType

        public object GetReferenceKey()
        {
            return this.IdAlarmStatus;
        }

        public object GetReferenceValue()
        {
            return this;
        }

        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmStatus)
                return this.IdAlarmStatus == ((OpAlarmStatus)obj).IdAlarmStatus;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmStatus left, OpAlarmStatus right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmStatus left, OpAlarmStatus right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmStatus.GetHashCode();
        }
        #endregion
    }
#endif
}