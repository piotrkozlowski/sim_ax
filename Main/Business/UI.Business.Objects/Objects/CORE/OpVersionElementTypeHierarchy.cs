using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionElementTypeHierarchy : DB_VERSION_ELEMENT_TYPE_HIERARCHY, IComparable, IEquatable<OpVersionElementTypeHierarchy>, IOpChangeState
    {
    #region Properties
        public int IdVersionElementTypeHierarchy { get { return this.ID_VERSION_ELEMENT_TYPE_HIERARCHY; } set { if (this.ID_VERSION_ELEMENT_TYPE_HIERARCHY != value) { this.ID_VERSION_ELEMENT_TYPE_HIERARCHY = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_TYPE_HIERARCHY, value, this); } } }
        public int? IdVersionElementTypeHierarchyParent { get { return this.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT; } set { if (this.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT != value) { this.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT, value, this); } } }
        public int IdVersionElementType { get { return this.ID_VERSION_ELEMENT_TYPE; } set { if (this.ID_VERSION_ELEMENT_TYPE != value) { this.ID_VERSION_ELEMENT_TYPE = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_TYPE, value, this); } } }
        public bool Required { get { return this.REQUIRED; } set { if (this.REQUIRED != value) { this.REQUIRED = OpDataUtil.SetNewValue(this.REQUIRED, value, this); } } }
        public string Hierarchy { get { return this.HIERARCHY; } set { if (this.HIERARCHY != value) { this.HIERARCHY = OpDataUtil.SetNewValue(this.HIERARCHY, value, this); } } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
		private OpVersionElementType _VersionElementType;
		public OpVersionElementType VersionElementType { get { return this._VersionElementType; } set { this._VersionElementType = value; this.ID_VERSION_ELEMENT_TYPE = (value == null)? 0 : (int)value.ID_VERSION_ELEMENT_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpVersionElementTypeHierarchy()
			:base() {}
		
		public OpVersionElementTypeHierarchy(DB_VERSION_ELEMENT_TYPE_HIERARCHY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
			return this.HIERARCHY;
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionElementTypeHierarchy> ConvertList(DB_VERSION_ELEMENT_TYPE_HIERARCHY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionElementTypeHierarchy> ConvertList(DB_VERSION_ELEMENT_TYPE_HIERARCHY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionElementTypeHierarchy> ret = new List<OpVersionElementTypeHierarchy>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionElementTypeHierarchy(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionElementTypeHierarchy> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpVersionElementType> VersionElementTypeDict = dataProvider.GetVersionElementType(list.Select(l => l.ID_VERSION_ELEMENT_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_ELEMENT_TYPE);

                foreach (var loop in list)
                {
                    loop.VersionElementType = VersionElementTypeDict.TryGetValue(loop.ID_VERSION_ELEMENT_TYPE);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionElementTypeHierarchy> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionElementTypeHierarchy)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionElementTypeHierarchy).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionElementTypeHierarchy> Members
        public bool Equals(OpVersionElementTypeHierarchy other)
        {
            if (other == null)
				return false;
			return this.IdVersionElementTypeHierarchy.Equals(other.IdVersionElementTypeHierarchy);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionElementTypeHierarchy)
				return this.IdVersionElementTypeHierarchy == ((OpVersionElementTypeHierarchy)obj).IdVersionElementTypeHierarchy;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionElementTypeHierarchy left, OpVersionElementTypeHierarchy right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionElementTypeHierarchy left, OpVersionElementTypeHierarchy right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionElementTypeHierarchy.GetHashCode();
		}
    #endregion
	}
#endif
}