﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMapping : DB_MAPPING, IComparable, IEquatable<OpMapping>
    {
    #region Properties
        public long IdMapping { get { return this.ID_MAPPING; } set { this.ID_MAPPING = value; } }
        public int IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { this.ID_REFERENCE_TYPE = value; } }
        public long IdSource { get { return this.ID_SOURCE; } set { this.ID_SOURCE = value; } }
        public long IdDestination { get { return this.ID_DESTINATION; } set { this.ID_DESTINATION = value; } }
        public int IdImrServer { get { return this.ID_IMR_SERVER; } set { this.ID_IMR_SERVER = value; } }
        public DateTime Timestamp { get { return this.TIMESTAMP; } set { this.TIMESTAMP = value; } }
    #endregion

    #region	Navigation Properties
        private OpReferenceType _ReferenceType;
        public OpReferenceType ReferenceType { get { return this._ReferenceType; } set { this._ReferenceType = value; this.ID_REFERENCE_TYPE = (value == null) ? 0 : (int)value.ID_REFERENCE_TYPE; } }
        private OpImrServer _ImrServer;
        public OpImrServer ImrServer { get { return this._ImrServer; } set { this._ImrServer = value; this.ID_IMR_SERVER = (value == null) ? 0 : (int)value.ID_IMR_SERVER; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpMapping()
            : base() { }

        public OpMapping(DB_MAPPING clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "Mapping [" + IdMapping.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpMapping> ConvertList(DB_MAPPING[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMapping> ConvertList(DB_MAPPING[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMapping> ret = new List<OpMapping>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMapping(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMapping> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpReferenceType> ReferenceTypeDict = dataProvider.GetReferenceType(list.Select(l => l.ID_REFERENCE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_REFERENCE_TYPE);
                Dictionary<int, OpImrServer> ImrServerDict = dataProvider.GetImrServer(list.Select(l => l.ID_IMR_SERVER).Distinct().ToArray(),false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_IMR_SERVER);

                foreach (var loop in list)
                {
                    loop.ReferenceType = ReferenceTypeDict.TryGetValue(loop.ID_REFERENCE_TYPE);
                    loop.ImrServer = ImrServerDict.TryGetValue(loop.ID_IMR_SERVER);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpMapping> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMapping)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMapping).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpMapping> Members
        public bool Equals(OpMapping other)
        {
            if (other == null)
                return false;
            return this.IdMapping.Equals(other.IdMapping);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMapping)
                return this.IdMapping == ((OpMapping)obj).IdMapping;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMapping left, OpMapping right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMapping left, OpMapping right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMapping.GetHashCode();
        }
    #endregion
    }
#endif
}