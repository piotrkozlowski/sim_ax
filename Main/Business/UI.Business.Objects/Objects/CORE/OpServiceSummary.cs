﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceSummary : DB_SERVICE_SUMMARY, IComparable, IEquatable<OpServiceSummary>
    {
    #region Properties
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public long IdServicePackage { get { return this.ID_SERVICE_PACKAGE; } set { this.ID_SERVICE_PACKAGE = value; } }
        public int Amount { get { return this.AMOUNT; } set { this.AMOUNT = value; } }
        public Double Cost { get { return this.COST; } set { this.COST = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? 0 : (int)value.ID_SERVICE_LIST; } }
        private OpServicePackage _ServicePackage;
        public OpServicePackage ServicePackage { get { return this._ServicePackage; } set { this._ServicePackage = value; this.ID_SERVICE_PACKAGE = (value == null) ? 0 : (long)value.ID_SERVICE_PACKAGE; } }
    #endregion

    #region	Custom Properties
        public string ServiceListSummary
        {
            get;
            private set;
        }

        public OpDataList<OpServiceSummaryData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpServiceSummary()
            : base() 
        {
            DataList = new OpDataList<OpServiceSummaryData>();
        }

        public OpServiceSummary(DB_SERVICE_SUMMARY clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpServiceSummaryData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceSummary [" + IdServiceList.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceSummary> ConvertList(DB_SERVICE_SUMMARY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceSummary> ConvertList(DB_SERVICE_SUMMARY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceSummary> ret = new List<OpServiceSummary>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceSummary(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceSummary> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpServicePackage> ServicePackageDict = dataProvider.GetServicePackage(list.Select(l => l.ID_SERVICE_PACKAGE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_PACKAGE);

                foreach (var loop in list)
                {
                    loop.ServicePackage = ServicePackageDict.TryGetValue(loop.ID_SERVICE_PACKAGE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceSummary> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            int currentListId = 0;
            double currentCost = 0.0;
            OpServiceList slItem = null;
            if (list != null && list.Count > 0)
            {
                slItem = dataProvider.GetServiceList(new int[]{ list[0].IdServiceList }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();
            }
            foreach (OpServiceSummary ssItem in list)
            {
                if (currentListId != ssItem.IdServiceList)
                {
                    currentListId = ssItem.IdServiceList;
                    currentCost = 0.0;
                    foreach (OpServiceSummary ssItemCurr in list)
                    {
                        currentCost += ssItemCurr.Cost * ssItemCurr.Amount;
                    }
                }
                ssItem.ServiceListSummary = (slItem == null ? "" : slItem.ServiceName + " - ") + currentCost + " PLN";
                ssItem.ServiceList = slItem;
            }
          //  OpServiceSummary suList = dataProvider.GetServiceSummaryFilter(IdServiceList: new int[]{ this.
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceSummary)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceSummary).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceSummary> Members
        public bool Equals(OpServiceSummary other)
        {
            if (other == null)
                return false;
            return this.IdServiceList.Equals(other.IdServiceList);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceSummary)
                return this.IdServiceList == ((OpServiceSummary)obj).IdServiceList;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceSummary left, OpServiceSummary right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceSummary left, OpServiceSummary right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceList.GetHashCode();
        }
    #endregion
    }
#endif
}