using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpOrderStateType : DB_ORDER_STATE_TYPE, IComparable, IEquatable<OpOrderStateType>
    {
    #region Properties
            public int IdOrderStateType { get { return this.ID_ORDER_STATE_TYPE;} set {this.ID_ORDER_STATE_TYPE = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
                public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpOrderStateType()
			:base() {}
		
		public OpOrderStateType(DB_ORDER_STATE_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpOrderStateType> ConvertList(DB_ORDER_STATE_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpOrderStateType> ConvertList(DB_ORDER_STATE_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpOrderStateType> ret = new List<OpOrderStateType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpOrderStateType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpOrderStateType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
			
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpOrderStateType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpOrderStateType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpOrderStateType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpOrderStateType> Members
        public bool Equals(OpOrderStateType other)
        {
            if (other == null)
				return false;
			return this.IdOrderStateType.Equals(other.IdOrderStateType);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpOrderStateType)
				return this.IdOrderStateType == ((OpOrderStateType)obj).IdOrderStateType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpOrderStateType left, OpOrderStateType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpOrderStateType left, OpOrderStateType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdOrderStateType.GetHashCode();
		}
    #endregion
    }
#endif
}