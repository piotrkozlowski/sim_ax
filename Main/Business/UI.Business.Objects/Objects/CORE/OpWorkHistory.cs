using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable]
    public class OpWorkHistory : DB_WORK_HISTORY, IComparable, IEquatable<OpWorkHistory>
    {
        #region Properties
        [XmlIgnore]
        public long IdWorkHistory { get { return this.ID_WORK_HISTORY; } set { this.ID_WORK_HISTORY = value; } }
        [XmlIgnore]
        public long IdWork { get { return this.ID_WORK; } set { this.ID_WORK = value; } }
        [XmlIgnore]
        public int IdWorkStatus { get { return this.ID_WORK_STATUS; } set { this.ID_WORK_STATUS = value; } }
        [XmlIgnore]
        public DateTime? StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        [XmlIgnore]
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public int? IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        [XmlIgnore]
        public string Assembly { get { return this.ASSEMBLY; } set { this.ASSEMBLY = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpWork _Work;
        [XmlIgnore]
        public OpWork Work { get { return this._Work; } set { this._Work = value; this.ID_WORK = (value == null) ? 0 : (long)value.ID_WORK; } }
        [DataMember]
        private OpWorkStatus _WorkStatus;
        [XmlIgnore]
        public OpWorkStatus WorkStatus { get { return this._WorkStatus; } set { this._WorkStatus = value; this.ID_WORK_STATUS = (value == null) ? 0 : (int)value.ID_WORK_STATUS; } }
        [DataMember]
        private OpDescr _Descr;
        [XmlIgnore]
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        [DataMember]
        private OpModule _Module;
        [XmlIgnore]
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null) ? null : (int?)value.ID_MODULE; } }
        #endregion

        #region	Custom Properties
        [DataMember]
        public List<OpWorkHistoryData> DataList { get; set; }
        #endregion

        #region	Ctor
        public OpWorkHistory()
            : base() { }

        public OpWorkHistory(DB_WORK_HISTORY clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.ASSEMBLY;
        }
        #endregion

        #region	ConvertList
        public static List<OpWorkHistory> ConvertList(DB_WORK_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpWorkHistory> ConvertList(DB_WORK_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpWorkHistory> ret = new List<OpWorkHistory>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpWorkHistory(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpWorkHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpWork> WorkDict = dataProvider.GetWork(list.Select(l => l.ID_WORK).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_WORK);
                Dictionary<int, OpWorkStatus> WorkStatusDict = dataProvider.GetWorkStatus(list.Select(l => l.ID_WORK_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_WORK_STATUS);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_MODULE);

                foreach (var loop in list)
                {
                    loop.Work = WorkDict.TryGetValue(loop.ID_WORK);
                    loop.WorkStatus = WorkStatusDict.TryGetValue(loop.ID_WORK_STATUS);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    if (loop.ID_MODULE.HasValue)
                        loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpWorkHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<OpWorkHistoryData> data = dataProvider.GetWorkHistoryDataFilter(IdWorkHistory: list.Select(l => l.IdWorkHistory).ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                Dictionary<long, OpWorkHistory> workHistoryDict = list.ToDictionary(l => l.IdWorkHistory);

                foreach (var dataValue in data)
                {
                    workHistoryDict[dataValue.IdWorkHistory].DataList.Add(dataValue);
                }
            }
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpWorkHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpWorkHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpWorkHistory> Members
        public bool Equals(OpWorkHistory other)
        {
            if (other == null)
                return false;
            return this.IdWorkHistory.Equals(other.IdWorkHistory);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpWorkHistory)
                return this.IdWorkHistory == ((OpWorkHistory)obj).IdWorkHistory;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpWorkHistory left, OpWorkHistory right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpWorkHistory left, OpWorkHistory right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdWorkHistory.GetHashCode();
        }
        #endregion
    }
}