﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpIssueGroup : DB_ISSUE_GROUP, IComparable, IReferenceType
    {
        #region Properties
        public int IdIssueGroup { get { return this.ID_ISSUE_GROUP; } set { this.ID_ISSUE_GROUP = value; } }
        public int? IdParentGroup { get { return this.ID_PARENT_GROUP; } set { this.ID_PARENT_GROUP = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public DateTime DateCreated { get { return this.DATE_CREATED; } set { this.DATE_CREATED = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        #endregion

        #region	Navigation Properties
        private OpIssueGroup _ParentGroup;
        public OpIssueGroup ParentGroup { get { return this._ParentGroup; } set { this._ParentGroup = value; this.ID_PARENT_GROUP = (value == null) ? null : (int?)value.ID_ISSUE_GROUP; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpIssueGroup()
            : base() { }

        public OpIssueGroup(DB_ISSUE_GROUP clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpIssueGroup> ConvertList(DB_ISSUE_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpIssueGroup> ConvertList(DB_ISSUE_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpIssueGroup> ret = new List<OpIssueGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpIssueGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpIssueGroup> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpIssueGroup> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpIssueGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpIssueGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpIssueGroup)
                return this.IdIssueGroup == ((OpIssueGroup)obj).IdIssueGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpIssueGroup left, OpIssueGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpIssueGroup left, OpIssueGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdIssueGroup.GetHashCode();
        }
        #endregion
        
        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdIssueGroup;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion
    }
#endif
}