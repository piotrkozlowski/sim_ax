using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpIssueType : DB_ISSUE_TYPE, IComparable, IReferenceType, IEquatable<OpIssueType>
    {
    #region Properties
        public int IdIssueType { get { return this.ID_ISSUE_TYPE; } set { this.ID_ISSUE_TYPE = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpIssueType()
            : base() { }

        public OpIssueType(DB_ISSUE_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.DESCRIPTION;
        }
    #endregion

    #region	ConvertList
        public static List<OpIssueType> ConvertList(DB_ISSUE_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpIssueType> ConvertList(DB_ISSUE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpIssueType> ret = new List<OpIssueType>(list.Length);
            foreach (var loop in list)
            {
                OpIssueType insert = new OpIssueType(loop);

                if (loadNavigationProperties)
                {

                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[]{ loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();


                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpIssueType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpIssueType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpIssueType).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Enum
        [Serializable]
        public enum Enum
        {
            EXPRESS = 1,
            HARMO = 2,
            OTHER = 3,
            VALVE_UNLOCK = 4,
            MONTAGE = 5,
            REFUEL_AUDIT = 6,
            DELIVERY = 7,
            INFRASTRUCTURE = 8,
            SERVER = 9,
            REPORT = 10,
            CLIENT_PORTAL = 11,
            HARDWARE = 12,
            MAINTENANCE = 13,
            SITA_DIAGNOSTIC = 14,
            TRANSMISSION_DRIVER = 15,
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdIssueType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region IEquatable<OpIssueType>

        public bool Equals(OpIssueType obj)
        {
            if (obj is OpIssueType)
                return this.IdIssueType == ((OpIssueType)obj).IdIssueType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpIssueType left, OpIssueType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpIssueType left, OpIssueType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdIssueType.GetHashCode();
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpIssueType)
                return this.IdIssueType == ((OpIssueType)obj).IdIssueType;
            else
                return base.Equals(obj);
        }
    #endregion
    }
#endif
}