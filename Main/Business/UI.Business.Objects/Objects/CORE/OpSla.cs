using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpSla : DB_SLA, IComparable, IEquatable<OpSla>
    {
    #region Properties
            public int IdSla { get { return this.ID_SLA;} set {this.ID_SLA = value;}}
                public int IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
                public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
                public string IssueReceiveWd { get { return this.ISSUE_RECEIVE_WD;} set {this.ISSUE_RECEIVE_WD = value;}}
                public string IssueReceiveFd { get { return this.ISSUE_RECEIVE_FD;} set {this.ISSUE_RECEIVE_FD = value;}}
                public string TechnicalSupportWd { get { return this.TECHNICAL_SUPPORT_WD;} set {this.TECHNICAL_SUPPORT_WD = value;}}
                public string TechnicalSupportFd { get { return this.TECHNICAL_SUPPORT_FD;} set {this.TECHNICAL_SUPPORT_FD = value;}}
                public int? IssueResponseWd { get { return this.ISSUE_RESPONSE_WD;} set {this.ISSUE_RESPONSE_WD = value;}}
                public int? IssueResponseFd { get { return this.ISSUE_RESPONSE_FD;} set {this.ISSUE_RESPONSE_FD = value;}}
                public int? SystemFailureRemoveWd { get { return this.SYSTEM_FAILURE_REMOVE_WD;} set {this.SYSTEM_FAILURE_REMOVE_WD = value;}}
                public int? SystemFailureRemoveFd { get { return this.SYSTEM_FAILURE_REMOVE_FD;} set {this.SYSTEM_FAILURE_REMOVE_FD = value;}}
                public int? ObjectFailureRemoveWd { get { return this.OBJECT_FAILURE_REMOVE_WD;} set {this.OBJECT_FAILURE_REMOVE_WD = value;}}
                public int? ObjectFailureRemoveFd { get { return this.OBJECT_FAILURE_REMOVE_FD;} set {this.OBJECT_FAILURE_REMOVE_FD = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? 0 : (int)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpSla()
			:base() {}
		
		public OpSla(DB_SLA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            if (Descr != null)
                return Descr.Description;
            return this.DESCRIPTION;
		}
    #endregion

    #region	ConvertList
		public static List<OpSla> ConvertList(DB_SLA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpSla> ConvertList(DB_SLA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSla> ret = new List<OpSla>(list.Length);
			foreach (var loop in list)
			{
			OpSla insert = new OpSla(loop);
				
				if(loadNavigationProperties)
				{
											
																									insert.Descr = dataProvider.GetDescr(loop.ID_DESCR); 
													
											
											
											
											
											
											
											
											
											
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSla> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSla)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSla).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpSla> Members
        public bool Equals(OpSla other)
        {
            if (other == null)
				return false;
			return this.IdSla.Equals(other.IdSla);
        }
    #endregion
    }
#endif
}