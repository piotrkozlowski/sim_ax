using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerSettlementStatus : DB_CONSUMER_SETTLEMENT_STATUS, IComparable, IEquatable<OpConsumerSettlementStatus>
    {
    #region Properties
        public int IdConsumerSettlementStatus { get { return this.ID_CONSUMER_SETTLEMENT_STATUS;} set {this.ID_CONSUMER_SETTLEMENT_STATUS = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public bool IsError { get { return this.IS_ERROR;} set {this.IS_ERROR = value;}}
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerSettlementStatus()
			:base() {}
		
		public OpConsumerSettlementStatus(DB_CONSUMER_SETTLEMENT_STATUS clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
        public static List<OpConsumerSettlementStatus> ConvertList(DB_CONSUMER_SETTLEMENT_STATUS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpConsumerSettlementStatus> ConvertList(DB_CONSUMER_SETTLEMENT_STATUS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpConsumerSettlementStatus> ret = new List<OpConsumerSettlementStatus>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerSettlementStatus(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion
		
    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConsumerSettlementStatus> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConsumerSettlementStatus> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerSettlementStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerSettlementStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerSettlementStatus> Members
        public bool Equals(OpConsumerSettlementStatus other)
        {
            if (other == null)
				return false;
			return this.IdConsumerSettlementStatus.Equals(other.IdConsumerSettlementStatus);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerSettlementStatus)
				return this.IdConsumerSettlementStatus == ((OpConsumerSettlementStatus)obj).IdConsumerSettlementStatus;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerSettlementStatus left, OpConsumerSettlementStatus right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerSettlementStatus left, OpConsumerSettlementStatus right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerSettlementStatus.GetHashCode();
		}
    #endregion
    }
#endif
}