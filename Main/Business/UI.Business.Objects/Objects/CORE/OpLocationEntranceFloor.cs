using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpLocationEntranceFloor : DB_LOCATION_ENTRANCE_FLOOR, IComparable, IEquatable<OpLocationEntranceFloor>
    {
    #region Properties
        public long IdLocation { get { return this.ID_LOCATION;} set {this.ID_LOCATION = value;}}
        public int EntranceIndexNbr { get { return this.ENTRANCE_INDEX_NBR;} set {this.ENTRANCE_INDEX_NBR = value;}}
        public int FloorIndexNbr { get { return this.FLOOR_INDEX_NBR;} set {this.FLOOR_INDEX_NBR = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
    #endregion

    #region	Navigation Properties
		private OpLocation _Location;
				public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null)? 0 : (long)value.ID_LOCATION; } }
    #endregion

    #region	Custom Properties
                public string UniqueFloorId
                {
                    get
                    {
                        return String.Format("entrance_{0}_floor{1}", this.EntranceIndexNbr, this.FloorIndexNbr);
                    }
                }
    #endregion
		
    #region	Ctor
		public OpLocationEntranceFloor()
			:base() {}
		
		public OpLocationEntranceFloor(DB_LOCATION_ENTRANCE_FLOOR clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpLocationEntranceFloor> ConvertList(DB_LOCATION_ENTRANCE_FLOOR[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpLocationEntranceFloor> ConvertList(DB_LOCATION_ENTRANCE_FLOOR[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpLocationEntranceFloor> ret = new List<OpLocationEntranceFloor>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpLocationEntranceFloor(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpLocationEntranceFloor> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpLocationEntranceFloor> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationEntranceFloor)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationEntranceFloor).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpLocationEntranceFloor> Members
        public bool Equals(OpLocationEntranceFloor other)
        {
            if (other == null)
				return false;
			return this.IdLocation.Equals(other.IdLocation);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpLocationEntranceFloor)
				return this.IdLocation == ((OpLocationEntranceFloor)obj).IdLocation;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpLocationEntranceFloor left, OpLocationEntranceFloor right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpLocationEntranceFloor left, OpLocationEntranceFloor right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdLocation.GetHashCode();
		}
    #endregion
    }
#endif
}