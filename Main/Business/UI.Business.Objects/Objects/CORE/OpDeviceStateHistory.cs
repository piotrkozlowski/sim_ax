using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceStateHistory : DB_DEVICE_STATE_HISTORY, IOpChangeState, IComparable
    {
    #region Properties
        [XmlIgnore]
        public long IdDeviceState { get { return this.ID_DEVICE_STATE; } set { this.ID_DEVICE_STATE = value; } }
        [XmlIgnore]
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
		public int IdDeviceStateType
		{
			get { return this.ID_DEVICE_STATE_TYPE; }
			set { if (this.ID_DEVICE_STATE_TYPE != value) { this.ID_DEVICE_STATE_TYPE = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
		public DateTime StartTime
		{
			get { return this.START_TIME; }
			set { if (this.START_TIME != value) { this.START_TIME = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
		public DateTime? EndTime
		{
			get { return this.END_TIME; }
			set { if (this.END_TIME != value) { this.END_TIME = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
		public string Notes
		{
			get { return this.NOTES; }
			set { if (this.NOTES != value) { this.NOTES = value; this.OpState = OpChangeState.Modified; } }
		}
    #endregion

    #region Navigation Properties
        private OpDeviceStateType _DeviceStateType;
        [XmlIgnore]
        public OpDeviceStateType DeviceStateType { get { return this._DeviceStateType; } set { this._DeviceStateType = value; this.ID_DEVICE_STATE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_STATE_TYPE; } }

        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? 0 : (int)value.SERIAL_NBR; } }
    #endregion

    #region Custom Properties
		public OpChangeState OpState { get; set; }
    #endregion

    #region Ctor
        public OpDeviceStateHistory()
			: base() { this.OpState = OpChangeState.New; }

        public OpDeviceStateHistory(DB_DEVICE_STATE_HISTORY clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region ConvertList
        public static List<OpDeviceStateHistory> ConvertList(DB_DEVICE_STATE_HISTORY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDeviceStateHistory> ConvertList(DB_DEVICE_STATE_HISTORY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceStateHistory> ret = new List<OpDeviceStateHistory>(list.Length);
            foreach (var loop in list)
            {
                OpDeviceStateHistory insert = new OpDeviceStateHistory(loop);

                if (loadNavigationProperties)
                {
                    insert.DeviceStateType = dataProvider.GetDeviceStateType(loop.ID_DEVICE_STATE_TYPE);
                    insert.Device = dataProvider.GetDevice(loop.SERIAL_NBR);

                    if (insert.Device != null && insert.EndTime == null)
                        insert.Device.DeviceStateType = insert.DeviceStateType;
                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceStateHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region AssignReferences
		public void AssignReferences(IDataProvider dataProvider)
		{
			this.Device = dataProvider.GetDevice(this.SERIAL_NBR);
			this.DeviceStateType = dataProvider.GetDeviceStateType(this.ID_DEVICE_STATE_TYPE);
		}
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceStateHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceStateHistory).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion
    }
#endif
}