using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpActionSmsText : DB_ACTION_SMS_TEXT, IComparable, IEquatable<OpActionSmsText>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public long IdActionSmsText { get { return this.ID_ACTION_SMS_TEXT;} set {this.ID_ACTION_SMS_TEXT = value;}}
        [XmlIgnore]
        public int IdLanguage { get { return this.ID_LANGUAGE;} set {this.ID_LANGUAGE = value;}}
        [XmlIgnore]
        public string SmsText { get { return this.SMS_TEXT;} set {this.SMS_TEXT = value;}}
        [XmlIgnore]
        public long? IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
        [XmlIgnore]
        public bool IsIncoming { get { return this.IS_INCOMING;} set {this.IS_INCOMING = value;}}
        [XmlIgnore]
        public int? IdTransmissionType { get { return this.ID_TRANSMISSION_TYPE;} set {this.ID_TRANSMISSION_TYPE = value;}}
    #endregion

    #region	Navigation Properties
		private OpLanguage _Language;
        [XmlIgnore]
		public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null)? 0 : (int)value.ID_LANGUAGE; } }
		private OpDataType _DataType;
        [XmlIgnore]
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? null : (long?)value.ID_DATA_TYPE; } }
		private OpTransmissionType _TransmissionType;
        [XmlIgnore]
		public OpTransmissionType TransmissionType { get { return this._TransmissionType; } set { this._TransmissionType = value; this.ID_TRANSMISSION_TYPE = (value == null)? null : (int?)value.ID_TRANSMISSION_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpActionSmsText()
			:base() {}
		
		public OpActionSmsText(DB_ACTION_SMS_TEXT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.SMS_TEXT;
		}
    #endregion

    #region	ConvertList
		public static List<OpActionSmsText> ConvertList(DB_ACTION_SMS_TEXT[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpActionSmsText> ConvertList(DB_ACTION_SMS_TEXT[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpActionSmsText> ret = new List<OpActionSmsText>(list.Length);
			foreach (var loop in list)
			{
				OpActionSmsText insert = new OpActionSmsText(loop);
				ret.Add(insert);
			}
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpActionSmsText> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray()).ToDictionary(l => l.ID_LANGUAGE);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Where(l => l.ID_DATA_TYPE.HasValue).Select(l => l.ID_DATA_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
	Dictionary<int,OpTransmissionType> TransmissionTypeDict = dataProvider.GetTransmissionType(list.Where(l => l.ID_TRANSMISSION_TYPE.HasValue).Select(l => l.ID_TRANSMISSION_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_TYPE);			
				foreach (var loop in list)
				{
						loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE); 
		if (loop.ID_DATA_TYPE.HasValue)
						loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE.Value); 
		if (loop.ID_TRANSMISSION_TYPE.HasValue)
						loop.TransmissionType = TransmissionTypeDict.TryGetValue(loop.ID_TRANSMISSION_TYPE.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpActionSmsText> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionSmsText)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionSmsText).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpActionSmsText> Members
        public bool Equals(OpActionSmsText other)
        {
            if (other == null)
				return false;
			return this.IdActionSmsText.Equals(other.IdActionSmsText);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpActionSmsText)
				return this.IdActionSmsText == ((OpActionSmsText)obj).IdActionSmsText;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpActionSmsText left, OpActionSmsText right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpActionSmsText left, OpActionSmsText right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdActionSmsText.GetHashCode();
		}
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdActionSmsText;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    }
#endif
}