using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpLocationEntrance : DB_LOCATION_ENTRANCE, IComparable, IEquatable<OpLocationEntrance>
    {
    #region Properties
        public long IdLocation { get { return this.ID_LOCATION;} set {this.ID_LOCATION = value;}}
        public int EntranceIndexNbr { get { return this.ENTRANCE_INDEX_NBR;} set {this.ENTRANCE_INDEX_NBR = value;}}
        public int SectionCount { get { return this.SECTION_COUNT;} set {this.SECTION_COUNT = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public object FloorPlan { get { return this.FLOOR_PLAN;} set {this.FLOOR_PLAN = value;}}
    #endregion

    #region	Navigation Properties
		private OpLocation _Location;
				public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null)? 0 : (long)value.ID_LOCATION; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpLocationEntrance()
			:base() {}
		
		public OpLocationEntrance(DB_LOCATION_ENTRANCE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpLocationEntrance> ConvertList(DB_LOCATION_ENTRANCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpLocationEntrance> ConvertList(DB_LOCATION_ENTRANCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpLocationEntrance> ret = new List<OpLocationEntrance>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpLocationEntrance(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpLocationEntrance> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpLocationEntrance> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationEntrance)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationEntrance).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpLocationEntrance> Members
        public bool Equals(OpLocationEntrance other)
        {
            if (other == null)
				return false;
			return this.IdLocation.Equals(other.IdLocation);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpLocationEntrance)
				return this.IdLocation == ((OpLocationEntrance)obj).IdLocation;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpLocationEntrance left, OpLocationEntrance right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpLocationEntrance left, OpLocationEntrance right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdLocation.GetHashCode();
		}
    #endregion
    }
#endif
}