using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable]
	public class OpWork : DB_WORK, IComparable, IEquatable<OpWork>
    {
		#region Properties
        public long IdWork { get { return this.ID_WORK;} set {this.ID_WORK = value;}}
        public long IdWorkType { get { return this.ID_WORK_TYPE;} set {this.ID_WORK_TYPE= value;}}
        public int IdWorkStatus { get { return this.ID_WORK_STATUS;} set {this.ID_WORK_STATUS = value;}}
        public long? IdWorkData { get { return this.ID_WORK_DATA;} set {this.ID_WORK_DATA = value;}}
        public long? IdWorkParent { get { return this.ID_WORK_PARENT;} set {this.ID_WORK_PARENT = value;}}
        public int? IdModule { get { return this.ID_MODULE;} set {this.ID_MODULE = value;}}
        public int? IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
        public DateTime CreationDate { get { return this.CREATION_DATE;} set {this.CREATION_DATE = value;}}
		#endregion

		#region	Navigation Properties
		private OpWorkStatus _WorkStatus;
        public OpWorkStatus WorkStatus { get { return this._WorkStatus; } set { this._WorkStatus = value; this.ID_WORK_STATUS = (value == null) ? 0 : (int)value.ID_WORK_STATUS; } }
        private OpWorkType _WorkType;
        public OpWorkType WorkType { get { return this._WorkType; } set { this._WorkType = value; this.ID_WORK_TYPE = (value == null) ? 0 : (int)value.ID_WORK_TYPE; } }
		private OpWorkData _WorkData;
		public OpWorkData WorkData { get { return this._WorkData; } set { this._WorkData = value; this.ID_WORK_DATA = (value == null)? 0 : (long)value.ID_WORK_DATA; } }
		private OpModule _Module;
		public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null)? null : (int?)value.ID_MODULE; } }
		private OpOperator _Operator;
		public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? null : (int?)value.ID_OPERATOR; } }
		#endregion

		#region	Custom Properties
		#endregion
		
		#region	Ctor
		public OpWork()
			:base() {}
		
		public OpWork(DB_WORK clone)
			:base(clone) {}
		#endregion
		
		#region	ToString
		public override string ToString()
		{
            return string.Concat("Work [", IdWork.ToString(),  "]");
        }
		#endregion

		#region	ConvertList
		public static List<OpWork> ConvertList(DB_WORK[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpWork> ConvertList(DB_WORK[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpWork> ret = new List<OpWork>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpWork(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
		#endregion
		
		#region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpWork> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<long, OpWorkType> WorkTypeDict = dataProvider.GetWorkType(list.Select(l => l.ID_WORK_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_WORK_TYPE);
                Dictionary<int, OpWorkStatus> WorkStatusDict = dataProvider.GetWorkStatus(list.Select(l => l.ID_WORK_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_WORK_STATUS);
	            //Dictionary<long, OpWorkData> WorkDataDict = dataProvider.GetWorkData(list.Select(l => l.ID_WORK_DATA).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_WORK_DATA);
	            Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_MODULE);
	            Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
			
				foreach (var loop in list)
				{
                    loop.WorkType = WorkTypeDict.TryGetValue(loop.ID_WORK_TYPE);
                    loop.WorkStatus = WorkStatusDict.TryGetValue(loop.ID_WORK_STATUS); 
		            //loop.WorkData = WorkDataDict.TryGetValue(loop.ID_WORK_DATA); 
		            if (loop.ID_MODULE.HasValue)
						loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value); 
		            if (loop.ID_OPERATOR.HasValue)
						loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value); 
				}
			}
		}
		#endregion
		
		#region LoadCustomData
		private static void LoadCustomData(ref List<OpWork> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
		#endregion
		
		#region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpWork)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpWork).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
		#endregion
		
		#region IEquatable<OpWork> Members
        public bool Equals(OpWork other)
        {
            if (other == null)
				return false;
			return this.IdWork.Equals(other.IdWork);
        }
        #endregion

		#region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpWork)
				return this.IdWork == ((OpWork)obj).IdWork;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpWork left, OpWork right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpWork left, OpWork right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdWork.GetHashCode();
		}
		#endregion
    }
}