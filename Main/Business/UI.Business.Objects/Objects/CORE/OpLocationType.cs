using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLocationType : DB_LOCATION_TYPE, IComparable, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdLocationType { get { return this.ID_LOCATION_TYPE; } set { this.ID_LOCATION_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        [XmlIgnore]
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpLocationType()
            : base() { }

        public OpLocationType(DB_LOCATION_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationType> ConvertList(DB_LOCATION_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLocationType> ConvertList(DB_LOCATION_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpLocationType> ret = new List<OpLocationType>(list.Length);
            foreach (var loop in list)
            {
                OpLocationType insert = new OpLocationType(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationType).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IReferenceType Members
        public object GetReferenceKey()
        {
            return this.IdLocationType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion


    #region Enum
        [Serializable]
        public enum Enum
        {
            CUSTOMER = 1,
            DEPOSITORY = 2,
            SERVICE = 3,
            GROUPING = 4,
            RELATED = 5,
            DRAFT = 6,
            UNIDENTIFIED = 7
        }
    #endregion
    }
#endif
}