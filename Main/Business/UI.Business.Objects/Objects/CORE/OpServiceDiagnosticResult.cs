﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceDiagnosticResult : DB_SERVICE_DIAGNOSTIC_RESULT, IComparable, IEquatable<OpServiceDiagnosticResult>
    {
    #region Properties
        public int IdServiceDiagnosticResult { get { return this.ID_SERVICE_DIAGNOSTIC_RESULT; } set { this.ID_SERVICE_DIAGNOSTIC_RESULT = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public bool? IsServiceResult { get { return this.IS_SERVICE_RESULT; } set { this.IS_SERVICE_RESULT = value; } }
        public bool? IsDiagnosticResult { get { return this.IS_DIAGNOSTIC_RESULT; } set { this.IS_DIAGNOSTIC_RESULT = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpServiceDiagnosticResult()
            : base() { }

        public OpServiceDiagnosticResult(DB_SERVICE_DIAGNOSTIC_RESULT clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (this.Descr != null && !String.IsNullOrEmpty(this.Descr.ToString()))
                return this.Descr.ToString();
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceDiagnosticResult> ConvertList(DB_SERVICE_DIAGNOSTIC_RESULT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceDiagnosticResult> ConvertList(DB_SERVICE_DIAGNOSTIC_RESULT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceDiagnosticResult> ret = new List<OpServiceDiagnosticResult>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceDiagnosticResult(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceDiagnosticResult> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);

                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceDiagnosticResult> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceDiagnosticResult)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceDiagnosticResult).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceDiagnosticResult> Members
        public bool Equals(OpServiceDiagnosticResult other)
        {
            if (other == null)
                return false;
            return this.IdServiceDiagnosticResult.Equals(other.IdServiceDiagnosticResult);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceDiagnosticResult)
                return this.IdServiceDiagnosticResult == ((OpServiceDiagnosticResult)obj).IdServiceDiagnosticResult;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceDiagnosticResult left, OpServiceDiagnosticResult right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceDiagnosticResult left, OpServiceDiagnosticResult right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceDiagnosticResult.GetHashCode();
        }
    #endregion
    }
#endif
}