﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDiagnosticActionData : DB_DIAGNOSTIC_ACTION_DATA, IComparable, IEquatable<OpDiagnosticActionData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdDiagnosticActionData { get { return this.ID_DIAGNOSTIC_ACTION_DATA; } set { this.ID_DIAGNOSTIC_ACTION_DATA = value; } }
        public int IdDiagnosticAction { get { return this.ID_DIAGNOSTIC_ACTION; } set { this.ID_DIAGNOSTIC_ACTION = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }

        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpDiagnosticAction _DiagnosticAction;
        public OpDiagnosticAction DiagnosticAction { get { return this._DiagnosticAction; } set { this._DiagnosticAction = value; this.ID_DIAGNOSTIC_ACTION = (value == null) ? 0 : (int)value.ID_DIAGNOSTIC_ACTION; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpDiagnosticActionData()
            : base() { }

        public OpDiagnosticActionData(DB_DIAGNOSTIC_ACTION_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DiagnosticActionData [" + IdDiagnosticActionData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDiagnosticActionData> ConvertList(DB_DIAGNOSTIC_ACTION_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDiagnosticActionData> ConvertList(DB_DIAGNOSTIC_ACTION_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDiagnosticActionData> ret = new List<OpDiagnosticActionData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDiagnosticActionData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDiagnosticActionData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
               // Dictionary<int, OpDiagnosticAction> DiagnosticActionDict = dataProvider.GetDiagnosticAction(list.Select(l => l.ID_DIAGNOSTIC_ACTION).Distinct().ToArray()).ToDictionary(l => l.ID_DIAGNOSTIC_ACTION);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                //    loop.DiagnosticAction = DiagnosticActionDict.TryGetValue(loop.ID_DIAGNOSTIC_ACTION);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDiagnosticActionData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDiagnosticActionData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDiagnosticActionData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDiagnosticActionData> Members
        public bool Equals(OpDiagnosticActionData other)
        {
            if (other == null)
                return false;
            return this.IdDiagnosticActionData.Equals(other.IdDiagnosticActionData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDiagnosticActionData)
                return this.IdDiagnosticActionData == ((OpDiagnosticActionData)obj).IdDiagnosticActionData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDiagnosticActionData left, OpDiagnosticActionData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDiagnosticActionData left, OpDiagnosticActionData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDiagnosticActionData.GetHashCode();
        }
    #endregion


    #region IOpData

        public long IdData { get { return IdDiagnosticActionData; } set { IdDiagnosticActionData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpDiagnosticActionData clone = new OpDiagnosticActionData();
            clone.IdData = this.IdData;
            clone.IdDiagnosticAction = this.IdDiagnosticAction;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_DIAGNOSTIC_ACTION_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}