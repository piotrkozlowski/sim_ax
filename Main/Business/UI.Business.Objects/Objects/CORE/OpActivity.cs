using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpActivity : DB_ACTIVITY, IComparable, IOpChangeState, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdActivity { get { return this.ID_ACTIVITY; } set { if (this.ID_ACTIVITY != value) { this.ID_ACTIVITY = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { if (this.ID_REFERENCE_TYPE != value) { this.ID_REFERENCE_TYPE = value; this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region Navigation Properties
        [XmlIgnore, DataMember]
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        [XmlIgnore, DataMember]
        private OpReferenceType _ReferenceType;
        public OpReferenceType ReferenceType { get { return this._ReferenceType; } set { this._ReferenceType = value; this.IdReferenceType = (value == null) ? null : (int?)value.IdReferenceType; } }
        #endregion

        #region Custom Properties
        [XmlIgnore, DataMember]
        public int? IdModule { get; set; }
        [XmlIgnore, DataMember]
        public OpChangeState OpState { get; set; }
        [XmlIgnore, DataMember]
        public object ReferenceValue { get; set; }
        [XmlIgnore, DataMember]
        public bool Deny { get; set; }
        [XmlIgnore]
        public bool Allow { get { return !Deny; } set { Deny = !value; } }
        [DataMember]
        public long? IdRoleActivity { get; set; }
        [DataMember]
        public long? IdOperatorActivity { get; set; }
        #endregion

        #region Ctor
        public OpActivity()
            : base()
        {
            OpState = OpChangeState.New;
        }

        public OpActivity(DB_ACTIVITY clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            return this.NAME;
        }
    #endregion

    #region ConvertList
        public static List<OpActivity> ConvertList(DB_ACTIVITY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpActivity> ConvertList(DB_ACTIVITY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActivity> ret = new List<OpActivity>(list.Length);
            list.ToList().ForEach(db_object => ret.Add(new OpActivity(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActivity> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpReferenceType> ReferenceTypeDict = dataProvider.GetReferenceType(list.Where(l => l.ID_REFERENCE_TYPE.HasValue).Select(l => l.ID_REFERENCE_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_REFERENCE_TYPE);

                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    if (loop.ID_REFERENCE_TYPE.HasValue)
                        loop.ReferenceType = ReferenceTypeDict.TryGetValue(loop.ID_REFERENCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActivity> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public OpActivity Clone(IDataProvider dataProvider)
        {
            OpActivity clone = new OpActivity();

            clone.IdActivity = this.IdActivity;
            clone.Name = this.Name;
            clone.IdDescr = this.IdDescr;
            clone.IdReferenceType = this.IdReferenceType;
            clone.ReferenceValue = this.ReferenceValue;
            clone.Deny = this.Deny;

            if (dataProvider != null)
                clone = ConvertList(new DB_ACTIVITY[] { clone }, dataProvider)[0];

            return clone;
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActivity)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActivity).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActivity)
                return this.IdActivity == ((OpActivity)obj).IdActivity;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActivity left, OpActivity right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActivity left, OpActivity right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActivity.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdActivity;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}