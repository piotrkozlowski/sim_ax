using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerPaymentModule : DB_CONSUMER_PAYMENT_MODULE, IComparable, IEquatable<OpConsumerPaymentModule>
    {
    #region Properties
        public long IdConsumerPaymentModule { get { return this.ID_CONSUMER_PAYMENT_MODULE;} set {this.ID_CONSUMER_PAYMENT_MODULE = value;}}
        public int IdConsumer { get { return this.ID_CONSUMER;} set {this.ID_CONSUMER = value;}}
        public int IdPaymentModule { get { return this.ID_PAYMENT_MODULE;} set {this.ID_PAYMENT_MODULE = value;}}
        public string PaymentSystemNumber { get { return this.PAYMENT_SYSTEM_NUMBER;} set {this.PAYMENT_SYSTEM_NUMBER = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumer _Consumer;
		public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null)? 0 : (int)value.ID_CONSUMER; } }
		private OpPaymentModule _PaymentModule;
		public OpPaymentModule PaymentModule { get { return this._PaymentModule; } set { this._PaymentModule = value; this.ID_PAYMENT_MODULE = (value == null)? 0 : (int)value.ID_PAYMENT_MODULE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerPaymentModule()
			:base() 
        {
        }
		
		public OpConsumerPaymentModule(DB_CONSUMER_PAYMENT_MODULE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.PAYMENT_SYSTEM_NUMBER;
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerPaymentModule> ConvertList(DB_CONSUMER_PAYMENT_MODULE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerPaymentModule> ConvertList(DB_CONSUMER_PAYMENT_MODULE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpConsumerPaymentModule> ret = new List<OpConsumerPaymentModule>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerPaymentModule(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerPaymentModule> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Select(l => l.ID_CONSUMER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER);
	Dictionary<int,OpPaymentModule> PaymentModuleDict = dataProvider.GetPaymentModule(list.Select(l => l.ID_PAYMENT_MODULE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PAYMENT_MODULE);
			
				foreach (var loop in list)
				{
						loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER); 
		loop.PaymentModule = PaymentModuleDict.TryGetValue(loop.ID_PAYMENT_MODULE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerPaymentModule> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerPaymentModule)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerPaymentModule).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerPaymentModule> Members
        public bool Equals(OpConsumerPaymentModule other)
        {
            if (other == null)
				return false;
			return this.IdConsumerPaymentModule.Equals(other.IdConsumerPaymentModule);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerPaymentModule)
				return this.IdConsumerPaymentModule == ((OpConsumerPaymentModule)obj).IdConsumerPaymentModule;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerPaymentModule left, OpConsumerPaymentModule right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerPaymentModule left, OpConsumerPaymentModule right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerPaymentModule.GetHashCode();
		}
    #endregion
    }
#endif
}