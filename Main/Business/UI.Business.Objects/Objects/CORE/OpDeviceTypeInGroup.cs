using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTypeInGroup : DB_DEVICE_TYPE_IN_GROUP, IComparable, IOpChangeState
    {
    #region Properties
        public int IdDeviceTypeGroup { get { return this.ID_DEVICE_TYPE_GROUP; } set { this.ID_DEVICE_TYPE_GROUP = OpDataUtil.SetNewIndex(this.ID_DEVICE_TYPE_GROUP, value, this); } }
            public int IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = OpDataUtil.SetNewIndex(this.ID_DEVICE_TYPE, value, this);} }
    #endregion

    #region	Navigation Properties
		private OpDeviceTypeGroup _DeviceTypeGroup;
        public OpDeviceTypeGroup DeviceTypeGroup { get { return this._DeviceTypeGroup; } set { this._DeviceTypeGroup = value; this.IdDeviceTypeGroup = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE_GROUP; } }
		private OpDeviceType _DeviceType;
        public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.IdDeviceType = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpDeviceTypeInGroup()
			:base() 
        {
            OpState = OpChangeState.New;
        }
		
		public OpDeviceTypeInGroup(DB_DEVICE_TYPE_IN_GROUP clone)
			:base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "DeviceTypeInGroup [" + IdDeviceTypeGroup.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpDeviceTypeInGroup> ConvertList(DB_DEVICE_TYPE_IN_GROUP[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpDeviceTypeInGroup> ConvertList(DB_DEVICE_TYPE_IN_GROUP[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceTypeInGroup> ret = new List<OpDeviceTypeInGroup>(list.Length);
			foreach (var loop in list)
			{
			OpDeviceTypeInGroup insert = new OpDeviceTypeInGroup(loop);
				
				if(loadNavigationProperties)
				{
											
																									insert.DeviceType = dataProvider.GetDeviceType(loop.ID_DEVICE_TYPE); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceTypeInGroup> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTypeInGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTypeInGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}