using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionElementType : DB_VERSION_ELEMENT_TYPE, IComparable, IEquatable<OpVersionElementType>, IOpChangeState
    {
        #region Properties
        public int IdVersionElementType { get { return this.ID_VERSION_ELEMENT_TYPE; } set { if (this.ID_VERSION_ELEMENT_TYPE != value) { this.ID_VERSION_ELEMENT_TYPE = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_TYPE, value, this); } } }
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = OpDataUtil.SetNewValue(this.NAME, value, this); } } }
        public long? IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = OpDataUtil.SetNewValue(this.ID_DESCR, value, this); } } }
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
		private OpDescr _Descr;
		public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties

        public OpDataList<OpVersionElementTypeData> DataList { get; set; }

        #endregion
		
        #region	Ctor
		public OpVersionElementType()
			:base() {}
		
		public OpVersionElementType(DB_VERSION_ELEMENT_TYPE clone)
			:base(clone) {}
        #endregion
		
        #region	ToString
		public override string ToString()
		{
            if (Descr != null)
                return Descr.ToString();
            return this.NAME;
		}
        #endregion

        #region	ConvertList
		public static List<OpVersionElementType> ConvertList(DB_VERSION_ELEMENT_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionElementType> ConvertList(DB_VERSION_ELEMENT_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionElementType> ret = new List<OpVersionElementType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionElementType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
        #endregion
		
        #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionElementType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
			}
		}
        #endregion
		
        #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionElementType> list, IDataProvider dataProvider, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                int[] idVersionElementTypeList = list.Select(l => l.IdVersionElementType).ToArray();
                List<OpVersionElementTypeData> data = new List<OpVersionElementTypeData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetVersionElementTypeDataFilter(IdVersionElementType: idVersionElementTypeList.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpVersionElementType> tasksDict = list.ToDictionary(l => l.IdVersionElementType);
                    foreach (var dataValue in data)
                    {
                        tasksDict[dataValue.IdVersionElementType].DataList.Add(dataValue);
                    }
                }
            }
		}
        #endregion
		
        #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionElementType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionElementType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion
		
        #region IEquatable<OpVersionElementType> Members
        public bool Equals(OpVersionElementType other)
        {
            if (other == null)
				return false;
			return this.IdVersionElementType.Equals(other.IdVersionElementType);
        }
        #endregion

        #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionElementType)
				return this.IdVersionElementType == ((OpVersionElementType)obj).IdVersionElementType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionElementType left, OpVersionElementType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionElementType left, OpVersionElementType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionElementType.GetHashCode();
		}
        #endregion
	}
#endif
}