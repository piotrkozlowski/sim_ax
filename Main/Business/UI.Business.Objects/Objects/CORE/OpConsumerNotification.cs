using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerNotification : DB_CONSUMER_NOTIFICATION, IComparable, IEquatable<OpConsumerNotification>
    {
    #region Properties
        public long IdConsumerNotification { get { return this.ID_CONSUMER_NOTIFICATION;} set {this.ID_CONSUMER_NOTIFICATION = value;}}
        public int IdConsumer { get { return this.ID_CONSUMER;} set {this.ID_CONSUMER = value;}}
        public int IdNotificationDeliveryType { get { return this.ID_NOTIFICATION_DELIVERY_TYPE;} set {this.ID_NOTIFICATION_DELIVERY_TYPE = value;}}
        public int IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
        public DateTime StartTime { get { return this.START_TIME;} set {this.START_TIME = value;}}
        public DateTime? EndTime { get { return this.END_TIME;} set {this.END_TIME = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumer _Consumer;
	    public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null)? 0 : (int)value.ID_CONSUMER; } }
	    private OpNotificationDeliveryType _NotificationDeliveryType;
	    public OpNotificationDeliveryType NotificationDeliveryType { get { return this._NotificationDeliveryType; } set { this._NotificationDeliveryType = value; this.ID_NOTIFICATION_DELIVERY_TYPE = (value == null)? 0 : (int)value.ID_NOTIFICATION_DELIVERY_TYPE; } }
	    private OpOperator _Operator;
	    public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
        public string NotificationAddress { get; private set; }

        public string GcmMobileDeviceUniqueId { get; private set; }
    #endregion
		
    #region	Ctor
		public OpConsumerNotification()
			:base() {}
		
		public OpConsumerNotification(DB_CONSUMER_NOTIFICATION clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ConsumerNotification [" + IdConsumerNotification.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerNotification> ConvertList(DB_CONSUMER_NOTIFICATION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerNotification> ConvertList(DB_CONSUMER_NOTIFICATION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpConsumerNotification> ret = new List<OpConsumerNotification>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerNotification(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerNotification> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			    Dictionary<int,OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Select(l => l.ID_CONSUMER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER);
	            Dictionary<int,OpNotificationDeliveryType> NotificationDeliveryTypeDict = dataProvider.GetNotificationDeliveryType(list.Select(l => l.ID_NOTIFICATION_DELIVERY_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_NOTIFICATION_DELIVERY_TYPE);
	            Dictionary<int,OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
			
				foreach (var loop in list)
				{
					loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER); 
		            loop.NotificationDeliveryType = NotificationDeliveryTypeDict.TryGetValue(loop.ID_NOTIFICATION_DELIVERY_TYPE); 
		            loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerNotification> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            //Load NotificationAddress

            Dictionary<int, OpOperator> operatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray(), false, loadNavigationProperties: false, loadCustomData: true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

            var addressActor = list.Where(cn => cn.IdNotificationDeliveryType.In(new [] 
                                                        { 
                                                            (int)Enums.NotificationDeliveryType.EMail, 
                                                            (int)Enums.NotificationDeliveryType.SMS }))
                                                  .ToList();
            var actorsToGet = operatorDict.Where(kv => kv.Key.In(addressActor.Select(cn => cn.IdOperator).ToArray())).Select(kv => kv.Value.IdActor).Distinct().ToArray();
            Dictionary<int, OpActor> actorsDict = dataProvider.GetActor(actorsToGet, false, loadNavigationProperties: false, loadCustomData: false).ToDictionary(k => k.IdActor);
            
            foreach (var consumerNotification in list)
            {
                OpOperator notificationOperator = operatorDict.TryGetValue(consumerNotification.ID_OPERATOR);

                switch (consumerNotification.IdNotificationDeliveryType)
                {
                    case (int)Enums.NotificationDeliveryType.SMS:
                        consumerNotification.NotificationAddress = actorsDict.TryGetValue(notificationOperator.IdActor).Mobile;
                        break;
                    case (int)Enums.NotificationDeliveryType.GCM:
                        consumerNotification.NotificationAddress = notificationOperator.DataList.TryGetValue<string>(DataType.OPERATOR_GCM_REGISTRATION_ID);
                        consumerNotification.GcmMobileDeviceUniqueId = notificationOperator.DataList.TryGetValue<string>(DataType.OPERATOR_GCM_MOBILE_DEVICE_UNIQUE_ID);
                        break;
                    case (int)Enums.NotificationDeliveryType.EMail:
                        consumerNotification.NotificationAddress = actorsDict.TryGetValue(notificationOperator.IdActor).Email;
                        break;
                }          
            }   
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerNotification)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerNotification).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerNotification> Members
        public bool Equals(OpConsumerNotification other)
        {
            if (other == null)
				return false;
			return this.IdConsumerNotification.Equals(other.IdConsumerNotification);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerNotification)
				return this.IdConsumerNotification == ((OpConsumerNotification)obj).IdConsumerNotification;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerNotification left, OpConsumerNotification right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerNotification left, OpConsumerNotification right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerNotification.GetHashCode();
		}
    #endregion
    }
#endif
}
