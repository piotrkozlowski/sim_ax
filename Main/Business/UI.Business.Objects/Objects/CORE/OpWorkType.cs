using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable]
	public class OpWorkType : DB_WORK_TYPE, IComparable, IEquatable<OpWorkType>
    {
		#region Properties
        public long IdWorkType { get { return this.ID_WORK_TYPE;} set {this.ID_WORK_TYPE = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public string PluginName { get { return this.PLUGIN_NAME;} set {this.PLUGIN_NAME = value;}}
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
		#endregion

		#region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
				#endregion

		#region	Custom Properties
		#endregion
		
		#region	Ctor
		public OpWorkType()
			:base() {}
		
		public OpWorkType(DB_WORK_TYPE clone)
			:base(clone) {}
		#endregion
		
		#region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
		#endregion

		#region	ConvertList
		public static List<OpWorkType> ConvertList(DB_WORK_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpWorkType> ConvertList(DB_WORK_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpWorkType> ret = new List<OpWorkType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpWorkType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
		#endregion
		
		#region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpWorkType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
		#endregion
		
		#region LoadCustomData
		private static void LoadCustomData(ref List<OpWorkType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
		#endregion
		
		#region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpWorkType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpWorkType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
		#endregion
		
		#region IEquatable<OpWorkType> Members
        public bool Equals(OpWorkType other)
        {
            if (other == null)
				return false;
			return this.IdWorkType.Equals(other.IdWorkType);
        }
        #endregion

		#region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpWorkType)
				return this.IdWorkType == ((OpWorkType)obj).IdWorkType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpWorkType left, OpWorkType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpWorkType left, OpWorkType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdWorkType.GetHashCode();
		}
		#endregion
    }
}