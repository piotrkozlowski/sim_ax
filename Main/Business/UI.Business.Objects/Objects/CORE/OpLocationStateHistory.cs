using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpLocationStateHistory : DB_LOCATION_STATE_HISTORY, IOpChangeState, IComparable
    {
    #region Properties
        [XmlIgnore]
        public long IdLocation
        {
            get { return this.ID_LOCATION; }
            set { if (this.ID_LOCATION != value) { this.ID_LOCATION = value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public long IdLocationState
        {
            get { return this.ID_LOCATION_STATE; }
            set { if (this.ID_LOCATION_STATE != value) { this.ID_LOCATION_STATE = value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public int IdLocationStateType
        {
            get { return this.ID_LOCATION_STATE_TYPE; }
            set { if (this.ID_LOCATION_STATE_TYPE != value) { this.ID_LOCATION_STATE_TYPE = value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public bool InKpi
        {
            get { return this.IN_KPI; }
            set { if (this.IN_KPI != value) { this.IN_KPI = value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public DateTime StartDate
        {
            get { return this.START_DATE; }
            set { if (this.START_DATE != value) { this.START_DATE = value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public DateTime? EndDate
        {
            get { return this.END_DATE; }
            set
            {
                DateTime? prevValue = this.END_DATE;
                this.END_DATE = value; if (this.END_DATE != prevValue) this.OpState = OpChangeState.Modified;
            }
        }
        [XmlIgnore]
        public string Notes
        {
            get { return this.NOTES; }
            set { if (this.NOTES != value) { this.NOTES = value; this.OpState = OpChangeState.Modified; } }
        }
    #endregion

    #region Navigation Properties
        private OpLocation _Location;
        [XmlIgnore]
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.IdLocation = (value == null) ? 0 : (long)value.ID_LOCATION; } }
        private OpLocationStateType _LocationStateType;
        [XmlIgnore]
        public OpLocationStateType LocationStateType { get { return this._LocationStateType; } set { this._LocationStateType = value; this.IdLocationStateType = (value == null) ? 0 : (int)value.ID_LOCATION_STATE_TYPE; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
    #endregion

    #region Ctor
        public OpLocationStateHistory()
            : base() { this.OpState = OpChangeState.New; }

        public OpLocationStateHistory(DB_LOCATION_STATE_HISTORY clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region ConvertList
        public static List<OpLocationStateHistory> ConvertList(DB_LOCATION_STATE_HISTORY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLocationStateHistory> ConvertList(DB_LOCATION_STATE_HISTORY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpLocationStateHistory> ret = new List<OpLocationStateHistory>(list.Length);
            list.ToList().ForEach(db_item => ret.Add(new OpLocationStateHistory(db_item)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpLocationStateHistory> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Select(l => l.ID_LOCATION).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpLocationStateType> LocationStateTypeDict = dataProvider.GetLocationStateType(list.Select(l => l.ID_LOCATION_STATE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION_STATE_TYPE);

                foreach (var loop in list)
                {
                    loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION);
                    loop.LocationStateType = LocationStateTypeDict.TryGetValue(loop.ID_LOCATION_STATE_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationStateHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.Location = dataProvider.GetLocation(this.ID_LOCATION);
            this.LocationStateType = dataProvider.GetLocationStateType(this.ID_LOCATION_STATE_TYPE);
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationStateHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationStateHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}