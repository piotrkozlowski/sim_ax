﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDiagnosticActionResult : DB_DIAGNOSTIC_ACTION_RESULT, IComparable, IEquatable<OpDiagnosticActionResult>
    {
    #region Properties
        public int IdDiagnosticActionResult { get { return this.ID_DIAGNOSTIC_ACTION_RESULT; } set { this.ID_DIAGNOSTIC_ACTION_RESULT = value; } }
        public int IdDiagnosticAction { get { return this.ID_DIAGNOSTIC_ACTION; } set { this.ID_DIAGNOSTIC_ACTION = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public bool AllowInputText { get { return this.ALLOW_INPUT_TEXT; } set { this.ALLOW_INPUT_TEXT = value; } }
        public int DamageLevel { get { return this.DAMAGE_LEVEL; } set { this.DAMAGE_LEVEL = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public int? IdServiceReferenceType { get { return this.ID_SERVICE_REFERENCE_TYPE; } set { this.ID_SERVICE_REFERENCE_TYPE = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDiagnosticAction _DiagnosticAction;
        public OpDiagnosticAction DiagnosticAction { get { return this._DiagnosticAction; } set { this._DiagnosticAction = value; this.ID_DIAGNOSTIC_ACTION = (value == null) ? 0 : (int)value.ID_DIAGNOSTIC_ACTION; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpServiceReferenceType _ServiceReferenceType;
        public OpServiceReferenceType ServiceReferenceType { get { return this._ServiceReferenceType; } set { this._ServiceReferenceType = value; this.ID_SERVICE_REFERENCE_TYPE = (value == null) ? null : (int?)value.ID_SERVICE_REFERENCE_TYPE; } }
    #endregion

    #region	Custom Properties

        public List<OpDescr> AllLanguagesDescription { get; set; }

    #endregion

    #region	Ctor
        public OpDiagnosticActionResult()
            : base() { }

        public OpDiagnosticActionResult(DB_DIAGNOSTIC_ACTION_RESULT clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDiagnosticActionResult> ConvertList(DB_DIAGNOSTIC_ACTION_RESULT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDiagnosticActionResult> ConvertList(DB_DIAGNOSTIC_ACTION_RESULT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDiagnosticActionResult> ret = new List<OpDiagnosticActionResult>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDiagnosticActionResult(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDiagnosticActionResult> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDiagnosticAction> DiagnosticActionDict = dataProvider.GetDiagnosticAction(list.Select(l => l.ID_DIAGNOSTIC_ACTION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DIAGNOSTIC_ACTION);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpServiceReferenceType> ServiceReferenceTypeDict = dataProvider.GetServiceReferenceType(list.Where(l => l.ID_SERVICE_REFERENCE_TYPE.HasValue).Select(l => l.ID_SERVICE_REFERENCE_TYPE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_REFERENCE_TYPE);

                foreach (var loop in list)
                {
                    loop.DiagnosticAction = DiagnosticActionDict.TryGetValue(loop.ID_DIAGNOSTIC_ACTION);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    if (loop.ID_SERVICE_REFERENCE_TYPE.HasValue)
                        loop.ServiceReferenceType = ServiceReferenceTypeDict.TryGetValue(loop.ID_SERVICE_REFERENCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDiagnosticActionResult> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDiagnosticActionResult)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDiagnosticActionResult).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDiagnosticActionResult> Members
        public bool Equals(OpDiagnosticActionResult other)
        {
            if (other == null)
                return false;
            return this.IdDiagnosticActionResult.Equals(other.IdDiagnosticActionResult);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDiagnosticActionResult)
                return this.IdDiagnosticActionResult == ((OpDiagnosticActionResult)obj).IdDiagnosticActionResult;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDiagnosticActionResult left, OpDiagnosticActionResult right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDiagnosticActionResult left, OpDiagnosticActionResult right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDiagnosticActionResult.GetHashCode();
        }
    #endregion
    }
#endif
}