using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpReferenceType : DB_REFERENCE_TYPE, IComparable, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { this.ID_REFERENCE_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpReferenceType()
            : base() { }

        public OpReferenceType(DB_REFERENCE_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.ToString();
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpReferenceType> ConvertList(DB_REFERENCE_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpReferenceType> ConvertList(DB_REFERENCE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpReferenceType> ret = new List<OpReferenceType>(list.Length);
            foreach (var loop in list)
            {
                OpReferenceType insert = new OpReferenceType(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[] { loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpReferenceType> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpReferenceType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpReferenceType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpReferenceType> Members
        public bool Equals(OpReferenceType other)
        {
            if (other == null)
                return false;
            return this.IdReferenceType.Equals(other.IdReferenceType);
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdReferenceType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpReferenceType)
                return this.IdReferenceType == ((OpReferenceType)obj).IdReferenceType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpReferenceType left, OpReferenceType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpReferenceType left, OpReferenceType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdReferenceType.GetHashCode();
        }
    #endregion

    #region Enum
        //zmiany wprowadza� r�wnie� w DB
        [Obsolete("Use Common.Enums.ReferenceType instead")]
        public enum Enum
        {
            None = 0,
            IdOperator = 1,
            IdAtgType = 2,
            IdReport = 3,
            IdReportDataType = 4,
            IdDataType = 5,
            SerialNbr = 6,
            IdMeter = 7,
            IdLocation = 8,
            IdProductCode = 9,
            IdDeviceDriver = 10,
            IdDistributor = 11,
            IdDelivery = 12,
            IdMeterType = 13,
            IdDeviceType = 14,
            IdValveState = 15,
            IdIssueStatus = 16,
            IdIssueType = 17,
            IdTaskStatus = 18,
            IdTaskType = 19,
            IdRoute = 20,
            IdRouteStatus = 21,
            OperatorList = 23
        }
    #endregion

    }
#endif
}