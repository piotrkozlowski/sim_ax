﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceOrderNumberInArticle : DB_DEVICE_ORDER_NUMBER_IN_ARTICLE, IComparable, IEquatable<OpDeviceOrderNumberInArticle>
    {
    #region Properties
        public int IdDeviceOrderNumber { get { return this.ID_DEVICE_ORDER_NUMBER; } set { this.ID_DEVICE_ORDER_NUMBER = value; } }
        public long IdArticle { get { return this.ID_ARTICLE; } set { this.ID_ARTICLE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceOrderNumber _DeviceOrderNumber;
        public OpDeviceOrderNumber DeviceOrderNumber { get { return this._DeviceOrderNumber; } set { this._DeviceOrderNumber = value; this.ID_DEVICE_ORDER_NUMBER = (value == null) ? 0 : (int)value.ID_DEVICE_ORDER_NUMBER; } }
        private OpArticle _Article;
        public OpArticle Article { get { return this._Article; } set { this._Article = value; this.ID_ARTICLE = (value == null) ? 0 : (long)value.ID_ARTICLE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceOrderNumberInArticle()
            : base() { }

        public OpDeviceOrderNumberInArticle(DB_DEVICE_ORDER_NUMBER_IN_ARTICLE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeviceOrderNumberInArticle [" + IdDeviceOrderNumber.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceOrderNumberInArticle> ConvertList(DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceOrderNumberInArticle> ConvertList(DB_DEVICE_ORDER_NUMBER_IN_ARTICLE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceOrderNumberInArticle> ret = new List<OpDeviceOrderNumberInArticle>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceOrderNumberInArticle(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceOrderNumberInArticle> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpArticle> ArticleDict = dataProvider.GetArticle(list.Select(l => l.ID_ARTICLE).Distinct().ToArray()).ToDictionary(l => l.ID_ARTICLE);
                foreach (var loop in list)
                {
                    loop.Article = ArticleDict.TryGetValue(loop.ID_ARTICLE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceOrderNumberInArticle> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceOrderNumberInArticle)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceOrderNumberInArticle).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceOrderNumberInArticle> Members
        public bool Equals(OpDeviceOrderNumberInArticle other)
        {
            if (other == null)
                return false;
            return this.IdDeviceOrderNumber.Equals(other.IdDeviceOrderNumber);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceOrderNumberInArticle)
                return this.IdDeviceOrderNumber == ((OpDeviceOrderNumberInArticle)obj).IdDeviceOrderNumber;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceOrderNumberInArticle left, OpDeviceOrderNumberInArticle right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceOrderNumberInArticle left, OpDeviceOrderNumberInArticle right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceOrderNumber.GetHashCode();
        }
    #endregion
    }
#endif
}