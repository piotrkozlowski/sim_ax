﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceList : DB_SERVICE_LIST, IComparable, IEquatable<OpServiceList>
    {
    #region Properties
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
        public int IdOperatorReceiver { get { return this.ID_OPERATOR_RECEIVER; } set { this.ID_OPERATOR_RECEIVER = value; } }
        public int IdOperatorServicer { get { return this.ID_OPERATOR_SERVICER; } set { this.ID_OPERATOR_SERVICER = value; } }
        public string CourierName { get { return this.COURIER_NAME; } set { this.COURIER_NAME = value; } }
        public int Priority { get { return this.PRIORITY; } set { this.PRIORITY = value; } }
        public string ShippingLetterNbr { get { return this.SHIPPING_LETTER_NBR; } set { this.SHIPPING_LETTER_NBR = value; } }
        public DateTime ExpectedFinishDate { get { return this.EXPECTED_FINISH_DATE; } set { this.EXPECTED_FINISH_DATE = value; } }
        public string ServiceName { get { return this.SERVICE_NAME; } set { this.SERVICE_NAME = value; } }
        public int? IdDocumentPackage { get { return this.ID_DOCUMENT_PACKAGE; } set { this.ID_DOCUMENT_PACKAGE = value; } }
        public bool Confirmed { get { return this.CONFIRMED; } set { this.CONFIRMED = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpOperator _OperatorReceiver;
        [CopyableProperty]
        public OpOperator OperatorReceiver { get { return this._OperatorReceiver; } set { this._OperatorReceiver = value; this.IdOperatorReceiver = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorServicer;
        [CopyableProperty]
        public OpOperator OperatorServicer { get { return this._OperatorServicer; } set { this._OperatorServicer = value; this.IdOperatorServicer = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties

        public long? IdLocationMagazine
        {
            get
            {
                return DataList.TryGetNullableValue<long>(DataType.SERVICE_LIST_MAGAZINE);
            }
            set
            {
                if (!DataList.Exists(d => d.IdDataType == DataType.SERVICE_LIST_MAGAZINE))
                {
                    OpServiceListData newData = new OpServiceListData();
                    newData.IdDataType = DataType.SERVICE_LIST_MAGAZINE;
                    newData.IndexNbr = 0;
                    newData.IdServiceList = this.IdServiceList;
                    newData.OpState = OpChangeState.New;
                    newData.Value = value;
                    DataList.Add(newData);
                }
                else
                    DataList.SetValue(DataType.SERVICE_LIST_MAGAZINE, value);
            }
        }

        public OpLocation Magazine
        {
            get;
            set;
        }

        public OpDataList<OpServiceListData> DataList { get; set; }

    #endregion

    #region	Ctor
        public OpServiceList()
            : base() 
        {
            DataList = new OpDataList<OpServiceListData>();
        }

        public OpServiceList(DB_SERVICE_LIST clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpServiceListData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.COURIER_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceList> ConvertList(DB_SERVICE_LIST[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceList> ConvertList(DB_SERVICE_LIST[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceList> ret = new List<OpServiceList>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceList(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceList> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpOperator> OperatorReceiverDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_RECEIVER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorServicerDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_SERVICER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.OperatorReceiver = OperatorReceiverDict.TryGetValue(loop.ID_OPERATOR_RECEIVER);
                    loop.OperatorServicer = OperatorServicerDict.TryGetValue(loop.ID_OPERATOR_SERVICER);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceList> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IServiceListDataTypes != null)
            {
                List<int> idServiceList = list.Select(l => l.IdServiceList).ToList();

                if (dataProvider.IServiceListDataTypes.Count > 0)
                {
                    List<OpServiceListData> data = dataProvider.GetServiceListDataFilter(IdServiceList: idServiceList.ToArray(), IdDataType: dataProvider.IServiceListDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpServiceList> serviceListDataDict = list.ToDictionary<OpServiceList, int>(l => l.IdServiceList);
                        foreach (var dataValue in data)
                        {
                            serviceListDataDict[dataValue.IdServiceList].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceList)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceList).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceList> Members
        public bool Equals(OpServiceList other)
        {
            if (other == null)
                return false;
            return this.IdServiceList.Equals(other.IdServiceList);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceList)
                return this.IdServiceList == ((OpServiceList)obj).IdServiceList;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceList left, OpServiceList right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceList left, OpServiceList right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceList.GetHashCode();
        }
    #endregion
    }
#endif
}