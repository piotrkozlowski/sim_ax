using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpAlarmMessage : DB_ALARM_MESSAGE, IComparable, IEquatable<OpAlarmMessage>
    {
    #region Properties
            public long IdAlarmEvent { get { return this.ID_ALARM_EVENT;} set {this.ID_ALARM_EVENT = value;}}
                public int IdLanguage { get { return this.ID_LANGUAGE;} set {this.ID_LANGUAGE = value;}}
                public string AlarmMessage { get { return this.ALARM_MESSAGE;} set {this.ALARM_MESSAGE = value;}}
    #endregion

    #region	Navigation Properties
		private OpAlarmEvent _AlarmEvent;
				public OpAlarmEvent AlarmEvent { get { return this._AlarmEvent; } set { this._AlarmEvent = value; this.ID_ALARM_EVENT = (value == null)? 0 : (long)value.ID_ALARM_EVENT; } }
				private OpLanguage _Language;
				public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null)? 0 : (int)value.ID_LANGUAGE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpAlarmMessage()
			:base() {}
		
		public OpAlarmMessage(DB_ALARM_MESSAGE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.ALARM_MESSAGE;
		}
    #endregion

    #region	ConvertList
		public static List<OpAlarmMessage> ConvertList(DB_ALARM_MESSAGE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpAlarmMessage> ConvertList(DB_ALARM_MESSAGE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpAlarmMessage> ret = new List<OpAlarmMessage>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmMessage(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpAlarmMessage> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray()).ToDictionary(l => l.ID_LANGUAGE);
			
				foreach (var loop in list)
				{
						loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpAlarmMessage> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmMessage)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmMessage).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpAlarmMessage> Members
        public bool Equals(OpAlarmMessage other)
        {
            if (other == null)
				return false;
			return this.IdAlarmEvent.Equals(other.IdAlarmEvent);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpAlarmMessage)
				return this.IdAlarmEvent == ((OpAlarmMessage)obj).IdAlarmEvent;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpAlarmMessage left, OpAlarmMessage right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpAlarmMessage left, OpAlarmMessage right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdAlarmEvent.GetHashCode();
		}
    #endregion
    }
#endif
}