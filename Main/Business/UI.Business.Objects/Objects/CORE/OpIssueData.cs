using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpIssueData : DB_ISSUE_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpIssueData>
	{
    #region Properties
		public long IdIssueData { get { return this.ID_ISSUE_DATA; } set { this.ID_ISSUE_DATA = value; } }
		public int IdIssue { get { return this.ID_ISSUE; } set { this.ID_ISSUE = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpIssue _Issue;
		public OpIssue Issue { get { return this._Issue; } set { this._Issue = value; this.ID_ISSUE = (value == null) ? 0 : (int)value.ID_ISSUE; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdIssueData; } set { IdIssueData = value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpIssueData()
			: base() { this.OpState = OpChangeState.New; }

		public OpIssueData(DB_ISSUE_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "IssueData [" + IdIssueData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpIssueData> ConvertList(DB_ISSUE_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpIssueData> ConvertList(DB_ISSUE_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpIssueData> ret = new List<OpIssueData>();
			foreach (var loop in list)
			{
				OpIssueData insert = new OpIssueData(loop);

                ret.Add(insert);
			}

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider);

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpIssueData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
               // Dictionary<int, OpIssue> IssueDict = dataProvider.GetIssue(list.Select(l => l.IdIssue).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.IdIssue);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.IdDataType).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.IdDataType);
                
                foreach (var loop in list)
                {
                  //  loop.Issue = IssueDict.TryGetValue(loop.ID_ISSUE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }

        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpIssueData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpIssueData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpIssueData).ToString());
				else
					return -1;
			}
			else
				return -1;
		}
    #endregion

    #region IEquatable<OpIssueData> Members
		public bool Equals(OpIssueData other)
		{
			if (other == null)
				return false;
			return this.IdIssueData.Equals(other.IdIssueData);
		}
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public object Clone(IDataProvider dataProvider)
		{
			OpIssueData clone = new OpIssueData();
			clone.IdData = this.IdData;
			clone.IdIssue = this.IdIssue;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_ISSUE_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.Issue = dataProvider.GetIssue(this.ID_ISSUE);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpIssueData)
				return this.IdData == ((OpIssueData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpIssueData left, OpIssueData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpIssueData left, OpIssueData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}