using System;
using System.Collections.Generic;
using System.Linq;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataArch : DB_DATA_ARCH, IComparable, IEquatable<OpDataArch>, IReferenceType
    {
        #region Properties
        public long IdDataArch { get { return this.ID_DATA_ARCH; } set { this.ID_DATA_ARCH = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public bool IsAction { get { return this.IS_ACTION; } set { this.IS_ACTION = value; } }
        public bool IsAlarm { get { return this.IS_ALARM; } set { this.IS_ALARM = value; } }
        public long? IdPacket { get { return this.ID_PACKET; } set { this.ID_PACKET = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        #endregion

        #region	Navigation Properties
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpDataArch()
            : base() { }

        public OpDataArch(DB_DATA_ARCH clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "DataArch [" + IdDataArch.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpDataArch> ConvertList(DB_DATA_ARCH[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataArch> ConvertList(DB_DATA_ARCH[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDataArch> ret = new List<OpDataArch>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataArch(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataArch> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataArch> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataArch)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataArch).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpDataArch> Members
        public bool Equals(OpDataArch other)
        {
            if (other == null)
                return false;
            return this.IdDataArch.Equals(other.IdDataArch);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataArch)
                return this.IdDataArch == ((OpDataArch)obj).IdDataArch;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataArch left, OpDataArch right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataArch left, OpDataArch right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataArch.GetHashCode();
        }
        #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdDataArch;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion
    }
#endif
}