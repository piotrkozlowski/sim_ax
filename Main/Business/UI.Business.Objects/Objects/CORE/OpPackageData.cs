using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpPackageData : DB_PACKAGE_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpPackageData>
	{
    #region Properties
		public long IdPackageData { get { return this.ID_PACKAGE_DATA; } set { this.ID_PACKAGE_DATA = value; } }
		public int IdPackage { get { return this.ID_PACKAGE; } set { this.ID_PACKAGE = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpPackage _Package;
		public OpPackage Package { get { return this._Package; } set { this._Package = value; this.ID_PACKAGE = (value == null) ? 0 : (int)value.ID_PACKAGE; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdPackageData; } set { IdPackageData = value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpPackageData()
			: base() { this.OpState = OpChangeState.New; }

		public OpPackageData(DB_PACKAGE_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "PackageData [" + IdPackageData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpPackageData> ConvertList(DB_PACKAGE_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpPackageData> ConvertList(DB_PACKAGE_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpPackageData> ret = new List<OpPackageData>(list.Length);
			foreach (var loop in list)
			{
				OpPackageData insert = new OpPackageData(loop);
				if (loadNavigationProperties)
				{
                    //insert.Package = dataProvider.GetPackage(loop.ID_PACKAGE);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpPackageData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.Package = dataProvider.GetPackage(this.ID_PACKAGE);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpPackageData clone = new OpPackageData();
            clone.IdData = this.IdData;
            clone.IdPackage = this.IdPackage;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_PACKAGE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpPackageData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpPackageData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IEquatable<OpPackageData> Members
		public bool Equals(OpPackageData other)
		{
			if (other == null)
				return false;
			return this.IdPackageData.Equals(other.IdPackageData);
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpPackageData)
				return this.IdData == ((OpPackageData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpPackageData left, OpPackageData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpPackageData left, OpPackageData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}