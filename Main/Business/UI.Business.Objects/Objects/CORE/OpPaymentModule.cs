using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpPaymentModule : DB_PAYMENT_MODULE, IComparable, IEquatable<OpPaymentModule>
    {
    #region Properties
        public int IdPaymentModule { get { return this.ID_PAYMENT_MODULE;} set {this.ID_PAYMENT_MODULE = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public int IdPaymentModuleType { get { return this.ID_PAYMENT_MODULE_TYPE;} set {this.ID_PAYMENT_MODULE_TYPE = value;}}
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpPaymentModuleType _PaymentModuleType;
				public OpPaymentModuleType PaymentModuleType { get { return this._PaymentModuleType; } set { this._PaymentModuleType = value; this.ID_PAYMENT_MODULE_TYPE = (value == null)? 0 : (int)value.ID_PAYMENT_MODULE_TYPE; } }
				private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpPaymentModule()
			:base() {}
		
		public OpPaymentModule(DB_PAYMENT_MODULE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpPaymentModule> ConvertList(DB_PAYMENT_MODULE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpPaymentModule> ConvertList(DB_PAYMENT_MODULE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpPaymentModule> ret = new List<OpPaymentModule>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpPaymentModule(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpPaymentModule> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			//dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue)
                //public List<OpDescr> GetDescr(long[] Ids, bool queryDatabase, Enums.Language language, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
               Dictionary<int,OpPaymentModuleType> PaymentModuleTypeDict = dataProvider.GetPaymentModuleType(list.Select(l => l.ID_PAYMENT_MODULE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PAYMENT_MODULE_TYPE);
	Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);			
			foreach (var loop in list)
			{
				loop.PaymentModuleType = PaymentModuleTypeDict.TryGetValue(loop.ID_PAYMENT_MODULE_TYPE); 
		        if (loop.ID_DESCR.HasValue)
					loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpPaymentModule> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPaymentModule)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPaymentModule).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpPaymentModule> Members
        public bool Equals(OpPaymentModule other)
        {
            if (other == null)
				return false;
			return this.IdPaymentModule.Equals(other.IdPaymentModule);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpPaymentModule)
				return this.IdPaymentModule == ((OpPaymentModule)obj).IdPaymentModule;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpPaymentModule left, OpPaymentModule right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpPaymentModule left, OpPaymentModule right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdPaymentModule.GetHashCode();
		}
    #endregion
    }
#endif
}