using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpSettlementDocumentType : DB_SETTLEMENT_DOCUMENT_TYPE, IComparable, IEquatable<OpSettlementDocumentType>
    {
    #region Properties
        public int IdSettlementDocumentType { get { return this.ID_SETTLEMENT_DOCUMENT_TYPE;} set {this.ID_SETTLEMENT_DOCUMENT_TYPE = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
        public bool Active { get { return this.ACTIVE;} set {this.ACTIVE = value;}}
        public int? IdDistributor { get { return this.ID_DISTRIBUTOR;} set {this.ID_DISTRIBUTOR = value;}}
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDistributor _Distributor;
				public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null)? null : (int?)value.ID_DISTRIBUTOR; } }
				private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpSettlementDocumentType()
			:base() {}
		
		public OpSettlementDocumentType(DB_SETTLEMENT_DOCUMENT_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpSettlementDocumentType> ConvertList(DB_SETTLEMENT_DOCUMENT_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpSettlementDocumentType> ConvertList(DB_SETTLEMENT_DOCUMENT_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSettlementDocumentType> ret = new List<OpSettlementDocumentType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpSettlementDocumentType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpSettlementDocumentType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Where(l => l.ID_DISTRIBUTOR.HasValue).Select(l => l.ID_DISTRIBUTOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
	Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
						if (loop.ID_DISTRIBUTOR.HasValue)
						loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR.Value); 
		if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSettlementDocumentType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSettlementDocumentType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSettlementDocumentType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpSettlementDocumentType> Members
        public bool Equals(OpSettlementDocumentType other)
        {
            if (other == null)
				return false;
			return this.IdSettlementDocumentType.Equals(other.IdSettlementDocumentType);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpSettlementDocumentType)
				return this.IdSettlementDocumentType == ((OpSettlementDocumentType)obj).IdSettlementDocumentType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpSettlementDocumentType left, OpSettlementDocumentType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpSettlementDocumentType left, OpSettlementDocumentType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdSettlementDocumentType.GetHashCode();
		}
    #endregion
    }
#endif
}