using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;
using IMR.Suite.Common;
using System.Drawing;
using System.IO;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLanguage : DB_LANGUAGE, IComparable, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdLanguage { get { return this.ID_LANGUAGE; } set { this.ID_LANGUAGE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public string CultureCode { get { return this.CULTURE_CODE; } set { this.CULTURE_CODE = value; } }
        [XmlIgnore]
        public int CultureLangid { get { return this.CULTURE_LANGID; } set { this.CULTURE_LANGID = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public OpDataList<OpLanguageData> DataList { get; set; }
        public OpChangeState OpState { get; set; }
        [NonSerialized]//docelowo trzeba zamienic Image na np byte[] ktore jest serializowalne
        private Image _SmallFlag;
        [XmlIgnore]
        public Image SmallFlag { get { return this._SmallFlag; } set { this._SmallFlag = value; } }
    #endregion

    #region	Ctor
        public OpLanguage()
            : base() 
        {
            this.OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpLanguageData>();
        }

        public OpLanguage(DB_LANGUAGE clone)
            : base(clone) 
        {
            this.OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpLanguageData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpLanguage> ConvertList(DB_LANGUAGE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLanguage> ConvertList(DB_LANGUAGE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpLanguage> ret = new List<OpLanguage>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpLanguage(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpLanguage> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLanguage> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.ILanguageDataTypes != null)
            {
                List<int> idLanguage = list.Select(l => l.IdLanguage).ToList();

                List<OpLanguageData> data = dataProvider.GetLanguageDataFilter(IdLanguage: idLanguage.ToArray(), IdDataType: dataProvider.ILanguageDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpLanguage> languagesDict = list.ToDictionary<OpLanguage, int>(l => l.IdLanguage);
                    foreach (var dataValue in data)
                    {
                        languagesDict[dataValue.IdLanguage].DataList.Add(dataValue);
                    }
                }
                
                List<long> idFile = new List<long>();
                foreach (var language in list) 
                {
                    long? idSmallFlagFile = language.DataList.TryGetNullableValue<long>(DataType.LANGUAGE_SMALL_FLAG_ID_FILE);
                    if(idSmallFlagFile.HasValue)
                        idFile.Add(idSmallFlagFile.Value);
                }
                Dictionary<long, OpFile> FileDict = dataProvider.GetFile(idFile.ToArray(), false, autoTransaction, transactionLevel, commandTimeout).ToDictionary(q => q.ID_FILE);
                foreach (var loop in FileDict.Values)
                {
                        dataProvider.GetFileContent(loop);
                }
                foreach (var item in list)
                {
                    long? idSmallFlagFile = item.DataList.TryGetNullableValue<long>(DataType.LANGUAGE_SMALL_FLAG_ID_FILE);
                    if(idSmallFlagFile.HasValue)
                    {
                        OpFile fileFlag = FileDict.TryGetValue(idSmallFlagFile.Value);
                        if (fileFlag != null) 
                        {
                            if (fileFlag.FileBytes is byte[])
                            {
                                MemoryStream ms = new MemoryStream((byte[])fileFlag.FileBytes);
                                try
                                {
                                    if (ms.Length > 0)
                                        item.SmallFlag = Image.FromStream(ms);
                                }
                                catch (Exception ex)
                                {

                                }
                                finally
                                {
                                    ((IDisposable)ms).Dispose();
                                }
                            }
                        }
                    }
            }
                
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLanguage)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLanguage).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IReferenceType
        public object GetReferenceKey()
        {
            return this.IdLanguage;
        }

        public object GetReferenceValue()
        {
            return this;
        } 
    #endregion
    }
#endif
}