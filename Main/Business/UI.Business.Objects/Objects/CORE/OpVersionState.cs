using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpVersionState : DB_VERSION_STATE, IComparable, IEquatable<OpVersionState>, IOpChangeState, IReferenceType
    {
        #region Properties
        public int IdVersionState { get { return this.ID_VERSION_STATE; } set { if (this.ID_VERSION_STATE != value) { this.ID_VERSION_STATE = OpDataUtil.SetNewValue(this.ID_VERSION_STATE, value, this); } } }
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = OpDataUtil.SetNewValue(this.NAME, value, this); } } }
        public long? IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = OpDataUtil.SetNewValue(this.ID_DESCR, value, this); } } }
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpVersionState()
            : base() { }

        public OpVersionState(DB_VERSION_STATE clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpVersionState> ConvertList(DB_VERSION_STATE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpVersionState> ConvertList(DB_VERSION_STATE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpVersionState> ret = new List<OpVersionState>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionState(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpVersionState> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = dataProvider.GetDescr(new long[] { loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpVersionState> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionState)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionState).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpVersionState> Members
        public bool Equals(OpVersionState other)
        {
            if (other == null)
                return false;
            return this.IdVersionState.Equals(other.IdVersionState);
        }
        #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdVersionState;
        }

        public object GetReferenceValue()
        {
            return this;
        }

        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpVersionState)
                return this.IdVersionState == ((OpVersionState)obj).IdVersionState;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpVersionState left, OpVersionState right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpVersionState left, OpVersionState right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdVersionState.GetHashCode();
        }
        #endregion

    }
#endif
}