using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;
using System.Runtime.Serialization;
using System.Xml.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpRoute : DB_ROUTE, IComparable, IReferenceType, IOpChangeState, IOpDynamic, IOpObject<OpRouteData>
    {
        #region Properties
        public long IdRoute { get { return this.ID_ROUTE; } set { if (this.ID_ROUTE != value) { this.ID_ROUTE = value; this.OpState = OpChangeState.Modified; } } }
        public int? IdRouteDef { get { return this.ID_ROUTE_DEF; } set { if (this.ID_ROUTE_DEF != value) { this.ID_ROUTE_DEF = value; this.OpState = OpChangeState.Modified; } } }
        public int Year { get { return this.YEAR; } set { if (this.YEAR != value) { this.YEAR = value; this.OpState = OpChangeState.Modified; } } }
        public int Month { get { return this.MONTH; } set { if (this.MONTH != value) { this.MONTH = value; this.OpState = OpChangeState.Modified; } } }
        public int Week { get { return this.WEEK; } set { if (this.WEEK != value) { this.WEEK = value; this.OpState = OpChangeState.Modified; } } }
        public int IdOperatorExecutor { get { return this.ID_OPERATOR_EXECUTOR; } set { if (this.ID_OPERATOR_EXECUTOR != value) { this.ID_OPERATOR_EXECUTOR = value; this.OpState = OpChangeState.Modified; } } }
        public int? IdOperatorApproved { get { return this.ID_OPERATOR_APPROVED; } set { if (this.ID_OPERATOR_APPROVED != value) { this.ID_OPERATOR_APPROVED = value; this.OpState = OpChangeState.Modified; } } }
        public DateTime? DateUploaded { get { return this.DATE_UPLOADED; } set { if (this.DATE_UPLOADED != ((value == null) ? null : (DateTime?)value.Value)) { this.DATE_UPLOADED = (value == null) ? null : (DateTime?)value.Value; this.OpState = OpChangeState.Modified; } } }
        public DateTime? DateApproved { get { return this.DATE_APPROVED; } set { if (this.DATE_APPROVED != ((value == null) ? null : (DateTime?)value.Value)) { this.DATE_APPROVED = (value == null) ? null : (DateTime?)value.Value; this.OpState = OpChangeState.Modified; } } }
        public DateTime? DateFinished { get { return this.DATE_FINISHED; } set { if (this.DATE_FINISHED != ((value == null) ? null : (DateTime?)value.Value)) { this.DATE_FINISHED = (value == null) ? null : (DateTime?)value.Value; this.OpState = OpChangeState.Modified; } } }
        public int IdRouteStatus { get { return this.ID_ROUTE_STATUS; } set { if (this.ID_ROUTE_STATUS != value) { this.ID_ROUTE_STATUS = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public object RowVersion { get { return this.ROW_VERSION; } set { if (this.ROW_VERSION != value) { this.ROW_VERSION = value; this.OpState = OpChangeState.Modified; } } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { if (this.ID_DISTRIBUTOR != value) { this.ID_DISTRIBUTOR = value; this.OpState = OpChangeState.Modified; } } }
        public int IdRouteType { get { return this.ID_ROUTE_TYPE; } set { this.ID_ROUTE_TYPE = value; this.OpState = OpChangeState.Modified; } }
        public DateTime? CreationDate { get { return this.CREATION_DATE; } set { if (this.CREATION_DATE != ((value == null) ? null : (DateTime?)value.Value)) { this.CREATION_DATE = (value == null) ? null : (DateTime?)value.Value; this.OpState = OpChangeState.Modified; } } }
        public DateTime? ExpirationDate { get { return this.EXPIRATION_DATE; } set { if (this.EXPIRATION_DATE != ((value == null) ? null : (DateTime?)value.Value)) { this.EXPIRATION_DATE = (value == null) ? null : (DateTime?)value.Value; this.OpState = OpChangeState.Modified; } } }
        public string Name { get { return this.NAME; } set { this.NAME = OpDataUtil.SetNewValue(this.NAME, value, this); } }

        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpRouteDef _RouteDef;
        public OpRouteDef RouteDef { get { return this._RouteDef; } set { this._RouteDef = value; this.IdRouteDef = (value == null) ? null : (int?)value.ID_ROUTE_DEF; } }
        [DataMember]
        private OpRouteStatus _RouteStatus;
        public OpRouteStatus RouteStatus { get { return this._RouteStatus; } set { this._RouteStatus = value; this.IdRouteStatus = (value == null) ? 0 : (int)value.ID_ROUTE_STATUS; } }
        [DataMember]
        private OpOperator _OperatorExecutor;
        public OpOperator OperatorExecutor { get { return this._OperatorExecutor; } set { this._OperatorExecutor = value; this.IdOperatorExecutor = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        [DataMember]
        private OpOperator _OperatorApproved;
        public OpOperator OperatorApproved { get { return this._OperatorApproved; } set { this._OperatorApproved = value; this.IdOperatorApproved = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        [DataMember]
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.IdDistributor = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        [DataMember]
        private OpRouteType _RouteType;
        public OpRouteType RouteType { get { return this._RouteType; } set { this._RouteType = value; this.ID_ROUTE_TYPE = (value == null) ? 0 : (int)value.ID_ROUTE_TYPE; } }
        #endregion

        #region	Custom Properties
        public string Symbol
        {
            get
            {
                OpRouteData rdAdditionalSymbol = DataList.Find(delegate(OpRouteData rd) { return rd.IdDataType == DataType.ROUTE_ADDITIONAL_SYMBOL; });
                if (OperatorExecutor != null)
                    return this.YEAR.ToString("D4") + this.MONTH.ToString("D2") + "_" + this.WEEK.ToString("D1") + 
                        (String.IsNullOrEmpty(this.OperatorExecutor.Suffix) ? this.OperatorExecutor.Initials : this.OperatorExecutor.Suffix) +
                        (rdAdditionalSymbol == null ? "" : rdAdditionalSymbol.Value.ToString());
                else
                    return this.YEAR.ToString("D4") + this.MONTH.ToString("D2") + "_" + this.WEEK.ToString("D1") + "X" +
                        (rdAdditionalSymbol == null ? "" : rdAdditionalSymbol.Value.ToString());
            }
        }

        public string WeekSymbol
        {
            get
            {
                return this.YEAR.ToString("D4") + this.MONTH.ToString("D2") + "_" + this.WEEK.ToString("D1");
            }
        }

        public string MonthSymbol
        {
            get
            {
                return this.YEAR.ToString("D4") + this.MONTH.ToString("D2");
            }
        }

        public DateTime RouteDate
        {
            get
            {
                if (Year > 0 && Month > 0 && Week > 0)
                {
                    return new DateTime(Year, Month, 1).GetFirstDayOfWeek(Week);
                }
                return DateTime.MinValue;
            }
        }

        [DataMember]
        public List<OpTask> TaskList;

        [DataMember]
        public List<OpOperator> OperatorList;

        [XmlIgnore, DataMember]
        private Dictionary<string, object> dynamicValues;
        #endregion

        #region struct MonthYear
        public struct MonthYear
        {
            public int Year { get; set; }
            public int Month { get; set; }
        }
        #endregion

        #region	Ctor
        public OpRoute()
            : base()
        {
            DataList = new OpDataList<OpRouteData>();
            this.OpState = OpChangeState.New;
        }

        public OpRoute(DB_ROUTE clone)
            : base(clone)
        {
            DataList = new OpDataList<OpRouteData>();
            this.OpState = OpChangeState.Loaded;
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.Symbol;
        }
        #endregion

        #region	ConvertList
        public static List<OpRoute> ConvertList(DB_ROUTE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRoute> ConvertList(DB_ROUTE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpRoute> ret = new List<OpRoute>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRoute(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRoute> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<int, OpRouteDef> RouteDefDict = dataProvider.GetRouteDef(list.Where(l => l.ID_ROUTE_DEF.HasValue).Select(l => l.ID_ROUTE_DEF.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ROUTE_DEF);
                Dictionary<int, OpRouteStatus> RouteStatusDict = dataProvider.GetRouteStatus(list.Select(l => l.ID_ROUTE_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ROUTE_STATUS);
                Dictionary<int, OpOperator> OperatorApprovedDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR_APPROVED.HasValue).Select(l => l.ID_OPERATOR_APPROVED.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorExecutorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_EXECUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpRouteType> RouteTypeDict = dataProvider.GetRouteType(list.Select(l => l.ID_ROUTE_TYPE).Distinct().ToArray(), false).ToDictionary(l => l.ID_ROUTE_TYPE);

                foreach (var loop in list)
                {
                    //if (loop.ID_ROUTE_DEF.HasValue)
                    //    loop.RouteDef = RouteDefDict.TryGetValue(loop.ID_ROUTE_DEF.Value);
                    if (loop.ID_OPERATOR_APPROVED.HasValue)
                        loop.OperatorApproved = OperatorApprovedDict.TryGetValue(loop.ID_OPERATOR_APPROVED.Value);
                    loop.RouteStatus = RouteStatusDict.TryGetValue(loop.ID_ROUTE_STATUS);

                    loop.OperatorExecutor = OperatorExecutorDict.TryGetValue(loop.ID_OPERATOR_EXECUTOR);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);

                    loop.RouteType = RouteTypeDict.TryGetValue(loop.ID_ROUTE_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        public static void LoadCustomData(ref List<OpRoute> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            LoadCustomData(ref list, dataProvider, null, autoTransaction, transactionLevel, commandTimeout);
        }

        public static void LoadCustomData(ref List<OpRoute> list, IDataProvider dataProvider, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && (dataProvider.IRouteDataTypes != null || customDataTypes != null))
            {
                List<long> idRoute = list.Select(l => l.IdRoute).ToList();
                List<OpRouteData> data = new List<OpRouteData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetRouteDataFilter(IdRoute: idRoute.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.ITaskDataTypes != null && dataProvider.ITaskDataTypes.Count > 0)
                    data = dataProvider.GetRouteDataFilter(IdRoute: idRoute.ToArray(), IdDataType: dataProvider.IRouteDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data != null && data.Count > 0)
                {
                    Dictionary<long, OpRoute> routeDict = list.ToDictionary<OpRoute, long>(l => l.IdRoute);
                    foreach (var dataValue in data)
                    {
                        routeDict[dataValue.IdRoute].DataList.Add(dataValue);
                    }
                }
            }
        }
        #endregion

        #region Copy

        public void Copy(OpRoute toCopy, bool includeNavigationProperties, bool includeCustomProperties, bool includeDataList, bool includeDynamicValues)
        {
            if(includeNavigationProperties)
            {
                this.RouteDef = toCopy.RouteDef;
                this.OperatorExecutor = toCopy.OperatorExecutor;
                this.OperatorApproved = toCopy.OperatorApproved;
                this.RouteStatus = toCopy.RouteStatus;
                this.RouteType = toCopy.RouteType;
                this.Distributor = toCopy.Distributor;
            }
            if(includeDataList)
            {

            }
            if (includeDynamicValues)
            {
                this.DynamicValues = toCopy.DynamicValues;
            }
            this.IdRoute = toCopy.IdRoute;
            this.IdRouteDef = toCopy.IdRouteDef;
            this.Year = toCopy.Year;
            this.Month = toCopy.Month;
            this.Week = toCopy.Week;
            this.IdOperatorExecutor = toCopy.IdOperatorExecutor;
            this.IdOperatorApproved = toCopy.IdOperatorApproved;
            this.DateUploaded = toCopy.DateUploaded;
            this.DateApproved = toCopy.DateApproved;
            this.DateFinished = toCopy.DateFinished;
            this.IdRouteStatus = toCopy.IdRouteStatus;
            this.IdDistributor = toCopy.IdDistributor;
            this.IdRouteType = toCopy.IdRouteType;
            this.CreationDate = toCopy.CreationDate;
            this.ExpirationDate = toCopy.ExpirationDate;
            this.Name = toCopy.Name;
    }

        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRoute)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRoute).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

        #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdRoute;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion

        #region IEquatable<OpDeviceDetails> Members
        public bool Equals(OpRoute other)
        {
            if (other == null)
                return false;
            return this.IdRoute.Equals(other.IdRoute);
        }
        #endregion

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdRoute; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdRoute; }
            set { IdRoute = Convert.ToInt32(value); }
        }

        [DataMember]
        public OpDataList<OpRouteData> DataList { get; set; }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRoute)
                return this.IdRoute == ((OpRoute)obj).IdRoute;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRoute left, OpRoute right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRoute left, OpRoute right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRoute.GetHashCode();
        }
    #endregion
    }
#endif
}