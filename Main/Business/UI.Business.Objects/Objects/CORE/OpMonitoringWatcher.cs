using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMonitoringWatcher : DB_MONITORING_WATCHER, IComparable, IEquatable<OpMonitoringWatcher>
    {
        #region Properties
        public int IdMonitoringWatcher { get { return this.ID_MONITORING_WATCHER; } set { this.ID_MONITORING_WATCHER = value; } }
        public int IdMonitoringWatcherType { get { return this.ID_MONITORING_WATCHER_TYPE; } set { this.ID_MONITORING_WATCHER_TYPE = value; } }
        public bool IsActive { get { return this.IS_ACTIVE; } set { this.IS_ACTIVE = value; } }
        #endregion

        #region	Navigation Properties
        private OpMonitoringWatcherType _MonitoringWatcherType;
        public OpMonitoringWatcherType MonitoringWatcherType { get { return this._MonitoringWatcherType; } set { this._MonitoringWatcherType = value; this.ID_MONITORING_WATCHER_TYPE = (value == null) ? 0 : (int)value.ID_MONITORING_WATCHER_TYPE; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpMonitoringWatcher()
            : base() { }

        public OpMonitoringWatcher(DB_MONITORING_WATCHER clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "MonitoringWatcher [" + IdMonitoringWatcher.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpMonitoringWatcher> ConvertList(DB_MONITORING_WATCHER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMonitoringWatcher> ConvertList(DB_MONITORING_WATCHER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMonitoringWatcher> ret = new List<OpMonitoringWatcher>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMonitoringWatcher(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMonitoringWatcher> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpMonitoringWatcherType> MonitoringWatcherTypeDict = dataProvider.GetMonitoringWatcherType(list.Select(l => l.ID_MONITORING_WATCHER_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_MONITORING_WATCHER_TYPE);

                foreach (var loop in list)
                {
                    loop.MonitoringWatcherType = MonitoringWatcherTypeDict.TryGetValue(loop.ID_MONITORING_WATCHER_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpMonitoringWatcher> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMonitoringWatcher)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMonitoringWatcher).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpMonitoringWatcher> Members
        public bool Equals(OpMonitoringWatcher other)
        {
            if (other == null)
                return false;
            return this.IdMonitoringWatcher.Equals(other.IdMonitoringWatcher);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMonitoringWatcher)
                return this.IdMonitoringWatcher == ((OpMonitoringWatcher)obj).IdMonitoringWatcher;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMonitoringWatcher left, OpMonitoringWatcher right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMonitoringWatcher left, OpMonitoringWatcher right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMonitoringWatcher.GetHashCode();
        }
        #endregion
    }
#endif
}