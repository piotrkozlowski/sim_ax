using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Xml.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPackage : DB_PACKAGE, IComparable, IEquatable<OpPackage>, IOpDynamic, IOpDynamicProperty<OpPackage>
    {

    #region Properties
        public int IdPackage { get { return this.ID_PACKAGE; } set { this.ID_PACKAGE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public string Code { get { return this.CODE; } set { this.CODE = value; } }

        public DateTime? SendDate { get { return this.SEND_DATE; } set { this.SEND_DATE = value; } }
        public int IdOperatorCreator { get { return this.ID_OPERATOR_CREATOR; } set { this.ID_OPERATOR_CREATOR = value; } }
        public int? IdOperatorReceiver { get { return this.ID_OPERATOR_RECEIVER; } set { this.ID_OPERATOR_RECEIVER = value; } }
        public bool Received { get { return this.RECEIVED; } set { this.RECEIVED = value; } }

        public DateTime? ReceiveDate { get { return this.RECEIVE_DATE; } set { this.RECEIVE_DATE = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public int IdPackageStatus { get { return this.ID_PACKAGE_STATUS; } set { this.ID_PACKAGE_STATUS = value; } }
    #endregion

    #region	Navigation Properties
        private OpOperator _OperatorCreator;
        public OpOperator OperatorCreator { get { return this._OperatorCreator; } set { this._OperatorCreator = value; this.ID_OPERATOR_CREATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorReceiver;
        public OpOperator OperatorReceiver { get { return this._OperatorReceiver; } set { this._OperatorReceiver = value; this.ID_OPERATOR_RECEIVER = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpPackageStatus _PackageStatus;
        public OpPackageStatus PackageStatus { get { return this._PackageStatus; } set { this._PackageStatus = value; this.ID_PACKAGE_STATUS = (value == null) ? 0 : (int)value.ID_PACKAGE_STATUS; } }

        private OpOperator _OperatorAcceptingOrder;
        public OpOperator OperatorAcceptingOrder { get { return this._OperatorAcceptingOrder; } set { this._OperatorAcceptingOrder = value; } }
        private OpOperator _OperatorCompletingOrder;
        public OpOperator OperatorCompletingOrder { get { return this._OperatorCompletingOrder; } set { this._OperatorCompletingOrder = value; } }

        private OpFile _ReceiptConfirmation;
        public OpFile ReceiptConfirmation { get { return this._ReceiptConfirmation; } set { this._ReceiptConfirmation = value; } }
    #endregion

    #region	Custom Properties
        /// <summary>
        /// true if AIUT send package to operator, otherwise false
        /// </summary>
        public bool FromAIUT
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.PACKAGE_DIRECTION);
            }
            set
            {
                DataList.SetValue(DataType.PACKAGE_DIRECTION, value);
            }
        }
        public string Address
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.PACKAGE_ADDRESS);
            }
            set
            {
                DataList.SetValue(DataType.PACKAGE_ADDRESS, value);
            }
        }
        public DateTime? RealizationDate
        {
            get
            {
                DateTime? date = DataList.TryGetValue<DateTime>(DataType.PACKAGE_PRODUCTION_REALIZATION_DATE);
                if(!date.HasValue || date.Value == DateTime.MinValue)
                    date = DataList.TryGetValue<DateTime>(DataType.PACKAGE_ISU_REALIZATION_DATE);
                if (date == DateTime.MinValue)
                    date = null;
                return date;
            }
            set
            {
                if(this.IdPackageStatus.In(new int[]{ (int)Enums.PackageStatus.Ordered, 
                                                      (int)Enums.PackageStatus.ToCompletion }))
                    DataList.SetValue(DataType.PACKAGE_ISU_REALIZATION_DATE, value);
                else
                    DataList.SetValue(DataType.PACKAGE_PRODUCTION_REALIZATION_DATE, value);
            }
        }
        public string Waybill
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.PACKAGE_WAYBILL); 
            }
            set
            {
                DataList.SetValue(DataType.PACKAGE_WAYBILL, value);
            }
        }
        public bool? ReceiptConfirmationProvided
        {
            get
            {
                return DataList.TryGetNullableValue<bool>(DataType.PACKAGE_RECEIPT_CONFIRMATION_PROVIDED);
            }
            set
            {
                DataList.SetValue(DataType.PACKAGE_RECEIPT_CONFIRMATION_PROVIDED, value);
            }
        }
        public OpDataList<OpPackageData> DataList { get; set; }
        private Dictionary<string, object> dynamicValues;
        private OpDynamicPropertyDict dynamicProperties;
    #endregion

    #region	Ctor
        public OpPackage()
            : base()
        {
            DataList = new OpDataList<OpPackageData>();
            DynamicProperties = new OpDynamicPropertyDict();
            DynamicValues = new Dictionary<string, object>();
        }

        public OpPackage(DB_PACKAGE clone)
            : base(clone)
        {
            DataList = new OpDataList<OpPackageData>();
            DynamicProperties = new OpDynamicPropertyDict();
            DynamicValues = new Dictionary<string, object>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return String.Format("{0}; {1}", this.Name, this.Code);
        }
    #endregion

    #region	ConvertList
        public static List<OpPackage> ConvertList(DB_PACKAGE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPackage> ConvertList(DB_PACKAGE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpPackage> ret = new List<OpPackage>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPackage(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPackage> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpOperator> OperatorReceiverDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR_RECEIVER.HasValue).Select(l => l.ID_OPERATOR_RECEIVER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorCreatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_CREATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpPackageStatus> PackageStatusDict = dataProvider.GetPackageStatus(list.Select(l => l.ID_PACKAGE_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_PACKAGE_STATUS);
                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    if (loop.ID_OPERATOR_RECEIVER.HasValue)
                        loop.OperatorReceiver = OperatorReceiverDict.TryGetValue(loop.ID_OPERATOR_RECEIVER.Value);

                    loop.OperatorCreator = OperatorCreatorDict.TryGetValue(loop.ID_OPERATOR_CREATOR);

                    loop.PackageStatus = PackageStatusDict.TryGetValue(loop.ID_PACKAGE_STATUS); 
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPackage> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0 && dataProvider.IPackageDataTypes.Count > 0)
            {
                List<int> idPackage = list.Select(l => l.IdPackage).ToList();

                List<OpPackageData> data = dataProvider.GetPackageDataFilter(IdPackage: idPackage.ToArray(), IdDataType: dataProvider.IPackageDataTypes.ToArray());
                if (data != null && data.Count > 0)
                {
                    foreach (var package in list)
                    {
                        package.DataList.AddRange(data.Where(d => d.IdPackage == package.IdPackage));
                    }

                    List<int> operatorToGet = new List<int>();
                    List<long> idFileToGet = new List<long>();
                    List<int> idPackages = new List<int>();
                    foreach (OpPackageData pdItem in data.Where(pd => pd.IdDataType == DataType.PACKAGE_OPERATOR_ACCEPTING_ORDER ||
                                                                        pd.IdDataType == DataType.PACKAGE_OPERATOR_COMPLETING_ORDER))
                    {
                        operatorToGet.Add(Convert.ToInt32(pdItem.Value));
                        idPackages.Add(pdItem.IdPackage);
                    }
                    foreach (OpPackageData pdItem in data.Where(pd => pd.IdDataType == DataType.PACKAGE_RECEIPT_CONFIRMATION))
                    {
                        idFileToGet.Add(Convert.ToInt64(pdItem.Value));
                        idPackages.Add(pdItem.IdPackage);
                    }
                    idFileToGet = idFileToGet.Distinct().ToList();
                    idPackages = idPackages.Distinct().ToList();

                    List<OpOperator> operators = new List<OpOperator>();
                    Dictionary<long, OpFile> fDict = new Dictionary<long, OpFile>();
                    if (operatorToGet.Count > 0)
                        operators = dataProvider.GetOperator(operatorToGet.ToArray());
                    if (idFileToGet.Count > 0)
                        fDict = dataProvider.GetFile(idFileToGet.ToArray()).ToDictionary(f => f.IdFile);

                    if (operatorToGet.Count > 0 || fDict.Count > 0)
                    {
                        foreach (OpPackage pItem in list.Where(p => p.IdPackage.In(idPackages.ToArray())))
                        {
                            if (pItem.DataList.Exists(d => d.IdDataType == DataType.PACKAGE_OPERATOR_ACCEPTING_ORDER))
                            {
                                int idOperAccepting = Convert.ToInt32(pItem.DataList.Find(d => d.IdDataType == DataType.PACKAGE_OPERATOR_ACCEPTING_ORDER).Value);
                                if (idOperAccepting > 0)
                                    pItem.OperatorAcceptingOrder = operators.Find(o => o.IdOperator == idOperAccepting);
                            }
                            if (pItem.DataList.Exists(d => d.IdDataType == DataType.PACKAGE_OPERATOR_COMPLETING_ORDER))
                            {
                                int idOperatorCompleting = Convert.ToInt32(pItem.DataList.Find(d => d.IdDataType == DataType.PACKAGE_OPERATOR_COMPLETING_ORDER).Value);
                                if (idOperatorCompleting > 0)
                                    pItem.OperatorCompletingOrder = operators.Find(o => o.IdOperator == idOperatorCompleting);
                            }
                            if (pItem.DataList.Exists(d => d.IdDataType == DataType.PACKAGE_RECEIPT_CONFIRMATION))
                            {
                                long idFile = Convert.ToInt64(pItem.DataList.Find(d => d.IdDataType == DataType.PACKAGE_RECEIPT_CONFIRMATION).Value);
                                if (idFile > 0)
                                    pItem.ReceiptConfirmation = fDict.TryGetValue(idFile);
                            }
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPackage)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPackage).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPackage> Members
        public bool Equals(OpPackage other)
        {
            if (other == null)
                return false;
            return this.IdPackage.Equals(other.IdPackage);
        }
    #endregion
    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdPackage; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpPackage IOpDynamicProperty<OpPackage>.Owner
        {
            get { return this; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
    #endregion
    }
#endif
}