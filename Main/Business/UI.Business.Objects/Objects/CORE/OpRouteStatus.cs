using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpRouteStatus : DB_ROUTE_STATUS, IComparable, IReferenceType
    {
    #region Properties
            public int IdRouteStatus { get { return this.ID_ROUTE_STATUS;} set {this.ID_ROUTE_STATUS = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
                public string ComboBoxString
                {
                    get { return this.ToString(); }
                }
    #endregion
		
    #region	Ctor
		public OpRouteStatus()
			:base() {}
		
		public OpRouteStatus(DB_ROUTE_STATUS clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
		}
    #endregion

    #region	ConvertList
		public static List<OpRouteStatus> ConvertList(DB_ROUTE_STATUS[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpRouteStatus> ConvertList(DB_ROUTE_STATUS[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpRouteStatus> ret = new List<OpRouteStatus>(list.Length);
			foreach (var loop in list)
			{
			OpRouteStatus insert = new OpRouteStatus(loop);
				
				if(loadNavigationProperties)
				{
											
											
																									if (loop.ID_DESCR.HasValue)
                                                                                                        insert.Descr = dataProvider.GetDescr(new long[]{ loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault(); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRouteStatus> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Enum
        //generated from DB, don't change it!
        public enum Enum : int
        {
            Planned = 1,
            Accepted = 2,
            InProgress = 3,
            Finished = 4
            //Closes = 5,
            //Downloaded = 6,
            //Uploaded = 7,
            //Completed = 8,
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdRouteStatus;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}