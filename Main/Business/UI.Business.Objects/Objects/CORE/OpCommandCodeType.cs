using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpCommandCodeType : DB_COMMAND_CODE_TYPE, IComparable, IEquatable<OpCommandCodeType>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public long IdCommandCode { get { return this.ID_COMMAND_CODE; } set { this.ID_COMMAND_CODE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpCommandCodeType()
            : base() { }

        public OpCommandCodeType(DB_COMMAND_CODE_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpCommandCodeType> ConvertList(DB_COMMAND_CODE_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpCommandCodeType> ConvertList(DB_COMMAND_CODE_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpCommandCodeType> ret = new List<OpCommandCodeType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpCommandCodeType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpCommandCodeType> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpCommandCodeType> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpCommandCodeType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpCommandCodeType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpCommandCodeType> Members
        public bool Equals(OpCommandCodeType other)
        {
            if (other == null)
                return false;
            return this.IdCommandCode.Equals(other.IdCommandCode);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpCommandCodeType)
                return this.IdCommandCode == ((OpCommandCodeType)obj).IdCommandCode;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpCommandCodeType left, OpCommandCodeType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpCommandCodeType left, OpCommandCodeType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdCommandCode.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdCommandCode;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    }
#endif
}