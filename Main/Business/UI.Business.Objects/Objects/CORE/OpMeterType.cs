using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMeterType : DB_METER_TYPE, IComparable, IReferenceType, IEquatable<OpMeterType>
    {
    #region Properties
        [XmlIgnore]
        public int IdMeterType { get { return this.ID_METER_TYPE; } set { this.ID_METER_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public int IdMeterTypeClass { get { return this.ID_METER_TYPE_CLASS; } set { this.ID_METER_TYPE_CLASS = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpMeterTypeClass _MeterTypeClass;
        [XmlIgnore]
        public OpMeterTypeClass MeterTypeClass { get { return this._MeterTypeClass; } set { this._MeterTypeClass = value; this.ID_METER_TYPE_CLASS = (value == null) ? 0 : (int)value.ID_METER_TYPE_CLASS; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpMeterTypeData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpMeterType()
            : base() 
        {
            DataList = new OpDataList<OpMeterTypeData>();
        }

        public OpMeterType(DB_METER_TYPE clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpMeterTypeData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpMeterType> ConvertList(DB_METER_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMeterType> ConvertList(DB_METER_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMeterType> ret = new List<OpMeterType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMeterType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMeterType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpMeterTypeClass> MeterTypeClassDict = dataProvider.GetMeterTypeClass(list.Select(l => l.ID_METER_TYPE_CLASS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_METER_TYPE_CLASS);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    loop.MeterTypeClass = MeterTypeClassDict.TryGetValue(loop.ID_METER_TYPE_CLASS);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpMeterType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IMeterTypeDataTypes != null)
            {
                List<int> idMeterType = list.Select(l => l.IdMeterType).ToList();

                if (dataProvider.IMeterTypeDataTypes.Count > 0)
                {
                    List<OpMeterTypeData> data = dataProvider.GetMeterTypeDataFilter(IdMeterType: idMeterType.ToArray(), IdDataType: dataProvider.IMeterTypeDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpMeterType> meterTypeDataDict = list.ToDictionary<OpMeterType, int>(l => l.IdMeterType);
                        foreach (var dataValue in data)
                        {
                            meterTypeDataDict[dataValue.IdMeterType].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMeterType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMeterType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdMeterType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region IEquatable<OpMeterType>

        public bool Equals(OpMeterType obj)
        {
            if (obj is OpMeterType)
                return this.IdMeterType == ((OpMeterType)obj).IdMeterType;
            else
                return base.Equals(obj);
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMeterType)
                return this.IdMeterType == ((OpMeterType)obj).IdMeterType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMeterType left, OpMeterType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMeterType left, OpMeterType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMeterType.GetHashCode();
        }
    #endregion
    }
#endif
}