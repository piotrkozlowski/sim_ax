using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDeviceTypeProfileStep : DB_DEVICE_TYPE_PROFILE_STEP, IComparable, IEquatable<OpDeviceTypeProfileStep>
    {
    #region Properties
            public int IdDeviceTypeProfileStep { get { return this.ID_DEVICE_TYPE_PROFILE_STEP;} set {this.ID_DEVICE_TYPE_PROFILE_STEP = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDeviceTypeProfileStep()
			:base() {}
		
		public OpDeviceTypeProfileStep(DB_DEVICE_TYPE_PROFILE_STEP clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpDeviceTypeProfileStep> ConvertList(DB_DEVICE_TYPE_PROFILE_STEP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDeviceTypeProfileStep> ConvertList(DB_DEVICE_TYPE_PROFILE_STEP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceTypeProfileStep> ret = new List<OpDeviceTypeProfileStep>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceTypeProfileStep(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDeviceTypeProfileStep> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceTypeProfileStep> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTypeProfileStep)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTypeProfileStep).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDeviceTypeProfileStep> Members
        public bool Equals(OpDeviceTypeProfileStep other)
        {
            if (other == null)
				return false;
			return this.IdDeviceTypeProfileStep.Equals(other.IdDeviceTypeProfileStep);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDeviceTypeProfileStep)
				return this.IdDeviceTypeProfileStep == ((OpDeviceTypeProfileStep)obj).IdDeviceTypeProfileStep;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceTypeProfileStep left, OpDeviceTypeProfileStep right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceTypeProfileStep left, OpDeviceTypeProfileStep right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDeviceTypeProfileStep.GetHashCode();
		}
    #endregion
    }
#endif
}