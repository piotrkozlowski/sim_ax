using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpConfigurationProfile : DB_CONFIGURATION_PROFILE, IComparable, IEquatable<OpConfigurationProfile>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public long IdConfigurationProfile { get { return this.ID_CONFIGURATION_PROFILE; } set { this.ID_CONFIGURATION_PROFILE = value; } }
        [XmlIgnore]
        public int IdConfigurationProfileType { get { return this.ID_CONFIGURATION_PROFILE_TYPE; } set { this.ID_CONFIGURATION_PROFILE_TYPE = value; } }
        [XmlIgnore]
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public int IdOperatorCreated { get { return this.ID_OPERATOR_CREATED; } set { this.ID_OPERATOR_CREATED = value; } }
        [XmlIgnore]
        public DateTime CreatedTime { get { return this.CREATED_TIME; } set { this.CREATED_TIME = value; } }
    #endregion

    #region	Navigation Properties
        private OpConfigurationProfileType _ConfigurationProfileType;
        [XmlIgnore]
        public OpConfigurationProfileType ConfigurationProfileType { get { return this._ConfigurationProfileType; } set { this._ConfigurationProfileType = value; this.ID_CONFIGURATION_PROFILE_TYPE = (value == null) ? 0 : (int)value.ID_CONFIGURATION_PROFILE_TYPE; } }
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpDescr _Descr;
        [XmlIgnore]
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        
        private OpOperator _OperatorCreated;
        [XmlIgnore]
        public OpOperator OperatorCreated { get { return this._OperatorCreated; } set { this._OperatorCreated = value; this.ID_OPERATOR_CREATED = (value == null) ? 0 : (int)value.ID_OPERATOR; } }

    #endregion

    #region	Custom Properties
        public OpDataList<OpConfigurationProfileData> DataList { get; set; }

        [XmlIgnore]
        public int? ApproximatelySMSConsumption { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.DEVICE_SMS_SENT_COUNT); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_SMS_SENT_COUNT, value); } }

        [XmlIgnore]
        public double? ApproximatelyBatteryConsumption { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.BATTERY_VOLTAGE_VALUE); } set { DataList.SetValueForDefaultConfig(DataType.BATTERY_VOLTAGE_VALUE, value); } }


        [XmlIgnore]
        public int? RadioListenTime { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.DEVICE_RADIO_LISTEN_TIME); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_RADIO_LISTEN_TIME, value); } }

        [XmlIgnore]
        public bool? DeviceWLEnabled { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_WL_ENABLED); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_WL_ENABLED, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableLoLo { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_LOLO_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_LOLO_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableLo { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_LO_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_LO_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableHi { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_HI_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_HI_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableHiHi { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_HIHI_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_HIHI_EVENT_ENABLE, value); } }
        #region NewEvents
        //public bool? AlevelAlarmLevelSingleEnableLoLoUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LOLO_UP_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LOLO_UP_EVENT_ENABLE, value); } }
        //public bool? AlevelAlarmLevelSingleEnableLoUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LO_UP_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LO_UP_EVENT_ENABLE, value); } }
        //public bool? AlevelAlarmLevelSingleEnableHiUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HI_UP_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_HI_UP_EVENT_ENABLE, value); } }
        //public bool? AlevelAlarmLevelSingleEnableHiHiUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HIHI_UP_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE, value); } }


        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableLoLoDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableLoDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LO_DOWN_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LO_DOWN_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableHiDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HI_DOWN_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_HI_DOWN_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableHiHiDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HIHI_DOWN_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_HIHI_DOWN_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableStepRelEmergency { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_STEPREL_EMERGENCY_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_STEPREL_EMERGENCY_EVENT_ENABLE, value); } }
        //public double? AlevelAlarmLevelSingleHisteresis { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_HYSTERESIS); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_HYSTERESIS, value); } }

        #endregion



        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableStepRel { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_STEPREL_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_STEPREL_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelSingleEnableStepAbs { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_STEPABS_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_STEPABS_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelSingleLoLo { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_LOLO_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_LOLO_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelSingleLo { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_LO_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_LO_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelSingleHi { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_HI_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_HI_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelSingleHiHi { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_HIHI_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_HIHI_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelSingleStepRel { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_STEPREL_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_STEPREL_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelSingleStepAbs { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_STEPABS_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_STEPABS_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelSingleBias { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_BIAS); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_BIAS, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgEnableLoLo { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_AVG_CONFIG_LOLO_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_LOLO_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgEnableLo { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_AVG_CONFIG_LO_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_LO_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgEnableHi { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_AVG_CONFIG_HI_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_HI_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgEnableHiHi { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_AVG_CONFIG_HIHI_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_HIHI_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgEnableStepRel { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_AVG_CONFIG_STEPREL_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_STEPREL_EVENT_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgEnableStepAbs { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_AVG_CONFIG_STEPABS_EVENT_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_STEPABS_EVENT_ENABLE, value); } }


        [XmlIgnore]
        public double? AlevelAlarmLevelAvgLoLo { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_LOLO_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_LOLO_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelAvgLo { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_LO_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_LO_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelAvgHi { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_HI_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_HI_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelAvgHiHi { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_HIHI_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_HIHI_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelAvgStepRel { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_STEPREL_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_STEPREL_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelAvgStepAbs { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_STEPABS_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_STEPABS_LEVEL, value); } }

        [XmlIgnore]
        public double? AlevelAlarmLevelAvgBias { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_BIAS); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_BIAS, value); } }

        #region NewEvents AVG
        //public bool? AlevelAlarmLevelAvgLoLoUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LOLO_UP_EVENT_ENABLE,7); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LOLO_UP_EVENT_ENABLE, 7, value); } }
        //public bool? AlevelAlarmLevelAvgLoUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LO_UP_EVENT_ENABLE, 7); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LO_UP_EVENT_ENABLE, 7, value); } }
        //public bool? AlevelAlarmLevelAvgHiUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HI_UP_EVENT_ENABLE, 7); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_HI_UP_EVENT_ENABLE, 7, value); } }
        //public bool? AlevelAlarmLevelAvgHiHiUP { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HIHI_UP_EVENT_ENABLE, 7); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE, 7, value); } }


        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgLoLoDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE, 7, null); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE, 7, value, null); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgLoDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_LO_DOWN_EVENT_ENABLE, 7, null); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_LO_DOWN_EVENT_ENABLE, 7, value, null); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgHiDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HI_DOWN_EVENT_ENABLE, 7, null); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_HI_DOWN_EVENT_ENABLE, 7, value, null); } }

        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgHiHiDOWM { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_HIHI_DOWN_EVENT_ENABLE, 7, null); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_HIHI_DOWN_EVENT_ENABLE, 7, value, null); } }
        //public double? AlevelAlarmLevelAvgStepRelN { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_STEPREL_LEVEL, 7); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_STEPREL_LEVEL, 7, value); } }
        //public double? AlevelAlarmLevelAvgStepAbsN { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_AVG_CONFIG_STEPABS_LEVEL, 7); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_AVG_CONFIG_STEPABS_LEVEL, 7, value); } }


        [XmlIgnore]
        public bool? AlevelAlarmLevelAvgEnableStepRelEmergency { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_ANALOG_CONFIG_STEPREL_EMERGENCY_EVENT_ENABLE, 7, null); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ANALOG_CONFIG_STEPREL_EMERGENCY_EVENT_ENABLE, 7, value, null); } }
        //public double? AlevelAlarmLevelAvgHisteresis { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.ALEVEL_CONFIG_HYSTERESIS, 7); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_HYSTERESIS, 7, value); } }
        #endregion


        [XmlIgnore]
        public bool? AlevelNVREnableUp { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_NVR_UP_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_NVR_UP_ENABLE, value); } }

        [XmlIgnore]
        public bool? AlevelNVREnableDown { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ALEVEL_CONFIG_NVR_DOWN_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.ALEVEL_CONFIG_NVR_DOWN_ENABLE, value); } }

        [XmlIgnore]
        public bool? DeviceDeadLinkEnableUp { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_WL_LINK_DEAD_UP_EVENT_ENABLED); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_WL_LINK_DEAD_UP_EVENT_ENABLED, value); } }

        [XmlIgnore]
        public bool? DeviceDeadLinkEnableDown { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_WL_LINK_DEAD_DOWN_EVENT_ENABLED); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_WL_LINK_DEAD_DOWN_EVENT_ENABLED, value); } }

        [XmlIgnore]
        public bool? DeviceLostLinkEnableUp { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_WL_LINK_LOST_UP_EVENT_ENABLED); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_WL_LINK_LOST_UP_EVENT_ENABLED, value); } }

        [XmlIgnore]
        public bool? DeviceLostLinkEnableDown { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_WL_LINK_LOST_DOWN_EVENT_ENABLED); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_WL_LINK_LOST_DOWN_EVENT_ENABLED, value); } }

        [XmlIgnore]
        public int? DeviceRapidWindowWidth { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.DEVICE_EVENT_RAPID_WINDOW_WIDTH); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_WINDOW_WIDTH, value); } }

        [XmlIgnore]
        public double? DeviceRapidTriggerLevel { get { return DataList.TryGetNullableValueForDefaultConfig<double>(DataType.DEVICE_EVENT_RAPID_TRIGGER_LEVEL); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_TRIGGER_LEVEL, value); } }

        [XmlIgnore]
        public bool? DeviceRapidEnableForStart { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_EVENT_RAPID_UP_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_UP_ENABLE, value); } }

        [XmlIgnore]
        public bool? DeviceRapidEnableForEnd { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_EVENT_RAPID_DOWN_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_DOWN_ENABLE, value); } }
        //public int? DeviceArchiveLatchPeriod { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.DEVICE_ARCHIVE_LATCH_PERIOD); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_ARCHIVE_LATCH_PERIOD, value); } }

        [XmlIgnore]
        public int? AnalogArchiveLatchOffset { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.ANALOG_ARCHIVE_LATCH_OFFSET); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_LATCH_OFFSET, value); } }

        [XmlIgnore]
        public int? AnalogArchiveRowsCount { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.ANALOG_ARCHIVE_READOUT_ROWS_COUNT); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_READOUT_ROWS_COUNT, value); } }
        //public bool? AnalogArchiveSingleEnableMeasurmentsCount { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_READOUT_MEASUMRENTS_COUNT_ENABLED); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_READOUT_MEASUMRENTS_COUNT_ENABLED, value); } }
        //public bool? AnalogArchiveAvgEnableMeasurmentsCount { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_READOUT_AVERAGE_MEASUMRENTS_COUNT_ENABLED); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_READOUT_AVERAGE_MEASUMRENTS_COUNT_ENABLED, value); } }

        [XmlIgnore]
        public int? AnalogArchiveLatchPeriod { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.ANALOG_ARCHIVE_LATCH_PERIOD); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_LATCH_PERIOD, value); } }

        [XmlIgnore]
        public bool? AnalogArchiveValuesMin { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_READOUT_MIN_ENABLED, 0, 10); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_READOUT_MIN_ENABLED, 0, value, 10); } }

        [XmlIgnore]
        public bool? AnalogArchiveValuesMax { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_READOUT_MAX_ENABLED, 0, 10); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_READOUT_MAX_ENABLED, 0, value, 10); } }

        [XmlIgnore]
        public bool? AnalogArchiveValuesAvg { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_READOUT_AVG_ENABLED, 0, 10); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_READOUT_AVG_ENABLED, 0, value, 10); } }

        [XmlIgnore]
        public int? AnalogArchiveReadoutType { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.ANALOG_ARCHIVE_DATA,1, 10); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_DATA,1, value, 10); } }

        [XmlIgnore]
        public bool? AnalogArchiveEnableMeasurmentsCount { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_DATA,2, 10); } set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_DATA,2, value, 10); } }

        [XmlIgnore]
        public bool? AnalogArchiveEnableLatching
        {
            get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_DATA, 3, 10); }
            set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_DATA, 3, value, 10); }
        }

        [XmlIgnore]
        public bool? AnalogArchiveSendInFreq
        {
            get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.ANALOG_ARCHIVE_DATA, 4, 10); }
            set { DataList.SetValueForDefaultConfig(DataType.ANALOG_ARCHIVE_DATA, 4, value, 10); }
        }

        [XmlIgnore]
        public int? ArchivePackagesCount { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.DEVICE_COMPACT_ANALOG_ARCHIVE_DEFAULT_ITEMS_COUNT); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_COMPACT_ANALOG_ARCHIVE_DEFAULT_ITEMS_COUNT, value); } }


        [XmlIgnore]
        public bool? RapidDropUp { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_EVENT_RAPID_DROP_UP_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_DROP_UP_ENABLE, value); } }

        [XmlIgnore]
        public bool? RapidDropDown { get { return DataList.TryGetNullableValueForDefaultConfig<bool>(DataType.DEVICE_EVENT_RAPID_DROP_DOWN_ENABLE); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_DROP_DOWN_ENABLE, value); } }

        [XmlIgnore]
        public int? RapidWindowHandle { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.DEVICE_EVENT_RAPID_WINDOW_WIDTH, 1, null); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_WINDOW_WIDTH, 1, value, null); } }

        [XmlIgnore]
        public int? RapidValidityWindowsCount { get { return DataList.TryGetNullableValueForDefaultConfig<int>(DataType.DEVICE_EVENT_RAPID_VALIDITY_WINDOWS_COUNT); } set { DataList.SetValueForDefaultConfig(DataType.DEVICE_EVENT_RAPID_VALIDITY_WINDOWS_COUNT, value); } }
       
        
    #endregion

    #region	Ctor
        public OpConfigurationProfile()
            : base() { this.DataList = new OpDataList<OpConfigurationProfileData>(); }

        public OpConfigurationProfile(DB_CONFIGURATION_PROFILE clone)
            : base(clone) { this.DataList = new OpDataList<OpConfigurationProfileData>(); }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpConfigurationProfile> ConvertList(DB_CONFIGURATION_PROFILE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpConfigurationProfile> ConvertList(DB_CONFIGURATION_PROFILE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpConfigurationProfile> ret = new List<OpConfigurationProfile>(db_objects.Length);
            db_objects.ToList().ForEach(db_object =>
                                            {
                                                //ret.Add(new OpConfigurationProfile(db_object));
                                                OpConfigurationProfile configurationProfile = new OpConfigurationProfile(db_object);
                                                configurationProfile.DataList = dataProvider.GetConfigurationProfileDataFilter(IdConfigurationProfile: new long[] { configurationProfile.IdConfigurationProfile }).ToOpDataList();
                                                ret.Add(configurationProfile);
                                            });

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConfigurationProfile> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpConfigurationProfileType> ConfigurationProfileTypeDict = dataProvider.GetConfigurationProfileType(list.Select(l => l.ID_CONFIGURATION_PROFILE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_CONFIGURATION_PROFILE_TYPE);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_CREATED).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.ConfigurationProfileType = ConfigurationProfileTypeDict.TryGetValue(loop.ID_CONFIGURATION_PROFILE_TYPE);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.OperatorCreated = OperatorDict.TryGetValue(loop.IdOperatorCreated);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConfigurationProfile> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConfigurationProfile)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConfigurationProfile).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpConfigurationProfile> Members
        public bool Equals(OpConfigurationProfile other)
        {
            if (other == null)
                return false;
            return this.IdConfigurationProfile.Equals(other.IdConfigurationProfile);
        }
    #endregion

    #region IReferenceType Members
        public object GetReferenceKey()
        {
            return this.IdConfigurationProfile;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpConfigurationProfile)
                return this.IdConfigurationProfile == ((OpConfigurationProfile)obj).IdConfigurationProfile;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpConfigurationProfile left, OpConfigurationProfile right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpConfigurationProfile left, OpConfigurationProfile right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdConfigurationProfile.GetHashCode();
        }
    #endregion
    }

    public static class ConfigurationProfileExtensions
    {
        public static void SetValueForDefaultConfig(this OpDataList<OpConfigurationProfileData> dataList, long idDataType, int index, object value, int? step = null)
        {
            OpConfigurationProfileData item = dataList.Find(d => d.IdDataType == idDataType && d.Index == index && d.IdDeviceType == 0 && d.IdDeviceTypeProfileStep == step);
            if (item != null)
            {
                if (!OpDataUtil.ValueEquals(item.Value, value))
                {
                    item.Value = value;
                    if (item.OpState != OpChangeState.New && item.OpState != OpChangeState.Delete)
                    {
                        item.OpState = OpChangeState.Modified;
                    }
                }
            }
            else
                dataList.Add(new OpConfigurationProfileData() { IdDeviceType = 0, IdDataType = idDataType, Index = index, Value = value, OpState = OpChangeState.New, IdDeviceTypeProfileStep = step});
        }
        public static void SetValueForDefaultConfig(this OpDataList<OpConfigurationProfileData> dataList, long idDataType, object value, int? step = null)
        {
            SetValueForDefaultConfig(dataList, idDataType, 0, value,step);
        }
        public static Nullable<T> GetNullableValueForDefaultConfig<T>(this OpDataList<OpConfigurationProfileData> dataList, long idDataType, int index, int? step = null) where T : struct, IConvertible
        {
            object value = dataList.Find(d => d.IdDataType == idDataType && d.Index == index && d.IdDeviceType == 0 && d.IdDeviceTypeProfileStep == step).Value;
            if (value == null)
                return null;
            else
                return GenericConverter.Parse<T>(value);
        }
        public static Nullable<T> TryGetNullableValueForDefaultConfig<T>(this OpDataList<OpConfigurationProfileData> dataList, long idDataType, int? step = null) where T : struct, IConvertible
        {
            return TryGetNullableValueForDefaultConfig<T>(dataList, idDataType, 0,step);
        }
        public static Nullable<T> TryGetNullableValueForDefaultConfig<T>(this OpDataList<OpConfigurationProfileData> dataList, long idDataType, int index, int? step = null) where T : struct, IConvertible
        {
            if (dataList.Exists(d => d.IdDataType == idDataType && d.Index == index && d.IdDeviceType == 0 && d.IdDeviceTypeProfileStep == step))
                return GetNullableValueForDefaultConfig<T>(dataList, idDataType, index, step);
            else
                return null;
        }
    }
#endif
}