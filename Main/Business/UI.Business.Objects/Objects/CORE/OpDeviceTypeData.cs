using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTypeData : DB_DEVICE_TYPE_DATA, IOpData, IOpDataProvider, IComparable
	{
    #region Properties
        [XmlIgnore]
        public long IdDeviceTypeData { get { return this.ID_DEVICE_TYPE_DATA; } set { this.ID_DEVICE_TYPE_DATA = value; } }
        [XmlIgnore]
        public int IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = value; } }
        [XmlIgnore]
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
		public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpDeviceType _DeviceType;
        [XmlIgnore]
		public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.ID_DEVICE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
		private OpDataType _DataType;
        [XmlIgnore]
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdDeviceTypeData; } set { IdDeviceTypeData = (long)value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpDeviceTypeData()
			: base() { this.OpState = OpChangeState.New; }

		public OpDeviceTypeData(DB_DEVICE_TYPE_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "DeviceTypeData [" + IdDeviceType.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpDeviceTypeData> ConvertList(DB_DEVICE_TYPE_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpDeviceTypeData> ConvertList(DB_DEVICE_TYPE_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceTypeData> ret = new List<OpDeviceTypeData>(list.Length);
			foreach (var loop in list)
			{
				OpDeviceTypeData insert = new OpDeviceTypeData(loop);
				if (loadNavigationProperties)
				{
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}
				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceTypeData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.DeviceType = dataProvider.GetDeviceType(this.ID_DEVICE_TYPE);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDeviceTypeData clone = new OpDeviceTypeData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEVICE_TYPE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpDeviceTypeData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpDeviceTypeData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDeviceTypeData)
				return this.IdData == ((OpDeviceTypeData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceTypeData left, OpDeviceTypeData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceTypeData left, OpDeviceTypeData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}