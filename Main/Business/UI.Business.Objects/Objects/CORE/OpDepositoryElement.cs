using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpDepositoryElement : DB_DEPOSITORY_ELEMENT, IComparable, IEquatable<OpDepositoryElement>, IOpDynamic, IOpDynamicProperty<OpDepositoryElement>, IOpChangeState, IReferenceType
    {
        #region Properties
        [XmlIgnore]
        public int IdDepositoryElement { get { return this.ID_DEPOSITORY_ELEMENT; } set { this.ID_DEPOSITORY_ELEMENT = value; } }
        [XmlIgnore]
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        [XmlIgnore]
        public int? IdTask { get { return this.ID_TASK; } set { this.ID_TASK = value; } }
        [XmlIgnore]
        public int? IdPackage { get { return this.ID_PACKAGE; } set { this.ID_PACKAGE = value; } }
        [XmlIgnore]
        public long IdArticle { get { return this.ID_ARTICLE; } set { this.ID_ARTICLE = value; } }
        [XmlIgnore]
        public int IdDeviceTypeClass { get { return this.ID_DEVICE_TYPE_CLASS; } set { this.ID_DEVICE_TYPE_CLASS = value; } }
        [XmlIgnore]
        public int IdDeviceStateType { get { return this.ID_DEVICE_STATE_TYPE; } set { this.ID_DEVICE_STATE_TYPE = value; } }
        [XmlIgnore]
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        [XmlIgnore]
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
        [XmlIgnore]
        public bool Removed { get { return this.REMOVED; } set { this.REMOVED = value; } }
        [XmlIgnore]
        public bool Ordered { get { return this.ORDERED; } set { this.ORDERED = value; } }
        [XmlIgnore]
        public bool? Accepted { get { return this.ACCEPTED; } set { this.ACCEPTED = value; } }
        [XmlIgnore]
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
        [XmlIgnore]
        public string ExternalSn { get { return this.EXTERNAL_SN; } set { this.EXTERNAL_SN = value; } }
        [XmlIgnore]
        public int? IdReference { get { return this.ID_REFERENCE; } set { this.ID_REFERENCE = value; } }

        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpLocation _Location;
        [XmlIgnore]
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }

        [DataMember]
        private OpTask _Task;
        [XmlIgnore]
        public OpTask Task { get { return this._Task; } set { this._Task = value; this.ID_TASK = (value == null) ? null : (int?)value.ID_TASK; } }

        [DataMember]
        private OpPackage _Package;
        [XmlIgnore]
        public OpPackage Package { get { return this._Package; } set { this._Package = value; this.ID_PACKAGE = (value == null) ? null : (int?)value.ID_PACKAGE; } }

        [DataMember]
        private OpArticle _Article;
        [XmlIgnore]
        public OpArticle Article { get { return this._Article; } set { this._Article = value; this.ID_ARTICLE = value.ID_ARTICLE; } }

        [DataMember]
        private OpDeviceTypeClass _DeviceTypeClass;
        [XmlIgnore]
        public OpDeviceTypeClass DeviceTypeClass { get { return this._DeviceTypeClass; } set { this._DeviceTypeClass = value; this.ID_DEVICE_TYPE_CLASS = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE_CLASS; } }

        [DataMember]
        private OpDeviceStateType _DeviceStateType;
        [XmlIgnore]
        public OpDeviceStateType DeviceStateType { get { return this._DeviceStateType; } set { this._DeviceStateType = value; this.ID_DEVICE_STATE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_STATE_TYPE; } }

        [DataMember]
        private OpDevice _Device;
        [XmlIgnore]
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }

        [DataMember]
        private OpDeviceDetails _DeviceDetails;
        [XmlIgnore]
        public OpDeviceDetails DeviceDetails { get { return this._DeviceDetails; } set { this._DeviceDetails = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }
        #endregion

        #region	Custom Properties
        [DataMember]
        public OpDataList<OpDepositoryElementData> DataList { get; set; }
        [DataMember]
        public int Count { get; set; }

        [XmlIgnore]
        public OpLocation CustomLocation
        {
            get
            {
                if (Location != null)
                    return Location;
                if (Task != null)
                    return Task.Location;
                else
                    return null;
            }
        }
        [DataMember]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;

    #endregion

    #region	Ctor
        public OpDepositoryElement()
            : base()
        {
            this.DataList = new OpDataList<OpDepositoryElementData>();
        }

        public OpDepositoryElement(DB_DEPOSITORY_ELEMENT clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpDepositoryElementData>();
        }

        public OpDepositoryElement(OpDepositoryElement clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpDepositoryElementData>();
			if(clone.DataList != null)
			{
                foreach (OpDepositoryElementData ddItem in clone.DataList)
				{
                    this.DataList.Add(new OpDepositoryElementData(ddItem));
				}
			}
            if(this.Location != null)
                this.Location = clone.Location;
            if(this.Package != null)
                this.Package = clone.Package;
            if(this.Task != null)
                this.Task = clone.Task;
            if(this.Article != null)
                this.Article = clone.Article;
            if(this.Device != null)
                this.Device = clone.Device;
           // this.OpState = clone.OpState;
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        } 
        public OpDepositoryElement(IDataProvider dataProvider, int? idPackage, long idArticle, OpDeviceTypeClass.Enum deviceTypeClass,
            OpDeviceStateType.Enum deviceStateType, long? serialNbr, string externalSN, bool removed, string notes)
            : this(dataProvider, null, null, idPackage, idArticle, deviceTypeClass, deviceStateType, serialNbr, externalSN, removed, false, null, notes, null) { }

        public OpDepositoryElement(IDataProvider dataProvider, long? idlocation, long idArticle, OpDeviceTypeClass.Enum deviceTypeClass,
            OpDeviceStateType.Enum deviceStateType, long? serialNbr, string externalSN, bool removed, string notes)
            : this(dataProvider, idlocation, null, null, idArticle, deviceTypeClass, deviceStateType, serialNbr, externalSN, removed, false, null, notes, null) { }

        public OpDepositoryElement(IDataProvider dataProvider, int? idTask, long idArticle, OpDeviceTypeClass.Enum deviceTypeClass,
            OpDeviceStateType.Enum deviceStateType, long? serialNbr, bool ordered, bool removed)
            : this(dataProvider, null, idTask, null, idArticle, deviceTypeClass, deviceStateType, serialNbr, null, removed, ordered, null, null, null) { }

        public OpDepositoryElement(IDataProvider dataProvider, int? idTask, long idArticle, OpDeviceTypeClass.Enum deviceTypeClass,
            OpDeviceStateType.Enum deviceStateType, long? serialNbr, string externalSN, bool ordered, bool removed, string notes)
            : this(dataProvider, null, idTask, null, idArticle, deviceTypeClass, deviceStateType, serialNbr, externalSN, removed, ordered, null, notes, null) { }

        public OpDepositoryElement(IDataProvider dataProvider, int? idPackage, long? idLocation, long idArticle, OpDeviceTypeClass.Enum deviceTypeClass,
            OpDeviceStateType.Enum deviceStateType, long? serialNbr, bool removed)
            : this(dataProvider, idLocation, null, idPackage, idArticle, deviceTypeClass, deviceStateType, serialNbr, null, removed, false, null, null, null) { }

        public OpDepositoryElement(IDataProvider dataProvider, long? idLocation, int? idTask, int? idPackage, long idArticle, OpDeviceTypeClass.Enum deviceTypeClass,
            OpDeviceStateType.Enum deviceStateType, long? serialNbr, string externalSN, bool removed, bool ordered, bool? accepted, string notes, int? id_reference)
            : base()
        {
            this.Location = idLocation.HasValue ? dataProvider.GetLocation(idLocation.Value) : null;
            this.Task = idTask.HasValue ? dataProvider.GetTask(idTask.Value) : null;
            this.Package = idPackage.HasValue ? dataProvider.GetPackage(idPackage.Value) : null;
            this.Article = dataProvider.GetArticle(idArticle);
            this.DeviceTypeClass = dataProvider.GetDeviceTypeClass((int)deviceTypeClass);
            this.DeviceStateType = dataProvider.GetDeviceStateType((int)deviceStateType);
            this.Device = serialNbr.HasValue ? dataProvider.GetDevice(serialNbr.Value) : null;
            this.ExternalSn = externalSN;
            // this.Count = count;
            this.IdReference = id_reference;
            this.Removed = removed;
            this.Ordered = ordered;
            this.Accepted = accepted;
            this.Notes = notes;
            this.StartDate = dataProvider.DateTimeNow;
            this.DataList = new OpDataList<OpDepositoryElementData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region	ConvertList
        //public static List<OpDepositoryElement> ConvertList(DB_DEPOSITORY_ELEMENT[] list, IDataProvider dataProvider)
        //{
        //    return ConvertList(list, dataProvider, true);
        //}

        //public static List<OpDepositoryElement> ConvertList(DB_DEPOSITORY_ELEMENT[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        //{
        //    List<OpDepositoryElement> ret = new List<OpDepositoryElement>(list.Length);
        //    foreach (var loop in list)
        //    {
        //        OpDepositoryElement insert = new OpDepositoryElement(loop);

        //        if (loadNavigationProperties)
        //        {
        //            if (loop.ID_LOCATION.HasValue && dataProvider.LocationCacheEnabled)
        //                insert.Location = dataProvider.GetLocation(loop.ID_LOCATION.Value);

        //            //if (loop.ID_TASK.HasValue)
        //            //    insert.Task = dataProvider.GetTask(loop.ID_TASK.Value);

        //            //if (loop.ID_PACKAGE.HasValue)
        //            //    insert.Package = dataProvider.GetPackage(loop.ID_PACKAGE.Value);

        //            if (loop.SERIAL_NBR.HasValue /*&& dataProvider.DeviceCacheEnabled*/)
        //                insert.Device = dataProvider.GetDevice(loop.SERIAL_NBR.Value);

        //            insert.Article = dataProvider.GetArticle(loop.ID_ARTICLE);
        //            insert.DeviceTypeClass = dataProvider.GetDeviceTypeClass(loop.ID_DEVICE_TYPE_CLASS);
        //            insert.DeviceStateType = dataProvider.GetDeviceStateType(loop.ID_DEVICE_STATE_TYPE);
        //        }

        //        ret.Add(insert);
        //    }

        //    LoadCustomData(ref ret, dataProvider); // Loads user custom data
        //    return ret;
        //}
    #endregion

    #region	ConvertList
        public static List<OpDepositoryElement> ConvertList(DB_DEPOSITORY_ELEMENT[] db_objects, IDataProvider dataProvider, bool loadDeviceDetails = false)
        {
            return ConvertList(db_objects, dataProvider, true, loadDeviceDetails);
        }


        public static List<OpDepositoryElement> ConvertList(DB_DEPOSITORY_ELEMENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadDeviceDetails = false)
        {
            List<OpDepositoryElement> ret = new List<OpDepositoryElement>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDepositoryElement(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, loadDeviceDetails); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        const string cTask = "Task"; // TO DO: zamienic na MdDepositoryElement.Task
        const string cLocation = "Location"; // TO DO: zamienic na MdDepositoryElement.Location
        const string cDevice = "Device"; // TO DO: zamienic na MdDepositoryElement.Device
        const string cArticle = "Article"; // TO DO: zamienic na MdDepositoryElement.Article
        const string cDeviceTypeClass = "DeviceTypeClass"; // TO DO: zamienic na MdDepositoryElement.DeviceTypeClass
        const string cDeviceStateType = "DeviceStateType"; // TO DO: zamienic na MdDepositoryElement.DeviceStateType
        const string cPackage = "Package"; // TO DO: zamienic na MdDepositoryElement.Package
        public static void LoadNavigationProperties(ref List<OpDepositoryElement> list, IDataProvider dataProvider, bool loadDeviceDetails = false)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTask> TaskDict = null;
                Dictionary<long, OpLocation> LocationDict = null;
                Dictionary<long, OpDevice> DeviceDict = null;
                Dictionary<long, OpDeviceDetails> DeviceDetailsDict = null;
                Dictionary<long, OpArticle> ArticleDict = null;
                Dictionary<int, OpDeviceTypeClass> DeviceTypeClassDict = null;
                Dictionary<int, OpDeviceStateType> DeviceStateTypeDict = null;
                Dictionary<int, OpPackage> PackageDict = null;

                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpDepositoryElement), cTask))
                    TaskDict = dataProvider.GetTask(list.Where(l => l.ID_TASK.HasValue).Select(l => l.ID_TASK.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TASK);
                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpDepositoryElement), cLocation))
                    LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpDepositoryElement), cDevice))
                {
                    if(!loadDeviceDetails)
                        DeviceDict = dataProvider.GetDevice(list.Where(l => l.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);
                    else
                        DeviceDetailsDict = dataProvider.GetDeviceDetailsCustom(list.Where(l => l.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR.Value);
                }
                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpDepositoryElement), cArticle))
                    ArticleDict = dataProvider.GetArticle(list.Select(l => l.ID_ARTICLE).Distinct().ToArray()).ToDictionary(l => l.ID_ARTICLE);
                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpDepositoryElement), cDeviceTypeClass))
                    DeviceTypeClassDict = dataProvider.GetDeviceTypeClass(list.Select(l => l.ID_DEVICE_TYPE_CLASS).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE_CLASS);
                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpDepositoryElement), cDeviceStateType))
                    DeviceStateTypeDict = dataProvider.GetDeviceStateType(list.Select(l => l.ID_DEVICE_STATE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_STATE_TYPE);
                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpDepositoryElement), cPackage))
                    PackageDict = dataProvider.GetPackage(list.Where(l => l.ID_PACKAGE.HasValue).Select(l => l.ID_PACKAGE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_PACKAGE);

                foreach (var loop in list)
                {
                    if (loop.ID_LOCATION.HasValue && (LocationDict != null || (dataProvider.LocationCacheEnabled && LocationDict != null))) //drugi warunek dla zachowania kompatybilności wstecz
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    if (loop.SERIAL_NBR.HasValue && ((loadDeviceDetails && DeviceDetailsDict != null) || (!loadDeviceDetails && DeviceDict != null)))
                    {
                        if (!loadDeviceDetails)
                            loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR.Value);
                        else
                            loop.DeviceDetails = DeviceDetailsDict.TryGetValue(loop.SERIAL_NBR.Value);
                    }
                    if (loop.ID_TASK.HasValue && TaskDict != null)
                        loop.Task = TaskDict.TryGetValue(loop.ID_TASK.Value);
                    if (loop.ID_PACKAGE.HasValue && PackageDict != null)
                        loop.Package = PackageDict.TryGetValue(loop.ID_PACKAGE.Value);
                    if (ArticleDict != null)
                        loop.Article = ArticleDict.TryGetValue(loop.ID_ARTICLE);
                    if (DeviceTypeClassDict != null)
                        loop.DeviceTypeClass = DeviceTypeClassDict.TryGetValue(loop.ID_DEVICE_TYPE_CLASS);
                    if (DeviceStateTypeDict != null)
                        loop.DeviceStateType = DeviceStateTypeDict.TryGetValue(loop.ID_DEVICE_STATE_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDepositoryElement> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0 && dataProvider.IDepositoryElementDataTypes != null)
            {
                List<int> idDepositoryElement = list.Select(l => l.IdDepositoryElement).ToList();

                List<OpDepositoryElementData> data = dataProvider.GetDepositoryElementDataFilter(IdDepositoryElement: idDepositoryElement.ToArray(), IdDataType: dataProvider.IDepositoryElementDataTypes.ToArray());
                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpDepositoryElement> tasksDict = list.ToDictionary<OpDepositoryElement, int>(l => l.IdDepositoryElement);
                    foreach (var dataValue in data)
                    {
                        tasksDict[dataValue.IdDepositoryElement].DataList.Add(dataValue);
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDepositoryElement)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDepositoryElement).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDepositoryElement> Members
        public bool Equals(OpDepositoryElement other)
        {
            if (other == null)
                return false;
            return this.IdDepositoryElement.Equals(other.IdDepositoryElement);
        }
    #endregion

    #region IOpDynamic

        [XmlIgnore]
        public object Key
        {
            get { return IdDepositoryElement; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return this.dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }

    #endregion

    #region IOpDynamicProperty<OpDepositoryElement>

        OpDepositoryElement IOpDynamicProperty<OpDepositoryElement>.Owner
        {
            get { return this; }
        }

    #endregion

    #region IReferenceType

        public object GetReferenceKey()
        {
            return ID_DEPOSITORY_ELEMENT;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
    #endregion
    }
#endif
}