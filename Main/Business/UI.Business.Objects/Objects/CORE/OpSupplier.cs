using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpSupplier : DB_SUPPLIER, IComparable, IEquatable<OpSupplier>, IReferenceType
    {
    #region Properties
            public int IdSupplier { get { return this.ID_SUPPLIER;} set {this.ID_SUPPLIER = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpSupplier()
			:base() {}
		
		public OpSupplier(DB_SUPPLIER clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpSupplier> ConvertList(DB_SUPPLIER[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpSupplier> ConvertList(DB_SUPPLIER[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSupplier> ret = new List<OpSupplier>(list.Length);
			foreach (var loop in list)
			{
			OpSupplier insert = new OpSupplier(loop);
				
				if(loadNavigationProperties)
				{
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSupplier> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSupplier)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSupplier).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpSupplier> Members
        public bool Equals(OpSupplier other)
        {
            if (other == null)
				return false;
			return this.IdSupplier.Equals(other.IdSupplier);
        }
    #endregion

    #region IReferenceType

        public object GetReferenceKey()
        {
            return this.IdSupplier;
        }

        public object GetReferenceValue()
        {
            return this;
        }

    #endregion
    }
#endif
}