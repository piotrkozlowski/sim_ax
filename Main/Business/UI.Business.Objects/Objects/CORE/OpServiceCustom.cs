﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceCustom : DB_SERVICE_CUSTOM, IComparable, IEquatable<OpServiceCustom>
    {
    #region Properties
        public long IdServiceCustom { get { return this.ID_SERVICE_CUSTOM; } set { this.ID_SERVICE_CUSTOM = value; } }
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public Double? EditorCost { get { return this.EDITOR_COST; } set { this.EDITOR_COST = value; } }
        public Double? EngineerCost { get { return this.ENGINEER_COST; } set { this.ENGINEER_COST = value; } }
        public Double? DirectCost { get { return this.DIRECT_COST; } set { this.DIRECT_COST = value; } }
        public int? IndirectCost { get { return this.INDIRECT_COST; } set { this.INDIRECT_COST = value; } }
        public Double? MaterialCost { get { return this.MATERIAL_COST; } set { this.MATERIAL_COST = value; } }
        public Double? SupplyCost { get { return this.SUPPLY_COST; } set { this.SUPPLY_COST = value; } }
        public Double? TransportCost { get { return this.TRANSPORT_COST; } set { this.TRANSPORT_COST = value; } }
        public long? IdFile { get { return this.ID_FILE; } set { this.ID_FILE = value; } }
        public bool? CustomValue { get { return this.CUSTOM_VALUE; } set { this.CUSTOM_VALUE = value; } }
        public Double? EditorTime { get { return this.EDITOR_TIME; } set { this.EDITOR_TIME = value; } }
        public Double? EngineerTime { get { return this.ENGINEER_TIME; } set { this.ENGINEER_TIME = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? 0 : (int)value.ID_SERVICE_LIST; } }
        private OpFile _File;
        public OpFile File { get { return this._File; } set { this._File = value; this.ID_FILE = (value == null) ? null : (long?)value.ID_FILE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpServiceCustom()
            : base() { }

        public OpServiceCustom(DB_SERVICE_CUSTOM clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceCustom [" + IdServiceCustom.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceCustom> ConvertList(DB_SERVICE_CUSTOM[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceCustom> ConvertList(DB_SERVICE_CUSTOM[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceCustom> ret = new List<OpServiceCustom>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceCustom(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceCustom> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Select(l => l.ID_SERVICE_LIST).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<long, OpFile> FileDict = dataProvider.GetFile(list.Where(l => l.ID_FILE.HasValue).Select(l => l.ID_FILE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_FILE);

                foreach (var loop in list)
                {
                    loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST);
                    if (loop.ID_FILE.HasValue)
                        loop.File = FileDict.TryGetValue(loop.ID_FILE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceCustom> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceCustom)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceCustom).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceCustom> Members
        public bool Equals(OpServiceCustom other)
        {
            if (other == null)
                return false;
            return this.IdServiceCustom.Equals(other.IdServiceCustom);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceCustom)
                return this.IdServiceCustom == ((OpServiceCustom)obj).IdServiceCustom;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceCustom left, OpServiceCustom right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceCustom left, OpServiceCustom right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceCustom.GetHashCode();
        }
    #endregion
    }
#endif
}