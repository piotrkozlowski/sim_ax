using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceType : DB_DEVICE_TYPE, IComparable, IReferenceType, IOpChangeState
    {
    #region Properties
        [XmlIgnore]
        public int IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = OpDataUtil.SetNewIndex(this.ID_DEVICE_TYPE, value, this); } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = OpDataUtil.SetNewValue(this.NAME, value, this); } }
        [XmlIgnore]
        public int IdDeviceTypeClass { get { return this.ID_DEVICE_TYPE_CLASS; } set { this.ID_DEVICE_TYPE_CLASS = OpDataUtil.SetNewIndex(this.ID_DEVICE_TYPE_CLASS, value, this); } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = OpDataUtil.SetNewValue(this.ID_DESCR, value, this); } }
        [XmlIgnore]
        public bool TwoWayTransAvailable { get { return this.TWO_WAY_TRANS_AVAILABLE; } set { this.TWO_WAY_TRANS_AVAILABLE = OpDataUtil.SetNewValue(this.TWO_WAY_TRANS_AVAILABLE, value, this); } }
        [XmlIgnore]
        public int? IdDeviceDriver { get { return this.ID_DEVICE_DRIVER; } set { this.ID_DEVICE_DRIVER = OpDataUtil.SetNewValue(this.ID_DEVICE_DRIVER, value, this); } }
        [XmlIgnore]
        public int? IdProtocol { get { return this.ID_PROTOCOL; } set { this.ID_PROTOCOL = OpDataUtil.SetNewValue(this.ID_PROTOCOL, value, this); } }
        [XmlIgnore]
        public int? DefaultIdDeviceOderNumber { get { return this.DEFAULT_ID_DEVICE_ODER_NUMBER; } set { this.DEFAULT_ID_DEVICE_ODER_NUMBER = OpDataUtil.SetNewValue(this.DEFAULT_ID_DEVICE_ODER_NUMBER, value, this); } }
        //Projekt: DEVICE_TYPE bez FRAME_DEFINITION
        //[XmlIgnore]
        //public object FrameDefinition { get { return this.FRAME_DEFINITION; } set { this.FRAME_DEFINITION = OpDataUtil.SetNewValue(this.FRAME_DEFINITION, value, this); } }
    #endregion

    #region	Navigation Properties
        private OpDeviceTypeClass _DeviceTypeClass;
        [XmlIgnore]
        public OpDeviceTypeClass DeviceTypeClass { get { return this._DeviceTypeClass; } set { this._DeviceTypeClass = value; this.IdDeviceTypeClass = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE_CLASS; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpDeviceDriver _DeviceDriver;
        [XmlIgnore]
        public OpDeviceDriver DeviceDriver { get { return this._DeviceDriver; } set { this._DeviceDriver = value; this.IdDeviceDriver = (value == null) ? null : (int?)value.ID_DEVICE_DRIVER; } }
        private OpProtocol _Protocol;
        [XmlIgnore]
        public OpProtocol Protocol { get { return this._Protocol; } set { this._Protocol = value; this.IdProtocol = (value == null) ? null : (int?)value.ID_PROTOCOL; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
        public OpDataList<OpDeviceTypeData> DataList { get; set; }
        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpDeviceType()
            : base() 
        {
            OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpDeviceTypeData>();
        }

        public OpDeviceType(DB_DEVICE_TYPE clone)
            : base(clone) 
        {
            OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpDeviceTypeData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (!String.IsNullOrEmpty(this.Name))
                return this.Name;

            if (Descr != null)
                return Descr.Description;

            return "[unknown]";

        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceType> ConvertList(DB_DEVICE_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceType> ConvertList(DB_DEVICE_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDeviceType> ret = new List<OpDeviceType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceTypeClass> DeviceTypeClassDict = dataProvider.GetDeviceTypeClass(list.Select(l => l.ID_DEVICE_TYPE_CLASS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_TYPE_CLASS);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpDeviceDriver> DeviceDriverDict = dataProvider.GetDeviceDriver(list.Where(l => l.ID_DEVICE_DRIVER.HasValue).Select(l => l.ID_DEVICE_DRIVER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_DRIVER);
                Dictionary<int, OpProtocol> ProtocolDict = dataProvider.GetProtocol(list.Where(l => l.ID_PROTOCOL.HasValue).Select(l => l.ID_PROTOCOL.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PROTOCOL);

                foreach (var loop in list)
                {
                    loop.DeviceTypeClass = DeviceTypeClassDict.TryGetValue(loop.ID_DEVICE_TYPE_CLASS);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    if (loop.ID_DEVICE_DRIVER.HasValue)
                        loop.DeviceDriver = DeviceDriverDict.TryGetValue(loop.ID_DEVICE_DRIVER.Value);
                    if (loop.ID_PROTOCOL.HasValue)
                        loop.Protocol = ProtocolDict.TryGetValue(loop.ID_PROTOCOL.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> ids = list.Select(l => l.IdDeviceType).ToList();

                List<OpDeviceTypeData> data = dataProvider.GetDeviceTypeDataFilter(IdDeviceType: ids.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);//, IdDataType: DataProvider.DataTypes.ToArray());
                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpDeviceType> DeviceTypeDict = list.ToDictionary<OpDeviceType, int>(l => l.IdDeviceType);
                    foreach (var dataValue in data)
                    {
                        DeviceTypeDict[dataValue.IdDeviceType].DataList.Add(dataValue);
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdDeviceType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceType)
                return this.IdDeviceType == ((OpDeviceType)obj).IdDeviceType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceType left, OpDeviceType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceType left, OpDeviceType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceType.GetHashCode();
        }
    #endregion
    }
#endif
}