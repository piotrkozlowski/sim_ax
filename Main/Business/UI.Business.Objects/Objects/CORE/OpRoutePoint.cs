using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpRoutePoint : DB_ROUTE_POINT, IComparable
    {
    #region Properties
        public int IdRoutePoint { get { return this.ID_ROUTE_POINT; } set { this.ID_ROUTE_POINT = value; } }
        public long IdRoute { get { return this.ID_ROUTE; } set { this.ID_ROUTE = value; } }
        public int Type { get { return this.TYPE; } set { this.TYPE = value; } }
        public int? IdTask { get { return this.ID_TASK; } set { this.ID_TASK = value; } }
        public DateTime? StartDateTime { get { return this.START_DATE_TIME; } set { this.START_DATE_TIME = value; } }
        public DateTime? EndDateTime { get { return this.END_DATE_TIME; } set { this.END_DATE_TIME = value; } }
        public Double Latitude { get { return this.LATITUDE; } set { this.LATITUDE = value; } }
        public Double Longitude { get { return this.LONGITUDE; } set { this.LONGITUDE = value; } }
        public int? KmCounter { get { return this.KM_COUNTER; } set { this.KM_COUNTER = value; } }
        public Double? Quote { get { return this.QUOTE; } set { this.QUOTE = value; } }
        public string InvoiceNumber { get { return this.INVOICE_NUMBER; } set { this.INVOICE_NUMBER = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
        public object RowVersion { get { return this.ROW_VERSION; } set { this.ROW_VERSION = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
    #endregion

    #region	Navigation Properties
        private OpRoute _Route;
        public OpRoute Route { get { return this._Route; } set { this._Route = value; this.ID_ROUTE = (value == null) ? 0 : (long)value.ID_ROUTE; } }
        private OpTask _Task;
        public OpTask Task { get { return this._Task; } set { this._Task = value; this.ID_TASK = (value == null) ? null : (int?)value.ID_TASK; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
        public string TypeString
        {
            get
            {
                switch ((RoutePointType)this.Type)
                {
                    case RoutePointType.Service:
                        return ResourcesText.ObjectIntervention;// "Interwencja obiektowa";
                    case RoutePointType.Accomodation:
                        return ResourcesText.Accomoodation;//"Nocleg";
                    default:
                        return ResourcesText.Unknown;//"Unknown";
                }
            }
        }
        public OpDataList<OpRoutePointData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpRoutePoint()
            : base()
        {
            DataList = new OpDataList<OpRoutePointData>();
        }

        public OpRoutePoint(DB_ROUTE_POINT clone)
            : base(clone)
        {
            DataList = new OpDataList<OpRoutePointData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.INVOICE_NUMBER;
        }
    #endregion

    #region	ConvertList
        public static List<OpRoutePoint> ConvertList(DB_ROUTE_POINT[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpRoutePoint> ConvertList(DB_ROUTE_POINT[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true)
        {
            List<OpRoutePoint> ret = new List<OpRoutePoint>(list.Length);
            list.ToList().ForEach(db_item => ret.Add(new OpRoutePoint(db_item)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if(loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;

    #region Old
            //List<OpRoutePoint> ret = new List<OpRoutePoint>(list.Length);

            //foreach (var loop in list)
            //{
            //    OpRoutePoint insert = new OpRoutePoint(loop);

            //    if (loadNavigationProperties)
            //    {
            //        //if (dataProvider.RouteCacheEnabled)
            //        //    insert.Route = dataProvider.GetRoute(loop.ID_ROUTE);

            //        if (dataProvider.TaskCacheEnabled && loop.ID_TASK.HasValue)
            //            insert.Task = dataProvider.GetTask(loop.ID_TASK.Value);
            //    }
            //    ret.Add(insert);
            //}

            //LoadCustomData(ref ret, dataProvider); // Loads user custom data
            //return ret; 
    #endregion
        }
    #endregion

    #region LoadNavigationProperties
        //const string cTask = "Task"; // TO DO: zmienic na MdRoutePoint.Task
        //const string cDistributor = "Task"; // TO DO: zmienic na MdRoutePoint.Distributor // Swoj� drog� tu jest b��d, zamiast Task powinno by� Distributor
        public static void LoadNavigationProperties(ref List<OpRoutePoint> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTask> TaskDict = null;
                Dictionary<int, OpDistributor> DistributorDict = null;

                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpRoutePoint), IMR.Suite.UI.Business.Objects.CORE.Metadata.MdRoutePoint.Task))
                    TaskDict = dataProvider.GetTask(list.Where(w => w.ID_TASK.HasValue).Select(l => l.ID_TASK.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TASK);
                if (!dataProvider.LoadOptionsEnabled || dataProvider.LoadOptions.ContainsMember(typeof(OpRoutePoint), IMR.Suite.UI.Business.Objects.CORE.Metadata.MdRoutePoint.Distributor))
                    DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);

                foreach (var loop in list)
                {
                    if (loop.ID_TASK.HasValue && TaskDict != null)
                        loop.Task = TaskDict.TryGetValue(loop.ID_TASK.Value);
                    if (DistributorDict != null)
                        loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRoutePoint> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0 && dataProvider.IRoutePointDataTypes != null)
            {
                List<int> idRoutePoint = list.Select(l => l.IdRoutePoint).ToList();

                List<OpRoutePointData> data = dataProvider.GetRoutePointDataFilter(IdRoutePoint: idRoutePoint.ToArray(), IdDataType: dataProvider.IRoutePointDataTypes.ToArray());
                if (data != null && data.Count > 0)
                {
                    foreach (var routePoint in list)
                    {
                        routePoint.DataList.AddRange(data.Where(d => d.IdRoutePoint == routePoint.IdRoutePoint));
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRoutePoint)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRoutePoint).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region enum RoutePointType
        public enum RoutePointType : int
        {
            Service = 2,
            Accomodation = 3
        }
    #endregion
    }
#endif
}