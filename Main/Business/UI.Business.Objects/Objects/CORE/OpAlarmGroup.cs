using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAlarmGroup : DB_ALARM_GROUP, IComparable, IReferenceType, IEquatable<OpAlarmGroup>
    {
        #region Properties
        public int IdAlarmGroup { get { return this.ID_ALARM_GROUP; } set { this.ID_ALARM_GROUP = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public bool IsEnabled { get { return this.IS_ENABLED; } set { this.IS_ENABLED = value; } }
        #endregion

        #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpAlarmGroup()
            : base() { }

        public OpAlarmGroup(DB_ALARM_GROUP clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpAlarmGroup> ConvertList(DB_ALARM_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmGroup> ConvertList(DB_ALARM_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAlarmGroup> ret = new List<OpAlarmGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmGroup> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpAlarmGroup> Members
        public bool Equals(OpAlarmGroup other)
        {
            if (other == null)
                return false;
            return this.IdAlarmGroup.Equals(other.IdAlarmGroup);
        }
        #endregion

        #region Implementation of IReferenceType

        public object GetReferenceKey()
        {
            return this.IdAlarmGroup;
        }

        public object GetReferenceValue()
        {
            return this;
        }

        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmGroup)
                return this.IdAlarmGroup == ((OpAlarmGroup)obj).IdAlarmGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmGroup left, OpAlarmGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmGroup left, OpAlarmGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmGroup.GetHashCode();
        }
        #endregion
    }
#endif
}