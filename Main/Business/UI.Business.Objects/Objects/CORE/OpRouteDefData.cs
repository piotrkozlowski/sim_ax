using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
     [Serializable]
    public class OpRouteDefData : DB_ROUTE_DEF_DATA, IOpData, IOpDataProvider, IComparable, IOpChangeState
	{
    #region Properties
		public int IdRouteDef { get { return this.ID_ROUTE_DEF; } set { this.ID_ROUTE_DEF = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
    #endregion

    #region Navigation Properties
		private OpRouteDef _RouteDef;
        public OpRouteDef RouteDef { get { return this._RouteDef; } set { this._RouteDef = value; this.IdRouteDef = (value == null) ? 0 : (int)value.IdRouteDef; } }
		private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.IdDataType = (value == null) ? 0 : (long)value.IdDataType; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdRouteDef; } set { IdRouteDef = (int)value; } }
		public int Index { get; set; }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpRouteDefData()
			: base() { this.OpState = OpChangeState.New; }

		public OpRouteDefData(DB_ROUTE_DEF_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "RouteDefData [" + IdRouteDef.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpRouteDefData> ConvertList(DB_ROUTE_DEF_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpRouteDefData> ConvertList(DB_ROUTE_DEF_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpRouteDefData> ret = new List<OpRouteDefData>(list.Length);
			foreach (var loop in list)
			{
				OpRouteDefData insert = new OpRouteDefData(loop);
				if (loadNavigationProperties)
				{
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRouteDefData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpRouteDefData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpRouteDefData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public object Clone(IDataProvider dataProvider)
		{
			OpRouteDefData clone = new OpRouteDefData();
			clone.IdData = this.IdData;
			clone.IdRouteDef = this.IdRouteDef;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_ROUTE_DEF_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.RouteDef = dataProvider.GetRouteDef(this.ID_ROUTE_DEF);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpRouteDefData)
				return this.IdData == ((OpRouteDefData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpRouteDefData left, OpRouteDefData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpRouteDefData left, OpRouteDefData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}