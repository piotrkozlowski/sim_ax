using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpConsumer : DB_CONSUMER, IComparable, IEquatable<OpConsumer>
    {
    #region Properties
        [XmlIgnore]
        public int IdConsumer { get { return this.ID_CONSUMER; } set { this.ID_CONSUMER = value; } }
        [XmlIgnore]
        public int? IdConsumerType { get { return this.ID_CONSUMER_TYPE; } set { this.ID_CONSUMER_TYPE = value; } }
        [XmlIgnore]
        public int IdActor { get { return this.ID_ACTOR; } set { this.ID_ACTOR = value; } }
        [XmlIgnore]
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        [XmlIgnore]
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        [XmlIgnore]
        public bool IsBlocked { get { return this.IS_BLOCKED; } set { this.IS_BLOCKED = value; } }
    #endregion

    #region Navigation Properties
        private OpConsumerType _ConsumerType;
        [XmlIgnore]
        public OpConsumerType ConsumerType { get { return this._ConsumerType; } set { this._ConsumerType = value; this.ID_CONSUMER_TYPE = (value == null) ? null : (int?)value.ID_CONSUMER_TYPE; } }
        private OpActor _Actor;
        public OpActor Actor { get { return this._Actor; } set { this._Actor = value; this.ID_ACTOR = (value == null) ? 0 : (int)value.ID_ACTOR; } }
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }

    #endregion

    #region Custom Properties
        public OpDataList<OpConsumerData> DataList { get; set; }

        [XmlIgnore]
        public double? DT_RECOMMENDED_TOP_UP
        {
            get
            {
                return DataList.TryGetNullableValue<double>(DataType.CONSUMER_RECOMMENDED_TOP_UP);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_RECOMMENDED_TOP_UP, value);
            }
        }

        [XmlIgnore]
        public string DT_CONSUMER_TAG_NO
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.CONSUMER_TAG_NO);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_TAG_NO, value);
            }
        }

        [XmlIgnore]
        public double DT_CONSUMER_CREDIT
        {
            get
            {
                return DataList.TryGetValue<double>(DataType.CONSUMER_CREDIT);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_CREDIT, value);
            }
        }

        [XmlIgnore]
        public double DT_CONSUMER_DEBT
        {
            get
            {
                return DataList.TryGetValue<double>(DataType.CONSUMER_DEBT);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_DEBT, value);
            }
        }

        [XmlIgnore]
        public OpLocation CurrentLocation { get; set; }
    #endregion

    #region Ctor
        public OpConsumer()
            : base() { DataList = new OpDataList<OpConsumerData>(); }

        public OpConsumer(DB_CONSUMER clone)
            : base(clone) { DataList = new OpDataList<OpConsumerData>(); }
    #endregion

    #region ToString
        public override string ToString()
        {
            if (Actor != null)
                return this.Actor.ToString();
            else
                return "[undefined]";
        }
    #endregion

    #region ConvertList
        public static List<OpConsumer> ConvertList(DB_CONSUMER[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpConsumer> ConvertList(DB_CONSUMER[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpConsumer> ret = new List<OpConsumer>(list.Length);
            list.ToList().ForEach(db_item => ret.Add(new OpConsumer(db_item)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConsumer> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpConsumerType> ConsumerTypeDict = dataProvider.GetConsumerType(list.Where(l => l.ID_CONSUMER_TYPE.HasValue).Select(l => l.ID_CONSUMER_TYPE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER_TYPE);
                Dictionary<int, OpActor> ActorDict = dataProvider.GetActor(list.Select(l => l.ID_ACTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTOR);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);

                foreach (var loop in list)
                {
                    if (loop.ID_CONSUMER_TYPE.HasValue)
                        loop.ConsumerType = ConsumerTypeDict.TryGetValue(loop.ID_CONSUMER_TYPE.Value);
                    loop.Actor = ActorDict.TryGetValue(loop.ID_ACTOR);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConsumer> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IConsumerDataTypes != null)
            {
                List<int> idConsumer = list.Select(l => l.IdConsumer).ToList();

                List<OpConsumerData> data = dataProvider.GetConsumerDataFilter(IdConsumer: idConsumer.ToArray(), IdDataType: dataProvider.IConsumerDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (data != null && data.Count > 0)
                {
                    foreach (var issue in list)
                        issue.DataList.AddRange(data.Where(d => d.IdConsumer == issue.IdConsumer));
                }
            }
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumer)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumer).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpConsumer> Members
        public bool Equals(OpConsumer other)
        {
            if (other == null)
                return false;
            return this.IdConsumer.Equals(other.IdConsumer);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpConsumer)
                return this.IdConsumer == ((OpConsumer)obj).IdConsumer;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpConsumer left, OpConsumer right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpConsumer left, OpConsumer right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdConsumer.GetHashCode();
        }
    #endregion
    }
#endif
}