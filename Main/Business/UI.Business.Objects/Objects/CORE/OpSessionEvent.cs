using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpSessionEvent : DB_SESSION_EVENT, IComparable
    {
    #region Properties
        public long IdSessionEvent { get { return this.ID_SESSION_EVENT;} set {this.ID_SESSION_EVENT = value;}}
        public long IdSession { get { return this.ID_SESSION;} set {this.ID_SESSION = value;}}
        public int IdEvent { get { return this.ID_EVENT;} set {this.ID_EVENT = value;}}
        public DateTime Time { get { return this.TIME;} set {this.TIME = value; } }
    #endregion

    #region	Navigation Properties
		private OpSession _Session;
				public OpSession Session { get { return this._Session; } set { this._Session = value; this.ID_SESSION = (value == null)? 0 : (long)value.ID_SESSION; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpSessionEvent()
			:base() {}
		
		public OpSessionEvent(DB_SESSION_EVENT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "SessionEvent [" + IdSessionEvent.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpSessionEvent> ConvertList(DB_SESSION_EVENT[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpSessionEvent> ConvertList(DB_SESSION_EVENT[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSessionEvent> ret = new List<OpSessionEvent>(list.Length);
			foreach (var loop in list)
			{
			OpSessionEvent insert = new OpSessionEvent(loop);
				
				if(loadNavigationProperties)
				{
											
																									insert.Session = dataProvider.GetSession(loop.ID_SESSION); 
													
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSessionEvent> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSessionEvent)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSessionEvent).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}