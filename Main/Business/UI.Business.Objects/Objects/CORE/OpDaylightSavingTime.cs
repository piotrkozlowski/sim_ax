using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDaylightSavingTime : DB_DAYLIGHT_SAVING_TIME, IComparable, IEquatable<OpDaylightSavingTime>
    {
    #region Properties
        public int IdDaylightSaving { get { return this.ID_DAYLIGHT_SAVING;} set {this.ID_DAYLIGHT_SAVING = value;}}
        public DateTime StartTime { get { return this.START_TIME;} set {this.START_TIME = value;}}
        public DateTime EndTime { get { return this.END_TIME;} set {this.END_TIME = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDaylightSavingTime()
			:base() {}
		
		public OpDaylightSavingTime(DB_DAYLIGHT_SAVING_TIME clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "DaylightSavingTime [" + IdDaylightSaving.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpDaylightSavingTime> ConvertList(DB_DAYLIGHT_SAVING_TIME[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDaylightSavingTime> ConvertList(DB_DAYLIGHT_SAVING_TIME[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpDaylightSavingTime> ret = new List<OpDaylightSavingTime>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDaylightSavingTime(db_object)));
			
			if(loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDaylightSavingTime> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDaylightSavingTime> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDaylightSavingTime)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDaylightSavingTime).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDaylightSavingTime> Members
        public bool Equals(OpDaylightSavingTime other)
        {
            if (other == null)
				return false;
			return this.IdDaylightSaving.Equals(other.IdDaylightSaving);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDaylightSavingTime)
				return this.IdDaylightSaving == ((OpDaylightSavingTime)obj).IdDaylightSaving;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDaylightSavingTime left, OpDaylightSavingTime right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDaylightSavingTime left, OpDaylightSavingTime right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDaylightSaving.GetHashCode();
		}
    #endregion
    }
#endif
}