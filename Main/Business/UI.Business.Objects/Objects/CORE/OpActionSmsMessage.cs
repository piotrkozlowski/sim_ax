using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionSmsMessage : DB_ACTION_SMS_MESSAGE, IComparable, IEquatable<OpActionSmsMessage>
    {
    #region Properties
        public long IdActionSmsMessage { get { return this.ID_ACTION_SMS_MESSAGE; } set { this.ID_ACTION_SMS_MESSAGE = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public bool IsIncoming { get { return this.IS_INCOMING; } set { this.IS_INCOMING = value; } }
        public int IdTransmissionStatus { get { return this.ID_TRANSMISSION_STATUS; } set { this.ID_TRANSMISSION_STATUS = value; } }
        public long? IdAction { get { return this.ID_ACTION; } set { this.ID_ACTION = value; } }
        public string Message { get { return this.MESSAGE; } set { this.MESSAGE = value; } }
    #endregion

    #region	Navigation Properties
        private OpTransmissionStatus _TransmissionStatus;
        public OpTransmissionStatus TransmissionStatus { get { return this._TransmissionStatus; } set { this._TransmissionStatus = value; this.ID_TRANSMISSION_STATUS = (value == null) ? 0 : (int)value.ID_TRANSMISSION_STATUS; } }
        private OpAction _Action;
        public OpAction Action { get { return this._Action; } set { this._Action = value; this.ID_ACTION = (value == null) ? null : (long?)value.ID_ACTION; } }
    #endregion

    #region	Custom Properties
        public OpConsumer Consumer { get; set; }
    #endregion

    #region	Ctor
        public OpActionSmsMessage()
            : base() { }

        public OpActionSmsMessage(DB_ACTION_SMS_MESSAGE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.MESSAGE;
        }
    #endregion

    #region	ConvertList
        public static List<OpActionSmsMessage> ConvertList(DB_ACTION_SMS_MESSAGE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpActionSmsMessage> ConvertList(DB_ACTION_SMS_MESSAGE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActionSmsMessage> ret = new List<OpActionSmsMessage>(list.Length);
            foreach (var loop in list)
            {
                OpActionSmsMessage insert = new OpActionSmsMessage(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionSmsMessage> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTransmissionStatus> TransmissionStatusDict = dataProvider.GetTransmissionStatus(list.Select(l => l.ID_TRANSMISSION_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_STATUS);
                Dictionary<long, OpAction> ActionDict = dataProvider.GetAction(list.Where(l => l.ID_ACTION.HasValue).Select(l => l.ID_ACTION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION);

                foreach (var loop in list)
                {
                    loop.TransmissionStatus = TransmissionStatusDict.TryGetValue(loop.ID_TRANSMISSION_STATUS);
                    if (loop.ID_ACTION.HasValue)
                        loop.Action = ActionDict.TryGetValue(loop.ID_ACTION.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionSmsMessage> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionSmsMessage)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionSmsMessage).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionSmsMessage> Members
        public bool Equals(OpActionSmsMessage other)
        {
            if (other == null)
                return false;
            return this.IdActionSmsMessage.Equals(other.IdActionSmsMessage);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActionSmsMessage)
                return this.IdActionSmsMessage == ((OpActionSmsMessage)obj).IdActionSmsMessage;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActionSmsMessage left, OpActionSmsMessage right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionSmsMessage left, OpActionSmsMessage right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActionSmsMessage.GetHashCode();
        }
    #endregion
    }
#endif
}