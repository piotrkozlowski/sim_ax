using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpActionHistoryData : DB_ACTION_HISTORY_DATA, IComparable, IEquatable<OpActionHistoryData>
    {
    #region Properties
            public long IdActionHistoryData { get { return this.ID_ACTION_HISTORY_DATA;} set {this.ID_ACTION_HISTORY_DATA = value;}}
                public int ArgNbr { get { return this.ARG_NBR;} set {this.ARG_NBR = value;}}
                public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpActionHistoryData()
			:base() {}
		
		public OpActionHistoryData(DB_ACTION_HISTORY_DATA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ActionHistoryData [" + IdActionHistoryData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpActionHistoryData> ConvertList(DB_ACTION_HISTORY_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpActionHistoryData> ConvertList(DB_ACTION_HISTORY_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpActionHistoryData> ret = new List<OpActionHistoryData>(list.Length);
			foreach (var loop in list)
			{
				OpActionHistoryData insert = new OpActionHistoryData(loop);
				ret.Add(insert);
			}
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpActionHistoryData> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpActionHistoryData> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionHistoryData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionHistoryData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpActionHistoryData> Members
        public bool Equals(OpActionHistoryData other)
        {
            if (other == null)
				return false;
			return this.IdActionHistoryData.Equals(other.IdActionHistoryData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpActionHistoryData)
				return this.IdActionHistoryData == ((OpActionHistoryData)obj).IdActionHistoryData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpActionHistoryData left, OpActionHistoryData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpActionHistoryData left, OpActionHistoryData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdActionHistoryData.GetHashCode();
		}
    #endregion
    }
#endif
}