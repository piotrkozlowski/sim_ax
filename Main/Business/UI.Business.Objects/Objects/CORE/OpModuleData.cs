using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
     [Serializable]
    public class OpModuleData : DB_MODULE_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpModuleData>
	{
    #region Properties
        [XmlIgnore]
        public long IdModuleData { get { return this.ID_MODULE_DATA; } set { this.ID_MODULE_DATA = OpDataUtil.SetNewValue<long, OpModuleData>(this.ID_MODULE_DATA, value, this); } }
        [XmlIgnore]
        public int IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = OpDataUtil.SetNewValue<int, OpModuleData>(this.ID_MODULE, value, this); } }
        [XmlIgnore]
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = OpDataUtil.SetNewValue<long, OpModuleData>(this.ID_DATA_TYPE, value, this); } }
        [XmlIgnore]
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpModule _Module;
		public OpModule Module { get { return this._Module; } set { this._Module = value; this.IdModule = (value == null) ? 0 : (int)value.ID_MODULE; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.IdDataType = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdModuleData; } set { IdModuleData = value; } }
        [XmlIgnore]
		public int Index { get; set; }
        [XmlIgnore]
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpModuleData()
			: base() { this.OpState = OpChangeState.New; }

		public OpModuleData(DB_MODULE_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "ModuleData [" + IdModuleData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpModuleData> ConvertList(DB_MODULE_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpModuleData> ConvertList(DB_MODULE_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpModuleData> ret = new List<OpModuleData>(list.Length);
			foreach (var loop in list)
			{
				OpModuleData insert = new OpModuleData(loop);
				if (loadNavigationProperties)
				{
                    //insert.Module = dataProvider.GetModule(loop.ID_MODULE);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpModuleData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.Module = dataProvider.GetModule(this.ID_MODULE);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpModuleData clone = new OpModuleData();
            clone.IdData = this.IdData;
            clone.IdModule = this.IdModule;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_MODULE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpModuleData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpModuleData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
            OpModuleData objToCmp = obj as OpModuleData;
            if (objToCmp != null)
            {
                if (this.IdData == 0 && objToCmp.IdData == 0) 
                    return ReferenceEquals(this, objToCmp);
                return this.IdData == objToCmp.IdData;
            }
            else
                return base.Equals(obj);
		}
		public static bool operator ==(OpModuleData left, OpModuleData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpModuleData left, OpModuleData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion

    #region IEquatable interface
        public bool Equals(OpModuleData other)
        {
            if (other == null)
                return false;
            return this.IdModuleData.Equals(other.IdModuleData);
        }
    #endregion

    }
#endif
}