using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpVersionData : DB_VERSION_DATA, IComparable, IEquatable<OpVersionData>, IOpChangeState, IOpData, IOpDataProvider
    {
        #region Properties
        public long IdVersionData { get { return this.ID_VERSION_DATA; } set { if (this.ID_VERSION_DATA != value) { this.ID_VERSION_DATA = OpDataUtil.SetNewValue(this.ID_VERSION_DATA, value, this); } } }
        public long IdVersion { get { return this.ID_VERSION; } set { if (this.ID_VERSION != value) { this.ID_VERSION = OpDataUtil.SetNewValue(this.ID_VERSION, value, this); } } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { if (this.ID_DATA_TYPE != value) { this.ID_DATA_TYPE = OpDataUtil.SetNewValue(this.ID_DATA_TYPE, value, this); } } }
        //public int IndexNbr { get { return this.INDEX_NBR; } set { if (this.INDEX_NBR != value) { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } } }
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object Value { get { return this.VALUE; } set { if (this.VALUE != value) { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } } }

        #endregion

        #region	Navigation Properties
        private OpVersion _Version;
        public OpVersion Version { get { return this._Version; } set { this._Version = value; this.ID_VERSION = (value == null) ? 0 : (long)value.ID_VERSION; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region	Custom Properties
        public long IdData { get { return IdVersionData; } set { IdVersionData = value; } }
        public object ReferencedObject { get; set; }
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Ctor
        public OpVersionData()
            : base() { }

        public OpVersionData(DB_VERSION_DATA clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "VersionData [" + IdVersionData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpVersionData> ConvertList(DB_VERSION_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpVersionData> ConvertList(DB_VERSION_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpVersionData> ret = new List<OpVersionData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpVersionData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpVersion> VersionDict = dataProvider.GetVersion(list.Select(l => l.ID_VERSION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.Version = VersionDict.TryGetValue(loop.ID_VERSION);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpVersionData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpVersionData> Members
        public bool Equals(OpVersionData other)
        {
            if (other == null)
                return false;
            return this.IdVersionData.Equals(other.IdVersionData);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpVersionData)
                return this.IdVersionData == ((OpVersionData)obj).IdVersionData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpVersionData left, OpVersionData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpVersionData left, OpVersionData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdVersionData.GetHashCode();
        }
        #endregion

        #region IOpDataProvider

        #region Clone

        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpVersionData clone = new OpVersionData();
            clone.IdData = this.IdData;
            clone.IdVersion = this.IdVersion;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_VERSION_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        #endregion

        #region AssignReferences

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

        #endregion

        #endregion
    }
#endif
}