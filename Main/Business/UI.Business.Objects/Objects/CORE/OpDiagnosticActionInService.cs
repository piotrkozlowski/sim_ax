﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDiagnosticActionInService : DB_DIAGNOSTIC_ACTION_IN_SERVICE, IComparable, IEquatable<OpDiagnosticActionInService>
    {
    #region Properties
        public int IdService { get { return this.ID_SERVICE; } set { this.ID_SERVICE = value; } }
        public int IdDiagnosticAction { get { return this.ID_DIAGNOSTIC_ACTION; } set { this.ID_DIAGNOSTIC_ACTION = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
    #endregion

    #region	Navigation Properties
        private OpService _Service;
        public OpService Service { get { return this._Service; } set { this._Service = value; this.ID_SERVICE = (value == null) ? 0 : (int)value.ID_SERVICE; } }
        private OpDiagnosticAction _DiagnosticAction;
        public OpDiagnosticAction DiagnosticAction { get { return this._DiagnosticAction; } set { this._DiagnosticAction = value; this.ID_DIAGNOSTIC_ACTION = (value == null) ? 0 : (int)value.ID_DIAGNOSTIC_ACTION; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDiagnosticActionInService()
            : base() { }

        public OpDiagnosticActionInService(DB_DIAGNOSTIC_ACTION_IN_SERVICE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DiagnosticActionInService [" + IdService.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDiagnosticActionInService> ConvertList(DB_DIAGNOSTIC_ACTION_IN_SERVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDiagnosticActionInService> ConvertList(DB_DIAGNOSTIC_ACTION_IN_SERVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDiagnosticActionInService> ret = new List<OpDiagnosticActionInService>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDiagnosticActionInService(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDiagnosticActionInService> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDiagnosticAction> DiagnosticActionDict = dataProvider.GetDiagnosticAction(list.Select(l => l.ID_DIAGNOSTIC_ACTION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DIAGNOSTIC_ACTION);

                foreach (var loop in list)
                {
                    loop.DiagnosticAction = DiagnosticActionDict.TryGetValue(loop.ID_DIAGNOSTIC_ACTION);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDiagnosticActionInService> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDiagnosticActionInService)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDiagnosticActionInService).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDiagnosticActionInService> Members
        public bool Equals(OpDiagnosticActionInService other)
        {
            if (other == null)
                return false;
            return this.IdService.Equals(other.IdService);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDiagnosticActionInService)
                return this.IdService == ((OpDiagnosticActionInService)obj).IdService;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDiagnosticActionInService left, OpDiagnosticActionInService right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDiagnosticActionInService left, OpDiagnosticActionInService right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdService.GetHashCode();
        }
    #endregion
    }
#endif
}