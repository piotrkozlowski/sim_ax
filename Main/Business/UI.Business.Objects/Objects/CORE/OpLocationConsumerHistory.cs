using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLocationConsumerHistory : DB_LOCATION_CONSUMER_HISTORY, IComparable, IEquatable<OpLocationConsumerHistory>
    {
    #region Properties
        public long IdLocationConsumerHistory { get { return this.ID_LOCATION_CONSUMER_HISTORY; } set { this.ID_LOCATION_CONSUMER_HISTORY = value; } }
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public int IdConsumer { get { return this.ID_CONSUMER; } set { this.ID_CONSUMER = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
    #endregion

    #region	Navigation Properties
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? 0 : (long)value.ID_LOCATION; } }
        private OpConsumer _Consumer;
        public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null) ? 0 : (int)value.ID_CONSUMER; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpLocationConsumerHistory()
            : base() { }

        public OpLocationConsumerHistory(DB_LOCATION_CONSUMER_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationConsumerHistory> ConvertList(DB_LOCATION_CONSUMER_HISTORY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLocationConsumerHistory> ConvertList(DB_LOCATION_CONSUMER_HISTORY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpLocationConsumerHistory> ret = new List<OpLocationConsumerHistory>(list.Length);
            foreach (var loop in list)
            {
                OpLocationConsumerHistory insert = new OpLocationConsumerHistory(loop);

                if (loadNavigationProperties)
                {

                    insert.Location = dataProvider.GetLocation(loop.ID_LOCATION);
                    insert.Consumer = dataProvider.GetConsumer(loop.ID_CONSUMER);

                    if (insert.Consumer != null && insert.EndTime == null)
                        insert.Consumer.CurrentLocation = insert.Location;
                    if (insert.Location != null && insert.EndTime == null)
                        insert.Location.Consumer = insert.Consumer;


                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationConsumerHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationConsumerHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationConsumerHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpLocationConsumerHistory> Members
        public bool Equals(OpLocationConsumerHistory other)
        {
            if (other == null)
                return false;
            return this.IdLocationConsumerHistory.Equals(other.IdLocationConsumerHistory);
        }
    #endregion
    }
#endif
}