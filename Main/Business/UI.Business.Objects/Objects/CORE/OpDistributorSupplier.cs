using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDistributorSupplier : DB_DISTRIBUTOR_SUPPLIER, IComparable, IEquatable<OpDistributorSupplier>
    {
    #region Properties
            public int IdDistributorSupplier { get { return this.ID_DISTRIBUTOR_SUPPLIER;} set {this.ID_DISTRIBUTOR_SUPPLIER = value;}}
                public int IdDistributor { get { return this.ID_DISTRIBUTOR;} set {this.ID_DISTRIBUTOR = value;}}
                public int? IdSupplier { get { return this.ID_SUPPLIER;} set {this.ID_SUPPLIER = value;}}
                public int? IdProductCode { get { return this.ID_PRODUCT_CODE;} set {this.ID_PRODUCT_CODE = value;}}
                public int IdActor { get { return this.ID_ACTOR;} set {this.ID_ACTOR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDistributor _Distributor;
				public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null)? 0 : (int)value.ID_DISTRIBUTOR; } }
				private OpSupplier _Supplier;
				public OpSupplier Supplier { get { return this._Supplier; } set { this._Supplier = value; this.ID_SUPPLIER = (value == null)? null : (int?)value.ID_SUPPLIER; } }
				private OpProductCode _ProductCode;
				public OpProductCode ProductCode { get { return this._ProductCode; } set { this._ProductCode = value; this.ID_PRODUCT_CODE = (value == null)? null : (int?)value.ID_PRODUCT_CODE; } }
				private OpActor _Actor;
				public OpActor Actor { get { return this._Actor; } set { this._Actor = value; this.ID_ACTOR = (value == null)? 0 : (int)value.ID_ACTOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDistributorSupplier()
			:base() {}
		
		public OpDistributorSupplier(DB_DISTRIBUTOR_SUPPLIER clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            return Actor.ToString();
		}
    #endregion

    #region	ConvertList
		public static List<OpDistributorSupplier> ConvertList(DB_DISTRIBUTOR_SUPPLIER[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpDistributorSupplier> ConvertList(DB_DISTRIBUTOR_SUPPLIER[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDistributorSupplier> ret = new List<OpDistributorSupplier>(list.Length);
			foreach (var loop in list)
			{
			OpDistributorSupplier insert = new OpDistributorSupplier(loop);
				
				if(loadNavigationProperties)
				{
											
																									insert.Distributor = dataProvider.GetDistributor(loop.ID_DISTRIBUTOR); 
													
																									if (loop.ID_SUPPLIER.HasValue)
								insert.Supplier = dataProvider.GetSupplier(loop.ID_SUPPLIER.Value); 
													
																									if (loop.ID_PRODUCT_CODE.HasValue)
								insert.ProductCode = dataProvider.GetProductCode(loop.ID_PRODUCT_CODE.Value); 
													
																									insert.Actor = dataProvider.GetActor(loop.ID_ACTOR); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDistributorSupplier> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDistributorSupplier)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDistributorSupplier).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDistributorSupplier> Members
        public bool Equals(OpDistributorSupplier other)
        {
            if (other == null)
				return false;
			return this.IdDistributorSupplier.Equals(other.IdDistributorSupplier);
        }
    #endregion
    }
#endif
}