﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpArticleData : DB_ARTICLE_DATA, IComparable, IEquatable<OpArticleData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdArticleData { get { return this.ID_ARTICLE_DATA; } set { this.ID_ARTICLE_DATA = value; } }
        public long IdArticle { get { return this.ID_ARTICLE; } set { this.ID_ARTICLE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
    #endregion

    #region	Navigation Properties
        private OpArticle _Article;
        public OpArticle Article { get { return this._Article; } set { this._Article = value; this.ID_ARTICLE = (value == null) ? 0 : (long)value.ID_ARTICLE; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpArticleData()
            : base() { }

        public OpArticleData(DB_ARTICLE_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ArticleData [" + IdArticleData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpArticleData> ConvertList(DB_ARTICLE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpArticleData> ConvertList(DB_ARTICLE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpArticleData> ret = new List<OpArticleData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpArticleData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpArticleData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<long, OpArticle> ArticleDict = dataProvider.GetArticle(list.Select(l => l.ID_ARTICLE).Distinct().ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ARTICLE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    //loop.Article = ArticleDict.TryGetValue(loop.ID_ARTICLE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpArticleData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpArticleData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpArticleData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpArticleData> Members
        public bool Equals(OpArticleData other)
        {
            if (other == null)
                return false;
            return this.IdArticleData.Equals(other.IdArticleData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpArticleData)
                return this.IdArticleData == ((OpArticleData)obj).IdArticleData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpArticleData left, OpArticleData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpArticleData left, OpArticleData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdArticleData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdArticleData; } set { IdArticleData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpArticleData clone = new OpArticleData();
            clone.IdData = this.IdData;
            clone.IdArticle = this.IdArticle;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_ARTICLE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }


    #endregion
    }
#endif
}