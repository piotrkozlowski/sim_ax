﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceWarranty : DB_DEVICE_WARRANTY, IComparable, IEquatable<OpDeviceWarranty>
    {
    #region Properties
        public int IdDeviceWarranty { get { return this.ID_DEVICE_WARRANTY; } set { this.ID_DEVICE_WARRANTY = value; } }
        public int? IdShippingList { get { return this.ID_SHIPPING_LIST; } set { this.ID_SHIPPING_LIST = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public int IdComponent { get { return this.ID_COMPONENT; } set { this.ID_COMPONENT = value; } }
        public int? IdContract { get { return this.ID_CONTRACT; } set { this.ID_CONTRACT = value; } }
        public int WarrantyLength { get { return this.WARRANTY_LENGTH; } 
            set 
            {
                this.WARRANTY_LENGTH = value;
            } 
        }

        public DateTime? ShippingDate { get { return SHIPPING_DATE; } set { this.SHIPPING_DATE = value; } }
        public DateTime? InstallationDate { get { return this.INSTALLATION_DATE; } set { this.INSTALLATION_DATE = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpShippingList _ShippingList;
        public OpShippingList ShippingList { get { return this._ShippingList; } set { this._ShippingList = value; this.ID_SHIPPING_LIST = (value == null) ? null : (int?)value.ID_SHIPPING_LIST; } }
        private OpComponent _Component;
        public OpComponent Component { get { return this._Component; } set { this._Component = value; this.ID_COMPONENT = (value == null) ? 0 : (int)value.ID_COMPONENT; } }
        private OpContract _Contract;
        public OpContract Contract 
        { 
            get { return this._Contract; } 
            set 
            { 
                this._Contract = value; this.ID_CONTRACT = (value == null) ? null : (int?)value.ID_CONTRACT;
                if (value != null && Device != null) 
                {
                    OpWarrantyContract[] contracts = ((OpContract)value).WarrantyContract.Where(c => c.DeviceOrderNumber.Name == Device.DeviceOrderNumber.Name && c.DeviceOrderNumber.FirstQuarter == Device.DeviceOrderNumber.FirstQuarter && c.DeviceOrderNumber.SecondQuarter == Device.DeviceOrderNumber.SecondQuarter).ToArray(); 
                    if (contracts.Length > 0) 
                        this.WarrantyContract = contracts[0]; 
                } 
            } 
        }
        private OpWarrantyContract _WarrantyContract;
        public OpWarrantyContract WarrantyContract { get { return this._WarrantyContract; } 
            set { this._WarrantyContract = value;
            DateTime data;
            switch ((IMR.Suite.Common.Enums.WarrantyLengthCalculationMode)((OpWarrantyContract)value).IdWarrantyStartDateCalculationMethod)
            { 
                case Enums.WarrantyLengthCalculationMode.ShippingDate:
                    data = this.ShippingDate.Value.AddMonths(((OpWarrantyContract)value).WarrantyTime);
                    this.WarrantyLength = ((TimeSpan)(data - this.ShippingDate)).Days;
                    break;
                case Enums.WarrantyLengthCalculationMode.InstallationDate:
                    data = this.InstallationDate.Value.AddMonths(((OpWarrantyContract)value).WarrantyTime);
                    this.WarrantyLength = ((TimeSpan)(this.InstallationDate - data)).Days;
                    break;
                case Enums.WarrantyLengthCalculationMode.LastMonthDay:
                    DateTime shDate = this.ShippingDate.Value.AddMonths(1);
                    DateTime customDate = new DateTime(shDate.Year, shDate.Month, 1);
                    shDate = customDate.AddMonths(((OpWarrantyContract)value).WarrantyTime);
                    this.WarrantyLength = ((TimeSpan)(shDate - customDate)).Days;
                    break;
            }
            } }
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; } }
    #endregion

    #region	Custom Properties

        public DateTime WarrantyExpiration
        {
            get
            {
                if (this.Contract !=null)
                {
                    if (WarrantyContract != null)
                    {
                        switch ((IMR.Suite.Common.Enums.WarrantyLengthCalculationMode)WarrantyContract.IdWarrantyStartDateCalculationMethod)
                        {
                            case Enums.WarrantyLengthCalculationMode.ShippingDate:
                                return this.ShippingDate.Value.AddDays(this.WarrantyLength);// data;
                            case Enums.WarrantyLengthCalculationMode.InstallationDate:
                                return this.InstallationDate.Value.AddDays(this.WarrantyLength);
                            case Enums.WarrantyLengthCalculationMode.LastMonthDay:
                                DateTime shDate = this.ShippingDate.Value.AddMonths(1);
                                DateTime customDate = new DateTime(shDate.Year, shDate.Month, 1).AddDays(this.WarrantyLength);
                                //this.WarrantyLength = ((TimeSpan)(shDate - customDate)).Days;
                                return customDate;
                            default:
                                return this.ShippingDate.Value.AddMonths(this.WarrantyLength);
                        }
                    }
                }
                if (!this.ShippingDate.HasValue && !this.InstallationDate.HasValue)
                    return (DataProvider != null) ? DataProvider.DateTimeNow.AddDays(WarrantyLength) : DateTime.Now.AddDays(WarrantyLength);
                else
                {
                    if (this.InstallationDate.HasValue)
                        return this.InstallationDate.Value.AddDays(this.WarrantyLength);
                    else
                        return this.ShippingDate.Value.AddDays(this.WarrantyLength);
                }
            }
        }

    #endregion

    #region	Ctor
        public OpDeviceWarranty()
            : base() { }

        public OpDeviceWarranty(DB_DEVICE_WARRANTY clone)
            : base(clone) { }
    #endregion

    #region Private Memebers
        private static IDataProvider DataProvider = null;
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeviceWarranty [" + IdDeviceWarranty.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceWarranty> ConvertList(DB_DEVICE_WARRANTY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceWarranty> ConvertList(DB_DEVICE_WARRANTY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            DataProvider = dataProvider;

            List<OpDeviceWarranty> ret = new List<OpDeviceWarranty>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceWarranty(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceWarranty> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpShippingList> ShippingListDict = dataProvider.GetShippingList(list.Where(l => l.ID_SHIPPING_LIST.HasValue).Select(l => l.ID_SHIPPING_LIST.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SHIPPING_LIST);
                Dictionary<int, OpComponent> ComponentDict = dataProvider.GetComponent(list.Select(l => l.ID_COMPONENT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_COMPONENT);
                Dictionary<int, OpContract> ContractDict = dataProvider.GetContract(list.Where(l => l.ID_CONTRACT.HasValue).Select(l => l.ID_CONTRACT.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONTRACT);
                long[] sn = list.Select(l => l.SERIAL_NBR).Distinct().ToArray();
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(sn).ToDictionary(l => l.SERIAL_NBR);
                foreach (var loop in list)
                {
                    if (loop.ID_SHIPPING_LIST.HasValue)
                        loop.ShippingList = ShippingListDict.TryGetValue(loop.ID_SHIPPING_LIST.Value);
                    loop.Component = ComponentDict.TryGetValue(loop.ID_COMPONENT);
                    loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR);
                    if (loop.ID_CONTRACT.HasValue)
                    {
                        OpContract contract = ContractDict.TryGetValue(loop.ID_CONTRACT.Value);
                        List<OpWarrantyContract> to = contract.WarrantyContract;
                        OpWarrantyContract.LoadNavigationProperties(ref to, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                        contract.WarrantyContract = to;
                        loop.Contract = contract;
                    }
                    
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceWarranty> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceWarranty)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceWarranty).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceWarranty> Members
        public bool Equals(OpDeviceWarranty other)
        {
            if (other == null)
                return false;
            return this.IdDeviceWarranty.Equals(other.IdDeviceWarranty);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceWarranty)
                return (this.IdDeviceWarranty == ((OpDeviceWarranty)obj).IdDeviceWarranty && this.IdComponent == ((OpDeviceWarranty)obj).IdComponent && this.SerialNbr == ((OpDeviceWarranty)obj).SerialNbr);
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceWarranty left, OpDeviceWarranty right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceWarranty left, OpDeviceWarranty right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceWarranty.GetHashCode();
        }
    #endregion
    }
#endif
}