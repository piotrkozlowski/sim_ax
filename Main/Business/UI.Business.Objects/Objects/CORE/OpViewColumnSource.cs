﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable, DataContract]
    public class OpViewColumnSource : DB_VIEW_COLUMN_SOURCE, IComparable, IEquatable<OpViewColumnSource>
    {
        #region Properties
        public int IdViewColumnSource { get { return this.ID_VIEW_COLUMN_SOURCE; } set { this.ID_VIEW_COLUMN_SOURCE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpViewColumnSource()
            : base() { }

        public OpViewColumnSource(DB_VIEW_COLUMN_SOURCE clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpViewColumnSource> ConvertList(DB_VIEW_COLUMN_SOURCE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpViewColumnSource> ConvertList(DB_VIEW_COLUMN_SOURCE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpViewColumnSource> ret = new List<OpViewColumnSource>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpViewColumnSource(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpViewColumnSource> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpViewColumnSource> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpViewColumnSource)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpViewColumnSource).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpViewColumnSource> Members
        public bool Equals(OpViewColumnSource other)
        {
            if (other == null)
                return false;
            return this.IdViewColumnSource.Equals(other.IdViewColumnSource);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpViewColumnSource)
                return this.IdViewColumnSource == ((OpViewColumnSource)obj).IdViewColumnSource;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpViewColumnSource left, OpViewColumnSource right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpViewColumnSource left, OpViewColumnSource right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdViewColumnSource.GetHashCode();
        }
        #endregion
    }
}