using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDeviceTypeProfileStepKind : DB_DEVICE_TYPE_PROFILE_STEP_KIND, IComparable, IEquatable<OpDeviceTypeProfileStepKind>
    {
    #region Properties
            public int IdDeviceTypeProfileStepKind { get { return this.ID_DEVICE_TYPE_PROFILE_STEP_KIND;} set {this.ID_DEVICE_TYPE_PROFILE_STEP_KIND = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDeviceTypeProfileStepKind()
			:base() {}
		
		public OpDeviceTypeProfileStepKind(DB_DEVICE_TYPE_PROFILE_STEP_KIND clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpDeviceTypeProfileStepKind> ConvertList(DB_DEVICE_TYPE_PROFILE_STEP_KIND[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDeviceTypeProfileStepKind> ConvertList(DB_DEVICE_TYPE_PROFILE_STEP_KIND[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceTypeProfileStepKind> ret = new List<OpDeviceTypeProfileStepKind>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceTypeProfileStepKind(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDeviceTypeProfileStepKind> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceTypeProfileStepKind> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTypeProfileStepKind)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTypeProfileStepKind).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDeviceTypeProfileStepKind> Members
        public bool Equals(OpDeviceTypeProfileStepKind other)
        {
            if (other == null)
				return false;
			return this.IdDeviceTypeProfileStepKind.Equals(other.IdDeviceTypeProfileStepKind);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDeviceTypeProfileStepKind)
				return this.IdDeviceTypeProfileStepKind == ((OpDeviceTypeProfileStepKind)obj).IdDeviceTypeProfileStepKind;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceTypeProfileStepKind left, OpDeviceTypeProfileStepKind right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceTypeProfileStepKind left, OpDeviceTypeProfileStepKind right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDeviceTypeProfileStepKind.GetHashCode();
		}
    #endregion
    }
#endif
}