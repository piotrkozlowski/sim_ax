using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;
using System.Runtime.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDevice : DB_DEVICE, IOpChangeState, IComparable, IReferenceType, IOpDynamic, IOpDynamicProperty<OpDevice>, IOpDataProperty, IOpObject<OpData>
    {
    #region Properties
        [XmlIgnore]
        public long SerialNbr
		{
			get { return this.SERIAL_NBR; }
            set { if (this.SERIAL_NBR != value) { this.SERIAL_NBR = OpDataUtil.SetNewValue(this.SERIAL_NBR, value, this); } }//value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public int IdDeviceType
		{
			get { return this.ID_DEVICE_TYPE; }
            set { if (this.ID_DEVICE_TYPE != value) { this.ID_DEVICE_TYPE = OpDataUtil.SetNewValue(this.ID_DEVICE_TYPE, value, this); } }//value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public long? SerialNbrPattern
		{ 
			get { return this.SERIAL_NBR_PATTERN; }
            set { if (this.SERIAL_NBR_PATTERN != value) { this.SERIAL_NBR_PATTERN = OpDataUtil.SetNewValue(this.SERIAL_NBR_PATTERN, value, this); } }//value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public long? IdDescrPattern
		{
			get { return this.ID_DESCR_PATTERN; }
            set { if (this.ID_DESCR_PATTERN != value) { this.ID_DESCR_PATTERN = OpDataUtil.SetNewValue(this.ID_DESCR_PATTERN, value, this); } }//value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public int IdDeviceOrderNumber
		{
			get { return this.ID_DEVICE_ORDER_NUMBER; }
            set { if (this.ID_DEVICE_ORDER_NUMBER != value) { this.ID_DEVICE_ORDER_NUMBER = OpDataUtil.SetNewValue(this.ID_DEVICE_ORDER_NUMBER, value, this); } }//value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public int IdDistributor
		{
			get { return this.ID_DISTRIBUTOR; }
            set { if (this.ID_DISTRIBUTOR != value) { this.ID_DISTRIBUTOR = OpDataUtil.SetNewValue(this.ID_DISTRIBUTOR, value, this); } }//value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public int IdDeviceStateType
		{
			get { return this.ID_DEVICE_STATE_TYPE; }
            set { if (this.ID_DEVICE_STATE_TYPE != value) { this.ID_DEVICE_STATE_TYPE = OpDataUtil.SetNewValue(this.ID_DEVICE_STATE_TYPE, value, this); } }//value; this.OpState = OpChangeState.Modified; } }
		}
    #endregion

    #region Navigation Properties
        private OpDeviceType _DeviceType;
        [XmlIgnore]
        public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.IdDeviceType = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
        private OpDeviceOrderNumber _DeviceOrderNumber;        
        public OpDeviceOrderNumber DeviceOrderNumber { get { return this._DeviceOrderNumber; } set { this._DeviceOrderNumber = value; this.ID_DEVICE_ORDER_NUMBER = (value == null) ? 0 : (int)value.ID_DEVICE_ORDER_NUMBER; } }
        [XmlIgnore]
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.IdDistributor = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        [XmlIgnore]
        private OpDeviceStateType _DeviceStateType;
        [XmlIgnore]
        public OpDeviceStateType DeviceStateType { get { return this._DeviceStateType; } set { this._DeviceStateType = value; this.IdDeviceStateType = (value == null) ? 0 : (int)value.ID_DEVICE_STATE_TYPE; } }
        //[XmlIgnore]
        private OpDevice _PatternDevice;
        //[XmlIgnore]
        public OpDevice PatternDevice { get { return this._PatternDevice; } set { this._PatternDevice = value; this.SerialNbrPattern = (value == null) ? null : (long?)value.SerialNbr; } }
    #endregion

    #region Custom Properties
        [XmlIgnore, DataMember]
		public OpChangeState OpState { get; set; }
        [XmlIgnore]
		public string GridLookUpEditString { get { return this.ToString(); } }
        [XmlIgnore]
        public DateTime? WarrantyDate
        {
            get
            {
                return DataList.TryGetNullableValue<DateTime>(DataType.DEVICE_WARRANTY_DATE);
            }
            set
            {
                DataList.SetValue(DataType.DEVICE_WARRANTY_DATE, value);
            }
        }
        [XmlIgnore]
        public DateTime? ProductionDate
		{
			get
			{
				return DataList.TryGetNullableValue<DateTime>(DataType.DEVICE_PRODUCTION_DATE);
			}
			set
			{
				DataList.SetValue(DataType.DEVICE_PRODUCTION_DATE, value);
			}
		}
        [XmlIgnore]
        public int? IdDelivery
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.DEVICE_ID_DELIVERY);
            }
            set
            {
                DataList.SetValue(DataType.DEVICE_ID_DELIVERY, value);
            }
        }
        [XmlIgnore]
        private OpDelivery _Delivery;
        [XmlIgnore]
        public OpDelivery Delivery
        {
            get { return _Delivery; }
            set
            {
                _Delivery = value;
                if (value != null)
                    IdDelivery = value.IdDelivery;
                else
                    IdDelivery = null;
            }
        }
        [XmlIgnore]
        public string FactoryNumber
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.DEVICE_FACTORY_NUMBER);
            }
            set
            {
                DataList.SetValue(DataType.DEVICE_FACTORY_NUMBER, value);
            }
        }

        [XmlIgnore]
        public DeviceStateUsok UsokDeviceState { get; set; }

        [XmlIgnore]
        public DateTime? InstallationDate { get; set; }

        [XmlIgnore]
        public List<OpMeter> CurrentMeters;
        [XmlIgnore]
        public List<OpLocation> CurrentLocations;

        [XmlIgnore]
        public OpMeter CurrentMeter { get { return CurrentMeters.FirstOrDefault(); } }
        [XmlIgnore]
        public OpLocation CurrentLocation { get { return CurrentLocations.FirstOrDefault(); } }
        [XmlIgnore]
        public bool IsTemplate { get { return this.SerialNbr == this.SerialNbrPattern; } }
        private OpDescr _TemplateDescription;
        public OpDescr TemplateDescription 
        { 
            get 
            {
                return _TemplateDescription;
            } 
            set 
            {
                _TemplateDescription = value;
                if (IsTemplate)
                    this.IdDescrPattern = value != null ? (long?)value.IdDescr : null;
            } 
        }
        [XmlIgnore]
        public string TemplateDescrString
        {
            get
            {
                if (TemplateDescription != null)
                    return TemplateDescription.Description;
                else if (IsTemplate)
                    return this.ToString();
                else if (PatternDevice != null)
                    return PatternDevice.ToString();
                else if (SerialNbrPattern.HasValue)
                    return SerialNbrPattern.Value.ToString();
                else
                    return "-";
            }
        }

        [XmlIgnore]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;

    #endregion

    #region Ctor
        public OpDevice()
			: base()
        {
			this.OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpData>();
            this.CurrentMeters = new List<OpMeter>();
            this.CurrentLocations = new List<OpLocation>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpDevice(DB_DEVICE clone)
			: base(clone)
        {
			this.OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpData>();
            this.CurrentMeters = new List<OpMeter>();
            this.CurrentLocations = new List<OpLocation>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }
        public OpDevice(OpDevice clone)
            : base(clone)
        {
            this.OpState = clone.OpState;
            this.DataList = new OpDataList<OpData>();
            if (clone.DataList != null)
            {
                foreach (OpData tdItem in clone.DataList)
                {
                    OpData dItem = new OpData(tdItem);
                    dItem.OpState = tdItem.OpState;
                    this.DataList.Add(dItem);
                }
            }
            if (clone.DeviceOrderNumber != null)
                this.DeviceOrderNumber = clone.DeviceOrderNumber;
            if (clone.DeviceStateType != null)
                this.DeviceStateType = clone.DeviceStateType;
            if (clone.DeviceType != null)
                this.DeviceType = clone.DeviceType;
            if (clone.Distributor != null)
                this.Distributor = clone.Distributor;
            if (clone.PatternDevice != null)
                this.PatternDevice = clone.PatternDevice;

            if (clone.TemplateDescription != null)
                this.TemplateDescription = clone.TemplateDescription;

            this.CurrentMeters = clone.CurrentMeters;
            this.CurrentLocations = clone.CurrentLocations;

            this.dynamicValues = clone.dynamicValues;
            this.dynamicProperties = clone.dynamicProperties;

            this.OpState = clone.OpState;
        }
        #endregion

        #region ToString
        public override string ToString()
        {
            string retStr = String.Empty;
            if (DeviceType != null)
                retStr = String.Format("{0}: {1}", DeviceType, SerialNbr);
            else if (DynamicValues != null && DynamicValues.ContainsKey(DataType.HELPER_ID_DEVICE_TYPE.ToString()) && DynamicValues[DataType.HELPER_ID_DEVICE_TYPE.ToString()] != null)
                retStr = String.Format("{0}: {1}", DynamicValues[DataType.HELPER_ID_DEVICE_TYPE.ToString()], SerialNbr);
            else
                retStr = SerialNbr.ToString();

            return retStr;
        }
    #endregion

    #region ConvertList
        public static List<OpDevice> ConvertList(DB_DEVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, List<long> customDataTypes = null,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            List<OpDevice> ret = new List<OpDevice>(db_objects.Length);
            db_objects.ToList().ForEach(db_object =>
                {
                    OpDevice device = new OpDevice(db_object);
                    if (device.U_DATA_ARRAY != null && device.U_DATA_ARRAY.Length > 0)
                        device.DataList.AddRange(OpData.ConvertList(device.U_DATA_ARRAY, dataProvider));
                    ret.Add(device);
                }
            );

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes: customDataTypes, transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout); // Loads user custom data

            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDevice> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Select(l => l.ID_DEVICE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_TYPE);
                Dictionary<int, OpDeviceOrderNumber> DeviceOrderNumberDict = dataProvider.GetDeviceOrderNumber(list.Select(l => l.ID_DEVICE_ORDER_NUMBER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_ORDER_NUMBER);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpDeviceStateType> DeviceStateTypeDict = dataProvider.GetDeviceStateType(list.Select(l => l.ID_DEVICE_STATE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_STATE_TYPE);
                
                List<long> serialNbrsPatterns = list.Where(w => w.SERIAL_NBR_PATTERN.HasValue && w.SERIAL_NBR_PATTERN.Value != 0).Select(s => s.SERIAL_NBR_PATTERN.Value).Distinct().ToList();
                List<OpDevice> patternsInList = list.Join(serialNbrsPatterns, q => q.SERIAL_NBR, p => p, (q, p) => q).ToList();
                List<long> patternsNotInList = serialNbrsPatterns.Except(patternsInList.Select(s => s.SERIAL_NBR)).ToList();

                Dictionary<long, OpDevice> PatternsDict = patternsInList.Distinct(q => q.SERIAL_NBR).ToDictionary(d => d.SERIAL_NBR);
                if (patternsNotInList.Count > 0)
                    PatternsDict = PatternsDict.Union(dataProvider.GetDevice(patternsNotInList.Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(q => q.SERIAL_NBR), q => q.Key)
                                               .ToDictionary(q => q.Key, w => w.Value);
                
                /*
                List<long> PatternsNotInList = new List<long>();
                Dictionary<long, OpDevice> PatternsDict = new Dictionary<long,OpDevice>();
                foreach (var loop in list)
                {
                    if (!loop.SERIAL_NBR_PATTERN.HasValue || loop.SERIAL_NBR_PATTERN == 0 || PatternsDict.ContainsKey(loop.SERIAL_NBR_PATTERN.Value))
                        continue;
                    OpDevice pattern = list.Find(q => q.SERIAL_NBR == loop.SERIAL_NBR_PATTERN);
                    if (pattern == null)
                        PatternsNotInList.Add(loop.SERIAL_NBR_PATTERN.Value);
                    else
                        PatternsDict.Add(pattern.SERIAL_NBR, pattern);
                }
                if (PatternsNotInList.Count > 0)
                    PatternsDict = PatternsDict.Union(dataProvider.GetDevice(PatternsNotInList.Distinct().ToArray()).ToDictionary(q => q.SERIAL_NBR), q => q.Key)
                                               .ToDictionary(q => q.Key, w => w.Value);
                */
                foreach (var loop in list)
                {
                    loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE);
                    loop.DeviceOrderNumber = DeviceOrderNumberDict.TryGetValue(loop.ID_DEVICE_ORDER_NUMBER);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.DeviceStateType = DeviceStateTypeDict.TryGetValue(loop.ID_DEVICE_STATE_TYPE);
                    if (loop.SERIAL_NBR_PATTERN.HasValue && loop.SERIAL_NBR_PATTERN != 0)
                        loop.PatternDevice = PatternsDict.TryGetValue(loop.SERIAL_NBR_PATTERN.Value);
                }
                
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpDevice> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<long> serialNbr = list.Select(l => l.SerialNbr).ToList();
                List<OpData> data = new List<OpData>();
                
                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetDataFilter(SerialNbr: serialNbr.ToArray(), IdDataType: customDataTypes.ToArray(), transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);
                else if (dataProvider.IDeviceDataTypes != null && dataProvider.IDeviceDataTypes.Count > 0)
                    data = dataProvider.GetDataFilter(SerialNbr: serialNbr.ToArray(), IdDataType: dataProvider.IDeviceDataTypes.ToArray(), transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);

                if (data.Count > 0)
                {
                    Dictionary<long, OpDevice> devicesDict = list.ToDictionary<OpDevice, long>(l => l.SerialNbr);
                    data.ForEach(w => devicesDict[w.SerialNbr.Value].DataList.Add(w) );
                }

                Dictionary<long, OpDescr> descrDict = dataProvider.GetDescr(list.Where(q => (q.IsTemplate && q.IdDescrPattern.HasValue) ||
                                                                                            (!q.IsTemplate && q.PatternDevice != null && q.PatternDevice.IdDescrPattern.HasValue))
                                                                                .Select(q => q.IsTemplate ? q.IdDescrPattern.Value : q.PatternDevice.IdDescrPattern.Value)
                                                                                .Distinct().ToArray())
                                                                  .ToDictionary(q => q.IdDescr);

                foreach (var device in list)
                {
                    if (device.IdDelivery.HasValue)
                        device.Delivery = dataProvider.GetDelivery(device.IdDelivery.Value);

                    if (device.IsTemplate && device.IdDescrPattern.HasValue)
                        device.TemplateDescription = descrDict.TryGetValue(device.IdDescrPattern.Value);
                    else if (!device.IsTemplate && device.PatternDevice != null && device.PatternDevice.IdDescrPattern.HasValue)
                        device.TemplateDescription = descrDict.TryGetValue(device.PatternDevice.IdDescrPattern.Value);
                }
            }
        }
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            DeviceType = dataProvider.GetDeviceType(this.ID_DEVICE_TYPE);
            DeviceOrderNumber = dataProvider.GetDeviceOrderNumber(this.ID_DEVICE_ORDER_NUMBER);
            Distributor = dataProvider.GetDistributor(this.ID_DISTRIBUTOR);
            DeviceStateType = dataProvider.GetDeviceStateType(this.ID_DEVICE_STATE_TYPE);
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDevice)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDevice).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region DeviceStateUsok
        public class DeviceStateUsok
        {
            public int ID_DEVICE_STATE { get; set; }
            public string DESCR { get; set; }
            public override string ToString()
            {
                return DESCR;
            }
        }
    #endregion

    #region IEquatable<OpDevice> Members
        public bool Equals(OpDevice other)
        {
            if (other != null)
                return this.SerialNbr.Equals(other.SerialNbr);
            else
                return false;
        }
    #endregion

    #region Clone
		/// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public OpDevice Clone(IDataProvider dataProvider)
        {
            OpDevice clone = new OpDevice();
            clone.SerialNbr = this.SerialNbr;
            clone.IdDeviceType = this.IdDeviceType;
            clone.SerialNbrPattern = this.SerialNbrPattern;
            clone.IdDescrPattern = this.IdDescrPattern;
            clone.IdDeviceOrderNumber = this.IdDeviceOrderNumber;
            clone.IdDistributor = this.IdDistributor;
            clone.IdDeviceStateType = this.IdDeviceStateType;

			clone.DataList = this.DataList.Clone(dataProvider);

			if (dataProvider != null)
				clone = ConvertList(new DB_DEVICE[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState; 
			return clone;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDevice)
                return this.SerialNbr == ((OpDevice)obj).SerialNbr;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDevice left, OpDevice right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDevice left, OpDevice right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return SerialNbr.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return SerialNbr;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return SerialNbr; }
        }
        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }
        [XmlIgnore]
        OpDevice IOpDynamicProperty<OpDevice>.Owner
        {
            get { return this; }
        }
        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }
        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
    #endregion
    #region Implementation of IOpDataProperty
        public object GetOpDataPropertyValue(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return null;

            OpData opData = null;
            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.CurrentLocation != null)
                        return this.CurrentLocation.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
                case Enums.ReferenceType.IdMeter:
                    if (this.CurrentMeter != null)
                        return this.CurrentMeter.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
            }

            opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opDataProperty.OpDataType.IdReferenceType.HasValue && opData.ReferencedObject != null)
                    return opData.ReferencedObject;

                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && opData.Value != null)
                    return OpUnit.ChangeUnit(opData.Value, opDataProperty.OpDataType, opData.DataType.Unit, opDataProperty.OpUnit, OpDataTypeFormat.GetPrecision(opDataProperty.OpDataTypeFormat)); //(Convert.ToDouble(opData.Value) - opDataProperty.OpUnit.Bias) / opDataProperty.OpUnit.Scale;

                return opData.Value;
            }

            return null;
        }
        public void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return;

            OpData opData = null;
            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.CurrentLocation != null)
                        this.CurrentLocation.SetOpDataPropertyValue(opDataProperty, value);
                    return;
                case Enums.ReferenceType.IdMeter:
                    if (this.CurrentMeter != null)
                        this.CurrentMeter.SetOpDataPropertyValue(opDataProperty, value);
                    return;
            }

            opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && value != null)
                    opData.Value = OpUnit.ChangeUnit(value, opDataProperty.OpDataType, opDataProperty.OpUnit, opData.DataType.Unit); //Convert.ToDouble(value) * opDataProperty.OpUnit.Scale + opDataProperty.OpUnit.Bias;
                else
                    opData.Value = value;
            }
        }
        private OpData GetColumnOpData(OpDataType dataType)
        {
            if (dataType == null) return null;
            switch (dataType.IdDataType)
            {
                case DataType.HELPER_SERIAL_NBR:
                    return new OpData() { DataType = dataType, Value = this.SerialNbr, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_DEVICE_TYPE:
                    return new OpData() { DataType = dataType, Value = this.DeviceType, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_DEVICE_ORDER_NUMBER:
                    return new OpData() { DataType = dataType, Value = this.DeviceOrderNumber, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_DISTRIBUTOR:
                    return new OpData() { DataType = dataType, Value = this.Distributor, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_DEVICE_STATE_TYPE:
                    return new OpData() { DataType = dataType, Value = this.DeviceStateType, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_INSTALLATION_DATE:
                    return new OpData() { DataType = dataType, Value = this.InstallationDate, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
            }
            return null;
        }
        public Type GetOpDataPropertyType(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null || !opDataProperty.ReturnReferencedTypeIfHelper.HasValue) return null;
            Type type = Utils.GetHelperType(opDataProperty.OpDataType, opDataProperty.ReturnReferencedTypeIfHelper.Value);

            if (type == null)
                type = DataType.GetSystemType(opDataProperty.OpDataType.IdDataTypeClass);

            return type;
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
    public object IdObject
    {
        get { return SerialNbr; }
        set { SerialNbr = Convert.ToInt64(value); }
    }

    [DataMember]
    public OpDataList<OpData> DataList { get; set; }
    #endregion
    }
#endif
}