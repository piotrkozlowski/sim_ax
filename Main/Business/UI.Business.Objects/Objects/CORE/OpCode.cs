﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpCode : DB_CODE, IOpChangeState, IComparable, IEquatable<OpCode>
	{
    #region Properties
        [XmlIgnore]
        public long IdCode { get { return this.ID_CODE; } set { this.ID_CODE = value; } }
        [XmlIgnore]
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        [XmlIgnore]
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        [XmlIgnore]
        public int IdCodeType { get { return this.ID_CODE_TYPE; } set { this.ID_CODE_TYPE = value; } }
        [XmlIgnore]
		public int CodeNbr
		{ 
			get { return this.CODE_NBR; }
			set { if (this.CODE_NBR != value) { this.CODE_NBR = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
		public string Code 
		{ 
			get { return this.CODE; }
			set { if (this.CODE != value) { this.CODE = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
		public bool IsActive 
		{ 
			get { return this.IS_ACTIVE; } 
			set { if (this.IS_ACTIVE != value) { this.IS_ACTIVE = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
		public DateTime? ActivationTime 
		{ 
			get { return this.ACTIVATION_TIME; }
			set { if (this.ACTIVATION_TIME != value) { this.ACTIVATION_TIME = value; this.OpState = OpChangeState.Modified; } }
		}
    #endregion

    #region Navigation Properties
		private OpMeter _Meter;
        [XmlIgnore]
		public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
		private OpLocation _Location;
        [XmlIgnore]
		public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
		private OpCodeType _CodeType;
        [XmlIgnore]
		public OpCodeType CodeType { get { return this._CodeType; } set { this._CodeType = value; this.ID_CODE_TYPE = (value == null) ? 0 : (int)value.ID_CODE_TYPE; } }
    #endregion

    #region Custom Properties
		public OpChangeState OpState { get; set; }
    #endregion

    #region Ctor
		public OpCode()
			: base() { this.OpState = OpChangeState.New; }

		public OpCode(DB_CODE clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return this.CODE;
		}
    #endregion

    #region ConvertList
		public static List<OpCode> ConvertList(DB_CODE[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpCode> ConvertList(DB_CODE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpCode> ret = new List<OpCode>(list.Length);
			foreach (var loop in list)
			{
				OpCode insert = new OpCode(loop);
				ret.Add(insert);
			}

			if (loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpCode> list, IDataProvider dataProvider)
		{
			if (list != null && list.Count > 0)
			{
				Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
				Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
				Dictionary<int, OpCodeType> CodeTypeDict = dataProvider.GetCodeType(list.Select(l => l.ID_CODE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_CODE_TYPE);

				foreach (var loop in list)
				{
					if (loop.ID_METER.HasValue)
						loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
					if (loop.ID_LOCATION.HasValue)
						loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
					loop.CodeType = CodeTypeDict.TryGetValue(loop.ID_CODE_TYPE);
				}
			}
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpCode> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region AssignReferences
		public void AssignReferences(IDataProvider dataProvider)
		{
			if (this.ID_LOCATION.HasValue)
				this.Location = dataProvider.GetLocation(this.ID_LOCATION.Value);
			if (this.ID_METER.HasValue)
				this.Meter = dataProvider.GetMeter(this.ID_METER.Value);
			//if (this.SERIAL_NBR.HasValue)
			//	this.Device = dataProvider.GetDevice(this.SERIAL_NBR.Value);
			this.CodeType = dataProvider.GetCodeType(this.ID_CODE_TYPE);
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpCode)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpCode).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IEquatable<OpCode> Members
		public bool Equals(OpCode other)
		{
			if (other == null)
				return false;
			return this.IdCode.Equals(other.IdCode);
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpCode)
				return this.IdCode == ((OpCode)obj).IdCode;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpCode left, OpCode right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpCode left, OpCode right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdCode.GetHashCode();
		}
    #endregion
	}
#endif
}