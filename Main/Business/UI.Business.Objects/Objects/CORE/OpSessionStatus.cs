using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpSessionStatus : DB_SESSION_STATUS, IComparable
    {
    #region Properties
            public int IdSessionStatus { get { return this.ID_SESSION_STATUS;} set {this.ID_SESSION_STATUS = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        [XmlIgnore]
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion
		
    #region	Ctor
		public OpSessionStatus()
			:base() {}
		
		public OpSessionStatus(DB_SESSION_STATUS clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            if (this.Descr != null)
                return this.Descr.Description;
            return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpSessionStatus> ConvertList(DB_SESSION_STATUS[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpSessionStatus> ConvertList(DB_SESSION_STATUS[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSessionStatus> ret = new List<OpSessionStatus>(list.Length);
			foreach (var loop in list)
			{
			OpSessionStatus insert = new OpSessionStatus(loop);
				
				if(loadNavigationProperties)
				{
											
											
																									if (loop.ID_DESCR.HasValue)
								insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSessionStatus> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSessionStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSessionStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}