using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpImrServer : DB_IMR_SERVER, IComparable, IEquatable<OpImrServer>, IReferenceType
    {
        #region Properties
        [XmlIgnore]
        public int IdImrServer { get { return this.ID_IMR_SERVER; } set { this.ID_IMR_SERVER = value; } }
        [XmlIgnore]
        public string IpAddress { get { return this.IP_ADDRESS; } set { this.IP_ADDRESS = value; } }
        [XmlIgnore]
        public string DnsName { get { return this.DNS_NAME; } set { this.DNS_NAME = value; } }
        [XmlIgnore]
        public string WebserviceAddress { get { return this.WEBSERVICE_ADDRESS; } set { this.WEBSERVICE_ADDRESS = value; } }
        [XmlIgnore]
        public int? WebserviceTimeout { get { return this.WEBSERVICE_TIMEOUT; } set { this.WEBSERVICE_TIMEOUT = value; } }
        [XmlIgnore]
        public string Login { get { return this.LOGIN; } set { this.LOGIN = value; } }
        [XmlIgnore]
        public string Password { get { return this.PASSWORD; } set { this.PASSWORD = value; } }
        [XmlIgnore]
        public string StandardDescription { get { return this.STANDARD_DESCRIPTION; } set { this.STANDARD_DESCRIPTION = value; } }
        [XmlIgnore]
        public bool IsGood { get { return this.IS_GOOD; } set { this.IS_GOOD = value; } }
        [XmlIgnore]
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        [XmlIgnore]
        public string UtdcSsecUser { get { return this.UTDC_SSEC_USER; } set { this.UTDC_SSEC_USER = value; } }
        [XmlIgnore]
        public string UtdcSsecPass { get { return this.UTDC_SSEC_PASS; } set { this.UTDC_SSEC_PASS = value; } }
        [XmlIgnore]
        public string Collation { get { return this.COLLATION; } set { this.COLLATION = value; } }
        [XmlIgnore]
        public string DbName { get { return this.DB_NAME; } set { this.DB_NAME = value; } }
        [XmlIgnore]
        public int ImrServerVersion { get { return this.IMR_SERVER_VERSION; } set { this.IMR_SERVER_VERSION = value; } }
        [XmlIgnore]
        public bool IsImrlt { get { return this.IS_IMRLT; } set { this.IS_IMRLT = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public static readonly int LocalhostID = 18;

        [XmlIgnore]
        public bool IsLocalhost
        {
            get
            {
                bool? retValue = DataList.TryGetValue<bool>(DataType.IMR_SERVER_IS_LOCALHOST);
                return (retValue.HasValue ? retValue.Value : false);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_IS_LOCALHOST, value);
            }
        }
        [XmlIgnore]
        public bool IsImrSc
        {
            get
            {
                bool? retValue = DataList.TryGetValue<bool>(DataType.IMR_SERVER_IS_CORE_IMRSC);
                return (retValue.HasValue ? retValue.Value : false);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_IS_CORE_IMRSC, value);
            }
        }

        [DataMember]
        public OpDataList<OpImrServerData> DataList { get; set; }

        [XmlIgnore]
        public string UserPortalWebServiceAddress
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.IMR_SERVER_USER_PORTAL_WEBSERVICE_ADDRESS);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_USER_PORTAL_WEBSERVICE_ADDRESS, value);
            }
        }
        [XmlIgnore]
        public string DBCollectorWebServiceAddress
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_ADDRESS);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_ADDRESS, value);
            }
        }
        [XmlIgnore]
        public string DBCollectorWebServiceBinding
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_BINDING);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_BINDING, value);
            }
        }
        [XmlIgnore]
        public string ESBDriverWebServiceAddress
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.IMR_SERVER_ESB_DRIVER_WEBSERVICE_ADDRESS);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_ESB_DRIVER_WEBSERVICE_ADDRESS, value);
            }
        }
        [XmlIgnore]
        public string Phone
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.IMR_SERVER_PHONE);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_PHONE, value);
            }
        }
        [XmlIgnore]
        public string HostMSMQ
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.IMR_SERVER_MSMQ_HOST);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_MSMQ_HOST, value);
            }
        }
        [XmlIgnore]
        public string ActionQueueName
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.IMR_SERVER_MSMQ_SEND_ACTION);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_MSMQ_SEND_ACTION, value);
            }
        }
        [XmlIgnore]
        public int? DwServerId
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.IMR_SERVER_DW_SERVER_ID);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_DW_SERVER_ID, value);
            }
        }
        [XmlIgnore]
        public int? CoreServerId
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.IMR_SERVER_CORE_SERVER_ID);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_CORE_SERVER_ID, value);
            }
        }
        [XmlIgnore]
        public int? DeploymentServerId
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.IMR_SERVER_TEST_SERVER_ID);
            }
            set
            {
                DataList.SetValue(DataType.IMR_SERVER_TEST_SERVER_ID, value);
            }
        }

        [DataMember]
        public List<OpDistributor> DistributorsExitingOnServer { get; set; }
        [DataMember]
        public List<OpDistributor> DistributorsAssignedToServer { get; set; }

        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        [XmlIgnore]
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }

    #endregion

    #region	Ctor
        public OpImrServer()
            : base() 
        {
            DataList = new OpDataList<OpImrServerData>();
            DistributorsExitingOnServer = new List<OpDistributor>();
            DistributorsAssignedToServer = new List<OpDistributor>();
        }

        public OpImrServer(DB_IMR_SERVER clone)
            : base(clone)
        {
            DataList = new OpDataList<OpImrServerData>();
            DistributorsExitingOnServer = new List<OpDistributor>();
            DistributorsAssignedToServer = new List<OpDistributor>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.StandardDescription;
        }
    #endregion

    #region	ConvertList
        public static List<OpImrServer> ConvertList(DB_IMR_SERVER[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpImrServer> ConvertList(DB_IMR_SERVER[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpImrServer> ret = new List<OpImrServer>(list.Length);
            foreach (var loop in list)
            {
                OpImrServer insert = new OpImrServer(loop);

                if (loadNavigationProperties)
                {
















                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpImrServer> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IImrSeverDataTypes != null)
            {
                List<int> idImrServer = list.Select(l => l.IdImrServer).ToList();

                if (dataProvider.IImrSeverDataTypes.Count > 0)
                {
                    List<OpImrServerData> data = dataProvider.GetImrServerDataFilter(IdImrServer: idImrServer.ToArray(), IdDataType: dataProvider.IImrSeverDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpImrServer> imrServerDataDict = list.ToDictionary<OpImrServer, int>(l => l.IdImrServer);
                        foreach (var dataValue in data)
                        {
                            imrServerDataDict[dataValue.IdImrServer].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpImrServer)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpImrServer).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpImrServer> Members
        public bool Equals(OpImrServer other)
        {
            if (other == null)
                return false;
            return this.IdImrServer.Equals(other.IdImrServer);
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdImrServer;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}