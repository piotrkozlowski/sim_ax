using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
    public class OpLocationEquipment : DB_LOCATION_EQUIPMENT, IOpChangeState, IComparable, IReferenceType, IOpDynamic, IOpDynamicProperty<OpLocationEquipment>, IOpDataProperty
    {
    #region Properties
        [XmlIgnore]
        public long IdLocationEquipment { get { return this.ID_LOCATION_EQUIPMENT; } set { this.ID_LOCATION_EQUIPMENT = value; } }
        [XmlIgnore]
        public long? IdLocation
		{
			get { return this.ID_LOCATION; }
			set { if (this.ID_LOCATION != value) { this.ID_LOCATION = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public long? IdMeter 
		{ 
			get { return this.ID_METER; }
			set { if (this.ID_METER != value) { this.ID_METER = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public long? SerialNbr
		{
			get { return this.SERIAL_NBR; }
			set { if (this.SERIAL_NBR != value) { this.SERIAL_NBR = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public DateTime StartTime
		{ 
			get { return this.START_TIME; }
			set { if (this.START_TIME != value) { this.START_TIME = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public DateTime? EndTime 
		{ 
			get { return this.END_TIME; }
			set { if (this.END_TIME != value) { this.END_TIME = value; this.OpState = OpChangeState.Modified; } }
		}
    #endregion

    #region Navigation Properties
        private OpLocation _Location;
        [XmlIgnore]
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.IdLocation = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.IdMeter = (value == null) ? null : (long?)value.ID_METER; } }
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SerialNbr = (value == null) ? null : (long?)value.SERIAL_NBR; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public OpChangeState OpState { get; set; }

        private Dictionary<string, object> dynamicValues;
        private OpDynamicPropertyDict dynamicProperties;
    #endregion

    #region Ctor
        public OpLocationEquipment()
			: base()
        { 
            this.OpState = OpChangeState.New;
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpLocationEquipment(DB_LOCATION_EQUIPMENT clone)
			: base(clone)
        { 
            this.OpState = OpChangeState.Loaded;
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }
    #endregion

    #region ToString
        public override string ToString()
        {
            return "LocationEquipment [" + IdLocationEquipment.ToString() + "]";
        }
    #endregion

    #region ConvertList
        public static List<OpLocationEquipment> ConvertList(DB_LOCATION_EQUIPMENT[] list, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
			List<OpLocationEquipment> ret = new List<OpLocationEquipment>(list.Length);
			list.ToList().ForEach(db_item => ret.Add(new OpLocationEquipment(db_item)));

			if (loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if (loadCustomData)
			    LoadCustomData(ref ret, dataProvider); // Loads user custom data

			return ret;
        }
    #endregion

    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpLocationEquipment> list, IDataProvider dataProvider, bool queryDatabase = false)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(w => w.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray(), queryDatabase).ToDictionary(l => l.ID_LOCATION);
				Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(w => w.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray(), queryDatabase).ToDictionary(l => l.ID_METER);
				Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Where(w => w.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray(), queryDatabase).ToDictionary(l => l.SERIAL_NBR);

				foreach (var loop in list)
				{
					if (loop.ID_LOCATION.HasValue)
						loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
					if (loop.ID_METER.HasValue)
						loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
					if (loop.SERIAL_NBR.HasValue)
						loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR.Value);
				}
			}
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpLocationEquipment> list, IDataProvider dataProvider)
        {
            foreach (var loop in list)
            {
                //Load additional references
                if (loop.END_TIME == null)
                {
                    if (loop.Device != null)
                    {
                        if (loop.Meter != null && !loop.Device.CurrentMeters.Contains(loop.Meter))
                            loop.Device.CurrentMeters.Add (loop.Meter);
                        if (loop.Location != null && !loop.Device.CurrentLocations.Contains(loop.Location))
                            loop.Device.CurrentLocations.Add(loop.Location);
                    }

                    if (loop.Meter != null)
                    {
                        if (loop.Device != null && !loop.Meter.CurrentDevices.Contains(loop.Device))
                            loop.Meter.CurrentDevices.Add(loop.Device);

                        if (loop.Location != null && !loop.Meter.CurrentLocations.Contains(loop.Location))
                            loop.Meter.CurrentLocations.Add(loop.Location);
                    }

                    if (loop.Location != null)
                    {
                        if (loop.Device != null && !loop.Location.CurrentDevices.Contains(loop.Device))
                            loop.Location.CurrentDevices.Add(loop.Device);
                        if (loop.Meter != null && !loop.Location.CurrentMeters.Contains(loop.Meter))
                            loop.Location.CurrentMeters.Add(loop.Meter);
                    }
                }
            }
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationEquipment)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationEquipment).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Clone
		public OpLocationEquipment Clone(IDataProvider dataProvider)
		{
			OpLocationEquipment clone = new OpLocationEquipment();
			clone.IdLocationEquipment = this.IdLocationEquipment;
			clone.IdLocation = this.IdLocation;
			clone.IdMeter = this.IdMeter;
			clone.SerialNbr = this.SerialNbr;
			clone.StartTime = this.StartTime;
			clone.EndTime = this.EndTime;

			if (dataProvider != null)
				clone = ConvertList(new DB_LOCATION_EQUIPMENT[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpLocationEquipment)
				return this.IdLocationEquipment == ((OpLocationEquipment)obj).IdLocationEquipment;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpLocationEquipment left, OpLocationEquipment right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpLocationEquipment left, OpLocationEquipment right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdLocationEquipment.GetHashCode();
		}
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdLocationEquipment;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdLocationEquipment; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpLocationEquipment IOpDynamicProperty<OpLocationEquipment>.Owner
        {
            get { return this; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
    #endregion
    #region Implementation of IOpDataProperty
        public object GetOpDataPropertyValue(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return null;

            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.Location != null)
                        return this.Location.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
                case Enums.ReferenceType.IdMeter:
                    if (this.Meter != null)
                        return this.Meter.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
                case Enums.ReferenceType.SerialNbr:
                    if (this.Device != null)
                        return this.Device.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
            }

            return null;
        }
        public void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return;

            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.Location != null)
                        this.Location.SetOpDataPropertyValue(opDataProperty, value);
                    return;
                case Enums.ReferenceType.IdMeter:
                    if (this.Meter != null)
                        this.Meter.SetOpDataPropertyValue(opDataProperty, value);
                    return;
                case Enums.ReferenceType.SerialNbr:
                    if (this.Device != null)
                        this.Device.SetOpDataPropertyValue(opDataProperty, value);
                    return;
            }
        }
        public Type GetOpDataPropertyType(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null || !opDataProperty.ReturnReferencedTypeIfHelper.HasValue) return null;
            Type type = Utils.GetHelperType(opDataProperty.OpDataType, opDataProperty.ReturnReferencedTypeIfHelper.Value);

            if (type == null)
                type = DataType.GetSystemType(opDataProperty.OpDataType.IdDataTypeClass);

            return type;
        }
        #endregion
    }
#endif
}