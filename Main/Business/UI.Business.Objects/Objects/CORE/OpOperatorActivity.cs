﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpOperatorActivity : DB_OPERATOR_ACTIVITY, IComparable, IEquatable<OpOperatorActivity>, IOpChangeState
    {
    #region Properties
        public long IdOperatorActivity { get { return this.ID_OPERATOR_ACTIVITY; } set { if (this.ID_OPERATOR_ACTIVITY != value) { this.ID_OPERATOR_ACTIVITY = value; this.OpState = OpChangeState.Modified; } } }
        public int? IdOperator { get { return this.ID_OPERATOR; } set { if (this.ID_OPERATOR != value) { this.ID_OPERATOR = value; this.OpState = OpChangeState.Modified; } } }
        public int IdActivity { get { return this.ID_ACTIVITY; } set { if (this.ID_ACTIVITY != value) { this.ID_ACTIVITY = value; this.OpState = OpChangeState.Modified; } } }
        public object ReferenceValue { get { return this.REFERENCE_VALUE; } set { if (!OpDataUtil.ValueEquals(this.REFERENCE_VALUE, value)) { this.REFERENCE_VALUE = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; } } }
        public bool Deny { get { return this.DENY; } set { if (this.DENY != value) { this.DENY = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; } } }
        public int? IdModule { get { return this.ID_MODULE; } set { if (this.ID_MODULE != value) { this.ID_MODULE = value; this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region Navigation Properties
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        private OpActivity _Activity;
        public OpActivity Activity { get { return this._Activity; } set { this._Activity = value; this.ID_ACTIVITY = (value == null) ? 0 : (int)value.ID_ACTIVITY; } }
        private OpModule _Module;
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.IdModule = (value == null) ? null : (int?)value.ID_MODULE; } }
    #endregion

    #region Custom Properties
        public OpChangeState OpState { get; set; }
        public bool Allow { get { return !Deny; } set { Deny = !value; } }
    #endregion

    #region Ctor
        public OpOperatorActivity()
            : base() { this.OpState = OpChangeState.New; }

        public OpOperatorActivity(DB_OPERATOR_ACTIVITY clone)
            : base(clone) { OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
        public override string ToString()
        {
            return "OperatorActivity [" + IdOperatorActivity.ToString() + "]";
        }
    #endregion

    #region ConvertList
        public static List<OpOperatorActivity> ConvertList(DB_OPERATOR_ACTIVITY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpOperatorActivity> ConvertList(DB_OPERATOR_ACTIVITY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpOperatorActivity> ret = new List<OpOperatorActivity>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpOperatorActivity(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpOperatorActivity> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpActivity> ActivityDict = dataProvider.GetActivity(list.Select(l => l.ID_ACTIVITY).Distinct().ToArray()).ToDictionary(l => l.ID_ACTIVITY);
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_MODULE);

                foreach (var loop in list)
                {
                    if (loop.ID_OPERATOR.HasValue)
                        loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value);
                    loop.Activity = ActivityDict.TryGetValue(loop.ID_ACTIVITY).Clone(dataProvider);
                    if (loop.ID_MODULE.HasValue)
                        loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value).Clone(dataProvider);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpOperatorActivity> list, IDataProvider dataProvider)
        {
            foreach (OpOperatorActivity loop in list)
            {
                if (loop.Activity != null)
                {
                    loop.Activity.ReferenceValue = loop.ReferenceValue;
                    loop.Activity.Deny = loop.Deny;
                    loop.Activity.IdModule = loop.IdModule;
                    loop.Activity.IdRoleActivity = null;
                    loop.Activity.IdOperatorActivity = loop.IdOperatorActivity;
                }
            }
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpOperatorActivity)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpOperatorActivity).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpOperatorActivity> Members
        public bool Equals(OpOperatorActivity other)
        {
            if (other == null)
                return false;
            return this.IdOperatorActivity.Equals(other.IdOperatorActivity);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpOperatorActivity)
                return this.IdOperatorActivity == ((OpOperatorActivity)obj).IdOperatorActivity;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpOperatorActivity left, OpOperatorActivity right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpOperatorActivity left, OpOperatorActivity right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdOperatorActivity.GetHashCode();
        }
    #endregion
    }
#endif
}