using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpAlarmData : DB_ALARM_DATA, IComparable, IEquatable<OpAlarmData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdAlarmData { get { return this.ID_ALARM_DATA; } set { this.ID_ALARM_DATA = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); }  }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region	Custom Properties

        [DataMember]
        public OpChangeState OpState { get; set; }
        public long IdData { get { return this.IdAlarmData; } set { this.IdAlarmData = value; } }
        public int Index { get { return this.IndexNbr; } set { this.IndexNbr = value; } }
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpAlarmData()
            : base() { this.OpState = OpChangeState.New; }

        public OpAlarmData(DB_ALARM_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            //return "AlarmData [" + IdAlarmData.ToString() + "]";
            return "AlarmData [" + Value.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpAlarmData> ConvertList(DB_ALARM_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmData> ConvertList(DB_ALARM_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAlarmData> ret = new List<OpAlarmData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpAlarmData> Members
        public bool Equals(OpAlarmData other)
        {
            if (other == null)
                return false;
            return this.IdAlarmData.Equals(other.IdAlarmData);
        }
    #endregion

    #region IOpDataProvider

    #region Clone
        public object Clone(IDataProvider dataProvider)
        {
            OpAlarmData clone = new OpAlarmData();
            clone.IdAlarmData = this.IdAlarmData;
            clone.IdDataType = this.IdDataType;
            clone.IndexNbr = this.IndexNbr;
            clone.Value = this.Value;
            clone.IdData = this.IdData;
            clone.Index = this.Index;


            if (dataProvider != null)
                clone = ConvertList(new DB_ALARM_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmData)
                return this.IdAlarmData == ((OpAlarmData)obj).IdAlarmData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmData left, OpAlarmData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmData left, OpAlarmData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmData.GetHashCode();
        }
    #endregion
    }
#endif
}