﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpCodeType : DB_CODE_TYPE, IComparable, IEquatable<OpCodeType>
	{
    #region Properties
        [XmlIgnore]
        public int IdCodeType { get { return this.ID_CODE_TYPE; } set { this.ID_CODE_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
		public int? IdDataTypeFormat { get { return this.ID_DATA_TYPE_FORMAT; } set { this.ID_DATA_TYPE_FORMAT = value; } }
    #endregion

    #region Navigation Properties
		private OpDescr _Descr;
		public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
		private OpDataTypeFormat _DataTypeFormat;
        [XmlIgnore]
		public OpDataTypeFormat DataTypeFormat { get { return this._DataTypeFormat; } set { this._DataTypeFormat = value; this.ID_DATA_TYPE_FORMAT = (value == null) ? null : (int?)value.ID_DATA_TYPE_FORMAT; } }
    #endregion

    #region Custom Properties
    #endregion

    #region Ctor
		public OpCodeType()
			: base() { }

		public OpCodeType(DB_CODE_TYPE clone)
			: base(clone) { }
    #endregion

    #region ToString
		public override string ToString()
		{
			return this.NAME;
		}
    #endregion

    #region ConvertList
		public static List<OpCodeType> ConvertList(DB_CODE_TYPE[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpCodeType> ConvertList(DB_CODE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpCodeType> ret = new List<OpCodeType>(list.Length);
			foreach (var loop in list)
			{
				OpCodeType insert = new OpCodeType(loop);
				ret.Add(insert);
			}

			if (loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpCodeType> list, IDataProvider dataProvider)
		{
			if (list != null && list.Count > 0)
			{
				Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
				Dictionary<int, OpDataTypeFormat> DataTypeFormatDict = dataProvider.GetDataTypeFormat(list.Where(l => l.ID_DATA_TYPE_FORMAT.HasValue).Select(l => l.ID_DATA_TYPE_FORMAT.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE_FORMAT);
				foreach (var loop in list)
				{
					if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
					if (loop.ID_DATA_TYPE_FORMAT.HasValue)
						loop.DataTypeFormat = DataTypeFormatDict.TryGetValue(loop.ID_DATA_TYPE_FORMAT.Value);
				}
			}
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpCodeType> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpCodeType)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpCodeType).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IEquatable<OpCodeType> Members
		public bool Equals(OpCodeType other)
		{
			if (other == null)
				return false;
			return this.IdCodeType.Equals(other.IdCodeType);
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpCodeType)
				return this.IdCodeType == ((OpCodeType)obj).IdCodeType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpCodeType left, OpCodeType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpCodeType left, OpCodeType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdCodeType.GetHashCode();
		}
    #endregion
	}
#endif
}