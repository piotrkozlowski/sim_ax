using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
    public class OpDepositoryElementData : DB_DEPOSITORY_ELEMENT_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpDepositoryElementData>
	{
    #region Properties
		public long IdDepositoryElementData { get { return this.ID_DEPOSITORY_ELEMENT_DATA; } set { this.ID_DEPOSITORY_ELEMENT_DATA = value; } }
		public int IdDepositoryElement { get { return this.ID_DEPOSITORY_ELEMENT; } set { this.ID_DEPOSITORY_ELEMENT = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
		public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
    #endregion

    #region Navigation Properties
		private OpDepositoryElement _DepositoryElement;
		public OpDepositoryElement DepositoryElement { get { return this._DepositoryElement; } set { this._DepositoryElement = value; this.ID_DEPOSITORY_ELEMENT = (value == null) ? 0 : (int)value.ID_DEPOSITORY_ELEMENT; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdDepositoryElementData; } set { IdDepositoryElementData = value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpDepositoryElementData()
			: base() { this.OpState = OpChangeState.New; }

		public OpDepositoryElementData(DB_DEPOSITORY_ELEMENT_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }

        public OpDepositoryElementData(OpDepositoryElementData clone)
            : base(clone) 
        {
            this.OpState = clone.OpState; 
        }

    #endregion

    #region ToString
		public override string ToString()
		{
			return "DepositoryElementData [" + IdDepositoryElementData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpDepositoryElementData> ConvertList(DB_DEPOSITORY_ELEMENT_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpDepositoryElementData> ConvertList(DB_DEPOSITORY_ELEMENT_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDepositoryElementData> ret = new List<OpDepositoryElementData>(list.Length);
			foreach (var loop in list)
			{
				OpDepositoryElementData insert = new OpDepositoryElementData(loop);
				if (loadNavigationProperties)
				{
					//insert.DepositoryElement = dataProvider.GetDepositoryElement(loop.ID_DEPOSITORY_ELEMENT);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}
				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDepositoryElementData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.DepositoryElement = dataProvider.GetDepositoryElement(this.ID_DEPOSITORY_ELEMENT);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDepositoryElementData clone = new OpDepositoryElementData();
            clone.IdData = this.IdData;
            clone.IdDepositoryElement = this.IdDepositoryElement;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEPOSITORY_ELEMENT_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpDepositoryElementData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpDepositoryElementData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IEquatable<OpDepositoryElementData> Members
		public bool Equals(OpDepositoryElementData other)
		{
			if (other == null)
				return false;
			return this.IdDepositoryElementData.Equals(other.IdDepositoryElementData);
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDepositoryElementData)
				return this.IdData == ((OpDepositoryElementData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDepositoryElementData left, OpDepositoryElementData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDepositoryElementData left, OpDepositoryElementData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}