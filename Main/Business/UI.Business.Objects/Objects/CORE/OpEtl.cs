using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpEtl : DB_ETL, IComparable, IReferenceType
    {
    #region Properties
        public int IdParam { get { return this.ID_PARAM; } set { if (this.ID_PARAM != value) { this.ID_PARAM = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string Code { get { return this.CODE; } set { if (this.CODE != value) { this.CODE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string Descr { get { return this.DESCR; } set { if (this.DESCR != value) { this.DESCR = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public object Value { get { return this.VALUE; } set { if (this.VALUE != value) { this.VALUE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpEtl()
			:base() {}
		
		public OpEtl(DB_ETL clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.CODE;
		}
    #endregion

    #region	ConvertList
		public static List<OpEtl> ConvertList(DB_ETL[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpEtl> ConvertList(DB_ETL[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpEtl> ret = new List<OpEtl>(list.Length);
			foreach (var loop in list)
			{
			OpEtl insert = new OpEtl(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpEtl> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpEtl)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpEtl).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region #region Implementation of IReferenceType

        public object GetReferenceKey()
        {
            return this.IdParam;
        }

        public object GetReferenceValue()
        {
            return this;
        }

        #endregion
    }
#endif
}