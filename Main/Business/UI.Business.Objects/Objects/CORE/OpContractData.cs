﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpContractData : DB_CONTRACT_DATA, IComparable, IEquatable<OpContractData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdContractData { get { return this.ID_CONTRACT_DATA; } set { this.ID_CONTRACT_DATA = value; } }
        public int IdContract { get { return this.ID_CONTRACT; } set { this.ID_CONTRACT = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }

        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpContract _Contract;
        public OpContract Contract { get { return this._Contract; } set { this._Contract = value; this.ID_CONTRACT = (value == null) ? 0 : (int)value.ID_CONTRACT; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpContractData()
            : base() { }

        public OpContractData(DB_CONTRACT_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ContractData [" + IdContractData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpContractData> ConvertList(DB_CONTRACT_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpContractData> ConvertList(DB_CONTRACT_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpContractData> ret = new List<OpContractData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpContractData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpContractData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<int, OpContract> ContractDict = dataProvider.GetContract(list.Select(l => l.ID_CONTRACT).Distinct().ToArray()).ToDictionary(l => l.ID_CONTRACT);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                   // loop.Contract = ContractDict.TryGetValue(loop.ID_CONTRACT);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpContractData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpContractData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpContractData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpContractData> Members
        public bool Equals(OpContractData other)
        {
            if (other == null)
                return false;
            return this.IdContractData.Equals(other.IdContractData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpContractData)
                return this.IdContractData == ((OpContractData)obj).IdContractData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpContractData left, OpContractData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpContractData left, OpContractData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdContractData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdContractData; } set { IdContractData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpContractData clone = new OpContractData();
            clone.IdData = this.IdData;
            clone.IdContract = this.IdContract;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_CONTRACT_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}