using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpConfigurationProfileData : DB_CONFIGURATION_PROFILE_DATA, IComparable, IEquatable<OpConfigurationProfileData>, IOpData, IOpDataProvider
    {
    #region Properties
        [XmlIgnore]
        public long IdConfigurationProfileData { get { return this.ID_CONFIGURATION_PROFILE_DATA; } set { this.ID_CONFIGURATION_PROFILE_DATA = value; } }
        [XmlIgnore]
        public long IdConfigurationProfile { get { return this.ID_CONFIGURATION_PROFILE; } set { this.ID_CONFIGURATION_PROFILE = value; } }
        [XmlIgnore]
        public int IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = value; } }
        [XmlIgnore]
        public int? IdDeviceTypeProfileStep { get { return this.ID_DEVICE_TYPE_PROFILE_STEP; } set { this.ID_DEVICE_TYPE_PROFILE_STEP = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }

        [XmlIgnore]
        public long IdData { get { return this.IdConfigurationProfileData; } set { this.IdConfigurationProfileData = value; } }
    #endregion

    #region	Navigation Properties
        //private OpConfigurationProfile _ConfigurationProfile;
        //public OpConfigurationProfile ConfigurationProfile { get { return this._ConfigurationProfile; } set { this._ConfigurationProfile = value; this.ID_CONFIGURATION_PROFILE = (value == null) ? 0 : (long)value.ID_CONFIGURATION_PROFILE; } }
        private OpDeviceType _DeviceType;
        [XmlIgnore]
        public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.ID_DEVICE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
        private OpDeviceTypeProfileStep _DeviceTypeProfileStep;
        [XmlIgnore]
        public OpDeviceTypeProfileStep DeviceTypeProfileStep { get { return this._DeviceTypeProfileStep; } set { this._DeviceTypeProfileStep = value; this.ID_DEVICE_TYPE_PROFILE_STEP = (value == null) ? null : (int?)value.ID_DEVICE_TYPE_PROFILE_STEP; } }
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpConfigurationProfileData()
            : base() { this.OpState = OpChangeState.New; }

        public OpConfigurationProfileData(DB_CONFIGURATION_PROFILE_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ConfigurationProfileData [" + IdConfigurationProfileData.ToString() + "]";
        }
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //if (this.IdConfigurationProfile != 0)
                //this.ConfigurationProfile = dataProvider.GetConfigurationProfile(this.IdConfigurationProfile);
            this.DataType = dataProvider.GetDataType(this.IdDataType);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpConfigurationProfileData clone = new OpConfigurationProfileData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.IdConfigurationProfile = this.IdConfigurationProfile;
            clone.IdDeviceType = this.IdDeviceType;

            if (dataProvider != null)
                clone = ConvertList(new DB_CONFIGURATION_PROFILE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        }
    #endregion

    #endregion

    #region	ConvertList
        public static List<OpConfigurationProfileData> ConvertList(DB_CONFIGURATION_PROFILE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpConfigurationProfileData> ConvertList(DB_CONFIGURATION_PROFILE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpConfigurationProfileData> ret = new List<OpConfigurationProfileData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpConfigurationProfileData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConfigurationProfileData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<long, OpConfigurationProfile> ConfigurationProfileDict = dataProvider.GetConfigurationProfile(list.Select(l => l.ID_CONFIGURATION_PROFILE).Distinct().ToArray()).ToDictionary(l => l.ID_CONFIGURATION_PROFILE);
                Dictionary<int, OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Select(l => l.ID_DEVICE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpDeviceTypeProfileStep> DeviceTypeProfileStepDict = dataProvider.GetDeviceTypeProfileStep(list.Where(l => l.ID_DEVICE_TYPE_PROFILE_STEP.HasValue).Select(l => l.ID_DEVICE_TYPE_PROFILE_STEP.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE_PROFILE_STEP);


                foreach (var loop in list)
                {
                    //loop.ConfigurationProfile = ConfigurationProfileDict.TryGetValue(loop.ID_CONFIGURATION_PROFILE);
                    loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DEVICE_TYPE_PROFILE_STEP.HasValue)
                        loop.DeviceTypeProfileStep = DeviceTypeProfileStepDict.TryGetValue(loop.ID_DEVICE_TYPE_PROFILE_STEP.Value);

                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConfigurationProfileData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConfigurationProfileData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConfigurationProfileData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpConfigurationProfileData> Members
        public bool Equals(OpConfigurationProfileData other)
        {
            if (other == null)
                return false;
            return this.IdConfigurationProfileData.Equals(other.IdConfigurationProfileData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpConfigurationProfileData)
                return this.IdConfigurationProfileData == ((OpConfigurationProfileData)obj).IdConfigurationProfileData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpConfigurationProfileData left, OpConfigurationProfileData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpConfigurationProfileData left, OpConfigurationProfileData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdConfigurationProfileData.GetHashCode();
        }
    #endregion
    }
#endif
}