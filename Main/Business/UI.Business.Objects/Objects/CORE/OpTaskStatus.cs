using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTaskStatus : DB_TASK_STATUS, IComparable, IReferenceType, IEquatable<OpTaskStatus>
    {
    #region Properties
        public int IdTaskStatus { get { return this.ID_TASK_STATUS; } set { this.ID_TASK_STATUS = value; } }
        public long IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        public bool IsFinished { get { return this.IS_FINISHED; } set { this.IS_FINISHED = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? 0 : (long)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties

        public string ComboBoxString
        {
            get { return this.ToString(); }
        }

    #endregion

    #region	Ctor
        public OpTaskStatus()
            : base() { }

        public OpTaskStatus(DB_TASK_STATUS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Description;
        }
    #endregion

    #region	ConvertList
        public static List<OpTaskStatus> ConvertList(DB_TASK_STATUS[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpTaskStatus> ConvertList(DB_TASK_STATUS[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTaskStatus> ret = new List<OpTaskStatus>(list.Length);
            foreach (var loop in list)
            {
                OpTaskStatus insert = new OpTaskStatus(loop);

                if (loadNavigationProperties)
                {

                    insert.Descr = dataProvider.GetDescr(new long[]{ loop.ID_DESCR }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel,  commandTimeout: commandTimeout).FirstOrDefault();



                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTaskStatus> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTaskStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTaskStatus).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Enum
        //generated from DB, don't change it!
        public enum Enum : int
        {
            Unknown = 0,
            Not_Started = 1,
            In_progress = 2,
            WaitingForAcceptance = 3,
            Finished_succesfully = 4,
            Finished_with_error = 5,
            Planned = 6,
            Canceled = 7,
            Suspended = 8,
            Verified = 9,
            RealizationNotPossible = 10,
            Rejected = 11,
            Aborted = 12,
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdTaskStatus;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region IEquatable<OpTaskStatus>

        public bool Equals(OpTaskStatus obj)
        {
            if (obj is OpTaskStatus)
                return this.IdTaskStatus == ((OpTaskStatus)obj).IdTaskStatus;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTaskStatus left, OpTaskStatus right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTaskStatus left, OpTaskStatus right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTaskStatus.GetHashCode();
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTaskStatus)
                return this.IdTaskStatus == ((OpTaskStatus)obj).IdTaskStatus;
            else
                return base.Equals(obj);
        }
    #endregion
    }
#endif
}