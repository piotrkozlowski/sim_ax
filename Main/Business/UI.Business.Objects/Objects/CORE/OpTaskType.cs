﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpTaskType : DB_TASK_TYPE, IComparable, IReferenceType, IEquatable<OpTaskType>
    {
    #region Properties
        public int IdTaskType { get { return this.ID_TASK_TYPE; } set { this.ID_TASK_TYPE = value; } }
        public int IdTaskTypeGroup { get { return this.ID_TASK_TYPE_GROUP; } set { this.ID_TASK_TYPE_GROUP = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Descr { get { return this.DESCR; } set { this.DESCR = value; } }
        public bool Remote { get { return this.REMOTE; } set { this.REMOTE = value; } }
        public int? DurationEstimate { get { return this.DURATION_ESTIMATE; } set { this.DURATION_ESTIMATE = value; } }
        public int IdOperatorCreator { get { return this.ID_OPERATOR_CREATOR; } set { this.ID_OPERATOR_CREATOR = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public bool Authorized { get { return this.AUTHORIZED; } set { this.AUTHORIZED = value; } }
        public bool ImrOperator { get { return this.IMR_OPERATOR; } set { this.IMR_OPERATOR = value; } }
        public object RowVersion { get { return this.ROW_VERSION; } set { this.ROW_VERSION = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpTaskTypeGroup _TaskTypeGroup;
        public OpTaskTypeGroup TaskTypeGroup { get { return this._TaskTypeGroup; } set { this._TaskTypeGroup = value; this.ID_TASK_TYPE_GROUP = (value == null) ? 0 : (int)value.ID_TASK_TYPE_GROUP; } }

        private OpDescr _Description;
        public OpDescr Description { get { return this._Description; } set { this._Description = value; this.DESCR = (value == null) ? "0" : ((long)value.ID_DESCR).ToString(); } }
    #endregion

    #region	Custom Properties
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpTaskType()
            : base() { }

        public OpTaskType(DB_TASK_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Description != null)
                return Description.Description;
            else
                return this.Name; // Kompatybilność wstecz
        }
    #endregion

    #region	ConvertList
        public static List<OpTaskType> ConvertList(DB_TASK_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpTaskType> ConvertList(DB_TASK_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
            bool loadCustomData = true)
        {
            List<OpTaskType> ret = new List<OpTaskType>(list.Length);
            list.ToList().ForEach(db_object => ret.Add(new OpTaskType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction, transactionLevel, commandTimeout);

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTaskType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {

        }
    #endregion

        #region LoadNavigationProperties

        public static void LoadNavigationProperties(ref List<OpTaskType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            Dictionary<long, OpDescr> descrDict = dataProvider.GetDescr(list.Select(x => Convert.ToInt64(x.DESCR)).ToArray()).Where(x => x.IdLanguage == (int)dataProvider.UserLanguage).Distinct(x => x.IdDescr).ToDictionary(k => k.IdDescr);

            foreach (OpTaskType item in list)
            {
                item.TaskTypeGroup = dataProvider.GetTaskTypeGroup(new int[] { item.ID_TASK_TYPE_GROUP }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();

                try
                {
                    long idDescr = Convert.ToInt64(item.DESCR);
                    if (descrDict.ContainsKey(idDescr))
                    {
                        item.Description = descrDict[idDescr];
                    }
                }
                catch
                {
                    
                }

                // Kompatybilność wstecz
                switch (dataProvider.UserLanguage)
                {
                    case IMR.Suite.Common.Enums.Language.Polish:
                        switch (item.ID_TASK_TYPE)
                        {
                            case 16:
                                item.Name = "Rozpoznanie/przegląd instalacji";
                                break;
                            case 18:
                                item.Name = "Serwis instalacji";
                                break;
                            case 19:
                                item.Name = "Montaż instalacji";
                                break;
                        }
                        break;
                    case IMR.Suite.Common.Enums.Language.English:
                        switch (item.ID_TASK_TYPE)
                        {
                            case 16:
                                item.Name = "Maintenance";
                                break;
                            case 18:
                                item.Name = "Service";
                                break;
                            case 19:
                                item.Name = "Installation";
                                break;
                        }
                        break;
                }
            }
        }

        #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTaskType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTaskType).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Enum
        public enum Enum
        {
            Maintenance = 16,
            Service = 18,
            Installation = 19,
            Deinstallation = 20,
            Readout = 21,
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdTaskType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region IEquatable<OpTaskType>

        public bool Equals(OpTaskType obj)
        {
            if (obj is OpTaskType)
                return this.IdTaskType == ((OpTaskType)obj).IdTaskType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTaskType left, OpTaskType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTaskType left, OpTaskType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTaskType.GetHashCode();
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTaskType)
                return this.IdTaskType == ((OpTaskType)obj).IdTaskType;
            else
                return base.Equals(obj);
        }
    #endregion
    }
#endif
}