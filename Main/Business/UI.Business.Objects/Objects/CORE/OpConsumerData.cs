using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerData : DB_CONSUMER_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpConsumerData>
    {
    #region Properties
        [XmlIgnore]
        public long IdConsumerData { get { return this.ID_CONSUMER_DATA; } set { this.ID_CONSUMER_DATA = value; } }
        [XmlIgnore]
        public int IdConsumer { get { return this.ID_CONSUMER; } set { this.ID_CONSUMER = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpConsumer _Consumer;
        [XmlIgnore]
        public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null) ? 0 : (int)value.ID_CONSUMER; } }
		private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdConsumerData; } set { IdConsumerData = value; } }
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion
		
    #region Ctor
		public OpConsumerData()
			: base() { this.OpState = OpChangeState.New; }
		
		public OpConsumerData(DB_CONSUMER_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion
		
    #region ToString
		public override string ToString()
		{
			return "ConsumerData [" + IdConsumerData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpConsumerData> ConvertList(DB_CONSUMER_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpConsumerData> ConvertList(DB_CONSUMER_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpConsumerData> ret = new List<OpConsumerData>(list.Length);
			foreach (var loop in list)
			{
				OpConsumerData insert = new OpConsumerData(loop);
				if(loadNavigationProperties)
				{
				    //insert.Consumer = dataProvider.GetConsumer(loop.ID_CONSUMER); 									
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE); 
			    }
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConsumerData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.Consumer = dataProvider.GetConsumer(this.ID_CONSUMER);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        public object Clone(IDataProvider dataProvider)
        {
            OpConsumerData clone = new OpConsumerData();
            clone.IdData = this.IdData;
            clone.IdConsumer = this.IdConsumer;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_CONSUMER_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerData> Members
        public bool Equals(OpConsumerData other)
        {
            if (other == null)
				return false;
			return this.IdConsumerData.Equals(other.IdConsumerData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerData)
				return this.IdData == ((OpConsumerData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerData left, OpConsumerData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerData left, OpConsumerData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}