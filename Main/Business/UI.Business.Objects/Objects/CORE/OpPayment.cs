using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpPayment : DB_PAYMENT, IComparable, IEquatable<OpPayment>
    {
    #region Properties
        public int IdPayment { get { return this.ID_PAYMENT;} set {this.ID_PAYMENT = value;}}
        public int IdConsumer { get { return this.ID_CONSUMER;} set {this.ID_CONSUMER = value;}}
        public int IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
        public int IdDistributor { get { return this.ID_DISTRIBUTOR;} set {this.ID_DISTRIBUTOR = value;}}
        public int IdDistributorSupplier { get { return this.ID_DISTRIBUTOR_SUPPLIER;} set {this.ID_DISTRIBUTOR_SUPPLIER = value;}}
        public DateTime Date { get { return this.DATE;} set {this.DATE = value; } }
        public Double TotalPayment { get { return this.TOTAL_PAYMENT;} set {this.TOTAL_PAYMENT = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumer _Consumer;
				public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null)? 0 : (int)value.ID_CONSUMER; } }
				private OpOperator _Operator;
				public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }
				private OpDistributor _Distributor;
				public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null)? 0 : (int)value.ID_DISTRIBUTOR; } }
				private OpDistributorSupplier _DistributorSupplier;
				public OpDistributorSupplier DistributorSupplier { get { return this._DistributorSupplier; } set { this._DistributorSupplier = value; this.ID_DISTRIBUTOR_SUPPLIER = (value == null)? 0 : (int)value.ID_DISTRIBUTOR_SUPPLIER; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpPayment()
			:base() {}
		
		public OpPayment(DB_PAYMENT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "Payment [" + IdPayment.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpPayment> ConvertList(DB_PAYMENT[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpPayment> ConvertList(DB_PAYMENT[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpPayment> ret = new List<OpPayment>(list.Length);
			foreach (var loop in list)
			{
			OpPayment insert = new OpPayment(loop);
				
				if(loadNavigationProperties)
				{
											
																									insert.Consumer = dataProvider.GetConsumer(loop.ID_CONSUMER); 
													
																									insert.Operator = dataProvider.GetOperator(loop.ID_OPERATOR); 
													
																									insert.Distributor = dataProvider.GetDistributor(loop.ID_DISTRIBUTOR); 
													
																									insert.DistributorSupplier = dataProvider.GetDistributorSupplier(loop.ID_DISTRIBUTOR_SUPPLIER); 
													
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpPayment> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPayment)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPayment).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpPayment> Members
        public bool Equals(OpPayment other)
        {
            if (other == null)
				return false;
			return this.IdPayment.Equals(other.IdPayment);
        }
    #endregion
    }
#endif
}