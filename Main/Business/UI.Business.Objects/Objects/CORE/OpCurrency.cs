using IMR.Suite.Data.DB;
using System;
using System.Collections.Generic;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpCurrency : DB_CURRENCY, IComparable, IReferenceType, IEquatable<OpCurrency>
    {
    #region Properties
            public int IdCurrency { get { return this.ID_CURRENCY;} set {this.ID_CURRENCY = value;}}
                public string Symbol { get { return this.SYMBOL;} set {this.SYMBOL = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpCurrency()
			:base() {}
		
		public OpCurrency(DB_CURRENCY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpCurrency> ConvertList(DB_CURRENCY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpCurrency> ConvertList(DB_CURRENCY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpCurrency> ret = new List<OpCurrency>(list.Length);
			foreach (var loop in list)
			{
			OpCurrency insert = new OpCurrency(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
																									if (loop.ID_DESCR.HasValue)
								insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpCurrency> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpCurrency)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpCurrency).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpCurrency> Members
        public bool Equals(OpCurrency other)
        {
            if (other == null)
				return false;
			return this.IdCurrency.Equals(other.IdCurrency);
        }
        #endregion

    #region Implementation of IReferenceType

        public object GetReferenceKey()
        {
            return this.IdCurrency;
        }

        public object GetReferenceValue()
        {
            return this;
        }

    #endregion
    }
#endif
}