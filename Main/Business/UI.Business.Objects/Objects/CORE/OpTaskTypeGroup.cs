using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpTaskTypeGroup : DB_TASK_TYPE_GROUP, IComparable
    {
    #region Properties
            public int IdTaskTypeGroup { get { return this.ID_TASK_TYPE_GROUP;} set {this.ID_TASK_TYPE_GROUP = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public string Descr { get { return this.DESCR;} set {this.DESCR = value;}}
                public int? IdTaskTypeGroupParent { get { return this.ID_TASK_TYPE_GROUP_PARENT;} set {this.ID_TASK_TYPE_GROUP_PARENT = value;}}
                public int ILeft { get { return this.I_LEFT;} set {this.I_LEFT = value;}}
                public int IRight { get { return this.I_RIGHT;} set {this.I_RIGHT = value;}}
                public object RowVersion { get { return this.ROW_VERSION;} set {this.ROW_VERSION = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpTaskTypeGroup()
			:base() {}
		
		public OpTaskTypeGroup(DB_TASK_TYPE_GROUP clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpTaskTypeGroup> ConvertList(DB_TASK_TYPE_GROUP[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpTaskTypeGroup> ConvertList(DB_TASK_TYPE_GROUP[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpTaskTypeGroup> ret = new List<OpTaskTypeGroup>(list.Length);
			foreach (var loop in list)
			{
			OpTaskTypeGroup insert = new OpTaskTypeGroup(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
											
											
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTaskTypeGroup> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTaskTypeGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTaskTypeGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}