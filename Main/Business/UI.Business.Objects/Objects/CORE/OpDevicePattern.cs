using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDevicePattern : DB_DEVICE_PATTERN, IComparable, IEquatable<OpDevicePattern>
    {
    #region Properties
        [XmlIgnore]
        public long IdPattern { get { return this.ID_PATTERN; } set { this.ID_PATTERN = value; } }
        [XmlIgnore]
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
        public int? IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        [XmlIgnore]
        public int CreatedBy { get { return this.CREATED_BY; } set { this.CREATED_BY = value; } }
        [XmlIgnore]
        public int IdTemplate { get { return this.ID_TEMPLATE; } set { this.ID_TEMPLATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? null : (int?)value.ID_DISTRIBUTOR; } }
        private OpDeviceTemplate _Template;
        [XmlIgnore]
        public OpDeviceTemplate Template { get { return this._Template; } set { this._Template = value; this.ID_TEMPLATE = value.ID_TEMPLATE; } }
        private OpOperator _CreatedOperator;
        public OpOperator CreatedOperator { get { return this._CreatedOperator; } set { this._CreatedOperator = value; this.CREATED_BY = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
        public List<OpDevicePatternData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpDevicePattern()
            : base()
        {
            this.DataList = new List<OpDevicePatternData>();
        }

        public OpDevicePattern(DB_DEVICE_PATTERN clone)
            : base(clone)
        {
            this.DataList = new List<OpDevicePatternData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDevicePattern> ConvertList(DB_DEVICE_PATTERN[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDevicePattern> ConvertList(DB_DEVICE_PATTERN[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDevicePattern> ret = new List<OpDevicePattern>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDevicePattern(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDevicePattern> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Where(l => l.ID_DISTRIBUTOR.HasValue).Select(l => l.ID_DISTRIBUTOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpDeviceTemplate> TemplateDict = dataProvider.GetDeviceTemplate(list.Select(l => l.ID_TEMPLATE).Distinct().ToArray()).ToDictionary(l => l.ID_TEMPLATE);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.CREATED_BY).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    if (loop.ID_DISTRIBUTOR.HasValue)
                        loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR.Value);
                    OpDeviceTemplate Template = TemplateDict.TryGetValue(loop.ID_TEMPLATE);
                    if (Template != null)
                        loop.Template = TemplateDict.TryGetValue(loop.ID_TEMPLATE);
                    loop.CreatedOperator = OperatorDict.TryGetValue(loop.CREATED_BY);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDevicePattern> list, IDataProvider dataProvider)
        {
            foreach (OpDevicePattern devicePattern in list)
            {
                devicePattern.DataList = dataProvider.GetDevicePatternData(devicePattern.IdPattern);
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDevicePattern)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDevicePattern).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDevicePattern> Members
        public bool Equals(OpDevicePattern other)
        {
            if (other == null)
                return false;
            return this.IdPattern.Equals(other.IdPattern);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public OpDevicePattern Clone(IDataProvider dataProvider)
        {
            OpDevicePattern clone = new OpDevicePattern();
            clone.IdPattern = this.IdPattern;
            clone.SerialNbr = this.SerialNbr;
            clone.IdDistributor = this.IdDistributor;
            clone.Name = this.Name;
            clone.CreationDate = this.CreationDate;
            clone.CreatedBy = this.CreatedBy;
            clone.IdTemplate = this.IdTemplate;
            clone.DataList = this.DataList;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEVICE_PATTERN[] { clone }, dataProvider)[0];

            return clone;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDevicePattern)
                return this.IdPattern == ((OpDevicePattern)obj).IdPattern;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDevicePattern left, OpDevicePattern right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDevicePattern left, OpDevicePattern right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPattern.GetHashCode();
        }
    #endregion
    }
#endif
}