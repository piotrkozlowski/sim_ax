using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionData : DB_ACTION_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpActionData>
    {
    #region Properties
        [XmlIgnore]
        public long IdActionData { get { return this.ID_ACTION_DATA; } set { this.ID_ACTION_DATA = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore] 
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public OpChangeState OpState { get; set; }
		public long IdData { get { return this.IdActionData; } set { this.IdActionData = value; } }
		public int Index { get { return this.IndexNbr; } set { this.IndexNbr = value; } }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
        public OpActionData()
			: base() { this.OpState = OpChangeState.New; }

        public OpActionData(DB_ACTION_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
        public override string ToString()
        {
            return "ActionData [" + IdActionData.ToString() + "]";
        }
    #endregion

    #region ConvertList
        public static List<OpActionData> ConvertList(DB_ACTION_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpActionData> ConvertList(DB_ACTION_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActionData> ret = new List<OpActionData>(list.Length);
            foreach (var loop in list)
            {
                OpActionData insert = new OpActionData(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionData> Members
        public bool Equals(OpActionData other)
        {
            if (other == null)
                return false;
            return this.IdData.Equals(other.IdData) && this.IdDataType.Equals(other.IdDataType) && this.Index.Equals(other.Index);
        }
    #endregion

    #region IOpDataProvider

    #region Clone
        public object Clone(IDataProvider dataProvider)
		{
			OpActionData clone = new OpActionData();
			clone.IdActionData = this.IdActionData;
			clone.IdData = this.IdData;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_ACTION_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
		public void AssignReferences(IDataProvider dataProvider)
		{
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
			if (obj is OpActionData)
				return this.IdData == ((OpActionData)obj).IdData &&
						this.IdDataType == ((OpActionData)obj).IdDataType &&
						this.Index == ((OpActionData)obj).Index;
			else
				return base.Equals(obj);
        }
        public static bool operator ==(OpActionData left, OpActionData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionData left, OpActionData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdData.GetHashCode() ^ IdDataType.GetHashCode() ^ Index.GetHashCode();
        }
    #endregion
    }
#endif
}