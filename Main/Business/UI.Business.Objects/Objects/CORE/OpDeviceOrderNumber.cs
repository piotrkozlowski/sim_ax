using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceOrderNumber : DB_DEVICE_ORDER_NUMBER, IComparable, IEquatable<OpDeviceOrderNumber>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
            public int IdDeviceOrderNumber { get { return this.ID_DEVICE_ORDER_NUMBER;} set {this.ID_DEVICE_ORDER_NUMBER = value;}}
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public string FirstQuarter { get { return this.FIRST_QUARTER; } set { this.FIRST_QUARTER = value; } }
        [XmlIgnore]
        public string SecondQuarter { get { return this.SECOND_QUARTER; } set { this.SECOND_QUARTER = value; } }
        [XmlIgnore]
        public string ThirdQuarter { get { return this.THIRD_QUARTER; } set { this.THIRD_QUARTER = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public OpDataList<OpDeviceOrderNumberData> DataList { get; set; }

        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion
		
    #region	Ctor
		public OpDeviceOrderNumber()
			:base() 
        {
            DataList = new OpDataList<OpDeviceOrderNumberData>();
        }
		
		public OpDeviceOrderNumber(DB_DEVICE_ORDER_NUMBER clone)
			:base(clone) 
        {
            DataList = new OpDataList<OpDeviceOrderNumberData>();
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            if (String.IsNullOrEmpty(this.FirstQuarter) && String.IsNullOrEmpty(this.SecondQuarter) && String.IsNullOrEmpty(this.ThirdQuarter))
                return this.Name;
            return String.Format("{0} {1}-{2}-{3}", this.Name, this.FirstQuarter, this.SecondQuarter, this.ThirdQuarter);
		}
    #endregion

    #region	ConvertList
		public static List<OpDeviceOrderNumber> ConvertList(DB_DEVICE_ORDER_NUMBER[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDeviceOrderNumber> ConvertList(DB_DEVICE_ORDER_NUMBER[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpDeviceOrderNumber> ret = new List<OpDeviceOrderNumber>(list.Length);
			foreach (var loop in list)
			{
			OpDeviceOrderNumber insert = new OpDeviceOrderNumber(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
											
											
									}
				
				ret.Add(insert);
			}
            if (loadCustomData)
            {
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            }
			return ret;
		}
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceOrderNumber> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IDeviceOrderNumberDataTypes != null)
            {
                List<int> idDeviceOrderNumber = list.Select(l => l.IdDeviceOrderNumber).ToList();

                if (dataProvider.IDeviceOrderNumberDataTypes.Count > 0)
                {
                    List<OpDeviceOrderNumberData> data = dataProvider.GetDeviceOrderNumberDataFilter(IdDeviceOrderNumber: idDeviceOrderNumber.ToArray(), IdDataType: dataProvider.IDeviceOrderNumberDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpDeviceOrderNumber> deviceOrderNumberDataDict = list.ToDictionary<OpDeviceOrderNumber, int>(l => l.IdDeviceOrderNumber);
                        foreach (var dataValue in data)
                        {
                            deviceOrderNumberDataDict[dataValue.IdDeviceOrderNumber].DataList.Add(dataValue);
                        }
                    }
                }
            }
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceOrderNumber)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceOrderNumber).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable

        public bool Equals(OpDeviceOrderNumber other)
        {
            if (other == null)
                return false;
            return this.IdDeviceOrderNumber.Equals(other.IdDeviceOrderNumber);
        }

    #endregion

    #region IReferenceType

        public object GetReferenceKey()
        {
            return ID_DEVICE_ORDER_NUMBER;
        }

        public object GetReferenceValue()
        {
            return this;
        }

    #endregion
    }
#endif
}