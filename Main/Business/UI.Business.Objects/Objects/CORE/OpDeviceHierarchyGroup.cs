﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceHierarchyGroup : DB_DEVICE_HIERARCHY_GROUP, IComparable, IEquatable<OpDeviceHierarchyGroup>
    {
    #region Properties
        public long IdDeviceHierarchyGroup { get { return this.ID_DEVICE_HIERARCHY_GROUP; } set { this.ID_DEVICE_HIERARCHY_GROUP = value; } }
        public string DeviceHierarchySerialNbr { get { return this.DEVICE_HIERARCHY_SERIAL_NBR; } set { this.DEVICE_HIERARCHY_SERIAL_NBR = value; } }
        public int? IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public long? IdDepositoryLocation { get { return this.ID_DEPOSITORY_LOCATION; } set { this.ID_DEPOSITORY_LOCATION = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public DateTime? FinishDate { get { return this.FINISH_DATE; } set { this.FINISH_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? null : (int?)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
        public List<OpDeviceHierarchy> DeviceHierarchy { get; private set; }
    #endregion

    #region	Ctor
        public OpDeviceHierarchyGroup()
            : base() { }

        public OpDeviceHierarchyGroup(DB_DEVICE_HIERARCHY_GROUP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.DEVICE_HIERARCHY_SERIAL_NBR;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceHierarchyGroup> ConvertList(DB_DEVICE_HIERARCHY_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceHierarchyGroup> ConvertList(DB_DEVICE_HIERARCHY_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceHierarchyGroup> ret = new List<OpDeviceHierarchyGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceHierarchyGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceHierarchyGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Where(l => l.ID_DISTRIBUTOR.HasValue).Select(l => l.ID_DISTRIBUTOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                
                foreach (var loop in list)
                {
                    if (loop.ID_DISTRIBUTOR.HasValue)
                        loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR.Value);
                    loop.DeviceHierarchy = dataProvider.GetDeviceHierarchy(dataProvider.GetDeviceHierarchyInGroupFilter(IdDeviceHierarchyGroup: new long[] { loop.ID_DEVICE_HIERARCHY_GROUP }).Select(l => l.ID_DEVICE_HIERARCHY).Distinct().ToArray());
                }

            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceHierarchyGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceHierarchyGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceHierarchyGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceHierarchyGroup> Members
        public bool Equals(OpDeviceHierarchyGroup other)
        {
            if (other == null)
                return false;
            return this.IdDeviceHierarchyGroup.Equals(other.IdDeviceHierarchyGroup);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceHierarchyGroup)
                return this.IdDeviceHierarchyGroup == ((OpDeviceHierarchyGroup)obj).IdDeviceHierarchyGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceHierarchyGroup left, OpDeviceHierarchyGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceHierarchyGroup left, OpDeviceHierarchyGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceHierarchyGroup.GetHashCode();
        }
    #endregion
    }
#endif
}