using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;
using IMR.Suite.UI.Business.Objects.Custom;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpActor : DB_ACTOR, IComparable, IEquatable<OpActor>, IReferenceType, IOpChangeState, IOpDynamic, IOpDynamicProperty<OpActor>, IOpObject<OpActorData>
    {
        #region Properties
        [XmlIgnore]
        public int IdActor { get { return this.ID_ACTOR; } set { if (this.ID_ACTOR != value) { this.ID_ACTOR = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Surname { get { return this.SURNAME; } set { if (this.SURNAME != value) { this.SURNAME = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string City { get { return this.CITY; } set { if (this.CITY != value) { this.CITY = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Address { get { return this.ADDRESS; } set { if (this.ADDRESS != value) { this.ADDRESS = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Postcode { get { return this.POSTCODE; } set { if (this.POSTCODE != value) { this.POSTCODE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Email { get { return this.EMAIL; } set { if (this.EMAIL != value) { this.EMAIL = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Mobile { get { return this.MOBILE; } set { if (this.MOBILE != value) { this.MOBILE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Phone { get { return this.PHONE; } set { if (this.PHONE != value) { this.PHONE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdLanguage { get { return this.ID_LANGUAGE; } set { if (this.ID_LANGUAGE != value) { this.ID_LANGUAGE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Description { get { return this.DESCRIPTION; } set { if (this.DESCRIPTION != value) { this.DESCRIPTION = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public byte[] Avatar { get { return this.AVATAR; } set { if (this.AVATAR != value) { this.AVATAR = value; this.OpState = OpChangeState.Modified; } } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpLanguage _Language;
        public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null) ? 0 : (int)value.ID_LANGUAGE; } }
        #endregion

        #region	Custom Properties
        [XmlIgnore, DataMember]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;

        [DataMember]
        public OpChangeState OpState { get; set; }

        [XmlIgnore]
        public string GridLookUpEditString { get { return this.ToString(); } }
        [XmlIgnore]
        public string FullInfo
        {
            get
            {
                StringBuilder strBuilder = new StringBuilder();
                if (!String.IsNullOrEmpty(Name) || !String.IsNullOrEmpty(Surname))
                    strBuilder.AppendFormat("{0} {1}{2}", Name, Surname, Environment.NewLine);
                if (!String.IsNullOrEmpty(City) || !String.IsNullOrEmpty(Address) || !String.IsNullOrEmpty(Postcode))
                    strBuilder.AppendFormat("{0} {1} {2}{3}", Address, Postcode, City, Environment.NewLine);
                if (!String.IsNullOrEmpty(Email) || !String.IsNullOrEmpty(Mobile) || !String.IsNullOrEmpty(Phone))
                    strBuilder.AppendFormat("{0} {1} {2}{3}", Email, Mobile, Phone, Environment.NewLine);
                if (!String.IsNullOrEmpty(Description))
                    strBuilder.AppendFormat("{0}", Description);
                return strBuilder.ToString();
            }
        }

        [XmlIgnore]
        public string FullName { get { return String.Format("{0} {1}", this.Name, this.Surname); } }

        [XmlIgnore, DataMember]
        public List<OpActorGroup> CurrentGroups;
        [XmlIgnore, DataMember]
        public List<OpLocation> CurrentLocations;

        #endregion

        #region	Ctor
        public OpActor()
            : base()
        {
            this.OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpActorData>();
            this.CurrentGroups = new List<OpActorGroup>();
            this.CurrentLocations = new List<OpLocation>();
            this.DynamicValues = new Dictionary<string, object>();
        }

        public OpActor(DB_ACTOR clone)
            : base(clone)
        {
            this.OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpActorData>();
            this.CurrentGroups = new List<OpActorGroup>();
            this.CurrentLocations = new List<OpLocation>();
            this.DynamicValues = new Dictionary<string, object>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return String.Format("{0} {1}", this.Surname, this.Name);
        }
        #endregion

        #region	ConvertList
        public static List<OpActor> ConvertList(DB_ACTOR[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActor> ConvertList(DB_ACTOR[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpActor> ret = new List<OpActor>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActor(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data

            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActor> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LANGUAGE);
                foreach (var loop in list)
                {
                    loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpActor> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActor)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActor).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
        #endregion

        #region IEquatable<OpActor> Members

        public bool Equals(OpActor other)
        {
            if (other != null)
                return this.IdActor.Equals(other.IdActor);
            else
                return false;
        }

        #endregion
        #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdActor;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion

        #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdActor; }
        }
        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }
        [XmlIgnore]
        OpActor IOpDynamicProperty<OpActor>.Owner
        {
            get { return this; }
        }
        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }
        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdActor; }
            set { IdActor = Convert.ToInt32(value); }
        }

        [DataMember]
        public OpDataList<OpActorData> DataList { get; set; }
        #endregion
    }
#endif
}
