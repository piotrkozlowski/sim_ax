using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpRouteData : DB_ROUTE_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpRouteData>
	{
    #region Properties
		public long IdRouteData { get { return this.ID_ROUTE_DATA; } set { this.ID_ROUTE_DATA = value; } }
		public long IdRoute { get { return this.ID_ROUTE; } set { this.ID_ROUTE = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
		public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
    #endregion

    #region Navigation Properties
		private OpRoute _Route;
		public OpRoute Route { get { return this._Route; } set { this._Route = value; this.ID_ROUTE = (value == null) ? 0 : (long)value.ID_ROUTE; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdRouteData; } set { IdRouteData = value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpRouteData()
			: base() { this.OpState = OpChangeState.New; }

		public OpRouteData(DB_ROUTE_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "RouteData [" + IdRouteData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpRouteData> ConvertList(DB_ROUTE_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpRouteData> ConvertList(DB_ROUTE_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpRouteData> ret = new List<OpRouteData>(list.Length);
			foreach (var loop in list)
			{
				OpRouteData insert = new OpRouteData(loop);
				if (loadNavigationProperties)
				{
                    //insert.Route = dataProvider.GetRoute(loop.ID_ROUTE);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRouteData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpRouteData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpRouteData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IEquatable<OpRouteData> Members
		public bool Equals(OpRouteData other)
		{
			if (other == null)
				return false;
			return this.IdRouteData.Equals(other.IdRouteData);
		}
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public object Clone(IDataProvider dataProvider)
		{
			OpRouteData clone = new OpRouteData();
			clone.IdData = this.IdData;
			clone.IdRoute = this.IdRoute;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_ROUTE_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.Route = dataProvider.GetRoute(this.ID_ROUTE);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpRouteData)
				return this.IdData == ((OpRouteData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpRouteData left, OpRouteData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpRouteData left, OpRouteData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}