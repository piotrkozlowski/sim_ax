using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Linq;

using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [DebuggerDisplay("IdLocation:{IdLocation}; IdDataType:{DataType}; Index:{Index}; Value:{Value}; OpState:{OpState}")]
    [Serializable]
    public class OpData : DB_DATA, IOpData, IOpDataProvider, IComparable//, IData
    {
        #region Properties
        [XmlIgnore]
        public long IdData { get { return this.ID_DATA; } set { this.ID_DATA = value; } }
        [XmlIgnore]
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        [XmlIgnore]
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this, this.DataType); } }
        [XmlIgnore]
        public Enums.DataStatus DataStatus { get { return (Enums.DataStatus)this.STATUS; } set { this.STATUS = (int)value; } }
        #endregion

        #region Navigation Properties
        private OpMeter _Meter;
        [XmlIgnore]
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        [XmlIgnore]
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region Custom Properties
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
        //wykorzystywany do zmiany jednostki bazowej dla wybranego pomiaru
        [XmlIgnore]
        public OpUnit Unit { get; set; }
        [XmlIgnore]
        public string ValueUnit
        {
            get
            {
                if (DataType != null && Unit != null && !string.IsNullOrEmpty(Unit.Name))
                    return String.Format("{0} [{1}]", Value, Unit.Name);
                return (Value == null) ? null : Value.ToString();
            }
        }
        #endregion

        #region Ctor
        public OpData()
            : base() { this.OpState = OpChangeState.New; this.DataStatus = Enums.DataStatus.Normal; }

        public OpData(DB_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; /*this.DataStatus = Enums.DataStatus.Normal; tego tu nie moze by� !!!*/  }
        #endregion

        #region ToString
        public override string ToString()
        {
            return "Data [" + IdData.ToString() + "]";
        }
        #endregion

        #region ConvertList
        public static List<OpData> ConvertList(DB_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpData> ConvertList(DB_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpData> ret = new List<OpData>(list.Length);

            Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, loadNavigationProperties: loadNavigationProperties).ToDictionary(l => l.ID_DATA_TYPE);
            list.ToList().ForEach(db_object =>
                {
                    OpData newObject = new OpData(db_object);
                    newObject.DataType = DataTypeDict.TryGetValue(newObject.ID_DATA_TYPE);
                    try
                    {
                        // sdudzik - probujemy poprawic wartosc, zeby byla wlasciwego typu
                        // CELOWO (!) uzywamy VALUE, a nie Value - nie zmieniac tego!
                        // VALUE uzywane do tego, zeby raz poprawic typ rzeczywistej wartosci, Value to tylko get/set
                        newObject.VALUE = OpDataUtil.CorrectValueType(newObject.DataType, newObject.VALUE);
                    }
                    catch
                    {
                        // korekcja sie nie udala, oznaczamy wartosc jako bledna
                        newObject.OpState = OpChangeState.Incorrect;
                    }
                    ret.Add(newObject);
                });

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpData> list, IDataProvider dataProvider)
        {
            //if (list != null && list.Count > 0)
            //{
            //    foreach (OpData loop in list)
            //    {

            //    }
            //}
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpData> list, IDataProvider dataProvider)
        {
            foreach (OpData loop in list)
            {
                if (loop.DataType != null && loop.DataType.Unit != null)
                    loop.Unit = (OpUnit)loop.DataType.Unit.Clone(dataProvider);
            }
        }
        #endregion

        #region IOpDataProvider

        #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            if (this.ID_METER.HasValue)
                this.Meter = dataProvider.GetMeter(this.ID_METER.Value);
            if (this.ID_LOCATION.HasValue)
                this.Location = dataProvider.GetLocation(this.ID_LOCATION.Value);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
        #endregion

        #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpData clone = new OpData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.SerialNbr = this.SerialNbr;
            clone.IdLocation = this.IdLocation;
            clone.IdMeter = this.IdMeter;
            clone.DataStatus = this.DataStatus;

            if (dataProvider != null)
                clone = ConvertList(new DB_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        }
        #endregion

        #endregion

        #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpData).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpData)
                return this.IdData == ((OpData)obj).IdData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpData left, OpData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpData left, OpData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdData.GetHashCode();
        }
        #endregion

        #region CorrectValuetype
        [Obsolete("Use IMR.Suite.Common.DataType.CorrectValueType", false)]
        public void CorrectValueType()
        {
            if (DataType != null && Value is IConvertible)
            {
                try
                {
                    switch ((Enums.DataTypeClass)DataType.IdDataTypeClass)
                    {
                        case Enums.DataTypeClass.Boolean:
                            bool bVal = Convert.ToBoolean(Value);
                            Value = null;
                            Value = bVal;
                            break;
                        case Enums.DataTypeClass.Date:
                            DateTime dateVal = Convert.ToDateTime(Value).Date;
                            Value = null;
                            Value = dateVal;
                            break;

                        case Enums.DataTypeClass.Datetime:
                            DateTime dtVal = Convert.ToDateTime(Value);
                            Value = null;
                            Value = dtVal;
                            break;

                        case Enums.DataTypeClass.Integer:
                            long lVal = Convert.ToInt64(Value);
                            Value = null;
                            Value = lVal;
                            break;

                        case Enums.DataTypeClass.Real:
                            double dVal;
                            if (Value is string)
                                dVal = double.Parse(Value.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                            else
                                dVal = Convert.ToDouble(Value);
                            Value = null;
                            Value = dVal;
                            break;

                        case Enums.DataTypeClass.Decimal:
                            decimal decVal;
                            if (Value is string)
                                decVal = decimal.Parse(Value.ToString(), System.Globalization.CultureInfo.InvariantCulture);
                            else
                                decVal = Convert.ToDecimal(Value);
                            Value = null;
                            Value = decVal;
                            break;

                        case Enums.DataTypeClass.Text:
                            string sVal = Convert.ToString(Value);
                            Value = null;
                            Value = sVal;
                            break;

                        case Enums.DataTypeClass.Time:
                            TimeSpan tVal = Convert.ToDateTime(Value).TimeOfDay;
                            Value = null;
                            Value = tVal;
                            break;

                    }
                }
                catch (Exception ex) { }
            }
        }
        #endregion

        #region ChangeUnit
        public static List<Objects.CORE.OpData> ChangeUnit(List<Objects.CORE.OpData> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit newUnit = dataTypeUnitDict[dataType.IdDataType];
                            if (newUnit != null && dataType.Unit != null && dataType.Unit.IdUnitBase == newUnit.IdUnitBase && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                {
                                    d.Unit = newUnit;

                                    if (d.Value != null)
                                        d.Value = OpUnit.ChangeUnit(d.Value, d.DataType, dataType.Unit, newUnit,
                                            dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType) ? dataTypeFractionalDigitsDict[dataType.IdDataType] : null);
                                });
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        public static List<Objects.CORE.OpData> ChangeUnit(List<Objects.CORE.OpData> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (fromDataTypeUnitDict.Keys.Count > 0 && toDataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataType != null && fromDataTypeUnitDict.ContainsKey(dataType.IdDataType) && toDataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            OpUnit fromUnit = fromDataTypeUnitDict[dataType.IdDataType];
                            OpUnit toUnit = toDataTypeUnitDict[dataType.IdDataType];
                            if (fromUnit != null && toUnit != null && fromUnit.IdUnitBase == toUnit.IdUnitBase && fromUnit.IdUnit != toUnit.IdUnit && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                                dataSource.Where(d => d.IdDataType == dataType.IdDataType).ToList().ForEach(d => d.Unit = toUnit);
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = (fromUnit.Scale * Convert.ToDouble(d.Value) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDouble(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d =>
                                        {
                                            d.Value = ((decimal)fromUnit.Scale * Convert.ToDecimal(d.Value) + (decimal)fromUnit.Bias - (decimal)toUnit.Bias) / (decimal)toUnit.Scale;

                                            if (dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType)
                                                && dataTypeFractionalDigitsDict[dataType.IdDataType].HasValue && dataTypeFractionalDigitsDict[dataType.IdDataType].Value >= 0)
                                                d.Value = Math.Round(Convert.ToDecimal(d.Value, System.Globalization.CultureInfo.InvariantCulture), dataTypeFractionalDigitsDict[dataType.IdDataType].Value);
                                        });
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
    }
#endif
}