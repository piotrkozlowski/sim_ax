using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE.Schema;
using System.Data;
using System.Runtime.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataType : DB_DATA_TYPE, IComparable, IReferenceType, IOpChangeState
    {
    #region Properties
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { if (this.ID_DATA_TYPE != value) { this.ID_DATA_TYPE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdDataTypeClass { get { return this.ID_DATA_TYPE_CLASS; } set { if (this.ID_DATA_TYPE_CLASS != value) { this.ID_DATA_TYPE_CLASS = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { if (this.ID_REFERENCE_TYPE != value) { this.ID_REFERENCE_TYPE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdDataTypeFormat { get { return this.ID_DATA_TYPE_FORMAT; } set { if (this.ID_DATA_TYPE_FORMAT != value) { this.ID_DATA_TYPE_FORMAT = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public bool IsArchiveOnly { get { return this.IS_ARCHIVE_ONLY; } set { if (this.IS_ARCHIVE_ONLY != value) { this.IS_ARCHIVE_ONLY = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public bool IsRemoteRead { get { return this.IS_REMOTE_READ; } set { if (this.IS_REMOTE_READ != value) { this.IS_REMOTE_READ = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public bool IsRemoteWrite { get { return this.IS_REMOTE_WRITE; } set { if (this.IS_REMOTE_WRITE != value) { this.IS_REMOTE_WRITE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public bool IsEditable { get { return this.IS_EDITABLE; } set { if (this.IS_EDITABLE != value) { this.IS_EDITABLE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdUnit { get { return this.ID_UNIT; } set { if (this.ID_UNIT != value) { this.ID_UNIT = value; this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.IdDescr = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpDataTypeClass _DataTypeClass;
        [XmlIgnore]
        public OpDataTypeClass DataTypeClass { get { return this._DataTypeClass; } set { this._DataTypeClass = value; this.IdDataTypeClass = (value == null) ? 0 : (int)value.ID_DATA_TYPE_CLASS; } }
        private OpReferenceType _ReferenceType;
        [XmlIgnore]
        public OpReferenceType ReferenceType { get { return this._ReferenceType; } set { this._ReferenceType = value; this.IdReferenceType = (value == null) ? null : (int?)value.ID_REFERENCE_TYPE; } }
        private OpDataTypeFormat _DataTypeFormat;
        [XmlIgnore]
        public OpDataTypeFormat DataTypeFormat { get { return this._DataTypeFormat; } set { this._DataTypeFormat = value; this.IdDataTypeFormat = (value == null) ? null : (int?)value.ID_DATA_TYPE_FORMAT; } }
        private OpUnit _Unit;
        [XmlIgnore]
        public OpUnit Unit { get { return this._Unit; } set { this._Unit = value; this.IdUnit = (value == null) ? 0 : (int)value.ID_UNIT; } }
    #endregion

    #region	Custom Properties
        [XmlIgnore, DataMember]
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpDataType()
            : base()
        {
            OpState = OpChangeState.New;
        }

        public OpDataType(DB_DATA_TYPE clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpDataType> ConvertList(DB_DATA_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataType> ConvertList(DB_DATA_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDataType> ret = new List<OpDataType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataType> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpDataTypeClass> DataTypeClassDict = dataProvider.GetDataTypeClass(list.Select(l => l.ID_DATA_TYPE_CLASS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE_CLASS);
                Dictionary<int, OpReferenceType> ReferenceTypeDict = dataProvider.GetReferenceType(list.Where(l => l.ID_REFERENCE_TYPE.HasValue).Select(l => l.ID_REFERENCE_TYPE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_REFERENCE_TYPE);
                Dictionary<int, OpDataTypeFormat> DataTypeFormatDict = dataProvider.GetDataTypeFormat(list.Where(l => l.ID_DATA_TYPE_FORMAT.HasValue).Select(l => l.ID_DATA_TYPE_FORMAT.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE_FORMAT);
                Dictionary<int, OpUnit> UnitDict = dataProvider.GetUnit(list.Select(l => l.ID_UNIT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_UNIT);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                    {
                        OpDescr descr;
                        if (DescrDict.TryGetValue(loop.ID_DESCR.Value, out descr))
                            loop.Descr = descr;
                    }
                    loop.DataTypeClass = DataTypeClassDict.TryGetValue(loop.ID_DATA_TYPE_CLASS);
                    if (loop.ID_REFERENCE_TYPE.HasValue)
                        loop.ReferenceType = ReferenceTypeDict.TryGetValue(loop.ID_REFERENCE_TYPE.Value);
                    if (loop.ID_DATA_TYPE_FORMAT.HasValue)
                        loop.DataTypeFormat = DataTypeFormatDict.TryGetValue(loop.ID_DATA_TYPE_FORMAT.Value);
                    loop.Unit = UnitDict.TryGetValue(loop.ID_UNIT);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataType> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataType).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdDataType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region implicit operator XsdDataType
        public static implicit operator OpDataType(XsdDataType dataTypeXsd)
        {
            OpDataType dataType = new OpDataType();
            dataType.IdDataType = dataTypeXsd.IdDataType;
            dataType.Name = dataTypeXsd.Name;
            dataType.IdDescr = dataTypeXsd.IdDescr;
            dataType.IdDataTypeClass = dataTypeXsd.IdDataTypeClass;
            dataType.IdReferenceType = dataTypeXsd.IdReferenceType;
            dataType.IdDataTypeFormat = dataTypeXsd.IdDataTypeFormat;
            dataType.IsArchiveOnly = dataTypeXsd.IsArchiveOnly;
            dataType.IsRemoteRead = dataTypeXsd.IsRemoteRead;
            dataType.IsRemoteWrite = dataTypeXsd.IsRemoteWrite;
            dataType.IsEditable = dataTypeXsd.IsEditable;
            dataType.IdUnit = dataTypeXsd.IdUnit;
            return dataType;
        }

        public static implicit operator XsdDataType(OpDataType dataType)
        {
            XsdDataType dataTypeXsd = new XsdDataType();
            dataTypeXsd.IdDataType = dataType.IdDataType;
            dataTypeXsd.Name = dataType.Name;
            dataTypeXsd.IdDescr = dataType.IdDescr;
            dataTypeXsd.IdDataTypeClass = dataType.IdDataTypeClass;
            dataTypeXsd.IdReferenceType = dataType.IdReferenceType;
            dataTypeXsd.IdDataTypeFormat = dataType.IdDataTypeFormat;
            dataTypeXsd.IsArchiveOnly = dataType.IsArchiveOnly;
            dataTypeXsd.IsRemoteRead = dataType.IsRemoteRead;
            dataTypeXsd.IsRemoteWrite = dataType.IsRemoteWrite;
            dataTypeXsd.IsEditable = dataType.IsEditable;
            dataTypeXsd.IdUnit = dataType.IdUnit;
            return dataTypeXsd;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataType)
                return this.IdDataType == ((OpDataType)obj).IdDataType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataType left, OpDataType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataType left, OpDataType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataType.GetHashCode();
        }
    #endregion
    }
#endif
}