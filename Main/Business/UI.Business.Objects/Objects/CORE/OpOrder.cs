using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpOrder : DB_ORDER, IComparable, IEquatable<OpOrder>
    {
    #region Properties
            public int IdOrder { get { return this.ID_ORDER;} set {this.ID_ORDER = value;}}
                public int IdOrderStateType { get { return this.ID_ORDER_STATE_TYPE;} set {this.ID_ORDER_STATE_TYPE = value;}}
                public int IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
    #endregion

    #region	Navigation Properties
		private OpOrderStateType _OrderStateType;
				public OpOrderStateType OrderStateType { get { return this._OrderStateType; } set { this._OrderStateType = value; this.ID_ORDER_STATE_TYPE = (value == null)? 0 : (int)value.ID_ORDER_STATE_TYPE; } }
				private OpOperator _Operator;
				public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpOrder()
			:base() {}
		
		public OpOrder(DB_ORDER clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "Order [" + IdOrder.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpOrder> ConvertList(DB_ORDER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpOrder> ConvertList(DB_ORDER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpOrder> ret = new List<OpOrder>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpOrder(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpOrder> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpOrderStateType> OrderStateTypeDict = dataProvider.GetOrderStateType(list.Select(l => l.ID_ORDER_STATE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ORDER_STATE_TYPE);
	Dictionary<int,OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);			
				foreach (var loop in list)
				{
						loop.OrderStateType = OrderStateTypeDict.TryGetValue(loop.ID_ORDER_STATE_TYPE); 
		loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpOrder> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpOrder)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpOrder).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpOrder> Members
        public bool Equals(OpOrder other)
        {
            if (other == null)
				return false;
			return this.IdOrder.Equals(other.IdOrder);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpOrder)
				return this.IdOrder == ((OpOrder)obj).IdOrder;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpOrder left, OpOrder right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpOrder left, OpOrder right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdOrder.GetHashCode();
		}
    #endregion
    }
#endif
}