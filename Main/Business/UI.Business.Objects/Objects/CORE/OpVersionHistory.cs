using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionHistory : DB_VERSION_HISTORY, IComparable, IEquatable<OpVersionHistory>, IOpChangeState
    {
    #region Properties
        public long IdVersionHistory { get { return this.ID_VERSION_HISTORY; } set { if (this.ID_VERSION_HISTORY != value) { this.ID_VERSION_HISTORY = OpDataUtil.SetNewValue(this.ID_VERSION_HISTORY, value, this); } } }
        public long IdVersion { get { return this.ID_VERSION; } set { if (this.ID_VERSION != value) { this.ID_VERSION = OpDataUtil.SetNewValue(this.ID_VERSION, value, this); } } }
        public int? IdVersionState { get { return this.ID_VERSION_STATE; } set { if (this.ID_VERSION_STATE != value) { this.ID_VERSION_STATE = OpDataUtil.SetNewValue(this.ID_VERSION_STATE, value, this); } } }
        public DateTime StartTime { get { return this.START_TIME; } set { if (this.START_TIME != value) { this.START_TIME = OpDataUtil.SetNewValue(this.START_TIME, value, this); } } }
        public DateTime? EndTime { get { return this.END_TIME; } set { if (this.END_TIME != value) { this.END_TIME = OpDataUtil.SetNewValue(this.END_TIME, value, this); } } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { if (this.ID_OPERATOR != value) { this.ID_OPERATOR = OpDataUtil.SetNewValue(this.ID_OPERATOR, value, this); } } }
        public string Notes { get { return this.NOTES; } set { if (this.NOTES != value) { this.NOTES = OpDataUtil.SetNewValue(this.NOTES, value, this); } } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
		private OpVersion _Version;
		public OpVersion Version { get { return this._Version; } set { this._Version = value; this.ID_VERSION = (value == null)? 0 : (long)value.ID_VERSION; } }
		private OpVersionState _VersionState;
		public OpVersionState VersionState { get { return this._VersionState; } set { this._VersionState = value; this.ID_VERSION_STATE = (value == null)? null : (int?)value.ID_VERSION_STATE; } }
		private OpOperator _Operator;
		public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpVersionHistory()
			:base() {}
		
		public OpVersionHistory(DB_VERSION_HISTORY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
			return this.NOTES;
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionHistory> ConvertList(DB_VERSION_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionHistory> ConvertList(DB_VERSION_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionHistory> ret = new List<OpVersionHistory>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionHistory(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<long, OpVersion> VersionDict = dataProvider.GetVersion(list.Select(l => l.ID_VERSION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION);
                Dictionary<int, OpVersionState> VersionStateDict = dataProvider.GetVersionState(list.Where(l => l.ID_VERSION_STATE.HasValue).Select(l => l.ID_VERSION_STATE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_STATE);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.Version = VersionDict.TryGetValue(loop.ID_VERSION);
                    if (loop.ID_VERSION_STATE.HasValue)
                        loop.VersionState = VersionStateDict.TryGetValue(loop.ID_VERSION_STATE.Value);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionHistory> Members
        public bool Equals(OpVersionHistory other)
        {
            if (other == null)
				return false;
			return this.IdVersionHistory.Equals(other.IdVersionHistory);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionHistory)
				return this.IdVersionHistory == ((OpVersionHistory)obj).IdVersionHistory;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionHistory left, OpVersionHistory right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionHistory left, OpVersionHistory right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionHistory.GetHashCode();
		}
    #endregion
	}
#endif
}