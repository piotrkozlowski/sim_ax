using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAlarmDef : DB_ALARM_DEF, IComparable, IEquatable<OpAlarmDef>, IReferenceType
    {
        #region Properties
        public long IdAlarmDef { get { return this.ID_ALARM_DEF; } set { this.ID_ALARM_DEF = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public int IdAlarmType { get { return this.ID_ALARM_TYPE; } set { this.ID_ALARM_TYPE = value; } }
        public long IdDataTypeAlarm { get { return this.ID_DATA_TYPE_ALARM; } set { this.ID_DATA_TYPE_ALARM = value; } }
        public long? IdDataTypeConfig { get { return this.ID_DATA_TYPE_CONFIG; } set { this.ID_DATA_TYPE_CONFIG = value; } }
        public object SystemAlarmValue { get { return this.SYSTEM_ALARM_VALUE; } set { this.SYSTEM_ALARM_VALUE = value; } }
        public bool IsEnabled { get { return this.IS_ENABLED; } set { this.IS_ENABLED = value; } }
        public long? IdAlarmText { get { return this.ID_ALARM_TEXT; } set { this.ID_ALARM_TEXT = value; } }
        public long? IdAlarmData { get { return this.ID_ALARM_DATA; } set { this.ID_ALARM_DATA = value; } }
        public bool CheckLastAlarm { get { return this.CHECK_LAST_ALARM; } set { this.CHECK_LAST_ALARM = value; } }
        public int SuspensionTime { get { return this.SUSPENSION_TIME; } set { this.SUSPENSION_TIME = value; } }
        public bool IsConfirmable { get { return this.IS_CONFIRMABLE; } set { this.IS_CONFIRMABLE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        #endregion

        #region	Navigation Properties
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpAlarmType _AlarmType;
        public OpAlarmType AlarmType { get { return this._AlarmType; } set { this._AlarmType = value; this.ID_ALARM_TYPE = (value == null) ? 0 : (int)value.ID_ALARM_TYPE; } }
        private OpDataType _DataTypeAlarm;
        public OpDataType DataTypeAlarm { get { return this._DataTypeAlarm; } set { this._DataTypeAlarm = value; this.ID_DATA_TYPE_ALARM = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpDataType _DataTypeConfig;
        public OpDataType DataTypeConfig { get { return this._DataTypeConfig; } set { this._DataTypeConfig = value; this.ID_DATA_TYPE_CONFIG = (value == null) ? null : (long?)value.ID_DATA_TYPE; } }
        private OpAlarmText _AlarmText;
        public OpAlarmText AlarmText { get { return this._AlarmText; } set { this._AlarmText = value; this.ID_ALARM_TEXT = (value == null) ? null : (long?)value.ID_ALARM_TEXT; } }
       // private OpAlarmData _AlarmData;
       // public OpAlarmData AlarmData { get { return this._AlarmData; } set { this._AlarmData = value; this.ID_ALARM_DATA = (value == null) ? null : (long?)value.ID_ALARM_DATA; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpAlarmDef()
            : base() { }

        public OpAlarmDef(DB_ALARM_DEF clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpAlarmDef> ConvertList(DB_ALARM_DEF[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmDef> ConvertList(DB_ALARM_DEF[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAlarmDef> ret = new List<OpAlarmDef>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmDef(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmDef> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDeviceFilter(mergeIntoCache: false,SerialNbr: list.Where(l => l.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeterFilter(true ,false, list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocationFilter(true, true, false, list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpAlarmType> AlarmTypeDict = dataProvider.GetAlarmType(list.Select(l => l.ID_ALARM_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_TYPE);
                Dictionary<long, OpAlarmText> AlarmTextDict = dataProvider.GetAlarmText(list.Where(l => l.ID_ALARM_TEXT.HasValue).Select(l => l.ID_ALARM_TEXT.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_TEXT);
                //Dictionary<long, OpAlarmData> AlarmDataDict = dataProvider.GetAlarmData(list.Where(l => l.ID_ALARM_DATA.HasValue).Select(l => l.ID_ALARM_DATA.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_DATA);
                Dictionary<long, OpDataType> DataTypeAlarmDict = dataProvider.GetDataType(list.Where(l => l.ID_DATA_TYPE_ALARM > 0).Select(l => l.ID_DATA_TYPE_ALARM).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<long, OpDataType> DataTypeConfigDict = dataProvider.GetDataType(list.Where(l => l.ID_DATA_TYPE_CONFIG.HasValue).Select(l => l.ID_DATA_TYPE_CONFIG.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    if (loop.SERIAL_NBR.HasValue)
                        loop.Device= DeviceDict.TryGetValue(loop.SERIAL_NBR.Value);
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.AlarmType = AlarmTypeDict.TryGetValue(loop.ID_ALARM_TYPE);
                    if (loop.ID_ALARM_TEXT.HasValue)
                        loop.AlarmText = AlarmTextDict.TryGetValue(loop.ID_ALARM_TEXT.Value);
                  //  if (loop.ID_ALARM_DATA.HasValue)
                  //      loop.AlarmData = AlarmDataDict.TryGetValue(loop.ID_ALARM_DATA.Value);
                    if (loop.ID_DATA_TYPE_ALARM > 0)
                        loop.DataTypeAlarm = DataTypeAlarmDict.TryGetValue(loop.ID_DATA_TYPE_ALARM);
                    if (loop.ID_DATA_TYPE_CONFIG.HasValue)
                        loop.DataTypeConfig = DataTypeConfigDict.TryGetValue(loop.ID_DATA_TYPE_CONFIG.Value);

                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmDef> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmDef)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmDef).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpAlarmDef> Members
        public bool Equals(OpAlarmDef other)
        {
            if (other == null)
                return false;
            return this.IdAlarmDef.Equals(other.IdAlarmDef);
        }
        #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdAlarmDef;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmDef)
                return this.IdAlarmDef == ((OpAlarmDef)obj).IdAlarmDef;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmDef left, OpAlarmDef right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmDef left, OpAlarmDef right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmDef.GetHashCode();
        }
        #endregion
    }
#endif
}