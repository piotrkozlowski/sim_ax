using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Globalization;
using System.Text.RegularExpressions;
using IMR.Suite.UI.Business.Objects.Custom;
using System.Data;
using System.Runtime.Serialization;



namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpLocation : DB_LOCATION, IOpChangeState, IComparable, IEquatable<OpLocation>, IReferenceType, IOpDynamic, IOpDynamicProperty<OpLocation>, IOpDataProperty, IOpObject<OpData>
    {
    #region Properties
        [XmlIgnore]
        public long IdLocation
        {
            get { return this.ID_LOCATION; }
            set { if (this.ID_LOCATION != value) { this.ID_LOCATION = OpDataUtil.SetNewValue(this.ID_LOCATION, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public int IdLocationType
        {
            get { return this.ID_LOCATION_TYPE; }
            set { if (this.ID_LOCATION_TYPE != value) { this.ID_LOCATION_TYPE = OpDataUtil.SetNewValue(this.ID_LOCATION_TYPE, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public long? IdLocationPattern
        {
            get { return this.ID_LOCATION_PATTERN; }
            set { if (this.ID_LOCATION_PATTERN != value) { this.ID_LOCATION_PATTERN = OpDataUtil.SetNewValue(this.ID_LOCATION_PATTERN, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public long? IdDescrPattern
        {
            get { return this.ID_DESCR_PATTERN; }
            set { if (this.ID_DESCR_PATTERN != value) { this.ID_DESCR_PATTERN = OpDataUtil.SetNewValue(this.ID_DESCR_PATTERN, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public int IdDistributor
        {
            get { return this.ID_DISTRIBUTOR; }
            set { if (this.ID_DISTRIBUTOR != value) { this.ID_DISTRIBUTOR = OpDataUtil.SetNewValue(this.ID_DISTRIBUTOR, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public int? IdConsumer
        {
            get { return this.ID_CONSUMER; }
            set { if (this.ID_CONSUMER != value) { this.ID_CONSUMER = OpDataUtil.SetNewValue(this.ID_CONSUMER, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public int IdLocationStateType
        {
            get { return this.ID_LOCATION_STATE_TYPE; }
            set { if (this.ID_LOCATION_STATE_TYPE != value) { this.ID_LOCATION_STATE_TYPE = OpDataUtil.SetNewValue(this.ID_LOCATION_STATE_TYPE, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public bool InKpi
        {
            get { return this.IN_KPI; }
            set { if (this.IN_KPI != value) { this.IN_KPI = OpDataUtil.SetNewValue(this.IN_KPI, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public bool AllowGrouping
        {
            get { return this.ALLOW_GROUPING; }
            set { if (this.ALLOW_GROUPING != value) { this.ALLOW_GROUPING = OpDataUtil.SetNewValue(this.ALLOW_GROUPING, value, this); } }// this.OpState = OpChangeState.Modified; } }
        }
    #endregion

    #region Navigation Properties
        [DataMember]
        private OpLocationType _LocationType;
        [XmlIgnore]
        public OpLocationType LocationType { get { return this._LocationType; } set { this._LocationType = value; this.IdLocationType = (value == null) ? 0 : (int)value.ID_LOCATION_TYPE; } }
        [DataMember]
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.IdDistributor = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        [DataMember]
        private OpConsumer _Consumer;
        public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.IdConsumer = (value == null) ? null : (int?)value.ID_CONSUMER; } }
        [DataMember]
        private OpLocationStateType _LocationStateType;
        [XmlIgnore]
        public OpLocationStateType LocationStateType { get { return this._LocationStateType; } set { this._LocationStateType = value; this.IdLocationStateType = (value == null) ? 0 : (int)value.ID_LOCATION_STATE_TYPE; } }

    #endregion

    #region Custom Properties
        [XmlIgnore, DataMember]
        public OpChangeState OpState { get; set; }

        [CopyableProperty]
        [XmlIgnore]
        public string Name
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_NAME);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_NAME, value);
            }
        }
        [CopyableProperty]
        [XmlIgnore]
        public string PostalCode
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_ZIP);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_ZIP, value);
            }
        }
        [CopyableProperty]
        [XmlIgnore]
        public string City
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_CITY);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_CITY, value);
            }
        }
        [CopyableProperty]
        [XmlIgnore]
        public string Address
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_ADDRESS);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_ADDRESS, value);
            }
        }
        [CopyableProperty]
        [XmlIgnore]
        public string CID
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_CID);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_CID, value);
            }
        }
        [XmlIgnore]
        public string HostCID
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_HOST_CID);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_HOST_CID, value);
            }
        }
        [XmlIgnore]
        public string Country
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_COUNTRY);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_COUNTRY, value);
            }
        }
        [CopyableProperty]
        [XmlIgnore]
        public string FullAddress
        {
            get
            {
                StringBuilder strFullAddres = new StringBuilder();
                if (!String.IsNullOrEmpty(this.PostalCode))
                    strFullAddres.AppendFormat("{0} ", this.PostalCode);
                if (!String.IsNullOrEmpty(this.City))
                    strFullAddres.AppendFormat("{0}, ", this.City);
                if (!String.IsNullOrEmpty(this.Address))
                    strFullAddres.AppendFormat("{0} ", this.Address);
                return strFullAddres.ToString();
            }
        }
        [XmlIgnore]
        public string GoogleFullAddress
        {
            get
            {
                StringBuilder strFullAddres = new StringBuilder();
                if (!String.IsNullOrEmpty(this.PostalCode))
                    strFullAddres.AppendFormat("{0} ", this.PostalCode);
                if (!String.IsNullOrEmpty(this.City))
                    strFullAddres.AppendFormat("{0}, ", this.City);
                if (!String.IsNullOrEmpty(this.Address))
                    strFullAddres.AppendFormat("{0}, ", this.Address);
                if (!String.IsNullOrEmpty(this.Country))
                    strFullAddres.AppendFormat("{0} ", this.Country);
                return strFullAddres.ToString();
            }
        }
        [XmlIgnore]
        public DateTime? CreationDate
        {
            get
            {
                return DataList.TryGetNullableValue<DateTime>(DataType.LOCATION_CREATION_DATE);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_CREATION_DATE, value);
            }
        }
        [XmlIgnore]
        public double Latitude
        {
            get
            {
                return DataList.TryGetValue<double>(DataType.LOCATION_LATITUDE, CultureInfo.InvariantCulture);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_LATITUDE, value);
            }
        }
        [XmlIgnore]
        public double Longitude
        {
            get
            {
                return DataList.TryGetValue<double>(DataType.LOCATION_LONGITUDE, CultureInfo.InvariantCulture);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_LONGITUDE, value);
            }
        }
        [XmlIgnore]
        public string Memo
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.LOCATION_MEMO);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_MEMO, value);
            }
        }
        [XmlIgnore]
        public int? IdImrServer
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.LOCATION_IMR_SERVER_ID);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_IMR_SERVER_ID, value);
            }
        }
        private OpImrServer _ImrServer;
        [XmlIgnore]
        public OpImrServer ImrServer
        {
            get { return _ImrServer; }
            set
            {
                _ImrServer = value;
                if (value != null)
                {
                    IdImrServer = value.IdImrServer;
                }
                else
                {
                    IdImrServer = null;
                }
            }
        }
        [XmlIgnore]
        public int? IdActor
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.LOCATION_ACTOR_ID);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_ACTOR_ID, value);
            }
        }
        private OpActor _Actor;
        public OpActor Actor
        {
            get { return _Actor; }
            set
            {
                _Actor = value;
                if (value != null)
                {
                    IdActor = value.IdActor;
                }
                else
                {
                    IdActor = null;
                }
            }
        }

        //lokalizacja wzorcowa
        //wykorzystane na razie tylko w LocationDetailsControl (SIMA)
        //nie jest pobierane domy�lnie w ConvertList
        [DataMember]
        private OpLocation _LocationPattern;
        public OpLocation LocationPattern { get { return this._LocationPattern; } set { this._LocationPattern = value; this.IdLocationPattern = (value == null) ? null : (long?)value.ID_LOCATION; } }

        [XmlIgnore]
        public long IdLocationOriginal
        {
            get
            {
                return DataList.TryGetValue<long>(DataType.LOCATION_SOURCE_SERVER_ID);
            }
            set
            {
                DataList.SetValue(DataType.LOCATION_SOURCE_SERVER_ID, value);
            }
        }
        [XmlIgnore]
        public string GridLookUpEditString
        {
            get
            {
                StringBuilder strFullAddres = new StringBuilder();
                if (!String.IsNullOrEmpty(this.CID))
                    strFullAddres.AppendFormat("[{0}] ", this.CID);
                if (!String.IsNullOrEmpty(this.Name))
                    strFullAddres.AppendFormat("{0}, ", this.Name);
                if (!String.IsNullOrEmpty(this.FullAddress))
                    strFullAddres.AppendFormat("{0} ", this.FullAddress);
                return strFullAddres.ToString();
            }
        }
         [XmlIgnore]
        public decimal MaxTruckSize
        {
            get
            {
                return DataList.TryGetValue<decimal>(DataType.REFUEL_TRUCK_MAX_SIZE);
            }
            set
            {
                DataList.SetValue(DataType.REFUEL_TRUCK_MAX_SIZE, value);
            }
        }

        public object Tag;

        [XmlIgnore, DataMember]
        public OpArea CurrentArea { get; set; }
        [XmlIgnore, DataMember]
        public OpDistributorSupplier CurrentSupplier { get; set; }
        [XmlIgnore, DataMember]
        public OpTariff CurrentTariff { get; set; }

        [XmlIgnore, DataMember]
        public List<OpDevice> CurrentDevices;
        [XmlIgnore, DataMember]
        public List<OpMeter> CurrentMeters;
        [XmlIgnore, DataMember]
        public List<OpLocation> ParentGroups;

        [XmlIgnore]
        public OpDevice CurrentDevice { get { return CurrentDevices.FirstOrDefault(); } }
        [XmlIgnore]
        public OpMeter CurrentMeter { get { return CurrentMeters.FirstOrDefault(); } }

        [DataMember]
        private Dictionary<string, object> dynamicValues;
        private OpDynamicPropertyDict dynamicProperties;

    #endregion

    #region Ctor
        public OpLocation()
            : base()
        {
            this.OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpData>();
            this.CurrentDevices = new List<OpDevice>();
            this.CurrentMeters = new List<OpMeter>();
            this.ParentGroups = new List<OpLocation>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpLocation(DB_LOCATION clone)
            : base(clone)
        {
            this.OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpData>();
            this.CurrentDevices = new List<OpDevice>();
            this.CurrentMeters = new List<OpMeter>();
            this.ParentGroups = new List<OpLocation>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpLocation(OpLocation clone)
            : base(clone)
        {
            this.OpState = clone.OpState;
            this.DataList = new OpDataList<OpData>();
            if (clone.DataList != null)
            {
                foreach (OpData tdItem in clone.DataList)
                {
                    this.DataList.Add(new OpData(tdItem));
                }
            }
            this.CurrentDevices = clone.CurrentDevices;
            this.CurrentMeters = clone.CurrentMeters;
            this.ParentGroups = clone.ParentGroups;
            this.dynamicValues = clone.dynamicValues;
            this.dynamicProperties = clone.dynamicProperties;
        }
    #endregion

    #region ToString()
        public override string ToString()
        {
            string retStr = String.Empty;

            string name = Name;
            string cid = CID;
            string address = Address;

            if (DynamicValues != null)
            {
                if(String.IsNullOrEmpty(name))
                    name = DynamicValues.ContainsKey(DataType.LOCATION_NAME.ToString()) && DynamicValues[DataType.LOCATION_NAME.ToString()] != null ? DynamicValues[DataType.LOCATION_NAME.ToString()].ToString() : null;
                if(String.IsNullOrEmpty(cid))
                    cid = DynamicValues.ContainsKey(DataType.LOCATION_CID.ToString()) && DynamicValues[DataType.LOCATION_CID.ToString()] != null ? DynamicValues[DataType.LOCATION_CID.ToString()].ToString() : null;
                if(String.IsNullOrEmpty(address))
                    address = DynamicValues.ContainsKey(DataType.LOCATION_ADDRESS.ToString()) && DynamicValues[DataType.LOCATION_ADDRESS.ToString()] != null ? DynamicValues[DataType.LOCATION_ADDRESS.ToString()].ToString() : null;
            }
            retStr = (String.IsNullOrEmpty(name) ? cid : name) + " " + Address;

            return retStr;
        }
    #endregion

    #region ConvertList
        public static List<OpLocation> ConvertList(DB_LOCATION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpLocation> ret = new List<OpLocation>(db_objects.Length);
             db_objects.ToList().ForEach(db_object =>
                {
                    OpLocation location = new OpLocation(db_object);
                    if (location.U_DATA_ARRAY != null && location.U_DATA_ARRAY.Length > 0)
                        location.DataList.AddRange(OpData.ConvertList(location.U_DATA_ARRAY, dataProvider));
                    ret.Add(location);
                }
            );

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes: customDataTypes, transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout); // Loads user custom data

            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpLocation> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpLocationType> LocationTypeDict = dataProvider.GetLocationType(list.Select(l => l.ID_LOCATION_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION_TYPE);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Where(l => l.ID_CONSUMER.HasValue).Select(l => l.ID_CONSUMER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER);
                Dictionary<int, OpLocationStateType> LocationStateTypeDict = dataProvider.GetLocationStateType(list.Select(l => l.ID_LOCATION_STATE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION_STATE_TYPE);

                foreach (var loop in list)
                {
                    loop.LocationType = LocationTypeDict.TryGetValue(loop.ID_LOCATION_TYPE);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    if (loop.ID_CONSUMER.HasValue)
                        loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER.Value);
                    loop.LocationStateType = LocationStateTypeDict.TryGetValue(loop.ID_LOCATION_STATE_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpLocation> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<long> idLocation = list.Select(l => l.ID_LOCATION).ToList();
                List<OpData> data = new List<OpData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetDataFilter(IdLocation: idLocation.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.ILocationDataTypes != null && dataProvider.ILocationDataTypes.Count > 0)
                    data = dataProvider.GetDataFilter(IdLocation: idLocation.ToArray(), IdDataType: dataProvider.ILocationDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data.Count > 0)
                {
                    Dictionary<long, OpLocation> locationsDict = list.ToDictionary<OpLocation, long>(l => l.IdLocation);
                    data.ForEach(w => locationsDict[w.IdLocation.Value].DataList.Add(w));
                }
                //if (data.Count > 0)
                //{
                //    Dictionary<long?, List<OpData>> dataDict = data.GroupBy(x => x.ID_LOCATION).ToDictionary(x => x.Key, x => x.ToList());
                //    list.ForEach(x => { if (dataDict.ContainsKey(x.ID_LOCATION)) { x.DataList.AddRange(dataDict[x.ID_LOCATION]); } });
                //}

                Dictionary<int, OpImrServer> ImrServerDict = new Dictionary<int, OpImrServer>();
                if (list.Any(w => w.IdImrServer.HasValue))
                    ImrServerDict = dataProvider.GetImrServer(list.Where(l => l.IdImrServer.HasValue).Select(l => l.IdImrServer.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_IMR_SERVER);
                Dictionary<int, OpActor> ActorDict = dataProvider.GetActor(list.Where(l => l.IdActor.HasValue).Select(l => l.IdActor.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTOR);

                foreach (var loop in list)
                {
                    if (loop.IdImrServer.HasValue)
                        loop.ImrServer = ImrServerDict.TryGetValue(loop.IdImrServer.Value);
                    else
                    {
                        //loop.ImrServer = dataProvider.GetImrServer(OpImrServer.LocalhostID);
                        loop.ImrServer = new OpImrServer() { IdImrServer = 18, DnsName = "localhost", StandardDescription = "localhost" };
                        loop.DataList.SetStatus(DataType.LOCATION_IMR_SERVER_ID, OpChangeState.Loaded);
                    }

                    if (loop.IdActor.HasValue && loop.IdActor > 0)
                        loop.Actor = ActorDict.TryGetValue(loop.IdActor.Value);
                }
            }
        }
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            LocationType = dataProvider.GetLocationType(this.ID_LOCATION_TYPE);
            Distributor = dataProvider.GetDistributor(this.ID_DISTRIBUTOR);
            if (this.ID_CONSUMER.HasValue)
                Consumer = dataProvider.GetConsumer(this.ID_CONSUMER.Value);
            LocationStateType = dataProvider.GetLocationStateType(this.ID_LOCATION_STATE_TYPE);
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocation)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocation).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpLocation> Members
        public bool Equals(OpLocation other)
        {
            if (other != null)
                return this.IdLocation.Equals(other.IdLocation);
            else
                return false;
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public OpLocation Clone(IDataProvider dataProvider)
        {
            OpLocation clone = new OpLocation();
            clone.IdLocation = this.IdLocation;
            clone.IdLocationType = this.IdLocationType;
            clone.IdLocationPattern = this.IdLocationPattern;
            clone.IdDescrPattern = this.IdDescrPattern;
            clone.IdDistributor = this.IdDistributor;
            clone.IdConsumer = this.IdConsumer;
            clone.IdLocationStateType = this.IdLocationStateType;
            clone.InKpi = this.InKpi;
            clone.AllowGrouping = this.AllowGrouping;

            clone.DataList = this.DataList.Clone(dataProvider);

            if (dataProvider != null)
                clone = ConvertList(new DB_LOCATION[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpLocation)
                return this.IdLocation == ((OpLocation)obj).IdLocation;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpLocation left, OpLocation right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpLocation left, OpLocation right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdLocation.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdLocation;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdLocation; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpLocation IOpDynamicProperty<OpLocation>.Owner
        {
            get { return this; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
    #endregion
    #region Implementation of IOpDataProperty
        public object GetOpDataPropertyValue(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return null;

            OpData opData = null;
            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdMeter:
                    if (this.CurrentMeter != null)
                        return this.CurrentMeter.GetOpDataPropertyValue(opDataProperty);
                    else 
                        return null;
                case Enums.ReferenceType.SerialNbr:
                    if (this.CurrentDevice != null)
                        return this.CurrentDevice.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
            }

            opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opDataProperty.OpDataType.IdReferenceType.HasValue && opData.ReferencedObject != null)
                    return opData.ReferencedObject;

                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && opData.Value != null)
                    return OpUnit.ChangeUnit(opData.Value, opDataProperty.OpDataType, opData.DataType.Unit, opDataProperty.OpUnit, OpDataTypeFormat.GetPrecision(opDataProperty.OpDataTypeFormat)); //(Convert.ToDouble(opData.Value) - opDataProperty.OpUnit.Bias) / opDataProperty.OpUnit.Scale;

                return opData.Value;
            }

            return null;
        }
        public void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return;

            OpData opData = null;
            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdMeter:
                    if (this.CurrentMeter != null)
                        this.CurrentMeter.SetOpDataPropertyValue(opDataProperty, value);
                    return;
                case Enums.ReferenceType.SerialNbr:
                    if (this.CurrentDevice != null)
                        this.CurrentDevice.SetOpDataPropertyValue(opDataProperty, value);
                    return;
            }

            opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && value != null)
                    opData.Value = OpUnit.ChangeUnit(value, opDataProperty.OpDataType, opDataProperty.OpUnit, opData.DataType.Unit); //Convert.ToDouble(value) * opDataProperty.OpUnit.Scale + opDataProperty.OpUnit.Bias;
                else
                    opData.Value = value;
            }
        }
        private OpData GetColumnOpData(OpDataType dataType)
        {
            if (dataType == null) return null;
            switch (dataType.IdDataType)
            {
                case DataType.HELPER_ID_LOCATION:
                    return new OpData() { DataType = dataType, Value = this.IdLocation, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_LOCATION_TYPE:
                    return new OpData() { DataType = dataType, Value = this.LocationType, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_DISTRIBUTOR:
                    return new OpData() { DataType = dataType, Value = this.Distributor, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_DEVICE_TEMPLATE:
                    return new OpData() { DataType = dataType, Value = this.Consumer, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_LOCATION_STATE_TYPE:
                    return new OpData() { DataType = dataType, Value = this.LocationStateType, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_LOCATION_IN_KPI:
                    return new OpData() { DataType = dataType, Value = this.InKpi, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_LOCATION_ALLOW_GROUPING:
                    return new OpData() { DataType = dataType, Value = this.AllowGrouping, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_LOCATION_PARENT_GROUPS:
                    return new OpData() { DataType = dataType, Value = ((ParentGroups == null || ParentGroups.Count == 0) ? null : String.Join(",", this.ParentGroups.Select(p => !String.IsNullOrEmpty(p.Name) ? p.Name : p.CID))) , OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
            }
            return null;
        }
        public Type GetOpDataPropertyType(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null || !opDataProperty.ReturnReferencedTypeIfHelper.HasValue) return null;
            Type type = Utils.GetHelperType(opDataProperty.OpDataType, opDataProperty.ReturnReferencedTypeIfHelper.Value);

            if (type == null)
                type = DataType.GetSystemType(opDataProperty.OpDataType.IdDataTypeClass);

            return type;
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
    public object IdObject
    {
        get { return IdLocation; }
        set { IdLocation = Convert.ToInt64(value); }
    }

    [DataMember]
    public OpDataList<OpData> DataList { get; set; }
    #endregion
    }
#endif
}
