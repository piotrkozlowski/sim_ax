using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpIssueHistory : DB_ISSUE_HISTORY, IComparable
    {
    #region Properties
        public int IdIssueHistory { get { return this.ID_ISSUE_HISTORY; } set { this.ID_ISSUE_HISTORY = value; } }
        public int IdIssue { get { return this.ID_ISSUE; } set { this.ID_ISSUE = value; } }
        public int IdIssueStatus { get { return this.ID_ISSUE_STATUS; } set { this.ID_ISSUE_STATUS = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
        public string ShortDescr { get { return this.SHORT_DESCR; } set { this.SHORT_DESCR = value; } }
        public DateTime? Deadline { get { return this.DEADLINE; } set { this.DEADLINE = value; } }
        public int? IdPriority { get { return this.ID_PRIORITY; } set { this.ID_PRIORITY = value; } }
        public object RowVersion { get { return this.ROW_VERSION; } set { this.ROW_VERSION = value; } }
    #endregion

    #region	Navigation Properties
        private OpIssue _Issue;
        public OpIssue Issue { get { return this._Issue; } set { this._Issue = value; this.ID_ISSUE = (value == null) ? 0 : (int)value.ID_ISSUE; } }
        private OpIssueStatus _IssueStatus;
        public OpIssueStatus IssueStatus { get { return this._IssueStatus; } set { this._IssueStatus = value; this.ID_ISSUE_STATUS = (value == null) ? 0 : (int)value.ID_ISSUE_STATUS; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpPriority _Priority;
        public OpPriority Priority { get { return this._Priority; } set { this._Priority = value; this.ID_PRIORITY = (value == null) ? null : (int?)value.ID_PRIORITY; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpIssueHistory()
            : base() { }

        public OpIssueHistory(DB_ISSUE_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region	ConvertList
        public static List<OpIssueHistory> ConvertList(DB_ISSUE_HISTORY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpIssueHistory> ConvertList(DB_ISSUE_HISTORY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpIssueHistory> ret = new List<OpIssueHistory>(list.Length);
            foreach (var loop in list)
            {
                OpIssueHistory insert = new OpIssueHistory(loop);
                ret.Add(insert);
            }
            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpIssueHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpIssueStatus> IssueStatusDict = dataProvider.GetIssueStatus(list.Select(l => l.ID_ISSUE_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ISSUE_STATUS);
                Dictionary<int, OpPriority> PriorityDict = dataProvider.GetPriority(list.Where(l => l.ID_PRIORITY.HasValue).Select(l => l.ID_PRIORITY.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PRIORITY);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop._IssueStatus = IssueStatusDict.TryGetValue(loop.ID_ISSUE_STATUS);
                    if (loop.ID_PRIORITY.HasValue)
                        loop.Priority = PriorityDict.TryGetValue(loop.ID_PRIORITY.Value);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpIssueHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpIssueHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpIssueHistory).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion
    }
#endif
}