﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpContract : DB_CONTRACT, IComparable, IEquatable<OpContract>
    {
    #region Properties
        public int IdContract { get { return this.ID_CONTRACT; } set { this.ID_CONTRACT = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Descr { get { return this.DESCR; } set { this.DESCR = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private List<OpWarrantyContract> _WarrantyContract;
        public List<OpWarrantyContract> WarrantyContract { get { return this._WarrantyContract; } set { this._WarrantyContract = value; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpContractData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpContract()
            : base() 
        {
            DataList = new OpDataList<OpContractData>();
        }

        public OpContract(DB_CONTRACT clone)
            : base(clone)
        {
            DataList = new OpDataList<OpContractData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME + (this.Distributor != null ? "(" + this.Distributor.ToString() + ")" : "");
        }
    #endregion

    #region	ConvertList
        public static List<OpContract> ConvertList(DB_CONTRACT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpContract> ConvertList(DB_CONTRACT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpContract> ret = new List<OpContract>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpContract(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpContract> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                //Dictionary<int, OpWarrantyContract> WarratnyDict = dataProvider.GetWarrantyContract(list.Select(l => l.ID_CONTRACT).Distinct().ToArray()).ToDictionary(l => l.ID_CONTRACT);
                foreach (var loop in list)
                {
                    loop.WarrantyContract = dataProvider.GetWarrantyContractFilter(IdContract: new int[] { loop.ID_CONTRACT }, loadNavigationProperties: false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);//list.Select(l => l.ID_CONTRACT).Distinct().ToArray());//dataProvider.GetWarrantyContract(//list.Select(l => l.ID_CONTRACT).Distinct().ToArray());
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    //loop.WarrantyContract = WarratnyDict.TryGetValue<(loop.ID_CONTRACT);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpContract> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IContractDataTypes != null)
            {
                List<int> idContract = list.Select(l => l.IdContract).ToList();

                if (dataProvider.IContractDataTypes.Count > 0)
                {
                    List<OpContractData> data1 = dataProvider.GetContractDataFilter(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    OpContractData data2 = dataProvider.GetContractData(new long[]{ 1 }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();
                    List<OpContractData> data = dataProvider.GetContractDataFilter(IdContract: idContract.ToArray(), IdDataType: dataProvider.IContractDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpContract> contractDataDict = list.ToDictionary<OpContract, int>(l => l.IdContract);
                        foreach (var dataValue in data)
                        {
                            contractDataDict[dataValue.IdContract].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpContract)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpContract).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpContract> Members
        public bool Equals(OpContract other)
        {
            if (other == null)
                return false;
            return this.IdContract.Equals(other.IdContract);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpContract)
                return this.IdContract == ((OpContract)obj).IdContract;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpContract left, OpContract right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpContract left, OpContract right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdContract.GetHashCode();
        }
    #endregion
    }
#endif
}