﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataFormatGroup : DB_DATA_FORMAT_GROUP, IComparable, IEquatable<OpDataFormatGroup>, IReferenceType
    {
    #region Properties
        public int IdDataFormatGroup { get { return this.ID_DATA_FORMAT_GROUP; } set { this.ID_DATA_FORMAT_GROUP = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int? IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
    #endregion

    #region	Navigation Properties
        private OpModule _Module;
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null) ? null : (int?)value.ID_MODULE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDataFormatGroup()
            : base() { }

        public OpDataFormatGroup(DB_DATA_FORMAT_GROUP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDataFormatGroup> ConvertList(DB_DATA_FORMAT_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataFormatGroup> ConvertList(DB_DATA_FORMAT_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true)
        {
            List<OpDataFormatGroup> ret = new List<OpDataFormatGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataFormatGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
            if(loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataFormatGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_MODULE);
                foreach (var loop in list)
                {
                    if (loop.ID_MODULE.HasValue)
                        loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataFormatGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataFormatGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataFormatGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataFormatGroup> Members
        public bool Equals(OpDataFormatGroup other)
        {
            if (other == null)
                return false;
            return this.IdDataFormatGroup.Equals(other.IdDataFormatGroup);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataFormatGroup)
                return this.IdDataFormatGroup == ((OpDataFormatGroup)obj).IdDataFormatGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataFormatGroup left, OpDataFormatGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataFormatGroup left, OpDataFormatGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataFormatGroup.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdDataFormatGroup;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}