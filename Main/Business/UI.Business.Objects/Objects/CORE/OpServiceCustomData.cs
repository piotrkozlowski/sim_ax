﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceCustomData : DB_SERVICE_CUSTOM_DATA, IComparable, IEquatable<OpServiceCustomData>
    {
    #region Properties
        public long IdServiceCustomData { get { return this.ID_SERVICE_CUSTOM_DATA; } set { this.ID_SERVICE_CUSTOM_DATA = value; } }
        public long IdServiceCustom { get { return this.ID_SERVICE_CUSTOM; } set { this.ID_SERVICE_CUSTOM = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceCustom _ServiceCustom;
        public OpServiceCustom ServiceCustom { get { return this._ServiceCustom; } set { this._ServiceCustom = value; this.ID_SERVICE_CUSTOM = (value == null) ? 0 : (long)value.ID_SERVICE_CUSTOM; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpServiceCustomData()
            : base() { }

        public OpServiceCustomData(DB_SERVICE_CUSTOM_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceCustomData [" + IdServiceCustomData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceCustomData> ConvertList(DB_SERVICE_CUSTOM_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceCustomData> ConvertList(DB_SERVICE_CUSTOM_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceCustomData> ret = new List<OpServiceCustomData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceCustomData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceCustomData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpServiceCustom> ServiceCustomDict = dataProvider.GetServiceCustom(list.Select(l => l.ID_SERVICE_CUSTOM).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_CUSTOM);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.ServiceCustom = ServiceCustomDict.TryGetValue(loop.ID_SERVICE_CUSTOM);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceCustomData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceCustomData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceCustomData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceCustomData> Members
        public bool Equals(OpServiceCustomData other)
        {
            if (other == null)
                return false;
            return this.IdServiceCustomData.Equals(other.IdServiceCustomData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceCustomData)
                return this.IdServiceCustomData == ((OpServiceCustomData)obj).IdServiceCustomData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceCustomData left, OpServiceCustomData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceCustomData left, OpServiceCustomData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceCustomData.GetHashCode();
        }
    #endregion
    }
#endif
}