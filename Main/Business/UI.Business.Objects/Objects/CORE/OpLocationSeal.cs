using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpLocationSeal : DB_LOCATION_SEAL, IComparable
    {
    #region Properties
            public long IdLocation { get { return this.ID_LOCATION;} set {this.ID_LOCATION = value;}}
                public long IdSeal { get { return this.ID_SEAL;} set {this.ID_SEAL = value;}}
    #endregion

    #region	Navigation Properties
		private OpLocation _Location;
				public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null)? 0 : (long)value.ID_LOCATION; } }
				private OpSeal _Seal;
				public OpSeal Seal { get { return this._Seal; } set { this._Seal = value; this.ID_SEAL = (value == null)? 0 : (long)value.ID_SEAL; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpLocationSeal()
			:base() {}
		
		public OpLocationSeal(DB_LOCATION_SEAL clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "LocationSeal [" + IdLocation.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpLocationSeal> ConvertList(DB_LOCATION_SEAL[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpLocationSeal> ConvertList(DB_LOCATION_SEAL[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpLocationSeal> ret = new List<OpLocationSeal>(list.Length);
			foreach (var loop in list)
			{
			OpLocationSeal insert = new OpLocationSeal(loop);
				
				if(loadNavigationProperties)
				{
											
																									insert.Seal = dataProvider.GetSeal(loop.ID_SEAL); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpLocationSeal> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationSeal)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationSeal).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}