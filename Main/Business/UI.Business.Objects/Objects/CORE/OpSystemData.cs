using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpSystemData : DB_SYSTEM_DATA, IOpData, IOpDataProvider, IComparable
    {
    #region Properties
         public long IdSystemData { get { return this.ID_SYSTEM_DATA; } set { this.ID_SYSTEM_DATA = OpDataUtil.SetNewValue<long, OpSystemData>(this.ID_SYSTEM_DATA, value, this); } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = OpDataUtil.SetNewValue<long, OpSystemData>(this.ID_DATA_TYPE, value, this); } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
    #endregion

    #region Navigation Properties
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.IdDataType = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdSystemData; } set { IdSystemData = value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion
		
    #region Ctor
		public OpSystemData()
			: base() { this.OpState = OpChangeState.New; }
		
		public OpSystemData(DB_SYSTEM_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion
		
    #region ToString
		public override string ToString()
		{
			return "SystemData [" + IdSystemData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpSystemData> ConvertList(DB_SYSTEM_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpSystemData> ConvertList(DB_SYSTEM_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSystemData> ret = new List<OpSystemData>(list.Length);
			foreach (var loop in list)
			{
				OpSystemData insert = new OpSystemData(loop);
				if(loadNavigationProperties)
				{
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE); 
				}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSystemData> list, IDataProvider dataProvider)
        {
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpSystemData clone = new OpSystemData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_SYSTEM_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSystemData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSystemData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
            OpSystemData objToCmp = obj as OpSystemData;
            if (objToCmp != null)
            {
                if (this.IdData == 0 && objToCmp.IdData == 0)
                    return ReferenceEquals(this, objToCmp);
                return this.IdData == objToCmp.IdData;
            }
            else
                return base.Equals(obj);
		}
		public static bool operator ==(OpSystemData left, OpSystemData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpSystemData left, OpSystemData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
    }
#endif
}