﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServicePackageData : DB_SERVICE_PACKAGE_DATA, IComparable, IEquatable<OpServicePackageData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdServicePackageData { get { return this.ID_SERVICE_PACKAGE_DATA; } set { this.ID_SERVICE_PACKAGE_DATA = value; } }
        public long IdServicePackage { get { return this.ID_SERVICE_PACKAGE; } set { this.ID_SERVICE_PACKAGE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public int? Min { get { return this.MIN; } set { this.MIN = value; } }
        public int? Max { get { return this.MAX; } set { this.MAX = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }

        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpServicePackage _ServicePackage;
        public OpServicePackage ServicePackage { get { return this._ServicePackage; } set { this._ServicePackage = value; this.ID_SERVICE_PACKAGE = (value == null) ? 0 : (long)value.ID_SERVICE_PACKAGE; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpServicePackageData()
            : base() { }

        public OpServicePackageData(DB_SERVICE_PACKAGE_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServicePackageData [" + IdServicePackageData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServicePackageData> ConvertList(DB_SERVICE_PACKAGE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServicePackageData> ConvertList(DB_SERVICE_PACKAGE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServicePackageData> ret = new List<OpServicePackageData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServicePackageData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServicePackageData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
             //   Dictionary<long, OpServicePackage> ServicePackageDict = dataProvider.GetServicePackage(list.Select(l => l.ID_SERVICE_PACKAGE).Distinct().ToArray()).ToDictionary(l => l.ID_SERVICE_PACKAGE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                 //   loop.ServicePackage = ServicePackageDict.TryGetValue(loop.ID_SERVICE_PACKAGE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServicePackageData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServicePackageData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServicePackageData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServicePackageData> Members
        public bool Equals(OpServicePackageData other)
        {
            if (other == null)
                return false;
            return this.IdServicePackageData.Equals(other.IdServicePackageData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServicePackageData)
                return this.IdServicePackageData == ((OpServicePackageData)obj).IdServicePackageData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServicePackageData left, OpServicePackageData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServicePackageData left, OpServicePackageData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServicePackageData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdServicePackageData; } set { IdServicePackageData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion
        
    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpServicePackageData clone = new OpServicePackageData();
            clone.IdData = this.IdData;
            clone.IdServicePackage = this.IdServicePackage;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.Min = this.Min;
            clone.Max = this.Max;

            if (dataProvider != null)
                clone = ConvertList(new DB_SERVICE_PACKAGE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}