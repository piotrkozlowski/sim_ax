using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConfigurationProfileTypeStep : DB_CONFIGURATION_PROFILE_TYPE_STEP, IComparable, IEquatable<OpConfigurationProfileTypeStep>
    {
    #region Properties
            public int IdConfigurationProfileType { get { return this.ID_CONFIGURATION_PROFILE_TYPE;} set {this.ID_CONFIGURATION_PROFILE_TYPE = value;}}
                public int IdDeviceTypeProfileStep { get { return this.ID_DEVICE_TYPE_PROFILE_STEP;} set {this.ID_DEVICE_TYPE_PROFILE_STEP = value;}}
    #endregion

    #region	Navigation Properties
		private OpConfigurationProfileType _ConfigurationProfileType;
				public OpConfigurationProfileType ConfigurationProfileType { get { return this._ConfigurationProfileType; } set { this._ConfigurationProfileType = value; this.ID_CONFIGURATION_PROFILE_TYPE = (value == null)? 0 : (int)value.ID_CONFIGURATION_PROFILE_TYPE; } }
				private OpDeviceTypeProfileStep _DeviceTypeProfileStep;
				public OpDeviceTypeProfileStep DeviceTypeProfileStep { get { return this._DeviceTypeProfileStep; } set { this._DeviceTypeProfileStep = value; this.ID_DEVICE_TYPE_PROFILE_STEP = (value == null)? 0 : (int)value.ID_DEVICE_TYPE_PROFILE_STEP; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConfigurationProfileTypeStep()
			:base() {}
		
		public OpConfigurationProfileTypeStep(DB_CONFIGURATION_PROFILE_TYPE_STEP clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ConfigurationProfileTypeStep [" + IdConfigurationProfileType.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpConfigurationProfileTypeStep> ConvertList(DB_CONFIGURATION_PROFILE_TYPE_STEP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConfigurationProfileTypeStep> ConvertList(DB_CONFIGURATION_PROFILE_TYPE_STEP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpConfigurationProfileTypeStep> ret = new List<OpConfigurationProfileTypeStep>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConfigurationProfileTypeStep(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConfigurationProfileTypeStep> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpDeviceTypeProfileStep> DeviceTypeProfileStepDict = dataProvider.GetDeviceTypeProfileStep(list.Select(l => l.ID_DEVICE_TYPE_PROFILE_STEP).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE_PROFILE_STEP);			
				foreach (var loop in list)
				{
						loop.DeviceTypeProfileStep = DeviceTypeProfileStepDict.TryGetValue(loop.ID_DEVICE_TYPE_PROFILE_STEP); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConfigurationProfileTypeStep> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConfigurationProfileTypeStep)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConfigurationProfileTypeStep).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConfigurationProfileTypeStep> Members
        public bool Equals(OpConfigurationProfileTypeStep other)
        {
            if (other == null)
				return false;
			return this.IdConfigurationProfileType.Equals(other.IdConfigurationProfileType);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConfigurationProfileTypeStep)
				return this.IdConfigurationProfileType == ((OpConfigurationProfileTypeStep)obj).IdConfigurationProfileType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConfigurationProfileTypeStep left, OpConfigurationProfileTypeStep right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConfigurationProfileTypeStep left, OpConfigurationProfileTypeStep right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConfigurationProfileType.GetHashCode();
		}
    #endregion
    }
#endif
}