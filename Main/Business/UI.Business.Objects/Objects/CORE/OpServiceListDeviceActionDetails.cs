﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceListDeviceActionDetails : DB_SERVICE_LIST_DEVICE_ACTION_DETAILS, IComparable, IEquatable<OpServiceListDeviceActionDetails>
    {
    #region Properties
        public long IdServiceListDeviceActionDetails { get { return this.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS; } set { this.ID_SERVICE_LIST_DEVICE_ACTION_DETAILS = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public long IdServicePackage { get { return this.ID_SERVICE_PACKAGE; } set { this.ID_SERVICE_PACKAGE = value; } }
        public int IdService { get { return this.ID_SERVICE; } set { this.ID_SERVICE = value; } }
        public string InputText { get { return this.INPUT_TEXT; } set { this.INPUT_TEXT = value; } }
        public int? IdServiceActionResult { get { return this.ID_SERVICE_ACTION_RESULT; } set { this.ID_SERVICE_ACTION_RESULT = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? 0 : (int)value.ID_SERVICE_LIST; } }
        private OpServicePackage _ServicePackage;
        public OpServicePackage ServicePackage { get { return this._ServicePackage; } set { this._ServicePackage = value; this.ID_SERVICE_PACKAGE = (value == null) ? 0 : (long)value.ID_SERVICE_PACKAGE; } }
        private OpService _Service;
        public OpService Service { get { return this._Service; } set { this._Service = value; this.ID_SERVICE = (value == null) ? 0 : (int)value.ID_SERVICE; } }
        private OpServiceActionResult _ServiceActionResult;
        public OpServiceActionResult ServiceActionResult { get { return this._ServiceActionResult; } set { this._ServiceActionResult = value; this.ID_SERVICE_ACTION_RESULT = (value == null) ? null : (int?)value.ID_SERVICE_ACTION_RESULT; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpServiceListDeviceActionDetails()
            : base() { }

        public OpServiceListDeviceActionDetails(DB_SERVICE_LIST_DEVICE_ACTION_DETAILS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.INPUT_TEXT;
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceListDeviceActionDetails> ConvertList(DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceListDeviceActionDetails> ConvertList(DB_SERVICE_LIST_DEVICE_ACTION_DETAILS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceListDeviceActionDetails> ret = new List<OpServiceListDeviceActionDetails>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceListDeviceActionDetails(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceListDeviceActionDetails> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Select(l => l.ID_SERVICE_LIST).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<long, OpServicePackage> ServicePackageDict = dataProvider.GetServicePackage(list.Select(l => l.ID_SERVICE_PACKAGE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_PACKAGE);
                Dictionary<int, OpService> ServiceDict = dataProvider.GetService(list.Select(l => l.ID_SERVICE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE);
                Dictionary<int, OpServiceActionResult> ServiceActionResultDict = dataProvider.GetServiceActionResult(list.Where(l => l.ID_SERVICE_ACTION_RESULT.HasValue).Select(l => l.ID_SERVICE_ACTION_RESULT.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_ACTION_RESULT);
                foreach (var loop in list)
                {
                    loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST);
                    loop.ServicePackage = ServicePackageDict.TryGetValue(loop.ID_SERVICE_PACKAGE);
                    loop.Service = ServiceDict.TryGetValue(loop.ID_SERVICE);
                    if (loop.ID_SERVICE_ACTION_RESULT.HasValue)
                        loop.ServiceActionResult = ServiceActionResultDict.TryGetValue(loop.ID_SERVICE_ACTION_RESULT.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceListDeviceActionDetails> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceListDeviceActionDetails)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceListDeviceActionDetails).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceListDeviceActionDetails> Members
        public bool Equals(OpServiceListDeviceActionDetails other)
        {
            if (other == null)
                return false;
            return this.IdServiceListDeviceActionDetails.Equals(other.IdServiceListDeviceActionDetails);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceListDeviceActionDetails)
                return this.IdServiceListDeviceActionDetails == ((OpServiceListDeviceActionDetails)obj).IdServiceListDeviceActionDetails;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceListDeviceActionDetails left, OpServiceListDeviceActionDetails right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceListDeviceActionDetails left, OpServiceListDeviceActionDetails right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceListDeviceActionDetails.GetHashCode();
        }
    #endregion
    }
#endif
}