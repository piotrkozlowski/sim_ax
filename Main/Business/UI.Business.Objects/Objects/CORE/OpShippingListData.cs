﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpShippingListData : DB_SHIPPING_LIST_DATA, IComparable, IEquatable<OpShippingListData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdShippingListData { get { return this.ID_SHIPPING_LIST_DATA; } set { this.ID_SHIPPING_LIST_DATA = value; } }
        public int IdShippingList { get { return this.ID_SHIPPING_LIST; } set { this.ID_SHIPPING_LIST = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }

        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpShippingList _ShippingList;
        public OpShippingList ShippingList { get { return this._ShippingList; } set { this._ShippingList = value; this.ID_SHIPPING_LIST = (value == null) ? 0 : (int)value.ID_SHIPPING_LIST; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpShippingListData()
            : base() { }

        public OpShippingListData(DB_SHIPPING_LIST_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ShippingListData [" + IdShippingListData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpShippingListData> ConvertList(DB_SHIPPING_LIST_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpShippingListData> ConvertList(DB_SHIPPING_LIST_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpShippingListData> ret = new List<OpShippingListData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpShippingListData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpShippingListData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<int, OpShippingList> ShippingListDict = dataProvider.GetShippingList(list.Select(l => l.ID_SHIPPING_LIST).Distinct().ToArray()).ToDictionary(l => l.ID_SHIPPING_LIST);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                  //  loop.ShippingList = ShippingListDict.TryGetValue(loop.ID_SHIPPING_LIST);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpShippingListData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpShippingListData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpShippingListData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpShippingListData> Members
        public bool Equals(OpShippingListData other)
        {
            if (other == null)
                return false;
            return this.IdShippingListData.Equals(other.IdShippingListData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpShippingListData)
                return this.IdShippingListData == ((OpShippingListData)obj).IdShippingListData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpShippingListData left, OpShippingListData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpShippingListData left, OpShippingListData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdShippingListData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdShippingListData; } set { IdShippingListData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpShippingListData clone = new OpShippingListData();
            clone.IdData = this.IdData;
            clone.IdShippingList = this.IdShippingList;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_SHIPPING_LIST_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}