using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionDef : DB_ACTION_DEF, IComparable, IEquatable<OpActionDef>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public long IdActionDef { get { return this.ID_ACTION_DEF; } set { this.ID_ACTION_DEF = value; } }
        [XmlIgnore]
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        [XmlIgnore]
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        [XmlIgnore]
        public long? IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int IdActionType { get { return this.ID_ACTION_TYPE; } set { this.ID_ACTION_TYPE = value; } }
        [XmlIgnore]
        public long IdActionData { get { return this.ID_ACTION_DATA; } set { this.ID_ACTION_DATA = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? null : (long?)value.ID_DATA_TYPE; } }
        private OpActionType _ActionType;
        [XmlIgnore]
        public OpActionType ActionType { get { return this._ActionType; } set { this._ActionType = value; this.ID_ACTION_TYPE = (value == null) ? 0 : (int)value.ID_ACTION_TYPE; } }
        private OpActionData _ActionData;
        [XmlIgnore]
        public OpActionData ActionData { get { return this._ActionData; } set { this._ActionData = value; this.ID_ACTION_DATA = (value == null) ? 0 : (long)value.ID_ACTION_DATA; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
        public OpDataList<OpActionData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpActionDef()
            : base()
        {
            OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpActionData>();
        }

        public OpActionDef(DB_ACTION_DEF clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpActionData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null && Descr.Description != null)
                return String.Format("{0} [{1}]", Descr.Description, IdActionDef.ToString());
            else if (ActionType != null && ActionType.Name != null)
                return String.Format("{0} [{1}]", ActionType.Name, IdActionDef.ToString());
            else
                return String.Format("[{0}]", IdActionDef.ToString());
        }
    #endregion

    #region	ConvertList
        public static List<OpActionDef> ConvertList(DB_ACTION_DEF[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpActionDef> ConvertList(DB_ACTION_DEF[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool useDBCollector = false)
        {
            List<OpActionDef> ret = new List<OpActionDef>(list.Length);
            foreach (var loop in list)
            {
                OpActionDef insert = new OpActionDef(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, useDBCollector); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionDef> list, IDataProvider dataProvider, bool useDBCollector = false)
        {
            if (list != null && list.Count > 0)
            {
                bool? dbCollectorEnabled = dataProvider.IsDBCollectorEnabled(useDBCollector);
                List<OpActionDef> toRemoveList = new List<OpActionDef>();
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Where(l => l.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Where(l => l.ID_DATA_TYPE.HasValue).Select(l => l.ID_DATA_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpActionType> ActionTypeDict = dataProvider.GetActionType(list.Select(l => l.ID_ACTION_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_TYPE);
                //Dictionary<long, OpActionData> ActionDataDict = dataProvider.GetActionData(list.Select(l => l.ID_ACTION_DATA).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_DATA);
                Dictionary<int, Dictionary<long, OpDescr>> DescrDict = new Dictionary<int, Dictionary<long, OpDescr>>();
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    #region Descr

                    //List<OpDescr> dList = OpDescr.ConvertList(dataProvider.dbCollectorClient.GetDescrFilter(ref dataProvider.dbCollectorSession,
                    //                                                                                              list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(),
                    //                                                                                              new int[]{ (int)dataProvider.UserLanguage }, null, null, null, null, false, System.Data.IsolationLevel.ReadUncommitted, 0), dataProvider, true);
                    List<OpDescr> dList = dataProvider.GetDescrFilter(loadNavigationProperties: true,
                                                                        IdDescr: list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(),
                                                                        IdLanguage: new int[] { (int)dataProvider.UserLanguage },
                                                                        transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                        autoTransaction: false,
                                                                        commandTimeout: 0,
                                                                        useDBCollector: useDBCollector);
                    DescrDict.Add(0, new Dictionary<long, OpDescr>());
                    dList.Where(p => p.ID_SERVER.HasValue).Select(p => p.ID_SERVER.Value).Distinct().ToList().ForEach(p => DescrDict.Add(p, new Dictionary<long, OpDescr>()));
                    foreach (OpDescr dItem in dList)
                    {
                        if (!dItem.ID_SERVER.HasValue)
                            DescrDict[0].Add(dItem.IdDescr, dItem);
                        else
                            DescrDict[dItem.ID_SERVER.Value].Add(dItem.IdDescr, dItem);
                    }

    #endregion
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
    #region Descr
                    DescrDict.Add(0, new Dictionary<long, OpDescr>());
                    DescrDict[0] = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
    #endregion
                }

                foreach (var loop in list)
                {
                    if (loop.ID_METER.HasValue)
                    {
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                        if (loop.Meter == null) toRemoveList.Add(loop);
                    }
                    if (loop.ID_LOCATION.HasValue)
                    {
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                        if (loop.Location == null) toRemoveList.Add(loop);
                    }
                    if (loop.SERIAL_NBR.HasValue)
                    {
                        loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR.Value);
                        if (loop.Device == null) toRemoveList.Add(loop);
                    }
                    if (loop.ID_DATA_TYPE.HasValue)
                        loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE.Value);
                    loop.ActionType = ActionTypeDict.TryGetValue(loop.ID_ACTION_TYPE);
                    //loop.ActionData = ActionDataDict.TryGetValue(loop.ID_ACTION_DATA);
                    if (loop.ID_DESCR.HasValue)
                    {
                        if (loop.ID_SERVER.HasValue && DescrDict.ContainsKey(loop.ID_SERVER.Value))
                            loop.Descr = DescrDict[loop.ID_SERVER.Value].TryGetValue(loop.ID_DESCR.Value);
                        else if(DescrDict[0].ContainsKey(loop.ID_DESCR.Value))
                            loop.Descr = DescrDict[0].TryGetValue(loop.ID_DESCR.Value);
                    }
                }

                foreach (OpActionDef loop in toRemoveList)
                {
                    list.Remove(loop);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionDef> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                List<long> ids = list.Select(l => l.IdActionData).ToList();

                List<OpActionData> data = dataProvider.GetActionDataFilter(IdActionData: ids.ToArray());//, IdDataType: DataProvider.DataTypes.ToArray());
                if (data != null && data.Count > 0)
                {
                    List<OpActionDef> ActionDefDict = list.ToList();
                    foreach (var dataValue in data)
                    {
                        ActionDefDict.Where(a => a.IdActionData == dataValue.IdActionData).ToList().ForEach(ad => ad.DataList.Add(dataValue));
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionDef)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionDef).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionDef> Members
        public bool Equals(OpActionDef other)
        {
            if (other == null)
                return false;
            return this.IdActionDef.Equals(other.IdActionDef);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActionDef)
                return this.IdActionDef == ((OpActionDef)obj).IdActionDef;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActionDef left, OpActionDef right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionDef left, OpActionDef right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActionDef.GetHashCode();
        }
    #endregion

    #region IReferenceType Members
        public object GetReferenceKey()
        {
            return this.IdActionDef;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    }
#endif
}