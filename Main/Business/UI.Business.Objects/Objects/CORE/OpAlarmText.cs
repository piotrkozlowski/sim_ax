using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAlarmText : DB_ALARM_TEXT, IComparable, IEquatable<OpAlarmText>, IReferenceType 
    {
    #region Properties
            public long IdAlarmText { get { return this.ID_ALARM_TEXT;} set {this.ID_ALARM_TEXT = value;}}
                public int IdLanguage { get { return this.ID_LANGUAGE;} set {this.ID_LANGUAGE = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public string AlarmText { get { return this.ALARM_TEXT;} set {this.ALARM_TEXT = value;}}
                public int? IdDistributor { get { return this.ID_DISTRIBUTOR;} set {this.ID_DISTRIBUTOR = value;}}
    #endregion

    #region	Navigation Properties
		private OpLanguage _Language;
				public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null)? 0 : (int)value.ID_LANGUAGE; } }
				private OpDistributor _Distributor;
				public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null)? null : (int?)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpAlarmText()
			:base() {}
		
		public OpAlarmText(DB_ALARM_TEXT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpAlarmText> ConvertList(DB_ALARM_TEXT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpAlarmText> ConvertList(DB_ALARM_TEXT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpAlarmText> ret = new List<OpAlarmText>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmText(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpAlarmText> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray()).ToDictionary(l => l.ID_LANGUAGE);
	Dictionary<int,OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Where(l => l.ID_DISTRIBUTOR.HasValue).Select(l => l.ID_DISTRIBUTOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);			
				foreach (var loop in list)
				{
						loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE); 
		if (loop.ID_DISTRIBUTOR.HasValue)
						loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpAlarmText> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmText)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmText).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpAlarmText> Members
        public bool Equals(OpAlarmText other)
        {
            if (other == null)
				return false;
			return this.IdAlarmText.Equals(other.IdAlarmText) && this.IdLanguage.Equals(other.IdLanguage);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpAlarmText)
                return this.IdAlarmText == ((OpAlarmText)obj).IdAlarmText && this.IdLanguage == ((OpAlarmText)obj).IdLanguage;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpAlarmText left, OpAlarmText right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpAlarmText left, OpAlarmText right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdAlarmText.GetHashCode();
		}
    #endregion

    #region Implementation of IReferenceType
    public object GetReferenceKey()
    {
        return this.IdAlarmText;
    }

    public object GetReferenceValue()
    {
        return this;
    }
    #endregion
    }
#endif
}