﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceListDevice : DB_SERVICE_LIST_DEVICE, IComparable, IEquatable<OpServiceListDevice>
    {
    #region Properties
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public string FailureDescription { get { return this.FAILURE_DESCRIPTION; } set { this.FAILURE_DESCRIPTION = value; } }
        public string FailureSuggestion { get { return this.FAILURE_SUGGESTION; } set { this.FAILURE_SUGGESTION = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
        public long? Photo { get { return this.PHOTO; } set { this.PHOTO = value; } }
        public int? IdOperatorServicer { get { return this.ID_OPERATOR_SERVICER; } set { this.ID_OPERATOR_SERVICER = value; } }
        public string ServiceShortDescr { get { return this.SERVICE_SHORT_DESCR; } set { this.SERVICE_SHORT_DESCR = value; } }
        public string ServiceLongDescr { get { return this.SERVICE_LONG_DESCR; } set { this.SERVICE_LONG_DESCR = value; } }
        public long? PhotoServiced { get { return this.PHOTO_SERVICED; } set { this.PHOTO_SERVICED = value; } }
        public bool? AtexOk { get { return this.ATEX_OK; } set { this.ATEX_OK = value; } }
        public bool? PayForService { get { return this.PAY_FOR_SERVICE; } set { this.PAY_FOR_SERVICE = value; } }
        public int IdServiceStatus { get { return this.ID_SERVICE_STATUS; } set { this.ID_SERVICE_STATUS = value; } }
        public int IdDiagnosticStatus { get { return this.ID_DIAGNOSTIC_STATUS; } set { this.ID_DIAGNOSTIC_STATUS = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? 0 : (int)value.ID_SERVICE_LIST; } }
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? 0 : (int)value.SERIAL_NBR; } }
        private OpOperator _OperatorServicer;
        [CopyableProperty]
        public OpOperator OperatorServicer { get { return this._OperatorServicer; } set { this._OperatorServicer = value; this.IdOperatorServicer = (value == null) ? 0 : (int)value.ID_OPERATOR; } }

        private OpServiceDiagnosticResult _ServiceStatus;
        [CopyableProperty]
        public OpServiceDiagnosticResult ServiceStatus { get { return this._ServiceStatus; } set { this._ServiceStatus = value; this.IdServiceStatus = (value == null) ? 0 : (int)value.IdServiceDiagnosticResult; } }

        private OpServiceDiagnosticResult _DiagnosticStatus;
        [CopyableProperty]
        public OpServiceDiagnosticResult DiagnosticStatus { get { return this._DiagnosticStatus; } set { this._DiagnosticStatus = value; this.IdDiagnosticStatus = (value == null) ? 0 : (int)value.IdServiceDiagnosticResult; } }

    #endregion

    #region	Custom Properties
        public OpDataList<OpServiceListDeviceData> DataList { get; set; }

        private OpDeviceDetails _DeviceDetails;
        public OpDeviceDetails DeviceDetails { get { return this._DeviceDetails; } set { this._DeviceDetails = value; this.SERIAL_NBR = (value == null) ? 0 : (int)value.SERIAL_NBR; } }
    #endregion

    #region	Ctor
        public OpServiceListDevice()
            : base() 
        {
            DataList = new OpDataList<OpServiceListDeviceData>();
        }

        public OpServiceListDevice(DB_SERVICE_LIST_DEVICE clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpServiceListDeviceData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.FAILURE_DESCRIPTION;
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceListDevice> ConvertList(DB_SERVICE_LIST_DEVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceListDevice> ConvertListCustom(DB_SERVICE_LIST_DEVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertListCustom(db_objects, dataProvider, true);
        }

        public static List<OpServiceListDevice> ConvertList(DB_SERVICE_LIST_DEVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceListDevice> ret = new List<OpServiceListDevice>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceListDevice(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }

        public static List<OpServiceListDevice> ConvertListCustom(DB_SERVICE_LIST_DEVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceListDevice> ret = new List<OpServiceListDevice>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceListDevice(db_object)));

            if (loadNavigationProperties)
                LoadNavigationPropertiesCustom(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceListDevice> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpOperator> OperatorServicerDict = dataProvider.GetOperator(list.Where(w => w.ID_OPERATOR_SERVICER != null).Select(l => l.ID_OPERATOR_SERVICER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpServiceDiagnosticResult> ServiceStatusDict = dataProvider.GetServiceDiagnosticResult(list.Select(l => l.ID_SERVICE_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_DIAGNOSTIC_RESULT);
                Dictionary<int, OpServiceDiagnosticResult> ServiceDiagnosticDict = dataProvider.GetServiceDiagnosticResult(list.Select(l => l.ID_DIAGNOSTIC_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_DIAGNOSTIC_RESULT);
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Select(l => l.ID_SERVICE_LIST).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Select(l => l.SERIAL_NBR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.SERIAL_NBR);
                foreach (var loop in list)
                {
                    if(loop.ID_OPERATOR_SERVICER != null)
                        loop.OperatorServicer = OperatorServicerDict.TryGetValue(loop.ID_OPERATOR_SERVICER.Value);
                    loop.ServiceStatus = ServiceStatusDict.TryGetValue(loop.ID_SERVICE_STATUS);
                    loop.DiagnosticStatus = ServiceDiagnosticDict.TryGetValue(loop.ID_DIAGNOSTIC_STATUS);
                    loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST);
                    loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR);
                }
            }
        }

        public static void LoadNavigationPropertiesCustom(ref List<OpServiceListDevice> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpOperator> OperatorServicerDict = dataProvider.GetOperator(list.Where(w => w.ID_OPERATOR_SERVICER != null).Select(l => l.ID_OPERATOR_SERVICER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpServiceDiagnosticResult> ServiceStatusDict = dataProvider.GetServiceDiagnosticResult(list.Select(l => l.ID_SERVICE_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_DIAGNOSTIC_RESULT);
                Dictionary<int, OpServiceDiagnosticResult> ServiceDiagnosticDict = dataProvider.GetServiceDiagnosticResult(list.Select(l => l.ID_DIAGNOSTIC_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_DIAGNOSTIC_RESULT);
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Select(l => l.ID_SERVICE_LIST).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<long, OpDeviceDetails> DeviceDetailsDict = dataProvider.GetDeviceDetailsCustom(list.Select(l => l.SERIAL_NBR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).Where(l => l.SERIAL_NBR.HasValue).ToDictionary(l => l.SERIAL_NBR.Value);
                foreach (var loop in list)
                {
                    if (loop.ID_OPERATOR_SERVICER != null)
                        loop.OperatorServicer = OperatorServicerDict.TryGetValue(loop.ID_OPERATOR_SERVICER.Value);
                    loop.ServiceStatus = ServiceStatusDict.TryGetValue(loop.ID_SERVICE_STATUS);
                    loop.DiagnosticStatus = ServiceDiagnosticDict.TryGetValue(loop.ID_DIAGNOSTIC_STATUS);
                    loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST);
                    if(DeviceDetailsDict.ContainsKey(loop.SERIAL_NBR))
                        loop.DeviceDetails = DeviceDetailsDict.TryGetValue(loop.SERIAL_NBR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceListDevice> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IDeviceInServiceListDataTypes != null)
            {
                List<long> serialNbrList = list.Select(l => l.SerialNbr).ToList();
                List<int> idServiceListList = list.Select(l => l.IdServiceList).Distinct().ToList();

                if (dataProvider.IDeviceInServiceListDataTypes.Count > 0)
                {
                    List<OpServiceListDeviceData> data = dataProvider.GetServiceListDeviceDataFilter(SerialNbr: serialNbrList.ToArray(),
                        IdServiceList: idServiceListList.ToArray(),
                        IdDataType: dataProvider.IDeviceInServiceListDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        foreach (OpServiceListDevice listItem in list)
                        {
                            foreach (OpServiceListDeviceData dataItem in data)
                            {
                                if ((listItem.SerialNbr == dataItem.SerialNbr) && (listItem.IdServiceList == dataItem.IdServiceList))
                                    listItem.DataList.Add(dataItem);
                            }
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceListDevice)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceListDevice).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceListDevice> Members
        public bool Equals(OpServiceListDevice other)
        {
            if (other == null)
                return false;
            return this.IdServiceList.Equals(other.IdServiceList);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceListDevice)
                return this.IdServiceList == ((OpServiceListDevice)obj).IdServiceList;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceListDevice left, OpServiceListDevice right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceListDevice left, OpServiceListDevice right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceList.GetHashCode();
        }
    #endregion
    }
#endif
}