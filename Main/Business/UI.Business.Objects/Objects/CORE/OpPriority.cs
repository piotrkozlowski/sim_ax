using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPriority : DB_PRIORITY, IComparable, IEquatable<OpPriority>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdPriority { get { return this.ID_PRIORITY; } set { this.ID_PRIORITY = value; } }
        [XmlIgnore]
        public long IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public int Value { get { return this.VALUE; } set { this.VALUE = value; } }
        [XmlIgnore]
        public int? RealizationTime { get { return this.REALIZATION_TIME; } set { this.REALIZATION_TIME = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        [XmlIgnore]
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? 0 : (long)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpPriority()
            : base() { }

        public OpPriority(DB_PRIORITY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return String.Format("({0}) {1}", Value, Descr.Description);
            else
                return this.Value.ToString();
        }
    #endregion

    #region	ConvertList
        public static List<OpPriority> ConvertList(DB_PRIORITY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpPriority> ConvertList(DB_PRIORITY[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpPriority> ret = new List<OpPriority>(list.Length);
            foreach (var loop in list)
            {
                OpPriority insert = new OpPriority(loop);

                if (loadNavigationProperties)
                {

                    insert.Descr = dataProvider.GetDescr(new long[]{ loop.ID_DESCR }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();



                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPriority> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPriority)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPriority).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Enum
        [Serializable]
        public enum Enum
        {
            None = 0,
            VeryLow = 1,
            Low2 = 2,
            Low3 = 3,
            Medium4 = 4,
            Medium5 = 5,
            Medium6 = 6,
            High = 7,
            VeryHigh = 8,
            Critical = 9
        }
    #endregion

    #region IEquatable<OpPriority>

        public bool Equals(OpPriority obj)
        {
            if (obj is OpPriority)
                return this.IdPriority == ((OpPriority)obj).IdPriority;
            else
                return base.Equals(obj);
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPriority)
                return this.IdPriority == ((OpPriority)obj).IdPriority;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPriority left, OpPriority right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPriority left, OpPriority right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPriority.GetHashCode();
        }
    #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdPriority;
        }

        public object GetReferenceValue()
        {
            return this;
        } 

        #endregion
    }
#endif
}