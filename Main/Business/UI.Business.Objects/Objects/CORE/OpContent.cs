using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpContent : DB_CONTENT, IComparable, IEquatable<OpContent>
    {
    #region Properties
        public int IdContent { get { return this.ID_CONTENT; } set { this.ID_CONTENT = value; } }
        public int IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        public string Content { get { return this.CONTENT; } set { this.CONTENT = value; } }
        public int IdLanguage { get { return this.ID_LANGUAGE; } set { this.ID_LANGUAGE = value; } }
    #endregion

    #region	Navigation Properties
        private OpModule _Module;
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null) ? 0 : (int)value.ID_MODULE; } }
        private OpLanguage _Language;
        public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null) ? 0 : (int)value.ID_LANGUAGE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpContent()
            : base() { }

        public OpContent(DB_CONTENT clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpContent> ConvertList(DB_CONTENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpContent> ConvertList(DB_CONTENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpContent> ret = new List<OpContent>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpContent(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpContent> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Select(l => l.ID_MODULE).Distinct().ToArray()).ToDictionary(l => l.ID_MODULE);
                Dictionary<int, OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray()).ToDictionary(l => l.ID_LANGUAGE);
                foreach (var loop in list)
                {
                    loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE);
                    loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpContent> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpContent)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpContent).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpContent> Members
        public bool Equals(OpContent other)
        {
            if (other == null)
                return false;
            return this.IdContent.Equals(other.IdContent);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpContent)
                return this.IdContent == ((OpContent)obj).IdContent;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpContent left, OpContent right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpContent left, OpContent right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdContent.GetHashCode();
        }
    #endregion
    }
#endif
}