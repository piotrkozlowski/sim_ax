using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMeterTypeGroup : DB_METER_TYPE_GROUP, IComparable, IEquatable<OpMeterTypeGroup>, IReferenceType
    {
    #region Properties
            public int IdMeterTypeGroup { get { return this.ID_METER_TYPE_GROUP;} set {this.ID_METER_TYPE_GROUP = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpMeterTypeGroup()
			:base() {}
		
		public OpMeterTypeGroup(DB_METER_TYPE_GROUP clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpMeterTypeGroup> ConvertList(DB_METER_TYPE_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpMeterTypeGroup> ConvertList(DB_METER_TYPE_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpMeterTypeGroup> ret = new List<OpMeterTypeGroup>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpMeterTypeGroup(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpMeterTypeGroup> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
					
				foreach (var loop in list)
				{
								}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpMeterTypeGroup> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMeterTypeGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMeterTypeGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpMeterTypeGroup> Members
        public bool Equals(OpMeterTypeGroup other)
        {
            if (other == null)
				return false;
			return this.IdMeterTypeGroup.Equals(other.IdMeterTypeGroup);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpMeterTypeGroup)
				return this.IdMeterTypeGroup == ((OpMeterTypeGroup)obj).IdMeterTypeGroup;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpMeterTypeGroup left, OpMeterTypeGroup right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpMeterTypeGroup left, OpMeterTypeGroup right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdMeterTypeGroup.GetHashCode();
		}
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdMeterTypeGroup;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}