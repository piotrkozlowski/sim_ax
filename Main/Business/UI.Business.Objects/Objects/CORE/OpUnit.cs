using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;

using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpUnit : DB_UNIT, IComparable, IReferenceType
    {
        #region Properties
        [XmlIgnore]
        public int IdUnit { get { return this.ID_UNIT; } set { this.ID_UNIT = value; } }
        [XmlIgnore]
        public int IdUnitBase { get { return this.ID_UNIT_BASE; } set { this.ID_UNIT_BASE = value; } }
        [XmlIgnore]
        public Double Scale { get { return this.SCALE; } set { this.SCALE = value; } }
        [XmlIgnore]
        public Double Bias { get { return this.BIAS; } set { this.BIAS = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        #endregion

        #region Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpUnit _UnitBase;
        [XmlIgnore]
        public OpUnit UnitBase { get { return this._UnitBase; } set { this._UnitBase = value; this.ID_UNIT_BASE = (value == null) ? this.ID_UNIT : (int)value.ID_UNIT_BASE; } }
        #endregion

        #region Custom Properties
        #endregion

        #region Ctor
        public OpUnit()
            : base() { }

        public OpUnit(DB_UNIT clone)
            : base(clone) { }
        #endregion

        #region ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
        #endregion

        #region ConvertList
        public static List<OpUnit> ConvertList(DB_UNIT[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpUnit> ConvertList(DB_UNIT[] list, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpUnit> ret = new List<OpUnit>(list.Length);
            foreach (var loop in list)
            {
                OpUnit insert = new OpUnit(loop);

                if (loadNavigationProperties)
                {
                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[] { loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();
                    if (loop.ID_UNIT_BASE != loop.ID_UNIT)
                        insert.UnitBase = dataProvider.GetUnit(new int[] { loop.ID_UNIT_BASE }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();
                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpUnit> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region Clone
        public object Clone(IDataProvider dataProvider)
        {
            OpUnit clone = new OpUnit();
            clone.IdUnit = this.IdUnit;
            clone.IdUnitBase = this.IdUnitBase;
            clone.Scale = this.Scale;
            clone.Bias = this.Bias;
            clone.Name = this.Name;
            clone.IdDescr = this.IdDescr;

            if (dataProvider != null)
                clone = ConvertList(new DB_UNIT[] { clone }, dataProvider)[0];

            return clone;
        }
        #endregion

        #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpUnit)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpUnit).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpUnit)
                return this.IdUnit == ((OpUnit)obj).IdUnit;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpUnit left, OpUnit right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpUnit left, OpUnit right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdUnit.GetHashCode();
        }
        #endregion

        #region ChangeUnit
        public static object ChangeUnit(object data, OpDataType dataType, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            if (data != null)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    if (dataType != null && dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                    {
                        OpUnit newUnit = dataTypeUnitDict[dataType.IdDataType];
                        if (newUnit != null && dataType.Unit != null && dataType.Unit.IdUnitBase == newUnit.IdUnitBase && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                        {
                            //zmianiamy jednostke jesli jest inna ni� ta, ktora chcemy i typ danych jest liczbowy
                            data = ChangeUnit(data, dataType, dataType.Unit, newUnit,
                                dataTypeFractionalDigitsDict != null && dataTypeFractionalDigitsDict.ContainsKey(dataType.IdDataType) ? dataTypeFractionalDigitsDict[dataType.IdDataType] : null);
                        }
                    }
                }
            }
            return data;
        }
        public static object ChangeUnit(object data, OpDataType dataType, OpUnit fromUnit, OpUnit toUnit, int? fractionalDigits = null)
        {
            if (data == null || dataType == null
                || fromUnit == null || toUnit == null
                || fromUnit.IdUnitBase != toUnit.IdUnitBase
                || dataType.IdReferenceType.HasValue)
                return data;

            if (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer
                || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real
                || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal)
            {
                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                {
                    case Enums.DataTypeClass.Integer:
                        if (fromUnit.IdUnit != toUnit.IdUnit)
                            data = (fromUnit.Scale * Convert.ToDouble(data, System.Globalization.CultureInfo.InvariantCulture) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;
                        if (fractionalDigits.HasValue && fractionalDigits.Value >= 0)
                            data = Math.Round(Convert.ToDouble(data, System.Globalization.CultureInfo.InvariantCulture), fractionalDigits.Value);
                        break;
                    case Enums.DataTypeClass.Real:
                        if (fromUnit.IdUnit != toUnit.IdUnit)
                            data = (fromUnit.Scale * Convert.ToDouble(data, System.Globalization.CultureInfo.InvariantCulture) + fromUnit.Bias - toUnit.Bias) / toUnit.Scale;
                        if (fractionalDigits.HasValue && fractionalDigits.Value >= 0)
                            data = Math.Round(Convert.ToDouble(data, System.Globalization.CultureInfo.InvariantCulture), fractionalDigits.Value);
                        break;
                    case Enums.DataTypeClass.Decimal:
                        if (fromUnit.IdUnit != toUnit.IdUnit)
                            data = ((decimal)fromUnit.Scale * Convert.ToDecimal(data, System.Globalization.CultureInfo.InvariantCulture) + (decimal)fromUnit.Bias - (decimal)toUnit.Bias) / (decimal)toUnit.Scale;
                        if (fractionalDigits.HasValue && fractionalDigits.Value >= 0)
                            data = Math.Round(Convert.ToDecimal(data, System.Globalization.CultureInfo.InvariantCulture), fractionalDigits.Value);
                        break;
                }
            }

            return data;
        }
        #endregion
        #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdUnit;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion
    }
#endif
}