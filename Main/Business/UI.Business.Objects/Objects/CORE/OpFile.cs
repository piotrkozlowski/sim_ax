using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;

using IMR.Suite.Common;
using System.IO;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpFile : DB_FILE, IComparable, IEquatable<OpFile>, IReferenceType
    {
    #region Properties
        public long IdFile { get { return this.ID_FILE; } set { this.ID_FILE = value; } }
        public string PhysicalFileName { get { return this.PHYSICAL_FILE_NAME; } set { this.PHYSICAL_FILE_NAME = value; } }
        public object FileBytes { get { return this.FILE_BYTES; } set { this.FILE_BYTES = value; } }
        public long? Size { get { return this.SIZE; } set { this.SIZE = value; } }
        public DateTime InsertDate { get { return this.INSERT_DATE; } set { this.INSERT_DATE = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        public string Version { get { return this.VERSION; } set { this.VERSION = value; } }
        public DateTime? LastDownloaded { get { return this.LAST_DOWNLOADED; } set { this.LAST_DOWNLOADED = value; } }
        public bool IsBlocked { get { return this.IS_BLOCKED; } set { this.IS_BLOCKED = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public string FriendlyName
        {
            get { return String.Format("{0} ({1} kB)", this.ToString(), Size/1024); }
        }
        public Common.Enums.FileExtension Extension
        {
            get
            {
                Enums.FileExtension retValue = Enums.FileExtension.Unknown;

                string extensionStr = Path.GetExtension(this.PhysicalFileName).Replace(".", "").Trim();

                foreach (Enums.FileExtension feItem in Enum.GetValues(typeof(Enums.FileExtension)))
                {
                    if (String.Equals(feItem.ToString().ToUpper(), extensionStr.ToUpper()))
                    {
                        retValue = feItem;
                        break;
                    }
                }

                return retValue;
            }
        }
    #endregion

    #region	Ctor
        public OpFile()
            : base() { }

        public OpFile(DB_FILE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.PHYSICAL_FILE_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpFile> ConvertList(DB_FILE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpFile> ConvertList(DB_FILE[] list, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpFile> ret = new List<OpFile>(list.Length);
            foreach (var loop in list)
            {
                OpFile insert = new OpFile(loop);

                if (loadNavigationProperties)
                {







                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }

    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpFile> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpFile)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpFile).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpFile> Members
        public bool Equals(OpFile other)
        {
            if (other == null)
                return false;
            return this.IdFile.Equals(other.IdFile);
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdFile;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}