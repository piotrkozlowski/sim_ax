using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpSchedule : DB_SCHEDULE, IComparable, IEquatable<OpSchedule>
    {
    #region Properties
        public int IdSchedule { get { return this.ID_SCHEDULE;} set {this.ID_SCHEDULE = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
        public int IdScheduleType { get { return this.ID_SCHEDULE_TYPE;} set {this.ID_SCHEDULE_TYPE = value;}}
        public int? ScheduleInterval { get { return this.SCHEDULE_INTERVAL;} set {this.SCHEDULE_INTERVAL = value;}}
        public Enums.ScheduleSubdayType SubdayType { get { return (Enums.ScheduleSubdayType)this.SUBDAY_TYPE; } set { this.SUBDAY_TYPE = (int)value; } }
        public int? SubdayInterval { get { return this.SUBDAY_INTERVAL;} set {this.SUBDAY_INTERVAL = value;}}
        public Enums.ScheduleRelativeInterval? RelativeInterval { get { return this.RELATIVE_INTERVAL == null ? null : (Enums.ScheduleRelativeInterval?)this.RELATIVE_INTERVAL;} set {this.RELATIVE_INTERVAL = value == null ? null : (int?)value;}}
        public int? RecurrenceFactor { get { return this.RECURRENCE_FACTOR;} set {this.RECURRENCE_FACTOR = value;}}
        public DateTime StartDate { get { return this.START_DATE.Date; } set { this.START_DATE = value.Date; } }
        public DateTime? EndDate { get { return (this.END_DATE == null) ? (null) : (DateTime?)this.END_DATE.Value.Date; } set { this.END_DATE = (value == null) ? null : (DateTime?)value.Value.Date; } }
        public TimeSpan? StartTime { get { return (this.START_TIME == null) ? (null) : (TimeSpan?)this.START_TIME.Value; } set { this.START_TIME = (value == null) ? null : (TimeSpan?)value.Value; } }
        public TimeSpan? EndTime { get { return (this.END_TIME == null) ? (null) : (TimeSpan?)this.END_TIME.Value; } set { this.END_TIME = (value == null) ? null : (TimeSpan?)value.Value; } }
        public bool TimesInUtc { get { return this.TIMES_IN_UTC;} set { this.TIMES_IN_UTC = value; } }
		public DateTime? LastRunDate { get { return this.LAST_RUN_DATE;} set {this.LAST_RUN_DATE = value; } }
        public bool IsActive { get { return this.IS_ACTIVE;} set {this.IS_ACTIVE = value;}}
    #endregion

    #region	Navigation Properties
		private OpScheduleType _ScheduleType;
		public OpScheduleType ScheduleType { get { return this._ScheduleType; } set { this._ScheduleType = value; this.ID_SCHEDULE_TYPE = (value == null)? 0 : (int)value.ID_SCHEDULE_TYPE; } }
    #endregion

    #region	Custom Properties

        // lpiela - dodatkowa informacja identyfikujaca wpisy z uwzglednieniem serwerow, z ktorych akcje zostaly pobrane. Zwiazane z DBCollector-em
        public Tuple<int?, long> GUID { get { return new Tuple<int?, long>(this.ID_SERVER, this.ID_SCHEDULE); } }

    #endregion
		
    #region	Ctor
		public OpSchedule()
			:base() {}
		
		public OpSchedule(DB_SCHEDULE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpSchedule> ConvertList(DB_SCHEDULE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpSchedule> ConvertList(DB_SCHEDULE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSchedule> ret = new List<OpSchedule>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpSchedule(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpSchedule> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			    Dictionary<int,OpScheduleType> ScheduleTypeDict = dataProvider.GetScheduleType(list.Select(l => l.ID_SCHEDULE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_SCHEDULE_TYPE);
			
				foreach (var loop in list)
				{
					loop.ScheduleType = ScheduleTypeDict.TryGetValue(loop.ID_SCHEDULE_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSchedule> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSchedule)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSchedule).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpSchedule> Members
        public bool Equals(OpSchedule other)
        {
            if (other == null)
				return false;
			return this.IdSchedule.Equals(other.IdSchedule);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpSchedule)
				return this.IdSchedule == ((OpSchedule)obj).IdSchedule;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpSchedule left, OpSchedule right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpSchedule left, OpSchedule right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdSchedule.GetHashCode();
		}
    #endregion
    }
#endif
}