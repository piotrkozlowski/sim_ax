using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
   [Serializable]
    public class OpIssueStatus : DB_ISSUE_STATUS, IComparable, IReferenceType, IEquatable<OpIssueStatus>
    {
    #region Properties
        public int IdIssueStatus { get { return this.ID_ISSUE_STATUS; } set { this.ID_ISSUE_STATUS = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpIssueStatus()
            : base() { }

        public OpIssueStatus(DB_ISSUE_STATUS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.DESCRIPTION;
        }
    #endregion

    #region	ConvertList
        public static List<OpIssueStatus> ConvertList(DB_ISSUE_STATUS[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpIssueStatus> ConvertList(DB_ISSUE_STATUS[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpIssueStatus> ret = new List<OpIssueStatus>(list.Length);
            foreach (var loop in list)
            {
                OpIssueStatus insert = new OpIssueStatus(loop);

                if (loadNavigationProperties)
                {

                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[]{ loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();


                }

                ret.Add(insert);
            } 

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpIssueStatus> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpIssueStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpIssueStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Enum
        [Serializable]
        public enum Enum
        {
            NEW = 1,
            ACCEPTED = 2,
            QUEUED = 3,
            IN_PROGRESS = 4,
            WAITING_FOR_RESPONSE = 5,
            FINISHED = 6,
            SUSPENDED = 7,
            REOPENED = 8,
            CANCELLED = 9,
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdIssueStatus;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region IEquatable<OpIssueStatus>

        public bool Equals(OpIssueStatus obj)
        {
            if (obj is OpIssueStatus)
                return this.IdIssueStatus == ((OpIssueStatus)obj).IdIssueStatus;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpIssueStatus left, OpIssueStatus right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpIssueStatus left, OpIssueStatus right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdIssueStatus.GetHashCode();
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpIssueStatus)
                return this.IdIssueStatus == ((OpIssueStatus)obj).IdIssueStatus;
            else
                return base.Equals(obj);
        }
    #endregion
    }
#endif
}