using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpFaultCodeType : DB_FAULT_CODE_TYPE, IComparable, IEquatable<OpFaultCodeType>
    {
    #region Properties
        public int IdFaultCodeType { get { return this.ID_FAULT_CODE_TYPE;} set {this.ID_FAULT_CODE_TYPE = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpFaultCodeType()
			:base() {}
		
		public OpFaultCodeType(DB_FAULT_CODE_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpFaultCodeType> ConvertList(DB_FAULT_CODE_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpFaultCodeType> ConvertList(DB_FAULT_CODE_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpFaultCodeType> ret = new List<OpFaultCodeType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpFaultCodeType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpFaultCodeType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpFaultCodeType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpFaultCodeType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpFaultCodeType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpFaultCodeType> Members
        public bool Equals(OpFaultCodeType other)
        {
            if (other == null)
				return false;
			return this.IdFaultCodeType.Equals(other.IdFaultCodeType);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpFaultCodeType)
				return this.IdFaultCodeType == ((OpFaultCodeType)obj).IdFaultCodeType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpFaultCodeType left, OpFaultCodeType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpFaultCodeType left, OpFaultCodeType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdFaultCodeType.GetHashCode();
		}
    #endregion
    }
#endif
}