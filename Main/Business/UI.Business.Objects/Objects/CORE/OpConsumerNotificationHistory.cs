using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerNotificationHistory : DB_CONSUMER_NOTIFICATION_HISTORY, IComparable, IEquatable<OpConsumerNotificationHistory>
    {
    #region Properties
        public long IdConsumerNotificationHistory { get { return this.ID_CONSUMER_NOTIFICATION_HISTORY;} set {this.ID_CONSUMER_NOTIFICATION_HISTORY = value;}}
        public int IdConsumer { get { return this.ID_CONSUMER;} set {this.ID_CONSUMER = value;}}
        public int IdNotificationDeliveryType { get { return this.ID_NOTIFICATION_DELIVERY_TYPE;} set {this.ID_NOTIFICATION_DELIVERY_TYPE = value;}}
        public int IdNotificationType { get { return this.ID_NOTIFICATION_TYPE;} set {this.ID_NOTIFICATION_TYPE = value;}}
        public DateTime Time { get { return this.TIME;} set {this.TIME = value;}}
        public string Body { get { return this.BODY;} set {this.BODY = value;}}
        public string DeliveryAddress { get { return this.DELIVERY_ADDRESS;} set {this.DELIVERY_ADDRESS = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumer _Consumer;
				public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null)? 0 : (int)value.ID_CONSUMER; } }
				private OpNotificationDeliveryType _NotificationDeliveryType;
				public OpNotificationDeliveryType NotificationDeliveryType { get { return this._NotificationDeliveryType; } set { this._NotificationDeliveryType = value; this.ID_NOTIFICATION_DELIVERY_TYPE = (value == null)? 0 : (int)value.ID_NOTIFICATION_DELIVERY_TYPE; } }
				private OpNotificationType _NotificationType;
				public OpNotificationType NotificationType { get { return this._NotificationType; } set { this._NotificationType = value; this.ID_NOTIFICATION_TYPE = (value == null)? 0 : (int)value.ID_NOTIFICATION_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerNotificationHistory()
			:base() {}
		
		public OpConsumerNotificationHistory(DB_CONSUMER_NOTIFICATION_HISTORY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.BODY;
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerNotificationHistory> ConvertList(DB_CONSUMER_NOTIFICATION_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerNotificationHistory> ConvertList(DB_CONSUMER_NOTIFICATION_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpConsumerNotificationHistory> ret = new List<OpConsumerNotificationHistory>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerNotificationHistory(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerNotificationHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Select(l => l.ID_CONSUMER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER);
	Dictionary<int,OpNotificationDeliveryType> NotificationDeliveryTypeDict = dataProvider.GetNotificationDeliveryType(list.Select(l => l.ID_NOTIFICATION_DELIVERY_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_NOTIFICATION_DELIVERY_TYPE);
	Dictionary<int,OpNotificationType> NotificationTypeDict = dataProvider.GetNotificationType(list.Select(l => l.ID_NOTIFICATION_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_NOTIFICATION_TYPE);
			
				foreach (var loop in list)
				{
						loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER); 
		loop.NotificationDeliveryType = NotificationDeliveryTypeDict.TryGetValue(loop.ID_NOTIFICATION_DELIVERY_TYPE); 
		loop.NotificationType = NotificationTypeDict.TryGetValue(loop.ID_NOTIFICATION_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerNotificationHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerNotificationHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerNotificationHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerNotificationHistory> Members
        public bool Equals(OpConsumerNotificationHistory other)
        {
            if (other == null)
				return false;
			return this.IdConsumerNotificationHistory.Equals(other.IdConsumerNotificationHistory);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerNotificationHistory)
				return this.IdConsumerNotificationHistory == ((OpConsumerNotificationHistory)obj).IdConsumerNotificationHistory;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerNotificationHistory left, OpConsumerNotificationHistory right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerNotificationHistory left, OpConsumerNotificationHistory right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerNotificationHistory.GetHashCode();
		}
    #endregion
    }
#endif
}