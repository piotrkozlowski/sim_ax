﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataTransfer : DB_DATA_TRANSFER, IComparable, IEquatable<OpDataTransfer>
    {
    #region Properties
        public long IdDataTransfer { get { return this.ID_DATA_TRANSFER; } set { this.ID_DATA_TRANSFER = value; } }
        public long BatchId { get { return this.BATCH_ID; } set { this.BATCH_ID = value; } }
        public int ChangeType { get { return this.CHANGE_TYPE; } set { this.CHANGE_TYPE = value; } }
        public int ChangeDirection { get { return this.CHANGE_DIRECTION; } set { this.CHANGE_DIRECTION = value; } }
        public int? IdSourceServer { get { return this.ID_SOURCE_SERVER; } set { this.ID_SOURCE_SERVER = value; } }
        public int? IdDestinationServer { get { return this.ID_DESTINATION_SERVER; } set { this.ID_DESTINATION_SERVER = value; } }
        public string TableName { get { return this.TABLE_NAME; } set { this.TABLE_NAME = value; } }
        public long? Key1 { get { return this.KEY1; } set { this.KEY1 = value; } }
        public long? Key2 { get { return this.KEY2; } set { this.KEY2 = value; } }
        public long? Key3 { get { return this.KEY3; } set { this.KEY3 = value; } }
        public long? Key4 { get { return this.KEY4; } set { this.KEY4 = value; } }
        public long? Key5 { get { return this.KEY5; } set { this.KEY5 = value; } }
        public long? Key6 { get { return this.KEY6; } set { this.KEY6 = value; } }
        public string ColumnName { get { return this.COLUMN_NAME; } set { this.COLUMN_NAME = value; } }
        public object OldValue { get { return this.OLD_VALUE; } set { this.OLD_VALUE = value; } }
        public object NewValue { get { return this.NEW_VALUE; } set { this.NEW_VALUE = value; } }
        public DateTime InsertTime { get { return this.INSERT_TIME; } set { this.INSERT_TIME = value; } }
        public DateTime? ProceedTime { get { return this.PROCEED_TIME; } set { this.PROCEED_TIME = value; } }
        public string User { get { return this.USER; } set { this.USER = value; } }
        public string Host { get { return this.HOST; } set { this.HOST = value; } }
        public string Application { get { return this.APPLICATION; } set { this.APPLICATION = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDataTransfer()
            : base() { }

        public OpDataTransfer(DB_DATA_TRANSFER clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.TABLE_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDataTransfer> ConvertList(DB_DATA_TRANSFER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataTransfer> ConvertList(DB_DATA_TRANSFER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDataTransfer> ret = new List<OpDataTransfer>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataTransfer(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataTransfer> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataTransfer> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataTransfer)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataTransfer).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataTransfer> Members
        public bool Equals(OpDataTransfer other)
        {
            if (other == null)
                return false;
            return this.IdDataTransfer.Equals(other.IdDataTransfer);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataTransfer)
                return this.IdDataTransfer == ((OpDataTransfer)obj).IdDataTransfer;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataTransfer left, OpDataTransfer right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataTransfer left, OpDataTransfer right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataTransfer.GetHashCode();
        }
    #endregion
    }
#endif
}