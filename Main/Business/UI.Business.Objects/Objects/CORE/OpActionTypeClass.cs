using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpActionTypeClass : DB_ACTION_TYPE_CLASS, IComparable, IEquatable<OpActionTypeClass>
    {
    #region Properties
        [XmlIgnore]
        public int IdActionTypeClass { get { return this.ID_ACTION_TYPE_CLASS; } set { this.ID_ACTION_TYPE_CLASS = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpActionTypeClass()
			:base() {}
		
		public OpActionTypeClass(DB_ACTION_TYPE_CLASS clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
        public static List<OpActionTypeClass> ConvertList(DB_ACTION_TYPE_CLASS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActionTypeClass> ConvertList(DB_ACTION_TYPE_CLASS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpActionTypeClass> ret = new List<OpActionTypeClass>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActionTypeClass(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion
		
    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionTypeClass> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                    {
                        //loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                        loop.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);
                    }
                }
            }
        }
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionTypeClass> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionTypeClass)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionTypeClass).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpActionTypeClass> Members
        public bool Equals(OpActionTypeClass other)
        {
            if (other == null)
				return false;
			return this.IdActionTypeClass.Equals(other.IdActionTypeClass);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpActionTypeClass)
				return this.IdActionTypeClass == ((OpActionTypeClass)obj).IdActionTypeClass;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpActionTypeClass left, OpActionTypeClass right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpActionTypeClass left, OpActionTypeClass right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdActionTypeClass.GetHashCode();
		}
    #endregion
    }
#endif
}