using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpVersion : DB_VERSION, IComparable, IEquatable<OpVersion>, IOpChangeState, IOpDynamic
    {
        #region Properties
        public long IdVersion { get { return this.ID_VERSION; } set { if (this.ID_VERSION != value) { this.ID_VERSION = OpDataUtil.SetNewValue(this.ID_VERSION, value, this); } } }
        public int IdVersionState { get { return this.ID_VERSION_STATE; } set { if (this.ID_VERSION_STATE != value) { this.ID_VERSION_STATE = OpDataUtil.SetNewValue(this.ID_VERSION_STATE, value, this); } } }
        public int? IdVersionElementType { get { return this.ID_VERSION_ELEMENT_TYPE; } set { if (this.ID_VERSION_ELEMENT_TYPE != value) { this.ID_VERSION_ELEMENT_TYPE = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_TYPE, value, this); } } }
        public int IdVersionType { get { return this.ID_VERSION_TYPE; } set { if (this.ID_VERSION_TYPE != value) { this.ID_VERSION_TYPE = OpDataUtil.SetNewValue(this.ID_VERSION_TYPE, value, this); } } }
        public string VersionNbr { get { return this.VERSION_NBR; } set { if (this.VERSION_NBR != value) { this.VERSION_NBR = OpDataUtil.SetNewValue(this.VERSION_NBR, value, this); } } }
        public long? VersionRaw { get { return this.VERSION_RAW; } set { if (this.VERSION_RAW != value) { this.VERSION_RAW = OpDataUtil.SetNewValue(this.VERSION_RAW, value, this); } } }
        public DateTime? ReleaseDate { get { return this.RELEASE_DATE; } set { if (this.RELEASE_DATE != value) { this.RELEASE_DATE = OpDataUtil.SetNewValue(this.RELEASE_DATE, value, this); } } }
        public int? IdOperatorPublisher { get { return this.ID_OPERATOR_PUBLISHER; } set { if (this.ID_OPERATOR_PUBLISHER != value) { this.ID_OPERATOR_PUBLISHER = OpDataUtil.SetNewValue(this.ID_OPERATOR_PUBLISHER, value, this); } } }
        public DateTime? NotificationDate { get { return this.NOTIFICATION_DATE; } set { if (this.NOTIFICATION_DATE != value) { this.NOTIFICATION_DATE = OpDataUtil.SetNewValue(this.NOTIFICATION_DATE, value, this); } } }
        public int? IdOperatorNotifier { get { return this.ID_OPERATOR_NOTIFIER; } set { if (this.ID_OPERATOR_NOTIFIER != value) { this.ID_OPERATOR_NOTIFIER = OpDataUtil.SetNewValue(this.ID_OPERATOR_NOTIFIER, value, this); } } }
        public DateTime? ConfirmDate { get { return this.CONFIRM_DATE; } set { if (this.CONFIRM_DATE != value) { this.CONFIRM_DATE = OpDataUtil.SetNewValue(this.CONFIRM_DATE, value, this); } } }
        public int? IdOperatorCofirming { get { return this.ID_OPERATOR_COFIRMING; } set { if (this.ID_OPERATOR_COFIRMING != value) { this.ID_OPERATOR_COFIRMING = OpDataUtil.SetNewValue(this.ID_OPERATOR_COFIRMING, value, this); } } }
        public string ReleaseNotes { get { return this.RELEASE_NOTES; } set { if (this.RELEASE_NOTES != value) { this.RELEASE_NOTES = OpDataUtil.SetNewValue(this.RELEASE_NOTES, value, this); } } }
        public int IdPriority { get { return this.ID_PRIORITY; } set { if (this.ID_PRIORITY != value) { this.ID_PRIORITY = OpDataUtil.SetNewValue(this.ID_PRIORITY, value, this); } } }
        public DateTime? Deadline { get { return this.DEADLINE; } set { if (this.DEADLINE != value) { this.DEADLINE = OpDataUtil.SetNewValue(this.DEADLINE, value, this); } } }
        public DateTime StartTime { get { return this.START_TIME; } set { if (this.START_TIME != value) { this.START_TIME = OpDataUtil.SetNewValue(this.START_TIME, value, this); } } }
        public DateTime? EndTime { get { return this.END_TIME; } set { if (this.END_TIME != value) { this.END_TIME = OpDataUtil.SetNewValue(this.END_TIME, value, this); } } }
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        private OpVersionState _VersionState;
        public OpVersionState VersionState { get { return this._VersionState; } set { this._VersionState = value; this.ID_VERSION_STATE = (value == null) ? 0 : (int)value.ID_VERSION_STATE; } }
        private OpVersionElementType _VersionElementType;
        public OpVersionElementType VersionElementType { get { return this._VersionElementType; } set { this._VersionElementType = value; this.ID_VERSION_ELEMENT_TYPE = (value == null) ? null : (int?)value.ID_VERSION_ELEMENT_TYPE; } }
        private OpVersionType _VersionType;
        public OpVersionType VersionType { get { return this._VersionType; } set { this._VersionType = value; this.ID_VERSION_TYPE = (value == null) ? 0 : (int)value.ID_VERSION_TYPE; } }
        private OpPriority _Priority;
        public OpPriority Priority { get { return this._Priority; } set { this._Priority = value; this.ID_PRIORITY = (value == null) ? 0 : (int)value.ID_PRIORITY; } }
        private OpOperator _OperatorNotifier;
        public OpOperator OperatorNotifier { get { return this._OperatorNotifier; } set { this._OperatorNotifier = value; this.ID_OPERATOR_NOTIFIER = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        private OpOperator _OperatorPublisher;
        public OpOperator OperatorPublisher { get { return this._OperatorPublisher; } set { this._OperatorPublisher = value; this.ID_OPERATOR_PUBLISHER = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        #endregion

        #region	Custom Properties
        #region ResultsAndConclusionsNote

        public string ResultsAndConclusionsNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_RESULTS_AND_CONCLUSIONS_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_RESULTS_AND_CONCLUSIONS_NOTE, value);
            }
        }

        #endregion
        #region ChangeScheduleNote

        public string ChangeScheduleNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_CHANGE_SCHEDULE_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_CHANGE_SCHEDULE_NOTE, value);
            }
        }

        #endregion
        #region ThreatAnalysisNote

        public string ThreatAnalysisNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_THREAT_ANALYSIS_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_THREAT_ANALYSIS_NOTE, value);
            }
        }

        #endregion
        #region InstallationPlanNote

        public string InstallationPlanNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_INSTALLATION_PLAN_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_INSTALLATION_PLAN_NOTE, value);
            }
        }

        #endregion
        #region RollbackPlanNote

        public string RollbackPlanNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_ROLLBACK_PLAN_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_ROLLBACK_PLAN_NOTE, value);
            }
        }

        #endregion
        #region TestsScenarioNote

        public string TestsScenarioNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_TESTS_SCENARIO_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_TESTS_SCENARIO_NOTE, value);
            }
        }

        #endregion
        #region InvolvedOperators

        public List<OpOperator> InvolvedOperators
        {
            get;
            set;
        }

        #endregion
        #region EstimatedSystemDowntimeStartTime

        public DateTime? EstimatedSystemDowntimeStartTime
        {
            get
            {
                return DataList.TryGetValue<DateTime>(DataType.VERSION_ESTIMATED_SYSTEM_DOWNTIME_START_TIME);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_ESTIMATED_SYSTEM_DOWNTIME_START_TIME, value);
            }
        }

        #endregion
        #region EstimatedSystemDowntimeEndTime

        public DateTime? EstimatedSystemDowntimeEndTime
        {
            get
            {
                return DataList.TryGetValue<DateTime>(DataType.VERSION_ESTIMATED_SYSTEM_DOWNTIME_END_TIME);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_ESTIMATED_SYSTEM_DOWNTIME_END_TIME, value);
            }
        }

        #endregion
        #region EstimatedSystemDowntimeCompletionDate

        public DateTime? EstimatedSystemDowntimeCompletionDate
        {
            get
            {
                return DataList.TryGetValue<DateTime>(DataType.VERSION_ESTIMATED_SYSTEM_DOWNTIME_COMPLETION_DATE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_ESTIMATED_SYSTEM_DOWNTIME_COMPLETION_DATE, value);
            }
        }

        #endregion
        #region NotificationNote

        public string NotificationNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_NOTIFICATION_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_NOTIFICATION_NOTE, value);
            }
        }

        #endregion
        #region VersionRollbackReasonNote

        public string VersionRollbackReasonNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_ROLLBACK_REASON_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_ROLLBACK_REASON_NOTE, value);
            }
        }

        #endregion
        #region VersionValidationSteps

        public string VersionValidationSteps
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_VALIDATION_STEPS);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_VALIDATION_STEPS, value);
            }
        }

        #endregion
        #region InstallationDate

        public DateTime? InstallationDate
        {
            get
            {
                return DataList.TryGetValue<DateTime>(DataType.VERSION_INSTALLATION_DATE);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_INSTALLATION_DATE, value);
            }
        }

        #endregion
        #region EarlyPostDeploymentNotices

        public string EarlyPostDeploymentNotices
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VERSION_EARLY_POST_DEPLOYMENT_NOTICES);
            }
            set
            {
                DataList.SetValue(DataType.VERSION_EARLY_POST_DEPLOYMENT_NOTICES, value);
            }
        }

        #endregion

        public OpDataList<OpVersionData> DataList { get; set; }
        public List<OpVersionHistory> VersionHistory { get; set; }


        private Dictionary<string, object> dynamicValues;
        #endregion

        #region	Ctor
        public OpVersion()
            : base()
        {
            this.DataList = new OpDataList<OpVersionData>();
            this.DynamicValues = new Dictionary<string, object>();
        }

        public OpVersion(DB_VERSION clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpVersionData>();
            this.DynamicValues = new Dictionary<string, object>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(this.VERSION_NBR) ? string.Format("[{0}]", IdVersion) : this.VERSION_NBR;
        }
        #endregion

        #region	ConvertList
        public static List<OpVersion> ConvertList(DB_VERSION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpVersion> ConvertList(DB_VERSION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpVersion> ret = new List<OpVersion>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpVersion(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpVersion> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpVersionState> VersionStateDict = dataProvider.GetVersionState(list.Select(l => l.ID_VERSION_STATE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_STATE);
                Dictionary<int, OpVersionElementType> VersionElementTypeDict = dataProvider.GetVersionElementType(list.Where(l => l.ID_VERSION_ELEMENT_TYPE.HasValue).Select(l => l.ID_VERSION_ELEMENT_TYPE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_ELEMENT_TYPE);
                Dictionary<int, OpVersionType> VersionTypeDict = dataProvider.GetVersionType(list.Select(l => l.ID_VERSION_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_TYPE);
                Dictionary<int, OpPriority> PriorityDict = dataProvider.GetPriority(list.Select(v => v.ID_PRIORITY).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PRIORITY);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(v => v.ID_OPERATOR_NOTIFIER.HasValue).Select(v => v.ID_OPERATOR_NOTIFIER.Value).Union(list.Where(v => v.ID_OPERATOR_PUBLISHER.HasValue).Select(v => v.ID_OPERATOR_PUBLISHER.Value)).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);


                foreach (var loop in list)
                {
                    loop.VersionState = VersionStateDict.TryGetValue(loop.ID_VERSION_STATE);
                    if (loop.ID_VERSION_ELEMENT_TYPE.HasValue)
                        loop.VersionElementType = VersionElementTypeDict.TryGetValue(loop.ID_VERSION_ELEMENT_TYPE.Value);
                    loop.VersionType = VersionTypeDict.TryGetValue(loop.ID_VERSION_TYPE);
                    loop.Priority = PriorityDict[loop.ID_PRIORITY];
                    if (loop.ID_OPERATOR_NOTIFIER.HasValue)
                        loop.OperatorNotifier = OperatorDict.TryGetValue(loop.ID_OPERATOR_NOTIFIER.Value);
                    if (loop.ID_OPERATOR_PUBLISHER.HasValue)
                        loop.OperatorPublisher = OperatorDict.TryGetValue(loop.ID_OPERATOR_PUBLISHER.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpVersion> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            //if (list != null && list.Count > 0 && DataProvider.VersionDataTypes != null)
            //{
            //    List<long> idVersion = list.Select(l => l.IdVersion).ToList();

            //    List<OpVersionData> data = dataProvider.GetVersionDataFilter(IdVersion: idVersion.ToArray(), IdDataType: DataProvider.TaskDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
            //    if (data != null && data.Count > 0)
            //    {
            //        Dictionary<long, OpVersion> versionsDict = list.ToDictionary<OpVersion, long>(l => l.IdVersion);
            //        foreach (var dataValue in data)
            //        {
            //            versionsDict[dataValue.IdVersion].DataList.Add(dataValue);
            //        }
            //    }
            //}
        }
        #endregion

        #region LoadVersionHistory

        public static void LoadVersionHistory(OpVersion version, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpVersion> verList = new List<OpVersion>() { version };
            LoadVersionHistory(ref verList, dataProvider, autoTransaction, transactionLevel, commandTimeout);
        }

        public static void LoadVersionHistory(ref List<OpVersion> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                long[] idVersionList = list.Select(l => l.IdVersion).ToArray();

                List<OpVersionHistory> history = dataProvider.GetVersionHistoryFilter(IdVersion: idVersionList, loadNavigationProperties: false, autoTransaction: false, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (history != null && history.Count > 0)
                {
                    list.ForEach(l => l.VersionHistory = new List<OpVersionHistory>());
                    Dictionary<long, OpVersion> versionsDict = list.ToDictionary(k => k.IdVersion);

                    foreach (var historyValue in history)
                    {
                        versionsDict[historyValue.IdVersion].VersionHistory.Add(historyValue);
                    }

                    foreach (OpVersion tItem in list)
                    {
                        if (tItem.VersionHistory != null)
                            tItem.VersionHistory = tItem.VersionHistory.OrderBy(t => t.StartTime).ToList();
                    }
                }
            }
        }

        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersion)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersion).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpVersion> Members
        public bool Equals(OpVersion other)
        {
            if (other == null)
                return false;
            return this.IdVersion.Equals(other.IdVersion);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpVersion)
                return this.IdVersion == ((OpVersion)obj).IdVersion;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpVersion left, OpVersion right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpVersion left, OpVersion right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdVersion.GetHashCode();
        }
        #endregion

        #region IOpDynamic Members

        [System.Xml.Serialization.XmlIgnore]
        public object Key
        {
            get { return IdVersion; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public object Owner
        {
            get { return this; }
        }

        #endregion

        
    }
#endif
}