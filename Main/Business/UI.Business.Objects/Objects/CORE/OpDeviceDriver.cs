using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceDriver : DB_DEVICE_DRIVER, IComparable, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdDeviceDriver { get { return this.ID_DEVICE_DRIVER; } set { this.ID_DEVICE_DRIVER = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public string PluginName { get { return this.PLUGIN_NAME; } set { this.PLUGIN_NAME = value; } }
        public bool AutoUpdate { get { return this.AUTO_UPDATE; } set { this.AUTO_UPDATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpDeviceDriverData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpDeviceDriver()
            : base()
        {
            DataList = new OpDataList<OpDeviceDriverData>();
        }

        public OpDeviceDriver(DB_DEVICE_DRIVER clone)
            : base(clone)
        {
            DataList = new OpDataList<OpDeviceDriverData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceDriver> ConvertList(DB_DEVICE_DRIVER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceDriver> ConvertList(DB_DEVICE_DRIVER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDeviceDriver> ret = new List<OpDeviceDriver>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceDriver(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceDriver> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);

                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceDriver> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> ids = list.Select(l => l.IdDeviceDriver).ToList();

                List<OpDeviceDriverData> data = dataProvider.GetDeviceDriverDataFilter(IdDeviceDriver: ids.ToArray(), IdDataType: dataProvider.IDeviceDriverDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpDeviceDriver> TDriversDict = list.ToDictionary<OpDeviceDriver, int>(l => l.IdDeviceDriver);
                    foreach (var dataValue in data)
                    {
                        TDriversDict[dataValue.IdDeviceDriver].DataList.Add(dataValue);
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceDriver)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceDriver).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdDeviceDriver;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}