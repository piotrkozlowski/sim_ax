using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpMeterTypeDeviceType : DB_METER_TYPE_DEVICE_TYPE, IOpChangeState, IComparable
	{
    #region Properties
        [XmlIgnore]
		public int IdMeterType
		{
			get { return this.ID_METER_TYPE; }
			set { if (this.ID_METER_TYPE != value) { this.ID_METER_TYPE = value; this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
		public int IdDeviceType
		{
			get { return this.ID_DEVICE_TYPE; }
			set { if (this.ID_DEVICE_TYPE != value) { this.ID_DEVICE_TYPE = value; this.OpState = OpChangeState.Modified; } }
		}
    #endregion

    #region Navigation Properties
		private OpMeterType _MeterType;
        [XmlIgnore]
		public OpMeterType MeterType { get { return this._MeterType; } set { this._MeterType = value; this.IdMeterType = (value == null) ? 0 : (int)value.ID_METER_TYPE; } }
		private OpDeviceType _DeviceType;
        [XmlIgnore]
		public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.IdDeviceType = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
		public OpChangeState OpState { get; set; }
    #endregion

    #region Ctor
		public OpMeterTypeDeviceType()
			: base() { this.OpState = OpChangeState.New; }

		public OpMeterTypeDeviceType(DB_METER_TYPE_DEVICE_TYPE clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return string.Format("MeterTypeDeviceType [{0},{1}]", IdMeterType, IdDeviceType);
		}
    #endregion

    #region ConvertList
		public static List<OpMeterTypeDeviceType> ConvertList(DB_METER_TYPE_DEVICE_TYPE[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpMeterTypeDeviceType> ConvertList(DB_METER_TYPE_DEVICE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpMeterTypeDeviceType> ret = new List<OpMeterTypeDeviceType>(list.Length);
			list.ToList().ForEach(db_object => ret.Add(new OpMeterTypeDeviceType(db_object)));

			if (loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpMeterTypeDeviceType> list, IDataProvider dataProvider)
		{
			if (list != null && list.Count > 0)
			{
				Dictionary<int, OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Select(l => l.ID_DEVICE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE);
				Dictionary<int, OpMeterType> MeterTypeDict = dataProvider.GetMeterType(list.Select(l => l.ID_METER_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_METER_TYPE);
				foreach (var loop in list)
				{
					loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE);
					loop.MeterType = MeterTypeDict.TryGetValue(loop.ID_METER_TYPE);
				}
			}
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpMeterTypeDeviceType> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpMeterTypeDeviceType)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpMeterTypeDeviceType).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpMeterTypeDeviceType)
				return this.IdMeterType == ((OpMeterTypeDeviceType)obj).IdMeterType && this.IdDeviceType == ((OpMeterTypeDeviceType)obj).IdDeviceType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpMeterTypeDeviceType left, OpMeterTypeDeviceType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpMeterTypeDeviceType left, OpMeterTypeDeviceType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdMeterType.GetHashCode() ^ IdDeviceType.GetHashCode();
		}
    #endregion
	}
#endif
}