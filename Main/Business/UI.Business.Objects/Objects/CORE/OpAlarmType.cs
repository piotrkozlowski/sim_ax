using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
	public class OpAlarmType : DB_ALARM_TYPE, IComparable, IEquatable<OpAlarmType>, IReferenceType
    {
        #region Properties
        public int IdAlarmType { get { return this.ID_ALARM_TYPE;} set {this.ID_ALARM_TYPE = value;}}
        public string Symbol { get { return this.SYMBOL;} set {this.SYMBOL = value;}}
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
        #endregion

        #region	Navigation Properties
        [DataMember]
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties
        #endregion
		
        #region	Ctor
		public OpAlarmType()
			:base() {}
		
		public OpAlarmType(DB_ALARM_TYPE clone)
			:base(clone) {}
        #endregion
		
        #region	ToString
		public override string ToString()
		{
            if (this.Descr != null)
                return String.Format("{0} [{1}]", this.Descr.ToString(), this.Symbol);
			return this.SYMBOL;
		}
        #endregion

        #region	ConvertList
		public static List<OpAlarmType> ConvertList(DB_ALARM_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpAlarmType> ConvertList(DB_ALARM_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpAlarmType> ret = new List<OpAlarmType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
        #endregion
		
        #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpAlarmType> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);		
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
        #endregion
		
        #region LoadCustomData
		private static void LoadCustomData(ref List<OpAlarmType> list, IDataProvider dataProvider)
        {
		}
        #endregion
		
        #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion
		
        #region IEquatable<OpAlarmType> Members
        public bool Equals(OpAlarmType other)
        {
            if (other == null)
				return false;
			return this.IdAlarmType.Equals(other.IdAlarmType);
        }
        #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdAlarmType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion

        #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpAlarmType)
				return this.IdAlarmType == ((OpAlarmType)obj).IdAlarmType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpAlarmType left, OpAlarmType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpAlarmType left, OpAlarmType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdAlarmType.GetHashCode();
		}
        #endregion
    }
#endif
}