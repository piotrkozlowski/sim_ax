﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceListDeviceDiagnostic : DB_SERVICE_LIST_DEVICE_DIAGNOSTIC, IComparable, IEquatable<OpServiceListDeviceDiagnostic>
    {
    #region Properties
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public int IdDiagnosticAction { get { return this.ID_DIAGNOSTIC_ACTION; } set { this.ID_DIAGNOSTIC_ACTION = value; } }
        public int? IdDiagnosticActionResult { get { return this.ID_DIAGNOSTIC_ACTION_RESULT; } set { this.ID_DIAGNOSTIC_ACTION_RESULT = value; } }
        public string InputText { get { return this.INPUT_TEXT; } set { this.INPUT_TEXT = value; } }
        public object DamageLevel { get { return this.DAMAGE_LEVEL; } set { this.DAMAGE_LEVEL = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? 0 : (int)value.ID_SERVICE_LIST; } }
        private OpDiagnosticAction _DiagnosticAction;
        public OpDiagnosticAction DiagnosticAction { get { return this._DiagnosticAction; } set { this._DiagnosticAction = value; this.ID_DIAGNOSTIC_ACTION = (value == null) ? 0 : (int)value.ID_DIAGNOSTIC_ACTION; } }
        private OpDiagnosticActionResult _DiagnosticActionResult;
        public OpDiagnosticActionResult DiagnosticActionResult { get { return this._DiagnosticActionResult; } set { this._DiagnosticActionResult = value; this.ID_DIAGNOSTIC_ACTION_RESULT = (value == null) ? null : (int?)value.ID_DIAGNOSTIC_ACTION_RESULT; } }
    #endregion

    #region	Custom Properties

        public string DiagnosticActionResultValue { get { return IdDiagnosticActionResult.HasValue ? (DiagnosticActionResult.AllowInputText ? DiagnosticActionResult.Descr.Description + " : " + InputText : DiagnosticActionResult.Descr.Description) : InputText; } }

    #endregion

    #region	Ctor
        public OpServiceListDeviceDiagnostic()
            : base() { }

        public OpServiceListDeviceDiagnostic(DB_SERVICE_LIST_DEVICE_DIAGNOSTIC clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceListDeviceDiagnostic [" + SerialNbr.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceListDeviceDiagnostic> ConvertList(DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceListDeviceDiagnostic> ConvertList(DB_SERVICE_LIST_DEVICE_DIAGNOSTIC[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceListDeviceDiagnostic> ret = new List<OpServiceListDeviceDiagnostic>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceListDeviceDiagnostic(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceListDeviceDiagnostic> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Select(l => l.ID_SERVICE_LIST).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<int, OpDiagnosticAction> DiagnosticActionDict = dataProvider.GetDiagnosticAction(list.Select(l => l.ID_DIAGNOSTIC_ACTION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DIAGNOSTIC_ACTION);
                Dictionary<int, OpDiagnosticActionResult> DiagnosticActionResultDict = dataProvider.GetDiagnosticActionResult(list.Where(l => l.ID_DIAGNOSTIC_ACTION_RESULT.HasValue).Select(l => l.ID_DIAGNOSTIC_ACTION_RESULT.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DIAGNOSTIC_ACTION_RESULT);

                foreach (var loop in list)
                {
                    loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST);
                    loop.DiagnosticAction = DiagnosticActionDict.TryGetValue(loop.ID_DIAGNOSTIC_ACTION);
                    if (loop.ID_DIAGNOSTIC_ACTION_RESULT.HasValue)
                        loop.DiagnosticActionResult = DiagnosticActionResultDict.TryGetValue(loop.ID_DIAGNOSTIC_ACTION_RESULT.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceListDeviceDiagnostic> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceListDeviceDiagnostic)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceListDeviceDiagnostic).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceListDeviceDiagnostic> Members
        public bool Equals(OpServiceListDeviceDiagnostic other)
        {
            if (other == null)
                return false;
            return this.SerialNbr.Equals(other.SerialNbr);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceListDeviceDiagnostic)
                return this.SerialNbr == ((OpServiceListDeviceDiagnostic)obj).SerialNbr;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceListDeviceDiagnostic left, OpServiceListDeviceDiagnostic right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceListDeviceDiagnostic left, OpServiceListDeviceDiagnostic right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return SerialNbr.GetHashCode();
        }
    #endregion
    }
#endif
}