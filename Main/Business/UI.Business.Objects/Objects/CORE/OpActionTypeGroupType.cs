using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpActionTypeGroupType : DB_ACTION_TYPE_GROUP_TYPE, IComparable, IEquatable<OpActionTypeGroupType>
    {
    #region Properties
            public int IdActionTypeGroupType { get { return this.ID_ACTION_TYPE_GROUP_TYPE;} set {this.ID_ACTION_TYPE_GROUP_TYPE = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpActionTypeGroupType()
			:base() {}
		
		public OpActionTypeGroupType(DB_ACTION_TYPE_GROUP_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpActionTypeGroupType> ConvertList(DB_ACTION_TYPE_GROUP_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpActionTypeGroupType> ConvertList(DB_ACTION_TYPE_GROUP_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpActionTypeGroupType> ret = new List<OpActionTypeGroupType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpActionTypeGroupType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpActionTypeGroupType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpActionTypeGroupType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionTypeGroupType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionTypeGroupType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpActionTypeGroupType> Members
        public bool Equals(OpActionTypeGroupType other)
        {
            if (other == null)
				return false;
			return this.IdActionTypeGroupType.Equals(other.IdActionTypeGroupType);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpActionTypeGroupType)
				return this.IdActionTypeGroupType == ((OpActionTypeGroupType)obj).IdActionTypeGroupType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpActionTypeGroupType left, OpActionTypeGroupType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpActionTypeGroupType left, OpActionTypeGroupType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdActionTypeGroupType.GetHashCode();
		}
    #endregion
    }
#endif
}