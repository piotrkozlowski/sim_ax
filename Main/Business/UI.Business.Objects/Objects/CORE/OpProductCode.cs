using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpProductCode : DB_PRODUCT_CODE, IComparable, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdProductCode { get { return this.ID_PRODUCT_CODE; } set { this.ID_PRODUCT_CODE = value; } }
        [XmlIgnore]
        public string Code { get { return this.CODE; } set { this.CODE = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public int? Density { get { return this.DENSITY; } set { this.DENSITY = value; } }
        [XmlIgnore]
        public Double CalorificValue { get { return this.CALORIFIC_VALUE; } set { this.CALORIFIC_VALUE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpProductCode()
            : base() { }

        public OpProductCode(DB_PRODUCT_CODE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Code;
        }
    #endregion

    #region	ConvertList
        public static List<OpProductCode> ConvertList(DB_PRODUCT_CODE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpProductCode> ConvertList(DB_PRODUCT_CODE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpProductCode> ret = new List<OpProductCode>(list.Length);
            foreach (var loop in list)
            {
                OpProductCode insert = new OpProductCode(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);


                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpProductCode> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpProductCode)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpProductCode).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpProductCode)
                return this.IdProductCode == ((OpProductCode)obj).IdProductCode;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpProductCode left, OpProductCode right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpProductCode left, OpProductCode right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdProductCode.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdProductCode;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}