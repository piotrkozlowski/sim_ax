using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerSettlementError : DB_CONSUMER_SETTLEMENT_ERROR, IComparable, IEquatable<OpConsumerSettlementError>
    {
    #region Properties
        public int IdConsumerSettlementError { get { return this.ID_CONSUMER_SETTLEMENT_ERROR;} set {this.ID_CONSUMER_SETTLEMENT_ERROR = value;}}
        public long IdConsumerSettlement { get { return this.ID_CONSUMER_SETTLEMENT;} set {this.ID_CONSUMER_SETTLEMENT = value;}}
        public int IdConsumerSettlementErrorType { get { return this.ID_CONSUMER_SETTLEMENT_ERROR_TYPE;} set {this.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = value;}}
        public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumerSettlement _ConsumerSettlement;
				public OpConsumerSettlement ConsumerSettlement { get { return this._ConsumerSettlement; } set { this._ConsumerSettlement = value; this.ID_CONSUMER_SETTLEMENT = (value == null)? 0 : (long)value.ID_CONSUMER_SETTLEMENT; } }
				private OpConsumerSettlementErrorType _ConsumerSettlementErrorType;
				public OpConsumerSettlementErrorType ConsumerSettlementErrorType { get { return this._ConsumerSettlementErrorType; } set { this._ConsumerSettlementErrorType = value; this.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = (value == null)? 0 : (int)value.ID_CONSUMER_SETTLEMENT_ERROR_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerSettlementError()
			:base() {}
		
		public OpConsumerSettlementError(DB_CONSUMER_SETTLEMENT_ERROR clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.DESCRIPTION;
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerSettlementError> ConvertList(DB_CONSUMER_SETTLEMENT_ERROR[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerSettlementError> ConvertList(DB_CONSUMER_SETTLEMENT_ERROR[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpConsumerSettlementError> ret = new List<OpConsumerSettlementError>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerSettlementError(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerSettlementError> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpConsumerSettlement> ConsumerSettlementDict = dataProvider.GetConsumerSettlement(list.Select(l => l.ID_CONSUMER_SETTLEMENT).Distinct().ToArray()).ToDictionary(l => l.ID_CONSUMER_SETTLEMENT);
	Dictionary<int,OpConsumerSettlementErrorType> ConsumerSettlementErrorTypeDict = dataProvider.GetConsumerSettlementErrorType(list.Select(l => l.ID_CONSUMER_SETTLEMENT_ERROR_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_CONSUMER_SETTLEMENT_ERROR_TYPE);
			
				foreach (var loop in list)
				{
						loop.ConsumerSettlement = ConsumerSettlementDict.TryGetValue(loop.ID_CONSUMER_SETTLEMENT); 
		loop.ConsumerSettlementErrorType = ConsumerSettlementErrorTypeDict.TryGetValue(loop.ID_CONSUMER_SETTLEMENT_ERROR_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerSettlementError> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerSettlementError)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerSettlementError).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerSettlementError> Members
        public bool Equals(OpConsumerSettlementError other)
        {
            if (other == null)
				return false;
			return this.IdConsumerSettlementError.Equals(other.IdConsumerSettlementError);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerSettlementError)
				return this.IdConsumerSettlementError == ((OpConsumerSettlementError)obj).IdConsumerSettlementError;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerSettlementError left, OpConsumerSettlementError right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerSettlementError left, OpConsumerSettlementError right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerSettlementError.GetHashCode();
		}
    #endregion
    }
#endif
}