using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpProtocolDriver : DB_PROTOCOL_DRIVER, IComparable
    {
    #region Properties
        [XmlIgnore]
        public int IdProtocolDriver { get { return this.ID_PROTOCOL_DRIVER; } set { if (this.ID_PROTOCOL_DRIVER != value) { this.ID_PROTOCOL_DRIVER = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string PluginName { get { return this.PLUGIN_NAME; } set { if (this.PLUGIN_NAME != value) { this.PLUGIN_NAME = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region Navigation Properties
		private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.IdDescr = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region Ctor
		public OpProtocolDriver()
			:base() 
        {
            OpState = OpChangeState.New;
        }
		
		public OpProtocolDriver(DB_PROTOCOL_DRIVER clone)
			:base(clone) 
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region ToString
		public override string ToString()
		{
			return this.NAME;
		}
    #endregion

    #region ConvertList
		public static List<OpProtocolDriver> ConvertList(DB_PROTOCOL_DRIVER[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpProtocolDriver> ConvertList(DB_PROTOCOL_DRIVER[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpProtocolDriver> ret = new List<OpProtocolDriver>(list.Length);
			list.ToList().ForEach(db_item => ret.Add(new OpProtocolDriver(db_item)));

			if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpProtocolDriver> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			if (list != null && list.Count > 0)
			{
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
				foreach (var loop in list)
				{
					if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
				}
			}
		}
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpProtocolDriver> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpProtocolDriver)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpProtocolDriver).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpProtocolDriver)
				return this.IdProtocolDriver == ((OpProtocolDriver)obj).IdProtocolDriver;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpProtocolDriver left, OpProtocolDriver right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpProtocolDriver left, OpProtocolDriver right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdProtocolDriver.GetHashCode();
		}
    #endregion
    }
#endif
}