using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpErrorLog : DB_ERROR_LOG, IComparable
    {
    #region Properties
        public int IdErrorLog { get { return this.ID_ERROR_LOG;} set {this.ID_ERROR_LOG = value;}}
        public int ApplicationId { get { return this.APPLICATION_ID;} set {this.APPLICATION_ID = value;}}
        public DateTime ErrorDate { get { return this.ERROR_DATE;} set {this.ERROR_DATE = value; } }
        public string ClassName { get { return this.CLASS_NAME;} set {this.CLASS_NAME = value;}}
        public string FunctionName { get { return this.FUNCTION_NAME;} set {this.FUNCTION_NAME = value;}}
        public string Source { get { return this.SOURCE;} set {this.SOURCE = value;}}
        public string Message { get { return this.MESSAGE;} set {this.MESSAGE = value;}}
        public string StackTrace { get { return this.STACK_TRACE;} set {this.STACK_TRACE = value;}}
        public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
        public int Severity { get { return this.SEVERITY;} set {this.SEVERITY = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpErrorLog()
			:base() {}
		
		public OpErrorLog(DB_ERROR_LOG clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.CLASS_NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpErrorLog> ConvertList(DB_ERROR_LOG[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpErrorLog> ConvertList(DB_ERROR_LOG[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpErrorLog> ret = new List<OpErrorLog>(list.Length);
			foreach (var loop in list)
			{
			OpErrorLog insert = new OpErrorLog(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
											
											
											
											
											
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpErrorLog> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpErrorLog)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpErrorLog).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}