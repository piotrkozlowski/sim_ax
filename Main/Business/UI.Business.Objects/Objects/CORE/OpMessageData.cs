﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMessageData : DB_MESSAGE_DATA, IComparable, IEquatable<OpMessageData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdMessageData { get { return this.ID_MESSAGE_DATA; } set { this.ID_MESSAGE_DATA = value; } }
        public long IdMessage { get { return this.ID_MESSAGE; } set { this.ID_MESSAGE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpMessage _Message;
        public OpMessage Message { get { return this._Message; } set { this._Message = value; this.ID_MESSAGE = (value == null) ? 0 : (long)value.ID_MESSAGE; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpMessageData()
            : base() { }

        public OpMessageData(DB_MESSAGE_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "MessageData [" + IdMessageData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpMessageData> ConvertList(DB_MESSAGE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMessageData> ConvertList(DB_MESSAGE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpMessageData> ret = new List<OpMessageData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMessageData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMessageData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
               // Dictionary<long, OpMessage> MessageDict = dataProvider.GetMessage(list.Select(l => l.ID_MESSAGE).Distinct().ToArray()).ToDictionary(l => l.ID_MESSAGE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                  //  loop.Message = MessageDict.TryGetValue(loop.ID_MESSAGE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpMessageData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMessageData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMessageData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpMessageData> Members
        public bool Equals(OpMessageData other)
        {
            if (other == null)
                return false;
            return this.IdMessageData.Equals(other.IdMessageData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMessageData)
                return this.IdMessageData == ((OpMessageData)obj).IdMessageData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMessageData left, OpMessageData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMessageData left, OpMessageData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMessageData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdMessageData; } set { IdMessageData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpMessageData clone = new OpMessageData();
            clone.IdData = this.IdData;
            clone.IdMessage = this.IdMessage;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_MESSAGE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}