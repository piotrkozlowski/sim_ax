using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionElement : DB_VERSION_ELEMENT, IComparable, IEquatable<OpVersionElement>, IOpChangeState, IOpDynamic, IReferenceType
    {
    #region Properties
        public int IdVersionElement { get { return this.ID_VERSION_ELEMENT; } set { if (this.ID_VERSION_ELEMENT != value) { this.ID_VERSION_ELEMENT = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT, value, this); } } }
        public int IdVersionElementType { get { return this.ID_VERSION_ELEMENT_TYPE; } set { if (this.ID_VERSION_ELEMENT_TYPE != value) { this.ID_VERSION_ELEMENT_TYPE = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_TYPE, value, this); } } }
        public int? IdVersionNbrFormat { get { return this.ID_VERSION_NBR_FORMAT; } set { if (this.ID_VERSION_NBR_FORMAT != value) { this.ID_VERSION_NBR_FORMAT = OpDataUtil.SetNewValue(this.ID_VERSION_NBR_FORMAT, value, this); } } }
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = OpDataUtil.SetNewValue(this.NAME, value, this); } } }
        public long? IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = OpDataUtil.SetNewValue(this.ID_DESCR, value, this); } } }
        public int? ReferenceType { get { return this.REFERENCE_TYPE; } set { if (this.REFERENCE_TYPE != value) { this.REFERENCE_TYPE = OpDataUtil.SetNewValue(this.REFERENCE_TYPE, value, this); } } }
        public object ReferenceValue { get { return this.REFERENCE_VALUE; } set { if (this.REFERENCE_VALUE != value) { this.REFERENCE_VALUE = OpDataUtil.SetNewValue(this.REFERENCE_VALUE, value, this); } } }
        public long? IdVersion { get { return this.ID_VERSION; } set { if (this.ID_VERSION != value) { this.ID_VERSION = OpDataUtil.SetNewValue(this.ID_VERSION, value, this); } } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
		private OpVersionElementType _VersionElementType;
		public OpVersionElementType VersionElementType { get { return this._VersionElementType; } set { this._VersionElementType = value; this.ID_VERSION_ELEMENT_TYPE = (value == null)? 0 : (int)value.ID_VERSION_ELEMENT_TYPE; } }
		private OpVersionNbrFormat _VersionNbrFormat;
		public OpVersionNbrFormat VersionNbrFormat { get { return this._VersionNbrFormat; } set { this._VersionNbrFormat = value; this.ID_VERSION_NBR_FORMAT = (value == null)? null : (int?)value.ID_VERSION_NBR_FORMAT; } }
		private OpDescr _Descr;
		public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
		private OpVersion _Version;
		public OpVersion Version { get { return this._Version; } set { this._Version = value; this.ID_VERSION = (value == null)? null : (long?)value.ID_VERSION; } }
    #endregion

    #region	Custom Properties
        private Dictionary<string, object> dynamicValues;
        public OpDataList<OpVersionElementData> DataList;
    #endregion
		
    #region	Ctor
        public OpVersionElement()
            : base()
        {
            DataList = new OpDataList<OpVersionElementData>();
            DynamicValues = new Dictionary<string, object>();
        }
		
		public OpVersionElement(DB_VERSION_ELEMENT clone)
			:base(clone)
        {
            DataList = new OpDataList<OpVersionElementData>();
            DynamicValues = new Dictionary<string, object>();
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
			return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionElement> ConvertList(DB_VERSION_ELEMENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionElement> ConvertList(DB_VERSION_ELEMENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionElement> ret = new List<OpVersionElement>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionElement(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionElement> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpVersionElementType> VersionElementTypeDict = dataProvider.GetVersionElementType(list.Select(l => l.ID_VERSION_ELEMENT_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_ELEMENT_TYPE);
                Dictionary<int, OpVersionNbrFormat> VersionNbrFormatDict = dataProvider.GetVersionNbrFormat(list.Where(l => l.ID_VERSION_NBR_FORMAT.HasValue).Select(l => l.ID_VERSION_NBR_FORMAT.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_NBR_FORMAT);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<long, OpVersion> VersionDict = dataProvider.GetVersion(list.Where(l => l.ID_VERSION.HasValue).Select(l => l.ID_VERSION.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION);
                foreach (var loop in list)
                {
                    loop.VersionElementType = VersionElementTypeDict.TryGetValue(loop.ID_VERSION_ELEMENT_TYPE);
                    if (loop.ID_VERSION_NBR_FORMAT.HasValue)
                        loop.VersionNbrFormat = VersionNbrFormatDict.TryGetValue(loop.ID_VERSION_NBR_FORMAT.Value);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    if (loop.ID_VERSION.HasValue)
                        loop.Version = VersionDict.TryGetValue(loop.ID_VERSION.Value);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionElement> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionElement)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionElement).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionElement> Members
        public bool Equals(OpVersionElement other)
        {
            if (other == null)
				return false;
			return this.IdVersionElement.Equals(other.IdVersionElement);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionElement)
				return this.IdVersionElement == ((OpVersionElement)obj).IdVersionElement;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionElement left, OpVersionElement right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionElement left, OpVersionElement right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionElement.GetHashCode();
		}
    #endregion

    #region IOpDynamic Members

        [System.Xml.Serialization.XmlIgnore]
        public object Key
        {
            get { return IdVersionElement; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [System.Xml.Serialization.XmlIgnore]
        public object Owner
        {
            get { return this; }
        }

    #endregion

    #region IReferenceType Members

        public object GetReferenceKey()
        {
            return IdVersionElement;
        }

        public object GetReferenceValue()
        {
            return this;
        }

    #endregion
    }
#endif
}