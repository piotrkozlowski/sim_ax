using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpOperatorRole : DB_OPERATOR_ROLE, IComparable, IOpChangeState, IEquatable<OpOperatorRole>
    {
    #region Properties
        public int IdOperator { get { return this.ID_OPERATOR; } set { if (this.ID_OPERATOR != value) { this.ID_OPERATOR = value; this.OpState = OpChangeState.Modified; } } }
        public int IdRole { get { return this.ID_ROLE; } set { if (this.ID_ROLE != value) { this.ID_ROLE = value; this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpRole _Role;
        public OpRole Role { get { return this._Role; } set { this._Role = value; this.ID_ROLE = (value == null) ? 0 : (int)value.ID_ROLE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }

    #endregion

    #region	Ctor
        public OpOperatorRole()
            : base()
        {
            OpState = OpChangeState.New;
        }

        public OpOperatorRole(DB_OPERATOR_ROLE clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "OperatorRole [" + IdOperator.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpOperatorRole> ConvertList(DB_OPERATOR_ROLE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true)
        {
            List<OpOperatorRole> ret = new List<OpOperatorRole>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpOperatorRole(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpOperatorRole> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpRole> RoleDict = dataProvider.GetRole(list.Select(l => l.ID_ROLE).Distinct().ToArray()).ToDictionary(l => l.ID_ROLE);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.Role = RoleDict.TryGetValue(loop.ID_ROLE);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpOperatorRole> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpOperatorRole)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpOperatorRole).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpRoleGroup> Members
        public bool Equals(OpOperatorRole other)
        {
            if (other == null)
                return false;
            return this.IdOperator.Equals(other.IdOperator) && this.IdRole.Equals(other.IdRole);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpOperatorRole)
                return this.IdOperator == ((OpOperatorRole)obj).IdOperator && this.IdRole == ((OpOperatorRole)obj).IdRole;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpOperatorRole left, OpOperatorRole right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpOperatorRole left, OpOperatorRole right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdOperator.GetHashCode() ^ IdRole.GetHashCode();
        }
    #endregion
    }
#endif
}
