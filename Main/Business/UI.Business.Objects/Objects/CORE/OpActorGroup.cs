using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpActorGroup : DB_ACTOR_GROUP, IComparable, IEquatable<OpActorGroup>, IOpChangeState, IReferenceType
    {
        #region Properties
        public int IdActorGroup
        {
            get { return this.ID_ACTOR_GROUP; }
            set { if (this.ID_ACTOR_GROUP != value) { this.ID_ACTOR_GROUP = value; this.OpState = OpChangeState.Modified; } }
        }

        public string Name
        {
            get { return this.NAME; }
            set { if (this.NAME != value) { this.NAME = value; this.OpState = OpChangeState.Modified; } }
        }

        public bool BuiltIn
        {
            get { return this.BUILT_IN; }
            set { if (this.BUILT_IN != value) { this.BUILT_IN = value; this.OpState = OpChangeState.Modified; } }
        }

        public int IdDistributor
        {
            get { return this.ID_DISTRIBUTOR; }
            set { if (this.ID_DISTRIBUTOR != value) { this.ID_DISTRIBUTOR = value; this.OpState = OpChangeState.Modified; } }
        }

        public long? IdDescr
        {
            get { return this.ID_DESCR; }
            set { if (this.ID_DESCR != value) { this.ID_DESCR = value; this.OpState = OpChangeState.Modified; } }
        }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        [DataMember]
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (int?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties
        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Ctor
        public OpActorGroup()
            : base()
        {
            OpState = OpChangeState.New;
        }

        public OpActorGroup(DB_ACTOR_GROUP clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpActorGroup> ConvertList(DB_ACTOR_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActorGroup> ConvertList(DB_ACTOR_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActorGroup> ret = new List<OpActorGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActorGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActorGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpActorGroup> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActorGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActorGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpActorGroup> Members
        public bool Equals(OpActorGroup other)
        {
            if (other == null)
                return false;
            return this.IdActorGroup.Equals(other.IdActorGroup);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActorGroup)
                return this.IdActorGroup == ((OpActorGroup)obj).IdActorGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActorGroup left, OpActorGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActorGroup left, OpActorGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActorGroup.GetHashCode();
        }
        #endregion
        
        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdActorGroup;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion
    }
#endif
}