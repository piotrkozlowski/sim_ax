using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable]
	public class OpConsumerNotificationTypeData : DB_CONSUMER_NOTIFICATION_TYPE_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpConsumerNotificationTypeData>
    {
		#region Properties
        public long IdConsumerNotificationTypeData { get { return this.ID_CONSUMER_NOTIFICATION_TYPE_DATA;} set {this.ID_CONSUMER_NOTIFICATION_TYPE_DATA = value;}}
        public int IdConsumer { get { return this.ID_CONSUMER;} set {this.ID_CONSUMER = value;}}
        public int IdNotificationType { get { return this.ID_NOTIFICATION_TYPE;} set {this.ID_NOTIFICATION_TYPE = value;}}
        public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
        public int Index { get { return this.INDEX_NBR;} set {this.INDEX_NBR = value;}}
        public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
		#endregion

		#region	Navigation Properties
		private OpConsumer _Consumer;
				public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null)? 0 : (int)value.ID_CONSUMER; } }
				private OpNotificationType _NotificationType;
				public OpNotificationType NotificationType { get { return this._NotificationType; } set { this._NotificationType = value; this.ID_NOTIFICATION_TYPE = (value == null)? 0 : (int)value.ID_NOTIFICATION_TYPE; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
				#endregion

		#region	Custom Properties

        public OpChangeState OpState { get; set; }

        public long IdData { get { return IdConsumerNotificationTypeData; } set { IdConsumerNotificationTypeData = value; } }

        public object ReferencedObject { get; set; }

        #endregion

        #region	Ctor
        public OpConsumerNotificationTypeData()
			:base() {}
		
		public OpConsumerNotificationTypeData(DB_CONSUMER_NOTIFICATION_TYPE_DATA clone)
			:base(clone) {}
		#endregion
		
		#region	ToString
		public override string ToString()
		{
				return "ConsumerNotificationTypeData [" + IdConsumerNotificationTypeData.ToString() + "]";
		}
		#endregion

		#region	ConvertList

		public static List<OpConsumerNotificationTypeData> ConvertList(DB_CONSUMER_NOTIFICATION_TYPE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerNotificationTypeData> ConvertList(DB_CONSUMER_NOTIFICATION_TYPE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpConsumerNotificationTypeData> ret = new List<OpConsumerNotificationTypeData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerNotificationTypeData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			
            if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			
            return ret;
		}

		#endregion
		
		#region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerNotificationTypeData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Select(l => l.ID_CONSUMER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER);
	Dictionary<int,OpNotificationType> NotificationTypeDict = dataProvider.GetNotificationType(list.Select(l => l.ID_NOTIFICATION_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_NOTIFICATION_TYPE);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
						loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER); 
		loop.NotificationType = NotificationTypeDict.TryGetValue(loop.ID_NOTIFICATION_TYPE); 
		loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
		#endregion
		
		#region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerNotificationTypeData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
        #endregion

        #region IOpDataProvider

        #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
        #endregion

        #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpConsumerNotificationTypeData clone = new OpConsumerNotificationTypeData();
            clone.IdData = this.IdData;
            clone.IdConsumer = this.IdConsumer;
            clone.IdNotificationType = this.IdNotificationType;
            clone.IdConsumerNotificationTypeData = this.IdConsumerNotificationTypeData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_CONSUMER_NOTIFICATION_TYPE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
        #endregion

        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerNotificationTypeData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerNotificationTypeData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
		#endregion
		
		#region IEquatable<OpConsumerNotificationTypeData> Members
        public bool Equals(OpConsumerNotificationTypeData other)
        {
            if (other == null)
				return false;
			return this.IdConsumerNotificationTypeData.Equals(other.IdConsumerNotificationTypeData);
        }
        #endregion

		#region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerNotificationTypeData)
				return this.IdConsumerNotificationTypeData == ((OpConsumerNotificationTypeData)obj).IdConsumerNotificationTypeData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerNotificationTypeData left, OpConsumerNotificationTypeData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerNotificationTypeData left, OpConsumerNotificationTypeData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerNotificationTypeData.GetHashCode();
		}
		#endregion
    }
}