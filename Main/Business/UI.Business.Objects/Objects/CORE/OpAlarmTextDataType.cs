using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpAlarmTextDataType : DB_ALARM_TEXT_DATA_TYPE, IComparable, IEquatable<OpAlarmTextDataType>
    {
    #region Properties
            public long IdAlarmText { get { return this.ID_ALARM_TEXT;} set {this.ID_ALARM_TEXT = value;}}
                public int IdLanguage { get { return this.ID_LANGUAGE;} set {this.ID_LANGUAGE = value;}}
                public int ArgNbr { get { return this.ARG_NBR;} set {this.ARG_NBR = value; OpState = OpChangeState.Modified;}}
                public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; OpState = OpChangeState.Modified; } }
                public int IdUnit { get { return this.ID_UNIT; } set { this.ID_UNIT = value; OpState = OpChangeState.Modified; } }
    #endregion

    #region	Navigation Properties
		private OpAlarmText _AlarmText;
				public OpAlarmText AlarmText { get { return this._AlarmText; } set { this._AlarmText = value; this.ID_ALARM_TEXT = (value == null)? 0 : (long)value.ID_ALARM_TEXT; } }
				private OpLanguage _Language;
				public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null)? 0 : (int)value.ID_LANGUAGE; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
				private OpUnit _Unit;
				public OpUnit Unit { get { return this._Unit; } set { this._Unit = value; this.ID_UNIT = (value == null)? 0 : (int)value.ID_UNIT; } }
    #endregion

    #region	Custom Properties
                public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpAlarmTextDataType()
			:base() {OpState = OpChangeState.New;}
		
		public OpAlarmTextDataType(DB_ALARM_TEXT_DATA_TYPE clone)
			:base(clone) {OpState = OpChangeState.Loaded;}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "AlarmTextDataType [" + IdAlarmText.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpAlarmTextDataType> ConvertList(DB_ALARM_TEXT_DATA_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpAlarmTextDataType> ConvertList(DB_ALARM_TEXT_DATA_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpAlarmTextDataType> ret = new List<OpAlarmTextDataType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmTextDataType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpAlarmTextDataType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray()).ToDictionary(l => l.ID_LANGUAGE);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
	Dictionary<int,OpUnit> UnitDict = dataProvider.GetUnit(list.Select(l => l.ID_UNIT).Distinct().ToArray()).ToDictionary(l => l.ID_UNIT);			
				foreach (var loop in list)
				{
						loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE); 
		loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
		loop.Unit = UnitDict.TryGetValue(loop.ID_UNIT); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpAlarmTextDataType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmTextDataType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmTextDataType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpAlarmTextDataType> Members
        public bool Equals(OpAlarmTextDataType other)
        {
            if (other == null)
				return false;
            return this.IdAlarmText.Equals(other.IdAlarmText) && this.IdLanguage.Equals(other.IdLanguage) && this.ArgNbr.Equals(other.ArgNbr);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpAlarmTextDataType)
                return this.IdAlarmText == ((OpAlarmTextDataType)obj).IdAlarmText && this.IdLanguage == ((OpAlarmTextDataType)obj).IdLanguage && this.ArgNbr == ((OpAlarmTextDataType)obj).ArgNbr;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpAlarmTextDataType left, OpAlarmTextDataType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpAlarmTextDataType left, OpAlarmTextDataType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdAlarmText.GetHashCode();
		}
    #endregion
    }
#endif
}