using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionType : DB_ACTION_TYPE, IComparable, IEquatable<OpActionType>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdActionType { get { return this.ID_ACTION_TYPE; } set { this.ID_ACTION_TYPE = value; } }
        [XmlIgnore]
        public int IdActionTypeClass { get { return this.ID_ACTION_TYPE_CLASS; } set { this.ID_ACTION_TYPE_CLASS = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public string PluginName { get { return this.PLUGIN_NAME; } set { this.PLUGIN_NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public bool IsActionDataFixed { get { return this.IS_ACTION_DATA_FIXED; } set { this.IS_ACTION_DATA_FIXED = value; } }
    #endregion

    #region	Navigation Properties
        private OpActionTypeClass _ActionTypeClass;
        [XmlIgnore]
        public OpActionTypeClass ActionTypeClass { get { return this._ActionTypeClass; } set { this._ActionTypeClass = value; this.ID_ACTION_TYPE_CLASS = (value == null) ? 0 : (int)value.ID_ACTION_TYPE_CLASS; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpActionType()
            : base() { }

        public OpActionType(DB_ACTION_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpActionType> ConvertList(DB_ACTION_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActionType> ConvertList(DB_ACTION_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpActionType> ret = new List<OpActionType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActionType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpActionTypeClass> ActionTypeClassDict = dataProvider.GetActionTypeClass(list.Select(l => l.ID_ACTION_TYPE_CLASS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTION_TYPE_CLASS);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);

                foreach (var loop in list)
                {
                    loop.ActionTypeClass = ActionTypeClassDict.TryGetValue(loop.ID_ACTION_TYPE_CLASS);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionType> Members
        public bool Equals(OpActionType other)
        {
            if (other == null)
                return false;
            return this.IdActionType.Equals(other.IdActionType);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActionType)
                return this.IdActionType == ((OpActionType)obj).IdActionType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActionType left, OpActionType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionType left, OpActionType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActionType.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdActionType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}