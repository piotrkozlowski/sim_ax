using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpRouteType : DB_ROUTE_TYPE, IComparable, IReferenceType
    {
    #region Properties
            public int IdRouteType { get { return this.ID_ROUTE_TYPE;} set {this.ID_ROUTE_TYPE = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpRouteType()
			:base() {}
		
		public OpRouteType(DB_ROUTE_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
		}
    #endregion

    #region	ConvertList
		public static List<OpRouteType> ConvertList(DB_ROUTE_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpRouteType> ConvertList(DB_ROUTE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpRouteType> ret = new List<OpRouteType>(list.Length);
			foreach (var loop in list)
			{
			OpRouteType insert = new OpRouteType(loop);
				
				if(loadNavigationProperties)
				{
											
											
																									if (loop.ID_DESCR.HasValue)
								insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRouteType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRouteType)
                return this.IdRouteType == ((OpRouteType)obj).IdRouteType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRouteType left, OpRouteType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRouteType left, OpRouteType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRouteType.GetHashCode();
        }
    #endregion

    #region Enum
        [Obsolete("Use Common.Enums version")]
        public enum Enum
        {
            UNKNOWN = 0,
            READOUT = 1,
            INSTALLATION = 2,
            SERVICE = 3,
            DELIVERY = 4
        }
    #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdRouteType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion
    }
#endif
}