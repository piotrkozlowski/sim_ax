﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataFormatGroupDetails : DB_DATA_FORMAT_GROUP_DETAILS, IComparable, IEquatable<OpDataFormatGroupDetails>
    {
    #region Properties
        public int IdDataFormatGroup { get { return this.ID_DATA_FORMAT_GROUP; } set { this.ID_DATA_FORMAT_GROUP = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int? IdDataTypeFormatIn { get { return this.ID_DATA_TYPE_FORMAT_IN; } set { this.ID_DATA_TYPE_FORMAT_IN = value; } }
        public int? IdDataTypeFormatOut { get { return this.ID_DATA_TYPE_FORMAT_OUT; } set { this.ID_DATA_TYPE_FORMAT_OUT = value; } }
        public int? IdUnitIn { get { return this.ID_UNIT_IN; } set { this.ID_UNIT_IN = value; } }
        public int? IdUnitOut { get { return this.ID_UNIT_OUT; } set { this.ID_UNIT_OUT = value; } }
    #endregion

    #region	Navigation Properties
        private OpDataFormatGroup _DataFormatGroup;
        public OpDataFormatGroup DataFormatGroup { get { return this._DataFormatGroup; } set { this._DataFormatGroup = value; this.ID_DATA_FORMAT_GROUP = (value == null) ? 0 : (int)value.ID_DATA_FORMAT_GROUP; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpUnit _UnitIn;
        public OpUnit UnitIn { get { return this._UnitIn; } set { this._UnitIn = value; this.ID_UNIT_IN = (value == null) ? null : (int?)value.ID_UNIT; } }
        private OpUnit _UnitOut;
        public OpUnit UnitOut { get { return this._UnitOut; } set { this._UnitOut = value; this.ID_UNIT_OUT = (value == null) ? null : (int?)value.ID_UNIT; } }
        private OpDataTypeFormat _DataTypeFormatIn;
        public OpDataTypeFormat DataTypeFormatIn { get { return this._DataTypeFormatIn; } set { this._DataTypeFormatIn = value; this.ID_DATA_TYPE_FORMAT_IN = (value == null) ? null : (int?)value.ID_DATA_TYPE_FORMAT; } }
        private OpDataTypeFormat _DataTypeFormatOut;
        public OpDataTypeFormat DataTypeFormatOut { get { return this._DataTypeFormatOut; } set { this._DataTypeFormatOut = value; this.ID_DATA_TYPE_FORMAT_OUT = (value == null) ? null : (int?)value.ID_DATA_TYPE_FORMAT; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpDataFormatGroupDetails()
            : base() { OpState = OpChangeState.New; }

        public OpDataFormatGroupDetails(DB_DATA_FORMAT_GROUP_DETAILS clone)
            : base(clone) { OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DataFormatGroupDetails [" + IdDataFormatGroup.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDataFormatGroupDetails> ConvertList(DB_DATA_FORMAT_GROUP_DETAILS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataFormatGroupDetails> ConvertList(DB_DATA_FORMAT_GROUP_DETAILS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDataFormatGroupDetails> ret = new List<OpDataFormatGroupDetails>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataFormatGroupDetails(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataFormatGroupDetails> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpDataTypeFormat> DataTypeFormatDict = new Dictionary<int, OpDataTypeFormat>();
                Dictionary<int, OpUnit> UnitDict = new Dictionary<int, OpUnit>();
                List<int> DataTypeFormatIds = new List<int>();
                List<int> UnitIds = new List<int>();
                if (list.Any(q => q.ID_DATA_TYPE_FORMAT_IN.HasValue))
                    DataTypeFormatIds.AddRange(list.Where(q => q.ID_DATA_TYPE_FORMAT_IN.HasValue).Select(q => q.ID_DATA_TYPE_FORMAT_IN.Value));
                if (list.Any(q => q.ID_DATA_TYPE_FORMAT_OUT.HasValue))
                    DataTypeFormatIds.AddRange(list.Where(q => q.ID_DATA_TYPE_FORMAT_OUT.HasValue).Select(q => q.ID_DATA_TYPE_FORMAT_OUT.Value));
                if (list.Any(q => q.ID_UNIT_IN.HasValue))
                    UnitIds.AddRange(list.Where(q => q.ID_UNIT_IN.HasValue).Select(q => q.ID_UNIT_IN.Value));
                if (list.Any(q => q.ID_UNIT_OUT.HasValue))
                    UnitIds.AddRange(list.Where(q => q.ID_UNIT_OUT.HasValue).Select(q => q.ID_UNIT_OUT.Value));
                if (DataTypeFormatIds.Count > 0)
                    DataTypeFormatDict = dataProvider.GetDataTypeFormat(DataTypeFormatIds.Distinct().ToArray()).ToDictionary(q => q.ID_DATA_TYPE_FORMAT);
                if (UnitIds.Count > 0)
                    UnitDict = dataProvider.GetUnit(UnitIds.Distinct().ToArray()).ToDictionary(q => q.ID_UNIT);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DATA_TYPE_FORMAT_IN.HasValue)
                        loop.DataTypeFormatIn = DataTypeFormatDict.TryGetValue(loop.ID_DATA_TYPE_FORMAT_IN.Value);
                    if (loop.ID_DATA_TYPE_FORMAT_OUT.HasValue)
                        loop.DataTypeFormatOut = DataTypeFormatDict.TryGetValue(loop.ID_DATA_TYPE_FORMAT_OUT.Value);
                    if (loop.ID_UNIT_IN.HasValue)
                        loop.UnitIn = UnitDict.TryGetValue(loop.ID_UNIT_IN.Value);
                    if (loop.ID_UNIT_OUT.HasValue)
                        loop.UnitOut = UnitDict.TryGetValue(loop.ID_UNIT_OUT.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataFormatGroupDetails> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataFormatGroupDetails)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataFormatGroupDetails).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDataFormatGroupDetails> Members
        public bool Equals(OpDataFormatGroupDetails other)
        {
            if (other == null)
                return false;
            return this.IdDataFormatGroup.Equals(other.IdDataFormatGroup);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDataFormatGroupDetails)
                return this.IdDataFormatGroup == ((OpDataFormatGroupDetails)obj).IdDataFormatGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDataFormatGroupDetails left, OpDataFormatGroupDetails right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDataFormatGroupDetails left, OpDataFormatGroupDetails right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDataFormatGroup.GetHashCode();
        }
    #endregion
    }
#endif
}