﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceHierarchyInGroup : DB_DEVICE_HIERARCHY_IN_GROUP, IComparable, IEquatable<OpDeviceHierarchyInGroup>
    {
    #region Properties
        public long IdDeviceHierarchyGroup { get { return this.ID_DEVICE_HIERARCHY_GROUP; } set { this.ID_DEVICE_HIERARCHY_GROUP = value; } }
        public long IdDeviceHierarchy { get { return this.ID_DEVICE_HIERARCHY; } set { this.ID_DEVICE_HIERARCHY = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceHierarchyGroup _DeviceHierarchyGroup;
        public OpDeviceHierarchyGroup DeviceHierarchyGroup { get { return this._DeviceHierarchyGroup; } set { this._DeviceHierarchyGroup = value; this.ID_DEVICE_HIERARCHY_GROUP = (value == null) ? 0 : (long)value.ID_DEVICE_HIERARCHY_GROUP; } }
        private OpDeviceHierarchy _DeviceHierarchy;
        public OpDeviceHierarchy DeviceHierarchy { get { return this._DeviceHierarchy; } set { this._DeviceHierarchy = value; this.ID_DEVICE_HIERARCHY = (value == null) ? 0 : (long)value.ID_DEVICE_HIERARCHY; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceHierarchyInGroup()
            : base() { }

        public OpDeviceHierarchyInGroup(DB_DEVICE_HIERARCHY_IN_GROUP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeviceHierarchyInGroup [" + IdDeviceHierarchyGroup.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceHierarchyInGroup> ConvertList(DB_DEVICE_HIERARCHY_IN_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceHierarchyInGroup> ConvertList(DB_DEVICE_HIERARCHY_IN_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceHierarchyInGroup> ret = new List<OpDeviceHierarchyInGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceHierarchyInGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceHierarchyInGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDeviceHierarchy> DeviceHierarchyDict = dataProvider.GetDeviceHierarchy(list.Select(l => l.ID_DEVICE_HIERARCHY).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_HIERARCHY);
                foreach (var loop in list)
                {
                    loop.DeviceHierarchy = DeviceHierarchyDict.TryGetValue(loop.ID_DEVICE_HIERARCHY);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceHierarchyInGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceHierarchyInGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceHierarchyInGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceHierarchyInGroup> Members
        public bool Equals(OpDeviceHierarchyInGroup other)
        {
            if (other == null)
                return false;
            return this.IdDeviceHierarchyGroup.Equals(other.IdDeviceHierarchyGroup);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceHierarchyInGroup)
                return this.IdDeviceHierarchyGroup == ((OpDeviceHierarchyInGroup)obj).IdDeviceHierarchyGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceHierarchyInGroup left, OpDeviceHierarchyInGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceHierarchyInGroup left, OpDeviceHierarchyInGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceHierarchyGroup.GetHashCode();
        }
    #endregion
    }
#endif
}