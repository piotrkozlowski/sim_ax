﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable, DataContract]
    public class OpView : DB_VIEW, IComparable, IEquatable<OpView>, IOpChangeState, IReferenceType
    {
        #region Properties
        public int IdView { get { return this.ID_VIEW; } set { this.ID_VIEW = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { this.ID_REFERENCE_TYPE = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpReferenceType _ReferenceType;

        public OpReferenceType ReferenceType { get { return this._ReferenceType; } set { this._ReferenceType = value; this.ID_REFERENCE_TYPE = (value == null) ? 0 : (int)value.ID_REFERENCE_TYPE; } }

        [DataMember]
        public OpChangeState OpState { get; set; }

        [DataMember]
        public OpDataList<OpViewData> DataList { get; set; }

        #endregion

        #region	Custom Properties

        [DataMember]
        public List<OpViewColumn> Columns { get; set; }
        public OpViewColumn PrimaryKeyColumn { get { return Columns == null ? null : this.Columns.Find(c => c.PrimaryKey && c.IdDataType != DataType.HELPER_ID_IMR_SERVER); } }
        public OpViewColumn ImrServerColumn { get { return Columns == null ? null : this.Columns.Find(c => c.PrimaryKey && c.IdDataType == DataType.HELPER_ID_IMR_SERVER); } }

        [DataMember]
        public List<OpImrServer> ImrServers { get; set; }

        [DataMember]
        public Enums.Tables Table { get; set; }

        public string DatabaseName
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.VIEW_DATABASE_NAME);
            }
            set
            {
                DataList.SetValue(DataType.VIEW_DATABASE_NAME, value);
            }
        }
        public bool IsSynchronizable
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.VIEW_IS_SYNCHRONIZABLE);
            }
            set
            {
                DataList.SetValue(DataType.VIEW_IS_SYNCHRONIZABLE, value);
            }
        }
        public bool SimpleColumnNames
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.VIEW_SIMPLE_COLUMN_NAMES);
            }
            set
            {
                DataList.SetValue(DataType.VIEW_SIMPLE_COLUMN_NAMES, value);
            }
        }
        public Enums.DatabaseType DatabaseType
        {
            get
            {
                return (Enums.DatabaseType)DataList.TryGetValue<int>(DataType.VIEW_ID_DATABASE_TYPE);
            }
            set
            {
                DataList.SetValue(DataType.VIEW_ID_DATABASE_TYPE, (int)value);
            }
        }
        
        public IEnumerable<OpViewColumn> ColumnsWithAssociativeTable
        {
            get
            {
                return Columns.Where(x => x.DataList.Any(y => y.IdDataType == DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE));
            }
        }

        #endregion

        #region	Ctor
        public OpView()
            : base()
        {
            this.OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpViewData>();
            this.Columns = new List<OpViewColumn>();
            this.ImrServers = new List<OpImrServer>();
        }

        public OpView(DB_VIEW clone)
            : base(clone)
        {
            this.OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpViewData>();
            this.Columns = new List<OpViewColumn>();
            this.ImrServers = new List<OpImrServer>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpView> ConvertList(DB_VIEW[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpView> ConvertList(DB_VIEW[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, List<long> customDataTypes = null, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpView> ret = new List<OpView>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpView(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes: customDataTypes, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpView> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpReferenceType> ReferenceTypeDict = dataProvider.GetReferenceType(list.Select(l => l.ID_REFERENCE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_REFERENCE_TYPE);
                foreach (var loop in list)
                {
                    loop.ReferenceType = ReferenceTypeDict.TryGetValue(loop.ID_REFERENCE_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        public static void LoadCustomData(ref List<OpView> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> idView = list.Select(l => l.ID_VIEW).ToList();
                List<OpViewData> data = new List<OpViewData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetViewDataFilter(IdView: idView.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.ILocationDataTypes != null && dataProvider.ILocationDataTypes.Count > 0)
                    data = dataProvider.GetViewDataFilter(IdView: idView.ToArray(), IdDataType: dataProvider.ILocationDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data.Count > 0)
                {
                    Dictionary<int, OpView> viewsDict = list.ToDictionary<OpView, int>(l => l.IdView);
                    data.ForEach(w => viewsDict[w.IdView].DataList.Add(w));
                }
                foreach(OpView vItem in list)
                {
                    if (Enum.IsDefined(typeof(Enums.Tables), vItem.Name))
                        vItem.Table = (Enums.Tables)Enum.Parse(typeof(Enums.Tables), vItem.Name);
                    else
                        vItem.Table = Enums.Tables.UNKNOWN;
                }
            }
        }
        #endregion

        #region LoadViewColumns
        public static void LoadViewColumns(ref List<OpView> list, IDataProvider dataProvider,
            List<long> customViewColumnDataType = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> idView = list.Select(l => l.ID_VIEW).ToList();
                List<OpViewColumn> columns = dataProvider.GetViewColumnFilter(IdView: idView.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                OpViewColumn.LoadCustomData(ref columns, dataProvider, customViewColumnDataType, autoTransaction, transactionLevel, commandTimeout); 
                
                list.ForEach(v => v.Columns = new List<OpViewColumn>());
                if (columns.Count > 0)
                {
                    Dictionary<int, OpView> viewsDict = list.ToDictionary<OpView, int>(l => l.IdView);
                    columns.ForEach(w => viewsDict[w.IdView].Columns.Add(w));
                }

            }
        }
        #endregion

        #region GetViewColumn
        public OpViewColumn GetViewColumn(long idDataType,
            string columnName = null, int indexNbr = 0,
            Enums.ViewColumnType viewColumnType = Enums.ViewColumnType.Unknown,
            OpViewColumn keyColumn = null)
        {
            OpViewColumn column =  this.Columns.SingleOrDefault(c =>
                c.IdDataType == idDataType
                && c.ColumnIndexNbr == indexNbr
                && (String.IsNullOrEmpty(columnName) || String.Equals(columnName, c.Name))
                && (IdView == 0 || viewColumnType == Enums.ViewColumnType.Unknown || c.IdViewColumnType == (int)viewColumnType)
                && (keyColumn == null || (keyColumn != null && c.IdKeyViewColumn == keyColumn.IdViewColumn)));
            return column;
        }
        public List<OpViewColumn> GetViewColumns(long idDataType,
            string columnName = null, int indexNbr = 0,
            Enums.ViewColumnType viewColumnType = Enums.ViewColumnType.Unknown,
            OpViewColumn keyColumn = null)
        {
            return this.Columns.FindAll(c =>
               c.IdDataType == idDataType
               && c.ColumnIndexNbr == indexNbr
               && (String.IsNullOrEmpty(columnName) || String.Equals(columnName, c.Name))
               && (IdView == 0 || viewColumnType == Enums.ViewColumnType.Unknown || c.IdViewColumnType == (int)viewColumnType)
               && (keyColumn == null || (keyColumn != null && c.IdKeyViewColumn == keyColumn.IdViewColumn)));
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpView)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpView).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpView> Members
        public bool Equals(OpView other)
        {
            if (other == null)
                return false;
            return this.IdView.Equals(other.IdView);
        }
        #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdView;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpView)
                return this.IdView == ((OpView)obj).IdView;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpView left, OpView right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpView left, OpView right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdView.GetHashCode();
        }
        #endregion
    }
}