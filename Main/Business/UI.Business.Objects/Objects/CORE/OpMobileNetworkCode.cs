using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpMobileNetworkCode : DB_MOBILE_NETWORK_CODE, IComparable
    {
    #region Properties
            public int Code { get { return this.CODE;} set {this.CODE = value;}}
                public string Mmc { get { return this.MMC;} set {this.MMC = value;}}
                public string Mnc { get { return this.MNC;} set {this.MNC = value;}}
                public string Brand { get { return this.BRAND;} set {this.BRAND = value;}}
                public string Operator { get { return this.OPERATOR;} set {this.OPERATOR = value;}}
                public string Bands { get { return this.BANDS;} set {this.BANDS = value;}}
                public string SmsServiceCenter { get { return this.SMS_SERVICE_CENTER;} set {this.SMS_SERVICE_CENTER = value;}}
                public string ApnAddress { get { return this.APN_ADDRESS;} set {this.APN_ADDRESS = value;}}
                public string ApnLogin { get { return this.APN_LOGIN;} set {this.APN_LOGIN = value;}}
                public string ApnPassword { get { return this.APN_PASSWORD;} set {this.APN_PASSWORD = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpMobileNetworkCode()
			:base() {}
		
		public OpMobileNetworkCode(DB_MOBILE_NETWORK_CODE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            StringBuilder sb = new StringBuilder();
            if (!String.IsNullOrWhiteSpace(this.Operator))
                sb.AppendFormat("{0} ", this.Operator);
            if (!String.IsNullOrWhiteSpace(this.Brand))
                sb.AppendFormat("[{0}] ", this.Brand);
            sb.AppendFormat("({0})", this.Code);

            return sb.ToString();
		}
    #endregion

    #region	ConvertList
		public static List<OpMobileNetworkCode> ConvertList(DB_MOBILE_NETWORK_CODE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpMobileNetworkCode> ConvertList(DB_MOBILE_NETWORK_CODE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpMobileNetworkCode> ret = new List<OpMobileNetworkCode>(list.Length);
			foreach (var loop in list)
			{
			OpMobileNetworkCode insert = new OpMobileNetworkCode(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
											
											
											
											
											
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpMobileNetworkCode> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMobileNetworkCode)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMobileNetworkCode).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}