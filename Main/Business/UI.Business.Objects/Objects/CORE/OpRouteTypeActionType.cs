﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpRouteTypeActionType : DB_ROUTE_TYPE_ACTION_TYPE, IComparable, IEquatable<OpRouteTypeActionType>
    {
    #region Properties
        public int IdRouteTypeActionType { get { return this.ID_ROUTE_TYPE_ACTION_TYPE; } set { this.ID_ROUTE_TYPE_ACTION_TYPE = value; } }
        public int IdRouteType { get { return this.ID_ROUTE_TYPE; } set { this.ID_ROUTE_TYPE = value; } }
        public int IdActionType { get { return this.ID_ACTION_TYPE; } set { this.ID_ACTION_TYPE = value; } }
        public bool Default { get { return this.DEFAULT; } set { this.DEFAULT = value; } }
    #endregion

    #region	Navigation Properties
        private OpRouteType _RouteType;
        public OpRouteType RouteType { get { return this._RouteType; } set { this._RouteType = value; this.ID_ROUTE_TYPE = (value == null) ? 0 : (int)value.ID_ROUTE_TYPE; } }
        private OpActionType _ActionType;
        public OpActionType ActionType { get { return this._ActionType; } set { this._ActionType = value; this.ID_ACTION_TYPE = (value == null) ? 0 : (int)value.ID_ACTION_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpRouteTypeActionType()
            : base() { }

        public OpRouteTypeActionType(DB_ROUTE_TYPE_ACTION_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "RouteTypeActionType [" + IdRouteTypeActionType.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpRouteTypeActionType> ConvertList(DB_ROUTE_TYPE_ACTION_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRouteTypeActionType> ConvertList(DB_ROUTE_TYPE_ACTION_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpRouteTypeActionType> ret = new List<OpRouteTypeActionType>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRouteTypeActionType(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRouteTypeActionType> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpRouteType> RouteTypeDict = dataProvider.GetRouteType(list.Select(l => l.ID_ROUTE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ROUTE_TYPE);
                Dictionary<int, OpActionType> ActionTypeDict = dataProvider.GetActionType(list.Select(l => l.ID_ACTION_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_TYPE);

                foreach (var loop in list)
                {
                    loop.RouteType = RouteTypeDict.TryGetValue(loop.ID_ROUTE_TYPE);
                    loop.ActionType = ActionTypeDict.TryGetValue(loop.ID_ACTION_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRouteTypeActionType> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteTypeActionType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteTypeActionType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpRouteTypeActionType> Members
        public bool Equals(OpRouteTypeActionType other)
        {
            if (other == null)
                return false;
            return this.IdRouteTypeActionType.Equals(other.IdRouteTypeActionType);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRouteTypeActionType)
                return this.IdRouteTypeActionType == ((OpRouteTypeActionType)obj).IdRouteTypeActionType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRouteTypeActionType left, OpRouteTypeActionType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRouteTypeActionType left, OpRouteTypeActionType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRouteTypeActionType.GetHashCode();
        }
    #endregion
    }
#endif
}