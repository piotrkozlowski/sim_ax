using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpProfileData : DB_PROFILE_DATA, IComparable, IEquatable<OpProfileData>, IOpData, IOpDataProvider
    {
    #region Properties
        [XmlIgnore]
        public long IdProfileData { get { return this.ID_PROFILE_DATA;} set {this.ID_PROFILE_DATA = value;}}
        [XmlIgnore]
        public int IdProfile { get { return this.ID_PROFILE;} set {this.ID_PROFILE = value;}}
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
        [XmlIgnore]
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region	Navigation Properties
        [DataMember]
		private OpProfile _Profile;
		public OpProfile Profile { get { return this._Profile; } set { this._Profile = value; this.ID_PROFILE = (value == null)? 0 : (int)value.ID_PROFILE; } }

        [DataMember]
        private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdProfileData; } set { IdProfileData = value; } }
        [XmlIgnore]
        public int Index { get { return IndexNbr; } set { IndexNbr = value; } }
        
        [XmlIgnore, DataMember]
        public OpChangeState OpState { get; set; }
        [XmlIgnore]
        public object ReferencedObject { get; set; }
    #endregion
		
    #region	Ctor
		public OpProfileData()
			:base() 
        {
            OpState = OpChangeState.New;
        }
		
		public OpProfileData(DB_PROFILE_DATA clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
		{
			return "ProfileData [" + IdProfileData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpProfileData> ConvertList(DB_PROFILE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpProfileData> ConvertList(DB_PROFILE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpProfileData> ret = new List<OpProfileData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpProfileData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpProfileData> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
                //Dictionary<int,OpProfile> ProfileDict = dataProvider.GetProfile(list.Select(l => l.ID_PROFILE).Distinct().ToArray()).ToDictionary(l => l.ID_PROFILE);
	            Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
                    //loop.Profile = ProfileDict.TryGetValue(loop.ID_PROFILE); 
		            loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpProfileData> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpProfileData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpProfileData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpProfileData> Members
        public bool Equals(OpProfileData other)
        {
            if (other == null)
				return false;
			return this.IdProfileData.Equals(other.IdProfileData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpProfileData)
				return this.IdProfileData == ((OpProfileData)obj).IdProfileData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpProfileData left, OpProfileData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpProfileData left, OpProfileData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdProfileData.GetHashCode();
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
            //this.Profile = dataProvider.GetProfile(this.ID_PROFILE);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider = null)
        {
            OpProfileData clone = new OpProfileData();
            clone.IdProfileData = this.IdProfileData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.IdProfile = this.IdProfile;

            if (dataProvider != null)
                clone = ConvertList(new DB_PROFILE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        }
    #endregion

    #endregion
    }
#endif
}