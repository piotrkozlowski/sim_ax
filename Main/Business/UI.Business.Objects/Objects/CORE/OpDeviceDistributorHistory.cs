﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceDistributorHistory : DB_DEVICE_DISTRIBUTOR_HISTORY, IComparable, IEquatable<OpDeviceDistributorHistory>
    {
    #region Properties
        public long IdDeviceDistributorHistory { get { return this.ID_DEVICE_DISTRIBUTOR_HISTORY; } set { this.ID_DEVICE_DISTRIBUTOR_HISTORY = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public int? IdDistributorOwner { get { return this.ID_DISTRIBUTOR_OWNER; } set { this.ID_DISTRIBUTOR_OWNER = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public int? IdServiceReferenceType { get { return this.ID_SERVICE_REFERENCE_TYPE; } set { this.ID_SERVICE_REFERENCE_TYPE = value; } }
        public object ReferenceValue { get { return this.REFERENCE_VALUE; } set { this.REFERENCE_VALUE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceDistributorHistory()
            : base() { }

        public OpDeviceDistributorHistory(DB_DEVICE_DISTRIBUTOR_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceDistributorHistory> ConvertList(DB_DEVICE_DISTRIBUTOR_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceDistributorHistory> ConvertList(DB_DEVICE_DISTRIBUTOR_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDeviceDistributorHistory> ret = new List<OpDeviceDistributorHistory>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceDistributorHistory(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceDistributorHistory> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceDistributorHistory> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceDistributorHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceDistributorHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceDistributorHistory> Members
        public bool Equals(OpDeviceDistributorHistory other)
        {
            if (other == null)
                return false;
            return this.IdDeviceDistributorHistory.Equals(other.IdDeviceDistributorHistory);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceDistributorHistory)
                return this.IdDeviceDistributorHistory == ((OpDeviceDistributorHistory)obj).IdDeviceDistributorHistory;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceDistributorHistory left, OpDeviceDistributorHistory right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceDistributorHistory left, OpDeviceDistributorHistory right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceDistributorHistory.GetHashCode();
        }
    #endregion
    }
#endif
}