using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
    public class OpConsumerTypeData : DB_CONSUMER_TYPE_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpConsumerTypeData>
	{
    #region Properties
        [XmlIgnore]
        public long IdConsumerTypeData { get { return this.ID_CONSUMER_TYPE_DATA; } set { this.ID_CONSUMER_TYPE_DATA = value; } }
        [XmlIgnore]
        public int IdConsumerType { get { return this.ID_CONSUMER_TYPE; } set { this.ID_CONSUMER_TYPE = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpConsumerType _ConsumerType;
        [XmlIgnore]
        public OpConsumerType ConsumerType { get { return this._ConsumerType; } set { this._ConsumerType = value; this.ID_CONSUMER_TYPE = (value == null) ? 0 : (int)value.ID_CONSUMER_TYPE; } }
		private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdConsumerTypeData; } set { IdConsumerTypeData = value; } }
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpConsumerTypeData()
			: base() { this.OpState = OpChangeState.New; }

		public OpConsumerTypeData(DB_CONSUMER_TYPE_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "ConsumerTypeData [" + IdConsumerTypeData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpConsumerTypeData> ConvertList(DB_CONSUMER_TYPE_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpConsumerTypeData> ConvertList(DB_CONSUMER_TYPE_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpConsumerTypeData> ret = new List<OpConsumerTypeData>(list.Length);
			foreach (var loop in list)
			{
				OpConsumerTypeData insert = new OpConsumerTypeData(loop);
				if (loadNavigationProperties)
				{
                    //insert.ConsumerType = dataProvider.GetConsumerType(loop.ID_CONSUMER_TYPE);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerTypeData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.ConsumerType = dataProvider.GetConsumerType(this.ID_CONSUMER_TYPE);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpConsumerTypeData clone = new OpConsumerTypeData();
            clone.IdData = this.IdData;
            clone.IdConsumerType = this.IdConsumerType;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_CONSUMER_TYPE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpConsumerTypeData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpConsumerTypeData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IEquatable<OpConsumerTypeData> Members
		public bool Equals(OpConsumerTypeData other)
		{
			if (other == null)
				return false;
			return this.IdConsumerTypeData.Equals(other.IdConsumerTypeData);
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerTypeData)
				return this.IdData == ((OpConsumerTypeData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerTypeData left, OpConsumerTypeData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerTypeData left, OpConsumerTypeData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}