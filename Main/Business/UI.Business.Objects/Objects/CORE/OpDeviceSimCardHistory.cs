using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDeviceSimCardHistory : DB_DEVICE_SIM_CARD_HISTORY, IComparable, IEquatable<OpDeviceSimCardHistory>
    {
    #region Properties
        public long IdDeviceSimCardHistory { get { return this.ID_DEVICE_SIM_CARD_HISTORY;} set {this.ID_DEVICE_SIM_CARD_HISTORY = value;}}
        public long SerialNbr { get { return this.SERIAL_NBR;} set {this.SERIAL_NBR = value;}}
        public int? IdSimCard { get { return this.ID_SIM_CARD;} set {this.ID_SIM_CARD = value;}} 
        public DateTime StartTime { get { return this.START_TIME;} set {this.START_TIME = value;}}
        public DateTime? EndTime { get { return this.END_TIME;} set {this.END_TIME = value;}}
        public string Notes { get { return this.NOTES;} set {this.NOTES = value;}}
    #endregion

    #region	Navigation Properties
		private OpSimCard _SimCard;
				public OpSimCard SimCard { get { return this._SimCard; } set { this._SimCard = value; this.ID_SIM_CARD = (value == null)? 0 : (int)value.ID_SIM_CARD; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDeviceSimCardHistory()
			:base() {}
		
		public OpDeviceSimCardHistory(DB_DEVICE_SIM_CARD_HISTORY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NOTES;
		}
    #endregion

    #region	ConvertList
		public static List<OpDeviceSimCardHistory> ConvertList(DB_DEVICE_SIM_CARD_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDeviceSimCardHistory> ConvertList(DB_DEVICE_SIM_CARD_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceSimCardHistory> ret = new List<OpDeviceSimCardHistory>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceSimCardHistory(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDeviceSimCardHistory> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpSimCard> SimCardDict = dataProvider.GetSimCard(list.Where(l => l.ID_SIM_CARD.HasValue).Select(l => l.ID_SIM_CARD.Value).Distinct().ToArray(), false, loadCustomData: false).ToDictionary(l => l.ID_SIM_CARD);
			
				foreach (var loop in list)
				{
                    if(loop.ID_SIM_CARD.HasValue)
					    loop.SimCard = SimCardDict.TryGetValue(loop.ID_SIM_CARD.Value); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceSimCardHistory> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceSimCardHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceSimCardHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDeviceSimCardHistory> Members
        public bool Equals(OpDeviceSimCardHistory other)
        {
            if (other == null)
				return false;
			return this.IdDeviceSimCardHistory.Equals(other.IdDeviceSimCardHistory);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDeviceSimCardHistory)
				return this.IdDeviceSimCardHistory == ((OpDeviceSimCardHistory)obj).IdDeviceSimCardHistory;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceSimCardHistory left, OpDeviceSimCardHistory right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceSimCardHistory left, OpDeviceSimCardHistory right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDeviceSimCardHistory.GetHashCode();
		}
    #endregion
    }
#endif
}