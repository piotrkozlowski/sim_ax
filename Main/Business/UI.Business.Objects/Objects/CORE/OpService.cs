﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpService : DB_SERVICE, IComparable, IEquatable<OpService>
    {
    #region Properties
        public int IdService { get { return this.ID_SERVICE; } set { this.ID_SERVICE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int? IdDeviceOrderNumber { get { return this.ID_DEVICE_ORDER_NUMBER; } set { this.ID_DEVICE_ORDER_NUMBER = value; } }
        public DateTime? DeleteDate { get { return this.DELETE_DATE; } set { this.DELETE_DATE = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceOrderNumber _DeviceOrderNumber;
        public OpDeviceOrderNumber DeviceOrderNumber { get { return this._DeviceOrderNumber; } set { this._DeviceOrderNumber = value; this.ID_DEVICE_ORDER_NUMBER = (value == null) ? 0 : (int)value.ID_DEVICE_ORDER_NUMBER; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        public List<OpDiagnosticAction> DiagnosticActions { get; private set; }
        public List<OpDescr> AllLanguagesDescription { get; set; }
        public OpDataList<OpServiceData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpService()
            : base() 
        {
            DataList = new OpDataList<OpServiceData>();
        }

        public OpService(DB_SERVICE clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpServiceData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpService> ConvertList(DB_SERVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpService> ConvertList(DB_SERVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpService> ret = new List<OpService>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpService(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpService> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceOrderNumber> DeviceOrderNumberDict = dataProvider.GetDeviceOrderNumber(list.Where(t => t.IdDeviceOrderNumber != null).Select(l => (int)l.ID_DEVICE_ORDER_NUMBER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_ORDER_NUMBER);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);			

                foreach (var loop in list)
                {
                    if(loop.ID_DEVICE_ORDER_NUMBER != null)
                        loop.DeviceOrderNumber = DeviceOrderNumberDict.TryGetValue((int)loop.ID_DEVICE_ORDER_NUMBER);
                    loop.DiagnosticActions = dataProvider.GetDiagnosticActionInServiceFilter(IdService: new int[]{ loop.IdService }).Select(t => t.DiagnosticAction).ToList();
                    if(loop.ID_DESCR != null)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpService> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IServiceDataTypes != null)
            {
                List<int> idService = list.Select(l => l.IdService).ToList();

                if (dataProvider.IServiceDataTypes.Count > 0)
                {
                    List<OpServiceData> data = dataProvider.GetServiceDataFilter(IdService: idService.ToArray(), IdDataType: dataProvider.IServiceDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpService> serviceDataDict = list.ToDictionary<OpService, int>(l => l.IdService);
                        foreach (var dataValue in data)
                        {
                            serviceDataDict[dataValue.IdService].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpService)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpService).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpService> Members
        public bool Equals(OpService other)
        {
            if (other == null)
                return false;
            return this.IdService.Equals(other.IdService);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpService)
                return this.IdService == ((OpService)obj).IdService;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpService left, OpService right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpService left, OpService right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdService.GetHashCode();
        }
    #endregion

    #region Copy
        public OpService Copy()
        {
            OpService copyService = new OpService();

            copyService.IdService = this.IdService;
            copyService.IdDeviceOrderNumber = this.IdDeviceOrderNumber;
            copyService.Name = this.Name;
            copyService.DeviceOrderNumber = this.DeviceOrderNumber;
            copyService.DeleteDate = this.DeleteDate;

            return copyService;
        }
    #endregion
    }
#endif
}