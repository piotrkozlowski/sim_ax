using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerSettlementErrorType : DB_CONSUMER_SETTLEMENT_ERROR_TYPE, IComparable, IEquatable<OpConsumerSettlementErrorType>
    {
    #region Properties
        public int IdConsumerSettlementErrorType { get { return this.ID_CONSUMER_SETTLEMENT_ERROR_TYPE;} set {this.ID_CONSUMER_SETTLEMENT_ERROR_TYPE = value;}}
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerSettlementErrorType()
			:base() {}
		
		public OpConsumerSettlementErrorType(DB_CONSUMER_SETTLEMENT_ERROR_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerSettlementErrorType> ConvertList(DB_CONSUMER_SETTLEMENT_ERROR_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerSettlementErrorType> ConvertList(DB_CONSUMER_SETTLEMENT_ERROR_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpConsumerSettlementErrorType> ret = new List<OpConsumerSettlementErrorType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerSettlementErrorType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerSettlementErrorType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerSettlementErrorType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerSettlementErrorType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerSettlementErrorType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerSettlementErrorType> Members
        public bool Equals(OpConsumerSettlementErrorType other)
        {
            if (other == null)
				return false;
			return this.IdConsumerSettlementErrorType.Equals(other.IdConsumerSettlementErrorType);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerSettlementErrorType)
				return this.IdConsumerSettlementErrorType == ((OpConsumerSettlementErrorType)obj).IdConsumerSettlementErrorType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerSettlementErrorType left, OpConsumerSettlementErrorType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerSettlementErrorType left, OpConsumerSettlementErrorType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerSettlementErrorType.GetHashCode();
		}
    #endregion
    }
#endif
}