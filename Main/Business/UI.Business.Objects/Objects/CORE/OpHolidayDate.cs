﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpHolidayDate : DB_HOLIDAY_DATE, IComparable, IEquatable<OpHolidayDate>
    {
    #region Properties
        public DateTime Date { get { return this.DATE; } set { this.DATE = value; } }
        public string CountryCode { get { return this.COUNTRY_CODE; } set { this.COUNTRY_CODE = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        private string _CountryName = null;
        public string CountryName { get { return _CountryName; } }

       // public string CountryName { get { return GetCountryName(CountryCode); } }
    #endregion

    #region	Ctor
        public OpHolidayDate()
            : base() {
                _CountryName = GetCountryName(CountryCode);
        }

        public OpHolidayDate(DB_HOLIDAY_DATE clone)
            : base(clone) {
                _CountryName = GetCountryName(CountryCode);
        }
    #endregion

    #region	ToString
        public override string ToString() 
        {
            if (!String.IsNullOrWhiteSpace(CountryName))
                return String.Format("{0} {1}", CountryCode, CountryName);
            else
                return this.COUNTRY_CODE;
        }
    #endregion

    #region GetCountryName
        public string GetCountryName(string CCode) 
        {
            string name = "";
            CCode = CCode.Trim();
            switch(CCode)
            {
                case "1":
                    name = ResourcesText.UnitedStates;
                break;
                case "44":
                    name = ResourcesText.UnitedKnigdom;
                break;
                case "48":
                    name = ResourcesText.Poland;
                break;
                default:
                    name = CCode;
                break;

            }
            return name;
        }
    #endregion

    #region	ConvertList
        public static List<OpHolidayDate> ConvertList(DB_HOLIDAY_DATE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpHolidayDate> ConvertList(DB_HOLIDAY_DATE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpHolidayDate> ret = new List<OpHolidayDate>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpHolidayDate(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpHolidayDate> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpHolidayDate> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpHolidayDate)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpHolidayDate).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpHolidayDate> Members
        public bool Equals(OpHolidayDate other)
        {
            if (other == null)
                return false;
            return this.Date.Equals(other.Date);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpHolidayDate)
                return this.Date == ((OpHolidayDate)obj).Date;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpHolidayDate left, OpHolidayDate right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpHolidayDate left, OpHolidayDate right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return Date.GetHashCode();
        }
    #endregion
    }
#endif
}