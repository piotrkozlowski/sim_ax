using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Drawing;
using System.Data;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpIssue : DB_ISSUE, IComparable, IEquatable<OpIssue>, IOpChangeState, IOpDynamicProperty<OpIssue>, IReferenceType, IOpDynamic, IOpObject<OpIssueData>
    {
    #region Properties
        [XmlIgnore]
        public int IdIssue
        {
            get { return this.ID_ISSUE; }
            set
            {
                if (this.ID_ISSUE != value)
                {
                    this.ID_ISSUE = value;
                    this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public int IdActor
        {
            get { return this.ID_ACTOR; }
            set
            {
                if (this.ID_ACTOR != value)
                {
                    this.ID_ACTOR = value;
                    this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public int IdOperatorRegistering
        {
            get { return this.ID_OPERATOR_REGISTERING; }
            set
            {
                if (this.ID_OPERATOR_REGISTERING != value)
                {
                    this.ID_OPERATOR_REGISTERING = value; 
                    this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public int? IdOperatorPerformer
        {
            get { return this.ID_OPERATOR_PERFORMER; }
            set
            {
                if (this.ID_OPERATOR_PERFORMER != value)
                {
                    this.ID_OPERATOR_PERFORMER = value; 
                    this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public int IdIssueType
        {
            get { return this.ID_ISSUE_TYPE; }
            set
            {
                if (this.ID_ISSUE_TYPE != value)
                {
                    this.ID_ISSUE_TYPE = value; 
                    this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public int IdIssueStatus
        {
            get { return this.ID_ISSUE_STATUS; }
            set
            {
                if (this.ID_ISSUE_STATUS != value)
                {
                    this.IdPrevoiusIssueStatus = this.ID_ISSUE_STATUS;
                    this.ID_ISSUE_STATUS = value; 
                    this.OpState = OpChangeState.Modified;
                }
            }
        }
        public int IdPrevoiusIssueStatus
        {
            get;
            set;
        }
        [XmlIgnore]
        public DateTime? RealizationDate
        {
            get { return this.REALIZATION_DATE; }
            set { if (this.REALIZATION_DATE != value) { this.REALIZATION_DATE = value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public DateTime CreationDate
        {
            get { return this.CREATION_DATE; }
            set { if (this.CREATION_DATE != value) { this.CREATION_DATE = value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public string ShortDescr
        {
            get { return this.SHORT_DESCR; }
            set
            {
                if (this.SHORT_DESCR != value)
                {
                    this.SHORT_DESCR = value; this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public string LongDescr
        {
            get { return this.LONG_DESCR; }
            set
            {
                if (this.LONG_DESCR != value)
                {
                    this.LONG_DESCR = value; this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public long? IdLocation
        {
            get { return this.ID_LOCATION; }
            set
            {
                if (this.ID_LOCATION != value)
                {
                    this.ID_LOCATION = value; this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public DateTime? Deadline
        {
            get 
            {
                if (this.DEADLINE != null)
                {
                    if (this.IssueHistory != null)
                    {
                        if (this.IssueHistory.Exists(th => th.IdIssueStatus == (int)OpIssueStatus.Enum.SUSPENDED))
                        {
                            DateTime tmp = new DateTime(DEADLINE.Value.Year, DEADLINE.Value.Month, DEADLINE.Value.Day, DEADLINE.Value.Hour, DEADLINE.Value.Minute, 0);
                            TimeSpan ts;
                            foreach (OpIssueHistory thItem in this.IssueHistory.Where(th => th.IdIssueStatus == (int)OpIssueStatus.Enum.SUSPENDED))
                            {
                                if (thItem.EndDate.HasValue)
                                    ts = thItem.END_DATE.Value - thItem.START_DATE;
                                else
                                    ts = DataProvider.DateTimeNow - thItem.START_DATE;
                                tmp = tmp.AddDays(ts.Days);
                                tmp = tmp.AddHours(ts.Hours);
                                tmp = tmp.AddMinutes(ts.Minutes);
                            }
                            return tmp;
                        }
                        else
                            return this.DEADLINE;
                    }
                    else
                        return this.DEADLINE;
                }
                else
                    return null;
            }
            set
            {
                if (this.DEADLINE != ((value == null) ? null : (DateTime?)value.Value))
                {
                    this.DEADLINE = value; this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public int IdPriority
        {
            get { return this.ID_PRIORITY; }
            set
            {
                if (this.ID_PRIORITY != value)
                {
                    this.ID_PRIORITY = value; this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public object RowVersion
        {
            get { return this.ROW_VERSION; }
            set
            {
                if (this.ROW_VERSION != value)
                {
                    this.ROW_VERSION = value; this.OpState = OpChangeState.Modified;
                }
            }
        }
        [XmlIgnore]
        public int IdDistributor
        {
            get { return this.ID_DISTRIBUTOR; }
            set
            {
                if (this.ID_DISTRIBUTOR != value)
                {
                    this.ID_DISTRIBUTOR = value; this.OpState = OpChangeState.Modified;
                }
            }
        }

        [DataMember]
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpActor _Actor;
        [CopyableProperty, XmlIgnore]
        public OpActor Actor { get { return this._Actor; } set { this._Actor = value; this.IdActor = (value == null) ? 0 : (int)value.ID_ACTOR; } }
        private OpIssueType _IssueType;
        [XmlIgnore]
        public OpIssueType IssueType { get { return this._IssueType; } set { this._IssueType = value; this.IdIssueType = (value == null) ? 0 : (int)value.ID_ISSUE_TYPE; } }
        private OpIssueStatus _IssueStatus;
        [XmlIgnore]
        public OpIssueStatus IssueStatus
        {
            get { return this._IssueStatus; }
            set
            {
                if (value != null && this._IssueStatus != value)
                    IssueStatusChanged = true;

                this._IssueStatus = value; this.IdIssueStatus = (value == null) ? 0 : (int)value.ID_ISSUE_STATUS;
            }
        }
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.IdDistributor = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpLocation _Location;
        [CopyableProperty, XmlIgnore]
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.IdLocation = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpPriority _Priority;
        [XmlIgnore]
        public OpPriority Priority { get { return this._Priority; } set { this._Priority = value; this.IdPriority = (value == null) ? 0 : (int)value.ID_PRIORITY; } }
        private OpOperator _OperatorRegistering;
        [CopyableProperty, XmlIgnore]
        public OpOperator OperatorRegistering { get { return this._OperatorRegistering; } set { this._OperatorRegistering = value; this.IdOperatorRegistering = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorPerformer;
        [CopyableProperty, XmlIgnore]
        public OpOperator OperatorPerformer { get { return this._OperatorPerformer; } set { this._OperatorPerformer = value; this.IdOperatorPerformer = (value == null) ? null : (int?)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
        //Indicates if the TaskStatus change from the last db query
        [DataMember]
        public bool IssueStatusChanged { get; set; }
        [XmlIgnore]
        public string GridLookUpEditString
        {
            get
            {
                StringBuilder strBuilder = new StringBuilder();
                strBuilder.AppendFormat("[{0}] ", this.IdIssue);
                strBuilder.AppendFormat("{0}, ", this.IssueType);
                if (this.Location != null)
                    strBuilder.AppendFormat("{0} ", this.Location);
                return strBuilder.ToString();
            }
        }
        [XmlIgnore]
        public int? IdWorkOrderJCI
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.ISSUE_WORK_ORDER_JCI_ID);
            }
            set
            {
                DataList.SetValue(DataType.ISSUE_WORK_ORDER_JCI_ID, value);
            }
        }
        [XmlIgnore]
        public bool IMRMySCVisible
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.ISSUE_IMRMYSC_VISIBLE);
            }
            set
            {
                DataList.SetValue(DataType.ISSUE_IMRMYSC_VISIBLE, value);
            }
        }
        [DataMember]
        private OpWorkOrderJci _WorkOrderJCI;
        [XmlIgnore]
        public OpWorkOrderJci WorkOrderJCI
        {
            get { return _WorkOrderJCI; }
            set
            {
                _WorkOrderJCI = value;
                if (value != null)
                    IdWorkOrderJCI = value.IdWorkOrderJci;
                else
                    IdWorkOrderJCI = null;
            }
        }
        [XmlIgnore]
        public string JCI { get { return this.IdWorkOrderJCI.HasValue && this.IdWorkOrderJCI != 0 ? "JCI" : null; } }
        [XmlIgnore]
        public string ShortDescrCollapsed
        {
            get
            {
                if (ShortDescr != null && ShortDescr.Length > 50)
                    return String.Format("{0}...", ShortDescr.Substring(0, 50));
                else
                    return ShortDescr;
            }
        }
        [DataMember]
        public List<OpIssueHistory> IssueHistory { get; set; }
    #region Overdue
        [XmlIgnore]
        public int? Overdue
        {
            get
            {
                int? overdue = null;
                if (this.IssueHistory != null && this.IssueHistory.Count > 0 && this.Deadline.HasValue)
                {
                    OpIssueHistory lastEntry = this.IssueHistory.Find(h => !h.EndDate.HasValue);
                    OpIssueHistory firstEntry = this.IssueHistory.First();

                    if (lastEntry != null)
                    {
                        TimeSpan completeTaskTime = this.DEADLINE.Value - firstEntry.StartDate;
                        if (lastEntry.IdIssueStatus == (int)OpIssueStatus.Enum.FINISHED ||
                            lastEntry.IdIssueStatus == (int)OpIssueStatus.Enum.SUSPENDED ||
                            lastEntry.IdIssueStatus == (int)OpIssueStatus.Enum.CANCELLED ||
                            lastEntry.IdIssueStatus == (int)OpIssueStatus.Enum.WAITING_FOR_RESPONSE)
                        {
                            //overdue = (lastEntry.StartDate - task.Deadline.Value).Days;
                            overdue = (lastEntry.StartDate - firstEntry.StartDate).Days;
                        }
                        else
                        //if (this.IdIssueStatus == (int)OpIssueStatus.Enum.ACCEPTED ||
                        //    this.IdIssueStatus == (int)OpIssueStatus.Enum.QUEUED ||
                        //    this.IdIssueStatus == (int)OpIssueStatus.Enum.IN_PROGRESS ||
                        //    this.IdIssueStatus == (int)OpIssueStatus.Enum.REOPENED)
                        {
                            //overdue = (DataProvider.DateTimeNow - task.Deadline.Value).Days;//Deadline w getterze i setterze jest konwertowany do czasu lokalnego
                            overdue = (DataProvider.DateTimeNow - firstEntry.StartDate).Days;
                        }
                        if (overdue.HasValue)
                        {
                            List<OpIssueHistory> toSkip = this.IssueHistory.Where(h => h.EndDate.HasValue &&
                                                                                 (h.IdIssueStatus == (int)OpIssueStatus.Enum.FINISHED ||
                                                                                 h.IdIssueStatus == (int)OpIssueStatus.Enum.CANCELLED ||
                                                                                 h.IdIssueStatus == (int)OpIssueStatus.Enum.SUSPENDED ||
                                                                                 h.IdIssueStatus == (int)OpIssueStatus.Enum.WAITING_FOR_RESPONSE))
                                                                           .ToList();
                            TimeSpan tsToSkip;
                            int toSkipDays = 0;
                            int toSkipHours = 0;
                            //odliczamy wszystkie stany kt�re mo�na pomin��
                            foreach (OpIssueHistory thItem in toSkip)
                            {
                                tsToSkip = thItem.EndDate.Value - thItem.StartDate;
                                toSkipDays += tsToSkip.Days;
                                toSkipHours += tsToSkip.Hours;
                            }
                            //odliczamy czas na wykonanie zadania
                            toSkipDays += completeTaskTime.Days;
                            toSkipHours += completeTaskTime.Hours;
                            if (toSkipDays != 0)
                            {
                                overdue -= toSkipDays + (int)(toSkipHours / 24) + (toSkipHours % 24 > 0 ? 1 : 0);//zaokr�glamy dni w g�re
                            }
                        }
                    }
                }

                return overdue;
            }
        }
        public Color OverdueColor
        {
            get
            {
                if (Overdue.HasValue)
                {
                    if (Overdue > 0)
                        return Color.Red;
                    else
                        return Color.Yellow;
                }
                else
                    return Color.Transparent;
            }
        }
    #endregion
        [XmlIgnore]
        public int? SuspendedDaysCnt
        {
            get
            {
                int? suspendedDaysCnt = null;
                if (this.IssueHistory != null && this.IssueHistory.Count > 0)
                {
                    suspendedDaysCnt = 0;
                    int suspendedHoursCnt = 0;
                    foreach (OpIssueHistory ihItem in this.IssueHistory)
                    {
                        if (ihItem.IdIssueStatus == (int)OpIssueStatus.Enum.SUSPENDED)
                        {
                            DateTime start = ihItem.StartDate;
                            DateTime end = ihItem.EndDate.HasValue ? ihItem.EndDate.Value : DataProvider.DateTimeNow;
                            suspendedDaysCnt += (int)(end - start).TotalDays;
                            suspendedHoursCnt += (int)(end - start).Hours;
                        }
                    }
                    if (suspendedHoursCnt > 0)
                    {
                        suspendedDaysCnt += (int)Math.Ceiling(((double)suspendedHoursCnt / 24.0));
                    }
                }

                return suspendedDaysCnt;
            }
        }
        [XmlIgnore]
        public int? IdReport
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.ISSUE_REPORT_ID);
            }
            set
            {
                DataList.SetValue(DataType.ISSUE_REPORT_ID, value);
            }
        }
        [XmlIgnore]
        public int? IdTransmissionDriver
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.ISSUE_TRANSMISSION_DRIVER_ID);
            }
            set
            {
                DataList.SetValue(DataType.ISSUE_TRANSMISSION_DRIVER_ID, value);
            }
        }
        [XmlIgnore]
        public int? IdImrServer
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.ISSUE_SERVER_ID);
            }
            set
            {
                DataList.SetValue(DataType.ISSUE_SERVER_ID, value);
            }
        }

        [DataMember]
        public List<OpTask> TaskList;// { get; set; }
        
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;
        [DataMember]
        private Dictionary<string, object> dynamicValues;
        #endregion
        #region Private Members
        private static IDataProvider DataProvider = null;
    #endregion

    #region	Ctor
        public OpIssue()
            : base()
        {
            DataList = new OpDataList<OpIssueData>();
            this.OpState = OpChangeState.New;
            dynamicProperties = new OpDynamicPropertyDict();
            TaskList = new List<OpTask>();
            IssueHistory = new List<OpIssueHistory>();
        }

        public OpIssue(DB_ISSUE clone)
            : base(clone)
        {
            DataList = new OpDataList<OpIssueData>();
            this.OpState = OpChangeState.Loaded;
            dynamicProperties = new OpDynamicPropertyDict();
            TaskList = new List<OpTask>();
            IssueHistory = new List<OpIssueHistory>();
        }

        public OpIssue(OpIssue clone)
            : base(clone)
        {
            DataList = new OpDataList<OpIssueData>();
            this.OpState = clone.OpState;
            dynamicProperties = new OpDynamicPropertyDict();
            TaskList = new List<OpTask>();
            IssueHistory = new List<OpIssueHistory>();
        }

    #endregion

    #region Clone
        public OpIssue Clone(IDataProvider dataProvider)
        {
            OpIssue newIssue = new OpIssue(this);
            //properties which have to be reset
            newIssue.IdIssue = 0;
            newIssue.CreationDate = dataProvider.DateTimeNow;
            newIssue.RealizationDate = null;
            newIssue.DataList = this.DataList;
            newIssue.IdIssueStatus = (int)OpIssueStatus.Enum.NEW;
            newIssue.Deadline = null;
            newIssue = ConvertList(new DB_ISSUE[1] { newIssue }, dataProvider)[0];
            newIssue.OpState = OpChangeState.New;

            newIssue.DataList = DataList.Clone(dataProvider);
            foreach (OpIssueData issueData in newIssue.DataList)
            {
                issueData.OpState = OpChangeState.New;
                issueData.IdData = 0;
            }

            return newIssue;
        }
    #endregion

    #region	override ToString,  Equals, GetHashCode
        public override string ToString()
        {
            return String.Format("[{0}] {1}", this.IdIssue, this.ShortDescr);
        }
        public override bool Equals(object obj)
        {
            if (obj is OpIssue)
                return this.IdIssue == ((OpIssue)obj).IdIssue;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpIssue left, OpIssue right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpIssue left, OpIssue right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }
        public override int GetHashCode()
        {
            return this.IdIssue.GetHashCode();
        }
    #endregion

    #region	ConvertList
        public static List<OpIssue> ConvertList(DB_ISSUE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true, false, true);
        }

        public static List<OpIssue> ConvertList(DB_ISSUE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadIssueHistory, bool loadCustomData, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            DataProvider = dataProvider;

            List<OpIssue> ret = new List<OpIssue>(list.Length);
            list.ToList().ForEach(db_issue => ret.Add(new OpIssue(db_issue)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data

            if (loadIssueHistory)
                LoadIssueHistory(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpIssue> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpActor> ActorDict = dataProvider.GetActor(list.Select(l => l.ID_ACTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTOR);
                Dictionary<int, OpIssueType> IssueTypeDict = dataProvider.GetIssueType(list.Select(l => l.ID_ISSUE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ISSUE_TYPE);
                Dictionary<int, OpIssueStatus> IssueStatusDict = dataProvider.GetIssueStatus(list.Select(l => l.ID_ISSUE_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ISSUE_STATUS);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpPriority> PriorityDict = dataProvider.GetPriority(list.Select(l => l.ID_PRIORITY).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PRIORITY);
                Dictionary<int, OpOperator> OperatorPerformerDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR_PERFORMER.HasValue).Select(l => l.ID_OPERATOR_PERFORMER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorRegisteringDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_REGISTERING).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.Actor = ActorDict.TryGetValue(loop.ID_ACTOR);
                    loop.IssueType = IssueTypeDict.TryGetValue(loop.ID_ISSUE_TYPE);
                    loop._IssueStatus = IssueStatusDict.TryGetValue(loop.ID_ISSUE_STATUS);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.Priority = PriorityDict.TryGetValue(loop.ID_PRIORITY);
                    if (loop.ID_OPERATOR_PERFORMER.HasValue)
                        loop.OperatorPerformer = OperatorPerformerDict.TryGetValue(loop.ID_OPERATOR_PERFORMER.Value);
                    loop.OperatorRegistering = OperatorRegisteringDict.TryGetValue(loop.ID_OPERATOR_REGISTERING);
                }
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpIssue> list, IDataProvider dataProvider, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && DataProvider.IIssueDataTypes != null)
            {
                List<int> idIssue = list.Select(l => l.IdIssue).ToList();
                Dictionary<int, List<OpIssueData>> data = new Dictionary<int, List<OpIssueData>>();

                //List<OpIssueData> data = dataProvider.GetIssueDataFilter(IdIssue: idIssue.ToArray(), IdDataType: DataProvider.IssueDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetIssueDataFilter(IdIssue: idIssue.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout)
                        .GroupBy(x => x.ID_ISSUE)
                        .ToDictionary(x => x.Key, x => x.ToList());
                else if (dataProvider.IIssueDataTypes != null && dataProvider.IIssueDataTypes.Count > 0)
                    data = dataProvider.GetIssueDataFilter(IdIssue: idIssue.ToArray(), IdDataType: DataProvider.IIssueDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout)
                        .GroupBy(x => x.ID_ISSUE)
                        .ToDictionary(x => x.Key, x => x.ToList());


                if (data != null && data.Count > 0)
                {
                    foreach (var issue in list)
                    {
                        //issue.DataList.AddRange(data.Where(d => d.IdIssue == issue.IdIssue));
                        if (data.ContainsKey(issue.ID_ISSUE))
                        {
                            issue.DataList.AddRange(data[issue.ID_ISSUE]);
                        }
                    }
                }

                Dictionary<int, OpWorkOrderJci> WorkOrderJciDict = dataProvider.GetWorkOrderJci(list.Where(l => l.IdWorkOrderJCI.HasValue).Select(l => l.IdWorkOrderJCI.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_WORK_ORDER_JCI);
                foreach (var loop in list)
                {
                    if (loop.IdWorkOrderJCI.HasValue)
                        loop.WorkOrderJCI = WorkOrderJciDict.TryGetValue(loop.IdWorkOrderJCI.Value);
                }
            }
        }
    #endregion

    #region LoadIssueHistory
        public static void LoadIssueHistory(ref List<OpIssue> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> idIssue = list.Select(l => l.IdIssue).ToList();

                List<OpIssueHistory> history = dataProvider.GetIssueHistoryFilter(IdIssue: idIssue.ToArray(), loadNavigationProperties: false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (history != null && history.Count > 0)
                {
                    list.ForEach(l => l.IssueHistory = new List<OpIssueHistory>());
                    Dictionary<int, OpIssue> tasksDict = list.ToDictionary<OpIssue, int>(l => l.IdIssue);

                    foreach (var historyValue in history)
                    {
                        tasksDict[historyValue.IdIssue].IssueHistory.Add(historyValue);
                    }
                    foreach (OpIssue iItem in list)
                    {
                        if (iItem.IssueHistory != null)
                            iItem.IssueHistory = iItem.IssueHistory.OrderBy(t => t.StartDate).ToList();
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpIssue)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpIssue).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpIssue> Members

        public bool Equals(OpIssue other)
        {
            if (other != null)
                return this.IdIssue.Equals(other.IdIssue);
            else
                return false;
        }

        #endregion
        #region IOpDynamicProperty<OpLocation> Members
        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpIssue IOpDynamicProperty<OpIssue>.Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdIssue;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion

        #region Implementation of IOpDynamic 
        [XmlIgnore]
        public object Key
        {
            get
            {
                return this.IdIssue;
            }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get
            {
                return this;
            }
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdIssue; }
            set { IdIssue = Convert.ToInt32(value); }
        }

        [DataMember]
        public OpDataList<OpIssueData> DataList { get; set; }
        #endregion
    }
#endif
}