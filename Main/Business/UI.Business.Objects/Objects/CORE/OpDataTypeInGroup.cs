using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataTypeInGroup : DB_DATA_TYPE_IN_GROUP, IComparable, IOpChangeState
    {
    #region Properties
        public int IdDataTypeGroup { get { return this.ID_DATA_TYPE_GROUP; } set { if (this.ID_DATA_TYPE_GROUP != value) { this.ID_DATA_TYPE_GROUP = value; this.OpState = OpChangeState.Modified; } } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { if (this.ID_DATA_TYPE != value) { this.ID_DATA_TYPE = value; this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpDataTypeGroup _DataTypeGroup;
        public OpDataTypeGroup DataTypeGroup { get { return this._DataTypeGroup; } set { this._DataTypeGroup = value; this.ID_DATA_TYPE_GROUP = (value == null) ? 0 : (int)value.ID_DATA_TYPE_GROUP; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpDataTypeInGroup()
            : base() { OpState = OpChangeState.New; }

        public OpDataTypeInGroup(DB_DATA_TYPE_IN_GROUP clone)
            : base(clone) { OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DataTypeInGroup [" + IdDataTypeGroup.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDataTypeInGroup> ConvertList(DB_DATA_TYPE_IN_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataTypeInGroup> ConvertList(DB_DATA_TYPE_IN_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDataTypeInGroup> ret = new List<OpDataTypeInGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataTypeInGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataTypeInGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpDataTypeGroup> DataTypeGroupDict = dataProvider.GetDataTypeGroup(list.Select(l => l.ID_DATA_TYPE_GROUP).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE_GROUP);
                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    loop.DataTypeGroup = DataTypeGroupDict.TryGetValue(loop.ID_DATA_TYPE_GROUP);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataTypeInGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataTypeInGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataTypeInGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}