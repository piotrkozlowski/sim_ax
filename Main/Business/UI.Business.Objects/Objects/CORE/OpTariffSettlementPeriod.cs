using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpTariffSettlementPeriod : DB_TARIFF_SETTLEMENT_PERIOD, IComparable, IEquatable<OpTariffSettlementPeriod>
    {
    #region Properties
        public int IdTariffSettlementPeriod { get { return this.ID_TARIFF_SETTLEMENT_PERIOD;} set {this.ID_TARIFF_SETTLEMENT_PERIOD = value;}}
        public int IdTariff { get { return this.ID_TARIFF;} set {this.ID_TARIFF = value;}}
        public DateTime StartTime { get { return this.START_TIME;} set {this.START_TIME = value;}}
        public DateTime EndTime { get { return this.END_TIME;} set {this.END_TIME = value;}}
    #endregion

    #region	Navigation Properties
		private OpTariff _Tariff;
				public OpTariff Tariff { get { return this._Tariff; } set { this._Tariff = value; this.ID_TARIFF = (value == null)? 0 : (int)value.ID_TARIFF; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpTariffSettlementPeriod()
			:base() {}
		
		public OpTariffSettlementPeriod(DB_TARIFF_SETTLEMENT_PERIOD clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "TariffSettlementPeriod [" + IdTariffSettlementPeriod.ToString() + "]";
		}
    #endregion

    #region	ConvertList
        public static List<OpTariffSettlementPeriod> ConvertList(DB_TARIFF_SETTLEMENT_PERIOD[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTariffSettlementPeriod> ConvertList(DB_TARIFF_SETTLEMENT_PERIOD[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTariffSettlementPeriod> ret = new List<OpTariffSettlementPeriod>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTariffSettlementPeriod(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion
		
    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTariffSettlementPeriod> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpTariff> TariffDict = dataProvider.GetTariff(list.Select(l => l.ID_TARIFF).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_TARIFF);

                foreach (var loop in list)
                {
                    loop.Tariff = TariffDict.TryGetValue(loop.ID_TARIFF);
                }
            }
        }
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTariffSettlementPeriod> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTariffSettlementPeriod)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTariffSettlementPeriod).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpTariffSettlementPeriod> Members
        public bool Equals(OpTariffSettlementPeriod other)
        {
            if (other == null)
				return false;
			return this.IdTariffSettlementPeriod.Equals(other.IdTariffSettlementPeriod);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpTariffSettlementPeriod)
				return this.IdTariffSettlementPeriod == ((OpTariffSettlementPeriod)obj).IdTariffSettlementPeriod;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpTariffSettlementPeriod left, OpTariffSettlementPeriod right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpTariffSettlementPeriod left, OpTariffSettlementPeriod right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdTariffSettlementPeriod.GetHashCode();
		}
    #endregion
    }
#endif
}