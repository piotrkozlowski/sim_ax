﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable]
    public class OpVersionElementTypeData : DB_VERSION_ELEMENT_TYPE_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpVersionElementTypeData>
    {
        #region Properties
        public long IdVersionElementTypeData { get { return this.ID_VERSION_ELEMENT_TYPE_DATA; } set { this.ID_VERSION_ELEMENT_TYPE_DATA = value; } }
        public int IdVersionElementType { get { return this.ID_VERSION_ELEMENT_TYPE; } set { this.ID_VERSION_ELEMENT_TYPE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        #endregion

        #region	Navigation Properties
        private OpVersionElementType _VersionElementType;
        public OpVersionElementType VersionElementType { get { return this._VersionElementType; } set { this._VersionElementType = value; this.ID_VERSION_ELEMENT_TYPE = (value == null) ? 0 : (int)value.ID_VERSION_ELEMENT_TYPE; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region	Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdVersionElementTypeData; } set { IdVersionElementTypeData = value; } }
        [DataMember]
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
        #endregion

        #region	Ctor
        public OpVersionElementTypeData()
            : base() { }

        public OpVersionElementTypeData(DB_VERSION_ELEMENT_TYPE_DATA clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "VersionElementTypeData [" + IdVersionElementTypeData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpVersionElementTypeData> ConvertList(DB_VERSION_ELEMENT_TYPE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpVersionElementTypeData> ConvertList(DB_VERSION_ELEMENT_TYPE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpVersionElementTypeData> ret = new List<OpVersionElementTypeData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionElementTypeData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpVersionElementTypeData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpVersionElementType> VersionElementTypeDict = dataProvider.GetVersionElementType(list.Select(l => l.ID_VERSION_ELEMENT_TYPE).Distinct().ToArray(), queryDatabase: false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_ELEMENT_TYPE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), queryDatabase: false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.VersionElementType = VersionElementTypeDict.TryGetValue(loop.ID_VERSION_ELEMENT_TYPE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpVersionElementTypeData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionElementTypeData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionElementTypeData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpVersionElementTypeData> Members
        public bool Equals(OpVersionElementTypeData other)
        {
            if (other == null)
                return false;
            return this.IdVersionElementTypeData.Equals(other.IdVersionElementTypeData);
        }
        #endregion

        #region IOpDataProvider

        #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpVersionElementTypeData clone = new OpVersionElementTypeData();
            clone.IdData = this.IdData;
            clone.IdVersionElementType = this.IdVersionElementType;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_VERSION_ELEMENT_TYPE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
        #endregion

        #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
        #endregion

        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpVersionElementTypeData)
                return this.IdVersionElementTypeData == ((OpVersionElementTypeData)obj).IdVersionElementTypeData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpVersionElementTypeData left, OpVersionElementTypeData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpVersionElementTypeData left, OpVersionElementTypeData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdVersionElementTypeData.GetHashCode();
        }
        #endregion
    }
}