﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDiagnosticAction : DB_DIAGNOSTIC_ACTION, IComparable, IEquatable<OpDiagnosticAction>
    {
    #region Properties
        public int IdDiagnosticAction { get { return this.ID_DIAGNOSTIC_ACTION; } set { this.ID_DIAGNOSTIC_ACTION = value; } }
        public int? IdDeviceOrderNumber { get { return this.ID_DEVICE_ORDER_NUMBER; } set { this.ID_DEVICE_ORDER_NUMBER = value; } }
        public int? IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceOrderNumber _DeviceOrderNumber;
        public OpDeviceOrderNumber DeviceOrderNumber { get { return this._DeviceOrderNumber; } set { this._DeviceOrderNumber = value; this.ID_DEVICE_ORDER_NUMBER = (value == null) ? null : (int?)value.ID_DEVICE_ORDER_NUMBER; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? null : (int?)value.ID_DISTRIBUTOR; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpDiagnosticActionData> DataList { get; set; }

        public List<OpDescr> AllLanguagesDescription { get; set; }

        public bool InvisibleInServiceReport
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.DIAGNOSTIC_ACTION_INVISIBLE_IN_SERVICE_REPORT);
            }
            set
            {
                DataList.SetValue(DataType.DIAGNOSTIC_ACTION_INVISIBLE_IN_SERVICE_REPORT, value);
            }
        }
        public bool ResultEditableAfterService
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.DIAGNOSTIC_ACTION_RESULT_EDITABLE_AFTER_SERVICE);
            }
            set
            {
                DataList.SetValue(DataType.DIAGNOSTIC_ACTION_RESULT_EDITABLE_AFTER_SERVICE, value);
            }
        }
    #endregion

    #region	Ctor
        public OpDiagnosticAction()
            : base()
        {
            DataList = new OpDataList<OpDiagnosticActionData>();
        }

        public OpDiagnosticAction(DB_DIAGNOSTIC_ACTION clone)
            : base(clone)
        {
            DataList = new OpDataList<OpDiagnosticActionData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (this.Descr == null)
                return this.NAME;
            else
                return Descr.ToString();
        }
    #endregion

    #region	ConvertList
        public static List<OpDiagnosticAction> ConvertList(DB_DIAGNOSTIC_ACTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDiagnosticAction> ConvertList(DB_DIAGNOSTIC_ACTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDiagnosticAction> ret = new List<OpDiagnosticAction>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDiagnosticAction(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDiagnosticAction> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceOrderNumber> DeviceOrderNumberDict = dataProvider.GetDeviceOrderNumber(list.Where(l => l.ID_DEVICE_ORDER_NUMBER.HasValue).Select(l => l.ID_DEVICE_ORDER_NUMBER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_ORDER_NUMBER);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Where(l => l.ID_DISTRIBUTOR.HasValue).Select(l => l.ID_DISTRIBUTOR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);

                foreach (var loop in list)
                {
                    if (loop.ID_DEVICE_ORDER_NUMBER.HasValue)
                        loop.DeviceOrderNumber = DeviceOrderNumberDict.TryGetValue(loop.ID_DEVICE_ORDER_NUMBER.Value);
                    if (loop.ID_DISTRIBUTOR.HasValue)
                        loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR.Value);
                    if(loop.ID_DESCR != null)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDiagnosticAction> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IDiagnosticActionDataTypes != null)
            {
                List<int> idDiagnosticAction = list.Select(l => l.IdDiagnosticAction).ToList();

                if (dataProvider.IDiagnosticActionDataTypes.Count > 0)
                {
                    List<OpDiagnosticActionData> data1 = dataProvider.GetDiagnosticActionDataFilter(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    OpDiagnosticActionData data2 = dataProvider.GetDiagnosticActionData(new long[] { 1 }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();
                    List<OpDiagnosticActionData> data = dataProvider.GetDiagnosticActionDataFilter(IdDiagnosticAction: idDiagnosticAction.ToArray(), IdDataType: dataProvider.IDiagnosticActionDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpDiagnosticAction> diagnosticActionDataDict = list.ToDictionary<OpDiagnosticAction, int>(l => l.IdDiagnosticAction);
                        foreach (var dataValue in data)
                        {
                            diagnosticActionDataDict[dataValue.IdDiagnosticAction].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDiagnosticAction)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDiagnosticAction).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDiagnosticAction> Members
        public bool Equals(OpDiagnosticAction other)
        {
            if (other == null)
                return false;
            return this.IdDiagnosticAction.Equals(other.IdDiagnosticAction);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDiagnosticAction)
                return this.IdDiagnosticAction == ((OpDiagnosticAction)obj).IdDiagnosticAction;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDiagnosticAction left, OpDiagnosticAction right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDiagnosticAction left, OpDiagnosticAction right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDiagnosticAction.GetHashCode();
        }
    #endregion

    #region Copy
        public OpDiagnosticAction Copy()
        {
            OpDiagnosticAction copyDiagnosticAction = new OpDiagnosticAction();

            copyDiagnosticAction.IdDiagnosticAction = this.IdDiagnosticAction;
            copyDiagnosticAction.IdDeviceOrderNumber = this.IdDeviceOrderNumber;
            copyDiagnosticAction.Name = this.Name;
            copyDiagnosticAction.DeviceOrderNumber = this.DeviceOrderNumber;
            copyDiagnosticAction.StartDate = this.StartDate;
            copyDiagnosticAction.EndDate = this.EndDate;
            copyDiagnosticAction.Distributor = this.Distributor;
            copyDiagnosticAction.Descr = this.Descr;



            return copyDiagnosticAction;
        }
    #endregion
    }
#endif
}