using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.Custom;
using System.Data;
using System.Xml.Serialization;
using System.Runtime.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class
        OpTariff : DB_TARIFF, IComparable, IEquatable<OpTariff>, IOpDynamic, IOpDynamicProperty<OpTariff>, IOpObject<OpTariffData>
    {
        #region Properties
        public int IdTariff { get { return this.ID_TARIFF; } set { this.ID_TARIFF = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        #endregion

        #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return _Distributor; } set { _Distributor = value; IdDistributor = (value != null) ? value.IdDistributor : 0; } }
        #endregion

        #region	Custom Properties
        [XmlIgnore, DataMember]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;

        public List<OpTariffScheme> TariffOptions;

        public string DT_CONSUMER_TAG_NO
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.CONSUMER_TAG_NO);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_TAG_NO, value);
            }
        }

        public List<string> DT_TARIFF_BILLING_PLUGIN_PREPAID_FREQ_RECEIVED
        {
            get
            {
                return DataList.Where(dt => dt.IdDataType == DataType.TARIFF_BILLING_PLUGIN_PREPAID_FREQ_RECEIVED)
                               .OrderBy(dt => dt.Index)
                               .Select(dt => dt.TryGetValue<string>())
                               .ToList();
            }
        }

        public List<string> DT_TARIFF_BILLING_PLUGIN_TOPUP_ACTION_FINISHED
        {
            get
            {
                return DataList.Where(dt => dt.IdDataType == DataType.TARIFF_BILLING_PLUGIN_TOPUP_ACTION_FINISHED)
                               .OrderBy(dt => dt.Index)
                               .Select(dt => dt.TryGetValue<string>())
                               .ToList();
            }
        }

        public int DT_ID_PRODUCT_CODE
        {
            get { return DataList.TryGetValue<int>(DataType.TARIFF_PRODUCT_CODE); }
            set { DataList.SetValue(DataType.TARIFF_PRODUCT_CODE, value); }
        }

        public Double DT_CALORIFIC_VALUE
        {
            get { return DataList.TryGetValue<Double>(DataType.TARIFF_CALOFIFIC_VALUE); }
            set { DataList.SetValue(DataType.TARIFF_CALOFIFIC_VALUE, value); }
        }

        public Double DT_STANDING_CHARGE
        {
            get { return DataList.TryGetValue<Double>(DataType.TARIFF_STANDING_CHARGE); }
            set { DataList.SetValue(DataType.TARIFF_STANDING_CHARGE, value); }
        }

        public Double DT_STANDING_CHARGE_VAT
        {
            get { return DataList.TryGetValue<Double>(DataType.TARIFF_STANDING_CHARGE_VAT); }
            set { DataList.SetValue(DataType.TARIFF_STANDING_CHARGE_VAT, value); }
        }

        public Double DT_CARBON_TAX
        {
            get { return DataList.TryGetValue<Double>(DataType.TARIFF_CARBON_TAX); }
            set { DataList.SetValue(DataType.TARIFF_CARBON_TAX, value); }
        }

        public Double DT_CARBON_TAX_VAT
        {
            get { return DataList.TryGetValue<Double>(DataType.TARIFF_CARBON_TAX_VAT); }
            set { DataList.SetValue(DataType.TARIFF_CARBON_TAX_VAT, value); }
        }

        public Double DT_PRODUCT_CHARGE
        {
            get { return DataList.TryGetValue<Double>(DataType.TARIFF_PRODUCT_CHARGE); }
            set { DataList.SetValue(DataType.TARIFF_PRODUCT_CHARGE, value); }
        }

        public Double DT_PRODUCT_CHARGE_VAT
        {
            get { return DataList.TryGetValue<Double>(DataType.TARIFF_PRODUCT_CHARGE_VAT); }
            set { DataList.SetValue(DataType.TARIFF_PRODUCT_CHARGE_VAT, value); }
        }

        public string DT_NOTES
        {
            get { return DataList.TryGetValue<string>(DataType.TARIFF_NOTES); }
            set { DataList.SetValue(DataType.TARIFF_NOTES, value); }
        }

        public string DT_TARIFF_CODE
        {
            get { return DataList.TryGetValue<string>(DataType.TARIFF_CODE); }
            set { DataList.SetValue(DataType.TARIFF_CODE, value); }
        }

        public bool DT_TARIFF_IS_ACTIVE
        {
            get { return DataList.TryGetValue<bool>(DataType.TARIFF_IS_ACTIVE); }
            set { DataList.SetValue(DataType.TARIFF_CODE, value); }
        }

        public double DT_PRICE_PREPAID_VARIABLE
        {
            get { return DataList.TryGetValue<double>(DataType.PRICE_PREPAID_VARIABLE); }
            set { DataList.SetValue(DataType.PRICE_PREPAID_VARIABLE, value); }
        }

        public double DT_PRICE_POSTPAID_FIXED_DAILY
        {
            get { return DataList.TryGetValue<double>(DataType.PRICE_POSTPAID_FIXED_DAILY); }
            set { DataList.SetValue(DataType.PRICE_POSTPAID_FIXED_DAILY, value); }
        }

        public DateTime ActivationDate
        {
            get { return DataList.TryGetValue<DateTime>(DataType.TARIFF_ACTIVATION_TIME); }
            set { DataList.SetValue(DataType.TARIFF_ACTIVATION_TIME, value); }
        }

        public Double EffectiveUnitPrice
        {
            get { return DT_PRODUCT_CHARGE*(1 + DT_PRODUCT_CHARGE_VAT / 100) + DT_CARBON_TAX * (1 + DT_CARBON_TAX_VAT / 100); }
        }

        public Double EffectiveUnitPriceSM
        {
            get { return EffectiveUnitPrice * DT_CALORIFIC_VALUE; }
        }

        public Double EffectiveStandingCharge
        {
            get { return DT_STANDING_CHARGE*(1 + DT_STANDING_CHARGE_VAT/100); }
        }

        public String FriendlyName
        {
            get { return String.Format("{0} - ({1})", Name, IdTariff); }
        }

        #endregion

        #region	Ctor
        public OpTariff()
            : base()
        {
            DataList = new OpDataList<OpTariffData>();
        }

        public OpTariff(DB_TARIFF clone)
            : base(clone)
        {
            DataList = new OpDataList<OpTariffData>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpTariff> ConvertList(DB_TARIFF[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTariff> ConvertList(DB_TARIFF[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTariff> ret = new List<OpTariff>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTariff(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpTariff> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.ITariffDataTypes != null)
            {
                List<int> idTarrif = list.Select(l => l.IdTariff).ToList();
                List<OpTariffData> data = dataProvider.GetTariffDataFilter(IdTariff: idTarrif.ToArray(), IdDataType: dataProvider.ITariffDataTypes.ToArray());
                if (data != null && data.Count > 0)
                {
                    foreach (var loop in list)
                    {
                        loop.DataList.AddRange(data.Where(d => d.IdTariff == loop.IdTariff));
                        loop.CreateTariffSchemes();
                    }
                }
            }
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTariff> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);

                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTariff)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTariff).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpTariff> Members
        public bool Equals(OpTariff other)
        {
            if (other == null)
                return false;
            return this.IdTariff.Equals(other.IdTariff);
        }
        #endregion

        public void CreateTariffSchemes()
        {
            TariffOptions = new List<OpTariffScheme>();
            for (int i = 0; i < OpTariffScheme.OptionsCount; i++)
            {
                OpTariffScheme TarifOption = new OpTariffScheme(DataList, i);
                TarifOption.IdTariff = IdTariff;
                TarifOption.OrderNo = i;
                TariffOptions.Add(TarifOption);
            }
        }

        #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdTariff; }
        }
        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }
        [XmlIgnore]
        OpTariff IOpDynamicProperty<OpTariff>.Owner
        {
            get { return this; }
        }
        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }
        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdTariff; }
            set { IdTariff = Convert.ToInt32(value); }
        }

        public OpDataList<OpTariffData> DataList { get; set; }
        #endregion
    }
#endif
}