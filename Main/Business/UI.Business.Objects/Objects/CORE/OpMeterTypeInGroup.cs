using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpMeterTypeInGroup : DB_METER_TYPE_IN_GROUP, IComparable, IEquatable<OpMeterTypeInGroup>
    {
    #region Properties
            public int IdMeterTypeGroup { get { return this.ID_METER_TYPE_GROUP;} set {this.ID_METER_TYPE_GROUP = value;}}
                public int IdMeterType { get { return this.ID_METER_TYPE;} set {this.ID_METER_TYPE = value;}}
    #endregion

    #region	Navigation Properties
		private OpMeterTypeGroup _MeterTypeGroup;
				public OpMeterTypeGroup MeterTypeGroup { get { return this._MeterTypeGroup; } set { this._MeterTypeGroup = value; this.ID_METER_TYPE_GROUP = (value == null)? 0 : (int)value.ID_METER_TYPE_GROUP; } }
				private OpMeterType _MeterType;
				public OpMeterType MeterType { get { return this._MeterType; } set { this._MeterType = value; this.ID_METER_TYPE = (value == null)? 0 : (int)value.ID_METER_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpMeterTypeInGroup()
			:base() 
        {
                OpState = OpChangeState.New;
        }
		
		public OpMeterTypeInGroup(DB_METER_TYPE_IN_GROUP clone)
			:base(clone) 
        {
                OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "MeterTypeInGroup [" + IdMeterTypeGroup.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpMeterTypeInGroup> ConvertList(DB_METER_TYPE_IN_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpMeterTypeInGroup> ConvertList(DB_METER_TYPE_IN_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpMeterTypeInGroup> ret = new List<OpMeterTypeInGroup>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpMeterTypeInGroup(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpMeterTypeInGroup> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpMeterType> MeterTypeDict = dataProvider.GetMeterType(list.Select(l => l.ID_METER_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_METER_TYPE);			
				foreach (var loop in list)
				{
						loop.MeterType = MeterTypeDict.TryGetValue(loop.ID_METER_TYPE); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpMeterTypeInGroup> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMeterTypeInGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMeterTypeInGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpMeterTypeInGroup> Members
        public bool Equals(OpMeterTypeInGroup other)
        {
            if (other == null)
				return false;
			return this.IdMeterTypeGroup.Equals(other.IdMeterTypeGroup);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpMeterTypeInGroup)
				return this.IdMeterTypeGroup == ((OpMeterTypeInGroup)obj).IdMeterTypeGroup;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpMeterTypeInGroup left, OpMeterTypeInGroup right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpMeterTypeInGroup left, OpMeterTypeInGroup right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdMeterTypeGroup.GetHashCode();
		}
    #endregion
    }
#endif
}