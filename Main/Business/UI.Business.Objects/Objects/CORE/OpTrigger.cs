﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTrigger : DB_TRIGGER, IComparable, IEquatable<OpTrigger>
    {
    #region Properties
        public long IdTrigger { get { return this.ID_TRIGGER; } set { if (this.ID_TRIGGER != value) { this.ID_TRIGGER = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string TableName { get { return this.TABLE_NAME; } set { if (this.TABLE_NAME != value) { this.TABLE_NAME = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public string Audit 
        { 
            get { return this.AUDIT; }
            set { if (this.AUDIT != value) { this.AUDIT = (value == "") ? null : this.AUDIT = value; ; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } 
        }
        public string DataChange 
        { 
            get { return this.DATA_CHANGE; }
            set { if (this.DATA_CHANGE != value) { this.DATA_CHANGE = (value == "") ? null : this.DATA_CHANGE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } 
        }
        public string DictTable 
        { 
            get { return this.DICT_TABLE; }
            set { if (this.DICT_TABLE != value) { this.DICT_TABLE = (value == "") ? null : this.DICT_TABLE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } 
        }
        public string ObjectTable 
        { 
            get { return this.OBJECT_TABLE; }
            set { if (this.OBJECT_TABLE != value) { this.OBJECT_TABLE = (value == "") ? null : this.OBJECT_TABLE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } 
        }
        public string DataTable 
        { 
            get { return this.DATA_TABLE; }
            set { if (this.DATA_TABLE != value) { this.DATA_TABLE = (value == "") ? null : this.DATA_TABLE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } 
        }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpTrigger()
            : base() { }

        public OpTrigger(DB_TRIGGER clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.TABLE_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpTrigger> ConvertList(DB_TRIGGER[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTrigger> ConvertList(DB_TRIGGER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpTrigger> ret = new List<OpTrigger>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTrigger(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTrigger> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTrigger> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTrigger)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTrigger).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpTrigger> Members
        public bool Equals(OpTrigger other)
        {
            if (other == null)
                return false;
            return this.IdTrigger.Equals(other.IdTrigger);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTrigger)
                return this.IdTrigger == ((OpTrigger)obj).IdTrigger;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTrigger left, OpTrigger right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTrigger left, OpTrigger right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTrigger.GetHashCode();
        }
    #endregion
    }
#endif
}
