using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerSettlementDocument : DB_CONSUMER_SETTLEMENT_DOCUMENT, IComparable, IEquatable<OpConsumerSettlementDocument>
    {
    #region Properties
        public long IdConsumerSettlementDocument { get { return this.ID_CONSUMER_SETTLEMENT_DOCUMENT;} set {this.ID_CONSUMER_SETTLEMENT_DOCUMENT = value;}}
        public long IdConsumerSettlement { get { return this.ID_CONSUMER_SETTLEMENT;} set {this.ID_CONSUMER_SETTLEMENT = value;}}
        public int IdSettlementDocumentType { get { return this.ID_SETTLEMENT_DOCUMENT_TYPE;} set {this.ID_SETTLEMENT_DOCUMENT_TYPE = value;}}
        public string Metadata { get { return this.METADATA;} set {this.METADATA = value;}}
        public byte[] DocumentLink { get { return this.DOCUMENT_LINK;} set {this.DOCUMENT_LINK = value;}}
        public Guid? DocumentGuid { get { return this.DOCUMENT_GUID;} set {this.DOCUMENT_GUID = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumerSettlement _ConsumerSettlement;
				public OpConsumerSettlement ConsumerSettlement { get { return this._ConsumerSettlement; } set { this._ConsumerSettlement = value; this.ID_CONSUMER_SETTLEMENT = (value == null)? 0 : (long)value.ID_CONSUMER_SETTLEMENT; } }
				private OpSettlementDocumentType _SettlementDocumentType;
				public OpSettlementDocumentType SettlementDocumentType { get { return this._SettlementDocumentType; } set { this._SettlementDocumentType = value; this.ID_SETTLEMENT_DOCUMENT_TYPE = (value == null)? 0 : (int)value.ID_SETTLEMENT_DOCUMENT_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerSettlementDocument()
			:base() {}
		
		public OpConsumerSettlementDocument(DB_CONSUMER_SETTLEMENT_DOCUMENT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.METADATA;
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerSettlementDocument> ConvertList(DB_CONSUMER_SETTLEMENT_DOCUMENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerSettlementDocument> ConvertList(DB_CONSUMER_SETTLEMENT_DOCUMENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpConsumerSettlementDocument> ret = new List<OpConsumerSettlementDocument>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerSettlementDocument(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerSettlementDocument> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpConsumerSettlement> ConsumerSettlementDict = dataProvider.GetConsumerSettlement(list.Select(l => l.ID_CONSUMER_SETTLEMENT).Distinct().ToArray()).ToDictionary(l => l.ID_CONSUMER_SETTLEMENT);
	Dictionary<int,OpSettlementDocumentType> SettlementDocumentTypeDict = dataProvider.GetSettlementDocumentType(list.Select(l => l.ID_SETTLEMENT_DOCUMENT_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_SETTLEMENT_DOCUMENT_TYPE);
			
				foreach (var loop in list)
				{
						loop.ConsumerSettlement = ConsumerSettlementDict.TryGetValue(loop.ID_CONSUMER_SETTLEMENT); 
		loop.SettlementDocumentType = SettlementDocumentTypeDict.TryGetValue(loop.ID_SETTLEMENT_DOCUMENT_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerSettlementDocument> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerSettlementDocument)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerSettlementDocument).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerSettlementDocument> Members
        public bool Equals(OpConsumerSettlementDocument other)
        {
            if (other == null)
				return false;
			return this.IdConsumerSettlementDocument.Equals(other.IdConsumerSettlementDocument);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerSettlementDocument)
				return this.IdConsumerSettlementDocument == ((OpConsumerSettlementDocument)obj).IdConsumerSettlementDocument;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerSettlementDocument left, OpConsumerSettlementDocument right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerSettlementDocument left, OpConsumerSettlementDocument right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerSettlementDocument.GetHashCode();
		}
    #endregion
    }
#endif
}