﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceActionResult : DB_SERVICE_ACTION_RESULT, IComparable, IEquatable<OpServiceActionResult>
    {
    #region Properties
        public int IdServiceActionResult { get { return this.ID_SERVICE_ACTION_RESULT; } set { this.ID_SERVICE_ACTION_RESULT = value; } }
        public int IdService { get { return this.ID_SERVICE; } set { this.ID_SERVICE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public bool AllowInputText { get { return this.ALLOW_INPUT_TEXT; } set { this.ALLOW_INPUT_TEXT = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public int? IdServiceReferenceType { get { return this.ID_SERVICE_REFERENCE_TYPE; } set { this.ID_SERVICE_REFERENCE_TYPE = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpService _Service;
        public OpService Service { get { return this._Service; } set { this._Service = value; this.ID_SERVICE = (value == null) ? 0 : (int)value.ID_SERVICE; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpServiceReferenceType _ServiceReferenceType;
        public OpServiceReferenceType ServiceReferenceType { get { return this._ServiceReferenceType; } set { this._ServiceReferenceType = value; this.ID_SERVICE_REFERENCE_TYPE = (value == null) ? null : (int?)value.ID_SERVICE_REFERENCE_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpServiceActionResult()
            : base() { }

        public OpServiceActionResult(DB_SERVICE_ACTION_RESULT clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (this.Descr != null)
                return Descr.ToString();
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceActionResult> ConvertList(DB_SERVICE_ACTION_RESULT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceActionResult> ConvertList(DB_SERVICE_ACTION_RESULT[] db_objects, IDataProvider dataProvider, 
            bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceActionResult> ret = new List<OpServiceActionResult>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceActionResult(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceActionResult> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpService> ServiceDict = dataProvider.GetService(list.Select(l => l.ID_SERVICE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpServiceReferenceType> ServiceReferenceTypeDict = dataProvider.GetServiceReferenceType(list.Where(l => l.ID_SERVICE_REFERENCE_TYPE.HasValue).Select(l => l.ID_SERVICE_REFERENCE_TYPE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_REFERENCE_TYPE);

                foreach (var loop in list)
                {
                    loop.Service = ServiceDict.TryGetValue(loop.ID_SERVICE);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    if (loop.ID_SERVICE_REFERENCE_TYPE.HasValue)
                        loop.ServiceReferenceType = ServiceReferenceTypeDict.TryGetValue(loop.ID_SERVICE_REFERENCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceActionResult> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceActionResult)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceActionResult).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceActionResult> Members
        public bool Equals(OpServiceActionResult other)
        {
            if (other == null)
                return false;
            return this.IdServiceActionResult.Equals(other.IdServiceActionResult);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceActionResult)
                return this.IdServiceActionResult == ((OpServiceActionResult)obj).IdServiceActionResult;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceActionResult left, OpServiceActionResult right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceActionResult left, OpServiceActionResult right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceActionResult.GetHashCode();
        }
    #endregion
    }
#endif
}