﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceDetails : DB_DEVICE_DETAILS, IComparable, IEquatable<OpDeviceDetails>, IOpDynamic, IOpDynamicProperty<OpDeviceDetails>
    {
    #region Properties
        public long IdDeviceDetails { get { return this.ID_DEVICE_DETAILS; } set { this.ID_DEVICE_DETAILS = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public string FactoryNbr { get { return this.FACTORY_NBR; } set { this.FACTORY_NBR = value; } }
        public DateTime? ShippingDate { get { return this.SHIPPING_DATE; } set { this.SHIPPING_DATE = value; } }
        public DateTime? WarrantyDate { get { return this.WARRANTY_DATE; } set { this.WARRANTY_DATE = value; } }
        public string Phone { get { return this.PHONE; } set { this.PHONE = value; } }
        public int IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = value; } }
        public int IdDeviceOrderNumber { get { return this.ID_DEVICE_ORDER_NUMBER; } set { this.ID_DEVICE_ORDER_NUMBER = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public int IdDeviceStateType { get { return this.ID_DEVICE_STATE_TYPE; } set { this.ID_DEVICE_STATE_TYPE = value; } }
        public string ProductionDeviceOrderNumber { get { return this.PRODUCTION_DEVICE_ORDER_NUMBER; } set { this.PRODUCTION_DEVICE_ORDER_NUMBER = value; } }
        public string DeviceBatchNumber { get { return this.DEVICE_BATCH_NUMBER; } set { this.DEVICE_BATCH_NUMBER = value; } }
        public int? IdDistributorOwner { get { return this.ID_DISTRIBUTOR_OWNER; } set { this.ID_DISTRIBUTOR_OWNER = value; } }
        public string ManufacturingBatch { get { return this.MANUFACTURING_BATCH; } set { this.MANUFACTURING_BATCH = value; } }
    #endregion

    #region	Navigation Properties
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpDeviceType _DeviceType;
        public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.ID_DEVICE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
        private OpDeviceOrderNumber _DeviceOrderNumber;
        public OpDeviceOrderNumber DeviceOrderNumber { get { return this._DeviceOrderNumber; } set { this._DeviceOrderNumber = value; this.ID_DEVICE_ORDER_NUMBER = (value == null) ? 0 : (int)value.ID_DEVICE_ORDER_NUMBER; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpDeviceStateType _DeviceStateType;
        public OpDeviceStateType DeviceStateType { get { return this._DeviceStateType; } set { this._DeviceStateType = value; this.ID_DEVICE_STATE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_STATE_TYPE; } }

        private OpDistributor _DistributorOwner;
        public OpDistributor DistributorOwner { get { return this._DistributorOwner; } set { this._DistributorOwner = value; this.ID_DISTRIBUTOR_OWNER = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
        private Dictionary<string, object> dynamicValues = new Dictionary<string, object>();
        private OpDynamicPropertyDict dynamicProperties = new OpDynamicPropertyDict();
    #endregion

    #region	Ctor
        public OpDeviceDetails()
            : base() { }

        public OpDeviceDetails(DB_DEVICE_DETAILS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.FACTORY_NBR;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceDetails> ConvertList(DB_DEVICE_DETAILS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceDetails> ConvertList(DB_DEVICE_DETAILS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDeviceDetails> ret = new List<OpDeviceDetails>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceDetails(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceDetails> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Select(l => l.ID_DEVICE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_TYPE);
                Dictionary<int, OpDeviceOrderNumber> DeviceOrderNumberDict = dataProvider.GetDeviceOrderNumber(list.Select(l => l.ID_DEVICE_ORDER_NUMBER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_ORDER_NUMBER);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpDistributor> DistributorOwnerDict = dataProvider.GetDistributor(list.Where(l => l.ID_DISTRIBUTOR_OWNER.HasValue).Select(l => l.ID_DISTRIBUTOR_OWNER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpDeviceStateType> DeviceStateTypeDict = dataProvider.GetDeviceStateType(list.Select(l => l.ID_DEVICE_STATE_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_STATE_TYPE);
                foreach (var loop in list)
                {
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    if (loop.ID_DISTRIBUTOR_OWNER.HasValue)
                        loop.DistributorOwner = DistributorOwnerDict.TryGetValue(loop.ID_DISTRIBUTOR_OWNER.Value);
                    loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE);
                    loop.DeviceOrderNumber = DeviceOrderNumberDict.TryGetValue(loop.ID_DEVICE_ORDER_NUMBER);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.DeviceStateType = DeviceStateTypeDict.TryGetValue(loop.ID_DEVICE_STATE_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceDetails> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceDetails)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceDetails).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceDetails> Members
        public bool Equals(OpDeviceDetails other)
        {
            if (other == null)
                return false;
            return this.IdDeviceDetails.Equals(other.IdDeviceDetails);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceDetails)
                return this.IdDeviceDetails == ((OpDeviceDetails)obj).IdDeviceDetails;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceDetails left, OpDeviceDetails right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceDetails left, OpDeviceDetails right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceDetails.GetHashCode();
        }
    #endregion

    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return SerialNbr.Value; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpDeviceDetails IOpDynamicProperty<OpDeviceDetails>.Owner
        {
            get { return this; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
    #endregion
    }
#endif
}