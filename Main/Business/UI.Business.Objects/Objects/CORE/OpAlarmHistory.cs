using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpAlarmHistory : DB_ALARM_HISTORY, IComparable, IEquatable<OpAlarmHistory>
    {
    #region Properties
        public long IdAlarmHistory { get { return this.ID_ALARM_HISTORY;} set {this.ID_ALARM_HISTORY = value;}}
        public long IdAlarm { get { return this.ID_ALARM;} set {this.ID_ALARM = value;}}
        public int IdAlarmStatus { get { return this.ID_ALARM_STATUS;} set {this.ID_ALARM_STATUS = value;}}
        public DateTime StartTime { get { return this.START_TIME;} set {this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME;} set {this.END_TIME = value; } }
        public string StartRuleHost { get { return this.START_RULE_HOST;} set {this.START_RULE_HOST = value;}}
        public string StartRuleUser { get { return this.START_RULE_USER;} set {this.START_RULE_USER = value;}}
        public string EndRuleHost { get { return this.END_RULE_HOST;} set {this.END_RULE_HOST = value;}}
        public string EndRuleUser { get { return this.END_RULE_USER;} set {this.END_RULE_USER = value;}}
    #endregion

    #region	Navigation Properties
		private OpAlarm _Alarm;
				public OpAlarm Alarm { get { return this._Alarm; } set { this._Alarm = value; this.ID_ALARM = (value == null)? 0 : (long)value.ID_ALARM; } }
				private OpAlarmStatus _AlarmStatus;
				public OpAlarmStatus AlarmStatus { get { return this._AlarmStatus; } set { this._AlarmStatus = value; this.ID_ALARM_STATUS = (value == null)? 0 : (int)value.ID_ALARM_STATUS; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpAlarmHistory()
			:base() {}
		
		public OpAlarmHistory(DB_ALARM_HISTORY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.START_RULE_HOST;
		}
    #endregion

    #region	ConvertList
		public static List<OpAlarmHistory> ConvertList(DB_ALARM_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpAlarmHistory> ConvertList(DB_ALARM_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpAlarmHistory> ret = new List<OpAlarmHistory>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmHistory(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpAlarmHistory> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpAlarm> AlarmDict = dataProvider.GetAlarm(list.Select(l => l.ID_ALARM).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM);
	Dictionary<int,OpAlarmStatus> AlarmStatusDict = dataProvider.GetAlarmStatus(list.Select(l => l.ID_ALARM_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_STATUS);
			
				foreach (var loop in list)
				{
						loop.Alarm = AlarmDict.TryGetValue(loop.ID_ALARM); 
		loop.AlarmStatus = AlarmStatusDict.TryGetValue(loop.ID_ALARM_STATUS); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpAlarmHistory> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpAlarmHistory> Members
        public bool Equals(OpAlarmHistory other)
        {
            if (other == null)
				return false;
			return this.IdAlarmHistory.Equals(other.IdAlarmHistory);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpAlarmHistory)
				return this.IdAlarmHistory == ((OpAlarmHistory)obj).IdAlarmHistory;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpAlarmHistory left, OpAlarmHistory right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpAlarmHistory left, OpAlarmHistory right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdAlarmHistory.GetHashCode();
		}
    #endregion
    }
#endif
}