using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerSettlement : DB_CONSUMER_SETTLEMENT, IComparable, IEquatable<OpConsumerSettlement>
    {
    #region Properties
        public long IdConsumerSettlement { get { return this.ID_CONSUMER_SETTLEMENT;} set {this.ID_CONSUMER_SETTLEMENT = value;}}
        public int IdConsumer { get { return this.ID_CONSUMER;} set {this.ID_CONSUMER = value;}}
        public int IdTariffSettlementPeriod { get { return this.ID_TARIFF_SETTLEMENT_PERIOD;} set {this.ID_TARIFF_SETTLEMENT_PERIOD = value;}}
        public DateTime SettlementTime { get { return this.SETTLEMENT_TIME;} set {this.SETTLEMENT_TIME = value;}}
        public int IdModule { get { return this.ID_MODULE;} set {this.ID_MODULE = value;}}
        public int? IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
        public int IdConsumerSettlementReason { get { return this.ID_CONSUMER_SETTLEMENT_REASON;} set {this.ID_CONSUMER_SETTLEMENT_REASON = value;}}
        public int IdConsumerSettlementStatus { get { return this.ID_CONSUMER_SETTLEMENT_STATUS;} set {this.ID_CONSUMER_SETTLEMENT_STATUS = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumer _Consumer;
				public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null)? 0 : (int)value.ID_CONSUMER; } }
				private OpTariffSettlementPeriod _TariffSettlementPeriod;
				public OpTariffSettlementPeriod TariffSettlementPeriod { get { return this._TariffSettlementPeriod; } set { this._TariffSettlementPeriod = value; this.ID_TARIFF_SETTLEMENT_PERIOD = (value == null)? 0 : (int)value.ID_TARIFF_SETTLEMENT_PERIOD; } }
				private OpModule _Module;
				public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null)? 0 : (int)value.ID_MODULE; } }
				private OpOperator _Operator;
				public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? null : (int?)value.ID_OPERATOR; } }
				private OpConsumerSettlementReason _ConsumerSettlementReason;
				public OpConsumerSettlementReason ConsumerSettlementReason { get { return this._ConsumerSettlementReason; } set { this._ConsumerSettlementReason = value; this.ID_CONSUMER_SETTLEMENT_REASON = (value == null)? 0 : (int)value.ID_CONSUMER_SETTLEMENT_REASON; } }
				private OpConsumerSettlementStatus _ConsumerSettlementStatus;
				public OpConsumerSettlementStatus ConsumerSettlementStatus { get { return this._ConsumerSettlementStatus; } set { this._ConsumerSettlementStatus = value; this.ID_CONSUMER_SETTLEMENT_STATUS = (value == null)? 0 : (int)value.ID_CONSUMER_SETTLEMENT_STATUS; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerSettlement()
			:base() {}
		
		public OpConsumerSettlement(DB_CONSUMER_SETTLEMENT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ConsumerSettlement [" + IdConsumerSettlement.ToString() + "]";
		}
    #endregion

    #region	ConvertList
        public static List<OpConsumerSettlement> ConvertList(DB_CONSUMER_SETTLEMENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpConsumerSettlement> ConvertList(DB_CONSUMER_SETTLEMENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpConsumerSettlement> ret = new List<OpConsumerSettlement>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerSettlement(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion
		
    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConsumerSettlement> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Select(l => l.ID_CONSUMER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER);
                Dictionary<int, OpTariffSettlementPeriod> TariffSettlementPeriodDict = dataProvider.GetTariffSettlementPeriod(list.Select(l => l.ID_TARIFF_SETTLEMENT_PERIOD).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_TARIFF_SETTLEMENT_PERIOD);
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Select(l => l.ID_MODULE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_MODULE);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpConsumerSettlementReason> ConsumerSettlementReasonDict = dataProvider.GetConsumerSettlementReason(list.Select(l => l.ID_CONSUMER_SETTLEMENT_REASON).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER_SETTLEMENT_REASON);
                Dictionary<int, OpConsumerSettlementStatus> ConsumerSettlementStatusDict = dataProvider.GetConsumerSettlementStatus(list.Select(l => l.ID_CONSUMER_SETTLEMENT_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER_SETTLEMENT_STATUS);
                foreach (var loop in list)
                {
                    loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER);
                    loop.TariffSettlementPeriod = TariffSettlementPeriodDict.TryGetValue(loop.ID_TARIFF_SETTLEMENT_PERIOD);
                    loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE);
                    if (loop.ID_OPERATOR.HasValue)
                        loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value);
                    loop.ConsumerSettlementReason = ConsumerSettlementReasonDict.TryGetValue(loop.ID_CONSUMER_SETTLEMENT_REASON);
                    loop.ConsumerSettlementStatus = ConsumerSettlementStatusDict.TryGetValue(loop.ID_CONSUMER_SETTLEMENT_STATUS);
                }
            }
        }
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConsumerSettlement> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerSettlement)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerSettlement).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerSettlement> Members
        public bool Equals(OpConsumerSettlement other)
        {
            if (other == null)
				return false;
			return this.IdConsumerSettlement.Equals(other.IdConsumerSettlement);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerSettlement)
				return this.IdConsumerSettlement == ((OpConsumerSettlement)obj).IdConsumerSettlement;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerSettlement left, OpConsumerSettlement right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerSettlement left, OpConsumerSettlement right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerSettlement.GetHashCode();
		}
    #endregion
    }
#endif
}