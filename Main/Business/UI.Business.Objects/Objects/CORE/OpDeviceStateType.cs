using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceStateType : DB_DEVICE_STATE_TYPE, IComparable, IEquatable<OpDeviceStateType>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdDeviceStateType { get { return this.ID_DEVICE_STATE_TYPE; } set { this.ID_DEVICE_STATE_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpDeviceStateType()
            : base() { }

        public OpDeviceStateType(DB_DEVICE_STATE_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceStateType> ConvertList(DB_DEVICE_STATE_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDeviceStateType> ConvertList(DB_DEVICE_STATE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDeviceStateType> ret = new List<OpDeviceStateType>(list.Length);
            foreach (var loop in list)
            {
                OpDeviceStateType insert = new OpDeviceStateType(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[] { loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();


                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceStateType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceStateType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceStateType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Enum
        //generated from DB, don't change it!
        public enum Enum : int
        {
            NEW = 1,
            IDLE = 2,
            PASSIVE = 3,
            ACTIVE = 4,
            INSPECTED = 5,
            SERVICED = 6,
            ERASED = 7,
            PRODUCED_NOTWORKING = 8,
            PRODUCED_WORKING = 9,
            SENT = 10,
            AFTER_SERVICE = 11,
            PRODUCED_WRONG_CONF = 12,
            STORED_AFTER_SERVICE = 13,
            REMOVED = 14,
            SOT_RECEIVED = 15
        }
    #endregion

    #region IEquatable<OpDeviceStateType>

        public bool Equals(OpDeviceStateType obj)
        {
            if (obj is OpDeviceStateType)
                return this.IdDeviceStateType == ((OpDeviceStateType)obj).IdDeviceStateType;
            else
                return base.Equals(obj);
        }

        #endregion

    #region Implementation of IReferenceType
    public object GetReferenceKey()
    {
        return this.IdDeviceStateType;
    }

    public object GetReferenceValue()
    {
        return this;
    }
    #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceStateType)
                return this.IdDeviceStateType == ((OpDeviceStateType)obj).IdDeviceStateType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceStateType left, OpDeviceStateType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceStateType left, OpDeviceStateType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceStateType.GetHashCode();
        }
    #endregion
    }
#endif
}