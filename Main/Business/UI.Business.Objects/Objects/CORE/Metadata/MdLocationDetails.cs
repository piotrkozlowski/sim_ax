namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpLocationDetails Metadata
    /// </summary>
    public class MdLocationDetails
    {
        public const string IdLocationDetails = "IdLocationDetails";
        public const string IdLocation = "IdLocation";
        public const string Cid = "Cid";
        public const string Name = "Name";
        public const string City = "City";
        public const string Address = "Address";
        public const string Zip = "Zip";
        public const string Country = "Country";
        public const string Latitude = "Latitude";
        public const string Longitude = "Longitude";
        public const string ImrServer = "ImrServer";
        public const string CreationDate = "CreationDate";
        public const string LocationMemo = "LocationMemo";
        public const string LocationSourceServerId = "LocationSourceServerId";
        public const string IdDistributor = "IdDistributor";
        public const string IdLocationType = "IdLocationType";
        public const string IdLocationStateType = "IdLocationStateType";
        public const string InKpi = "InKpi";
        public const string AllowGrouping = "AllowGrouping";
        public const string Distributor = "Distributor";
        public const string IMRServer = "IMRServer";
        public const string LocationType = "LocationType";
        public const string LocationStateType = "LocationStateType";
        public const string IdLocationGroup = "IdLocationGroup";
        public const string FullAddress = "FullAddress";
        public const string LocationDetails = "LocationDetails";
    }
}