namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpIssueGroup Metadata
	/// </summary>
	public class MdIssueGroup
	{
		public const string IdIssueGroup = "IdIssueGroup";
		public const string IdParentGroup = "IdParentGroup";
		public const string Name = "Name";
		public const string DateCreated = "DateCreated";
		public const string IdDistributor = "IdDistributor";
		public const string ParentGroup = "ParentGroup";
		public const string Distributor = "Distributor";
	
	}
}
	