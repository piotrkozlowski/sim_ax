namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConfigurationProfileType Metadata
	/// </summary>
	public class MdConfigurationProfileType
	{
		public const string IdConfigurationProfileType = "IdConfigurationProfileType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	