namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMeterType Metadata
	/// </summary>
	public class MdMeterType
	{
		public const string IdMeterType = "IdMeterType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdMeterTypeClass = "IdMeterTypeClass";
		public const string Descr = "Descr";
		public const string MeterTypeClass = "MeterTypeClass";
		public const string DataList = "DataList";
	
	}
}
	