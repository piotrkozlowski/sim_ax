namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceType Metadata
	/// </summary>
	public class MdDeviceType
	{
		public const string IdDeviceType = "IdDeviceType";
		public const string Name = "Name";
		public const string IdDeviceTypeClass = "IdDeviceTypeClass";
		public const string IdDescr = "IdDescr";
		public const string TwoWayTransAvailable = "TwoWayTransAvailable";
		public const string IdDeviceDriver = "IdDeviceDriver";
		public const string IdProtocol = "IdProtocol";
		public const string DefaultIdDeviceOderNumber = "DefaultIdDeviceOderNumber";
		public const string FrameDefinition = "FrameDefinition";
		public const string DeviceTypeClass = "DeviceTypeClass";
		public const string Descr = "Descr";
		public const string DeviceDriver = "DeviceDriver";
		public const string Protocol = "Protocol";
		public const string OpState = "OpState";
		public const string DataList = "DataList";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	