namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMobileNetworkCode Metadata
	/// </summary>
	public class MdMobileNetworkCode
	{
		public const string Code = "Code";
		public const string Mmc = "Mmc";
		public const string Mnc = "Mnc";
		public const string Brand = "Brand";
		public const string Operator = "Operator";
		public const string Bands = "Bands";
		public const string SmsServiceCenter = "SmsServiceCenter";
		public const string ApnAddress = "ApnAddress";
		public const string ApnLogin = "ApnLogin";
		public const string ApnPassword = "ApnPassword";
	
	}
}
	