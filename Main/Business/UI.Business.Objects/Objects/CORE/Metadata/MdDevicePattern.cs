namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDevicePattern Metadata
	/// </summary>
	public class MdDevicePattern
	{
		public const string IdPattern = "IdPattern";
		public const string SerialNbr = "SerialNbr";
		public const string IdDistributor = "IdDistributor";
		public const string Name = "Name";
		public const string CreationDate = "CreationDate";
		public const string CreatedBy = "CreatedBy";
		public const string IdTemplate = "IdTemplate";
		public const string Distributor = "Distributor";
		public const string Template = "Template";
		public const string CreatedOperator = "CreatedOperator";
		public const string DataList = "DataList";
	
	}
}
	