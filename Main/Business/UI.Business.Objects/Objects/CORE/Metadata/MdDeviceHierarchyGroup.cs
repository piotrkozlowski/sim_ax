namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceHierarchyGroup Metadata
	/// </summary>
	public class MdDeviceHierarchyGroup
	{
		public const string IdDeviceHierarchyGroup = "IdDeviceHierarchyGroup";
		public const string DeviceHierarchySerialNbr = "DeviceHierarchySerialNbr";
		public const string IdDistributor = "IdDistributor";
		public const string IdDepositoryLocation = "IdDepositoryLocation";
		public const string CreationDate = "CreationDate";
		public const string FinishDate = "FinishDate";
		public const string Distributor = "Distributor";
		public const string DeviceHierarchy = "DeviceHierarchy";
	
	}
}
	