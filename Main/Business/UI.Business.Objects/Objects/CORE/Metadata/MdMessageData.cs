namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMessageData Metadata
	/// </summary>
	public class MdMessageData
	{
		public const string IdMessageData = "IdMessageData";
		public const string IdMessage = "IdMessage";
		public const string IndexNbr = "IndexNbr";
		public const string IdDataType = "IdDataType";
		public const string Value = "Value";
		public const string OpState = "OpState";
		public const string Message = "Message";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	