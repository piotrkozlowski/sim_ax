namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceHierarchyPattern Metadata
	/// </summary>
	public class MdDeviceHierarchyPattern
	{
		public const string IdDeviceHierarchyPattern = "IdDeviceHierarchyPattern";
		public const string IdDeviceTypeParent = "IdDeviceTypeParent";
		public const string IdSlotType = "IdSlotType";
		public const string SlotNbrMin = "SlotNbrMin";
		public const string SlotNbrMax = "SlotNbrMax";
		public const string IdProtocolIn = "IdProtocolIn";
		public const string IdProtocolOut = "IdProtocolOut";
		public const string IdDeviceType = "IdDeviceType";
		public const string IdDeviceTypeGroup = "IdDeviceTypeGroup";
		public const string SlotType = "SlotType";
		public const string DeviceType = "DeviceType";
		public const string DeviceTypeGroup = "DeviceTypeGroup";
		public const string DeviceTypeParent = "DeviceTypeParent";
		public const string ProtocolIn = "ProtocolIn";
		public const string ProtocolOut = "ProtocolOut";
		public const string OpState = "OpState";
	
	}
}
	