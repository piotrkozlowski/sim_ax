namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionElementType Metadata
	/// </summary>
	public class MdVersionElementType
	{
		public const string IdVersionElementType = "IdVersionElementType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string OpState = "OpState";
		public const string Descr = "Descr";
	
	}
}
	