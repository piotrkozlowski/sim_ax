namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDevice Metadata
	/// </summary>
	public class MdDevice
	{
		public const string SerialNbr = "SerialNbr";
		public const string IdDeviceType = "IdDeviceType";
		public const string SerialNbrPattern = "SerialNbrPattern";
		public const string IdDescrPattern = "IdDescrPattern";
		public const string IdDeviceOrderNumber = "IdDeviceOrderNumber";
		public const string IdDistributor = "IdDistributor";
		public const string IdDeviceStateType = "IdDeviceStateType";
		public const string DeviceType = "DeviceType";
		public const string DeviceOrderNumber = "DeviceOrderNumber";
		public const string Distributor = "Distributor";
		public const string DeviceStateType = "DeviceStateType";
		public const string PatternDevice = "PatternDevice";
		public const string OpState = "OpState";
		public const string GridLookUpEditString = "GridLookUpEditString";
		public const string WarrantyDate = "WarrantyDate";
		public const string ProductionDate = "ProductionDate";
		public const string IdDelivery = "IdDelivery";
		public const string Delivery = "Delivery";
		public const string FactoryNumber = "FactoryNumber";
		public const string DataList = "DataList";
		public const string UsokDeviceState = "UsokDeviceState";
		public const string CurrentMeter = "CurrentMeter";
		public const string CurrentLocation = "CurrentLocation";
		public const string IsTemplate = "IsTemplate";
		public const string TemplateDescription = "TemplateDescription";
		public const string TemplateDescrString = "TemplateDescrString";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	