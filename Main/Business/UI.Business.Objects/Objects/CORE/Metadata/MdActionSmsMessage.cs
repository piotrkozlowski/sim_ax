namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionSmsMessage Metadata
	/// </summary>
	public class MdActionSmsMessage
	{
		public const string IdActionSmsMessage = "IdActionSmsMessage";
		public const string SerialNbr = "SerialNbr";
		public const string Time = "Time";
		public const string IsIncoming = "IsIncoming";
		public const string IdTransmissionStatus = "IdTransmissionStatus";
		public const string IdAction = "IdAction";
		public const string Message = "Message";
		public const string TransmissionStatus = "TransmissionStatus";
		public const string Action = "Action";
		public const string Consumer = "Consumer";
	
	}
}
	