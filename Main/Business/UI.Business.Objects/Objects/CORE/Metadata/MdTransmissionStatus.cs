namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTransmissionStatus Metadata
	/// </summary>
	public class MdTransmissionStatus
	{
		public const string IdTransmissionStatus = "IdTransmissionStatus";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	