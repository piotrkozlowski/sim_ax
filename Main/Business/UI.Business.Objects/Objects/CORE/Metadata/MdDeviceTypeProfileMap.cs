namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTypeProfileMap Metadata
	/// </summary>
	public class MdDeviceTypeProfileMap
	{
		public const string IdDeviceTypeProfileMap = "IdDeviceTypeProfileMap";
		public const string IdDeviceType = "IdDeviceType";
		public const string IdOperatorCreated = "IdOperatorCreated";
		public const string CreatedTime = "CreatedTime";
		public const string IdOperatorModified = "IdOperatorModified";
		public const string ModifiedTime = "ModifiedTime";
		public const string DeviceType = "DeviceType";
		public const string OperatorCreated = "OperatorCreated";
		public const string OperatorModified = "OperatorModified";
	
	}
}
	