namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRoute Metadata
	/// </summary>
	public class MdRoute
	{
		public const string IdRoute = "IdRoute";
		public const string IdRouteDef = "IdRouteDef";
		public const string Year = "Year";
		public const string Month = "Month";
		public const string Week = "Week";
		public const string IdOperatorExecutor = "IdOperatorExecutor";
		public const string IdOperatorApproved = "IdOperatorApproved";
		public const string DateUploaded = "DateUploaded";
		public const string DateApproved = "DateApproved";
		public const string DateFinished = "DateFinished";
		public const string IdRouteStatus = "IdRouteStatus";
		public const string RowVersion = "RowVersion";
		public const string IdDistributor = "IdDistributor";
		public const string OpState = "OpState";
		public const string RouteDef = "RouteDef";
		public const string RouteStatus = "RouteStatus";
		public const string OperatorExecutor = "OperatorExecutor";
		public const string OperatorApproved = "OperatorApproved";
		public const string Distributor = "Distributor";
		public const string Symbol = "Symbol";
		public const string WeekSymbol = "WeekSymbol";
		public const string MonthSymbol = "MonthSymbol";
		public const string RouteDate = "RouteDate";
		public const string DataList = "DataList";
	
	}
}
	