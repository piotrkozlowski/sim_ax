namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDevicePatternData Metadata
	/// </summary>
	public class MdDevicePatternData
	{
		public const string IdPattern = "IdPattern";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
		public const string DataStatus = "DataStatus";
	
	}
}
	