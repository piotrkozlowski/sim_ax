namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionData Metadata
	/// </summary>
	public class MdVersionData
	{
		public const string IdVersionData = "IdVersionData";
		public const string IdVersion = "IdVersion";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string Version = "Version";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string ReferencedObject = "ReferencedObject";
		public const string OpState = "OpState";
	
	}
}
	