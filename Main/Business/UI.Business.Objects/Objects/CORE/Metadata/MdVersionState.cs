namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionState Metadata
	/// </summary>
	public class MdVersionState
	{
		public const string IdVersionState = "IdVersionState";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string OpState = "OpState";
		public const string Descr = "Descr";
	
	}
}
	