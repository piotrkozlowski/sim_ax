namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpShippingList Metadata
	/// </summary>
	public class MdShippingList
	{
		public const string IdShippingList = "IdShippingList";
		public const string ContractNbr = "ContractNbr";
		public const string ShippingListNbr = "ShippingListNbr";
		public const string IdOperatorCreator = "IdOperatorCreator";
		public const string IdOperatorContact = "IdOperatorContact";
		public const string IdOperatorQuality = "IdOperatorQuality";
		public const string IdOperatorProtocol = "IdOperatorProtocol";
		public const string ShippingLetterNbr = "ShippingLetterNbr";
		public const string IdLocation = "IdLocation";
		public const string IdShippingListType = "IdShippingListType";
		public const string CreationDate = "CreationDate";
		public const string FinishDate = "FinishDate";
		public const string IdDistributor = "IdDistributor";
		public const string Location = "Location";
		public const string ShippingListType = "ShippingListType";
		public const string Distributor = "Distributor";
		public const string OperatorCreator = "OperatorCreator";
		public const string OperatorContact = "OperatorContact";
		public const string OperatorQuality = "OperatorQuality";
		public const string OperatorProtocol = "OperatorProtocol";
		public const string DataList = "DataList";
		public const string DevicesAmount = "DevicesAmount";
        public const string PurchaseOrder = "PurchaseOrder";

    }
}
	