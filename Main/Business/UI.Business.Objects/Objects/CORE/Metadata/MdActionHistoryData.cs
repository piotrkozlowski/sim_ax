namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionHistoryData Metadata
	/// </summary>
	public class MdActionHistoryData
	{
		public const string IdActionHistoryData = "IdActionHistoryData";
		public const string ArgNbr = "ArgNbr";
		public const string Value = "Value";
	
	}
}
	