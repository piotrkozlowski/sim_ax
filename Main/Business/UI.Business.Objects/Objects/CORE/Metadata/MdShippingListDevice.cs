namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpShippingListDevice Metadata
	/// </summary>
	public class MdShippingListDevice
	{
		public const string IdShippingListDevice = "IdShippingListDevice";
		public const string IdShippingList = "IdShippingList";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdArticle = "IdArticle";
		public const string InsertDate = "InsertDate";
		public const string ShippingList = "ShippingList";
		public const string Meter = "Meter";
		public const string Article = "Article";
		public const string Device = "Device";
		public const string DeviceHierarchy = "DeviceHierarchy";
		public const string DeviceDetails = "DeviceDetails";
	
	}
}
	