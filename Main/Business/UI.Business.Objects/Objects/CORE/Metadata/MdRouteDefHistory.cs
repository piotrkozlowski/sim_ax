namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRouteDefHistory Metadata
	/// </summary>
	public class MdRouteDefHistory
	{
		public const string IdRouteDefHistory = "IdRouteDefHistory";
		public const string IdRouteDef = "IdRouteDef";
		public const string IdRouteStatus = "IdRouteStatus";
		public const string IdOperator = "IdOperator";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string RouteDef = "RouteDef";
		public const string RouteStatus = "RouteStatus";
		public const string Operator = "Operator";
	
	}
}
	