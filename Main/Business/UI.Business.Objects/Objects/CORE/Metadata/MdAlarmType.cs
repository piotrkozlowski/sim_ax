namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmType Metadata
	/// </summary>
	public class MdAlarmType
	{
		public const string IdAlarmType = "IdAlarmType";
		public const string Symbol = "Symbol";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	