namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmText Metadata
	/// </summary>
	public class MdAlarmText
	{
		public const string IdAlarmText = "IdAlarmText";
		public const string IdLanguage = "IdLanguage";
		public const string Name = "Name";
		public const string AlarmText = "AlarmText";
		public const string IdDistributor = "IdDistributor";
		public const string Language = "Language";
		public const string Distributor = "Distributor";
	
	}
}
	