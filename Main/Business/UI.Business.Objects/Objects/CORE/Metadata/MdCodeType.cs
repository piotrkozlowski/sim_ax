namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpCodeType Metadata
	/// </summary>
	public class MdCodeType
	{
		public const string IdCodeType = "IdCodeType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdDataTypeFormat = "IdDataTypeFormat";
		public const string Descr = "Descr";
		public const string DataTypeFormat = "DataTypeFormat";
	
	}
}
	