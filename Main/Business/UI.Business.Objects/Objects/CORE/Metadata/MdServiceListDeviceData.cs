namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceListDeviceData Metadata
	/// </summary>
	public class MdServiceListDeviceData
	{
		public const string IdServiceListDeviceData = "IdServiceListDeviceData";
		public const string IdServiceList = "IdServiceList";
		public const string SerialNbr = "SerialNbr";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string ServiceList = "ServiceList";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
		public const string OpState = "OpState";
	
	}
}
	