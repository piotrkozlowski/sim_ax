namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceCustom Metadata
	/// </summary>
	public class MdServiceCustom
	{
		public const string IdServiceCustom = "IdServiceCustom";
		public const string IdServiceList = "IdServiceList";
		public const string EditorCost = "EditorCost";
		public const string EngineerCost = "EngineerCost";
		public const string DirectCost = "DirectCost";
		public const string IndirectCost = "IndirectCost";
		public const string MaterialCost = "MaterialCost";
		public const string SupplyCost = "SupplyCost";
		public const string TransportCost = "TransportCost";
		public const string IdFile = "IdFile";
		public const string CustomValue = "CustomValue";
		public const string EditorTime = "EditorTime";
		public const string EngineerTime = "EngineerTime";
		public const string ServiceList = "ServiceList";
		public const string File = "File";
	
	}
}
	