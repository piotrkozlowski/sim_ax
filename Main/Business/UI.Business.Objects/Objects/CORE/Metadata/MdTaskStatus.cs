namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTaskStatus Metadata
	/// </summary>
	public class MdTaskStatus
	{
		public const string IdTaskStatus = "IdTaskStatus";
		public const string IdDescr = "IdDescr";
		public const string Description = "Description";
		public const string IsFinished = "IsFinished";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	