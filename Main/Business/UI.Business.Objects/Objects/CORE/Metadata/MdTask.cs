namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpTask Metadata
    /// </summary>
    public class MdTask
    {
        public const string IdTask = "IdTask";
        public const string IdTaskType = "IdTaskType";
        public const string IdIssue = "IdIssue";
        public const string IdTaskGroup = "IdTaskGroup";
        public const string IdPlannedRoute = "IdPlannedRoute";
        public const string IdOperatorRegistering = "IdOperatorRegistering";
        public const string IdOperatorPerformer = "IdOperatorPerformer";
        public const string IdTaskStatus = "IdTaskStatus";
        public const string CreationDate = "CreationDate";
        public const string Accepted = "Accepted";
        public const string IdOperatorAccepted = "IdOperatorAccepted";
        public const string AcceptanceDate = "AcceptanceDate";
        public const string Notes = "Notes";
        public const string Deadline = "Deadline";
        public const string IdLocation = "IdLocation";
        public const string IdActor = "IdActor";
        public const string TopicNumber = "TopicNumber";
        public const string Priority = "Priority";
        public const string OperationCode = "OperationCode";
        public const string RowVersion = "RowVersion";
        public const string IdDistributor = "IdDistributor";
        public const string OpState = "OpState";
        public const string TaskType = "TaskType";
        public const string Issue = "Issue";
        public const string TaskGroup = "TaskGroup";
        public const string TaskStatus = "TaskStatus";
        public const string Location = "Location";
        public const string Actor = "Actor";
        public const string OperatorRegistering = "OperatorRegistering";
        public const string OperatorPerformer = "OperatorPerformer";
        public const string Route = "Route";
        public const string Distributor = "Distributor";
        public const string FitterVisitDate = "FitterVisitDate";
        public const string ProtocolSubmitDate = "ProtocolSubmitDate";
        public const string ArrangedFitterVisitDate = "ArrangedFitterVisitDate";
        public const string ArrangedFitterVisitNote = "ArrangedFitterVisitNote";
        public const string OperationCodeTMQ = "OperationCodeTMQ";
        public const string FitterPaymentDone = "FitterPaymentDone";
        public const string FitterPaymentDate = "FitterPaymentDate";
        public const string PaymentAmount = "PaymentAmount";
        public const string PaymentNote = "PaymentNote";
        public const string FitterOrderedToPay = "FitterOrderedToPay";
        public const string ClientPaymentDone = "ClientPaymentDone";
        public const string ClientPaymentAmount = "ClientPaymentAmount";
        public const string ClientPaymentDate = "ClientPaymentDate";
        public const string ClientOrderedToPay = "ClientOrderedToPay";
        public const string IMRMySCVisible = "IMRMySCVisible";
        public const string WarrantyService = "WarrantyService";
        public const string AdditionalPayment = "AdditionalPayment";
        public const string ValidationDate = "ValidationDate";
        public const string MaintenanceNotes = "MaintenanceNotes";
        public const string DataList = "DataList";
        public const string TaskHistory = "TaskHistory";
        public const string Overdue = "Overdue";
        public const string DeadlineDaysLeft = "DeadlineDaysLeft";
        public const string OverdueColor = "OverdueColor";
        public const string SalesOrder = "SalesOrder";
        public const string DynamicValues = "DynamicValues";


    }
}