namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationStateType Metadata
	/// </summary>
	public class MdLocationStateType
	{
		public const string IdLocationStateType = "IdLocationStateType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Description = "Description";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	