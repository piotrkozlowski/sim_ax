namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmGroupOperator Metadata
	/// </summary>
	public class MdAlarmGroupOperator
	{
		public const string IdAlarmGroup = "IdAlarmGroup";
		public const string IdOperator = "IdOperator";
		public const string IdDataType = "IdDataType";
		public const string AlarmGroup = "AlarmGroup";
		public const string Operator = "Operator";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
	
	}
}
	