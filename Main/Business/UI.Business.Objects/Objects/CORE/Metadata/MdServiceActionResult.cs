namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceActionResult Metadata
	/// </summary>
	public class MdServiceActionResult
	{
		public const string IdServiceActionResult = "IdServiceActionResult";
		public const string IdService = "IdService";
		public const string Name = "Name";
		public const string AllowInputText = "AllowInputText";
		public const string IdDescr = "IdDescr";
		public const string IdServiceReferenceType = "IdServiceReferenceType";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string Service = "Service";
		public const string Descr = "Descr";
		public const string ServiceReferenceType = "ServiceReferenceType";
	
	}
}
	