namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataType Metadata
	/// </summary>
	public class MdDataType
	{
		public const string IdDataType = "IdDataType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdDataTypeClass = "IdDataTypeClass";
		public const string IdReferenceType = "IdReferenceType";
		public const string IdDataTypeFormat = "IdDataTypeFormat";
		public const string IsArchiveOnly = "IsArchiveOnly";
		public const string IsRemoteRead = "IsRemoteRead";
		public const string IsRemoteWrite = "IsRemoteWrite";
		public const string IsEditable = "IsEditable";
		public const string IdUnit = "IdUnit";
		public const string Descr = "Descr";
		public const string DataTypeClass = "DataTypeClass";
		public const string ReferenceType = "ReferenceType";
		public const string DataTypeFormat = "DataTypeFormat";
		public const string Unit = "Unit";
		public const string OpState = "OpState";
	
	}
}
	