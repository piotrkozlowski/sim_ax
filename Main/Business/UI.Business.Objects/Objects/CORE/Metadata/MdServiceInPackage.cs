namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceInPackage Metadata
	/// </summary>
	public class MdServiceInPackage
	{
		public const string IdServiceInPackage = "IdServiceInPackage";
		public const string IdServicePackage = "IdServicePackage";
		public const string IdService = "IdService";
		public const string ServicePackage = "ServicePackage";
		public const string Service = "Service";
	
	}
}
	