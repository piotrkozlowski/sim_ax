namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServicePackage Metadata
	/// </summary>
	public class MdServicePackage
	{
		public const string IdServicePackage = "IdServicePackage";
		public const string IdContract = "IdContract";
		public const string Name = "Name";
		public const string Descr = "Descr";
		public const string IsSingle = "IsSingle";
		public const string Contract = "Contract";
		public const string DataList = "DataList";
	
	}
}
	