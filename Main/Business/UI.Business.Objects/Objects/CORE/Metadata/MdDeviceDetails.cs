namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpDeviceDetails Metadata
    /// </summary>
    public class MdDeviceDetails
    {
        public const string IdDeviceDetails = "IdDeviceDetails";
        public const string SerialNbr = "SerialNbr";
        public const string IdLocation = "IdLocation";
        public const string FactoryNbr = "FactoryNbr";
        public const string ShippingDate = "ShippingDate";
        public const string WarrantyDate = "WarrantyDate";
        public const string Phone = "Phone";
        public const string IdDeviceType = "IdDeviceType";
        public const string IdDeviceOrderNumber = "IdDeviceOrderNumber";
        public const string IdDistributor = "IdDistributor";
        public const string IdDeviceStateType = "IdDeviceStateType";
        public const string ProductionDeviceOrderNumber = "ProductionDeviceOrderNumber";
        public const string DeviceBatchNumber = "DeviceBatchNumber";
        public const string IdDistributorOwner = "IdDistributorOwner";
        public const string Location = "Location";
        public const string DeviceType = "DeviceType";
        public const string DeviceOrderNumber = "DeviceOrderNumber";
        public const string Distributor = "Distributor";
        public const string DeviceStateType = "DeviceStateType";
        public const string DistributorOwner = "DistributorOwner";
        public const string ManufacturingBatch = "ManufacturingBatch";
    }
}