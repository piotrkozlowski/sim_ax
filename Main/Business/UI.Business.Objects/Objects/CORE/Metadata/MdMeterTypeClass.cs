namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMeterTypeClass Metadata
	/// </summary>
	public class MdMeterTypeClass
	{
		public const string IdMeterTypeClass = "IdMeterTypeClass";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	