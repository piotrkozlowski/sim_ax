namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationConsumerHistory Metadata
	/// </summary>
	public class MdLocationConsumerHistory
	{
		public const string IdLocationConsumerHistory = "IdLocationConsumerHistory";
		public const string IdLocation = "IdLocation";
		public const string IdConsumer = "IdConsumer";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Notes = "Notes";
		public const string Location = "Location";
		public const string Consumer = "Consumer";
	
	}
}
	