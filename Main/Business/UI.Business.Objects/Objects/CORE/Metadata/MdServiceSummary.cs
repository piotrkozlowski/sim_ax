namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceSummary Metadata
	/// </summary>
	public class MdServiceSummary
	{
		public const string IdServiceList = "IdServiceList";
		public const string IdServicePackage = "IdServicePackage";
		public const string Amount = "Amount";
		public const string Cost = "Cost";
		public const string ServiceList = "ServiceList";
		public const string ServicePackage = "ServicePackage";
		public const string ServiceListSummary = "ServiceListSummary";
		public const string DataList = "DataList";
	
	}
}
	