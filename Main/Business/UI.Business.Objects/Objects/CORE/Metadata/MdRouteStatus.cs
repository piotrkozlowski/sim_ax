namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRouteStatus Metadata
	/// </summary>
	public class MdRouteStatus
	{
		public const string IdRouteStatus = "IdRouteStatus";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	