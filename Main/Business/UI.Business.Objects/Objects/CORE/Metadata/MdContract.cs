namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpContract Metadata
	/// </summary>
	public class MdContract
	{
		public const string IdContract = "IdContract";
		public const string Name = "Name";
		public const string Descr = "Descr";
		public const string IdDistributor = "IdDistributor";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string Distributor = "Distributor";
		public const string WarrantyContract = "WarrantyContract";
		public const string DataList = "DataList";
	
	}
}
	