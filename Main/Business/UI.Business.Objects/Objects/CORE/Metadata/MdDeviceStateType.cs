namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceStateType Metadata
	/// </summary>
	public class MdDeviceStateType
	{
		public const string IdDeviceStateType = "IdDeviceStateType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Description = "Description";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	