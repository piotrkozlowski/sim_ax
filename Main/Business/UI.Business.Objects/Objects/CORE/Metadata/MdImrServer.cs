namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpImrServer Metadata
    /// </summary>
    public class MdImrServer
    {
        public const string IdImrServer = "IdImrServer";
        public const string IpAddress = "IpAddress";
        public const string DnsName = "DnsName";
        public const string WebserviceAddress = "WebserviceAddress";
        public const string WebserviceTimeout = "WebserviceTimeout";
        public const string Login = "Login";
        public const string Password = "Password";
        public const string StandardDescription = "StandardDescription";
        public const string IsGood = "IsGood";
        public const string CreationDate = "CreationDate";
        public const string UtdcSsecUser = "UtdcSsecUser";
        public const string UtdcSsecPass = "UtdcSsecPass";
        public const string Collation = "Collation";
        public const string DbName = "DbName";
        public const string ImrServerVersion = "ImrServerVersion";
        public const string IsImrlt = "IsImrlt";
        public const string IsLocalhost = "IsLocalhost";
        public const string DataList = "DataList";
        public const string UserPortalWebServiceAddress = "UserPortalWebServiceAddress";
        public const string DBCollectorWebServiceAddress = "DBCollectorWebServiceAddress";
        public const string DwServerId = "DwServerId";
        public const string CoreServerId = "CoreServerId";
        public const string DistributorsExitingOnServer = "DistributorsExitingOnServer";
        public const string DistributorsAssignedToServer = "DistributorsAssignedToServer";
    }
}