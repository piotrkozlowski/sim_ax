namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionHistory Metadata
	/// </summary>
	public class MdVersionHistory
	{
		public const string IdVersionHistory = "IdVersionHistory";
		public const string IdVersion = "IdVersion";
		public const string IdVersionState = "IdVersionState";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string IdOperator = "IdOperator";
		public const string Notes = "Notes";
		public const string OpState = "OpState";
		public const string Version = "Version";
		public const string VersionState = "VersionState";
		public const string Operator = "Operator";
	
	}
}
	