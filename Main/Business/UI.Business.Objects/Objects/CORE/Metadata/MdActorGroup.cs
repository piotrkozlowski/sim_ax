namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActorGroup Metadata
	/// </summary>
	public class MdActorGroup
	{
		public const string IdActorGroup = "IdActorGroup";
		public const string Name = "Name";
		public const string BuiltIn = "BuiltIn";
		public const string IdDistributor = "IdDistributor";
		public const string IdDescr = "IdDescr";
		public const string Distributor = "Distributor";
		public const string Descr = "Descr";
		public const string OpState = "OpState";
	
	}
}
	