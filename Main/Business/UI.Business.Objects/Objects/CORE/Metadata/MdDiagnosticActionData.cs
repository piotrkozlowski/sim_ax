namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDiagnosticActionData Metadata
	/// </summary>
	public class MdDiagnosticActionData
	{
		public const string IdDiagnosticActionData = "IdDiagnosticActionData";
		public const string IdDiagnosticAction = "IdDiagnosticAction";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string OpState = "OpState";
		public const string DiagnosticAction = "DiagnosticAction";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	