namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceOrderNumber Metadata
	/// </summary>
	public class MdDeviceOrderNumber
	{
		public const string IdDeviceOrderNumber = "IdDeviceOrderNumber";
		public const string Name = "Name";
		public const string FirstQuarter = "FirstQuarter";
		public const string SecondQuarter = "SecondQuarter";
		public const string ThirdQuarter = "ThirdQuarter";
		public const string DataList = "DataList";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	