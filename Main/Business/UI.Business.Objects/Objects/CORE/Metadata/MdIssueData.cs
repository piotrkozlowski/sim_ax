namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpIssueData Metadata
	/// </summary>
	public class MdIssueData
	{
		public const string IdIssueData = "IdIssueData";
		public const string IdIssue = "IdIssue";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string Issue = "Issue";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string OpState = "OpState";
	
	}
}
	