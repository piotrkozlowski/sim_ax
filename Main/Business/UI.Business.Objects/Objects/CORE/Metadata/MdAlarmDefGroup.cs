namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmDefGroup Metadata
	/// </summary>
	public class MdAlarmDefGroup
	{
		public const string IdAlarmDef = "IdAlarmDef";
		public const string IdAlarmGroup = "IdAlarmGroup";
		public const string IdTransmissionType = "IdTransmissionType";
		public const string ConfirmLevel = "ConfirmLevel";
		public const string ConfirmTimeout = "ConfirmTimeout";
		public const string AlarmDef = "AlarmDef";
		public const string AlarmGroup = "AlarmGroup";
		public const string TransmissionType = "TransmissionType";
		public const string OpState = "OpState";
		public const string OldIdTransmissionType = "OldIdTransmissionType";	
	}
}
	