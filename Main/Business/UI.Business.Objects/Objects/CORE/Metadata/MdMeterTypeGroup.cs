namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMeterTypeGroup Metadata
	/// </summary>
	public class MdMeterTypeGroup
	{
		public const string IdMeterTypeGroup = "IdMeterTypeGroup";
		public const string Name = "Name";
		public const string Description = "Description";
	
	}
}
	