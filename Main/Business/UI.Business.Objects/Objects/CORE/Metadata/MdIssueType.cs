namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpIssueType Metadata
	/// </summary>
	public class MdIssueType
	{
		public const string IdIssueType = "IdIssueType";
		public const string IdDescr = "IdDescr";
		public const string Description = "Description";
		public const string Descr = "Descr";
	
	}
}
	