namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersion Metadata
	/// </summary>
	public class MdVersion
	{
		public const string IdVersion = "IdVersion";
		public const string IdVersionState = "IdVersionState";
		public const string IdVersionElementType = "IdVersionElementType";
		public const string IdVersionType = "IdVersionType";
		public const string VersionNbr = "VersionNbr";
		public const string VersionRaw = "VersionRaw";
		public const string ReleaseDate = "ReleaseDate";
		public const string IdOperatorPublisher = "IdOperatorPublisher";
		public const string NotificationDate = "NotificationDate";
		public const string IdOperatorNotifier = "IdOperatorNotifier";
		public const string ConfirmDate = "ConfirmDate";
		public const string IdOperatorCofirming = "IdOperatorCofirming";
		public const string ReleaseNotes = "ReleaseNotes";
		public const string IdPriority = "IdPriority";
		public const string Deadline = "Deadline";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string OpState = "OpState";
		public const string VersionState = "VersionState";
		public const string VersionElementType = "VersionElementType";
		public const string VersionType = "VersionType";
		public const string Priority = "Priority";
		public const string OperatorNotifier = "OperatorNotifier";
		public const string OperatorPublisher = "OperatorPublisher";
		public const string ResultsAndConclusionsNote = "ResultsAndConclusionsNote";
		public const string ChangeScheduleNote = "ChangeScheduleNote";
		public const string ThreatAnalysisNote = "ThreatAnalysisNote";
		public const string InstallationPlanNote = "InstallationPlanNote";
		public const string RollbackPlanNote = "RollbackPlanNote";
		public const string TestsScenarioNote = "TestsScenarioNote";
		public const string InvolvedOperators = "InvolvedOperators";
		public const string EstimatedSystemDowntimeStartTime = "EstimatedSystemDowntimeStartTime";
		public const string EstimatedSystemDowntimeEndTime = "EstimatedSystemDowntimeEndTime";
		public const string EstimatedSystemDowntimeCompletionDate = "EstimatedSystemDowntimeCompletionDate";
		public const string NotificationNote = "NotificationNote";
		public const string VersionRollbackReasonNote = "VersionRollbackReasonNote";
		public const string VersionValidationSteps = "VersionValidationSteps";
		public const string InstallationDate = "InstallationDate";
		public const string DataList = "DataList";
		public const string Key = "Key";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	