namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTaskHistory Metadata
	/// </summary>
	public class MdTaskHistory
	{
		public const string IdTaskHistory = "IdTaskHistory";
		public const string IdTask = "IdTask";
		public const string IdTaskStatus = "IdTaskStatus";
		public const string IdOperator = "IdOperator";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string Notes = "Notes";
		public const string Task = "Task";
		public const string TaskStatus = "TaskStatus";
		public const string Operator = "Operator";
	
	}
}
	