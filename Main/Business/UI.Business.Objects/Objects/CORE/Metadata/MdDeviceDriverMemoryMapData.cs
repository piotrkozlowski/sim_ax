namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceDriverMemoryMapData Metadata
	/// </summary>
	public class MdDeviceDriverMemoryMapData
	{
		public const string IdDeviceDriverMemoryMapData = "IdDeviceDriverMemoryMapData";
		public const string IdDdmmdParent = "IdDdmmdParent";
		public const string IdDeviceDriverMemoryMap = "IdDeviceDriverMemoryMap";
		public const string IdOmb = "IdOmb";
		public const string Name = "Name";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string IsRead = "IsRead";
		public const string IsWrite = "IsWrite";
		public const string IsPutDataOmb = "IsPutDataOmb";
		public const string IdDdmmdPutData = "IdDdmmdPutData";
		public const string IdDescr = "IdDescr";
		public const string DeviceDriverMemoryMap = "DeviceDriverMemoryMap";
		public const string DataType = "DataType";
		public const string Descr = "Descr";
	
	}
}
	