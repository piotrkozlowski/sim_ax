namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceSimCardHistory Metadata
	/// </summary>
	public class MdDeviceSimCardHistory
	{
		public const string IdDeviceSimCardHistory = "IdDeviceSimCardHistory";
		public const string SerialNbr = "SerialNbr";
		public const string IdSimCard = "IdSimCard";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Notes = "Notes";
		public const string SimCard = "SimCard";
	
	}
}
	