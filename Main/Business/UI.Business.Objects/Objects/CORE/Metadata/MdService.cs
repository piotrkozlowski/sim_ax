namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpService Metadata
	/// </summary>
	public class MdService
	{
		public const string IdService = "IdService";
		public const string Name = "Name";
		public const string IdDeviceOrderNumber = "IdDeviceOrderNumber";
		public const string DeleteDate = "DeleteDate";
		public const string IdDescr = "IdDescr";
		public const string DeviceOrderNumber = "DeviceOrderNumber";
		public const string Descr = "Descr";
		public const string DiagnosticActions = "DiagnosticActions";
		public const string AllLanguagesDescription = "AllLanguagesDescription";
		public const string DataList = "DataList";
	
	}
}
	