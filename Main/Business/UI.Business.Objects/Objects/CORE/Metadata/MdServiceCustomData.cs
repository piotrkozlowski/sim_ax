namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceCustomData Metadata
	/// </summary>
	public class MdServiceCustomData
	{
		public const string IdServiceCustomData = "IdServiceCustomData";
		public const string IdServiceCustom = "IdServiceCustom";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string ServiceCustom = "ServiceCustom";
		public const string DataType = "DataType";
	
	}
}
	