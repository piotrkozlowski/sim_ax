namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionElementData Metadata
	/// </summary>
	public class MdVersionElementData
	{
		public const string IdVersionElementData = "IdVersionElementData";
		public const string IdVersionElement = "IdVersionElement";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string VersionElement = "VersionElement";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string ReferencedObject = "ReferencedObject";
		public const string OpState = "OpState";
	
	}
}
	