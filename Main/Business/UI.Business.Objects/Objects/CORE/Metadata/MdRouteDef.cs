namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRouteDef Metadata
	/// </summary>
	public class MdRouteDef
	{
		public const string IdRouteDef = "IdRouteDef";
		public const string Name = "Name";
		public const string Description = "Description";
		public const string CreationDate = "CreationDate";
		public const string ExpirationDate = "ExpirationDate";
		public const string DeleteDate = "DeleteDate";
		public const string IdRouteType = "IdRouteType";
		public const string IdRouteStatus = "IdRouteStatus";
		public const string IdOperator = "IdOperator";
		public const string AdHoc = "AdHoc";
		public const string RouteType = "RouteType";
		public const string RouteStatus = "RouteStatus";
		public const string Operator = "Operator";
		public const string DataList = "DataList";
		public const string OpState = "OpState";
	
	}
}
	