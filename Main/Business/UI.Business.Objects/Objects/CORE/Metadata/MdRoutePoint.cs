namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRoutePoint Metadata
	/// </summary>
	public class MdRoutePoint
	{
		public const string IdRoutePoint = "IdRoutePoint";
		public const string IdRoute = "IdRoute";
		public const string Type = "Type";
		public const string IdTask = "IdTask";
		public const string StartDateTime = "StartDateTime";
		public const string EndDateTime = "EndDateTime";
		public const string Latitude = "Latitude";
		public const string Longitude = "Longitude";
		public const string KmCounter = "KmCounter";
		public const string Quote = "Quote";
		public const string InvoiceNumber = "InvoiceNumber";
		public const string Notes = "Notes";
		public const string RowVersion = "RowVersion";
		public const string IdDistributor = "IdDistributor";
		public const string Route = "Route";
		public const string Task = "Task";
		public const string Distributor = "Distributor";
		public const string TypeString = "TypeString";
		public const string DataList = "DataList";
	
	}
}
	