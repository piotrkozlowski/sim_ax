namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTemplateData Metadata
	/// </summary>
	public class MdDeviceTemplateData
	{
		public const string IdTemplate = "IdTemplate";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
		public const string DataStatus = "DataStatus";
	
	}
}
	