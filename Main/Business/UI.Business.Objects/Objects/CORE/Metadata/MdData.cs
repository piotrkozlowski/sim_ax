namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpData Metadata
	/// </summary>
	public class MdData
	{
		public const string IdData = "IdData";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string DataStatus = "DataStatus";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
		public const string Unit = "Unit";
		public const string ValueUnit = "ValueUnit";
	
	}
}
	