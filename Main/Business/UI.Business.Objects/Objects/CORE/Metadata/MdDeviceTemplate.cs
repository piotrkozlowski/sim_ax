namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTemplate Metadata
	/// </summary>
	public class MdDeviceTemplate
	{
		public const string IdTemplate = "IdTemplate";
		public const string IdTemplatePattern = "IdTemplatePattern";
		public const string IdDeviceType = "IdDeviceType";
		public const string FirmwareVersion = "FirmwareVersion";
		public const string Name = "Name";
		public const string CreatedBy = "CreatedBy";
		public const string CreationDate = "CreationDate";
		public const string ConfirmedBy = "ConfirmedBy";
		public const string ConfirmDate = "ConfirmDate";
		public const string DeviceType = "DeviceType";
		public const string TemplatePattern = "TemplatePattern";
		public const string CreatedOperator = "CreatedOperator";
		public const string ConfirmedOperator = "ConfirmedOperator";
		public const string DataList = "DataList";
	
	}
}
	