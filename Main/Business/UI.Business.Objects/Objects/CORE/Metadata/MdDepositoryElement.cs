namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDepositoryElement Metadata
	/// </summary>
	public class MdDepositoryElement
	{
		public const string IdDepositoryElement = "IdDepositoryElement";
		public const string IdLocation = "IdLocation";
		public const string IdTask = "IdTask";
		public const string IdPackage = "IdPackage";
		public const string IdArticle = "IdArticle";
		public const string IdDeviceTypeClass = "IdDeviceTypeClass";
		public const string IdDeviceStateType = "IdDeviceStateType";
		public const string SerialNbr = "SerialNbr";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string Removed = "Removed";
		public const string Ordered = "Ordered";
		public const string Accepted = "Accepted";
		public const string Notes = "Notes";
		public const string ExternalSn = "ExternalSn";
		public const string IdReference = "IdReference";
		public const string Location = "Location";
		public const string Task = "Task";
		public const string Package = "Package";
		public const string Article = "Article";
		public const string DeviceTypeClass = "DeviceTypeClass";
		public const string DeviceStateType = "DeviceStateType";
		public const string Device = "Device";
		public const string DeviceDetails = "DeviceDetails";
		public const string DataList = "DataList";
		public const string Count = "Count";
		public const string CustomLocation = "CustomLocation";
	
	}
}
	