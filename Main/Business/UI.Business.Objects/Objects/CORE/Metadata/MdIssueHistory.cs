namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpIssueHistory Metadata
	/// </summary>
	public class MdIssueHistory
	{
		public const string IdIssueHistory = "IdIssueHistory";
		public const string IdIssue = "IdIssue";
		public const string IdIssueStatus = "IdIssueStatus";
		public const string IdOperator = "IdOperator";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string Notes = "Notes";
		public const string ShortDescr = "ShortDescr";
		public const string Deadline = "Deadline";
		public const string IdPriority = "IdPriority";
		public const string RowVersion = "RowVersion";
		public const string Issue = "Issue";
		public const string IssueStatus = "IssueStatus";
		public const string Operator = "Operator";
		public const string Priority = "Priority";
	
	}
}
	