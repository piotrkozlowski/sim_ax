namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTaskData Metadata
	/// </summary>
	public class MdTaskData
	{
		public const string IdTaskData = "IdTaskData";
		public const string IdTask = "IdTask";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string Task = "Task";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string OpState = "OpState";
	
	}
}
	