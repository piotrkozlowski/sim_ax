namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionElementTypeData Metadata
	/// </summary>
	public class MdVersionElementTypeData
	{
		public const string IdVersionElementTypeData = "IdVersionElementTypeData";
		public const string IdVersionElementType = "IdVersionElementType";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string VersionElementType = "VersionElementType";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string OpState = "OpState";
		public const string ReferencedObject = "ReferencedObject";
	
	}
}
	