namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataFormatGroupDetails Metadata
	/// </summary>
	public class MdDataFormatGroupDetails
	{
		public const string IdDataFormatGroup = "IdDataFormatGroup";
		public const string IdDataType = "IdDataType";
		public const string IdDataTypeFormatIn = "IdDataTypeFormatIn";
		public const string IdDataTypeFormatOut = "IdDataTypeFormatOut";
		public const string IdUnitIn = "IdUnitIn";
		public const string IdUnitOut = "IdUnitOut";
		public const string DataFormatGroup = "DataFormatGroup";
		public const string DataType = "DataType";
		public const string UnitIn = "UnitIn";
		public const string UnitOut = "UnitOut";
		public const string DataTypeFormatIn = "DataTypeFormatIn";
		public const string DataTypeFormatOut = "DataTypeFormatOut";
		public const string OpState = "OpState";
	
	}
}
	