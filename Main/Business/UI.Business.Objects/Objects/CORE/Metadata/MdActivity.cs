namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActivity Metadata
	/// </summary>
	public class MdActivity
	{
		public const string IdActivity = "IdActivity";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdReferenceType = "IdReferenceType";
		public const string Descr = "Descr";
		public const string ReferenceType = "ReferenceType";
		public const string OpState = "OpState";
		public const string ReferenceValue = "ReferenceValue";
		public const string Deny = "Deny";
		public const string Allow = "Allow";
	
	}
}
	