namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceHierarchy Metadata
	/// </summary>
	public class MdDeviceHierarchy
	{
		public const string IdDeviceHierarchy = "IdDeviceHierarchy";
		public const string SerialNbrParent = "SerialNbrParent";
		public const string IdSlotType = "IdSlotType";
		public const string SlotNbr = "SlotNbr";
		public const string IdProtocolIn = "IdProtocolIn";
		public const string IdProtocolOut = "IdProtocolOut";
		public const string SerialNbr = "SerialNbr";
		public const string IsActive = "IsActive";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string DeviceParent = "DeviceParent";
		public const string SlotType = "SlotType";
		public const string ProtocolIn = "ProtocolIn";
		public const string ProtocolOut = "ProtocolOut";
		public const string Device = "Device";
		public const string OpState = "OpState";
	
	}
}
	