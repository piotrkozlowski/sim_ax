namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceListDeviceDiagnostic Metadata
	/// </summary>
	public class MdServiceListDeviceDiagnostic
	{
		public const string SerialNbr = "SerialNbr";
		public const string IdServiceList = "IdServiceList";
		public const string IdDiagnosticAction = "IdDiagnosticAction";
		public const string IdDiagnosticActionResult = "IdDiagnosticActionResult";
		public const string InputText = "InputText";
		public const string DamageLevel = "DamageLevel";
		public const string ServiceList = "ServiceList";
		public const string DiagnosticAction = "DiagnosticAction";
		public const string DiagnosticActionResult = "DiagnosticActionResult";
		public const string DiagnosticActionResultValue = "DiagnosticActionResultValue";
	
	}
}
	