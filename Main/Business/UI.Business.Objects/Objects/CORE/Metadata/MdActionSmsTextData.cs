namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionSmsTextData Metadata
	/// </summary>
	public class MdActionSmsTextData
	{
		public const string IdActionSmsText = "IdActionSmsText";
		public const string IdLanguage = "IdLanguage";
		public const string ArgNbr = "ArgNbr";
		public const string IdDataType = "IdDataType";
		public const string IdUnit = "IdUnit";
		public const string ActionSmsText = "ActionSmsText";
		public const string Language = "Language";
		public const string DataType = "DataType";
		public const string Unit = "Unit";
		public const string OpState = "OpState";
	
	}
}
	