namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTypeInGroup Metadata
	/// </summary>
	public class MdDeviceTypeInGroup
	{
		public const string IdDeviceTypeGroup = "IdDeviceTypeGroup";
		public const string IdDeviceType = "IdDeviceType";
		public const string DeviceTypeGroup = "DeviceTypeGroup";
		public const string DeviceType = "DeviceType";
		public const string OpState = "OpState";
	
	}
}
	