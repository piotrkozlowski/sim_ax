namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpArticle Metadata
	/// </summary>
	public class MdArticle
	{
		public const string IdArticle = "IdArticle";
		public const string ExternalId = "ExternalId";
		public const string Name = "Name";
		public const string Description = "Description";
		public const string IsUnique = "IsUnique";
		public const string Visible = "Visible";
		public const string IsAiut = "IsAiut";
		public const string GridLookUpEditString = "GridLookUpEditString";
	
	}
}
	