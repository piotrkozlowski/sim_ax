namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationHierarchy Metadata
	/// </summary>
	public class MdLocationHierarchy
	{
		public const string IdLocationHierarchy = "IdLocationHierarchy";
		public const string IdLocationHierarchyParent = "IdLocationHierarchyParent";
		public const string IdLocation = "IdLocation";
		public const string Hierarchy = "Hierarchy";
		public const string Location = "Location";
		public const string LocationHierarchyParent = "LocationHierarchyParent";
	
	}
}
	