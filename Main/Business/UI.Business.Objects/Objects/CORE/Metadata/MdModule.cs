namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpModule Metadata
	/// </summary>
	public class MdModule
	{
		public const string IdModule = "IdModule";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
		public const string DataList = "DataList";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
        public const string ComboBoxString = "ComboBoxString";
	
	}
}
	