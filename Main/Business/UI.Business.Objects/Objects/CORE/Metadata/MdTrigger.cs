namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTrigger Metadata
	/// </summary>
	public class MdTrigger
	{
		public const string IdTrigger = "IdTrigger";
		public const string TableName = "TableName";
		public const string Audit = "Audit";
		public const string DataChange = "DataChange";
		public const string DictTable = "DictTable";
		public const string ObjectTable = "ObjectTable";
		public const string DataTable = "DataTable";
		public const string OpState = "OpState";
	
	}
}
	