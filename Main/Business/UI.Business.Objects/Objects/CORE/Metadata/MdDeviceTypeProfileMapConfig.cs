namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTypeProfileMapConfig Metadata
	/// </summary>
	public class MdDeviceTypeProfileMapConfig
	{
		public const string IdDeviceTypeProfileMap = "IdDeviceTypeProfileMap";
		public const string IdDeviceTypeProfileStep = "IdDeviceTypeProfileStep";
		public const string IdDeviceTypeProfileStepKind = "IdDeviceTypeProfileStepKind";
		public const string DeviceTypeProfileMap = "DeviceTypeProfileMap";
		public const string DeviceTypeProfileStep = "DeviceTypeProfileStep";
		public const string DeviceTypeProfileStepKind = "DeviceTypeProfileStepKind";
		public const string OpState = "OpState";
	
	}
}
	