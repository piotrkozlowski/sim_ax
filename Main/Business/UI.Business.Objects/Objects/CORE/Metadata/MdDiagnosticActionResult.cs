namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDiagnosticActionResult Metadata
	/// </summary>
	public class MdDiagnosticActionResult
	{
		public const string IdDiagnosticActionResult = "IdDiagnosticActionResult";
		public const string IdDiagnosticAction = "IdDiagnosticAction";
		public const string Name = "Name";
		public const string AllowInputText = "AllowInputText";
		public const string DamageLevel = "DamageLevel";
		public const string IdDescr = "IdDescr";
		public const string IdServiceReferenceType = "IdServiceReferenceType";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string DiagnosticAction = "DiagnosticAction";
		public const string Descr = "Descr";
		public const string ServiceReferenceType = "ServiceReferenceType";
		public const string AllLanguagesDescription = "AllLanguagesDescription";
	
	}
}
	