namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpProfileData Metadata
	/// </summary>
	public class MdProfileData
	{
		public const string IdProfileData = "IdProfileData";
		public const string IdProfile = "IdProfile";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string Profile = "Profile";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
		public const string OpState = "OpState";
	
	}
}
	