namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceDiagnosticResult Metadata
	/// </summary>
	public class MdServiceDiagnosticResult
	{
		public const string IdServiceDiagnosticResult = "IdServiceDiagnosticResult";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IsServiceResult = "IsServiceResult";
		public const string IsDiagnosticResult = "IsDiagnosticResult";
		public const string Descr = "Descr";
	
	}
}
	