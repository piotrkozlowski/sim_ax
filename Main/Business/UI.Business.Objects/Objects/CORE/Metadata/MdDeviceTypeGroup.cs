namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTypeGroup Metadata
	/// </summary>
	public class MdDeviceTypeGroup
	{
		public const string IdDeviceTypeGroup = "IdDeviceTypeGroup";
		public const string Name = "Name";
		public const string Description = "Description";
	
	}
}
	