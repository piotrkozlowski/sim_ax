namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarm Metadata
	/// </summary>
	public class MdAlarm
	{
		public const string IdAlarm = "IdAlarm";
		public const string IdAlarmEvent = "IdAlarmEvent";
		public const string IdOperator = "IdOperator";
		public const string IdAlarmGroup = "IdAlarmGroup";
		public const string IdAlarmStatus = "IdAlarmStatus";
		public const string IdTransmissionType = "IdTransmissionType";
		public const string AlarmEvent = "AlarmEvent";
		public const string Operator = "Operator";
		public const string AlarmGroup = "AlarmGroup";
		public const string AlarmStatus = "AlarmStatus";
		public const string TransmissionType = "TransmissionType";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	