namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpReferenceType Metadata
	/// </summary>
	public class MdReferenceType
	{
		public const string IdReferenceType = "IdReferenceType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	