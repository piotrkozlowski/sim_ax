namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataArch Metadata
	/// </summary>
	public class MdDataArch
	{
		public const string IdDataArch = "IdDataArch";
		public const string SerialNbr = "SerialNbr";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string Time = "Time";
		public const string IsAction = "IsAction";
		public const string IsAlarm = "IsAlarm";
		public const string IdPacket = "IdPacket";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string DataType = "DataType";
	
	}
}
	