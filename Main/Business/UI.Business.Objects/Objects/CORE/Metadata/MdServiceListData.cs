namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceListData Metadata
	/// </summary>
	public class MdServiceListData
	{
		public const string IdServiceListData = "IdServiceListData";
		public const string IdServiceList = "IdServiceList";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string OpState = "OpState";
		public const string ServiceList = "ServiceList";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	