namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationSupplier Metadata
	/// </summary>
	public class MdLocationSupplier
	{
		public const string IdLocationSupplier = "IdLocationSupplier";
		public const string IdLocation = "IdLocation";
		public const string IdDistributorSupplier = "IdDistributorSupplier";
		public const string AccountNo = "AccountNo";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Notes = "Notes";
		public const string Location = "Location";
		public const string DistributorSupplier = "DistributorSupplier";
	
	}
}
	