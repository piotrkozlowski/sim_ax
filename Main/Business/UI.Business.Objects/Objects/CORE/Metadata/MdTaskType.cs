namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTaskType Metadata
	/// </summary>
	public class MdTaskType
	{
		public const string IdTaskType = "IdTaskType";
		public const string IdTaskTypeGroup = "IdTaskTypeGroup";
		public const string Name = "Name";
		public const string Descr = "Descr";
		public const string Remote = "Remote";
		public const string DurationEstimate = "DurationEstimate";
		public const string IdOperatorCreator = "IdOperatorCreator";
		public const string CreationDate = "CreationDate";
		public const string Authorized = "Authorized";
		public const string ImrOperator = "ImrOperator";
		public const string RowVersion = "RowVersion";
		public const string TaskTypeGroup = "TaskTypeGroup";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	