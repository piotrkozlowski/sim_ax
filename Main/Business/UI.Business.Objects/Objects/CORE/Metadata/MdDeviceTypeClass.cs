namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTypeClass Metadata
	/// </summary>
	public class MdDeviceTypeClass
	{
		public const string IdDeviceTypeClass = "IdDeviceTypeClass";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	