namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpContractData Metadata
	/// </summary>
	public class MdContractData
	{
		public const string IdContractData = "IdContractData";
		public const string IdContract = "IdContract";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string OpState = "OpState";
		public const string Contract = "Contract";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	