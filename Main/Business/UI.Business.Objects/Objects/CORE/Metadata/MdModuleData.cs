namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpModuleData Metadata
	/// </summary>
	public class MdModuleData
	{
		public const string IdModuleData = "IdModuleData";
		public const string IdModule = "IdModule";
		public const string IdDataType = "IdDataType";
		public const string Value = "Value";
		public const string Module = "Module";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
		public const string OpState = "OpState";
	
	}
}
	