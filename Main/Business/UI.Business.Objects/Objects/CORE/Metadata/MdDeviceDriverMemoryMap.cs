namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceDriverMemoryMap Metadata
	/// </summary>
	public class MdDeviceDriverMemoryMap
	{
		public const string IdDeviceDriverMemoryMap = "IdDeviceDriverMemoryMap";
		public const string IdDeviceDriver = "IdDeviceDriver";
		public const string FileName = "FileName";
		public const string FileVersion = "FileVersion";
		public const string DeviceDriver = "DeviceDriver";
	
	}
}
	