namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationTariff Metadata
	/// </summary>
	public class MdLocationTariff
	{
		public const string IdLocationTariff = "IdLocationTariff";
		public const string IdLocation = "IdLocation";
		public const string IdTariff = "IdTariff";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Notes = "Notes";
		public const string Location = "Location";
		public const string Tariff = "Tariff";
	
	}
}
	