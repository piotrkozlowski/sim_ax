namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMessage Metadata
	/// </summary>
	public class MdMessage
	{
		public const string IdMessage = "IdMessage";
		public const string IdMessageType = "IdMessageType";
		public const string IdMessageParent = "IdMessageParent";
		public const string IdOperator = "IdOperator";
		public const string MessageContent = "MessageContent";
		public const string HasAttachment = "HasAttachment";
		public const string Points = "Points";
		public const string Hierarchy = "Hierarchy";
		public const string InsertDate = "InsertDate";
		public const string Operator = "Operator";
		public const string DataList = "DataList";
	
	}
}
	