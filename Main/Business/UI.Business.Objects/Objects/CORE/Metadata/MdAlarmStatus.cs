namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmStatus Metadata
	/// </summary>
	public class MdAlarmStatus
	{
		public const string IdAlarmStatus = "IdAlarmStatus";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	