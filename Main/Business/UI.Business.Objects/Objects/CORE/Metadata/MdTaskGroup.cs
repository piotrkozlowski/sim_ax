namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTaskGroup Metadata
	/// </summary>
	public class MdTaskGroup
	{
		public const string IdTaskGroup = "IdTaskGroup";
		public const string IdParrentGroup = "IdParrentGroup";
		public const string Name = "Name";
		public const string DateCreated = "DateCreated";
		public const string IdDistributor = "IdDistributor";
		public const string ParentGroup = "ParentGroup";
		public const string Distributor = "Distributor";
	
	}
}
	