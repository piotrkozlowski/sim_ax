namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActor Metadata
	/// </summary>
	public class MdActor
	{
		public const string IdActor = "IdActor";
		public const string Name = "Name";
		public const string Surname = "Surname";
		public const string City = "City";
		public const string Address = "Address";
		public const string Postcode = "Postcode";
		public const string Email = "Email";
		public const string Mobile = "Mobile";
		public const string Phone = "Phone";
		public const string IdLanguage = "IdLanguage";
		public const string Description = "Description";
		public const string Avatar = "Avatar";
		public const string Language = "Language";
		public const string OpState = "OpState";
		public const string GridLookUpEditString = "GridLookUpEditString";
		public const string FullInfo = "FullInfo";
		public const string FullName = "FullName";
	
	}
}
	