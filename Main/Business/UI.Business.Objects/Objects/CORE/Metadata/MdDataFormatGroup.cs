namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataFormatGroup Metadata
	/// </summary>
	public class MdDataFormatGroup
	{
		public const string IdDataFormatGroup = "IdDataFormatGroup";
		public const string Name = "Name";
		public const string IdModule = "IdModule";
		public const string Module = "Module";
	
	}
}
	