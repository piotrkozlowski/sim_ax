namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpSlotType Metadata
	/// </summary>
	public class MdSlotType
	{
		public const string IdSlotType = "IdSlotType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	