namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConfigurationProfileDeviceType Metadata
	/// </summary>
	public class MdConfigurationProfileDeviceType
	{
		public const string IdConfigurationProfile = "IdConfigurationProfile";
		public const string IdDeviceType = "IdDeviceType";
		public const string ConfigurationProfile = "ConfigurationProfile";
		public const string DeviceType = "DeviceType";
	
	}
}
	