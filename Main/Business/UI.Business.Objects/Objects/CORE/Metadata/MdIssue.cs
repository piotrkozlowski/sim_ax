namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpIssue Metadata
    /// </summary>
    public class MdIssue
    {
        public const string IdIssue = "IdIssue";
        public const string IdActor = "IdActor";
        public const string IdOperatorRegistering = "IdOperatorRegistering";
        public const string IdOperatorPerformer = "IdOperatorPerformer";
        public const string IdIssueType = "IdIssueType";
        public const string IdIssueStatus = "IdIssueStatus";
        public const string IdPrevoiusIssueStatus = "IdPrevoiusIssueStatus";
        public const string RealizationDate = "RealizationDate";
        public const string CreationDate = "CreationDate";
        public const string ShortDescr = "ShortDescr";
        public const string LongDescr = "LongDescr";
        public const string IdLocation = "IdLocation";
        public const string Deadline = "Deadline";
        public const string IdPriority = "IdPriority";
        public const string RowVersion = "RowVersion";
        public const string IdDistributor = "IdDistributor";
        public const string OpState = "OpState";
        public const string Actor = "Actor";
        public const string IssueType = "IssueType";
        public const string IssueStatus = "IssueStatus";
        public const string Distributor = "Distributor";
        public const string Location = "Location";
        public const string Priority = "Priority";
        public const string OperatorRegistering = "OperatorRegistering";
        public const string OperatorPerformer = "OperatorPerformer";
        public const string IssueStatusChanged = "IssueStatusChanged";
        public const string GridLookUpEditString = "GridLookUpEditString";
        public const string IdWorkOrderJCI = "IdWorkOrderJCI";
        public const string IMRMySCVisible = "IMRMySCVisible";
        public const string WorkOrderJCI = "WorkOrderJCI";
        public const string JCI = "JCI";
        public const string DataList = "DataList";
        public const string ShortDescrCollapsed = "ShortDescrCollapsed";
        public const string IssueHistory = "IssueHistory";
        public const string Overdue = "Overdue";
        public const string OverdueColor = "OverdueColor";
        public const string DynamicProperties = "DynamicProperties";
    }
}