namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActorInGroup Metadata
	/// </summary>
	public class MdActorInGroup
	{
		public const string IdActor = "IdActor";
		public const string IdActorGroup = "IdActorGroup";
		public const string Actor = "Actor";
		public const string ActorGroup = "ActorGroup";
		public const string OpState = "OpState";
	
	}
}
	