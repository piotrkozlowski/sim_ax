namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpSimCard Metadata
	/// </summary>
	public class MdSimCard
	{
        public const string IdSimCard = "IdSimCard";
        public const string IdDistributor = "IdDistributor";
        public const string SerialNbr = "SerialNbr";
        public const string Phone = "Phone";
        public const string Pin = "Pin";
        public const string Puk = "Puk";
        public const string MobileNetworkCode = "MobileNetworkCode";
        public const string Ip = "Ip";
        public const string ApnLogin = "ApnLogin";
        public const string ApnPassword = "ApnPassword";
        public const string Distributor = "Distributor";
        public const string Apn = "Apn";
        public const string Topic = "Topic";
        public const string CollectionDate = "CollectionDate";//data pobrania
        public const string ContractConcludeDate = "ContractConcludeDate";//data podpisania umowy
        public const string DuplicateRequestDate = "DuplicateRequestDate";
        public const string DuplicateIsRequested = "DuplicateIsRequested";
        public const string DuplicateOrderDate = "DuplicateOrderDate";
        public const string DuplicateIsOrdered = "DuplicateIsOrdered";
        public const string DuplicateProductionDate = "DuplicateProductionDate";
        public const string DuplicateIsProduced = "DuplicateIsProduced";
        public const string DuplicateRequestOperator = "DuplicateRequestOperator";
        public const string DuplicateOrderOperator = "DuplicateOrderOperator";
        public const string DuplicateProductionOperator = "DuplicateProductionOperator";
        public const string PUK2 = "PUK2";
        public const string PIN2 = "PIN2";
        public const string HLR = "HLR";
        public const string CollectingPerson = "CollectingPerson";
        public const string AccountUE = "AccountUE";
        public const string AccountPL = "AccountPL";
        public const string GPRS = "GPRS";
        public const string GPRSOnDate = "GPRSOnDate";
        public const string GPRSOffDate = "GPRSOffDate";
        public const string AdditionallInfo = "AdditionallInfo";
        public const string OffDate = "OffDate";
        public const string ContractNbr = "ContractNbr";
        public const string ContractExpirationDate = "ContractExpirationDate";
        public const string ContractTerminationDate = "ContractTerminationDate";
        public const string Tariff = "Tariff";
        public const string OperatorTariff = "OperatorTariff";
        public const string DistributorOwner = "DistributorOwner";
        public const string IdDistributorOwner = "IdDistributorOwner";
        public const string IsActive = "IsActive";
        
	}
}
	