namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRoleGroup Metadata
	/// </summary>
	public class MdRoleGroup
	{
		public const string IdRoleParent = "IdRoleParent";
		public const string IdRole = "IdRole";
		public const string Role = "Role";
		public const string ParentRole = "ParentRole";
		public const string OpState = "OpState";
	
	}
}
	