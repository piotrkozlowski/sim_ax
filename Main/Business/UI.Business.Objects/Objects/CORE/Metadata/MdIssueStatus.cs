namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpIssueStatus Metadata
	/// </summary>
	public class MdIssueStatus
	{
		public const string IdIssueStatus = "IdIssueStatus";
		public const string IdDescr = "IdDescr";
		public const string Description = "Description";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	