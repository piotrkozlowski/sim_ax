﻿namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpSessionEventData Metadata
    /// </summary>
    public class MdSessionEventData
    {
        public const string IdSessionEventData = "IdSessionEventData";
        public const string IdSessionEvent = "IdSessionEvent";
        public const string IdDataType = "IdDataType";
        public const string Value = "Value";
        public const string SessionEvent = "SessionEvent";
        public const string DataType = "DataType";
        public const string IdData = "IdData";
        public const string Index = "Index";
        public const string OpState = "OpState";
        public const string ReferencedObject = "ReferencedObject";

    }
}
