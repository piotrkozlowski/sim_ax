namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConversion Metadata
	/// </summary>
	public class MdConversion
	{
		public const string IdConversion = "IdConversion";
		public const string IdReferenceType = "IdReferenceType";
		public const string ReferenceValue = "ReferenceValue";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Slope = "Slope";
		public const string Bias = "Bias";
		public const string ReferenceType = "ReferenceType";
		public const string DataType = "DataType";
	
	}
}
	