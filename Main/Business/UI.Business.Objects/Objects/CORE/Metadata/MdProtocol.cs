namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpProtocol Metadata
	/// </summary>
	public class MdProtocol
	{
		public const string IdProtocol = "IdProtocol";
		public const string Name = "Name";
		public const string IdProtocolDriver = "IdProtocolDriver";
		public const string ProtocolDriver = "ProtocolDriver";
	
	}
}
	