namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpFaultCode Metadata
	/// </summary>
	public class MdFaultCode
	{
		public const string IdFaultCode = "IdFaultCode";
		public const string IdFaultCodeType = "IdFaultCodeType";
		public const string IdFaultCodeParent = "IdFaultCodeParent";
		public const string Name = "Name";
		public const string IdClientDescr = "IdClientDescr";
		public const string IdCompanyDescr = "IdCompanyDescr";
		public const string Priority = "Priority";
		public const string FaultCodeHierarchy = "FaultCodeHierarchy";
		public const string FaultCodeType = "FaultCodeType";
		public const string DataList = "DataList";
		public const string IsBigFault = "IsBigFault";
		public const string ClientDescriptionPL = "ClientDescriptionPL";
		public const string CompanyDescriptionPL = "CompanyDescriptionPL";
		public const string ClientDescriptionEN = "ClientDescriptionEN";
		public const string CompanyDescriptionEN = "CompanyDescriptionEN";
	
	}
}
	