namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmTextDataType Metadata
	/// </summary>
	public class MdAlarmTextDataType
	{
		public const string IdAlarmText = "IdAlarmText";
		public const string IdLanguage = "IdLanguage";
		public const string ArgNbr = "ArgNbr";
		public const string IdDataType = "IdDataType";
		public const string IdUnit = "IdUnit";
		public const string AlarmText = "AlarmText";
		public const string Language = "Language";
		public const string DataType = "DataType";
		public const string Unit = "Unit";
		public const string OpState = "OpState";
	
	}
}
	