namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmData Metadata
	/// </summary>
	public class MdAlarmData
	{
		public const string IdAlarmData = "IdAlarmData";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	