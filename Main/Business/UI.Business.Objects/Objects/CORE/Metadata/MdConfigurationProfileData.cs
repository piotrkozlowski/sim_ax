namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConfigurationProfileData Metadata
	/// </summary>
	public class MdConfigurationProfileData
	{
		public const string IdConfigurationProfileData = "IdConfigurationProfileData";
		public const string IdConfigurationProfile = "IdConfigurationProfile";
		public const string IdDeviceType = "IdDeviceType";
		public const string IdDeviceTypeProfileStep = "IdDeviceTypeProfileStep";
		public const string IdDataType = "IdDataType";
		public const string Index = "Index";
		public const string Value = "Value";
		public const string IdData = "IdData";
		public const string DeviceType = "DeviceType";
		public const string DeviceTypeProfileStep = "DeviceTypeProfileStep";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
	
	}
}
	