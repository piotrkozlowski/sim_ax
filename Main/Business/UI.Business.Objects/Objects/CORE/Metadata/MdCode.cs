namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpCode Metadata
	/// </summary>
	public class MdCode
	{
		public const string IdCode = "IdCode";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdCodeType = "IdCodeType";
		public const string CodeNbr = "CodeNbr";
		public const string Code = "Code";
		public const string IsActive = "IsActive";
		public const string ActivationTime = "ActivationTime";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string CodeType = "CodeType";
		public const string OpState = "OpState";
	
	}
}
	