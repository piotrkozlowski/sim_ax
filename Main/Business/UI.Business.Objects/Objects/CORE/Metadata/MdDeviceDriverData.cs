namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceDriverData Metadata
	/// </summary>
	public class MdDeviceDriverData
	{
		public const string IdDeviceDriverData = "IdDeviceDriverData";
		public const string IdDeviceDriver = "IdDeviceDriver";
		public const string IdDataType = "IdDataType";
		public const string Value = "Value";
		public const string Index = "Index";
		public const string DeviceDriver = "DeviceDriver";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string OpState = "OpState";
	
	}
}
	