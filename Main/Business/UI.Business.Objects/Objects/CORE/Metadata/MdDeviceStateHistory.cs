namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceStateHistory Metadata
	/// </summary>
	public class MdDeviceStateHistory
	{
		public const string IdDeviceState = "IdDeviceState";
		public const string SerialNbr = "SerialNbr";
		public const string IdDeviceStateType = "IdDeviceStateType";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Notes = "Notes";
		public const string DeviceStateType = "DeviceStateType";
		public const string Device = "Device";
		public const string OpState = "OpState";
	
	}
}
	