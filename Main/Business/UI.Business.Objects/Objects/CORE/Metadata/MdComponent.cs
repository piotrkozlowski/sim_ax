namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpComponent Metadata
	/// </summary>
	public class MdComponent
	{
		public const string IdComponent = "IdComponent";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	