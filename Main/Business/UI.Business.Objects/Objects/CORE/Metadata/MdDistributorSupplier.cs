namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDistributorSupplier Metadata
	/// </summary>
	public class MdDistributorSupplier
	{
		public const string IdDistributorSupplier = "IdDistributorSupplier";
		public const string IdDistributor = "IdDistributor";
		public const string IdSupplier = "IdSupplier";
		public const string IdProductCode = "IdProductCode";
		public const string IdActor = "IdActor";
		public const string Distributor = "Distributor";
		public const string Supplier = "Supplier";
		public const string ProductCode = "ProductCode";
		public const string Actor = "Actor";
	
	}
}
	