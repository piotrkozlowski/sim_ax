namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTariff Metadata
	/// </summary>
	public class MdTariff
	{
		public const string IdTariff = "IdTariff";
		public const string IdDistributor = "IdDistributor";
		public const string Name = "Name";
		public const string Distributor = "Distributor";
		public const string DT_ID_PRODUCT_CODE = "DT_ID_PRODUCT_CODE";
		public const string DT_CALORIFIC_VALUE = "DT_CALORIFIC_VALUE";
		public const string DT_STANDING_CHARGE = "DT_STANDING_CHARGE";
		public const string DT_STANDING_CHARGE_VAT = "DT_STANDING_CHARGE_VAT";
		public const string DT_CARBON_TAX = "DT_CARBON_TAX";
		public const string DT_CARBON_TAX_VAT = "DT_CARBON_TAX_VAT";
		public const string DT_PRODUCT_CHARGE = "DT_PRODUCT_CHARGE";
		public const string DT_PRODUCT_CHARGE_VAT = "DT_PRODUCT_CHARGE_VAT";
		public const string DT_NOTES = "DT_NOTES";
		public const string DT_PRICE_PREPAID_VARIABLE = "DT_PRICE_PREPAID_VARIABLE";
		public const string DT_PRICE_POSTPAID_FIXED_DAILY = "DT_PRICE_POSTPAID_FIXED_DAILY";
		public const string ActivationDate = "ActivationDate";
		public const string EffectiveUnitPrice = "EffectiveUnitPrice";
		public const string EffectiveUnitPriceSM = "EffectiveUnitPriceSM";
		public const string EffectiveStandingCharge = "EffectiveStandingCharge";
		public const string FriendlyName = "FriendlyName";
	
	}
}
	