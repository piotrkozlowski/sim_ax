namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionType Metadata
	/// </summary>
	public class MdActionType
	{
		public const string IdActionType = "IdActionType";
		public const string IdActionTypeClass = "IdActionTypeClass";
		public const string Name = "Name";
		public const string PluginName = "PluginName";
		public const string IdDescr = "IdDescr";
		public const string IsActionDataFixed = "IsActionDataFixed";
		public const string ActionTypeClass = "ActionTypeClass";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	