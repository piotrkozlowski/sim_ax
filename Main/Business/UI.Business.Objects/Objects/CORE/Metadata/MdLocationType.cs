namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationType Metadata
	/// </summary>
	public class MdLocationType
	{
		public const string IdLocationType = "IdLocationType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	