namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConfigurationProfile Metadata
	/// </summary>
	public class MdConfigurationProfile
	{
		public const string IdConfigurationProfile = "IdConfigurationProfile";
		public const string IdConfigurationProfileType = "IdConfigurationProfileType";
		public const string IdDistributor = "IdDistributor";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdOperatorCreated = "IdOperatorCreated";
		public const string CreatedTime = "CreatedTime";
		public const string ConfigurationProfileType = "ConfigurationProfileType";
		public const string Distributor = "Distributor";
		public const string Descr = "Descr";
		public const string OperatorCreated = "OperatorCreated";
		public const string DataList = "DataList";
		public const string ApproximatelySMSConsumption = "ApproximatelySMSConsumption";
		public const string ApproximatelyBatteryConsumption = "ApproximatelyBatteryConsumption";
		public const string RadioListenTime = "RadioListenTime";
		public const string DeviceWLEnabled = "DeviceWLEnabled";
		public const string AlevelAlarmLevelSingleEnableLoLo = "AlevelAlarmLevelSingleEnableLoLo";
		public const string AlevelAlarmLevelSingleEnableLo = "AlevelAlarmLevelSingleEnableLo";
		public const string AlevelAlarmLevelSingleEnableHi = "AlevelAlarmLevelSingleEnableHi";
		public const string AlevelAlarmLevelSingleEnableHiHi = "AlevelAlarmLevelSingleEnableHiHi";
		public const string AlevelAlarmLevelSingleEnableStepRel = "AlevelAlarmLevelSingleEnableStepRel";
		public const string AlevelAlarmLevelSingleEnableStepAbs = "AlevelAlarmLevelSingleEnableStepAbs";

        public const string AlevelAlarmLevelSingleEnableLoLoUP = "AlevelAlarmLevelSingleEnableLoLoUP";
        public const string AlevelAlarmLevelSingleEnableLoUP = "AlevelAlarmLevelSingleEnableLoUP";
        public const string AlevelAlarmLevelSingleEnableHiUP = "AlevelAlarmLevelSingleEnableHiUP";
        public const string AlevelAlarmLevelSingleEnableHiHiUP = "AlevelAlarmLevelSingleEnableHiHiUP";

        public const string AlevelAlarmLevelSingleEnableLoLoDOWM = "AlevelAlarmLevelSingleEnableLoLoDOWM";
        public const string AlevelAlarmLevelSingleEnableLoDOWM = "AlevelAlarmLevelSingleEnableLoDOWM";
        public const string AlevelAlarmLevelSingleEnableHiDOWM = "AlevelAlarmLevelSingleEnableHiDOWM";
        public const string AlevelAlarmLevelSingleEnableHiHiDOWM = "AlevelAlarmLevelSingleEnableHiHiDOWM";
        public const string AlevelAlarmLevelSingleEnableStepRelEmergency = "AlevelAlarmLevelSingleEnableStepRelEmergency";
        public const string AlevelAlarmLevelSingleHisteresis = "AlevelAlarmLevelSingleHisteresis";

        public const string AlevelAlarmLevelSingleLoLo = "AlevelAlarmLevelSingleLoLo";
        public const string AlevelAlarmLevelSingleLo = "AlevelAlarmLevelSingleLo";
        public const string AlevelAlarmLevelSingleHi = "AlevelAlarmLevelSingleHi";
        public const string AlevelAlarmLevelSingleHiHi = "AlevelAlarmLevelSingleHiHi";
        public const string AlevelAlarmLevelSingleStepRel = "AlevelAlarmLevelSingleStepRel";
		public const string AlevelAlarmLevelSingleStepAbs = "AlevelAlarmLevelSingleStepAbs";
		public const string AlevelAlarmLevelSingleBias = "AlevelAlarmLevelSingleBias";
		public const string AlevelAlarmLevelAvgEnableLoLo = "AlevelAlarmLevelAvgEnableLoLo";
		public const string AlevelAlarmLevelAvgEnableLo = "AlevelAlarmLevelAvgEnableLo";
		public const string AlevelAlarmLevelAvgEnableHi = "AlevelAlarmLevelAvgEnableHi";
		public const string AlevelAlarmLevelAvgEnableHiHi = "AlevelAlarmLevelAvgEnableHiHi";
		public const string AlevelAlarmLevelAvgEnableStepRel = "AlevelAlarmLevelAvgEnableStepRel";
		public const string AlevelAlarmLevelAvgEnableStepAbs = "AlevelAlarmLevelAvgEnableStepAbs";
		public const string AlevelAlarmLevelAvgLoLo = "AlevelAlarmLevelAvgLoLo";
		public const string AlevelAlarmLevelAvgLo = "AlevelAlarmLevelAvgLo";
		public const string AlevelAlarmLevelAvgHi = "AlevelAlarmLevelAvgHi";
		public const string AlevelAlarmLevelAvgHiHi = "AlevelAlarmLevelAvgHiHi";
		public const string AlevelAlarmLevelAvgStepRel = "AlevelAlarmLevelAvgStepRel";
		public const string AlevelAlarmLevelAvgStepAbs = "AlevelAlarmLevelAvgStepAbs";
		public const string AlevelAlarmLevelAvgBias = "AlevelAlarmLevelAvgBias";
		public const string AlevelNVREnableUp = "AlevelNVREnableUp";
		public const string AlevelNVREnableDown = "AlevelNVREnableDown";
		public const string DeviceDeadLinkEnableUp = "DeviceDeadLinkEnableUp";
		public const string DeviceDeadLinkEnableDown = "DeviceDeadLinkEnableDown";
		public const string DeviceLostLinkEnableUp = "DeviceLostLinkEnableUp";
		public const string DeviceLostLinkEnableDown = "DeviceLostLinkEnableDown";
		public const string DeviceRapidWindowWidth = "DeviceRapidWindowWidth";
		public const string DeviceRapidTriggerLevel = "DeviceRapidTriggerLevel";
		public const string DeviceRapidEnableForStart = "DeviceRapidEnableForStart";
		public const string DeviceRapidEnableForEnd = "DeviceRapidEnableForEnd";
		public const string AnalogArchiveLatchOffset = "AnalogArchiveLatchOffset";
		public const string AnalogArchiveRowsCount = "AnalogArchiveRowsCount";
		public const string AnalogArchiveLatchPeriod = "AnalogArchiveLatchPeriod";
		public const string AnalogArchiveValuesMin = "AnalogArchiveValuesMin";
		public const string AnalogArchiveValuesMax = "AnalogArchiveValuesMax";
		public const string AnalogArchiveValuesAvg = "AnalogArchiveValuesAvg";
		public const string AnalogArchiveReadoutType = "AnalogArchiveReadoutType";
		public const string AnalogArchiveEnableMeasurmentsCount = "AnalogArchiveEnableMeasurmentsCount";
		public const string AnalogArchiveEnableLatching = "AnalogArchiveEnableLatching";
		public const string AnalogArchiveSendInFreq = "AnalogArchiveSendInFreq";

        public const string ArchivePackagesCount = "ArchivePackagesCount";
	
	}
}
	