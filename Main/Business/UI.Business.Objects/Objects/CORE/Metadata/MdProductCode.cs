namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpProductCode Metadata
	/// </summary>
	public class MdProductCode
	{
		public const string IdProductCode = "IdProductCode";
		public const string Code = "Code";
		public const string IdDescr = "IdDescr";
		public const string Density = "Density";
		public const string CalorificValue = "CalorificValue";
		public const string Descr = "Descr";
	
	}
}
	