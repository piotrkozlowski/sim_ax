namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDiagnosticActionInService Metadata
	/// </summary>
	public class MdDiagnosticActionInService
	{
		public const string IdService = "IdService";
		public const string IdDiagnosticAction = "IdDiagnosticAction";
		public const string IndexNbr = "IndexNbr";
		public const string Service = "Service";
		public const string DiagnosticAction = "DiagnosticAction";
	
	}
}
	