namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConsumer Metadata
	/// </summary>
	public class MdConsumer
	{
		public const string IdConsumer = "IdConsumer";
		public const string IdConsumerType = "IdConsumerType";
		public const string IdActor = "IdActor";
		public const string IdDistributor = "IdDistributor";
		public const string Description = "Description";
		public const string ConsumerType = "ConsumerType";
		public const string Actor = "Actor";
		public const string Distributor = "Distributor";
		public const string DataList = "DataList";
		public const string DT_RECOMMENDED_TOP_UP = "DT_RECOMMENDED_TOP_UP";
		public const string DT_CONSUMER_TAG_NO = "DT_CONSUMER_TAG_NO";
		public const string DT_CONSUMER_CREDIT = "DT_CONSUMER_CREDIT";
		public const string DT_CONSUMER_DEBT = "DT_CONSUMER_DEBT";
		public const string CurrentLocation = "CurrentLocation";
	
	}
}
	