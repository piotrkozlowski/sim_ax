namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceTypeProfileStepKind Metadata
	/// </summary>
	public class MdDeviceTypeProfileStepKind
	{
		public const string IdDeviceTypeProfileStepKind = "IdDeviceTypeProfileStepKind";
		public const string Name = "Name";
	
	}
}
	