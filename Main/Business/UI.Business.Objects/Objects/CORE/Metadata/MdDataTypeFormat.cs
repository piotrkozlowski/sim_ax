namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataTypeFormat Metadata
	/// </summary>
	public class MdDataTypeFormat
	{
		public const string IdDataTypeFormat = "IdDataTypeFormat";
		public const string IdDescr = "IdDescr";
		public const string TextMinLength = "TextMinLength";
		public const string TextMaxLength = "TextMaxLength";
		public const string NumberMinPrecision = "NumberMinPrecision";
		public const string NumberMaxPrecision = "NumberMaxPrecision";
		public const string NumberMinScale = "NumberMinScale";
		public const string NumberMaxScale = "NumberMaxScale";
		public const string NumberMinValue = "NumberMinValue";
		public const string NumberMaxValue = "NumberMaxValue";
		public const string DatetimeFormat = "DatetimeFormat";
		public const string RegularExpression = "RegularExpression";
		public const string IsRequired = "IsRequired";
		public const string IdUniqueType = "IdUniqueType";
		public const string Descr = "Descr";
		public const string UniqueType = "UniqueType";
	
	}
}
	