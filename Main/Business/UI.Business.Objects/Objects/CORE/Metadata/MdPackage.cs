namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpPackage Metadata
	/// </summary>
	public class MdPackage
	{
		public const string IdPackage = "IdPackage";
		public const string Name = "Name";
		public const string CreationDate = "CreationDate";
		public const string Code = "Code";
		public const string SendDate = "SendDate";
		public const string IdOperatorCreator = "IdOperatorCreator";
		public const string IdOperatorReceiver = "IdOperatorReceiver";
		public const string Received = "Received";
		public const string ReceiveDate = "ReceiveDate";
		public const string IdDistributor = "IdDistributor";
		public const string IdPackageStatus = "IdPackageStatus";
		public const string OperatorCreator = "OperatorCreator";
		public const string OperatorReceiver = "OperatorReceiver";
		public const string Distributor = "Distributor";
		public const string PackageStatus = "PackageStatus";
		public const string OperatorAcceptingOrder = "OperatorAcceptingOrder";
		public const string OperatorCompletingOrder = "OperatorCompletingOrder";
		public const string ReceiptConfirmation = "ReceiptConfirmation";
		public const string FromAIUT = "FromAIUT";
		public const string Address = "Address";
		public const string RealizationDate = "RealizationDate";
		public const string Waybill = "Waybill";
		public const string ReceiptConfirmationProvided = "ReceiptConfirmationProvided";
		public const string DataList = "DataList";
	
	}
}
	