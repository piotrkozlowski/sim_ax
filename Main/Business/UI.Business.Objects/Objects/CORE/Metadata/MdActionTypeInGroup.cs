namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionTypeInGroup Metadata
	/// </summary>
	public class MdActionTypeInGroup
	{
		public const string IdActionTypeGroup = "IdActionTypeGroup";
		public const string ActionTypeName = "ActionTypeName";
		public const string ActionTypeGroup = "ActionTypeGroup";
		public const string ActionType = "ActionType";
		public const string OpState = "OpState";
	
	}
}
	