namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpOperatorRole Metadata
	/// </summary>
	public class MdOperatorRole
	{
		public const string IdOperator = "IdOperator";
		public const string IdRole = "IdRole";
		public const string Operator = "Operator";
		public const string Role = "Role";
		public const string OpState = "OpState";
	
	}
}
	