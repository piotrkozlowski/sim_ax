﻿namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpSession Metadata
    /// </summary>
    public class MdSession
    {
        public const string IdSession = "IdSession";
        public const string SessionGuid = "SessionGuid";
        public const string IdModule = "IdModule";
        public const string IdOperator = "IdOperator";
        public const string IdSessionStatus = "IdSessionStatus";
        public const string Module = "Module";
        public const string Operator = "Operator";
        public const string SessionStatus = "SessionStatus";
        public const string DataList = "DataList";
        public const string OpState = "OpState";

    }
}
