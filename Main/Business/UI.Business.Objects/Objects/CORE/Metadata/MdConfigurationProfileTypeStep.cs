namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConfigurationProfileTypeStep Metadata
	/// </summary>
	public class MdConfigurationProfileTypeStep
	{
		public const string IdConfigurationProfileType = "IdConfigurationProfileType";
		public const string IdDeviceTypeProfileStep = "IdDeviceTypeProfileStep";
		public const string ConfigurationProfileType = "ConfigurationProfileType";
		public const string DeviceTypeProfileStep = "DeviceTypeProfileStep";
	
	}
}
	