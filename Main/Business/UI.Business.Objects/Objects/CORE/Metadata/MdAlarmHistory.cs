namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmHistory Metadata
	/// </summary>
	public class MdAlarmHistory
	{
		public const string IdAlarmHistory = "IdAlarmHistory";
		public const string IdAlarm = "IdAlarm";
		public const string IdAlarmStatus = "IdAlarmStatus";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string StartRuleHost = "StartRuleHost";
		public const string StartRuleUser = "StartRuleUser";
		public const string EndRuleHost = "EndRuleHost";
		public const string EndRuleUser = "EndRuleUser";
		public const string Alarm = "Alarm";
		public const string AlarmStatus = "AlarmStatus";
	
	}
}
	