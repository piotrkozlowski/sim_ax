namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionElement Metadata
	/// </summary>
	public class MdVersionElement
	{
		public const string IdVersionElement = "IdVersionElement";
		public const string IdVersionElementType = "IdVersionElementType";
		public const string IdVersionNbrFormat = "IdVersionNbrFormat";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string ReferenceType = "ReferenceType";
		public const string ReferenceValue = "ReferenceValue";
		public const string IdVersion = "IdVersion";
		public const string OpState = "OpState";
		public const string VersionElementType = "VersionElementType";
		public const string VersionNbrFormat = "VersionNbrFormat";
		public const string Descr = "Descr";
		public const string Version = "Version";
		public const string Key = "Key";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	