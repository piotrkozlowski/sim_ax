namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionTypeGroup Metadata
	/// </summary>
	public class MdActionTypeGroup
	{
		public const string IdActionTypeGroup = "IdActionTypeGroup";
		public const string IdActionTypeGroupType = "IdActionTypeGroupType";
		public const string IdReferenceType = "IdReferenceType";
		public const string ReferenceValue = "ReferenceValue";
		public const string IdParentGroup = "IdParentGroup";
		public const string ActionTypeGroupType = "ActionTypeGroupType";
		public const string ReferenceType = "ReferenceType";
		public const string ParentGroup = "ParentGroup";
		public const string Value = "Value";
	
	}
}
	