namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRoleActivity Metadata
	/// </summary>
	public class MdRoleActivity
	{
		public const string IdRoleActivity = "IdRoleActivity";
		public const string IdRole = "IdRole";
		public const string IdActivity = "IdActivity";
		public const string ReferenceValue = "ReferenceValue";
		public const string Deny = "Deny";
		public const string Role = "Role";
		public const string Activity = "Activity";
		public const string OpState = "OpState";
		public const string Allow = "Allow";
	
	}
}
	