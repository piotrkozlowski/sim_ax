namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDescr Metadata
	/// </summary>
	public class MdDescr
	{
		public const string IdDescr = "IdDescr";
		public const string IdLanguage = "IdLanguage";
		public const string Description = "Description";
		public const string ExtendedDescription = "ExtendedDescription";
		public const string Language = "Language";
		public const string OpState = "OpState";
		public const string AllLanguagesDescription = "AllLanguagesDescription";
	
	}
}
	