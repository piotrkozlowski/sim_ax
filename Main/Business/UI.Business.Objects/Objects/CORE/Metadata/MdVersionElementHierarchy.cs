namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionElementHierarchy Metadata
	/// </summary>
	public class MdVersionElementHierarchy
	{
		public const string IdVersionElementHierarchy = "IdVersionElementHierarchy";
		public const string IdVersionElementHierarchyParent = "IdVersionElementHierarchyParent";
		public const string IdVersionElement = "IdVersionElement";
		public const string Hierarchy = "Hierarchy";
		public const string OpState = "OpState";
		public const string VersionElement = "VersionElement";
	
	}
}
	