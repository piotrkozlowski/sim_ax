namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServicePackageData Metadata
	/// </summary>
	public class MdServicePackageData
	{
		public const string IdServicePackageData = "IdServicePackageData";
		public const string IdServicePackage = "IdServicePackage";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Min = "Min";
		public const string Max = "Max";
		public const string Value = "Value";
		public const string OpState = "OpState";
		public const string ServicePackage = "ServicePackage";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	