namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceListDevice Metadata
	/// </summary>
	public class MdServiceListDevice
	{
		public const string IdServiceList = "IdServiceList";
		public const string SerialNbr = "SerialNbr";
		public const string FailureDescription = "FailureDescription";
		public const string FailureSuggestion = "FailureSuggestion";
		public const string Notes = "Notes";
		public const string Photo = "Photo";
		public const string IdOperatorServicer = "IdOperatorServicer";
		public const string ServiceShortDescr = "ServiceShortDescr";
		public const string ServiceLongDescr = "ServiceLongDescr";
		public const string PhotoServiced = "PhotoServiced";
		public const string AtexOk = "AtexOk";
		public const string PayForService = "PayForService";
		public const string IdServiceStatus = "IdServiceStatus";
		public const string IdDiagnosticStatus = "IdDiagnosticStatus";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string ServiceList = "ServiceList";
		public const string Device = "Device";
		public const string OperatorServicer = "OperatorServicer";
		public const string ServiceStatus = "ServiceStatus";
		public const string DiagnosticStatus = "DiagnosticStatus";
		public const string DataList = "DataList";
		public const string DeviceDetails = "DeviceDetails";
	
	}
}
	