namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionHistory Metadata
	/// </summary>
	public class MdActionHistory
	{
		public const string IdActionHistory = "IdActionHistory";
		public const string IdAction = "IdAction";
		public const string IdActionStatus = "IdActionStatus";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string IdDescr = "IdDescr";
		public const string IdActionHistoryData = "IdActionHistoryData";
        public const string IdModule = "IdModule";
        public const string Assembly = "Assembly";
		public const string Action = "Action";
		public const string ActionStatus = "ActionStatus";
		public const string Descr = "Descr";
		public const string ActionHistoryData = "ActionHistoryData";
        public const string Module = "Module";
		public const string DescriptionString = "DescriptionString";
	
	}
}
	