namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionType Metadata
	/// </summary>
	public class MdVersionType
	{
		public const string IdVersionType = "IdVersionType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string OpState = "OpState";
		public const string Descr = "Descr";
	
	}
}
	