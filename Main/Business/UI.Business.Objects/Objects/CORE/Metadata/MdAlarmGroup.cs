namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmGroup Metadata
	/// </summary>
	public class MdAlarmGroup
	{
		public const string IdAlarmGroup = "IdAlarmGroup";
		public const string Name = "Name";
		public const string Description = "Description";
		public const string IdDistributor = "IdDistributor";
		public const string IsEnabled = "IsEnabled";
		public const string Distributor = "Distributor";
	
	}
}
	