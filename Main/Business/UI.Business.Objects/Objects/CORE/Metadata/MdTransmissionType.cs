namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpTransmissionType Metadata
	/// </summary>
	public class MdTransmissionType
	{
		public const string IdTransmissionType = "IdTransmissionType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	