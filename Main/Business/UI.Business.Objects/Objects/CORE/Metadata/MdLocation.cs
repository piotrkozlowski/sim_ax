namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocation Metadata
	/// </summary>
	public class MdLocation
	{
		public const string IdLocation = "IdLocation";
		public const string IdLocationType = "IdLocationType";
		public const string IdLocationPattern = "IdLocationPattern";
		public const string IdDescrPattern = "IdDescrPattern";
		public const string IdDistributor = "IdDistributor";
		public const string IdConsumer = "IdConsumer";
		public const string IdLocationStateType = "IdLocationStateType";
		public const string InKpi = "InKpi";
		public const string AllowGrouping = "AllowGrouping";
		public const string LocationType = "LocationType";
		public const string Distributor = "Distributor";
		public const string Consumer = "Consumer";
		public const string LocationStateType = "LocationStateType";
		public const string OpState = "OpState";
		public const string Name = "Name";
		public const string PostalCode = "PostalCode";
		public const string City = "City";
		public const string Address = "Address";
		public const string CID = "CID";
		public const string Country = "Country";
		public const string FullAddress = "FullAddress";
		public const string GoogleFullAddress = "GoogleFullAddress";
		public const string CreationDate = "CreationDate";
		public const string Latitude = "Latitude";
		public const string Longitude = "Longitude";
		public const string Memo = "Memo";
		public const string IdImrServer = "IdImrServer";
		public const string ImrServer = "ImrServer";
		public const string IdActor = "IdActor";
		public const string Actor = "Actor";
		public const string LocationPattern = "LocationPattern";
		public const string IdLocationOriginal = "IdLocationOriginal";
		public const string GridLookUpEditString = "GridLookUpEditString";
		public const string MaxTruckSize = "MaxTruckSize";
		public const string DataList = "DataList";
		public const string CurrentArea = "CurrentArea";
		public const string CurrentSupplier = "CurrentSupplier";
		public const string CurrentTariff = "CurrentTariff";
		public const string CurrentDevice = "CurrentDevice";
		public const string CurrentMeter = "CurrentMeter";
        public const string CurrentMeters = "CurrentMeters";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
        public const string HostCID = "HostCID";
	}
}
	