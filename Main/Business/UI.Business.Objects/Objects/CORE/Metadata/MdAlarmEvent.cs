namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmEvent Metadata
	/// </summary>
	public class MdAlarmEvent
	{
		public const string IdAlarmEvent = "IdAlarmEvent";
		public const string IdAlarmDef = "IdAlarmDef";
		public const string IdDataArch = "IdDataArch";
		public const string IsActive = "IsActive";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string AlarmDef = "AlarmDef";
		public const string DataArch = "DataArch";
		public const string Device = "Device";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	