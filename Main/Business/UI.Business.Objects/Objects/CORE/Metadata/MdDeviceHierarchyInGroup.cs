namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceHierarchyInGroup Metadata
	/// </summary>
	public class MdDeviceHierarchyInGroup
	{
		public const string IdDeviceHierarchyGroup = "IdDeviceHierarchyGroup";
		public const string IdDeviceHierarchy = "IdDeviceHierarchy";
		public const string DeviceHierarchyGroup = "DeviceHierarchyGroup";
		public const string DeviceHierarchy = "DeviceHierarchy";
	
	}
}
	