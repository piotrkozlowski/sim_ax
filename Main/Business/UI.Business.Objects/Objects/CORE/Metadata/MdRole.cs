namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRole Metadata
	/// </summary>
	public class MdRole
	{
		public const string IdRole = "IdRole";
		public const string Name = "Name";
		public const string IdModule = "IdModule";
		public const string IdDescr = "IdDescr";
		public const string Module = "Module";
		public const string Descr = "Descr";
	
	}
}
	