namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpWarrantyContract Metadata
	/// </summary>
	public class MdWarrantyContract
	{
		public const string IdDeviceOrderNumber = "IdDeviceOrderNumber";
		public const string IdComponent = "IdComponent";
		public const string IdContract = "IdContract";
		public const string WarrantyTime = "WarrantyTime";
		public const string IdWarrantyStartDateCalculationMethod = "IdWarrantyStartDateCalculationMethod";
		public const string DeviceOrderNumber = "DeviceOrderNumber";
		public const string Component = "Component";
		public const string Contract = "Contract";
	
	}
}
	