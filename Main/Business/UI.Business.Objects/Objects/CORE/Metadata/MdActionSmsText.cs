namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionSmsText Metadata
	/// </summary>
	public class MdActionSmsText
	{
		public const string IdActionSmsText = "IdActionSmsText";
		public const string IdLanguage = "IdLanguage";
		public const string SmsText = "SmsText";
		public const string IdDataType = "IdDataType";
		public const string IsIncoming = "IsIncoming";
		public const string IdTransmissionType = "IdTransmissionType";
		public const string Language = "Language";
		public const string DataType = "DataType";
		public const string TransmissionType = "TransmissionType";
	
	}
}
	