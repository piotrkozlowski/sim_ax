namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpImrServerData Metadata
	/// </summary>
	public class MdImrServerData
	{
		public const string IdImrServerData = "IdImrServerData";
		public const string IdImrServer = "IdImrServer";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string OpState = "OpState";
		public const string ImrServer = "ImrServer";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	