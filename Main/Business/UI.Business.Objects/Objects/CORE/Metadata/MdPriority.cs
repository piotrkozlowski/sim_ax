namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpPriority Metadata
	/// </summary>
	public class MdPriority
	{
		public const string IdPriority = "IdPriority";
		public const string IdDescr = "IdDescr";
		public const string Value = "Value";
		public const string RealizationTime = "RealizationTime";
		public const string Descr = "Descr";
	
	}
}
	