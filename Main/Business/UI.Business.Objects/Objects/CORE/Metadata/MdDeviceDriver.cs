namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceDriver Metadata
	/// </summary>
	public class MdDeviceDriver
	{
		public const string IdDeviceDriver = "IdDeviceDriver";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string PluginName = "PluginName";
		public const string AutoUpdate = "AutoUpdate";
		public const string Descr = "Descr";
		public const string DataList = "DataList";
	
	}
}
	