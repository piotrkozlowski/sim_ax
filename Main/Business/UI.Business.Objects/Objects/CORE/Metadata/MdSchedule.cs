namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpSchedule Metadata
    /// </summary>
    public class MdSchedule
    {
        public const string IdSchedule = "IdSchedule";
        public const string Name = "Name";
        public const string Description = "Description";
        public const string IdScheduleType = "IdScheduleType";
        public const string ScheduleInterval = "ScheduleInterval";
        public const string SubdayType = "SubdayType";
        public const string SubdayInterval = "SubdayInterval";
        public const string RelativeInterval = "RelativeInterval";
        public const string RecurrenceFactor = "RecurrenceFactor";
        public const string StartDate = "StartDate";
        public const string EndDate = "EndDate";
        public const string StartTime = "StartTime";
        public const string EndTime = "EndTime";
        public const string TimesInUtc = "TimesInUtc";
        public const string LastRunDate = "LastRunDate";
        public const string IsActive = "IsActive";
        public const string ScheduleType = "ScheduleType";
    }
}