namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionRequirement Metadata
	/// </summary>
	public class MdVersionRequirement
	{
		public const string IdVersion = "IdVersion";
		public const string IdVersionRequired = "IdVersionRequired";
		public const string IdOperator = "IdOperator";
		public const string IdVersionRequirement = "IdVersionRequirement";
		public const string OpState = "OpState";
		public const string Version = "Version";
		public const string VersionRequired = "VersionRequired";
		public const string Operator = "Operator";
	
	}
}
	