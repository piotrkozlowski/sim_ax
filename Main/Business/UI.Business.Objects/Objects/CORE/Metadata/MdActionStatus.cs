namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionStatus Metadata
	/// </summary>
	public class MdActionStatus
	{
		public const string IdActionStatus = "IdActionStatus";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	