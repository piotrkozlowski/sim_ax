namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionElementHistory Metadata
	/// </summary>
	public class MdVersionElementHistory
	{
		public const string IdVersionElementHistory = "IdVersionElementHistory";
		public const string IdVersionElement = "IdVersionElement";
		public const string IdVersion = "IdVersion";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string IdOperator = "IdOperator";
		public const string OpState = "OpState";
		public const string VersionElement = "VersionElement";
		public const string Version = "Version";
		public const string Operator = "Operator";
	
	}
}
	