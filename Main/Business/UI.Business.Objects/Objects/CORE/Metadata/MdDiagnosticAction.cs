namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDiagnosticAction Metadata
	/// </summary>
	public class MdDiagnosticAction
	{
		public const string IdDiagnosticAction = "IdDiagnosticAction";
		public const string IdDeviceOrderNumber = "IdDeviceOrderNumber";
		public const string IdDistributor = "IdDistributor";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string DeviceOrderNumber = "DeviceOrderNumber";
		public const string Distributor = "Distributor";
		public const string Descr = "Descr";
		public const string DataList = "DataList";
		public const string AllLanguagesDescription = "AllLanguagesDescription";
		public const string InvisibleInServiceReport = "InvisibleInServiceReport";
		public const string ResultEditableAfterService = "ResultEditableAfterService";
	
	}
}
	