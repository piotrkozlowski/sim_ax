namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpProfile Metadata
	/// </summary>
	public class MdProfile
	{
		public const string IdProfile = "IdProfile";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdModule = "IdModule";
		public const string Descr = "Descr";
		public const string Module = "Module";
		public const string Parameters = "Parameters";
		public const string Devices = "Devices";
		public const string Meters = "Meters";
		public const string Locations = "Locations";
		public const string Distributors = "Distributors";
	
	}
}
	