namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataTypeInGroup Metadata
	/// </summary>
	public class MdDataTypeInGroup
	{
		public const string IdDataTypeGroup = "IdDataTypeGroup";
		public const string IdDataType = "IdDataType";
		public const string DataTypeGroup = "DataTypeGroup";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
	
	}
}
	