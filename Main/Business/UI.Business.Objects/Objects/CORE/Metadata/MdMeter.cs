namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMeter Metadata
	/// </summary>
	public class MdMeter
	{
		public const string IdMeter = "IdMeter";
		public const string IdMeterType = "IdMeterType";
		public const string IdMeterPattern = "IdMeterPattern";
		public const string IdDescrPattern = "IdDescrPattern";
		public const string IdDistributor = "IdDistributor";
		public const string MeterType = "MeterType";
		public const string Distributor = "Distributor";
		public const string OpState = "OpState";
		public const string GridLookUpEditString = "GridLookUpEditString";
		public const string MeterSerialNbr = "MeterSerialNbr";
		public const string TankCapacity = "TankCapacity";
		public const string IdProductCode = "IdProductCode";
		public const string TankNbr = "TankNbr";
		public const string TankName = "TankName";
		public const string ProductCode = "ProductCode";
		public const string DataList = "DataList";
		public const string CurrentDevice = "CurrentDevice";
		public const string CurrentLocation = "CurrentLocation";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	