﻿namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpSessionEvent Metadata
    /// </summary>
    public class MdSessionEvent
    {
        public const string IdSessionEvent = "IdSessionEvent";
        public const string IdSession = "IdSession";
        public const string IdEvent = "IdEvent";
        public const string Time = "Time";
        public const string Session = "Session";

    }
}
