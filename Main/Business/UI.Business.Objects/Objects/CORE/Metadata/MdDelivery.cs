namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDelivery Metadata
	/// </summary>
	public class MdDelivery
	{
		public const string IdDelivery = "IdDelivery";
		public const string Name = "Name";
		public const string Description = "Description";
		public const string UploadedFile = "UploadedFile";
		public const string CreationDate = "CreationDate";
		public const string IdShippingList = "IdShippingList";
		public const string IdOperator = "IdOperator";
		public const string Operator = "Operator";
	
	}
}
	