namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceOrderNumberData Metadata
	/// </summary>
	public class MdDeviceOrderNumberData
	{
		public const string IdDeviceOrderNumberData = "IdDeviceOrderNumberData";
		public const string IdDeviceOrderNumber = "IdDeviceOrderNumber";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string OpState = "OpState";
		public const string DeviceOrderNumber = "DeviceOrderNumber";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	