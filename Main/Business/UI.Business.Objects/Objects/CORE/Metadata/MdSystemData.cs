namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpSystemData Metadata
	/// </summary>
	public class MdSystemData
	{
		public const string IdSystemData = "IdSystemData";
		public const string IdDataType = "IdDataType";
		public const string Value = "Value";
		public const string Index = "Index";
		public const string DataType = "DataType";
		public const string IdData = "IdData";
		public const string OpState = "OpState";
	
	}
}
	