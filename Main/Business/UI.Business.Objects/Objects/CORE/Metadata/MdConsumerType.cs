namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpConsumerType Metadata
	/// </summary>
	public class MdConsumerType
	{
		public const string IdConsumerType = "IdConsumerType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdDistributor = "IdDistributor";
		public const string Descr = "Descr";
		public const string Distributor = "Distributor";
		public const string DataList = "DataList";
		public const string DT_CONSUMER_TYPE_OWED_REPAYMENT_PERCENTAGE = "DT_CONSUMER_TYPE_OWED_REPAYMENT_PERCENTAGE";
		public const string DT_CONSUMER_TYPE_EC_LIMIT = "DT_CONSUMER_TYPE_EC_LIMIT";
		public const string DT_CONSUMER_TYPE_EC_REPAYMENT_PERCENTAGE = "DT_CONSUMER_TYPE_EC_REPAYMENT_PERCENTAGE";
		public const string DT_CONSUMER_TYPE_PREPAID_ENABLED = "DT_CONSUMER_TYPE_PREPAID_ENABLED";
		public const string DT_EMERGENCY_CREDIT_TOPUP_VALUE = "DT_EMERGENCY_CREDIT_TOPUP_VALUE";
		public const string DT_RECOMMENDED_TOPUP_DAYS = "DT_RECOMMENDED_TOPUP_DAYS";
		public const string DT_RECOMMENDED_TOPUP_BEFORE_EMPTY_DAYS = "DT_RECOMMENDED_TOPUP_BEFORE_EMPTY_DAYS";
		public const string DT_CONSUMPTION_DAILY_AVG_WEIGHT_PERCENT = "DT_CONSUMPTION_DAILY_AVG_WEIGHT_PERCENT";
		public const string DT_CONSUMPTION_DAILY_MIN = "DT_CONSUMPTION_DAILY_MIN";
		public const string DT_EMERGENCY_CREDIT_AVAILABLE_AT = "DT_EMERGENCY_CREDIT_AVAILABLE_AT";
		public const string ConsumptionDailyMin = "ConsumptionDailyMin";
		public const string RecommendedTopupDays = "RecommendedTopupDays";
	
	}
}
	