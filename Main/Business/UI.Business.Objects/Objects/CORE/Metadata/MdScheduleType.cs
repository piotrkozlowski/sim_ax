namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpScheduleType Metadata
	/// </summary>
	public class MdScheduleType
	{
		public const string IdScheduleType = "IdScheduleType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	