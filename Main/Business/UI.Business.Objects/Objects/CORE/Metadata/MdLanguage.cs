namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLanguage Metadata
	/// </summary>
	public class MdLanguage
	{
		public const string IdLanguage = "IdLanguage";
		public const string Name = "Name";
		public const string CultureCode = "CultureCode";
		public const string CultureLangid = "CultureLangid";
	
	}
}
	