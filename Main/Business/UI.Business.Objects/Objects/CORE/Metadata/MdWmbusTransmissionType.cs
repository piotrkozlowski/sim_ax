namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpWmbusTransmissionType Metadata
	/// </summary>
	public class MdWmbusTransmissionType
	{
		public const string IdWmbusTransmissionType = "IdWmbusTransmissionType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	