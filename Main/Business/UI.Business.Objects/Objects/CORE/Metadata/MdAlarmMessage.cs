namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmMessage Metadata
	/// </summary>
	public class MdAlarmMessage
	{
		public const string IdAlarmEvent = "IdAlarmEvent";
		public const string IdLanguage = "IdLanguage";
		public const string AlarmMessage = "AlarmMessage";
		public const string AlarmEvent = "AlarmEvent";
		public const string Language = "Language";
	
	}
}
	