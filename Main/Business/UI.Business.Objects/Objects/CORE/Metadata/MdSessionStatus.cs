﻿namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpSessionStatus Metadata
    /// </summary>
    public class MdSessionStatus
    {
        public const string IdSessionStatus = "IdSessionStatus";
        public const string Name = "Name";
        public const string IdDescr = "IdDescr";
        public const string Descr = "Descr";
        public const string ComboBoxString = "ComboBoxString";

    }
}
