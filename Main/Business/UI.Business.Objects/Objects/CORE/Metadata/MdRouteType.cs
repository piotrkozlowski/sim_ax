namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpRouteType Metadata
	/// </summary>
	public class MdRouteType
	{
		public const string IdRouteType = "IdRouteType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	