namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpScheduleAction Metadata
	/// </summary>
	public class MdScheduleAction
	{
		public const string IdScheduleAction = "IdScheduleAction";
		public const string IdSchedule = "IdSchedule";
		public const string IdActionDef = "IdActionDef";
		public const string IsEnabled = "IsEnabled";
		public const string IdOperator = "IdOperator";
		public const string IdProfile = "IdProfile";
		public const string Schedule = "Schedule";
		public const string ActionDef = "ActionDef";
		public const string Operator = "Operator";
		public const string Profile = "Profile";
		public const string OpState = "OpState";
	
	}
}
	