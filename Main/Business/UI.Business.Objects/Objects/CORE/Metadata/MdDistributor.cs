namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDistributor Metadata
	/// </summary>
	public class MdDistributor
	{
		public const string IdDistributor = "IdDistributor";
		public const string Name = "Name";
		public const string City = "City";
		public const string Address = "Address";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
		public const string DitributorIMRServerId = "DitributorIMRServerId";
		public const string EquipmentLocationId = "EquipmentLocationId";
		public const string DataList = "DataList";
		public const string ComboBoxString = "ComboBoxString";
		public const string OpState = "OpState";
		public const string IdServiceMagazine = "IdServiceMagazine";
		public const string ServiceMagazine = "ServiceMagazine";
		public const string IdIMRServer = "IdIMRServer";
		public const string ImrServer = "ImrServer";
	
	}
}
	