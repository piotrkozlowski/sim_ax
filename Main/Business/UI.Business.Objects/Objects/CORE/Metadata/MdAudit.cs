namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAudit Metadata
	/// </summary>
	public class MdAudit
	{
		public const string IdAudit = "IdAudit";
		public const string BatchId = "BatchId";
		public const string ChangeType = "ChangeType";
		public const string TableName = "TableName";
		public const string Key1 = "Key1";
		public const string Key2 = "Key2";
		public const string Key3 = "Key3";
		public const string Key4 = "Key4";
		public const string Key5 = "Key5";
		public const string Key6 = "Key6";
		public const string ColumnName = "ColumnName";
		public const string OldValue = "OldValue";
		public const string NewValue = "NewValue";
		public const string Time = "Time";
		public const string User = "User";
		public const string Host = "Host";
		public const string Application = "Application";
        public const string Contextinfo = "Contextinfo";
		public const string AuditFullInfo = "AuditFullInfo";
	
	}
}
	