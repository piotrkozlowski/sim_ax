namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataTypeGroup Metadata
	/// </summary>
	public class MdDataTypeGroup
	{
		public const string IdDataTypeGroup = "IdDataTypeGroup";
		public const string IdDataTypeGroupType = "IdDataTypeGroupType";
		public const string IdReferenceType = "IdReferenceType";
		public const string ReferenceValue = "ReferenceValue";
		public const string Name = "Name";
		public const string IdParentGroup = "IdParentGroup";
		public const string DataTypeGroupType = "DataTypeGroupType";
		public const string ReferenceType = "ReferenceType";
		public const string ParentGroup = "ParentGroup";
		public const string Value = "Value";
	
	}
}
	