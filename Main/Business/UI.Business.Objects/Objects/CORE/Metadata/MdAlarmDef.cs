namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAlarmDef Metadata
	/// </summary>
	public class MdAlarmDef
	{
		public const string IdAlarmDef = "IdAlarmDef";
		public const string Name = "Name";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdAlarmType = "IdAlarmType";
		public const string IdDataTypeAlarm = "IdDataTypeAlarm";
		public const string IdDataTypeConfig = "IdDataTypeConfig";
		public const string SystemAlarmValue = "SystemAlarmValue";
		public const string IsEnabled = "IsEnabled";
		public const string IdAlarmText = "IdAlarmText";
		public const string IdAlarmData = "IdAlarmData";
		public const string CheckLastAlarm = "CheckLastAlarm";
		public const string SuspensionTime = "SuspensionTime";
		public const string IsConfirmable = "IsConfirmable";
		public const string Device = "Device";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string AlarmType = "AlarmType";
		public const string DataTypeAlarm = "DataTypeAlarm";
		public const string DataTypeConfig = "DataTypeConfig";
		public const string AlarmText = "AlarmText";
        public const string IndexNbr = "IndexNbr";
	
	}
}
	