namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationStateHistory Metadata
	/// </summary>
	public class MdLocationStateHistory
	{
		public const string IdLocation = "IdLocation";
		public const string IdLocationState = "IdLocationState";
		public const string IdLocationStateType = "IdLocationStateType";
		public const string InKpi = "InKpi";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string Notes = "Notes";
		public const string Location = "Location";
		public const string LocationStateType = "LocationStateType";
		public const string OpState = "OpState";
	
	}
}
	