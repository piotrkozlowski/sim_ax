namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpShippingListType Metadata
	/// </summary>
	public class MdShippingListType
	{
		public const string IdShippingListType = "IdShippingListType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	