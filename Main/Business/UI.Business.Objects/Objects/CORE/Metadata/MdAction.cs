namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpAction Metadata
	/// </summary>
	public class MdAction
	{
		public const string IdAction = "IdAction";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdActionType = "IdActionType";
		public const string IdActionStatus = "IdActionStatus";
		public const string IdActionData = "IdActionData";
		public const string IdActionParent = "IdActionParent";
		public const string IdDataArch = "IdDataArch";
		public const string IdModule = "IdModule";
		public const string IdOperator = "IdOperator";
		public const string CreationDate = "CreationDate";
		public const string Device = "Device";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string ActionType = "ActionType";
		public const string ActionStatus = "ActionStatus";
		public const string ActionData = "ActionData";
		public const string DataArch = "DataArch";
		public const string Module = "Module";
		public const string Operator = "Operator";
		public const string Target = "Target";
		public const string DispModule = "DispModule";
		public const string ResponseGridColumn = "ResponseGridColumn";
		public const string GUID = "GUID";
		public const string GUIDParent = "GUIDParent";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	