namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionTypeGroupType Metadata
	/// </summary>
	public class MdActionTypeGroupType
	{
		public const string IdActionTypeGroupType = "IdActionTypeGroupType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	