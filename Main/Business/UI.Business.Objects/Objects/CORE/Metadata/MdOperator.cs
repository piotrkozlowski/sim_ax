namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpOperator Metadata
	/// </summary>
	public class MdOperator
	{
		public const string IdOperator = "IdOperator";
		public const string IdActor = "IdActor";
		public const string IdDistributor = "IdDistributor";
		public const string SerialNbr = "SerialNbr";
		public const string Login = "Login";
		public const string Password = "Password";
		public const string IsBlocked = "IsBlocked";
		public const string Description = "Description";
		public const string Actor = "Actor";
		public const string Distributor = "Distributor";
		public const string NewPassword = "NewPassword";
		public const string Initials = "Initials";
		public const string GuiColorInt = "GuiColorInt";
		public const string GuiColor = "GuiColor";
		public const string DepositoryLocation = "DepositoryLocation";
		public const string Suffix = "Suffix";
		public const string MinLogLevel = "MinLogLevel";
		public const string DataList = "DataList";
		public const string DefaultRole = "DefaultRole";
		public const string ComboBoxString = "ComboBoxString";
	
	}
}
	