﻿namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
    /// <summary>
    /// OpSessionData Metadata
    /// </summary>
    public class MdSessionData
    {
        public const string IdSessionData = "IdSessionData";
        public const string IdSession = "IdSession";
        public const string IdDataType = "IdDataType";
        public const string Value = "Value";
        public const string Session = "Session";
        public const string DataType = "DataType";
        public const string IdData = "IdData";
        public const string Index = "Index";
        public const string OpState = "OpState";
        public const string ReferencedObject = "ReferencedObject";

    }
}
