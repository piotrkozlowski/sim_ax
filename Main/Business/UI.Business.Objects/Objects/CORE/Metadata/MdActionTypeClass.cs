namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionTypeClass Metadata
	/// </summary>
	public class MdActionTypeClass
	{
		public const string IdActionTypeClass = "IdActionTypeClass";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	