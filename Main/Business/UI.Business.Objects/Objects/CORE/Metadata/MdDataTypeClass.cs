namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataTypeClass Metadata
	/// </summary>
	public class MdDataTypeClass
	{
		public const string IdDataTypeClass = "IdDataTypeClass";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	