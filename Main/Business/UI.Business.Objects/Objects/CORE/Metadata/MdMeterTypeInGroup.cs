namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMeterTypeInGroup Metadata
	/// </summary>
	public class MdMeterTypeInGroup
	{
		public const string IdMeterTypeGroup = "IdMeterTypeGroup";
		public const string IdMeterType = "IdMeterType";
		public const string MeterTypeGroup = "MeterTypeGroup";
		public const string MeterType = "MeterType";
		public const string OpState = "OpState";
	
	}
}
	