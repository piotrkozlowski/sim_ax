namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpCommandCodeType Metadata
	/// </summary>
	public class MdCommandCodeType
	{
		public const string IdCommandCode = "IdCommandCode";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	