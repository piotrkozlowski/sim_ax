namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpUnit Metadata
	/// </summary>
	public class MdUnit
	{
		public const string IdUnit = "IdUnit";
		public const string IdUnitBase = "IdUnitBase";
		public const string Scale = "Scale";
		public const string Bias = "Bias";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
		public const string UnitBase = "UnitBase";
	
	}
}
	