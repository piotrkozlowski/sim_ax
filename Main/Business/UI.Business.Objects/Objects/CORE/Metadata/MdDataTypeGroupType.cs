namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDataTypeGroupType Metadata
	/// </summary>
	public class MdDataTypeGroupType
	{
		public const string IdDataTypeGroupType = "IdDataTypeGroupType";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string Descr = "Descr";
	
	}
}
	