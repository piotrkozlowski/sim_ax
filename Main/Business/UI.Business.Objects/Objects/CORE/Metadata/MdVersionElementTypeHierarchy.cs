namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionElementTypeHierarchy Metadata
	/// </summary>
	public class MdVersionElementTypeHierarchy
	{
		public const string IdVersionElementTypeHierarchy = "IdVersionElementTypeHierarchy";
		public const string IdVersionElementTypeHierarchyParent = "IdVersionElementTypeHierarchyParent";
		public const string IdVersionElementType = "IdVersionElementType";
		public const string Required = "Required";
		public const string Hierarchy = "Hierarchy";
		public const string OpState = "OpState";
		public const string VersionElementType = "VersionElementType";
	
	}
}
	