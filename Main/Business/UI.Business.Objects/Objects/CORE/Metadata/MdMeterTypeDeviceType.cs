namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpMeterTypeDeviceType Metadata
	/// </summary>
	public class MdMeterTypeDeviceType
	{
		public const string IdMeterType = "IdMeterType";
		public const string IdDeviceType = "IdDeviceType";
		public const string MeterType = "MeterType";
		public const string DeviceType = "DeviceType";
		public const string OpState = "OpState";
	
	}
}
	