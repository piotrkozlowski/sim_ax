namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpFile Metadata
	/// </summary>
	public class MdFile
	{
		public const string IdFile = "IdFile";
		public const string PhysicalFileName = "PhysicalFileName";
		public const string FileBytes = "FileBytes";
		public const string Size = "Size";
		public const string InsertDate = "InsertDate";
		public const string Description = "Description";
		public const string Version = "Version";
		public const string LastDownloaded = "LastDownloaded";
		public const string FriendlyName = "FriendlyName";
		public const string Extension = "Extension";
        public const string IsBlocked = "IsBlocked";
	
	}
}
	