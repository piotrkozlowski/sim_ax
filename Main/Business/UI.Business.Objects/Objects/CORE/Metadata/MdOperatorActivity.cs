namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpOperatorActivity Metadata
	/// </summary>
	public class MdOperatorActivity
	{
		public const string IdOperatorActivity = "IdOperatorActivity";
		public const string IdOperator = "IdOperator";
		public const string IdActivity = "IdActivity";
		public const string ReferenceValue = "ReferenceValue";
		public const string Deny = "Deny";
		public const string IdModule = "IdModule";
		public const string Operator = "Operator";
		public const string Activity = "Activity";
		public const string Module = "Module";
		public const string OpState = "OpState";
		public const string Allow = "Allow";
	
	}
}
	