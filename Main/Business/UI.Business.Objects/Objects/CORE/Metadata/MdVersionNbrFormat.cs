namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpVersionNbrFormat Metadata
	/// </summary>
	public class MdVersionNbrFormat
	{
		public const string IdVersionNbrFormat = "IdVersionNbrFormat";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string IdDataTypeClass = "IdDataTypeClass";
		public const string RegularExpression = "RegularExpression";
		public const string OpState = "OpState";
		public const string Descr = "Descr";
		public const string DataTypeClass = "DataTypeClass";
	
	}
}
	