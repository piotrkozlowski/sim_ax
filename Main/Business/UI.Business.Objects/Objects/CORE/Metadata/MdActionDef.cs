namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionDef Metadata
	/// </summary>
	public class MdActionDef
	{
		public const string IdActionDef = "IdActionDef";
		public const string SerialNbr = "SerialNbr";
		public const string IdMeter = "IdMeter";
		public const string IdLocation = "IdLocation";
		public const string IdDataType = "IdDataType";
		public const string IdActionType = "IdActionType";
		public const string IdActionData = "IdActionData";
		public const string Value = "Value";
		public const string IdDescr = "IdDescr";
		public const string Meter = "Meter";
		public const string Location = "Location";
		public const string DataType = "DataType";
		public const string ActionType = "ActionType";
		public const string ActionData = "ActionData";
		public const string Descr = "Descr";
		public const string Device = "Device";
		public const string OpState = "OpState";
		public const string DataList = "DataList";
	
	}
}
	