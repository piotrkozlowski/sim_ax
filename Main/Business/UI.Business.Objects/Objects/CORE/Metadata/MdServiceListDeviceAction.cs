namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceListDeviceAction Metadata
	/// </summary>
	public class MdServiceListDeviceAction
	{
		public const string SerialNbr = "SerialNbr";
		public const string IdServiceList = "IdServiceList";
		public const string IdServicePackage = "IdServicePackage";
		public const string IdServiceCustom = "IdServiceCustom";
		public const string ServiceList = "ServiceList";
		public const string ServicePackage = "ServicePackage";
		public const string ServiceCustom = "ServiceCustom";
	
	}
}
	