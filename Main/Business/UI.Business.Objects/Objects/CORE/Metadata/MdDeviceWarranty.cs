namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpDeviceWarranty Metadata
	/// </summary>
	public class MdDeviceWarranty
	{
		public const string IdDeviceWarranty = "IdDeviceWarranty";
		public const string IdShippingList = "IdShippingList";
		public const string SerialNbr = "SerialNbr";
		public const string IdComponent = "IdComponent";
		public const string IdContract = "IdContract";
		public const string WarrantyLength = "WarrantyLength";
		public const string ShippingDate = "ShippingDate";
		public const string InstallationDate = "InstallationDate";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string ShippingList = "ShippingList";
		public const string Component = "Component";
		public const string Contract = "Contract";
		public const string WarrantyContract = "WarrantyContract";
		public const string Device = "Device";
		public const string WarrantyExpiration = "WarrantyExpiration";
	
	}
}
	