namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpEtl Metadata
	/// </summary>
	public class MdEtl
	{
		public const string IdParam = "IdParam";
		public const string Code = "Code";
		public const string Descr = "Descr";
		public const string Value = "Value";
	
	}
}
	