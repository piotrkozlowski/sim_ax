namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpActionData Metadata
	/// </summary>
	public class MdActionData
	{
		public const string IdActionData = "IdActionData";
		public const string IdDataType = "IdDataType";
		public const string IndexNbr = "IndexNbr";
		public const string Value = "Value";
		public const string DataType = "DataType";
		public const string OpState = "OpState";
		public const string IdData = "IdData";
		public const string Index = "Index";
	
	}
}
	