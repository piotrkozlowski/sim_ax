namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpLocationEquipment Metadata
	/// </summary>
	public class MdLocationEquipment
	{
		public const string IdLocationEquipment = "IdLocationEquipment";
		public const string IdLocation = "IdLocation";
		public const string IdMeter = "IdMeter";
		public const string SerialNbr = "SerialNbr";
		public const string StartTime = "StartTime";
		public const string EndTime = "EndTime";
		public const string Location = "Location";
		public const string Meter = "Meter";
		public const string Device = "Device";
		public const string OpState = "OpState";
		public const string Key = "Key";
		public const string DynamicProperties = "DynamicProperties";
		public const string DynamicValues = "DynamicValues";
		public const string Owner = "Owner";
	
	}
}
	