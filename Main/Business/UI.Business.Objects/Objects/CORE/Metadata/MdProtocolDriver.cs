namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpProtocolDriver Metadata
	/// </summary>
	public class MdProtocolDriver
	{
		public const string IdProtocolDriver = "IdProtocolDriver";
		public const string Name = "Name";
		public const string IdDescr = "IdDescr";
		public const string PluginName = "PluginName";
		public const string Descr = "Descr";
		public const string OpState = "OpState";
	
	}
}
	