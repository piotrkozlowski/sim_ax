namespace IMR.Suite.UI.Business.Objects.CORE.Metadata
{
	/// <summary>
	/// OpServiceList Metadata
	/// </summary>
	public class MdServiceList
	{
		public const string IdServiceList = "IdServiceList";
		public const string IdDistributor = "IdDistributor";
		public const string StartDate = "StartDate";
		public const string EndDate = "EndDate";
		public const string IdOperatorReceiver = "IdOperatorReceiver";
		public const string IdOperatorServicer = "IdOperatorServicer";
		public const string CourierName = "CourierName";
		public const string Priority = "Priority";
		public const string ShippingLetterNbr = "ShippingLetterNbr";
		public const string ExpectedFinishDate = "ExpectedFinishDate";
		public const string ServiceName = "ServiceName";
		public const string IdDocumentPackage = "IdDocumentPackage";
		public const string Confirmed = "Confirmed";
		public const string Distributor = "Distributor";
		public const string OperatorReceiver = "OperatorReceiver";
		public const string OperatorServicer = "OperatorServicer";
		public const string IdLocationMagazine = "IdLocationMagazine";
		public const string Magazine = "Magazine";
		public const string DataList = "DataList";
	
	}
}
	