using System;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using System.Linq;
using IMR.Suite.Common;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
	public class OpDistributorData : DB_DISTRIBUTOR_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpDistributorData>
    {
        #region Properties
        [XmlIgnore]
        public long IdDistributorData { get { return this.ID_DISTRIBUTOR_DATA; } set { this.ID_DISTRIBUTOR_DATA = value; } }
        [XmlIgnore]
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
        [DataMember]
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }

        [DataMember]
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdDistributorData; } set { IdDistributorData = value; } }

        [DataMember]
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion
		
    #region Ctor
		public OpDistributorData()
			: base() { this.OpState = OpChangeState.New; }
		
		public OpDistributorData(DB_DISTRIBUTOR_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion
		
    #region ToString
		public override string ToString()
		{
			return Value != null ? Value.ToString() : "";
		}
    #endregion

    #region ConvertList
        public static List<OpDistributorData> ConvertList(DB_DISTRIBUTOR_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDistributorData> ConvertList(DB_DISTRIBUTOR_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDistributorData> ret = new List<OpDistributorData>(list.Length);

            Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, loadNavigationProperties: loadNavigationProperties).ToDictionary(l => l.ID_DATA_TYPE);

            foreach (var loop in list)
            {
                OpDistributorData insert = new OpDistributorData(loop);
                insert.DataType = DataTypeDict.TryGetValue(insert.ID_DATA_TYPE);
                try
                {
                    // sdudzik - probujemy poprawic wartosc, zeby byla wlasciwego typu
                    // CELOWO (!) uzywamy VALUE, a nie Value - nie zmieniac tego!
                    // VALUE uzywane do tego, zeby raz poprawic typ rzeczywistej wartosci, Value to tylko get/set
                    insert.VALUE = OpDataUtil.CorrectValueType(insert.DataType, insert.VALUE);
                }
                catch
                {
                    // korekcja sie nie udala, oznaczamy wartosc jako bledna
                    insert.OpState = OpChangeState.Incorrect;
                }
                //if (loadNavigationProperties)
                //{
                //    //insert.Distributor = dataProvider.GetDistributor(loop.ID_DISTRIBUTOR);
                //    insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
                //}
                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDistributorData> list, IDataProvider dataProvider)
        {
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.Distributor = dataProvider.GetDistributor(this.ID_DISTRIBUTOR);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDistributorData clone = new OpDistributorData();
            clone.IdData = this.IdData;
            clone.IdDistributor = this.IdDistributor;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_DISTRIBUTOR_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDistributorData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDistributorData).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpDistributorData> Members
        public bool Equals(OpDistributorData other)
        {
            if (other == null)
                return false;
            return this.IdDistributorData.Equals(other.IdDistributorData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDistributorData)
				return this.IdData == ((OpDistributorData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDistributorData left, OpDistributorData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDistributorData left, OpDistributorData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
    }
#endif
}