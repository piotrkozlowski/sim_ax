﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable]
    public class OpViewData : DB_VIEW_DATA, IComparable, IEquatable<OpViewData>, IOpData, IOpDataProvider
    {
        #region Properties
        public long IdViewData { get { return this.ID_VIEW_DATA; } set { this.ID_VIEW_DATA = value; } }
        public int IdView { get { return this.ID_VIEW; } set { this.ID_VIEW = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        #endregion

        #region	Navigation Properties
        private OpView _View;
        public OpView View { get { return this._View; } set { this._View = value; this.ID_VIEW = (value == null) ? 0 : (int)value.ID_VIEW; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpViewData()
            : base()
        {
            this.OpState = OpChangeState.New;
        }

        public OpViewData(DB_VIEW_DATA clone)
            : base(clone)
        {
            this.OpState = OpChangeState.Loaded;
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "ViewData [" + IdViewData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpViewData> ConvertList(DB_VIEW_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpViewData> ConvertList(DB_VIEW_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpViewData> ret = new List<OpViewData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpViewData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpViewData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpView> ViewDict = dataProvider.GetView(list.Select(l => l.ID_VIEW).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VIEW);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.View = ViewDict.TryGetValue(loop.ID_VIEW);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpViewData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpViewData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpViewData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpViewData> Members
        public bool Equals(OpViewData other)
        {
            if (other == null)
                return false;
            return this.IdViewData.Equals(other.IdViewData);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpViewData)
                return this.IdViewData == ((OpViewData)obj).IdViewData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpViewData left, OpViewData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpViewData left, OpViewData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdViewData.GetHashCode();
        }
        #endregion

        #region IOpState
        public OpChangeState OpState { get; set; }

        #endregion

        #region IOpData

        public long IdData { get { return IdViewData; } set { IdViewData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object ReferencedObject { get; set; }

        #endregion

        #region IOpDataProvider

        #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
        #endregion

        #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpViewData clone = new OpViewData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.IdView = this.IdView;

            if (dataProvider != null)
                clone = ConvertList(new DB_VIEW_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        }
        #endregion

        #endregion
    }
}