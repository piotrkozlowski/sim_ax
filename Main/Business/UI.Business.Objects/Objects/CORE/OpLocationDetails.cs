﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLocationDetails : DB_LOCATION_DETAILS, IComparable, IEquatable<OpLocationDetails>, IReferenceType, IOpDynamic, IOpDynamicProperty<OpLocationDetails>
    {
    #region Properties
        public long IdLocationDetails { get { return this.ID_LOCATION_DETAILS; } set { this.ID_LOCATION_DETAILS = value; } }
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public string Cid { get { return this.CID; } set { this.CID = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string City { get { return this.CITY; } set { this.CITY = value; } }
        public string Address { get { return this.ADDRESS; } set { this.ADDRESS = value; } }
        public string Zip { get { return this.ZIP; } set { this.ZIP = value; } }
        public string Country { get { return this.COUNTRY; } set { this.COUNTRY = value; } }
        public string Latitude { get { return this.LATITUDE; } set { this.LATITUDE = value; } }
        public string Longitude { get { return this.LONGITUDE; } set { this.LONGITUDE = value; } }
        public int? ImrServer { get { return this.IMR_SERVER; } set { this.IMR_SERVER = value; } }
        public DateTime? CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public string LocationMemo { get { return this.LOCATION_MEMO; } set { this.LOCATION_MEMO = value; } }
        public long? LocationSourceServerId { get { return this.LOCATION_SOURCE_SERVER_ID; } set { this.LOCATION_SOURCE_SERVER_ID = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        public int IdLocationType { get { return this.ID_LOCATION_TYPE; } set { this.ID_LOCATION_TYPE = value; } }
        public int IdLocationStateType { get { return this.ID_LOCATION_STATE_TYPE; } set { this.ID_LOCATION_STATE_TYPE = value; } }
        public bool InKpi { get { return this.IN_KPI; } set { this.IN_KPI = value; } }
        public bool AllowGrouping { get { return this.ALLOW_GROUPING; } set { this.ALLOW_GROUPING = value; } }
    #endregion

    #region	Navigation Properties
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpImrServer _IMRServer;
        public OpImrServer IMRServer { get { return this._IMRServer; } set { this._IMRServer = value; this.IMR_SERVER = (value == null) ? 0 : (int)value.ID_IMR_SERVER; } }
        private OpLocationType _LocationType;
        public OpLocationType LocationType { get { return this._LocationType; } set { this._LocationType = value; this.ID_LOCATION_TYPE = (value == null) ? 0 : (int)value.ID_LOCATION_TYPE; } }
        private OpLocationStateType _LocationStateType;
        public OpLocationStateType LocationStateType { get { return this._LocationStateType; } set { this._LocationStateType = value; this.ID_LOCATION_STATE_TYPE = (value == null) ? 0 : (int)value.ID_LOCATION_STATE_TYPE; } }
    #endregion

    #region	Custom Properties
        public List<int> IdLocationGroup { get; set; }
        public string FullAddress
        {
            get
            {
                StringBuilder strFullAddres = new StringBuilder();
                if (!String.IsNullOrEmpty(this.Zip))
                    strFullAddres.AppendFormat("{0} ", this.Zip);
                if (!String.IsNullOrEmpty(this.City))
                    strFullAddres.AppendFormat("{0}, ", this.City);
                if (!String.IsNullOrEmpty(this.Address))
                    strFullAddres.AppendFormat("{0} ", this.Address);
                return strFullAddres.ToString();
            }
        }
    #endregion

    #region	Ctor
        public OpLocationDetails()
            : base() { }

        public OpLocationDetails(DB_LOCATION_DETAILS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return (String.IsNullOrEmpty(Name) ? CID : Name) + " " + Address;
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationDetails> ConvertList(DB_LOCATION_DETAILS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpLocationDetails> ConvertList(DB_LOCATION_DETAILS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true)
        {
            List<OpLocationDetails> ret = new List<OpLocationDetails>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpLocationDetails(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpLocationDetails> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpLocationType> LocationTypeDict = dataProvider.GetLocationType(list.Select(l => l.ID_LOCATION_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION_TYPE);
                Dictionary<int, OpLocationStateType> LocationStateTypeDict = dataProvider.GetLocationStateType(list.Select(l => l.ID_LOCATION_STATE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION_STATE_TYPE);
                Dictionary<int, OpImrServer> ImrServerDict = dataProvider.GetImrServer(list.Where(l => l.IMR_SERVER != null).Select(l => l.IMR_SERVER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_IMR_SERVER);

                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.LocationType = LocationTypeDict.TryGetValue(loop.ID_LOCATION_TYPE);
                    loop.LocationStateType = LocationStateTypeDict.TryGetValue(loop.ID_LOCATION_STATE_TYPE);
                    if(loop.ImrServer.HasValue)
                        loop.IMRServer = ImrServerDict.TryGetValue(loop.ImrServer.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationDetails> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                List<OpData> locationGroupData = dataProvider.GetDataFilter(
                            loadNavigationProperties: false,
                            IdLocation: list.Select(w => w.IdLocation).ToArray(),
                            IdDataType: new long[] { DataType.LOCATION_DISTRIBUTOR_GROUP_ID });
                if (locationGroupData != null && locationGroupData.Count > 0)
                {
                    Dictionary<long, OpLocationDetails> LocationDetailsDict = list.ToDictionary<OpLocationDetails, long>(l => l.ID_LOCATION);
                    foreach (OpData dataItem in locationGroupData)
                    {
                        if(dataItem.IdLocation.HasValue)
                        {
                            if (LocationDetailsDict[dataItem.IdLocation.Value].IdLocationGroup == null)
                                LocationDetailsDict[dataItem.IdLocation.Value].IdLocationGroup =  new List<int>();
                            LocationDetailsDict[dataItem.IdLocation.Value].IdLocationGroup.Add(Convert.ToInt32(dataItem.Value));
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationDetails)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationDetails).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpLocationDetails> Members
        public bool Equals(OpLocationDetails other)
        {
            if (other == null)
                return false;
            return this.IdLocationDetails.Equals(other.IdLocationDetails);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpLocationDetails)
                return this.IdLocationDetails == ((OpLocationDetails)obj).IdLocationDetails;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpLocationDetails left, OpLocationDetails right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpLocationDetails left, OpLocationDetails right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdLocationDetails.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdLocation;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

        private Dictionary<string, object> dynamicValues = new Dictionary<string, object>();
        private OpDynamicPropertyDict dynamicProperties = new OpDynamicPropertyDict();

    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdLocation; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        [XmlIgnore]
        OpLocationDetails IOpDynamicProperty<OpLocationDetails>.Owner
        {
            get { return this; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
    #endregion
    }
#endif
}