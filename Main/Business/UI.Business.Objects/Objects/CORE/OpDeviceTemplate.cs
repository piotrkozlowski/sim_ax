using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTemplate : DB_DEVICE_TEMPLATE, IComparable, IEquatable<OpDeviceTemplate>
    {
    #region Properties
        [XmlIgnore]
        public int IdTemplate { get { return this.ID_TEMPLATE; } set { this.ID_TEMPLATE = value; } }
        [XmlIgnore]
        public int? IdTemplatePattern { get { return this.ID_TEMPLATE_PATTERN; } set { this.ID_TEMPLATE_PATTERN = value; } }
        [XmlIgnore]
        public int IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = value; } }
        [XmlIgnore]
        public string FirmwareVersion { get { return this.FIRMWARE_VERSION; } set { this.FIRMWARE_VERSION = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public int CreatedBy { get { return this.CREATED_BY; } set { this.CREATED_BY = value; } }
        [XmlIgnore]
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        [XmlIgnore]
        public int? ConfirmedBy { get { return this.CONFIRMED_BY; } set { this.CONFIRMED_BY = value; } }
        [XmlIgnore]
        public DateTime? ConfirmDate { get { return this.CONFIRM_DATE; } set { this.CONFIRM_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceType _DeviceType;
        [XmlIgnore]
        public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.ID_DEVICE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
        private OpDeviceTemplate _TemplatePattern;
        public OpDeviceTemplate TemplatePattern { get { return this._TemplatePattern; } set { this._TemplatePattern = value; this.ID_TEMPLATE_PATTERN = (value == null) ? null : (int?)value.ID_TEMPLATE; } }
        private OpOperator _CreatedOperator;
        public OpOperator CreatedOperator { get { return this._CreatedOperator; } set { this._CreatedOperator = value; this.CREATED_BY = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _ConfirmedOperator;
        public OpOperator ConfirmedOperator { get { return this._ConfirmedOperator; } set { this._ConfirmedOperator = value; this.CONFIRMED_BY = (value == null) ? null : (int?)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
        public List<OpDeviceTemplateData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpDeviceTemplate()
            : base()
        {
            this.DataList = new List<OpDeviceTemplateData>();
        }

        public OpDeviceTemplate(DB_DEVICE_TEMPLATE clone)
            : base(clone)
        {
            this.DataList = new List<OpDeviceTemplateData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceTemplate> ConvertList(DB_DEVICE_TEMPLATE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceTemplate> ConvertList(DB_DEVICE_TEMPLATE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceTemplate> ret = new List<OpDeviceTemplate>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceTemplate(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceTemplate> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Select(l => l.ID_DEVICE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE);
                Dictionary<int, OpDeviceTemplate> TemplatePatternDict = dataProvider.GetDeviceTemplate(list.Where(l => l.ID_TEMPLATE_PATTERN.HasValue).Select(l => l.ID_TEMPLATE_PATTERN.Value).Distinct().ToArray()).ToDictionary(l => l.ID_TEMPLATE);
                Dictionary<int, OpOperator> OperatorCreatedDict = dataProvider.GetOperator(list.Select(l => l.CREATED_BY).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorConfirmedDict = dataProvider.GetOperator(list.Where(l => l.CONFIRMED_BY.HasValue).Select(l => l.CONFIRMED_BY.Value).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE);
                    if (loop.ID_TEMPLATE_PATTERN.HasValue)
                        loop.TemplatePattern = TemplatePatternDict.TryGetValue(loop.ID_TEMPLATE_PATTERN.Value);
                    loop.CreatedOperator = OperatorCreatedDict.TryGetValue(loop.CREATED_BY);
                    if (loop.CONFIRMED_BY.HasValue)
                        loop.ConfirmedOperator = OperatorConfirmedDict.TryGetValue(loop.CONFIRMED_BY.Value);

                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceTemplate> list, IDataProvider dataProvider)
        {
            foreach (OpDeviceTemplate deviceTemplate in list)
            {
                deviceTemplate.DataList = dataProvider.GetDeviceTemplateData(deviceTemplate.IdTemplate);
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTemplate)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTemplate).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceTemplate> Members
        public bool Equals(OpDeviceTemplate other)
        {
            if (other == null)
                return false;
            return this.IdTemplate.Equals(other.IdTemplate);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public OpDeviceTemplate Clone(IDataProvider dataProvider)
        {
            OpDeviceTemplate clone = new OpDeviceTemplate();
            clone.IdTemplate = this.IdTemplate;
            clone.IdTemplatePattern = this.IdTemplatePattern;
            clone.IdDeviceType = this.IdDeviceType;
            clone.FirmwareVersion = this.FirmwareVersion;
            clone.Name = this.Name;
            clone.CreatedBy = this.CreatedBy;
            clone.CreationDate = this.CreationDate;
            clone.ConfirmedBy = this.ConfirmedBy;
            clone.ConfirmDate = this.ConfirmDate;
            clone.DataList = this.DataList;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEVICE_TEMPLATE[] { clone }, dataProvider)[0];

            return clone;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceTemplate)
                return this.IdTemplate == ((OpDeviceTemplate)obj).IdTemplate;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceTemplate left, OpDeviceTemplate right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceTemplate left, OpDeviceTemplate right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTemplate.GetHashCode();
        }
    #endregion
    }
#endif
}