using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpActionSmsTextData : DB_ACTION_SMS_TEXT_DATA, IComparable, IEquatable<OpActionSmsTextData>
    {
    #region Properties
        [XmlIgnore]
        public long IdActionSmsText { get { return this.ID_ACTION_SMS_TEXT;} set {this.ID_ACTION_SMS_TEXT = value;}}
        [XmlIgnore]
        public int IdLanguage { get { return this.ID_LANGUAGE;} set {this.ID_LANGUAGE = value;}}
        [XmlIgnore]
        public int ArgNbr { get { return this.ARG_NBR; } set { this.ARG_NBR = value; OpState = OpChangeState.Modified; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; OpState = OpChangeState.Modified; } }
        [XmlIgnore]
        public int? IdUnit { get { return this.ID_UNIT; } set { this.ID_UNIT = value; OpState = OpChangeState.Modified; } }
    #endregion

    #region	Navigation Properties
		private OpActionSmsText _ActionSmsText;
        [XmlIgnore]
		public OpActionSmsText ActionSmsText { get { return this._ActionSmsText; } set { this._ActionSmsText = value; this.ID_ACTION_SMS_TEXT = (value == null)? 0 : (long)value.ID_ACTION_SMS_TEXT; } }
		private OpLanguage _Language;
        [XmlIgnore]
		public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null)? 0 : (int)value.ID_LANGUAGE; } }
		private OpDataType _DataType;
        [XmlIgnore]
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
		private OpUnit _Unit;
        [XmlIgnore]
		public OpUnit Unit { get { return this._Unit; } set { this._Unit = value; this.ID_UNIT = (value == null)? null : (int?)value.ID_UNIT; } }
    #endregion

    #region	Custom Properties
                public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpActionSmsTextData()
                    : base() { OpState = OpChangeState.New; }
		
		public OpActionSmsTextData(DB_ACTION_SMS_TEXT_DATA clone)
            : base(clone) { OpState = OpChangeState.Loaded; }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ActionSmsTextData [" + IdActionSmsText.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpActionSmsTextData> ConvertList(DB_ACTION_SMS_TEXT_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpActionSmsTextData> ConvertList(DB_ACTION_SMS_TEXT_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpActionSmsTextData> ret = new List<OpActionSmsTextData>(list.Length);
			foreach (var loop in list)
			{
				OpActionSmsTextData insert = new OpActionSmsTextData(loop);
				ret.Add(insert);
			}
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpActionSmsTextData> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray()).ToDictionary(l => l.ID_LANGUAGE);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
	Dictionary<int,OpUnit> UnitDict = dataProvider.GetUnit(list.Where(l => l.ID_UNIT.HasValue).Select(l => l.ID_UNIT.Value).Distinct().ToArray()).ToDictionary(l => l.ID_UNIT);			
				foreach (var loop in list)
				{
						loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE); 
		loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
		if (loop.ID_UNIT.HasValue)
						loop.Unit = UnitDict.TryGetValue(loop.ID_UNIT.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpActionSmsTextData> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionSmsTextData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionSmsTextData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpActionSmsTextData> Members
        public bool Equals(OpActionSmsTextData other)
        {
            if (other == null)
				return false;
			return this.IdActionSmsText.Equals(other.IdActionSmsText) && this.IdLanguage.Equals(other.IdLanguage) && this.ArgNbr.Equals(other.ArgNbr);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpActionSmsTextData)
                return this.IdActionSmsText == ((OpActionSmsTextData)obj).IdActionSmsText && this.IdLanguage == ((OpActionSmsTextData)obj).IdLanguage && this.ArgNbr == ((OpActionSmsTextData)obj).ArgNbr;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpActionSmsTextData left, OpActionSmsTextData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpActionSmsTextData left, OpActionSmsTextData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdActionSmsText.GetHashCode();
		}
    #endregion
    }
#endif
}