﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceListDeviceAction : DB_SERVICE_LIST_DEVICE_ACTION, IComparable, IEquatable<OpServiceListDeviceAction>
    {
    #region Properties
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public long IdServicePackage { get { return this.ID_SERVICE_PACKAGE; } set { this.ID_SERVICE_PACKAGE = value; } }
        public long? IdServiceCustom { get { return this.ID_SERVICE_CUSTOM; } set { this.ID_SERVICE_CUSTOM = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? 0 : (int)value.ID_SERVICE_LIST; } }
        private OpServicePackage _ServicePackage;
        public OpServicePackage ServicePackage { get { return this._ServicePackage; } set { this._ServicePackage = value; this.ID_SERVICE_PACKAGE = (value == null) ? 0 : (long)value.ID_SERVICE_PACKAGE; } }
        private OpServiceCustom _ServiceCustom;
        public OpServiceCustom ServiceCustom { get { return this._ServiceCustom; } set { this._ServiceCustom = value; this.ID_SERVICE_CUSTOM = (value == null) ? 0 : (long)value.ID_SERVICE_CUSTOM; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpServiceListDeviceAction()
            : base() { }

        public OpServiceListDeviceAction(DB_SERVICE_LIST_DEVICE_ACTION clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceListDeviceAction [" + SerialNbr.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceListDeviceAction> ConvertList(DB_SERVICE_LIST_DEVICE_ACTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceListDeviceAction> ConvertList(DB_SERVICE_LIST_DEVICE_ACTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceListDeviceAction> ret = new List<OpServiceListDeviceAction>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceListDeviceAction(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceListDeviceAction> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Select(l => l.ID_SERVICE_LIST).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<long, OpServicePackage> ServicePackageDict = dataProvider.GetServicePackage(list.Select(l => l.ID_SERVICE_PACKAGE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_PACKAGE);
                Dictionary<long, OpServiceCustom> ServiceCustomDict = dataProvider.GetServiceCustom(list.Where(l => l.ID_SERVICE_CUSTOM != null).Select(l => l.ID_SERVICE_CUSTOM.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_CUSTOM);
                foreach (var loop in list)
                {
                    loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST);
                    loop.ServicePackage = ServicePackageDict.TryGetValue(loop.ID_SERVICE_PACKAGE);
                    loop.ServiceCustom = ServiceCustomDict.TryGetValue(loop.ID_SERVICE_CUSTOM.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceListDeviceAction> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceListDeviceAction)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceListDeviceAction).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceListDeviceAction> Members
        public bool Equals(OpServiceListDeviceAction other)
        {
            if (other == null)
                return false;
            return this.SerialNbr.Equals(other.SerialNbr);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceListDeviceAction)
                return this.SerialNbr == ((OpServiceListDeviceAction)obj).SerialNbr;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceListDeviceAction left, OpServiceListDeviceAction right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceListDeviceAction left, OpServiceListDeviceAction right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return SerialNbr.GetHashCode();
        }
    #endregion
    }
#endif
}