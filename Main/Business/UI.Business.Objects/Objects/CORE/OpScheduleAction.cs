using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpScheduleAction : DB_SCHEDULE_ACTION, IComparable, IEquatable<OpScheduleAction>, IOpChangeState
    {
    #region Properties
        public long IdScheduleAction { get { return this.ID_SCHEDULE_ACTION; } set { this.ID_SCHEDULE_ACTION = OpDataUtil.SetNewValue(this.ID_SCHEDULE_ACTION, value, this); } }
        public int IdSchedule { get { return this.ID_SCHEDULE; } set { this.ID_SCHEDULE = OpDataUtil.SetNewValue(this.ID_SCHEDULE, value, this); } }
        public long IdActionDef { get { return this.ID_ACTION_DEF; } set { this.ID_ACTION_DEF = OpDataUtil.SetNewValue(this.ID_ACTION_DEF, value, this); } }
        public bool IsEnabled { get { return this.IS_ENABLED; } set { this.IS_ENABLED = OpDataUtil.SetNewValue(this.IS_ENABLED, value, this); } }
        public int? IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = OpDataUtil.SetNewValue(this.ID_OPERATOR, value, this); } }
        public int? IdProfile { get { return this.ID_PROFILE; } set { this.ID_PROFILE = OpDataUtil.SetNewValue(this.ID_PROFILE, value, this); } }
    #endregion

    #region	Navigation Properties
		private OpSchedule _Schedule;
		public OpSchedule Schedule { get { return this._Schedule; } set { this._Schedule = value; this.IdSchedule = (value == null)? 0 : (int)value.ID_SCHEDULE; } }
		private OpActionDef _ActionDef;
		public OpActionDef ActionDef { get { return this._ActionDef; } set { this._ActionDef = value; this.IdActionDef = (value == null)? 0 : (long)value.ID_ACTION_DEF; } }
		private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.IdOperator = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        private OpProfile _Profile;
        public OpProfile Profile { get { return this._Profile; } set { this._Profile = value; this.IdProfile = (value == null) ? null : (int?)value.ID_PROFILE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpScheduleAction()
			:base() 
        {
            this.OpState = OpChangeState.New;
        }
		
		public OpScheduleAction(DB_SCHEDULE_ACTION clone)
			:base(clone) 
        {
            this.OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ScheduleAction [" + IdScheduleAction.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpScheduleAction> ConvertList(DB_SCHEDULE_ACTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpScheduleAction> ConvertList(DB_SCHEDULE_ACTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool useDBCollector = false)
		{
			List<OpScheduleAction> ret = new List<OpScheduleAction>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpScheduleAction(db_object)));
			
			if(loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, useDBCollector); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpScheduleAction> list, IDataProvider dataProvider, bool useDBCollector = false)
        {
			if (list != null && list.Count > 0)
            {
                bool? dbCollectorEnabled = dataProvider.IsDBCollectorEnabled(useDBCollector);
                Dictionary<int, Dictionary<int, OpSchedule>> ScheduleDict = new Dictionary<int, Dictionary<int, OpSchedule>>(); //dataProvider.GetSchedule(list.Select(l => l.ID_SCHEDULE).Distinct().ToArray()).ToDictionary(l => l.ID_SCHEDULE);
                Dictionary<int, Dictionary<long, OpActionDef>> ActionDefDict = new Dictionary<int, Dictionary<long, OpActionDef>>(); //dataProvider.GetActionDef(list.Select(l => l.ID_ACTION_DEF).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_DEF);
	            Dictionary<int,OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, Dictionary<int, OpProfile>> ProfileDict = new Dictionary<int, Dictionary<int, OpProfile>>();
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    #region Profile
                    //List<OpProfile> pList = OpProfile.ConvertList(dataProvider.dbCollectorClient.GetProfileFilter(ref dataProvider.dbCollectorSession, 
                    //                                                                                              list.Where(l => l.ID_PROFILE.HasValue).Select(l => l.ID_PROFILE.Value).Distinct().ToArray(),
                    //                                                                                              null,null, null, null, null, false, System.Data.IsolationLevel.ReadUncommitted, 0), dataProvider, true, useDBCollector);
                    List<OpProfile> pList = dataProvider.GetProfileFilter(loadNavigationProperties: true,
                                                                          IdProfile: list.Where(l => l.ID_PROFILE.HasValue).Select(l => l.ID_PROFILE.Value).Distinct().ToArray(),
                                                                          transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                          autoTransaction: false,
                                                                          commandTimeout: 0,
                                                                          useDBCollector: useDBCollector);

                    ProfileDict.Add(0, new Dictionary<int, OpProfile>());
                    pList.Where(p => p.ID_SERVER.HasValue).Select(p => p.ID_SERVER.Value).Distinct().ToList().ForEach(p => ProfileDict.Add(p, new Dictionary<int, OpProfile>()));
                    foreach (OpProfile pItem in pList)
                    {
                        if (!pItem.ID_SERVER.HasValue)
                            ProfileDict[0].Add(pItem.IdProfile, pItem);
                        else
                            ProfileDict[pItem.ID_SERVER.Value].Add(pItem.IdProfile, pItem);
                        //ProfileDict = .ToDictionary(l => l.ID_PROFILE);
                    }
                    #endregion
                    #region ActionDef
                    //List<OpActionDef> adList = OpActionDef.ConvertList(dataProvider.dbCollectorClient.GetActionDefFilter(ref dataProvider.dbCollectorSession,
                    //                                                                                                    list.Select(l => l.ID_ACTION_DEF).Distinct().ToArray(),
                    //                                                                                                    null, null, null, null, null, null, null, null, null, false, System.Data.IsolationLevel.ReadUncommitted, 0), dataProvider, true, useDBCollector);
                    List<OpActionDef> adList = dataProvider.GetActionDefFilter(loadNavigationProperties: true,
                                                                                IdActionDef: list.Select(l => l.ID_ACTION_DEF).Distinct().ToArray(),
                                                                                transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                autoTransaction: false,
                                                                                useDBCollector: useDBCollector,
                                                                                commandTimeout: 0);

                    ActionDefDict.Add(0, new Dictionary<long, OpActionDef>());
                    adList.Where(p => p.ID_SERVER.HasValue).Select(p => p.ID_SERVER.Value).Distinct().ToList().ForEach(p => ActionDefDict.Add(p, new Dictionary<long, OpActionDef>()));
                    foreach (OpActionDef adItem in adList)
                    {
                        if (!adItem.ID_SERVER.HasValue)
                            ActionDefDict[0].Add(adItem.IdActionDef, adItem);
                        else
                            ActionDefDict[adItem.ID_SERVER.Value].Add(adItem.IdActionDef, adItem);
                    }
                    #endregion
                    #region Schedule
                    //List<OpSchedule> sList = OpSchedule.ConvertList(dataProvider.dbCollectorClient.GetScheduleFilter(ref dataProvider.dbCollectorSession,
                    //                                                                                                    list.Select(l => l.ID_SCHEDULE).Distinct().ToArray(),
                    //                                                                                                    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null, 
                    //                                                                                                    false, System.Data.IsolationLevel.ReadUncommitted, 0), dataProvider);
                    List<OpSchedule> sList = dataProvider.GetScheduleFilter(loadNavigationProperties: true,
                                                                            IdSchedule: list.Select(l => l.ID_SCHEDULE).Distinct().ToArray(),
                                                                            useDBCollector: useDBCollector,
                                                                            commandTimeout: 0,
                                                                            autoTransaction: false,
                                                                            transactionLevel: System.Data.IsolationLevel.ReadUncommitted);

                    ScheduleDict.Add(0, new Dictionary<int, OpSchedule>());
                    sList.Where(p => p.ID_SERVER.HasValue).Select(p => p.ID_SERVER.Value).Distinct().ToList().ForEach(p => ScheduleDict.Add(p, new Dictionary<int, OpSchedule>()));
                    foreach (OpSchedule sItem in sList)
                    {
                        if (!sItem.ID_SERVER.HasValue)
                            ScheduleDict[0].Add(sItem.IdSchedule, sItem);
                        else
                            ScheduleDict[sItem.ID_SERVER.Value].Add(sItem.IdSchedule, sItem);
                    }
    #endregion
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
    #region Profile
                    ProfileDict.Add(0, new Dictionary<int, OpProfile>());
                    ProfileDict[0] = dataProvider.GetProfile(list.Where(l => l.ID_PROFILE.HasValue).Select(l => l.ID_PROFILE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_PROFILE);
    #endregion
    #region ActionDef
                    ActionDefDict.Add(0, new Dictionary<long, OpActionDef>());
                    ActionDefDict[0] = dataProvider.GetActionDef(list.Select(l => l.ID_ACTION_DEF).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_DEF);
    #endregion
    #region Schedule
                    ScheduleDict.Add(0, new Dictionary<int, OpSchedule>());
                    ScheduleDict[0] = dataProvider.GetSchedule(list.Select(l => l.ID_SCHEDULE).Distinct().ToArray()).ToDictionary(l => l.ID_SCHEDULE);
    #endregion
                }
				foreach (var loop in list)
				{
                    if (loop.ID_SERVER.HasValue)
                    {
                        if(ScheduleDict.ContainsKey(loop.ID_SERVER.Value))
                            loop.Schedule = ScheduleDict[loop.ID_SERVER.Value].TryGetValue(loop.ID_SCHEDULE);
                    }
                    else
                        loop.Schedule = ScheduleDict[0].TryGetValue(loop.ID_SCHEDULE);
                    if (loop.ID_SERVER.HasValue)
                    {
                        if (ActionDefDict.ContainsKey(loop.ID_SERVER.Value))
                            loop.ActionDef = ActionDefDict[loop.ID_SERVER.Value].TryGetValue(loop.ID_ACTION_DEF);
                    }
                    else
                        loop.ActionDef = ActionDefDict[0].TryGetValue(loop.ID_ACTION_DEF);
                    if (loop.ID_OPERATOR.HasValue)
                    {
                        loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value);
                    }
                    if (loop.ID_PROFILE.HasValue)
                    {
                        if (loop.ID_SERVER.HasValue)
                        {
                            if (ProfileDict.ContainsKey(loop.ID_SERVER.Value))
                                loop.Profile = ProfileDict[loop.ID_SERVER.Value].TryGetValue(loop.ID_PROFILE.Value);
                        }
                        else
                            loop.Profile = ProfileDict[0].TryGetValue(loop.ID_PROFILE.Value);
                    }
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpScheduleAction> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpScheduleAction)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpScheduleAction).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpScheduleAction> Members
        public bool Equals(OpScheduleAction other)
        {
            if (other == null)
				return false;
            return this.IdScheduleAction == other.IdScheduleAction;
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
            OpScheduleAction castObj = obj as OpScheduleAction;
            if (castObj != null)
                return this.IdScheduleAction == castObj.IdScheduleAction;
            else
                return base.Equals(obj);
		}
		public static bool operator ==(OpScheduleAction left, OpScheduleAction right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpScheduleAction left, OpScheduleAction right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
            return IdScheduleAction.GetHashCode();
		}
    #endregion
    }
#endif
}