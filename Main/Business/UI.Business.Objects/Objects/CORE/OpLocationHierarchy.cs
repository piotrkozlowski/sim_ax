using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;

using IMR.Suite.Common;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpLocationHierarchy : DB_LOCATION_HIERARCHY, IComparable, IOpDataProperty
    {
    #region Properties
        [XmlIgnore]
        public long IdLocationHierarchy { get { return this.ID_LOCATION_HIERARCHY; } set { this.ID_LOCATION_HIERARCHY = value; } }
        [XmlIgnore]
        public long? IdLocationHierarchyParent { get { return this.ID_LOCATION_HIERARCHY_PARENT; } set { this.ID_LOCATION_HIERARCHY_PARENT = value; } }
        [XmlIgnore]
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        [XmlIgnore]
        public string Hierarchy { get { return this.HIERARCHY; } set { this.HIERARCHY = value; } }
    #endregion

    #region	Navigation Properties
        private OpLocation _Location;
        //[XmlIgnore]
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? 0 : (long)value.ID_LOCATION; } }

        private OpLocationHierarchy _LocationHierarchyParent;
        public OpLocationHierarchy LocationHierarchyParent { get { return this._LocationHierarchyParent; } set { this._LocationHierarchyParent = value; this.ID_LOCATION_HIERARCHY_PARENT = (value == null) ? null : (long?)value.ID_LOCATION_HIERARCHY; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpLocationHierarchy()
            : base() { }

        public OpLocationHierarchy(DB_LOCATION_HIERARCHY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.HIERARCHY;
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationHierarchy> ConvertList(DB_LOCATION_HIERARCHY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLocationHierarchy> ConvertList(DB_LOCATION_HIERARCHY[] list, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            Dictionary<long, OpLocationHierarchy> dict = new Dictionary<long,OpLocationHierarchy>();
            
            List<OpLocationHierarchy> ret = new List<OpLocationHierarchy>(list.Length);
            foreach (var loop in list)
            {
                OpLocationHierarchy insert = new OpLocationHierarchy(loop);

                dict[insert.ID_LOCATION_HIERARCHY] = insert;
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        private static void LoadNavigationProperties(ref List<OpLocationHierarchy> list, IDataProvider dataProvider, bool queryDatabase = false, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            Dictionary<long, OpLocation> locationsDict = dataProvider.GetLocation(list.Select(l => l.ID_LOCATION).Distinct().ToArray(), queryDatabase, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);
            Dictionary<long, OpLocationHierarchy> locHierDict = list.ToDictionary(l => l.IdLocationHierarchy);
            
            foreach (var loop in list)
            {
                if (locationsDict.ContainsKey(loop.ID_LOCATION))
                    loop.Location = locationsDict[loop.ID_LOCATION];

                if (loop.ID_LOCATION_HIERARCHY_PARENT.HasValue && locHierDict.ContainsKey(loop.ID_LOCATION_HIERARCHY_PARENT.Value))
                    loop.LocationHierarchyParent = locHierDict[loop.ID_LOCATION_HIERARCHY_PARENT.Value];
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationHierarchy> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationHierarchy)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationHierarchy).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Implementation of IOpDataProperty

        public object GetOpDataPropertyValue(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return null;

            if (this.Location != null)
                return this.Location.GetOpDataPropertyValue(opDataProperty);
            else
                return null;
        }
        public void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return;
     
            if (this.Location != null)
                this.Location.SetOpDataPropertyValue(opDataProperty, value);
        }
        public Type GetOpDataPropertyType(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null || !opDataProperty.ReturnReferencedTypeIfHelper.HasValue) return null;
            Type type = Utils.GetHelperType(opDataProperty.OpDataType, opDataProperty.ReturnReferencedTypeIfHelper.Value);

            if (type == null)
                type = DataType.GetSystemType(opDataProperty.OpDataType.IdDataTypeClass);

            return type;
        }

        #endregion
    }
#endif
}