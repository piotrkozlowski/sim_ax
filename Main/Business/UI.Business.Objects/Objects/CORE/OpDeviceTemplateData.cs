using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTemplateData : DB_DEVICE_TEMPLATE_DATA, IComparable, IEquatable<OpDeviceTemplateData>, IOpChangeState
    {
    #region Properties
        [XmlIgnore]
        public int IdTemplate { get { return this.ID_TEMPLATE; } set { this.ID_TEMPLATE = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region	Navigation Properties
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
        public Enums.DataStatus DataStatus { get; set; }
    #endregion

    #region	Ctor
        public OpDeviceTemplateData()
            : base() { this.OpState = OpChangeState.New; }

        public OpDeviceTemplateData(DB_DEVICE_TEMPLATE_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeviceTemplateData [" + IdTemplate.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceTemplateData> ConvertList(DB_DEVICE_TEMPLATE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceTemplateData> ConvertList(DB_DEVICE_TEMPLATE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceTemplateData> ret = new List<OpDeviceTemplateData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceTemplateData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceTemplateData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceTemplateData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDeviceTemplateData clone = new OpDeviceTemplateData();
            clone.IdTemplate = this.IdTemplate;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.DataStatus = this.DataStatus;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEVICE_TEMPLATE_DATA [] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        }
    #endregion


    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTemplateData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTemplateData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceTemplateData> Members
        public bool Equals(OpDeviceTemplateData other)
        {
            if (other == null)
                return false;
            return this.IdTemplate.Equals(other.IdTemplate);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceTemplateData)
                return this.IdTemplate == ((OpDeviceTemplateData)obj).IdTemplate;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceTemplateData left, OpDeviceTemplateData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceTemplateData left, OpDeviceTemplateData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTemplate.GetHashCode();
        }
    #endregion
    }
#endif
}