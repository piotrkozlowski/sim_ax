using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpSlotType : DB_SLOT_TYPE, IComparable, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdSlotType { get { return this.ID_SLOT_TYPE; } set { this.ID_SLOT_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpSlotType()
            : base() { }

        public OpSlotType(DB_SLOT_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpSlotType> ConvertList(DB_SLOT_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpSlotType> ConvertList(DB_SLOT_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpSlotType> ret = new List<OpSlotType>(list.Length);
            foreach (var loop in list)
            {
                OpSlotType insert = new OpSlotType(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpSlotType> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSlotType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSlotType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdSlotType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}