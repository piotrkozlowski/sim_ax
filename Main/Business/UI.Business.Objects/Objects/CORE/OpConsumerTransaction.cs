using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.CORE;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerTransaction : DB_CONSUMER_TRANSACTION, IComparable, IEquatable<OpConsumerTransaction>
    {
    #region Properties
        public long IdConsumerTransaction { get { return this.ID_CONSUMER_TRANSACTION;} set {this.ID_CONSUMER_TRANSACTION = value;}}
        public int IdConsumer { get { return this.ID_CONSUMER;} set {this.ID_CONSUMER = value;}}
        public int? IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
        public int IdDistributor { get { return this.ID_DISTRIBUTOR;} set {this.ID_DISTRIBUTOR = value;}}
        public int IdConsumerTransactionType { get { return this.ID_CONSUMER_TRANSACTION_TYPE;} set {this.ID_CONSUMER_TRANSACTION_TYPE = value;}}
        public int? IdTariff { get { return this.ID_TARIFF;} set {this.ID_TARIFF = value;}}
        public DateTime Time { get { return this.TIME;} set {this.TIME = value;}}
        public Decimal ChargePrepaid { get { return this.CHARGE_PREPAID;} set {this.CHARGE_PREPAID = value;}}
        public Decimal ChargeEc { get { return this.CHARGE_EC;} set {this.CHARGE_EC = value;}}
        public Decimal ChargeOwed { get { return this.CHARGE_OWED;} set {this.CHARGE_OWED = value;}}
        public Decimal BalancePrepaid { get { return this.BALANCE_PREPAID;} set {this.BALANCE_PREPAID = value;}}
        public Decimal BalanceEc { get { return this.BALANCE_EC;} set {this.BALANCE_EC = value;}}
        public Decimal BalanceOwed { get { return this.BALANCE_OWED;} set {this.BALANCE_OWED = value;}}
        public Double? MeterIndex { get { return this.METER_INDEX;} set {this.METER_INDEX = value;}}
        public long? IdConsumerSettlement { get { return this.ID_CONSUMER_SETTLEMENT;} set {this.ID_CONSUMER_SETTLEMENT = value;}}
        public int? IdPaymentModule { get { return this.ID_PAYMENT_MODULE;} set {this.ID_PAYMENT_MODULE = value;}}
        public string TransactionGuid { get { return this.TRANSACTION_GUID;} set {this.TRANSACTION_GUID = value;}}
        public long? IdPacket { get { return this.ID_PACKET;} set {this.ID_PACKET = value;}}
        public long? IdAction { get { return this.ID_ACTION;} set {this.ID_ACTION = value;}}
        public long? IdPreviousConsumerTransaction { get { return this.ID_PREVIOUS_CONSUMER_TRANSACTION;} set {this.ID_PREVIOUS_CONSUMER_TRANSACTION = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumer _Consumer;
				public OpConsumer Consumer { get { return this._Consumer; } set { this._Consumer = value; this.ID_CONSUMER = (value == null)? 0 : (int)value.ID_CONSUMER; } }
				private OpOperator _Operator;
				public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? null : (int?)value.ID_OPERATOR; } }
				private OpDistributor _Distributor;
				public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null)? 0 : (int)value.ID_DISTRIBUTOR; } }
				private OpConsumerTransactionType _ConsumerTransactionType;
				public OpConsumerTransactionType ConsumerTransactionType { get { return this._ConsumerTransactionType; } set { this._ConsumerTransactionType = value; this.ID_CONSUMER_TRANSACTION_TYPE = (value == null)? 0 : (int)value.ID_CONSUMER_TRANSACTION_TYPE; } }
				private OpTariff _Tariff;
				public OpTariff Tariff { get { return this._Tariff; } set { this._Tariff = value; this.ID_TARIFF = (value == null)? null : (int?)value.ID_TARIFF; } }
				private OpConsumerSettlement _ConsumerSettlement;
				public OpConsumerSettlement ConsumerSettlement { get { return this._ConsumerSettlement; } set { this._ConsumerSettlement = value; this.ID_CONSUMER_SETTLEMENT = (value == null)? null : (long?)value.ID_CONSUMER_SETTLEMENT; } }
				private OpPaymentModule _PaymentModule;
				public OpPaymentModule PaymentModule { get { return this._PaymentModule; } set { this._PaymentModule = value; this.ID_PAYMENT_MODULE = (value == null)? null : (int?)value.ID_PAYMENT_MODULE; } }
				private OpAction _Action;
				public OpAction Action { get { return this._Action; } set { this._Action = value; this.ID_ACTION = (value == null)? null : (long?)value.ID_ACTION; } }
    #endregion

    #region	Custom Properties
        public Decimal TotalPaid
        {
            get
            {
                return ChargeOwed + ChargePrepaid;
            }
        }

        public Decimal BalancePrepaidBeforeTransaction
        {
            get { return this.BalancePrepaid - this.ChargePrepaid; }
        }

        public Decimal Debt
        {
            get { return BalanceEc + BalanceOwed; }
        }

        public Decimal Balance
        {
            get { return this.BalancePrepaid - this.BalanceEc; }
        }

        public Decimal PositiveChargePrepaid { get { return -ChargePrepaid; } }
        public Decimal PositiveChargeEc { get { return -ChargeEc; } }
        public Decimal PositiveChargeOwed { get { return -ChargeOwed; } }

        public Double? MeterIndexM3 { get { if (MeterIndex.HasValue) return MeterIndex / 1000.0; else return null; } }
    #endregion
		
    #region	Ctor
		public OpConsumerTransaction()
			:base() {}
		
		public OpConsumerTransaction(DB_CONSUMER_TRANSACTION clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.TRANSACTION_GUID;
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerTransaction> ConvertList(DB_CONSUMER_TRANSACTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerTransaction> ConvertList(DB_CONSUMER_TRANSACTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpConsumerTransaction> ret = new List<OpConsumerTransaction>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerTransaction(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerTransaction> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpConsumer> ConsumerDict = dataProvider.GetConsumer(list.Select(l => l.ID_CONSUMER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER);
	        Dictionary<int,OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
	        Dictionary<int,OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
            Dictionary<int, OpConsumerTransactionType> ConsumerTransactionTypeDict = dataProvider.GetConsumerTransactionType(list.Select(l => l.ID_CONSUMER_TRANSACTION_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER_TRANSACTION_TYPE);
            Dictionary<int, OpTariff> TariffDict = dataProvider.GetTariff(list.Where(l => l.ID_TARIFF.HasValue).Select(l => l.ID_TARIFF.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_TARIFF);
	        Dictionary<long,OpConsumerSettlement> ConsumerSettlementDict = dataProvider.GetConsumerSettlement(list.Where(l => l.ID_CONSUMER_SETTLEMENT.HasValue).Select(l => l.ID_CONSUMER_SETTLEMENT.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER_SETTLEMENT);
	        Dictionary<int,OpPaymentModule> PaymentModuleDict = dataProvider.GetPaymentModule(list.Where(l => l.ID_PAYMENT_MODULE.HasValue).Select(l => l.ID_PAYMENT_MODULE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PAYMENT_MODULE);
            Dictionary<long, OpAction> ActionDict = dataProvider.GetAction(list.Where(l => l.ID_ACTION.HasValue).Select(l => l.ID_ACTION.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTION);
			
				foreach (var loop in list)
				{
						loop.Consumer = ConsumerDict.TryGetValue(loop.ID_CONSUMER); 
		if (loop.ID_OPERATOR.HasValue)
						loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value); 
		loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR); 
		loop.ConsumerTransactionType = ConsumerTransactionTypeDict.TryGetValue(loop.ID_CONSUMER_TRANSACTION_TYPE); 
		if (loop.ID_TARIFF.HasValue)
						loop.Tariff = TariffDict.TryGetValue(loop.ID_TARIFF.Value); 
		if (loop.ID_CONSUMER_SETTLEMENT.HasValue)
						loop.ConsumerSettlement = ConsumerSettlementDict.TryGetValue(loop.ID_CONSUMER_SETTLEMENT.Value); 
		if (loop.ID_PAYMENT_MODULE.HasValue)
						loop.PaymentModule = PaymentModuleDict.TryGetValue(loop.ID_PAYMENT_MODULE.Value); 
		if (loop.ID_ACTION.HasValue)
						loop.Action = ActionDict.TryGetValue(loop.ID_ACTION.Value); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerTransaction> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerTransaction)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerTransaction).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerTransaction> Members
        public bool Equals(OpConsumerTransaction other)
        {
            if (other == null)
				return false;
			return this.IdConsumerTransaction.Equals(other.IdConsumerTransaction);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerTransaction)
				return this.IdConsumerTransaction == ((OpConsumerTransaction)obj).IdConsumerTransaction;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerTransaction left, OpConsumerTransaction right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerTransaction left, OpConsumerTransaction right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerTransaction.GetHashCode();
		}
    #endregion
    }
#endif
}