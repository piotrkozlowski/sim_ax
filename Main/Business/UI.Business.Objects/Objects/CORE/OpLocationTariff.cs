using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLocationTariff : DB_LOCATION_TARIFF, IComparable, IEquatable<OpLocationTariff>
    {
    #region Properties
        public int IdLocationTariff { get { return this.ID_LOCATION_TARIFF; } set { this.ID_LOCATION_TARIFF = value; } }
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public int IdTariff { get { return this.ID_TARIFF; } set { this.ID_TARIFF = value; } }
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
    #endregion

    #region	Navigation Properties
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? 0 : (long)value.ID_LOCATION; } }
        private OpTariff _Tariff;
        public OpTariff Tariff { get { return this._Tariff; } set { this._Tariff = value; this.ID_TARIFF = (value == null) ? 0 : (int)value.ID_TARIFF; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpLocationTariff()
            : base() { }

        public OpLocationTariff(DB_LOCATION_TARIFF clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationTariff> ConvertList(DB_LOCATION_TARIFF[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLocationTariff> ConvertList(DB_LOCATION_TARIFF[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpLocationTariff> ret = new List<OpLocationTariff>(list.Length);
            foreach (var loop in list)
            {
                OpLocationTariff insert = new OpLocationTariff(loop);

                if (loadNavigationProperties)
                {
                    insert.Location = dataProvider.GetLocation(loop.ID_LOCATION);
                    insert.Tariff = dataProvider.GetTariff(loop.ID_TARIFF);

                    if (insert.Location != null && insert.EndTime == null)
                        insert.Location.CurrentTariff = insert.Tariff;
                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationTariff> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationTariff)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationTariff).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpLocationTariff> Members
        public bool Equals(OpLocationTariff other)
        {
            if (other == null)
                return false;
            return this.IdLocationTariff.Equals(other.IdLocationTariff);
        }
    #endregion
    }
#endif
}