﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDevicePatternData : DB_DEVICE_PATTERN_DATA, IComparable, IEquatable<OpDevicePatternData>, IOpChangeState
    {
    #region Properties
        [XmlIgnore]
        public long IdPattern { get { return this.ID_PATTERN; } set { this.ID_PATTERN = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region	Navigation Properties
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
        public Enums.DataStatus DataStatus { get; set; }
    #endregion

    #region	Ctor
        public OpDevicePatternData()
            : base() { this.OpState = OpChangeState.New; }

        public OpDevicePatternData(DB_DEVICE_PATTERN_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DevicePatternData [" + IdPattern.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDevicePatternData> ConvertList(DB_DEVICE_PATTERN_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDevicePatternData> ConvertList(DB_DEVICE_PATTERN_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDevicePatternData> ret = new List<OpDevicePatternData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDevicePatternData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDevicePatternData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDevicePatternData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDevicePatternData clone = new OpDevicePatternData();
            clone.IdPattern = this.IdPattern;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.DataStatus = this.DataStatus;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEVICE_PATTERN_DATA[] {clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDevicePatternData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDevicePatternData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDevicePatternConfig> Members
        public bool Equals(OpDevicePatternData other)
        {
            if (other == null)
                return false;
            return this.IdPattern.Equals(other.IdPattern) && this.IdDataType.Equals(other.IdDataType) && this.Index.Equals(other.Index);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            OpDevicePatternData item = obj as OpDevicePatternData;
            if (item != null)
                return this.IdPattern == item.IdPattern && this.IdDataType == item.IdDataType && this.Index == item.Index;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDevicePatternData left, OpDevicePatternData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDevicePatternData left, OpDevicePatternData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPattern.GetHashCode();
        }
    #endregion
    }
#endif
}