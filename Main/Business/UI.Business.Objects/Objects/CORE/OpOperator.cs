using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Security.Principal;
using IMR.Suite.Common;
//using IMR.Suite.UI.Business.Components.CORE;
using System.Data;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public partial class OpOperator : DB_OPERATOR, IComparable, IReferenceType, IOpChangeState, IOpDynamic, IOpObject<OpOperatorData>
    {
    #region Properties
        [XmlIgnore]
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        [XmlIgnore]
        public int IdActor { get { return this.ID_ACTOR; } set { this.ID_ACTOR = value; } }
        [XmlIgnore]
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        [XmlIgnore]
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
        public string Login { get { return this.LOGIN; } set { this.LOGIN = value; } }
        [XmlIgnore]
        public string Password { get { return this.PASSWORD; } set { this.PASSWORD = value; } }
        [XmlIgnore]
        public bool IsBlocked { get { return this.IS_BLOCKED; } set { this.IS_BLOCKED = value; } }
        [XmlIgnore]
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }

        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region Navigation Properties

        [DataMember]
        private OpActor _Actor;        
        public OpActor Actor { get { return this._Actor; } set { this._Actor = value; this.ID_ACTOR = (value == null) ? 0 : (int)value.ID_ACTOR; } }
         
        [DataMember]
        private OpDistributor _Distributor;      
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public string NewPassword
        {
            get
            {
                return Password;
            }
            set
            {
                if (value != null)
                {
                    Password = DataConverter.EncodePassword(value);
                    _CryptedPassword = RSA.Encrypt(value);
                    _NewPasswordSet = true;
                }
            }
        }

        [XmlIgnore]
        private string _CryptedPassword = null;
        [XmlIgnore]
        public string CryptedPassword { get { return _CryptedPassword; } }

        [DataMember]
        private bool _NewPasswordSet = false;
        [XmlIgnore]
        public bool NewPasswordSet
        {
            get { return _NewPasswordSet; }
            set { _NewPasswordSet = value; }
        }

        [XmlIgnore]
        public string Initials
        {
            get
            {
                if (!String.IsNullOrEmpty(Actor.Name) && !String.IsNullOrEmpty(Actor.Surname))
                    return String.Format("{0}{1}", Actor.Name.Substring(0, 1), Actor.Surname.Substring(0, 1)).ToUpper();
                else
                    return Login;
            }
        }

        [XmlIgnore]
        public int? GuiColorInt
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.OPERATOR_IMRSC_COLOR);
            }
            set
            {
                DataList.SetValue(DataType.OPERATOR_IMRSC_COLOR, value);
            }
        }
        [XmlIgnore]
        public Color GuiColor
        {
            get
            {
                if (GuiColorInt.HasValue)
                    return Color.FromArgb(GuiColorInt.Value);
                else
                    return Color.Silver;
            }
            set
            {
                GuiColorInt = value.ToArgb();
            }
        }
        [XmlIgnore]
        public long? IdDepositoryLocation
        {
            get
            {
                return DataList.TryGetNullableValue<long>(DataType.DEPOSITORY_ID);
            }
            set
            {
                DataList.SetValue(DataType.DEPOSITORY_ID, value);
            }
        }

        [DataMember]
        private OpLocation _DepositoryLocation;
        [XmlIgnore]
        public OpLocation DepositoryLocation
        {
            get { return _DepositoryLocation; }
            set
            {
                _DepositoryLocation = value;
                if (value != null)
                    IdDepositoryLocation = value.IdLocation;
                else
                    IdDepositoryLocation = null;
            }
        }
        [XmlIgnore]
        public string Suffix
        {   
            get
            {
                return DataList.TryGetValue<string>(DataType.OPERATOR_ROUTE_SUFFIX);
            }
            set
            {
                if (String.IsNullOrWhiteSpace(value))
                    DataList.SetValue(DataType.OPERATOR_ROUTE_SUFFIX, null);
                else
                    DataList.SetValue(DataType.OPERATOR_ROUTE_SUFFIX, value);
            }
        }
        [XmlIgnore]
        public LogLevel MinLogLevel
        {
            get
            {
                int? value = DataList.TryGetNullableValue<int>(DataType.OPERATOR_MIN_LOG_LEVEL);
                return value == null ? LogLevel.Error : (LogLevel)value;
            }
            set
            {
                DataList.SetValue(DataType.OPERATOR_MIN_LOG_LEVEL, (int)value);
            }
        }

        /// <summary>
        /// Wszystkie powiadomienia email ustawione przez operatora dotycz�ce Task,Issue,WorkOrder itp.
        /// </summary>
        [XmlIgnore, DataMember]
        public EmailNotificationOptions EmailNotificationOptions;
        /// <summary>
        /// Wszystkie powiadomienia sms ustawione przez operatora dotycz�ce Task,Issue,WorkOrder itp.
        /// </summary>
        [XmlIgnore, DataMember]
        public SmsNotificationOptions SmsNotificationOptions;


        /// <summary>
        /// Wszystkie uprawnienia posiadane przez operatora (w ramach wszystkich modu��w)
        /// </summary>
        [XmlIgnore, DataMember]
        public List<OpActivity> AllActivities;
        /// <summary>
        /// Wszystkie uprawnienia posiadane przez operatora (w ramach wybranego modu�u)
        /// </summary>
        [XmlIgnore, DataMember]
        public List<OpActivity> Activities;
        [XmlIgnore, DataMember]
        public List<OpRole> Roles;
        [XmlIgnore]
        public OpRole DefaultRole { get { return (Roles != null && Roles.Count > 0) ? Roles[0] : null; } }

        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        [XmlIgnore]
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }

        [XmlIgnore, DataMember]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;
        #endregion

        #region Ctor
        public OpOperator()
            : base()
        {
            DataList = new OpDataList<OpOperatorData>();
            Activities = new List<OpActivity>();
            Roles = new List<OpRole>();
            Actor = new OpActor();
            EmailNotificationOptions = new EmailNotificationOptions(this);
            SmsNotificationOptions = new SmsNotificationOptions(this);
            this.OpState = OpChangeState.New;
        }

        public OpOperator(DB_OPERATOR clone)
            : base(clone)
        {
            DataList = new OpDataList<OpOperatorData>();
            Activities = new List<OpActivity>();
            Roles = new List<OpRole>();
            EmailNotificationOptions = new EmailNotificationOptions(this);
            SmsNotificationOptions = new SmsNotificationOptions(this);
            this.OpState = OpChangeState.Loaded;
        }

        public OpOperator(OpOperator clone)
            : base(clone)
        {
            DataList = new OpDataList<OpOperatorData>();
            if (clone.DataList != null)
            {
                foreach (OpOperatorData tdItem in clone.DataList)
                {
                    this.DataList.Add(new OpOperatorData(tdItem));
                }
            }
            Activities = clone.Activities;
            AllActivities = clone.AllActivities;
            Roles = clone.Roles;
            EmailNotificationOptions = clone.EmailNotificationOptions;
            SmsNotificationOptions = clone.SmsNotificationOptions;
            if(clone.Actor != null)
                Actor = clone.Actor;
            if(clone.Distributor != null)
                Distributor = clone.Distributor;
            this.OpState = clone.OpState;
        }
    #endregion

    #region Clone
        public OpOperator Clone(IDataProvider dataProvider)
        {
            OpOperator newOperator = new OpOperator(this);
            //properties which have to be reset
            newOperator.IdOperator = 0;
            newOperator.Actor = this.Actor;
            newOperator.Actor.IdActor = 0;
            newOperator.Distributor = this.Distributor;

            foreach (var item in DataList)
            {
                item.IdData = 0;
            }

            //this line removes actor from an operator. Commented out by af 14-02-2013:
            //newOperator = ConvertList(new DB_OPERATOR[1] { newOperator }, dataProvider)[0];

            return newOperator;
        }
    #endregion

    #region ToString
        public override string ToString()
        {
            if (this.Actor != null)
            {
                if (!String.IsNullOrEmpty(this.Actor.Surname))
                    return String.Format("{0} {1}", this.Actor.Surname, this.Actor.Name);
                else if (!String.IsNullOrEmpty(this.Actor.Name))
                    return this.Actor.Name;
            }
            return this.Login;
        }

    #endregion

    #region ConvertList
        public static List<OpOperator> ConvertList(DB_OPERATOR[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpOperator> ret = new List<OpOperator>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpOperator(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout:commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpOperator> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpActor> ActorDict = dataProvider.GetActor(list.Select(l => l.ID_ACTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTOR);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);

                foreach (var loop in list)
                {
                    loop.Actor = ActorDict.TryGetValue(loop.ID_ACTOR);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);

                    //po co dla wszystkich operatorow zawsze ciagnac dla nich role???
                    //loop.Roles = new List<OpRole>();
                    //foreach (var loop2 in dataProvider.GetOperatorRole(loop.ID_OPERATOR).FindAll(n => n.IdOperator == loop.IdOperator))
                    //    loop.Roles.Add(dataProvider.GetRole(loop2.IdRole));
                }
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpOperator> list, IDataProvider dataProvider, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> idOperator = list.Select(l => l.IdOperator).ToList();
                List<OpOperatorData> data = new List<OpOperatorData>();
                int[] idDistributor = list.Select(d => d.IdDistributor).Distinct().ToArray();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetOperatorDataFilter(IdOperator: idOperator.ToArray(), IdDistributor: idDistributor, IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.IOperatorDataTypes != null && dataProvider.IOperatorDataTypes.Count > 0)
                    data = dataProvider.GetOperatorDataFilter(IdOperator: idOperator.ToArray(), IdDistributor: idDistributor, IdDataType: dataProvider.IOperatorDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpOperator> operatorDict = list.ToDictionary<OpOperator, int>(l => l.IdOperator);
                    foreach (var dataValue in data)
                    {
                        operatorDict[dataValue.IdOperator].DataList.Add(dataValue);
                    }

                    Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.IdDepositoryLocation.HasValue).Select(l => l.IdDepositoryLocation.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);

                    foreach (var loop in list)
                    {
                        loop.EmailNotificationOptions = new EmailNotificationOptions(loop);
                        loop.SmsNotificationOptions = new SmsNotificationOptions(loop);

                        if (loop.IdDepositoryLocation.HasValue && loop.IdDepositoryLocation > 0)
                            loop.DepositoryLocation = LocationDict.TryGetValue(loop.IdDepositoryLocation.Value);
                    }
                }
            }
        }
    #endregion

    #region HasPermission
        public bool HasPermission(int idActivity, object referenceValue = null, bool defaultIfNoPermissionDefined = false)
        {
            //return RoleComponent.HasPermission(this, idActivity, referenceValue);

            // Metoda zosta�a przeniesiona z RoleComponent, poniewa� z poziomu UI.Business.Object nie mieliby�my do niej dost�pu.

            if (this.Activities != null)
            {
                List<OpActivity> activites = this.Activities.Where(w => w.IdActivity == idActivity).ToList();
                if (referenceValue == null) //szukamy czy operator ma zdefiniowane idActivity i nie jest ustawione Deny na True
                {
                    if (activites.Count == 0)
                    {
                        // nie ma takich uprawnien
                        return defaultIfNoPermissionDefined;
                    }
                    return activites.Any(a => !a.Deny) && !activites.Any(a => a.Deny);
                }
                else //podano ID obiektu do ktorego mamy sprawdzi� uprawnienie
                {
                    if (activites.Count == 0)
                    {
                        // nie ma takich uprawnien
                        return defaultIfNoPermissionDefined;
                    }
                    else if (activites.Any(w => OpDataUtil.ValueEquals(w.ReferenceValue, referenceValue) && w.Deny == true))
                    {
                        //zdefiniowane uprawnienie z tym referenceValue z Deny = True
                        return false;
                    }
                    else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
                    {
                        //wszystkie elementy sa wykluczone
                        return false;
                    }
                    else if (activites.Any(w => w.ReferenceValue == null && w.Deny == false))
                    {
                        //wszystkie elementy dozwolone
                        return true;
                    }
                    else if (activites.Any(w => OpDataUtil.ValueEquals(w.ReferenceValue, referenceValue) && w.Deny == false))
                    {
                        //zdefiniowane uprawnienie z tym referenceValue z Deny = False
                        return true;
                    }
                    else if (defaultIfNoPermissionDefined && !activites.Any(w => OpDataUtil.ValueEquals(w.ReferenceValue, referenceValue) && w.Deny == false))
                    {
                        // nie ma zdefiniowanego uprawnienia z tym referenceValue z Deny = False
                        return false;
                    }
                }
            }
            return defaultIfNoPermissionDefined; //jesli nie ma zdefiniowanych uprawnie�
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpOperator)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpOperator).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IReferenceType Members
        public object GetReferenceKey()
        {
            return this.IdOperator;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion

    #region Implementation of IOpDynamic
    public object Key
    {
        get { return IdOperator; }
    }

    [XmlIgnore]
    public OpDynamicPropertyDict DynamicProperties
    {
        get { return this.dynamicProperties; }
        set { this.dynamicProperties = value; }
    }

    [XmlIgnore]
    public Dictionary<string, object> DynamicValues
    {
        get { return dynamicValues; }
        set { dynamicValues = value; }
    }

    public object Owner
    {
        get { return this; }
    }
        #endregion
    #region Implementation of IOpObject
    [XmlIgnore]
    public object IdObject
    {
        get { return IdOperator; }
        set { IdOperator = Convert.ToInt32(value); }
    }

    [DataMember]
    public OpDataList<OpOperatorData> DataList { get; set; }
    #endregion

    #region override Equals
    public override bool Equals(object obj)
    {
        if (obj is OpOperator)
            return this.IdOperator == ((OpOperator)obj).IdOperator;
        else
            return base.Equals(obj);
    }
    public static bool operator ==(OpOperator left, OpOperator right)
    {
        if ((object)left == null)
            return ((object)right == null);
        return left.Equals(right);
    }
    public static bool operator !=(OpOperator left, OpOperator right)
    {
        if ((object)left == null)
            return ((object)right != null);
        return !left.Equals(right);
    }

    public override int GetHashCode()
    {
        return IdOperator.GetHashCode();
    }
    #endregion

    #region LoadNotificationOptions

    public static void LoadNotificationOptions(OpOperator oper)
    {
        if (oper != null)
        {
            oper.EmailNotificationOptions = new EmailNotificationOptions(oper);
            oper.SmsNotificationOptions = new SmsNotificationOptions(oper);
        }
    }

    public static void LoadNotificationOptions(List<OpOperator> operatorList)
    {
        if (operatorList != null && operatorList.Count > 0)
        {
            foreach (OpOperator oper in operatorList)
            {
                oper.EmailNotificationOptions = new EmailNotificationOptions(oper);
                oper.SmsNotificationOptions = new SmsNotificationOptions(oper);
            }
        }
    }

    #endregion

    }
#endif
}