using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpRoutePointData : DB_ROUTE_POINT_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpRoutePointData>
	{
    #region Properties
		public long IdRoutePointData { get { return this.ID_ROUTE_POINT_DATA; } set { this.ID_ROUTE_POINT_DATA = value; } }
		public int IdRoutePoint { get { return this.ID_ROUTE_POINT; } set { this.ID_ROUTE_POINT = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpRoutePoint _RoutePoint;
		public OpRoutePoint RoutePoint { get { return this._RoutePoint; } set { this._RoutePoint = value; this.ID_ROUTE_POINT = (value == null) ? 0 : (int)value.ID_ROUTE_POINT; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdRoutePointData; } set { IdRoutePointData = value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpRoutePointData()
			: base() { this.OpState = OpChangeState.New; }

		public OpRoutePointData(DB_ROUTE_POINT_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "RoutePointData [" + IdRoutePointData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpRoutePointData> ConvertList(DB_ROUTE_POINT_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpRoutePointData> ConvertList(DB_ROUTE_POINT_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpRoutePointData> ret = new List<OpRoutePointData>(list.Length);
			foreach (var loop in list)
			{
				OpRoutePointData insert = new OpRoutePointData(loop);
				if (loadNavigationProperties)
				{
                    //insert.RoutePoint = dataProvider.GetRoutePoint(loop.ID_ROUTE_POINT);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRoutePointData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpRoutePointData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpRoutePointData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IEquatable<OpRoutePointData> Members
		public bool Equals(OpRoutePointData other)
		{
			if (other == null)
				return false;
			return this.IdRoutePointData.Equals(other.IdRoutePointData);
		}
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public object Clone(IDataProvider dataProvider)
		{
			OpRoutePointData clone = new OpRoutePointData();
			clone.IdData = this.IdData;
			clone.IdRoutePoint = this.IdRoutePoint;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_ROUTE_POINT_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.RoutePoint = dataProvider.GetRoutePoint((int)this.ID_ROUTE_POINT_DATA);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpRoutePointData)
				return this.IdData == ((OpRoutePointData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpRoutePointData left, OpRoutePointData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpRoutePointData left, OpRoutePointData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}