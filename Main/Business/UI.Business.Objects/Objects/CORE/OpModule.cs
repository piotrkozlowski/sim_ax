using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpModule : DB_MODULE, IComparable, IOpDynamic, IOpDynamicProperty<OpModule>, IReferenceType
    {
    #region Properties
        [XmlIgnore]
        public int IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        [XmlIgnore]
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
        public OpDataList<OpModuleData> DataList { get; set; }
        [XmlIgnore]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;
    #endregion

    #region	Ctor
        public OpModule()
            : base()
        {
            DataList = new OpDataList<OpModuleData>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpModule(DB_MODULE clone)
            : base(clone)
        {
            DataList = new OpDataList<OpModuleData>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (this.Descr != null && !String.IsNullOrWhiteSpace(this.Descr.Description))
                return this.Descr.Description;
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpModule> ConvertList(DB_MODULE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpModule> ConvertList(DB_MODULE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpModule> ret = new List<OpModule>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpModule(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpModule> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                    {
                        //loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                        loop.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);
                    }
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpModule> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public OpModule Clone(IDataProvider dataProvider)
        {
            OpModule clone = new OpModule();

            clone.IdModule = this.IdModule;
            clone.Name = this.Name;
            clone.IdDescr = this.IdDescr;

            clone.DataList = this.DataList.Clone(dataProvider);

            if (dataProvider != null)
                clone = ConvertList(new DB_MODULE[] { clone }, dataProvider)[0];

            return clone;
        }
    #endregion


    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpModule)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpModule).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        
        #endregion

        #region Enum
        //zmiany wprowadza� r�wnie� w DB
        public enum Enum
        {
            Unknown = 0,
            CORE_Controller = 1,
            CORE_Scheduler = 2,
            CORE_Actions = 3,
            CORE_Alarms = 4,
            DAQ_Controller = 5,
            DAQ_Router = 6,
            DAQ_Transmission = 7,
            DW_Controller = 8,
            DW_Scheduler = 9,
            DW_Estimator = 10,
            DW_ETL = 11,
            Operator = 12,
            Basic = 13,
            WebIMR = 14,
            DataService = 15,
            RDPConnection = 16,
            SDServicePostVerifier = 17,
            TMQ_ShippingTest = 18,
            SRTPostVerifier = 19,
            SIMA = 20,
            SITA = 21,
            DB_StoredProcedure = 22,
            IMRServiceCentre = 23,
            IMRMyServiceCentre = 24,
            IMRServiceTerminal = 25
        }
    #endregion

    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdModule; }
        }
        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }
        [XmlIgnore]
        OpModule IOpDynamicProperty<OpModule>.Owner
        {
            get { return this; }
        }
        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }
        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
        #endregion

        #region Implementation of IReferenceType

        public object GetReferenceKey()
        {
            return this.IdModule;
        }

        public object GetReferenceValue()
        {
            return this;
        }

        #endregion

    }
#endif
}