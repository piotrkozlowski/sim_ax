using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTypeProfileMap : DB_DEVICE_TYPE_PROFILE_MAP, IComparable, IEquatable<OpDeviceTypeProfileMap>
    {
    #region Properties
        public int IdDeviceTypeProfileMap { get { return this.ID_DEVICE_TYPE_PROFILE_MAP; } set { this.ID_DEVICE_TYPE_PROFILE_MAP = value; } }
        public int IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = value; } }
        public int IdOperatorCreated { get { return this.ID_OPERATOR_CREATED; } set { this.ID_OPERATOR_CREATED = value; } }
        public DateTime CreatedTime { get { return this.CREATED_TIME; } set { this.CREATED_TIME = value; } }
        public int? IdOperatorModified { get { return this.ID_OPERATOR_MODIFIED; } set { this.ID_OPERATOR_MODIFIED = value; } }
        public DateTime? ModifiedTime { get { return this.MODIFIED_TIME; } set { this.MODIFIED_TIME = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceType _DeviceType;
        public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.ID_DEVICE_TYPE = (value == null) ? 0 : (int)value.ID_DEVICE_TYPE; } }
        private OpOperator _OperatorCreated;
        public OpOperator OperatorCreated { get { return this._OperatorCreated; } set { this._OperatorCreated = value; this.ID_OPERATOR_CREATED = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorModified;
        public OpOperator OperatorModified { get { return this._OperatorModified; } set { this._OperatorModified = value; this.ID_OPERATOR_MODIFIED = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceTypeProfileMap()
            : base() { }

        public OpDeviceTypeProfileMap(DB_DEVICE_TYPE_PROFILE_MAP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeviceTypeProfileMap [" + IdDeviceTypeProfileMap.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceTypeProfileMap> ConvertList(DB_DEVICE_TYPE_PROFILE_MAP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceTypeProfileMap> ConvertList(DB_DEVICE_TYPE_PROFILE_MAP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceTypeProfileMap> ret = new List<OpDeviceTypeProfileMap>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceTypeProfileMap(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceTypeProfileMap> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Select(l => l.ID_DEVICE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_CREATED).Union(list.Where(l1 => l1.ID_OPERATOR_MODIFIED.HasValue).Select(l2 => l2.ID_OPERATOR_MODIFIED.Value)).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                foreach (var loop in list)
                {
                    loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE);
                    loop.OperatorCreated = OperatorDict.TryGetValue(loop.ID_OPERATOR_CREATED);
                    if (loop.ID_OPERATOR_MODIFIED.HasValue)
                        loop.OperatorModified = OperatorDict.TryGetValue(loop.ID_OPERATOR_MODIFIED.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceTypeProfileMap> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTypeProfileMap)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTypeProfileMap).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceTypeProfileMap> Members
        public bool Equals(OpDeviceTypeProfileMap other)
        {
            if (other == null)
                return false;
            return this.IdDeviceTypeProfileMap.Equals(other.IdDeviceTypeProfileMap);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceTypeProfileMap)
                return this.IdDeviceTypeProfileMap == ((OpDeviceTypeProfileMap)obj).IdDeviceTypeProfileMap;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceTypeProfileMap left, OpDeviceTypeProfileMap right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceTypeProfileMap left, OpDeviceTypeProfileMap right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceTypeProfileMap.GetHashCode();
        }
    #endregion
    }
#endif
}