﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.Objects.CORE;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPackageStatus : DB_PACKAGE_STATUS, IComparable, IEquatable<OpPackageStatus>
    {
    #region Properties
        public int IdPackageStatus { get { return this.ID_PACKAGE_STATUS; } set { this.ID_PACKAGE_STATUS = value; } }
        public string PackageStatus { get { return this.PACKAGE_STATUS; } set { this.PACKAGE_STATUS = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpPackageStatus()
            : base() { }

        public OpPackageStatus(DB_PACKAGE_STATUS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            return this.PACKAGE_STATUS;
        }
    #endregion

    #region	ConvertList
        public static List<OpPackageStatus> ConvertList(DB_PACKAGE_STATUS[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPackageStatus> ConvertList(DB_PACKAGE_STATUS[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpPackageStatus> ret = new List<OpPackageStatus>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPackageStatus(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPackageStatus> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPackageStatus> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPackageStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPackageStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPackageStatus> Members
        public bool Equals(OpPackageStatus other)
        {
            if (other == null)
                return false;
            return this.IdPackageStatus.Equals(other.IdPackageStatus);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPackageStatus)
                return this.IdPackageStatus == ((OpPackageStatus)obj).IdPackageStatus;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPackageStatus left, OpPackageStatus right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPackageStatus left, OpPackageStatus right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPackageStatus.GetHashCode();
        }
    #endregion
    }
#endif
}
