﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpPackageStatusHistory : DB_PACKAGE_STATUS_HISTORY, IComparable, IEquatable<OpPackageStatusHistory>
    {
    #region Properties
        public int IdPackageStatusHistory { get { return this.ID_PACKAGE_STATUS_HISTORY; } set { this.ID_PACKAGE_STATUS_HISTORY = value; } }
        public int IdPackage { get { return this.ID_PACKAGE; } set { this.ID_PACKAGE = value; } }
        public int IdPackageStatus { get { return this.ID_PACKAGE_STATUS; } set { this.ID_PACKAGE_STATUS = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpPackage _Package;
        public OpPackage Package { get { return this._Package; } set { this._Package = value; this.ID_PACKAGE = (value == null) ? 0 : (int)value.ID_PACKAGE; } }
        private OpPackageStatus _PackageStatus;
        public OpPackageStatus PackageStatus { get { return this._PackageStatus; } set { this._PackageStatus = value; this.ID_PACKAGE_STATUS = (value == null) ? 0 : (int)value.ID_PACKAGE_STATUS; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpPackageStatusHistory()
            : base() { }

        public OpPackageStatusHistory(DB_PACKAGE_STATUS_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "PackageStatusHistory [" + IdPackageStatusHistory.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpPackageStatusHistory> ConvertList(DB_PACKAGE_STATUS_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpPackageStatusHistory> ConvertList(DB_PACKAGE_STATUS_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpPackageStatusHistory> ret = new List<OpPackageStatusHistory>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpPackageStatusHistory(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpPackageStatusHistory> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpPackage> PackageDict = dataProvider.GetPackage(list.Select(l => l.ID_PACKAGE).Distinct().ToArray()).ToDictionary(l => l.ID_PACKAGE);
                Dictionary<int, OpPackageStatus> PackageStatusDict = dataProvider.GetPackageStatus(list.Select(l => l.ID_PACKAGE_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_PACKAGE_STATUS);

                foreach (var loop in list)
                {
                    loop.Package = PackageDict.TryGetValue(loop.ID_PACKAGE);
                    loop.PackageStatus = PackageStatusDict.TryGetValue(loop.ID_PACKAGE_STATUS);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpPackageStatusHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPackageStatusHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPackageStatusHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpPackageStatusHistory> Members
        public bool Equals(OpPackageStatusHistory other)
        {
            if (other == null)
                return false;
            return this.IdPackageStatusHistory.Equals(other.IdPackageStatusHistory);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpPackageStatusHistory)
                return this.IdPackageStatusHistory == ((OpPackageStatusHistory)obj).IdPackageStatusHistory;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpPackageStatusHistory left, OpPackageStatusHistory right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpPackageStatusHistory left, OpPackageStatusHistory right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdPackageStatusHistory.GetHashCode();
        }
    #endregion
    }
#endif
}