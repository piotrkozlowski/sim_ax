﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceListDeviceData : DB_SERVICE_LIST_DEVICE_DATA, IComparable, IEquatable<OpServiceListDeviceData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdServiceListDeviceData { get { return this.ID_SERVICE_LIST_DEVICE_DATA; } set { this.ID_SERVICE_LIST_DEVICE_DATA = value; } }
        public int? IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? null : (int?)value.ID_SERVICE_LIST; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpServiceListDeviceData()
            : base() { }

        public OpServiceListDeviceData(DB_SERVICE_LIST_DEVICE_DATA clone)
            : base(clone) { }

        public OpServiceListDeviceData(int? idServiceListDeviceData, int idServiceList, long serialNbr, long idDataType, int indexNbr, object value)
        {
            IdServiceListDeviceData = idServiceListDeviceData.GetValueOrDefault();
            IdServiceList = idServiceList;
            SerialNbr = serialNbr;
            IdDataType = idDataType;
            IndexNbr = indexNbr;
            Value = value;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceListDeviceData [" + IdServiceListDeviceData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceListDeviceData> ConvertList(DB_SERVICE_LIST_DEVICE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceListDeviceData> ConvertList(DB_SERVICE_LIST_DEVICE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceListDeviceData> ret = new List<OpServiceListDeviceData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceListDeviceData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceListDeviceData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Where(l => l.ID_SERVICE_LIST.HasValue).Select(l => l.ID_SERVICE_LIST.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    if (loop.ID_SERVICE_LIST.HasValue)
                        loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST.Value);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceListDeviceData> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceListDeviceData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceListDeviceData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceListDeviceData> Members
        public bool Equals(OpServiceListDeviceData other)
        {
            if (other == null)
                return false;
            return this.IdServiceListDeviceData.Equals(other.IdServiceListDeviceData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceListDeviceData)
                return this.IdServiceListDeviceData == ((OpServiceListDeviceData)obj).IdServiceListDeviceData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceListDeviceData left, OpServiceListDeviceData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceListDeviceData left, OpServiceListDeviceData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceListDeviceData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdServiceListDeviceData; } set { IdServiceListDeviceData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

        public OpChangeState OpState { get; set; }
    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpServiceListDeviceData clone = new OpServiceListDeviceData();
            clone.IdData = this.IdData;
            clone.IdServiceList = this.IdServiceList;
            clone.SerialNbr = this.SerialNbr;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_SERVICE_LIST_DEVICE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }


    #endregion
    }
#endif
}