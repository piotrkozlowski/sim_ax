﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpShippingList : DB_SHIPPING_LIST, IComparable, IEquatable<OpShippingList>
    {
        #region Properties
        public int IdShippingList { get { return this.ID_SHIPPING_LIST; } set { this.ID_SHIPPING_LIST = value; } }
        public string ContractNbr { get { return this.CONTRACT_NBR; } set { this.CONTRACT_NBR = value; } }
        public string ShippingListNbr { get { return this.SHIPPING_LIST_NBR; } set { this.SHIPPING_LIST_NBR = value; } }
        public int IdOperatorCreator { get { return this.ID_OPERATOR_CREATOR; } set { this.ID_OPERATOR_CREATOR = value; } }
        public int IdOperatorContact { get { return this.ID_OPERATOR_CONTACT; } set { this.ID_OPERATOR_CONTACT = value; } }
        public int IdOperatorQuality { get { return this.ID_OPERATOR_QUALITY; } set { this.ID_OPERATOR_QUALITY = value; } }
        public int IdOperatorProtocol { get { return this.ID_OPERATOR_PROTOCOL; } set { this.ID_OPERATOR_PROTOCOL = value; } }
        public string ShippingLetterNbr { get { return this.SHIPPING_LETTER_NBR; } set { this.SHIPPING_LETTER_NBR = value; } }
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public int IdShippingListType { get { return this.ID_SHIPPING_LIST_TYPE; } set { this.ID_SHIPPING_LIST_TYPE = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE.ToLocalTime(); } set { this.CREATION_DATE = value.ToUniversalTime(); } }

        public DateTime? FinishDate { get { return (this.FINISH_DATE == null) ? (null) : (DateTime?)this.FINISH_DATE.Value.ToLocalTime(); } set { this.FINISH_DATE = (value == null) ? null : (DateTime?)value.Value.ToUniversalTime(); } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        #endregion

        #region	Navigation Properties
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? 0 : (long)value.ID_LOCATION; } }
        private OpShippingListType _ShippingListType;
        public OpShippingListType ShippingListType { get { return this._ShippingListType; } set { this._ShippingListType = value; this.ID_SHIPPING_LIST_TYPE = (value == null) ? 0 : (int)value.ID_SHIPPING_LIST_TYPE; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        private OpOperator _OperatorCreator;
        [CopyableProperty]
        public OpOperator OperatorCreator { get { return this._OperatorCreator; } set { this._OperatorCreator = value; this.IdOperatorCreator = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorContact;
        [CopyableProperty]
        public OpOperator OperatorContact { get { return this._OperatorContact; } set { this._OperatorContact = value; this.IdOperatorContact = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorQuality;
        [CopyableProperty]
        public OpOperator OperatorQuality { get { return this._OperatorQuality; } set { this._OperatorQuality = value; this.IdOperatorQuality = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorProtocol;
        [CopyableProperty]
        public OpOperator OperatorProtocol { get { return this._OperatorProtocol; } set { this._OperatorProtocol = value; this.IdOperatorProtocol = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        #endregion

        #region	Custom Properties
        public OpDataList<OpShippingListData> DataList { get; set; }

        public int DevicesAmount { get; set; }

        /// <summary>
        /// Gets or sets the purchase order value.
        /// If the data list or purchase order is not defined, property returns null value.
        /// </summary>
        /// <value>
        /// The purchase order.
        /// </value>
        public string PurchaseOrder
        {
            get
            {
                if (this.DataList != null)
                {
                    var po = this.DataList.FirstOrDefault(x => x.IdDataType == DataType.SHIPPING_LIST_PURCHASE_ORDER);
                    if (po != null)
                    {
                        return po.Value.ToString();
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }

            set
            {
                if (this.DataList != null)
                {
                    // get purchase order data type from data list.
                    var po = this.DataList.FirstOrDefault(x => x.IdDataType == DataType.SHIPPING_LIST_PURCHASE_ORDER);
                    if (po != null)
                    {
                        // set the value and state
                        po.Value = value;
                        po.OpState = OpChangeState.Modified;
                    }
                    else
                    {
                        // if purchase order not exists create a new one and push it to data list.
                        po = new OpShippingListData();
                        po.IdShippingList = this.IdShippingList;
                        po.IdDataType = DataType.SHIPPING_LIST_PURCHASE_ORDER;
                        
                        // set default index
                        po.IndexNbr = 0;
                        po.Value = value;
                        po.ShippingList = this;
                        po.OpState = Business.Objects.OpChangeState.New;
                        this.DataList.Add(po);
                    }
                }
            }
        }

        #endregion

        #region	Ctor
        public OpShippingList()
            : base()
        {
            DataList = new OpDataList<OpShippingListData>();
        }

        public OpShippingList(DB_SHIPPING_LIST clone)
            : base(clone)
        {
            DataList = new OpDataList<OpShippingListData>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.CONTRACT_NBR;
        }
        #endregion

        #region	ConvertList
        public static List<OpShippingList> ConvertList(DB_SHIPPING_LIST[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpShippingList> ConvertList(DB_SHIPPING_LIST[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpShippingList> ret = new List<OpShippingList>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpShippingList(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpShippingList> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Select(l => l.ID_LOCATION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpShippingListType> ShippingListTypeDict = dataProvider.GetShippingListType(list.Select(l => l.ID_SHIPPING_LIST_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SHIPPING_LIST_TYPE);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpOperator> OperatorCreatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_CREATOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorContactDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_CONTACT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorQualityDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_QUALITY).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorProtocolDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_PROTOCOL).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION);
                    loop.ShippingListType = ShippingListTypeDict.TryGetValue(loop.ID_SHIPPING_LIST_TYPE);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    loop.OperatorCreator = OperatorCreatorDict.TryGetValue(loop.ID_OPERATOR_CREATOR);
                    loop.OperatorContact = OperatorContactDict.TryGetValue(loop.ID_OPERATOR_CONTACT);
                    loop.OperatorQuality = OperatorQualityDict.TryGetValue(loop.ID_OPERATOR_QUALITY);
                    loop.OperatorProtocol = OperatorProtocolDict.TryGetValue(loop.ID_OPERATOR_PROTOCOL);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpShippingList> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IShippingListDataTypes != null)
            {
                List<int> idShippingList = list.Select(l => l.IdShippingList).ToList();

                if (dataProvider.IServiceListDataTypes.Count > 0)
                {
                    List<OpShippingListData> data = dataProvider.GetShippingListDataFilter(IdShippingList: idShippingList.ToArray(), IdDataType: dataProvider.IShippingListDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpShippingList> shippingListDataDict = list.ToDictionary<OpShippingList, int>(l => l.IdShippingList);
                        foreach (var dataValue in data)
                        {
                            shippingListDataDict[dataValue.IdShippingList].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpShippingList)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpShippingList).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpShippingList> Members
        public bool Equals(OpShippingList other)
        {
            if (other == null)
                return false;
            return this.IdShippingList.Equals(other.IdShippingList);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpShippingList)
                return this.IdShippingList == ((OpShippingList)obj).IdShippingList;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpShippingList left, OpShippingList right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpShippingList left, OpShippingList right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdShippingList.GetHashCode();
        }
        #endregion
    }
#endif
}
