﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpSimCardData : DB_SIM_CARD_DATA, IComparable, IEquatable<OpSimCardData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdSimCardData { get { return this.ID_SIM_CARD_DATA; } set { this.ID_SIM_CARD_DATA = value; } }
        public int IdSimCard { get { return this.ID_SIM_CARD; } set { this.ID_SIM_CARD = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpSimCard _SimCard;
        public OpSimCard SimCard { get { return this._SimCard; } set { this._SimCard = value; this.ID_SIM_CARD = (value == null) ? 0 : (int)value.ID_SIM_CARD; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpSimCardData()
            : base() { }

        public OpSimCardData(DB_SIM_CARD_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "SimCardData [" + IdSimCardData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpSimCardData> ConvertList(DB_SIM_CARD_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpSimCardData> ConvertList(DB_SIM_CARD_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpSimCardData> ret = new List<OpSimCardData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpSimCardData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpSimCardData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
              //  Dictionary<int, OpSimCard> SimCardDict = dataProvider.GetSimCard(list.Select(l => l.ID_SIM_CARD).Distinct().ToArray()).ToDictionary(l => l.ID_SIM_CARD);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                //    loop.SimCard = SimCardDict.TryGetValue(loop.ID_SIM_CARD);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpSimCardData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSimCardData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSimCardData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpSimCardData> Members
        public bool Equals(OpSimCardData other)
        {
            if (other == null)
                return false;
            return this.IdSimCardData.Equals(other.IdSimCardData);
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdSimCardData; } set { IdSimCardData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpSimCardData clone = new OpSimCardData();
            clone.IdData = this.IdData;
            clone.IdSimCard = this.IdSimCard;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_SIM_CARD_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpSimCardData)
                return this.IdSimCardData == ((OpSimCardData)obj).IdSimCardData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpSimCardData left, OpSimCardData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpSimCardData left, OpSimCardData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdSimCardData.GetHashCode();
        }
    #endregion
    }
#endif
}