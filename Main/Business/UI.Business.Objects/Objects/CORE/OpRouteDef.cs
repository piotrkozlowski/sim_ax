using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
     [Serializable]
    public class OpRouteDef : DB_ROUTE_DEF, IComparable, IOpChangeState, IReferenceType
    {
    #region Properties
        public int IdRouteDef { get { return this.ID_ROUTE_DEF; } set { this.ID_ROUTE_DEF = OpDataUtil.SetNewValue(this.ID_ROUTE_DEF, value, this); } }
        public string Name { get { return this.NAME; } set { this.NAME = OpDataUtil.SetNewValue(this.NAME, value, this); } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = OpDataUtil.SetNewValue(this.DESCRIPTION, value, this); } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = OpDataUtil.SetNewValue(this.CREATION_DATE, value, this); } }
        public DateTime ExpirationDate { get { return this.EXPIRATION_DATE; } set { this.EXPIRATION_DATE = OpDataUtil.SetNewValue(this.EXPIRATION_DATE, value, this); } }
		public DateTime? DeleteDate 
        { 
            get { return this.DELETE_DATE;}
            set { this.DELETE_DATE = OpDataUtil.SetNewValue(this.DELETE_DATE, value.HasValue ? value : null, this); } 
        }
        public int IdRouteType { get { return this.ID_ROUTE_TYPE; } set { this.ID_ROUTE_TYPE = OpDataUtil.SetNewValue(this.ID_ROUTE_TYPE, value, this); } }
        public int IdRouteStatus { get { return this.ID_ROUTE_STATUS; } set { this.ID_ROUTE_STATUS = OpDataUtil.SetNewValue(this.ID_ROUTE_STATUS, value, this); } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = OpDataUtil.SetNewValue(this.ID_OPERATOR, value, this); } }
        public bool AdHoc { get { return this.AD_HOC; } set { this.AD_HOC = OpDataUtil.SetNewValue(this.AD_HOC, value, this); } }
    #endregion

    #region	Navigation Properties
		private OpRouteType _RouteType;
        public OpRouteType RouteType { get { return this._RouteType; } set { this._RouteType = value; this.IdRouteType = (value == null) ? 0 : (int)value.IdRouteType; } }
		private OpRouteStatus _RouteStatus;
        public OpRouteStatus RouteStatus { get { return this._RouteStatus; } set { this._RouteStatus = value; this.IdRouteStatus = (value == null) ? 0 : (int)value.IdRouteStatus; } }
		private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.IdOperator = (value == null) ? 0 : (int)value.IdOperator; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpRouteDefData> DataList { get; set; }

        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpRouteDef()
			:base()
        {
            this.DataList = new OpDataList<OpRouteDefData>();
            OpState = OpChangeState.New;
        }
		
		public OpRouteDef(DB_ROUTE_DEF clone)
			:base(clone)
        {
            this.DataList = new OpDataList<OpRouteDefData>();
            OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpRouteDef> ConvertList(DB_ROUTE_DEF[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpRouteDef> ConvertList(DB_ROUTE_DEF[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpRouteDef> ret = new List<OpRouteDef>(list.Length);
            Dictionary<int, OpRouteType> routeTypeDict = new Dictionary<int,OpRouteType>();
            Dictionary<int, OpRouteStatus> routeStatusDict = new Dictionary<int,OpRouteStatus>();
            Dictionary<int, OpOperator> operatorDict = new Dictionary<int,OpOperator>();
            if (loadNavigationProperties && list.Length > 0)
            {
                routeTypeDict = dataProvider.GetRouteType(list.Select(q => q.ID_ROUTE_TYPE).Distinct().ToArray()).ToDictionary(q => q.ID_ROUTE_TYPE);
                routeStatusDict = dataProvider.GetRouteStatus(list.Select(q => q.ID_ROUTE_STATUS).Distinct().ToArray()).ToDictionary(q => q.ID_ROUTE_STATUS);
                operatorDict = dataProvider.GetOperator(list.Select(q => q.ID_OPERATOR).Distinct().ToArray()).ToDictionary(q => q.ID_OPERATOR);
            }
			foreach (var loop in list)
			{
			    OpRouteDef insert = new OpRouteDef(loop);
				if(loadNavigationProperties)
				{
                    insert.RouteType = routeTypeDict.TryGetValue(loop.ID_ROUTE_TYPE);
                    insert.RouteStatus = routeStatusDict.TryGetValue(loop.ID_ROUTE_STATUS);
                    insert.Operator = operatorDict.TryGetValue(loop.ID_OPERATOR); 		
				}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRouteDef> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteDef)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteDef).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IReferenceType

        public object GetReferenceKey()
        {
            return IdRouteDef;
        }

        public object GetReferenceValue()
        {
            return this;
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRouteDef)
                return this.IdRouteDef == ((OpRouteDef)obj).IdRouteDef;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRouteDef left, OpRouteDef right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRouteDef left, OpRouteDef right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRouteDef.GetHashCode();
        }
    #endregion
    }
#endif
}