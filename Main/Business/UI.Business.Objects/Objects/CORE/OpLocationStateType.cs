using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLocationStateType : DB_LOCATION_STATE_TYPE, IComparable, IReferenceType, IEquatable<OpLocationStateType>
    {
    #region Properties
        [XmlIgnore]
        public int IdLocationStateType { get { return this.ID_LOCATION_STATE_TYPE; } set { this.ID_LOCATION_STATE_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        [XmlIgnore]
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }
    #endregion

    #region	Ctor
        public OpLocationStateType()
            : base() { }

        public OpLocationStateType(DB_LOCATION_STATE_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpLocationStateType> ConvertList(DB_LOCATION_STATE_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpLocationStateType> ConvertList(DB_LOCATION_STATE_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpLocationStateType> ret = new List<OpLocationStateType>(list.Length);
            foreach (var loop in list)
            {
                OpLocationStateType insert = new OpLocationStateType(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[]{ loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();


                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpLocationStateType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLocationStateType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLocationStateType).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Enum
        [Serializable]
        public enum Enum
        {
            NEW = 1,
            SCHEDULED = 2,
            INTERVENTION = 3,
            PENDING = 4,
            OPERATIONAL = 5,
            SUSPENDED = 6,
            UNUSED = 7,
            DELETED = 8,
            APPROVED = 9
        }
    #endregion

    #region IReferenceType
        public object GetReferenceKey()
        {
            return this.IdLocationStateType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region IEquatable<OpLocation> Members
    public bool Equals(OpLocationStateType other)
    {
        if (other != null)
            return this.IdLocationStateType.Equals(other.IdLocationStateType);
        else
            return false;
    }
    #endregion

    #region override Equals
    public override bool Equals(object obj)
    {
        if (obj is OpLocationStateType)
            return this.IdLocationStateType == ((OpLocationStateType)obj).IdLocationStateType;
        else
            return base.Equals(obj);
    }
    public static bool operator ==(OpLocationStateType left, OpLocationStateType right)
    {
        if ((object)left == null)
            return ((object)right == null);
        return left.Equals(right);
    }
    public static bool operator !=(OpLocationStateType left, OpLocationStateType right)
    {
        if ((object)left == null)
            return ((object)right != null);
        return !left.Equals(right);
    }

    public override int GetHashCode()
    {
        return IdLocationStateType.GetHashCode();
    }
    #endregion
    }
#endif
}