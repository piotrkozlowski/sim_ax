using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpCommandCodeParamMap : DB_COMMAND_CODE_PARAM_MAP, IComparable, IEquatable<OpCommandCodeParamMap>
    {
        #region Properties
        public long IdCommandCodeParamMap { get { return this.ID_COMMAND_CODE_PARAM_MAP; } set { this.ID_COMMAND_CODE_PARAM_MAP = value; } }
        public long IdCommandCode { get { return this.ID_COMMAND_CODE; } set { this.ID_COMMAND_CODE = value; } }
        public long ArgNbr { get { return this.ARG_NBR; } set { this.ARG_NBR = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int ByteOffset { get { return this.BYTE_OFFSET; } set { this.BYTE_OFFSET = value; } }
        public int BitOffset { get { return this.BIT_OFFSET; } set { this.BIT_OFFSET = value; } }
        public int Length { get { return this.LENGTH; } set { this.LENGTH = value; } }
        public int IdDataTypeClass { get { return this.ID_DATA_TYPE_CLASS; } set { this.ID_DATA_TYPE_CLASS = value; } }
        public int IdUnit { get { return this.ID_UNIT; } set { this.ID_UNIT = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public int? IdProtocol { get { return this.ID_PROTOCOL; } set { this.ID_PROTOCOL = value; } }
        public bool IsRequired { get { return this.IS_REQUIRED; } set { this.IS_REQUIRED = value; } }
        #endregion

        #region	Navigation Properties
        private OpDataTypeClass _DataTypeClass;
        public OpDataTypeClass DataTypeClass { get { return this._DataTypeClass; } set { this._DataTypeClass = value; this.ID_DATA_TYPE_CLASS = (value == null) ? 0 : (int)value.ID_DATA_TYPE_CLASS; } }
        private OpUnit _Unit;
        public OpUnit Unit { get { return this._Unit; } set { this._Unit = value; this.ID_UNIT = (value == null) ? 0 : (int)value.ID_UNIT; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpProtocol _Protocol;
        public OpProtocol Protocol { get { return this._Protocol; } set { this._Protocol = value; this.ID_PROTOCOL = (value == null) ? null : (int?)value.ID_PROTOCOL; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpCommandCodeParamMap()
            : base() { }

        public OpCommandCodeParamMap(DB_COMMAND_CODE_PARAM_MAP clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpCommandCodeParamMap> ConvertList(DB_COMMAND_CODE_PARAM_MAP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpCommandCodeParamMap> ConvertList(DB_COMMAND_CODE_PARAM_MAP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpCommandCodeParamMap> ret = new List<OpCommandCodeParamMap>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpCommandCodeParamMap(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpCommandCodeParamMap> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDataTypeClass> DataTypeClassDict = dataProvider.GetDataTypeClass(list.Select(l => l.ID_DATA_TYPE_CLASS).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE_CLASS);
                Dictionary<int, OpUnit> UnitDict = dataProvider.GetUnit(list.Select(l => l.ID_UNIT).Distinct().ToArray()).ToDictionary(l => l.ID_UNIT);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpProtocol> ProtocolDict = dataProvider.GetProtocol(list.Where(l => l.ID_PROTOCOL.HasValue).Select(l => l.ID_PROTOCOL.Value).Distinct().ToArray()).ToDictionary(l => l.ID_PROTOCOL);
                foreach (var loop in list)
                {
                    loop.DataTypeClass = DataTypeClassDict.TryGetValue(loop.ID_DATA_TYPE_CLASS);
                    loop.Unit = UnitDict.TryGetValue(loop.ID_UNIT);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    if(loop.ID_PROTOCOL.HasValue)
                        loop.Protocol = ProtocolDict.TryGetValue(loop.ID_PROTOCOL.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpCommandCodeParamMap> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpCommandCodeParamMap)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpCommandCodeParamMap).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpCommandCodeParamMap> Members
        public bool Equals(OpCommandCodeParamMap other)
        {
            if (other == null)
                return false;
            return this.IdCommandCodeParamMap.Equals(other.IdCommandCodeParamMap);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpCommandCodeParamMap)
                return this.IdCommandCodeParamMap == ((OpCommandCodeParamMap)obj).IdCommandCodeParamMap;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpCommandCodeParamMap left, OpCommandCodeParamMap right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpCommandCodeParamMap left, OpCommandCodeParamMap right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdCommandCodeParamMap.GetHashCode();
        }
        #endregion
    }
#endif
}