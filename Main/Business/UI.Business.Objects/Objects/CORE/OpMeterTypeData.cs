﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMeterTypeData : DB_METER_TYPE_DATA, IComparable, IEquatable<OpMeterTypeData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdMeterTypeData { get { return this.ID_METER_TYPE_DATA; } set { this.ID_METER_TYPE_DATA = value; } }
        public int IdMeterType { get { return this.ID_METER_TYPE; } set { this.ID_METER_TYPE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region	Navigation Properties
        private OpMeterType _MeterType;
        public OpMeterType MeterType { get { return this._MeterType; } set { this._MeterType = value; this.ID_METER_TYPE = (value == null) ? 0 : (int)value.ID_METER_TYPE; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpMeterTypeData()
            : base() { this.OpState = OpChangeState.New; }

        public OpMeterTypeData(DB_METER_TYPE_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "MeterTypeData [" + IdMeterTypeData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpMeterTypeData> ConvertList(DB_METER_TYPE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMeterTypeData> ConvertList(DB_METER_TYPE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpMeterTypeData> ret = new List<OpMeterTypeData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMeterTypeData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMeterTypeData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpMeterType> MeterTypeDict = dataProvider.GetMeterType(list.Select(l => l.ID_METER_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_METER_TYPE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.MeterType = MeterTypeDict.TryGetValue(loop.ID_METER_TYPE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpMeterTypeData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMeterTypeData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMeterTypeData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpMeterTypeData> Members
        public bool Equals(OpMeterTypeData other)
        {
            if (other == null)
                return false;
            return this.IdMeterTypeData.Equals(other.IdMeterTypeData);
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdMeterTypeData; } set { IdMeterTypeData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

        public OpChangeState OpState { get; set; }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpMeterTypeData clone = new OpMeterTypeData();
            clone.IdData = this.IdData;
            clone.IdMeterType = this.IdMeterType;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_METER_TYPE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMeterTypeData)
                return this.IdMeterTypeData == ((OpMeterTypeData)obj).IdMeterTypeData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMeterTypeData left, OpMeterTypeData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMeterTypeData left, OpMeterTypeData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMeterTypeData.GetHashCode();
        }
    #endregion
    }
#endif
}