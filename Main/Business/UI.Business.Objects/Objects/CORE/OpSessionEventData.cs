using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
     [Serializable]
    public class OpSessionEventData : DB_SESSION_EVENT_DATA, IOpData, IOpDataProvider, IComparable
	{
    #region Properties
		public long IdSessionEventData { get { return this.ID_SESSION_EVENT_DATA; } set { this.ID_SESSION_EVENT_DATA = value; } }
		public long IdSessionEvent { get { return this.ID_SESSION_EVENT; } set { this.ID_SESSION_EVENT = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpSessionEvent _SessionEvent;
		public OpSessionEvent SessionEvent { get { return this._SessionEvent; } set { this._SessionEvent = value; this.ID_SESSION_EVENT = (value == null) ? 0 : (long)value.ID_SESSION_EVENT; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdSessionEventData; } set { IdSessionEventData = value; } }
		public int Index { get; set; }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpSessionEventData()
			: base() { this.OpState = OpChangeState.New; }

		public OpSessionEventData(DB_SESSION_EVENT_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "SessionEventData [" + IdSessionEventData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpSessionEventData> ConvertList(DB_SESSION_EVENT_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpSessionEventData> ConvertList(DB_SESSION_EVENT_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSessionEventData> ret = new List<OpSessionEventData>(list.Length);
			foreach (var loop in list)
			{
				OpSessionEventData insert = new OpSessionEventData(loop);
				if (loadNavigationProperties)
				{
					insert.SessionEvent = dataProvider.GetSessionEvent(loop.ID_SESSION_EVENT);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSessionEventData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpSessionEventData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpSessionEventData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public object Clone(IDataProvider dataProvider)
		{
			OpSessionEventData clone = new OpSessionEventData();
			clone.IdData = this.IdData;
			clone.IdSessionEvent = this.IdSessionEvent;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_SESSION_EVENT_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.SessionEvent = dataProvider.GetSessionEvent(this.ID_SESSION_EVENT);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpSessionEventData)
				return this.IdData == ((OpSessionEventData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpSessionEventData left, OpSessionEventData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpSessionEventData left, OpSessionEventData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}