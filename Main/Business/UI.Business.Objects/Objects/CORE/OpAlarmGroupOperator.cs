using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAlarmGroupOperator : DB_ALARM_GROUP_OPERATOR, IComparable, IEquatable<OpAlarmGroupOperator>
    {
    #region Properties
        public int IdAlarmGroup { get { return this.ID_ALARM_GROUP; } set { this.ID_ALARM_GROUP = value; } }
        public int? IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; } }
        public long? IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; } }
    #endregion

    #region	Navigation Properties
        private OpAlarmGroup _AlarmGroup;
        public OpAlarmGroup AlarmGroup { get { return this._AlarmGroup; } set { this._AlarmGroup = value; this.IdAlarmGroup = (value == null) ? 0 : (int)value.IdAlarmGroup; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.IdOperator = (value == null) ? null : (int?)value.IdOperator; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.IdDataType = (value == null) ? null : (long?)value.IdDataType; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpAlarmGroupOperator()
            : base() { this.OpState = OpChangeState.New; }

        public OpAlarmGroupOperator(DB_ALARM_GROUP_OPERATOR clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "AlarmGroupOperator [" + IdAlarmGroup.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpAlarmGroupOperator> ConvertList(DB_ALARM_GROUP_OPERATOR[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmGroupOperator> ConvertList(DB_ALARM_GROUP_OPERATOR[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAlarmGroupOperator> ret = new List<OpAlarmGroupOperator>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmGroupOperator(db_object)));

            if (loadNavigationProperties)
            {
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmGroupOperator> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Where(l => l.ID_DATA_TYPE.HasValue).Select(l => l.ID_DATA_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                foreach (var loop in list)
                {
                    OpChangeState stateTemp = loop.OpState;
                    if (loop.ID_OPERATOR.HasValue)
                        loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value);
                    if (loop.ID_DATA_TYPE.HasValue)
                        loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE.Value);
                    loop.OpState = stateTemp;
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmGroupOperator> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmGroupOperator)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmGroupOperator).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpAlarmGroupOperator> Members
        public bool Equals(OpAlarmGroupOperator other)
        {
            if (other == null)
                return false;
            return (this.IdAlarmGroup.Equals(other.IdAlarmGroup) && this.IdDataType.Equals(other.IdDataType) && this.IdOperator.Equals(other.IdOperator));
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmGroupOperator)
                return (this.IdAlarmGroup == ((OpAlarmGroupOperator)obj).IdAlarmGroup && this.IdDataType == ((OpAlarmGroupOperator)obj).IdDataType && this.IdOperator == ((OpAlarmGroupOperator)obj).IdOperator);
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmGroupOperator left, OpAlarmGroupOperator right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmGroupOperator left, OpAlarmGroupOperator right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmGroup.GetHashCode();
        }
    #endregion
    }
#endif
}