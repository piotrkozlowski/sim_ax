﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpWarrantyContract : DB_WARRANTY_CONTRACT, IComparable, IEquatable<OpWarrantyContract>
    {
    #region Properties
        public int IdDeviceOrderNumber { get { return this.ID_DEVICE_ORDER_NUMBER; } set { this.ID_DEVICE_ORDER_NUMBER = value; } }
        public int IdComponent { get { return this.ID_COMPONENT; } set { this.ID_COMPONENT = value; } }
        public int IdContract { get { return this.ID_CONTRACT; } set { this.ID_CONTRACT = value; } }
        public int WarrantyTime { get { return this.WARRANTY_TIME; } set { this.WARRANTY_TIME = value; } }
        public int IdWarrantyStartDateCalculationMethod { get { return this.ID_WARRANTY_START_DATE_CALCULATION_METHOD; } set { this.ID_WARRANTY_START_DATE_CALCULATION_METHOD = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceOrderNumber _DeviceOrderNumber;
        public OpDeviceOrderNumber DeviceOrderNumber { get { return this._DeviceOrderNumber; } set { this._DeviceOrderNumber = value; this.ID_DEVICE_ORDER_NUMBER = (value == null) ? 0 : (int)value.ID_DEVICE_ORDER_NUMBER; } }
        private OpComponent _Component;
        public OpComponent Component { get { return this._Component; } set { this._Component = value; this.ID_COMPONENT = (value == null) ? 0 : (int)value.ID_COMPONENT; } }
        private OpContract _Contract;
        public OpContract Contract { get { return this._Contract; } set { this._Contract = value; this.ID_CONTRACT = (value == null) ? 0 : (int)value.ID_CONTRACT; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpWarrantyContract()
            : base() { }

        public OpWarrantyContract(DB_WARRANTY_CONTRACT clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "WarrantyContract [" + IdDeviceOrderNumber.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpWarrantyContract> ConvertList(DB_WARRANTY_CONTRACT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpWarrantyContract> ConvertList(DB_WARRANTY_CONTRACT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpWarrantyContract> ret = new List<OpWarrantyContract>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpWarrantyContract(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpWarrantyContract> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpComponent> ComponentDict = dataProvider.GetComponent(list.Select(l => l.ID_COMPONENT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_COMPONENT);
                Dictionary<int, OpContract> ContractDict = dataProvider.GetContract(list.Select(l => l.ID_CONTRACT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONTRACT);
                Dictionary<int, OpDeviceOrderNumber> DeviceOrderNumberDict = dataProvider.GetDeviceOrderNumber(list.Select(l => l.ID_DEVICE_ORDER_NUMBER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DEVICE_ORDER_NUMBER);

                foreach (var loop in list)
                {
                    loop.Component = ComponentDict.TryGetValue(loop.ID_COMPONENT);
                    loop.Contract = ContractDict.TryGetValue(loop.ID_CONTRACT);
                    loop.DeviceOrderNumber = DeviceOrderNumberDict.TryGetValue(loop.ID_DEVICE_ORDER_NUMBER);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpWarrantyContract> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpWarrantyContract)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpWarrantyContract).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpWarrantyContract> Members
        public bool Equals(OpWarrantyContract other)
        {
            if (other == null)
                return false;
            return this.IdDeviceOrderNumber.Equals(other.IdDeviceOrderNumber);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpWarrantyContract)
                return this.IdDeviceOrderNumber == ((OpWarrantyContract)obj).IdDeviceOrderNumber;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpWarrantyContract left, OpWarrantyContract right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpWarrantyContract left, OpWarrantyContract right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceOrderNumber.GetHashCode();
        }
    #endregion
    }
#endif
}