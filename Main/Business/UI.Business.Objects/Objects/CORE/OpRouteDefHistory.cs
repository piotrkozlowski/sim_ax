using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpRouteDefHistory : DB_ROUTE_DEF_HISTORY, IComparable, IEquatable<OpRouteDefHistory>
    {
    #region Properties
        public long IdRouteDefHistory { get { return this.ID_ROUTE_DEF_HISTORY;} set {this.ID_ROUTE_DEF_HISTORY = value;}}
        public int IdRouteDef { get { return this.ID_ROUTE_DEF;} set {this.ID_ROUTE_DEF = value;}}
        public int IdRouteStatus { get { return this.ID_ROUTE_STATUS;} set {this.ID_ROUTE_STATUS = value;}}
        public int IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
        public DateTime StartTime { get { return this.START_TIME;} set {this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME;} set {this.END_TIME = value; } }
    #endregion

    #region	Navigation Properties
		private OpRouteDef _RouteDef;
				public OpRouteDef RouteDef { get { return this._RouteDef; } set { this._RouteDef = value; this.ID_ROUTE_DEF = (value == null)? 0 : (int)value.ID_ROUTE_DEF; } }
				private OpRouteStatus _RouteStatus;
				public OpRouteStatus RouteStatus { get { return this._RouteStatus; } set { this._RouteStatus = value; this.ID_ROUTE_STATUS = (value == null)? 0 : (int)value.ID_ROUTE_STATUS; } }
				private OpOperator _Operator;
				public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpRouteDefHistory()
			:base() {}
		
		public OpRouteDefHistory(DB_ROUTE_DEF_HISTORY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "RouteDefHistory [" + IdRouteDefHistory.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpRouteDefHistory> ConvertList(DB_ROUTE_DEF_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpRouteDefHistory> ConvertList(DB_ROUTE_DEF_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpRouteDefHistory> ret = new List<OpRouteDefHistory>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpRouteDefHistory(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpRouteDefHistory> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpRouteDef> RouteDefDict = dataProvider.GetRouteDef(list.Select(l => l.ID_ROUTE_DEF).Distinct().ToArray()).ToDictionary(l => l.ID_ROUTE_DEF);
	Dictionary<int,OpRouteStatus> RouteStatusDict = dataProvider.GetRouteStatus(list.Select(l => l.ID_ROUTE_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_ROUTE_STATUS);
	Dictionary<int,OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
			
				foreach (var loop in list)
				{
						loop.RouteDef = RouteDefDict.TryGetValue(loop.ID_ROUTE_DEF); 
		loop.RouteStatus = RouteStatusDict.TryGetValue(loop.ID_ROUTE_STATUS); 
		loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRouteDefHistory> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteDefHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteDefHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpRouteDefHistory> Members
        public bool Equals(OpRouteDefHistory other)
        {
            if (other == null)
				return false;
			return this.IdRouteDefHistory.Equals(other.IdRouteDefHistory);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpRouteDefHistory)
				return this.IdRouteDefHistory == ((OpRouteDefHistory)obj).IdRouteDefHistory;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpRouteDefHistory left, OpRouteDefHistory right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpRouteDefHistory left, OpRouteDefHistory right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdRouteDefHistory.GetHashCode();
		}
    #endregion
    }
#endif
}