using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpDataChange : DB_DATA_CHANGE, IComparable
    {
    #region Properties
        public long IdDataChange { get { return this.ID_DATA_CHANGE;} set {this.ID_DATA_CHANGE = value;}}
        public string TableName { get { return this.TABLE_NAME;} set {this.TABLE_NAME = value;}}
        public long? ChangedId { get { return this.CHANGED_ID;} set {this.CHANGED_ID = value;}}
        public long? ChangedId2 { get { return this.CHANGED_ID2;} set {this.CHANGED_ID2 = value;}}
        public long? ChangedId3 { get { return this.CHANGED_ID3;} set {this.CHANGED_ID3 = value;}}
        public long? ChangedId4 { get { return this.CHANGED_ID4;} set {this.CHANGED_ID4 = value;}}
        public int IndexNbr { get { return this.INDEX_NBR;} set {this.INDEX_NBR = value;}}
        public int ChangeType { get { return this.CHANGE_TYPE;} set {this.CHANGE_TYPE = value;}}
        public DateTime Time { get { return this.TIME;} set {this.TIME = value; } }
        public string User { get { return this.USER;} set {this.USER = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDataChange()
			:base() {}
		
		public OpDataChange(DB_DATA_CHANGE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.TABLE_NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpDataChange> ConvertList(DB_DATA_CHANGE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpDataChange> ConvertList(DB_DATA_CHANGE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDataChange> ret = new List<OpDataChange>(list.Length);
			foreach (var loop in list)
			{
			OpDataChange insert = new OpDataChange(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
											
											
											
											
											
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDataChange> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataChange)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataChange).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}