using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDelivery : DB_DELIVERY, IComparable, IReferenceType
    {
    #region Properties
        public int IdDelivery { get { return this.ID_DELIVERY; } set { this.ID_DELIVERY = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        public string UploadedFile { get { return this.UPLOADED_FILE; } set { this.UPLOADED_FILE = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public int IdShippingList { get { return this.ID_SHIPPING_LIST; } set { this.ID_SHIPPING_LIST = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
    #endregion

    #region	Navigation Properties
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDelivery()
            : base() { }

        public OpDelivery(DB_DELIVERY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDelivery> ConvertList(DB_DELIVERY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDelivery> ConvertList(DB_DELIVERY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDelivery> ret = new List<OpDelivery>(list.Length);
            foreach (var loop in list)
            {
                OpDelivery insert = new OpDelivery(loop);

                if (loadNavigationProperties)
                {






                    insert.Operator = dataProvider.GetOperator(loop.ID_OPERATOR);

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDelivery> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDelivery)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDelivery).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdDelivery;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}