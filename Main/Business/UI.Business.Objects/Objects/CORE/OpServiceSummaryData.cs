﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceSummaryData : DB_SERVICE_SUMMARY_DATA, IComparable, IEquatable<OpServiceSummaryData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdServiceSummaryData { get { return this.ID_SERVICE_SUMMARY_DATA; } set { this.ID_SERVICE_SUMMARY_DATA = value; } }
        public int IdServiceList { get { return this.ID_SERVICE_LIST; } set { this.ID_SERVICE_LIST = value; } }
        public long IdServicePackage { get { return this.ID_SERVICE_PACKAGE; } set { this.ID_SERVICE_PACKAGE = value; } }
        public Double Cost { get { return this.COST; } set { this.COST = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpServiceList _ServiceList;
        public OpServiceList ServiceList { get { return this._ServiceList; } set { this._ServiceList = value; this.ID_SERVICE_LIST = (value == null) ? 0 : (int)value.ID_SERVICE_LIST; } }
        private OpServicePackage _ServicePackage;
        public OpServicePackage ServicePackage { get { return this._ServicePackage; } set { this._ServicePackage = value; this.ID_SERVICE_PACKAGE = (value == null) ? 0 : (long)value.ID_SERVICE_PACKAGE; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpServiceSummaryData()
            : base() { }

        public OpServiceSummaryData(DB_SERVICE_SUMMARY_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceSummaryData [" + IdServiceSummaryData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceSummaryData> ConvertList(DB_SERVICE_SUMMARY_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceSummaryData> ConvertList(DB_SERVICE_SUMMARY_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceSummaryData> ret = new List<OpServiceSummaryData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceSummaryData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceSummaryData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpServiceList> ServiceListDict = dataProvider.GetServiceList(list.Select(l => l.ID_SERVICE_LIST).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_LIST);
                Dictionary<long, OpServicePackage> ServicePackageDict = dataProvider.GetServicePackage(list.Select(l => l.ID_SERVICE_PACKAGE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_PACKAGE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.ServiceList = ServiceListDict.TryGetValue(loop.ID_SERVICE_LIST);
                    loop.ServicePackage = ServicePackageDict.TryGetValue(loop.ID_SERVICE_PACKAGE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceSummaryData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceSummaryData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceSummaryData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceSummaryData> Members
        public bool Equals(OpServiceSummaryData other)
        {
            if (other == null)
                return false;
            return this.IdServiceSummaryData.Equals(other.IdServiceSummaryData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceSummaryData)
                return this.IdServiceSummaryData == ((OpServiceSummaryData)obj).IdServiceSummaryData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceSummaryData left, OpServiceSummaryData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceSummaryData left, OpServiceSummaryData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceSummaryData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdServiceSummaryData; } set { IdServiceSummaryData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpServiceSummaryData clone = new OpServiceSummaryData();
            clone.IdData = this.IdData;
            clone.IdServiceList = this.IdServiceList;
            clone.IdServicePackage = this.IdServicePackage;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_SERVICE_SUMMARY_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}