using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionTypeGroup : DB_ACTION_TYPE_GROUP, IComparable, IEquatable<OpActionTypeGroup>
    {
    #region Properties
        public int IdActionTypeGroup { get { return this.ID_ACTION_TYPE_GROUP; } set { this.ID_ACTION_TYPE_GROUP = value; } }
        public int IdActionTypeGroupType { get { return this.ID_ACTION_TYPE_GROUP_TYPE; } set { this.ID_ACTION_TYPE_GROUP_TYPE = value; } }
        public int? IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { this.ID_REFERENCE_TYPE = value; } }
        public object ReferenceValue { get { return this.REFERENCE_VALUE; } set { this.REFERENCE_VALUE = value; } }
        public int? IdParentGroup { get { return this.ID_PARENT_GROUP; } set { this.ID_PARENT_GROUP = value; } }
    #endregion

    #region	Navigation Properties
        private OpActionTypeGroupType _ActionTypeGroupType;
        public OpActionTypeGroupType ActionTypeGroupType { get { return this._ActionTypeGroupType; } set { this._ActionTypeGroupType = value; this.ID_ACTION_TYPE_GROUP_TYPE = (value == null) ? 0 : (int)value.ID_ACTION_TYPE_GROUP_TYPE; } }
        private OpReferenceType _ReferenceType;
        public OpReferenceType ReferenceType { get { return this._ReferenceType; } set { this._ReferenceType = value; this.ID_REFERENCE_TYPE = (value == null) ? null : (int?)value.ID_REFERENCE_TYPE; } }
        private OpActionTypeGroup _ParentGroup;
        public OpActionTypeGroup ParentGroup { get { return this._ParentGroup; } set { this._ParentGroup = value; this.ID_PARENT_GROUP = (value == null) ? 0 : (int)value.ID_ACTION_TYPE_GROUP; } }
    #endregion

    #region	Custom Properties
        public object Value { get; set; }
    #endregion

    #region	Ctor
        public OpActionTypeGroup()
            : base() { }

        public OpActionTypeGroup(DB_ACTION_TYPE_GROUP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.ActionTypeGroupType.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpActionTypeGroup> ConvertList(DB_ACTION_TYPE_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActionTypeGroup> ConvertList(DB_ACTION_TYPE_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActionTypeGroup> ret = new List<OpActionTypeGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActionTypeGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionTypeGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpActionTypeGroupType> ActionTypeGroupTypeDict = dataProvider.GetActionTypeGroupType(list.Select(l => l.ID_ACTION_TYPE_GROUP_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_TYPE_GROUP_TYPE);
                Dictionary<int, OpReferenceType> ReferenceTypeDict = dataProvider.GetReferenceType(list.Where(l => l.ID_REFERENCE_TYPE.HasValue).Select(l => l.ID_REFERENCE_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_REFERENCE_TYPE);
                Dictionary<int, OpActionTypeGroup> ParentGroupDict = dataProvider.GetActionTypeGroup(list.Where(l => l.ID_PARENT_GROUP.HasValue).Select(l => l.ID_PARENT_GROUP.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_TYPE_GROUP);

                foreach (var loop in list)
                {
                    loop.ActionTypeGroupType = ActionTypeGroupTypeDict.TryGetValue(loop.ID_ACTION_TYPE_GROUP_TYPE);
                    if (loop.ID_PARENT_GROUP.HasValue)
                        loop.ParentGroup = ParentGroupDict.TryGetValue(loop.ID_PARENT_GROUP.Value);
                    if (loop.ID_REFERENCE_TYPE.HasValue)
                        loop.ReferenceType = ReferenceTypeDict.TryGetValue(loop.ID_REFERENCE_TYPE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionTypeGroup> list, IDataProvider dataProvider)
        {
            foreach (var loop in list)
            {
    #region Value
                loop.Value = loop.ReferenceValue; //przypisujemy na wst�pie domysln� warto��, jesli jest podany IdReference to pr�bujemy wyciagnac wlasciwy obiekt poni�ej

                if (loop.IdReferenceType.HasValue && loop.ReferenceValue != null)
                {
                    try
                    {
                        loop.Value = dataProvider.GetReferenceObject((Enums.ReferenceType)loop.IdReferenceType.Value, loop.ReferenceValue);
                    }
                    catch
                    {
                    }
                }
    #endregion
            }
        }
    #endregion
    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionTypeGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionTypeGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionTypeGroup> Members
        public bool Equals(OpActionTypeGroup other)
        {
            if (other == null)
                return false;
            return this.IdActionTypeGroup.Equals(other.IdActionTypeGroup);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActionTypeGroup)
                return this.IdActionTypeGroup == ((OpActionTypeGroup)obj).IdActionTypeGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActionTypeGroup left, OpActionTypeGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionTypeGroup left, OpActionTypeGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActionTypeGroup.GetHashCode();
        }
    #endregion
    }
#endif
}