using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpSeal : DB_SEAL, IComparable
    {
    #region Properties
            public long IdSeal { get { return this.ID_SEAL;} set {this.ID_SEAL = value;}}
                public string SerialNbr { get { return this.SERIAL_NBR;} set {this.SERIAL_NBR = value;}}
                public string Description { get { return this.DESCRIPTION;} set {this.DESCRIPTION = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpSeal()
			:base() {}
		
		public OpSeal(DB_SEAL clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.SERIAL_NBR;
		}
    #endregion

    #region	ConvertList
		public static List<OpSeal> ConvertList(DB_SEAL[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpSeal> ConvertList(DB_SEAL[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSeal> ret = new List<OpSeal>(list.Length);
			foreach (var loop in list)
			{
			OpSeal insert = new OpSeal(loop);
				
				if(loadNavigationProperties)
				{
											
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSeal> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSeal)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSeal).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}