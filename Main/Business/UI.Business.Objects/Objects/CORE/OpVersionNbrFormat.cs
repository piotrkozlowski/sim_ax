using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionNbrFormat : DB_VERSION_NBR_FORMAT, IComparable, IEquatable<OpVersionNbrFormat>, IOpChangeState
    {
    #region Properties
        public int IdVersionNbrFormat { get { return this.ID_VERSION_NBR_FORMAT; } set { if (this.ID_VERSION_NBR_FORMAT != value) { this.ID_VERSION_NBR_FORMAT = OpDataUtil.SetNewValue(this.ID_VERSION_NBR_FORMAT, value, this); } } }
        public string Name { get { return this.NAME; } set { if (this.NAME != value) { this.NAME = OpDataUtil.SetNewValue(this.NAME, value, this); } } }
        public long? IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = OpDataUtil.SetNewValue(this.ID_DESCR, value, this); } } }
        public int IdDataTypeClass { get { return this.ID_DATA_TYPE_CLASS; } set { if (this.ID_DATA_TYPE_CLASS != value) { this.ID_DATA_TYPE_CLASS = OpDataUtil.SetNewValue(this.ID_DATA_TYPE_CLASS, value, this); } } }
        public string RegularExpression { get { return this.REGULAR_EXPRESSION; } set { if (this.REGULAR_EXPRESSION != value) { this.REGULAR_EXPRESSION = OpDataUtil.SetNewValue(this.REGULAR_EXPRESSION, value, this); } } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
		public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
		private OpDataTypeClass _DataTypeClass;
		public OpDataTypeClass DataTypeClass { get { return this._DataTypeClass; } set { this._DataTypeClass = value; this.ID_DATA_TYPE_CLASS = (value == null)? 0 : (int)value.ID_DATA_TYPE_CLASS; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpVersionNbrFormat()
			:base() {}
		
		public OpVersionNbrFormat(DB_VERSION_NBR_FORMAT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionNbrFormat> ConvertList(DB_VERSION_NBR_FORMAT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionNbrFormat> ConvertList(DB_VERSION_NBR_FORMAT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionNbrFormat> ret = new List<OpVersionNbrFormat>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionNbrFormat(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionNbrFormat> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                Dictionary<int, OpDataTypeClass> DataTypeClassDict = dataProvider.GetDataTypeClass(list.Select(l => l.ID_DATA_TYPE_CLASS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE_CLASS);

                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    loop.DataTypeClass = DataTypeClassDict.TryGetValue(loop.ID_DATA_TYPE_CLASS);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionNbrFormat> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionNbrFormat)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionNbrFormat).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionNbrFormat> Members
        public bool Equals(OpVersionNbrFormat other)
        {
            if (other == null)
				return false;
			return this.IdVersionNbrFormat.Equals(other.IdVersionNbrFormat);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionNbrFormat)
				return this.IdVersionNbrFormat == ((OpVersionNbrFormat)obj).IdVersionNbrFormat;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionNbrFormat left, OpVersionNbrFormat right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionNbrFormat left, OpVersionNbrFormat right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionNbrFormat.GetHashCode();
		}
    #endregion
	}
#endif
}