using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable]
    public class OpWorkHistoryData : DB_WORK_HISTORY_DATA, IComparable, IEquatable<OpWorkHistoryData>
    {
        #region Properties
        [XmlIgnore]
        public long IdWorkHistoryData { get { return this.ID_WORK_HISTORY_DATA; } set { this.ID_WORK_HISTORY_DATA = value; } }
        [XmlIgnore]
        public long IdWorkHistory { get { return this.ID_WORK_HISTORY; } set { this.ID_WORK_HISTORY = value; } }
        [XmlIgnore]
        public int ArgNbr { get { return this.ARG_NBR; } set { this.ARG_NBR = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpWorkHistory _WorkHistory;
        [XmlIgnore]
        public OpWorkHistory WorkHistory { get { return this._WorkHistory; } set { this._WorkHistory = value; this.ID_WORK_HISTORY = (value == null) ? 0 : (long)value.ID_WORK_HISTORY; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpWorkHistoryData()
            : base() { }

        public OpWorkHistoryData(DB_WORK_HISTORY_DATA clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "WorkHistoryData [" + IdWorkHistoryData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpWorkHistoryData> ConvertList(DB_WORK_HISTORY_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpWorkHistoryData> ConvertList(DB_WORK_HISTORY_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpWorkHistoryData> ret = new List<OpWorkHistoryData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpWorkHistoryData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpWorkHistoryData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<long, OpWorkHistory> WorkHistoryDict = dataProvider.GetWorkHistory(list.Select(l => l.ID_WORK_HISTORY).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_WORK_HISTORY);

                //foreach (var loop in list)
                //{
                    //loop.WorkHistory = WorkHistoryDict.TryGetValue(loop.ID_WORK_HISTORY);
                //}
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpWorkHistoryData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpWorkHistoryData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpWorkHistoryData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpWorkHistoryData> Members
        public bool Equals(OpWorkHistoryData other)
        {
            if (other == null)
                return false;
            return this.IdWorkHistoryData.Equals(other.IdWorkHistoryData);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpWorkHistoryData)
                return this.IdWorkHistoryData == ((OpWorkHistoryData)obj).IdWorkHistoryData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpWorkHistoryData left, OpWorkHistoryData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpWorkHistoryData left, OpWorkHistoryData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdWorkHistoryData.GetHashCode();
        }
        #endregion
    }
}