﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpConversion : DB_CONVERSION, IComparable, IEquatable<OpConversion>
    {
    #region Properties
        public long IdConversion { get { return this.ID_CONVERSION; } set { if (this.ID_CONVERSION != value) { this.ID_CONVERSION = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { if (this.ID_REFERENCE_TYPE != value) { this.ID_REFERENCE_TYPE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public long ReferenceValue { get { return this.REFERENCE_VALUE; } set { if (this.REFERENCE_VALUE != value) { this.REFERENCE_VALUE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { if (this.ID_DATA_TYPE != value) { this.ID_DATA_TYPE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { if (this.INDEX_NBR != value) { this.INDEX_NBR = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public Double Slope { get { return this.SLOPE; } set { if (this.SLOPE != value) { this.SLOPE = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        public Double Bias { get { return this.BIAS; } set { if (this.BIAS != value) { this.BIAS = value; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpReferenceType _ReferenceType;
        public OpReferenceType ReferenceType { get { return this._ReferenceType; } set { if (this._ReferenceType != value) { this._ReferenceType = value; this.ID_REFERENCE_TYPE = (value == null) ? 0 : (int)value.ID_REFERENCE_TYPE; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { if (this._DataType != value) { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; if (this.OpState == OpChangeState.Loaded || this.OpState == OpChangeState.Undefined) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpConversion()
            : base() { }

        public OpConversion(DB_CONVERSION clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "Conversion [" + IdConversion.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpConversion> ConvertList(DB_CONVERSION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpConversion> ConvertList(DB_CONVERSION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpConversion> ret = new List<OpConversion>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpConversion(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConversion> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpReferenceType> ReferenceTypeDict = dataProvider.GetReferenceType(list.Select(l => l.ID_REFERENCE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_REFERENCE_TYPE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.ReferenceType = ReferenceTypeDict.TryGetValue(loop.ID_REFERENCE_TYPE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConversion> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConversion)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConversion).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpConversion> Members
        public bool Equals(OpConversion other)
        {
            if (other == null)
                return false;
            return this.IdConversion.Equals(other.IdConversion);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpConversion)
                return this.IdConversion == ((OpConversion)obj).IdConversion;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpConversion left, OpConversion right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpConversion left, OpConversion right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdConversion.GetHashCode();
        }
    #endregion
    }
#endif
}