using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpRouteDevice : DB_ROUTE_DEVICE, IComparable
    {
    #region Properties
        public long IdRouteDevice { get { return this.ID_ROUTE_DEVICE; } set { this.ID_ROUTE_DEVICE = value; } }
        public long IdRoute { get { return this.ID_ROUTE; } set { this.ID_ROUTE = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
    #endregion

    #region	Navigation Properties
        private OpRoute _Route;
        public OpRoute Route { get { return this._Route; } set { this._Route = value; this.ID_ROUTE = (value == null) ? 0 : (long)value.ID_ROUTE; } }
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? 0 : (long)value.SERIAL_NBR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpRouteDevice()
            : base() { }

        public OpRouteDevice(DB_ROUTE_DEVICE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "RouteDevice [" + IdRouteDevice.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpRouteDevice> ConvertList(DB_ROUTE_DEVICE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpRouteDevice> ConvertList(DB_ROUTE_DEVICE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpRouteDevice> ret = new List<OpRouteDevice>(list.Length);
            foreach (var loop in list)
            {
                OpRouteDevice insert = new OpRouteDevice(loop);

                if (loadNavigationProperties)
                {

                    insert.Route = dataProvider.GetRoute(loop.ID_ROUTE);
                    insert.Device = dataProvider.GetDevice(loop.SERIAL_NBR);
                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRouteDevice> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteDevice)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteDevice).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion
    }
#endif
}