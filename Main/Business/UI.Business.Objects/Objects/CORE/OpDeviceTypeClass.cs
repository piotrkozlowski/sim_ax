using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTypeClass : DB_DEVICE_TYPE_CLASS, IComparable
    {
    #region Properties
        [XmlIgnore]
        public int IdDeviceTypeClass { get { return this.ID_DEVICE_TYPE_CLASS; } set { this.ID_DEVICE_TYPE_CLASS = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceTypeClass()
            : base() { }

        public OpDeviceTypeClass(DB_DEVICE_TYPE_CLASS clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
                return this.Name;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceTypeClass> ConvertList(DB_DEVICE_TYPE_CLASS[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDeviceTypeClass> ConvertList(DB_DEVICE_TYPE_CLASS[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDeviceTypeClass> ret = new List<OpDeviceTypeClass>(list.Length);
            foreach (var loop in list)
            {
                OpDeviceTypeClass insert = new OpDeviceTypeClass(loop);

                if (loadNavigationProperties)
                {


                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[] { loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceTypeClass> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTypeClass)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTypeClass).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Enum
        public enum Enum
        {
            OKO = 1,
            RADIO_DEVICE = 2,
            ATG = 3,
            OTHER = 4,
            COMPONENT = 5
        }
    #endregion
    }
#endif
}