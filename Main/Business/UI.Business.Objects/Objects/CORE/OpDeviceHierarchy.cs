using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[DebuggerDisplay("SerialNbrParent:{SerialNbrParent}; IdSlotType:{IdSlotType}; SlotNbr:{SlotNbr}; IdProtocolIn:{IdProtocolIn}; IdProtocolOut:{IdProtocolOut}; SerialNbr:{SerialNbr}; OpState:{OpState}")]
    [Serializable]
    public class OpDeviceHierarchy : DB_DEVICE_HIERARCHY, IOpChangeState, IComparable
    {
    #region Properties
        [XmlIgnore]
        public long IdDeviceHierarchy { get { return this.ID_DEVICE_HIERARCHY; } set { this.ID_DEVICE_HIERARCHY = value; } }
        [XmlIgnore]
        public long SerialNbrParent { get { return this.SERIAL_NBR_PARENT; } set { this.SERIAL_NBR_PARENT = value; } }
        [XmlIgnore]
        public int IdSlotType { get { return this.ID_SLOT_TYPE; } set { this.ID_SLOT_TYPE = value; } }
        [XmlIgnore]
        public int SlotNbr { get { return this.SLOT_NBR; } set { this.SLOT_NBR = value; } }
        [XmlIgnore]
        public int? IdProtocolIn { get { return this.ID_PROTOCOL_IN; } set { this.ID_PROTOCOL_IN = value; } }
        [XmlIgnore]
        public int? IdProtocolOut { get { return this.ID_PROTOCOL_OUT; } set { this.ID_PROTOCOL_OUT = value; } }
        [XmlIgnore]
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        [XmlIgnore]
        public bool IsActive { get { return this.IS_ACTIVE; } set { this.IS_ACTIVE = value; } }
        [XmlIgnore]
        public DateTime StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        [XmlIgnore]
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
    #endregion

    #region Navigation Properties
		private OpDevice _DeviceParent;
		public OpDevice DeviceParent { get { return this._DeviceParent; } set { this._DeviceParent = value; this.SerialNbrParent = (value == null) ? 0 : value.SERIAL_NBR; } }
		private OpSlotType _SlotType;
        [XmlIgnore]
		public OpSlotType SlotType { get { return this._SlotType; } set { this._SlotType = value; this.IdSlotType = (value == null)? 0 : (int)value.ID_SLOT_TYPE; } }
		private OpProtocol _ProtocolIn;
        [XmlIgnore]
		public OpProtocol ProtocolIn { get { return this._ProtocolIn; } set { this._ProtocolIn = value; this.IdProtocolIn = (value == null) ? null : (int?)value.ID_PROTOCOL; } }
		private OpProtocol _ProtocolOut;
        [XmlIgnore]
		public OpProtocol ProtocolOut { get { return this._ProtocolOut; } set { this._ProtocolOut = value; this.IdProtocolOut = (value == null) ? null : (int?)value.ID_PROTOCOL; } }
		private OpDevice _Device;
		public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SerialNbr = (value == null) ? null : (long?)value.SERIAL_NBR; } }
    #endregion

    #region Custom Properties
		public OpChangeState OpState { get; set; }
    #endregion
		
    #region Ctor
		public OpDeviceHierarchy()
			: base() { this.OpState = OpChangeState.New; }
		
		public OpDeviceHierarchy(DB_DEVICE_HIERARCHY clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion
		
    #region ToString
		public override string ToString()
		{
				return "DeviceHierarchy [" + IdDeviceHierarchy.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpDeviceHierarchy> ConvertList(DB_DEVICE_HIERARCHY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpDeviceHierarchy> ConvertList(DB_DEVICE_HIERARCHY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceHierarchy> ret = new List<OpDeviceHierarchy>(list.Length);
			foreach (var loop in list)
			{
				OpDeviceHierarchy insert = new OpDeviceHierarchy(loop);
				ret.Add(insert);
			}

			if (loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDeviceHierarchy> list, IDataProvider dataProvider, bool queryDatabase = false)
		{
			if (list != null && list.Count > 0)
			{
				Dictionary<long, OpDevice> DeviceParentDict = dataProvider.GetDevice(list.Select(l => l.SERIAL_NBR_PARENT).Distinct().ToArray(), queryDatabase).ToDictionary(l => l.SERIAL_NBR);
				Dictionary<int, OpSlotType> SlotTypeDict = dataProvider.GetSlotType(list.Select(l => l.ID_SLOT_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_SLOT_TYPE);
				Dictionary<int, OpProtocol> ProtocolInDict = dataProvider.GetProtocol(list.Where(l => l.ID_PROTOCOL_IN.HasValue).Select(l => l.ID_PROTOCOL_IN.Value).Distinct().ToArray()).ToDictionary(l => l.ID_PROTOCOL);
				Dictionary<int, OpProtocol> ProtocolOutDict = dataProvider.GetProtocol(list.Where(l => l.ID_PROTOCOL_OUT.HasValue).Select(l => l.ID_PROTOCOL_OUT.Value).Distinct().ToArray()).ToDictionary(l => l.ID_PROTOCOL);
				Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Where(l => l.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray(), queryDatabase).ToDictionary(l => l.SERIAL_NBR);

				foreach (var loop in list)
				{
					loop.DeviceParent = DeviceParentDict.TryGetValue(loop.SERIAL_NBR_PARENT);
					loop.SlotType = SlotTypeDict.TryGetValue(loop.ID_SLOT_TYPE);
					if (loop.ID_PROTOCOL_IN.HasValue)
						loop.ProtocolIn = ProtocolInDict.TryGetValue(loop.ID_PROTOCOL_IN.Value);
					if (loop.ID_PROTOCOL_OUT.HasValue)
						loop.ProtocolOut = ProtocolOutDict.TryGetValue(loop.ID_PROTOCOL_OUT.Value);
					if (loop.SERIAL_NBR.HasValue)
						loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR.Value);
				}
			}
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceHierarchy> list, IDataProvider dataProvider)
        {
		}
    #endregion

    #region AssignReferences
		public void AssignReferences(IDataProvider dataProvider, bool queryDatabase = false)
		{
			this.DeviceParent = dataProvider.GetDevice(this.SERIAL_NBR_PARENT);
			this.SlotType = dataProvider.GetSlotType(this.ID_SLOT_TYPE);
			if (this.ID_PROTOCOL_IN.HasValue)
				this.ProtocolIn = dataProvider.GetProtocol(this.ID_PROTOCOL_IN.Value);
			if (this.ID_PROTOCOL_OUT.HasValue)
				this.ProtocolOut = dataProvider.GetProtocol(this.ID_PROTOCOL_OUT.Value);
			if (this.SERIAL_NBR.HasValue)
				this.Device = dataProvider.GetDevice(this.SERIAL_NBR.Value, queryDatabase);
		}
    #endregion

    #region IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceHierarchy)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceHierarchy).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Clone
		/// <summary>
		/// Attention! After Clone() Navigation Properties will be empty. If needed use AssignReferences(...)
		/// </summary>
		public object Clone()
		{
			OpDeviceHierarchy clone = new OpDeviceHierarchy();
			clone.IdDeviceHierarchy = this.IdDeviceHierarchy;
			clone.SerialNbrParent = this.SerialNbrParent;
			clone.IdSlotType = this.IdSlotType;
			clone.SlotNbr = this.SlotNbr;
			clone.IdProtocolIn = this.IdProtocolIn;
			clone.IdProtocolOut = this.IdProtocolOut;
			clone.SerialNbr = this.SerialNbr;
			clone.IsActive = this.IsActive;
			clone.StartTime = this.StartTime;
			clone.EndTime = this.EndTime;

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
            OpDeviceHierarchy castObj = obj as OpDeviceHierarchy;
            if (castObj != null)
            {
                if (this.IdDeviceHierarchy == castObj.IdDeviceHierarchy)
                {
                    if (this.IdDeviceHierarchy == 0)
                        return ReferenceEquals(this, castObj);
                    else
                        return true;
                }
                else
                    return false;
            }
            else
                return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceHierarchy left, OpDeviceHierarchy right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceHierarchy left, OpDeviceHierarchy right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDeviceHierarchy.GetHashCode();
		}
    #endregion
    }
#endif
}