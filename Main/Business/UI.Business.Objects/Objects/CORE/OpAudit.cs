﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAudit : DB_AUDIT, IComparable, IEquatable<OpAudit>
    {
    #region Properties
        public long IdAudit { get { return this.ID_AUDIT; } set { this.ID_AUDIT = value; } }
        public long BatchId { get { return this.BATCH_ID; } set { this.BATCH_ID = value; } }
        public int ChangeType { get { return this.CHANGE_TYPE; } set { this.CHANGE_TYPE = value; } }
        public string TableName { get { return this.TABLE_NAME; } set { this.TABLE_NAME = value; } }
        public long? Key1 { get { return this.KEY1; } set { this.KEY1 = value; } }
        public long? Key2 { get { return this.KEY2; } set { this.KEY2 = value; } }
        public long? Key3 { get { return this.KEY3; } set { this.KEY3 = value; } }
        public long? Key4 { get { return this.KEY4; } set { this.KEY4 = value; } }
        public long? Key5 { get { return this.KEY5; } set { this.KEY5 = value; } }
        public long? Key6 { get { return this.KEY6; } set { this.KEY6 = value; } }
        public string ColumnName { get { return this.COLUMN_NAME; } set { this.COLUMN_NAME = value; } }
        public object OldValue { get { return this.OLD_VALUE; } set { this.OLD_VALUE = value; } }
        public object NewValue { get { return this.NEW_VALUE; } set { this.NEW_VALUE = value; } }
        public DateTime Time { get { return this.TIME; } set { this.TIME = value; } }
        public string User { get { return this.USER; } set { this.USER = value; } }
        public string Host { get { return this.HOST; } set { this.HOST = value; } }
        public string Application { get { return this.APPLICATION; } set { this.APPLICATION = value; } }
        public string Contextinfo { get { return this.CONTEXTINFO; } set { this.CONTEXTINFO = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
        public string AuditFullInfo
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (!String.IsNullOrWhiteSpace(this.Host))
                    sb.AppendFormat("{0} ", this.Host);
                if (!String.IsNullOrWhiteSpace(this.User))
                    sb.AppendFormat("({0}) ", this.User);
                if (!String.IsNullOrWhiteSpace(this.Application))
                    sb.AppendFormat("[{0}] ", this.Application);
                if (!String.IsNullOrWhiteSpace(this.Contextinfo))
                    sb.Append("{").AppendFormat("{0}", this.Contextinfo).Append("} ");

                if (sb.Length == 0)
                    return null;
                return sb.ToString();
            }
        }
    #endregion

    #region	Ctor
        public OpAudit()
            : base() { }

        public OpAudit(DB_AUDIT clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.TABLE_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpAudit> ConvertList(DB_AUDIT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAudit> ConvertList(DB_AUDIT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAudit> ret = new List<OpAudit>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAudit(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAudit> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpAudit> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAudit)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAudit).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpAudit> Members
        public bool Equals(OpAudit other)
        {
            if (other == null)
                return false;
            return this.IdAudit.Equals(other.IdAudit);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAudit)
                return this.IdAudit == ((OpAudit)obj).IdAudit;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAudit left, OpAudit right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAudit left, OpAudit right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAudit.GetHashCode();
        }
    #endregion
    }
#endif
}





