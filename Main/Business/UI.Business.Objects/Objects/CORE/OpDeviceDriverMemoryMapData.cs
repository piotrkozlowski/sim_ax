using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceDriverMemoryMapData : DB_DEVICE_DRIVER_MEMORY_MAP_DATA, IComparable, IEquatable<OpDeviceDriverMemoryMapData>
    {
    #region Properties
        public long IdDeviceDriverMemoryMapData { get { return this.ID_DEVICE_DRIVER_MEMORY_MAP_DATA; } set { this.ID_DEVICE_DRIVER_MEMORY_MAP_DATA = value; } }
        public long? IdDdmmdParent { get { return this.ID_DDMMD_PARENT; } set { this.ID_DDMMD_PARENT = value; } }
        public int IdDeviceDriverMemoryMap { get { return this.ID_DEVICE_DRIVER_MEMORY_MAP; } set { this.ID_DEVICE_DRIVER_MEMORY_MAP = value; } }
        public int? IdOmb { get { return this.ID_OMB; } set { this.ID_OMB = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int? IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public bool IsRead { get { return this.IS_READ; } set { this.IS_READ = value; } }
        public bool IsWrite { get { return this.IS_WRITE; } set { this.IS_WRITE = value; } }
        public bool IsPutDataOmb { get { return this.IS_PUT_DATA_OMB; } set { this.IS_PUT_DATA_OMB = value; } }
        public long? IdDdmmdPutData { get { return this.ID_DDMMD_PUT_DATA; } set { this.ID_DDMMD_PUT_DATA = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceDriverMemoryMap _DeviceDriverMemoryMap;
        public OpDeviceDriverMemoryMap DeviceDriverMemoryMap { get { return this._DeviceDriverMemoryMap; } set { this._DeviceDriverMemoryMap = value; this.ID_DEVICE_DRIVER_MEMORY_MAP = (value == null) ? 0 : (int)value.ID_DEVICE_DRIVER_MEMORY_MAP; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? null : (long?)value.ID_DATA_TYPE; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceDriverMemoryMapData()
            : base() { }

        public OpDeviceDriverMemoryMapData(DB_DEVICE_DRIVER_MEMORY_MAP_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceDriverMemoryMapData> ConvertList(DB_DEVICE_DRIVER_MEMORY_MAP_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceDriverMemoryMapData> ConvertList(DB_DEVICE_DRIVER_MEMORY_MAP_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceDriverMemoryMapData> ret = new List<OpDeviceDriverMemoryMapData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceDriverMemoryMapData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceDriverMemoryMapData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceDriverMemoryMap> DeviceDriverMemoryMapDict = dataProvider.GetDeviceDriverMemoryMap(list.Select(l => l.ID_DEVICE_DRIVER_MEMORY_MAP).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_DRIVER_MEMORY_MAP);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Where(l => l.ID_DATA_TYPE.HasValue).Select(l => l.ID_DATA_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    loop.DeviceDriverMemoryMap = DeviceDriverMemoryMapDict.TryGetValue(loop.ID_DEVICE_DRIVER_MEMORY_MAP);
                    if (loop.ID_DATA_TYPE.HasValue)
                        loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE.Value);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceDriverMemoryMapData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceDriverMemoryMapData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceDriverMemoryMapData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceDriverMemoryMapData> Members
        public bool Equals(OpDeviceDriverMemoryMapData other)
        {
            if (other == null)
                return false;
            return this.IdDeviceDriverMemoryMapData.Equals(other.IdDeviceDriverMemoryMapData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceDriverMemoryMapData)
                return this.IdDeviceDriverMemoryMapData == ((OpDeviceDriverMemoryMapData)obj).IdDeviceDriverMemoryMapData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceDriverMemoryMapData left, OpDeviceDriverMemoryMapData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceDriverMemoryMapData left, OpDeviceDriverMemoryMapData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceDriverMemoryMapData.GetHashCode();
        }
    #endregion
    }
#endif
}