using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpConsumerType : DB_CONSUMER_TYPE, IComparable, IEquatable<OpConsumerType>
    {
    #region Properties
        [XmlIgnore]
        public int IdConsumerType { get { return this.ID_CONSUMER_TYPE; } set { this.ID_CONSUMER_TYPE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
    #endregion

    #region Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 1 : value.ID_DISTRIBUTOR; } }
    #endregion

    #region Custom Properties
        public OpDataList<OpConsumerTypeData> DataList { get; set; }

        [XmlIgnore]
        public int? DT_CONSUMER_TYPE_OWED_REPAYMENT_PERCENTAGE
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.CONSUMER_TYPE_AGREED_OWED);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_TYPE_AGREED_OWED, value);
            }
        }

        [XmlIgnore]
        public double? DT_CONSUMER_TYPE_EC_LIMIT
        {
            get
            {
                return DataList.TryGetNullableValue<double>(DataType.CONSUMER_TYPE_EMERGENCY_CREDIT);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_TYPE_EMERGENCY_CREDIT, value);
            }
        }

        [XmlIgnore]
        public int? DT_CONSUMER_TYPE_EC_REPAYMENT_PERCENTAGE
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.CONSUMER_TYPE_REPAYMENT_RULE);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_TYPE_REPAYMENT_RULE, value);
            }
        }

        [XmlIgnore]
        public bool? DT_CONSUMER_TYPE_PREPAID_ENABLED
        {
            get
            {
                return DataList.TryGetNullableValue<bool>(DataType.CONSUMER_TYPE_PREPAID_ENABLED);
            }
            set
            {
                DataList.SetValue(DataType.CONSUMER_TYPE_PREPAID_ENABLED, value);
            }
        }

        [XmlIgnore]
        public double DT_EMERGENCY_CREDIT_TOPUP_VALUE
        {
            get
            {
                return DataList.TryGetValue<double>(DataType.EMERGENCY_CREDIT_TOPUP_VALUE);
            }
            set
            {
                DataList.SetValue(DataType.EMERGENCY_CREDIT_TOPUP_VALUE, value);
            }
        }


        [XmlIgnore]
        public int DT_RECOMMENDED_TOPUP_DAYS
        {
            get
            {
                return DataList.TryGetValue<int>(DataType.RECOMMENDED_TOPUP_DAYS);
            }
            set
            {
                DataList.SetValue(DataType.RECOMMENDED_TOPUP_DAYS, value);
            }
        }

        [XmlIgnore]
        public int DT_RECOMMENDED_TOPUP_BEFORE_EMPTY_DAYS
        {
            get { return DataList.TryGetValue<int>(DataType.RECOMMENDED_TOPUP_BEFORE_EMPTY_DAYS); }
            set { DataList.SetValue(DataType.RECOMMENDED_TOPUP_BEFORE_EMPTY_DAYS, value); }
        }

        [XmlIgnore]
        public int DT_CONSUMPTION_DAILY_AVG_WEIGHT_PERCENT
        {
            get { return DataList.TryGetValue<int>(DataType.CONSUMPTION_DAILY_AVG_WEIGHT_PERCENT); }
            set { DataList.SetValue(DataType.CONSUMPTION_DAILY_AVG_WEIGHT_PERCENT, value); }
        }

        [XmlIgnore]
        public double DT_CONSUMPTION_DAILY_MIN
        {
            get { return DataList.TryGetValue<double>(DataType.CONSUMPTION_DAILY_MIN); }
            set { DataList.SetValue(DataType.CONSUMPTION_DAILY_MIN, value); }
        }

        [XmlIgnore]
        public double DT_EMERGENCY_CREDIT_AVAILABLE_AT
        {
            get { return DataList.TryGetValue<double>(DataType.EMERGENCY_CREDIT_AVALIABLE_AT); }
            set { DataList.SetValue(DataType.EMERGENCY_CREDIT_AVALIABLE_AT, value); }
        }

        public double? ConsumptionDailyMin { get; set; }

        public int RecommendedTopupDays { get; set; }

    #endregion

    #region Ctor
        public OpConsumerType()
            : base()
        {
            DataList = new OpDataList<OpConsumerTypeData>();
        }

        public OpConsumerType(DB_CONSUMER_TYPE clone)
            : base(clone)
        {
            DataList = new OpDataList<OpConsumerTypeData>();
        }
    #endregion

    #region ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region ConvertList
        public static List<OpConsumerType> ConvertList(DB_CONSUMER_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpConsumerType> ConvertList(DB_CONSUMER_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			List<OpConsumerType> ret = new List<OpConsumerType>(list.Length);
			list.ToList().ForEach(db_item => ret.Add(new OpConsumerType(db_item)));

			if (loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if (loadCustomData)
			    LoadCustomData(ref ret, dataProvider); // Loads user custom data

			return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpConsumerType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			if (list != null && list.Count > 0)
			{
				Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DESCR);
                var distrList = list.Select(t => t.IdDistributor).Distinct().ToArray();
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(t => t.IdDistributor).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.IdDistributor);
				foreach (var loop in list)
				{
					if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                    loop.Distributor = DistributorDict.TryGetValue(loop.IdDistributor);
				}
			}
		}
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpConsumerType> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IConsumerTypeDataTypes != null)
            {
                List<int> idConsumerType = list.Select(l => l.IdConsumerType).ToList();

                List<OpConsumerTypeData> data = dataProvider.GetConsumerTypeDataFilter(IdConsumerType: idConsumerType.ToArray(), IdDataType: dataProvider.IConsumerTypeDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (data != null && data.Count > 0)
                {
                    foreach (var loop in list)
                        loop.DataList.AddRange(data.Where(d => d.IdConsumerType == loop.IdConsumerType));
                }
            }
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpConsumerType> Members
        public bool Equals(OpConsumerType other)
        {
            if (other == null)
                return false;
            return this.IdConsumerType.Equals(other.IdConsumerType);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpConsumerType)
                return this.IdConsumerType == ((OpConsumerType)obj).IdConsumerType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpConsumerType left, OpConsumerType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpConsumerType left, OpConsumerType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdConsumerType.GetHashCode();
        }
    #endregion
    }
#endif
}