using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpTransmissionStatus : DB_TRANSMISSION_STATUS, IComparable, IEquatable<OpTransmissionStatus>
    {
    #region Properties
            public int IdTransmissionStatus { get { return this.ID_TRANSMISSION_STATUS;} set {this.ID_TRANSMISSION_STATUS = value;}}
                public string Name { get { return this.NAME;} set {this.NAME = value;}}
                public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpTransmissionStatus()
			:base() {}
		
		public OpTransmissionStatus(DB_TRANSMISSION_STATUS clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
            if (this.Descr != null && !String.IsNullOrWhiteSpace(this.Descr.Description))
                return this.Descr.Description;
            return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpTransmissionStatus> ConvertList(DB_TRANSMISSION_STATUS[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpTransmissionStatus> ConvertList(DB_TRANSMISSION_STATUS[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpTransmissionStatus> ret = new List<OpTransmissionStatus>(list.Length);
			foreach (var loop in list)
			{
				OpTransmissionStatus insert = new OpTransmissionStatus(loop);
				ret.Add(insert);
			}
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpTransmissionStatus> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			//Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);
						//loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				
                        }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpTransmissionStatus> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTransmissionStatus)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTransmissionStatus).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpTransmissionStatus> Members
        public bool Equals(OpTransmissionStatus other)
        {
            if (other == null)
				return false;
			return this.IdTransmissionStatus.Equals(other.IdTransmissionStatus);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpTransmissionStatus)
				return this.IdTransmissionStatus == ((OpTransmissionStatus)obj).IdTransmissionStatus;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpTransmissionStatus left, OpTransmissionStatus right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpTransmissionStatus left, OpTransmissionStatus right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdTransmissionStatus.GetHashCode();
		}
    #endregion
    }
#endif
}