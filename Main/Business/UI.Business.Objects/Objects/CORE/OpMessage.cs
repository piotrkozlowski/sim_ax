﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMessage : DB_MESSAGE, IComparable, IEquatable<OpMessage>
    {
    #region Properties
        public long IdMessage { get { return this.ID_MESSAGE; } set { this.ID_MESSAGE = value; } }
        public int IdMessageType { get { return this.ID_MESSAGE_TYPE; } set { this.ID_MESSAGE_TYPE = value; } }
        public long? IdMessageParent { get { return this.ID_MESSAGE_PARENT; } set { this.ID_MESSAGE_PARENT = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public string MessageContent { get { return this.MESSAGE_CONTENT; } set { this.MESSAGE_CONTENT = value; } }
        public bool HasAttachment { get { return this.HAS_ATTACHMENT; } set { this.HAS_ATTACHMENT = value; } }
        public int? Points { get { return this.POINTS; } set { this.POINTS = value; } }
        public string Hierarchy { get { return this.HIERARCHY; } set { this.HIERARCHY = value; } }
        public DateTime InsertDate { get { return this.INSERT_DATE; } set { this.INSERT_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpMessageData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpMessage()
            : base() 
        {
            DataList = new OpDataList<OpMessageData>();
        }

        public OpMessage(DB_MESSAGE clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpMessageData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.MESSAGE_CONTENT;
        }
    #endregion

    #region	ConvertList
        public static List<OpMessage> ConvertList(DB_MESSAGE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMessage> ConvertList(DB_MESSAGE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpMessage> ret = new List<OpMessage>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMessage(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMessage> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpMessage> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0 && dataProvider.IMessageDataTypes != null)
            {
                List<long> idMessage = list.Select(l => l.IdMessage).ToList();

                if (dataProvider.IMessageDataTypes.Count > 0)
                {
                    List<OpMessageData> data = dataProvider.GetMessageDataFilter(IdMessage: idMessage.ToArray(), IdDataType: dataProvider.IMessageDataTypes.ToArray());
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<long, OpMessage> messageDataDict = list.ToDictionary<OpMessage, long>(l => l.IdMessage);
                        foreach (var dataValue in data)
                        {
                            messageDataDict[dataValue.IdMessage].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMessage)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMessage).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpMessage> Members
        public bool Equals(OpMessage other)
        {
            if (other == null)
                return false;
            return this.IdMessage.Equals(other.IdMessage);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMessage)
                return this.IdMessage == ((OpMessage)obj).IdMessage;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMessage left, OpMessage right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMessage left, OpMessage right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMessage.GetHashCode();
        }
    #endregion
    }
#endif
}