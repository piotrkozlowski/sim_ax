using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAlarmDefGroup : DB_ALARM_DEF_GROUP, IComparable, IEquatable<OpAlarmDefGroup>
    {
    #region Properties
        public long IdAlarmDef { get { return this.ID_ALARM_DEF; } set { this.ID_ALARM_DEF = value; } }
        public int IdAlarmGroup { get { return this.ID_ALARM_GROUP; } set { this.ID_ALARM_GROUP = value; } }
        public int IdTransmissionType
        {
            get { return this.ID_TRANSMISSION_TYPE; }
            set { this.OldIdTransmissionType = this.ID_TRANSMISSION_TYPE; this.ID_TRANSMISSION_TYPE = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; }
        }
        public int ConfirmLevel
        {
            get { return this.CONFIRM_LEVEL; }
            set { this.CONFIRM_LEVEL = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; }
        }
        public int ConfirmTimeout
        {
            get { return this.CONFIRM_TIMEOUT; }
            set { this.CONFIRM_TIMEOUT = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; }
        }
    #endregion

    #region	Navigation Properties
        private OpAlarmDef _AlarmDef;
        public OpAlarmDef AlarmDef { get { return this._AlarmDef; } set { this._AlarmDef = value; this.ID_ALARM_DEF = (value == null) ? 0 : (long)value.ID_ALARM_DEF; } }
        private OpAlarmGroup _AlarmGroup;
        public OpAlarmGroup AlarmGroup { get { return this._AlarmGroup; } set { this._AlarmGroup = value; this.IdAlarmGroup = (value == null) ? 0 : (int)value.IdAlarmGroup; } }
        private OpTransmissionType _TransmissionType;
        public OpTransmissionType TransmissionType { get { return this._TransmissionType; } set { this._TransmissionType = value; this.IdTransmissionType = (value == null) ? 0 : (int)value.IdTransmissionType; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
        public int OldIdTransmissionType { get; set; }
    #endregion

    #region	Ctor
        public OpAlarmDefGroup()
            : base() { this.OpState = OpChangeState.New; }

        public OpAlarmDefGroup(DB_ALARM_DEF_GROUP clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "AlarmDefGroup [" + IdAlarmDef.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpAlarmDefGroup> ConvertList(DB_ALARM_DEF_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmDefGroup> ConvertList(DB_ALARM_DEF_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAlarmDefGroup> ret = new List<OpAlarmDefGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmDefGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmDefGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpAlarmDef> AlarmDefDict = dataProvider.GetAlarmDefFilter(true, false,list.Select(l => l.ID_ALARM_DEF).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_DEF);
                Dictionary<int, OpAlarmGroup> AlarmGroupDict = dataProvider.GetAlarmGroupFilter(true, false,list.Select(l => l.ID_ALARM_GROUP).Distinct().ToArray(), IdDistributor: new int[0]).ToDictionary(l => l.ID_ALARM_GROUP);
                Dictionary<int, OpTransmissionType> TransmissionTypeDict = dataProvider.GetTransmissionType(list.Select(l => l.ID_TRANSMISSION_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_TYPE);

                foreach (var loop in list)
                {
                    OpChangeState stateTemp = loop.OpState;
                    loop.AlarmDef = AlarmDefDict.TryGetValue(loop.IdAlarmDef);
                    loop.AlarmGroup = AlarmGroupDict.TryGetValue(loop.ID_ALARM_GROUP);
                    loop.TransmissionType = TransmissionTypeDict.TryGetValue(loop.ID_TRANSMISSION_TYPE);
                    loop.OpState = stateTemp;
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmDefGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmDefGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmDefGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpAlarmDefGroup> Members
        public bool Equals(OpAlarmDefGroup other)
        {
            if (other == null)
                return false;
            return this.IdAlarmDef.Equals(other.IdAlarmDef) && this.IdAlarmGroup.Equals(other.IdAlarmGroup);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmDefGroup)
                return this.IdAlarmDef == ((OpAlarmDefGroup)obj).IdAlarmDef && this.IdAlarmGroup == ((OpAlarmDefGroup)obj).IdAlarmGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmDefGroup left, OpAlarmDefGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmDefGroup left, OpAlarmDefGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmDef.GetHashCode();
        }
    #endregion
    }
#endif
}