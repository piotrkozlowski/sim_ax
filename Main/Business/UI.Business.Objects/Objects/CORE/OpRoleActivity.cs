using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpRoleActivity : DB_ROLE_ACTIVITY, IComparable, IEquatable<OpRoleActivity>, IOpChangeState
    {
    #region Properties
        [XmlIgnore]
        public long IdRoleActivity { get { return this.ID_ROLE_ACTIVITY; } set { if (this.ID_ROLE_ACTIVITY != value) { this.ID_ROLE_ACTIVITY = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdRole { get { return this.ID_ROLE; } set { if (this.ID_ROLE != value) { this.ID_ROLE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdActivity { get { return this.ID_ACTIVITY; } set { if (this.ID_ACTIVITY != value) { this.ID_ACTIVITY = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public object ReferenceValue { get { return this.REFERENCE_VALUE; } set { if (!OpDataUtil.ValueEquals(this.REFERENCE_VALUE, value)) { this.REFERENCE_VALUE = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public bool Deny { get { return this.DENY; } set { if (this.DENY != value) { this.DENY = value; if (this.OpState != OpChangeState.New && this.OpState != OpChangeState.Delete) this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpRole _Role;
        public OpRole Role { get { return this._Role; } set { this._Role = value; this.ID_ROLE = (value == null) ? 0 : (int)value.ID_ROLE; } }
        private OpActivity _Activity;
        public OpActivity Activity { get { return this._Activity; } set { this._Activity = value; this.ID_ACTIVITY = (value == null) ? 0 : (int)value.ID_ACTIVITY; } }
    #endregion

    #region	Custom Properties
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
        public bool Allow { get { return !Deny; } set { Deny = !value; } }
    #endregion

    #region	Ctor
        public OpRoleActivity()
            : base()
        {
            OpState = OpChangeState.New;
        }

        public OpRoleActivity(DB_ROLE_ACTIVITY clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "RoleActivity [" + IdRoleActivity.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpRoleActivity> ConvertList(DB_ROLE_ACTIVITY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRoleActivity> ConvertList(DB_ROLE_ACTIVITY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpRoleActivity> ret = new List<OpRoleActivity>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRoleActivity(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRoleActivity> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpRole> RoleDict = dataProvider.GetRole(list.Select(l => l.ID_ROLE).Distinct().ToArray()).ToDictionary(l => l.ID_ROLE);
                Dictionary<int, OpActivity> ActivityDict = dataProvider.GetActivity(list.Select(l => l.ID_ACTIVITY).Distinct().ToArray()).ToDictionary(l => l.ID_ACTIVITY);

                foreach (var loop in list)
                {
                    loop.Role = RoleDict.TryGetValue(loop.ID_ROLE);
                    loop.Activity = ActivityDict.TryGetValue(loop.ID_ACTIVITY).Clone(dataProvider);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRoleActivity> list, IDataProvider dataProvider)
        {
            foreach (OpRoleActivity loop in list)
            {
                if (loop.Activity != null)
                {
                    loop.Activity.ReferenceValue = loop.ReferenceValue;
                    loop.Activity.Deny = loop.Deny;
                    loop.Activity.IdRoleActivity = loop.IdRoleActivity;
                    loop.Activity.IdOperatorActivity = null;
                    if(loop.Role != null)
                        loop.Activity.IdModule = loop.Role.IdModule;
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRoleActivity)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRoleActivity).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpRoleActivity> Members
        public bool Equals(OpRoleActivity other)
        {
            if (other == null)
                return false;
            return this.IdRoleActivity.Equals(other.IdRoleActivity);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRoleActivity)
                return this.IdRoleActivity == ((OpRoleActivity)obj).IdRoleActivity;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRoleActivity left, OpRoleActivity right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRoleActivity left, OpRoleActivity right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRoleActivity.GetHashCode();
        }
    #endregion
    }
#endif
}