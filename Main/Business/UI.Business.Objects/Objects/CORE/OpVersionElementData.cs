using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionElementData : DB_VERSION_ELEMENT_DATA, IComparable, IEquatable<OpVersionElementData>, IOpChangeState, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdVersionElementData { get { return this.ID_VERSION_ELEMENT_DATA; } set { if (this.ID_VERSION_ELEMENT_DATA != value) { this.ID_VERSION_ELEMENT_DATA = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_DATA, value, this); } } }
        public int IdVersionElement { get { return this.ID_VERSION_ELEMENT; } set { if (this.ID_VERSION_ELEMENT != value) { this.ID_VERSION_ELEMENT = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT, value, this); } } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { if (this.ID_DATA_TYPE != value) { this.ID_DATA_TYPE = OpDataUtil.SetNewValue(this.ID_DATA_TYPE, value, this); } } }
        //public int IndexNbr { get { return this.INDEX_NBR; } set { if (this.INDEX_NBR != value) { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } } }
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object Value { get { return this.VALUE; } set { if (this.VALUE != value) { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } } }
    #endregion

    #region	Navigation Properties
		private OpVersionElement _VersionElement;
		public OpVersionElement VersionElement { get { return this._VersionElement; } set { this._VersionElement = value; this.ID_VERSION_ELEMENT = (value == null)? 0 : (int)value.ID_VERSION_ELEMENT; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public long IdData { get { return IdVersionElementData; } set { IdVersionElementData = value; } }
        public object ReferencedObject { get; set; }
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpVersionElementData()
			:base() {}
		
		public OpVersionElementData(DB_VERSION_ELEMENT_DATA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
			return "VersionElementData [" + IdVersionElementData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionElementData> ConvertList(DB_VERSION_ELEMENT_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionElementData> ConvertList(DB_VERSION_ELEMENT_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionElementData> ret = new List<OpVersionElementData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionElementData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionElementData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpVersionElement> VersionElementDict = dataProvider.GetVersionElement(list.Select(l => l.ID_VERSION_ELEMENT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_ELEMENT);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.VersionElement = VersionElementDict.TryGetValue(loop.ID_VERSION_ELEMENT);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionElementData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionElementData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionElementData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionElementData> Members
        public bool Equals(OpVersionElementData other)
        {
            if (other == null)
				return false;
			return this.IdVersionElementData.Equals(other.IdVersionElementData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionElementData)
				return this.IdVersionElementData == ((OpVersionElementData)obj).IdVersionElementData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionElementData left, OpVersionElementData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionElementData left, OpVersionElementData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionElementData.GetHashCode();
		}
    #endregion

    #region IOpDataProvider

    #region Clone

        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpVersionElementData clone = new OpVersionElementData();
            clone.IdData = this.IdData;
            clone.IdVersionElement = this.IdVersionElement;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_VERSION_ELEMENT_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

    #endregion

    #region AssignReferences

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion

    #endregion
    }
#endif
}