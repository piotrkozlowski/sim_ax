using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionHistory : DB_ACTION_HISTORY, IComparable, IEquatable<OpActionHistory>
    {
    #region Properties
        public long IdActionHistory { get { return this.ID_ACTION_HISTORY; } set { this.ID_ACTION_HISTORY = value; } }
        public long IdAction { get { return this.ID_ACTION; } set { this.ID_ACTION = value; } }
        public int IdActionStatus { get { return this.ID_ACTION_STATUS; } set { this.ID_ACTION_STATUS = value; } }
        public DateTime? StartTime { get { return this.START_TIME; } set { this.START_TIME = value; } }
        public DateTime? EndTime { get { return this.END_TIME; } set { this.END_TIME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        public long? IdActionHistoryData { get { return this.ID_ACTION_HISTORY_DATA; } set { this.ID_ACTION_HISTORY_DATA = value; } }
        public int? IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        public string Assembly { get { return this.ASSEMBLY; } set { this.ASSEMBLY = value; } }
    #endregion

    #region	Navigation Properties
        private OpAction _Action;
        public OpAction Action { get { return this._Action; } set { this._Action = value; this.ID_ACTION = (value == null) ? 0 : (long)value.ID_ACTION; } }
        private OpActionStatus _ActionStatus;
        public OpActionStatus ActionStatus { get { return this._ActionStatus; } set { this._ActionStatus = value; this.ID_ACTION_STATUS = (value == null) ? 0 : (int)value.ID_ACTION_STATUS; } }
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpActionHistoryData _ActionHistoryData;
        public OpActionHistoryData ActionHistoryData { get { return this._ActionHistoryData; } set { this._ActionHistoryData = value; this.ID_ACTION_HISTORY_DATA = (value == null) ? null : (long?)value.ID_ACTION_HISTORY_DATA; } }
        private OpModule _Module;
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null) ? null : (int?)value.ID_MODULE; } }
    #endregion

    #region	Custom Properties

        //This field is to store converted action description string if conversion took place
        private String _DescriptionString;
        public String DescriptionString
        {
            get
            {
                if (String.IsNullOrEmpty(_DescriptionString))
                    return null;
                return _DescriptionString;
            }
            set
            {
                _DescriptionString = value;
            }
        }
    #endregion

    #region	Ctor
        public OpActionHistory()
            : base() { }

        public OpActionHistory(DB_ACTION_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ActionHistory [" + IdActionHistory.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpActionHistory> ConvertList(DB_ACTION_HISTORY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpActionHistory> ConvertList(DB_ACTION_HISTORY[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActionHistory> ret = new List<OpActionHistory>(list.Length);
            foreach (var loop in list)
            {
                OpActionHistory insert = new OpActionHistory(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionHistory> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpAction> ActionDict = dataProvider.GetAction(list.Select(l => l.ID_ACTION).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION);
                Dictionary<int, OpActionStatus> ActionStatusDict = dataProvider.GetActionStatus(list.Select(l => l.ID_ACTION_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_STATUS);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                //Dictionary<long, OpActionHistoryData> ActionHistoryDataDict = dataProvider.GetActionHistoryData(list.Where(l => l.ID_ACTION_HISTORY_DATA.HasValue).Select(l => l.ID_ACTION_HISTORY_DATA.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_HISTORY_DATA);
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_MODULE);
                foreach (var loop in list)
                {
                    loop.Action = ActionDict.TryGetValue(loop.ID_ACTION);
                    loop.ActionStatus = ActionStatusDict.TryGetValue(loop.ID_ACTION_STATUS);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);

                    //this is many to many relation - no dictionary can be used!
                    //if (loop.ID_ACTION_HISTORY_DATA.HasValue)
                    //loop.ActionHistoryData = ActionHistoryDataDict.TryGetValue(loop.ID_ACTION_HISTORY_DATA.Value);
                    if (loop.ID_MODULE.HasValue)
                        loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionHistory> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionHistory> Members
        public bool Equals(OpActionHistory other)
        {
            if (other == null)
                return false;
            return this.IdActionHistory.Equals(other.IdActionHistory);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActionHistory)
                return this.IdActionHistory == ((OpActionHistory)obj).IdActionHistory;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActionHistory left, OpActionHistory right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionHistory left, OpActionHistory right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActionHistory.GetHashCode();
        }
    #endregion
    }
#endif

}