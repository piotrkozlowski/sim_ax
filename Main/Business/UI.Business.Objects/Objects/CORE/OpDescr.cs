using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE.Schema;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    [XmlRoot("Descr")]
    public class OpDescr : DB_DESCR, IComparable, IOpChangeState, IReferenceType
    {
    #region Properties
        [XmlAttribute("IdDescr")]
        public long IdDescr { get { return this.ID_DESCR; } set { if (this.ID_DESCR != value) { this.ID_DESCR = value; this.OpState = OpChangeState.Modified; } } }
        [XmlAttribute("IdLanguage")]
        public int IdLanguage { get { return this.ID_LANGUAGE; } set { if (this.ID_LANGUAGE != value) { this.ID_LANGUAGE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlAttribute("Description")]
        public string Description { get { return this.DESCRIPTION; } set { if (this.DESCRIPTION != value) { this.DESCRIPTION = value; this.OpState = OpChangeState.Modified; } } }
        [XmlAttribute("ExtendedDescription")]
        public string ExtendedDescription { get { return this.EXTENDED_DESCRIPTION; } set { if (this.EXTENDED_DESCRIPTION != value) { this.EXTENDED_DESCRIPTION = value; this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpLanguage _Language;
        [XmlIgnore]
        public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.IdLanguage = (value == null) ? 0 : (int)value.ID_LANGUAGE; } }      
    #endregion

    #region	Custom Properties
        [XmlIgnore]
        public OpChangeState OpState { get; set; }
        private List<OpDescr> _AllLanguagesDescription = new List<OpDescr>();
        public List<OpDescr> AllLanguagesDescription { get { return this._AllLanguagesDescription; } set { this._AllLanguagesDescription = value; } }
    #endregion

    #region	Ctor
        public OpDescr()
            : base()
        {
            OpState = OpChangeState.New;
        }

        public OpDescr(DB_DESCR clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.Description;
        }
    #endregion

    #region	ConvertList
        public static List<OpDescr> ConvertList(DB_DESCR[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDescr> ConvertList(DB_DESCR[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDescr> ret = new List<OpDescr>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDescr(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDescr> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpLanguage> LanguageDict = dataProvider.GetLanguage(list.Select(l => l.ID_LANGUAGE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LANGUAGE);

                foreach (var loop in list)
                {
                    loop.Language = LanguageDict.TryGetValue(loop.ID_LANGUAGE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDescr> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDescr)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDescr).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDescr> Members
        public bool Equals(OpDescr other)
        {
            if (other == null)
                return false;
            return this.IdDescr.Equals(other.IdDescr) && this.IdLanguage.Equals(other.IdLanguage);
            //dodany warunek Language
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDescr)
                return (this.IdDescr == ((OpDescr)obj).IdDescr && this.IdLanguage == ((OpDescr)obj).IdLanguage);
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDescr left, OpDescr right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDescr left, OpDescr right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDescr.GetHashCode() ^ IdLanguage.GetHashCode();
        }
    #endregion

    #region implicit operator XsdDescr
        public static implicit operator OpDescr(XsdDescr xsdDescr)
        {
            OpDescr descr = new OpDescr();
            descr.IdDescr = xsdDescr.IdDescr;
            descr.IdLanguage = xsdDescr.IdLanguage;
            descr.Description = xsdDescr.Description;
            descr.ExtendedDescription = xsdDescr.ExtendedDescription;
            return descr;
        }

        public static implicit operator XsdDescr(OpDescr descr)
        {
            XsdDescr xsdDescr = new XsdDescr();
            xsdDescr.IdDescr = descr.IdDescr;
            xsdDescr.IdLanguage = descr.IdLanguage;
            xsdDescr.Description = descr.Description;
            xsdDescr.ExtendedDescription = descr.ExtendedDescription;
            return xsdDescr;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdDescr;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}