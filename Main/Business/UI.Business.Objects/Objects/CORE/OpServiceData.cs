﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceData : DB_SERVICE_DATA, IComparable, IEquatable<OpServiceData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdServiceData { get { return this.ID_SERVICE_DATA; } set { this.ID_SERVICE_DATA = value; } }
        public int IdService { get { return this.ID_SERVICE; } set { this.ID_SERVICE = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpService _Service;
        public OpService Service { get { return this._Service; } set { this._Service = value; this.ID_SERVICE = (value == null) ? 0 : (int)value.ID_SERVICE; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpServiceData()
            : base() { }

        public OpServiceData(DB_SERVICE_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceData [" + IdServiceData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceData> ConvertList(DB_SERVICE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceData> ConvertList(DB_SERVICE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceData> ret = new List<OpServiceData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceData> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
              //  Dictionary<int, OpService> ServiceDict = dataProvider.GetService(list.Select(l => l.ID_SERVICE).Distinct().ToArray()).ToDictionary(l => l.ID_SERVICE);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                 //   loop.Service = ServiceDict.TryGetValue(loop.ID_SERVICE);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceData> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceData> Members
        public bool Equals(OpServiceData other)
        {
            if (other == null)
                return false;
            return this.IdServiceData.Equals(other.IdServiceData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceData)
                return this.IdServiceData == ((OpServiceData)obj).IdServiceData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceData left, OpServiceData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceData left, OpServiceData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdServiceData; } set { IdServiceData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpServiceData clone = new OpServiceData();
            clone.IdData = this.IdData;
            clone.IdService = this.IdService;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_SERVICE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}