using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpFaultCode : DB_FAULT_CODE, IComparable, IEquatable<OpFaultCode>
    {
    #region Properties
        public int IdFaultCode { get { return this.ID_FAULT_CODE; } set { this.ID_FAULT_CODE = value; } }
        public int IdFaultCodeType { get { return this.ID_FAULT_CODE_TYPE; } set { this.ID_FAULT_CODE_TYPE = value; } }
        public int? IdFaultCodeParent { get { return this.ID_FAULT_CODE_PARENT; } set { this.ID_FAULT_CODE_PARENT = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdClientDescr { get { return this.ID_CLIENT_DESCR; } set { this.ID_CLIENT_DESCR = value; } }
        public long? IdCompanyDescr { get { return this.ID_COMPANY_DESCR; } set { this.ID_COMPANY_DESCR = value; } }
        public int Priority { get { return this.PRIORITY; } set { this.PRIORITY = value; } }
        public string FaultCodeHierarchy { get { return this.FAULT_CODE_HIERARCHY; } set { this.FAULT_CODE_HIERARCHY = value; } }
    #endregion

    #region	Navigation Properties
        private OpFaultCodeType _FaultCodeType;
        public OpFaultCodeType FaultCodeType { get { return this._FaultCodeType; } set { this._FaultCodeType = value; this.ID_FAULT_CODE_TYPE = (value == null) ? 0 : (int)value.ID_FAULT_CODE_TYPE; } }
    #endregion

    #region	Custom Properties

        public OpDataList<OpFaultCodeData> DataList { get; set; }

        public bool IsBigFault
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.FAULT_CODES_BIG_FAULT);
            }
            set
            {
                DataList.SetValue(DataType.FAULT_CODES_BIG_FAULT, value);
            }
        }

        public OpDescr _clientDescrPL;
        public OpDescr ClientDescriptionPL
        {
            get
            {
                return _clientDescrPL;
            }
        }
        public OpDescr _companyDescrPL;
        public OpDescr CompanyDescriptionPL
        {
            get
            {
                return _companyDescrPL;
            }
        }
        public OpDescr _clientDescrEN;
        public OpDescr ClientDescriptionEN
        {
            get
            {
                return _clientDescrEN;
            }
        }
        public OpDescr _companyDescrEN;
        public OpDescr CompanyDescriptionEN
        {
            get
            {
                return _companyDescrEN;
            }
        }

    #endregion

    #region	Ctor
        public OpFaultCode()
            : base() 
        {
            DataList = new OpDataList<OpFaultCodeData>();
        }

        public OpFaultCode(DB_FAULT_CODE clone)
            : base(clone)
        {
            DataList = new OpDataList<OpFaultCodeData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpFaultCode> ConvertList(DB_FAULT_CODE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpFaultCode> ConvertList(DB_FAULT_CODE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpFaultCode> ret = new List<OpFaultCode>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpFaultCode(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpFaultCode> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpFaultCodeType> FaultCodeTypeDict = dataProvider.GetFaultCodeType(list.Select(l => l.ID_FAULT_CODE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_FAULT_CODE_TYPE);
                List<long> IDDescrList = new List<long>();
                IDDescrList.AddRange(list.Select(l => l.ID_CLIENT_DESCR.HasValue ? l.ID_CLIENT_DESCR.Value : 0).Distinct().ToList());
                IDDescrList.AddRange(list.Select(l => l.ID_COMPANY_DESCR.HasValue ? l.ID_COMPANY_DESCR.Value : 0).Distinct().ToArray());
                IDDescrList.RemoveAll(v => v == 0);
                Dictionary<long, OpDescr> FaultCodeDescriptionPL = dataProvider.GetDescr(IDDescrList.ToArray(), true, Enums.Language.Polish).ToDictionary(l => l.ID_DESCR);
                Dictionary<long, OpDescr> FaultCodeDescriptionEN = dataProvider.GetDescr(IDDescrList.ToArray(), true, Enums.Language.English).ToDictionary(l => l.ID_DESCR);

                foreach (var loop in list)
                {
                    loop.FaultCodeType = FaultCodeTypeDict.TryGetValue(loop.ID_FAULT_CODE_TYPE);
                    loop._clientDescrPL = loop.ID_CLIENT_DESCR.HasValue ? FaultCodeDescriptionPL.TryGetValue(loop.ID_CLIENT_DESCR.Value) : null;
                    loop._companyDescrPL = loop.ID_COMPANY_DESCR.HasValue ? FaultCodeDescriptionPL.TryGetValue(loop.ID_COMPANY_DESCR.Value) : null;
                    loop._clientDescrEN = loop.ID_CLIENT_DESCR.HasValue ? FaultCodeDescriptionEN.TryGetValue(loop.ID_CLIENT_DESCR.Value) : null;
                    loop._companyDescrEN = loop.ID_COMPANY_DESCR.HasValue ? FaultCodeDescriptionEN.TryGetValue(loop.ID_COMPANY_DESCR.Value) : null;

                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpFaultCode> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0 && dataProvider.IFaultCodeDataTypes != null)
            {
                List<int> idFaultCode = list.Select(l => l.IdFaultCode).ToList();

                if (dataProvider.IFaultCodeDataTypes.Count > 0)
                {
                    List<OpFaultCodeData> data = dataProvider.GetFaultCodeDataFilter(IdFaultCode: idFaultCode.ToArray(), IdDataType: dataProvider.IFaultCodeDataTypes.ToArray());
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpFaultCode> faultCodeDataDict = list.ToDictionary<OpFaultCode, int>(l => l.IdFaultCode);
                        foreach (var dataValue in data)
                        {
                            faultCodeDataDict[dataValue.IdFaultCode].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpFaultCode)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpFaultCode).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpFaultCode> Members
        public bool Equals(OpFaultCode other)
        {
            if (other == null)
                return false;
            return this.IdFaultCode.Equals(other.IdFaultCode);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpFaultCode)
                return this.IdFaultCode == ((OpFaultCode)obj).IdFaultCode;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpFaultCode left, OpFaultCode right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpFaultCode left, OpFaultCode right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdFaultCode.GetHashCode();
        }
    #endregion
    }
#endif
}