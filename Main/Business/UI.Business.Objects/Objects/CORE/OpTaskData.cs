using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpTaskData : DB_TASK_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpTaskData>
    {
        #region Properties
        [XmlIgnore]
        public long IdTaskData { get { return this.ID_TASK_DATA; } set { this.ID_TASK_DATA = value; } }
        [XmlIgnore]
        public int IdTask { get { return this.ID_TASK; } set { this.ID_TASK = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
        private OpTask _Task;
        [XmlIgnore]
        public OpTask Task { get { return this._Task; } set { this._Task = value; this.ID_TASK = (value == null) ? 0 : (int)value.ID_TASK; } }
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdTaskData; } set { IdTaskData = value; } }
        [DataMember]
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
        public OpTaskData()
			: base() { this.OpState = OpChangeState.New; }

        public OpTaskData(DB_TASK_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }

        public OpTaskData(OpTaskData clone)
            : base(clone) 
        {
            this.OpState = clone.OpState; 
        }
    #endregion

    #region ToString
        public override string ToString()
        {
            return "TaskData [" + IdTaskData.ToString() + "]";
        }
    #endregion

    #region ConvertList
        public static List<OpTaskData> ConvertList(DB_TASK_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpTaskData> ConvertList(DB_TASK_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpTaskData> ret = new List<OpTaskData>(list.Length);
            
            Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, loadNavigationProperties: loadNavigationProperties).ToDictionary(l => l.ID_DATA_TYPE);
            list.ToList().ForEach(db_object =>
            {
                OpTaskData newObject = new OpTaskData(db_object);
                newObject.DataType = DataTypeDict.TryGetValue(newObject.ID_DATA_TYPE);
                try
                {
                    // sdudzik - probujemy poprawic wartosc, zeby byla wlasciwego typu
                    // CELOWO (!) uzywamy VALUE, a nie Value - nie zmieniac tego!
                    // VALUE uzywane do tego, zeby raz poprawic typ rzeczywistej wartosci, Value to tylko get/set
                    newObject.VALUE = OpDataUtil.CorrectValueType(newObject.DataType, newObject.VALUE);
                }
                catch
                {
                    // korekcja sie nie udala, oznaczamy wartosc jako bledna
                    newObject.OpState = OpChangeState.Incorrect;
                }
                ret.Add(newObject);
            });
            
            /*
            foreach (var loop in list)
            {
                OpTaskData insert = new OpTaskData(loop);
                if (loadNavigationProperties)
                {
                    //insert.Task = dataProvider.GetTask(loop.ID_TASK);
                    insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
                }

                ret.Add(insert);
            }
            */
            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTaskData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTaskData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTaskData).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpTaskData> Members
        public bool Equals(OpTaskData other)
        {
            if (other == null)
                return false;
            return this.IdTaskData.Equals(other.IdTaskData);
        }
    #endregion

    #region IOpDataProvider

    #region Clone
        /// <summary>
		/// if dataProvider==null NavigationProperties will not be assigned 
		/// </summary>
		public object Clone(IDataProvider dataProvider)
		{
			OpTaskData clone = new OpTaskData();
			clone.IdData = this.IdData;
			clone.IdTask = this.IdTask;
			clone.IdDataType = this.IdDataType;
			clone.Index = this.Index;
			clone.Value = this.Value;

			if (dataProvider != null)
				clone = ConvertList(new DB_TASK_DATA[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;

			return clone;
		}
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.Task = dataProvider.GetTask(this.ID_TASK);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #endregion

    #region override Equals
        public override bool Equals(object obj)
		{
			if (obj is OpTaskData)
				return this.IdData == ((OpTaskData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpTaskData left, OpTaskData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpTaskData left, OpTaskData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}