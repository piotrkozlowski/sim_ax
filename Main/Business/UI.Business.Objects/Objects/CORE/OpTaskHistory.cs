using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpTaskHistory : DB_TASK_HISTORY, IComparable
    {
    #region Properties
        public int IdTaskHistory { get { return this.ID_TASK_HISTORY; } set { this.ID_TASK_HISTORY = value; } }
        public int IdTask { get { return this.ID_TASK; } set { this.ID_TASK = value; } }
        public int IdTaskStatus { get { return this.ID_TASK_STATUS; } set { this.ID_TASK_STATUS = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public DateTime StartDate { get { return this.START_DATE; } set { this.START_DATE = value; } }
        public DateTime? EndDate { get { return this.END_DATE; } set { this.END_DATE = value; } }
        public string Notes { get { return this.NOTES; } set { this.NOTES = value; } }
    #endregion

    #region	Navigation Properties
        private OpTask _Task;
        public OpTask Task { get { return this._Task; } set { this._Task = value; this.ID_TASK = (value == null) ? 0 : (int)value.ID_TASK; } }
        private OpTaskStatus _TaskStatus;
        public OpTaskStatus TaskStatus { get { return this._TaskStatus; } set { this._TaskStatus = value; this.ID_TASK_STATUS = (value == null) ? 0 : (int)value.ID_TASK_STATUS; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpTaskHistory()
            : base() { }

        public OpTaskHistory(DB_TASK_HISTORY clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NOTES;
        }
    #endregion

    #region	ConvertList
        public static List<OpTaskHistory> ConvertList(DB_TASK_HISTORY[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpTaskHistory> ConvertList(DB_TASK_HISTORY[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTaskHistory> ret = new List<OpTaskHistory>(list.Length);
            foreach (var loop in list)
            {
                OpTaskHistory insert = new OpTaskHistory(loop);

                if (loadNavigationProperties)
                {

                    //insert.Task = dataProvider.GetTask(loop.ID_TASK);

                    insert.TaskStatus = dataProvider.GetTaskStatus(new int[]{ loop.ID_TASK_STATUS }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();

                    insert.Operator = dataProvider.GetOperator(new int[]{ loop.ID_OPERATOR }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel,commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTaskHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTaskHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTaskHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}