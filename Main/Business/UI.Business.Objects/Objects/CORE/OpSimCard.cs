using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;
using System.Xml.Serialization;
using System.Runtime.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpSimCard : DB_SIM_CARD, IComparable, IEquatable<OpSimCard>, IOpChangeState, IOpDynamic, IOpDynamicProperty<OpSimCard>, IOpObject<OpSimCardData>
    {
        #region Properties
        public int IdSimCard
        {
            get { return this.ID_SIM_CARD; }
            set { if (this.ID_SIM_CARD != value) { this.ID_SIM_CARD = OpDataUtil.SetNewValue(this.ID_SIM_CARD, value, this); } }
        }
        public int IdDistributor
        {
            get { return this.ID_DISTRIBUTOR;}
            set { if (this.ID_DISTRIBUTOR != value) { this.ID_DISTRIBUTOR = OpDataUtil.SetNewValue(this.ID_DISTRIBUTOR, value, this); } }
        }
        public string SerialNbr
        {
            get { return this.SERIAL_NBR;}
            set { if (this.SERIAL_NBR != value) { this.SERIAL_NBR = OpDataUtil.SetNewValue(this.SERIAL_NBR, value, this); } }
        }
        public string Phone
        {
            get { return this.PHONE;}
            set { if (this.PHONE != value) { this.PHONE = OpDataUtil.SetNewValue(this.PHONE, value, this); } }
        }
        public string Pin
        {
            get { return this.PIN;}
            set { if (this.PIN != value) { this.PIN = OpDataUtil.SetNewValue(this.PIN, value, this); } }
        }
        public string Puk
        {
            get { return this.PUK;}
            set { if (this.PUK != value) { this.PUK = OpDataUtil.SetNewValue(this.PUK, value, this); } }
        }
        public int? MobileNetworkCode
        {
            get { return this.MOBILE_NETWORK_CODE;}
            set { if (this.MOBILE_NETWORK_CODE != value) { this.MOBILE_NETWORK_CODE = OpDataUtil.SetNewValue(this.MOBILE_NETWORK_CODE, value, this); } }
        }
        public string Ip
        {
            get { return this.IP;}
            set { if (this.IP != value) { this.IP = OpDataUtil.SetNewValue(this.IP, value, this); } }
        }
        public string ApnLogin
        {
            get { return this.APN_LOGIN;}
            set { if (this.APN_LOGIN != value) { this.APN_LOGIN = OpDataUtil.SetNewValue(this.APN_LOGIN, value, this); } }
        }
        public string ApnPassword
        {
            get { return this.APN_PASSWORD;}
            set { if (this.APN_PASSWORD != value) { this.APN_PASSWORD = OpDataUtil.SetNewValue(this.APN_PASSWORD, value, this); } }
        }
        #endregion

        #region	Navigation Properties
		private OpDistributor _Distributor;
		public OpDistributor Distributor { get { return this._Distributor; } set
        {
            this._Distributor = OpDataUtil.SetNewValue(this._Distributor, value, this);
            this.ID_DISTRIBUTOR = (value == null)? 0 : (int)value.ID_DISTRIBUTOR;
        } }
        #endregion

        #region	Custom Properties

        [XmlIgnore, DataMember]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;

        private List<OpLocation> _Locations;
        public List<OpLocation> Locations
        {
            get { return _Locations; }
            set { this._Locations = OpDataUtil.SetNewValue(this._Locations, value, this); }
        }

        public OpLocation Location
        {
            get
            {
                if (this.Locations == null || this.Locations.Count == 0)
                    return null;
                return this.Locations.First();
            }
        }

        private List<OpDevice> _Devices;
        public List<OpDevice> Devices
        {
            get { return _Devices; }
            set { this._Devices = OpDataUtil.SetNewValue(this._Devices, value, this); }
        }

        public OpDevice Device
        {
            get
            {
                if (this.Devices == null || this.Devices.Count == 0)
                    return null;
                return this.Devices.First();
            }
        }

        #endregion
		
        #region	Ctor
		public OpSimCard()
			:base() 
        {
            DataList = new OpDataList<OpSimCardData>();
            Devices = new List<OpDevice>();
            Locations = new List<OpLocation>();
            DynamicValues = new Dictionary<string, object>();
        }
		
		public OpSimCard(DB_SIM_CARD clone)
			:base(clone) 
        {
            DataList = new OpDataList<OpSimCardData>();
            Devices = new List<OpDevice>();
            Locations = new List<OpLocation>();
            DynamicValues = new Dictionary<string, object>();
        }
        #endregion
		
        #region	ToString
		public override string ToString()
		{
				return this.SERIAL_NBR;
		}
        #endregion

        #region	ConvertList
		public static List<OpSimCard> ConvertList(DB_SIM_CARD[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpSimCard> ConvertList(DB_SIM_CARD[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
            List<OpSimCard> ret = new List<OpSimCard>(list.Length);
            list.ToList().ForEach(db_object => ret.Add(new OpSimCard(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            
			return ret;
		}
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpSimCard> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);

                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }        
        #endregion
		
        #region LoadCustomData
        public static void LoadCustomData(ref List<OpSimCard> list, IDataProvider dataProvider, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.ISimCardDataTypes != null)
            {
                List<int> idSimCard = list.Select(l => l.IdSimCard).ToList();
                List<OpSimCardData> data = new List<OpSimCardData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetSimCardDataFilter(IdSimCard: idSimCard.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.ISimCardDataTypes != null && dataProvider.ISimCardDataTypes.Count > 0)
                    data = dataProvider.GetSimCardDataFilter(IdSimCard: idSimCard.ToArray(), IdDataType: dataProvider.ISimCardDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpSimCard> simCardDict = list.ToDictionary(l => l.IdSimCard);
                    foreach (var dataValue in data)
                    {
                        simCardDict[dataValue.IdSimCard].DataList.Add(dataValue);
                    }
                }
            }
		}
        #endregion
		
        #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSimCard)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSimCard).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpSimCard> Members
        public bool Equals(OpSimCard other)
        {
            if (other == null)
                return false;
            return this.IdSimCard.Equals(other.IdSimCard);
        }
        #endregion

        #region IOpChangeState Members
        public OpChangeState OpState { get; set; }
        #endregion

        #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdSimCard; }
        }
        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }
        [XmlIgnore]
        OpSimCard IOpDynamicProperty<OpSimCard>.Owner
        {
            get { return this; }
        }
        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }
        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IOpObject
        [XmlIgnore]
        public object IdObject
        {
            get { return IdSimCard; }
            set { IdSimCard = Convert.ToInt32(value); }
        }
        
        public OpDataList<OpSimCardData> DataList { get; set; }
        #endregion
    }
#endif
}