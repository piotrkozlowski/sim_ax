using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpRole : DB_ROLE, IComparable, IEquatable<OpRole>, IReferenceType, IOpChangeState
    {
        #region Properties
        [XmlIgnore]
        public int IdRole { get { return this.ID_ROLE; } set { this.ID_ROLE = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public int? IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpModule _Module;
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null) ? null : (int?)value.ID_MODULE; } }
        [DataMember]
        private OpDescr _Descr;
        [XmlIgnore]
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties

        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Ctor
        public OpRole()
            : base() { }

        public OpRole(DB_ROLE clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.ToString();
            return this.NAME;
        }
        #endregion

    #region	ConvertList
        public static List<OpRole> ConvertList(DB_ROLE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRole> ConvertList(DB_ROLE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpRole> ret = new List<OpRole>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRole(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRole> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_MODULE);
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if (loop.ID_MODULE.HasValue)
                        loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value);
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRole> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRole)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRole).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpRole> Members
        public bool Equals(OpRole other)
        {
            if (other == null)
                return false;
            return this.IdRole.Equals(other.IdRole);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRole)
                return this.IdRole == ((OpRole)obj).IdRole;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRole left, OpRole right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRole left, OpRole right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRole.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdRole;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}