using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpWorkOrderJci : DB_WORK_ORDER_JCI, IComparable, IEquatable<OpWorkOrderJci>
    {
    #region Properties
        public int IdWorkOrderJci { get { return this.ID_WORK_ORDER_JCI; } set { this.ID_WORK_ORDER_JCI = value; } }
        public string Wonum { get { return this.WONUM; } set { this.WONUM = value; } }
        public DateTime Sentdatetime { get { return this.SENTDATETIME; } set { this.SENTDATETIME = value; } }
        public string Wodesc { get { return this.WODESC; } set { this.WODESC = value; } }
        public string Ivrcode { get { return this.IVRCODE; } set { this.IVRCODE = value; } }
        public string Subcasdesc { get { return this.SUBCASDESC; } set { this.SUBCASDESC = value; } }
        public string Problemdesc { get { return this.PROBLEMDESC; } set { this.PROBLEMDESC = value; } }
        public int Priority { get { return this.PRIORITY; } set { this.PRIORITY = value; } }
        public string Typedesc { get { return this.TYPEDESC; } set { this.TYPEDESC = value; } }
        public string Statusdesc { get { return this.STATUSDESC; } set { this.STATUSDESC = value; } }
        public DateTime Reportdatetime { get { return this.REPORTDATETIME; } set { this.REPORTDATETIME = value; } }
        public DateTime Rfdatetime { get { return this.RFDATETIME; } set { this.RFDATETIME = value; } }
        public string Ponumber { get { return this.PONUMBER; } set { this.PONUMBER = value; } }
        public string Supplierid { get { return this.SUPPLIERID; } set { this.SUPPLIERID = value; } }
        public string Suppliername { get { return this.SUPPLIERNAME; } set { this.SUPPLIERNAME = value; } }
        public string Siteid { get { return this.SITEID; } set { this.SITEID = value; } }
        public string Sitename { get { return this.SITENAME; } set { this.SITENAME = value; } }
        public string Sitephone { get { return this.SITEPHONE; } set { this.SITEPHONE = value; } }
        public string Equipid { get { return this.EQUIPID; } set { this.EQUIPID = value; } }
        public string Equipdesc { get { return this.EQUIPDESC; } set { this.EQUIPDESC = value; } }
        public string DownTime { get { if (this.DOWNTIME == null)return ""; else return (String.Equals("Y", this.DOWNTIME.ToUpper()) ? "Yes" : "No"); } set { this.DOWNTIME = value; } }
        public string Lumpsum { get { if (this.LUMPSUM == null) return ""; else return (String.Equals("Y", this.LUMPSUM.ToUpper()) ? "Yes" : "No"); } set { this.LUMPSUM = value; } }
        public string DisclaimerText { get { return this.DISCLAIMER_TEXT; } set { this.DISCLAIMER_TEXT = value; } }
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpWorkOrderJci()
            : base() { }

        public OpWorkOrderJci(DB_WORK_ORDER_JCI clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.WONUM;
        }
    #endregion

    #region	ConvertList
        public static List<OpWorkOrderJci> ConvertList(DB_WORK_ORDER_JCI[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpWorkOrderJci> ConvertList(DB_WORK_ORDER_JCI[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpWorkOrderJci> ret = new List<OpWorkOrderJci>(list.Length);
            foreach (var loop in list)
            {
                OpWorkOrderJci insert = new OpWorkOrderJci(loop);

                if (loadNavigationProperties)
                {




















                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpWorkOrderJci> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpWorkOrderJci)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpWorkOrderJci).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpWorkOrderJci> Members
        public bool Equals(OpWorkOrderJci other)
        {
            if (other == null)
                return false;
            return this.IdWorkOrderJci.Equals(other.IdWorkOrderJci);
        }
    #endregion

        public string GetHTMLEmailString()
        {
            try
            {
                StringBuilder strBuild = new StringBuilder();
                strBuild.AppendFormat("<b>WONUM</b>: {0}<br>", this.WONUM);
                strBuild.AppendFormat("<b>SENTDATETIME</b>: {0}<br>", this.SENTDATETIME);
                strBuild.AppendFormat("<b>WODESC</b>: {0}<br>", this.WODESC);
                strBuild.AppendFormat("<b>IVRCODE</b>: {0}<br>", this.IVRCODE);
                strBuild.AppendFormat("<b>SUBCASDESC</b>: {0}<br>", this.SUBCASDESC);
                strBuild.AppendFormat("<b>PROBLEMDESC</b>: {0}<br>", this.PROBLEMDESC);
                strBuild.AppendFormat("<b>PRIORITY</b>: {0}<br>", this.PRIORITY);
                strBuild.AppendFormat("<b>TYPEDESC</b>: {0}<br>", this.TYPEDESC);
                strBuild.AppendFormat("<b>LUMPSUM</b>: {0}<br>", this.LUMPSUM);
                strBuild.AppendFormat("<b>REPORTDATETIME</b>: {0}<br>", this.REPORTDATETIME);
                strBuild.AppendFormat("<b>RFDATETIME</b>: {0}<br>", this.RFDATETIME);
                strBuild.AppendFormat("<b>SITEID</b>: {0}<br>", this.SITEID);
                strBuild.AppendFormat("<b>SITENAME</b>: {0}<br>", this.SITENAME);
                strBuild.AppendFormat("<b>SITEPHONE</b>: {0}<br>", this.SITEPHONE);
                strBuild.AppendFormat("<b>EQUIPID</b>: {0}<br>", this.EQUIPID);
                strBuild.AppendFormat("<b>EQUIPDESC</b>: {0}<br>", this.EQUIPDESC);
                strBuild.AppendFormat("<b>DOWNTIME</b>: {0}<br>", this.DOWNTIME);
                strBuild.AppendFormat("<b>DISCLAIMER_TEXT</b>: {0}<br>", this.DISCLAIMER_TEXT);
                return strBuild.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
#endif
}