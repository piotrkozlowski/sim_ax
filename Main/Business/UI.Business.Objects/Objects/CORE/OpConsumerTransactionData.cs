using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.Data.DB.CORE;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConsumerTransactionData : DB_CONSUMER_TRANSACTION_DATA, IComparable, IEquatable<OpConsumerTransactionData>
    {
    #region Properties
        public long IdConsumerTransactionData { get { return this.ID_CONSUMER_TRANSACTION_DATA;} set {this.ID_CONSUMER_TRANSACTION_DATA = value;}}
        public long IdConsumerTransaction { get { return this.ID_CONSUMER_TRANSACTION;} set {this.ID_CONSUMER_TRANSACTION = value;}}
        public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
        public int IndexNbr { get { return this.INDEX_NBR;} set {this.INDEX_NBR = value;}}
        public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
    #endregion

    #region	Navigation Properties
		private OpConsumerTransaction _ConsumerTransaction;
				public OpConsumerTransaction ConsumerTransaction { get { return this._ConsumerTransaction; } set { this._ConsumerTransaction = value; this.ID_CONSUMER_TRANSACTION = (value == null)? 0 : (long)value.ID_CONSUMER_TRANSACTION; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConsumerTransactionData()
			:base() {}
		
		public OpConsumerTransactionData(DB_CONSUMER_TRANSACTION_DATA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ConsumerTransactionData [" + IdConsumerTransactionData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpConsumerTransactionData> ConvertList(DB_CONSUMER_TRANSACTION_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConsumerTransactionData> ConvertList(DB_CONSUMER_TRANSACTION_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpConsumerTransactionData> ret = new List<OpConsumerTransactionData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConsumerTransactionData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConsumerTransactionData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpConsumerTransaction> ConsumerTransactionDict = dataProvider.GetConsumerTransactionCORE(list.Select(l => l.ID_CONSUMER_TRANSACTION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONSUMER_TRANSACTION);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
						loop.ConsumerTransaction = ConsumerTransactionDict.TryGetValue(loop.ID_CONSUMER_TRANSACTION); 
		loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConsumerTransactionData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConsumerTransactionData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConsumerTransactionData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConsumerTransactionData> Members
        public bool Equals(OpConsumerTransactionData other)
        {
            if (other == null)
				return false;
			return this.IdConsumerTransactionData.Equals(other.IdConsumerTransactionData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConsumerTransactionData)
				return this.IdConsumerTransactionData == ((OpConsumerTransactionData)obj).IdConsumerTransactionData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConsumerTransactionData left, OpConsumerTransactionData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConsumerTransactionData left, OpConsumerTransactionData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConsumerTransactionData.GetHashCode();
		}
    #endregion
    }
#endif
}