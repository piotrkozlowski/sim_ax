using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpConfigurationProfileDeviceType : DB_CONFIGURATION_PROFILE_DEVICE_TYPE, IComparable, IEquatable<OpConfigurationProfileDeviceType>
    {
    #region Properties
            public long IdConfigurationProfile { get { return this.ID_CONFIGURATION_PROFILE;} set {this.ID_CONFIGURATION_PROFILE = value;}}
                public int IdDeviceType { get { return this.ID_DEVICE_TYPE;} set {this.ID_DEVICE_TYPE = value;}}
    #endregion

    #region	Navigation Properties
		private OpConfigurationProfile _ConfigurationProfile;
				public OpConfigurationProfile ConfigurationProfile { get { return this._ConfigurationProfile; } set { this._ConfigurationProfile = value; this.ID_CONFIGURATION_PROFILE = (value == null)? 0 : (long)value.ID_CONFIGURATION_PROFILE; } }
				private OpDeviceType _DeviceType;
				public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.ID_DEVICE_TYPE = (value == null)? 0 : (int)value.ID_DEVICE_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpConfigurationProfileDeviceType()
			:base() {}
		
		public OpConfigurationProfileDeviceType(DB_CONFIGURATION_PROFILE_DEVICE_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "ConfigurationProfileDeviceType [" + IdConfigurationProfile.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpConfigurationProfileDeviceType> ConvertList(DB_CONFIGURATION_PROFILE_DEVICE_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpConfigurationProfileDeviceType> ConvertList(DB_CONFIGURATION_PROFILE_DEVICE_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpConfigurationProfileDeviceType> ret = new List<OpConfigurationProfileDeviceType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpConfigurationProfileDeviceType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpConfigurationProfileDeviceType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Select(l => l.ID_DEVICE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE);			
				foreach (var loop in list)
				{
						loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpConfigurationProfileDeviceType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpConfigurationProfileDeviceType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpConfigurationProfileDeviceType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpConfigurationProfileDeviceType> Members
        public bool Equals(OpConfigurationProfileDeviceType other)
        {
            if (other == null)
				return false;
			return this.IdConfigurationProfile.Equals(other.IdConfigurationProfile);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpConfigurationProfileDeviceType)
				return this.IdConfigurationProfile == ((OpConfigurationProfileDeviceType)obj).IdConfigurationProfile;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpConfigurationProfileDeviceType left, OpConfigurationProfileDeviceType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpConfigurationProfileDeviceType left, OpConfigurationProfileDeviceType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdConfigurationProfile.GetHashCode();
		}
    #endregion
    }
#endif
}