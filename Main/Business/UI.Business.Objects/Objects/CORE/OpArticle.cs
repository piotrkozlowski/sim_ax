using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpArticle : DB_ARTICLE, IComparable, IEquatable<OpArticle>, IReferenceType
    {
    #region Properties

        public long IdArticle { get { return this.ID_ARTICLE; } set { this.ID_ARTICLE = value; } }
        public long? ExternalId { get { return this.EXTERNAL_ID; } set { this.EXTERNAL_ID = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        public bool IsUnique { get { return this.IS_UNIQUE; } set { this.IS_UNIQUE = value; } }
        public bool? Visible { get { return this.VISIBLE; } set { this.VISIBLE = value; } }
        public bool IsAiut { get { return this.IS_AIUT; } set { this.IS_AIUT = value; } }

        public OpChangeState OpState { get; set; }

    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties

        public string GridLookUpEditString { get { return this.ToString(); } }
        public OpDataList<OpArticleData> DataList { get; set; }

    #endregion

    #region	Ctor
        public OpArticle()
            : base() 
        {
            DataList = new OpDataList<OpArticleData>();
            this.OpState = OpChangeState.New;
        }

        public OpArticle(DB_ARTICLE clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpArticleData>();
            this.OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpArticle> ConvertList(DB_ARTICLE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpArticle> ConvertList(DB_ARTICLE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpArticle> ret = new List<OpArticle>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpArticle(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpArticle> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {

                foreach (var loop in list)
                {
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpArticle> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IArticleDataTypes != null)
            {
                List<long> idArticle = list.Select(l => l.IdArticle).ToList();

                List<OpArticleData> data = dataProvider.GetArticleDataFilter(IdArticle: idArticle.ToArray(), IdDataType: dataProvider.IArticleDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (data != null && data.Count > 0)
                {
                    foreach (var article in list)
                    {
                        article.DataList.AddRange(data.Where(d => d.IdArticle == article.IdArticle));
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpArticle)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpArticle).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpArticle> Members
        public bool Equals(OpArticle other)
        {
            if (other == null)
                return false;
            return this.IdArticle.Equals(other.IdArticle);
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdArticle;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpArticle)
                return this.IdArticle == ((OpArticle)obj).IdArticle;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpArticle left, OpArticle right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpArticle left, OpArticle right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdArticle.GetHashCode();
        }
    #endregion
    }
#endif
}