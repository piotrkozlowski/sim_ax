using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpRoleGroup : DB_ROLE_GROUP, IComparable, IEquatable<OpRoleGroup>, IOpChangeState
    {
    #region Properties
        public int IdRoleParent { get { return this.ID_ROLE_PARENT; } set { if (this.ID_ROLE_PARENT != value) { this.ID_ROLE_PARENT = value; this.OpState = OpChangeState.Modified; } } }
        public int IdRole { get { return this.ID_ROLE; } set { if (this.ID_ROLE != value) { this.ID_ROLE = value; this.OpState = OpChangeState.Modified; } } }
    #endregion

    #region	Navigation Properties
        private OpRole _Role;
        public OpRole Role { get { return this._Role; } set { this._Role = value; this.ID_ROLE = (value == null) ? 0 : (int)value.ID_ROLE; } }
        private OpRole _ParentRole;
        public OpRole ParentRole { get { return this._ParentRole; } set { this._ParentRole = value; this.ID_ROLE_PARENT = (value == null) ? 0 : (int)value.ID_ROLE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpRoleGroup()
            : base()
        {
            OpState = OpChangeState.New;
        }

        public OpRoleGroup(DB_ROLE_GROUP clone)
            : base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "RoleGroup [" + IdRoleParent.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpRoleGroup> ConvertList(DB_ROLE_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpRoleGroup> ConvertList(DB_ROLE_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpRoleGroup> ret = new List<OpRoleGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpRoleGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpRoleGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpRole> RoleDict = dataProvider.GetRole(list.Select(l => l.ID_ROLE).Distinct().ToArray()).ToDictionary(l => l.ID_ROLE);
                Dictionary<int, OpRole> ParentRoleDict = dataProvider.GetRole(list.Select(l => l.ID_ROLE_PARENT).Distinct().ToArray()).ToDictionary(l => l.ID_ROLE);
                foreach (var loop in list)
                {
                    loop.Role = RoleDict.TryGetValue(loop.ID_ROLE);
                    loop.ParentRole = ParentRoleDict.TryGetValue(loop.ID_ROLE_PARENT);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpRoleGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRoleGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRoleGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpRoleGroup> Members
        public bool Equals(OpRoleGroup other)
        {
            if (other == null)
                return false;
            return this.IdRoleParent.Equals(other.IdRoleParent) && this.IdRole.Equals(other.IdRole);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpRoleGroup)
                return (this.IdRoleParent == ((OpRoleGroup)obj).IdRoleParent && this.IdRole == ((OpRoleGroup)obj).IdRole);
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpRoleGroup left, OpRoleGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpRoleGroup left, OpRoleGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdRole.GetHashCode() ^ IdRoleParent.GetHashCode();
        }
    #endregion
    }
#endif
}