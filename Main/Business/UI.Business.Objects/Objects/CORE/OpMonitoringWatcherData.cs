using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMonitoringWatcherData : DB_MONITORING_WATCHER_DATA, IComparable, IEquatable<OpMonitoringWatcherData>
    {
        #region Properties
        public int IdMonitoringWatcherData { get { return this.ID_MONITORING_WATCHER_DATA; } set { this.ID_MONITORING_WATCHER_DATA = value; } }
        public int IdMonitoringWatcher { get { return this.ID_MONITORING_WATCHER; } set { this.ID_MONITORING_WATCHER = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int Index { get { return this.INDEX; } set { this.INDEX = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        #endregion

        #region	Navigation Properties
        private OpMonitoringWatcher _MonitoringWatcher;
        public OpMonitoringWatcher MonitoringWatcher { get { return this._MonitoringWatcher; } set { this._MonitoringWatcher = value; this.ID_MONITORING_WATCHER = (value == null) ? 0 : (int)value.ID_MONITORING_WATCHER; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region	Custom Properties

        public OpChangeState OpState { get; set; }

        #endregion

        #region	Ctor
        public OpMonitoringWatcherData()
            : base() { this.OpState = OpChangeState.New; }

        public OpMonitoringWatcherData(DB_MONITORING_WATCHER_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "MonitoringWatcherData [" + IdMonitoringWatcherData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpMonitoringWatcherData> ConvertList(DB_MONITORING_WATCHER_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpMonitoringWatcherData> ConvertList(DB_MONITORING_WATCHER_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMonitoringWatcherData> ret = new List<OpMonitoringWatcherData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpMonitoringWatcherData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMonitoringWatcherData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpMonitoringWatcher> MonitoringWatcherDict = dataProvider.GetMonitoringWatcher(list.Select(l => l.ID_MONITORING_WATCHER).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_MONITORING_WATCHER);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.MonitoringWatcher = MonitoringWatcherDict.TryGetValue(loop.ID_MONITORING_WATCHER);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpMonitoringWatcherData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMonitoringWatcherData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMonitoringWatcherData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpMonitoringWatcherData> Members
        public bool Equals(OpMonitoringWatcherData other)
        {
            if (other == null)
                return false;
            return this.IdMonitoringWatcherData.Equals(other.IdMonitoringWatcherData);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMonitoringWatcherData)
                return this.IdMonitoringWatcherData == ((OpMonitoringWatcherData)obj).IdMonitoringWatcherData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMonitoringWatcherData left, OpMonitoringWatcherData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMonitoringWatcherData left, OpMonitoringWatcherData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMonitoringWatcherData.GetHashCode();
        }
        #endregion
    }
#endif
}