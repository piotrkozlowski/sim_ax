using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceDriverMemoryMap : DB_DEVICE_DRIVER_MEMORY_MAP, IComparable, IEquatable<OpDeviceDriverMemoryMap>
    {
    #region Properties
        public int IdDeviceDriverMemoryMap { get { return this.ID_DEVICE_DRIVER_MEMORY_MAP; } set { this.ID_DEVICE_DRIVER_MEMORY_MAP = value; } }
        public int IdDeviceDriver { get { return this.ID_DEVICE_DRIVER; } set { this.ID_DEVICE_DRIVER = value; } }
        public string FileName { get { return this.FILE_NAME; } set { this.FILE_NAME = value; } }
        public string FileVersion { get { return this.FILE_VERSION; } set { this.FILE_VERSION = value; } }
    #endregion

    #region	Navigation Properties
        private OpDeviceDriver _DeviceDriver;
        public OpDeviceDriver DeviceDriver { get { return this._DeviceDriver; } set { this._DeviceDriver = value; this.ID_DEVICE_DRIVER = (value == null) ? 0 : (int)value.ID_DEVICE_DRIVER; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpDeviceDriverMemoryMap()
            : base() { }

        public OpDeviceDriverMemoryMap(DB_DEVICE_DRIVER_MEMORY_MAP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.FILE_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceDriverMemoryMap> ConvertList(DB_DEVICE_DRIVER_MEMORY_MAP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceDriverMemoryMap> ConvertList(DB_DEVICE_DRIVER_MEMORY_MAP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceDriverMemoryMap> ret = new List<OpDeviceDriverMemoryMap>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceDriverMemoryMap(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceDriverMemoryMap> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDeviceDriver> DeviceDriverDict = dataProvider.GetDeviceDriver(list.Select(l => l.ID_DEVICE_DRIVER).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_DRIVER);

                foreach (var loop in list)
                {
                    loop.DeviceDriver = DeviceDriverDict.TryGetValue(loop.ID_DEVICE_DRIVER);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceDriverMemoryMap> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceDriverMemoryMap)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceDriverMemoryMap).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceDriverMemoryMap> Members
        public bool Equals(OpDeviceDriverMemoryMap other)
        {
            if (other == null)
                return false;
            return this.IdDeviceDriverMemoryMap.Equals(other.IdDeviceDriverMemoryMap);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceDriverMemoryMap)
                return this.IdDeviceDriverMemoryMap == ((OpDeviceDriverMemoryMap)obj).IdDeviceDriverMemoryMap;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceDriverMemoryMap left, OpDeviceDriverMemoryMap right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceDriverMemoryMap left, OpDeviceDriverMemoryMap right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceDriverMemoryMap.GetHashCode();
        }
    #endregion
    }
#endif
}