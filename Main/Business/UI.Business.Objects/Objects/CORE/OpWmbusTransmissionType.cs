using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpWmbusTransmissionType : DB_WMBUS_TRANSMISSION_TYPE, IComparable, IReferenceType, IEquatable<OpWmbusTransmissionType>
    {
    #region Properties
            [XmlIgnore]
            public long IdWmbusTransmissionType { get { return this.ID_WMBUS_TRANSMISSION_TYPE;} set {this.ID_WMBUS_TRANSMISSION_TYPE = value;}}
            [XmlIgnore]
            public string Name { get { return this.NAME;} set {this.NAME = value;}}
            [XmlIgnore]
            public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
    #endregion

    #region	Navigation Properties
		private OpDescr _Descr;
				public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpWmbusTransmissionType()
			:base() {}
		
		public OpWmbusTransmissionType(DB_WMBUS_TRANSMISSION_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpWmbusTransmissionType> ConvertList(DB_WMBUS_TRANSMISSION_TYPE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpWmbusTransmissionType> ConvertList(DB_WMBUS_TRANSMISSION_TYPE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpWmbusTransmissionType> ret = new List<OpWmbusTransmissionType>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpWmbusTransmissionType(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpWmbusTransmissionType> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
				foreach (var loop in list)
				{
						if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpWmbusTransmissionType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpWmbusTransmissionType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpWmbusTransmissionType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpWmbusTransmissionType> Members
        public bool Equals(OpWmbusTransmissionType other)
        {
            if (other == null)
				return false;
			return this.IdWmbusTransmissionType.Equals(other.IdWmbusTransmissionType);
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdWmbusTransmissionType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpWmbusTransmissionType)
				return this.IdWmbusTransmissionType == ((OpWmbusTransmissionType)obj).IdWmbusTransmissionType;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpWmbusTransmissionType left, OpWmbusTransmissionType right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpWmbusTransmissionType left, OpWmbusTransmissionType right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdWmbusTransmissionType.GetHashCode();
		}
    #endregion
    }
#endif
}