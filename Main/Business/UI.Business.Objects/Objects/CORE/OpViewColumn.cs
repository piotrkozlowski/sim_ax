﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable, DataContract]
    public class OpViewColumn : DB_VIEW_COLUMN, IComparable, IEquatable<OpViewColumn>, IOpChangeState, IReferenceType
    {
        #region Properties
        public int IdViewColumn { get { return this.ID_VIEW_COLUMN; } set { this.ID_VIEW_COLUMN = value; } }
        public int IdView { get { return this.ID_VIEW; } set { this.ID_VIEW = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Table { get { return this.TABLE; } set { this.TABLE = value; } }
        public bool PrimaryKey { get { return this.PRIMARY_KEY; } set { this.PRIMARY_KEY = value; } }
        public object DefaultValue { get { return this.DEFAULT_VALUE; } set { this.DEFAULT_VALUE = value; } }
        public long? Size { get { return this.SIZE; } set { this.SIZE = value; } }
        public int IdViewColumnSource { get { return this.ID_VIEW_COLUMN_SOURCE; } set { this.ID_VIEW_COLUMN_SOURCE = value; } }
        public int IdViewColumnType { get { return this.ID_VIEW_COLUMN_TYPE; } set { this.ID_VIEW_COLUMN_TYPE = value; } }
        public bool Key { get { return this.KEY; } set { this.KEY = value; } }
        public int? IdKeyViewColumn { get { return this.ID_KEY_VIEW_COLUMN; } set { this.ID_KEY_VIEW_COLUMN = value; } }

        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpView _View;
        public OpView View { get { return this._View; } set { this._View = value; this.ID_VIEW = (value == null) ? 0 : (int)value.ID_VIEW; } }
        [DataMember]
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        [DataMember]
        private OpViewColumnSource _ViewColumnSource;
        public OpViewColumnSource ViewColumnSource { get { return this._ViewColumnSource; } set { this._ViewColumnSource = value; this.ID_VIEW_COLUMN_SOURCE = (value == null) ? 0 : (int)value.ID_VIEW_COLUMN_SOURCE; } }
        [DataMember]
        private OpViewColumnType _ViewColumnType;
        public OpViewColumnType ViewColumnType { get { return this._ViewColumnType; } set { this._ViewColumnType = value; this.ID_VIEW_COLUMN_TYPE = (value == null) ? 0 : (int)value.ID_VIEW_COLUMN_TYPE; } }

        [DataMember]
        public OpDataList<OpViewColumnData> DataList { get; set; }
        #endregion

        #region	Custom Properties

        public string ColumnPrefix
        {
            get
            {
                string[] split = this.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                return String.Join("_", split.Take(split.Count() - 2));
            }
        }
        public long ColumnIdDataType
        {
            get
            {
                return GetColumnIdDataType(this.Name);
            }
        }
        public int ColumnIndexNbr
        {
            get
            {
                return GetColumnIndexNbr(this.Name);
                //string[] split = this.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                //return Convert.ToInt32(split[split.Count() - 1]);
            }
        }

        public string AssociativeTable
        {
            get { return DataList == null ? null : DataList.TryGetValue<string>(Common.DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE); }
        }

        public long? AssociativeTableIdDataType
        {
            get { return DataList == null ? null : DataList.TryGetNullableValue<long>(Common.DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE_ID_DATA_TYPE); }
        }

        public int? AssociativeTableIndexNbr
        {
            get { return DataList == null ? null : DataList.TryGetNullableValue<int>(Common.DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE_INDEX); }
        }

        public object Tag { get; set; }

        #endregion

        #region	Ctor
        public OpViewColumn()
            : base()
        {
            this.DataList = new OpDataList<OpViewColumnData>();
        }

        public OpViewColumn(DB_VIEW_COLUMN clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpViewColumnData>();
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpViewColumn> ConvertList(DB_VIEW_COLUMN[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpViewColumn> ConvertList(DB_VIEW_COLUMN[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpViewColumn> ret = new List<OpViewColumn>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpViewColumn(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes: customDataTypes, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpViewColumn> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpView> ViewDict = dataProvider.GetView(list.Select(l => l.ID_VIEW).Distinct().ToArray()).ToDictionary(l => l.ID_VIEW);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpViewColumnSource> ViewColumnSourceDict = dataProvider.GetViewColumnSource(list.Select(l => l.ID_VIEW_COLUMN_SOURCE).Distinct().ToArray()).ToDictionary(l => l.ID_VIEW_COLUMN_SOURCE);
                Dictionary<int, OpViewColumnType> ViewColumnTypeDict = dataProvider.GetViewColumnType(list.Select(l => l.ID_VIEW_COLUMN_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_VIEW_COLUMN_TYPE);
                foreach (var loop in list)
                {
                    loop.View = ViewDict.TryGetValue(loop.ID_VIEW);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    loop.ViewColumnSource = ViewColumnSourceDict.TryGetValue(loop.ID_VIEW_COLUMN_SOURCE);
                    loop.ViewColumnType = ViewColumnTypeDict.TryGetValue(loop.ID_VIEW_COLUMN_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        public static void LoadCustomData(ref List<OpViewColumn> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> idViewColumn = list.Select(l => l.ID_VIEW_COLUMN).ToList();
                List<OpViewColumnData> data = new List<OpViewColumnData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetViewColumnDataFilter(IdViewColumn: idViewColumn.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.ILocationDataTypes != null && dataProvider.ILocationDataTypes.Count > 0)
                    data = dataProvider.GetViewColumnDataFilter(IdViewColumn: idViewColumn.ToArray(), IdDataType: dataProvider.ILocationDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data.Count > 0)
                {
                    Dictionary<int, OpViewColumn> viewColumnsDict = list.ToDictionary<OpViewColumn, int>(l => l.IdViewColumn);
                    data.ForEach(w => viewColumnsDict[w.IdViewColumn].DataList.Add(w));
                }

            }
        }
        #endregion

        #region GetColumnIdDataType

        public static long GetColumnIdDataType(string columnName)
        {
            string[] split = columnName.Replace("[", "").Replace("]", "").Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            if (split.Length == 1)
                return Convert.ToInt64(split[0]);
            return Convert.ToInt64(split[split.Length - 2]);
        }

        #endregion
        #region GetColumnIndexNbr

        public static int GetColumnIndexNbr(string columnName)
        {
            string[] split = columnName.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            int indexNbr = 0;
            if (split.Length <= 1 || !int.TryParse(split[split.Length - 1], out indexNbr))
                return 0;
            return indexNbr;
        }

        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpViewColumn)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpViewColumn).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpViewColumn> Members
        public bool Equals(OpViewColumn other)
        {
            if (other == null)
                return false;
            return this.IdViewColumn.Equals(other.IdViewColumn);
        }
        #endregion

        #region IReferenceType

        public object GetReferenceKey()
        {
            return IdViewColumn;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpViewColumn)
                return this.IdViewColumn == ((OpViewColumn)obj).IdViewColumn;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpViewColumn left, OpViewColumn right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpViewColumn left, OpViewColumn right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdViewColumn.GetHashCode();
        }
        #endregion
    }
}