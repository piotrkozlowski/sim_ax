﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServicePackage : DB_SERVICE_PACKAGE, IComparable, IEquatable<OpServicePackage>
    {
    #region Properties
        public long IdServicePackage { get { return this.ID_SERVICE_PACKAGE; } set { this.ID_SERVICE_PACKAGE = value; } }
        public int? IdContract { get { return this.ID_CONTRACT; } set { this.ID_CONTRACT = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Descr { get { return this.DESCR; } set { this.DESCR = value; } }
        public bool IsSingle { get { return this.IS_SINGLE; } set { this.IS_SINGLE = value; } }
    #endregion

    #region	Navigation Properties
        private OpContract _Contract;
        public OpContract Contract { get { return this._Contract; } set { this._Contract = value; this.ID_CONTRACT = (value == null) ? 0 : (int)value.ID_CONTRACT; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpServicePackageData> DataList { get; set; }
    #endregion

    #region	Ctor
        public OpServicePackage()
            : base() 
        {
            DataList = new OpDataList<OpServicePackageData>();
        }

        public OpServicePackage(DB_SERVICE_PACKAGE clone)
            : base(clone) 
        {
            DataList = new OpDataList<OpServicePackageData>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpServicePackage> ConvertList(DB_SERVICE_PACKAGE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServicePackage> ConvertList(DB_SERVICE_PACKAGE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServicePackage> ret = new List<OpServicePackage>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServicePackage(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServicePackage> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpContract> ContractDict = dataProvider.GetContract(list.Where(t => t.IdContract != null).Select(l => (int)l.ID_CONTRACT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_CONTRACT);

                foreach (var loop in list)
                {
                    loop.Contract = ContractDict.TryGetValue((int)loop.ID_CONTRACT);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServicePackage> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IContractDataTypes != null)
            {
                List<long> idServicePackage = list.Select(l => l.IdServicePackage).ToList();

                if (dataProvider.IContractDataTypes.Count > 0)
                {
                    List<OpServicePackageData> data1 = dataProvider.GetServicePackageDataFilter(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    OpServicePackageData data2 = dataProvider.GetServicePackageData(1);
                    List<OpServicePackageData> data = dataProvider.GetServicePackageDataFilter(IdServicePackage: idServicePackage.ToArray(), IdDataType: dataProvider.IServicePackageDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<long, OpServicePackage> servicePackageDataDict = list.ToDictionary<OpServicePackage, long>(l => l.IdServicePackage);
                        foreach (var dataValue in data)
                        {
                            servicePackageDataDict[dataValue.IdServicePackage].DataList.Add(dataValue);
                        }
                    }
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServicePackage)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServicePackage).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServicePackage> Members
        public bool Equals(OpServicePackage other)
        {
            if (other == null)
                return false;
            return this.IdServicePackage.Equals(other.IdServicePackage);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServicePackage)
                return this.IdServicePackage == ((OpServicePackage)obj).IdServicePackage;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServicePackage left, OpServicePackage right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServicePackage left, OpServicePackage right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServicePackage.GetHashCode();
        }
    #endregion
    }
#endif
}