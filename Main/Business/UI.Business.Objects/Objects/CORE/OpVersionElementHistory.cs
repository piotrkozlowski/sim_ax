using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionElementHistory : DB_VERSION_ELEMENT_HISTORY, IComparable, IEquatable<OpVersionElementHistory>, IOpChangeState
    {
    #region Properties
        public long IdVersionElementHistory { get { return this.ID_VERSION_ELEMENT_HISTORY; } set { if (this.ID_VERSION_ELEMENT_HISTORY != value) { this.ID_VERSION_ELEMENT_HISTORY = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_HISTORY, value, this); } } }
        public int IdVersionElement { get { return this.ID_VERSION_ELEMENT; } set { if (this.ID_VERSION_ELEMENT != value) { this.ID_VERSION_ELEMENT = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT, value, this); } } }
        public long IdVersion { get { return this.ID_VERSION; } set { if (this.ID_VERSION != value) { this.ID_VERSION = OpDataUtil.SetNewValue(this.ID_VERSION, value, this); } } }
        public DateTime StartTime { get { return this.START_TIME; } set { if (this.START_TIME != value) { this.START_TIME = OpDataUtil.SetNewValue(this.START_TIME, value, this); } } }
        public DateTime? EndTime { get { return this.END_TIME; } set { if (this.END_TIME != value) { this.END_TIME = OpDataUtil.SetNewValue(this.END_TIME, value, this); } } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { if (this.ID_OPERATOR != value) { this.ID_OPERATOR = OpDataUtil.SetNewValue(this.ID_OPERATOR, value, this); } } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
		private OpVersionElement _VersionElement;
		public OpVersionElement VersionElement { get { return this._VersionElement; } set { this._VersionElement = value; this.ID_VERSION_ELEMENT = (value == null)? 0 : (int)value.ID_VERSION_ELEMENT; } }
		private OpVersion _Version;
		public OpVersion Version { get { return this._Version; } set { this._Version = value; this.ID_VERSION = (value == null)? 0 : (long)value.ID_VERSION; } }
		private OpOperator _Operator;
		public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpVersionElementHistory()
			:base() {}
		
		public OpVersionElementHistory(DB_VERSION_ELEMENT_HISTORY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
			return "VersionElementHistory [" + IdVersionElementHistory.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionElementHistory> ConvertList(DB_VERSION_ELEMENT_HISTORY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionElementHistory> ConvertList(DB_VERSION_ELEMENT_HISTORY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionElementHistory> ret = new List<OpVersionElementHistory>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionElementHistory(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionElementHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpVersionElement> VersionElementDict = dataProvider.GetVersionElement(list.Select(l => l.ID_VERSION_ELEMENT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_ELEMENT);
                Dictionary<long, OpVersion> VersionDict = dataProvider.GetVersion(list.Select(l => l.ID_VERSION).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                foreach (var loop in list)
                {
                    loop.VersionElement = VersionElementDict.TryGetValue(loop.ID_VERSION_ELEMENT);
                    loop.Version = VersionDict.TryGetValue(loop.ID_VERSION);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionElementHistory> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionElementHistory)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionElementHistory).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionElementHistory> Members
        public bool Equals(OpVersionElementHistory other)
        {
            if (other == null)
				return false;
			return this.IdVersionElementHistory.Equals(other.IdVersionElementHistory);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionElementHistory)
				return this.IdVersionElementHistory == ((OpVersionElementHistory)obj).IdVersionElementHistory;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionElementHistory left, OpVersionElementHistory right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionElementHistory left, OpVersionElementHistory right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionElementHistory.GetHashCode();
		}
    #endregion
	}
#endif
}