﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
    [Serializable, DataContract]
    public class OpViewColumnData : DB_VIEW_COLUMN_DATA, IComparable, IEquatable<OpViewColumnData>, IOpData, IOpDataProvider
    {
        #region Properties
        public long IdViewColumnData { get { return this.ID_VIEW_COLUMN_DATA; } set { this.ID_VIEW_COLUMN_DATA = value; } }
        public int IdViewColumn { get { return this.ID_VIEW_COLUMN; } set { this.ID_VIEW_COLUMN = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpViewColumn _ViewColumn;
        public OpViewColumn ViewColumn { get { return this._ViewColumn; } set { this._ViewColumn = value; this.ID_VIEW_COLUMN = (value == null) ? 0 : (int)value.ID_VIEW_COLUMN; } }
        [DataMember]
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpViewColumnData()
            : base() { }

        public OpViewColumnData(DB_VIEW_COLUMN_DATA clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "ViewColumnData [" + IdViewColumnData.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpViewColumnData> ConvertList(DB_VIEW_COLUMN_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpViewColumnData> ConvertList(DB_VIEW_COLUMN_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpViewColumnData> ret = new List<OpViewColumnData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpViewColumnData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpViewColumnData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpViewColumn> ViewColumnDict = dataProvider.GetViewColumn(list.Select(l => l.ID_VIEW_COLUMN).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VIEW_COLUMN);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.ViewColumn = ViewColumnDict.TryGetValue(loop.ID_VIEW_COLUMN);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpViewColumnData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion


        #region IOpState
        public OpChangeState OpState { get; set; }

        #endregion

        #region IOpData

        public long IdData { get { return IdViewColumnData; } set { IdViewColumnData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object ReferencedObject { get; set; }

        #endregion

        #region IOpDataProvider

        #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
        #endregion

        #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpViewColumnData clone = new OpViewColumnData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;
            clone.IdViewColumn = this.IdViewColumn;

            if (dataProvider != null)
                clone = ConvertList(new DB_VIEW_COLUMN_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;
            return clone;
        }
        #endregion

        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpViewColumnData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpViewColumnData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region IEquatable<OpViewColumnData> Members
        public bool Equals(OpViewColumnData other)
        {
            if (other == null)
                return false;
            return this.IdViewColumnData.Equals(other.IdViewColumnData);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpViewColumnData)
                return this.IdViewColumnData == ((OpViewColumnData)obj).IdViewColumnData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpViewColumnData left, OpViewColumnData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpViewColumnData left, OpViewColumnData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdViewColumnData.GetHashCode();
        }
        #endregion
    }
}