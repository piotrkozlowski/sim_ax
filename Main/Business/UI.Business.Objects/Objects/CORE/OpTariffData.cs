using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpTariffData : DB_TARIFF_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpTariffData>
    {
    #region Properties
		public long IdTariffData { get { return this.ID_TARIFF_DATA; } set { this.ID_TARIFF_DATA = value; } }
		public int IdTariff { get { return this.ID_TARIFF; } set { this.ID_TARIFF = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
		public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
    #endregion

    #region Navigation Properties
		private OpTariff _Tariff;
		public OpTariff Tariff { get { return this._Tariff; } set { this._Tariff = value; this.ID_TARIFF = (value == null)? 0 : (int)value.ID_TARIFF; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdTariffData; } set { IdTariffData = value; } }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion
		
    #region Ctor
		public OpTariffData()
			: base() { this.OpState = OpChangeState.New; }
		
		public OpTariffData(DB_TARIFF_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion
		
    #region ToString
		public override string ToString()
		{
				return "TariffData [" + IdTariffData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpTariffData> ConvertList(DB_TARIFF_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpTariffData> ConvertList(DB_TARIFF_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpTariffData> ret = new List<OpTariffData>(list.Length);
			foreach (var loop in list)
			{
				OpTariffData insert = new OpTariffData(loop);
				if(loadNavigationProperties)
				{
                    //insert.Tariff = dataProvider.GetTariff(loop.ID_TARIFF); 
					//insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE); 
				}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		public static void LoadCustomData(ref List<OpTariffData> list, IDataProvider dataProvider)
        {
            foreach (var loop in list)
            {
                loop.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
                //loop.Tariff = dataProvider.GetTariff(loop.ID_TARIFF);
            }
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.Tariff = dataProvider.GetTariff(this.ID_TARIFF);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpTariffData clone = new OpTariffData();
            clone.IdData = this.IdData;
            clone.IdTariff = this.IdTariff;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_TARIFF_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTariffData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTariffData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpTariffData> Members
        public bool Equals(OpTariffData other)
        {
            if (other == null)
				return false;
			return this.IdTariffData.Equals(other.IdTariffData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpTariffData)
				return this.IdData == ((OpTariffData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpTariffData left, OpTariffData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpTariffData left, OpTariffData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}