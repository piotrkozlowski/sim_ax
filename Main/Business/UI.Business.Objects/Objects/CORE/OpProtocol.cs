using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpProtocol : DB_PROTOCOL, IComparable, IReferenceType
    {
        #region Properties
        [XmlIgnore]
		public int IdProtocol { get { return this.ID_PROTOCOL;} set {this.ID_PROTOCOL = value;}}
        [XmlIgnore]
		public string Name { get { return this.NAME;} set {this.NAME = value;}}
        [XmlIgnore]
		public int? IdProtocolDriver { get { return this.ID_PROTOCOL_DRIVER;} set {this.ID_PROTOCOL_DRIVER = value;}}
        #endregion

        #region Navigation Properties
		private OpProtocolDriver _ProtocolDriver;
        [XmlIgnore]
		public OpProtocolDriver ProtocolDriver { get { return this._ProtocolDriver; } set { this._ProtocolDriver = value; this.ID_PROTOCOL_DRIVER = (value == null)? null : (int?)value.ID_PROTOCOL_DRIVER; } }
        #endregion

        #region Custom Properties
        #endregion
		
        #region Ctor
		public OpProtocol()
			:base() {}
		
		public OpProtocol(DB_PROTOCOL clone)
			:base(clone) {}
        #endregion
		
        #region ToString
		public override string ToString()
		{
			return this.NAME;
		}
        #endregion

        #region ConvertList
		public static List<OpProtocol> ConvertList(DB_PROTOCOL[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpProtocol> ConvertList(DB_PROTOCOL[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpProtocol> ret = new List<OpProtocol>(list.Length);
			list.ToList().ForEach(db_item => ret.Add(new OpProtocol(db_item)));

			if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpProtocol> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			if (list != null && list.Count > 0)
			{
                Dictionary<int, OpProtocolDriver> ProtocolDriverDict = dataProvider.GetProtocolDriver(list.Where(l => l.ID_PROTOCOL_DRIVER.HasValue).Select(l => l.ID_PROTOCOL_DRIVER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PROTOCOL_DRIVER);
				foreach (var loop in list)
				{
					if (loop.ID_PROTOCOL_DRIVER.HasValue)
						loop.ProtocolDriver = ProtocolDriverDict.TryGetValue(loop.ID_PROTOCOL_DRIVER.Value);
				}
			}
		}
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpProtocol> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
        #endregion
		
        #region IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpProtocol)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpProtocol).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpProtocol)
				return this.IdProtocol == ((OpProtocol)obj).IdProtocol;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpProtocol left, OpProtocol right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpProtocol left, OpProtocol right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdProtocol.GetHashCode();
		}
        #endregion        

        #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdProtocol;
        }

        public object GetReferenceValue()
        {
            return this;
        }
        #endregion
    }
#endif
}