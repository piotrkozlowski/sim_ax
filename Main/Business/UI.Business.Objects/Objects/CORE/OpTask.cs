using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Drawing;
using System.Xml.Serialization;
using System.Data;
using System.Runtime.Serialization;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTask : DB_TASK, IComparable, IOpChangeState, IReferenceType, IOpDynamic, IOpDynamicProperty<OpTask>, IOpDataProperty, IOpObject<OpTaskData>
    {
    #region Properties 
        [XmlIgnore]
        public int IdTask { get { return this.ID_TASK; } set { if (this.ID_TASK != value) { this.ID_TASK = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdTaskType { get { return this.ID_TASK_TYPE; } set { if (this.ID_TASK_TYPE != value) { this.ID_TASK_TYPE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdIssue { get { return this.ID_ISSUE; } set { if (this.ID_ISSUE != value) { this.ID_ISSUE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdTaskGroup { get { return this.ID_TASK_GROUP; } set { if (this.ID_TASK_GROUP != value) { this.ID_TASK_GROUP = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public long? IdPlannedRoute { get { return this.ID_PLANNED_ROUTE; } set { if (this.ID_PLANNED_ROUTE != value) { this.ID_PLANNED_ROUTE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdOperatorRegistering { get { return this.ID_OPERATOR_REGISTERING; } set { if (this.ID_OPERATOR_REGISTERING != value) { this.ID_OPERATOR_REGISTERING = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdOperatorPerformer { get { return this.ID_OPERATOR_PERFORMER; } set { if (this.ID_OPERATOR_PERFORMER != value) { this.ID_OPERATOR_PERFORMER = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdTaskStatus { get { return this.ID_TASK_STATUS; } set { if (this.ID_TASK_STATUS != value) { this.ID_TASK_STATUS = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { if (this.CREATION_DATE != value) { this.CREATION_DATE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public bool? Accepted { get { return this.ACCEPTED; } set { if (this.ACCEPTED != value) { this.ACCEPTED = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdOperatorAccepted { get { return this.ID_OPERATOR_ACCEPTED; } set { if (this.ID_OPERATOR_ACCEPTED != value) { this.ID_OPERATOR_ACCEPTED = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public DateTime? AcceptanceDate { get { return this.ACCEPTANCE_DATE; } set { if (this.ACCEPTANCE_DATE != ((value == null) ? null : (DateTime?)value.Value)) { this.ACCEPTANCE_DATE = (value == null) ? null : (DateTime?)value.Value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string Notes { get { return this.NOTES; } set { if (this.NOTES != value) { this.NOTES = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public DateTime? Deadline 
        { 
            get 
            {
                if (this.DEADLINE == null)
                    return null;
                else
                {
                    if (this.TaskHistory != null)
                    {
                        if (this.TaskHistory.Exists(th => th.IdTaskStatus == (int)OpTaskStatus.Enum.Suspended))
                        {
                            DateTime tmp = new DateTime(DEADLINE.Value.Year, DEADLINE.Value.Month, DEADLINE.Value.Day, DEADLINE.Value.Hour, DEADLINE.Value.Minute, 0);
                            TimeSpan ts;
                            foreach (OpTaskHistory thItem in this.TaskHistory.Where(th => th.IdTaskStatus == (int)OpTaskStatus.Enum.Suspended))
                            {
                                if (thItem.EndDate.HasValue)
                                    ts = thItem.END_DATE.Value - thItem.START_DATE;
                                else
                                    ts = DataProvider.DateTimeNow - thItem.START_DATE;
                                tmp.AddDays(ts.Days);
                                tmp.AddHours(ts.Hours);
                                tmp.AddMinutes(ts.Minutes);
                            }
                            return tmp;
                        }
                        else
                            return DEADLINE.Value;
                    }
                    else
                        return DEADLINE.Value;
                }
            } 
            set { if (this.DEADLINE != ((value == null) ? null : (DateTime?)value.Value)) { this.DEADLINE = (value == null) ? null : (DateTime?)value.Value; this.OpState = OpChangeState.Modified; } }
        }
        [XmlIgnore]
        public long? IdLocation { get { return this.ID_LOCATION; } set { if (this.ID_LOCATION != value) { this.ID_LOCATION = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int? IdActor { get { return this.ID_ACTOR; } set { if (this.ID_ACTOR != value) { this.ID_ACTOR = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public string TopicNumber { get { return this.TOPIC_NUMBER; } set { if (this.TOPIC_NUMBER != value) { this.TOPIC_NUMBER = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int Priority { get { return this.PRIORITY; } set { if (this.PRIORITY != value) { this.PRIORITY = value; this.OpState = OpChangeState.Modified; } } }
        /// <summary>
        /// Use OperationCodeTMQ to get enum
        /// </summary>
        [XmlIgnore]
        public long? OperationCode { get { return this.OPERATION_CODE; } set { if (this.OPERATION_CODE != value) { this.OPERATION_CODE = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public object RowVersion { get { return this.ROW_VERSION; } set { if (this.ROW_VERSION != value) { this.ROW_VERSION = value; this.OpState = OpChangeState.Modified; } } }
        [XmlIgnore]
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { if (this.ID_DISTRIBUTOR != value) { this.ID_DISTRIBUTOR = value; this.OpState = OpChangeState.Modified; } } }

        [DataMember]
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpTaskType _TaskType;
        [XmlIgnore]
        public OpTaskType TaskType { get { return this._TaskType; } set { this._TaskType = value; this.IdTaskType = (value == null) ? 0 : (int)value.ID_TASK_TYPE; } }
        private OpIssue _Issue;
        [CopyableProperty, XmlIgnore]
        public OpIssue Issue { get { return this._Issue; } set { this._Issue = value; this.IdIssue = (value == null) ? null : (int?)value.ID_ISSUE; } }
        private OpTaskGroup _TaskGroup;
        [CopyableProperty, XmlIgnore]
        public OpTaskGroup TaskGroup { get { return this._TaskGroup; } set { this._TaskGroup = value; this.IdTaskGroup = (value == null) ? null : (int?)value.ID_TASK_GROUP; } }
        private OpTaskStatus _TaskStatus;
        [CopyableProperty, XmlIgnore]
        public OpTaskStatus TaskStatus
        {
            get { return this._TaskStatus; }
            set
            {
                //if (value != null && this._TaskStatus != value)
                //    TaskStatusChanged = true;

                this._TaskStatus = value; this.IdTaskStatus = (value == null) ? 0 : (int)value.ID_TASK_STATUS;
            }
        }
        private OpLocation _Location;
        [CopyableProperty, XmlIgnore]
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.IdLocation = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpActor _Actor;
        [CopyableProperty, XmlIgnore]
        public OpActor Actor { get { return this._Actor; } set { this._Actor = value; this.IdActor = (value == null) ? null : (int?)value.ID_ACTOR; } }
        private OpOperator _OperatorRegistering;
        [CopyableProperty, XmlIgnore]
        public OpOperator OperatorRegistering { get { return this._OperatorRegistering; } set { this._OperatorRegistering = value; this.IdOperatorRegistering = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpOperator _OperatorPerformer;
        [CopyableProperty, XmlIgnore]
        public OpOperator OperatorPerformer { get { return this._OperatorPerformer; } set { this._OperatorPerformer = value; this.IdOperatorPerformer = (value == null) ? null : (int?)value.ID_OPERATOR; } }
        private OpRoute _Route;
        [CopyableProperty, XmlIgnore]
        public OpRoute Route { get { return this._Route; } set { this._Route = value; this.IdPlannedRoute = (value == null) ? null : (int?)value.ID_ROUTE; } }
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.IdDistributor = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
        #endregion

        #region	Custom Properties
        //Indicates if the TaskStatus change from the last db query
        //public bool TaskStatusChanged { get; set; }
        [XmlIgnore]
        public DateTime? FitterVisitDate
        {
            get
            {
                return DataList.TryGetNullableValue<DateTime>(DataType.TASK_FITTER_VISIT_DATE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_FITTER_VISIT_DATE, value);
            }
        }
        [XmlIgnore]
        public DateTime? ProtocolSubmitDate
        {
            get
            {
                return DataList.TryGetNullableValue<DateTime>(DataType.TASK_PROTOCOL_SUBMIT_DATE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_PROTOCOL_SUBMIT_DATE, value);
            }
        }
        [XmlIgnore]
        public DateTime? ArrangedFitterVisitDate
        {
            get
            {
                return DataList.TryGetNullableValue<DateTime>(DataType.TASK_ARRANGED_FITTER_VISIT_DATE, 0);
            }
            set
            {
                DataList.SetValue(DataType.TASK_ARRANGED_FITTER_VISIT_DATE, 0, value);
                if (value == null)
                    this.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_DATE).OpState = OpChangeState.Delete;
            }
        }
        [XmlIgnore]
        public string ArrangedFitterVisitNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES, 0);
            }
            set
            {
                DataList.SetValue(DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES, 0, value);
            }
        }
        [XmlIgnore]
        public Enums.TMQObjectServiceAction OperationCodeTMQ
        {
            get
            {
                if (OperationCode.HasValue)
                    return (Enums.TMQObjectServiceAction)OperationCode.Value;
                else
                    return Enums.TMQObjectServiceAction.NONE;
            }
            set
            {
                OperationCode = (long)value;
            }
        }

        [XmlIgnore]
        public bool FitterPaymentDone
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.TASK_FITTER_PAYMENT_DONE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_FITTER_PAYMENT_DONE, value);
            }
        }
        [XmlIgnore]
        public DateTime? FitterPaymentDate
        {
            get
            {
                return DataList.TryGetNullableValue<DateTime>(DataType.TASK_FITTER_PAYMENT_DATE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_FITTER_PAYMENT_DATE, value);
            }
        }
        [XmlIgnore]
        public double PaymentAmount
        {
            get
            {
                return DataList.TryGetValue<double>(DataType.TASK_PAYMENT_AMOUNT);
            }
            set
            {
                DataList.SetValue(DataType.TASK_PAYMENT_AMOUNT, value);
            }
        }
        [XmlIgnore]
        public string PaymentNote
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.TASK_PAYMENT_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_PAYMENT_NOTE, value);
            }
        }
        [XmlIgnore]
        public bool FitterOrderedToPay
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.TASK_FITTER_ORDERED_TO_PAY);
            }
            set
            {
                DataList.SetValue(DataType.TASK_FITTER_ORDERED_TO_PAY, value);
            }
        }

        [XmlIgnore]
        public bool ClientPaymentDone
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.TASK_CLIENT_PAYMENT_DONE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_CLIENT_PAYMENT_DONE, value);
            }
        }
        [XmlIgnore]
        public double ClientPaymentAmount
        {
            get
            {
                return DataList.TryGetValue<double>(DataType.TASK_CLIENT_PAYMENT_AMOUNT);
            }
            set
            {
                DataList.SetValue(DataType.TASK_CLIENT_PAYMENT_AMOUNT, value);
            }
        }
        [XmlIgnore]
        public DateTime? ClientPaymentDate
        {
            get
            {
                DateTime? date = DataList.TryGetValue<DateTime>(DataType.TASK_CLIENT_PAYMENT_DATE);
                if (date == DateTime.MinValue)
                    date = null;
                return date;
            }
            set
            {
                DataList.SetValue(DataType.TASK_CLIENT_PAYMENT_DATE, value);
            }
        }
        [XmlIgnore]
        public bool ClientOrderedToPay
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.TASK_CLIENT_ORDERED_TO_PAY);
            }
            set
            {
                DataList.SetValue(DataType.TASK_CLIENT_ORDERED_TO_PAY, value);
            }
        }

        [XmlIgnore]
        public bool IMRMySCVisible
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.TASK_IMRMYSC_VISIBLE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_IMRMYSC_VISIBLE, value);
            }
        }
        [XmlIgnore]
        public bool WarrantyService
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.TASK_WARRANTY_SERVICE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_WARRANTY_SERVICE, value);
            }
        }
        [XmlIgnore]
        public bool AdditionalPayment
        {
            get
            {
                return DataList.TryGetValue<bool>(DataType.TASK_ADDITIONAL_PAYMENT);
            }
            set
            {
                DataList.SetValue(DataType.TASK_ADDITIONAL_PAYMENT, value);
            }
        }
        [XmlIgnore]
        public DateTime? ValidationDate
        {
            get
            {
                if (this.Location != null)
                {
                    int? validHours = this.Location.DataList.TryGetNullableValue<int>(DataType.LOCATION_SERVICE_VALIDATION_PERIOD);
                    if (validHours.HasValue && this.FitterVisitDate.HasValue)
                    {
                        return this.FitterVisitDate.Value.AddHours(validHours.Value);
                    }
                }
                return null;
            }
        }
        [XmlIgnore]
        public string MaintenanceNotes
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.TASK_MAINTENANCE_NOTE);
            }
            set
            {
                DataList.SetValue(DataType.TASK_MAINTENANCE_NOTE, value);
            }
        }
        public List<OpTaskHistory> TaskHistory { get; set; }
        #region Overdue
        [XmlIgnore]
        public int? Overdue
        {
            get
            {
                int? overdue = null;
                if (this.TaskHistory != null && this.TaskHistory.Count > 0 && this.Deadline.HasValue)
                {
                    OpTaskHistory lastEntry = this.TaskHistory.Find(h => !h.EndDate.HasValue);
                    OpTaskHistory firstEntry = this.TaskHistory.First();

                    if (lastEntry != null)
                    {
                        TimeSpan completeTaskTime = this.DEADLINE.Value - firstEntry.StartDate;
                        if (!this.FitterVisitDate.HasValue)
                        {
                            if (lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_succesfully ||
                                lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_with_error ||
                                lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.Canceled ||
                                lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.Verified ||
                                lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.RealizationNotPossible ||
                                lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.Rejected ||
                                lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.Suspended ||
                                lastEntry.IdTaskStatus == (int)OpTaskStatus.Enum.WaitingForAcceptance)
                            {
                                //overdue = (lastEntry.StartDate - task.Deadline.Value).Days;
                                overdue = (lastEntry.StartDate - firstEntry.StartDate).Days;
                            }
                            if (this.IdTaskStatus == (int)OpTaskStatus.Enum.In_progress ||
                                this.IdTaskStatus == (int)OpTaskStatus.Enum.Not_Started ||
                                this.IdTaskStatus == (int)OpTaskStatus.Enum.Planned)
                            {
                                //overdue = (DataProvider.DateTimeNow - task.Deadline.Value).Days;//Deadline w getterze i setterze jest konwertowany do czasu lokalnego
                                overdue = (DataProvider.DateTimeNow - firstEntry.StartDate).Days;
                            }
                        }
                        else
                            overdue = (this.FitterVisitDate.Value - firstEntry.StartDate).Days;
                        if (overdue.HasValue)
                        {
                            List<OpTaskHistory> toSkip = this.TaskHistory.Where(h => h.EndDate.HasValue && 
                                                                                (!this.FitterVisitDate.HasValue || h.StartDate < this.FitterVisitDate.Value) &&
                                                                                (h.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_succesfully ||
                                                                                h.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_with_error ||
                                                                                h.IdTaskStatus == (int)OpTaskStatus.Enum.Canceled ||
                                                                                h.IdTaskStatus == (int)OpTaskStatus.Enum.Verified ||
                                                                                h.IdTaskStatus == (int)OpTaskStatus.Enum.RealizationNotPossible ||
                                                                                h.IdTaskStatus == (int)OpTaskStatus.Enum.Rejected ||
                                                                                h.IdTaskStatus == (int)OpTaskStatus.Enum.Suspended ||//tego nie biezemy pod uwage jako skip bo przy odwieszaniu w terminalu deadline taska jest przedluzany o ilosc dni issue w stanie suspended
                                                                                h.IdTaskStatus == (int)OpTaskStatus.Enum.WaitingForAcceptance))
                                                                    .ToList();
                            TimeSpan tsToSkip;
                            int toSkipDays = 0;
                            int toSkipHours = 0;
                            //odliczamy wszystkie stany kt�re mo�na pomin��
                            foreach (OpTaskHistory thItem in toSkip)
                            {
                                if (this.FitterVisitDate.HasValue && thItem.EndDate.Value > this.FitterVisitDate.Value)
                                    tsToSkip = this.FitterVisitDate.Value - thItem.StartDate;
                                else
                                    tsToSkip = thItem.EndDate.Value - thItem.StartDate;
                                toSkipDays += tsToSkip.Days;
                                toSkipHours += tsToSkip.Hours;
                            }
                            //odliczamy czas na wykonanie zadania
                            toSkipDays += completeTaskTime.Days;
                            toSkipHours += completeTaskTime.Hours;
                            if (toSkipDays != 0)
                            {
                                overdue -= toSkipDays + (int)(toSkipHours / 24) + (toSkipHours % 24 > 0 ? 1 : 0);//zaokr�glamy dni w g�re
                            }
                        }
                    }
                }

                return overdue;
            }
        }
        public int? DeadlineDaysLeft
        {
            get
            {
                int? retValue = this.Overdue;
                if (retValue.HasValue)
                {
                    if (retValue < 0)
                        retValue = Math.Abs(retValue.Value);
                    else
                        retValue = 0;
                }
                return retValue;
            }
        }
        public int? SuspendedDaysCnt
        {
            get
            {
                int? suspendedDaysCnt = null;
                if (this.TaskHistory != null && this.TaskHistory.Count > 0)
                {
                    suspendedDaysCnt = 0;
                    int suspendedHoursCnt = 0;
                    foreach(OpTaskHistory thItem in this.TaskHistory)
                    {
                        if(thItem.IdTaskStatus == (int)OpTaskStatus.Enum.Suspended)
                        {
                            DateTime start = thItem.StartDate;
                            DateTime end = thItem.EndDate.HasValue ? thItem.EndDate.Value: DataProvider.DateTimeNow;
                            suspendedDaysCnt += (int)(end - start).TotalDays;
                            suspendedHoursCnt += (int)(end - start).Hours;
                        }
                    }
                    if(suspendedHoursCnt > 0)
                    {
                        suspendedDaysCnt += (int)Math.Ceiling(((double)suspendedHoursCnt / 24.0));
                    }
                }

                return suspendedDaysCnt;
            }
        }

        public Color OverdueColor
        {
            get
            {
                if (Overdue.HasValue)
                {
                    if (Overdue > 0)
                        return Color.Red;
                    else
                        return Color.Yellow;
                }
                else
                    return Color.Transparent;
            }
        }
        #endregion

        [XmlIgnore]
        public string SalesOrder
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.TASK_SALES_ORDER_NUMBER);
            }
            set
            {
                DataList.SetValue(DataType.TASK_SALES_ORDER_NUMBER, value);
            }
        }

        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;
    #endregion
    #region Private Members
        private static IDataProvider DataProvider = null;
    #endregion

    #region	Ctor
        public OpTask()
            : base()
        {
            this.DataList = new OpDataList<OpTaskData>();
            this.OpState = OpChangeState.New;
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpTask(DB_TASK clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpTaskData>();
            this.OpState = OpChangeState.Loaded;
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpTask(OpTask clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpTaskData>();
			if(clone.DataList != null)
			{
				foreach(OpTaskData tdItem in clone.DataList)
				{
					this.DataList.Add(new OpTaskData(tdItem));
				}
			}
            if (this.TaskStatus != null)
                this.TaskStatus = clone.TaskStatus;
            if (this.TaskType != null)
                this.TaskType = clone.TaskType;
            if (this.OperatorPerformer != null)
                this.OperatorPerformer = clone.OperatorPerformer;
            if (this.OperatorRegistering != null)
                this.OperatorRegistering = clone.OperatorRegistering;
            if (this.Location != null)
                this.Location = clone.Location;
            if (this.Issue != null)
                this.Issue = clone.Issue;
            this.OpState = clone.OpState;
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        } 
    #endregion

    #region Clone
        public OpTask Clone(IDataProvider dataProvider)
        {
            OpTask newTask = new OpTask(this);
            //properties which have to be reset
            newTask.IdTask = 0;
            newTask.CreationDate = dataProvider.DateTimeNow;
            newTask.Deadline = null;
            newTask.FitterVisitDate = null;
            newTask.FitterPaymentDone = false;
            newTask.ClientPaymentDone = false;
            newTask.DataList = this.DataList;
            newTask.TaskStatus = dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Not_Started);
            newTask.TaskGroup = null;
            newTask.Issue = null;
            newTask.PaymentAmount = 0;
            newTask.PaymentNote = null;
            newTask.FitterPaymentDate = null;
            newTask = ConvertList(new DB_TASK[] { newTask }, dataProvider)[0];
            newTask.OpState = OpChangeState.New;

            newTask.DataList = DataList.Clone(dataProvider);
            foreach (OpTaskData taskData in newTask.DataList)
            {
                taskData.OpState = OpChangeState.New;
                taskData.IdData = 0;
            }
            newTask.dynamicValues = new Dictionary<string, object>();
            return newTask;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            string taskType = string.Empty;
            string location = string.Empty;

            if (this.TaskType != null)
            {
                taskType = this.TaskType.ToString();
            }
            else if (DynamicValues != null && DynamicValues.ContainsKey(DataType.HELPER_ID_TASK_TYPE.ToString()) && DynamicValues[DataType.HELPER_ID_TASK_TYPE.ToString()] != null)
            {
                taskType = DynamicValues[DataType.HELPER_ID_TASK_TYPE.ToString()].ToString();
            }
            else
            {
                taskType = this.IdTaskType.ToString();
            }

            if (this.Location != null)
            {
                location = this.Location.ToString();
            }
            else if (DynamicValues != null && DynamicValues.ContainsKey(DataType.HELPER_ID_LOCATION.ToString()) && DynamicValues[DataType.HELPER_ID_LOCATION.ToString()] != null)
            {
                location = DynamicValues[DataType.HELPER_ID_LOCATION.ToString()].ToString();
            }
            else if (this.IdLocation.HasValue)
            {
                location = this.IdLocation.Value.ToString();                
            }

            return string.Format("{0}; {1}", taskType, location);
        }
    #endregion

    #region	ConvertList
        public static List<OpTask> ConvertList(DB_TASK[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true, false);
        }

        public static List<OpTask> ConvertList(DB_TASK[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadTaskHistory, bool loadCustomData = true, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            DataProvider = dataProvider;

            List<OpTask> ret = new List<OpTask>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTask(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if(loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes: customDataTypes, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data

            if(loadTaskHistory)
                LoadTaskHistory(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads task history

            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTask> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpTaskType> TaskTypeDict = dataProvider.GetTaskType(list.Select(l => l.ID_TASK_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_TASK_TYPE);
                //Dictionary<int, OpIssue> IssueDict = dataProvider.GetIssue(list.Where(l => l.ID_ISSUE.HasValue).Select(l => l.ID_ISSUE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ISSUE);
                Dictionary<int, OpTaskGroup> TaskGroupDict = dataProvider.GetTaskGroup(list.Where(l => l.ID_TASK_GROUP.HasValue).Select(l => l.ID_TASK_GROUP.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_TASK_GROUP);
                Dictionary<int, OpTaskStatus> TaskStatusDict = dataProvider.GetTaskStatus(list.Select(l => l.ID_TASK_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_TASK_STATUS);

                Dictionary<int, OpActor> ActorDict = dataProvider.GetActor(list.Where(l => l.ID_ACTOR.HasValue).Select(l => l.ID_ACTOR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTOR);
                Dictionary<int, OpOperator> OperatorPerformerDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR_PERFORMER.HasValue).Select(l => l.ID_OPERATOR_PERFORMER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpOperator> OperatorRegisteringDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR_REGISTERING).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

                Dictionary<long, OpRoute> RouteDict = dataProvider.GetRoute(list.Where(l => l.ID_PLANNED_ROUTE.HasValue).Select(l => l.ID_PLANNED_ROUTE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ROUTE);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                Dictionary<int, OpIssue> IssueDict = dataProvider.GetIssue(list.Where(l => l.ID_ISSUE != null).Select(l => l.ID_ISSUE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ISSUE);

                foreach (var loop in list)
                {
                    loop.TaskType = TaskTypeDict.TryGetValue(loop.ID_TASK_TYPE);
                    //!!! BB zakomentowane z powod�w optymalizacyjnych
                    //if (loop.ID_ISSUE.HasValue)
                    //    loop.Issue = IssueDict.TryGetValue(loop.ID_ISSUE.Value);
                    if (loop.ID_TASK_GROUP.HasValue)
                        loop.TaskGroup = TaskGroupDict.TryGetValue(loop.ID_TASK_GROUP.Value);
                    loop.TaskStatus = TaskStatusDict.TryGetValue(loop.ID_TASK_STATUS);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    if (loop.ID_ACTOR.HasValue)
                        loop.Actor = ActorDict.TryGetValue(loop.ID_ACTOR.Value);
                    if (loop.ID_OPERATOR_PERFORMER.HasValue)
                        loop.OperatorPerformer = OperatorPerformerDict.TryGetValue(loop.ID_OPERATOR_PERFORMER.Value);
                    loop.OperatorRegistering = OperatorRegisteringDict.TryGetValue(loop.ID_OPERATOR_REGISTERING);
                    if (loop.ID_PLANNED_ROUTE.HasValue)
                        loop.Route = RouteDict.TryGetValue(loop.ID_PLANNED_ROUTE.Value);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                    if(loop.ID_ISSUE.HasValue)
                        loop.Issue = IssueDict.TryGetValue(loop.ID_ISSUE.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpTask> list, IDataProvider dataProvider, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.ITaskDataTypes != null)
            {
                List<int> idTask = list.Select(l => l.IdTask).ToList();
                List<OpTaskData> data = new List<OpTaskData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetTaskDataFilter(IdTask: idTask.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                else if (dataProvider.ITaskDataTypes != null && dataProvider.ITaskDataTypes.Count > 0)
                    data = dataProvider.GetTaskDataFilter(IdTask: idTask.ToArray(), IdDataType: dataProvider.ITaskDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);

                if (data != null && data.Count > 0)
                {
                    Dictionary<int, OpTask> tasksDict = list.ToDictionary<OpTask, int>(l => l.IdTask);
                    foreach (var dataValue in data)
                    {
                        tasksDict[dataValue.IdTask].DataList.Add(dataValue);
                    }
                }
            }
        }
    #endregion

    #region LoadTaskHistory

        public static void LoadTaskHistory(ref List<OpTask> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<int> idTask = list.Select(l => l.IdTask).ToList();
                
                List<OpTaskHistory> history = dataProvider.GetTaskHistoryFilter(IdTask: idTask.ToArray(), loadNavigationProperties: false, autoTransaction: false, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (history != null && history.Count > 0)
                {
                    list.ForEach(l => l.TaskHistory = new List<OpTaskHistory>());
                    Dictionary<int, OpTask> tasksDict = list.ToDictionary<OpTask, int>(l => l.IdTask);
                    
                    foreach (var historyValue in history)
                    {
                        tasksDict[historyValue.IdTask].TaskHistory.Add(historyValue);
                    }
                    foreach (OpTask tItem in list)
                    {
                        if (tItem.TaskHistory != null)
                            tItem.TaskHistory = tItem.TaskHistory.OrderBy(t => t.StartDate).ToList();
                    }
                }
            }
        }

    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTask)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTask).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion
    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdTask;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    #region Implementation of IOpDynamic
        [XmlIgnore]
        public object Key
        {
            get { return IdTask; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpTask IOpDynamicProperty<OpTask>.Owner
        {
            get { return this; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        [XmlIgnore]
        public object Owner
        {
            get { return this; }
        }
        #endregion
    #region Implementation of IOpObject
    [XmlIgnore]
    public object IdObject
    {
        get { return IdTask; }
        set { IdTask = Convert.ToInt32(value); }
    }

    [DataMember]
    public OpDataList<OpTaskData> DataList { get; set; }
    #endregion
    #region override Equals
    public override bool Equals(object obj)
    {
        if (obj is OpTask)
            return this.IdTask == ((OpTask)obj).IdTask;
        else
            return base.Equals(obj);
    }
    public static bool operator ==(OpTask left, OpTask right)
    {
        if ((object)left == null)
            return ((object)right == null);
        return left.Equals(right);
    }
    public static bool operator !=(OpTask left, OpTask right)
    {
        if ((object)left == null)
            return ((object)right != null);
        return !left.Equals(right);
    }

    public override int GetHashCode()
    {
        return IdTask.GetHashCode();
    }
    #endregion

    #region Implementation of IOpDataProperty

    #region GetOpDataPropertyValue

        public object GetOpDataPropertyValue(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return null;

            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.Location != null)
                        return this.Location.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
            }

            OpTaskData opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opDataProperty.OpDataType.IdReferenceType.HasValue && opData.ReferencedObject != null)
                    return opData.ReferencedObject;

                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && opData.Value != null)
                    return OpUnit.ChangeUnit(opData.Value, opDataProperty.OpDataType, opData.DataType.Unit, opDataProperty.OpUnit, OpDataTypeFormat.GetPrecision(opDataProperty.OpDataTypeFormat));

                return opData.Value;
            }

            return null;
        } 

    #endregion

    #region SetOpDataPropertyValue

        public void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return;

            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.Location != null)
                        this.Location.SetOpDataPropertyValue(opDataProperty, value);
                    return;
            }

            OpTaskData opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && value != null)
                    opData.Value = OpUnit.ChangeUnit(value, opDataProperty.OpDataType, opDataProperty.OpUnit, opData.DataType.Unit);
                else
                    opData.Value = value;
            }
        } 

    #endregion

    #region GetColumnOpData

        private OpTaskData GetColumnOpData(OpDataType dataType)
        {
            return null;
        }

        #endregion

    #region GetOpDataPropertyType
        public Type GetOpDataPropertyType(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null || !opDataProperty.ReturnReferencedTypeIfHelper.HasValue) return null;
            Type type = Utils.GetHelperType(opDataProperty.OpDataType, opDataProperty.ReturnReferencedTypeIfHelper.Value);

            if (type == null)
                type = DataType.GetSystemType(opDataProperty.OpDataType.IdDataTypeClass);

            return type;
        }
        #endregion
    #endregion
    }
#endif
}