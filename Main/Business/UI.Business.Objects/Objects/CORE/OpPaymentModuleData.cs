using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpPaymentModuleData : DB_PAYMENT_MODULE_DATA, IComparable, IEquatable<OpPaymentModuleData>
    {
    #region Properties
        public long IdPaymentModuleData { get { return this.ID_PAYMENT_MODULE_DATA;} set {this.ID_PAYMENT_MODULE_DATA = value;}}
        public int IdPaymentModule { get { return this.ID_PAYMENT_MODULE;} set {this.ID_PAYMENT_MODULE = value;}}
        public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
        public int IndexNbr { get { return this.INDEX_NBR;} set {this.INDEX_NBR = value;}}
        public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
    #endregion

    #region	Navigation Properties
		private OpPaymentModule _PaymentModule;
				public OpPaymentModule PaymentModule { get { return this._PaymentModule; } set { this._PaymentModule = value; this.ID_PAYMENT_MODULE = (value == null)? 0 : (int)value.ID_PAYMENT_MODULE; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpPaymentModuleData()
			:base() {}
		
		public OpPaymentModuleData(DB_PAYMENT_MODULE_DATA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "PaymentModuleData [" + IdPaymentModuleData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpPaymentModuleData> ConvertList(DB_PAYMENT_MODULE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpPaymentModuleData> ConvertList(DB_PAYMENT_MODULE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpPaymentModuleData> ret = new List<OpPaymentModuleData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpPaymentModuleData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpPaymentModuleData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpPaymentModule> PaymentModuleDict = dataProvider.GetPaymentModule(list.Select(l => l.ID_PAYMENT_MODULE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_PAYMENT_MODULE);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
						loop.PaymentModule = PaymentModuleDict.TryGetValue(loop.ID_PAYMENT_MODULE); 
		loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpPaymentModuleData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpPaymentModuleData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpPaymentModuleData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpPaymentModuleData> Members
        public bool Equals(OpPaymentModuleData other)
        {
            if (other == null)
				return false;
			return this.IdPaymentModuleData.Equals(other.IdPaymentModuleData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpPaymentModuleData)
				return this.IdPaymentModuleData == ((OpPaymentModuleData)obj).IdPaymentModuleData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpPaymentModuleData left, OpPaymentModuleData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpPaymentModuleData left, OpPaymentModuleData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdPaymentModuleData.GetHashCode();
		}
    #endregion
    }
#endif
}