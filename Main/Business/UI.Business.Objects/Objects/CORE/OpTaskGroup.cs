using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpTaskGroup : DB_TASK_GROUP, IComparable, IReferenceType
    {
    #region Properties
        public int IdTaskGroup { get { return this.ID_TASK_GROUP; } set { this.ID_TASK_GROUP = value; } }
        public int? IdParrentGroup { get { return this.ID_PARRENT_GROUP; } set { this.ID_PARRENT_GROUP = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public DateTime DateCreated { get { return this.DATE_CREATED; } set { this.DATE_CREATED = value; } }
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
    #endregion

    #region	Navigation Properties
        private OpTaskGroup _ParentGroup;
        public OpTaskGroup ParentGroup { get { return this._ParentGroup; } set { this._ParentGroup = value; this.ID_PARRENT_GROUP = (value == null) ? null : (int?)value.ID_TASK_GROUP; } }
        private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpTaskGroup()
            : base() { }

        public OpTaskGroup(DB_TASK_GROUP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpTaskGroup> ConvertList(DB_TASK_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpTaskGroup> ConvertList(DB_TASK_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTaskGroup> ret = new List<OpTaskGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpTaskGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTaskGroup> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                foreach (var loop in list)
                {
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTaskGroup> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTaskGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTaskGroup).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
        #endregion

    #region Implementation of IReferenceType
    public object GetReferenceKey()
    {
        return IdTaskGroup;
    }

    public object GetReferenceValue()
    {
        return this;
    }
    #endregion
    }
#endif
}