using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Data;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpDistributor : DB_DISTRIBUTOR, IComparable, IReferenceType, IOpChangeState, IOpDynamicProperty<OpDistributor>, IOpDynamic, IOpObject<OpDistributorData>
    {
        #region Properties 
        [XmlIgnore]
        public int IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
        [XmlIgnore]
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        [XmlIgnore]
        public string City { get { return this.CITY; } set { this.CITY = value; } }
        [XmlIgnore]
        public string Address { get { return this.ADDRESS; } set { this.ADDRESS = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        [DataMember]
        private OpDescr _Descr;
        [XmlIgnore]
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.IdDescr = (value == null) ? null : (long?)value.ID_DESCR; } }
        #endregion

        #region	Custom Properties

        [XmlIgnore]
        public int DitributorIMRServerId { get; set; }

        [XmlIgnore]
        public long? EquipmentLocationId
        {
            get
            {
                return DataList.TryGetNullableValue<long>(DataType.DEPOSITORY_ID);
            }
            set
            {
                DataList.SetValue(DataType.DEPOSITORY_ID, value);
            }
        }

        [XmlIgnore]
        /// <summary>
        /// Used for sorting items in ComboBox
        /// </summary>
        public string ComboBoxString
        {
            get { return this.ToString(); }
        }

        [DataMember]
        public OpChangeState OpState { get; set; }

        [XmlIgnore]
        public long? IdServiceMagazine
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.DISTRIBUTOR_SERVICE_MAGAZINE_LOCATION_ID);
            }
            set
            {
                if (DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_SERVICE_MAGAZINE_LOCATION_ID))
                    DataList.SetValue(DataType.DISTRIBUTOR_SERVICE_MAGAZINE_LOCATION_ID, value);
                else
                {
                    OpDistributorData newData = new OpDistributorData();
                    newData.IdDistributor = this.IdDistributor;
                    newData.Index = 0;
                    newData.IdDataType = DataType.DISTRIBUTOR_SERVICE_MAGAZINE_LOCATION_ID;
                    newData.Value = value;
                    newData.OpState = OpChangeState.New;
                    DataList.Add(newData);
                }
            }
        }

        [DataMember]
        private OpLocation _ServiceMagazine;

        [XmlIgnore]
        public OpLocation ServiceMagazine
        {
            get { return _ServiceMagazine; }
            set
            {
                _ServiceMagazine = value;
                if (value != null)
                    this.IdServiceMagazine = _ServiceMagazine.IdLocation;
                else
                    this.IdServiceMagazine = null;
            }
        }

        [XmlIgnore]
        public long? IdDepositoryMagazine
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.DISTRIBUTOR_DEPOSITORY_MAGAZINE_LOCATION_ID);
            }
            set
            {
                if (DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_DEPOSITORY_MAGAZINE_LOCATION_ID))
                    DataList.SetValue(DataType.DISTRIBUTOR_DEPOSITORY_MAGAZINE_LOCATION_ID, value);
                else
                {
                    OpDistributorData newData = new OpDistributorData();
                    newData.IdDistributor = this.IdDistributor;
                    newData.Index = 0;
                    newData.IdDataType = DataType.DISTRIBUTOR_DEPOSITORY_MAGAZINE_LOCATION_ID;
                    newData.Value = value;
                    newData.OpState = OpChangeState.New;
                    DataList.Add(newData);
                }
            }
        }

        [DataMember]
        private OpLocation _DepositoryMagazine;

        [XmlIgnore]
        public OpLocation DepositoryMagazine
        {
            get { return _DepositoryMagazine; }
            set
            {
                _DepositoryMagazine = value;
                if (value != null)
                    this.IdDepositoryMagazine = _DepositoryMagazine.IdLocation;
                else
                    this.IdDepositoryMagazine = null;
            }
        }

        [XmlIgnore]
        public int? IdIMRServer
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.DISTRIBUTOR_IMR_SERVER_ID);
            }
            set
            {
                if (DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_IMR_SERVER_ID))
                    DataList.SetValue(DataType.DISTRIBUTOR_IMR_SERVER_ID, value);
                else
                {
                    OpDistributorData newData = new OpDistributorData();
                    newData.IdDistributor = this.IdDistributor;
                    newData.Index = 0;
                    newData.IdDataType = DataType.DISTRIBUTOR_IMR_SERVER_ID;
                    newData.Value = value;
                    newData.OpState = OpChangeState.New;
                    DataList.Add(newData);
                }
            }
        }

        [DataMember]
        private OpImrServer _ImrServer;

        [XmlIgnore]
        public OpImrServer ImrServer
        {
            get 
            { 
               // if(_ImrServer != null && _ImrServer.ToString().Contains("localhost"))
                return _ImrServer; 
            }
            set
            {
                _ImrServer = value;
                if (value != null)
                    this.IdIMRServer = _ImrServer.IdImrServer;
                else
                    this.IdIMRServer = null;
            }
        }

        [XmlIgnore]  
        private OpDynamicPropertyDict dynamicProperties;

        [DataMember]
        private Dictionary<string, object> dynamicValues;

    #endregion

    #region	Ctor
        public OpDistributor()
            : base()
        {
			DataList = new OpDataList<OpDistributorData>();
            OpState = OpChangeState.New;
            this.dynamicProperties = new OpDynamicPropertyDict();
            this.DynamicValues = new Dictionary<string, object>();
        }

        public OpDistributor(DB_DISTRIBUTOR clone)
            : base(clone)
        {
			DataList = new OpDataList<OpDistributorData>();
            OpState = OpChangeState.Loaded;
            this.dynamicProperties = new OpDynamicPropertyDict();
            this.DynamicValues = new Dictionary<string, object>();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            //if (this.Sla != null)
            //    return string.Format("{0} ({1})", this.Name, this.Sla);
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDistributor> ConvertList(DB_DISTRIBUTOR[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDistributor> ConvertList(DB_DISTRIBUTOR[] list, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDistributor> ret = new List<OpDistributor>(list.Length);
            foreach (var loop in list)
            {
                OpDistributor insert = new OpDistributor(loop);

                if (loadNavigationProperties)
                {
                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(loop.ID_DESCR.Value);
                }

                ret.Add(insert);
            }

            if(loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDistributor> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray(), false, dataProvider.UserLanguage, autoTransaction, transactionLevel, commandTimeout).ToDictionary(l => l.ID_DESCR);
                foreach (var loop in list)
                {
                    if(loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpDistributor> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0 && dataProvider.IDistributorDataTypes != null)
            {
                List<int> idDistributor = list.Select(l => l.IdDistributor).ToList();

                if (dataProvider.IDistributorDataTypes.Count > 0)
                {
                    List<OpDistributorData> data = null;
                    if (customDataTypes != null && customDataTypes.Count > 0)
                        data = dataProvider.GetDistributorDataFilter(IdDistributor: idDistributor.ToArray(), IdDataType: customDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    else if (dataProvider.ILocationDataTypes != null && dataProvider.ILocationDataTypes.Count > 0)
                        data = dataProvider.GetDistributorDataFilter(IdDistributor: idDistributor.ToArray(), IdDataType: dataProvider.IDistributorDataTypes.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    
                    if (data != null && data.Count > 0)
                    {
                        Dictionary<int, OpDistributor> tasksDict = list.ToDictionary<OpDistributor, int>(l => l.IdDistributor);
                        foreach (var dataValue in data)
                        {
                            tasksDict[dataValue.IdDistributor].DataList.Add(dataValue);
                        }

                        //if (list.Any(d => d.DataList.Any(e => e.IdDataType == DataType.DEPOSITORY_ID && e.Value != null)))
                        //{
                        //    Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.EquipmentLocationId.HasValue).Select(l => l.EquipmentLocationId.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);

                        //    foreach (var loop in list)
                        //    {
                        //        if (loop.EquipmentLocationId.HasValue && loop.EquipmentLocationId > 0)
                        //            loop.EquipmentLocation = LocationDict.TryGetValue(loop.EquipmentLocationId.Value);
                        //    }
                        //}
                    }
                }
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(d => d.IdServiceMagazine.HasValue)
                                                                                             .Select(d => d.IdServiceMagazine.Value)
                                                                                             .Distinct()
                                                                              .Union(list.Where(d => d.IdDepositoryMagazine.HasValue)
                                                                                             .Select(d => d.IdDepositoryMagazine.Value)
                                                                                             .Distinct()).ToArray(), 
                                                                                     false, loadNavigationProperties: false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout)
                                                                        .ToDictionary(d => d.IdLocation);
                Dictionary<int, OpImrServer> ImrServerDict = dataProvider.GetImrServer(list.Where(d => d.IdIMRServer.HasValue).Select(d => d.IdIMRServer.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(d => d.IdImrServer);
                foreach (var loop in list)
                {
                    if(loop.IdServiceMagazine.HasValue)
                        loop.ServiceMagazine = LocationDict.TryGetValue(loop.IdServiceMagazine.Value);
                    if (loop.IdDepositoryMagazine.HasValue)
                        loop.DepositoryMagazine = LocationDict.TryGetValue(loop.IdDepositoryMagazine.Value);
                    if (loop.IdIMRServer.HasValue)
                        loop.ImrServer = ImrServerDict.TryGetValue(loop.IdIMRServer.Value);
                }
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDistributor)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDistributor).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDistributor)
                return this.IdDistributor == ((OpDistributor)obj).IdDistributor;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDistributor left, OpDistributor right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDistributor left, OpDistributor right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDistributor.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
       
    public object GetReferenceKey()
    {
        return this.IdDistributor;
    }

        
    public object GetReferenceValue()
    {
        return this;
    }
    #endregion

    #region Implementation of IOpDynamicProperty
    [XmlIgnore]
    public OpDynamicPropertyDict DynamicProperties
    {
        get { return this.dynamicProperties; }
        set { this.dynamicProperties = value; }
    }

    [XmlIgnore]
    OpDistributor IOpDynamicProperty<OpDistributor>.Owner
    {
        get { return this; }
    }
        #endregion

    #region Implementation of IOpDynamic
    [XmlIgnore]
    public object Key
    {
        get { return IdDistributor; }
    }

    [XmlIgnore]
    public Dictionary<string, object> DynamicValues
    {
        get { return dynamicValues; }
        set { dynamicValues = value; }
    }
    [XmlIgnore]
    public object Owner
    {
        get { return this; }
    }
    #endregion
    #region Implementation of IOpObject
    [XmlIgnore]
    public object IdObject
    {
        get { return IdDistributor; }
        set { IdDistributor = Convert.ToInt32(value); }
    }

    [DataMember]
    public OpDataList<OpDistributorData> DataList { get; set; }
    #endregion
    }
#endif
}