using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpFaultCodeSummary : DB_FAULT_CODE_SUMMARY, IComparable, IEquatable<OpFaultCodeSummary>
    {
    #region Properties
        public int IdFaultCodeSummary { get { return this.ID_FAULT_CODE_SUMMARY;} set {this.ID_FAULT_CODE_SUMMARY = value;}}
        public int? IdFaultCode { get { return this.ID_FAULT_CODE;} set {this.ID_FAULT_CODE = value;}}
        public int IdServiceReferenceType { get { return this.ID_SERVICE_REFERENCE_TYPE;} set {this.ID_SERVICE_REFERENCE_TYPE = value;}}
        public object ReferenceValue { get { return this.REFERENCE_VALUE;} set {this.REFERENCE_VALUE = value;}}
        public int FaultsCount { get { return this.FAULTS_COUNT;} set {this.FAULTS_COUNT = value;}}
        public int TotalCount { get { return this.TOTAL_COUNT; } set { this.TOTAL_COUNT = value; } }
        public int IdFaultCodeSummaryType { get { return this.ID_FAULT_CODE_SUMMARY_TYPE; } set { this.ID_FAULT_CODE_SUMMARY_TYPE = value; } }
    #endregion

    #region	Navigation Properties
		private OpFaultCode _FaultCode;
				public OpFaultCode FaultCode { get { return this._FaultCode; } set { this._FaultCode = value; this.ID_FAULT_CODE = (value == null)? 0 : (int)value.ID_FAULT_CODE; } }
				private OpServiceReferenceType _ServiceReferenceType;
				public OpServiceReferenceType ServiceReferenceType { get { return this._ServiceReferenceType; } set { this._ServiceReferenceType = value; this.ID_SERVICE_REFERENCE_TYPE = (value == null)? 0 : (int)value.ID_SERVICE_REFERENCE_TYPE; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpFaultCodeSummary()
			:base()
        {
            OpState = OpChangeState.New;
        }
		
		public OpFaultCodeSummary(DB_FAULT_CODE_SUMMARY clone)
			:base(clone)
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "FaultCodeSummary [" + IdFaultCodeSummary.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpFaultCodeSummary> ConvertList(DB_FAULT_CODE_SUMMARY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpFaultCodeSummary> ConvertList(DB_FAULT_CODE_SUMMARY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpFaultCodeSummary> ret = new List<OpFaultCodeSummary>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpFaultCodeSummary(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpFaultCodeSummary> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpFaultCode> FaultCodeDict = new Dictionary<int, OpFaultCode>();
                if(list.Exists(d => d.ID_FAULT_CODE.HasValue))
                    FaultCodeDict = dataProvider.GetFaultCode(list.Where(l => l.ID_FAULT_CODE.HasValue).Select(l => l.ID_FAULT_CODE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_FAULT_CODE);
	            Dictionary<int,OpServiceReferenceType> ServiceReferenceTypeDict = dataProvider.GetServiceReferenceType(list.Select(l => l.ID_SERVICE_REFERENCE_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_SERVICE_REFERENCE_TYPE);
			
				foreach (var loop in list)
				{
                    if(loop.ID_FAULT_CODE.HasValue)
					    loop.FaultCode = FaultCodeDict.TryGetValue(loop.ID_FAULT_CODE.Value); 
		            loop.ServiceReferenceType = ServiceReferenceTypeDict.TryGetValue(loop.ID_SERVICE_REFERENCE_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpFaultCodeSummary> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpFaultCodeSummary)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpFaultCodeSummary).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpFaultCodeSummary> Members
        public bool Equals(OpFaultCodeSummary other)
        {
            if (other == null)
				return false;
			return this.IdFaultCodeSummary.Equals(other.IdFaultCodeSummary);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpFaultCodeSummary)
				return this.IdFaultCodeSummary == ((OpFaultCodeSummary)obj).IdFaultCodeSummary;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpFaultCodeSummary left, OpFaultCodeSummary right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpFaultCodeSummary left, OpFaultCodeSummary right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdFaultCodeSummary.GetHashCode();
		}
    #endregion
    }
#endif
}