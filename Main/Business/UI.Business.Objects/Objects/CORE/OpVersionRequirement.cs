using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionRequirement : DB_VERSION_REQUIREMENT, IComparable, IEquatable<OpVersionRequirement>, IOpChangeState
    {
    #region Properties
        public long IdVersion { get { return this.ID_VERSION; } set { if (this.ID_VERSION != value) { this.ID_VERSION = OpDataUtil.SetNewValue(this.ID_VERSION, value, this); } } }
        public long IdVersionRequired { get { return this.ID_VERSION_REQUIRED; } set { if (this.ID_VERSION_REQUIRED != value) { this.ID_VERSION_REQUIRED = OpDataUtil.SetNewValue(this.ID_VERSION_REQUIRED, value, this); } } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { if (this.ID_OPERATOR != value) { this.ID_OPERATOR = OpDataUtil.SetNewValue(this.ID_OPERATOR, value, this); } } }
        public long IdVersionRequirement { get { return this.ID_VERSION_REQUIREMENT; } set { if (this.ID_VERSION_REQUIREMENT != value) { this.ID_VERSION_REQUIREMENT = OpDataUtil.SetNewValue(this.ID_VERSION_REQUIREMENT, value, this); } } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
		private OpVersion _Version;
		public OpVersion Version { get { return this._Version; } set { this._Version = value; this.ID_VERSION = (value == null)? 0 : (long)value.ID_VERSION; } }
        private OpVersion _VersionRequired;
        public OpVersion VersionRequired { get { return this._VersionRequired; } set { this._VersionRequired = value; this.ID_VERSION_REQUIRED = (value == null) ? 0 : (long)value.ID_VERSION; } }
		private OpOperator _Operator;
		public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? 0 : (int)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpVersionRequirement()
			:base() {}
		
		public OpVersionRequirement(DB_VERSION_REQUIREMENT clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "VersionRequirement [" + IdVersion.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionRequirement> ConvertList(DB_VERSION_REQUIREMENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionRequirement> ConvertList(DB_VERSION_REQUIREMENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionRequirement> ret = new List<OpVersionRequirement>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionRequirement(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionRequirement> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<long, OpVersion> VersionDict = dataProvider.GetVersion(list.Select(l => l.ID_VERSION).Union(list.Select(l => l.ID_VERSION_REQUIRED)).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION);

                foreach (var loop in list)
                {
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                    loop.Version = VersionDict.TryGetValue(loop.ID_VERSION);
                    loop.VersionRequired = VersionDict.TryGetValue(loop.ID_VERSION_REQUIRED);
                }
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionRequirement> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionRequirement)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionRequirement).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionRequirement> Members
        public bool Equals(OpVersionRequirement other)
        {
            if (other == null)
				return false;
			return this.IdVersion.Equals(other.IdVersion);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionRequirement)
				return this.IdVersion == ((OpVersionRequirement)obj).IdVersion;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionRequirement left, OpVersionRequirement right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionRequirement left, OpVersionRequirement right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersion.GetHashCode();
		}
    #endregion
	}
#endif
}