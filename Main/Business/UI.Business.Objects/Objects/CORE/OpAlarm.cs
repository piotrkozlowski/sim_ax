using System;
using System.Collections.Generic;
using System.Linq;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpAlarm : DB_ALARM, IComparable, IReferenceType, IOpChangeState, IEquatable<OpAlarm>, IOpDynamic, IOpDynamicProperty<OpAlarm>
    {
        #region Properties
        public long IdAlarm { get { return this.ID_ALARM; } set { this.ID_ALARM = value; } }
        public long IdAlarmEvent { get { return this.ID_ALARM_EVENT; } set { this.ID_ALARM_EVENT = value; } }
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public int IdAlarmGroup { get { return this.ID_ALARM_GROUP; } set { this.ID_ALARM_GROUP = value; } }
        public int IdAlarmStatus { get { return this.ID_ALARM_STATUS; } set { this.ID_ALARM_STATUS = value; } }
        public int IdTransmissionType { get { return this.ID_TRANSMISSION_TYPE; } set { this.ID_TRANSMISSION_TYPE = value; } }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpAlarmEvent _AlarmEvent;
        public OpAlarmEvent AlarmEvent { get { return this._AlarmEvent; } set { this._AlarmEvent = value; this.ID_ALARM_EVENT = (value == null) ? 0 : (long)value.ID_ALARM_EVENT; } }

        [DataMember]
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }

        [DataMember]
        private OpAlarmGroup _AlarmGroup;
        public OpAlarmGroup AlarmGroup { get { return this._AlarmGroup; } set { this._AlarmGroup = value; this.ID_ALARM_GROUP = (value == null) ? 0 : (int)value.ID_ALARM_GROUP; } }

        [DataMember]
        private OpAlarmStatus _AlarmStatus;
        public OpAlarmStatus AlarmStatus { get { return this._AlarmStatus; } set { this._AlarmStatus = value; this.ID_ALARM_STATUS = (value == null) ? 0 : (int)value.ID_ALARM_STATUS; } }

        [DataMember]
        private OpTransmissionType _TransmissionType;
        public OpTransmissionType TransmissionType { get { return this._TransmissionType; } set { this._TransmissionType = value; this.ID_TRANSMISSION_TYPE = (value == null) ? 0 : (int)value.ID_TRANSMISSION_TYPE; } }
        #endregion

        #region	Custom Properties
        [DataMember]
        private Dictionary<string, object> dynamicValues;
        private OpDynamicPropertyDict dynamicProperties;

        [DataMember]
        public OpDataList<OpAlarmData> DataList { get; set; }

        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Ctor
        public OpAlarm()
            : base()
        {
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
            this.DataList = new OpDataList<OpAlarmData>();
            OpState = OpChangeState.New;
        }

        public OpAlarm(DB_ALARM clone)
            : base(clone)
        {
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
            this.DataList = new OpDataList<OpAlarmData>();
            OpState = OpChangeState.Loaded;
        }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return "Alarm [" + IdAlarm.ToString() + "]";
        }
        #endregion

        #region	ConvertList
        public static List<OpAlarm> ConvertList(DB_ALARM[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarm> ConvertList(DB_ALARM[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAlarm> ret = new List<OpAlarm>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarm(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarm> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpAlarmEvent> AlarmEventDict = dataProvider.GetAlarmEvent(list.Select(l => l.ID_ALARM_EVENT).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_EVENT);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<int, OpAlarmGroup> AlarmGroupDict = dataProvider.GetAlarmGroup(list.Select(l => l.ID_ALARM_GROUP).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_GROUP);
                Dictionary<int, OpAlarmStatus> AlarmStatusDict = dataProvider.GetAlarmStatus(list.Select(l => l.ID_ALARM_STATUS).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_STATUS);
                Dictionary<int, OpTransmissionType> TransmissionTypeDict = dataProvider.GetTransmissionType(list.Select(l => l.ID_TRANSMISSION_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_TRANSMISSION_TYPE);
                foreach (var loop in list)
                {
                    loop.AlarmEvent = AlarmEventDict.TryGetValue(loop.ID_ALARM_EVENT);
                    loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                    loop.AlarmGroup = AlarmGroupDict.TryGetValue(loop.ID_ALARM_GROUP);
                    loop.AlarmStatus = AlarmStatusDict.TryGetValue(loop.ID_ALARM_STATUS);
                    loop.TransmissionType = TransmissionTypeDict.TryGetValue(loop.ID_TRANSMISSION_TYPE);
                }
            }
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarm> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {

        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarm)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarm).ToString());
            }
            return 0;
        }
        #endregion

        #region IEquatable<OpAlarm> Members
        public bool Equals(OpAlarm other)
        {
            if (other == null)
                return false;
            return this.IdAlarm.Equals(other.IdAlarm);
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarm)
                return this.IdAlarm == ((OpAlarm)obj).IdAlarm;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarm left, OpAlarm right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarm left, OpAlarm right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarm.GetHashCode();
        }

        #endregion

        #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdAlarm; }
        }

        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpAlarm IOpDynamicProperty<OpAlarm>.Owner
        {
            get { return this; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
        #endregion

        #region Implementation of IReferenceType

        public object GetReferenceKey()
        {
            return this.IdAlarm;
        }

        public object GetReferenceValue()
        {
            return this;
        }

        #endregion
    }
#endif
}