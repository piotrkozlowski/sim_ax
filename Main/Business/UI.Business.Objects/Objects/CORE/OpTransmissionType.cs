using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpTransmissionType : DB_TRANSMISSION_TYPE, IComparable, IEquatable<OpTransmissionType>, IReferenceType
    {
    #region Properties
        public int IdTransmissionType { get { return this.ID_TRANSMISSION_TYPE; } set { this.ID_TRANSMISSION_TYPE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
    #endregion

    #region	Navigation Properties
        [DataMember]
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpTransmissionType()
            : base() { }

        public OpTransmissionType(DB_TRANSMISSION_TYPE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpTransmissionType> ConvertList(DB_TRANSMISSION_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpTransmissionType> ConvertList(DB_TRANSMISSION_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpTransmissionType> ret = new List<OpTransmissionType>(list.Length);
            foreach (var loop in list)
            {
                OpTransmissionType insert = new OpTransmissionType(loop);
                ret.Add(insert);
            }

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpTransmissionType> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);			
                foreach (var loop in list)
                {
                    if (loop.ID_DESCR.HasValue)
                        loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 				
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpTransmissionType> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpTransmissionType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpTransmissionType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpTransmissionType> Members
        public bool Equals(OpTransmissionType other)
        {
            if (other == null)
                return false;
            return this.IdTransmissionType.Equals(other.IdTransmissionType);
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdTransmissionType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpTransmissionType)
                return this.IdTransmissionType == ((OpTransmissionType)obj).IdTransmissionType;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpTransmissionType left, OpTransmissionType right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpTransmissionType left, OpTransmissionType right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdTransmissionType.GetHashCode();
        }
    #endregion
    }
#endif
}