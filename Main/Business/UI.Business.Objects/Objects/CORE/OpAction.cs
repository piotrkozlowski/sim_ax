using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.DW;
using System.Data;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAction : DB_ACTION, IComparable, IOpChangeState, IReferenceType, IEquatable<OpAction>, IOpDynamic, IOpDynamicProperty<OpAction>, IOpObject<OpActionData>
    {
    #region Properties
        public long IdAction { get { return this.ID_ACTION; } set { this.ID_ACTION = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
        public int IdActionType { get { return this.ID_ACTION_TYPE; } set { this.ID_ACTION_TYPE = value; } }
        public int IdActionStatus { get { return this.ID_ACTION_STATUS; } set { this.ID_ACTION_STATUS = value; } }
        public long IdActionData { get { return this.ID_ACTION_DATA; } set { this.ID_ACTION_DATA = value; } }
        public long? IdActionParent { get { return this.ID_ACTION_PARENT; } set { this.ID_ACTION_PARENT = value; } }
        public long? IdDataArch { get { return this.ID_DATA_ARCH; } set { this.ID_DATA_ARCH = value; } }
        public int? IdModule { get { return this.ID_MODULE; } set { this.ID_MODULE = value; } }
        public int? IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        public DateTime CreationDate { get { return this.CREATION_DATE; } set { this.CREATION_DATE = value; } }
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
        private OpActionType _ActionType;
        public OpActionType ActionType { get { return this._ActionType; } set { this._ActionType = value; this.ID_ACTION_TYPE = (value == null) ? 0 : (int)value.ID_ACTION_TYPE; } }
        private OpActionStatus _ActionStatus;
        public OpActionStatus ActionStatus { get { return this._ActionStatus; } set { this._ActionStatus = value; this.ID_ACTION_STATUS = (value == null) ? 0 : (int)value.ID_ACTION_STATUS; } }
        private OpActionData _ActionData;
        public OpActionData ActionData { get { return this._ActionData; } set { this._ActionData = value; this.ID_ACTION_DATA = (value == null) ? 0 : (long)value.ID_ACTION_DATA; } }
        private OpDataArch _DataArch;
        public OpDataArch DataArch { get { return this._DataArch; } set { this._DataArch = value; this.ID_DATA_ARCH = (value == null) ? null : (long?)value.ID_DATA_ARCH; } }
        private OpModule _Module;
        public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null) ? null : (int?)value.ID_MODULE; } }
        private OpOperator _Operator;
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? null : (int?)value.ID_OPERATOR; } }
    #endregion

    #region	Custom Properties
        public string Target
        {
            get
            {
                if (Location != null)
                    return String.Format("Location [{0}]", IdLocation);
                if (Meter != null)
                    return String.Format("Meter [{0}]", IdMeter);
                if (Device != null)
                    return String.Format("Device [{0}]", SerialNbr);

                return "[unknown]";
            }
        }

        public string DispModule
        {
            get
            {
                if (Operator != null && Module != null)
                    return string.Format("{0} [{1}]", Module.ToString(), Operator);
                else if (Module != null)
                    return Module.ToString();
                else if (Operator != null)
                    return string.Format("[{0}]", Operator);
                else
                    return "";
            }
        }

        public object ResponseGridColumn
        {
            get;
            set;
        }

        private Dictionary<string, object> dynamicValues;
        private OpDynamicPropertyDict dynamicProperties;

        // sdudzik - dodatkowa informacja identyfikujaca wpisy z uwzglednieniem serwerow, z ktorych akcje zostaly pobrane. Zwiazane z DBCollector-em
        public string GUID { get { return String.Format("{0}-{1}", this.ID_SERVER, this.ID_ACTION); } }
        public string GUIDParent { get { if (this.ID_ACTION_PARENT.HasValue) return String.Format("{0}-{1}", this.ID_SERVER, this.ID_ACTION_PARENT.Value); else return null; } }

        #endregion

        #region	Ctor
        public OpAction()
            : base()
        {
            //ActionHistory = new List<OpActionHistory>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
            DataList = new OpDataList<OpActionData>();
            this.OpState = OpChangeState.New;
        }

        public OpAction(DB_ACTION clone)
            : base(clone)
        {
            //ActionHistory = new List<OpActionHistory>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
            DataList = new OpDataList<OpActionData>();
            this.OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "Action [" + IdAction.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpAction> ConvertList(DB_ACTION[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAction> ConvertList(DB_ACTION[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpAction> ret = new List<OpAction>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAction(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAction> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Where(l => l.SerialNbr.HasValue).Select(l => l.SerialNbr.Value).Distinct().ToArray()).ToDictionary(l => l.SerialNbr);
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_LOCATION);
                Dictionary<int, OpActionType> ActionTypeDict = dataProvider.GetActionType(list.Select(l => l.ID_ACTION_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTION_TYPE);
                Dictionary<int, OpActionStatus> ActionStatusDict = dataProvider.GetActionStatus(list.Select(l => l.ID_ACTION_STATUS).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTION_STATUS);
                //Dictionary<long, OpActionData> ActionDataDict = dataProvider.GetActionData(list.Select(l => l.ID_ACTION_DATA).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_ACTION_DATA);
                //Dictionary<long, OpDataArch> DataArchDict = dataProvider.GetDataArch(list.Where(l => l.ID_DATA_ARCH.HasValue).Select(l => l.ID_DATA_ARCH.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_ARCH);
                Dictionary<int, OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_MODULE);
                Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Where(l => l.ID_OPERATOR.HasValue).Select(l => l.ID_OPERATOR.Value).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_OPERATOR);

                foreach (var loop in list)
                {
                    if (loop.SerialNbr.HasValue)
                        loop.Device = DeviceDict.TryGetValue(loop.SerialNbr.Value);
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                    loop.ActionType = ActionTypeDict.TryGetValue(loop.ID_ACTION_TYPE);
                    loop.ActionStatus = ActionStatusDict.TryGetValue(loop.ID_ACTION_STATUS);
                    //loop.ActionData = ActionDataDict.TryGetValue(loop.ID_ACTION_DATA);
                    //if (loop.ID_DATA_ARCH.HasValue)
                    //    loop.DataArch = DataArchDict.TryGetValue(loop.ID_DATA_ARCH.Value);
                    if (loop.ID_MODULE.HasValue)
                        loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value);
                    if (loop.ID_OPERATOR.HasValue)
                        loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR.Value);
                }
            }
        }
        #endregion

        #region LoadCustomData
        public static void LoadCustomData(ref List<OpAction> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAction)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAction).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpAction> Members
        public bool Equals(OpAction other)
        {
            if (other == null)
                return false;
            return this.IdAction.Equals(other.IdAction);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAction)
                return this.IdAction == ((OpAction)obj).IdAction;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAction left, OpAction right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAction left, OpAction right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAction.GetHashCode();
        }
    #endregion

    #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdAction; }
        }

        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpAction IOpDynamicProperty<OpAction>.Owner
        {
            get { return this; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
        #endregion
        #region Implementation of IOpObject
        public object IdObject
        {
            get { return IdAction; }
            set { IdAction = Convert.ToInt64(value); }
        }
        
        public OpDataList<OpActionData> DataList { get; set; }
        #endregion

        #region Implementation of IReferenceType
        public object GetReferenceKey()
    {
        return this.IdAction;
    }

    public object GetReferenceValue()
    {
        return this;
    }
    #endregion
    }
#endif
}