using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
     [Serializable]
    public class OpSessionData : DB_SESSION_DATA, IOpData, IOpDataProvider, IComparable
    {
    #region Properties
		public long IdSessionData { get { return this.ID_SESSION_DATA; } set { this.ID_SESSION_DATA = value; } }
		public long IdSession { get { return this.ID_SESSION; } set { this.ID_SESSION = value; } }
		public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
		public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
    #endregion

    #region Navigation Properties
		private OpSession _Session;
		public OpSession Session { get { return this._Session; } set { this._Session = value; this.ID_SESSION = (value == null) ? 0 : (long)value.ID_SESSION; } }
		private OpDataType _DataType;
		public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
		public long IdData { get { return IdSessionData; } set { IdSessionData = value; } }
		public int Index { get; set; }
		public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
		public OpSessionData()
			: base() { this.OpState = OpChangeState.New; }

		public OpSessionData(DB_SESSION_DATA clone)
			: base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
		public override string ToString()
		{
			return "SessionData [" + IdSessionData.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpSessionData> ConvertList(DB_SESSION_DATA[] list, IDataProvider dataProvider)
		{
			return ConvertList(list, dataProvider, true);
		}

		public static List<OpSessionData> ConvertList(DB_SESSION_DATA[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSessionData> ret = new List<OpSessionData>(list.Length);
			foreach (var loop in list)
			{
				OpSessionData insert = new OpSessionData(loop);
				if (loadNavigationProperties)
				{
                    //insert.Session = dataProvider.GetSession(loop.ID_SESSION);
					insert.DataType = dataProvider.GetDataType(loop.ID_DATA_TYPE);
				}

				ret.Add(insert);
			}

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSessionData> list, IDataProvider dataProvider)
		{
		}
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
		{
			//this.Session = dataProvider.GetSession(this.ID_SESSION);
			this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
		}
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpSessionData clone = new OpSessionData();
            clone.IdData = this.IdData;
            clone.IdSession = this.IdSession;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_SESSION_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
		public int CompareTo(object obj)
		{
			if (obj is OpSessionData)
			{
				if (this.ToString() != null)
					return this.ToString().CompareTo((obj as OpSessionData).ToString());
				else
					return 0;
			}
			else
				return 0;
		}
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpSessionData)
				return this.IdData == ((OpSessionData)obj).IdData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpSessionData left, OpSessionData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpSessionData left, OpSessionData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdData.GetHashCode();
		}
    #endregion
	}
#endif
}