using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActorInGroup : DB_ACTOR_IN_GROUP, IComparable, IEquatable<OpActorInGroup>, IOpChangeState
    {
    #region Properties
        public int IdActor
        {
            get { return this.ID_ACTOR; }
            set { if (this.ID_ACTOR != value) { this.ID_ACTOR = value; this.OpState = OpChangeState.Modified; } }
        }
        public int IdActorGroup
        {
            get { return this.ID_ACTOR_GROUP; }
            set { if (this.ID_ACTOR_GROUP != value) { this.ID_ACTOR_GROUP = value; this.OpState = OpChangeState.Modified; } }
        }

    #endregion

    #region	Navigation Properties
        private OpActor _Actor;
        public OpActor Actor { get { return this._Actor; } set { this._Actor = value; this.ID_ACTOR = (value == null) ? 0 : (int)value.ID_ACTOR; } }
        private OpActorGroup _ActorGroup;
        public OpActorGroup ActorGroup { get { return this._ActorGroup; } set { this._ActorGroup = value; this.ID_ACTOR_GROUP = (value == null) ? 0 : (int)value.ID_ACTOR_GROUP; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpActorInGroup()
            : base()
        {
            this.OpState = OpChangeState.New;
        }

        public OpActorInGroup(DB_ACTOR_IN_GROUP clone)
            : base(clone)
        {
            this.OpState = OpChangeState.Loaded;
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ActorInGroup [" + IdActor.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpActorInGroup> ConvertList(DB_ACTOR_IN_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActorInGroup> ConvertList(DB_ACTOR_IN_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActorInGroup> ret = new List<OpActorInGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActorInGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActorInGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpActorGroup> ActorGroupDict = dataProvider.GetActorGroup(list.Select(l => l.ID_ACTOR_GROUP).Distinct().ToArray()).ToDictionary(l => l.ID_ACTOR_GROUP);
                Dictionary<int, OpActor> ActorDict = dataProvider.GetActor(list.Select(l => l.ID_ACTOR).Distinct().ToArray()).ToDictionary(l => l.ID_ACTOR);
                foreach (var loop in list)
                {
                    loop.ActorGroup = ActorGroupDict.TryGetValue(loop.ID_ACTOR_GROUP);
                    loop.Actor = ActorDict.TryGetValue(loop.ID_ACTOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActorInGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActorInGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActorInGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActorInGroup> Members
        public bool Equals(OpActorInGroup other)
        {
            if (other == null)
                return false;
            return this.IdActor.Equals(other.IdActor) && this.IdActorGroup.Equals(other.IdActorGroup);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActorInGroup)
                return this.IdActor == ((OpActorInGroup)obj).IdActor && this.IdActorGroup == ((OpActorInGroup)obj).IdActorGroup;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActorInGroup left, OpActorInGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActorInGroup left, OpActorInGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActor.GetHashCode() ^ IdActorGroup.GetHashCode();
        }
    #endregion
    }
#endif
}