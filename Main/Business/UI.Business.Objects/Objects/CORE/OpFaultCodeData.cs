using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpFaultCodeData : DB_FAULT_CODE_DATA, IComparable, IEquatable<OpFaultCodeData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdFaultCodeData { get { return this.ID_FAULT_CODE_DATA;} set {this.ID_FAULT_CODE_DATA = value;}}
        public int IdFaultCode { get { return this.ID_FAULT_CODE;} set {this.ID_FAULT_CODE = value;}}
        public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
        public int IndexNbr { get { return this.INDEX_NBR;} set {this.INDEX_NBR = value;}}
        public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
    #endregion

    #region	Navigation Properties
		private OpFaultCode _FaultCode;
				public OpFaultCode FaultCode { get { return this._FaultCode; } set { this._FaultCode = value; this.ID_FAULT_CODE = (value == null)? 0 : (int)value.ID_FAULT_CODE; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion
		
    #region	Ctor
		public OpFaultCodeData()
			:base() {}
		
		public OpFaultCodeData(DB_FAULT_CODE_DATA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "FaultCodeData [" + IdFaultCodeData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpFaultCodeData> ConvertList(DB_FAULT_CODE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpFaultCodeData> ConvertList(DB_FAULT_CODE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpFaultCodeData> ret = new List<OpFaultCodeData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpFaultCodeData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpFaultCodeData> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			//Dictionary<int,OpFaultCode> FaultCodeDict = dataProvider.GetFaultCode(list.Select(l => l.ID_FAULT_CODE).Distinct().ToArray()).ToDictionary(l => l.ID_FAULT_CODE);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
						//loop.FaultCode = FaultCodeDict.TryGetValue(loop.ID_FAULT_CODE); 
		loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpFaultCodeData> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpFaultCodeData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpFaultCodeData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpFaultCodeData> Members
        public bool Equals(OpFaultCodeData other)
        {
            if (other == null)
				return false;
			return this.IdFaultCodeData.Equals(other.IdFaultCodeData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpFaultCodeData)
				return this.IdFaultCodeData == ((OpFaultCodeData)obj).IdFaultCodeData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpFaultCodeData left, OpFaultCodeData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpFaultCodeData left, OpFaultCodeData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdFaultCodeData.GetHashCode();
		}
    #endregion

    #region IOpData

        public long IdData { get { return IdFaultCodeData; } set { IdFaultCodeData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

        public OpChangeState OpState
        {
            get;
            set;
        }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpFaultCodeData clone = new OpFaultCodeData();
            clone.IdData = this.IdData;
            clone.IdFaultCode = this.IdFaultCode;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_FAULT_CODE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}