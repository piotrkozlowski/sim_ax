using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpMeter : DB_METER, IOpChangeState, IComparable, IReferenceType, IOpDynamic, IOpDynamicProperty<OpMeter>, IOpDataProperty, IOpObject<OpData>
    {
    #region Properties
        [XmlIgnore]
        public long IdMeter
		{
			get { return this.ID_METER; }
            set { if (this.ID_METER != value) { this.ID_METER = OpDataUtil.SetNewValue(this.ID_METER, value, this); } }// this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public int IdMeterType
		{
			get { return this.ID_METER_TYPE; }
            set { if (this.ID_METER_TYPE != value) { this.ID_METER_TYPE = OpDataUtil.SetNewValue(this.ID_METER_TYPE, value, this); } }// this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public long? IdMeterPattern
		{
			get { return this.ID_METER_PATTERN; }
            set { if (this.ID_METER_PATTERN != value) { this.ID_METER_PATTERN = OpDataUtil.SetNewValue(this.ID_METER_PATTERN, value, this); } }// this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public long? IdDescrPattern
		{
			get { return this.ID_DESCR_PATTERN; }
            set { if (this.ID_DESCR_PATTERN != value) { this.ID_DESCR_PATTERN = OpDataUtil.SetNewValue(this.ID_DESCR_PATTERN, value, this); } }// this.OpState = OpChangeState.Modified; } }
		}
        [XmlIgnore]
        public int IdDistributor
		{
			get { return this.ID_DISTRIBUTOR; }
            set { if (this.ID_DISTRIBUTOR != value) { this.ID_DISTRIBUTOR = OpDataUtil.SetNewValue(this.ID_DISTRIBUTOR, value, this); } }// this.OpState = OpChangeState.Modified; } }
		}
    #endregion

    #region Navigation Properties
        private OpMeterType _MeterType;
        [XmlIgnore]
        public OpMeterType MeterType { get { return this._MeterType; } set { this._MeterType = value; this.IdMeterType = (value == null) ? 0 : (int)value.ID_METER_TYPE; } }
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.IdDistributor = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
		public OpChangeState OpState { get; set; }
        [XmlIgnore]
		public string GridLookUpEditString { get { return this.ToString(); } }
        [XmlIgnore]
        public string MeterSerialNbr
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.METER_SERIAL_NUMBER);
            }
            set
            {
                DataList.SetValue(DataType.METER_SERIAL_NUMBER, value);
            }
        }
        [XmlIgnore]
        public double? TankCapacity
        {
            get
            {
                return DataList.TryGetNullableValue<double>(DataType.METER_TANK_CAPACITY);
            }
            set
            {
                DataList.SetValue(DataType.METER_TANK_CAPACITY, value);
            }
        }
        [XmlIgnore]
        public int? IdProductCode
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.ATG_PARAMS_PRODUCT_CODE);
            }
            set
            {
                DataList.SetValue(DataType.ATG_PARAMS_PRODUCT_CODE, value);
            }
        }
        [XmlIgnore]
        public int? TankNbr
        {
            get
            {
                return DataList.TryGetNullableValue<int>(DataType.ATG_PARAMS_TANK_NUMBER);
            }
            set
            {
                DataList.SetValue(DataType.ATG_PARAMS_TANK_NUMBER, value);
            }
        }
        [XmlIgnore]
        public string TankName
        {
            get
            {
                return DataList.TryGetValue<string>(DataType.METER_NAME);
            }
            set
            {
                DataList.SetValue(DataType.METER_NAME, value);
            }
        }
        private OpProductCode _ProductCode;
        [XmlIgnore]
        public OpProductCode ProductCode
        {
            get { return _ProductCode; }
            set
            {
                _ProductCode = value;
                if (value != null)
                {
                    IdProductCode = value.IdProductCode;
                }
                else
                {
                    IdProductCode = null;
                }
            }
        }

        [XmlIgnore]
        public DateTime? InstallationDate { get; set; }

        [XmlIgnore]
        public List<OpLocation> CurrentLocations;
        public List<OpDevice> CurrentDevices;
        [XmlIgnore]
        public OpDevice CurrentDevice { get { return CurrentDevices.FirstOrDefault(); } }
        [XmlIgnore]
        public OpLocation CurrentLocation { get { return CurrentLocations.FirstOrDefault(); } }

        [XmlIgnore]
        private Dictionary<string, object> dynamicValues;
        [XmlIgnore]
        private OpDynamicPropertyDict dynamicProperties;
    #endregion

    #region Ctor
        public OpMeter()
 			: base()
        {
			this.OpState = OpChangeState.New;
            this.DataList = new OpDataList<OpData>();
            this.CurrentDevices = new List<OpDevice>();
            this.CurrentLocations = new List<OpLocation>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpMeter(DB_METER clone)
			: base(clone)
        {
			this.OpState = OpChangeState.Loaded;
            this.DataList = new OpDataList<OpData>();
            this.CurrentDevices = new List<OpDevice>();
            this.CurrentLocations = new List<OpLocation>();
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }
    #endregion

    #region ToString
        public override string ToString()
        {
            if (!String.IsNullOrWhiteSpace(this.MeterSerialNbr))
                return String.Format("{0}: {1}", MeterType, MeterSerialNbr);
			return String.Format("{0}: {1}", MeterType, IdMeter);
        }
    #endregion

    #region ConvertList
        public static List<OpMeter> ConvertList(DB_METER[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMeter> ret = new List<OpMeter>(db_objects.Length);
            db_objects.ToList().ForEach(db_object =>
                {
                    OpMeter meter = new OpMeter(db_object);
                    if (meter.U_DATA_ARRAY != null && meter.U_DATA_ARRAY.Length > 0)
                        meter.DataList.AddRange(OpData.ConvertList(meter.U_DATA_ARRAY, dataProvider));
                    ret.Add(meter);
                }
            );

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider, customDataTypes, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data

            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpMeter> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpMeterType> MeterTypeDict = dataProvider.GetMeterType(list.Select(l => l.ID_METER_TYPE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_METER_TYPE);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DISTRIBUTOR);
                foreach (var loop in list)
                {
                    loop.MeterType = MeterTypeDict.TryGetValue(loop.ID_METER_TYPE);
                    loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        public static void LoadCustomData(ref List<OpMeter> list, IDataProvider dataProvider, List<long> customDataTypes = null,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                List<long> idMeter = list.Select(l => l.IdMeter).ToList();
                List<OpData> data = new List<OpData>();

                if (customDataTypes != null && customDataTypes.Count > 0)
                    data = dataProvider.GetDataFilter(IdMeter: idMeter.ToArray(), IdDataType: customDataTypes.ToArray(), transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);
                else if (dataProvider.IMeterDataTypes != null && dataProvider.IMeterDataTypes.Count > 0)
                    data = dataProvider.GetDataFilter(IdMeter: idMeter.ToArray(), IdDataType: dataProvider.IMeterDataTypes.ToArray(), transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);

                if (data.Count > 0)
                {
                    Dictionary<long, OpMeter> metersDict = list.ToDictionary<OpMeter, long>(l => l.IdMeter);
                    data.ForEach(w => metersDict[w.IdMeter.Value].DataList.Add(w));
                }

                foreach (var meter in list)
                {
                    if (meter.IdProductCode.HasValue)
                        meter.ProductCode = dataProvider.GetProductCode(new int[]{ meter.IdProductCode.Value }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();
                }
            }
        }
    #endregion

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            MeterType = dataProvider.GetMeterType(this.ID_METER_TYPE);
            Distributor = dataProvider.GetDistributor(this.ID_DISTRIBUTOR);
        }
    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpMeter)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpMeter).ToString());
                else
                    return -1;
            }
            else
                return -1;
        }
    #endregion

    #region IEquatable<OpMeter> Members
        public bool Equals(OpMeter other)
        {
            if (other != null)
                return this.IdMeter.Equals(other.IdMeter);
            else
                return false;
        }
    #endregion

    #region Clone
		public OpMeter Clone(IDataProvider dataProvider)
        {
            OpMeter clone = new OpMeter();
            clone.IdMeter = this.IdMeter;
            clone.IdMeterType = this.IdMeterType;
            clone.IdMeterPattern = this.IdMeterPattern;
            clone.IdDescrPattern = this.IdDescrPattern;
            clone.IdDistributor = this.IdDistributor;

			clone.DataList = this.DataList.Clone(dataProvider);

			if (dataProvider != null)
				clone = ConvertList(new DB_METER[] { clone }, dataProvider)[0];

			clone.OpState = this.OpState;
			return clone;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpMeter)
                return this.IdMeter == ((OpMeter)obj).IdMeter;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpMeter left, OpMeter right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpMeter left, OpMeter right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdMeter.GetHashCode();
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdMeter;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdMeter; }
        }

        [XmlIgnore]
        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpMeter IOpDynamicProperty<OpMeter>.Owner
        {
            get { return this; }
        }

        [XmlIgnore]
        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
    #endregion
    #region Implementation of IOpDataProperty
        public object GetOpDataPropertyValue(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return null;

            OpData opData = null;
            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.CurrentLocation != null)
                        return this.CurrentLocation.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
                case Enums.ReferenceType.SerialNbr:
                    if (this.CurrentDevice != null)
                        return this.CurrentDevice.GetOpDataPropertyValue(opDataProperty);
                    else
                        return null;
            }

            opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opDataProperty.OpDataType.IdReferenceType.HasValue && opData.ReferencedObject != null)
                    return opData.ReferencedObject;

                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && opData.Value != null)
                    return OpUnit.ChangeUnit(opData.Value, opDataProperty.OpDataType, opData.DataType.Unit, opDataProperty.OpUnit, OpDataTypeFormat.GetPrecision(opDataProperty.OpDataTypeFormat)); // (Convert.ToDouble(opData.Value) - opDataProperty.OpUnit.Bias) / opDataProperty.OpUnit.Scale;

                return opData.Value;
            }

            return null;
        }
        public void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return;

            OpData opData = null;
            switch (opDataProperty.ReferenceType)
            {
                case Enums.ReferenceType.IdLocation:
                    if (this.CurrentLocation != null)
                        this.CurrentLocation.SetOpDataPropertyValue(opDataProperty, value);
                    return;
                case Enums.ReferenceType.SerialNbr:
                    if (this.CurrentDevice != null)
                        this.CurrentDevice.SetOpDataPropertyValue(opDataProperty, value);
                    return;
            }

            opData = GetColumnOpData(opDataProperty.OpDataType);

            if (opData == null)
                opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && value != null)
                    opData.Value = OpUnit.ChangeUnit(value, opDataProperty.OpDataType, opDataProperty.OpUnit, opData.DataType.Unit); //Convert.ToDouble(value) * opDataProperty.OpUnit.Scale + opDataProperty.OpUnit.Bias;
                else
                    opData.Value = value;
            }           
        }
        private OpData GetColumnOpData(OpDataType dataType)
        {
            if (dataType == null) return null;
            switch (dataType.IdDataType)
            {
                case DataType.HELPER_ID_METER:
                    return new OpData() { DataType = dataType, Value = this.IdMeter, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_METER_TYPE:
                    return new OpData() { DataType = dataType, Value = this.MeterType, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_METER_TYPE_CLASS:
                    return new OpData() { DataType = dataType, Value = this.MeterType != null ? this.MeterType.MeterTypeClass : null, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_ID_DISTRIBUTOR:
                    return new OpData() { DataType = dataType, Value = this.Distributor, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
                case DataType.HELPER_INSTALLATION_DATE:
                    return new OpData() { DataType = dataType, Value = this.InstallationDate, OpState = OpChangeState.Loaded, DataStatus = Enums.DataStatus.Normal };
            }
            return null;
        }

        public Type GetOpDataPropertyType(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null || !opDataProperty.ReturnReferencedTypeIfHelper.HasValue) return null;
            Type type = Utils.GetHelperType(opDataProperty.OpDataType, opDataProperty.ReturnReferencedTypeIfHelper.Value);

            if(type == null)
                type = DataType.GetSystemType(opDataProperty.OpDataType.IdDataTypeClass);

            return type;
        }
        #endregion
        #region Implementation of IOpObject
    [XmlIgnore]
    public object IdObject
    {
        get { return IdMeter; }
        set { IdMeter = Convert.ToInt64(value); }
    }
        
    public OpDataList<OpData> DataList { get; set; }
    #endregion
    }
#endif
}