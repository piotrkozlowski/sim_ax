using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTypeProfileMapConfig : DB_DEVICE_TYPE_PROFILE_MAP_CONFIG, IComparable, IEquatable<OpDeviceTypeProfileMapConfig>
    {
    #region Properties
            public int IdDeviceTypeProfileMap { get { return this.ID_DEVICE_TYPE_PROFILE_MAP;} set {this.ID_DEVICE_TYPE_PROFILE_MAP = value;}}
                public int IdDeviceTypeProfileStep { get { return this.ID_DEVICE_TYPE_PROFILE_STEP;} set {this.ID_DEVICE_TYPE_PROFILE_STEP = value;}}
                public int IdDeviceTypeProfileStepKind { get { return this.ID_DEVICE_TYPE_PROFILE_STEP_KIND;} set {this.ID_DEVICE_TYPE_PROFILE_STEP_KIND = value;}}
    #endregion

    #region	Navigation Properties
		private OpDeviceTypeProfileMap _DeviceTypeProfileMap;
				public OpDeviceTypeProfileMap DeviceTypeProfileMap { get { return this._DeviceTypeProfileMap; } set { this._DeviceTypeProfileMap = value; this.ID_DEVICE_TYPE_PROFILE_MAP = (value == null)? 0 : (int)value.ID_DEVICE_TYPE_PROFILE_MAP; } }
				private OpDeviceTypeProfileStep _DeviceTypeProfileStep;
				public OpDeviceTypeProfileStep DeviceTypeProfileStep { get { return this._DeviceTypeProfileStep; } set { this._DeviceTypeProfileStep = value; this.ID_DEVICE_TYPE_PROFILE_STEP = (value == null)? 0 : (int)value.ID_DEVICE_TYPE_PROFILE_STEP; } }
				private OpDeviceTypeProfileStepKind _DeviceTypeProfileStepKind;
				public OpDeviceTypeProfileStepKind DeviceTypeProfileStepKind { get { return this._DeviceTypeProfileStepKind; } set { this._DeviceTypeProfileStepKind = value; this.ID_DEVICE_TYPE_PROFILE_STEP_KIND = (value == null)? 0 : (int)value.ID_DEVICE_TYPE_PROFILE_STEP_KIND; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpDeviceTypeProfileMapConfig()
			:base() {OpState = OpChangeState.New;}
		
		public OpDeviceTypeProfileMapConfig(DB_DEVICE_TYPE_PROFILE_MAP_CONFIG clone)
			:base(clone) {OpState = OpChangeState.Loaded;}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "DeviceTypeProfileMapConfig [" + IdDeviceTypeProfileMap.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpDeviceTypeProfileMapConfig> ConvertList(DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDeviceTypeProfileMapConfig> ConvertList(DB_DEVICE_TYPE_PROFILE_MAP_CONFIG[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceTypeProfileMapConfig> ret = new List<OpDeviceTypeProfileMapConfig>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceTypeProfileMapConfig(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDeviceTypeProfileMapConfig> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpDeviceTypeProfileStep> DeviceTypeProfileStepDict = dataProvider.GetDeviceTypeProfileStep(list.Select(l => l.ID_DEVICE_TYPE_PROFILE_STEP).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE_PROFILE_STEP);
	Dictionary<int,OpDeviceTypeProfileStepKind> DeviceTypeProfileStepKindDict = dataProvider.GetDeviceTypeProfileStepKind(list.Select(l => l.ID_DEVICE_TYPE_PROFILE_STEP_KIND).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE_PROFILE_STEP_KIND);			
				foreach (var loop in list)
				{
						loop.DeviceTypeProfileStep = DeviceTypeProfileStepDict.TryGetValue(loop.ID_DEVICE_TYPE_PROFILE_STEP); 
		loop.DeviceTypeProfileStepKind = DeviceTypeProfileStepKindDict.TryGetValue(loop.ID_DEVICE_TYPE_PROFILE_STEP_KIND); 				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceTypeProfileMapConfig> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTypeProfileMapConfig)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTypeProfileMapConfig).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDeviceTypeProfileMapConfig> Members
        public bool Equals(OpDeviceTypeProfileMapConfig other)
        {
            if (other == null)
				return false;
			return this.IdDeviceTypeProfileMap.Equals(other.IdDeviceTypeProfileMap);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDeviceTypeProfileMapConfig)
				return this.IdDeviceTypeProfileMap == ((OpDeviceTypeProfileMapConfig)obj).IdDeviceTypeProfileMap;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceTypeProfileMapConfig left, OpDeviceTypeProfileMapConfig right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceTypeProfileMapConfig left, OpDeviceTypeProfileMapConfig right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDeviceTypeProfileMap.GetHashCode();
		}
    #endregion
    }
#endif
}