﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceOrderNumberData : DB_DEVICE_ORDER_NUMBER_DATA, IComparable, IEquatable<OpDeviceOrderNumberData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdDeviceOrderNumberData { get { return this.ID_DEVICE_ORDER_NUMBER_DATA; } set { this.ID_DEVICE_ORDER_NUMBER_DATA = value; } }
        public int IdDeviceOrderNumber { get { return this.ID_DEVICE_ORDER_NUMBER; } set { this.ID_DEVICE_ORDER_NUMBER = value; } }
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
        private OpDeviceOrderNumber _DeviceOrderNumber;
        public OpDeviceOrderNumber DeviceOrderNumber { get { return this._DeviceOrderNumber; } set { this._DeviceOrderNumber = value; this.ID_DEVICE_ORDER_NUMBER = (value == null) ? 0 : (int)value.ID_DEVICE_ORDER_NUMBER; } }
        private OpDataType _DataType;
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpDeviceOrderNumberData()
            : base() { }

        public OpDeviceOrderNumberData(DB_DEVICE_ORDER_NUMBER_DATA clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "DeviceOrderNumberData [" + IdDeviceOrderNumberData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpDeviceOrderNumberData> ConvertList(DB_DEVICE_ORDER_NUMBER_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDeviceOrderNumberData> ConvertList(DB_DEVICE_ORDER_NUMBER_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceOrderNumberData> ret = new List<OpDeviceOrderNumberData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceOrderNumberData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceOrderNumberData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
             //   Dictionary<int, OpDeviceOrderNumber> DeviceOrderNumberDict = dataProvider.GetDeviceOrderNumber(list.Select(l => l.ID_DEVICE_ORDER_NUMBER).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_ORDER_NUMBER);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                  //  loop.DeviceOrderNumber = DeviceOrderNumberDict.TryGetValue(loop.ID_DEVICE_ORDER_NUMBER);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceOrderNumberData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceOrderNumberData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceOrderNumberData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpDeviceOrderNumberData> Members
        public bool Equals(OpDeviceOrderNumberData other)
        {
            if (other == null)
                return false;
            return this.IdDeviceOrderNumberData.Equals(other.IdDeviceOrderNumberData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceOrderNumberData)
                return this.IdDeviceOrderNumberData == ((OpDeviceOrderNumberData)obj).IdDeviceOrderNumberData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceOrderNumberData left, OpDeviceOrderNumberData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceOrderNumberData left, OpDeviceOrderNumberData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdDeviceOrderNumberData.GetHashCode();
        }
    #endregion

    #region IOpData

        public long IdData { get { return IdDeviceOrderNumberData; } set { IdDeviceOrderNumberData = value; } }

        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpDeviceOrderNumberData clone = new OpDeviceOrderNumberData();
            clone.IdData = this.IdData;
            clone.IdDeviceOrderNumber = this.IdDeviceOrderNumber;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEVICE_ORDER_NUMBER_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}