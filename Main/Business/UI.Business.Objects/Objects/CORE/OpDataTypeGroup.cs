using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
//using IMR.Suite.UI.Business.Components.CORE;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataTypeGroup : DB_DATA_TYPE_GROUP, IComparable
    {
    #region Properties
        public int IdDataTypeGroup { get { return this.ID_DATA_TYPE_GROUP; } set { this.ID_DATA_TYPE_GROUP = value; } }
        public int IdDataTypeGroupType { get { return this.ID_DATA_TYPE_GROUP_TYPE; } set { this.ID_DATA_TYPE_GROUP_TYPE = value; } }
        public int? IdReferenceType { get { return this.ID_REFERENCE_TYPE; } set { this.ID_REFERENCE_TYPE = value; } }
        public object ReferenceValue { get { return this.REFERENCE_VALUE; } set { this.REFERENCE_VALUE = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public int? IdParentGroup { get { return this.ID_PARENT_GROUP; } set { this.ID_PARENT_GROUP = value; } }
    #endregion

    #region	Navigation Properties
        private OpDataTypeGroupType _DataTypeGroupType;
        public OpDataTypeGroupType DataTypeGroupType { get { return this._DataTypeGroupType; } set { this._DataTypeGroupType = value; this.ID_DATA_TYPE_GROUP_TYPE = (value == null) ? 0 : (int)value.ID_DATA_TYPE_GROUP_TYPE; } }
        private OpReferenceType _ReferenceType;
        public OpReferenceType ReferenceType { get { return this._ReferenceType; } set { this._ReferenceType = value; this.ID_REFERENCE_TYPE = (value == null) ? null : (int?)value.ID_REFERENCE_TYPE; } }
        private OpDataTypeGroup _ParentGroup;
        public OpDataTypeGroup ParentGroup { get { return this._ParentGroup; } set { this._ParentGroup = value; this.ID_PARENT_GROUP = (value == null) ? 0 : (int)value.ID_DATA_TYPE_GROUP; } }
    #endregion

    #region	Custom Properties
        public object Value { get; set; }
    #endregion

    #region	Ctor
        public OpDataTypeGroup()
            : base() { }

        public OpDataTypeGroup(DB_DATA_TYPE_GROUP clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpDataTypeGroup> ConvertList(DB_DATA_TYPE_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpDataTypeGroup> ConvertList(DB_DATA_TYPE_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool loadCustomData = true)
        {
            List<OpDataTypeGroup> ret = new List<OpDataTypeGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDataTypeGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            if (loadCustomData)
                LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDataTypeGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpDataTypeGroupType> DataTypeGroupTypeDict = dataProvider.GetDataTypeGroupType(list.Select(l => l.ID_DATA_TYPE_GROUP_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE_GROUP_TYPE);
                Dictionary<int, OpReferenceType> ReferenceTypeDict = dataProvider.GetReferenceType(list.Where(l => l.ID_REFERENCE_TYPE.HasValue).Select(l => l.ID_REFERENCE_TYPE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_REFERENCE_TYPE);
                Dictionary<int, OpDataTypeGroup> ParentGroupDict = dataProvider.GetDataTypeGroup(list.Where(l => l.ID_PARENT_GROUP.HasValue).Select(l => l.ID_PARENT_GROUP.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE_GROUP);

                foreach (var loop in list)
                {
                    loop.DataTypeGroupType = DataTypeGroupTypeDict.TryGetValue(loop.ID_DATA_TYPE_GROUP_TYPE);
                    if (loop.ID_REFERENCE_TYPE.HasValue)
                        loop.ReferenceType = ReferenceTypeDict.TryGetValue(loop.ID_REFERENCE_TYPE.Value);
                    if (loop.ID_PARENT_GROUP.HasValue)
                        loop.ParentGroup = ParentGroupDict.TryGetValue(loop.ID_PARENT_GROUP.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataTypeGroup> list, IDataProvider dataProvider)
        {
            foreach (var loop in list)
            {
    #region Value
                loop.Value = loop.ReferenceValue; //przypisujemy na wst�pie domysln� warto��, jesli jest podany IdReference to pr�bujemy wyciagnac wlasciwy obiekt poni�ej

                if (loop.IdReferenceType.HasValue && loop.ReferenceValue != null)
                {
                    try
                    {
                        loop.Value = dataProvider.GetReferenceObject((Enums.ReferenceType) loop.IdReferenceType.Value, loop.ReferenceValue);
                    }
                    catch
                    {
                    }
                }
    #endregion
            }
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataTypeGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataTypeGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}