using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpRouteDefLocation : DB_ROUTE_DEF_LOCATION, IComparable, IOpChangeState
    {
    #region Properties
        public long IdRouteDefLocation { get { return this.ID_ROUTE_DEF_LOCATION; } set { this.ID_ROUTE_DEF_LOCATION = OpDataUtil.SetNewValue(this.ID_ROUTE_DEF_LOCATION, value, this); } }
        public int IdRouteDef { get { return this.ID_ROUTE_DEF; } set { this.ID_ROUTE_DEF = OpDataUtil.SetNewValue(this.ID_ROUTE_DEF, value, this); } }
        public long IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = OpDataUtil.SetNewValue(this.ID_LOCATION, value, this); } }
        public int OrderNbr { get { return this.ORDER_NBR; } set { this.ORDER_NBR = OpDataUtil.SetNewValue(this.ORDER_NBR, value, this); } }
    #endregion

    #region	Navigation Properties
		private OpRouteDef _RouteDef;
        public OpRouteDef RouteDef { get { return this._RouteDef; } set { this._RouteDef = value; this.IdRouteDef = (value == null) ? 0 : (int)value.IdRouteDef; } }
		private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.IdLocation = (value == null) ? 0 : (long)value.IdLocation; } }
    #endregion

    #region	Custom Properties
        public OpDataList<OpRouteDefLocationData> DataList { get; set; }

        public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpRouteDefLocation()
			:base()
        {
            this.DataList = new OpDataList<OpRouteDefLocationData>();
            OpState = OpChangeState.New;
        }
		
		public OpRouteDefLocation(DB_ROUTE_DEF_LOCATION clone)
            : base(clone)
        {
            this.DataList = new OpDataList<OpRouteDefLocationData>();
            OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "RouteDefLocation [" + IdRouteDefLocation.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpRouteDefLocation> ConvertList(DB_ROUTE_DEF_LOCATION[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpRouteDefLocation> ConvertList(DB_ROUTE_DEF_LOCATION[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpRouteDefLocation> ret = new List<OpRouteDefLocation>(list.Length);
            Dictionary<int, OpRouteDef> routeDefDict = new Dictionary<int,OpRouteDef>();
            Dictionary<long, OpLocation> locationDict = new Dictionary<long,OpLocation>();
            if (loadNavigationProperties && list.Length > 0)
            {
                routeDefDict = dataProvider.GetRouteDef(list.Select(q => q.ID_ROUTE_DEF).Distinct().ToArray()).ToDictionary(q => q.ID_ROUTE_DEF);
                locationDict = dataProvider.GetLocation(list.Select(q => q.ID_LOCATION).Distinct().ToArray()).ToDictionary(q => q.ID_LOCATION);
            }
			foreach (var loop in list)
			{
			    OpRouteDefLocation insert = new OpRouteDefLocation(loop);
				if (loadNavigationProperties)
				{
                    insert.RouteDef = routeDefDict.TryGetValue(loop.ID_ROUTE_DEF);
                    insert.Location = locationDict.TryGetValue(loop.ID_LOCATION);
				}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpRouteDefLocation> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpRouteDefLocation)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpRouteDefLocation).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
    }
#endif
}