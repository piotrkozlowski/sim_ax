using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAtgType : DB_ATG_TYPE, IComparable, IReferenceType
    {
    #region Properties
            public int IdAtgType { get { return this.ID_ATG_TYPE;} set {this.ID_ATG_TYPE = value;}}
                public string Descr { get { return this.DESCR;} set {this.DESCR = value;}}
    #endregion

    #region	Navigation Properties
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpAtgType()
			:base() {}
		
		public OpAtgType(DB_ATG_TYPE clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.DESCR;
		}
    #endregion

    #region	ConvertList
		public static List<OpAtgType> ConvertList(DB_ATG_TYPE[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpAtgType> ConvertList(DB_ATG_TYPE[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpAtgType> ret = new List<OpAtgType>(list.Length);
			foreach (var loop in list)
			{
			OpAtgType insert = new OpAtgType(loop);
				
				if(loadNavigationProperties)
				{
											
											
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpAtgType> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAtgType)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAtgType).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return IdAtgType;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion
    }
#endif
}