﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpServiceInPackage : DB_SERVICE_IN_PACKAGE, IComparable, IEquatable<OpServiceInPackage>
    {
    #region Properties
        public long IdServiceInPackage { get { return this.ID_SERVICE_IN_PACKAGE; } set { this.ID_SERVICE_IN_PACKAGE = value; } }
        public long IdServicePackage { get { return this.ID_SERVICE_PACKAGE; } set { this.ID_SERVICE_PACKAGE = value; } }
        public int IdService { get { return this.ID_SERVICE; } set { this.ID_SERVICE = value; } }
    #endregion

    #region	Navigation Properties
        private OpServicePackage _ServicePackage;
        public OpServicePackage ServicePackage { get { return this._ServicePackage; } set { this._ServicePackage = value; this.ID_SERVICE_PACKAGE = (value == null) ? 0 : (long)value.ID_SERVICE_PACKAGE; } }
        private OpService _Service;
        public OpService Service { get { return this._Service; } set { this._Service = value; this.ID_SERVICE = (value == null) ? 0 : (int)value.ID_SERVICE; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpServiceInPackage()
            : base() { }

        public OpServiceInPackage(DB_SERVICE_IN_PACKAGE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ServiceInPackage [" + IdServiceInPackage.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpServiceInPackage> ConvertList(DB_SERVICE_IN_PACKAGE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpServiceInPackage> ConvertList(DB_SERVICE_IN_PACKAGE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpServiceInPackage> ret = new List<OpServiceInPackage>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpServiceInPackage(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpServiceInPackage> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpServicePackage> ServicePackageDict = dataProvider.GetServicePackage(list.Select(l => l.ID_SERVICE_PACKAGE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE_PACKAGE);
                Dictionary<int, OpService> ServiceDict = dataProvider.GetService(list.Select(l => l.ID_SERVICE).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_SERVICE);
                foreach (var loop in list)
                {
                    loop.ServicePackage = ServicePackageDict.TryGetValue(loop.ID_SERVICE_PACKAGE);
                    loop.Service = ServiceDict.TryGetValue(loop.ID_SERVICE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpServiceInPackage> list, IDataProvider dataProvider, 
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpServiceInPackage)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpServiceInPackage).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpServiceInPackage> Members
        public bool Equals(OpServiceInPackage other)
        {
            if (other == null)
                return false;
            return this.IdServiceInPackage.Equals(other.IdServiceInPackage);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpServiceInPackage)
                return this.IdServiceInPackage == ((OpServiceInPackage)obj).IdServiceInPackage;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpServiceInPackage left, OpServiceInPackage right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpServiceInPackage left, OpServiceInPackage right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdServiceInPackage.GetHashCode();
        }
    #endregion
    }
#endif
}