using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceHierarchyPattern : DB_DEVICE_HIERARCHY_PATTERN, IComparable, IOpChangeState
    {
    #region Properties
		public long IdDeviceHierarchyPattern { get { return this.ID_DEVICE_HIERARCHY_PATTERN;} set {this.ID_DEVICE_HIERARCHY_PATTERN = OpDataUtil.SetNewValue(this.ID_DEVICE_HIERARCHY_PATTERN, value, this);}}
        public int IdDeviceTypeParent { get { return this.ID_DEVICE_TYPE_PARENT; } set { this.ID_DEVICE_TYPE_PARENT = OpDataUtil.SetNewIndex(this.ID_DEVICE_TYPE_PARENT, value, this); } }
        public int IdSlotType { get { return this.ID_SLOT_TYPE; } set { this.ID_SLOT_TYPE = OpDataUtil.SetNewIndex(this.ID_SLOT_TYPE, value, this); } }
        public int SlotNbrMin { get { return this.SLOT_NBR_MIN; } set { this.SLOT_NBR_MIN = OpDataUtil.SetNewIndex(this.SLOT_NBR_MIN, value, this); } }
        public int SlotNbrMax { get { return this.SLOT_NBR_MAX; } set { this.SLOT_NBR_MAX = OpDataUtil.SetNewIndex(this.SLOT_NBR_MAX, value, this); } }
        public int? IdProtocolIn { get { return this.ID_PROTOCOL_IN; } set { this.ID_PROTOCOL_IN = OpDataUtil.SetNewValue(this.ID_PROTOCOL_IN, value, this); } }
        public int? IdProtocolOut { get { return this.ID_PROTOCOL_OUT; } set { this.ID_PROTOCOL_OUT = OpDataUtil.SetNewValue(this.ID_PROTOCOL_OUT, value, this); } }
        public int? IdDeviceType { get { return this.ID_DEVICE_TYPE; } set { this.ID_DEVICE_TYPE = OpDataUtil.SetNewValue(this.ID_DEVICE_TYPE, value, this); } }
        public int? IdDeviceTypeGroup { get { return this.ID_DEVICE_TYPE_GROUP; } set { this.ID_DEVICE_TYPE_GROUP = OpDataUtil.SetNewValue(this.ID_DEVICE_TYPE_GROUP, value, this); } }
    #endregion

    #region Navigation Properties
		private OpSlotType _SlotType;
		public OpSlotType SlotType { get { return this._SlotType; } set { this._SlotType = value; this.IdSlotType = (value == null)? 0 : (int)value.ID_SLOT_TYPE; } }
		private OpDeviceType _DeviceType;
		public OpDeviceType DeviceType { get { return this._DeviceType; } set { this._DeviceType = value; this.IdDeviceType = (value == null)? null : (int?)value.ID_DEVICE_TYPE; } }
		private OpDeviceTypeGroup _DeviceTypeGroup;
		public OpDeviceTypeGroup DeviceTypeGroup { get { return this._DeviceTypeGroup; } set { this._DeviceTypeGroup = value; this.IdDeviceTypeGroup = (value == null)? null : (int?)value.ID_DEVICE_TYPE_GROUP; } }

        private OpDeviceType _DeviceTypeParent;
        public OpDeviceType DeviceTypeParent { get { return this._DeviceTypeParent; } set { this._DeviceTypeParent = value; this.IdDeviceTypeParent = (value == null) ? 0 : value.IdDeviceType; } }
        private OpProtocol _ProtocolIn;
        public OpProtocol ProtocolIn { get { return this._ProtocolIn; } set { this._ProtocolIn = value; this.IdProtocolIn = (value == null) ? null : (int?)value.ID_PROTOCOL; } }
        private OpProtocol _ProtocolOut;
        public OpProtocol ProtocolOut { get { return this._ProtocolOut; } set { this._ProtocolOut = value; this.IdProtocolOut = (value == null) ? null : (int?)value.ID_PROTOCOL; } }
    #endregion

    #region Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion
		
    #region Ctor
		public OpDeviceHierarchyPattern()
			:base() 
        {
            OpState = OpChangeState.New;
        }
		
		public OpDeviceHierarchyPattern(DB_DEVICE_HIERARCHY_PATTERN clone)
			:base(clone) 
        {
            OpState = OpChangeState.Loaded;
        }
    #endregion
		
    #region ToString
		public override string ToString()
		{
				return "DeviceHierarchyPattern [" + IdDeviceHierarchyPattern.ToString() + "]";
		}
    #endregion

    #region ConvertList
		public static List<OpDeviceHierarchyPattern> ConvertList(DB_DEVICE_HIERARCHY_PATTERN[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpDeviceHierarchyPattern> ConvertList(DB_DEVICE_HIERARCHY_PATTERN[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDeviceHierarchyPattern> ret = new List<OpDeviceHierarchyPattern>(list.Length);
			foreach (var loop in list)
			{
				OpDeviceHierarchyPattern insert = new OpDeviceHierarchyPattern(loop);
				ret.Add(insert);
			}

			if (loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion

    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDeviceHierarchyPattern> list, IDataProvider dataProvider)
		{
			if (list != null && list.Count > 0)
			{
				Dictionary<int, OpSlotType> SlotTypeDict = dataProvider.GetSlotType(list.Select(l => l.ID_SLOT_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_SLOT_TYPE);
				Dictionary<int, OpDeviceType> DeviceTypeDict = dataProvider.GetDeviceType(list.Where(l => l.ID_DEVICE_TYPE.HasValue).Select(l => l.ID_DEVICE_TYPE.Value).Concat(list.Select(l => l.ID_DEVICE_TYPE_PARENT)).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE);
				Dictionary<int, OpDeviceTypeGroup> DeviceTypeGroupDict = dataProvider.GetDeviceTypeGroup(list.Where(l => l.ID_DEVICE_TYPE_GROUP.HasValue).Select(l => l.ID_DEVICE_TYPE_GROUP.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DEVICE_TYPE_GROUP);
               
                Dictionary<int, OpProtocol> ProtocolDict = dataProvider.GetProtocol(list.Where(l => l.ID_PROTOCOL_IN.HasValue).Select(l => l.ID_PROTOCOL_IN.Value).Concat(list.Where(l => l.ID_PROTOCOL_OUT.HasValue).Select(l => l.ID_PROTOCOL_OUT.Value)).Distinct().ToArray()).ToDictionary(l => l.ID_PROTOCOL);

				foreach (var loop in list)
				{
					loop.SlotType = SlotTypeDict.TryGetValue(loop.ID_SLOT_TYPE);
					if (loop.ID_DEVICE_TYPE.HasValue)
						loop.DeviceType = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE.Value);
					if (loop.ID_DEVICE_TYPE_GROUP.HasValue)
						loop.DeviceTypeGroup = DeviceTypeGroupDict.TryGetValue(loop.ID_DEVICE_TYPE_GROUP.Value);

                    loop.DeviceTypeParent = DeviceTypeDict.TryGetValue(loop.ID_DEVICE_TYPE_PARENT);
                    if (loop.ID_PROTOCOL_IN.HasValue)
                        loop.ProtocolIn = ProtocolDict.TryGetValue(loop.ID_PROTOCOL_IN.Value);
                    if (loop.ID_PROTOCOL_OUT.HasValue)
                        loop.ProtocolOut = ProtocolDict.TryGetValue(loop.ID_PROTOCOL_OUT.Value);
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDeviceHierarchyPattern> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceHierarchyPattern)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceHierarchyPattern).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDeviceHierarchyPattern)
				return this.IdDeviceHierarchyPattern == ((OpDeviceHierarchyPattern)obj).IdDeviceHierarchyPattern;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDeviceHierarchyPattern left, OpDeviceHierarchyPattern right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDeviceHierarchyPattern left, OpDeviceHierarchyPattern right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDeviceHierarchyPattern.GetHashCode();
		}
    #endregion
    }
#endif
}