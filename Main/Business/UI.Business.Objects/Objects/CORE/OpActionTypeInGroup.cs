using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpActionTypeInGroup : DB_ACTION_TYPE_IN_GROUP, IComparable, IEquatable<OpActionTypeInGroup>, IOpChangeState
    {
    #region Properties
        public int IdActionTypeGroup { get { return this.ID_ACTION_TYPE_GROUP; } set { this.ID_ACTION_TYPE_GROUP = value; this.OpState = OpChangeState.Modified; } }
        public string ActionTypeName { get { return this.ACTION_TYPE_NAME; } set { this.ACTION_TYPE_NAME = value; this.OpState = OpChangeState.Modified; } }
    #endregion

    #region	Navigation Properties
        private OpActionTypeGroup _ActionTypeGroup;
        public OpActionTypeGroup ActionTypeGroup { get { return this._ActionTypeGroup; } set { this._ActionTypeGroup = value; this.ID_ACTION_TYPE_GROUP = (value == null) ? 0 : (int)value.ID_ACTION_TYPE_GROUP; } }
        private OpActionType _ActionType;
        public OpActionType ActionType { get { return this._ActionType; } set { this._ActionType = value; this.ACTION_TYPE_NAME = (value == null) ? "" : (string)value.NAME; } }
    #endregion

    #region	Custom Properties
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Ctor
        public OpActionTypeInGroup()
            : base() { OpState = OpChangeState.New; }

        public OpActionTypeInGroup(DB_ACTION_TYPE_IN_GROUP clone)
            : base(clone) { OpState = OpChangeState.Loaded; }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return this.ACTION_TYPE_NAME;
        }
    #endregion

    #region	ConvertList
        public static List<OpActionTypeInGroup> ConvertList(DB_ACTION_TYPE_IN_GROUP[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpActionTypeInGroup> ConvertList(DB_ACTION_TYPE_IN_GROUP[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpActionTypeInGroup> ret = new List<OpActionTypeInGroup>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpActionTypeInGroup(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpActionTypeInGroup> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                string[] ActionTypeNames = list.Select(l => l.ACTION_TYPE_NAME).Distinct().ToArray();
                Dictionary<string, OpActionType> ActionTypeDict = dataProvider.GetActionTypeFilter().Where(a => ActionTypeNames.Contains(a.NAME)).ToDictionary(l => l.NAME);
                Dictionary<int, OpActionTypeGroup> ActionTypeGroupDict = dataProvider.GetActionTypeGroup(list.Select(l => l.ID_ACTION_TYPE_GROUP).Distinct().ToArray()).ToDictionary(l => l.ID_ACTION_TYPE_GROUP);
                foreach (var loop in list)
                {
                    loop.ActionType = ActionTypeDict.TryGetValue(loop.ACTION_TYPE_NAME);
                    loop.ActionTypeGroup = ActionTypeGroupDict.TryGetValue(loop.ID_ACTION_TYPE_GROUP);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpActionTypeInGroup> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpActionTypeInGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpActionTypeInGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpActionTypeInGroup> Members
        public bool Equals(OpActionTypeInGroup other)
        {
            if (other == null)
                return false;
            return this.IdActionTypeGroup.Equals(other.IdActionTypeGroup) && this.ActionTypeName.Equals(other.ActionTypeName);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpActionTypeInGroup)
                return this.IdActionTypeGroup == ((OpActionTypeInGroup)obj).IdActionTypeGroup && this.ActionTypeName == ((OpActionTypeInGroup)obj).ActionTypeName;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpActionTypeInGroup left, OpActionTypeInGroup right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpActionTypeInGroup left, OpActionTypeInGroup right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdActionTypeGroup.GetHashCode();
        }
    #endregion
    }
#endif
}