using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpLanguageData : DB_LANGUAGE_DATA, IComparable, IEquatable<OpLanguageData>, IOpData, IOpDataProvider
    {
    #region Properties
        public long IdLanguageData { get { return this.ID_LANGUAGE_DATA;} set {this.ID_LANGUAGE_DATA = value;}}
        public int IdLanguage { get { return this.ID_LANGUAGE;} set {this.ID_LANGUAGE = value;}}
        public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this, this.DataType); } }
    #endregion

    #region	Navigation Properties
		private OpLanguage _Language;
				public OpLanguage Language { get { return this._Language; } set { this._Language = value; this.ID_LANGUAGE = (value == null)? 0 : (int)value.ID_LANGUAGE; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
                public long IdData { get { return IdLanguageData; } set { IdLanguageData = value; } }
                public object ReferencedObject { get; set; }
                public OpChangeState OpState { get; set; }
    #endregion
		
    #region	Ctor
		public OpLanguageData()
            : base() { this.OpState = OpChangeState.New; }
		
		public OpLanguageData(DB_LANGUAGE_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "LanguageData [" + IdLanguageData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpLanguageData> ConvertList(DB_LANGUAGE_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpLanguageData> ConvertList(DB_LANGUAGE_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpLanguageData> ret = new List<OpLanguageData>(db_objects.Length);
            Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(db_objects.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_DATA_TYPE);
            db_objects.ToList().ForEach(db_object =>
            {
                OpLanguageData newObject = new OpLanguageData(db_object);
                    newObject.DataType = DataTypeDict.TryGetValue(newObject.ID_DATA_TYPE);
                    try
                    {
                        // sdudzik - probujemy poprawic wartosc, zeby byla wlasciwego typu
                        // CELOWO (!) uzywamy VALUE, a nie Value - nie zmieniac tego!
                        // VALUE uzywane do tego, zeby raz poprawic typ rzeczywistej wartosci, Value to tylko get/set
                        newObject.VALUE = OpDataUtil.CorrectValueType(newObject.DataType, newObject.VALUE);
                    }
                    catch
                    {
                        // korekcja sie nie udala, oznaczamy wartosc jako bledna
                        newObject.OpState = OpChangeState.Incorrect;
                    }
                ret.Add(newObject);
            });
                
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpLanguageData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                //foreach (var loop in list)
                //{
                //    //loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
                //}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpLanguageData> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion

    #region IOpDataProviders

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpLanguageData clone = new OpLanguageData();
            clone.IdData = this.IdData;
            clone.IdLanguage = this.IdLanguage;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_LANGUAGE_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpLanguageData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpLanguageData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpLanguageData> Members
        public bool Equals(OpLanguageData other)
        {
            if (other == null)
				return false;
			return this.IdLanguageData.Equals(other.IdLanguageData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpLanguageData)
				return this.IdLanguageData == ((OpLanguageData)obj).IdLanguageData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpLanguageData left, OpLanguageData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpLanguageData left, OpLanguageData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdLanguageData.GetHashCode();
		}
    #endregion
    }
#endif
}