using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpOrderData : DB_ORDER_DATA, IComparable, IEquatable<OpOrderData>
    {
    #region Properties
            public long IdOrderData { get { return this.ID_ORDER_DATA;} set {this.ID_ORDER_DATA = value;}}
                public int IdOrder { get { return this.ID_ORDER;} set {this.ID_ORDER = value;}}
                public long IdDataType { get { return this.ID_DATA_TYPE;} set {this.ID_DATA_TYPE = value;}}
                public int IndexNbr { get { return this.INDEX_NBR;} set {this.INDEX_NBR = value;}}
                public object Value { get { return this.VALUE;} set {this.VALUE = value;}}
    #endregion

    #region	Navigation Properties
		private OpOrder _Order;
				public OpOrder Order { get { return this._Order; } set { this._Order = value; this.ID_ORDER = (value == null)? 0 : (int)value.ID_ORDER; } }
				private OpDataType _DataType;
				public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null)? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpOrderData()
			:base() {}
		
		public OpOrderData(DB_ORDER_DATA clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "OrderData [" + IdOrderData.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpOrderData> ConvertList(DB_ORDER_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpOrderData> ConvertList(DB_ORDER_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpOrderData> ret = new List<OpOrderData>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpOrderData(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpOrderData> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpOrder> OrderDict = dataProvider.GetOrder(list.Select(l => l.ID_ORDER).Distinct().ToArray()).ToDictionary(l => l.ID_ORDER);
	Dictionary<long,OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
			
				foreach (var loop in list)
				{
						loop.Order = OrderDict.TryGetValue(loop.ID_ORDER); 
		loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpOrderData> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpOrderData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpOrderData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpOrderData> Members
        public bool Equals(OpOrderData other)
        {
            if (other == null)
				return false;
			return this.IdOrderData.Equals(other.IdOrderData);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpOrderData)
				return this.IdOrderData == ((OpOrderData)obj).IdOrderData;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpOrderData left, OpOrderData right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpOrderData left, OpOrderData right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdOrderData.GetHashCode();
		}
    #endregion
    }
#endif
}