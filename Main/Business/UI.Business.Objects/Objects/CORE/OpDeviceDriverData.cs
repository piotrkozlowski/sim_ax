using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceDriverData : DB_DEVICE_DRIVER_DATA, IOpData, IOpDataProvider, IComparable, IEquatable<OpDeviceDriverData>
    {
    #region Properties
        [XmlIgnore]
        public long IdDeviceDriverData { get { return this.ID_DEVICE_DRIVER_DATA; } set { this.ID_DEVICE_DRIVER_DATA = value; } }
        [XmlIgnore]
        public int IdDeviceDriver { get { return this.ID_DEVICE_DRIVER; } set { this.ID_DEVICE_DRIVER = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
    #endregion

    #region Navigation Properties
        private OpDeviceDriver _DeviceDriver;
        [XmlIgnore]
        public OpDeviceDriver DeviceDriver { get { return this._DeviceDriver; } set { this._DeviceDriver = value; this.ID_DEVICE_DRIVER = (value == null) ? 0 : (int)value.ID_DEVICE_DRIVER; } }
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region Custom Properties
        public long IdData { get { return IdDeviceDriverData; } set { IdDeviceDriverData = (long)value; } }
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
        public OpDeviceDriverData()
            : base() { this.OpState = OpChangeState.New; }

        public OpDeviceDriverData(DB_DEVICE_DRIVER_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString

        public override string ToString()
        {
            return "DeviceDriverData [" + IdDeviceDriver.ToString() + "]";
        }
    #endregion

    #region ConvertList
        public static List<OpDeviceDriverData> ConvertList(DB_DEVICE_DRIVER_DATA[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDeviceDriverData> ConvertList(DB_DEVICE_DRIVER_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceDriverData> ret = new List<OpDeviceDriverData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpDeviceDriverData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpDeviceDriverData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceDriverData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.DeviceDriver = dataProvider.GetDeviceDriver(this.ID_DEVICE_DRIVER);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpDeviceDriverData clone = new OpDeviceDriverData();
            clone.IdData = this.IdData;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_DEVICE_DRIVER_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceDriverData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceDriverData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpDeviceDriverData)
                return this.IdData == ((OpDeviceDriverData)obj).IdData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpDeviceDriverData left, OpDeviceDriverData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpDeviceDriverData left, OpDeviceDriverData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdData.GetHashCode();
        }

        public bool Equals(OpDeviceDriverData other)
        {
            if (other == null)
                return false;

            return this.IdDeviceDriverData.Equals(other.IdDeviceDriverData);
        }
    #endregion
    }
#endif
}