using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    public class OpSession : DB_SESSION, IComparable, IOpChangeState, IOpDataProperty
    {
    #region Properties
            public long IdSession { get { return this.ID_SESSION;} set {this.ID_SESSION = value;}}
                public Guid SessionGuid { get { return this.SESSION_GUID;} set {this.SESSION_GUID = value;}}
                public int IdModule { get { return this.ID_MODULE;} set {this.ID_MODULE = value;}}
                public int? IdOperator { get { return this.ID_OPERATOR;} set {this.ID_OPERATOR = value;}}
                public int IdSessionStatus { get { return this.ID_SESSION_STATUS;} set {this.ID_SESSION_STATUS = value;}}
    #endregion

    #region	Navigation Properties
		private OpModule _Module;
				public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null)? 0 : (int)value.ID_MODULE; } }
				private OpOperator _Operator;
				public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null)? null : (int?)value.ID_OPERATOR; } }
				private OpSessionStatus _SessionStatus;
				public OpSessionStatus SessionStatus { get { return this._SessionStatus; } set { this._SessionStatus = value; this.ID_SESSION_STATUS = (value == null)? 0 : (int)value.ID_SESSION_STATUS; } }
        #endregion

        #region	Custom Properties
        public OpDataList<OpSessionData> DataList { get; set; }

        public OpChangeState OpState { get; set; }

        #endregion

        #region	Ctor
        public OpSession()
			:base()
        {
            DataList = new OpDataList<OpSessionData>();
        }
		
		public OpSession(DB_SESSION clone)
			:base(clone)
        {
            DataList = new OpDataList<OpSessionData>();
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return "Session [" + IdSession.ToString() + "]";
		}
    #endregion

    #region	ConvertList
		public static List<OpSession> ConvertList(DB_SESSION[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }
		
		public static List<OpSession> ConvertList(DB_SESSION[] list, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpSession> ret = new List<OpSession>(list.Length);
			foreach (var loop in list)
			{
			OpSession insert = new OpSession(loop);
				
				if(loadNavigationProperties)
				{
											
											
																									insert.Module = dataProvider.GetModule(loop.ID_MODULE); 
													
																									if (loop.ID_OPERATOR.HasValue)
								insert.Operator = dataProvider.GetOperator(loop.ID_OPERATOR.Value); 
													
																									insert.SessionStatus = dataProvider.GetSessionStatus(loop.ID_SESSION_STATUS); 
													
									}
				
				ret.Add(insert);
			}
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpSession> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpSession)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpSession).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

        #region Implementation of IOpDataProperty
        public object GetOpDataPropertyValue(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return null;

            OpSessionData opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opDataProperty.OpDataType.IdReferenceType.HasValue && opData.ReferencedObject != null)
                    return opData.ReferencedObject;

                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && opData.Value != null)
                    return OpUnit.ChangeUnit(opData.Value, opDataProperty.OpDataType, opData.DataType.Unit, opDataProperty.OpUnit, OpDataTypeFormat.GetPrecision(opDataProperty.OpDataTypeFormat)); //(Convert.ToDouble(opData.Value) - opDataProperty.OpUnit.Bias) / opDataProperty.OpUnit.Scale;

                return opData.Value;
            }

            return null;
        }

        public void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null) return;

            OpSessionData opData = this.DataList.FirstOrDefault(q => q.IdDataType == opDataProperty.OpDataType.IdDataType);

            if (opData != null)
            {
                if (opData.DataType != null && opDataProperty.OpUnit != null && opDataProperty.OpUnit.IdUnit != 1 && value != null)
                    opData.Value = OpUnit.ChangeUnit(value, opDataProperty.OpDataType, opDataProperty.OpUnit, opData.DataType.Unit); //Convert.ToDouble(value) * opDataProperty.OpUnit.Scale + opDataProperty.OpUnit.Bias;
                else
                    opData.Value = value;
            }
        }
        public Type GetOpDataPropertyType(OpDataProperty opDataProperty)
        {
            if (opDataProperty == null || opDataProperty.OpDataType == null || !opDataProperty.ReturnReferencedTypeIfHelper.HasValue) return null;
            Type type = Utils.GetHelperType(opDataProperty.OpDataType, opDataProperty.ReturnReferencedTypeIfHelper.Value);

            if (type == null)
                type = DataType.GetSystemType(opDataProperty.OpDataType.IdDataTypeClass);

            return type;
        }
        #endregion
    }
#endif
}