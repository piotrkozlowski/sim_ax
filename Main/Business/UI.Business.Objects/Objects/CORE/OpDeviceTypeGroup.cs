using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDeviceTypeGroup : DB_DEVICE_TYPE_GROUP, IComparable, IReferenceType
    {
        #region Properties
        public int IdDeviceTypeGroup { get { return this.ID_DEVICE_TYPE_GROUP; } set { this.ID_DEVICE_TYPE_GROUP = value; } }
        public string Name { get { return this.NAME; } set { this.NAME = value; } }
        public string Description { get { return this.DESCRIPTION; } set { this.DESCRIPTION = value; } }
        #endregion

        #region	Navigation Properties
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpDeviceTypeGroup()
            : base() { }

        public OpDeviceTypeGroup(DB_DEVICE_TYPE_GROUP clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            return this.NAME;
        }
        #endregion

        #region	ConvertList
        public static List<OpDeviceTypeGroup> ConvertList(DB_DEVICE_TYPE_GROUP[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDeviceTypeGroup> ConvertList(DB_DEVICE_TYPE_GROUP[] list, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpDeviceTypeGroup> ret = new List<OpDeviceTypeGroup>(list.Length);
            foreach (var loop in list)
            {
                OpDeviceTypeGroup insert = new OpDeviceTypeGroup(loop);

                if (loadNavigationProperties)
                {



                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpDeviceTypeGroup> list, IDataProvider dataProvider)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDeviceTypeGroup)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDeviceTypeGroup).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region #region Implementation of IReferenceType

        public object GetReferenceKey()
        {
            return this.IdDeviceTypeGroup;
        }

        public object GetReferenceValue()
        {
            return this;
        }

        #endregion
    }
#endif
}