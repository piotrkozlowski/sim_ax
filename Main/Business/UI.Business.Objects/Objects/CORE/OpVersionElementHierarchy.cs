using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using System.Collections;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
	[Serializable]
	public class OpVersionElementHierarchy : DB_VERSION_ELEMENT_HIERARCHY, IComparable, IEquatable<OpVersionElementHierarchy>, IOpChangeState
    {
    #region Properties
        public long IdVersionElementHierarchy { get { return this.ID_VERSION_ELEMENT_HIERARCHY; } set { if (this.ID_VERSION_ELEMENT_HIERARCHY != value) { this.ID_VERSION_ELEMENT_HIERARCHY = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_HIERARCHY, value, this); } } }
        public long? IdVersionElementHierarchyParent { get { return this.ID_VERSION_ELEMENT_HIERARCHY_PARENT; } set { if (this.ID_VERSION_ELEMENT_HIERARCHY_PARENT != value) { this.ID_VERSION_ELEMENT_HIERARCHY_PARENT = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT_HIERARCHY_PARENT, value, this); } } }
        public int IdVersionElement { get { return this.ID_VERSION_ELEMENT; } set { if (this.ID_VERSION_ELEMENT != value) { this.ID_VERSION_ELEMENT = OpDataUtil.SetNewValue(this.ID_VERSION_ELEMENT, value, this); } } }
        public string Hierarchy { get { return this.HIERARCHY; } set { if (this.HIERARCHY != value) { this.HIERARCHY = OpDataUtil.SetNewValue(this.HIERARCHY, value, this); } } }
        public OpChangeState OpState { get; set; }
    #endregion

    #region	Navigation Properties
		private OpVersionElement _VersionElement;
		public OpVersionElement VersionElement { get { return this._VersionElement; } set { this._VersionElement = value; this.ID_VERSION_ELEMENT = (value == null)? 0 : (int)value.ID_VERSION_ELEMENT; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpVersionElementHierarchy()
			:base() {}
		
		public OpVersionElementHierarchy(DB_VERSION_ELEMENT_HIERARCHY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
			return this.HIERARCHY;
		}
    #endregion

    #region	ConvertList
		public static List<OpVersionElementHierarchy> ConvertList(DB_VERSION_ELEMENT_HIERARCHY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpVersionElementHierarchy> ConvertList(DB_VERSION_ELEMENT_HIERARCHY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
		{
			List<OpVersionElementHierarchy> ret = new List<OpVersionElementHierarchy>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpVersionElementHierarchy(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads navigation properties
			if(loadCustomData)
				LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpVersionElementHierarchy> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
			if (list != null && list.Count > 0)
            {
                Dictionary<int, OpVersionElement> VersionElementDict = dataProvider.GetVersionElement(list.Select(l => l.ID_VERSION_ELEMENT).Distinct().ToArray(), false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).ToDictionary(l => l.ID_VERSION_ELEMENT);
			
				foreach (var loop in list)
				{
					loop.VersionElement = VersionElementDict.TryGetValue(loop.ID_VERSION_ELEMENT); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpVersionElementHierarchy> list, IDataProvider dataProvider, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpVersionElementHierarchy)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpVersionElementHierarchy).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpVersionElementHierarchy> Members
        public bool Equals(OpVersionElementHierarchy other)
        {
            if (other == null)
				return false;
			return this.IdVersionElementHierarchy.Equals(other.IdVersionElementHierarchy);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpVersionElementHierarchy)
				return this.IdVersionElementHierarchy == ((OpVersionElementHierarchy)obj).IdVersionElementHierarchy;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpVersionElementHierarchy left, OpVersionElementHierarchy right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpVersionElementHierarchy left, OpVersionElementHierarchy right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdVersionElementHierarchy.GetHashCode();
		}
    #endregion
	}
#endif
}