﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Runtime.CompilerServices;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpShippingListDevice : DB_SHIPPING_LIST_DEVICE, IComparable, IEquatable<OpShippingListDevice>
    {
    #region Properties
        public int IdShippingListDevice { get { return this.ID_SHIPPING_LIST_DEVICE; } set { this.ID_SHIPPING_LIST_DEVICE = value; } }
        public int IdShippingList { get { return this.ID_SHIPPING_LIST; } set { this.ID_SHIPPING_LIST = value; } }
        public long SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdArticle { get { return this.ID_ARTICLE; } set { this.ID_ARTICLE = value; } }
        public DateTime InsertDate { get { return this.INSERT_DATE; } set { this.INSERT_DATE = value; } }
    #endregion

    #region	Navigation Properties
        private OpShippingList _ShippingList;
        public OpShippingList ShippingList { get { return this._ShippingList; } set { this._ShippingList = value; this.ID_SHIPPING_LIST = (value == null) ? 0 : (int)value.ID_SHIPPING_LIST; } }
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpArticle _Article;
        public OpArticle Article { get { return this._Article; } set { this._Article = value; this.ID_ARTICLE = (value == null) ? null : (long?)value.ID_ARTICLE; } }
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? 0 : (long)value.SERIAL_NBR; } }
        private OpDeviceHierarchy _DeviceHierarchy;
        public OpDeviceHierarchy DeviceHierarchy { get { return this._DeviceHierarchy; } set { this._DeviceHierarchy = value; } }

        private OpDeviceDetails _DeviceDetails;
        public OpDeviceDetails DeviceDetails { get { return this._DeviceDetails; } set { this._DeviceDetails = value; } }
    #endregion

    #region	Custom Properties
    #endregion

    #region	Ctor
        public OpShippingListDevice()
            : base() { }

        public OpShippingListDevice(DB_SHIPPING_LIST_DEVICE clone)
            : base(clone) { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ShippingListDevice [" + IdShippingListDevice.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpShippingListDevice> ConvertList(DB_SHIPPING_LIST_DEVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpShippingListDevice> ConvertListCustom(DB_SHIPPING_LIST_DEVICE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertListCustom(db_objects, dataProvider, true);
        }

        public static List<OpShippingListDevice> ConvertList(DB_SHIPPING_LIST_DEVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpShippingListDevice> ret = new List<OpShippingListDevice>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpShippingListDevice(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }

        public static List<OpShippingListDevice> ConvertListCustom(DB_SHIPPING_LIST_DEVICE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpShippingListDevice> ret = new List<OpShippingListDevice>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpShippingListDevice(db_object)));

            if (loadNavigationProperties)
                LoadNavigationPropertiesCustom(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpShippingListDevice> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpShippingList> ShippingListDict = dataProvider.GetShippingList(list.Select(l => l.ID_SHIPPING_LIST).Distinct().ToArray()).ToDictionary(l => l.ID_SHIPPING_LIST);
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpArticle> ArticleDict = dataProvider.GetArticle(list.Where(l => l.ID_ARTICLE.HasValue).Select(l => l.ID_ARTICLE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ARTICLE);
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Select(l => l.SERIAL_NBR).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);
                //Dictionary<long, OpDeviceHierarchy> DeviceHierarchyDict = dataProvider.GetDeviceHierarchy(list.Select(l => l.SERIAL_NBR).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);


                foreach (var loop in list)
                {
                    loop.ShippingList = ShippingListDict.TryGetValue(loop.ID_SHIPPING_LIST);
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_ARTICLE.HasValue)
                        loop.Article = ArticleDict.TryGetValue(loop.ID_ARTICLE.Value);
                    loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR);
                }
            }
        }

        public static void LoadNavigationPropertiesCustom(ref List<OpShippingListDevice> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<int, OpShippingList> ShippingListDict = dataProvider.GetShippingList(list.Select(l => l.ID_SHIPPING_LIST).Distinct().ToArray()).ToDictionary(l => l.ID_SHIPPING_LIST);
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpArticle> ArticleDict = dataProvider.GetArticle(list.Where(l => l.ID_ARTICLE.HasValue).Select(l => l.ID_ARTICLE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_ARTICLE);
                Dictionary<long, OpDeviceDetails> DeviceDict = dataProvider.GetDeviceDetailsCustom(list.Select(l => l.SERIAL_NBR).Distinct().ToArray(), queryDatabase: false, commandTimeout: 120).Where(l => l.SERIAL_NBR.HasValue).ToDictionary(l => l.SERIAL_NBR.Value);
                //Dictionary<long, OpDeviceHierarchy> DeviceHierarchyDict = dataProvider.GetDeviceHierarchy(list.Select(l => l.SERIAL_NBR).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);


                foreach (var loop in list)
                {
                    loop.ShippingList = ShippingListDict.TryGetValue(loop.ID_SHIPPING_LIST);
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.ID_ARTICLE.HasValue)
                        loop.Article = ArticleDict.TryGetValue(loop.ID_ARTICLE.Value);
                    loop.DeviceDetails = DeviceDict.TryGetValue(loop.SERIAL_NBR);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpShippingListDevice> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpShippingListDevice)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpShippingListDevice).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpShippingListDevice> Members
        public bool Equals(OpShippingListDevice other)
        {
            if (other == null)
                return false;
            return this.IdShippingListDevice.Equals(other.IdShippingListDevice);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if ((obj is OpShippingListDevice) && this.IdShippingListDevice != 0 && ((OpShippingListDevice)obj).IdShippingListDevice != 0)
                return this.IdShippingListDevice == ((OpShippingListDevice)obj).IdShippingListDevice;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpShippingListDevice left, OpShippingListDevice right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpShippingListDevice left, OpShippingListDevice right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            if (IdShippingListDevice == 0)
            {
                return RuntimeHelpers.GetHashCode(this);
            }
            else
            {
                return IdShippingListDevice.GetHashCode();
            }

            
        }
    #endregion
    }
#endif
}