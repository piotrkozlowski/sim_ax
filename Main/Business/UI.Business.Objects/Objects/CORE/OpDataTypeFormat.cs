using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using System.Data;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpDataTypeFormat : DB_DATA_TYPE_FORMAT, IComparable
    {
        #region Properties
        [XmlIgnore]
        public int IdDataTypeFormat { get { return this.ID_DATA_TYPE_FORMAT; } set { this.ID_DATA_TYPE_FORMAT = value; } }
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR; } set { this.ID_DESCR = value; } }
        [XmlIgnore]
        public int? TextMinLength { get { return this.TEXT_MIN_LENGTH; } set { this.TEXT_MIN_LENGTH = value; } }
        [XmlIgnore]
        public int? TextMaxLength { get { return this.TEXT_MAX_LENGTH; } set { this.TEXT_MAX_LENGTH = value; } }
        [XmlIgnore]
        public int? NumberMinPrecision { get { return this.NUMBER_MIN_PRECISION; } set { this.NUMBER_MIN_PRECISION = value; } }
        [XmlIgnore]
        public int? NumberMaxPrecision { get { return this.NUMBER_MAX_PRECISION; } set { this.NUMBER_MAX_PRECISION = value; } }
        [XmlIgnore]
        public int? NumberMinScale { get { return this.NUMBER_MIN_SCALE; } set { this.NUMBER_MIN_SCALE = value; } }
        [XmlIgnore]
        public int? NumberMaxScale { get { return this.NUMBER_MAX_SCALE; } set { this.NUMBER_MAX_SCALE = value; } }
        [XmlIgnore]
        public Double? NumberMinValue { get { return this.NUMBER_MIN_VALUE; } set { this.NUMBER_MIN_VALUE = value; } }
        [XmlIgnore]
        public Double? NumberMaxValue { get { return this.NUMBER_MAX_VALUE; } set { this.NUMBER_MAX_VALUE = value; } }
        [XmlIgnore]
        public string DatetimeFormat { get { return this.DATETIME_FORMAT; } set { this.DATETIME_FORMAT = value; } }
        [XmlIgnore]
        public string RegularExpression { get { return this.REGULAR_EXPRESSION; } set { this.REGULAR_EXPRESSION = value; } }
        [XmlIgnore]
        public bool? IsRequired { get { return this.IS_REQUIRED; } set { this.IS_REQUIRED = value; } }
        [XmlIgnore]
        public int? IdUniqueType { get { return this.ID_UNIQUE_TYPE; } set { this.ID_UNIQUE_TYPE = value; } }
        #endregion

        #region	Navigation Properties
        private OpDescr _Descr;
        public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null) ? null : (long?)value.ID_DESCR; } }
        private OpUniqueType _UniqueType;
        [XmlIgnore]
        public OpUniqueType UniqueType { get { return this._UniqueType; } set { this._UniqueType = value; this.ID_UNIQUE_TYPE = (value == null) ? null : (int?)value.ID_UNIQUE_TYPE; } }
        #endregion

        #region	Custom Properties
        #endregion

        #region	Ctor
        public OpDataTypeFormat()
            : base() { }

        public OpDataTypeFormat(DB_DATA_TYPE_FORMAT clone)
            : base(clone) { }
        #endregion

        #region	ToString
        public override string ToString()
        {
            if (Descr != null)
                return Descr.Description;
            else
            {
                StringBuilder sb = new StringBuilder();
                if (TextMinLength.HasValue || TextMaxLength.HasValue)
                    sb.AppendFormat("{0} [{1} - {2}] ",
                        ResourcesText.Text,
                        TextMinLength.HasValue ? TextMinLength.Value.ToString() : "?",
                        TextMaxLength.HasValue ? TextMaxLength.Value.ToString() : "?");
                if (NumberMinPrecision.HasValue || NumberMinScale.HasValue || NumberMinValue.HasValue
                    || NumberMaxPrecision.HasValue || NumberMaxScale.HasValue || NumberMaxValue.HasValue)
                    sb.AppendFormat("{0} [{1} - {2}] {3} [{4} - {5}] {6} [{7} - {8}] ",
                        ResourcesText.Number,
                        NumberMinValue.HasValue ? NumberMinValue.Value.ToString() : "?",
                        NumberMaxValue.HasValue ? NumberMaxValue.Value.ToString() : "?",
                        ResourcesText.Precision,
                        NumberMinPrecision.HasValue ? NumberMinPrecision.Value.ToString() : "?",
                        NumberMaxPrecision.HasValue ? NumberMaxPrecision.Value.ToString() : "?",
                        ResourcesText.Scale,
                        NumberMinScale.HasValue ? NumberMinScale.Value.ToString() : "?",
                        NumberMaxScale.HasValue ? NumberMaxScale.Value.ToString() : "?");
                if (!String.IsNullOrWhiteSpace(DatetimeFormat))
                    sb.AppendFormat("{0} [{1}] ", ResourcesText.DatetimeFormat, DatetimeFormat);
                if (!String.IsNullOrWhiteSpace(RegularExpression))
                    sb.AppendFormat("RegEx [{0}] ", RegularExpression);
                if (IsRequired.HasValue)
                    sb.AppendFormat("{0} [{1}] ", ResourcesText.Required, IsRequired.Value);
                if (sb.Length == 0) return ResourcesText.DescriptionMissing;
                else return sb.ToString();
            }
        }
        #endregion

        #region	ConvertList
        public static List<OpDataTypeFormat> ConvertList(DB_DATA_TYPE_FORMAT[] list, IDataProvider dataProvider)
        {
            return ConvertList(list, dataProvider, true);
        }

        public static List<OpDataTypeFormat> ConvertList(DB_DATA_TYPE_FORMAT[] list, IDataProvider dataProvider, bool loadNavigationProperties,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDataTypeFormat> ret = new List<OpDataTypeFormat>(list.Length);
            foreach (var loop in list)
            {
                OpDataTypeFormat insert = new OpDataTypeFormat(loop);

                if (loadNavigationProperties)
                {

                    if (loop.ID_DESCR.HasValue)
                        insert.Descr = dataProvider.GetDescr(new long[] { loop.ID_DESCR.Value }, false, dataProvider.UserLanguage, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();












                    if (loop.ID_UNIQUE_TYPE.HasValue)
                        insert.UniqueType = dataProvider.GetUniqueType(new int[] { loop.ID_UNIQUE_TYPE.Value }, false, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout).FirstOrDefault();

                }

                ret.Add(insert);
            }

            LoadCustomData(ref ret, dataProvider, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout); // Loads user custom data
            return ret;
        }
        #endregion

        #region LoadCustomData
        private static void LoadCustomData(ref List<OpDataTypeFormat> list, IDataProvider dataProvider,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
        }
        #endregion

        #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDataTypeFormat)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDataTypeFormat).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
        #endregion

        #region GetPrecision
        public static int? GetPrecision(OpDataTypeFormat dataTypeFormat)
        {
            if (dataTypeFormat == null) return null;

            if (dataTypeFormat.NumberMaxPrecision.HasValue)
                return dataTypeFormat.NumberMaxPrecision.Value;
            else if (dataTypeFormat.NumberMinPrecision.HasValue)
                return dataTypeFormat.NumberMinPrecision.Value;

            return null;
        }
        #endregion
    }
#endif
}