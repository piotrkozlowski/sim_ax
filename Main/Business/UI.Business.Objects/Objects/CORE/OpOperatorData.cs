using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpOperatorData : DB_OPERATOR_DATA, IOpData, IOpDataProvider, IComparable
    {
    #region Properties
        [XmlIgnore]
        public long IdOperatorData { get { return this.ID_OPERATOR_DATA; } set { this.ID_OPERATOR_DATA = value; } }
        [XmlIgnore]
        public int IdOperator { get { return this.ID_OPERATOR; } set { this.ID_OPERATOR = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = OpDataUtil.SetNewValue(this.VALUE, value, this); } }
        [XmlIgnore]
        public int? IdDistributor { get { return this.ID_DISTRIBUTOR; } set { this.ID_DISTRIBUTOR = value; } }
    #endregion

    #region Navigation Properties
        private OpOperator _Operator;
        [XmlIgnore]
        public OpOperator Operator { get { return this._Operator; } set { this._Operator = value; this.ID_OPERATOR = (value == null) ? 0 : (int)value.ID_OPERATOR; } }
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
        private OpDistributor _Distributor;
        [XmlIgnore]
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? null : (int?)value.ID_DISTRIBUTOR; } }
    #endregion

    #region Custom Properties
        [XmlIgnore]
        public long IdData { get { return IdOperatorData; } set { IdOperatorData = value; } }
        [XmlIgnore, DataMember]
        public OpChangeState OpState { get; set; }
        public object ReferencedObject { get; set; }
    #endregion

    #region Ctor
        public OpOperatorData()
            : base() { this.OpState = OpChangeState.New; }

        public OpOperatorData(DB_OPERATOR_DATA clone)
            : base(clone) { this.OpState = OpChangeState.Loaded; }
    #endregion

    #region ToString
        public override string ToString()
        {
            return "OperatorData [" + IdOperatorData.ToString() + "]";
        }
    #endregion

    #region ConvertList
        public static List<OpOperatorData> ConvertList(DB_OPERATOR_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpOperatorData> ConvertList(DB_OPERATOR_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpOperatorData> ret = new List<OpOperatorData>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpOperatorData(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpOperatorData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                //Dictionary<int, OpOperator> OperatorDict = dataProvider.GetOperator(list.Select(l => l.ID_OPERATOR).Distinct().ToArray()).ToDictionary(l => l.ID_OPERATOR);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);
                Dictionary<int, OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Where(l => l.ID_DISTRIBUTOR.HasValue).Select(l => l.ID_DISTRIBUTOR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
                foreach (var loop in list)
                {
                    //loop.Operator = OperatorDict.TryGetValue(loop.ID_OPERATOR);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                    if (loop.ID_DISTRIBUTOR.HasValue)
                        loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpOperatorData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region IOpDataProvider

    #region AssignReferences
        public void AssignReferences(IDataProvider dataProvider)
        {
            //this.Operator = dataProvider.GetOperator(this.ID_OPERATOR);
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
            if (this.ID_DISTRIBUTOR.HasValue)
                this.Distributor = dataProvider.GetDistributor(this.ID_DISTRIBUTOR.Value);
        }
    #endregion

    #region Clone
        /// <summary>
        /// if dataProvider==null NavigationProperties will not be assigned 
        /// </summary>
        public object Clone(IDataProvider dataProvider)
        {
            OpOperatorData clone = new OpOperatorData();
            clone.IdData = this.IdData;
            clone.IdOperator = this.IdOperator;
            clone.IdDistributor = this.IdDistributor;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_OPERATOR_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }
    #endregion

    #endregion

    #region IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpOperatorData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpOperatorData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpOperatorData)
                return this.IdData == ((OpOperatorData)obj).IdData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpOperatorData left, OpOperatorData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpOperatorData left, OpOperatorData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdData.GetHashCode();
        }
    #endregion
    }
#endif
}