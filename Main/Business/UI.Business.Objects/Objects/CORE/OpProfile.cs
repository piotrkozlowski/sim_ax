using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpProfile : DB_PROFILE, IComparable, IEquatable<OpProfile>, IReferenceType
    {
    #region Enum
        public enum ProfileSource : int
        {
            Unknown = 0,
            Device = 1,
            Meter = 2,
            Location = 3,
            Distributor = 4
        }
    #endregion
    #region Properties
        [XmlIgnore]
        public int IdProfile { get { return this.ID_PROFILE;} set {this.ID_PROFILE = value;}}
        [XmlIgnore]
        public string Name { get { return this.NAME;} set {this.NAME = value;}}
        [XmlIgnore]
        public long? IdDescr { get { return this.ID_DESCR;} set {this.ID_DESCR = value;}}
        [XmlIgnore]
        public int? IdModule { get { return this.ID_MODULE;} set {this.ID_MODULE = value;}}
    #endregion

    #region	Navigation Properties
        [DataMember]
		private OpDescr _Descr;
		public OpDescr Descr { get { return this._Descr; } set { this._Descr = value; this.ID_DESCR = (value == null)? null : (long?)value.ID_DESCR; } }

        [DataMember]
        private OpModule _Module;
		public OpModule Module { get { return this._Module; } set { this._Module = value; this.ID_MODULE = (value == null)? null : (int?)value.ID_MODULE; } }
        #endregion

        #region	Custom Properties
        [DataMember]
        public OpDataList<OpProfileData> Parameters { get; set; }
        [XmlIgnore]
        public Dictionary<OpDevice, System.Tuple<ProfileSource, object>> Devices { get; set; }
        [XmlIgnore]
        public Dictionary<OpMeter, System.Tuple<ProfileSource, object>> Meters { get; set; }
        [XmlIgnore]
        public Dictionary<OpLocation, System.Tuple<ProfileSource, object>> Locations { get; set; }
        [XmlIgnore]
        public Dictionary<int, OpDistributor> Distributors { get; set; }
    #endregion
		
    #region	Ctor
		public OpProfile()
			:base() 
        {
            Parameters = new OpDataList<OpProfileData>();
            Devices = new Dictionary<OpDevice, System.Tuple<ProfileSource, object>>();
            Meters = new Dictionary<OpMeter, System.Tuple<ProfileSource, object>>();
            Locations = new Dictionary<OpLocation, System.Tuple<ProfileSource, object>>();
            Distributors = new Dictionary<int, OpDistributor>();
        }
		
		public OpProfile(DB_PROFILE clone)
            : base(clone)
        {
            Parameters = new OpDataList<OpProfileData>();
            Devices = new Dictionary<OpDevice, System.Tuple<ProfileSource, object>>();
            Meters = new Dictionary<OpMeter, System.Tuple<ProfileSource, object>>();
            Locations = new Dictionary<OpLocation, System.Tuple<ProfileSource, object>>();
            Distributors = new Dictionary<int, OpDistributor>();
        }
    #endregion
		
    #region	ToString
		public override string ToString()
		{
			return this.NAME;
		}
    #endregion

    #region	ConvertList
		public static List<OpProfile> ConvertList(DB_PROFILE[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpProfile> ConvertList(DB_PROFILE[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties, bool useDBCollector = false)
		{
			List<OpProfile> ret = new List<OpProfile>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpProfile(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider, useDBCollector); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpProfile> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			    Dictionary<long,OpDescr> DescrDict = dataProvider.GetDescr(list.Where(l => l.ID_DESCR.HasValue).Select(l => l.ID_DESCR.Value).Distinct().ToArray()).ToDictionary(l => l.ID_DESCR);
	            Dictionary<int,OpModule> ModuleDict = dataProvider.GetModule(list.Where(l => l.ID_MODULE.HasValue).Select(l => l.ID_MODULE.Value).Distinct().ToArray()).ToDictionary(l => l.ID_MODULE);			
				foreach (var loop in list)
				{
					if (loop.ID_DESCR.HasValue)
						loop.Descr = DescrDict.TryGetValue(loop.ID_DESCR.Value); 
		            if (loop.ID_MODULE.HasValue)
						loop.Module = ModuleDict.TryGetValue(loop.ID_MODULE.Value); 				
                }
			}
		}
    #endregion
		
    #region LoadCustomData
        private static void LoadCustomData(ref List<OpProfile> list, IDataProvider dataProvider, bool useDBCollector = false)
        {
            if (list.Count > 0)
            {
                bool? dbCollectorEnabled = dataProvider.IsDBCollectorEnabled(useDBCollector);
                List<OpProfileData> data = new List<OpProfileData>();
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    #region DBCollector
                    //data = OpProfileData.ConvertList(dataProvider.dbCollectorClient.GetProfileDataFilter(ref dataProvider.dbCollectorSession, null, list.Select(q => q.IdProfile).ToArray(), null, null, null, null, false, System.Data.IsolationLevel.ReadUncommitted, 0), dataProvider);
                    data = dataProvider.GetProfileDataFilter(IdProfile: list.Select(q => q.IdProfile).ToArray(),
                                                             transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                             autoTransaction: false,
                                                             commandTimeout: 0,
                                                             useDBCollector: useDBCollector);
                    if (data.Count > 0)
                    {
                        Dictionary<int, Dictionary<int, OpProfile>> profilesDict = new Dictionary<int, Dictionary<int, OpProfile>>();
                        profilesDict.Add(0, new Dictionary<int, OpProfile>());
                        list.Where(p => p.ID_SERVER.HasValue).Select(p => p.ID_SERVER.Value).Distinct().ToList().ForEach(p => profilesDict.Add(p, new Dictionary<int, OpProfile>()));
                        foreach (OpProfile pItem in list)
                        {
                            if (pItem.ID_SERVER.HasValue)
                                profilesDict[pItem.ID_SERVER.Value].Add(pItem.IdProfile, pItem);
                            else
                                profilesDict[0].Add(pItem.IdProfile, pItem);
                        }
                        foreach (OpProfileData pdItem in data)
                        {
                            if (pdItem.ID_SERVER.HasValue)
                                profilesDict[pdItem.ID_SERVER.Value][pdItem.IdProfile].Parameters.Add(pdItem);
                            else
                                profilesDict[0][pdItem.IdProfile].Parameters.Add(pdItem);
                        }
                    }
    #endregion
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
    #region Local
                    data = dataProvider.GetProfileDataFilter(IdProfile: list.Select(q => q.IdProfile).ToArray());
                    if (data.Count > 0)
                    {
                        Dictionary<int, OpProfile> profilesDict = list.ToDictionary<OpProfile, int>(l => l.IdProfile);
                        data.ForEach(w => profilesDict[w.IdProfile].Parameters.Add(w));
                    }
    #endregion
                }
            }
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpProfile)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpProfile).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpProfile> Members
        public bool Equals(OpProfile other)
        {
            if (other == null)
				return false;
			return this.IdProfile.Equals(other.IdProfile);
        }
    #endregion

    #region Implementation of IReferenceType
        public object GetReferenceKey()
        {
            return this.IdProfile;
        }

        public object GetReferenceValue()
        {
            return this;
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpProfile)
				return this.IdProfile == ((OpProfile)obj).IdProfile;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpProfile left, OpProfile right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpProfile left, OpProfile right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdProfile.GetHashCode();
		}
    #endregion
    }
#endif
}