﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable, DataContract]
    public class OpImrServerData : DB_IMR_SERVER_DATA, IComparable, IEquatable<OpImrServerData>, IOpData, IOpDataProvider
    {
        #region Properties

        [XmlIgnore]
        public long IdImrServerData { get { return this.ID_IMR_SERVER_DATA; } set { this.ID_IMR_SERVER_DATA = value; } }
        [XmlIgnore]
        public int IdImrServer { get { return this.ID_IMR_SERVER; } set { this.ID_IMR_SERVER = value; } }
        [XmlIgnore]
        public long IdDataType { get { return this.ID_DATA_TYPE; } set { this.ID_DATA_TYPE = value; } }
        [XmlIgnore]
        public int IndexNbr { get { return this.INDEX_NBR; } set { this.INDEX_NBR = value; } }
        [XmlIgnore]
        public object Value { get { return this.VALUE; } set { this.VALUE = value; } }

        [DataMember]
        public OpChangeState OpState { get; set; }
        #endregion

        #region	Navigation Properties
        [DataMember]
        private OpImrServer _ImrServer;
        [XmlIgnore]
        public OpImrServer ImrServer { get { return this._ImrServer; } set { this._ImrServer = value; this.ID_IMR_SERVER = (value == null) ? 0 : (int)value.ID_IMR_SERVER; } }
        [DataMember]
        private OpDataType _DataType;
        [XmlIgnore]
        public OpDataType DataType { get { return this._DataType; } set { this._DataType = value; this.ID_DATA_TYPE = (value == null) ? 0 : (long)value.ID_DATA_TYPE; } }
    #endregion

    #region	Custom Properties
        public object ReferencedObject { get; set; }
    #endregion

    #region	Ctor
        public OpImrServerData()
            : base()
        { }

        public OpImrServerData(DB_IMR_SERVER_DATA clone)
            : base(clone)
        { }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "ImrServerData [" + IdImrServerData.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpImrServerData> ConvertList(DB_IMR_SERVER_DATA[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpImrServerData> ConvertList(DB_IMR_SERVER_DATA[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpImrServerData> ret = new List<OpImrServerData>(db_objects.Length);

            Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(db_objects.Select(l => l.ID_DATA_TYPE).Distinct().ToArray(), false, loadNavigationProperties: loadNavigationProperties).ToDictionary(l => l.ID_DATA_TYPE);

            db_objects.ToList().ForEach(db_object =>
            {
                OpImrServerData newObject = new OpImrServerData(db_object);
                newObject.DataType = DataTypeDict.TryGetValue(newObject.ID_DATA_TYPE);
                try
                {
                    // sdudzik - probujemy poprawic wartosc, zeby byla wlasciwego typu
                    // CELOWO (!) uzywamy VALUE, a nie Value - nie zmieniac tego!
                    // VALUE uzywane do tego, zeby raz poprawic typ rzeczywistej wartosci, Value to tylko get/set
                    newObject.VALUE = OpDataUtil.CorrectValueType(newObject.DataType, newObject.VALUE);
                }
                catch
                {
                    // korekcja sie nie udala, oznaczamy wartosc jako bledna
                    newObject.OpState = OpChangeState.Incorrect;
                }
                ret.Add(newObject);
            });

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpImrServerData> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                // Dictionary<int, OpImrServer> ImrServerDict = dataProvider.GetImrServer(list.Select(l => l.ID_IMR_SERVER).Distinct().ToArray()).ToDictionary(l => l.ID_IMR_SERVER);
                Dictionary<long, OpDataType> DataTypeDict = dataProvider.GetDataType(list.Select(l => l.ID_DATA_TYPE).Distinct().ToArray()).ToDictionary(l => l.ID_DATA_TYPE);

                foreach (var loop in list)
                {
                    // loop.ImrServer = ImrServerDict.TryGetValue(loop.ID_IMR_SERVER);
                    loop.DataType = DataTypeDict.TryGetValue(loop.ID_DATA_TYPE);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpImrServerData> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpImrServerData)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpImrServerData).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion

    #region IEquatable<OpImrServerData> Members
        public bool Equals(OpImrServerData other)
        {
            if (other == null)
                return false;
            return this.IdImrServerData.Equals(other.IdImrServerData);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpImrServerData)
                return this.IdImrServerData == ((OpImrServerData)obj).IdImrServerData;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpImrServerData left, OpImrServerData right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpImrServerData left, OpImrServerData right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdImrServerData.GetHashCode();
        }
        #endregion

        #region IOpData

        [XmlIgnore]
        public long IdData { get { return IdImrServerData; } set { IdImrServerData = value; } }

        [XmlIgnore]
        public int Index { get { return this.INDEX_NBR; } set { this.INDEX_NBR = OpDataUtil.SetNewIndex(this.INDEX_NBR, value, this); } }

    #endregion

    #region IOpDataProvider

        public object Clone(IDataProvider dataProvider = null)
        {
            OpImrServerData clone = new OpImrServerData();
            clone.IdData = this.IdData;
            clone.IdImrServer = this.IdImrServer;
            clone.IdDataType = this.IdDataType;
            clone.Index = this.Index;
            clone.Value = this.Value;

            if (dataProvider != null)
                clone = ConvertList(new DB_IMR_SERVER_DATA[] { clone }, dataProvider)[0];

            clone.OpState = this.OpState;

            return clone;
        }

        public void AssignReferences(IDataProvider dataProvider)
        {
            this.DataType = dataProvider.GetDataType(this.ID_DATA_TYPE);
        }

    #endregion
    }
#endif
}