using System;
using System.Collections.Generic;
using System.Linq;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
    public class OpAlarmEvent : DB_ALARM_EVENT, IComparable, IEquatable<OpAlarmEvent>, IOpDynamic, IOpDynamicProperty<OpAlarmEvent>
    {
    #region Properties
        public long IdAlarmEvent { get { return this.ID_ALARM_EVENT; } set { this.ID_ALARM_EVENT = value; } }
        public long IdAlarmDef { get { return this.ID_ALARM_DEF; } set { this.ID_ALARM_DEF = value; } }
        public long? IdDataArch { get { return this.ID_DATA_ARCH; } set { this.ID_DATA_ARCH = value; } }
        public bool IsActive { get { return this.IS_ACTIVE; } set { this.IS_ACTIVE = value; } }
        public long? SerialNbr { get { return this.SERIAL_NBR; } set { this.SERIAL_NBR = value; } }
        public long? IdMeter { get { return this.ID_METER; } set { this.ID_METER = value; } }
        public long? IdLocation { get { return this.ID_LOCATION; } set { this.ID_LOCATION = value; } }
    #endregion

    #region	Navigation Properties
        private OpAlarmDef _AlarmDef;
        public OpAlarmDef AlarmDef { get { return this._AlarmDef; } set { this._AlarmDef = value; this.ID_ALARM_DEF = (value == null) ? 0 : (long)value.ID_ALARM_DEF; } }
        private OpDataArch _DataArch;
        public OpDataArch DataArch { get { return this._DataArch; } set { this._DataArch = value; this.ID_DATA_ARCH = (value == null) ? null : (long?)value.ID_DATA_ARCH; } }
        private OpDevice _Device;
        public OpDevice Device { get { return this._Device; } set { this._Device = value; this.SERIAL_NBR = (value == null) ? null : (long?)value.SERIAL_NBR; } }
        private OpMeter _Meter;
        public OpMeter Meter { get { return this._Meter; } set { this._Meter = value; this.ID_METER = (value == null) ? null : (long?)value.ID_METER; } }
        private OpLocation _Location;
        public OpLocation Location { get { return this._Location; } set { this._Location = value; this.ID_LOCATION = (value == null) ? null : (long?)value.ID_LOCATION; } }
    #endregion

    #region	Custom Properties
        private Dictionary<string, object> dynamicValues;
        private OpDynamicPropertyDict dynamicProperties;
    #endregion

    #region	Ctor
        public OpAlarmEvent()
            : base()
        {
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }

        public OpAlarmEvent(DB_ALARM_EVENT clone)
            : base(clone)
        {
            this.dynamicValues = new Dictionary<string, object>();
            this.dynamicProperties = new OpDynamicPropertyDict();
        }
    #endregion

    #region	ToString
        public override string ToString()
        {
            return "AlarmEvent [" + IdAlarmEvent.ToString() + "]";
        }
    #endregion

    #region	ConvertList
        public static List<OpAlarmEvent> ConvertList(DB_ALARM_EVENT[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }

        public static List<OpAlarmEvent> ConvertList(DB_ALARM_EVENT[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
        {
            List<OpAlarmEvent> ret = new List<OpAlarmEvent>(db_objects.Length);
            db_objects.ToList().ForEach(db_object => ret.Add(new OpAlarmEvent(db_object)));

            if (loadNavigationProperties)
                LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties

            LoadCustomData(ref ret, dataProvider); // Loads user custom data
            return ret;
        }
    #endregion

    #region LoadNavigationProperties
        public static void LoadNavigationProperties(ref List<OpAlarmEvent> list, IDataProvider dataProvider)
        {
            if (list != null && list.Count > 0)
            {
                Dictionary<long, OpAlarmDef> AlarmDefDict = dataProvider.GetAlarmDef(list.Select(l => l.ID_ALARM_DEF).Distinct().ToArray()).ToDictionary(l => l.ID_ALARM_DEF);
                Dictionary<long, OpDataArch> DataArchDict = dataProvider.GetDataArchFilter(true, list.Where(l => l.IdDataArch.HasValue).Select(k => (long)k.ID_DATA_ARCH.Value).Distinct().ToArray(), null, null, null, null, null, null, new long[] { }).ToDictionary(w => w.IdDataArch);
                Dictionary<long, OpDevice> DeviceDict = dataProvider.GetDevice(list.Where(l => l.SERIAL_NBR.HasValue).Select(l => l.SERIAL_NBR.Value).Distinct().ToArray()).ToDictionary(l => l.SERIAL_NBR);
                Dictionary<long, OpMeter> MeterDict = dataProvider.GetMeter(list.Where(l => l.ID_METER.HasValue).Select(l => l.ID_METER.Value).Distinct().ToArray()).ToDictionary(l => l.ID_METER);
                Dictionary<long, OpLocation> LocationDict = dataProvider.GetLocation(list.Where(l => l.ID_LOCATION.HasValue).Select(l => l.ID_LOCATION.Value).Distinct().ToArray()).ToDictionary(l => l.ID_LOCATION);
                foreach (var loop in list)
                {
                    loop.AlarmDef = AlarmDefDict.TryGetValue(loop.ID_ALARM_DEF);
                    if (loop.ID_DATA_ARCH.HasValue)
                        loop.DataArch = DataArchDict.TryGetValue(loop.ID_DATA_ARCH.Value);
                    if (loop.ID_METER.HasValue)
                        loop.Meter = MeterDict.TryGetValue(loop.ID_METER.Value);
                    if (loop.SERIAL_NBR.HasValue)
                        loop.Device = DeviceDict.TryGetValue(loop.SERIAL_NBR.Value);
                    if (loop.ID_LOCATION.HasValue)
                        loop.Location = LocationDict.TryGetValue(loop.ID_LOCATION.Value);
                }
            }
        }
    #endregion

    #region LoadCustomData
        private static void LoadCustomData(ref List<OpAlarmEvent> list, IDataProvider dataProvider)
        {
        }
    #endregion

    #region	IComparable Members
        //Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpAlarmEvent)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpAlarmEvent).ToString());
            }
            return 0;
        }
    #endregion

    #region IEquatable<OpAlarmEvent> Members
        public bool Equals(OpAlarmEvent other)
        {
            if (other == null)
                return false;
            return this.IdAlarmEvent.Equals(other.IdAlarmEvent);
        }
    #endregion

    #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is OpAlarmEvent)
                return this.IdAlarmEvent == ((OpAlarmEvent)obj).IdAlarmEvent;
            else
                return base.Equals(obj);
        }
        public static bool operator ==(OpAlarmEvent left, OpAlarmEvent right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        public static bool operator !=(OpAlarmEvent left, OpAlarmEvent right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }

        public override int GetHashCode()
        {
            return IdAlarmEvent.GetHashCode();
        }
    #endregion

    #region Implementation of IOpDynamic
        public object Key
        {
            get { return IdAlarmEvent; }
        }

        public OpDynamicPropertyDict DynamicProperties
        {
            get { return this.dynamicProperties; }
            set { this.dynamicProperties = value; }
        }

        OpAlarmEvent IOpDynamicProperty<OpAlarmEvent>.Owner
        {
            get { return this; }
        }

        public Dictionary<string, object> DynamicValues
        {
            get { return dynamicValues; }
            set { dynamicValues = value; }
        }

        public object Owner
        {
            get { return this; }
        }
    #endregion

    }
#endif
}