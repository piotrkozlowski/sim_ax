using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;


namespace IMR.Suite.UI.Business.Objects.CORE
{
#if !USE_UNITED_OBJECTS
    [Serializable]
	public class OpDistributorHierarchy : DB_DISTRIBUTOR_HIERARCHY, IComparable, IEquatable<OpDistributorHierarchy>
    {
    #region Properties
            public int IdDistributorHierarchy { get { return this.ID_DISTRIBUTOR_HIERARCHY;} set {this.ID_DISTRIBUTOR_HIERARCHY = value;}}
                public int? IdDistributorHierarchyParent { get { return this.ID_DISTRIBUTOR_HIERARCHY_PARENT;} set {this.ID_DISTRIBUTOR_HIERARCHY_PARENT = value;}}
                public int IdDistributor { get { return this.ID_DISTRIBUTOR;} set {this.ID_DISTRIBUTOR = value;}}
                public string Hierarchy { get { return this.HIERARCHY;} set {this.HIERARCHY = value;}}
    #endregion

    #region	Navigation Properties
		private OpDistributor _Distributor;
        public OpDistributor Distributor { get { return this._Distributor; } set { this._Distributor = value; this.ID_DISTRIBUTOR = (value == null) ? 0 : (int)value.ID_DISTRIBUTOR; } }
    #endregion

    #region	Custom Properties
    #endregion
		
    #region	Ctor
		public OpDistributorHierarchy()
			:base() {}
		
		public OpDistributorHierarchy(DB_DISTRIBUTOR_HIERARCHY clone)
			:base(clone) {}
    #endregion
		
    #region	ToString
		public override string ToString()
		{
				return this.HIERARCHY;
		}
    #endregion

    #region	ConvertList
		public static List<OpDistributorHierarchy> ConvertList(DB_DISTRIBUTOR_HIERARCHY[] db_objects, IDataProvider dataProvider)
        {
            return ConvertList(db_objects, dataProvider, true);
        }
		
		public static List<OpDistributorHierarchy> ConvertList(DB_DISTRIBUTOR_HIERARCHY[] db_objects, IDataProvider dataProvider, bool loadNavigationProperties)
		{
			List<OpDistributorHierarchy> ret = new List<OpDistributorHierarchy>(db_objects.Length);
			db_objects.ToList().ForEach(db_object => ret.Add(new OpDistributorHierarchy(db_object)));
			
			if(loadNavigationProperties)
				LoadNavigationProperties(ref ret, dataProvider); // Loads navigation properties
			
			LoadCustomData(ref ret, dataProvider); // Loads user custom data
			return ret;
		}
    #endregion
		
    #region LoadNavigationProperties
		public static void LoadNavigationProperties(ref List<OpDistributorHierarchy> list, IDataProvider dataProvider)
        {
			if (list != null && list.Count > 0)
            {
			Dictionary<int,OpDistributor> DistributorDict = dataProvider.GetDistributor(list.Select(l => l.ID_DISTRIBUTOR).Distinct().ToArray()).ToDictionary(l => l.ID_DISTRIBUTOR);
			
				foreach (var loop in list)
				{
						loop.Distributor = DistributorDict.TryGetValue(loop.ID_DISTRIBUTOR); 
				}
			}
		}
    #endregion
		
    #region LoadCustomData
		private static void LoadCustomData(ref List<OpDistributorHierarchy> list, IDataProvider dataProvider)
        {
		}
    #endregion
		
    #region	IComparable Members
		//Used for XtraGridControl column sort feature
        public int CompareTo(object obj)
        {
            if (obj is OpDistributorHierarchy)
            {
                if (this.ToString() != null)
                    return this.ToString().CompareTo((obj as OpDistributorHierarchy).ToString());
                else
                    return 0;
            }
            else
                return 0;
        }
    #endregion
		
    #region IEquatable<OpDistributorHierarchy> Members
        public bool Equals(OpDistributorHierarchy other)
        {
            if (other == null)
				return false;
			return this.IdDistributorHierarchy.Equals(other.IdDistributorHierarchy);
        }
    #endregion

    #region override Equals
		public override bool Equals(object obj)
		{
			if (obj is OpDistributorHierarchy)
				return this.IdDistributorHierarchy == ((OpDistributorHierarchy)obj).IdDistributorHierarchy;
			else
				return base.Equals(obj);
		}
		public static bool operator ==(OpDistributorHierarchy left, OpDistributorHierarchy right)
		{
			if ((object)left == null)
				return ((object)right == null);
			return left.Equals(right);
		}
		public static bool operator !=(OpDistributorHierarchy left, OpDistributorHierarchy right)
		{
			if ((object)left == null)
				return ((object)right != null);
			return !left.Equals(right);
		}

		public override int GetHashCode()
		{
			return IdDistributorHierarchy.GetHashCode();
		}
    #endregion
    }
#endif
}