﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects;

namespace UI.Business.Objects
{
    public interface IOpDataProperty
    {
        object GetOpDataPropertyValue(OpDataProperty opDataProperty);
        void SetOpDataPropertyValue(OpDataProperty opDataProperty, object value);
    }
}
