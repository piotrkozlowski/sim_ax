﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using Objects_DW = IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class DataArchComponent
    {
        #region Methods
        #region RoundValues - List of DW.OpData
        public static List<Objects.DW.OpData> RoundValues(List<Objects.DW.OpData> dataSource, Dictionary<long, int> dataTypeUnitDict)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            int decimalPlaces = dataTypeUnitDict[dataType.IdDataType];
                            if ((dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d => d.Value = Math.Round(GenericConverter.Parse<double>(d.Value), decimalPlaces));
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d => d.Value = Math.Round(GenericConverter.Parse<decimal>(d.Value), decimalPlaces));
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
        #region RoundValues - List of DW.OpDataArch
        public static List<Objects.DW.OpDataArch> RoundValues(List<Objects.DW.OpDataArch> dataSource, Dictionary<long, int> dataTypeUnitDict)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            int decimalPlaces = dataTypeUnitDict[dataType.IdDataType];
                            if ((dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d => d.Value = Math.Round(GenericConverter.Parse<double>(d.Value), decimalPlaces));
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d => d.Value = Math.Round(GenericConverter.Parse<decimal>(d.Value), decimalPlaces));
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
        #region RoundValues - List of DW.OpDataTemporal
        public static List<Objects.DW.OpDataTemporal> RoundValues(List<Objects.DW.OpDataTemporal> dataSource, Dictionary<long, int> dataTypeUnitDict)
        {
            if (dataSource != null && dataSource.Count > 0)
            {
                if (dataTypeUnitDict.Keys.Count > 0)
                {
                    List<OpDataType> dataTypesList = dataSource.Select(d => d.DataType).Distinct().ToList();
                    foreach (OpDataType dataType in dataTypesList)
                    {
                        if (dataTypeUnitDict.ContainsKey(dataType.IdDataType))
                        {
                            int decimalPlaces = dataTypeUnitDict[dataType.IdDataType];
                            if ((dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                            {
                                switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                                {
                                    case Enums.DataTypeClass.Integer:
                                    case Enums.DataTypeClass.Real:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d => d.Value = Math.Round(GenericConverter.Parse<double>(d.Value), decimalPlaces));
                                        break;
                                    case Enums.DataTypeClass.Decimal:
                                        dataSource.Where(d => d.IdDataType == dataType.IdDataType && d.Value != null).ToList().ForEach(d => d.Value = Math.Round(GenericConverter.Parse<decimal>(d.Value), decimalPlaces));
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            return dataSource;
        }
        #endregion
        #region RoundValues
        public static object RoundValues(object data, OpDataType dataType, Dictionary<long, OpDataTypeFormat> dataTypeFormatDict)
        {
            if (data != null && dataType != null)
            {
                if (dataTypeFormatDict.Keys.Count > 0)
                {
                    if (dataType != null && dataTypeFormatDict.ContainsKey(dataType.IdDataType))
                    {
                        OpDataTypeFormat newFormat = dataTypeFormatDict[dataType.IdDataType];
                        int? decimalPlaces = null;
                        if (newFormat.NumberMaxPrecision.HasValue)
                            decimalPlaces = newFormat.NumberMaxPrecision.Value;
                        else if (newFormat.NumberMinPrecision.HasValue)
                            decimalPlaces = newFormat.NumberMinPrecision.Value;
                        if (decimalPlaces.HasValue && (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Integer || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Real || dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal))
                        {
                            switch ((Enums.DataTypeClass)dataType.IdDataTypeClass)
                            {
                                case Enums.DataTypeClass.Integer:
                                case Enums.DataTypeClass.Real:
                                    data = Math.Round(GenericConverter.Parse<double>(data), decimalPlaces.Value);
                                    break;
                                case Enums.DataTypeClass.Decimal:
                                    data = Math.Round(GenericConverter.Parse<decimal>(data), decimalPlaces.Value);
                                    break;
                            }
                        }
                    }
                }
            }
            return data;
        }
        #endregion

        #region Save
        public static long Save(DataProvider dataProvider, Objects_DW.OpDataArch objectToSave)
        {
            return dataProvider.SaveDataArch(objectToSave);
        }
        #endregion
        #endregion
    }
}
