﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Components.CORE;
using IMR.Suite.UI.Business.Objects.DAQ;
using System.Data;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class PerformanceComponent
    {
        #region Members

        public enum AggregateComputeType
        {
            SimpleAvg = 1,
            ShorterPeriodAvg = 2,// w kontekście tego samego performacu wyciągamy niższy agregat, dla agregatu dziennego np 15 minutowy
            HigherAggregate = 3,//liczymy agregat wyższy w heirarchi czyli np agregat dystrybutora na podstawie agregatów lokalziacji
            AverageLowerAggregates = 4,//liczymy agregat na podstwie wszystkich ShorterPeriodAvg
            NotCalculate = 5,//agregat dostarczany z innego źródła, nie chcemy go przeliczać
            DeviceCalculate = 6, // Metoda wyspecjalizowana do obliczania agregatów tylko i wyłącznie związanych z urządzeniami.
            //BaseAggregate = 6 // liczymy agregat wyzszy na podstawie wszystkich niższych dla konkretnego obiektu
            // np. agregat dzienny dla konkretnego raportu na podstawie 15 minutowych dla tego raportu
        }

        public enum DeviceReadoutScheduleOkoComputingMode
        {
            Unknown = 0,
            AlevelAverage = 1, // Liczymy średnią z aleveli
            OkoReadout = 2,
        }

        #endregion

        #region Properties
        public static List<OpDataType> DataTemporalPerformances { get; set; }
        public static List<OpDataType> DataTemporalDevicePerformances { get; set; }
        public static List<OpDataType> DistributorPerformances { get; set; }
        public static List<OpDataType> ImrServerPerformances { get; set; }
        public static List<OpDataType> ReportPerformances { get; set; }
        public static List<OpDataType> QueuePerformances { get; set; }
        public static List<OpDataType> ProcessPerformances { get; set; }
        public static List<OpDataType> TransmissionDriverPerformances { get; set; }
        public static List<OpDataType> EtlPerformances { get; set; }

        public const long MaximumNumberOfRevolutionsWhileLoop = 1000000; // maksymalna liczba obrotów pętli jesli przekroczy generowany jest wyjatek.

        public static int DaysPastToCompute = 2;
        public static DateTime ReferenceDate = new DateTime();
        public static bool ComputeHigherAggregationThanDay = false; // Obliczanie wyższych agregatów niż dniowe np. week, month
        public const int QueueAggregateIndexNumber = 0; // Numer indeksu na którym zapisywane sa agregaty dla kolejek
        public const int ProcessAggregateIndexNumber = -1; // Numer indeksu na którym zapisywane sa agregaty dla Procesów
        public static List<int> AggregationTypeToAnalyze { get; set; } // Lista potrzebnych do obliczeń aggregation typow
        public static List<OpDaylightSavingTime> DaylightSavingTimeList { get; set; }

        public static Dictionary<OpDistributorData, GOPerformanceHierarchyItem> ReferenceHierarchy { get; set; }//wzorcowa hierarchia dla Dystrybutora = 1

        #endregion

        #region RoundToNearestInterval

        public static DateTime RoundToNearestInterval(DateTime dt, TimeSpan d)
        {
            int f = 0;
            double m = (double)(dt.Ticks % d.Ticks) / d.Ticks;
            if (m >= 0.5)
                f = 1;
            return new DateTime(((dt.Ticks / d.Ticks) + f) * d.Ticks);
        }

        #endregion
        #region RoundUpToNearestInterval

        public static DateTime RoundUpToNearestInterval(DateTime dt, TimeSpan d)
        {
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }

        #endregion
        #region RoundDownToNearestInterval

        public static DateTime RoundDownToNearestInterval(DateTime dt, TimeSpan d)
        {
            var delta = dt.Ticks % d.Ticks;
            return new DateTime(dt.Ticks - delta);
        }

        #endregion

        #region CreateImrServerDistributorDict

        public static Dictionary<OpImrServer, List<OpDistributor>> CreateImrServerDistributorDict(List<OpDistributor> DistributorList)
        {
            Dictionary<OpImrServer, List<OpDistributor>> imrServerDistributorDict = new Dictionary<OpImrServer, List<OpDistributor>>();
            foreach (OpDistributor dItem in DistributorList)
            {
                if (!imrServerDistributorDict.ContainsKey(dItem.ImrServer))
                    imrServerDistributorDict.Add(dItem.ImrServer, new List<OpDistributor>());
                imrServerDistributorDict[dItem.ImrServer].Add(dItem);
            }
            return imrServerDistributorDict;
        }

        #endregion

        #region CreateImrServerWorkspace

        public static List<PerformanceComponent.GOImrServer> CreateImrServerWorkspace(DataProvider DataProvider,
                                                                                      List<OpDistributor> DistributorList,
                                                                                      List<OpReport> ReportList = null,
                                                                                      bool hierarchyWithIgnoredNodes = false,
                                                                                      bool loadAlarmNotificationsReferenceValue = true)
        {
            List<PerformanceComponent.GOImrServer> goisList = new List<PerformanceComponent.GOImrServer>();
            Dictionary<OpImrServer, List<OpDistributor>> imrServerDistributorDict = CreateImrServerDistributorDict(DistributorList);

            #region GetDaylightSavingTime
            if (DaylightSavingTimeList == null || DaylightSavingTimeList.Count() == 0)
            {
                PerformanceComponent.DaylightSavingTimeList = new List<OpDaylightSavingTime>();
                PerformanceComponent.DaylightSavingTimeList = DataProvider.GetDaylightSavingTimeFilter(loadNavigationProperties: false,
                                                                                 autoTransaction: false,
                                                                                 transactionLevel: System.Data.IsolationLevel.ReadUncommitted);
            }
            #endregion

            if (imrServerDistributorDict != null && imrServerDistributorDict.Count > 0 && !imrServerDistributorDict.Keys.ToList().Exists(s => s.DwServerId.HasValue))
            {
                List<long> imrServerDataTypesToGet = new List<long>();
                imrServerDataTypesToGet.Add(DataType.IMR_SERVER_DW_SERVER_ID);
                imrServerDataTypesToGet.Add(DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_ADDRESS);
                List<OpImrServerData> isdList = DataProvider.GetImrServerDataFilter(IdImrServer: imrServerDistributorDict.Keys.Select(k => k.IdImrServer).ToArray(),
                                                                                    IdDataType: imrServerDataTypesToGet.ToArray());

                foreach (OpImrServer isItem in imrServerDistributorDict.Keys)
                {
                    isItem.DataList.RemoveAll(d => d.IdDataType.In(imrServerDataTypesToGet.ToArray()));
                    isItem.DataList.AddRange(isdList.Where(d => d.IdImrServer == isItem.IdImrServer));
                }
            }

            foreach (OpImrServer isItem in imrServerDistributorDict.Keys)
            {
                // Jeśli serwer nie ma przypisanego dsytrybutora serwerowego to go pomijamy.
                OpDistributor serverDistributor = ImrServerComponent.GetServerDistributor(DataProvider, isItem.IdImrServer);
                if (serverDistributor == null)
                    continue;
                else
                {
                    if (!imrServerDistributorDict[isItem].Exists(e => e.IdDistributor == serverDistributor.IdDistributor))
                        imrServerDistributorDict[isItem].Add(serverDistributor);
                }

                PerformanceComponent.GOImrServer goisItem = new PerformanceComponent.GOImrServer(isItem, imrServerDistributorDict[isItem], ReportList, DataProvider);

                if (goisItem != null && (!goisItem.Server.IsGood || !goisItem.Server.IsImrlt || goisItem.Server.ImrServerVersion < (int)Enums.ImrServerVersion.IMR4))
                    continue;

                List<long> idLocationBaseDataTypes = new List<long>();
                idLocationBaseDataTypes.Add(DataType.MON_FP_AGGR_PC);
                idLocationBaseDataTypes.Add(DataType.MON_FP_AGGR_SERVICE);
                idLocationBaseDataTypes.Add(DataType.MON_FP_AGGR_TANKS);
                idLocationBaseDataTypes.Add(DataType.MON_FP_AGGR_SALES);

                idLocationBaseDataTypes.Add(DataType.MON_AWSR_RADIO_FRAMES);
                idLocationBaseDataTypes.Add(DataType.MON_AWSR_RADIO_AMPLIFIER);
                idLocationBaseDataTypes.Add(DataType.MON_AWSR_ATG);
                idLocationBaseDataTypes.Add(DataType.MON_AWSR_LON);
                idLocationBaseDataTypes.Add(DataType.MON_AWSR_PUMP_CONVERTER);
                idLocationBaseDataTypes.Add(DataType.MON_AWSR_IO_CONTROLLER);

                idLocationBaseDataTypes.Add(DataType.MON_IS_WORKING);
                idLocationBaseDataTypes.Add(DataType.MON_PERC_CPU);
                idLocationBaseDataTypes.Add(DataType.MON_BYTE_RAM);
                idLocationBaseDataTypes.Add(DataType.MON_DISK);
                idLocationBaseDataTypes.Add(DataType.MON_NETWORK);

                // W aplikacji dbamy o to żeby część hierarchi dla serwera byla jednakowa dla wszystkich dystrybutorów jacy się na nim znajdują
                // Należy pamiętać, ze hierarchia serwerowa musi być utworzona przed dystrybutorową! ponieważ jest wykorzystywana w Create[...]AggregatePerformancesList
                goisItem.ServerPerformanceHierarchy = CreatePerformanceHierarchy(DataProvider,
                                                                goisItem.DistributorList.Select(d => d.IdDistributor).ToArray(),
                                                                hierarchyWithIgnoredNodes,
                                                                isServerHierarchy: true,
                                                                loadAlarmNotificationsReferenceValue: loadAlarmNotificationsReferenceValue)[1/*goisItem.DistributorList[0].IdDistributor*/];

                foreach (OpDistributor dItem in goisItem.DistributorList)
                {
                    #region GetDataTypesList
                    //docelowo pobierana z bazy danych,dla konkretnego dystrybutora z konkretnymi wagami
                    //oraz aggregation typ dla ktorych maja byc wyliczone
                    /*
                    List<OpDataType> dtList = new List<OpDataType>();
                    List<long> dtImrServer = new List<long>();
                    List<long> dataTypes = new List<long>();
                    dataTypes.Add(DataType.MON_FP_AGGR_PC);
                    dataTypes.Add(DataType.MON_FP_AGGR_SERVICE);
                    dataTypes.Add(DataType.MON_FP_AGGR_TANKS);
                    dataTypes.Add(DataType.MON_FP_AGGR_SALES);
                    dataTypes.Add(DataType.DISTRIBUTOR_PERFORMANCE_VALUE);
                    dataTypes.Add(DataType.LOCATION_PERFORMANCE_VALUE);
                    dataTypes.Add(DataType.IMR_SERVER_PERFORMANCE_VALUE);

                    dtImrServer.Add(DataType.IMR_SERVER_PERFORMANCE_VALUE);

                    dtList.AddRange(DataProvider.GetDataType(dataTypes.ToArray()));

                    foreach (OpDataType dtItem in dtList)
                    {
                        List<Enums.AggregationType> allowedAggregationType = new List<Enums.AggregationType>();
                        #region Distributor 19 - Shell
                        switch (dtItem.IdDataType)
                        {
                            case DataType.DISTRIBUTOR_PERFORMANCE_VALUE:
                                allowedAggregationType.Add(Enums.AggregationType.Quarter);
                                allowedAggregationType.Add(Enums.AggregationType.Day);
                                break;
                            case DataType.IMR_SERVER_PERFORMANCE_VALUE:
                                allowedAggregationType.Add(Enums.AggregationType.Quarter);
                                allowedAggregationType.Add(Enums.AggregationType.Day);
                                break;
                            case DataType.LOCATION_PERFORMANCE_VALUE:
                                allowedAggregationType.Add(Enums.AggregationType.Quarter);
                                allowedAggregationType.Add(Enums.AggregationType.Day);
                                break;
                            case DataType.MON_FP_AGGR_PC:
                                allowedAggregationType.Add(Enums.AggregationType.Quarter);
                                allowedAggregationType.Add(Enums.AggregationType.Day);
                                break;
                            case DataType.MON_FP_AGGR_SERVICE:
                                allowedAggregationType.Add(Enums.AggregationType.Quarter);
                                allowedAggregationType.Add(Enums.AggregationType.Day);
                                break;
                            case DataType.MON_FP_AGGR_TANKS:
                                allowedAggregationType.Add(Enums.AggregationType.Quarter);
                                allowedAggregationType.Add(Enums.AggregationType.Day);
                                break;
                            case DataType.MON_FP_AGGR_SALES:
                                allowedAggregationType.Add(Enums.AggregationType.Quarter);
                                allowedAggregationType.Add(Enums.AggregationType.Day);
                                break;
                            default:
                                break;
                        }
                        
                        #endregion
                    }
                    */
                    #endregion

                    goisItem.PerformancesHierarchy[dItem.IdDistributor] = PerformanceComponent.CreatePerformanceHierarchy(DataProvider,
                                                                                new int[] { dItem.IdDistributor },
                                                                                hierarchyWithIgnoredNodes,
                                                                                loadAlarmNotificationsReferenceValue: loadAlarmNotificationsReferenceValue)[dItem.IdDistributor];

                    goisItem.BasePerformances[dItem.IdDistributor] = goisItem.CreateBasePerformancesList(dItem.IdDistributor);
                    goisItem.LocationAggregatePerformances[dItem.IdDistributor] = goisItem.CreateLocationAggregatePerformancesList(dItem.IdDistributor);
                    goisItem.DeviceAggregatePerformances[dItem.IdDistributor] = goisItem.CreateDeviceAggregatePerformancesList(dItem.IdDistributor);
                    goisItem.MeterAggregatePerformances[dItem.IdDistributor] = goisItem.CreateMeterAggregatePerformancesList(dItem.IdDistributor);
                    goisItem.DistributorAggregatePerformances[dItem.IdDistributor] = goisItem.CreateDistributorAggregatePerformancesList(dItem.IdDistributor);
                    goisItem.ImrServerAggregatePerformances = goisItem.CreateImrServerAggregatePerformancesList();
                    goisItem.ReportAggregatePerformances = goisItem.CreateReportAggregatePerformancesList();
                    goisItem.QueueAggregatePerformances = goisItem.CreateQueueAggregatePerformancesList();
                    goisItem.ProcessAggregatePerformances = goisItem.CreateProcessAggregatePerformancesList();
                    goisItem.TransmissionDriverAggregatePerformances = goisItem.CreateTransmissionDriverAggregatePerformancesList();
                    goisItem.EtlAggregatePerformances = goisItem.CreateEtlAggregatePerformancesList();
                }

                // W przypadku jeśli mamy serwer i żadnego z przypisanych dla niego dystrybutorów nie obserwujemy w Perfomance to go pomijamy. 
                // Nie tworzymy hierarchi i nie dodajemy go do ImrServerWorkspace
                bool distributorsOnServerHavePerformancesHierarchy = false;
                foreach (OpDistributor dItem in goisItem.DistributorList)
                {
                    if (goisItem.PerformancesHierarchy[dItem.IdDistributor].Count > 0)
                    {
                        distributorsOnServerHavePerformancesHierarchy = true;
                        break;
                    }
                }
                if (!distributorsOnServerHavePerformancesHierarchy)
                    continue;

                goisList.Add(goisItem);
            }

            return goisList;
        }

        #endregion

        #region CreatePerformanceHierarchy

        public static Dictionary<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>> CreatePerformanceHierarchy(DataProvider dataProvider, int[] idDistributor,
                                                                                                                            bool withIgnoredNodes = false,
                                                                                                                            bool isServerHierarchy = false,
                                                                                                                            bool loadAlarmNotificationsReferenceValue = true)
        {
            if (idDistributor != null && idDistributor.Count() > 0)
            {
                List<OpDistributor> dList = dataProvider.GetDistributor(idDistributor);
                return CreatePerformanceHierarchy(dataProvider, dList, withIgnoredNodes: withIgnoredNodes, isServerHierarchy: isServerHierarchy, loadAlarmNotificationsReferenceValue: loadAlarmNotificationsReferenceValue);
            }
            return new Dictionary<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>>();
        }

        public static Dictionary<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>> CreatePerformanceHierarchy(DataProvider dataProvider, List<OpDistributor> distributorList,
                                                                                                                            bool withIgnoredNodes = false,
                                                                                                                            bool isServerHierarchy = false,
                                                                                                                            bool loadAlarmNotificationsReferenceValue = true)
        {
            Dictionary<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>> retDict = new Dictionary<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>>();

            if (distributorList != null && distributorList.Count > 0)
            {

                foreach (int iItem in distributorList.Select(d => d.IdDistributor))
                    retDict.Add(iItem, new Dictionary<OpDistributorData, GOPerformanceHierarchyItem>());

                #region DataTypesToGet
                List<long> dataTypesToGet = new List<long>();
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_PARENT);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_RED);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_YELLOW);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_WEIGHT);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_IGNORED);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AGGREGATION_COMPUTE_TYPE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MAX_AGGREGATION_TYPE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_DOT_VISIBLE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_ADDITIONAL_EMAIL);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_LOCATION_IN_KPI);
                //dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_INDEX);
                //dla trzech poniższych data typów rezerwujemy 1000 indeksów - dla indeksu DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM == 1 mamy 1000 - 1999
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_ID_REFERENCE_TYPE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_REFERENCE_VALUE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_TYPE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_VALUE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ID_REFERENCE_TYPE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_OKO_READOUT_COMPUTING_MODE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_ISSUE);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_ISSUE_WITH_TASK);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_TASK);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ACCEPTABLE_INCORRECT_TIME);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ACCEPTABLE_INCORRECT_TIME_UNIT);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_RESPONSIBLE_OPERATOR);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MAX_DAY_ACCEPTABLE_INCORRECT_TIME);
                dataTypesToGet.Add(DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MONITORED_TYPE_OF_AGGREGATION);

                dataTypesToGet.Add(DataType.DISTRIBUTOR_IMR_SERVER_ID);
                #endregion

                List<OpDistributorData> ddList = dataProvider.GetDistributorDataFilter(IdDistributor: distributorList.Select(d => d.IdDistributor).ToArray(), IdDataType: dataTypesToGet.ToArray());
                List<OpDataType> dtList = null;
                if (ddList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM && d.Value != null))
                {
                    List<long> idDataType = new List<long>();
                    foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM && d.Value != null))
                    {
                        idDataType.Add(Convert.ToInt64(ddItem.Value));
                    }
                    idDataType = idDataType.Distinct().ToList();
                    dtList = dataProvider.GetDataType(idDataType.ToArray());
                }

                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_IMR_SERVER_ID || d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR))
                {
                    OpDistributor dItem = distributorList.Find(d => d.IdDistributor == ddItem.IdDistributor);
                    dItem.DataList.Add(ddItem);
                }

                if (distributorList.Exists(d => d.IdDistributor != 1 && d.IdIMRServer.HasValue))
                {
                    List<OpImrServer> isList = dataProvider.GetImrServer(distributorList.Select(d => d.IdIMRServer.Value).Distinct().ToArray());
                    foreach (OpDistributor dItem in distributorList)
                    {
                        dItem.ImrServer = isList.Find(s => s.IdImrServer == dItem.IdIMRServer);
                    }
                }

                foreach (int iItem in distributorList.Select(d => d.IdDistributor))
                {
                    if (!ddList.Exists(e => e.IdDistributor == iItem && e.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM))
                        continue;

                    Dictionary<OpDistributorData, GOPerformanceHierarchyItem> hierarchyTmpDict = CreateDistributorPerformanceHierarchy(dataProvider,
                                                                                                                            distributorList.Find(d => d.IdDistributor == iItem),
                                                                                                                            ddList.Where(d => d.IdDistributor == iItem).ToList(),
                                                                                                                            dtList, withIgnoredNodes,
                                                                                                                            loadAlarmNotificationsReferenceValue: loadAlarmNotificationsReferenceValue);
                    retDict[iItem] = hierarchyTmpDict;
                }

                if (isServerHierarchy)
                {
                    //zakładamy że częśc hierarchi serwerowej jest taka sama dla wszystkich dystrybutorów danego serwera
                    //wyciągamy dystrybutora serwerowego
                    //dla dystrybutora serwerowego sprawadzamy, czy inni dystrybutorzy mają wykonfiguraowane DistributorPerformance i jeśli tak
                    //to dodajemy do hierarchii serwera ten węzeł (domyślnie jest on pomijany)

                    Dictionary<OpDistributorData, GOPerformanceHierarchyItem> serverHierarchy = new Dictionary<OpDistributorData, GOPerformanceHierarchyItem>();

                    // Sprawdzamy czy dla któregoś z dystrybutorów na danym serwerze wykonfigurowana jest hierarchia
                    //    - Jeśli nie, to sprawdzanie czy istnieje dystrybutor serwerowy nie ma sensu, bo nawet jesli będzie to i tak hierarchia dla niego będzie pusta.
                    //    - Jeśli jest to sprawdzamy czy na tym serwerze istnieje dystrybutor serwerowy, może się zdażyć, że ktoś go odpiął wtedy wyświetlamy komunikat o jego braku.
                    bool distributorsOnServerHavePerformancesHierarchy = false;
                    foreach (KeyValuePair<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>> kvItem in retDict)
                    {
                        if (kvItem.Value.Count > 0)
                            distributorsOnServerHavePerformancesHierarchy = true;
                    }

                    if (!distributorsOnServerHavePerformancesHierarchy)
                    {
                        retDict.Clear();
                        retDict.Add(1, serverHierarchy);
                    }
                    else
                    {
                        if (distributorList.Select(d => d.IdIMRServer.Value).Distinct().Count() > 1)
                            throw new Exception("Multiple IMR Server for dustributor list");
                        OpDistributor serverDistributor = distributorList.Find(d => d.DataList.Exists(d1 => d1.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR && d1.Value != null && Convert.ToBoolean(d1.Value)));
                        if (serverDistributor == null)
                            serverDistributor = ImrServerComponent.GetServerDistributor(dataProvider, distributorList.First().IdIMRServer.Value);
                        if (serverDistributor != null && !retDict.ContainsKey(serverDistributor.IdDistributor))
                        {
                            List<OpDistributorData> ddListForServerDistributor = dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { serverDistributor.IdDistributor },
                                                                                                                       IdDataType: dataTypesToGet.ToArray());

                            Dictionary<OpDistributorData, GOPerformanceHierarchyItem> hierarchyTmpDict = CreateDistributorPerformanceHierarchy(dataProvider,
                                                                                                                                serverDistributor,
                                                                                                                                ddListForServerDistributor,
                                                                                                                                dtList, withIgnoredNodes,
                                                                                                                                loadAlarmNotificationsReferenceValue: loadAlarmNotificationsReferenceValue);
                            retDict[serverDistributor.IdDistributor] = hierarchyTmpDict;
                        }

                        if (serverDistributor == null)
                            throw new Exception("Server distributor doesn't exists for server: " + distributorList.First().IdIMRServer.Value);
                        if (!retDict.ContainsKey(serverDistributor.IdDistributor))
                            throw new Exception("There is no hierarchy for Distributor: " + serverDistributor.IdDistributor);
                        serverHierarchy = retDict[serverDistributor.IdDistributor];
                        Dictionary<int, OpDistributor> dDict = new Dictionary<int, OpDistributor>();
                        dDict = distributorList.Where(w => w.IdIMRServer == serverDistributor.IdIMRServer && w.IdDistributor != serverDistributor.IdDistributor).ToDictionary(d => d.IdDistributor);

                        OpDistributorData ddItem = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM &&
                                                                                Convert.ToInt64(f.Value) == DataType.DISTRIBUTOR_PERFORMANCE_VALUE &&
                                                                                f.IdDistributor.In(dDict.Keys.ToArray()));

                        if (ddItem != null && serverHierarchy.Count() > 0 && retDict[ddItem.IdDistributor].ContainsKey(ddItem))
                        {
                            serverHierarchy.FirstOrDefault(d => Convert.ToInt64(d.Key.Value) == DataType.IMR_SERVER_PERFORMANCE_VALUE).Value.Childs.Insert(0, ddItem);

                            if (!serverHierarchy.Keys.ToList().Exists(e => Convert.ToInt64(e.Value) == DataType.DISTRIBUTOR_PERFORMANCE_VALUE))
                                serverHierarchy.Add(ddItem, retDict[ddItem.IdDistributor][ddItem]);
                            else
                            {
                                serverHierarchy.Remove(serverHierarchy.Keys.FirstOrDefault(f => Convert.ToInt64(f.Value) == DataType.DISTRIBUTOR_PERFORMANCE_VALUE));
                                serverHierarchy.Add(ddItem, retDict[ddItem.IdDistributor][ddItem]);
                            }
                            serverHierarchy[ddItem].ParentDistributor = true;

                            // Przypisywanie thresholdow z konfiguracji dystrybutora serwerowego.
                            OpDistributorData ddItemRedThreshold = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_RED &&
                                                                                                f.IdDistributor == serverDistributor.IdDistributor &&
                                                                                                f.Index == ddItem.Index);
                            OpDistributorData ddItemYellowThreshold = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_YELLOW &&
                                                                                                f.IdDistributor == serverDistributor.IdDistributor &&
                                                                                                f.Index == ddItem.Index);
                            if (ddItemRedThreshold != null && ddItemYellowThreshold != null)
                            {
                                retDict[ddItem.IdDistributor][ddItem].SetYellowThreshold(Convert.ToInt32(ddItemYellowThreshold.Value));
                                retDict[ddItem.IdDistributor][ddItem].SetRedThreshold(Convert.ToInt32(ddItemRedThreshold.Value));
                            }
                        }

                        retDict.Clear();
                        retDict.Add(1, serverHierarchy);
                    }
                }
            }

            return retDict;
        }

        #endregion

        #region CreateDistributorPerformanceHierarchy
        public static Dictionary<OpDistributorData, GOPerformanceHierarchyItem> CreateDistributorPerformanceHierarchy(DataProvider dataProvider,
                                                                                                                      OpDistributor dItem,
                                                                                                                      List<OpDistributorData> ddList,
                                                                                                                      List<OpDataType> dtList,
                                                                                                                      bool withIgnoredNodes,
                                                                                                                      bool loadAlarmNotificationsReferenceValue = true)
        {
            BaseComponent.Log(EventID.Forms.PerformanceIndicatorCreateDistributorPerformanceHierarchyStart, true, new object[] { dItem.ToString() });
            BaseComponent.Log(EventID.Forms.PerformanceIndicatorCheckListCount, true, new object[] { "Distributor DataList", dItem.DataList == null ? "NULL" : dItem.DataList.Count().ToString() });
            BaseComponent.Log(EventID.Forms.PerformanceIndicatorCheckListCount, true, new object[] { "DistributorData List", ddList == null ? "NULL" : dItem.DataList.Count().ToString() });
            BaseComponent.Log(EventID.Forms.PerformanceIndicatorCheckListCount, true, new object[] { "DataType List", dtList == null ? "NULL" : dItem.DataList.Count().ToString() });

            Dictionary<OpDistributorData, GOPerformanceHierarchyItem> perfHierarchy = new Dictionary<OpDistributorData, GOPerformanceHierarchyItem>();

            OpDistributorData serverDistributor = dItem.DataList.FirstOrDefault(d => d.DataType.IdDataType == (int)DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR);
            BaseComponent.Log(EventID.Forms.PerformanceIndicatorIsServerDistributor, true, new object[] { serverDistributor == null ? "NULL" : serverDistributor.VALUE.ToString() });

            if (ddList != null && ddList.Count > 0)
            {
                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM))
                {
                    OpDistributorData ddIgnoredItem = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_IGNORED && d.Index == ddItem.Index);
                    if (withIgnoredNodes || ddIgnoredItem == null || !Convert.ToBoolean(ddIgnoredItem.Value))
                    {
                        OpDistributorData ddParentItemPointer = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_PARENT && d.Index == ddItem.Index);
                        OpDistributorData ddParentItem = null;
                        if (ddParentItemPointer != null)
                            ddParentItem = ddList.Find(d => d.IdDistributorData == Convert.ToInt64(ddParentItemPointer.Value));
                        OpDistributorData ddCurrentItem = ddItem;
                        OpDistributorData ddRedThreshold = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_RED && d.Index == ddItem.Index);
                        OpDistributorData ddYellowThreshold = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_THRESHOLD_YELLOW && d.Index == ddItem.Index);
                        OpDistributorData ddWeight = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_WEIGHT && d.Index == ddItem.Index);
                        OpDistributorData ddAlarmNotifications = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS && d.Index == ddItem.Index);
                        OpDistributorData ddReferenceType = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ID_REFERENCE_TYPE && d.Index == ddItem.Index);
                        OpDistributorData ddAggregationComputeType = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AGGREGATION_COMPUTE_TYPE && d.Index == ddItem.Index);
                        OpDistributorData ddMaxAggregationType = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MAX_AGGREGATION_TYPE && d.Index == ddItem.Index);
                        OpDistributorData ddDotVisible = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_DOT_VISIBLE && d.Index == ddItem.Index);
                        OpDistributorData ddLocationInKpi = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_LOCATION_IN_KPI && d.Index == ddItem.Index);
                        OpDistributorData ddComputingMode = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_OKO_READOUT_COMPUTING_MODE && d.Index == ddItem.Index);
                        OpDistributorData ddAutoCreateIssue = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_ISSUE && d.Index == ddItem.Index);
                        OpDistributorData ddAutoCreateIssueWithTask = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_ISSUE_WITH_TASK && d.Index == ddItem.Index);
                        OpDistributorData ddAutoCreateTask = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_AUTO_CREATE_TASK && d.Index == ddItem.Index);
                        OpDistributorData ddAcceptableIncorrectTime = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ACCEPTABLE_INCORRECT_TIME && d.Index == ddItem.Index);
                        OpDistributorData ddAcceptableIncorrectTimeUnit = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ACCEPTABLE_INCORRECT_TIME_UNIT && d.Index == ddItem.Index);
                        OpDistributorData ddResponsibleOperator = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_RESPONSIBLE_OPERATOR && d.Index == ddItem.Index);
                        OpDistributorData ddMaxDayAcceptableIncorrectTime = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MAX_DAY_ACCEPTABLE_INCORRECT_TIME && d.Index == ddItem.Index);
                        OpDistributorData ddMonitoredTypeOfAggregation = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_MONITORED_TYPE_OF_AGGREGATION && d.Index == ddItem.Index);

                        OpDataType dtParent = null;
                        if (ddParentItem != null)
                            dtParent = dtList.Find(d => d.IdDataType == Convert.ToInt64(ddParentItem.Value));
                        OpDataType dtCurrent = dtList.Find(d => d.IdDataType == Convert.ToInt64(ddCurrentItem.Value));
                        OpDistributorData ddAddistionalEmailReceivers = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_ADDITIONAL_EMAIL && d.Index == ddItem.Index);

                        int maxDayAcceptableIncorrectTime = 1;
                        if (dtCurrent != null && dtCurrent.IdDataType == DataType.PERFORMANCE_DEVICE_SCHEDULED_MEASUREMENT_READOUT)
                            maxDayAcceptableIncorrectTime = 7;

                        GOPerformanceHierarchyItem gophiItem = new GOPerformanceHierarchyItem(ddCurrentItem.IdDistributorData,
                                                                                              ddCurrentItem,
                                                                                              ddParentItem,
                                                                                              dItem.ImrServer,
                                                                                              dItem,
                                                                                              Convert.ToDouble(ddWeight.Value),
                                                                                              Convert.ToDouble(ddRedThreshold.Value),
                                                                                              Convert.ToDouble(ddYellowThreshold.Value),
                                                                                              ddIgnoredItem == null || ddIgnoredItem.Value == null ? false : Convert.ToBoolean(ddIgnoredItem.Value),
                                                                                              ddAlarmNotifications == null || ddAlarmNotifications.Value == null ? false : Convert.ToBoolean(ddAlarmNotifications.Value),
                                                                                              ddDotVisible == null || ddDotVisible.Value == null ? false : Convert.ToBoolean(ddDotVisible.Value),
                                                                                              ddMaxAggregationType == null || ddMaxAggregationType.Value == null ? Enums.AggregationType.Day : (Enums.AggregationType)Convert.ToInt32(ddMaxAggregationType.Value),
                                                                                              ddMonitoredTypeOfAggregation == null || ddMonitoredTypeOfAggregation.Value == null ? new GOMonitoredTypeOfAggregation(Enums.AggregationType.Quarter) : new GOMonitoredTypeOfAggregation((Enums.AggregationType)Convert.ToInt32(ddMonitoredTypeOfAggregation.Value)),
                                                                                              ddComputingMode == null || ddComputingMode.Value == null ? PerformanceComponent.DeviceReadoutScheduleOkoComputingMode.AlevelAverage : (PerformanceComponent.DeviceReadoutScheduleOkoComputingMode)Convert.ToInt32(ddComputingMode.Value),
                                                                                              ddAutoCreateIssue == null || ddAutoCreateIssue.Value == null ? false : Convert.ToBoolean(ddAutoCreateIssue.Value),
                                                                                              ddAutoCreateIssueWithTask == null || ddAutoCreateIssueWithTask.Value == null ? false : Convert.ToBoolean(ddAutoCreateIssueWithTask.Value),
                                                                                              ddAutoCreateTask == null || ddAutoCreateTask.Value == null ? false : Convert.ToBoolean(ddAutoCreateTask.Value),
                                                                                              ddAcceptableIncorrectTime == null || ddAcceptableIncorrectTime.Value == null ? 1 : Convert.ToInt32(ddAcceptableIncorrectTime.Value),
                                                                                              ddAcceptableIncorrectTimeUnit == null || ddAcceptableIncorrectTimeUnit.Value == null ? Enums.ReportScheduleUnit.Day : (Enums.ReportScheduleUnit)Convert.ToInt32(ddAcceptableIncorrectTimeUnit.Value),
                                                                                              ddMaxDayAcceptableIncorrectTime == null || ddMaxDayAcceptableIncorrectTime.Value == null ? maxDayAcceptableIncorrectTime : Convert.ToInt32(ddMaxDayAcceptableIncorrectTime.Value));
                        if (ddLocationInKpi != null && ddLocationInKpi.Value != null && !String.IsNullOrEmpty(ddLocationInKpi.Value.ToString()))
                        {
                            gophiItem.LocationInKpiConstraint = Convert.ToBoolean(ddLocationInKpi.Value);
                        }
                        if (ddAggregationComputeType != null && ddAggregationComputeType.Value != null && !String.IsNullOrEmpty(ddAggregationComputeType.Value.ToString()))
                        {
                            gophiItem.AggregationComputeType = new GOAggregateComputeType((AggregateComputeType)Convert.ToInt32(ddAggregationComputeType.Value));
                        }
                        if (ddMaxAggregationType != null && ddMaxAggregationType.Value != null && !String.IsNullOrEmpty(ddMaxAggregationType.Value.ToString()))
                        {
                            gophiItem.AggregationType = new GOAggregateType((Enums.AggregationType)Convert.ToInt32(ddMaxAggregationType.Value));
                        }
                        if (ddReferenceType != null && ddReferenceType.Value != null && !String.IsNullOrEmpty(ddReferenceType.Value.ToString()))
                        {
                            gophiItem.ReferenceType = (Enums.ReferenceType)Convert.ToInt32(ddReferenceType.Value);
                        }

                        gophiItem.AlarmNotificationsReceiversData = ddAddistionalEmailReceivers;

                        if (ddResponsibleOperator == null)
                            gophiItem.ResponsibleOperatorData = new OpDistributorData();
                        else
                            gophiItem.ResponsibleOperatorData = ddResponsibleOperator;
                        //gophiItem.AlarmNotificationsReceivers = ddAddistionalEmailReceivers.Select(d => d.Value.ToString()).ToList();

                        List<OpDistributorData> childItemList = ddList.FindAll(d => d.IdDistributor == ddItem.IdDistributor
                                                                    && d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_PARENT
                                                                    && Convert.ToInt64(d.Value) == ddCurrentItem.IdDistributorData);

                        if (childItemList.Count > 0)
                        {
                            childItemList = ddList.FindAll(d => d.IdDistributor == ddItem.IdDistributor
                                                                        && d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM
                                                                        && d.Index.In(childItemList.Select(c => c.Index).Distinct().ToArray()));

                            foreach (OpDistributorData ddChildItem in childItemList)
                            {
                                OpDistributorData ddChildIgnored = ddList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_IGNORED
                                                                                    && d.Index == ddChildItem.Index);
                                if (serverDistributor != null && Convert.ToBoolean(serverDistributor.Value) == true && Convert.ToInt64(ddChildItem.Value) == DataType.DISTRIBUTOR_PERFORMANCE_VALUE)
                                    continue;
                                else
                                    if (withIgnoredNodes || ddChildIgnored == null || !Convert.ToBoolean(ddChildIgnored.Value))
                                {
                                    gophiItem.Childs.Add(ddChildItem);
                                }
                            }
                        }

                        #region AdditionalContraints

                        foreach (OpDistributorData ddConstraintItem in ddList.Where(d => (d.Index >= ddCurrentItem.Index * 1000 && d.Index < ((ddCurrentItem.Index + 1) * 1000))
                                                                                      && d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_ID_REFERENCE_TYPE))
                        {
                            List<OpDistributorData> ddConstraintReferenceValueList = ddList.FindAll(d => d.Index == ddConstraintItem.Index && d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_REFERENCE_VALUE);
                            gophiItem.AdditionalConditionsData.Add(ddConstraintItem);
                            gophiItem.AdditionalConditionsData.AddRange(ddConstraintReferenceValueList);
                        }
                        gophiItem.RefreshAddtionalConstraintsDict(dataProvider);

                        #endregion

                        #region AlarmNotificationsReferenceValue

                        if (loadAlarmNotificationsReferenceValue && ddAlarmNotifications != null && Convert.ToBoolean(ddAlarmNotifications.Value))
                        {
                            foreach (OpDistributorData ddAlarmNotivicationsReferenceValueItem in ddList.Where(d => (d.Index >= ddCurrentItem.Index * 1000 && d.Index < ((ddCurrentItem.Index + 1) * 1000))
                                                                                          && d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_TYPE))
                            {
                                List<OpDistributorData> ddAlarmNotivicationsReferenceValueList = ddList.FindAll(d => d.Index == ddAlarmNotivicationsReferenceValueItem.Index && d.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_VALUE);
                                gophiItem.AlarmNotificationsReferenceValueData.Add(ddAlarmNotivicationsReferenceValueItem);
                                gophiItem.AlarmNotificationsReferenceValueData.AddRange(ddAlarmNotivicationsReferenceValueList);
                            }
                            gophiItem.RefreshAlarmNotificationsReferenceValueDict(dataProvider);
                        }
                        #endregion

                        perfHierarchy.Add(gophiItem.DataType, gophiItem);
                    }
                }
            }

            return perfHierarchy;
        }


        #endregion

        #region CreatePerformancesGroups

        public static void CreatePerformancesGroups(DataProvider dataProvider)
        {
            DataTemporalPerformances = new List<OpDataType>();
            DataTemporalDevicePerformances = new List<OpDataType>();
            DistributorPerformances = new List<OpDataType>();
            ImrServerPerformances = new List<OpDataType>();
            ReportPerformances = new List<OpDataType>();
            QueuePerformances = new List<OpDataType>();
            ProcessPerformances = new List<OpDataType>();
            TransmissionDriverPerformances = new List<OpDataType>();
            EtlPerformances = new List<OpDataType>();

            List<OpDataTypeGroup> dtgList = dataProvider.GetDataTypeGroupFilter(customWhereClause: "[NAME] like '" + Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES.ToString()
                                                                                                + "' OR [NAME] like '" + Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES.ToString()
                                                                                                + "' OR [NAME] like '" + Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES.ToString()
                                                                                                + "' OR [NAME] like '" + Enums.DataTypeGroup.REPORT_PERFORMANCES.ToString()
                                                                                                + "' OR [NAME] like '" + Enums.DataTypeGroup.QUEUE_PERFORMANCES.ToString()
                                                                                                + "' OR [NAME] like '" + Enums.DataTypeGroup.PROCESS_PERFORMANCES.ToString()
                                                                                                + "' OR [NAME] like '" + Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES.ToString()
                                                                                                + "' OR [NAME] like '" + Enums.DataTypeGroup.ETL_PERFORMANCES.ToString() + "'",
                                                                                                loadNavigationProperties: false,
                                                                                                transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                                autoTransaction: false);
            List<OpDataTypeInGroup> dtigList = dataProvider.GetDataTypeInGroup(dtgList.Select(d => d.IdDataTypeGroup).ToArray());
            foreach (OpDataTypeInGroup dtigItem in dtigList)
            {
                if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES.ToString()))
                {
                    DataTemporalPerformances.Add(dtigItem.DataType);
                }
                else if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES.ToString()))
                {
                    DistributorPerformances.Add(dtigItem.DataType);
                }
                else if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES.ToString()))
                {
                    ImrServerPerformances.Add(dtigItem.DataType);
                }
                else if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.REPORT_PERFORMANCES.ToString()))
                {
                    ReportPerformances.Add(dtigItem.DataType);
                }
                else if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.QUEUE_PERFORMANCES.ToString()))
                {
                    QueuePerformances.Add(dtigItem.DataType);
                }
                else if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.PROCESS_PERFORMANCES.ToString()))
                {
                    ProcessPerformances.Add(dtigItem.DataType);
                }
                else if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES.ToString()))
                {
                    TransmissionDriverPerformances.Add(dtigItem.DataType);
                }
                else if (String.Equals(dtigItem.DataTypeGroup.Name, Enums.DataTypeGroup.ETL_PERFORMANCES.ToString()))
                {
                    EtlPerformances.Add(dtigItem.DataType);
                }
            }
            DataTemporalDevicePerformances = DataTemporalPerformances.Where(d => d.IdDataType.In(new long[]{ DataType.PERFORMANCE_DEVICE_SCHEDULED_MEASUREMENT_READOUT,
                                                                                                                           DataType.PERFORMANCE_DISTRIBUTOR_DEVICE,
                                                                                                                           DataType.PERFORMANCE_DEVICE_READOUT_ENTRY_TIME,
                                                                                                                           DataType.PERFORMANCE_DEVICE_ESB_DRIVER_DATA_FLOW})).ToList();            
        }

        #endregion

        #region CreateHigherAggregationTypePeriods

        public static int CreateHigherAggregationTypePeriods(OpImrServerPerformance ispItem, DateTime ReferenceDate, Enums.AggregationType MaxAggregationType,
            PerformanceComponent.GOPeriod gopItem,
            int nextIdPeriod, Dictionary<int, PerformanceComponent.GOPeriod> periodsDict,
            bool computingAggregates)
        {
            return CreateHigherAggregationTypePeriods(ispItem.StartTime, ispItem.EndTime.Value, MaxAggregationType, ReferenceDate, gopItem, nextIdPeriod, periodsDict, computingAggregates);
        }

        public static int CreateHigherAggregationTypePeriods(OpDistributorPerformance dpItem, DateTime ReferenceDate, Enums.AggregationType MaxAggregationType,
            PerformanceComponent.GOPeriod gopItem,
            int nextIdPeriod, Dictionary<int, PerformanceComponent.GOPeriod> periodsDict,
            bool computingAggregates)
        {
            return CreateHigherAggregationTypePeriods(dpItem.StartTime, dpItem.EndTime.Value, MaxAggregationType, ReferenceDate, gopItem, nextIdPeriod, periodsDict, computingAggregates);
        }

        public static int CreateHigherAggregationTypePeriods(OpReportPerformance dpItem, DateTime ReferenceDate, Enums.AggregationType MaxAggregationType,
            PerformanceComponent.GOPeriod gopItem,
            int nextIdPeriod, Dictionary<int, PerformanceComponent.GOPeriod> periodsDict,
            bool computingAggregates)
        {
            return CreateHigherAggregationTypePeriods(dpItem.StartTime, dpItem.EndTime, MaxAggregationType, ReferenceDate, gopItem, nextIdPeriod, periodsDict, computingAggregates);
        }

        public static int CreateHigherAggregationTypePeriods(DB_DATA_TEMPORAL dtItem, DateTime ReferenceDate, Enums.AggregationType MaxAggregationType,
            PerformanceComponent.GOPeriod gopItem,
            int nextIdPeriod, Dictionary<int, PerformanceComponent.GOPeriod> periodsDict,
            bool computingAggregates)
        {
            return CreateHigherAggregationTypePeriods(dtItem.START_TIME.Value, dtItem.END_TIME.Value, MaxAggregationType, ReferenceDate, gopItem, nextIdPeriod, periodsDict, computingAggregates);
        }

        public static int CreateHigherAggregationTypePeriods(OpTransmissionDriverPerformance dpItem, DateTime ReferenceDate, Enums.AggregationType MaxAggregationType,
            PerformanceComponent.GOPeriod gopItem,
            int nextIdPeriod, Dictionary<int, PerformanceComponent.GOPeriod> periodsDict,
            bool computingAggregates)
        {
            return CreateHigherAggregationTypePeriods(dpItem.StartTime, dpItem.EndTime, MaxAggregationType, ReferenceDate, gopItem, nextIdPeriod, periodsDict, computingAggregates);
        }

        public static int CreateHigherAggregationTypePeriods(OpEtlPerformance dpItem, DateTime ReferenceDate, Enums.AggregationType MaxAggregationType,
            PerformanceComponent.GOPeriod gopItem,
            int nextIdPeriod, Dictionary<int, PerformanceComponent.GOPeriod> periodsDict,
            bool computingAggregates)
        {
            return CreateHigherAggregationTypePeriods(dpItem.StartTime, dpItem.EndTime, MaxAggregationType, ReferenceDate, gopItem, nextIdPeriod, periodsDict, computingAggregates);
        }

        public static int CreateHigherAggregationTypePeriods(DateTime StartTime, DateTime EndTime, Enums.AggregationType MaxAggregationType,
            DateTime ReferenceDate, PerformanceComponent.GOPeriod gopItem,
            int nextIdPeriod, Dictionary<int, PerformanceComponent.GOPeriod> periodsDict,
            bool computingAggregates)
        {
            StartTime = RoundToNearestInterval(StartTime, TimeSpan.FromMinutes(15));
            EndTime = RoundToNearestInterval(EndTime, TimeSpan.FromMinutes(15));

            DateTime? higherAggrTypeStart = null;
            DateTime? higherAggrTypeEnd = null;

            Enums.AggregationType higherAggrTypeTmp = GetHigherAggregationType((int)gopItem.HigherAggregationType);
            if (gopItem.HigherAggregationType == Enums.AggregationType.Hour)
            {
                higherAggrTypeStart = new DateTime(StartTime.Year, StartTime.Month, StartTime.Day, StartTime.Hour, 0, 0);
                higherAggrTypeEnd = higherAggrTypeStart.Value.AddHours(1);
            }
            else if (gopItem.HigherAggregationType == Enums.AggregationType.Day)
            {
                higherAggrTypeStart = new DateTime(StartTime.Year, StartTime.Month, StartTime.Day, 0, 0, 0);
                higherAggrTypeEnd = higherAggrTypeStart.Value.AddDays(1);
                //bool? daylightSavingTime = false;
                //if (higherAggrTypeStart.HasValue && higherAggrTypeEnd.HasValue)
                //    daylightSavingTime = CheckChangeTime(higherAggrTypeStart.Value, higherAggrTypeEnd.Value);
                //if (daylightSavingTime.Value)
                //{
                //    higherAggrTypeStart = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeStart.Value);
                //    higherAggrTypeEnd = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeEnd.Value);
                //}
            }
            else if (gopItem.HigherAggregationType == Enums.AggregationType.Week)
            {
                if (ComputeHigherAggregationThanDay)
                {
                    higherAggrTypeStart = new DateTime(EndTime.Year, EndTime.Month, EndTime.Day, 0, 0, 0).AddDays(-6);
                    higherAggrTypeEnd = new DateTime(EndTime.Year, EndTime.Month, EndTime.Day, 0, 0, 0).AddDays(1);
                    //bool? daylightSavingTime = false;
                    //if (higherAggrTypeStart.HasValue && higherAggrTypeEnd.HasValue)
                    //    daylightSavingTime = CheckChangeTime(higherAggrTypeStart.Value, higherAggrTypeEnd.Value);
                    //if (daylightSavingTime.Value)
                    //{
                    //    higherAggrTypeStart = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeStart.Value);
                    //    higherAggrTypeEnd = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeEnd.Value);
                    //}
                }
            }
            else if (gopItem.HigherAggregationType == Enums.AggregationType.Month)
            {
                if (ComputeHigherAggregationThanDay)
                {
                    higherAggrTypeStart = new DateTime(EndTime.Year, EndTime.Month, EndTime.Day, 0, 0, 0).AddDays(1).AddMonths(-1);
                    higherAggrTypeEnd = new DateTime(EndTime.Year, EndTime.Month, EndTime.Day, 0, 0, 0).AddDays(1);
                    //bool? daylightSavingTime = false;
                    //if (higherAggrTypeStart.HasValue && higherAggrTypeEnd.HasValue)
                    //    daylightSavingTime = CheckChangeTime(higherAggrTypeStart.Value, higherAggrTypeEnd.Value);
                    //if (daylightSavingTime.Value)
                    //{
                    //    higherAggrTypeStart = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeStart.Value);
                    //    higherAggrTypeEnd = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeEnd.Value);
                    //}
                }
            }
            else if (gopItem.HigherAggregationType == Enums.AggregationType.Year)
            {
                if (ComputeHigherAggregationThanDay)
                {
                    higherAggrTypeStart = new DateTime(EndTime.Year, EndTime.Month, EndTime.Day, 0, 0, 0).AddDays(1).AddYears(-1);
                    higherAggrTypeEnd = new DateTime(EndTime.Year, EndTime.Month, EndTime.Day, 0, 0, 0).AddDays(1);
                    //bool? daylightSavingTime = false;
                    //if (higherAggrTypeStart.HasValue && higherAggrTypeEnd.HasValue)
                    //    daylightSavingTime = CheckChangeTime(higherAggrTypeStart.Value, higherAggrTypeEnd.Value);
                    //if (daylightSavingTime.Value)
                    //{
                    //    higherAggrTypeStart = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeStart.Value);
                    //    higherAggrTypeEnd = ImproveDateTimeDependingOnDaylightSavingTime(higherAggrTypeEnd.Value);
                    //}
                }
            }
            PerformanceComponent.GOPeriod higherPeriod = periodsDict.Values.ToList().Find(p => p.AggregationType == gopItem.HigherAggregationType
                                                   && p.StartTime == higherAggrTypeStart
                                                   && p.EndTime == higherAggrTypeEnd);

            if (higherPeriod == null)
            {
                if (higherAggrTypeEnd <= ReferenceDate && higherAggrTypeEnd != null)
                {
                    DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                    if ((computingAggregates == true /*false*/ || higherAggrTypeStart >= lastFullDay) && AggregationTypeToAnalyze.Exists(d => d.Equals((int)gopItem.HigherAggregationType))) // allIdAggregationTypeToGet - sprawdzamy czy chcemy liczyc agregaty w obrębie tworzonego perioda.
                    {
                        nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                        higherPeriod = new PerformanceComponent.GOPeriod(nextIdPeriod, higherAggrTypeStart.Value, higherAggrTypeEnd.Value,
                                                    gopItem.HigherAggregationType, higherAggrTypeTmp, GetLowerAggregationType((int)gopItem.HigherAggregationType));
                        periodsDict.Add(nextIdPeriod, higherPeriod);
                    }
                }
            }
            // Jeżeli w hierarchi mamy ustawiony wyższy niż dzienny MaxAggregationType to za jednym razem tworzymy kolejne wyższe periody
            if (higherPeriod != null && MaxAggregationType > higherPeriod.AggregationType)
                nextIdPeriod = CreateHigherAggregationTypePeriods(StartTime, EndTime, MaxAggregationType, ReferenceDate, higherPeriod, nextIdPeriod, periodsDict, computingAggregates);
            return nextIdPeriod;
        }

        #endregion

        #region ClassifyDataPerPeriod

        #region ClassifyDataTemporalPerPeriod

        public static List<long> HierarchyItemMissingForLocalization = new List<long>();
        public static List<long> HierarchyItemMissingForDevice = new List<long>();
        public static List<long> HierarchyItemMissingForMeter = new List<long>();

        public static void ClassifyDataTemporalPerPeriod(DataProvider dataProvider,
                                                 DateTime ReferenceDate,
                                                 List<DB_DATA_TEMPORAL> dtList,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<long, OpLocation> locationDict,
                                                 Dictionary<long, OpDevice> deviceDict,
                                                 Dictionary<long, OpMeter> meterDict,
                                                 Dictionary<int, List<DB_DATA_TEMPORAL>> dataTemporalPeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;

                List<OpDistributor> distributorToAnalyzeList = new List<OpDistributor>();
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);

                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForLocalization = new List<long>();
                HierarchyItemMissingForDevice = new List<long>();
                HierarchyItemMissingForMeter = new List<long>();

                foreach (GOImrServer goisItem in serverWorkspace)
                    distributorToAnalyzeList.AddRange(goisItem.DistributorList);
                distributorToAnalyzeList = distributorToAnalyzeList.Distinct().ToList();

                foreach (DB_DATA_TEMPORAL dtItem in dtList)
                {
                    #region Validate

                    if (dtItem.SERIAL_NBR.HasValue && !deviceDict.ContainsKey(dtItem.SERIAL_NBR.Value))
                        continue;

                    #endregion

                    OpDistributor currentDistributor = null;
                    OpImrServer currentImrServer = null;

                    DateTime endTimeConvertedToDay = new DateTime(dtItem.END_TIME.Value.Year, dtItem.END_TIME.Value.Month, dtItem.END_TIME.Value.Day, 0, 0, 0);

                    isLatestPeriod = false;

                    #region CurrentImrServerAndDistributor

                    if (dtItem.ID_LOCATION.HasValue && !dtItem.ID_METER.HasValue)
                    {
                        if (!locationDict.ContainsKey(dtItem.ID_LOCATION.Value))
                            continue;//logujący się operator nie ma uprawnień do danej lokalziacji
                        OpLocation lItem = locationDict[dtItem.ID_LOCATION.Value];
                        currentDistributor = distributorToAnalyzeList.Find(d => d.IdDistributor == lItem.IdDistributor);
                    }

                    if (dtItem.SERIAL_NBR.HasValue && !dtItem.ID_METER.HasValue)
                    {
                        if (!deviceDict.ContainsKey(dtItem.SERIAL_NBR.Value))
                            continue;//logujący się operator nie ma uprawnień do danego urządzenia
                        OpDevice dItem = deviceDict[dtItem.SERIAL_NBR.Value];
                        currentDistributor = distributorToAnalyzeList.Find(d => d.IdDistributor == dItem.IdDistributor);
                    }

                    if (dtItem.ID_METER.HasValue)
                    {
                        if (!meterDict.ContainsKey(dtItem.ID_METER.Value))
                            continue;//logujący się operator nie ma uprawnień do danego metera
                        OpMeter mItem = meterDict[dtItem.ID_METER.Value];
                        currentDistributor = distributorToAnalyzeList.Find(d => d.IdDistributor == mItem.IdDistributor);
                    }

                    currentImrServer = currentDistributor.ImrServer;
                    if (!serverWorkspace.Exists(d => d.DistributorList.Exists(distr => distr.IdDistributor == currentDistributor.IdDistributor)))
                        continue;

                    #endregion

                    //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                    PerformanceComponent.GOPeriod gopItem = null;
                    PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                    DateTime stardDateTmp = dtItem.START_TIME.Value;
                    DateTime endDateTmp = dtItem.END_TIME.Value;

                    int? idAggregationType = dtItem.ID_AGGREGATION_TYPE;


                    Enums.AggregationType lowerAggregationType = GetLowerAggregationType(idAggregationType.HasValue ? idAggregationType.Value : (int)Enums.AggregationType.Quarter);
                    Enums.AggregationType higherAggregationType = GetHigherAggregationType(idAggregationType.HasValue ? idAggregationType.Value : (int)Enums.AggregationType.Year);

                    #region PeriodsAndAggregationType

                    if (dtItem.ID_AGGREGATION_TYPE == (int)Enums.AggregationType.Quarter)
                    {
                        stardDateTmp = RoundToNearestInterval(dtItem.START_TIME.Value, TimeSpan.FromMinutes(15));
                        endDateTmp = RoundToNearestInterval(dtItem.END_TIME.Value, TimeSpan.FromMinutes(15));

                        /*foreach (Enums.AggregationType atItem in Enum.GetValues(typeof(Enums.AggregationType)))
                        {
                            if ((int)atItem > (int)Enums.AggregationType.Quarter && godtPerformance.AllowedAggregatioType.Exists(a => a == atItem))
                            {
                                higherAggregationType = atItem;
                                break;
                            }
                        }*/
                    }

                    if (dtItem.ID_AGGREGATION_TYPE == (int)Enums.AggregationType.Day && !daylightSavingTime)
                    {
                        stardDateTmp = RoundToNearestInterval(dtItem.START_TIME.Value, TimeSpan.FromDays(1));
                        endDateTmp = RoundToNearestInterval(dtItem.END_TIME.Value, TimeSpan.FromDays(1));
                    }

                    #endregion

                    if (endDateTmp > ReferenceDate)
                        //nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                        continue;
                    //if (computingAggregates)
                    //{                    
                    //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                    //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                    //        continue;
                    //}

                    Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.PeriodsDict[currentDistributor.IdDistributor];
                    if (periodsDict == null)
                    {
                        periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                        gosiItem.PeriodsDict[currentDistributor.IdDistributor] = periodsDict;
                    }

                    PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.PerformancesHierarchy[currentDistributor.IdDistributor].Values
                                                                                                            .ToList()
                                                                                                            .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == dtItem.ID_DATA_TYPE);

                    if (gophItem == null)
                    {
                        if (dtItem.ID_LOCATION.HasValue && !dtItem.ID_METER.HasValue && locationDict.ContainsKey(dtItem.ID_LOCATION.Value))
                            HierarchyItemMissingForLocalization.Add(Convert.ToInt64(dtItem.ID_LOCATION.Value));
                        else if (dtItem.SERIAL_NBR.HasValue && !dtItem.ID_METER.HasValue && deviceDict.ContainsKey(dtItem.SERIAL_NBR.Value))
                            HierarchyItemMissingForDevice.Add(Convert.ToInt64(dtItem.SERIAL_NBR.Value));
                        else if (dtItem.ID_METER.HasValue && meterDict.ContainsKey(dtItem.ID_METER.Value))
                            HierarchyItemMissingForMeter.Add(Convert.ToInt64(dtItem.ID_METER.Value));

                        continue;
                    }

                    if (dtItem.ID_LOCATION.HasValue && !dtItem.ID_METER.HasValue && locationDict.ContainsKey(dtItem.ID_LOCATION.Value))
                    {
                        #region Location

                        gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                     && p.EndTime == endDateTmp);
                        if (gopItem == null)
                        {
                            nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                            gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                    (Enums.AggregationType)dtItem.ID_AGGREGATION_TYPE, higherAggregationType, lowerAggregationType);
                            periodsDict.Add(nextIdPeriod, gopItem);
                        }

                        //sprawdzenie wyższych aggregation type
                        if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                            isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                        if (gophItem != null && (int)gophItem.MaxAggregationType > dtItem.ID_AGGREGATION_TYPE && computingAggregates && isLatestPeriod)
                        {
                            nextIdPeriod = CreateHigherAggregationTypePeriods(dtItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                        }

                        if (!gopItem.LocationPerformanceDict.ContainsKey(dtItem.ID_LOCATION.Value))
                        {
                            gopItem.LocationPerformanceDict.Add(dtItem.ID_LOCATION.Value, new List<GOPerformanceValue>());
                        }

                        if (!gopItem.LocationPerformanceDict[dtItem.ID_LOCATION.Value].Exists(d => d.IdDataType == dtItem.ID_DATA_TYPE))
                            gopItem.LocationPerformanceDict[dtItem.ID_LOCATION.Value].Add(new GOPerformanceValue(dtItem.ID_DATA_TYPE, 0.0, dtItem.INDEX_NBR));
                        /*
                        foreach (OpDataType dataTypeItem in gosiItem.LocationAggregatePerformances[currentDistributor.IdDistributor])
                        {
                            if(!gopItem.LocationPerformanceDict[dtItem.IdLocation.Value].Exists(d => d.IdDataType == dataTypeItem.IdDataType))
                                gopItem.LocationPerformanceDict[dtItem.IdLocation.Value].Add(new GOPerformanceValue(dataTypeItem.IdDataType, 0.0));
                        }*/

                        if (!dataTemporalPeriodsDict.ContainsKey(gopItem.IdPeriod))
                            dataTemporalPeriodsDict.Add(gopItem.IdPeriod, new List<DB_DATA_TEMPORAL>());
                        dataTemporalPeriodsDict[gopItem.IdPeriod].Add(dtItem);

                        #endregion
                    }
                    if (dtItem.SERIAL_NBR.HasValue && !dtItem.ID_METER.HasValue && deviceDict.ContainsKey(dtItem.SERIAL_NBR.Value))
                    {
                        #region Device

                        gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                    && p.EndTime == endDateTmp);
                        if (gopItem == null)
                        {
                            nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                            gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                    (Enums.AggregationType)dtItem.ID_AGGREGATION_TYPE, higherAggregationType, lowerAggregationType);
                            periodsDict.Add(nextIdPeriod, gopItem);
                        }

                        //sprawdzenie wyższych aggregation type
                        if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                            isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                        if (gophItem != null && (int)gophItem.MaxAggregationType > dtItem.ID_AGGREGATION_TYPE && computingAggregates && isLatestPeriod)
                        {
                            nextIdPeriod = CreateHigherAggregationTypePeriods(dtItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                        }

                        if (!gopItem.DevicePerformanceDict.ContainsKey(dtItem.SERIAL_NBR.Value))
                        {
                            gopItem.DevicePerformanceDict.Add(dtItem.SERIAL_NBR.Value, new List<GOPerformanceValue>());
                        }

                        if (!gopItem.DevicePerformanceDict[dtItem.SERIAL_NBR.Value].Exists(d => d.IdDataType == dtItem.ID_DATA_TYPE))
                            gopItem.DevicePerformanceDict[dtItem.SERIAL_NBR.Value].Add(new GOPerformanceValue(dtItem.ID_DATA_TYPE, 0.0, dtItem.INDEX_NBR));

                        if (!dataTemporalPeriodsDict.ContainsKey(gopItem.IdPeriod))
                            dataTemporalPeriodsDict.Add(gopItem.IdPeriod, new List<DB_DATA_TEMPORAL>());
                        dataTemporalPeriodsDict[gopItem.IdPeriod].Add(dtItem);

                        #endregion
                    }
                    if (dtItem.ID_METER.HasValue && meterDict.ContainsKey(dtItem.ID_METER.Value))
                    {
                        #region Meter

                        gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                    && p.EndTime == endDateTmp);
                        if (gopItem == null)
                        {
                            nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                            gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                    (Enums.AggregationType)dtItem.ID_AGGREGATION_TYPE, higherAggregationType, lowerAggregationType);
                            periodsDict.Add(nextIdPeriod, gopItem);
                        }

                        //sprawdzenie wyższych aggregation type
                        if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                            isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                        if (gophItem != null && (int)gophItem.MaxAggregationType > dtItem.ID_AGGREGATION_TYPE && computingAggregates && isLatestPeriod)
                        {
                            nextIdPeriod = CreateHigherAggregationTypePeriods(dtItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                        }

                        if (!gopItem.MeterPerformanceDict.ContainsKey(dtItem.ID_METER.Value))
                        {
                            gopItem.MeterPerformanceDict.Add(dtItem.ID_METER.Value, new List<GOPerformanceValue>());
                        }

                        if (!gopItem.MeterPerformanceDict[dtItem.ID_METER.Value].Exists(d => d.IdDataType == dtItem.ID_DATA_TYPE))
                            gopItem.MeterPerformanceDict[dtItem.ID_METER.Value].Add(new GOPerformanceValue(dtItem.ID_DATA_TYPE, 0.0, dtItem.INDEX_NBR));

                        if (!dataTemporalPeriodsDict.ContainsKey(gopItem.IdPeriod))
                            dataTemporalPeriodsDict.Add(gopItem.IdPeriod, new List<DB_DATA_TEMPORAL>());
                        dataTemporalPeriodsDict[gopItem.IdPeriod].Add(dtItem);

                        #endregion
                    }

                    if (!gopItem.DistributorPerformanceDict.ContainsKey(currentDistributor.IdDistributor))
                        gopItem.DistributorPerformanceDict.Add(currentDistributor.IdDistributor, new List<OpDistributorPerformance>());
                    if (!gopItem.ImrServerPerformanceDict.ContainsKey(currentImrServer.IdImrServer))
                        gopItem.ImrServerPerformanceDict.Add(currentImrServer.IdImrServer, new List<OpImrServerPerformance>());

                    cnt++;
                }

                HierarchyItemMissingForLocalization = HierarchyItemMissingForLocalization.Distinct().ToList();
                HierarchyItemMissingForDevice = HierarchyItemMissingForDevice.Distinct().ToList();
                HierarchyItemMissingForMeter = HierarchyItemMissingForMeter.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {
                    foreach (OpDistributor dItem in goisItem.DistributorList)
                    {
                        if (dItem.DataList.Exists(e => e.IdDataType == DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR && Convert.ToBoolean(e.Value) == true))
                            continue;
                        if (AggregationTypeToAnalyze.Count == 1 && AggregationTypeToAnalyze.Exists(d => d.Equals((int)Enums.AggregationType.Quarter) && !goisItem.PeriodsDict[dItem.IdDistributor].Values.ToList().ToList().Exists(e => e.StartTime == ReferenceDate.AddMinutes(-30) && e.EndTime == ReferenceDate.AddMinutes(-15))))
                        {
                            // Wczesniej aktualnie analizowany period na podstawie ktorego liczylismy agregat dystrybutora, był tworzony na podstawie pobranych data temporali, teraz dla 
                            // urzadzen nie mamy ich na bierzaco co 15min, dlatego musimy go recznie wygenerowac
                            nextIdPeriod = goisItem.PeriodsDict[dItem.IdDistributor].Values.ToList().Count == 0 ? 0 : goisItem.PeriodsDict[dItem.IdDistributor].Values.ToList().Max(m => m.IdPeriod) + 1;
                            PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, ReferenceDate.AddMinutes(-30), ReferenceDate.AddMinutes(-15),
                                                    Enums.AggregationType.Quarter, Enums.AggregationType.Day, Enums.AggregationType.Unknown);
                            goisItem.PeriodsDict[dItem.IdDistributor].Add(nextIdPeriod, gopItem);
                            goisItem.PeriodsDict[dItem.IdDistributor][nextIdPeriod].DistributorPerformanceDict.Add(dItem.IdDistributor, new OpDistributorPerformance());
                        }
                    }
                    foreach (int idDistrKey in goisItem.PeriodsDict.Keys)
                    {
                        Dictionary<int, GOPeriod> dictTmp = goisItem.PeriodsDict[idDistrKey];
                        //foreach (Dictionary<int, PerformanceComponent.GOPeriod> kvItem in goisItem.PeriodsDict.Values)
                        //{
                        List<PerformanceComponent.GOPeriod> periods = dictTmp.Values.ToList();//kvItem.Values.ToList();
                        List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                        #region CreatePeriodHierarchy
                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                            {
                                if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                    continue;

                                List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                                && p.StartTime >= periods[i].StartTime
                                                                                && p.EndTime <= periods[i].EndTime).ToList();

                                #region AddingMissingPeriods

                                if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                                {
                                    lowerPeriodsTmp = lowerPeriods.ToList();
                                    AddingMissingDataTemporalPeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer, idDistrKey); // Dodawanie brakujacych okresow do dictTmp,
                                }

                                #endregion

                                dictTmp = goisItem.PeriodsDict[idDistrKey];
                                periods = dictTmp.Values.ToList();

                                lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                                && p.StartTime >= periods[i].StartTime
                                                                                && p.EndTime <= periods[i].EndTime).ToList();

                                if (lowerPeriods != null)
                                    periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));
                            }
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                                periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();
                        }
                        #endregion

                        #region FillDictDataHigherAggregates
                        for (int i = 0; i < periods.Count; i++)
                        {
                            if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                            {
                                if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                                {
                                    for (int j = 0; j < periods[i].LowerIdPeriods.Count; j++)
                                    {
                                        int idLowerPeriod = periods[i].LowerIdPeriods[j];
                                        PerformanceComponent.GOPeriod period = dictTmp[idLowerPeriod];
                                        foreach (long lItem in period.LocationPerformanceDict.Keys)
                                        {
                                            //if (!periods[i].LocationPerformanceDict.ContainsKey(lItem))
                                            //{
                                            if (!periods[i].LocationPerformanceDict.ContainsKey(lItem))
                                                periods[i].LocationPerformanceDict.Add(lItem, new List<GOPerformanceValue>());
                                            foreach (int iDistrItem in goisItem.LocationAggregatePerformances.Keys)
                                            {
                                                foreach (OpDataType dataTypeItem in goisItem.LocationAggregatePerformances[iDistrItem])
                                                {
                                                    if (!periods[i].LocationPerformanceDict[lItem].Exists(d => d.IdDataType == dataTypeItem.IdDataType))
                                                        periods[i].LocationPerformanceDict[lItem].Add(new GOPerformanceValue(dataTypeItem.IdDataType, 0.0, null));
                                                }
                                                // }
                                            }
                                        }
                                        foreach (long lItem in period.DevicePerformanceDict.Keys)
                                        {
                                            if (!periods[i].DevicePerformanceDict.ContainsKey(lItem))
                                                periods[i].DevicePerformanceDict.Add(lItem, new List<GOPerformanceValue>());
                                            foreach (int iDistrItem in goisItem.DeviceAggregatePerformances.Keys)
                                            {
                                                foreach (OpDataType dataTypeItem in goisItem.DeviceAggregatePerformances[iDistrItem])
                                                {
                                                    if (!periods[i].DevicePerformanceDict[lItem].Exists(d => d.IdDataType == dataTypeItem.IdDataType))
                                                        periods[i].DevicePerformanceDict[lItem].Add(new GOPerformanceValue(dataTypeItem.IdDataType, 0.0, null));
                                                }
                                            }
                                        }
                                        foreach (long lItem in period.MeterPerformanceDict.Keys)
                                        {
                                            if (!periods[i].MeterPerformanceDict.ContainsKey(lItem))
                                                periods[i].MeterPerformanceDict.Add(lItem, new List<GOPerformanceValue>());
                                            foreach (int iDistrItem in goisItem.MeterAggregatePerformances.Keys)
                                            {
                                                foreach (OpDataType dataTypeItem in goisItem.MeterAggregatePerformances[iDistrItem])
                                                {
                                                    if (!periods[i].MeterPerformanceDict[lItem].Exists(d => d.IdDataType == dataTypeItem.IdDataType))
                                                        periods[i].MeterPerformanceDict[lItem].Add(new GOPerformanceValue(dataTypeItem.IdDataType, 0.0, null));
                                                }
                                            }
                                        }
                                        foreach (int iItem in period.DistributorPerformanceDict.Keys)
                                        {
                                            if (!periods[i].DistributorPerformanceDict.ContainsKey(iItem))
                                                periods[i].DistributorPerformanceDict.Add(iItem, new List<OpDistributorPerformance>());
                                        }
                                        foreach (int iItem in period.ImrServerPerformanceDict.Keys)
                                        {
                                            if (!periods[i].ImrServerPerformanceDict.ContainsKey(iItem))
                                                periods[i].ImrServerPerformanceDict.Add(iItem, new List<OpImrServerPerformance>());
                                        }
                                        foreach (int iItem in period.ReportPerformanceDict.Keys)
                                        {
                                            if (!periods[i].ReportPerformanceDict.ContainsKey(iItem))
                                                periods[i].ReportPerformanceDict.Add(iItem, new List<OpReportPerformance>());
                                        }
                                    }
                                }
                                if (!dataTemporalPeriodsDict.ContainsKey(periods[i].IdPeriod))
                                    dataTemporalPeriodsDict.Add(periods[i].IdPeriod, new List<DB_DATA_TEMPORAL>());
                            }
                        }
                        #endregion

                        if (withValues)
                        {
                            #region FillAggregateValues

                            for (int i = 0; i < periods.Count; i++)
                            {
                                if (!dataTemporalPeriodsDict.ContainsKey(periods[i].IdPeriod))
                                    continue;
                                foreach (DB_DATA_TEMPORAL dtItem in dataTemporalPeriodsDict[periods[i].IdPeriod])
                                {
                                    if (dtItem.ID_LOCATION.HasValue && !dtItem.ID_METER.HasValue)
                                    {
                                        #region Location
                                        if (dtItem != null && dtItem.VALUE != null)
                                        {
                                            OpLocation lItemTmp = locationDict[dtItem.ID_LOCATION.Value];
                                            if (idDistrKey == lItemTmp.IdDistributor)
                                            {
                                                GOPerformanceValue gopvItem = periods[i].LocationPerformanceDict[dtItem.ID_LOCATION.Value].Find(p => p.IdDataType == dtItem.ID_DATA_TYPE);
                                                gopvItem.DataStatus = (Enums.DataStatus)dtItem.STATUS;
                                                if(dtItem.ID_DATA_SOURCE_TYPE.HasValue)
                                                    gopvItem.DataSourceType = (Enums.DataSourceType)dtItem.ID_DATA_SOURCE_TYPE;
                                                gopvItem.Value = ComputePerformanceValue(dtItem, dataProvider.GetDataType(dtItem.ID_DATA_TYPE)/*, (Enums.AggregationType)dtItem.IdAggregationType*/);
                                            }
                                        }
                                        #endregion
                                    }
                                    if (dtItem.SERIAL_NBR.HasValue && !dtItem.ID_METER.HasValue)
                                    {
                                        #region Device
                                        if (dtItem != null && dtItem.VALUE != null)
                                        {
                                            OpDevice dItemTmp = deviceDict[dtItem.SERIAL_NBR.Value];
                                            if (idDistrKey == dItemTmp.IdDistributor)
                                            {
                                                GOPerformanceValue gopvItem = periods[i].DevicePerformanceDict[dtItem.SERIAL_NBR.Value].Find(p => p.IdDataType == dtItem.ID_DATA_TYPE);
                                                gopvItem.DataStatus = (Enums.DataStatus)dtItem.STATUS;
                                                if (dtItem.ID_DATA_SOURCE_TYPE.HasValue)
                                                    gopvItem.DataSourceType = (Enums.DataSourceType)dtItem.ID_DATA_SOURCE_TYPE;
                                                if (dtItem.STATUS != (int)Enums.DataStatus.Normal)
                                                {
                                                }
                                                gopvItem.Value = ComputePerformanceValue(dtItem, dataProvider.GetDataType(dtItem.ID_DATA_TYPE));
                                            }
                                        }
                                        #endregion
                                    }
                                    if (dtItem.ID_METER.HasValue)
                                    {
                                        #region Meter
                                        if (dtItem != null && dtItem.VALUE != null)
                                        {
                                            OpMeter mItemTmp = meterDict[dtItem.ID_METER.Value];
                                            if (idDistrKey == mItemTmp.IdDistributor)
                                            {
                                                GOPerformanceValue gopvItem = periods[i].MeterPerformanceDict[dtItem.ID_METER.Value].Find(p => p.IdDataType == dtItem.ID_DATA_TYPE);
                                                gopvItem.DataStatus = (Enums.DataStatus)dtItem.STATUS;
                                                if (dtItem.ID_DATA_SOURCE_TYPE.HasValue)
                                                    gopvItem.DataSourceType = (Enums.DataSourceType)dtItem.ID_DATA_SOURCE_TYPE;
                                                gopvItem.Value = ComputePerformanceValue(dtItem, dataProvider.GetDataType(dtItem.ID_DATA_TYPE));
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }

                            #endregion
                            //}
                        }
                    }
                }
            }
        }
        #endregion

        #region ClassifyDistributorPerformancePerPeriod

        public static List<long> HierarchyItemMissingForDistributor = new List<long>();

        public static void ClassifyDistributorPerformancePerPeriod(DataProvider dataProvider,
                                                 List<OpDistributorPerformance> dpList,
                                                 DateTime ReferenceDate,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<int, List<OpDistributorPerformance>> distributorPerformancePeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForDistributor = new List<long>();

                List<OpDistributor> distributorToAnalyzeList = new List<OpDistributor>();
                foreach (GOImrServer goisItem in serverWorkspace)
                    distributorToAnalyzeList.AddRange(goisItem.DistributorList);
                distributorToAnalyzeList = distributorToAnalyzeList.Distinct().ToList();

                foreach (OpDistributorPerformance dpItem in dpList)
                {
                    OpDistributor currentDistributor = null;
                    OpImrServer currentImrServer = null;
                    int nextidPeriodLatch = nextIdPeriod;
                    DateTime endTimeConvertedToDay = new DateTime(dpItem.EndTime.Value.Year, dpItem.EndTime.Value.Month, dpItem.EndTime.Value.Day, 0, 0, 0);
                    isLatestPeriod = false;

                    #region CurrentImrServerAndDistributor

                    currentDistributor = distributorToAnalyzeList.Find(d => d.IdDistributor == dpItem.IdDistributor);
                    if (currentDistributor == null)
                        continue;//uzytkownik nie ma uprawnień do dystrybutora
                    currentImrServer = currentDistributor.ImrServer;

                    #endregion

                    //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                    PerformanceComponent.GOPeriod gopItem = null;
                    DateTime stardDateTmp = dpItem.StartTime;
                    DateTime endDateTmp = dpItem.EndTime.Value;

                    Enums.AggregationType lowerAggregationType = GetLowerAggregationType(dpItem.IdAggregationType);
                    Enums.AggregationType higherAggregationType = GetHigherAggregationType(dpItem.IdAggregationType);

                    #region PeriodsAndAggregationType

                    if (dpItem.IdAggregationType == (int)Enums.AggregationType.Quarter)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(15));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(15));
                    }
                    if (dpItem.IdAggregationType == (int)Enums.AggregationType.Day && !daylightSavingTime)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromDays(1));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromDays(1));
                    }

                    #endregion

                    if (endDateTmp > ReferenceDate)
                        continue;//nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                                 //if (computingAggregates)
                                 //{                    
                                 //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                                 //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                                 //        continue;
                                 //}

                    PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                    PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.PerformancesHierarchy[currentDistributor.IdDistributor].Values
                                                                                                                  .ToList()
                                                                                                                  .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == dpItem.IdDataType);

                    if (gophItem == null)
                    {
                        HierarchyItemMissingForDistributor.Add(Convert.ToInt64(dpItem.IdDistributor));
                        continue;
                    }

                    Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.PeriodsDict[currentDistributor.IdDistributor];
                    if (periodsDict == null)
                    {
                        periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                        gosiItem.PeriodsDict[currentDistributor.IdDistributor] = periodsDict;
                    }

                    gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                     && p.EndTime == endDateTmp);
                    if (gopItem == null)
                    {
                        nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                        gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)dpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                        periodsDict.Add(nextIdPeriod, gopItem);
                    }

                    //sprawdzenie wyższych aggregation type
                    if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                        isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                    if (gophItem != null && (int)gophItem.MaxAggregationType > dpItem.IdAggregationType && computingAggregates && isLatestPeriod)
                    {
                        nextIdPeriod = CreateHigherAggregationTypePeriods(dpItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                    }


                    if (!gopItem.DistributorPerformanceDict.ContainsKey(currentDistributor.IdDistributor))
                        gopItem.DistributorPerformanceDict.Add(currentDistributor.IdDistributor, new List<OpDistributorPerformance>());
                    for (int i = nextidPeriodLatch; i < nextIdPeriod; i++)
                    {
                        if (periodsDict.ContainsKey(i) && !periodsDict[i].DistributorPerformanceDict.ContainsKey(dpItem.IdDistributor))
                        {
                            periodsDict[i].DistributorPerformanceDict.Add(currentDistributor.IdDistributor, new List<OpDistributorPerformance>());
                        }
                    }

                    if (!distributorPerformancePeriodsDict.ContainsKey(gopItem.IdPeriod))
                        distributorPerformancePeriodsDict.Add(gopItem.IdPeriod, new List<OpDistributorPerformance>());
                    distributorPerformancePeriodsDict[gopItem.IdPeriod].Add(dpItem);

                    cnt++;
                }

                HierarchyItemMissingForDistributor = HierarchyItemMissingForDistributor.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {
                    foreach (int idDistrKey in goisItem.PeriodsDict.Keys)
                    {
                        Dictionary<int, GOPeriod> dictTmp = goisItem.PeriodsDict[idDistrKey];
                        List<PerformanceComponent.GOPeriod> periods = dictTmp.Values.ToList();
                        List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                        #region CreatePeriodHierarchy
                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                            {
                                if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                    continue;

                                List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                                && p.StartTime >= periods[i].StartTime
                                                                                && p.EndTime <= periods[i].EndTime).ToList();

                                #region AddingMissingPeriods

                                if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                                {
                                    lowerPeriodsTmp = lowerPeriods.ToList();
                                    AddingMissingDistributorPeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer, idDistrKey); // Dodawanie brakujacych okresow do PeriodsDict[idDistributor],
                                }

                                #endregion

                                periods = goisItem.PeriodsDict[idDistrKey].Values.ToList();
                                lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                                && p.StartTime >= periods[i].StartTime
                                                                                && p.EndTime <= periods[i].EndTime).ToList();

                                if (lowerPeriods != null)
                                    periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));
                            }
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                                periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();
                        }
                        #endregion

                        #region FillDictDataHigherAggregates
                        for (int i = 0; i < periods.Count; i++)
                        {
                            if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                            {
                                if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                                {
                                    for (int j = 0; j < periods[i].LowerIdPeriods.Count; j++)
                                    {
                                        int idLowerPeriod = periods[i].LowerIdPeriods[j];
                                        PerformanceComponent.GOPeriod period = dictTmp[idLowerPeriod];
                                        foreach (long lItem in period.LocationPerformanceDict.Keys)
                                        {
                                            if (!periods[i].LocationPerformanceDict.ContainsKey(lItem))
                                                periods[i].LocationPerformanceDict.Add(lItem, new List<GOPerformanceValue>());
                                            foreach (int iDistrItem in goisItem.LocationAggregatePerformances.Keys)
                                            {
                                                foreach (OpDataType dataTypeItem in goisItem.LocationAggregatePerformances[iDistrItem])
                                                {
                                                    if (!periods[i].LocationPerformanceDict[lItem].Exists(d => d.IdDataType == dataTypeItem.IdDataType))
                                                        periods[i].LocationPerformanceDict[lItem].Add(new GOPerformanceValue(dataTypeItem.IdDataType, 0.0, null));
                                                }
                                            }
                                        }
                                        foreach (long lItem in period.DevicePerformanceDict.Keys)
                                        {
                                            if (!periods[i].DevicePerformanceDict.ContainsKey(lItem))
                                                periods[i].DevicePerformanceDict.Add(lItem, new List<GOPerformanceValue>());
                                            foreach (int iDistrItem in goisItem.DeviceAggregatePerformances.Keys)
                                            {
                                                foreach (OpDataType dataTypeItem in goisItem.DeviceAggregatePerformances[iDistrItem])
                                                {
                                                    if (!periods[i].DevicePerformanceDict[lItem].Exists(d => d.IdDataType == dataTypeItem.IdDataType))
                                                        periods[i].DevicePerformanceDict[lItem].Add(new GOPerformanceValue(dataTypeItem.IdDataType, 0.0, null));
                                                }
                                            }
                                        }
                                        foreach (long lItem in period.MeterPerformanceDict.Keys)
                                        {
                                            if (!periods[i].MeterPerformanceDict.ContainsKey(lItem))
                                                periods[i].MeterPerformanceDict.Add(lItem, new List<GOPerformanceValue>());
                                            foreach (int iDistrItem in goisItem.MeterAggregatePerformances.Keys)
                                            {
                                                foreach (OpDataType dataTypeItem in goisItem.MeterAggregatePerformances[iDistrItem])
                                                {
                                                    if (!periods[i].MeterPerformanceDict[lItem].Exists(d => d.IdDataType == dataTypeItem.IdDataType))
                                                        periods[i].MeterPerformanceDict[lItem].Add(new GOPerformanceValue(dataTypeItem.IdDataType, 0.0, null));
                                                }
                                            }
                                        }
                                        foreach (int iItem in period.DistributorPerformanceDict.Keys)
                                        {
                                            if (!periods[i].DistributorPerformanceDict.ContainsKey(iItem))
                                                periods[i].DistributorPerformanceDict.Add(iItem, new List<OpDistributorPerformance>());
                                        }
                                        foreach (int iItem in period.ImrServerPerformanceDict.Keys)
                                        {
                                            if (!periods[i].ImrServerPerformanceDict.ContainsKey(iItem))
                                                periods[i].ImrServerPerformanceDict.Add(iItem, new List<OpImrServerPerformance>());
                                        }
                                        foreach (int iItem in period.ReportPerformanceDict.Keys)
                                        {
                                            if (!periods[i].ReportPerformanceDict.ContainsKey(iItem))
                                                periods[i].ReportPerformanceDict.Add(iItem, new List<OpReportPerformance>());
                                        }
                                    }
                                }
                                if (!distributorPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                                    distributorPerformancePeriodsDict.Add(periods[i].IdPeriod, new List<OpDistributorPerformance>());
                            }
                        }
                        #endregion

                        if (withValues)
                        {
                            #region FillAggregateValues

                            for (int i = 0; i < periods.Count; i++)
                            {
                                if (distributorPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                                {
                                    foreach (OpDistributorPerformance dpItem in distributorPerformancePeriodsDict[periods[i].IdPeriod].Where(d => d.IdDistributor == idDistrKey))
                                    {
                                        periods[i].DistributorPerformanceDict[dpItem.IdDistributor].Add(dpItem);
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
        }
        #endregion

        #region ClassifyImrServerPerformancePerPeriod

        public static List<long> HierarchyItemMissingForImrServer = new List<long>();

        public static void ClassifyImrServerPerformancePerPeriod(DataProvider dataProvider,
                                                 List<OpImrServerPerformance> ispList,
                                                 DateTime ReferenceDate,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<int, List<OpImrServerPerformance>> imrServerPerformancePeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForImrServer = new List<long>();

                foreach (OpImrServerPerformance dpItem in ispList)
                {
                    /*
                     * Do wyliczenia agregatów serwerowych bierzemy pod uwagę tylko te z indexem = 0 lub -1 
                     * 
                     * */

                    if (dpItem.IndexNbr > 0)
                        continue;

                    OpImrServer currentImrServer = null;
                    DateTime endTimeConvertedToDay = new DateTime(dpItem.EndTime.Value.Year, dpItem.EndTime.Value.Month, dpItem.EndTime.Value.Day, 0, 0, 0);
                    isLatestPeriod = false;

                    #region CurrentImrServer

                    if (!serverWorkspace.Exists(s => s.Server.IdImrServer == dpItem.IdImrServer))
                        continue;

                    currentImrServer = serverWorkspace.Find(s => s.Server.IdImrServer == dpItem.IdImrServer).Server;

                    #endregion

                    //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                    PerformanceComponent.GOPeriod gopItem = null;
                    DateTime stardDateTmp = dpItem.StartTime;
                    DateTime endDateTmp = dpItem.EndTime.Value;
                    Enums.AggregationType lowerAggregationType = GetLowerAggregationType(dpItem.IdAggregationType);
                    Enums.AggregationType higherAggregationType = GetHigherAggregationType(dpItem.IdAggregationType);

                    #region PeriodsAndAggregationType

                    if (dpItem.IdAggregationType == (int)Enums.AggregationType.Quarter)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(15));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(15));
                    }
                    /*if (dpItem.IdAggregationType == (int)Enums.AggregationType.Hour)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(60));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(60));
                    }*/
                    if (dpItem.IdAggregationType == (int)Enums.AggregationType.Day && !daylightSavingTime)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromDays(1));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromDays(1));
                    }

                    #endregion

                    dpItem.StartTime = stardDateTmp;
                    dpItem.EndTime = endDateTmp;

                    if (endDateTmp > ReferenceDate)
                        continue;//nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                                 //if (computingAggregates)
                                 //{                    
                                 //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                                 //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                                 //        continue;
                                 //}

                    PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                    PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.ServerPerformanceHierarchy.Values
                                                                                                                  .ToList()
                                                                                                                  .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == dpItem.IdDataType);

                    if (gophItem == null)
                    {
                        HierarchyItemMissingForImrServer.Add(Convert.ToInt64(dpItem.IdImrServer));
                        continue;
                    }

                    Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.ServerPeriodsDict;
                    if (periodsDict == null)
                    {
                        periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                        gosiItem.ServerPeriodsDict = periodsDict;
                    }

                    gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                     && p.EndTime == endDateTmp);


                    if (gopItem == null)
                    {
                        nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                        gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)dpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                        periodsDict.Add(nextIdPeriod, gopItem);
                    }


                    //sprawdzenie wyższych aggregation type

                    if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                        isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                    if (gophItem != null && (int)gophItem.MaxAggregationType > dpItem.IdAggregationType && computingAggregates && isLatestPeriod)
                    {
                        nextIdPeriod = CreateHigherAggregationTypePeriods(dpItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                    }

                    int count = periodsDict.Count(e => e.Value.AggregationType > Enums.AggregationType.Day);

                    if (!gopItem.ImrServerPerformanceDict.ContainsKey(currentImrServer.IdImrServer))
                        gopItem.ImrServerPerformanceDict.Add(currentImrServer.IdImrServer, new List<OpImrServerPerformance>());

                    if (!imrServerPerformancePeriodsDict.ContainsKey(gopItem.IdPeriod))
                        imrServerPerformancePeriodsDict.Add(gopItem.IdPeriod, new List<OpImrServerPerformance>());
                    imrServerPerformancePeriodsDict[gopItem.IdPeriod].Add(dpItem);

                    cnt++;
                }

                HierarchyItemMissingForImrServer = HierarchyItemMissingForImrServer.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {
                    #region CreateMissingServerPeriodsBasedOnDistributorPeriods

                    foreach (OpDistributor dItem in goisItem.DistributorList)
                    {
                        Dictionary<int, GOPeriod> gopDict = goisItem.PeriodsDict[dItem.IdDistributor];
                        foreach (GOPeriod gopItem in gopDict.Values)
                        {
                            if (!goisItem.ServerPeriodsDict.Values.ToList().Exists(d => d.AggregationType == gopItem.AggregationType
                                                                                    && d.StartTime == gopItem.StartTime
                                                                                    && d.EndTime == gopItem.EndTime))
                            {
                                isLatestPeriod = false;
                                nextIdPeriod = goisItem.ServerPeriodsDict.Values.Count == 0 ? 0 : goisItem.ServerPeriodsDict.Keys.Max() + 1;
                                PerformanceComponent.GOPeriod additionalnewPeriod = new PerformanceComponent.GOPeriod(nextIdPeriod, gopItem.StartTime, gopItem.EndTime,
                                                        (Enums.AggregationType)gopItem.AggregationType, gopItem.HigherAggregationType, gopItem.LowerAggregationType);
                                additionalnewPeriod.ImrServerPerformanceDict.Add(goisItem.Server.IdImrServer, new List<OpImrServerPerformance>());
                                goisItem.ServerPeriodsDict.Add(nextIdPeriod, additionalnewPeriod);

                                //sprawdzenie wyższych aggregation type
                                PerformanceComponent.GOPerformanceHierarchyItem gophItem = goisItem.ServerPerformanceHierarchy.Values
                                                                                                                  .ToList()
                                                                                                                  .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == DataType.DISTRIBUTOR_PERFORMANCE_VALUE);
                                DateTime endTimeConvertedToDay = new DateTime(gopItem.EndTime.Year, gopItem.EndTime.Month, gopItem.EndTime.Day, 0, 0, 0);
                                if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                                    isLatestPeriod = true;

                                if (gophItem != null && gophItem.MaxAggregationType > gopItem.AggregationType && computingAggregates && isLatestPeriod)
                                {
                                    nextIdPeriod = CreateHigherAggregationTypePeriods(gopItem.StartTime, gopItem.EndTime, Enums.AggregationType.Unknown, ReferenceDate, gopItem, nextIdPeriod, goisItem.ServerPeriodsDict, computingAggregates);
                                }
                            }
                        }
                    }

                    #endregion

                    List<PerformanceComponent.GOPeriod> periods = goisItem.ServerPeriodsDict.Values.ToList();
                    List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                    #region CreatePeriodHierarchy

                    for (int i = 0; i < periods.Count; i++)
                    {
                        if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                        {
                            if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                continue;

                            List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            #region AddingMissingPeriods

                            if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                            {
                                lowerPeriodsTmp = lowerPeriods.ToList();
                                AddingMissingImrServerPeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer); // Dodawanie brakujacych okresow do ServerPeriodsDict,
                            }

                            #endregion

                            periods = goisItem.ServerPeriodsDict.Values.ToList();
                            lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            if (lowerPeriods != null)
                                periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));
                        }
                        if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();
                    }
                    #endregion

                    #region FillDictDataHigherAggregates
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if (!imrServerPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            imrServerPerformancePeriodsDict.Add(periods[i].IdPeriod, new List<OpImrServerPerformance>());
                        if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                        {
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            {
                                for (int j = 0; j < periods[i].LowerIdPeriods.Count; j++)
                                {
                                    int idLowerPeriod = periods[i].LowerIdPeriods[j];
                                    PerformanceComponent.GOPeriod period = periods.Find(p => p.IdPeriod == idLowerPeriod);
                                    foreach (int iItem in period.ImrServerPerformanceDict.Keys)
                                    {
                                        if (!periods[i].ImrServerPerformanceDict.ContainsKey(iItem))
                                            periods[i].ImrServerPerformanceDict.Add(iItem, new List<OpImrServerPerformance>());
                                    }
                                }
                            }

                        }
                    }
                    #endregion

                    if (withValues)
                    {
                        #region FillAggregateValues

                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (imrServerPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            {
                                foreach (OpImrServerPerformance ispItem in imrServerPerformancePeriodsDict[periods[i].IdPeriod])
                                {
                                    if (periods[i].ImrServerPerformanceDict.ContainsKey(ispItem.IdImrServer))
                                        periods[i].ImrServerPerformanceDict[ispItem.IdImrServer].Add(ispItem);
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }
        #endregion

        #region ClassifyReportPerformancePerPeriod

        public static List<long> HierarchyItemMissingForReport = new List<long>();

        public static void ClassifyReportPerformancePerPeriod(DataProvider dataProvider,
                                                 List<OpReportPerformance> rpList,
                                                 DateTime ReferenceDate,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<int, List<OpReportPerformance>> reportPerformancePeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.REPORT_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;
                long? imrServerId = null;
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForReport = new List<long>();

                List<int> idReportList = rpList.Select(d => d.IdReport).Distinct().ToList();
                List<OpReportData> rdList = new List<OpReportData>();

                rdList = dataProvider.GetReportDataFilter(loadNavigationProperties: false, IdReport: idReportList.ToArray(), IdDataType: new long[] { DataType.REPORT_IMR_SERVER_ID }, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted);

                foreach (OpReportPerformance rpItem in rpList)
                {
                    OpImrServer currentImrServer = null;

                    if (rdList.Exists(e => e.IdReport == rpItem.IdReport))
                    {
                        if (rdList.Find(d => d.IdReport == rpItem.IdReport).Value != null)
                            imrServerId = Convert.ToInt64(rdList.Find(d => d.IdReport == rpItem.IdReport).Value);
                        else
                            continue;
                    }
                    else
                        continue;

                    DateTime endTimeConvertedToDay = new DateTime(rpItem.EndTime.Year, rpItem.EndTime.Month, rpItem.EndTime.Day, 0, 0, 0);
                    isLatestPeriod = false;

                    #region CurrentImrServer

                    if (!serverWorkspace.Exists(s => s.Server.IdImrServer == imrServerId))
                        continue;

                    currentImrServer = serverWorkspace.Find(s => s.Server.IdImrServer == imrServerId).Server;

                    #endregion

                    //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                    PerformanceComponent.GOPeriod gopItem = null;
                    DateTime stardDateTmp = rpItem.StartTime;
                    DateTime endDateTmp = rpItem.EndTime;

                    Enums.AggregationType lowerAggregationType = GetLowerAggregationType(rpItem.IdAggregationType);
                    Enums.AggregationType higherAggregationType = GetHigherAggregationType(rpItem.IdAggregationType);

                    #region PeriodsAndAggregationType

                    if (rpItem.IdAggregationType == (int)Enums.AggregationType.Quarter)
                    {
                        stardDateTmp = RoundToNearestInterval(rpItem.StartTime, TimeSpan.FromMinutes(15));
                        endDateTmp = RoundToNearestInterval(rpItem.EndTime, TimeSpan.FromMinutes(15));
                        higherAggregationType = Enums.AggregationType.Day;
                    }
                    /*if (dpItem.IdAggregationType == (int)Enums.AggregationType.Hour)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(60));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(60));
                        higherAggregationType = Enums.AggregationType.Day;
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }*/
                    if (rpItem.IdAggregationType == (int)Enums.AggregationType.Day && !daylightSavingTime)
                    {
                        stardDateTmp = RoundToNearestInterval(rpItem.StartTime, TimeSpan.FromDays(1));
                        endDateTmp = RoundToNearestInterval(rpItem.EndTime, TimeSpan.FromDays(1));
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }

                    #endregion

                    if (endDateTmp > ReferenceDate)
                        continue;//nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                                 //if (computingAggregates)
                                 //{                    
                                 //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                                 //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                                 //        continue;
                                 //}

                    PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                    PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.ServerPerformanceHierarchy.Values
                                                                                                                  .ToList()
                                                                                                                  .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == rpItem.IdDataType);

                    if (gophItem == null)
                    {
                        HierarchyItemMissingForReport.Add(Convert.ToInt64(rpItem.IdReport));
                        continue;
                    }

                    Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.ReportPeriodsDict;
                    if (periodsDict == null)
                    {
                        periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                        gosiItem.ReportPeriodsDict = periodsDict;
                    }

                    gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                     && p.EndTime == endDateTmp);
                    if (gopItem == null)
                    {
                        nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                        gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)rpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                        periodsDict.Add(nextIdPeriod, gopItem);
                        if (!gosiItem.ServerPeriodsDict.Values.ToList().Exists(e => e.StartTime == gopItem.StartTime && e.EndTime == gopItem.EndTime && e.AggregationType == gopItem.AggregationType))
                        {
                            // Żeby wyliczyć agregat serwerowy (26013), musimy zadbać aby w serwerPeriod był najnowszy period utworzony z aktualnych Raportow
                            // ponieważ jeśli nie mamy wykonfigurowanych wskaźników serwerowych, nie zostanie takowy utworzony i będziemy obliczać go z 15 min opóźnieniem.
                            // Dodatkowo należy zapewnić aby w PerformanceAggregateCompute, nowo wyznacozny wskaźnik TransmissionDriver po stronie serwera znalazł się w ImrServerPerformanceDict
                            // z odpowiednim IdPeriodu, aby przy wyznaczaniu wskaźnika serwera można było go wyciągnąć. 
                            int nextIdPeriodTmp = gosiItem.ServerPeriodsDict.Values.Count == 0 ? 0 : gosiItem.ServerPeriodsDict.Keys.Max() + 1;
                            GOPeriod gopItemTmp = new PerformanceComponent.GOPeriod(nextIdPeriodTmp, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)rpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                            if (!gopItemTmp.ImrServerPerformanceDict.ContainsKey(gosiItem.Server.IdImrServer))
                                gopItemTmp.ImrServerPerformanceDict.Add(gosiItem.Server.IdImrServer, new List<OpImrServerPerformance>());
                            gosiItem.ServerPeriodsDict.Add(nextIdPeriodTmp, gopItemTmp);
                        }
                    }
                    //sprawdzenie wyższych aggregation type
                    if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                        isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                    if (gophItem != null && (int)gophItem.MaxAggregationType > rpItem.IdAggregationType && computingAggregates && isLatestPeriod)
                    {
                        nextIdPeriod = CreateHigherAggregationTypePeriods(rpItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                    }


                    if (!gopItem.ReportPerformanceDict.ContainsKey(rpItem.IdReport))
                        gopItem.ReportPerformanceDict.Add(rpItem.IdReport, new List<OpReportPerformance>());

                    if (!reportPerformancePeriodsDict.ContainsKey(gopItem.IdPeriod))
                        reportPerformancePeriodsDict.Add(gopItem.IdPeriod, new List<OpReportPerformance>());
                    reportPerformancePeriodsDict[gopItem.IdPeriod].Add(rpItem);

                    cnt++;
                }

                HierarchyItemMissingForReport = HierarchyItemMissingForReport.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {
                    List<PerformanceComponent.GOPeriod> periods = goisItem.ReportPeriodsDict.Values.ToList();
                    List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                    #region CreatePeriodHierarchy
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                        {
                            if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                continue;

                            List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();
                            #region AddingMissingPeriods

                            if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                            {
                                lowerPeriodsTmp = lowerPeriods.ToList();
                                AddingMissingReportPeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer); // Dodawanie brakujacych okresow do ReportPeriodsDict,
                            }

                            #endregion

                            periods = goisItem.ReportPeriodsDict.Values.ToList();
                            lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            if (lowerPeriods != null)
                                periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));
                        }
                        if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();
                    }
                    #endregion

                    #region FillDictDataHigherAggregates
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                        {
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            {
                                for (int j = 0; j < periods[i].LowerIdPeriods.Count; j++)
                                {
                                    int idLowerPeriod = periods[i].LowerIdPeriods[j];
                                    PerformanceComponent.GOPeriod period = periods.Find(p => p.IdPeriod == idLowerPeriod);
                                    foreach (int iItem in period.ReportPerformanceDict.Keys)
                                    {
                                        if (!periods[i].ReportPerformanceDict.ContainsKey(iItem))
                                            periods[i].ReportPerformanceDict.Add(iItem, new List<OpReportPerformance>());
                                    }
                                }
                            }
                            if (!reportPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                                reportPerformancePeriodsDict.Add(periods[i].IdPeriod, new List<OpReportPerformance>());
                        }
                    }
                    #endregion

                    if (withValues)
                    {
                        #region FillAggregateValues

                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (reportPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            {
                                foreach (OpReportPerformance rpItem in reportPerformancePeriodsDict[periods[i].IdPeriod])
                                {
                                    if (periods[i].ReportPerformanceDict.ContainsKey(rpItem.IdReport))
                                        periods[i].ReportPerformanceDict[rpItem.IdReport].Add(rpItem);
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }
        #endregion

        #region ClassifyQueuePerformancePerPeriod

        public static List<string> HierarchyItemMissingForQueue = new List<string>();

        public static void ClassifyQueuePerformancePerPeriod(DataProvider dataProvider,
                                                 List<PerformanceComponent.GOQueue> goqList,
                                                 DateTime ReferenceDate,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<int, List<PerformanceComponent.GOQueue>> queuePerformancePeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForQueue = new List<string>();

                foreach (PerformanceComponent.GOQueue dpItem in goqList)
                {
                    if (dpItem.IndexNbr == QueueAggregateIndexNumber) // index = 0 zarezerowany dla agregatów serwerowych!
                        continue;

                    OpImrServer currentImrServer = null;
                    DateTime endTimeConvertedToDay = new DateTime(dpItem.EndTime.Value.Year, dpItem.EndTime.Value.Month, dpItem.EndTime.Value.Day, 0, 0, 0);
                    isLatestPeriod = false;

                    #region CurrentImrServer

                    if (!serverWorkspace.Exists(s => s.Server.IdImrServer == dpItem.IdImrServer))
                        continue;

                    currentImrServer = serverWorkspace.Find(s => s.Server.IdImrServer == dpItem.IdImrServer).Server;

                    #endregion

                    //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                    PerformanceComponent.GOPeriod gopItem = null;
                    DateTime stardDateTmp = dpItem.StartTime;
                    DateTime endDateTmp = dpItem.EndTime.Value;

                    Enums.AggregationType lowerAggregationType = GetLowerAggregationType(dpItem.IdAggregationType);
                    Enums.AggregationType higherAggregationType = GetHigherAggregationType(dpItem.IdAggregationType);

                    #region PeriodsAndAggregationType

                    if (dpItem.IdAggregationType == (int)Enums.AggregationType.Quarter)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(15));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(15));
                        higherAggregationType = Enums.AggregationType.Day;
                    }
                    /*if (dpItem.IdAggregationType == (int)Enums.AggregationType.Hour)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(60));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(60));
                        higherAggregationType = Enums.AggregationType.Day;
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }*/
                    if (dpItem.IdAggregationType == (int)Enums.AggregationType.Day && !daylightSavingTime)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromDays(1));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromDays(1));
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }

                    #endregion

                    dpItem.StartTime = stardDateTmp;
                    dpItem.EndTime = endDateTmp;

                    if (endDateTmp > ReferenceDate)
                        continue;//nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                                 //if (computingAggregates)
                                 //{                    
                                 //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                                 //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                                 //        continue;
                                 //}

                    PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                    PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.ServerPerformanceHierarchy.Values
                                                                                                                      .ToList()
                                                                                                                      .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == dpItem.IdDataType);

                    if (gophItem == null)
                    {
                        HierarchyItemMissingForQueue.Add(currentImrServer.IdImrServer + "_" + dpItem.IndexNbr);
                        continue;
                    }

                    Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.QueuePeriodsDict;
                    if (periodsDict == null)
                    {
                        periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                        gosiItem.QueuePeriodsDict = periodsDict;
                    }

                    gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                     && p.EndTime == endDateTmp);
                    if (gopItem == null)
                    {
                        nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                        gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)dpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                        periodsDict.Add(nextIdPeriod, gopItem);
                        if (!gosiItem.ServerPeriodsDict.Values.ToList().Exists(e => e.StartTime == gopItem.StartTime && e.EndTime == gopItem.EndTime && e.AggregationType == gopItem.AggregationType))
                        {
                            // Żeby wyliczyć agregat serwerowy (26013), musimy zadbać aby w serwerPeriod był najnowszy period utworzony z aktualnych Kolejek
                            // ponieważ jeśli nie mamy wykonfigurowanych wskaźników serwerowych, nie zostanie takowy utworzony i będziemy obliczać go z 15 min opóźnieniem.
                            // Dodatkowo należy zapewnić aby w PerformanceAggregateCompute, nowo wyznacozny wskaźnik TransmissionDriver po stronie serwera znalazł się w ImrServerPerformanceDict
                            // z odpowiednim IdPeriodu, aby przy wyznaczaniu wskaźnika serwera można było go wyciągnąć. 
                            int nextIdPeriodTmp = gosiItem.ServerPeriodsDict.Values.Count == 0 ? 0 : gosiItem.ServerPeriodsDict.Keys.Max() + 1;
                            GOPeriod gopItemTmp = new PerformanceComponent.GOPeriod(nextIdPeriodTmp, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)dpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                            if (!gopItemTmp.ImrServerPerformanceDict.ContainsKey(gosiItem.Server.IdImrServer))
                                gopItemTmp.ImrServerPerformanceDict.Add(gosiItem.Server.IdImrServer, new List<OpImrServerPerformance>());
                            gosiItem.ServerPeriodsDict.Add(nextIdPeriodTmp, gopItemTmp);
                        }
                    }
                    //sprawdzenie wyższych aggregation type
                    if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                        isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                    if (gophItem != null && (int)gophItem.MaxAggregationType > dpItem.IdAggregationType && computingAggregates && isLatestPeriod)
                    {
                        nextIdPeriod = CreateHigherAggregationTypePeriods(dpItem.ImrServerPerformance, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                    }

                    if (!gopItem.QueuePerformanceDict.ContainsKey(currentImrServer.IdImrServer + "_" + dpItem.IndexNbr))
                        gopItem.QueuePerformanceDict.Add(currentImrServer.IdImrServer + "_" + dpItem.IndexNbr, new List<PerformanceComponent.GOQueue>());

                    if (!queuePerformancePeriodsDict.ContainsKey(gopItem.IdPeriod))
                        queuePerformancePeriodsDict.Add(gopItem.IdPeriod, new List<PerformanceComponent.GOQueue>());
                    queuePerformancePeriodsDict[gopItem.IdPeriod].Add(dpItem);

                    cnt++;
                }

                HierarchyItemMissingForQueue = HierarchyItemMissingForQueue.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {

                    List<PerformanceComponent.GOPeriod> periods = goisItem.QueuePeriodsDict.Values.ToList();
                    List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                    #region CreatePeriodHierarchy
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                        {
                            if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                continue;

                            List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            #region AddingMissingPeriods

                            if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                            {
                                lowerPeriodsTmp = lowerPeriods.ToList();
                                AddingMissingQueuePeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer); // Dodawanie brakujacych okresow do QueuePeriodsDict,                            
                            }

                            #endregion

                            periods = goisItem.QueuePeriodsDict.Values.ToList();
                            lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            if (lowerPeriods != null)
                                periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));

                        }
                        if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();

                    }
                    #endregion

                    #region FillDictDataHigherAggregates
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                        {
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            {
                                foreach (int iIdItem in periods[i].LowerIdPeriods)
                                {
                                    int idLowerPeriod = iIdItem;
                                    PerformanceComponent.GOPeriod period = periods.Find(p => p.IdPeriod == idLowerPeriod);
                                    foreach (string iItem in period.QueuePerformanceDict.Keys)
                                    {
                                        if (!periods[i].QueuePerformanceDict.ContainsKey(iItem))
                                            periods[i].QueuePerformanceDict.Add(iItem, new List<PerformanceComponent.GOQueue>());
                                    }
                                }
                            }
                        }
                        if (!queuePerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            queuePerformancePeriodsDict.Add(periods[i].IdPeriod, new List<PerformanceComponent.GOQueue>());
                    }
                    #endregion

                    if (withValues)
                    {
                        #region FillAggregateValues

                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (queuePerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            {
                                foreach (PerformanceComponent.GOQueue goqItem in queuePerformancePeriodsDict[periods[i].IdPeriod])
                                {
                                    if (periods[i].QueuePerformanceDict.ContainsKey(goqItem.ImrServerPerformance.IdImrServer + "_" + goqItem.IndexNbr))
                                        periods[i].QueuePerformanceDict[goqItem.ImrServerPerformance.IdImrServer + "_" + goqItem.IndexNbr].Add(goqItem);
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }
        #endregion

        #region ClassifyProcessPerformancePerPeriod

        public static List<string> HierarchyItemMissingForProcess = new List<string>();

        public static void ClassifyProcessPerformancePerPeriod(DataProvider dataProvider,
                                                 List<PerformanceComponent.GOProcess> gopList,
                                                 DateTime ReferenceDate,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<int, List<PerformanceComponent.GOProcess>> processPerformancePeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForProcess = new List<string>();

                foreach (PerformanceComponent.GOProcess goProcessItem in gopList)
                {
                    if (goProcessItem.IndexNbr == ProcessAggregateIndexNumber) // index = 0 zarezerowany dla agregatów serwerowych!
                        continue;

                    OpImrServer currentImrServer = null;
                    DateTime endTimeConvertedToDay = new DateTime(goProcessItem.EndTime.Value.Year, goProcessItem.EndTime.Value.Month, goProcessItem.EndTime.Value.Day, 0, 0, 0);
                    isLatestPeriod = false;

                    #region CurrentImrServer

                    if (!serverWorkspace.Exists(s => s.Server.IdImrServer == goProcessItem.IdImrServer))
                        continue;

                    currentImrServer = serverWorkspace.Find(s => s.Server.IdImrServer == goProcessItem.IdImrServer).Server;

                    #endregion

                    //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                    PerformanceComponent.GOPeriod gopItem = null;
                    DateTime stardDateTmp = goProcessItem.StartTime;
                    DateTime endDateTmp = goProcessItem.EndTime.Value;

                    Enums.AggregationType lowerAggregationType = GetLowerAggregationType(goProcessItem.IdAggregationType);
                    Enums.AggregationType higherAggregationType = GetHigherAggregationType(goProcessItem.IdAggregationType);

                    #region PeriodsAndAggregationType

                    if (goProcessItem.IdAggregationType == (int)Enums.AggregationType.Quarter)
                    {
                        stardDateTmp = RoundToNearestInterval(goProcessItem.StartTime, TimeSpan.FromMinutes(15));
                        endDateTmp = RoundToNearestInterval(goProcessItem.EndTime.Value, TimeSpan.FromMinutes(15));
                        higherAggregationType = Enums.AggregationType.Day;
                    }
                    /*if (dpItem.IdAggregationType == (int)Enums.AggregationType.Hour)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(60));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(60));
                        higherAggregationType = Enums.AggregationType.Day;
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }*/
                    if (goProcessItem.IdAggregationType == (int)Enums.AggregationType.Day && !daylightSavingTime)
                    {
                        stardDateTmp = RoundToNearestInterval(goProcessItem.StartTime, TimeSpan.FromDays(1));
                        endDateTmp = RoundToNearestInterval(goProcessItem.EndTime.Value, TimeSpan.FromDays(1));
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }

                    #endregion

                    goProcessItem.StartTime = stardDateTmp;
                    goProcessItem.EndTime = endDateTmp;

                    if (endDateTmp > ReferenceDate)
                        continue;//nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                                 //if (computingAggregates)
                                 //{                    
                                 //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                                 //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                                 //        continue;
                                 //}

                    PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                    PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.ServerPerformanceHierarchy.Values
                                                                                                                      .ToList()
                                                                                                                      .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == goProcessItem.IdDataType);
                    if (gophItem == null)
                    {
                        HierarchyItemMissingForProcess.Add(currentImrServer.IdImrServer + "_" + goProcessItem.IndexNbr);
                        continue;
                    }

                    Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.ProcessPeriodsDict;
                    if (periodsDict == null)
                    {
                        periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                        gosiItem.ProcessPeriodsDict = periodsDict;
                    }

                    gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                     && p.EndTime == endDateTmp);
                    if (gopItem == null)
                    {
                        nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                        gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)goProcessItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                        periodsDict.Add(nextIdPeriod, gopItem);
                        if (!gosiItem.ServerPeriodsDict.Values.ToList().Exists(e => e.StartTime == gopItem.StartTime && e.EndTime == gopItem.EndTime && e.AggregationType == gopItem.AggregationType))
                        {
                            // Żeby wyliczyć agregat serwerowy (26013), musimy zadbać aby w serwerPeriod był najnowszy period utworzony z aktualnych Procesów
                            // ponieważ jeśli nie mamy wykonfigurowanych wskaźników serwerowych, nie zostanie takowy utworzony i będziemy obliczać go z 15 min opóźnieniem.
                            // Dodatkowo należy zapewnić aby w PerformanceAggregateCompute, nowo wyznacozny wskaźnik TransmissionDriver po stronie serwera znalazł się w ImrServerPerformanceDict
                            // z odpowiednim IdPeriodu, aby przy wyznaczaniu wskaźnika serwera można było go wyciągnąć. 
                            int nextIdPeriodTmp = gosiItem.ServerPeriodsDict.Values.Count == 0 ? 0 : gosiItem.ServerPeriodsDict.Keys.Max() + 1;
                            GOPeriod gopItemTmp = new PerformanceComponent.GOPeriod(nextIdPeriodTmp, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)goProcessItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                            if (!gopItemTmp.ImrServerPerformanceDict.ContainsKey(gosiItem.Server.IdImrServer))
                                gopItemTmp.ImrServerPerformanceDict.Add(gosiItem.Server.IdImrServer, new List<OpImrServerPerformance>());
                            gosiItem.ServerPeriodsDict.Add(nextIdPeriodTmp, gopItemTmp);
                        }
                    }
                    //sprawdzenie wyższych aggregation type

                    if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                        isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                    if (gophItem != null && (int)gophItem.MaxAggregationType > goProcessItem.IdAggregationType && computingAggregates && isLatestPeriod)
                    {
                        nextIdPeriod = CreateHigherAggregationTypePeriods(goProcessItem.ImrServerPerformance, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                    }

                    if (!gopItem.ProcessPerformanceDict.ContainsKey(currentImrServer.IdImrServer + "_" + goProcessItem.IndexNbr))
                        gopItem.ProcessPerformanceDict.Add(currentImrServer.IdImrServer + "_" + goProcessItem.IndexNbr, new List<PerformanceComponent.GOProcess>());

                    if (!processPerformancePeriodsDict.ContainsKey(gopItem.IdPeriod))
                        processPerformancePeriodsDict.Add(gopItem.IdPeriod, new List<PerformanceComponent.GOProcess>());
                    processPerformancePeriodsDict[gopItem.IdPeriod].Add(goProcessItem);

                    cnt++;
                }

                HierarchyItemMissingForProcess = HierarchyItemMissingForProcess.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {

                    List<PerformanceComponent.GOPeriod> periods = goisItem.ProcessPeriodsDict.Values.ToList();
                    List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                    #region CreatePeriodHierarchy
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                        {
                            if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                continue;

                            List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            #region AddingMissingPeriods

                            if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                            {
                                lowerPeriodsTmp = lowerPeriods.ToList();
                                AddingMissingProcessPeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer); // Dodawanie brakujacych okresow do ProcessPeriodsDict,

                            }

                            #endregion

                            periods = goisItem.ProcessPeriodsDict.Values.ToList();
                            lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            if (lowerPeriods != null)
                                periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));

                        }
                        if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();

                    }
                    #endregion

                    #region FillDictDataHigherAggregates
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                        {
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            {
                                foreach (int iIdItem in periods[i].LowerIdPeriods)
                                {
                                    int idLowerPeriod = iIdItem;
                                    PerformanceComponent.GOPeriod period = periods.Find(p => p.IdPeriod == idLowerPeriod);
                                    foreach (string iItem in period.ProcessPerformanceDict.Keys)
                                    {
                                        if (!periods[i].ProcessPerformanceDict.ContainsKey(iItem))
                                            periods[i].ProcessPerformanceDict.Add(iItem, new List<PerformanceComponent.GOProcess>());
                                    }
                                }
                            }
                        }
                        if (!processPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            processPerformancePeriodsDict.Add(periods[i].IdPeriod, new List<PerformanceComponent.GOProcess>());
                    }
                    #endregion

                    if (withValues)
                    {
                        #region FillAggregateValues

                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (processPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            {
                                foreach (PerformanceComponent.GOProcess gopItem in processPerformancePeriodsDict[periods[i].IdPeriod])
                                {
                                    if (periods[i].ProcessPerformanceDict.ContainsKey(gopItem.ImrServerPerformance.IdImrServer + "_" + gopItem.IndexNbr))
                                        periods[i].ProcessPerformanceDict[gopItem.ImrServerPerformance.IdImrServer + "_" + gopItem.IndexNbr].Add(gopItem);
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }
        #endregion

        #region ClassifyTransmissionDriverPerformancePerPeriod

        public static List<long> HierarchyItemMissingForTransmissionDriver = new List<long>();

        public static void ClassifyTransmissionDriverPerformancePerPeriod(DataProvider dataProvider,
                                                 List<OpTransmissionDriverPerformance> tdpList,
                                                 DateTime ReferenceDate,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<int, List<OpTransmissionDriverPerformance>> transmissionDriverPerformancePeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;
                long? imrServerId = null;
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForTransmissionDriver = new List<long>();

                List<int> idTransmissionDriverList = tdpList.Select(d => d.IdTransmissionDriver).Distinct().ToList();
                List<DB_TRANSMISSION_DRIVER_DATA> tddList = new List<DB_TRANSMISSION_DRIVER_DATA>();

                if (idTransmissionDriverList != null && idTransmissionDriverList.Count > 0)
                {
                    tddList = dataProvider.dbConnectionDw.GetTransmissionDriverDataFilter(IdTransmissionDriverData: null,
                                                                                            IdTransmissionDriver: idTransmissionDriverList.ToArray(),
                                                                                            IdDataType: new long[] { DataType.TRANSMISSION_DRIVER_IMR_SERVER_ID },
                                                                                            IndexNbr: null,
                                                                                            topCount: null,
                                                                                            customWhereClause: null,
                                                                                            autoTransaction: false,
                                                                                            transactionLevel: System.Data.IsolationLevel.ReadUncommitted).ToList();
                }
                if (tddList != null && tddList.Count > 0)
                {

                    foreach (OpTransmissionDriverPerformance tdpItem in tdpList)
                    {
                        OpImrServer currentImrServer = null;
                        if (tddList.Find(d => d.ID_TRANSMISSION_DRIVER == tdpItem.IdTransmissionDriver) != null && tddList.Find(d => d.ID_TRANSMISSION_DRIVER == tdpItem.IdTransmissionDriver).VALUE != null)
                        {
                            imrServerId = Convert.ToInt64(tddList.Find(d => d.ID_TRANSMISSION_DRIVER == tdpItem.IdTransmissionDriver).VALUE); // id serwera zawsze z DW
                            DateTime endTimeConvertedToDay = new DateTime(tdpItem.EndTime.Year, tdpItem.EndTime.Month, tdpItem.EndTime.Day, 0, 0, 0);
                            isLatestPeriod = false;

                            #region CurrentImrServer

                            currentImrServer = GetImrServerBasedOnTransmissionDriver(dataProvider.GetImrServer(Convert.ToInt32(imrServerId.Value)), serverWorkspace);

                            if (currentImrServer == null)
                                continue;

                            #endregion

                            //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                            PerformanceComponent.GOPeriod gopItem = null;
                            DateTime stardDateTmp = tdpItem.StartTime;
                            DateTime endDateTmp = tdpItem.EndTime;

                            Enums.AggregationType lowerAggregationType = GetLowerAggregationType(tdpItem.IdAggregationType);
                            Enums.AggregationType higherAggregationType = GetHigherAggregationType(tdpItem.IdAggregationType);

                            #region PeriodsAndAggregationType

                            if (tdpItem.IdAggregationType == (int)Enums.AggregationType.Quarter)
                            {
                                stardDateTmp = RoundToNearestInterval(tdpItem.StartTime, TimeSpan.FromMinutes(15));
                                endDateTmp = RoundToNearestInterval(tdpItem.EndTime, TimeSpan.FromMinutes(15));
                                higherAggregationType = Enums.AggregationType.Day;
                            }

                            if (tdpItem.IdAggregationType == (int)Enums.AggregationType.Day && !daylightSavingTime)
                            {
                                stardDateTmp = RoundToNearestInterval(tdpItem.StartTime, TimeSpan.FromDays(1));
                                endDateTmp = RoundToNearestInterval(tdpItem.EndTime, TimeSpan.FromDays(1));
                                lowerAggregationType = Enums.AggregationType.Quarter;
                            }

                            #endregion

                            if (endDateTmp > ReferenceDate)
                                continue;//nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                                         //if (computingAggregates)
                                         //{                    
                                         //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                                         //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                                         //        continue;
                                         //}

                            PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                            PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.ServerPerformanceHierarchy.Values
                                                                                                                          .ToList()
                                                                                                                          .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == tdpItem.IdDataType);

                            if (gophItem == null)
                            {
                                HierarchyItemMissingForTransmissionDriver.Add(Convert.ToInt64(tdpItem.IdTransmissionDriver));
                                continue;
                            }

                            Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.TransmissionDriverPeriodsDict;
                            if (periodsDict == null)
                            {
                                periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                                gosiItem.TransmissionDriverPeriodsDict = periodsDict;
                            }

                            gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                             && p.EndTime == endDateTmp);
                            if (gopItem == null)
                            {
                                nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                                gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                        (Enums.AggregationType)tdpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                                periodsDict.Add(nextIdPeriod, gopItem);
                                if (!gosiItem.ServerPeriodsDict.Values.ToList().Exists(e => e.StartTime == gopItem.StartTime && e.EndTime == gopItem.EndTime && e.AggregationType == gopItem.AggregationType))
                                {
                                    // Żeby wyliczyć agregat serwerowy (26013), musimy zadbać aby w serwerPeriod był najnowszy period utworzony z aktualnych TransmissionDriver
                                    // ponieważ jeśli nie mamy wykonfigurowanych wskaźników serwerowych, nie zostanie takowy utworzony i będziemy obliczać go z 15 min opóźnieniem.
                                    // Dodatkowo należy zapewnić aby w PerformanceAggregateCompute, nowo wyznacozny wskaźnik TransmissionDriver po stronie serwera znalazł się w ImrServerPerformanceDict
                                    // z odpowiednim IdPeriodu, aby przy wyznaczaniu wskaźnika serwera można było go wyciągnąć. 
                                    int nextIdPeriodTmp = gosiItem.ServerPeriodsDict.Values.Count == 0 ? 0 : gosiItem.ServerPeriodsDict.Keys.Max() + 1;
                                    GOPeriod gopItemTmp = new PerformanceComponent.GOPeriod(nextIdPeriodTmp, stardDateTmp, endDateTmp,
                                                        (Enums.AggregationType)tdpItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                                    if (!gopItemTmp.ImrServerPerformanceDict.ContainsKey(gosiItem.Server.IdImrServer))
                                        gopItemTmp.ImrServerPerformanceDict.Add(gosiItem.Server.IdImrServer, new List<OpImrServerPerformance>());
                                    gosiItem.ServerPeriodsDict.Add(nextIdPeriodTmp, gopItemTmp);
                                }
                            }
                            //sprawdzenie wyższych aggregation type
                            if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                                isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                            if (gophItem != null && (int)gophItem.MaxAggregationType > tdpItem.IdAggregationType && computingAggregates && isLatestPeriod)
                            {
                                nextIdPeriod = CreateHigherAggregationTypePeriods(tdpItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                            }


                            if (!gopItem.TransmissionDriverPerformanceDict.ContainsKey(tdpItem.IdTransmissionDriver))
                                gopItem.TransmissionDriverPerformanceDict.Add(tdpItem.IdTransmissionDriver, new List<OpTransmissionDriverPerformance>());

                            if (!transmissionDriverPerformancePeriodsDict.ContainsKey(gopItem.IdPeriod))
                                transmissionDriverPerformancePeriodsDict.Add(gopItem.IdPeriod, new List<OpTransmissionDriverPerformance>());
                            transmissionDriverPerformancePeriodsDict[gopItem.IdPeriod].Add(tdpItem);

                            cnt++;
                        }
                    }
                }

                HierarchyItemMissingForTransmissionDriver = HierarchyItemMissingForTransmissionDriver.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {
                    List<PerformanceComponent.GOPeriod> periods = goisItem.TransmissionDriverPeriodsDict.Values.ToList();
                    List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                    #region CreatePeriodHierarchy
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                        {
                            if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                continue;

                            List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();
                            #region AddingMissingPeriods

                            if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                            {
                                lowerPeriodsTmp = lowerPeriods.ToList();
                                AddingMissingTransmissionDriverPeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer); // Dodawanie brakujacych okresow do TransmissionDriverPeriodsDict,
                            }

                            #endregion

                            periods = goisItem.TransmissionDriverPeriodsDict.Values.ToList();
                            lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            if (lowerPeriods != null)
                                periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));
                        }
                        if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();
                    }
                    #endregion

                    #region FillDictDataHigherAggregates
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                        {
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            {
                                for (int j = 0; j < periods[i].LowerIdPeriods.Count; j++)
                                {
                                    int idLowerPeriod = periods[i].LowerIdPeriods[j];
                                    PerformanceComponent.GOPeriod period = periods.Find(p => p.IdPeriod == idLowerPeriod);
                                    foreach (int iItem in period.TransmissionDriverPerformanceDict.Keys)
                                    {
                                        if (!periods[i].TransmissionDriverPerformanceDict.ContainsKey(iItem))
                                            periods[i].TransmissionDriverPerformanceDict.Add(iItem, new List<OpTransmissionDriverPerformance>());
                                    }
                                }
                            }
                            if (!transmissionDriverPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                                transmissionDriverPerformancePeriodsDict.Add(periods[i].IdPeriod, new List<OpTransmissionDriverPerformance>());
                        }
                    }
                    #endregion

                    if (withValues)
                    {
                        #region FillAggregateValues

                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (transmissionDriverPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            {
                                foreach (OpTransmissionDriverPerformance tdpItem in transmissionDriverPerformancePeriodsDict[periods[i].IdPeriod])
                                {
                                    if (periods[i].TransmissionDriverPerformanceDict.ContainsKey(tdpItem.IdTransmissionDriver))
                                        periods[i].TransmissionDriverPerformanceDict[tdpItem.IdTransmissionDriver].Add(tdpItem);
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }
        #endregion

        #region ClassifyEtlPerformancePerPeriod

        public static List<long> HierarchyItemMissingForEtl = new List<long>();

        public static void ClassifyEtlPerformancePerPeriod(DataProvider dataProvider,
                                                 List<OpEtlPerformance> etlList,
                                                 DateTime ReferenceDate,
                                                 List<GOImrServer> serverWorkspace,
                                                 Dictionary<int, List<OpEtlPerformance>> etlPerformancePeriodsDict,
                                                 bool withValues = true,
                                                 bool computingAggregates = true,
                                                 bool daylightSavingTime = false)
        {
            Dictionary<string, List<long>> allConfiguredHierarchyDataType = GetAllConfiguredHierarchyDataType(serverWorkspace, dataProvider);

            if (allConfiguredHierarchyDataType[Enums.DataTypeGroup.ETL_PERFORMANCES].Count > 0)
            {
                int nextIdPeriod = 0;
                int cnt = 0;
                long? imrServerId = null;
                DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, DaysPastToCompute);
                DateTime lastEndTimeConvertedToDay = new DateTime();
                if (computingAggregates)
                    if (ComputeHigherAggregationThanDay || AggregationTypeToAnalyze.Exists(e => e.Equals((int)Enums.AggregationType.Day)))
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0).AddDays(-(DaysPastToCompute - 1));
                    else
                        lastEndTimeConvertedToDay = new DateTime(ReferenceDate.Year, ReferenceDate.Month, ReferenceDate.Day, 0, 0, 0);

                bool isLatestPeriod = false;

                HierarchyItemMissingForEtl = new List<long>();

                List<int> idEtlList = etlList.Select(d => d.IdEtl).Distinct().ToList();
                List<GOMapping> mappingList = new List<GOMapping>();

                dataProvider.GetMappingFilter(IdReferenceType: new int[] { (int)Enums.ReferenceType.IdEtl }, loadNavigationProperties: false,
                    mergeIntoCache: false, transactionLevel: IsolationLevel.ReadUncommitted, autoTransaction: false).ForEach(d => mappingList.Add(new GOMapping(d)));
                dataProvider.GetMappingFilterDW(IdReferenceType: new int[] { (int)Enums.ReferenceType.IdEtl }, loadNavigationProperties: false,
                    mergeIntoCache: false, transactionLevel: IsolationLevel.ReadUncommitted, autoTransaction: false).ForEach(d => mappingList.Add(new GOMapping(d)));

                foreach (OpEtlPerformance etlItem in etlList)
                {
                    OpImrServer currentImrServer = null;

                    /*
                     * WAŻNE: może być problem z IdEtl z tabeli Etl_Performance ponieważ i na core i na DW moga być etl o tym samym id.
                     * 
                     * 
                     * */
                    if (mappingList.Exists(e => e.IdSource == etlItem.IdEtl))
                    {
                        imrServerId = Convert.ToInt64(mappingList.Find(d => d.IdSource == etlItem.IdEtl).IdImrServer);
                    }
                    else
                        continue;

                    DateTime endTimeConvertedToDay = new DateTime(etlItem.EndTime.Year, etlItem.EndTime.Month, etlItem.EndTime.Day, 0, 0, 0);
                    isLatestPeriod = false;

                    #region CurrentImrServer

                    if (!serverWorkspace.Exists(s => s.Server.IdImrServer == imrServerId))
                        continue;

                    currentImrServer = serverWorkspace.Find(s => s.Server.IdImrServer == imrServerId).Server;

                    #endregion

                    //ConsoleLog.LogOverwriteCurrentLine(String.Format("Data analyzed data {0}/{1}", cnt, dtList.Count));
                    PerformanceComponent.GOPeriod gopItem = null;
                    DateTime stardDateTmp = etlItem.StartTime;
                    DateTime endDateTmp = etlItem.EndTime;

                    Enums.AggregationType lowerAggregationType = GetLowerAggregationType(etlItem.IdAggregationType);
                    Enums.AggregationType higherAggregationType = GetHigherAggregationType(etlItem.IdAggregationType);

                    #region PeriodsAndAggregationType

                    if (etlItem.IdAggregationType == (int)Enums.AggregationType.Quarter)
                    {
                        stardDateTmp = RoundToNearestInterval(etlItem.StartTime, TimeSpan.FromMinutes(15));
                        endDateTmp = RoundToNearestInterval(etlItem.EndTime, TimeSpan.FromMinutes(15));
                        higherAggregationType = Enums.AggregationType.Day;
                    }
                    /*if (dpItem.IdAggregationType == (int)Enums.AggregationType.Hour)
                    {
                        stardDateTmp = RoundToNearestInterval(dpItem.StartTime, TimeSpan.FromMinutes(60));
                        endDateTmp = RoundToNearestInterval(dpItem.EndTime.Value, TimeSpan.FromMinutes(60));
                        higherAggregationType = Enums.AggregationType.Day;
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }*/
                    if (etlItem.IdAggregationType == (int)Enums.AggregationType.Day && !daylightSavingTime)
                    {
                        stardDateTmp = RoundToNearestInterval(etlItem.StartTime, TimeSpan.FromDays(1));
                        endDateTmp = RoundToNearestInterval(etlItem.EndTime, TimeSpan.FromDays(1));
                        lowerAggregationType = Enums.AggregationType.Quarter;
                    }

                    #endregion

                    if (endDateTmp > ReferenceDate)
                        continue;//nie chcemy brać pod uwagę okresów które w stosunku do daty referencyjnej się jeszcze nei skończyły, obłuzone tez w CreateHigherAggregationTypePeriods
                                 //if (computingAggregates)
                                 //{                    
                                 //    if (stardDateTmp < lastFullDay && !ComputeHigherAggregationThanDay)
                                 //        //albo tych które obejmują okres wcześniejszy niz ten który analizujemy
                                 //        continue;
                                 //}

                    PerformanceComponent.GOImrServer gosiItem = serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer);
                    PerformanceComponent.GOPerformanceHierarchyItem gophItem = gosiItem.ServerPerformanceHierarchy.Values
                                                                                                                  .ToList()
                                                                                                                  .FirstOrDefault(d => Convert.ToInt64(d.DataType.Value) == etlItem.IdDataType);

                    if (gophItem == null)
                    {
                        HierarchyItemMissingForEtl.Add(Convert.ToInt64(etlItem.IdEtl));
                        continue;
                    }

                    Dictionary<int, PerformanceComponent.GOPeriod> periodsDict = gosiItem.EtlPeriodsDict;
                    if (periodsDict == null)
                    {
                        periodsDict = new Dictionary<int, PerformanceComponent.GOPeriod>();
                        gosiItem.EtlPeriodsDict = periodsDict;
                    }

                    gopItem = periodsDict.Values.ToList().Find(p => p.StartTime == stardDateTmp
                                                                     && p.EndTime == endDateTmp);
                    if (gopItem == null)
                    {
                        nextIdPeriod = periodsDict.Values.Count == 0 ? 0 : periodsDict.Keys.Max() + 1;
                        gopItem = new PerformanceComponent.GOPeriod(nextIdPeriod, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)etlItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                        periodsDict.Add(nextIdPeriod, gopItem);
                        if (!gosiItem.ServerPeriodsDict.Values.ToList().Exists(e => e.StartTime == gopItem.StartTime && e.EndTime == gopItem.EndTime && e.AggregationType == gopItem.AggregationType))
                        {
                            // Żeby wyliczyć agregat serwerowy (26013), musimy zadbać aby w serwerPeriod był najnowszy period utworzony z aktualnych Raportow
                            // ponieważ jeśli nie mamy wykonfigurowanych wskaźników serwerowych, nie zostanie takowy utworzony i będziemy obliczać go z 15 min opóźnieniem.
                            // Dodatkowo należy zapewnić aby w PerformanceAggregateCompute, nowo wyznacozny wskaźnik TransmissionDriver po stronie serwera znalazł się w ImrServerPerformanceDict
                            // z odpowiednim IdPeriodu, aby przy wyznaczaniu wskaźnika serwera można było go wyciągnąć. 
                            int nextIdPeriodTmp = gosiItem.ServerPeriodsDict.Values.Count == 0 ? 0 : gosiItem.ServerPeriodsDict.Keys.Max() + 1;
                            GOPeriod gopItemTmp = new PerformanceComponent.GOPeriod(nextIdPeriodTmp, stardDateTmp, endDateTmp,
                                                (Enums.AggregationType)etlItem.IdAggregationType, higherAggregationType, lowerAggregationType);
                            if (!gopItemTmp.ImrServerPerformanceDict.ContainsKey(gosiItem.Server.IdImrServer))
                                gopItemTmp.ImrServerPerformanceDict.Add(gosiItem.Server.IdImrServer, new List<OpImrServerPerformance>());
                            gosiItem.ServerPeriodsDict.Add(nextIdPeriodTmp, gopItemTmp);
                        }
                    }
                    //sprawdzenie wyższych aggregation type
                    if (endTimeConvertedToDay >= lastEndTimeConvertedToDay)
                        isLatestPeriod = true; // Mając 7 priodów dniowych, utworzy się 7 tygodniówek, ale my chcemy tylko jedną z ostatniego dnia który aktualnie analizujemy.

                    if (gophItem != null && (int)gophItem.MaxAggregationType > etlItem.IdAggregationType && computingAggregates && isLatestPeriod)
                    {
                        nextIdPeriod = CreateHigherAggregationTypePeriods(etlItem, ReferenceDate, gophItem.MaxAggregationType, gopItem, nextIdPeriod, periodsDict, computingAggregates);
                    }


                    if (!gopItem.EtlPerformanceDict.ContainsKey(etlItem.IdEtl))
                        gopItem.EtlPerformanceDict.Add(etlItem.IdEtl, new List<OpEtlPerformance>());

                    if (!etlPerformancePeriodsDict.ContainsKey(gopItem.IdPeriod))
                        etlPerformancePeriodsDict.Add(gopItem.IdPeriod, new List<OpEtlPerformance>());
                    etlPerformancePeriodsDict[gopItem.IdPeriod].Add(etlItem);

                    cnt++;
                }

                HierarchyItemMissingForEtl = HierarchyItemMissingForEtl.Distinct().ToList();

                foreach (PerformanceComponent.GOImrServer goisItem in serverWorkspace)
                {
                    List<PerformanceComponent.GOPeriod> periods = goisItem.EtlPeriodsDict.Values.ToList();
                    List<PerformanceComponent.GOPeriod> lowerPeriodsTmp = new List<PerformanceComponent.GOPeriod>();

                    #region CreatePeriodHierarchy
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if (periods[i].LowerAggregationType != Enums.AggregationType.Unknown)
                        {
                            if (ComputeHigherAggregationThanDay && periods[i].AggregationType == Enums.AggregationType.Day)
                                continue;

                            List<PerformanceComponent.GOPeriod> lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();
                            #region AddingMissingPeriods

                            if (lowerPeriods != null && lowerPeriods.Count > 0 && computingAggregates)
                            {
                                lowerPeriodsTmp = lowerPeriods.ToList();
                                AddingMissingEtlPeriods(goisItem, periods[i], lowerPeriodsTmp, goisItem.Server.IdImrServer); // Dodawanie brakujacych okresow do EtlPeriodsDict,
                            }

                            #endregion

                            periods = goisItem.EtlPeriodsDict.Values.ToList();
                            lowerPeriods = periods.Where(p => p.AggregationType == periods[i].LowerAggregationType
                                                                            && p.StartTime >= periods[i].StartTime
                                                                            && p.EndTime <= periods[i].EndTime).ToList();

                            if (lowerPeriods != null)
                                periods[i].LowerIdPeriods.AddRange(lowerPeriods.Select(p => p.IdPeriod));
                        }
                        if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            periods[i].LowerIdPeriods = periods[i].LowerIdPeriods.Distinct().ToList();
                    }
                    #endregion

                    #region FillDictDataHigherAggregates
                    for (int i = 0; i < periods.Count; i++)
                    {
                        if ((int)periods[i].AggregationType > (int)Enums.AggregationType.Quarter)
                        {
                            if (periods[i].LowerIdPeriods != null && periods[i].LowerIdPeriods.Count > 0)
                            {
                                for (int j = 0; j < periods[i].LowerIdPeriods.Count; j++)
                                {
                                    int idLowerPeriod = periods[i].LowerIdPeriods[j];
                                    PerformanceComponent.GOPeriod period = periods.Find(p => p.IdPeriod == idLowerPeriod);
                                    foreach (int iItem in period.EtlPerformanceDict.Keys)
                                    {
                                        if (!periods[i].EtlPerformanceDict.ContainsKey(iItem))
                                            periods[i].EtlPerformanceDict.Add(iItem, new List<OpEtlPerformance>());
                                    }
                                }
                            }
                            if (!etlPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                                etlPerformancePeriodsDict.Add(periods[i].IdPeriod, new List<OpEtlPerformance>());
                        }
                    }
                    #endregion

                    if (withValues)
                    {
                        #region FillAggregateValues

                        for (int i = 0; i < periods.Count; i++)
                        {
                            if (etlPerformancePeriodsDict.ContainsKey(periods[i].IdPeriod))
                            {
                                foreach (OpEtlPerformance etlItem in etlPerformancePeriodsDict[periods[i].IdPeriod])
                                {
                                    if (periods[i].EtlPerformanceDict.ContainsKey(etlItem.IdEtl))
                                        periods[i].EtlPerformanceDict[etlItem.IdEtl].Add(etlItem);
                                }
                            }
                        }

                        #endregion
                    }
                }
            }
        }
        #endregion

        #endregion

        #region AddingMissingPeriods

        #region AddingMissingQueuePeriods
        // Dodawanie brakujacych (niższych) okresow do QueuePeriodsDict,

        public static void AddingMissingQueuePeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // end time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // start time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;

            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay)
                {
                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                    {
                        int nextIdPeriod = GoIsItem.QueuePeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.QueuePeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(-GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.QueuePeriodsDict.Add(nextIdPeriod, gopItem);
                    }
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.QueuePeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.QueuePeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.QueuePeriodsDict.Add(nextIdPeriod, gopItem);
                    }

                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #region AddingMissingProcessPeriods
        // Dodawanie brakujacych (niższych) okresow do processPeriodsDict,

        public static void AddingMissingProcessPeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // end time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // start time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;

            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay)
                {
                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                    {
                        int nextIdPeriod = GoIsItem.ProcessPeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.ProcessPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(-GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.ProcessPeriodsDict.Add(nextIdPeriod, gopItem);
                    }
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.ProcessPeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.ProcessPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.ProcessPeriodsDict.Add(nextIdPeriod, gopItem);
                    }

                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #region AddingMissingImrServerPeriods
        // Dodawanie brakujacych (niższych) okresow do ServerPeriodsDict,

        public static void AddingMissingImrServerPeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // start time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // end time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;

            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay)
                {
                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                    {
                        int nextIdPeriod = GoIsItem.ServerPeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.ServerPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(-GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        if (!gopItem.ImrServerPerformanceDict.ContainsKey(IdImrServer))
                            gopItem.ImrServerPerformanceDict.Add(IdImrServer, new List<OpImrServerPerformance>());

                        GoIsItem.ServerPeriodsDict.Add(nextIdPeriod, gopItem);
                    }
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.ServerPeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.ServerPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        if (!gopItem.ImrServerPerformanceDict.ContainsKey(IdImrServer))
                            gopItem.ImrServerPerformanceDict.Add(IdImrServer, new List<OpImrServerPerformance>());

                        GoIsItem.ServerPeriodsDict.Add(nextIdPeriod, gopItem);
                    }
                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #region AddingMissingReportPeriods
        // Dodawanie brakujacych (niższych) okresow do reportPeriodsDict,

        public static void AddingMissingReportPeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // end time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // start time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;

            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay) // Poki co nie uzupełniamy agregatów dniowych o brakujące 15 minutowe.
                {
                    /* if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                     {
                         int nextIdPeriod = GoIsItem.ReportPeriodsDict.Max(m => m.Key) + 1;
                         int nextItemIdPeriod = GoIsItem.ReportPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                         PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(- GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                             PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                         GoIsItem.ReportPeriodsDict.Add(nextIdPeriod, gopItem);
                     }*/
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.ReportPeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.ReportPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.ReportPeriodsDict.Add(nextIdPeriod, gopItem);
                    }

                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                    //}
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #region AddingMissingDistributorPeriods
        // Dodawanie brakujacych (niższych) okresow do reportPeriodsDict,

        public static void AddingMissingDistributorPeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer, int IdDistributor)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // end time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // start time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;
            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay) // Poki co nie uzupełniamy agregatów dniowych o brakujące 15 minutowe.
                {
                    /* if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                     {
                         int nextIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Max(m => m.Key) + 1;
                         int nextItemIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Values.Max(m => m.IdPeriod) + 1;
                         PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(- GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                             PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                         GoIsItem.PeriodsDict[IdDistributor].Add(nextIdPeriod, gopItem);
                     }*/
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.PeriodsDict[IdDistributor].Add(nextIdPeriod, gopItem);
                    }

                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                    //}
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #region AddingMissingDataTemporalPeriods
        // Dodawanie brakujacych (niższych) okresow do ServerPeriodsDict,

        public static void AddingMissingDataTemporalPeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer, int IdDistributor)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // end time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // start time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;

            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay)
                {
                    /*if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                    {
                        int nextIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(- GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        if (!gopItem.ImrServerPerformanceDict.ContainsKey(IdImrServer))
                            gopItem.ImrServerPerformanceDict.Add(IdImrServer, new List<OpImrServerPerformance>());

                        GoIsItem.PeriodsDict[IdDistributor].Add(nextIdPeriod, gopItem);
                    }*/
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.PeriodsDict[IdDistributor].Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.PeriodsDict[IdDistributor].Add(nextIdPeriod, gopItem);
                    }
                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #region AddingMissingTransmissionDriverPeriods
        // Dodawanie brakujacych (niższych) okresow do reportPeriodsDict,

        public static void AddingMissingTransmissionDriverPeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // end time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // start time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;

            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay) // Poki co nie uzupełniamy agregatów dniowych o brakujące 15 minutowe.
                {
                    /* if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                     {
                         int nextIdPeriod = GoIsItem.ReportPeriodsDict.Max(m => m.Key) + 1;
                         int nextItemIdPeriod = GoIsItem.ReportPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                         PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(- GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                             PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                         GoIsItem.ReportPeriodsDict.Add(nextIdPeriod, gopItem);
                     }*/
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.TransmissionDriverPeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.TransmissionDriverPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.TransmissionDriverPeriodsDict.Add(nextIdPeriod, gopItem);
                    }

                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                    //}
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #region AddingMissingEtlPeriods
        // Dodawanie brakujacych (niższych) okresow do etlPeriodsDict,

        public static void AddingMissingEtlPeriods(PerformanceComponent.GOImrServer GoIsItem, PerformanceComponent.GOPeriod PeriodsItem, List<PerformanceComponent.GOPeriod> LowerPeriodsTmp, int IdImrServer)
        {
            DateTime timeTmp = LowerPeriodsTmp.Select(s => s.EndTime).Min(m => m); // end time najstarszego perioda - nie uzupełniamy wcześniejszych brakujących periodów
            DateTime endTimeTmp = new DateTime(); // end time ostatniego perioda
            DateTime newPeriodStartTime = new DateTime(); // start time dodawanego perioda

            endTimeTmp = PeriodsItem.EndTime;
            long whileCounter = 0;

            while (timeTmp < endTimeTmp)
            {
                if (!ComputeHigherAggregationThanDay) // Poki co nie uzupełniamy agregatów dniowych o brakujące 15 minutowe.
                {
                    /* if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp))
                     {
                         int nextIdPeriod = GoIsItem.EtlPeriodsDict.Max(m => m.Key) + 1;
                         int nextItemIdPeriod = GoIsItem.EtlPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                         PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, timeTmp.AddMinutes(- GetAggregationTime(PeriodsItem.LowerAggregationType)), timeTmp,
                             PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                         GoIsItem.EtlPeriodsDict.Add(nextIdPeriod, gopItem);
                     }*/
                    timeTmp = timeTmp.AddMinutes(GetAggregationTime(PeriodsItem.LowerAggregationType));
                }
                else
                {
                    if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Day)
                        newPeriodStartTime = timeTmp.AddDays(-GetAggregationTime(PeriodsItem.LowerAggregationType));
                    else if (PeriodsItem.LowerAggregationType == Enums.AggregationType.Month)
                        newPeriodStartTime = timeTmp.AddMonths(-GetAggregationTime(PeriodsItem.LowerAggregationType));

                    if (!LowerPeriodsTmp.Exists(e => e.EndTime == timeTmp && e.StartTime == newPeriodStartTime))
                    {
                        int nextIdPeriod = GoIsItem.EtlPeriodsDict.Max(m => m.Key) + 1;
                        int nextItemIdPeriod = GoIsItem.EtlPeriodsDict.Values.Max(m => m.IdPeriod) + 1;
                        PerformanceComponent.GOPeriod gopItem = new PerformanceComponent.GOPeriod(nextItemIdPeriod, newPeriodStartTime, timeTmp,
                            PeriodsItem.LowerAggregationType, PeriodsItem.AggregationType, GetLowerAggregationType((int)PeriodsItem.LowerAggregationType));

                        GoIsItem.EtlPeriodsDict.Add(nextIdPeriod, gopItem);
                    }

                    timeTmp = timeTmp.AddDays(GetAggregationTime(PeriodsItem.LowerAggregationType));
                    //}
                }
                whileCounter++;
                if (whileCounter > MaximumNumberOfRevolutionsWhileLoop)
                    throw new System.TimeoutException("exceeded maximum number of loop revolutions");
            }
        }
        #endregion

        #endregion

        #region GetAggregationTime
        // Czas w zależności od AggregationType
        public static int GetAggregationTime(Enums.AggregationType AggregationType)
        {
            int time = 0;
            switch (AggregationType)
            {
                case Enums.AggregationType.Quarter:
                    time = 15; // minut
                    break;
                case Enums.AggregationType.Day:
                    time = 1; // dni
                    break;
                case Enums.AggregationType.Month:
                    time = 1; // miesięcy
                    break;
            }
            return time;
        }
        #endregion

        #region GetLowerAggregationtype
        // Pobieranie loweraggregationtype w zaleznosci od AggregationType
        public static Enums.AggregationType GetLowerAggregationType(int AggregationType)
        {
            Enums.AggregationType aggregationType = Enums.AggregationType.Unknown;
            switch (AggregationType)
            {
                case (int)Enums.AggregationType.Quarter:
                    aggregationType = Enums.AggregationType.Unknown;
                    break;
                case (int)Enums.AggregationType.Day:
                    aggregationType = Enums.AggregationType.Quarter;
                    break;
                case (int)Enums.AggregationType.Week:
                    aggregationType = Enums.AggregationType.Day;
                    break;
                case (int)Enums.AggregationType.Month:
                    aggregationType = Enums.AggregationType.Day; // Ponieważ Agregaty miesięczne wyliczane będą z dniówek
                    break;
                case (int)Enums.AggregationType.Year:
                    aggregationType = Enums.AggregationType.Month;
                    break;
            }
            return aggregationType;
        }
        #endregion

        #region GetHigherAggregationType
        // Pobieranie higheraggregationtype w zaleznosci od AggregationType
        public static Enums.AggregationType GetHigherAggregationType(int AggregationType)
        {
            Enums.AggregationType aggregationType = Enums.AggregationType.Unknown;
            switch (AggregationType)
            {
                case (int)Enums.AggregationType.Unknown:
                    aggregationType = Enums.AggregationType.Quarter;
                    break;
                case (int)Enums.AggregationType.Quarter:
                    aggregationType = Enums.AggregationType.Day;
                    break;
                case (int)Enums.AggregationType.Day:
                    aggregationType = Enums.AggregationType.Week;
                    break;
                case (int)Enums.AggregationType.Week:
                    aggregationType = Enums.AggregationType.Month;
                    break;
                case (int)Enums.AggregationType.Month:
                    aggregationType = Enums.AggregationType.Year;
                    break;
                case (int)Enums.AggregationType.Year:
                    aggregationType = Enums.AggregationType.Unknown;
                    break;
            }
            return aggregationType;
        }
        #endregion

        #region ComputePerformanceValue

        private static List<long> QuarterBooleanDataTypes = new List<long>()
        {
            DataType.MON_FP_AGGR_PC, DataType.MON_FP_AGGR_SERVICE, DataType.MON_FP_AGGR_TANKS, DataType.MON_FP_AGGR_SALES,
            DataType.MON_AWSR_RADIO_FRAMES, DataType.MON_AWSR_RADIO_AMPLIFIER, DataType.MON_AWSR_ATG, DataType.MON_AWSR_LON, DataType.MON_AWSR_PUMP_CONVERTER, DataType.MON_AWSR_IO_CONTROLLER,
            DataType.MON_IS_WORKING, DataType.MON_PERC_CPU, DataType.MON_BYTE_RAM, DataType.MON_DISK, DataType.MON_NETWORK
        };


        public static double ComputePerformanceValue(IMR.Suite.Data.DB.DB_DATA_TEMPORAL dataTemporal, OpDataType dataType)
        {
            double retValue = 0.0;
            if (dataTemporal.VALUE != null && !String.IsNullOrEmpty(dataTemporal.VALUE.ToString()))
            {
                if (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Boolean
                    || (dataTemporal.ID_AGGREGATION_TYPE == (int)Enums.AggregationType.Quarter && dataTemporal.ID_DATA_TYPE.In(QuarterBooleanDataTypes.ToArray())))
                {
                    retValue = Convert.ToDouble(dataTemporal.VALUE) * 100.0;
                    if (retValue > 100)
                    {
                    }
                }
                else
                    retValue = Convert.ToDouble(dataTemporal.VALUE);
            }
            return retValue;
        }
        public static double ComputePerformanceValue(OpDataTemporal dataTemporal, OpDataType dataType)
        {
            return ComputePerformanceValue((IMR.Suite.Data.DB.DB_DATA_TEMPORAL)dataTemporal, dataType);
            /*
            double retValue = 0.0;
            if (dataTemporal.Value != null && !String.IsNullOrEmpty(dataTemporal.Value.ToString()))
            {
                if (dataType.IdDataTypeClass == (int)Enums.DataTypeClass.Boolean
                    || (dataTemporal.IdAggregationType == (int)Enums.AggregationType.Quarter && dataTemporal.IdDataType.In(QuarterBooleanDataTypes.ToArray())))
                {
                    retValue = Convert.ToDouble(dataTemporal.Value) * 100.0;
                    if (retValue > 100)
                    {
                    }
                }
                else
                    retValue = Convert.ToDouble(dataTemporal.Value);
            }
            return retValue;*/
        }

        #endregion

        #region FilterDuplicateValues

        public static List<DB_DATA_TEMPORAL> FilterDuplicateValues(List<DB_DATA_TEMPORAL> dtList)
        {
            List<DB_DATA_TEMPORAL> dtRetList = new List<DB_DATA_TEMPORAL>();

            Dictionary<long, List<DB_DATA_TEMPORAL>> LocationDataTempDict = new Dictionary<long, List<DB_DATA_TEMPORAL>>();
            Dictionary<long, List<DB_DATA_TEMPORAL>> DeviceDataTempDict = new Dictionary<long, List<DB_DATA_TEMPORAL>>();
            Dictionary<long, List<DB_DATA_TEMPORAL>> MeterDataTempDict = new Dictionary<long, List<DB_DATA_TEMPORAL>>();

            DateTime dt1 = DateTime.Now;
            dtList.Where(d => d.ID_LOCATION.HasValue).Select(d => d.ID_LOCATION.Value).Distinct().ToList().ForEach(d => LocationDataTempDict.Add(d, new List<DB_DATA_TEMPORAL>()));
            dtList.Where(d => d.SERIAL_NBR.HasValue).Select(d => d.SERIAL_NBR.Value).Distinct().ToList().ForEach(d => DeviceDataTempDict.Add(d, new List<DB_DATA_TEMPORAL>()));
            dtList.Where(d => d.ID_METER.HasValue).Select(d => d.ID_METER.Value).Distinct().ToList().ForEach(d => MeterDataTempDict.Add(d, new List<DB_DATA_TEMPORAL>()));

            TimeSpan ts1 = DateTime.Now - dt1;
            DateTime dt2 = DateTime.Now;

            for (int i = 0; i < dtList.Count; i++)
            {
                if (dtList[i].ID_LOCATION.HasValue && !dtList[i].ID_METER.HasValue)
                {
                    LocationDataTempDict[dtList[i].ID_LOCATION.Value].Add(dtList[i]);
                }
                else if (dtList[i].SERIAL_NBR.HasValue && !dtList[i].ID_METER.HasValue)
                {
                    DeviceDataTempDict[dtList[i].SERIAL_NBR.Value].Add(dtList[i]);
                }
                else if (dtList[i].ID_METER.HasValue)
                {
                    MeterDataTempDict[dtList[i].ID_METER.Value].Add(dtList[i]);
                }
            }

            TimeSpan ts2 = DateTime.Now - dt2;
            DateTime dt3 = DateTime.Now;

            #region Location

            List<long> idLocationList = LocationDataTempDict.Keys.ToList();
            foreach (long lItem in idLocationList)
            {
                if (LocationDataTempDict.ContainsKey(lItem) && LocationDataTempDict[lItem].Count > 0)
                {
                    List<DB_DATA_TEMPORAL> dtTmpList = new List<DB_DATA_TEMPORAL>(LocationDataTempDict[lItem]);

                    foreach (DB_DATA_TEMPORAL dtItem in dtTmpList)
                    {
                        List<DB_DATA_TEMPORAL> dtDuplicateList = LocationDataTempDict[lItem].FindAll(d => d.ID_LOCATION == dtItem.ID_LOCATION
                                                                                                        && d.ID_METER == dtItem.ID_METER
                                                                                                        && d.SERIAL_NBR == dtItem.SERIAL_NBR
                                                                                                        && d.INDEX_NBR == dtItem.INDEX_NBR
                                                                                                        && d.ID_DATA_TYPE == dtItem.ID_DATA_TYPE
                                                                                                        && d.ID_AGGREGATION_TYPE == dtItem.ID_AGGREGATION_TYPE
                                                                                                        && d.START_TIME == dtItem.START_TIME
                                                                                                        && d.END_TIME == dtItem.END_TIME);
                        if (dtDuplicateList.Count > 1)
                        {
                            long maxId = dtDuplicateList.Max(d => d.ID_DATA_TEMPORAL);
                            LocationDataTempDict[lItem].RemoveAll(d => d.ID_DATA_TEMPORAL != maxId);
                        }
                    }
                }
            }

            #endregion
            #region Device

            List<long> deviceList = DeviceDataTempDict.Keys.ToList();
            foreach (long lItem in deviceList)
            {
                if (DeviceDataTempDict.ContainsKey(lItem) && DeviceDataTempDict[lItem].Count > 0)
                {
                    List<DB_DATA_TEMPORAL> dtTmpList = new List<DB_DATA_TEMPORAL>(DeviceDataTempDict[lItem]);
                    if (dtTmpList != null && dtTmpList.Count() > 0)
                    {
                        foreach (DB_DATA_TEMPORAL dtItem in dtTmpList)
                        {
                            List<DB_DATA_TEMPORAL> dtDuplicateList = DeviceDataTempDict[lItem].FindAll(d => d.ID_LOCATION == dtItem.ID_LOCATION
                                                                                                         && d.ID_METER == dtItem.ID_METER
                                                                                                         && d.SERIAL_NBR == dtItem.SERIAL_NBR
                                                                                                         && d.INDEX_NBR == dtItem.INDEX_NBR
                                                                                                         && d.ID_DATA_TYPE == dtItem.ID_DATA_TYPE
                                                                                                         && d.ID_AGGREGATION_TYPE == dtItem.ID_AGGREGATION_TYPE
                                                                                                         && d.START_TIME == dtItem.START_TIME
                                                                                                         && d.END_TIME == dtItem.END_TIME);
                            if (dtDuplicateList.Count > 1)
                            {
                                long maxId = dtDuplicateList.Max(d => d.ID_DATA_TEMPORAL);
                                DeviceDataTempDict[lItem].RemoveAll(d => d.ID_DATA_TEMPORAL != maxId);
                            }
                        }
                    }
                }
            }

            #endregion
            #region Meter

            List<long> idMeterList = MeterDataTempDict.Keys.ToList();
            foreach (long lItem in idMeterList)
            {
                if (MeterDataTempDict.ContainsKey(lItem) && MeterDataTempDict[lItem].Count > 0)
                {
                    List<DB_DATA_TEMPORAL> dtTmpList = new List<DB_DATA_TEMPORAL>(MeterDataTempDict[lItem]);

                    if (dtTmpList != null && dtTmpList.Count() > 0)
                    {
                        foreach (DB_DATA_TEMPORAL dtItem in dtTmpList)
                        {
                            List<DB_DATA_TEMPORAL> dtDuplicateList = MeterDataTempDict[lItem].FindAll(d => d.ID_LOCATION == dtItem.ID_LOCATION
                                                                                                         && d.ID_METER == dtItem.ID_METER
                                                                                                         && d.SERIAL_NBR == dtItem.SERIAL_NBR
                                                                                                         && d.INDEX_NBR == dtItem.INDEX_NBR
                                                                                                         && d.ID_DATA_TYPE == dtItem.ID_DATA_TYPE
                                                                                                         && d.ID_AGGREGATION_TYPE == dtItem.ID_AGGREGATION_TYPE
                                                                                                         && d.START_TIME == dtItem.START_TIME
                                                                                                         && d.END_TIME == dtItem.END_TIME);
                            if (dtDuplicateList.Count > 1)
                            {
                                long maxId = dtDuplicateList.Max(d => d.ID_DATA_TEMPORAL);
                                MeterDataTempDict[lItem].RemoveAll(d => d.ID_DATA_TEMPORAL != maxId);
                            }
                        }
                    }
                }
            }

            #endregion

            foreach (List<DB_DATA_TEMPORAL> dtListItem in LocationDataTempDict.Values)
                dtRetList.AddRange(dtListItem);
            foreach (List<DB_DATA_TEMPORAL> dtListItem in MeterDataTempDict.Values)
                dtRetList.AddRange(dtListItem);
            foreach (List<DB_DATA_TEMPORAL> dtListItem in DeviceDataTempDict.Values)
                dtRetList.AddRange(dtListItem);

            TimeSpan ts3 = DateTime.Now - dt3;
            return dtRetList;
        }

        #endregion

        #region ComputeLastFullDay

        public static DateTime ComputeLastFullDay(DateTime referenceDate, bool rounded, int daysPastToCompute)
        {
            DateTime lastFullDay = new DateTime(referenceDate.AddDays(-daysPastToCompute).Year,
                                                referenceDate.AddDays(-daysPastToCompute).Month,
                                                referenceDate.AddDays(-daysPastToCompute).Day).AddDays(1);
            if (!rounded)
                lastFullDay = lastFullDay.AddSeconds(-450);//odejmujemy 7 i pół minuty, bo te czasy jeszcze zostaną zaokrąglone do pełnego dnia

            return lastFullDay;
        }

        #endregion

        #region GetDeviceAdditionalConstraints
        public static Enums.DataStatus GetDeviceTypeAdditionalConstraints(PerformanceComponent.GOPerformanceHierarchyItem GophiItem, OpDevice Device, DateTime EndTime, List<OpDeviceStateHistory> DeviceStateHistoryList = null)
        {
            Enums.DataStatus dsItem = Enums.DataStatus.Normal;
            if (DeviceStateHistoryList == null)
                DeviceStateHistoryList = new List<OpDeviceStateHistory>();

            if (GophiItem.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceStateType) || GophiItem.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceType))
            {
                OpDevice devItem = Device;
                int currentIdDeviceStateType = devItem.IdDeviceStateType;

                List<OpDeviceStateHistory> dshList = DeviceStateHistoryList;

                if (dshList.Count > 0)
                    dshList = dshList.OrderBy(l => l.StartTime).ToList();
                foreach (OpDeviceStateHistory dshItem in dshList)
                {
                    if (EndTime >= dshItem.StartTime && (!dshItem.EndTime.HasValue || EndTime < dshItem.EndTime.Value))
                    {
                        currentIdDeviceStateType = dshItem.IdDeviceStateType;
                        break;
                    }
                }

                if (GophiItem.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceStateType))
                {
                    List<OpDeviceStateType> dstList = GophiItem.AdditionalConstraints[Enums.ReferenceType.IdDeviceStateType] as List<OpDeviceStateType>;
                    if (!dstList.Exists(l => l.IdDeviceStateType == currentIdDeviceStateType))
                        dsItem = Enums.DataStatus.Unknown;
                }

                if (GophiItem.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceType))
                {
                    List<OpDeviceType> dstList = GophiItem.AdditionalConstraints[Enums.ReferenceType.IdDeviceType] as List<OpDeviceType>;
                    if (!dstList.Exists(d => d.IdDeviceType == devItem.IdDeviceType))
                        dsItem = Enums.DataStatus.Unknown;
                }

            }
            //gopvItem.DataStatus = dsItem;

            return dsItem;
        }
        #endregion

        #region GetDeviceStateTypeAdditionalContraints

        public static List<int> GetDeviceStateTypeAdditionalContraints(PerformanceComponent.GOPerformanceHierarchyItem GophiItem)
        {

            List<int> additionalStateType = new List<int>();
            if (GophiItem.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceStateType))
                additionalStateType = ((List<OpDeviceStateType>)GophiItem.AdditionalConstraints[Enums.ReferenceType.IdDeviceStateType]).Select(d => d.IdDeviceStateType).ToList();
            if (additionalStateType.Count == 0)
                additionalStateType.Add((int)Enums.DeviceState.ACTIVE);

            return additionalStateType;
        }

        #endregion

        #region GetDeviceTypeAdditionalConstraints

        public static List<int> GetDeviceTypeAdditionalConstraints(PerformanceComponent.GOPerformanceHierarchyItem GophiItem)
        {
            List<int> additionalType = new List<int>();
            if (GophiItem.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceType))
                additionalType = ((List<OpDeviceType>)GophiItem.AdditionalConstraints[Enums.ReferenceType.IdDeviceType]).Select(d => d.IdDeviceType).ToList();
            return additionalType;
        }

        #endregion

        #region GetDeviceTypeAdditionalConstraints

        public static List<long> GetSerialNbrAdditionalConstraints(PerformanceComponent.GOPerformanceHierarchyItem GophiItem)
        {
            List<long> additionalType = new List<long>();
            if (GophiItem.AdditionalConstraints.ContainsKey(Enums.ReferenceType.SerialNbr))
                additionalType = ((List<OpDevice>)GophiItem.AdditionalConstraints[Enums.ReferenceType.SerialNbr]).Select(d => d.SerialNbr).ToList();
            return additionalType;
        }

        #endregion

        #region GetObservableReports

        public static List<OpReport> GetObservableReports(DataProvider DataProvider, int IdImrServer)
        {
            List<OpReport> retList = new List<OpReport>();

            List<OpReportData> rdList = DataProvider.GetReportDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                         customWhereClause: String.Format("[ID_DATA_TYPE] = {0} AND [VALUE] is not null AND cast([VALUE] as int) = {1}", DataType.REPORT_IMR_SERVER_ID, IdImrServer));
            if (rdList.Count > 0)
            {
                List<int> idReportList = rdList.Select(r => r.IdReport).Distinct().ToList();
                rdList = DataProvider.GetReportDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                          IdDataType: new long[]{ DataType.REPORT_DATA_GENERATION_CHECK,
                                                                                  DataType.REPORT_EMAIL_SENT_CHECK,
                                                                                  DataType.REPORT_FTP_SENT_CHECK },
                                                          customWhereClause: "[VALUE] is not null and cast([VALUE] as bit) = 1");
                idReportList = rdList.Select(r => r.IdReport).Distinct().ToList();
                if (idReportList.Count > 0)
                {
                    List<long> customReportDataType = new List<long>();
                    customReportDataType.Add(DataType.REPORT_NAME);
                    customReportDataType.Add(DataType.REPORT_STORED_PROCEDURE);
                    customReportDataType.Add(DataType.REPORT_DATA_GENERATION_CHECK);
                    customReportDataType.Add(DataType.REPORT_EMAIL_SENT_CHECK);
                    customReportDataType.Add(DataType.REPORT_FTP_SENT_CHECK);
                    retList = DataProvider.GetReportFilter(IdReport: idReportList.ToArray(), customDataTypes: customReportDataType, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted);
                }
            }

            return retList;

        }

        #endregion

        #region ConvertPerformanceValueToDouble

        public static double ConvertPerformanceValueToDouble(object toDouble, int decimalPlaces = 2)
        {
            return ConvertValueToDouble(toDouble, decimalPlaces: decimalPlaces);
            //double toRet = 0.0;
            //if (toDouble != null && !String.IsNullOrEmpty(toDouble.ToString().Trim()))
            //{
            //    toRet = Math.Round(Convert.ToDouble(toDouble.ToString().Trim().Replace(",", "."), new System.Globalization.CultureInfo("en-US")), decimalPlaces);
            //}
            //return toRet;
        }

        public static double ConvertValueToDouble(object toDouble, int? decimalPlaces = null)
        {
            double toRet = 0.0;
            if (toDouble != null && !String.IsNullOrEmpty(toDouble.ToString().Trim()))
            {
                if (decimalPlaces.HasValue)
                    toRet = Math.Round(Convert.ToDouble(toDouble.ToString().Trim().Replace(",", "."), new System.Globalization.CultureInfo("en-US")), decimalPlaces.Value);
                else
                    toRet = Convert.ToDouble(toDouble.ToString().Trim().Replace(",", "."), new System.Globalization.CultureInfo("en-US"));
            }
            return toRet;
        }

        #endregion

        #region GetDaysAmount

        public static int GetDaysAmount(Enums.ReportScheduleUnit unit, int value)
        {
            switch (unit)
            {
                case Enums.ReportScheduleUnit.Day:
                    return value;
                case Enums.ReportScheduleUnit.Week:
                    return value * 7;
                case Enums.ReportScheduleUnit.Month:
                    return value * 30;
                case Enums.ReportScheduleUnit.Hour:
                    return (int)Math.Ceiling((double)value / 24.0);
                case Enums.ReportScheduleUnit.Minute:
                    return (int)Math.Ceiling((double)value / (24.0 * 60.0));
                default:
                    return value;
            }

        }

        public static int GetMinutesAmount(Enums.ReportScheduleUnit unit, int value)
        {
            switch (unit)
            {
                case Enums.ReportScheduleUnit.Month:
                    return value = 30 * 24 * 60;
                case Enums.ReportScheduleUnit.Week:
                    return value * 7 * 24 * 60;
                case Enums.ReportScheduleUnit.Day:
                    return value * 24 * 60;
                case Enums.ReportScheduleUnit.Hour:
                    return value * 60;
                case Enums.ReportScheduleUnit.Minute:
                    return value;
                default:
                    return value;
            }

        }

        #endregion

        #region CheckChangeTime

        //public static bool CheckChangeTime(DateTime startDate, DateTime endDate) // Sprawdzamy czy okres, który chcemy przeliczyć jest w innym czasie (letni - zimowy) niż ReferenceDate - wtedy występuje problem z konwersja na UTC
        //{
        //    bool result = false;
        //    // TRUE = czas letni, FALSE = zimowy
        //    bool referenceDateDaylightSavingTime = false;
        //    bool startDateDaylightSavingTime = false;
        //    bool endDateDaylightSavingTime = false;

        //    if (DaylightSavingTimeList.Exists(e => e.StartTime <= ReferenceDate && e.EndTime >= ReferenceDate))
        //        referenceDateDaylightSavingTime = true;
        //    if (DaylightSavingTimeList.Exists(e => e.StartTime <= startDate && e.EndTime >= startDate))
        //        startDateDaylightSavingTime = true;
        //    if (DaylightSavingTimeList.Exists(e => e.StartTime <= endDate && e.EndTime >= endDate))
        //        endDateDaylightSavingTime = true;

        //    if (referenceDateDaylightSavingTime != startDateDaylightSavingTime || referenceDateDaylightSavingTime != endDateDaylightSavingTime)
        //        result = true;

        //    return result;
        //}

        #endregion

        #region ImproveTimeDependingOnDaylightSavingTime

        public static DateTime ImproveDateTimeDependingOnDaylightSavingTime(DateTime referenceDate, DateTime dtTime)
        {
            bool referenceDateDaylightSavingTime = false; // czas odpalenia => TRUE = czas letni, FALSE = zimowy
            bool dtTimeDaylightSavingTime = false; // data do konwersji TRUE = czas letni, FALSE = zimowy

            // Jesli występują w tym samym czasie to nic nie zmieniamy!

            if (DaylightSavingTimeList.Exists(e => e.StartTime <= referenceDate && e.EndTime >= referenceDate))
                referenceDateDaylightSavingTime = true;
            if (DaylightSavingTimeList.Exists(e => e.StartTime <= dtTime && e.EndTime >= dtTime))
                dtTimeDaylightSavingTime = true;

            if (referenceDateDaylightSavingTime != dtTimeDaylightSavingTime && referenceDateDaylightSavingTime == true) // Odpalamy się z letnim, a przeliczamy jeszcze z zimowego
                dtTime = dtTime.AddHours(1);
            if (referenceDateDaylightSavingTime != dtTimeDaylightSavingTime && referenceDateDaylightSavingTime == false) // Odpalamy się z zimowym, a przeliczamy jeszcze z letniego
                dtTime = dtTime.AddHours(-1);

            return dtTime;
        }
        #endregion

        #region GetAggregationTypeString

        public static string GetAggregationTypeString(Enums.AggregationType atItem)
        {
            string retString = "";

            if (atItem == Enums.AggregationType.Quarter)
                retString = ResourcesText.Quarter;
            else if (atItem == Enums.AggregationType.Hour)
                retString = ResourcesText.Hour;
            else if (atItem == Enums.AggregationType.SixHours)
                retString = ResourcesText.SixHours;
            else if (atItem == Enums.AggregationType.Day)
                retString = ResourcesText.Day;
            else if (atItem == Enums.AggregationType.Week)
                retString = ResourcesText.Week;
            else if (atItem == Enums.AggregationType.HalfAnHour)
                retString = ResourcesText.HalfAnHour;
            else if (atItem == Enums.AggregationType.Month)
                retString = ResourcesText.Month;
            else if (atItem == Enums.AggregationType.Year)
                retString = ResourcesText.Year;

            return retString;
        }

        #endregion

        #region GetAllEtl
        public static List<GOETL> GetAllEtl(DataProvider dataProvider, int? idImrServer = null)
        {
            List<GOETL> etlList = new List<GOETL>();
            List<long> idEtlCore = new List<long>();
            List<long> idEtlDW = new List<long>();
            if (dataProvider != null)
            {
                idEtlCore = dataProvider.GetMappingFilter(IdReferenceType: new int[] { (int)Enums.ReferenceType.IdEtl },
                                              IdImrServer: idImrServer.HasValue ? new int[] { idImrServer.Value } : null,
                                              loadNavigationProperties: false,
                                              transactionLevel: IsolationLevel.ReadUncommitted,
                                              autoTransaction: false).Select(s => s.IdDestination).ToList();
                idEtlDW = dataProvider.GetMappingFilterDW(IdReferenceType: new int[] { (int)Enums.ReferenceType.IdEtl },
                                              IdImrServer: idImrServer.HasValue ? new int[] { idImrServer.Value } : null,
                                              loadNavigationProperties: false,
                                              transactionLevel: IsolationLevel.ReadUncommitted,
                                              autoTransaction: false).Select(s => s.IdDestination).Distinct().ToList();
                idEtlCore = idEtlCore.Distinct().ToList();
                idEtlDW = idEtlDW.Distinct().ToList();

                dataProvider.GetEtlFilter(IdParam: new List<int>(idEtlCore.Cast<int>()).ToArray(),
                                                           loadNavigationProperties: false,
                                                           transactionLevel: IsolationLevel.ReadUncommitted,
                                                           autoTransaction: false).ForEach(d => etlList.Add(new GOETL() { coreETL = d }));
                dataProvider.GetEtlFilterDW(IdParam: new List<int>(idEtlCore.Cast<int>()).ToArray(),
                                                           loadNavigationProperties: false,
                                                           transactionLevel: IsolationLevel.ReadUncommitted,
                                                           autoTransaction: false).ForEach(d => etlList.Add(new GOETL() { dwETL = d }));
                etlList = etlList.Distinct(d => d.IdParam).ToList();
            }

            return etlList;
        }
        #endregion

        #region GetImrServerBasedOnTransmissionDriver

        public static OpImrServer GetImrServerBasedOnTransmissionDriver(OpImrServer imrServerDW, List<GOImrServer> serverWorkspace)
        {
            /*
            * transmission drivery pobierane są z DW, dlatego w przypadku architektury 2-serwerowej collector w tabelce trnamission_driver pod datatypem z id_servera będzie miał serwer DW
            * Uniemożliwi nam to poprawne analizowanie performancow wykonfigurowanych w hierarchii na CORE. 
            * Dlatego najpierw sprawdzamy czy w serwerWorkspace istnieje DW i czy na nim jest konfiguracja wskaźnika, 
            * jeśli jej nie ma wyciągamy CORE i sprawdzamy czy jest konfiguracja wskaźnika.
            * w przypadku jeśli nie ma DW w workspace wyciągamy CORE
            * 
            * */

            OpImrServer currentImrServer = null;

            if (!serverWorkspace.Exists(s => s.Server.IdImrServer == imrServerDW.IdImrServer))
            {
                // W servreWorkspace nie mamy idServera z DW więc konfiguracja będzie na CORE      
                OpImrServer core = serverWorkspace.Where(w => w.Server.DataList.Exists(d => d.IdDataType == DataType.IMR_SERVER_DW_SERVER_ID && Convert.ToInt32(d.Value) == imrServerDW.IdImrServer)).Select(s => s.Server).FirstOrDefault();
                if (core == null || !serverWorkspace.Exists(s => s.Server.IdImrServer == core.IdImrServer))
                    return null;
                currentImrServer = serverWorkspace.Find(s => s.Server.IdImrServer == core.IdImrServer).Server;
            }
            else
            {
                currentImrServer = imrServerDW;
                // Jesli istnieje serwer DW musimy sprawdzić czy na nim został wykonfigurowany Transmission_Driver_Performance, jeśli nie wyciągamy CORE
                if (!serverWorkspace.Find(s => s.Server.IdImrServer == currentImrServer.IdImrServer).ServerPerformanceHierarchy.Values
                                                                                                              .ToList()
                                                                                                              .Exists(d => Convert.ToInt64(d.DataType.Value) == DataType.PERFORMANCE_TRANSMISSION_DRIVER_HEALTHY))
                {
                    OpImrServer core = serverWorkspace.Where(w => w.Server.DataList.Exists(d => d.IdDataType == DataType.IMR_SERVER_DW_SERVER_ID && Convert.ToInt32(d.Value) == imrServerDW.IdImrServer)).Select(s => s.Server).FirstOrDefault();
                    if (core == null || !serverWorkspace.Exists(s => s.Server.IdImrServer == core.IdImrServer))
                        return null;
                    currentImrServer = serverWorkspace.Find(s => s.Server.IdImrServer == core.IdImrServer).Server;
                }

            }

            return currentImrServer;
        }

        #endregion

        #region GetIdTransmissionDriverByDistributor

        public static List<int> GetIdTransmissionDriverByDistributor(int idDistributor, DataProvider dataProvider)
        {
            List<int> idTransmissionDriver = new List<int>();
            OpDistributor distributor = dataProvider.GetDistributor(idDistributor);

            if (distributor.IdIMRServer.HasValue)
            {
                OpImrServer imrServer = dataProvider.GetImrServer(distributor.IdIMRServer.Value); // konf moze byc na core lub na dw ale transmission drivery zawsze pochodza z dw. 
                if (imrServer.DataList.Exists(e => e.IdDataType == DataType.IMR_SERVER_DW_SERVER_ID)) // jesli core, wyciagamy serwer dw.
                    imrServer = dataProvider.GetImrServer(Convert.ToInt32(imrServer.DataList.FirstOrDefault(f => f.IdDataType == DataType.IMR_SERVER_DW_SERVER_ID).Value));

                if (imrServer != null)
                {
                    string customWhereClause = " [ID_DATA_TYPE] = " + DataType.TRANSMISSION_DRIVER_IMR_SERVER_ID + " and cast([VALUE] as int) = " + imrServer.IdImrServer;
                    List<DB_TRANSMISSION_DRIVER_DATA> tddList = dataProvider.dbConnectionDw.GetTransmissionDriverDataFilter(IdTransmissionDriverData: null,
                                                                                                                                    IdTransmissionDriver: null,
                                                                                                                                    IdDataType: null,
                                                                                                                                    IndexNbr: null,
                                                                                                                                    topCount: null,
                                                                                                                                    customWhereClause: customWhereClause,
                                                                                                                                    autoTransaction: false,
                                                                                                                                    transactionLevel: IsolationLevel.ReadUncommitted).ToList();

                    if (tddList != null && tddList.Count > 0)
                        idTransmissionDriver = tddList.Select(s => s.ID_TRANSMISSION_DRIVER).Distinct().ToList();
                }
            }

            return idTransmissionDriver;
        }

        #endregion

        #region CreateIssue

        public static OpIssue CreateIssue(DataProvider dataProvider, OpOperator issueNotifier, OpDistributor distributor, OpIssueType.Enum issueType, string note, object[] referenceValue, bool autoGenerated = false)
        {
            try
            {
                if (issueNotifier == null)
                    return null;

                OpIssue newIssue = new OpIssue();
                newIssue.IssueType = dataProvider.GetIssueType((int)issueType);
                newIssue.IssueStatus = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.NEW);
                newIssue.Distributor = distributor;
                newIssue.OperatorRegistering = issueNotifier;
                newIssue.Actor = issueNotifier.Actor;
                newIssue.CreationDate = dataProvider.DateTimeNow;
                newIssue.ShortDescr = note == null ? "" : note;
                newIssue.Priority = dataProvider.GetAllPriority().First();
                if (autoGenerated)
                    newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_AUTO_GENERATED, Index = 0, Value = 1, OpState = UI.Business.Objects.OpChangeState.New });
                int? timeToProceed = null;

                if (issueType == OpIssueType.Enum.HARDWARE)
                {
                    #region Hardware

                    if (referenceValue != null)
                    {
                        if (referenceValue.Count() > 0 && referenceValue[0] is long)
                            newIssue.IdLocation = Convert.ToInt64(referenceValue[0]);
                        if (referenceValue.Count() > 1 && referenceValue[1] is Tuple<Enums.ReferenceType, List<long>>)
                        {
                            Tuple<Enums.ReferenceType, List<long>> refVal = referenceValue[1] as Tuple<Enums.ReferenceType, List<long>>;
                            int index = 0;
                            if (refVal.Item1 == Enums.ReferenceType.SerialNbr && refVal.Item2 != null && refVal.Item2.Count > 0)
                            {
                                foreach (long lItem in refVal.Item2)
                                {
                                    if (!newIssue.DataList.Exists(d => d.IdDataType == DataType.ISSUE_DEVICE_SERIAL_NBR && d.Index == index && Convert.ToInt64(d.Value) == lItem))
                                    {
                                        newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_DEVICE_SERIAL_NBR, Index = index, Value = lItem, OpState = UI.Business.Objects.OpChangeState.New });
                                        index++;
                                    }
                                }
                            }
                        }
                        if (referenceValue.Count() > 2 && referenceValue[2] is List<GOPerformanceValue>)
                        {
                            List<GOPerformanceValue> dtList = referenceValue[2] as List<GOPerformanceValue>;
                            long? rvItem = null;
                            foreach (GOPerformanceValue dtItem in dtList)
                            {
                                if (dtItem.IdLocation.HasValue)
                                    newIssue.IdLocation = dtItem.IdLocation;
                                Enums.ReferenceType rtItem = Enums.ReferenceType.None;
                                rvItem = null;
                                if (dtItem.IdLocation.HasValue && !dtItem.IdMeter.HasValue)
                                {
                                    rtItem = Enums.ReferenceType.IdLocation;
                                    rvItem = dtItem.IdLocation.Value;
                                }
                                if (dtItem.SerialNbr.HasValue && !dtItem.IdMeter.HasValue)
                                {
                                    rtItem = Enums.ReferenceType.SerialNbr;
                                    rvItem = dtItem.SerialNbr.Value;
                                }
                                if (dtItem.IdMeter.HasValue)
                                {
                                    rtItem = Enums.ReferenceType.IdMeter;
                                    rvItem = dtItem.IdMeter.Value;
                                }
                                int index = newIssue.DataList.GetNextIndex(DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE);
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE, Index = index, Value = dtItem.IdDataType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_REFERENCE_TYPE, Index = index, Value = (int)rtItem, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_REFERENCE_VALUE, Index = index, Value = rvItem.Value, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_START_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(dtItem.StartTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_END_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(dtItem.EndTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_AGGREGATION_TYPE, Index = index, Value = (int)dtItem.AggregationType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_VALUE, Index = index, Value = dtItem.Value, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_INDEX, Index = index, Value = dtItem.IndexNbr, OpState = UI.Business.Objects.OpChangeState.New });
                            }                            
                        }
                        if (referenceValue.Count() > 3 && referenceValue[3] is int)
                        {
                            timeToProceed = Convert.ToInt32(referenceValue[3]);
                        }
                    }

                    #endregion
                }
                else if (issueType == OpIssueType.Enum.REPORT)
                {
                    #region Report

                    if (referenceValue != null)
                    {
                        if (referenceValue.Count() > 0 && referenceValue[0] is List<GOPerformanceValue>)
                        {
                            List<GOPerformanceValue> rpList = referenceValue[0] as List<GOPerformanceValue>;
                            foreach (GOPerformanceValue rpItem in rpList.Where(w => w.IdReport.HasValue))
                            {
                                Enums.ReferenceType rtItem = Enums.ReferenceType.IdReport;
                                int rvItem = rpItem.IdReport.Value;

                                int index = newIssue.DataList.GetNextIndex(DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE);
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE, Index = index, Value = rpItem.IdDataType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_REFERENCE_TYPE, Index = index, Value = (int)rtItem, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_REFERENCE_VALUE, Index = index, Value = rpItem.IdReport, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_START_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(rpItem.StartTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_END_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(rpItem.EndTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_AGGREGATION_TYPE, Index = index, Value = (int)rpItem.AggregationType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_VALUE, Index = index, Value = rpItem.Value, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_INDEX, Index = index, Value = rpItem.IndexNbr, OpState = UI.Business.Objects.OpChangeState.New });
                            }
                            newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_REPORT_ID, Index = 0, Value = rpList.First().IdReport, OpState = UI.Business.Objects.OpChangeState.New });
                        }
                        if (referenceValue.Count() > 1 && referenceValue[1] is int)
                        {
                            timeToProceed = Convert.ToInt32(referenceValue[1]);
                        }
                    }

                    #endregion
                }
                else if (issueType == OpIssueType.Enum.CLIENT_PORTAL)
                {
                    #region ClientPortal

                    if (referenceValue != null)
                    {
                        if (referenceValue.Count() > 0 && referenceValue[0] is List<GOPerformanceValue>)
                        {
                            List<GOPerformanceValue> ispList = referenceValue[0] as List<GOPerformanceValue>;
                            foreach (GOPerformanceValue ispItem in ispList.Where(w => w.IdImrServer.HasValue))
                            {
                                Enums.ReferenceType rtItem = Enums.ReferenceType.IdIMRServer;
                                int rvItem = ispItem.IdImrServer.Value;

                                int index = newIssue.DataList.GetNextIndex(DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE);
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE, Index = index, Value = ispItem.IdDataType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_REFERENCE_TYPE, Index = index, Value = (int)rtItem, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_REFERENCE_VALUE, Index = index, Value = ispItem.IdImrServer, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_START_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(ispItem.StartTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_END_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(ispItem.EndTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_AGGREGATION_TYPE, Index = index, Value = (int)ispItem.AggregationType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_VALUE, Index = index, Value = ispItem.Value, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_INDEX, Index = index, Value = ispItem.IndexNbr, OpState = UI.Business.Objects.OpChangeState.New });
                            }
                            newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_SERVER_ID, Index = 0, Value = ispList.First().IdImrServer, OpState = UI.Business.Objects.OpChangeState.New });
                        }
                        if (referenceValue.Count() > 1 && referenceValue[1] is int)
                        {
                            timeToProceed = Convert.ToInt32(referenceValue[1]);
                        }
                    }

                    #endregion
                }
                else if (issueType == OpIssueType.Enum.SERVER)
                {
                    #region Server

                    if (referenceValue != null)
                    {
                        if (referenceValue.Count() > 0 && referenceValue[0] is List<GOPerformanceValue>)
                        {
                            List<GOPerformanceValue> ispList = referenceValue[0] as List<GOPerformanceValue>;
                            foreach (GOPerformanceValue ispItem in ispList.Where(w => w.IdImrServer.HasValue))
                            {
                                Enums.ReferenceType rtItem = Enums.ReferenceType.IdIMRServer;
                                int rvItem = ispItem.IdImrServer.Value;

                                int index = newIssue.DataList.GetNextIndex(DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE);
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE, Index = index, Value = ispItem.IdDataType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_REFERENCE_TYPE, Index = index, Value = (int)rtItem, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_REFERENCE_VALUE, Index = index, Value = ispItem.IdImrServer, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_START_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(ispItem.StartTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_END_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(ispItem.EndTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_AGGREGATION_TYPE, Index = index, Value = (int)ispItem.AggregationType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_VALUE, Index = index, Value = ispItem.Value, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_INDEX, Index = index, Value = ispItem.IndexNbr, OpState = UI.Business.Objects.OpChangeState.New });

                                if (referenceValue.Count() > 2 && referenceValue[2] is Dictionary<long, List<OpImrServerPerformance>>)
                                {
                                    Dictionary<long, List<OpImrServerPerformance>> descriptionDict = referenceValue[2] as Dictionary<long, List<OpImrServerPerformance>>;
                                    if (ispItem.IndexNbr.HasValue && descriptionDict.ContainsKey(ispItem.IdDataType))
                                    {
                                        OpImrServerPerformance ispDecr = descriptionDict[ispItem.IdDataType].FirstOrDefault(d => d.IndexNbr == ispItem.IndexNbr.Value);
                                        if (ispDecr != null && ispDecr.Value != null)
                                            newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_DESCRIPTION, Index = index, Value = ispDecr.Value.ToString(), OpState = UI.Business.Objects.OpChangeState.New });
                                    }
                                }
                            }
                            newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_SERVER_ID, Index = 0, Value = ispList.First().IdImrServer, OpState = UI.Business.Objects.OpChangeState.New });
                        }
                        if (referenceValue.Count() > 1 && referenceValue[1] is int)
                        {
                            timeToProceed = Convert.ToInt32(referenceValue[1]);
                        }
                    }

                    #endregion
                }
                else if (issueType == OpIssueType.Enum.TRANSMISSION_DRIVER)
                {
                    #region TransmissionDriver

                    if (referenceValue != null)
                    {
                        if (referenceValue.Count() > 0 && referenceValue[0] is List<GOPerformanceValue>)
                        {
                            List<GOPerformanceValue> tdpList = referenceValue[0] as List<GOPerformanceValue>;
                            foreach (GOPerformanceValue tdpItem in tdpList.Where(w => w.IdTransmissionDriver.HasValue))
                            {
                                Enums.ReferenceType rtItem = Enums.ReferenceType.IdTransmissionDriver;
                                int rvItem = tdpItem.IdTransmissionDriver.Value;

                                int index = newIssue.DataList.GetNextIndex(DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE);
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE, Index = index, Value = tdpItem.IdDataType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_REFERENCE_TYPE, Index = index, Value = (int)rtItem, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_REFERENCE_VALUE, Index = index, Value = tdpItem.IdTransmissionDriver, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_START_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(tdpItem.StartTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_END_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(tdpItem.EndTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_AGGREGATION_TYPE, Index = index, Value = (int)tdpItem.AggregationType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_VALUE, Index = index, Value = tdpItem.Value, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_INDEX, Index = index, Value = tdpItem.IndexNbr, OpState = UI.Business.Objects.OpChangeState.New });
                            }
                            newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_TRANSMISSION_DRIVER_ID, Index = 0, Value = tdpList.First().IdTransmissionDriver, OpState = UI.Business.Objects.OpChangeState.New });
                        }
                        if (referenceValue.Count() > 1 && referenceValue[1] is int)
                        {
                            timeToProceed = Convert.ToInt32(referenceValue[1]);
                        }
                    }

                    #endregion
                }
                else if (issueType == OpIssueType.Enum.OTHER)
                {
                    #region Other

                    if (referenceValue != null)
                    {
                        if (referenceValue.Count() > 0 && referenceValue[0] is long)
                            newIssue.IdLocation = Convert.ToInt64(referenceValue[0]);
                        if (referenceValue.Count() > 1 && referenceValue[1] is List<GOPerformanceValue>)
                        {
                            List<GOPerformanceValue> dtList = referenceValue[1] as List<GOPerformanceValue>;
                            foreach (GOPerformanceValue dtItem in dtList.Where(w => w.IdLocation.HasValue))
                            {
                                Enums.ReferenceType rtItem = Enums.ReferenceType.IdLocation;
                                long rvItem = dtItem.IdLocation.Value;

                                int index = newIssue.DataList.GetNextIndex(DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE);
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_DATA_TYPE, Index = index, Value = dtItem.IdDataType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_REFERENCE_TYPE, Index = index, Value = (int)rtItem, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_REFERENCE_VALUE, Index = index, Value = rvItem, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_START_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(dtItem.StartTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_END_TIME, Index = index, Value = dataProvider.ConvertTimeZoneToUtcTime(dtItem.EndTime), OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_ID_AGGREGATION_TYPE, Index = index, Value = (int)dtItem.AggregationType, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_VALUE, Index = index, Value = dtItem.Value, OpState = UI.Business.Objects.OpChangeState.New });
                                newIssue.DataList.Add(new OpIssueData() { IdDataType = DataType.ISSUE_PERFORMANCE_INDEX, Index = index, Value = dtItem.IndexNbr, OpState = UI.Business.Objects.OpChangeState.New });
                            }
                        }
                        if (referenceValue.Count() > 2 && referenceValue[2] is int)
                        {
                            timeToProceed = Convert.ToInt32(referenceValue[2]);
                        }
                    }

                    #endregion
                }

                if (!timeToProceed.HasValue)
                {
                    string countryCode = null;
                    if (distributor.DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_COUNTRY && d.Value != null))
                        countryCode = distributor.DataList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_COUNTRY && d.Value != null).Value.ToString().Trim();
                    newIssue.Deadline = dataProvider.GetDeadlineDate(startDate: newIssue.CreationDate, days: timeToProceed, countryCode: countryCode, idIssue: newIssue.IdIssue);
                }

                return newIssue;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region GetAllConfiguredHierarchyDataType

        public static Dictionary<string,List<long>> GetAllConfiguredHierarchyDataType(List<PerformanceComponent.GOImrServer> serverPerformances, DataProvider dataProvider)
        {
            Dictionary<string, List<long>> dataTypeDict = new Dictionary<string, List<long>>();
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES, new List<long>());
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES, new List<long>());
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES, new List<long>());
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.REPORT_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.REPORT_PERFORMANCES, new List<long>());
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.QUEUE_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.QUEUE_PERFORMANCES, new List<long>());
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.PROCESS_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.PROCESS_PERFORMANCES, new List<long>());
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES, new List<long>());
            if (!dataTypeDict.ContainsKey(Enums.DataTypeGroup.ETL_PERFORMANCES))
                dataTypeDict.Add(Enums.DataTypeGroup.ETL_PERFORMANCES, new List<long>());

            if (serverPerformances != null)
            {
                if(PerformanceComponent.DataTemporalPerformances == null || PerformanceComponent.DistributorPerformances == null || PerformanceComponent.ImrServerPerformances == null ||
                   PerformanceComponent.ReportPerformances == null || PerformanceComponent.QueuePerformances == null || PerformanceComponent.ProcessPerformances == null || 
                   PerformanceComponent.TransmissionDriverPerformances == null || PerformanceComponent.EtlPerformances == null)
                    PerformanceComponent.CreatePerformancesGroups(dataProvider);

                foreach (PerformanceComponent.GOImrServer goisItem in serverPerformances)
                {
                    foreach (List<OpDataType> dtListItem in goisItem.BasePerformances.Values)
                    {
                        foreach (OpDataType dtItem in dtListItem)
                        {
                            if (PerformanceComponent.DataTemporalPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.DistributorPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ImrServerPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ReportPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.QueuePerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ProcessPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.TransmissionDriverPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.EtlPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES].Add(dtItem.IdDataType);
                        }
                    }
                    foreach (List<OpDataType> dtListItem in goisItem.LocationAggregatePerformances.Values)
                    {
                        foreach (OpDataType dtItem in dtListItem)
                        {
                            if (PerformanceComponent.DataTemporalPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.DistributorPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ImrServerPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ReportPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.QueuePerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ProcessPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.TransmissionDriverPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.EtlPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES].Add(dtItem.IdDataType);
                        }
                    }
                    foreach (List<OpDataType> dtListItem in goisItem.DistributorAggregatePerformances.Values)
                    {
                        foreach (OpDataType dtItem in dtListItem)
                        {
                            if (PerformanceComponent.DataTemporalPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.DistributorPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ImrServerPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ReportPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.QueuePerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.ProcessPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.TransmissionDriverPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Add(dtItem.IdDataType);
                            if (PerformanceComponent.EtlPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                                dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES].Add(dtItem.IdDataType);
                        }
                    }
                    foreach (OpDataType dtItem in goisItem.ReportAggregatePerformances)
                    {
                        if (PerformanceComponent.DataTemporalPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.DistributorPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ImrServerPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ReportPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.QueuePerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ProcessPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.TransmissionDriverPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.EtlPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES].Add(dtItem.IdDataType);
                    }
                    foreach (OpDataType dtItem in goisItem.TransmissionDriverAggregatePerformances)
                    {
                        if (PerformanceComponent.DataTemporalPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.DistributorPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ImrServerPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ReportPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.QueuePerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ProcessPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.TransmissionDriverPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.EtlPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES].Add(dtItem.IdDataType);
                    }
                    foreach (OpDataType dtItem in goisItem.EtlAggregatePerformances)
                    {
                        if (PerformanceComponent.DataTemporalPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.DistributorPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ImrServerPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ReportPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.QueuePerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.ProcessPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.TransmissionDriverPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Add(dtItem.IdDataType);
                        if (PerformanceComponent.EtlPerformances.Exists(d => d.IdDataType == dtItem.IdDataType))
                            dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES].Add(dtItem.IdDataType);
                    }
                    dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].AddRange(goisItem.ImrServerAggregatePerformances.Select(d => d.IdDataType));
                }
                dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.DATA_TEMPORAL_PERFORMANCES].Distinct().ToList();
                dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.DISTRIBUTOR_PERFORMANCES].Distinct().ToList();
                dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.IMR_SERVER_PERFORMANCES].Distinct().ToList();
                dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.REPORT_PERFORMANCES].Distinct().ToList();
                dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.QUEUE_PERFORMANCES].Distinct().ToList();
                dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.PROCESS_PERFORMANCES].Distinct().ToList();
                dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.TRANSMISSION_DRIVER_PERFORMANCES].Distinct().ToList();
                dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES] = dataTypeDict[Enums.DataTypeGroup.ETL_PERFORMANCES].Distinct().ToList();
            }
            
            return dataTypeDict;
        }

        #endregion

        #region Class
        public class GOPerformanceHierarchyItem
        {
            public long IdGOPerformanceHierarchyItem { get; private set; }

            public OpDistributorData Parent { get; set; }
            public OpDistributorData DataType { get; set; }
            public List<OpDistributorData> Childs { get; set; }
            public bool Ignored { get; set; }
            public bool AlarmNotifications { get; set; }
            public bool DotVisible { get; set; }
            public bool ParentDistributor { get; set; }
            public List<int> ChildDistributorList { get; set; }
            public List<GOAlarmNotificationsReceivers> AlarmNotificationsReceivers
            {
                get
                {
                    List<GOAlarmNotificationsReceivers> retList = new List<GOAlarmNotificationsReceivers>();
                    if (AlarmNotificationsReceiversData != null && AlarmNotificationsReceiversData.Value != null && !String.IsNullOrEmpty(AlarmNotificationsReceiversData.Value.ToString()))
                    {
                        string[] split = AlarmNotificationsReceiversData.Value.ToString().Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string sItem in split)
                            retList.Add(new GOAlarmNotificationsReceivers() { Email = sItem, OpState = OpChangeState.Loaded });
                    }
                    return retList;
                }
                set
                {
                    if (this.AlarmNotificationsReceiversData == null)
                    {
                        this.AlarmNotificationsReceiversData = new OpDistributorData();
                        this.AlarmNotificationsReceiversData.Index = this.DataType.Index;
                        this.AlarmNotificationsReceiversData.IdDistributor = this.DataType.IdDistributor;
                        this.AlarmNotificationsReceiversData.IdDataType = Common.DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_ADDITIONAL_EMAIL;
                    }
                    this.AlarmNotificationsReceiversData.Value = String.Join(";", value);
                    this.AlarmNotificationsReceiversData.OpState = Objects.OpChangeState.Modified;
                }
            }
            public OpDistributorData AlarmNotificationsReceiversData { get; set; }
            public List<OpDistributorData> AlarmNotificationsReferenceValueData { get; set; }
            public List<OpDistributorData> AdditionalConditionsData { get; set; }

            public OpDistributor Distributor { get; private set; }
            public OpImrServer Server { get; private set; }
            public Enums.ReferenceType ReferenceType { get; set; }
            public GOAggregateComputeType AggregationComputeType { get; set; }
            public GOAggregateType AggregationType { get; set; }
            public double Weight { get; private set; }
            public double RedThreshold { get; private set; }
            public double YellowThreshold { get; private set; }
            public Dictionary<Enums.ReferenceType, object> AdditionalConstraints { get; set; }
            public Dictionary<Enums.ReferenceType, object> AlarmNotificationsReferenceValue { get; set; }
            //Custom location constraints
            public bool? LocationInKpiConstraint { get; set; }
            public List<Enums.AggregationType> AllowedAggregatioType { get; set; }
            public Enums.AggregationType MaxAggregationType { get; set; }
            public PerformanceComponent.DeviceReadoutScheduleOkoComputingMode ComputingMode { get; set; }

            public bool AutoCreateIssue { get; set; }
            public bool AutoCreateIssueWithTask { get; set; }
            public bool AutoCreateTask { get; set; }

            public int AcceptableIncorrectTime { get; set; }
            public Enums.ReportScheduleUnit AcceptableIncorrectTimeUnit { get; set; }

            public void SetYellowThreshold(int Value)
            {
                this.YellowThreshold = Value;
            }

            public void SetRedThreshold(int Value)
            {
                this.RedThreshold = Value;
            }

            public List<int> IdResponsibleOperatorList
            {
                get
                {
                    List<int> retList = new List<int>();
                    if (ResponsibleOperatorData != null && ResponsibleOperatorData.Value != null && !String.IsNullOrEmpty(ResponsibleOperatorData.Value.ToString()))
                    {
                        string[] split = ResponsibleOperatorData.Value.ToString().Trim().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string sItem in split)
                            retList.Add(Convert.ToInt32(sItem));
                    }
                    return retList;
                }
                set
                {
                    if (this.ResponsibleOperatorData == null)
                    {
                        this.ResponsibleOperatorData = new OpDistributorData();
                        this.ResponsibleOperatorData.Index = this.DataType.Index;
                        this.ResponsibleOperatorData.IdDistributor = this.DataType.IdDistributor;
                        this.ResponsibleOperatorData.IdDataType = Common.DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_RESPONSIBLE_OPERATOR;
                    }
                    this.ResponsibleOperatorData.Value = String.Join(";", value);
                    this.ResponsibleOperatorData.OpState = Objects.OpChangeState.Modified;
                }
            }
            public OpDistributorData ResponsibleOperatorData { get; set; }

            public string ComboboxString { get; set; }

            public int MaxDayAcceptableIncorrectTime { get; set; }

            private GOMonitoredTypeOfAggregation _MonitoredTypeOfAggregation {get; set;}
            public GOMonitoredTypeOfAggregation MonitoredTypeOfAggregation
            {
                get
                {
                    if (this._MonitoredTypeOfAggregation == null)
                        return new GOMonitoredTypeOfAggregation(Enums.AggregationType.Quarter);
                    else
                        return this._MonitoredTypeOfAggregation;
                }
                set
                {
                    this._MonitoredTypeOfAggregation = value;
                }
            }

            public GOPerformanceHierarchyItem(long IdGOPerformanceHierarchyItem,
                OpDistributorData DataType,
                OpDistributorData Parent,
                OpImrServer Server,
                OpDistributor Distributor, double Weight, double RedThreshold, double YellowThreshold,
                bool Ignored, bool AlarmNotifications, bool DotVisible,
                Enums.AggregationType MaxAggregationType,
                GOMonitoredTypeOfAggregation MonitoredTypeOfAggregation,
                PerformanceComponent.DeviceReadoutScheduleOkoComputingMode ComputingMode = PerformanceComponent.DeviceReadoutScheduleOkoComputingMode.AlevelAverage,
                bool AutoCreateIssue = false,
                bool AutoCreateIssueWithTask = false,
                bool AutoCreateTask = false,
                int AcceptableIncorrectTime = 3,
                Enums.ReportScheduleUnit AcceptableIncorrectTimeUnit = Enums.ReportScheduleUnit.Day,
                int MaxDayAcceptableIncorrectTime = 1
                )
            {
                this.DataType = DataType;
                this.Parent = Parent;
                this.Childs = new List<OpDistributorData>();
                this.IdGOPerformanceHierarchyItem = IdGOPerformanceHierarchyItem;
                this.Server = Server;
                this.Distributor = Distributor;
                this.Weight = Weight;
                this.RedThreshold = RedThreshold;
                this.YellowThreshold = YellowThreshold;
                this.Ignored = Ignored;
                this.AlarmNotifications = AlarmNotifications;
                this.DotVisible = DotVisible;
                this.MaxAggregationType = MaxAggregationType;
                if (this.AlarmNotificationsReceivers == null)
                    this.AlarmNotificationsReceivers = new List<GOAlarmNotificationsReceivers>();
                this.AdditionalConditionsData = new List<OpDistributorData>();
                this.AlarmNotificationsReferenceValueData = new List<OpDistributorData>();
                this.AdditionalConstraints = new Dictionary<Enums.ReferenceType, object>();
                this.AlarmNotificationsReferenceValue = new Dictionary<Enums.ReferenceType, object>();
                this.ComputingMode = ComputingMode;
                this.AutoCreateIssue = AutoCreateIssue;
                this.AutoCreateIssueWithTask = AutoCreateIssueWithTask;
                this.AutoCreateTask = AutoCreateTask;
                this.AcceptableIncorrectTime = AcceptableIncorrectTime;
                this.AcceptableIncorrectTimeUnit = AcceptableIncorrectTimeUnit;
                this.ChildDistributorList = new List<int>();
                if (this.IdResponsibleOperatorList == null)
                    this.IdResponsibleOperatorList = new List<int>();
                this.MaxDayAcceptableIncorrectTime = MaxDayAcceptableIncorrectTime;
                this._MonitoredTypeOfAggregation = MonitoredTypeOfAggregation;
            }

            public void RefreshAddtionalConstraintsDict(DataProvider DataProvider)
            {
                this.AdditionalConstraints = new Dictionary<Enums.ReferenceType, object>();
                if (this.AdditionalConditionsData != null && this.AdditionalConditionsData.Count > 0)
                {
                    List<int> idLocationStateType = new List<int>();
                    List<int> idLocationType = new List<int>();
                    List<int> idDeviceStateType = new List<int>();
                    List<int> idDeviceType = new List<int>();
                    List<long> serialNbr = new List<long>();
                    List<string> etl = new List<string>();
                    List<int> idTransmissionDriver = new List<int>();
                    foreach (OpDistributorData ddConstraintItem in this.AdditionalConditionsData.Where(d => d.IdDataType == IMR.Suite.Common.DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_ID_REFERENCE_TYPE))
                    {
                        OpDistributorData ddConstraintReferenceType = ddConstraintItem;
                        List<OpDistributorData> ddConstraintReferenceValueList = this.AdditionalConditionsData.FindAll(d => d.Index == ddConstraintItem.Index && d.IdDataType == IMR.Suite.Common.DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_CONDITION_REFERENCE_VALUE);
                        if (ddConstraintReferenceValueList != null && ddConstraintReferenceValueList.Count > 0)
                        {
                            #region IdLocationStateType

                            if (Convert.ToInt32(ddConstraintReferenceType.Value) == (int)Enums.ReferenceType.IdLocationStateType)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddConstraintReferenceValueList)
                                {
                                    idLocationStateType.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region IdLocationType

                            if (Convert.ToInt32(ddConstraintReferenceType.Value) == (int)Enums.ReferenceType.IdLocationType)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddConstraintReferenceValueList)
                                {
                                    idLocationType.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region IdDeviceStateType

                            if (Convert.ToInt32(ddConstraintReferenceType.Value) == (int)Enums.ReferenceType.IdDeviceStateType)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddConstraintReferenceValueList)
                                {
                                    idDeviceStateType.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region IdDeviceType

                            if (Convert.ToInt32(ddConstraintReferenceType.Value) == (int)Enums.ReferenceType.IdDeviceType)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddConstraintReferenceValueList)
                                {
                                    idDeviceType.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region serialNbr

                            if (Convert.ToInt32(ddConstraintReferenceType.Value) == (int)Enums.ReferenceType.SerialNbr)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddConstraintReferenceValueList)
                                {
                                    serialNbr.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region etl

                            if (Convert.ToInt32(ddConstraintReferenceType.Value) == (int)Enums.ReferenceType.IdEtl)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddConstraintReferenceValueList)
                                {
                                    etl.Add(ddReverenceValue.Value.ToString());
                                }
                            }

                            #endregion
                            #region IdTransmissionDriver

                            if (Convert.ToInt32(ddConstraintReferenceType.Value) == (int)Enums.ReferenceType.IdTransmissionDriver)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddConstraintReferenceValueList)
                                {
                                    idTransmissionDriver.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                        }
                    }

                    if (idLocationStateType.Count > 0)
                    {
                        List<OpLocationStateType> lstList = DataProvider.GetLocationStateType(idLocationStateType.ToArray());
                        if (this.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdLocationStateType))
                            this.AdditionalConstraints[Enums.ReferenceType.IdLocationStateType] = lstList;
                        else
                            this.AdditionalConstraints.Add(Enums.ReferenceType.IdLocationStateType, lstList);

                    }
                    if (idLocationType.Count > 0)
                    {
                        List<OpLocationType> ltList = DataProvider.GetLocationType(idLocationType.ToArray());
                        if (this.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdLocationType))
                            this.AdditionalConstraints[Enums.ReferenceType.IdLocationType] = ltList;
                        else
                            this.AdditionalConstraints.Add(Enums.ReferenceType.IdLocationType, ltList);
                    }

                    if (idDeviceStateType.Count > 0)
                    {
                        List<OpDeviceStateType> lstList = DataProvider.GetDeviceStateType(idDeviceStateType.ToArray());
                        if (this.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceStateType))
                            this.AdditionalConstraints[Enums.ReferenceType.IdDeviceStateType] = lstList;
                        else
                            this.AdditionalConstraints.Add(Enums.ReferenceType.IdDeviceStateType, lstList);
                    }
                    if (idDeviceType.Count > 0)
                    {
                        List<OpDeviceType> ltList = DataProvider.GetDeviceType(idDeviceType.ToArray());
                        if (this.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdDeviceType))
                            this.AdditionalConstraints[Enums.ReferenceType.IdDeviceType] = ltList;
                        else
                            this.AdditionalConstraints.Add(Enums.ReferenceType.IdDeviceType, ltList);
                    }
                    if (serialNbr.Count > 0)
                    {
                        List<OpDevice> ltList = DataProvider.GetDevice(serialNbr.ToArray());
                        if (this.AdditionalConstraints.ContainsKey(Enums.ReferenceType.SerialNbr))
                            this.AdditionalConstraints[Enums.ReferenceType.SerialNbr] = ltList;
                        else
                            this.AdditionalConstraints.Add(Enums.ReferenceType.SerialNbr, ltList);
                    }
                    if (etl.Count > 0)
                    {
                        List<int> idEtlCore = new List<int>();
                        List<int> idEtlDW = new List<int>();
                        foreach (string sEtl in etl)
                        {
                            string[] idParam = sEtl.Split('-');
                            if ((Enums.DatabaseType)Convert.ToInt32(idParam[1]) == Enums.DatabaseType.CORE)
                                idEtlCore.Add(Convert.ToInt32(idParam[0]));
                            else if ((Enums.DatabaseType)Convert.ToInt32(idParam[1]) == Enums.DatabaseType.DW)
                                idEtlDW.Add(Convert.ToInt32(idParam[0]));
                        }

                        List<PerformanceComponent.GOETL> ltList = new List<GOETL>();
                        DataProvider.GetEtl(idEtlCore.ToArray()).ForEach(d => ltList.Add(new GOETL() { coreETL = d }));
                        DataProvider.GetEtlDW(idEtlDW.ToArray()).ForEach(d => ltList.Add(new GOETL() { dwETL = d }));

                        if (this.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdEtl))
                            this.AdditionalConstraints[Enums.ReferenceType.IdEtl] = ltList;
                        else
                            this.AdditionalConstraints.Add(Enums.ReferenceType.IdEtl, ltList);
                    }
                    if (idTransmissionDriver.Count > 0)
                    {
                        List<Objects.DW.OpTransmissionDriver> ltList = DataProvider.GetTransmissionDriverDW(Ids: idTransmissionDriver.ToArray(),
                                                                                      queryDatabase: false,
                                                                                      autoTransaction: false,
                                                                                      transactionLevel: IsolationLevel.ReadUncommitted);
                        if (this.AdditionalConstraints.ContainsKey(Enums.ReferenceType.IdTransmissionDriver))
                            this.AdditionalConstraints[Enums.ReferenceType.IdTransmissionDriver] = ltList;
                        else
                            this.AdditionalConstraints.Add(Enums.ReferenceType.IdTransmissionDriver, ltList);
                    }
                }
            }

            public void RefreshAlarmNotificationsReferenceValueDict(DataProvider DataProvider)
            {
                this.AlarmNotificationsReferenceValue = new Dictionary<Enums.ReferenceType, object>();
                if (this.AlarmNotificationsReferenceValueData != null && this.AlarmNotificationsReferenceValueData.Count > 0)
                {
                    List<int> idDistributor = new List<int>();
                    List<int> idReport = new List<int>();
                    List<string> nameQueueAndProcess = new List<string>();
                    List<long> idLocation = new List<long>();
                    List<long> idMeter = new List<long>();
                    List<long> idSerial = new List<long>();
                    List<int> idTransmissionDriver = new List<int>();
                    List<string> etl = new List<string>();
                    foreach (OpDistributorData ddAlarmNotificationReferenceValueItem in this.AlarmNotificationsReferenceValueData.Where(d => d.IdDataType == IMR.Suite.Common.DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_TYPE))
                    {
                        OpDistributorData ddAlaramNotificationReferenceType = ddAlarmNotificationReferenceValueItem;
                        List<OpDistributorData> ddAlarmNotificationReferenceValueList = this.AlarmNotificationsReferenceValueData.FindAll(d => d.Index == ddAlarmNotificationReferenceValueItem.Index && d.IdDataType == IMR.Suite.Common.DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_ITEM_ALARM_NOTIFICATIONS_REFERENCE_VALUE);
                        if (ddAlarmNotificationReferenceValueList != null && ddAlarmNotificationReferenceValueList.Count > 0)
                        {
                            #region IdDistributor

                            if (Convert.ToInt32(ddAlaramNotificationReferenceType.Value) == (int)Enums.ReferenceType.IdDistributor)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                {
                                    idDistributor.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region IdReport

                            if (Convert.ToInt32(ddAlaramNotificationReferenceType.Value) == (int)Enums.ReferenceType.IdReport)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                {
                                    idReport.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region idDevice

                            if (Convert.ToInt32(ddAlaramNotificationReferenceType.Value) == (int)Enums.ReferenceType.SerialNbr)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                {
                                    idSerial.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region Queue, Process

                            if (Convert.ToInt32(ddAlaramNotificationReferenceType.Value) == (int)Enums.ReferenceType.IdIMRServer)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                {
                                    nameQueueAndProcess.Add(ddReverenceValue.Value.ToString());
                                }
                            }

                            #endregion
                            #region Location, Meter

                            if (Convert.ToInt32(ddAlaramNotificationReferenceType.Value) == (int)Enums.ReferenceType.IdLocation)
                            {
                                if ((int)ddAlarmNotificationReferenceValueItem.Value == (int)Enums.ReferenceType.IdLocation)
                                    foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                    {
                                        idLocation.Add(Convert.ToInt32(ddReverenceValue.Value));
                                    }
                                if ((int)ddAlarmNotificationReferenceValueItem.Value == (int)Enums.ReferenceType.IdMeter)
                                    foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                    {
                                        idMeter.Add(Convert.ToInt32(ddReverenceValue.Value));
                                    }
                            }

                            #endregion
                            #region IdTransmissionDriver

                            if (Convert.ToInt32(ddAlaramNotificationReferenceType.Value) == (int)Enums.ReferenceType.IdTransmissionDriver)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                {
                                    idTransmissionDriver.Add(Convert.ToInt32(ddReverenceValue.Value));
                                }
                            }

                            #endregion
                            #region IdEtl

                            if (Convert.ToInt32(ddAlaramNotificationReferenceType.Value) == (int)Enums.ReferenceType.IdEtl)
                            {
                                foreach (OpDistributorData ddReverenceValue in ddAlarmNotificationReferenceValueList)
                                {
                                    etl.Add(ddReverenceValue.Value.ToString());
                                }
                            }

                            #endregion
                        }
                    }
                    if (idDistributor.Count > 0)
                    {
                        List<OpDistributor> dList = DataProvider.GetDistributor(idDistributor.ToArray());
                        if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdDistributor))
                            this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdDistributor] = dList;
                        else
                            this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdDistributor, dList);
                    }
                    if (idReport.Count > 0)
                    {
                        List<OpReport> ltList = DataProvider.GetReport(idReport.ToArray());
                        List<OpReportData> rdListtmp = DataProvider.GetReportDataFilter(IdDataType: new long[] { IMR.Suite.Common.DataType.REPORT_DATA_GENERATION_CHECK,
                                                                                                                 IMR.Suite.Common.DataType.REPORT_EMAIL_SENT_CHECK ,
                                                                                                                 IMR.Suite.Common.DataType.REPORT_FTP_SENT_CHECK },
                                                                                                                 IdReport: idReport.ToArray(),
                                                                                                                 loadNavigationProperties: false,
                                                                                                                 transactionLevel: IsolationLevel.ReadUncommitted,
                                                                                                                 autoTransaction: false);
                        ltList.RemoveAll(d => !d.IdReport.In(rdListtmp.Where(w => Convert.ToBoolean(w.Value) == true).Select(s => s.IdReport).ToArray()));

                        if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdReport))
                            this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdReport] = ltList;
                        else
                            this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdReport, ltList);
                    }
                    if (idTransmissionDriver.Count > 0)
                    {
                        List<Objects.DW.OpTransmissionDriver> ltList = DataProvider.GetTransmissionDriverDW(Ids: idTransmissionDriver.ToArray(),
                                                                                                  queryDatabase: false,
                                                                                                  autoTransaction: false,
                                                                                                  transactionLevel: IsolationLevel.ReadUncommitted);
                        if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdTransmissionDriver))
                            this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdTransmissionDriver] = ltList;
                        else
                            this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdTransmissionDriver, ltList);
                    }
                    if (etl.Count > 0)
                    {
                        List<GOETL> ltList = new List<GOETL>();
                        ltList = PerformanceComponent.GetAllEtl(DataProvider, Server.IdImrServer);

                        List<int> idEtl = new List<int>();
                        foreach (string sEtl in etl)
                        {
                            string[] idParam = sEtl.Split('-');
                            idEtl.Add(Convert.ToInt32(idParam[0]));

                        }
                        ltList.RemoveAll(r => !r.IdParam.In(idEtl.ToArray()));

                        if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdEtl))
                            this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdEtl] = ltList;
                        else
                            this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdEtl, ltList);
                    }

                    DateTime dateTimeNow = DataProvider.DateTimeNow;
                    DateTime endDate = DataProvider.ConvertTimeZoneToUtcTime(new DateTime(dateTimeNow.Year, dateTimeNow.Month, dateTimeNow.Day));
                    string customWhereClause = "[END_TIME] >= '" + endDate.AddDays(-1).ToString("yyyy-MM-dd HH:mm") + "' AND [END_TIME] <= '" + endDate.ToString("yyyy-MM-dd HH:mm") + "'";

                    if (nameQueueAndProcess.Count > 0)
                    {
                        List<OpImrServerPerformance> lstList = DataProvider.GetImrServerPerformanceFilter(customWhereClause: customWhereClause,
                                                                                                         IdDataType: new long[] { IMR.Suite.Common.DataType.MON_QUEUE_WATCHER_QUEUE_NAME, IMR.Suite.Common.DataType.MON_PROCESS_NAME },
                                                                                                         loadNavigationProperties: false,
                                                                                                         autoTransaction: false,
                                                                                                         transactionLevel: System.Data.IsolationLevel.ReadUncommitted);
                        lstList.RemoveAll(d => !d.Value.In(nameQueueAndProcess.ToArray()));
                        lstList = lstList.Distinct(d => d.Value).ToList();

                        List<GOQueue> goqList = new List<GOQueue>();
                        List<GOProcess> gopList = new List<GOProcess>();

                        foreach (OpImrServerPerformance ispItem in lstList)
                        {
                            if (ispItem.IdDataType == IMR.Suite.Common.DataType.MON_QUEUE_WATCHER_QUEUE_NAME)
                                goqList.Add(new GOQueue(ispItem, ispItem.Value.ToString()));
                            else
                                if (ispItem.IdDataType == IMR.Suite.Common.DataType.MON_PROCESS_NAME)
                                gopList.Add(new GOProcess(ispItem, ispItem.Value.ToString()));
                        }

                        if (Convert.ToInt64(this.DataType.Value) == IMR.Suite.Common.DataType.MON_QUEUE_WATCHER_QUEUE_HI_LEVEL)
                        {
                            if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdIMRServer))
                                this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdIMRServer] = goqList;
                            else
                                this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdIMRServer, goqList);
                        }
                        else
                        {
                            if (Convert.ToInt64(this.DataType.Value) == IMR.Suite.Common.DataType.MON_IS_WORKING)
                            {
                                if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdIMRServer))
                                    this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdIMRServer] = gopList;
                                else
                                    this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdIMRServer, gopList);
                            }
                        }
                    }

                    if (idLocation.Count > 0)
                    {
                        List<OpLocation> ltList = DataProvider.GetLocation(idLocation.ToArray());
                        if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdLocation))
                            this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdLocation] = ltList;
                        else
                            this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdLocation, ltList);
                    }
                    if (idMeter.Count > 0)
                    {
                        List<OpMeter> ltList = DataProvider.GetMeter(idMeter.ToArray());
                        if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.IdMeter))
                            this.AlarmNotificationsReferenceValue[Enums.ReferenceType.IdMeter] = ltList;
                        else
                            this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.IdMeter, ltList);
                    }
                    if (idSerial.Count > 0)
                    {
                        List<OpDevice> ltList = DataProvider.GetDevice(idSerial.ToArray());
                        if (this.AlarmNotificationsReferenceValue.ContainsKey(Enums.ReferenceType.SerialNbr))
                            this.AlarmNotificationsReferenceValue[Enums.ReferenceType.SerialNbr] = ltList;
                        else
                            this.AlarmNotificationsReferenceValue.Add(Enums.ReferenceType.SerialNbr, ltList);
                    }
                }
            }
        }

        public class GOPerformanceValue
        {
            public long? IdReferenceTypeValue
            {
                get
                {
                    long? retValue = null;
                    if (this.IdLocation.HasValue)
                        retValue = this.IdLocation.Value;
                    else if (this.IdMeter.HasValue)
                        retValue = this.IdMeter.Value;
                    else if (this.SerialNbr.HasValue)
                        retValue = this.SerialNbr.Value;
                    else if (this.IdDistributor.HasValue)
                        retValue = (long)this.IdDistributor.Value;
                    else if (this.IdImrServer.HasValue)
                        retValue = (long)this.IdImrServer.Value;
                    else if (this.IdReport.HasValue)
                        retValue = (long)this.IdReport.Value;
                    else if (this.IdTransmissionDriver.HasValue)
                        retValue = (long)this.IdTransmissionDriver.Value;
                    else if (this.IdEtl.HasValue)
                        retValue = (long)this.IdEtl.Value;
                    return retValue;
                }
            }

            public long IdDataType { get; set; }
            public double Value { get; set; }

            public Enums.AggregationType AggregationType { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public Enums.DataStatus DataStatus { get; set; }
            public Enums.DataSourceType DataSourceType { get; set; }

            public long? IdLocation { get; set; }
            public long? IdMeter { get; set; }
            public long? SerialNbr { get; set; }
            public int? IdDistributor { get; set; }
            public int? IdImrServer { get; set; }
            public int? IdReport { get; set; }
            public int? IndexNbr { get; set; }
            public int? IdTransmissionDriver { get; set; }
            public int? IdEtl { get; set; }

            private string _Description;

            public string Description
            {
                get
                {
                    if (string.IsNullOrEmpty(_Description))
                        return string.Empty;
                    else
                        return _Description;
                }
                set
                {
                    _Description = value;
                }
            }

            public GOPerformanceValue(long IdDataType, double Value, int? IndexNbr)
            {
                this.IdDataType = IdDataType;
                this.Value = Value;
                this.IndexNbr = IndexNbr;
            }
            /*
            public override int CompareTo(object obj)
            {
                if (obj is GOPerformanceValue)
                {
                    if((obj as GOPerformanceValue).IdReferenceTypeValue == this.IdReferenceTypeValue
                        && (obj as GOPerformanceValue).IdDataType == this.IdDataType
                        && (obj as GOPerformanceValue).AggregationType == this.AggregationType
                        && (obj as GOPerformanceValue).StartTime == this.StartTime
                        && (obj as GOPerformanceValue).EndTime == this.EndTime
                        && (obj as GOPerformanceValue).Value == this.Value)
                        return 1;
                    else
                        return 0;
                }
                else
                    return 0;
            }*/

            public override bool Equals(object other)
            {
                if (other is GOPerformanceValue)
                {
                    GOPerformanceValue obj = other as GOPerformanceValue;
                    if (obj.IdReferenceTypeValue == this.IdReferenceTypeValue
                        && obj.IdDataType == this.IdDataType
                        && obj.AggregationType == this.AggregationType
                        && obj.StartTime == this.StartTime
                        && obj.EndTime == this.EndTime
                        && obj.Value == this.Value
                        && obj.IndexNbr == this.IndexNbr)
                        return true;
                    else
                        return false;
                }
                else
                    return base.Equals(other);

            }

            public static bool operator ==(GOPerformanceValue left, GOPerformanceValue right)
            {
                if ((object)left == null)
                    return ((object)right == null);
                return left.Equals(right);
            }
            public static bool operator !=(GOPerformanceValue left, GOPerformanceValue right)
            {
                if ((object)left == null)
                    return ((object)right != null);
                return !left.Equals(right);
            }
        }

        public class GOPeriod
        {
            public int IdPeriod { get; private set; }

            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public Enums.AggregationType AggregationType { get; set; }
            public Enums.AggregationType HigherAggregationType { get; set; }
            public Enums.AggregationType LowerAggregationType { get; set; }
            public double Weight { get; set; }

            public List<int> LowerIdPeriods { get; set; }

            public Dictionary<long, List<GOPerformanceValue>> LocationPerformanceDict { get; set; }
            public Dictionary<long, List<GOPerformanceValue>> MeterPerformanceDict { get; set; }
            public Dictionary<long, List<GOPerformanceValue>> DevicePerformanceDict { get; set; }
            public Dictionary<int, List<OpDistributorPerformance>> DistributorPerformanceDict { get; set; }
            public Dictionary<int, List<OpImrServerPerformance>> ImrServerPerformanceDict { get; set; }
            public Dictionary<int, List<OpReportPerformance>> ReportPerformanceDict { get; set; }
            public Dictionary<string, List<PerformanceComponent.GOQueue>> QueuePerformanceDict { get; set; } // Key: IdImrServer_IndexQueue
            public Dictionary<string, List<PerformanceComponent.GOProcess>> ProcessPerformanceDict { get; set; } // Key: IdImrServer_IndexProcess
            public Dictionary<int, List<OpTransmissionDriverPerformance>> TransmissionDriverPerformanceDict { get; set; }
            public Dictionary<int, List<OpEtlPerformance>> EtlPerformanceDict { get; set; }

            public GOPeriod(int IdPeriod, DateTime StartTime, DateTime EndTime,
                            Enums.AggregationType AggregationType, Enums.AggregationType HigherAggregationType,
                            Enums.AggregationType LowerAggregationType,
                            double Weight = 1.0)
            {
                this.IdPeriod = IdPeriod;

                this.StartTime = StartTime;
                this.EndTime = EndTime;
                this.AggregationType = AggregationType;
                this.HigherAggregationType = HigherAggregationType;
                this.LowerAggregationType = LowerAggregationType;
                this.Weight = Weight;

                this.LocationPerformanceDict = new Dictionary<long, List<GOPerformanceValue>>();
                this.MeterPerformanceDict = new Dictionary<long, List<GOPerformanceValue>>();
                this.DevicePerformanceDict = new Dictionary<long, List<GOPerformanceValue>>();
                this.DistributorPerformanceDict = new Dictionary<int, List<OpDistributorPerformance>>();
                this.ImrServerPerformanceDict = new Dictionary<int, List<OpImrServerPerformance>>();
                this.ReportPerformanceDict = new Dictionary<int, List<OpReportPerformance>>();
                this.QueuePerformanceDict = new Dictionary<string, List<PerformanceComponent.GOQueue>>();
                this.ProcessPerformanceDict = new Dictionary<string, List<PerformanceComponent.GOProcess>>();
                this.TransmissionDriverPerformanceDict = new Dictionary<int, List<OpTransmissionDriverPerformance>>();
                this.EtlPerformanceDict = new Dictionary<int, List<OpEtlPerformance>>();

                this.LowerIdPeriods = new List<int>();
            }
        }

        public class GOImrServer : IOpDynamicProperty<GOImrServer>
        {
            public OpImrServer Server { get; private set; }
            public bool ServerMonitored { get; set; }
            public string ImrServerVersion
            {
                get
                {
                    return ((Enums.ImrServerVersion)this.Server.ImrServerVersion).ToString();
                }
            }
            public List<OpDistributor> DistributorList { get; private set; }
            public List<OpReport> ReportList { get; private set; }

            public List<GOPerformanceValue> CurrentImrServerPerformancesValues { get; set; }
            public Dictionary<int, List<GOPerformanceValue>> CurrentDistributorPerformancesValues { get; set; }
            public Dictionary<long, List<GOPerformanceValue>> CurrentLocationPerformancesValues { get; set; }
            public Dictionary<long, List<GOPerformanceValue>> CurrentDevicePerformancesValues { get; set; }

            public Dictionary<OpDistributorData, GOPerformanceHierarchyItem> ServerPerformanceHierarchy { get; set; }
            public Dictionary<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>> PerformancesHierarchy { get; set; }//Dictionary<ID_DISTRIBUTOR, Dictionary<ID_DISTRIBUTOR_DATA, GOPerformanceHierarchyItem>>
            public Dictionary<int, GOPeriod> ServerPeriodsDict { get; set; }
            public Dictionary<int, GOPeriod> ReportPeriodsDict { get; set; }
            public Dictionary<int, GOPeriod> QueuePeriodsDict { get; set; }
            public Dictionary<int, GOPeriod> ProcessPeriodsDict { get; set; }
            public Dictionary<int, GOPeriod> TransmissionDriverPeriodsDict { get; set; }
            public Dictionary<int, GOPeriod> EtlPeriodsDict { get; set; }
            public Dictionary<int, Dictionary<int, GOPeriod>> PeriodsDict { get; set; }

            public Dictionary<int, List<OpDataType>> BasePerformances { get; set; }
            public Dictionary<int, List<OpDataType>> DistributorAggregatePerformances { get; set; }
            public List<OpDataType> ImrServerAggregatePerformances { get; set; }
            public List<OpDataType> QueueAggregatePerformances { get; set; }
            public List<OpDataType> ProcessAggregatePerformances { get; set; }
            public List<OpDataType> ReportAggregatePerformances { get; set; }
            public List<OpDataType> TransmissionDriverAggregatePerformances { get; set; }
            public List<OpDataType> EtlAggregatePerformances { get; set; }
            public Dictionary<int, List<OpDataType>> LocationAggregatePerformances { get; set; }
            public Dictionary<int, List<OpDataType>> MeterAggregatePerformances { get; set; }
            public Dictionary<int, List<OpDataType>> DeviceAggregatePerformances { get; set; }

            private OpDynamicPropertyDict DynamicPropertiesDict;

            private DataProvider DataProvider { get; set; }

            public OpDynamicPropertyDict DynamicProperties
            {
                get { return this.DynamicPropertiesDict; }
                set { this.DynamicPropertiesDict = value; }
            }

            public GOImrServer Owner
            {
                get { return this; }
            }

            public GOImrServer(OpImrServer Server, List<OpDistributor> DistributorList, List<OpReport> ReportList, DataProvider DataProvider)
            {
                this.DataProvider = DataProvider;
                this.Server = Server;
                this.ServerMonitored = this.Server.IsImrlt && this.Server.IsGood;
                this.DistributorList = DistributorList;
                this.ReportList = ReportList;
                this.PerformancesHierarchy = new Dictionary<int, Dictionary<OpDistributorData, GOPerformanceHierarchyItem>>();
                this.PeriodsDict = new Dictionary<int, Dictionary<int, GOPeriod>>();
                this.ServerPeriodsDict = new Dictionary<int, GOPeriod>();
                this.ReportPeriodsDict = new Dictionary<int, GOPeriod>();
                this.QueuePeriodsDict = new Dictionary<int, GOPeriod>();
                this.ProcessPeriodsDict = new Dictionary<int, GOPeriod>();
                this.TransmissionDriverPeriodsDict = new Dictionary<int, GOPeriod>();
                this.EtlPeriodsDict = new Dictionary<int, GOPeriod>();
                this.BasePerformances = new Dictionary<int, List<OpDataType>>();
                this.DistributorAggregatePerformances = new Dictionary<int, List<OpDataType>>();
                this.LocationAggregatePerformances = new Dictionary<int, List<OpDataType>>();
                this.DeviceAggregatePerformances = new Dictionary<int, List<OpDataType>>();
                this.MeterAggregatePerformances = new Dictionary<int, List<OpDataType>>();
                this.ImrServerAggregatePerformances = new List<OpDataType>();
                this.QueueAggregatePerformances = new List<OpDataType>();
                this.ProcessAggregatePerformances = new List<OpDataType>();
                this.ReportAggregatePerformances = new List<OpDataType>();
                this.TransmissionDriverAggregatePerformances = new List<OpDataType>();
                this.EtlAggregatePerformances = new List<OpDataType>();
                this.ServerPerformanceHierarchy = new Dictionary<OpDistributorData, GOPerformanceHierarchyItem>();
                this.DynamicPropertiesDict = new OpDynamicPropertyDict();
                foreach (OpDistributor dItem in this.DistributorList)
                {
                    this.PerformancesHierarchy.Add(dItem.IdDistributor, new Dictionary<OpDistributorData, GOPerformanceHierarchyItem>());
                    this.PeriodsDict.Add(dItem.IdDistributor, new Dictionary<int, GOPeriod>());
                    this.BasePerformances.Add(dItem.IdDistributor, new List<OpDataType>());
                    this.DistributorAggregatePerformances.Add(dItem.IdDistributor, new List<OpDataType>());
                    this.LocationAggregatePerformances.Add(dItem.IdDistributor, new List<OpDataType>());
                    this.DeviceAggregatePerformances.Add(dItem.IdDistributor, new List<OpDataType>());
                    this.MeterAggregatePerformances.Add(dItem.IdDistributor, new List<OpDataType>());
                }
                CurrentImrServerPerformancesValues = new List<GOPerformanceValue>();
                CurrentDistributorPerformancesValues = new Dictionary<int, List<GOPerformanceValue>>();
                CurrentLocationPerformancesValues = new Dictionary<long, List<GOPerformanceValue>>();
                CurrentDevicePerformancesValues = new Dictionary<long, List<GOPerformanceValue>>();
            }

            public List<OpDataType> CreateBasePerformancesList(int idDistributor)
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                if (idDistributor == 19 || idDistributor == 321)
                {
                    idDataTypeList.Add(DataType.MON_FP_AGGR_PC);
                    idDataTypeList.Add(DataType.MON_FP_AGGR_SERVICE);
                    idDataTypeList.Add(DataType.MON_FP_AGGR_TANKS);
                    idDataTypeList.Add(DataType.MON_FP_AGGR_SALES);

                    idDataTypeList.Add(DataType.MON_AWSR_RADIO_FRAMES);
                    idDataTypeList.Add(DataType.MON_AWSR_RADIO_AMPLIFIER);
                    idDataTypeList.Add(DataType.MON_AWSR_ATG);
                    idDataTypeList.Add(DataType.MON_AWSR_LON);
                    idDataTypeList.Add(DataType.MON_AWSR_PUMP_CONVERTER);
                    idDataTypeList.Add(DataType.MON_AWSR_IO_CONTROLLER);

                    idDataTypeList.Add(DataType.MON_IS_WORKING);
                    idDataTypeList.Add(DataType.MON_PERC_CPU);
                    idDataTypeList.Add(DataType.MON_BYTE_RAM);
                    idDataTypeList.Add(DataType.MON_DISK);
                    idDataTypeList.Add(DataType.MON_NETWORK);
                }
                if (this.PerformancesHierarchy != null)
                {
                    if (this.PerformancesHierarchy.ContainsKey(idDistributor))
                        idDataTypeList.AddRange(this.PerformancesHierarchy[idDistributor].Values.ToList()
                                                                                                .Where(h => h.Childs == null || h.Childs.Count == 0)
                                                                                                .Select(h => Convert.ToInt64(h.DataType.Value)));
                }
                if (this.ServerPerformanceHierarchy != null)
                {
                    idDataTypeList.AddRange(this.ServerPerformanceHierarchy.Values.ToList()
                                                                                  .Where(h => h.Childs == null || h.Childs.Count == 0)
                                                                                  .Select(h => Convert.ToInt64(h.DataType.Value)));
                }
                idDataTypeList = idDataTypeList.Distinct().ToList();

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }

            public List<OpDataType> CreateDistributorAggregatePerformancesList(int idDistributor)
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                idDataTypeList.Add(DataType.DISTRIBUTOR_PERFORMANCE_VALUE);
                //idDataTypeList.Add(DataType.PERFORMANCE_DISTRIBUTOR_DEVICE);
                //if (idDistributor.In(new int[] { 19, 321 }))
                //idDataTypeList.Add(DataType.MON_FP_AGGR_PC);

                if (this.PerformancesHierarchy != null)
                {
                    if (this.PerformancesHierarchy.ContainsKey(idDistributor))
                        idDataTypeList.AddRange(this.PerformancesHierarchy[idDistributor].Values.ToList()
                                                                                                .Where(h => h.Childs != null && h.Childs.Count > 0 && (h.ReferenceType == Enums.ReferenceType.IdLocation || h.ReferenceType == Enums.ReferenceType.IdMeter || h.ReferenceType == Enums.ReferenceType.SerialNbr))
                                                                                                .Select(h => Convert.ToInt64(h.DataType.Value)));
                }

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateImrServerAggregatePerformancesList()
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                idDataTypeList.Add(DataType.IMR_SERVER_PERFORMANCE_VALUE);
                if (this.ServerPerformanceHierarchy != null)
                {
                    idDataTypeList.AddRange(this.ServerPerformanceHierarchy.Values.ToList()
                                                                                  .Where(h => h.Childs != null && h.Childs.Count > 0 && h.ReferenceType == Enums.ReferenceType.IdIMRServer)
                                                                                  .Select(h => Convert.ToInt64(h.DataType.Value)));
                }
                idDataTypeList = idDataTypeList.Distinct().ToList();

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateQueueAggregatePerformancesList()
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                idDataTypeList.Add(DataType.MON_QUEUE_WATCHER_QUEUE_HI_LEVEL);

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateProcessAggregatePerformancesList()
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                idDataTypeList.Add(DataType.MON_IS_WORKING);

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateReportAggregatePerformancesList()
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                if (this.ServerPerformanceHierarchy != null)
                {
                    idDataTypeList.AddRange(this.ServerPerformanceHierarchy.Values.ToList()
                                                                                  .Where(h => h.Childs != null && h.Childs.Count > 0 && h.ReferenceType == Enums.ReferenceType.IdReport)
                                                                                  .Select(h => Convert.ToInt64(h.DataType.Value)));
                }
                idDataTypeList = idDataTypeList.Distinct().ToList();

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateTransmissionDriverAggregatePerformancesList()
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                if (this.ServerPerformanceHierarchy != null)
                {
                    idDataTypeList.AddRange(this.ServerPerformanceHierarchy.Values.ToList()
                                                                                  .Where(h => h.Childs != null && h.Childs.Count > 0 && h.ReferenceType == Enums.ReferenceType.IdTransmissionDriver)
                                                                                  .Select(h => Convert.ToInt64(h.DataType.Value)));
                }
                idDataTypeList = idDataTypeList.Distinct().ToList();

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateEtlAggregatePerformancesList()
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                if (this.ServerPerformanceHierarchy != null)
                {
                    idDataTypeList.AddRange(this.ServerPerformanceHierarchy.Values.ToList()
                                                                                  .Where(h => h.Childs != null && h.Childs.Count > 0 && h.ReferenceType == Enums.ReferenceType.IdEtl)
                                                                                  .Select(h => Convert.ToInt64(h.DataType.Value)));
                }
                idDataTypeList = idDataTypeList.Distinct().ToList();

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }

            public List<OpDataType> CreateLocationAggregatePerformancesList(int idDistributor)
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();
                /*idDataTypeList.Add(DataType.LOCATION_PERFORMANCE_VALUE);
                idDataTypeList.Add(DataType.PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_PC_AGGR);
                idDataTypeList.Add(DataType.PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_SERVICE_AGGR);
                idDataTypeList.Add(DataType.PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_TANKS_AGGR);
                idDataTypeList.Add(DataType.PERFORMANCE_DISTRIBUTOR_MON_FP_AGGR_SALES_AGGR);
                */
                if (this.PerformancesHierarchy != null)
                {
                    if (this.PerformancesHierarchy.ContainsKey(idDistributor))
                        idDataTypeList.AddRange(this.PerformancesHierarchy[idDistributor].Values.ToList()
                                                                                                .Where(h => h.Childs != null && h.Childs.Count > 0 && h.ReferenceType == Enums.ReferenceType.IdLocation)
                                                                                                .Select(h => Convert.ToInt64(h.DataType.Value)));
                }

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateDeviceAggregatePerformancesList(int idDistributor)
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();

                if (this.PerformancesHierarchy != null)
                {
                    if (this.PerformancesHierarchy.ContainsKey(idDistributor))
                        idDataTypeList.AddRange(this.PerformancesHierarchy[idDistributor].Values.ToList()
                                                                                                .Where(h => h.Childs != null && h.Childs.Count > 0 && h.ReferenceType == Enums.ReferenceType.SerialNbr)
                                                                                                .Select(h => Convert.ToInt64(h.DataType.Value)));
                }

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }
            public List<OpDataType> CreateMeterAggregatePerformancesList(int idDistributor)
            {
                List<OpDataType> performances = new List<OpDataType>();

                List<long> idDataTypeList = new List<long>();

                performances.AddRange(this.DataProvider.GetDataType(idDataTypeList.ToArray()));

                return performances;
            }

            public void DetermineCurrentPerformanceValues(List<Enums.AggregationType> atList, DateTime ReferenceDate)
            {
                CurrentImrServerPerformancesValues = new List<GOPerformanceValue>();
                CurrentDistributorPerformancesValues = new Dictionary<int, List<GOPerformanceValue>>();
                CurrentLocationPerformancesValues = new Dictionary<long, List<GOPerformanceValue>>();
                CurrentDevicePerformancesValues = new Dictionary<long, List<GOPerformanceValue>>();

                DateTime? lastPeriodStart = null;
                DateTime? lastPeriodEnd = null;
                DateTime localReferenceDate = DataProvider.ConvertUtcTimeToTimeZone(ReferenceDate);
                DateTime lastStartTime = RoundDownToNearestInterval(localReferenceDate, TimeSpan.FromMinutes(15));
                DateTime lastEndTime = RoundUpToNearestInterval(localReferenceDate, TimeSpan.FromMinutes(15));

                #region ImrServer and Report
                if (this.ServerPeriodsDict != null)
                {
                    foreach (Enums.AggregationType atItem in atList)
                    {
                        List<GOPeriod> periods = this.ServerPeriodsDict.Values.ToList();
                        //periods = periods.Where(p => p.AggregationType == atItem).ToList();
                        periods = periods.OrderByDescending(d => d.StartTime).ToList();
                        PerformanceComponent.GOPeriod lastPeriods = null;
                        if (atItem == Enums.AggregationType.Quarter)
                        {
                            lastPeriods = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastStartTime.AddMinutes(-15) && p.EndTime == lastEndTime.AddMinutes(-15));
                            if (lastPeriods == null || lastPeriods.ImrServerPerformanceDict.Values.Count(v => v.Exists(t => t.IdDataType == DataType.IMR_SERVER_PERFORMANCE_VALUE)) == 0)
                                lastPeriods = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastStartTime.AddMinutes(-30) && p.EndTime == lastEndTime.AddMinutes(-30));
                        }
                        else if (atItem == Enums.AggregationType.Day)
                        {
                            DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, 1);
                            lastPeriods = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastFullDay.AddDays(-1) && p.EndTime == lastFullDay);
                            if (lastPeriods == null)
                                lastPeriods = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastFullDay.AddDays(-2) && p.EndTime == lastFullDay.AddDays(-1));
                        }

                        if (periods.Count > 0 && lastPeriods != null)
                        {
                            if (lastPeriods.ImrServerPerformanceDict != null && lastPeriods.ImrServerPerformanceDict.Values.Count(v => v.Exists(t => t.IdDataType == DataType.IMR_SERVER_PERFORMANCE_VALUE)) > 0)
                            {
                                foreach (KeyValuePair<int, List<OpImrServerPerformance>> kvItem in lastPeriods.ImrServerPerformanceDict)
                                {
                                    if (kvItem.Value.Count > 0)
                                    {
                                        foreach (PerformanceComponent.GOPerformanceHierarchyItem gophiItem in this.ServerPerformanceHierarchy.Values)
                                        {
                                            if (gophiItem.MonitoredTypeOfAggregation.AggregationType == atItem)
                                            {
                                                //szukamy najstarszego okresu z agregatami
                                                lastPeriodStart = lastPeriods.StartTime;
                                                lastPeriodEnd = lastPeriods.EndTime;
                                                if (kvItem.Value.Exists(e => e.IdDataType == Convert.ToInt64(gophiItem.DataType.Value)))
                                                {
                                                    OpImrServerPerformance ispItem = kvItem.Value.FirstOrDefault(d => d.IdDataType != DataType.MON_QUEUE_WATCHER_QUEUE_NAME && d.IdDataType != DataType.MON_PROCESS_NAME
                                                                                                                                                                            && d.IdDataType == Convert.ToInt64(gophiItem.DataType.Value));


                                                    GOPerformanceValue gopvLatest = new GOPerformanceValue(ispItem.IdDataType, Convert.ToDouble(ispItem.Value), ispItem.IndexNbr);
                                                    gopvLatest.AggregationType = atItem;
                                                    gopvLatest.DataSourceType = Enums.DataSourceType.Interface;
                                                    gopvLatest.StartTime = ispItem.StartTime;
                                                    gopvLatest.EndTime = ispItem.EndTime.Value;
                                                    CurrentImrServerPerformancesValues.Add(gopvLatest);
                                                }
                                                else
                                                {
                                                    GOPerformanceValue gopvLatest = new GOPerformanceValue(Convert.ToInt64(gophiItem.DataType.Value), 0, gophiItem.DataType.Index);
                                                    gopvLatest.AggregationType = atItem;
                                                    gopvLatest.DataSourceType = Enums.DataSourceType.Unknown;
                                                    gopvLatest.StartTime = lastPeriods.StartTime;
                                                    gopvLatest.EndTime = lastPeriods.EndTime;
                                                    CurrentImrServerPerformancesValues.Add(gopvLatest);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // if (lastPeriodEnd.HasValue)
                            //   break;

                            /*DateTime maxEndDate = periods.Max(p => p.EndTime);
                            GOPeriod lastPeriod = periods.Find(p => p.EndTime == maxEndDate);
                            if (lastPeriod.ImrServerPerformanceDict != null && lastPeriod.ImrServerPerformanceDict.Count > 0)
                            {
                                lastPeriodStart = lastPeriod.StartTime;
                                lastPeriodEnd = lastPeriod.EndTime;
                                foreach (KeyValuePair<int, List<OpImrServerPerformance>> kvItem in lastPeriod.ImrServerPerformanceDict)
                                {
                                    foreach (OpImrServerPerformance ispItem in kvItem.Value)
                                    {
                                        GOPerformanceValue gopvLatest = new GOPerformanceValue(ispItem.IdDataType, Convert.ToDouble(ispItem.Value));
                                        gopvLatest.AggregationType = atItem;
                                        gopvLatest.StartTime = ispItem.StartTime;
                                        gopvLatest.EndTime = ispItem.EndTime.Value;
                                        CurrentImrServerPerformancesValues.Add(gopvLatest);
                                    }
                                }
                            }*/
                        }
                    }
                }
                #endregion

                #region Distributor
                if (this.DistributorList != null && this.DistributorList.Count > 0 && this.PeriodsDict != null)
                {
                    GOPeriod lastPeriod = null;
                    foreach (Enums.AggregationType atItem in atList)
                    {
                        foreach (OpDistributor dItem in this.DistributorList)
                        {
                            if (this.PeriodsDict.ContainsKey(dItem.IdDistributor))
                            {
                                List<GOPeriod> periods = this.PeriodsDict[dItem.IdDistributor].Values.ToList();
                                periods = periods.Where(p => p.AggregationType == atItem).ToList();
                                if (atItem == Enums.AggregationType.Quarter)
                                {
                                    lastPeriod = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastStartTime.AddMinutes(-15) && p.EndTime == lastEndTime.AddMinutes(-15));
                                    if (lastPeriod == null)
                                        lastPeriod = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastStartTime.AddMinutes(-30) && p.EndTime == lastEndTime.AddMinutes(-30));
                                }
                                else if (atItem == Enums.AggregationType.Day)
                                {
                                    DateTime lastFullDay = ComputeLastFullDay(ReferenceDate, true, 1);
                                    lastPeriod = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastFullDay.AddDays(-1) && p.EndTime == lastFullDay);
                                    if(lastPeriod == null)
                                        lastPeriod = periods.Find(p => p.AggregationType == atItem && p.StartTime == lastFullDay.AddDays(-2) && p.EndTime == lastFullDay.AddDays(-1));
                                }
                                if (lastPeriod != null)
                                {                                    
                                    if (lastPeriod.LocationPerformanceDict != null && lastPeriod.LocationPerformanceDict.Count > 0)
                                    {
                                        foreach (KeyValuePair<long, List<GOPerformanceValue>> kvItem in lastPeriod.LocationPerformanceDict)
                                        {
                                            foreach (GOPerformanceValue gopvItem in kvItem.Value)
                                            {
                                                if (!CurrentLocationPerformancesValues.ContainsKey(kvItem.Key))
                                                    CurrentLocationPerformancesValues.Add(kvItem.Key, new List<GOPerformanceValue>());
                                                GOPerformanceValue gopvLatest = new GOPerformanceValue(gopvItem.IdDataType, gopvItem.Value, gopvItem.IndexNbr);
                                                gopvLatest.AggregationType = atItem;
                                                gopvLatest.StartTime = lastPeriod.StartTime;
                                                gopvLatest.EndTime = lastPeriod.EndTime;
                                                CurrentLocationPerformancesValues[kvItem.Key].Add(gopvLatest);
                                            }
                                        }
                                    }
                                    if (lastPeriod.DevicePerformanceDict != null && lastPeriod.DevicePerformanceDict.Count > 0)
                                    {
                                        foreach (KeyValuePair<long, List<GOPerformanceValue>> kvItem in lastPeriod.DevicePerformanceDict)
                                        {
                                            foreach (GOPerformanceValue gopvItem in kvItem.Value)
                                            {
                                                if (!CurrentDevicePerformancesValues.ContainsKey(kvItem.Key))
                                                    CurrentDevicePerformancesValues.Add(kvItem.Key, new List<GOPerformanceValue>());
                                                GOPerformanceValue gopvLatest = new GOPerformanceValue(gopvItem.IdDataType, gopvItem.Value, gopvItem.IndexNbr);
                                                gopvLatest.AggregationType = atItem;
                                                gopvLatest.StartTime = lastPeriod.StartTime;
                                                gopvLatest.EndTime = lastPeriod.EndTime;
                                                CurrentDevicePerformancesValues[kvItem.Key].Add(gopvLatest);
                                            }
                                        }
                                    }
                                    if (lastPeriod.DistributorPerformanceDict != null && lastPeriod.DistributorPerformanceDict.Count > 0)
                                    {
                                        foreach (KeyValuePair<int, List<OpDistributorPerformance>> kvItem in lastPeriod.DistributorPerformanceDict)
                                        {
                                            if (!CurrentDistributorPerformancesValues.ContainsKey(kvItem.Key))
                                                CurrentDistributorPerformancesValues.Add(kvItem.Key, new List<GOPerformanceValue>());
                                            foreach (OpDistributorPerformance dpItem in kvItem.Value)
                                            {
                                                GOPerformanceValue gopvLatest = new GOPerformanceValue(dpItem.IdDataType, Convert.ToDouble(dpItem.Value), dpItem.IndexNbr);
                                                gopvLatest.AggregationType = atItem;
                                                gopvLatest.DataSourceType = Enums.DataSourceType.Interface;
                                                gopvLatest.StartTime = dpItem.StartTime;
                                                gopvLatest.EndTime = dpItem.EndTime.Value;
                                                CurrentDistributorPerformancesValues[kvItem.Key].Add(gopvLatest);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion


                if (CurrentImrServerPerformancesValues.Count == 0
                    || !CurrentImrServerPerformancesValues.Exists(p => p.Value > 0))
                {
                    this.ServerMonitored = false;
                }
            }
        }

        public class GOPerformanceReportData
        {
            public GOPerformanceHierarchyItem HierarchyItem { get; private set; }
            public string TabDescr { get; private set; }
            public OpDataType DataType { get; private set; }
            public bool MainDataType { get; set; }
            public bool AttachHistory { get; set; }

            public GOPerformanceReportData(string TabDescr, OpDataType DataType, GOPerformanceHierarchyItem HierarchyItem)
            {
                this.DataType = DataType;
                this.TabDescr = TabDescr;
                this.HierarchyItem = HierarchyItem;
                if (DataType == null || HierarchyItem == null || String.IsNullOrEmpty(TabDescr))
                    throw new Exception("Nullable value");
                if (DataType.IdDataType != Convert.ToInt64(HierarchyItem.DataType.Value))
                    throw new Exception("Non matching data type ids");
            }

            public override string ToString()
            {
                return String.Format("{0} ({1})", DataType.ToString(), TabDescr);
            }
        }

        public class GOAggregateComputeType
        {
            public AggregateComputeType AggregateComputeType { get; private set; }
            public string ComboBoxString { get; private set; }
            public string Explanation { get; private set; }

            public GOAggregateComputeType(AggregateComputeType AggregateComputeType)
            {
                this.AggregateComputeType = AggregateComputeType;
                if (AggregateComputeType == PerformanceComponent.AggregateComputeType.AverageLowerAggregates)
                {
                    this.ComboBoxString = ResourcesText.AverageFromLowerPerformances;
                    this.Explanation = ResourcesText.AverageLowerAggregatesExplanation;
                }
                else if (AggregateComputeType == PerformanceComponent.AggregateComputeType.HigherAggregate)
                    this.ComboBoxString = ResourcesText.Unknown;
                else if (AggregateComputeType == PerformanceComponent.AggregateComputeType.ShorterPeriodAvg)
                    this.ComboBoxString = ResourcesText.Unknown;
                else if (AggregateComputeType == PerformanceComponent.AggregateComputeType.SimpleAvg)
                {
                    this.ComboBoxString = ResourcesText.Average;
                    this.Explanation = ResourcesText.AverageExplanation;
                }
                else if (AggregateComputeType == PerformanceComponent.AggregateComputeType.NotCalculate)
                {
                    this.ComboBoxString = ResourcesText.NotCalculate;
                    this.Explanation = ResourcesText.NotCalculateExplanation;
                }
            }

            public override string ToString()
            {
                return this.ComboBoxString;
            }
        }

        public class GOAlarmNotificationsReceivers
        {
            public OpChangeState OpState { get; set; }
            public string Email { get; set; }

            public override string ToString()
            {
                return this.Email;
            }
        }

        public class GOAlarmNotificationsReferenceValue
        {
            public OpDevice Device { get; set; }
            public OpLocation Location { get; set; }
            public OpMeter Meter { get; set; }
            public OpDistributor Distributor { get; set; }
            public OpReport Report { get; set; }
            public PerformanceComponent.GOProcess Process { get; set; }
            public PerformanceComponent.GOQueue Queue { get; set; }
            public Objects.DW.OpTransmissionDriver TransmissionDriver { get; set; }
            public GOETL Etl { get; set; }

            public string ReferenceValue { get; set; }
            public Enums.ReferenceType ReferenceType { get; set; }
            public string Type { get; set; }

            public GOAlarmNotificationsReferenceValue(string Type, OpDevice Device = null, OpLocation Location = null, OpMeter Meter = null, OpDistributor Distributor = null, OpReport Report = null, PerformanceComponent.GOProcess Process = null, PerformanceComponent.GOQueue Queue = null, Objects.DW.OpTransmissionDriver TransmissionDriver = null, GOETL Etl = null)
            {
                this.Device = Device;
                this.Location = Location;
                this.Meter = Meter;
                this.Distributor = Distributor;
                this.Report = Report;
                this.Queue = Queue;
                this.Process = Process;
                this.Type = Type;
                this.TransmissionDriver = TransmissionDriver;
                this.Etl = Etl;

                if (Device != null)
                {
                    this.ReferenceValue = Device.SerialNbr.ToString();
                    this.ReferenceType = Enums.ReferenceType.SerialNbr;
                }
                if (Distributor != null)
                {
                    this.ReferenceValue = Distributor.Name;
                    this.ReferenceType = Enums.ReferenceType.IdDistributor;
                }
                if (Location != null)
                {
                    this.ReferenceValue = Location.Name;
                    this.ReferenceType = Enums.ReferenceType.IdLocation;
                }
                if (Meter != null)
                {
                    this.ReferenceValue = (Meter.ProductCode != null ? Meter.ProductCode.ToString() + ", " : "") + (Meter.TankNbr != null ? Meter.TankNbr.ToString() + ", " : "") + (Meter.IdMeter != null ? Meter.IdMeter.ToString() : "");
                    this.ReferenceType = Enums.ReferenceType.IdMeter;
                }
                if (Report != null)
                {
                    this.ReferenceValue = Report.Name;
                    this.ReferenceType = Enums.ReferenceType.IdReport;
                }
                if (Queue != null)
                {
                    this.ReferenceValue = Queue.Name;
                    this.ReferenceType = Enums.ReferenceType.IdIMRServer;
                }
                if (Process != null)
                {
                    this.ReferenceValue = Process.Name;
                    this.ReferenceType = Enums.ReferenceType.IdIMRServer;
                }
                if (TransmissionDriver != null)
                {
                    this.ReferenceValue = "ID: " + TransmissionDriver.IdTransmissionDriver.ToString();
                    this.ReferenceType = Enums.ReferenceType.IdTransmissionDriver;
                }
                if (Etl != null)
                {
                    this.ReferenceValue = "ID: " + Etl.IdParam.ToString();
                    this.ReferenceType = Enums.ReferenceType.IdEtl;
                }
            }

            public override string ToString()
            {
                return this.ReferenceValue;
            }

            public override bool Equals(object obj)
            {
                if (obj is GOAlarmNotificationsReferenceValue)
                {
                    GOAlarmNotificationsReferenceValue objectDon = obj as GOAlarmNotificationsReferenceValue;
                    return String.Equals(this.ReferenceValue, objectDon.ReferenceValue);
                }
                else
                    return base.Equals(obj);
            }
        }

        public class GOQueue
        {
            public OpImrServerPerformance ImrServerPerformance { get; set; }
            public string Name { get; set; }
            public DateTime StartTime { get { return this.ImrServerPerformance.StartTime; } set { this.ImrServerPerformance.StartTime = value; } }
            public DateTime? EndTime { get { return this.ImrServerPerformance.EndTime; } set { this.ImrServerPerformance.EndTime = value; } }
            public int IdAggregationType { get { return this.ImrServerPerformance.IdAggregationType; } set { this.ImrServerPerformance.IdAggregationType = value; } }
            public long IdDataType { get { return this.ImrServerPerformance.IdDataType; } set { this.ImrServerPerformance.IdDataType = value; } }
            public int IdImrServer { get { return this.ImrServerPerformance.IdImrServer; } set { this.ImrServerPerformance.IdImrServer = value; } }
            public int IndexNbr { get { return this.ImrServerPerformance.IndexNbr; } set { this.ImrServerPerformance.IndexNbr = value; } }
            public Object Value { get { return this.ImrServerPerformance.Value; } set { this.ImrServerPerformance.Value = value; } }
            public long IdImrServerPerformance { get { return this.ImrServerPerformance.IdImrServerPerformance; } set { this.ImrServerPerformance.IdImrServerPerformance = value; } }
            public string ComboBoxString { get; private set; }

            public GOQueue(OpImrServerPerformance ImrServerPerformance, string Name)
            {
                this.ImrServerPerformance = ImrServerPerformance;
                this.Name = Name;
            }

            public override string ToString()
            {
                if (String.IsNullOrEmpty(Name))
                    if (ImrServerPerformance != null)
                        return ImrServerPerformance.IdImrServer + "_" + ImrServerPerformance.IndexNbr;
                    else
                        return "";
                else
                    return this.Name;
            }
        }

        public class GOProcess
        {
            public OpImrServerPerformance ImrServerPerformance { get; set; }
            public string Name { get; set; }
            public DateTime StartTime { get { return this.ImrServerPerformance.StartTime; } set { this.ImrServerPerformance.StartTime = value; } }
            public DateTime? EndTime { get { return this.ImrServerPerformance.EndTime; } set { this.ImrServerPerformance.EndTime = value; } }
            public int IdAggregationType { get { return this.ImrServerPerformance.IdAggregationType; } set { this.ImrServerPerformance.IdAggregationType = value; } }
            public long IdDataType { get { return this.ImrServerPerformance.IdDataType; } set { this.ImrServerPerformance.IdDataType = value; } }
            public int IdImrServer { get { return this.ImrServerPerformance.IdImrServer; } set { this.ImrServerPerformance.IdImrServer = value; } }
            public int IndexNbr { get { return this.ImrServerPerformance.IndexNbr; } set { this.ImrServerPerformance.IndexNbr = value; } }
            public Object Value { get { return this.ImrServerPerformance.Value; } set { this.ImrServerPerformance.Value = value; } }
            public long IdImrServerPerformance { get { return this.ImrServerPerformance.IdImrServerPerformance; } set { this.ImrServerPerformance.IdImrServerPerformance = value; } }

            public GOProcess(OpImrServerPerformance ImrServerPerformance, string Name)
            {
                this.ImrServerPerformance = ImrServerPerformance;
                this.Name = Name;
            }

            public override string ToString()
            {
                if (String.IsNullOrEmpty(Name))
                    if (ImrServerPerformance != null)
                        return ImrServerPerformance.IdImrServer + "_" + ImrServerPerformance.IndexNbr;
                    else
                        return "";
                else
                    return this.Name;
            }
        }

        public class GOAggregateType
        {
            public Enums.AggregationType AggregationType { get; set; }
            public string ComboBoxString { get; private set; }
            public string Explanation { get; private set; }

            public GOAggregateType(Enums.AggregationType AggregationType)
            {
                this.AggregationType = AggregationType;
                if (AggregationType == Enums.AggregationType.Quarter)
                {
                    ComboBoxString = ResourcesText.Quarter;
                    Explanation = ResourcesText.QuarterExplanation;
                }
                else
                    if (AggregationType == Enums.AggregationType.Day)
                {
                    ComboBoxString = ResourcesText.Day;
                    Explanation = ResourcesText.DayExplanation;
                }
                else if (AggregationType == Enums.AggregationType.Hour)
                {
                    ComboBoxString = ResourcesText.Hour;
                    Explanation = ResourcesText.HourExplanation;
                }
                else if (AggregationType == Enums.AggregationType.Week)
                {
                    ComboBoxString = ResourcesText.Week;
                    Explanation = ResourcesText.WeekExplanation;
                }
                else if (AggregationType == Enums.AggregationType.Month)
                {
                    ComboBoxString = ResourcesText.Month;
                    Explanation = ResourcesText.MonthExplanation;
                }
                else if (AggregationType == Enums.AggregationType.Year)
                {
                    ComboBoxString = ResourcesText.Year;
                    Explanation = ResourcesText.YearExplanation;
                }
            }

            public override string ToString()
            {
                return this.ComboBoxString;
            }
        }

        public class GOPerformancesReadoutType
        {
            public int IdDistributor { get { return (DeviceTypeData == null ? 0 : DeviceTypeData.IdDistributor); } }

            private OpChangeState _OpState;
            public OpChangeState OpState
            {
                get { return _OpState; }
                set
                {
                    _OpState = value;
                    MeterTypeClassData.OpState = value;
                    DeviceTypeData.OpState = value;
                    DataTypeData.OpState = value;
                    AcceptableReadoutDelay.OpState = value;
                }
            }

            private OpDistributorData MeterTypeClassData { get; set; }
            public int? IdMeterTypeClass
            {
                get
                {
                    int idMetreTypeClassTmp = 0;
                    if (MeterTypeClassData != null && MeterTypeClassData.Value != null && int.TryParse(MeterTypeClassData.Value.ToString(), out idMetreTypeClassTmp))
                    {
                        if (idMetreTypeClassTmp > 0)
                            return idMetreTypeClassTmp;
                        else
                            return null;
                    }
                    return null;
                }
                set
                {
                    this.MeterTypeClassData.Value = value;
                    this.MeterTypeClassData.OpState = Business.Objects.OpChangeState.Modified;
                    this.OpState = OpChangeState.Modified;
                }
            }
            private OpDistributorData DeviceTypeData { get; set; }
            public int IdDeviceTypeData
            {
                get
                {
                    return Convert.ToInt32(DeviceTypeData.Value);
                }
                set
                {
                    this.DeviceTypeData.Value = value;
                    this.DeviceTypeData.OpState = Business.Objects.OpChangeState.Modified;
                    this.OpState = OpChangeState.Modified;
                }
            }
            private OpDistributorData DataTypeData { get; set; }
            public long IdDataTypeData
            {
                get
                {
                    return Convert.ToInt64(DataTypeData.Value);
                }
                set
                {
                    this.DataTypeData.Value = value;
                    this.DataTypeData.OpState = Business.Objects.OpChangeState.Modified;
                    this.OpState = OpChangeState.Modified;
                }
            }
            private OpDistributorData AcceptableReadoutDelay { get; set; }
            public int AcceptableReadoutDelayValue
            {
                get
                {
                    int retValue = 0;
                    if (AcceptableReadoutDelay != null && AcceptableReadoutDelay.Value != null)
                    {
                        if (!int.TryParse(AcceptableReadoutDelay.Value.ToString(), out retValue))
                            retValue = 0;
                    }
                    return retValue;
                }
                set
                {
                    this.AcceptableReadoutDelay.Value = value;
                    this.AcceptableReadoutDelay.OpState = Business.Objects.OpChangeState.Modified;
                    this.OpState = OpChangeState.Modified;
                }
            }

            public GOPerformancesReadoutType(OpDistributorData MeterTypeClassData, OpDistributorData DeviceTypeData, OpDistributorData DataTypeData, OpDistributorData AcceptableReadoutDelay)
            {
                this.MeterTypeClassData = MeterTypeClassData;
                this.DeviceTypeData = DeviceTypeData;
                this.DataTypeData = DataTypeData;
                this.AcceptableReadoutDelay = AcceptableReadoutDelay;
            }
        }

        public class GOReportParamsDetails
        {
            public Enums.ReportType Type { get; set; }
            public string Name { get; set; }
            public int IdReport { get; set; }
            public string StoredProcedure { get; set; }
            public bool DataGenerationCheck { get; set; }
            public bool EmailSentCheck { get; set; }
            public bool FtpSentCheck { get; set; }
            public OpChangeState OpState { get; set; }
            public List<GOReportParamsGeneration> GenerationParamsList { get; set; }
            public int IndexNbr { get; set; }

            public GOReportParamsDetails()
            { }

            public override string ToString()
            {
                return this.Name;
            }
        }

        public class GOReportParamsGeneration
        {
            public int IdReport { get; set; }
            public int IndexNbr { get; set; }
            public DateTime FirstGenerationDate { get; set; }
            public int ScheduleFrequencyValue { get; set; }
            public GOReportUnit ScheduleFrequencyUnit { get; set; }
            public int ScheduleToleranceForwValue { get; set; }
            public GOReportUnit ScheduleToleranceForwUnit { get; set; }
            public DateTime? NextGenerationDate { get; set; }
            public OpChangeState OpState { get; set; }

            public GOReportParamsGeneration()
            { }

            public override bool Equals(object obj)
            {
                if (obj is GOReportParamsGeneration)
                {
                    GOReportParamsGeneration objectDon = obj as GOReportParamsGeneration;
                    return String.Equals(this.IndexNbr, objectDon.IndexNbr);
                }
                else
                    return base.Equals(obj);
            }

            public static bool operator ==(GOReportParamsGeneration left, GOReportParamsGeneration right)
            {
                if ((object)left == null)
                    return ((object)right == null);
                return left.Equals(right);
            }
            public static bool operator !=(GOReportParamsGeneration left, GOReportParamsGeneration right)
            {
                if ((object)left == null)
                    return ((object)right != null);
                return !left.Equals(right);
            }

            public override int GetHashCode()
            {
                return ((int)IndexNbr).GetHashCode();
            }
        }

        public class GOReportUnit
        {
            public Enums.ReportScheduleUnit Unit { get; set; }
            public string descr { get; set; }

            public GOReportUnit(Enums.ReportScheduleUnit Unit)
            {
                this.Unit = Unit;
                switch (Unit)
                {
                    case Enums.ReportScheduleUnit.Minute:
                        descr = ResourcesText.Minutes;
                        break;
                    case Enums.ReportScheduleUnit.Hour:
                        descr = ResourcesText.Hours;
                        break;
                    case Enums.ReportScheduleUnit.Day:
                        descr = ResourcesText.Days;
                        break;
                    case Enums.ReportScheduleUnit.Week:
                        descr = ResourcesText.Weeks;
                        break;
                    case Enums.ReportScheduleUnit.Month:
                        descr = ResourcesText.Months;
                        break;
                }
            }

            public override string ToString()
            {
                return this.descr;
            }
        }

        public class GOMonParamsResourceWatcherParamteres
        {
            private bool? _PercCPU;
            public bool? PercCPU
            {
                get
                {
                    if (this._PercCPU.HasValue)
                        return this._PercCPU.Value;
                    else
                        return null;
                }
                set { this._PercCPU = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; } }
            }
            private bool? _ByteRAM;
            public bool? ByteRAM
            {
                get
                {
                    if (this._ByteRAM.HasValue)
                        return this._ByteRAM.Value;
                    else
                        return null;
                }
                set { this._ByteRAM = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; } }
            }
            private int? _CPUThreshold;
            public int? CPUThreshold
            {
                get
                {
                    if (this._CPUThreshold.HasValue)
                        return this._CPUThreshold.Value;
                    else
                        return null;
                }
                set { this._CPUThreshold = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; } }
            }
            private int? _RAMThreshold;
            public int? RAMThreshold
            {
                get
                {
                    if (this._RAMThreshold.HasValue)
                        return this._RAMThreshold.Value;
                    else
                        return null;
                }
                set { this._RAMThreshold = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; } }
            }
            private int? _RAMThresholdPerc;
            public int? RAMThresholdPerc
            {
                get
                {
                    if (this._RAMThresholdPerc.HasValue)
                        return this._RAMThresholdPerc.Value;
                    else
                        return null;
                }
                set { this._RAMThresholdPerc = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; } }
            }
            private string _ProcessName;
            public string ProcessName
            {
                get
                {
                    return this._ProcessName;
                }
                set
                {
                    this._ProcessName = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; }
                }
            }
            public int Index { get; set; }
            public OpChangeState OpState { get; set; }

            public GOMonParamsResourceWatcherParamteres()
            {
            }
        }

        public class GOMonParamsQueueWatcherParamteres
        {
            private string _QueueName;
            public string QueueName
            {
                get
                {
                    return this._QueueName;
                }
                set
                {
                    this._QueueName = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; }
                }
            }
            private int _HiLevel;
            public int HiLevel
            {
                get
                {
                    return this._HiLevel;
                }
                set
                {
                    this._HiLevel = value; if (this.OpState == OpChangeState.Loaded) { this.OpState = OpChangeState.Modified; }
                }
            }
            public int Index { get; set; }
            public OpChangeState OpState { get; set; }


            public GOMonParamsQueueWatcherParamteres()
            {
            }
        }

        public class GODeviceSchedule
        {
            public DB_DEVICE_SCHEDULE DeviceSchedule { get; set; }
            public DateTime? NextDate { get; set; }

            public GODeviceSchedule(DB_DEVICE_SCHEDULE DeviceSchedule, DateTime? NextDate = null)
            {
                this.DeviceSchedule = DeviceSchedule;
                this.NextDate = NextDate;
            }
        }

        public class GOETL
        {
            public int IdParam
            {
                get
                {
                    if (this.coreETL != null)
                        return this.coreETL.IdParam;
                    else
                        return this.dwETL.IdParam;
                }
                set
                {
                    if (this.coreETL != null)
                        this.coreETL.IdParam = value;
                    else
                        this.dwETL.IdParam = value;
                }
            }
            public string Code
            {
                get
                {
                    if (this.coreETL != null)
                        return this.coreETL.Code;
                    else
                        return this.dwETL.Code;
                }
                set
                {
                    if (this.coreETL != null)
                        this.coreETL.Code = value;
                    else
                        this.dwETL.Code = value;
                }
            }
            public string Descr
            {
                get
                {
                    if (this.coreETL != null)
                        return this.coreETL.Descr;
                    else
                        return this.dwETL.Descr;
                }
                set
                {
                    if (this.coreETL != null)
                        this.coreETL.Descr = value;
                    else
                        this.dwETL.Descr = value;
                }
            }
            public object Value
            {
                get
                {
                    if (this.coreETL != null)
                        return this.coreETL.Value;
                    else
                        return this.dwETL.Value;
                }
                set
                {
                    if (this.coreETL != null)
                        this.coreETL.Value = value;
                    else
                        this.dwETL.Value = value;
                }
            }
            public Enums.DatabaseType DatabaseType
            {
                get
                {
                    if (this.coreETL != null)
                        return Enums.DatabaseType.CORE;
                    else
                        return Enums.DatabaseType.DW;
                }
            }

            public Objects.CORE.OpEtl coreETL { get; set; }

            //public Objects.DW.OpEtl dwETL { get; set; }
            public Objects.CORE.OpEtl dwETL { get; set; }

            //public GOETL(Objects.CORE.OpEtl coreETL)
            //{
            //    this.coreETL = coreETL;
            //}

            //public GOETL(Objects.DW.OpEtl dwETL)
            //{
            //    this.dwETL = dwETL;
            //}

            public override bool Equals(object obj)
            {
                if (obj is GOETL)
                {
                    GOETL objectDon = obj as GOETL;
                    return String.Equals(this.IdParam, objectDon.IdParam) & String.Equals(this.DatabaseType, objectDon.DatabaseType);
                }
                else
                    return base.Equals(obj);
            }

            public static bool operator ==(GOETL left, GOETL right)
            {
                if ((object)left == null)
                    return ((object)right == null);
                return left.Equals(right);
            }
            public static bool operator !=(GOETL left, GOETL right)
            {
                if ((object)left == null)
                    return ((object)right != null);
                return !left.Equals(right);
            }

            public override int GetHashCode()
            {
                if (this.coreETL != null)
                    return this.coreETL.GetHashCode();
                else
                    return this.dwETL.GetHashCode();
            }
        }

        public class GOMapping
        {
            public long IdMapping
            {
                get
                {
                    if (this.coreMapping != null)
                        return this.coreMapping.IdMapping;
                    else
                        return this.dwMapping.IdMapping;
                }
                set
                {
                    if (this.coreMapping != null)
                        this.coreMapping.IdMapping = value;
                    else
                        this.dwMapping.IdMapping = value;
                }
            }
            public int IdReferenceType
            {
                get
                {
                    if (this.coreMapping != null)
                        return this.coreMapping.IdReferenceType;
                    else
                        return this.dwMapping.IdReferenceType;
                }
                set
                {
                    if (this.coreMapping != null)
                        this.coreMapping.IdReferenceType = value;
                    else
                        this.dwMapping.IdReferenceType = value;
                }
            }
            public long IdSource
            {
                get
                {
                    if (this.coreMapping != null)
                        return this.coreMapping.IdSource;
                    else
                        return this.dwMapping.IdSource;
                }
                set
                {
                    if (this.coreMapping != null)
                        this.coreMapping.IdSource = value;
                    else
                        this.dwMapping.IdSource = value;
                }
            }
            public long IdDestination
            {
                get
                {
                    if (this.coreMapping != null)
                        return this.coreMapping.IdDestination;
                    else
                        return this.dwMapping.IdDestination;
                }
                set
                {
                    if (this.coreMapping != null)
                        this.coreMapping.IdDestination = value;
                    else
                        this.dwMapping.IdDestination = value;
                }
            }
            public int IdImrServer
            {
                get
                {
                    if (this.coreMapping != null)
                        return this.coreMapping.IdImrServer;
                    else
                        return this.dwMapping.IdImrServer;
                }
                set
                {
                    if (this.coreMapping != null)
                        this.coreMapping.IdImrServer = value;
                    else
                        this.dwMapping.IdImrServer = value;
                }
            }
            public DateTime Timestamp
            {
                get
                {
                    if (this.coreMapping != null)
                        return this.coreMapping.Timestamp;
                    else
                        return this.dwMapping.Timestamp;
                }
                set
                {
                    if (this.coreMapping != null)
                        this.coreMapping.Timestamp = value;
                    else
                        this.dwMapping.Timestamp = value;
                }
            }
            public Enums.DatabaseType DatabaseType
            {
                get
                {
                    if (this.coreMapping != null)
                        return Enums.DatabaseType.CORE;
                    else
                        return Enums.DatabaseType.DW;
                }
            }

            public Objects.CORE.OpMapping coreMapping { get; set; }

            public Objects.DW.OpMapping dwMapping { get; set; }

            public GOMapping(Objects.CORE.OpMapping coreMapping)
            {
                this.coreMapping = coreMapping;
            }

            public GOMapping(Objects.DW.OpMapping dwMapping)
            {
                this.dwMapping = dwMapping;
            }

            public override bool Equals(object obj)
            {
                if (obj is GOMapping)
                {
                    GOMapping objectDon = obj as GOMapping;
                    return String.Equals(this.IdMapping, objectDon.IdMapping) & String.Equals(this.DatabaseType, objectDon.DatabaseType);
                }
                else
                    return base.Equals(obj);
            }

            public static bool operator ==(GOMapping left, GOMapping right)
            {
                if ((object)left == null)
                    return ((object)right == null);
                return left.Equals(right);
            }
            public static bool operator !=(GOMapping left, GOMapping right)
            {
                if ((object)left == null)
                    return ((object)right != null);
                return !left.Equals(right);
            }

            public override int GetHashCode()
            {
                if (this.coreMapping != null)
                    return this.coreMapping.GetHashCode();
                else
                    return this.dwMapping.GetHashCode();
            }
        }

        public class GOMonitoredTypeOfAggregation
        {
            public Enums.AggregationType AggregationType { get; set; }
            public string ComboBoxString { get; private set; }
            public string Explanation { get; private set; }

            public GOMonitoredTypeOfAggregation(Enums.AggregationType AggregationType)
            {
                this.AggregationType = AggregationType;
                if (AggregationType == Enums.AggregationType.Quarter)
                {
                    ComboBoxString = ResourcesText.Quarter;
                    Explanation = ResourcesText.PresentingQuarterAggregatesInDot;
                }
                else
                    if (AggregationType == Enums.AggregationType.Day)
                {
                    ComboBoxString = ResourcesText.Day;
                    Explanation = ResourcesText.PresentingDailyAggregatesInDot;
                }
            }

            public override string ToString()
            {
                return this.ComboBoxString;
            }
        }

        #endregion
    }
}
