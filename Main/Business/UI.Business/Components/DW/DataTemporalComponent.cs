﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.DW;
using System;
using System.Linq;
using System.Collections.Generic;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class DataTemporalComponent
    {
        #region GetFuelConsumptionAggregatedByHour
        public static List<OpDataTemporal> GetFuelConsumptionAggregatedByHour(List<OpDataTemporal> list)
        {
            if (list == null) return new List<OpDataTemporal>();
            if (list.Count == 0) return list;

            // k1: IdMeter, k2: IdDataType, k3: DateTime HH:00, k4: Minute
            Dictionary<long, Dictionary<long, Dictionary<DateTime, Dictionary<int, List<double>>>>> tempMinuteDict = new Dictionary<long, Dictionary<long, Dictionary<DateTime, Dictionary<int, List<double>>>>>();
            foreach (OpDataTemporal dataTemporal in list)
            {
                if (!dataTemporal.IdMeter.HasValue
                    || !dataTemporal.StartTime.HasValue
                    || !dataTemporal.EndTime.HasValue
                    || dataTemporal.Value == null
                    || (dataTemporal.IdDataType != DataType.FUEL_CONSUMPTION
                        && dataTemporal.IdDataType != DataType.FUEL_CONSUMPTION_ESTIMATED)
                    || (dataTemporal.IdDataType == DataType.FUEL_CONSUMPTION_ESTIMATED
                        && (!dataTemporal.IdDataSourceType.HasValue || dataTemporal.IdDataSourceType.Value != (int)Enums.DataSourceType.Estimation))) continue;

                double value = GenericConverter.Parse<double>(dataTemporal.Value);
                double? valuePerMinute = null;

                long m = dataTemporal.IdMeter.Value;
                long d = dataTemporal.IdDataType;

                long totalMinutes = Convert.ToInt64((dataTemporal.EndTime.Value - dataTemporal.StartTime.Value).TotalMinutes);
                if (totalMinutes > 0)
                    valuePerMinute = value / totalMinutes;

                if (!valuePerMinute.HasValue) continue;

                DateTime startTime = dataTemporal.StartTime.Value;
                if (!tempMinuteDict.ContainsKey(m))
                    tempMinuteDict.Add(m, new Dictionary<long, Dictionary<DateTime, Dictionary<int, List<double>>>>());
                if (!tempMinuteDict[m].ContainsKey(d))
                    tempMinuteDict[m].Add(d, new Dictionary<DateTime, Dictionary<int, List<double>>>());

                while (startTime < dataTemporal.EndTime.Value)
                {
                    DateTime dateTime = new DateTime(startTime.Year, startTime.Month, startTime.Day, startTime.Hour, 0, 0);
                    if (!tempMinuteDict[m][d].ContainsKey(dateTime))
                        tempMinuteDict[m][d].Add(dateTime, new Dictionary<int, List<double>>());
                    if (!tempMinuteDict[m][d][dateTime].ContainsKey(startTime.Minute))
                        tempMinuteDict[m][d][dateTime].Add(startTime.Minute, new List<double>());
                    tempMinuteDict[m][d][dateTime][startTime.Minute].Add(valuePerMinute.Value);
                    startTime = startTime.AddMinutes(1);
                }
            }

            List<OpDataTemporal> retList = new List<OpDataTemporal>();
            foreach (var meterKeyValue in tempMinuteDict)
            {
                foreach (var dataTypeKeyValue in meterKeyValue.Value)
                {
                    foreach (var dateTimeKeyValue in dataTypeKeyValue.Value)
                    {
                        if (dateTimeKeyValue.Value.Count == 60)
                        {
                            double value = dateTimeKeyValue.Value.Select(q => q.Value.Sum()).Sum();
                            OpDataTemporal dataTemporal = new OpDataTemporal();
                            dataTemporal.IdDataType = dataTypeKeyValue.Key;
                            dataTemporal.IdMeter = meterKeyValue.Key;
                            dataTemporal.IndexNbr = 0;
                            dataTemporal.StartTime = dateTimeKeyValue.Key;
                            dataTemporal.EndTime = dateTimeKeyValue.Key.AddHours(1);
                            dataTemporal.Value = value;
                            if (dataTypeKeyValue.Key == DataType.FUEL_CONSUMPTION_ESTIMATED)
                                dataTemporal.IdDataSourceType = (int)Enums.DataSourceType.Estimation;
                            retList.Add(dataTemporal);
                        }
                    }
                }
            }
            return retList;
        }
        #endregion
    }
}
