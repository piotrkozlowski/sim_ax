﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using Objects_DW = IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class DataComponentDW
    {
        #region Methods
        public static long Save(DataProvider dataProvider, Objects_DW.OpData objectToSave)
        {
            return dataProvider.SaveData(objectToSave);
        }
        #endregion
    }
}
