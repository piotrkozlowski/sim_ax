﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class DeliveryOrderComponent
    {
        public static OpDeliveryOrder GetByID(DataProvider dataProvider, OpDeliveryOrder deliveryOrder)
        {
            return dataProvider.GetDeliveryOrder(deliveryOrder.IdDeliveryOrder);
        }
        public static OpDeliveryOrder Save(DataProvider dataProvider, OpDeliveryOrder objectToSave)
        {
            dataProvider.SaveDeliveryOrder(objectToSave);

            return dataProvider.GetDeliveryOrder(objectToSave.IdDeliveryOrder);
        }
        public static void Delete(DataProvider dataProvider, OpDeliveryOrder objectToDelete)
        {
            dataProvider.DeleteDeliveryOrder(objectToDelete);
        }

        #region CommitDeliveryOrder
        public static void CommitDeliveryOrder(DataProvider dataProvider, Enums.DeliveryCommitMethod CommitMethod, int idOperator, int idDistributor, OpDeliveryOrder deliveryOrder, int idProductCode,
            DateTime commitDate, Enums.Language language, int commandTimeout, out bool isSuccess, out string result, out string orderNo, out int errorCode)
        {
            isSuccess = false;
            errorCode = 0;
            result = null;
            orderNo = null;
            OpDeliveryOrderHistory orderHistory = new OpDeliveryOrderHistory();

            try
            {
                switch (CommitMethod)
                {
                    case Enums.DeliveryCommitMethod.Unknown:
                    case Enums.DeliveryCommitMethod.Simple:
                    case Enums.DeliveryCommitMethod.File:
                        {
                            isSuccess = true;
                            orderNo = null;
                            result = null;

                            break;

                        }
                    case Enums.DeliveryCommitMethod.Lomosoft:
                        {
                            try
                            {
                                dataProvider.dbConnectionDw.AddDeliveryToLomosoft(idOperator, deliveryOrder.IdLocation, deliveryOrder.IdMeter, deliveryOrder.DeliveryVolume,
                                                idProductCode, deliveryOrder.IdDeliveryAdvice, commitDate, commandTimeout, out isSuccess, out orderNo, out result, out errorCode);
                                if (!isSuccess && errorCode > 0)
                                {
                                    result = GetErrorCodeDescr(errorCode, language);
                                }
                            }
                            catch (Exception ex)
                            {
                                result = ex.Message;
                            }

                            break;
                        }
                    case Enums.DeliveryCommitMethod.GBS:
                    default:
                        {
                            result = "Unknown commit method for distributor " + dataProvider.GetDistributor(idDistributor);

                            break;
                        }
                }
                    

                deliveryOrder.IsSuccesfull = isSuccess;
                deliveryOrder.OrderNo = orderNo;
                Save(dataProvider, deliveryOrder);

                orderHistory.IdDeliveryOrder = deliveryOrder.IdDeliveryOrder;
                orderHistory.IdOperator = idOperator;
                orderHistory.CommitDate = commitDate;
                orderHistory.CommitResult = result;
                orderHistory.IsSuccesfull = isSuccess;
                orderHistory.Notes = null;
            }
            catch (Exception ex)
            {
                result = ex.Message;
                orderHistory.IsSuccesfull = false;
                orderHistory.CommitResult = result;
            }
            finally
            {
                dataProvider.SaveDeliveryOrderHistory(orderHistory);
            }
        }
        #endregion
        #region GetErrorCodeDescr
        private static string GetErrorCodeDescr(int errorCode, Enums.Language language)
        {
            switch (language)
            {
                case Enums.Language.Polish:
                    switch (errorCode)
                    {
                        case 1:
                            return "Nieprawidłowa wartość dostawy [l]";
                        case 2:
                            return "Brak operatora albo nie ma takiego na serwerze";
                        case 3:
                            return "Brak lokalizacji na serwerze";
                        case 4:
                            return "Brak zbiornika na serwerze";
                        case 5:
                            return "Brak id_delivery_advice albo nie ma takiego na serwerze";
                        case 6:
                            return "Brak id_product_code albo nie ma takiego na serwerze";
                        case 7:
                            return "Brak lomosoft customer number dla lokalizacji";
                        case 8:
                            return "Brak lomosoft site number dla lokalizacji";
                        case 9:
                            return "Brak lomosoft tank id dla lokalizacji (LPG) lub metera (OIL)";
                        default:
                            return "Nieznany kod błędu";
                    }
                    break;
                default:
                    switch (errorCode)
                    {
                        case 1:
                            return "Wrong ullage value";
                        case 2:
                            return "Operator not found";
                        case 3:
                            return "Location not found";
                        case 4:
                            return "Tank not found";
                        case 5:
                            return "Wrong/Missing Delivery Advice";
                        case 6:
                            return "Wrong/Missing Product Code";
                        case 7:
                            return "Lomosoft customer numer not found";
                        case 8:
                            return "Lomosoft site number not found";
                        case 9:
                            return "Lomosoft tank identifier not found";
                        default:
                            return "Unknown error code";
                    }
                    break;
            }
        }
        #endregion

        #region UpdateLocationDeliveryAdvice
        public static void UpdateLocationDeliveryAdvice(DataProvider dataProvider, long idLocation, Enums.DeliveryAdvice deliveryAdvice, int[] indexes)
        {
            List<OpData> dataList = dataProvider.GetDataFilterDW(IdLocation: new long[] { idLocation }, IdDataType: new long[] { DataType.LOCATION_DELIVERY_ADVICE });
            foreach (int index in indexes)
            {
                OpData opData = dataList.FirstOrDefault(d => d.Index == index) ?? new OpData();
                opData.Index = index;
                opData.IdLocation = idLocation;
                opData.Value = (int)deliveryAdvice;
                opData.Time = dataProvider.DateTimeNow;
                opData.IdDataType = DataType.LOCATION_DELIVERY_ADVICE;
                opData.DataStatus = Enums.DataStatus.Normal;
                dataProvider.SaveData(opData);
            }

        }
        #endregion
        #region UpdateMeterDeliveryAdvice
        public static void UpdateMeterDeliveryAdvice(DataProvider dataProvider, long idMeter, Enums.DeliveryAdvice deliveryAdvice, int[] indexes)
        {
            List<OpData> dataList = dataProvider.GetDataFilterDW(IdMeter: new long[] { idMeter }, IdDataType: new long[] { DataType.METER_DELIVERY_ADVICE }).ToList();
            foreach (int index in indexes)
            {
                OpData opData = dataList.FirstOrDefault(d => d.Index == index) ?? new OpData();
                opData.Index = index;
                opData.IdMeter = idMeter;
                opData.Value = (int)deliveryAdvice;
                opData.Time = dataProvider.DateTimeNow;
                opData.IdDataType = DataType.METER_DELIVERY_ADVICE;
                opData.DataStatus = Enums.DataStatus.Normal;
                dataProvider.SaveData(opData);
            }
        }
        #endregion
    }
}
