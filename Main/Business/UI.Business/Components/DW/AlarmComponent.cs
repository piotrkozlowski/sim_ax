﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.Custom;
using DW_OBJ = IMR.Suite.UI.Business.Objects.DW;
using CORE_OBJ = IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class AlarmComponent
    {
        #region Methods

        #region GetAlarmHistory

        public static List<OpHistory> GetAlarmHistory(DataProvider dataProvider, CORE_OBJ.OpOperator loggedOperator, long IdAlarm)
        {
            List<OpHistory> hList = new List<OpHistory>();

            List<DW_OBJ.OpAlarmHistory> alarmHistory = dataProvider.GetAlarmHistoryFilterDW(autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                            IdAlarm: new long[] { IdAlarm });

            alarmHistory.ForEach(ah => hList.Add(new OpHistory(ah.StartTime, Enums.ObjectHistoryEventType.ChangedStatus, ah.AlarmStatus.ToString(), null) { EventObject = ah }));
            hList = hList.OrderByDescending(h => h.EventDate).ToList();
            return hList;
        }

        #endregion

        #region SetHelperValues
        public static void SetHelperValues(DataProvider dataProvider, DW_OBJ.OpAlarm alarm, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdAlarm, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_ALARM:
                        alarm.DataList.SetValue(DataType.HELPER_ID_ALARM, 0, alarm.IdAlarm);
                        break;
                    case DataType.HELPER_ID_ALARM_EVENT:
                        alarm.DataList.SetValue(DataType.HELPER_ID_ALARM_EVENT, 0, alarm.IdAlarmEvent);
                        break;
                    case DataType.HELPER_ID_ALARM_DEF:
                        alarm.DataList.SetValue(DataType.HELPER_ID_ALARM_DEF, 0, alarm.IdAlarmDef);
                        break;
                    case DataType.HELPER_SERIAL_NBR:
                        alarm.DataList.SetValue(DataType.HELPER_SERIAL_NBR, 0, alarm.SerialNbr);
                        break;
                    case DataType.HELPER_ID_LOCATION:
                        alarm.DataList.SetValue(DataType.HELPER_ID_LOCATION, 0, alarm.IdLocation);
                        break;
                    case DataType.HELPER_ID_METER:
                        alarm.DataList.SetValue(DataType.HELPER_ID_METER, 0, alarm.IdMeter);
                        break;
                    case DataType.HELPER_ID_ALARM_TYPE:
                        alarm.DataList.SetValue(DataType.HELPER_ID_ALARM_TYPE, 0, alarm.IdAlarmType);
                        break;
                    case DataType.HELPER_ID_DATA_TYPE_ALARM:
                        alarm.DataList.SetValue(DataType.HELPER_ID_DATA_TYPE_ALARM, 0, alarm.IdDataTypeAlarm);
                        break;
                    case DataType.HELPER_SYSTEM_ALARM_VALUE:
                        alarm.DataList.SetValue(DataType.HELPER_SYSTEM_ALARM_VALUE, 0, alarm.SystemAlarmValue);
                        break;
                    case DataType.HELPER_ALARM_VALUE:
                        alarm.DataList.SetValue(DataType.HELPER_ALARM_VALUE, 0, alarm.AlarmValue);
                        break;
                    case DataType.HELPER_ID_OPERATOR:
                        alarm.DataList.SetValue(DataType.HELPER_ID_OPERATOR, 0, alarm.IdOperator);
                        break;
                    case DataType.HELPER_ID_ALARM_STATUS:
                        alarm.DataList.SetValue(DataType.HELPER_ID_ALARM_STATUS, 0, alarm.IdAlarmStatus);
                        break;
                    case DataType.HELPER_TRANSMISSION_TYPE:
                        alarm.DataList.SetValue(DataType.HELPER_TRANSMISSION_TYPE, 0, alarm.TransmissionType);
                        break;
                    case DataType.HELPER_ID_TRANSMISSION_TYPE:
                        alarm.DataList.SetValue(DataType.HELPER_ID_TRANSMISSION_TYPE, 0, alarm.IdTransmissionType);
                        break;
                    case DataType.HELPER_ID_ALARM_GROUP:
                        alarm.DataList.SetValue(DataType.HELPER_ID_ALARM_GROUP, 0, alarm.IdAlarmGroup);
                        break;
                    default:
                        break;
                }
            }
            alarm.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }
        public static void SetHelperValues(DataProvider dataProvider, DW_OBJ.OpAlarmEvent alarmEvent, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdAlarmEvent, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_ALARM_EVENT:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ID_ALARM_EVENT, 0, alarmEvent.IdAlarmEvent);
                        break;
                    case DataType.HELPER_ID_ALARM_DEF:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ID_ALARM_DEF, 0, alarmEvent.IdAlarmDef);
                        break;
                    case DataType.HELPER_SERIAL_NBR:
                        alarmEvent.DataList.SetValue(DataType.HELPER_SERIAL_NBR, 0, alarmEvent.SerialNbr);
                        break;
                    case DataType.HELPER_ID_LOCATION:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ID_LOCATION, 0, alarmEvent.IdLocation);
                        break;
                    case DataType.HELPER_ID_METER:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ID_METER, 0, alarmEvent.IdMeter);
                        break;
                    case DataType.HELPER_ID_ALARM_TYPE:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ID_ALARM_TYPE, 0, alarmEvent.IdAlarmType);
                        break;
                    case DataType.HELPER_ID_DATA_TYPE_ALARM:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ID_DATA_TYPE_ALARM, 0, alarmEvent.IdDataTypeAlarm);
                        break;
                    case DataType.HELPER_SYSTEM_ALARM_VALUE:
                        alarmEvent.DataList.SetValue(DataType.HELPER_SYSTEM_ALARM_VALUE, 0, alarmEvent.SystemAlarmValue);
                        break;
                    case DataType.HELPER_ALARM_VALUE:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ALARM_VALUE, 0, alarmEvent.AlarmValue);
                        break;
                    case DataType.HELPER_TIME:
                        alarmEvent.DataList.SetValue(DataType.HELPER_TIME, 0, alarmEvent.Time);
                        break;
                    case DataType.HELPER_ID_ISSUE:
                        alarmEvent.DataList.SetValue(DataType.HELPER_ID_ISSUE, 0, alarmEvent.IdIssue);
                        break;
                    case DataType.HELPER_IS_CONFIRMED:
                        alarmEvent.DataList.SetValue(DataType.HELPER_IS_CONFIRMED, 0, alarmEvent.IsConfirmed);
                        break;
                    case DataType.HELPER_CONFIRMED_BY:
                        alarmEvent.DataList.SetValue(DataType.HELPER_CONFIRMED_BY, 0, alarmEvent.ConfirmedBy);
                        break;
                    case DataType.HELPER_CONFIRM_TIME:
                        alarmEvent.DataList.SetValue(DataType.HELPER_CONFIRM_TIME, 0, alarmEvent.ConfirmTime);
                        break;
                    default:
                        break;
                }
            }
            alarmEvent.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }
        #endregion

        #endregion
    }
}
    