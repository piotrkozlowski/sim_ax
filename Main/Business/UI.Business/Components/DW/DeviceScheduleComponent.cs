﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class DeviceScheduleComponent
    {
        #region CheckDeviceSchedule
        public static List<long> CheckDeviceSchedule(DataProvider dataProvider, ref List<PerformanceComponent.GODeviceSchedule> deviceScheduleList, DateTime startTime, DateTime endTime, int commandCode)
        {
            // W wyliczeniach korzystamy z dat w UTC, natomiast koncowe next date wyznaczane jest w czasie lokalnym.  
            List<long> snWithSchedule = new List<long>();
            int c_dow = (int)startTime.DayOfWeek;
            int c_dom = startTime.Day;
            int c_h = startTime.Hour;
            int c_m = startTime.Minute;

            var tmpList = deviceScheduleList.GroupBy(d => new { d.DeviceSchedule.DAY_OF_MONTH, d.DeviceSchedule.DAY_OF_WEEK, d.DeviceSchedule.HOUR_OF_DAY, d.DeviceSchedule.MINUTE_OF_HOUR },
                                                                                                          (key, group) => new { DAY_OF_MONTH = key.DAY_OF_MONTH, DAY_OF_WEEK = key.DAY_OF_WEEK, HOUR_OF_DAY = key.HOUR_OF_DAY, MINUTE_OF_HOUR = key.MINUTE_OF_HOUR });

            //foreach (PerformanceComponent.GODeviceSchedule dsItem in deviceScheduleList)
            foreach (var vItem in tmpList)
            {
                DateTime time = startTime;
                time = time.AddHours(-time.Hour).AddMinutes(-time.Minute).AddSeconds(-time.Second);
                int dom = vItem.DAY_OF_MONTH;
                int dow = vItem.DAY_OF_WEEK;
                int h = vItem.HOUR_OF_DAY;
                int m = vItem.MINUTE_OF_HOUR;

                if (dow == 0)
                { }

                if (SchIsStopperOn(dom, dow, h, m))
                {
                    // Nie wykonujemy SET DATEFIRST 1, dlatego nie zmieniamy 
                    //if (dow == 0)
                    //    dow = 7;

                    if (m != 255)
                        time = time.AddMinutes(m);
                    else
                        time = time.AddMinutes(startTime.Minute);

                    if (h != 255)
                        time = time.AddHours(h);
                    else
                        time = time.AddHours(startTime.Hour);

                    if (dow == 255 && dom == 255)
                    {
                        if (time < startTime)
                            time = time.AddDays(1);
                    }

                    if (dow != 255)
                    {
                        if ((dow & 0x80) > 0)
                        {
                            //nowe wyznaczenia DOW na bitach
                            int id = 1;
                            List<Tuple<int, int, DateTime?>> daysOfWeek = new List<Tuple<int, int, DateTime?>>(); // <id, dow, next_date>
                            if ((@dow & 0x01) > 0)
                            {
                                daysOfWeek.Add(new Tuple<int, int, DateTime?>(id, 0, null));
                                id++;
                            }
                            if ((@dow & 0x02) > 0)
                            {
                                daysOfWeek.Add(new Tuple<int, int, DateTime?>(id, 1, null));
                                id++;
                            }
                            if ((@dow & 0x04) > 0)
                            {
                                daysOfWeek.Add(new Tuple<int, int, DateTime?>(id, 2, null));
                                id++;
                            }
                            if ((@dow & 0x08) > 0)
                            {
                                daysOfWeek.Add(new Tuple<int, int, DateTime?>(id, 3, null));
                                id++;
                            }
                            if ((@dow & 0x10) > 0)
                            {
                                daysOfWeek.Add(new Tuple<int, int, DateTime?>(id, 4, null));
                                id++;
                            }
                            if ((@dow & 0x20) > 0)
                            {
                                daysOfWeek.Add(new Tuple<int, int, DateTime?>(id, 5, null));
                                id++;
                            }
                            if ((@dow & 0x40) > 0)
                            {
                                daysOfWeek.Add(new Tuple<int, int, DateTime?>(id, 6, null));
                                id++;
                            }

                            int index = 1;
                            DateTime localTime = DateTime.MinValue;

                            while (daysOfWeek.Exists(e => e.Item1 == index))
                            {
                                dow = daysOfWeek.FirstOrDefault(f => f.Item1 == index).Item2;
                                if (dow >= c_dow)
                                    localTime = time.AddDays(dow - c_dow);
                                else
                                    localTime = time.AddDays(7 - (c_dow));

                                if (localTime < startTime)
                                    localTime = localTime.AddDays(7);

                                Tuple<int, int, DateTime?> tupleTmp = new Tuple<int, int, DateTime?>(index, dow, localTime);
                                daysOfWeek.RemoveAll(r => r.Item1 == index);
                                daysOfWeek.Add(tupleTmp);

                                index++;
                            }

                            if (daysOfWeek.Count > 0)
                                time = daysOfWeek.OrderBy(o => o.Item3).First().Item3.Value;
                        }
                        else
                        {
                            if (dow >= c_dow)
                                time = time.AddDays(dow - c_dow);
                            else
                                time = time.AddDays(7 - (c_dow - dow));

                            if (time < startTime)
                                time = time.AddDays(7);
                        }
                    }

                    if (dom != 255)
                    {
                        bool found = false;
                        int c_month = time.Month;
                        int c_year = time.Year;

                        while (!found)
                        {
                            int days_in_month = DateTime.DaysInMonth(c_year, c_month);
                            if (dow == 255)
                            {
                                if (dom > days_in_month)
                                    time = time.AddMonths(1);
                                else
                                {
                                    time = time.AddDays(-time.Day);
                                    time = time.AddDays(dom);
                                    found = true;
                                }
                            }
                            else
                            {
                                if (dom > days_in_month)
                                    time = time.AddMonths(1);
                                else
                                {
                                    time = time.AddDays(-time.Day);
                                    time = time.AddDays(dom);

                                    if ((int)time.DayOfWeek != dow)
                                        time = time.AddMonths(1);
                                    else
                                        found = true;
                                }
                            }
                            c_month = time.Month;
                            c_year = time.Year;
                        }
                    }
                    //update next_time
                    deviceScheduleList.Where(f => f.DeviceSchedule.DAY_OF_MONTH == vItem.DAY_OF_MONTH &&
                                                  f.DeviceSchedule.DAY_OF_WEEK == vItem.DAY_OF_WEEK &&
                                                  f.DeviceSchedule.HOUR_OF_DAY == vItem.HOUR_OF_DAY &&
                                                  f.DeviceSchedule.MINUTE_OF_HOUR == vItem.MINUTE_OF_HOUR).ToList().ForEach(f => f.NextDate = dataProvider.ConvertUtcTimeToTimeZone(time));
                }
            }

            snWithSchedule = deviceScheduleList.Where(w => w.NextDate >= dataProvider.ConvertUtcTimeToTimeZone(startTime) && w.NextDate <= dataProvider.ConvertUtcTimeToTimeZone(endTime) /*&& w.DeviceSchedule.MINUTE_OF_HOUR != 255*/).Select(s => s.DeviceSchedule.SERIAL_NBR).ToList();

            // ZMIANA ZOSTAŁA UWZGLEDNIONA WYŻEJ if(m != 255) else ...
            // Rozwiązanie dla dystrybutorów gdzie urządzenia maja w schedulu ustawione Minute_Of_Hour na 255 - oznacza to że urzadzenie może się zgłosic w ciągu całej godziny w dowolnym momencie.
            // np. Hour_Of_Day = 5, Minute_Of_Hour = 255 tz. że urządzenie może się zgłosić od 5:00 do 6:00 więc w tych godzinach powinien być aktywny schedul.
            //snWithSchedule.AddRange(deviceScheduleList.Where(w => DataProvider.ConvertUtcTimeToTimeZone(startTime) >= w.NextDate && DataProvider.ConvertUtcTimeToTimeZone(endTime) <= w.NextDate.Value.AddHours(1) && w.DeviceSchedule.MINUTE_OF_HOUR == 255).Select(s => s.DeviceSchedule.SERIAL_NBR).ToList());

            return snWithSchedule;
        }
        #endregion

        #region GetLatestDeviceScheduleDateTime
        public static Dictionary<long, Tuple<DateTime, OpDeviceSchedule>> GetLatestDeviceScheduleDateTime(DataProvider dataProvider, List<OpDeviceSchedule> deviceScheduleList, int commandCode, Dictionary<long, int> serialNumberIdProtocolDict, bool convertToLocalTime)
        {
            return GetLatestDeviceScheduleDateTime(dataProvider, deviceScheduleList,
                dataProvider != null ? dataProvider.DateTimeUtcNow : DateTime.UtcNow,
                commandCode, serialNumberIdProtocolDict, convertToLocalTime);
        }
        public static Dictionary<long, Tuple<DateTime, OpDeviceSchedule>> GetLatestDeviceScheduleDateTime(DataProvider dataProvider, List<OpDeviceSchedule> deviceScheduleList, DateTime utcTime, int commandCode, Dictionary<long, int> serialNumberIdProtocolDict, bool convertToLocalTime)
        {
            Dictionary<long, Tuple<DateTime, OpDeviceSchedule>> dict = new Dictionary<long, Tuple<DateTime, OpDeviceSchedule>>();
            if (deviceScheduleList == null || deviceScheduleList.Count == 0 || !deviceScheduleList.Any(q => q.CommandCode == commandCode)
                || serialNumberIdProtocolDict == null || serialNumberIdProtocolDict.Count == 0) return dict;

            List<int> idProtocolList = serialNumberIdProtocolDict.Select(q => q.Value).Distinct().ToList();

            foreach (var groupedDeviceSchedule in deviceScheduleList.Where(q => q.CommandCode == commandCode).GroupBy(d => new { d.DayOfMonth, d.DayOfWeek, d.HourOfDay, d.MinuteOfHour },
                                                                                                          (key, group) => new { DayOfMonth = key.DayOfMonth, DayOfWeek = key.DayOfWeek, HourOfDay = key.HourOfDay, MinuteOfHour = key.MinuteOfHour }))
            {
                int dom = groupedDeviceSchedule.DayOfMonth;
                int dow = groupedDeviceSchedule.DayOfWeek;
                int h = groupedDeviceSchedule.HourOfDay;
                int m = groupedDeviceSchedule.MinuteOfHour;

                foreach (int idProtocol in idProtocolList)
                {
                    int newTimeYear = utcTime.Year;
                    int newTimeMonth = utcTime.Month;
                    int newTimeDay = utcTime.Day;
                    int newTimeHour = utcTime.Hour;
                    int newTimeMinute = utcTime.Minute;

                    switch (idProtocol)
                    {
                        #region ImrWan1 - todo
                        case (int)Enums.Protocol.ImrWan1:
                            break;
                        #endregion
                        #region ImrWan2
                        case (int)Enums.Protocol.ImrWan2:
                            if (SchIsStopperOn(dom, dow, h, m))
                            {
                                if (dom == 255)
                                {
                                    if (dow == 255)
                                    {
                                        if (h == 255)
                                        {
                                            if (m >= 61 && m <= 119) // [co (m - 60) minut rozpoczynajac od 0 -> 1970-01-01 00:00:00]
                                                CheckEveryXMinuteDateTime(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc), utcTime, false, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m - 60);
                                            else if (m >= 0 && m <= 59) // [codziennie o kazdej godzinie o minucie m]
                                                CheckMinuteDateTime(utcTime, false, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m, null, 0, false, true);
                                        }
                                        else if (h >= 0 && h <= 23)
                                        {
                                            newTimeHour = h;
                                            if (m == 255) // [codziennie o godzinie h co minute]
                                                CheckHourDateTime(utcTime, false, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, null, 1, true);
                                            else if (m >= 61 && m <= 119) // [codziennie o godzinie h co (m - 60) minut]
                                                CheckHourEveryXMinuteDateTime(utcTime, false, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, m - 60, null, 1, true);
                                            else if (m >= 0 && m <= 59) // [codziennie o godzinie hh:mm]
                                                CheckHourMinuteDateTime(utcTime, false, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, m, null, 1, true);
                                        }
                                    }
                                    else if (((dow >> 7) & 0x01) == 1)
                                    {
                                        List<int> daysOfWeek = GetDaysOfWeek(dow);
                                        bool timeBack = false;
                                        if (!daysOfWeek.Contains((int)utcTime.DayOfWeek)) // nie jest to ustawiony dzien tygodnia, cofamy sie aby znalezc taka date
                                        {
                                            DateTime tmp = FindLatestDateTime(utcTime, daysOfWeek);
                                            newTimeYear = tmp.Year;
                                            newTimeMonth = tmp.Month;
                                            newTimeDay = tmp.Day;
                                            newTimeHour = tmp.Hour;
                                            newTimeMinute = tmp.Minute;
                                            timeBack = true;
                                        }
                                        if (h == 255)
                                        {
                                            if (m == 255) // [kazdego ustawionego dnia tygodnia co minute]
                                                CheckDateTime(timeBack, ref newTimeHour, ref newTimeMinute);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia tygodnia co (m - 60) minut]
                                                CheckEveryXMinuteDateTime(utcTime, utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m - 60);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia tygodnia o kazdej godzinie o minucie m]
                                                CheckMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m, daysOfWeek, 7, true, false);
                                        }
                                        else if (h >= 0 && h <= 23)
                                        {
                                            newTimeHour = h;
                                            if (m == 255) // [kazdego ustawionego dnia tygodnia o godzinie h co minute]
                                                CheckHourDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, daysOfWeek, 0, false);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia tygodnia o godzinie h co (m - 60) minut]
                                                CheckHourEveryXMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, m - 60, daysOfWeek, 0, false);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia tygodnia o godzinie hh:mm]
                                                CheckHourMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, m, daysOfWeek, 0, false);
                                        }
                                    }
                                    else if (dow < 7 && dow >= 0)
                                    {
                                        bool timeBack = false;
                                        int dowDiff = (int)utcTime.DayOfWeek - dow;
                                        if (dowDiff != 0) // aktualny dzien tygodnia jest rozny od ustawionego, cofamy sie o dowDiff dni
                                        {
                                            if (dowDiff < 0)
                                                dowDiff = 7 + dowDiff;
                                            DateTime tmp = utcTime.AddDays(-dowDiff);
                                            newTimeYear = tmp.Year;
                                            newTimeMonth = tmp.Month;
                                            newTimeDay = tmp.Day;
                                            newTimeHour = tmp.Hour;
                                            newTimeMinute = tmp.Minute;
                                            timeBack = true;
                                        }
                                        if (h == 255)
                                        {
                                            if (m == 255) // [kazdego jednego ustawionego dnia tygodnia co minute]
                                                CheckDateTime(timeBack, ref newTimeHour, ref newTimeMinute);
                                            else if (m >= 61 && m <= 119) // [kazdego jednego ustawionego dnia tygodnia co (m - 60) minut]
                                                CheckEveryXMinuteDateTime(utcTime, utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m - 60);
                                            else if (m >= 0 && m <= 59) // [kazdego jednego ustawionego dnia tygodnia o kazdej godzinie o minucie m]
                                                CheckMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m, new List<int>() { dow }, 7, true, true);
                                        }
                                        else if (h >= 0 && h <= 23)
                                        {
                                            newTimeHour = h;
                                            if (m == 255) // [kazdego jednego ustawionego dnia tygodnia o godzinie h co minute]
                                                CheckHourDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, null, 7, true);
                                            else if (m >= 61 && m <= 119) // [kazdego jednego ustawionego dnia tygodnia o godzinie h co (m - 60) minut]
                                                CheckHourEveryXMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, m - 60, new List<int>() { dow }, 0, false);
                                            else if (m >= 0 && m <= 59) // [kazdego jednego ustawionego dnia tygodnia o godzinie hh:mm]
                                                CheckHourMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, h, m, null, 7, true);
                                        }
                                    }
                                }
                                else if (dom >= 1 && dom <= 31)
                                {
                                    newTimeDay = dom;
                                    if (dow == 255)
                                    {
                                        bool timeBack = false;
                                        if (dom > utcTime.Day) // ustawiony dzien miesiaca jest wiekszy niz aktualny dzien miesiaca, cofamy sie o miesiac
                                        {
                                            DateTime tmp = FindLatestDateTime(utcTime.AddMonths(-1), dom);
                                            newTimeYear = tmp.Year;
                                            newTimeMonth = tmp.Month;
                                            newTimeHour = tmp.Hour;
                                            newTimeMinute = tmp.Minute;
                                            timeBack = true;
                                        }
                                        if (h == 255)
                                        {
                                            if (m == 255) // [kazdego ustawionego dnia miesiaca co minute]
                                                CheckDateTime(timeBack, ref newTimeHour, ref newTimeMinute);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia miesiaca co (m - 60) minut]
                                                CheckEveryXMinuteDateTime(utcTime, utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m - 60);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia miesiaca o kazdej godzinie o minucie m]
                                                CheckMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeHour, ref newTimeMinute, dom, m, null, true);
                                        }
                                        else if (h >= 0 && h <= 23)
                                        {
                                            newTimeHour = h;
                                            if (m == 255) // [kazdego ustawionego dnia miesiaca o godzinie h co minute]
                                                CheckHourDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeMinute, dom, h, null, true);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia miesiaca o godzinie h co (m - 60) minut]
                                                CheckHourEveryXMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, dom, h, m - 60, null, true);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia miesiaca o godzinie hh:mm]
                                                CheckHourMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeMinute, dom, h, m, null, true);
                                        }
                                    }
                                    else if (((dow >> 7) & 0x01) == 1)
                                    {
                                        List<int> daysOfWeek = GetDaysOfWeek(dow);
                                        bool timeBack = false;
                                        if (dom != utcTime.Day || !daysOfWeek.Contains((int)utcTime.DayOfWeek)) // nie jest to ustawiony dzien miesiaca i/lub nie jest to ustawiony dzien tygodnia, cofamy sie aby znalezc taka date
                                        {
                                            DateTime tmp = FindLatestDateTime(utcTime, dom, daysOfWeek);
                                            newTimeYear = tmp.Year;
                                            newTimeMonth = tmp.Month;
                                            newTimeHour = tmp.Hour;
                                            newTimeMinute = tmp.Minute;
                                            timeBack = true;
                                        }
                                        if (h == 255)
                                        {
                                            if (m == 255) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionych dniach tygodnia co minute]
                                                CheckDateTime(timeBack, ref newTimeHour, ref newTimeMinute);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionych dniach tygodnia co (m - 60) minut]
                                                CheckEveryXMinuteDateTime(utcTime, utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m - 60);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionych dniach tygodnia o kazdej godzinie o minucie m]
                                                CheckMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeHour, ref newTimeMinute, dom, m, daysOfWeek, false);
                                        }
                                        else if (h >= 0 && h <= 23)
                                        {
                                            newTimeHour = h;
                                            if (m == 255) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionych dniach tygodnia o godzinie h co minute]
                                                CheckHourDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeMinute, dom, h, daysOfWeek, false);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionych dniach tygodnia o godzinie h co (m - 60) minut]
                                                CheckHourEveryXMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, dom, h, m - 60, daysOfWeek, false);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionych dniach tygodnia o godzinie hh:mm]
                                                CheckHourMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeMinute, dom, h, m, daysOfWeek, false);
                                        }
                                    }
                                    else if (dow < 7 && dow >= 0)
                                    {
                                        bool timeBack = false;
                                        if (dom != utcTime.Day || dow != (int)utcTime.DayOfWeek) // nie jest to ustawiony dzien miesiaca i/lub nie jest to ustawiony dzien tygodnia, cofamy sie aby znalezc taka date
                                        {
                                            DateTime tmp = FindLatestDateTime(utcTime, dom, dow);
                                            newTimeYear = tmp.Year;
                                            newTimeMonth = tmp.Month;
                                            newTimeHour = tmp.Hour;
                                            newTimeMinute = tmp.Minute;
                                            timeBack = true;
                                        }
                                        if (h == 255)
                                        {
                                            if (m == 255) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionym dniu tygodnia co minute]
                                                CheckDateTime(timeBack, ref newTimeHour, ref newTimeMinute);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionym dniu tygodnia co (m - 60) minut]
                                                CheckEveryXMinuteDateTime(utcTime, utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeHour, ref newTimeMinute, m - 60);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionym dniu tygodnia o kazdej godzinie o minucie m]
                                                CheckMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeHour, ref newTimeMinute, dom, m, new List<int>() { dow }, false);
                                        }
                                        else if (h >= 0 && h <= 23)
                                        {
                                            newTimeHour = h;
                                            if (m == 255) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionym dniu tygodnia o godzinie h co minute]
                                                CheckHourDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeMinute, dom, h, new List<int>() { dow }, false);
                                            else if (m >= 61 && m <= 119) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionym dniu tygodnia o godzinie h co (m - 60) minut]
                                                CheckHourEveryXMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeDay, ref newTimeMinute, dom, h, m - 60, new List<int>() { dow }, false);
                                            else if (m >= 0 && m <= 59) // [kazdego ustawionego dnia miesiaca wypadajacego w ustawionym dniu tygodnia o godzinie hh:mm]
                                                CheckHourMinuteDateTime(utcTime, timeBack, ref newTimeYear, ref newTimeMonth, ref newTimeMinute, dom, h, m, new List<int>() { dow }, false);
                                        }
                                    }
                                }
                                
                                DateTime time = new DateTime(newTimeYear, newTimeMonth, newTimeDay, newTimeHour, newTimeMinute, 0, DateTimeKind.Utc);
                                if (convertToLocalTime)
                                    time = dataProvider != null ? dataProvider.ConvertUtcTimeToTimeZone(time) : time.ToLocalTime();
                                deviceScheduleList.Where(q => q.CommandCode == commandCode
                                    && q.DayOfMonth == groupedDeviceSchedule.DayOfMonth
                                    && q.DayOfWeek == groupedDeviceSchedule.DayOfWeek
                                    && q.HourOfDay == groupedDeviceSchedule.HourOfDay
                                    && q.MinuteOfHour == groupedDeviceSchedule.MinuteOfHour).ToList().ForEach(q =>
                                    {
                                        if (serialNumberIdProtocolDict.ContainsKey(q.SerialNbr)
                                            && serialNumberIdProtocolDict[q.SerialNbr] == idProtocol)
                                        {
                                            if (!dict.ContainsKey(q.SerialNbr))
                                                dict.Add(q.SerialNbr, new Tuple<DateTime, OpDeviceSchedule>(time, q));
                                            else if (dict[q.SerialNbr].Item1 < time)
                                                dict[q.SerialNbr] = new Tuple<DateTime, OpDeviceSchedule>(time, q);
                                        }
                                    });
                            }
                            break;
                        #endregion
                        #region ImrWan3 - todo
                        case (int)Enums.Protocol.ImrWan3:
                            break;
                        #endregion
                    }
                }
            }

            return dict;
        }
        #endregion

        #region CheckDateTime
        private static void CheckDateTime(bool timeBack, ref int newTimeHour, ref int newTimeMinute)
        {
            if (timeBack) // cofnieto czas
            {
                newTimeHour = 23;
                newTimeMinute = 59;
            }
        }
        #endregion
        #region CheckEveryXMinuteDateTime
        private static void CheckEveryXMinuteDateTime(DateTime startUtcTime, DateTime endUtcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeDay, ref int newTimeHour, ref int newTimeMinute, int minutes)
        {
            DateTime tmpStart = startUtcTime;
            DateTime tmpEnd = endUtcTime;
            if (timeBack) // cofnieto czas
            {
                tmpStart = new DateTime(newTimeYear, newTimeMonth, newTimeDay, 0, 0, 0, DateTimeKind.Utc);
                tmpEnd = new DateTime(newTimeYear, newTimeMonth, newTimeDay, 23, 59, 0, DateTimeKind.Utc);
            }
            else
            {
                tmpStart = new DateTime(tmpStart.Year, tmpStart.Month, tmpStart.Day, 0, 0, 0, DateTimeKind.Utc);
                tmpEnd = new DateTime(tmpEnd.Year, tmpEnd.Month, tmpEnd.Day, tmpEnd.Hour, tmpEnd.Minute, 0, DateTimeKind.Utc);
            }
            DateTime tmp = FindLatestDateTime(tmpStart, tmpEnd, minutes);
            newTimeYear = tmp.Year;
            newTimeMonth = tmp.Month;
            newTimeDay = tmp.Day;
            newTimeHour = tmp.Hour;
            newTimeMinute = tmp.Minute;
        }
        #endregion
        #region CheckMinuteDateTime
        private static void CheckMinuteDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeDay, ref int newTimeHour, ref int newTimeMinute, int minute, List<int> daysOfWeek, int days, bool checkNewTime, bool skipDaysOfWeek)
        {
            newTimeMinute = minute;
            if (timeBack) // cofnieto czas
                newTimeHour = 23;
            else
            {
                if (minute > utcTime.Minute) // ustawiona minuta jest wieksza niz minuta aktualnego czasu, cofamy sie
                {
                    DateTime tmp = utcTime.AddHours(-1);
                    newTimeYear = tmp.Year;
                    newTimeMonth = tmp.Month;
                    newTimeDay = tmp.Day;
                    newTimeHour = tmp.Hour;
                    if (checkNewTime)
                    {
                        if (!daysOfWeek.Contains((int)tmp.DayOfWeek)) // cofnieta data jest juz rozna od ustawionego dnia tygodnia, cofamy sie aby znalezc date
                        {
                            tmp = skipDaysOfWeek ? utcTime.AddDays(-days) : FindLatestDateTime(utcTime.AddDays(-1), daysOfWeek);
                            newTimeYear = tmp.Year;
                            newTimeMonth = tmp.Month;
                            newTimeDay = tmp.Day;
                            newTimeHour = 23;
                        }
                    }
                }
            }
        }
        private static void CheckMinuteDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeHour, ref int newTimeMinute, int dayOfMonth, int minute, List<int> daysOfWeek, bool skipDaysOfWeek)
        {
            newTimeMinute = minute;
            if (timeBack) // cofnieto czas
                newTimeHour = 23;
            else
            {
                if (minute > utcTime.Minute) // ustawiona minuta jest wieksza niz minuta aktualnego czasu, cofamy sie o godzine
                {
                    DateTime tmp = utcTime.AddHours(-1);
                    newTimeYear = tmp.Year;
                    newTimeMonth = tmp.Month;
                    newTimeHour = tmp.Hour;
                    if (dayOfMonth != tmp.Day) // nie jest to ustawiony dzien miesiaca, cofamy sie aby znalezc date
                    {
                        tmp = skipDaysOfWeek ? FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth) : FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth, daysOfWeek);
                        newTimeYear = tmp.Year;
                        newTimeMonth = tmp.Month;
                        newTimeHour = 23;
                    }
                }
            }
        }
        #endregion
        #region CheckHourEveryXMinuteDateTime
        private static void CheckHourEveryXMinuteDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeDay, ref int newTimeMinute, int hour, int minutes, List<int> daysOfWeek, int days, bool skipDaysOfWeek)
        {
            DateTime tmpStart = utcTime;
            DateTime tmpEnd = utcTime;
            if (timeBack) // cofnieto czas
            {
                tmpStart = new DateTime(newTimeYear, newTimeMonth, newTimeDay, hour, 0, 0, DateTimeKind.Utc);
                tmpEnd = new DateTime(newTimeYear, newTimeMonth, newTimeDay, hour, 59, 0, DateTimeKind.Utc);
            }
            else
            {
                if (hour > utcTime.Hour) // ustawiona godzina jest wieksza niz godzina aktualnego czasu, cofamy sie
                {
                    tmpStart = skipDaysOfWeek ? tmpStart.AddDays(-days) : FindLatestDateTime(utcTime.AddDays(-1), daysOfWeek);
                    tmpEnd = tmpStart;
                    tmpStart = new DateTime(tmpStart.Year, tmpStart.Month, tmpStart.Day, hour, 0, 0, DateTimeKind.Utc);
                    tmpEnd = new DateTime(tmpEnd.Year, tmpEnd.Month, tmpEnd.Day, hour, 59, 0, DateTimeKind.Utc);
                }
                else if (hour < utcTime.Hour)
                {
                    tmpStart = new DateTime(tmpStart.Year, tmpStart.Month, tmpStart.Day, hour, 0, 0, DateTimeKind.Utc);
                    tmpEnd = new DateTime(tmpEnd.Year, tmpEnd.Month, tmpEnd.Day, hour, 59, 0, DateTimeKind.Utc);
                }
                else
                {
                    tmpStart = new DateTime(tmpStart.Year, tmpStart.Month, tmpStart.Day, hour, 0, 0, DateTimeKind.Utc);
                    tmpEnd = new DateTime(tmpEnd.Year, tmpEnd.Month, tmpEnd.Day, hour, tmpEnd.Minute, 0, DateTimeKind.Utc);
                }
            }
            DateTime tmp = FindLatestDateTime(tmpStart, tmpEnd, minutes);
            newTimeYear = tmp.Year;
            newTimeMonth = tmp.Month;
            newTimeDay = tmp.Day;
            newTimeMinute = tmp.Minute;
        }
        private static void CheckHourEveryXMinuteDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeDay, ref int newTimeMinute, int dayOfMonth, int hour, int minutes, List<int> daysOfWeek, bool skipDaysOfWeek)
        {
            DateTime tmpStart = utcTime;
            DateTime tmpEnd = utcTime;
            if (timeBack) // cofnieto czas
            {
                tmpStart = new DateTime(newTimeYear, newTimeMonth, newTimeDay, hour, 0, 0, DateTimeKind.Utc);
                tmpEnd = new DateTime(newTimeYear, newTimeMonth, newTimeDay, hour, 59, 0, DateTimeKind.Utc);
            }
            else
            {
                if (hour > utcTime.Hour) // ustawiona godzina jest wieksza niz godzina aktualnego czasu, cofamy sie
                {
                    tmpStart = skipDaysOfWeek ? FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth) : FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth, daysOfWeek);
                    tmpEnd = tmpStart;
                    tmpStart = new DateTime(tmpStart.Year, tmpStart.Month, tmpStart.Day, hour, 0, 0, DateTimeKind.Utc);
                    tmpEnd = new DateTime(tmpEnd.Year, tmpEnd.Month, tmpEnd.Day, hour, 59, 0, DateTimeKind.Utc);
                }
                else if (hour < utcTime.Hour)
                {
                    tmpStart = new DateTime(tmpStart.Year, tmpStart.Month, tmpStart.Day, hour, 0, 0, DateTimeKind.Utc);
                    tmpEnd = new DateTime(tmpEnd.Year, tmpEnd.Month, tmpEnd.Day, hour, 59, 0, DateTimeKind.Utc);
                }
                else
                {
                    tmpStart = new DateTime(tmpStart.Year, tmpStart.Month, tmpStart.Day, hour, 0, 0, DateTimeKind.Utc);
                    tmpEnd = new DateTime(tmpEnd.Year, tmpEnd.Month, tmpEnd.Day, hour, tmpEnd.Minute, 0, DateTimeKind.Utc);
                }
            }
            DateTime tmp = FindLatestDateTime(tmpStart, tmpEnd, minutes);
            newTimeYear = tmp.Year;
            newTimeMonth = tmp.Month;
            newTimeDay = tmp.Day;
            newTimeMinute = tmp.Minute;
        }
        #endregion
        #region CheckHourDateTime
        private static void CheckHourDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeDay, ref int newTimeMinute, int hour, List<int> daysOfWeek, int days, bool skipDaysOfWeek)
        {
            if (timeBack) // cofnieto czas
                newTimeMinute = 59;
            else
            {
                if (hour > utcTime.Hour) // ustawiona godzina jest wieksza niz godzina aktualnego czasu, cofamy sie
                {
                    DateTime tmp = skipDaysOfWeek ? utcTime.AddDays(-days) : FindLatestDateTime(utcTime.AddDays(-1), daysOfWeek);
                    newTimeYear = tmp.Year;
                    newTimeMonth = tmp.Month;
                    newTimeDay = tmp.Day;
                    newTimeMinute = 59;
                }
                else if (hour < utcTime.Hour)
                    newTimeMinute = 59;
            }
        }
        private static void CheckHourDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeMinute, int dayOfMonth, int hour, List<int> daysOfWeek, bool skipDaysOfWeek)
        {
            if (timeBack) // cofnieto czas
                newTimeMinute = 59;
            else
            {
                if (hour > utcTime.Hour) // ustawiona godzina jest wieksza niz godzina aktualnego czasu, cofamy sie aby znalezc date
                {
                    DateTime tmp = skipDaysOfWeek ? FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth) : FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth, daysOfWeek);
                    newTimeYear = tmp.Year;
                    newTimeMonth = tmp.Month;
                    newTimeMinute = 59;
                }
                else if (hour < utcTime.Hour)
                    newTimeMinute = 59;
            }
        }
        #endregion
        #region CheckHourMinuteDateTime
        private static void CheckHourMinuteDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeDay, ref int newTimeMinute, int hour, int minute, List<int> daysOfWeek, int days, bool skipDaysOfWeek)
        {
            newTimeMinute = minute;
            if (!timeBack) // nie cofnieto czasu
            {
                if (hour > utcTime.Hour
                    || (hour == utcTime.Hour && minute > utcTime.Minute)) // ustawiony czas jest wiekszy niz aktualny czas, cofamy sie
                {
                    DateTime tmp = skipDaysOfWeek ? utcTime.AddDays(-days) : FindLatestDateTime(utcTime.AddDays(-1), daysOfWeek);
                    newTimeYear = tmp.Year;
                    newTimeMonth = tmp.Month;
                    newTimeDay = tmp.Day;
                }
            }
        }
        private static void CheckHourMinuteDateTime(DateTime utcTime, bool timeBack, ref int newTimeYear, ref int newTimeMonth, ref int newTimeMinute, int dayOfMonth, int hour, int minute, List<int> daysOfWeek, bool skipDaysOfWeek)
        {
            newTimeMinute = minute;
            if (!timeBack) // nie cofnieto czasu
            {
                if (hour > utcTime.Hour
                    || (hour == utcTime.Hour && minute > utcTime.Minute)) // ustawiony czas jest wiekszy niz aktualny czas, cofamy sie aby znalezc date
                {
                    DateTime tmp = skipDaysOfWeek ? FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth) : FindLatestDateTime(utcTime.AddMonths(-1), dayOfMonth, daysOfWeek);
                    newTimeYear = tmp.Year;
                    newTimeMonth = tmp.Month;
                }
            }
        }
        #endregion
        #region FindLatestDateTime
        private static DateTime FindLatestDateTime(DateTime startUtcTime, DateTime endUtcTime, int minutes)
        {
            DateTime tmp = startUtcTime;
            DateTime ret = startUtcTime;
            while (true)
            {
                if (tmp < endUtcTime)
                {
                    ret = tmp;
                    tmp = tmp.AddMinutes(minutes);
                }
                else
                    break;
            }
            return ret;
        }
        private static DateTime FindLatestDateTime(DateTime utcTime, List<int> daysOfWeek)
        {
            if (daysOfWeek == null || daysOfWeek.Count == 0) return utcTime;

            DateTime tmp = utcTime;
            while (true)
            {
                if (daysOfWeek.Contains((int)tmp.DayOfWeek))
                    break;
                else
                    tmp = tmp.AddDays(-1);
            }
            return tmp;
        }
        private static DateTime FindLatestDateTime(DateTime utcTime, int dayOfMonth)
        {
            DateTime tmp = utcTime;
            while (true)
            {
                if (dayOfMonth <= DateTime.DaysInMonth(tmp.Year, tmp.Month))
                    break;
                else
                    tmp = tmp.AddMonths(-1);
            }

            return tmp;
        }
        private static DateTime FindLatestDateTime(DateTime utcTime, int dayOfMonth, int dayOfWeek)
        {
            return FindLatestDateTime(utcTime, dayOfMonth, new List<int>() { dayOfWeek });
        }
        private static DateTime FindLatestDateTime(DateTime utcTime, int dayOfMonth, List<int> daysOfWeek)
        {
            if (daysOfWeek == null || daysOfWeek.Count == 0) return utcTime;

            DateTime tmp = utcTime;
            while (true)
            {
                if (dayOfMonth <= DateTime.DaysInMonth(tmp.Year, tmp.Month))
                {
                    tmp = new DateTime(tmp.Year, tmp.Month, dayOfMonth, tmp.Hour, tmp.Minute, 0);
                    if (!daysOfWeek.Contains((int)tmp.DayOfWeek))
                        tmp = tmp.AddMonths(-1);
                    else
                        break;
                }
                else
                    tmp = tmp.AddMonths(-1);
            }

            return tmp;
        }
        #endregion
        #region GetDaysOfWeek
        private static List<int> GetDaysOfWeek(int daysOfWeek)
        {
            List<int> list = new List<int>();
            for (int i = 0; i < 7; i++)
            {
                if ((daysOfWeek & (1 << i)) != 0)
                    list.Add(i);
            }
            return list;
        }
        #endregion

        #region SchIsStopperOn
        private static bool SchIsStopperOn(int date, int dow, int hour, int minute)
        {
            bool isStopperOn = false;
            if (date != 255)
            {
                isStopperOn = true;
                if (date > 31 || date < 1)
                    return false;
            }
            if (dow != 255)
            {
                isStopperOn = true;
                if (dow > 6 || dow < 0)
                    if ((dow & 0x80) == 0) // OKO55-C2 wprowadza bitowe oznaczenie dni, dlatego najpierw sprawdzamy czy DOW mieści sie z zakresu 0-6, jesli nie sprawdzamy czy jest zapalony pierwszy bit.
                        return false;
                    else if (GetDaysOfWeek(dow).Count == 0) // jest zapalony pierwszy bit, ale nic innego nie jest ustawione
                        return false;
            }
            if (hour != 255)
            {
                isStopperOn = true;
                if (hour > 23 || hour < 0)
                    return false;
            }
            if (minute != 255)
            {
                isStopperOn = true;
                if (minute < 0 || minute == 60 || minute > 119) // sa urzadzenia, dla ktorych mozna zapisac co ile minut jako 60+<ile minut> np. 75 jako co 15 minut
                    return false;
            }
            return isStopperOn;
        }
        #endregion
    }
}
