﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.DW
{
    public class DeliveryBatchComponent
    {
        public static OpDeliveryBatch GetByID(DataProvider dataProvider, int idDeliveryBatch, bool queryDatabase = false)
        {
            return dataProvider.GetDeliveryBatch(idDeliveryBatch, queryDatabase);
        }
        public static OpDeliveryBatch GetCurrent(DataProvider dataProvider, OpOperator opOperator, bool loadNavigationProperties = true)
        {
            return dataProvider.GetDeliveryBatchFilter(loadNavigationProperties: loadNavigationProperties, IdDeliveryBatchStatus: new int[] { (int)Enums.DeliveryBatchStatus.New }, IdOperator: new int[] { opOperator.IdOperator }).FirstOrDefault();
        }
        public static OpDeliveryBatch Save(DataProvider dataProvider, OpDeliveryBatch deliveryBatch)
        {
            dataProvider.SaveDeliveryBatch(deliveryBatch);
            return dataProvider.GetDeliveryBatch(deliveryBatch.IdDeliveryBatch);
        }
        public static List<OpDeliveryOrder> GetOrders(DataProvider dataProvider, OpDeliveryBatch deliveryBatch, bool loadNavigationProperties = true)
        {
            return dataProvider.GetDeliveryOrderFilter(loadNavigationProperties: loadNavigationProperties, IdDeliveryBatch: new int[] { deliveryBatch.IdDeliveryBatch }).OrderByDescending(d => d.CreationDate).ToList();
        }
        public static void Delete(DataProvider dataProvider, OpDeliveryBatch itemToDelete)
        {
            foreach (OpDeliveryOrder deliveryOrder in GetOrders(dataProvider, itemToDelete))
            {
                dataProvider.DeleteDeliveryOrder(deliveryOrder);
            }
            dataProvider.DeleteDeliveryBatch(itemToDelete);
        }

        public static void UpdateStats(DataProvider dataProvider, OpDeliveryBatch deliveryBatch, out int successTotal, out int failedTotal)
        {
            string customWhereClause = string.Format("ID_DELIVERY_BATCH = {0} AND IS_SUCCESFULL is not null ", deliveryBatch.IdDeliveryBatch);
            List<OpDeliveryOrder> deliveryOrders = dataProvider.GetDeliveryOrderFilter(customWhereClause: customWhereClause, autoTransaction: false);
            successTotal = deliveryBatch.SuccessTotal = deliveryOrders.Count(d => d.IsSuccesfull.HasValue && d.IsSuccesfull.Value);
            failedTotal = deliveryBatch.FailedTotal = deliveryOrders.Count(d => d.IsSuccesfull.HasValue && !d.IsSuccesfull.Value);
            dataProvider.SaveDeliveryBatch(deliveryBatch);
        }
    }
}
