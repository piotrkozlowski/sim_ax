﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Components.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.Custom;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Components.Custom
{
    public class DashboardComponent
    {
        public enum MapItemType
        {
            Unknown = 0,
            Marker = 1,
            Range = 2,
            Polygon = 3
        }

        #region GetOperatorDashboardConfiguration

        public static OpDashboard GetOperatorDashboardConfiguration(IDataProvider dataProvider, OpOperator opOperator,
            int? idGroup = null, Enums.DashboardItemClass? dashboardItemClass = null, List<long> idTypes = null,
            bool loadConfiguration = true)
        {
            OpDashboard returnDashboardConfiguration = new OpDashboard();
            List<OpDashboardControl> diList = new List<OpDashboardControl>();

            List<IOpData> ddList = new List<IOpData>();
            List<IOpData> odList = new List<IOpData>();
            Dictionary<long, OpDescr> descrDict = new Dictionary<long, OpDescr>();

            List<long> operatorDashboardDataTypes = new List<long>()
            {
                DataType.OPERATOR_DASHBOARD_INDICATOR_TYPE,
                DataType.OPERATOR_DASHBOARD_INDICATOR_CLASS,
                DataType.OPERATOR_DASHBOARD_INDICATOR_PROCEDURE,
                DataType.OPERATOR_DASHBOARD_INDICATOR_REFERENCE_TYPE,
                DataType.OPERATOR_DASHBOARD_INDICATOR_GROUP,
                DataType.OPERATOR_DASHBOARD_INDICATOR_WIDTH,
                DataType.OPERATOR_DASHBOARD_INDICATOR_IS_ALARM,
                DataType.OPERATOR_DASHBOARD_INDICATOR_ID_MODULE,
                DataType.OPERATOR_DASHBOARD_GROUP,
                DataType.OPERATOR_DASHBOARD_GROUP_NAME,
                DataType.OPERATOR_DASHBOARD_GROUP_ID_DESCR,
                DataType.OPERATOR_DASHBOARD_GROUP_BORDER_VISIBLE,
                DataType.OPERATOR_DASHBOARD_GROUP_WIDTH,
                DataType.OPERATOR_DASHBOARD_GROUP_REFRESH_PERIOD,
                DataType.OPERATOR_DASHBOARD_GROUP_ID_MODULE,
                DataType.OPERATOR_DASHBOARD_FIT_TO_SCREEN,
                DataType.OPERATOR_DASHBOARD_USE_MARGIN,
                DataType.OPERATOR_DASHBOARD_USE_PADDING,
                DataType.OPERATOR_DASHBOARD_KEEP_ASPECT_RATIO,
                DataType.OPERATOR_DASHBOARD_WIDTH,
                DataType.OPERATOR_DASHBOARD_MAX_WIDTH,
                DataType.OPERATOR_DASHBOARD_MAX_HEIGHT,
            };
            List<long> distributorDashboardDataTypes = new List<long>()
            {
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_TYPE,
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_CLASS,
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_PROCEDURE,
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_REFERENCE_TYPE,
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_GROUP,
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_WIDTH,
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_IS_ALARM,
                DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_ID_MODULE,
                DataType.DISTRIBUTOR_DASHBOARD_GROUP,
                DataType.DISTRIBUTOR_DASHBOARD_GROUP_NAME,
                DataType.DISTRIBUTOR_DASHBOARD_GROUP_ID_DESCR,
                DataType.DISTRIBUTOR_DASHBOARD_GROUP_BORDER_VISIBLE,
                DataType.DISTRIBUTOR_DASHBOARD_GROUP_WIDTH,
                DataType.DISTRIBUTOR_DASHBOARD_GROUP_REFRESH_PERIOD,
                DataType.DISTRIBUTOR_DASHBOARD_GROUP_ID_MODULE,
                DataType.DISTRIBUTOR_DASHBOARD_FIT_TO_SCREEN,
                DataType.DISTRIBUTOR_DASHBOARD_USE_MARGIN,
                DataType.DISTRIBUTOR_DASHBOARD_USE_PADDING,
                DataType.DISTRIBUTOR_DASHBOARD_KEEP_ASPECT_RATIO,
                DataType.DISTRIBUTOR_DASHBOARD_WIDTH,
                DataType.DISTRIBUTOR_DASHBOARD_MAX_WIDTH,
                DataType.DISTRIBUTOR_DASHBOARD_MAX_HEIGHT,
            };

            #region LoadConfiguration

            if (loadConfiguration)
            {

                ddList = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdDistributor: new int[] { opOperator.IdDistributor }, IdDataType: distributorDashboardDataTypes.ToArray()).Cast<IOpData>().ToList();
                odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdOperator: new int[] { opOperator.IdOperator }, IdDataType: operatorDashboardDataTypes.ToArray()).Cast<IOpData>().ToList();
            }
            else
            {
                if (opOperator.Distributor != null)
                    ddList = opOperator.Distributor.DataList.Where(d => d.IdDataType.In(distributorDashboardDataTypes.ToArray())).Cast<IOpData>().ToList();
                odList = opOperator.DataList.Where(d => d.IdDataType.In(operatorDashboardDataTypes.ToArray())).Cast<IOpData>().ToList();
            }
            #endregion
            #region LoadDescr

            List<long> idDescrToGet = new List<long>();
            List<long> idDescrDataTypesArray = new List<long>();
            idDescrDataTypesArray.Add(DataType.DISTRIBUTOR_DASHBOARD_GROUP_ID_DESCR);
            idDescrDataTypesArray.Add(DataType.OPERATOR_DASHBOARD_GROUP_ID_DESCR);
            idDescrDataTypesArray.Add(DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_TYPE);
            idDescrDataTypesArray.Add(DataType.OPERATOR_DASHBOARD_INDICATOR_TYPE);

            foreach (IOpData idItem in ddList.Concat(odList).Where(d => d.IdDataType.In(idDescrDataTypesArray.ToArray()) && d.Value != null))
            {
                long idDescrTmp = 0;
                if (long.TryParse(idItem.Value.ToString(), out idDescrTmp))
                    idDescrToGet.Add(idDescrTmp);
            }
            idDescrToGet = idDescrToGet.Distinct().ToList();
            idDescrToGet = idDescrToGet.Where(d => d >= 48201 && d <= 48500).ToList();
            if (idDescrToGet.Count > 0)
            {
                descrDict = dataProvider.GetDescr(idDescrToGet.Distinct().ToArray()).ToDictionary(d => d.IdDescr);
            }

            #endregion

            Dictionary<int, OpDashboardGroup> dgDict = new Dictionary<int, OpDashboardGroup>();

            #region AnalyzeDashboardProperties

            IOpData odFitToScreen = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_FIT_TO_SCREEN);
            IOpData odUseMargin = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_USE_MARGIN);
            IOpData odUsePadding = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_USE_PADDING);
            IOpData odKeepAspectRatio = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_KEEP_ASPECT_RATIO);
            IOpData odWidth = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_WIDTH);
            IOpData odMaxWidth = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_MAX_WIDTH);
            IOpData odMaxHeight = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_MAX_HEIGHT);

            if (odFitToScreen == null || odFitToScreen.Value == null)
                odFitToScreen = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_FIT_TO_SCREEN);
            if (odUseMargin == null || odUseMargin.Value == null)
                odUseMargin = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_USE_MARGIN);
            if (odUsePadding == null || odUsePadding.Value == null)
                odUsePadding = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_USE_PADDING);
            if (odKeepAspectRatio == null || odKeepAspectRatio.Value == null)
                odKeepAspectRatio = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_KEEP_ASPECT_RATIO);
            if (odWidth == null || odWidth.Value == null)
                odWidth = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_WIDTH);
            if (odMaxWidth == null || odMaxWidth.Value == null)
                odMaxWidth = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_MAX_WIDTH);
            if (odMaxHeight == null || odMaxHeight.Value == null)
                odMaxHeight = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_MAX_HEIGHT);

            returnDashboardConfiguration.FitToScreen = odFitToScreen != null && odFitToScreen.Value != null ? Convert.ToBoolean(odFitToScreen.Value) : false;
            returnDashboardConfiguration.UseMargin = odUseMargin != null && odUseMargin.Value != null ? Convert.ToBoolean(odUseMargin.Value) : false;
            returnDashboardConfiguration.UsePadding = odUsePadding != null && odUsePadding.Value != null ? Convert.ToBoolean(odUsePadding.Value) : false;
            returnDashboardConfiguration.KeepAspectRatio = odKeepAspectRatio != null && odKeepAspectRatio.Value != null ? Convert.ToBoolean(odKeepAspectRatio.Value) : false;
            returnDashboardConfiguration.Width = odWidth != null && odWidth.Value != null ? (int?)Convert.ToInt32(odWidth.Value) : null;
            returnDashboardConfiguration.MaxWidth = odMaxWidth != null && odMaxWidth.Value != null ? (int?)Convert.ToInt32(odMaxWidth.Value) : null;
            returnDashboardConfiguration.MaxHeight = odMaxHeight != null && odMaxHeight.Value != null ? (int?)Convert.ToInt32(odMaxHeight.Value) : null;

            #endregion

            #region AnalyzeOperatorData

            foreach (OpOperatorData odItem in odList.Where(w => w.IdDataType == DataType.OPERATOR_DASHBOARD_GROUP && w.Value != null)
                                                    .Distinct(d => Convert.ToInt32(d.Value)))
            {
                int idCurrentGroup = Convert.ToInt32(odItem.Value);

                if (idGroup.HasValue && idCurrentGroup != idGroup)
                    continue;

                List<int> groupIndexNbrList = odList.Where(w => w.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_GROUP && w.Value != null && Convert.ToInt32(w.Value) == Convert.ToInt32(odItem.Value))
                                                            .Select(s => Convert.ToInt32(s.Index))
                                                            .Distinct().ToList(); // Lista wszystkich indeksów w danej grupie


                OpDashboardGroup dgGroup = dgDict.TryGetValue(idCurrentGroup);
                if (dgGroup == null)
                {
                    IOpData odGroupName = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_GROUP_NAME && f.Index == odItem.Index); // może być nullem
                    IOpData odGroupIdDescr = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_GROUP_ID_DESCR && f.Index == odItem.Index && f.Value != null);
                    IOpData odGroupBorderVisible = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_GROUP_BORDER_VISIBLE && f.Index == odItem.Index && f.Value != null);
                    IOpData odGroupWidth = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_GROUP_WIDTH && f.Index == odItem.Index && f.Value != null);
                    IOpData odGroupRefreshPeriod = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_GROUP_REFRESH_PERIOD && f.Index == odItem.Index && f.Value != null);
                    IOpData odGroupIdModule = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_GROUP_ID_MODULE && f.Index == odItem.Index && f.Value != null);

                    string groupName = odGroupName.Value == null ? "Group_" + odItem.Index : odGroupName.Value.ToString();
                    OpDescr groupDescr = null;
                    long? idDescr = odGroupIdDescr != null && odGroupIdDescr.Value != null ? (long?)Convert.ToInt64(odGroupIdDescr.Value) : null;
                    bool isBorderVisible = odGroupBorderVisible != null && odGroupBorderVisible.Value != null ? Convert.ToBoolean(odGroupBorderVisible.Value) : false;
                    if (idDescr.HasValue && descrDict.ContainsKey(idDescr.Value) && descrDict[idDescr.Value] != null)
                        groupDescr = descrDict[idDescr.Value];
                    int refreshPeriod = 3 * 60;//domyślnie 3 minuty
                    if (odGroupRefreshPeriod != null && odGroupRefreshPeriod.Value != null)
                        int.TryParse(odGroupRefreshPeriod.Value.ToString(), out refreshPeriod);
                    int groupWidth = 100;//domyślnie 100%
                    if (odGroupWidth != null && odGroupWidth.Value != null)
                        int.TryParse(odGroupWidth.Value.ToString(), out groupWidth);
                    Enums.Module module = odGroupIdModule != null && odGroupIdModule.Value != null ? (Enums.Module)Convert.ToInt32(odGroupIdModule.Value) : Enums.Module.Unknown;


                    dgGroup = new OpDashboardGroup(idCurrentGroup, groupName, groupDescr, isBorderVisible, Module: module)
                    {
                        RefreshPeriod = refreshPeriod,
                        Width = groupWidth
                    };

                    dgDict[idCurrentGroup] = dgGroup;
                }

                if (groupIndexNbrList != null && groupIndexNbrList.Count > 0)
                {
                    foreach (int indexNbr in groupIndexNbrList)
                    {
                        IOpData odTypeIdDescr = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_TYPE && f.Index == indexNbr && f.Value != null);
                        IOpData odClass = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_CLASS && f.Index == indexNbr && f.Value != null);
                        IOpData odProcedure = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_PROCEDURE && f.Index == indexNbr && f.Value != null);
                        IOpData odReferenceType = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_REFERENCE_TYPE && f.Index == indexNbr); // może być nullem
                        IOpData odIndicatorWidth = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_WIDTH && f.Index == indexNbr);
                        IOpData odIsAlarm = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_IS_ALARM && f.Index == indexNbr);
                        IOpData odIdModule = odList.FirstOrDefault(f => f.IdDataType == DataType.OPERATOR_DASHBOARD_INDICATOR_ID_MODULE && f.Index == indexNbr);

                        long typeIdDescr = 0;
                        int? idReferenceType = null;
                        int idClass = 0;
                        string procedure = String.Empty;
                        int width = 100;
                        bool isAlarm = odIsAlarm != null && odIsAlarm.Value != null ? Convert.ToBoolean(odIsAlarm.Value) : false;
                        Enums.Module module = odIdModule != null && odIdModule.Value != null ? (Enums.Module)Convert.ToInt32(odIdModule.Value) : Enums.Module.Unknown;
                        if (odTypeIdDescr != null && odTypeIdDescr.Value != null)
                            long.TryParse(odTypeIdDescr.Value.ToString(), out typeIdDescr);
                        if (odReferenceType != null && odReferenceType.Value != null)
                        {
                            int tmpReferenceType = 0;
                            if (int.TryParse(odReferenceType.Value.ToString(), out tmpReferenceType))
                                idReferenceType = tmpReferenceType;
                        }
                        if (odClass != null && odClass.Value != null)
                            int.TryParse(odClass.Value.ToString(), out idClass);
                        if (odIndicatorWidth != null && odIndicatorWidth.Value != null)
                            int.TryParse(odIndicatorWidth.Value.ToString(), out width);
                        if (odProcedure != null && odProcedure.Value != null && !String.IsNullOrEmpty(odProcedure.Value.ToString().Trim()))
                            procedure = odProcedure.Value.ToString();

                        if (typeIdDescr > 0 && idClass >= 0 && !String.IsNullOrEmpty(procedure) && descrDict.ContainsKey(typeIdDescr))
                        {
                            OpDescr typeDescr = descrDict[typeIdDescr];
                            Enums.DashboardItemClass dicClass = (Enums.DashboardItemClass)idClass;

                            if (dashboardItemClass.HasValue && dashboardItemClass != dicClass)
                                continue;
                            if (idTypes != null && !idTypes.Exists(t => t == typeDescr.IdDescr))
                                continue;

                            OpDashboardControl diItem = new OpDashboardControl(typeDescr, dicClass, procedure,
                            idReferenceType.HasValue ? (Enums.ReferenceType?)idReferenceType.Value : null,
                            dgGroup, indexNbr, IsAlarm: isAlarm, Module: module)
                            {
                                Width = width
                            };

                            diList.Add(diItem);
                        }
                        else
                            continue;
                    }
                }
            }
            #endregion
            #region AnalyzeDistributorData

            foreach (OpDistributorData ddItem in ddList.Where(w => w.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_GROUP && w.Value != null)
                                                    .Distinct(d => Convert.ToInt32(d.Value)))
            {
                int idCurrentGroup = Convert.ToInt32(ddItem.Value);

                if (idGroup.HasValue && idCurrentGroup != idGroup)
                    continue;

                List<int> groupIndexNbrList = ddList.Where(w => w.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_GROUP && w.Value != null && Convert.ToInt32(w.Value) == Convert.ToInt32(ddItem.Value))
                                                            .Select(s => Convert.ToInt32(s.Index))
                                                            .Distinct().ToList(); // Lista wszystkich indeksów w danej grupie

                OpDashboardGroup dgGroup = dgDict.TryGetValue(idCurrentGroup);
                if (dgGroup == null)
                {
                    IOpData odGroupName = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_GROUP_NAME && f.Index == ddItem.Index); // może być nullem
                    IOpData odGroupIdDescr = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_GROUP_ID_DESCR && f.Index == ddItem.Index && f.Value != null);
                    IOpData odGroupBorderVisible = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_GROUP_BORDER_VISIBLE && f.Index == ddItem.Index && f.Value != null);
                    IOpData odGroupWidth = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_GROUP_WIDTH && f.Index == ddItem.Index && f.Value != null);
                    IOpData odGroupRefreshPeriod = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_GROUP_REFRESH_PERIOD && f.Index == ddItem.Index && f.Value != null);
                    IOpData odGroupIdModule = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_GROUP_ID_MODULE && f.Index == ddItem.Index && f.Value != null);

                    string groupName = odGroupName.Value == null ? "Group_" + ddItem.Index : odGroupName.Value.ToString();
                    OpDescr groupDescr = null;
                    long? idDescr = odGroupIdDescr != null && odGroupIdDescr.Value != null ? (long?)Convert.ToInt64(odGroupIdDescr.Value) : null;
                    bool isBorderVisible = odGroupBorderVisible != null && odGroupBorderVisible.Value != null ? Convert.ToBoolean(odGroupBorderVisible.Value) : false;
                    if (idDescr.HasValue && descrDict.ContainsKey(idDescr.Value) && descrDict[idDescr.Value] != null)
                        groupDescr = descrDict[idDescr.Value];
                    int refreshPeriod = 3 * 60;//domyślnie 3 minuty
                    if (odGroupRefreshPeriod != null && odGroupRefreshPeriod.Value != null)
                        int.TryParse(odGroupRefreshPeriod.Value.ToString(), out refreshPeriod);
                    int groupWidth = 100;//domyślnie 100%
                    if (odGroupWidth != null && odGroupWidth.Value != null)
                        int.TryParse(odGroupWidth.Value.ToString(), out groupWidth);
                    Enums.Module module = odGroupIdModule != null && odGroupIdModule.Value != null ? (Enums.Module)Convert.ToInt32(odGroupIdModule.Value) : Enums.Module.Unknown;

                    dgGroup = new OpDashboardGroup(idCurrentGroup, groupName, groupDescr, Convert.ToBoolean(odGroupBorderVisible.Value), Module: module)
                    {
                        RefreshPeriod = refreshPeriod,
                        Width = groupWidth
                    };
                    dgDict[idCurrentGroup] = dgGroup;
                }

                if (groupIndexNbrList != null && groupIndexNbrList.Count > 0)
                {
                    foreach (int indexNbr in groupIndexNbrList)
                    {
                        IOpData ddTypeIdDescr = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_TYPE && f.Index == indexNbr && f.Value != null);
                        IOpData ddClass = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_CLASS && f.Index == indexNbr && f.Value != null);
                        IOpData ddProcedure = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_PROCEDURE && f.Index == indexNbr && f.Value != null);
                        IOpData ddReferenceType = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_REFERENCE_TYPE && f.Index == indexNbr); // może być nullem
                        IOpData ddIndicatorWidth = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_WIDTH && f.Index == indexNbr); // może być nullem
                        IOpData odIsAlarm = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_IS_ALARM && f.Index == indexNbr);
                        IOpData odIdModule = ddList.FirstOrDefault(f => f.IdDataType == DataType.DISTRIBUTOR_DASHBOARD_INDICATOR_ID_MODULE && f.Index == indexNbr);

                        long typeIdDescr = 0;
                        int? idReferenceType = null;
                        int idClass = 0;
                        int width = 100;
                        bool isAlarm = odIsAlarm != null && odIsAlarm.Value != null ? Convert.ToBoolean(odIsAlarm.Value) : false;
                        Enums.Module module = odIdModule != null && odIdModule.Value != null ? (Enums.Module)Convert.ToInt32(odIdModule.Value) : Enums.Module.Unknown;
                        string procedure = String.Empty;
                        if (ddTypeIdDescr != null && ddTypeIdDescr.Value != null)
                            long.TryParse(ddTypeIdDescr.Value.ToString(), out typeIdDescr);
                        if (ddReferenceType != null && ddReferenceType.Value != null)
                        {
                            int tmpReferenceType = 0;
                            if (int.TryParse(ddReferenceType.Value.ToString(), out tmpReferenceType))
                                idReferenceType = tmpReferenceType;
                        }
                        if (ddClass != null && ddClass.Value != null)
                            int.TryParse(ddClass.Value.ToString(), out idClass);
                        if (ddIndicatorWidth != null && ddIndicatorWidth.Value != null)
                            int.TryParse(ddIndicatorWidth.Value.ToString(), out width);
                        if (ddProcedure != null && ddProcedure.Value != null && !String.IsNullOrEmpty(ddProcedure.Value.ToString().Trim()))
                            procedure = ddProcedure.Value.ToString();

                        if (typeIdDescr > 0 && idClass >= 0 && !String.IsNullOrEmpty(procedure) && descrDict.ContainsKey(typeIdDescr))
                        {
                            OpDescr typeDescr = descrDict[typeIdDescr];
                            Enums.DashboardItemClass dicClass = (Enums.DashboardItemClass)idClass;

                            OpDashboardControl diItem = new OpDashboardControl(typeDescr, dicClass, procedure,
                                idReferenceType.HasValue ? (Enums.ReferenceType?)idReferenceType.Value : null,
                                dgGroup, indexNbr, IsAlarm: isAlarm, Module: module)
                            {
                                Width = width
                            };

                            diList.Add(diItem);
                        }
                        else
                            continue;
                    }
                }
            }
            #endregion

            returnDashboardConfiguration.Controls = diList;

            return returnDashboardConfiguration;
        }

        #endregion

        #region LoadDashboardItemData

        public static List<OpDashboardControl> LoadDashboardItemData(IDataProvider dataProvider, OpOperator loggedOperator, List<OpDashboardControl> dashboardItems)
        {
            List<OpDashboardControl> retList = new List<OpDashboardControl>();
            foreach (Enums.DashboardItemClass dcItem in dashboardItems.Select(d => d.Class).Distinct())
            {
                foreach (string pItem in dashboardItems.Where(d => d.Class == dcItem).Select(d => d.Procedure).Distinct())
                {
                    List<OpDashboardControl> dashboardControls = dashboardItems.Where(d => d.Class == dcItem && String.Equals(d.Procedure, pItem))
                                                                               .OrderBy(d => d.Type.IdDescr)
                                                                               .ToList();
                    DataSet dsItem = dataProvider.GetDashboardData(pItem, dcItem, dashboardControls.Select(d => d.Type.IdDescr).ToArray(),
                                                                  loggedOperator.IdOperator,
                                                                  Language: dataProvider.UserLanguage,
                                                                  IdDistributor: dataProvider.DistributorFilter, IdLocation: dataProvider.LocationFilter,
                                                                  IdMeter: dataProvider.MeterFilter, SerialNbr: dataProvider.DeviceFilter,
                                                                  CommandTimeout: 300);
                    retList.AddRange(PrepareDashboardItemData(dataProvider, loggedOperator, dashboardControls, dsItem));
                }
            }
            return retList;
        }

        #endregion
        #region PrepareDashboardItemData

        public static List<OpDashboardControl> PrepareDashboardItemData(IDataProvider dataProvider, OpOperator loggedOperator, List<OpDashboardControl> dashboardControls, DataSet dataSet)
        {
            Dictionary<long, OpDashboardControl> retDict = new Dictionary<long, OpDashboardControl>();
            if (dashboardControls == null || dashboardControls.Count == 0)
                return retDict.Values.ToList();
            if (dashboardControls.Select(d => d.Class).Distinct().Count() > 1)
                throw new Exception("Cannot analyze data from more thant one class");

            Enums.DashboardItemClass dashboardItemClass = dashboardControls.First().Class;
            Enums.Module module = dashboardControls.FirstOrDefault().Module;

            if (dashboardItemClass == Enums.DashboardItemClass.TILE)
            {
                #region Tile
                if (dataSet.Tables != null && dataSet.Tables.Count > 0)
                {
                    for (int i = 0; i < dataSet.Tables.Count; i++)
                    {
                        if (i == 0 && dataSet.Tables[i].Rows != null && dataSet.Tables[i].Rows.Count > 0)
                        {
                            foreach (DataRow drItem in dataSet.Tables[i].Rows)
                            {
                                long idType = Convert.ToInt64(drItem.ItemArray[0]);
                                OpDashboardControl dcItem = dashboardControls.Find(d => d.IdType == idType);
                                if (dcItem == null)
                                    throw new Exception("Data mismatch");

                                string textHeader = drItem.ItemArray[1].ToString();
                                long amount = Convert.ToInt64(drItem.ItemArray[2]);

                                OpTileControl tcItem = new OpTileControl(textHeader, amount, dcItem);

                                if (drItem.ItemArray.Length > 3 && drItem.ItemArray[3] != null)
                                    try { tcItem.BackgroundColor = Color.FromArgb(Convert.ToInt32(drItem.ItemArray[3])); } catch { tcItem.BackgroundColor = null; }
                                if (drItem.ItemArray.Length > 4 && drItem.ItemArray[4] != null)
                                    try { tcItem.HoverColor = Color.FromArgb(Convert.ToInt32(drItem.ItemArray[4])); } catch { tcItem.HoverColor = null; }
                                if (drItem.ItemArray.Length > 5 && drItem.ItemArray[5] != null)
                                    try { tcItem.SelectColor = Color.FromArgb(Convert.ToInt32(drItem.ItemArray[5])); } catch { tcItem.SelectColor = null; }
                                if (drItem.ItemArray.Length > 6 && drItem.ItemArray[6] != null)
                                    try { tcItem.AmountVisible = Convert.ToBoolean(drItem.ItemArray[6]); } catch { /*zostaje tak jak w konstruktorze*/ }
                                if (drItem.ItemArray.Length > 7 && drItem.ItemArray[7] != null)
                                    try { tcItem.TexHeaderVisible = Convert.ToBoolean(drItem.ItemArray[7]); } catch { /*zostaje tak jak w konstruktorze*/ }
                                if (drItem.ItemArray.Length > 8 && drItem.ItemArray[8] != null)
                                    try { tcItem.IconVisible = Convert.ToBoolean(drItem.ItemArray[8]); } catch { /*zostaje tak jak w konstruktorze*/ }
                                if (drItem.ItemArray.Length > 9 && drItem.ItemArray[9] != null)
                                {
                                    try
                                    {
                                        OpFile iconFile = new OpFile();
                                        iconFile.FileBytes = (byte[])drItem.ItemArray[9];
                                        tcItem.Icon = iconFile;
                                    }
                                    catch { tcItem.Icon = null; }
                                }
                                if (drItem.ItemArray.Length > 10 && drItem.ItemArray[10] != null && tcItem.Icon == null)
                                {
                                    try
                                    {
                                        long idFile = 0;
                                        if (long.TryParse(drItem.ItemArray[10].ToString(), out idFile))
                                        {
                                            OpFile file = dataProvider.GetFile(idFile);
                                            if (file != null)
                                            {
                                                dataProvider.GetFileContent(file);
                                                tcItem.Icon = file;
                                            }
                                        }
                                    }
                                    catch { tcItem.Icon = null; }
                                }
                                if (drItem.ItemArray.Length > 11 && drItem.ItemArray[11] != null)
                                {
                                    try
                                    {
                                        OpFile iconFile = new OpFile();
                                        iconFile.FileBytes = (byte[])drItem.ItemArray[11];
                                        tcItem.BackgroundImage = iconFile;
                                    }
                                    catch { tcItem.BackgroundImage = null; }
                                }
                                if (drItem.ItemArray.Length > 12 && drItem.ItemArray[12] != null && tcItem.BackgroundImage == null)
                                {
                                    try
                                    {
                                        long idFile = 0;
                                        if (long.TryParse(drItem.ItemArray[12].ToString(), out idFile))
                                        {
                                            OpFile file = dataProvider.GetFile(idFile);
                                            if (file != null)
                                            {
                                                dataProvider.GetFileContent(file);
                                                tcItem.BackgroundImage = file;
                                            }
                                        }
                                    }
                                    catch { tcItem.BackgroundImage = null; }
                                }

                                if (!retDict.ContainsKey(idType))
                                    retDict.Add(idType, tcItem);

                                retDict[idType] = tcItem;
                            }
                        }

                        if (i == 1 && dataSet.Tables[i].Rows != null && dataSet.Tables[i].Rows.Count > 0)
                        {
                            foreach (DataRow drItem in dataSet.Tables[i].Rows)
                            {
                                long idType = Convert.ToInt64(drItem.ItemArray[0]);
                                object oItem = drItem.ItemArray[1];
                                if (retDict.ContainsKey(idType))
                                {
                                    OpTileControl tcItem = retDict[idType] as OpTileControl;
                                    if (tcItem != null)
                                    {
                                        if (tcItem.ReferenceData == null)
                                            tcItem.ReferenceData = new List<object>();
                                        tcItem.ReferenceData.Add(oItem);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            else if (dashboardItemClass == Enums.DashboardItemClass.CARD)
            {
                #region CARD

                #endregion
            }
            else if (dashboardItemClass == Enums.DashboardItemClass.CHART)
            {
                #region CHART

                // [0] - iddescr, serietype, [1] - title, nazwa serii [2] - typ x, typ y [3] - name x, name y [4] - caption x, caption y 
                // [5] Datatypy wyswietlane na wykresie wykorzystane do przeliczenia jednostek - oś X, oś Y, w przypadku datetime podajemy null
                // [6] Zarezerowowany na przyszłość [7]... - wart. x, wart y 
                List<OpChartControl.OpChartSeries> series = new List<OpChartControl.OpChartSeries>();
                OpChartControl ccItem = null;
                long? oldIdType = null;
                long idType = 0;
                if (dataSet.Tables != null && dataSet.Tables.Count > 0)
                {
                    for (int t = 0; t < dataSet.Tables.Count; t++)
                    {
                        if (dataSet.Tables[t].Rows != null && dataSet.Tables[t].Rows.Count > 0)
                        {
                            idType = Convert.ToInt64(dataSet.Tables[t].Rows[0].ItemArray[0]);
                            if (oldIdType.HasValue && oldIdType != idType)
                            {
                                oldIdType = idType;
                                if (ccItem != null)
                                {
                                    ccItem.SeriesList = series;
                                    retDict[ccItem.IdType] = ccItem;
                                }
                                series = new List<OpChartControl.OpChartSeries>();
                            }
                            else if (!oldIdType.HasValue)
                                oldIdType = idType;
                            string title = String.Format("Chart_{0}", idType);

                            OpDashboardControl dcItem = dashboardControls.Find(d => d.IdType == idType);
                            if (dcItem == null)
                                throw new Exception("Data mismatch");

                            ccItem = new OpChartControl(title, dcItem);
                            OpChartControl.OpChartSeries serie = new OpChartControl.OpChartSeries();

                            List<OpChartControl.OpChartSeriesPoint> seriesPoints = new List<OpChartControl.OpChartSeriesPoint>();
                            Type xType = null;
                            Type yType = null;

                            serie.SeriesType = dataSet.Tables[t].Rows[0].ItemArray[1].ToString();

                            OpDataType dataTypeX = null;
                            OpDataType dataTypeY = null;
                            Dictionary<long, int?> precisionDict = new Dictionary<long, int?>();
                            Dictionary<long, OpUnit> unitDict = new Dictionary<long, OpUnit>();

                            for (int i = 1; i < dataSet.Tables[t].Rows.Count; i++)
                            {
                                if (i == 1 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    ccItem.Title = dataSet.Tables[t].Rows[i].ItemArray[0].ToString();
                                    serie.SeriesName = dataSet.Tables[t].Rows[i].ItemArray[1].ToString();
                                }
                                else if (i == 2 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    try
                                    {
                                        xType = System.Type.GetType(dataSet.Tables[t].Rows[i].ItemArray[0].ToString());
                                        if (xType == null)
                                            xType = BaseComponent.GetSystemType(dataSet.Tables[t].Rows[i].ItemArray[0].ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        xType = BaseComponent.GetSystemType(dataSet.Tables[t].Rows[i].ItemArray[0].ToString());
                                    }
                                    try
                                    {
                                        yType = System.Type.GetType(dataSet.Tables[t].Rows[i].ItemArray[1].ToString());
                                        if (yType == null)
                                            yType = BaseComponent.GetSystemType(dataSet.Tables[t].Rows[i].ItemArray[1].ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        yType = BaseComponent.GetSystemType(dataSet.Tables[t].Rows[i].ItemArray[1].ToString());
                                    }
                                    if (xType != null && yType != null)
                                    {
                                        serie.XTypeName = xType.FullName;
                                        serie.YTypeName = yType.FullName;
                                    }
                                }
                                else if (i == 3 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    serie.XName = dataSet.Tables[t].Rows[i].ItemArray[0].ToString();
                                    serie.YName = dataSet.Tables[t].Rows[i].ItemArray[1].ToString();
                                }
                                else if (i == 4 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    serie.XCaption = dataSet.Tables[t].Rows[i].ItemArray[0].ToString();
                                    serie.YCaption = dataSet.Tables[t].Rows[i].ItemArray[1].ToString();
                                }
                                else if (i == 5 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    long idDataTypeX = 0;
                                    long idDataTypeY = 0;
                                    if (long.TryParse(dataSet.Tables[t].Rows[i].ItemArray[0].ToString(), out idDataTypeX))
                                        dataTypeX = dataProvider.GetDataType(idDataTypeX);
                                    if (long.TryParse(dataSet.Tables[t].Rows[i].ItemArray[1].ToString(), out idDataTypeY))
                                        dataTypeY = dataProvider.GetDataType(idDataTypeY);

                                    List<long> idDataTypeList = new List<long>();
                                    if (dataTypeX != null)
                                        idDataTypeList.Add(dataTypeX.IdDataType);
                                    if (dataTypeY != null)
                                        idDataTypeList.Add(dataTypeY.IdDataType);

                                    if (idDataTypeList != null && idDataTypeList.Count > 0)
                                    {
                                        #region LoadUnits

                                        List<OpDataFormatGroupDetails> dataFormatGroupDetails = DataFormatGroupComponent.GetHierarchicalDataFormatGroupDetails((DataProvider)dataProvider, loggedOperator, module)
                                                                                                                    .Where(d => d.IdDataType.In(idDataTypeList.ToArray()))
                                                                                                                    .ToList();
                                        precisionDict = DataTypeFormatComponent.GetDataTypePrecisionDict(dataFormatGroupDetails, true);
                                        unitDict = dataFormatGroupDetails.Distinct(x => x.ID_DATA_TYPE)
                                                                                                    .Where(x => x.IdUnitOut.HasValue)
                                                                                                    .ToDictionary(k => k.IdDataType, v => v.UnitOut == null ? dataProvider.GetUnit(v.IdUnitOut.Value) : v.UnitOut);
                                        #endregion
                                        #region UpdateLabel

                                        if (unitDict != null && dataTypeX != null && unitDict.ContainsKey(dataTypeX.IdDataType) && !serie.XCaption.Contains("[" + unitDict[dataTypeX.IdDataType].Name + "]"))
                                        {
                                            serie.XCaption += " [" + unitDict[dataTypeX.IdDataType].Name + "]";
                                        }
                                        else if (dataTypeX != null)
                                        {
                                            OpUnit unit = dataTypeX.Unit != null ? dataTypeX.Unit : dataProvider.GetUnit(dataTypeX.IdUnit);
                                            if (unit != null && !string.IsNullOrEmpty(unit.Name) && !serie.XCaption.Contains("[" + unit.Name + "]"))
                                                serie.XCaption += " [" + unit.Name + "]";
                                        }

                                        if (unitDict != null && dataTypeY != null && unitDict.ContainsKey(dataTypeY.IdDataType) && !serie.YCaption.Contains("[" + unitDict[dataTypeY.IdDataType].Name + "]"))
                                        {
                                            serie.YCaption += " [" + unitDict[dataTypeY.IdDataType].Name + "]";
                                        }
                                        else if (dataTypeY != null)
                                        {
                                            OpUnit unit = dataTypeY.Unit != null ? dataTypeY.Unit : dataProvider.GetUnit(dataTypeY.IdUnit);
                                            if (unit != null && !string.IsNullOrEmpty(unit.Name) && !serie.YCaption.Contains("[" + unit.Name + "]"))
                                                serie.YCaption += " [" + unit.Name + "]";
                                        }

                                        #endregion
                                    }
                                }
                                else if (i == 6 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    // zarezerwowane
                                }
                                else if (i >= 7 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    if (xType != null && yType != null)
                                    {
                                        Enums.DataTypeClass dtcX = DataTransferComponent.GetDataTypeClass(xType);
                                        Enums.DataTypeClass dtcY = DataTransferComponent.GetDataTypeClass(yType);
                                        object xValue = null;
                                        object yValue = null;
                                        if (dtcX == Enums.DataTypeClass.Datetime)
                                            xValue = dataProvider.ConvertUtcTimeToTimeZone(Convert.ToDateTime(dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[0], null, (int)dtcX)));
                                        else
                                            xValue = dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[0], null, (int)dtcX);
                                        if (dtcY == Enums.DataTypeClass.Datetime)
                                            yValue = dataProvider.ConvertUtcTimeToTimeZone(Convert.ToDateTime(dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[1], null, (int)dtcY)));
                                        else
                                            yValue = dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[1], null, (int)dtcY);


                                        bool conversionRequired = unitDict.Count > 0 || precisionDict.Count > 0;
                                        if (conversionRequired)
                                        {
                                            if (dataTypeX != null)
                                                xValue = UnitComponent.ChangeUnit(xValue, dataTypeX, unitDict, precisionDict);
                                            if (dataTypeY != null)
                                                yValue = UnitComponent.ChangeUnit(yValue, dataTypeY, unitDict, precisionDict);
                                        }

                                        seriesPoints.Add(new OpChartControl.OpChartSeriesPoint(xValue, yValue));
                                    }
                                }
                            }
                            serie.SeriesPoints = seriesPoints;
                            series.Add(serie);
                        }
                    }
                    if (ccItem != null && oldIdType.HasValue && oldIdType.Value == idType)
                    {
                        ccItem.SeriesList = series;
                        retDict[ccItem.IdType] = ccItem;
                    }
                }

                #endregion
            }
            else if (dashboardItemClass == Enums.DashboardItemClass.GAUGE)
            {
                #region GAUGE

                if (dataSet.Tables != null && dataSet.Tables.Count > 0)
                {
                    for (int t = 0; t < dataSet.Tables.Count; t++)
                    {
                        List<OpGaugeControl.OpGaugeRange> ranges = new List<OpGaugeControl.OpGaugeRange>();
                        if (dataSet.Tables[t].Rows != null && dataSet.Tables[t].Rows.Count > 0)
                        {
                            long idType = Convert.ToInt64(dataSet.Tables[t].Rows[0].ItemArray[0]);
                            OpDashboardControl dcItem = dashboardControls.Find(d => d.IdType == idType);
                            if (dcItem == null)
                                throw new Exception("Data mismatch");

                            for (int i = 1; i < dataSet.Tables[t].Rows.Count; i++)
                            {
                                if (i == 1 && dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                                {
                                    int minValue = Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[0]);
                                    int maxValue = Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[1]);
                                    string label = dataSet.Tables[t].Rows[i].ItemArray[2].ToString();

                                    Enums.DataTypeClass dtc = DataTransferComponent.GetDataTypeClass(typeof(float));

                                    object value = null;
                                    if (dataSet.Tables[t].Rows[i].ItemArray[3] != null && !String.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[3].ToString()))
                                        value = dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[3].ToString(), null, (int)dtc);
                                    else
                                        value = dataProvider.dbConnectionCore.ParamObject(0, null, (int)dtc); // w przypadku jesli coś się sypnie i w bazie nie będzie pomiaru wyświetlamy 0
                                    if (value == null)
                                        value = minValue;

                                    OpGaugeControl gcItem = new OpGaugeControl(minValue, maxValue, label, value, dcItem);
                                    retDict[idType] = gcItem;
                                }
                                else
                                {
                                    if (retDict.ContainsKey(idType))
                                    {
                                        double minValue = Convert.ToDouble(dataSet.Tables[t].Rows[i].ItemArray[0]);
                                        double maxValue = Convert.ToDouble(dataSet.Tables[t].Rows[i].ItemArray[1]);
                                        string name = dataSet.Tables[t].Rows[i].ItemArray[2].ToString();
                                        System.Drawing.Color color = System.Drawing.Color.FromName(dataSet.Tables[t].Rows[i].ItemArray[3].ToString());

                                        OpGaugeControl.OpGaugeRange range = new OpGaugeControl.OpGaugeRange(minValue, maxValue) { Color = color, Name = name };

                                        ranges.Add(range);
                                    }
                                }
                            }
                            (retDict[idType] as OpGaugeControl).Ranges = ranges;
                        }
                    }
                }

                #endregion
            }
            else if (dashboardItemClass == Enums.DashboardItemClass.GRID)
            {
                #region GRID

                if (dataSet.Tables != null && dataSet.Tables.Count > 0)
                {
                    for (int t = 0; t < dataSet.Tables.Count; t++)
                    {
                        if (dataSet.Tables[t].Rows != null && dataSet.Tables[t].Rows.Count > 0)
                        {
                            // [0] - idType, [1] - fieldname [2] - caption [3] - column type [4]... - value
                            long idType = Convert.ToInt64(dataSet.Tables[t].Rows[0].ItemArray[0]);
                            OpDashboardControl dcItem = dashboardControls.Find(d => d.IdType == idType);
                            if (dcItem == null)
                                throw new Exception("Data mismatch");
                            OpGridControl gcItem = new OpGridControl(dcItem);

                            List<OpGridControl.OpGridColumn> gpList = new List<OpGridControl.OpGridColumn>();
                            List<OpGridRow> dataSource = new List<OpGridRow>();

                            for (int j = 0; j < dataSet.Tables[t].Rows[1].ItemArray.Length; j++)
                            {
                                // Wyciągamy nazwy kolumn, caption oraz typy column
                                Type tItem = BaseComponent.GetSystemType(dataSet.Tables[t].Rows[3].ItemArray[j].ToString());
                                OpGridControl.OpGridColumn gridColumn = new OpGridControl.OpGridColumn(dataSet.Tables[t].Rows[1].ItemArray[j].ToString(),
                                    dataSet.Tables[t].Rows[2].ItemArray[j].ToString(), tItem.FullName);
                                gpList.Add(gridColumn);
                            }

                            int iter = 0;
                            for (int i = 4; i < dataSet.Tables[t].Rows.Count; i++)
                            {
                                // Wyciagamy dane do pokazania w gridzie
                                OpGridRow row = new OpGridRow(iter, new Dictionary<string, object>());
                                for (int j = 0; j < dataSet.Tables[t].Rows[i].ItemArray.Length; j++)
                                {
                                    Type systemType = gpList[j].Type;
                                    Enums.DataTypeClass dtc = DataTransferComponent.GetDataTypeClass(systemType);
                                    row[gpList[j].Name] = dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[j], null, (int)dtc); // konwertujemy na odpowiedni typ                                        
                                }
                                dataSource.Add(row);
                                iter++;
                            }

                            retDict[idType] = gcItem;

                            gcItem.Columns = gpList;
                            gcItem.DataSource = dataSource;

                            retDict[idType] = gcItem;
                        }
                    }
                }

                #endregion
            }
            else if (dashboardItemClass == Enums.DashboardItemClass.MAP)
            {
                #region MAP

                //[0] idType
                //[1] Type: 1, idMarker, Latitude, longitude, tooltip - lub format, color, Icon, null lub ID_DATA_TYPE:VALUE; ..., null
                //[2] Type: 2, IdRange, Latitude, longitude, Radius, Tooltip - lub format, LineColor, FillColor, null lub tooltip param
                //[3] Type: 3, IdPolygon, string Polygon coords lub NULL (jeśli tablica), tooltip - lub format, LineColor, FillColor, tooltip param, null, null
                //...

                if (dataSet.Tables != null && dataSet.Tables.Count > 0)
                {
                    int t = 0;
                    if (dataSet.Tables[t].Rows != null && dataSet.Tables[t].Rows.Count > 0)
                    {
                        long idType = Convert.ToInt64(dataSet.Tables[t].Rows[0].ItemArray[0]);
                        OpDashboardControl dcItem = dashboardControls.Find(d => d.IdType == idType);
                        if (dcItem == null)
                            throw new Exception("Data mismatch");
                        OpMapControl mcItem = new OpMapControl(dcItem);

                        List<OpMapControl.OpMarker> markerList = new List<OpMapControl.OpMarker>();
                        List<OpMapControl.OpRange> rangeList = new List<OpMapControl.OpRange>();
                        List<OpMapControl.OpPolygon> polygonList = new List<OpMapControl.OpPolygon>();
                        ColorConverter colorConverter = new ColorConverter();

                        Dictionary<int, Tuple<string, string>> markerToolTip = new Dictionary<int, Tuple<string, string>>();
                        Dictionary<int, Tuple<string, string>> rangeToolTip = new Dictionary<int, Tuple<string, string>>();
                        Dictionary<int, Tuple<string, string>> polygonToolTip = new Dictionary<int, Tuple<string, string>>();

                        DateTime dt1 = DateTime.Now;
                        for (int i = 1; i < dataSet.Tables[t].Rows.Count; i++)
                        {
                            DateTime dt3 = DateTime.Now;
                            if (dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                            {
                                MapItemType mapItemType = (MapItemType)Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[0]);
                                if (mapItemType == MapItemType.Marker)
                                {
                                    #region Marker

                                    if (dataSet.Tables[t].Rows[i].ItemArray[1] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[1].ToString()))
                                    {
                                        int idMarker = Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[1]);
                                        double? latitude = null;
                                        double? longitude = null;

                                        try { latitude = Convert.ToDouble(dataSet.Tables[t].Rows[i].ItemArray[2].ToString().Trim().Replace('.', ',')); } catch { latitude = null; };
                                        try { longitude = Convert.ToDouble(dataSet.Tables[t].Rows[i].ItemArray[3].ToString().Trim().Replace('.', ',')); } catch { longitude = null; }

                                        if (!latitude.HasValue || !longitude.HasValue)
                                            continue;

                                        OpMapControl.OpCoord coords = new OpMapControl.OpCoord(latitude.Value, longitude.Value);

                                        if (coords != null)
                                        {
                                            OpMapControl.OpMarker newMarker = new OpMapControl.OpMarker(coords);
                                            newMarker.Id = idMarker;

                                            string tooltipTmp = null;
                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 4 && dataSet.Tables[t].Rows[i].ItemArray[4] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[4].ToString()))
                                                tooltipTmp = dataSet.Tables[t].Rows[i].ItemArray[4].ToString();

                                            string tooptipParams = null;
                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 7 && dataSet.Tables[t].Rows[i].ItemArray[7] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[7].ToString()))
                                                tooptipParams = dataSet.Tables[t].Rows[i].ItemArray[7].ToString();

                                            if (!markerToolTip.ContainsKey(idMarker))
                                                markerToolTip.Add(idMarker, new Tuple<string, string>(tooltipTmp, tooptipParams));

                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 5 && dataSet.Tables[t].Rows[i].ItemArray[5] != null)
                                                try { newMarker.Color = Color.FromArgb(Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[5])); } catch { newMarker.Color = null; }

                                            long idFile = 0;
                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 6 && dataSet.Tables[t].Rows[i].ItemArray[6] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[6].ToString()))
                                            {
                                                if (long.TryParse(dataSet.Tables[t].Rows[i].ItemArray[6].ToString(), out idFile))
                                                    newMarker.IconIndex = idFile;
                                                else
                                                    newMarker.IconUrl = dataSet.Tables[t].Rows[i].ItemArray[6].ToString();
                                            }
                                            else
                                            {
                                                if (dataSet.Tables.Count > 2 && dataSet.Tables[2].Rows != null && dataSet.Tables[2].Rows.Count > 0)
                                                {
                                                    for (int j = 0; j < dataSet.Tables[2].Rows.Count; j++)
                                                    {
                                                        MapItemType mit = (MapItemType)Convert.ToInt32(dataSet.Tables[2].Rows[j].ItemArray[0]);
                                                        if (mit == MapItemType.Marker)
                                                        {
                                                            int idCurrentMarker = Convert.ToInt32(dataSet.Tables[2].Rows[j].ItemArray[1]);
                                                            if (idCurrentMarker == idMarker)
                                                            {
                                                                try { newMarker.Icon = (byte[])dataSet.Tables[2].Rows[j].ItemArray[2]; } catch { newMarker.Icon = null; }
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            markerList.Add(newMarker);
                                        }
                                    }

                                    #endregion
                                }
                                else if (mapItemType == MapItemType.Range)
                                {
                                    #region Range
                                    if (dataSet.Tables[t].Rows[i].ItemArray[1] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[1].ToString()))
                                    {
                                        int idRange = Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[1]);
                                        Enums.DataTypeClass dtc = DataTransferComponent.GetDataTypeClass(typeof(float));
                                        double latitude = (double)dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[2].ToString().Trim().Replace(',', '.'), null, (int)dtc);
                                        double longitude = (double)dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[3].ToString().Trim().Replace(',', '.'), null, (int)dtc);
                                        OpMapControl.OpCoord coords = new OpMapControl.OpCoord(latitude, longitude);
                                        double radius = 100.0;
                                        if (dataSet.Tables[t].Rows[i].ItemArray[4] != null)
                                            try { radius = (double)dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[t].Rows[i].ItemArray[4].ToString().Trim().Replace(',', '.'), null, (int)dtc); } catch { radius = 100.0; }

                                        if (coords != null)
                                        {
                                            OpMapControl.OpRange newRange = new OpMapControl.OpRange(coords, radius);
                                            newRange.Id = idRange;

                                            string tooltipTmp = null;
                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 5 && dataSet.Tables[t].Rows[i].ItemArray[5] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[5].ToString()))
                                                tooltipTmp = dataSet.Tables[t].Rows[i].ItemArray[5].ToString();

                                            string tooptipParams = null;
                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 8 && dataSet.Tables[t].Rows[i].ItemArray[8] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[8].ToString()))
                                                tooptipParams = dataSet.Tables[t].Rows[i].ItemArray[8].ToString();

                                            if (!rangeToolTip.ContainsKey(idRange))
                                                rangeToolTip.Add(idRange, new Tuple<string, string>(tooltipTmp, tooptipParams));

                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 6 && dataSet.Tables[t].Rows[i].ItemArray[6] != null)
                                                try { newRange.LineColor = Color.FromArgb(Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[6])); } catch { newRange.LineColor = null; }

                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 7 && dataSet.Tables[t].Rows[i].ItemArray[7] != null)
                                                try { newRange.FillColor = Color.FromArgb(Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[7])); } catch { newRange.FillColor = null; }

                                            rangeList.Add(newRange);
                                        }
                                    }

                                    #endregion
                                }
                                else if (mapItemType == MapItemType.Polygon)
                                {
                                    #region Polygon

                                    // Jeśli zwracamy coordy w tablicy, string z coordami musi być nullem.
                                    // t = 0 tablcia z obiektami do wyświetlania, i podstawową konfiguracją
                                    // t = 1 tablica z coordami do polygonow, jeśli nie zostały podane w stringu (mapItemType, idPolygon, latitude, longitude)
                                    if (t == 0 && dataSet.Tables[t].Rows[i].ItemArray[1] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[1].ToString()))
                                    {
                                        List<OpMapControl.OpCoord> coordList = new List<OpMapControl.OpCoord>();
                                        int idPolygon = Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[1]);
                                        if (dataSet.Tables[t].Rows[i].ItemArray[2] != null && dataSet.Tables[t].Rows[i].ItemArray[2] is string) // Pojedynczy string z wszystkimi coordami
                                        {
                                            Enums.DataTypeClass dtc = DataTransferComponent.GetDataTypeClass(typeof(float));
                                            string coords = dataSet.Tables[t].Rows[i].ItemArray[2].ToString();
                                            string[] splitCoords = coords.Split(';');

                                            for (int j = 0; j < splitCoords.Length; j++)
                                            {
                                                string[] singleCoords = splitCoords[j].ToString().Split(':');
                                                double latitude = 0.0;
                                                double longitude = 0.0;
                                                if (singleCoords.Length > 0)
                                                {
                                                    latitude = (double)dataProvider.dbConnectionCore.ParamObject(singleCoords[0].ToString().Trim().Replace(',', '.'), null, (int)dtc);
                                                    longitude = (double)dataProvider.dbConnectionCore.ParamObject(singleCoords[1].ToString().Trim().Replace(',', '.'), null, (int)dtc);
                                                    coordList.Add(new OpMapControl.OpCoord(latitude, longitude));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (dataSet.Tables.Count > 1 && dataSet.Tables[1].Rows != null && dataSet.Tables[1].Rows.Count > 0)
                                            {
                                                for (int j = 0; j < dataSet.Tables[1].Rows.Count; j++)
                                                {
                                                    MapItemType mit = (MapItemType)Convert.ToInt32(dataSet.Tables[1].Rows[j].ItemArray[0]);
                                                    if (mit == MapItemType.Polygon)
                                                    {
                                                        int idCurrentPolygon = Convert.ToInt32(dataSet.Tables[1].Rows[j].ItemArray[1]);
                                                        if (idCurrentPolygon == idPolygon)
                                                        {
                                                            Enums.DataTypeClass dtc = DataTransferComponent.GetDataTypeClass(typeof(float));
                                                            double latitude = (double)dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[1].Rows[i].ItemArray[2].ToString().Trim().Replace(',', '.'), null, (int)dtc);
                                                            double longitude = (double)dataProvider.dbConnectionCore.ParamObject(dataSet.Tables[1].Rows[i].ItemArray[3].ToString().Trim().Replace(',', '.'), null, (int)dtc);
                                                            OpMapControl.OpCoord coords = new OpMapControl.OpCoord(latitude, longitude);
                                                            coordList.Add(coords);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if (coordList != null && coordList.Count > 0)
                                        {
                                            OpMapControl.OpPolygon newPolygon = new OpMapControl.OpPolygon();
                                            newPolygon.Coords = coordList;
                                            newPolygon.Id = idPolygon;

                                            string tooltipTmp = null;
                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 3 && dataSet.Tables[t].Rows[i].ItemArray[3] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[3].ToString()))
                                                tooltipTmp = dataSet.Tables[t].Rows[i].ItemArray[3].ToString();

                                            string tooptipParams = null;
                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 6 && dataSet.Tables[t].Rows[i].ItemArray[6] != null && !string.IsNullOrEmpty(dataSet.Tables[t].Rows[i].ItemArray[6].ToString()))
                                                tooptipParams = dataSet.Tables[t].Rows[i].ItemArray[6].ToString();

                                            if (!polygonToolTip.ContainsKey(idPolygon))
                                                polygonToolTip.Add(idPolygon, new Tuple<string, string>(tooltipTmp, tooptipParams));

                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 4 && dataSet.Tables[t].Rows[i].ItemArray[4] != null)
                                                try { newPolygon.LineColor = Color.FromArgb(Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[4])); } catch { newPolygon.LineColor = null; }


                                            if (dataSet.Tables[t].Rows[i].ItemArray.Length > 5 && dataSet.Tables[t].Rows[i].ItemArray[5] != null)
                                                try { newPolygon.FillColor = Color.FromArgb(Convert.ToInt32(dataSet.Tables[t].Rows[i].ItemArray[5])); } catch { newPolygon.FillColor = null; }

                                            polygonList.Add(newPolygon);
                                        }
                                    }

                                    #endregion
                                }
                            }
                            TimeSpan ts3 = DateTime.Now - dt3;
                        }                        

                        #region LoadMarkerIcon
                        DateTime dt2 = DateTime.Now;
                        if (markerList != null && markerList.Where(w => w.IconIndex.HasValue).ToList().Count > 0)
                        {
                            foreach (OpMapControl.OpMarker marker in markerList.Where(w => w.IconIndex.HasValue).Distinct(d => d.IconIndex.Value).ToList())
                            {
                                OpFile file = dataProvider.GetFile(marker.IconIndex.Value);
                                if (file != null)
                                {
                                    dataProvider.GetFileContent(file);
                                    if (file.FileBytes != null && file.FileBytes is byte[])
                                    {
                                        byte[] bytes = file.FileBytes as byte[];
                                        if (!mcItem.IconDict.ContainsKey(marker.IconIndex.Value))
                                            mcItem.IconDict.Add(marker.IconIndex.Value, bytes);
                                        marker.Icon = bytes;
                                    }
                                }
                            }
                        }
                        TimeSpan ts2 = DateTime.Now - dt2;
                        #endregion

                        #region UpdateToolTip


                        if (markerToolTip != null && markerToolTip.Count > 0)
                        {
                            DateTime dt3 = DateTime.Now;
                            Dictionary<int, string> tooltips = GenerateToolTip(dataProvider, loggedOperator, module, markerToolTip);
                            if (tooltips != null && tooltips.Count > 0)
                            {
                                foreach (OpMapControl.OpMarker marker in markerList)
                                {
                                    if (tooltips.ContainsKey(marker.Id))
                                        marker.Tooltip = tooltips[marker.Id];
                                }
                            }
                            TimeSpan ts3 = DateTime.Now - dt3;
                        }
                        if (rangeToolTip != null && rangeToolTip.Count > 0)
                        {
                            Dictionary<int, string> tooltips = GenerateToolTip(dataProvider, loggedOperator, module, rangeToolTip);
                            if (tooltips != null && tooltips.Count > 0)
                            {
                                foreach (OpMapControl.OpRange range in rangeList)
                                {
                                    if (tooltips.ContainsKey(range.Id))
                                        range.Tooltip = tooltips[range.Id];
                                }
                            }
                        }
                        if (polygonToolTip != null && polygonToolTip.Count > 0)
                        {
                            Dictionary<int, string> tooltips = GenerateToolTip(dataProvider, loggedOperator, module, polygonToolTip);
                            if (tooltips != null && tooltips.Count > 0)
                            {
                                foreach (OpMapControl.OpPolygon polygon in polygonList)
                                {
                                    if (tooltips.ContainsKey(polygon.Id))
                                        polygon.Tooltip = tooltips[polygon.Id];
                                }
                            }
                        }

                        #endregion

                        TimeSpan ts1 = DateTime.Now - dt1;

                        mcItem.Markers = markerList;
                        mcItem.Ranges = rangeList;
                        mcItem.Polygons = polygonList;

                        retDict[idType] = mcItem;
                    }
                }

                #endregion
            }
            else if (dashboardItemClass == Enums.DashboardItemClass.SCHEMA)
            {
                #region SCHEMA

                //[0] idType
                //[1] ParentIdReferenceType, ParentReferenceValue, IdReferenceType, ReferenceValue, IdFileSchema, IdFileConfiguration
                //...
                if (dataSet.Tables != null && dataSet.Tables.Count > 0)
                {
                    int t = 0;
                    if (dataSet.Tables[t].Rows != null && dataSet.Tables[t].Rows.Count > 0)
                    {
                        long idType = Convert.ToInt64(dataSet.Tables[t].Rows[0].ItemArray[0]);
                        OpDashboardControl dcItem = dashboardControls.Find(d => d.IdType == idType);
                        if (dcItem == null)
                            throw new Exception("Data mismatch");
                        OpSchemaControl scItem = new OpSchemaControl(dcItem);

                        List<OpSchema> schemaList = new List<OpSchema>();

                        for (int i = 1; i < dataSet.Tables[t].Rows.Count; i++)
                        {
                            if (dataSet.Tables[t].Rows[i].ItemArray != null && dataSet.Tables[t].Rows[i].ItemArray.Length > 0)
                            {
                                #region Schema

                                Enums.ReferenceType parentReferenceType = Enums.ReferenceType.None;
                                long parentReferenceValue = 0;
                                Enums.ReferenceType referenceType = Enums.ReferenceType.None;
                                long referenceValue = 0;
                                long idFileSchema = 0;
                                long idFileConfiguration = 0;
                                #region ParentReferenceType
                                if (dataSet.Tables[t].Rows[i].ItemArray.Length > 1 && dataSet.Tables[t].Rows[i].ItemArray[0] != null)
                                {
                                    int idParentReferenceType = 0;
                                    if (int.TryParse(dataSet.Tables[t].Rows[i].ItemArray[0].ToString(), out idParentReferenceType) && idParentReferenceType > 0)
                                    {
                                        if (Enum.IsDefined(typeof(Enums.ReferenceType), idParentReferenceType))
                                        {
                                            parentReferenceType = (Enums.ReferenceType)idParentReferenceType;
                                        }
                                    }
                                }
                                #endregion
                                #region ParentReferenceValue
                                if (dataSet.Tables[t].Rows[i].ItemArray.Length > 2 && dataSet.Tables[t].Rows[i].ItemArray[1] != null)
                                {
                                    long.TryParse(dataSet.Tables[t].Rows[i].ItemArray[1].ToString(), out parentReferenceValue);
                                }
                                #endregion
                                #region ReferenceType
                                if (dataSet.Tables[t].Rows[i].ItemArray.Length > 3 && dataSet.Tables[t].Rows[i].ItemArray[2] != null)
                                {
                                    int idReferenceType = 0;
                                    if (int.TryParse(dataSet.Tables[t].Rows[i].ItemArray[2].ToString(), out idReferenceType) && idReferenceType > 0)
                                    {
                                        if (Enum.IsDefined(typeof(Enums.ReferenceType), idReferenceType))
                                        {
                                            referenceType = (Enums.ReferenceType)idReferenceType;
                                        }
                                    }
                                }
                                #endregion
                                #region ReferenceValue
                                if (dataSet.Tables[t].Rows[i].ItemArray.Length > 4 && dataSet.Tables[t].Rows[i].ItemArray[3] != null)
                                {
                                    long.TryParse(dataSet.Tables[t].Rows[i].ItemArray[3].ToString(), out referenceValue);
                                }
                                #endregion
                                #region IdFileSchema
                                if (dataSet.Tables[t].Rows[i].ItemArray.Length > 5 && dataSet.Tables[t].Rows[i].ItemArray[4] != null)
                                {
                                    long.TryParse(dataSet.Tables[t].Rows[i].ItemArray[4].ToString(), out idFileSchema);
                                }
                                #endregion
                                #region IdFileConfiguration
                                if (dataSet.Tables[t].Rows[i].ItemArray.Length > 6 && dataSet.Tables[t].Rows[i].ItemArray[5] != null)
                                {
                                    long.TryParse(dataSet.Tables[t].Rows[i].ItemArray[5].ToString(), out idFileConfiguration);
                                }
                                #endregion

                                if (referenceType != Enums.ReferenceType.None && referenceValue > 0)
                                {
                                    if (idFileSchema == 0)
                                    {
                                        switch (referenceType)
                                        {
                                            case Enums.ReferenceType.IdLocation:
                                                schemaList.AddRange(LocationComponent.GetSchema(dataProvider, loggedOperator, module, referenceValue));
                                                break;
                                            case Enums.ReferenceType.SerialNbr:
                                                schemaList.AddRange(DeviceComponent.GetSchema(dataProvider, loggedOperator, module, referenceValue));
                                                break;
                                            case Enums.ReferenceType.IdMeter:
                                                schemaList.AddRange(MeterComponent.GetSchema(dataProvider, loggedOperator, module, referenceValue));
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        throw new NotImplementedException();
                                    }
                                }
                            }

                            #endregion
                        }
                        scItem.Schemas = schemaList;

                        retDict[idType] = scItem;
                    }
                }

                #endregion
            }

            if (retDict.Values.Count != dashboardControls.Count)
            {
                List<long> missingIdType = new List<long>();
                dashboardControls.Select(s => s.IdType).ToList().ForEach(d => { if (!d.In(retDict.Keys.ToArray())) missingIdType.Add(d); });

                foreach (long idType in missingIdType)
                {

                    if (dashboardControls.FirstOrDefault(d => d.IdType == idType).Class == Enums.DashboardItemClass.TILE)
                    {
                        OpTileControl gcItem = new OpTileControl("", 0, dashboardControls.FirstOrDefault(d => d.IdType == idType));
                        retDict.Add(idType, gcItem);
                    }
                    else if (dashboardControls.FirstOrDefault(d => d.IdType == idType).Class == Enums.DashboardItemClass.CARD)
                    {

                    }
                    else if (dashboardControls.FirstOrDefault(d => d.IdType == idType).Class == Enums.DashboardItemClass.CHART)
                    {
                        OpChartControl gcItem = new OpChartControl("", dashboardControls.FirstOrDefault(d => d.IdType == idType));
                        retDict.Add(idType, gcItem);
                    }
                    else if (dashboardControls.FirstOrDefault(d => d.IdType == idType).Class == Enums.DashboardItemClass.GAUGE)
                    {
                        OpGaugeControl gcItem = new OpGaugeControl(0, 100, "", 0, dashboardControls.FirstOrDefault(d => d.IdType == idType));
                        retDict.Add(idType, gcItem);
                    }
                    else if (dashboardControls.FirstOrDefault(d => d.IdType == idType).Class == Enums.DashboardItemClass.GRID)
                    {
                        OpGridControl gcItem = new OpGridControl(dashboardControls.FirstOrDefault(d => d.IdType == idType));
                        retDict.Add(idType, gcItem);
                    }
                    else if (dashboardControls.FirstOrDefault(d => d.IdType == idType).Class == Enums.DashboardItemClass.MAP)
                    {
                        OpMapControl gcItem = new OpMapControl(dashboardControls.FirstOrDefault(d => d.IdType == idType));
                        retDict.Add(idType, gcItem);
                    }
                }
            }

            return retDict.Values.ToList();
        }

        #endregion

        public static Dictionary<int, string> GenerateToolTip(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, Dictionary<int, Tuple<string, string>> toolTipDict)
        {
            Dictionary<int, string> retDict = new Dictionary<int, string>();
            if (toolTipDict != null && toolTipDict.Count > 0)
            {
                Dictionary<int, List<Tuple<OpDataType, string>>> paramsDict = new Dictionary<int, List<Tuple<OpDataType, string>>>();
                foreach (KeyValuePair<int, Tuple<string, string>> kvItem in toolTipDict)
                {
                    try
                    {
                        if (!String.IsNullOrEmpty(kvItem.Value.Item1) && String.IsNullOrEmpty(kvItem.Value.Item2))
                        {
                            retDict[kvItem.Key] = kvItem.Value.Item1;
                        }
                        else if (!String.IsNullOrEmpty(kvItem.Value.Item1) && !String.IsNullOrEmpty(kvItem.Value.Item2))
                        {
                            DateTime dt = DateTime.Now;
                            string[] paramsSplit = kvItem.Value.Item2.Split(';');
                            foreach (string sItem in paramsSplit)
                            {
                                string[] s = sItem.Split(':');
                                long idDataType = 0;
                                if (s != null && s.Length > 1 && long.TryParse(s[0], out idDataType))
                                {
                                    OpDataType dtItem = dataProvider.GetDataType(idDataType);
                                    if (dtItem != null)
                                    {
                                        string toConvert = s[1];
                                        if (s.Length > 2) // Może sie zdażyć, że w sklejanym strigu pojawi się ":" np. w dacie wtedy tez nam to rozdzieli, a tego nie chcemy. Przyjmujemy że dostajemy dwa parametry DataType:Value.
                                            for (int i = 2; i < s.Length; i++)
                                                toConvert += ":" + s[i];
                                        if (!paramsDict.ContainsKey(kvItem.Key))
                                            paramsDict[kvItem.Key] = new List<Tuple<OpDataType, string>>();

                                        paramsDict[kvItem.Key].Add(new Tuple<OpDataType, string>(dtItem, toConvert));
                                    }
                                }
                                else
                                    break;
                            }
                            if (paramsDict.ContainsKey(kvItem.Key) && paramsSplit.Length != paramsDict[kvItem.Key].Count) // jeśi się nie uda poprawnie odczytać wszystkich parametrów, w resultacie tootip bedzie wyświetlał nie poprawne dane - niezgodne z formatem
                                paramsDict[kvItem.Key] = new List<Tuple<OpDataType, string>>();
                            TimeSpan ts = DateTime.Now - dt;
                        }
                    }
                    catch { continue; }
                }

                if (paramsDict != null && paramsDict.Count > 0)
                {
                    DateTime dt2 = DateTime.Now;
                    Dictionary<long, int?> precisionDict = new Dictionary<long, int?>();
                    Dictionary<long, OpUnit> unitDict = new Dictionary<long, OpUnit>();

                    #region LoadUnits

                    List<long> dataTypeList = new List<long>();
                    paramsDict.Values.ToList().ForEach(s => dataTypeList.AddRange(s.Select(d => d.Item1.IdDataType).Distinct().ToList()));
                    if (dataTypeList != null && dataTypeList.Count > 0)
                    {
                        dataTypeList = dataTypeList.Distinct().ToList();
                        List<OpDataFormatGroupDetails> dataFormatGroupDetails = DataFormatGroupComponent.GetHierarchicalDataFormatGroupDetails((DataProvider)dataProvider, loggedOperator, module)
                                                                                                    .Where(d => d.IdDataType.In(dataTypeList.ToArray()))
                                                                                                    .ToList();
                        precisionDict = DataTypeFormatComponent.GetDataTypePrecisionDict(dataFormatGroupDetails, true);
                        unitDict = dataFormatGroupDetails.Distinct(x => x.ID_DATA_TYPE)
                                                                                    .Where(x => x.IdUnitOut.HasValue)
                                                                                    .ToDictionary(k => k.IdDataType, v => v.UnitOut == null ? dataProvider.GetUnit(v.IdUnitOut.Value) : v.UnitOut);
                    }
                    #endregion
                    
                    foreach (int key in paramsDict.Keys)
                    {
                        try
                        {
                            List<string> convertedParams = new List<string>();
                            foreach (Tuple<OpDataType, string> tItem in paramsDict[key])
                            {
                                DateTime dItem = DateTime.MinValue;
                                if (DateTime.TryParse(tItem.Item2, out dItem))
                                {
                                    convertedParams.Add(dataProvider.ConvertUtcTimeToTimeZone(dItem).ToString());
                                }
                                else
                                {
                                    string sItem = tItem.Item2;
                                    bool conversionRequired = unitDict.Count > 0 || precisionDict.Count > 0;
                                    if (conversionRequired)
                                        sItem = UnitComponent.ChangeUnit(tItem.Item2, tItem.Item1, unitDict, precisionDict).ToString();

                                    double doubleItem = 0;
                                    if ((tItem.Item1.IdDataTypeClass == (int)Enums.DataTypeClass.Real || tItem.Item1.IdDataTypeClass == (int)Enums.DataTypeClass.Decimal) && double.TryParse(sItem.Replace('.', ','), out doubleItem))
                                        sItem = Math.Round(doubleItem, 2).ToString();
                                    if (unitDict.ContainsKey(tItem.Item1.IdDataType))
                                        sItem += " [" + unitDict[tItem.Item1.IdDataType].Name + "]";
                                    convertedParams.Add(sItem);
                                }
                            }
                            if (toolTipDict.ContainsKey(key))                            
                                    retDict[key] = String.Format(toolTipDict[key].Item1, convertedParams.ToArray());                            
                        }
                        catch { retDict[key] = null; }
                    }
                    TimeSpan ts1 = DateTime.Now - dt2;

                }
            }
            return retDict;
        }
    }
}
