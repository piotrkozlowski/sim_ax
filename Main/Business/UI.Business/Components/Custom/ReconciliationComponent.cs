﻿using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.CORE.Metadata;
using IMR.Suite.UI.Business.Objects.Custom;
using IMR.Suite.UI.Business.Objects.Custom.Metadata;
using IMR.Suite.UI.Business.Objects.DW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ObjDW = IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.UI.Business.Components.Custom
{
    public class ReconciliationComponent
    {
        private const int REFUEL_MATCH_TIME = 2;
        
        #region CountVariance
        public static double? CountVariance(double? dispenced, double? outcome, double? delivery)
        {
            if (dispenced.HasValue && outcome.HasValue && delivery.HasValue)
                return dispenced.Value - outcome.Value - delivery.Value;
            else if (dispenced.HasValue && outcome.HasValue && !delivery.HasValue)
                return dispenced.Value - outcome.Value;
            else
                return null;
        }
        #endregion

        #region CountOutcome
        public static double? CountOutcome(double? startFuelVolume, double? endFuelVolume)
        {
            if (startFuelVolume.HasValue && endFuelVolume.HasValue)
                return startFuelVolume.Value - endFuelVolume.Value;
            else
                return null;
        }
        #endregion

        #region GetReconciliationByRefuel
        public static List<OpDynamicMeasure> GetReconciliationByRefuel(IEnumerable<OpDataTemporal> archiveDataTankMeasure,
            DataProvider dataProvider, OpMeter meter, Dictionary<long, Tuple<OpUnit, int>> dataTypeUnitConfig,
            DateTime startTime, DateTime endTime, Enums.AggregationType aggregationType, double? outlier = null)
        {
            List<OpDynamicMeasure> recoData = new List<OpDynamicMeasure>();
            long? idLocation = meter.DynamicProperties.GetNullableValue<long>(MdDynamicProperty.InstalledLocationId);

            #region Przygotowanie pomiarów
            if (archiveDataTankMeasure == null) return recoData;
            (archiveDataTankMeasure as List<OpDataTemporal>).Where(d => d != null).ToList().ForEach(d => {
                d.DataType = dataProvider.GetDataType(d.IdDataType);
                if (d.DataType != null)
                    d.Unit = dataProvider.GetUnit(d.DataType.IdUnit);
            });
            archiveDataTankMeasure = archiveDataTankMeasure.OrderBy(dt => dt.StartTime);
            #endregion

            #region Pobieranie Dostaw    
            Dictionary<DateTime, List<OpDataTemporal>> deliveryData = new Dictionary<DateTime, List<OpDataTemporal>>();
            
            if (aggregationType == Enums.AggregationType.Day)
            {
                DateTime refuelStartTime = startTime.AddDays(-1);
                deliveryData = dataProvider.GetRefuelDataTemporals(idLocation, meter.IdMeter, refuelStartTime, endTime, aggregationType);
            }
            else if (aggregationType == Enums.AggregationType.Quarter)
            {
                deliveryData = dataProvider.GetRefuelDataTemporals(idLocation, meter.IdMeter, startTime.AddHours(-1), endTime, aggregationType);
            }
            #endregion

            if (deliveryData != null)
            {
                if (deliveryData.Any(d => d.Key.Hour == 23))
                {
                    deliveryData = deliveryData.ToDictionary(k => k.Key.AddHours(1), v => v.Value);
                }
            }

            if (!archiveDataTankMeasure.Any()) return recoData;

            //IEnumerable<OpDataTemporal> automaticMeasures = new List<OpDataTemporal>();
            //automaticMeasures = archiveDataTankMeasure.Where(d => d.IdDataSourceType != (int)Enums.DataSourceType.ManualDataEntry);
            recoData = ReconciliationByRefuel(archiveDataTankMeasure, deliveryData, dataTypeUnitConfig, aggregationType, outlier);
            
            return recoData;
        }
        #endregion

        #region GetDeliveryReconciliation
        public static Dictionary<long, List<OpRefuelData>> GetDeliveryReconciliation(DataProvider dataProvider, DateTime startTime, DateTime endTime, bool onlySpecifiedTimeRange,
            List<long> idLocations, List<long> idMeters, bool loadRefuelCustomDataTypes, List<long> refuelCustomDataTypes,
            bool returnPerProduct, bool returnPerTank, bool returnPerRefuel,
            out OpBindingList<OpDynamicMeasure> listPerProduct, out OpBindingList<OpDynamicMeasure> listPerTank, out OpBindingList<OpDynamicMeasure> listPerRefuel)
        {
            Dictionary<long, List<OpRefuelData>> retDict = new Dictionary<long, List<OpRefuelData>>();
            listPerProduct = new OpBindingList<OpDynamicMeasure>(new List<OpDynamicMeasure>());
            listPerTank = new OpBindingList<OpDynamicMeasure>(new List<OpDynamicMeasure>());
            listPerRefuel = new OpBindingList<OpDynamicMeasure>(new List<OpDynamicMeasure>());

            TypeDateTimeCode dateTimeCode = TypeDateTimeCode.Between(startTime, endTime);

            if ((idLocations == null || idLocations.Count == 0)
                && (dataProvider.LocationFilter == null || dataProvider.LocationFilter.Length == 0))
            {
                idLocations = dataProvider.GetLocationFilter(loadNavigationProperties: false, loadCustomData: false,
                    autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdLocation: new long[0], IdDistributor: dataProvider.DistributorFilter).Select(q => q.IdLocation).ToList();
                if (idLocations.Count == 0)
                    return retDict;
            }
            if ((idMeters == null || idMeters.Count == 0)
                && (dataProvider.MeterFilter == null || dataProvider.MeterFilter.Length == 0))
            {
                idMeters = dataProvider.GetMeterFilter(loadNavigationProperties: false, loadCustomData: false,
                    autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdMeter: new long[0], IdDistributor: dataProvider.DistributorFilter).Select(q => q.IdMeter).ToList();
                if (idMeters.Count == 0)
                    return retDict;
            }

            Dictionary<long, long> dataTypeIds = refuelCustomDataTypes != null ? refuelCustomDataTypes.Distinct().ToDictionary(q => q) : new Dictionary<long, long>();
            dataTypeIds[DataType.REFUEL_GUID] = DataType.REFUEL_GUID;
            dataTypeIds[DataType.REFUEL_TOTAL_VOLUME] = DataType.REFUEL_TOTAL_VOLUME;
            dataTypeIds[DataType.REFUEL_TOTAL_REFERENCE_VOLUME] = DataType.REFUEL_TOTAL_REFERENCE_VOLUME;

            List<OpRefuel> refuels = new List<OpRefuel>();
            refuels.AddRange(dataProvider.GetRefuelFilter(loadNavigationProperties: false, loadCustomData: loadRefuelCustomDataTypes, EndTime: dateTimeCode,
                IdLocation: idLocations == null ? null : idLocations.ToArray(), IdMeter: idMeters == null ? null : idMeters.ToArray(), customDataTypes: dataTypeIds.Keys.ToList())
                .Where(q => q.IdMeter.HasValue && q.IdLocation.HasValue && q.StartTime.HasValue));

            List<OpRefuelData> refuelDatas = new List<OpRefuelData>();
            if (refuels.Count > 0)
            {
                if (loadRefuelCustomDataTypes)
                    refuelDatas.AddRange(refuels.SelectMany(q => q.DataList));
                else
                    refuelDatas.AddRange(dataProvider.GetRefuelDataFilter(loadNavigationProperties: false,
                        IdRefuel: refuels.Select(q => q.IdRefuel).Distinct().ToArray(), IdDataType: dataTypeIds.Keys.ToArray(), commandTimeout: 100));
            }

            if (refuelDatas.Count > 0 && !onlySpecifiedTimeRange)
            {
                Dictionary<string, string> GUIDDict = refuelDatas.Where(q => q.IdDataType == DataType.REFUEL_GUID && q.Value != null).Select(q => q.Value.ToString()).Distinct().ToDictionary(q => q);
                //List<string> GUIDList = GUIDDict.Keys.ToList();
                if (GUIDDict.Count > 0)
                {
                    //List<string> JoinedGUIDList = new List<string>();
                    //int index = 0;
                    //JoinedGUIDList.Add("");
                    //for (int i = 0; i < GUIDList.Count; i++)
                    //{
                    //    GUIDList[i] = String.Format(@"'{0}'", GUIDList[i]);
                    //    if (JoinedGUIDList[index].Length + GUIDList[i].Length < 3800)
                    //    {
                    //        if (JoinedGUIDList[index].Length == 0)
                    //            JoinedGUIDList[index] = GUIDList[i];
                    //        else
                    //            JoinedGUIDList[index] += String.Format(",{0}", GUIDList[i]);
                    //    }
                    //    else
                    //    {
                    //        index++;
                    //        JoinedGUIDList.Add(GUIDList[i]);
                    //    }
                    //}
                    List<long> refuelIdsToAdd = new List<long>();
                    //if (JoinedGUIDList.Count <= 3)
                    //{
                    //    foreach (string customWhereGUID in JoinedGUIDList)
                    //    {
                    //        List<OpRefuelData> tmpRefuelDatas = dataProvider.GetRefuelDataFilter(loadNavigationProperties: false, IdDataType: new long[] { DataType.REFUEL_GUID },
                    //            customWhereClause: String.Format(@"[VALUE] IS NOT NULL and [VALUE] in ({0})", customWhereGUID));

                    //        if (tmpRefuelDatas.Count > 0)
                    //            refuelIdsToAdd.AddRange(tmpRefuelDatas.Select(q => q.IdRefuel).Distinct());
                    //    }
                    //}
                    //else
                    //{
                        List<OpRefuelData> tmpRefuelData = dataProvider.GetRefuelDataFilter(loadNavigationProperties: false,
                            IdDataType: new long[] { DataType.REFUEL_GUID },
                            commandTimeout: 120).Where(q => q.Value != null).ToList();
                        if (tmpRefuelData.Count > 0)
                            refuelIdsToAdd.AddRange(tmpRefuelData.Where(q => q.Value != null && GUIDDict.ContainsKey(q.Value.ToString())).Select(q => q.IdRefuel));
                    //}
                    if (refuelIdsToAdd.Count > 0)
                    {
                        if (refuels != null && refuels.Count > 0)
                        {
                            Dictionary<long, long> refuelIds = refuels.Select(q => q.IdRefuel).Distinct().ToDictionary(q => q);
                            refuelIdsToAdd = refuelIdsToAdd.Distinct().ToList();
                            refuelIdsToAdd.RemoveAll(q => refuelIds.ContainsKey(q));
                            if (refuelIdsToAdd.Count > 0)
                            {
                                List<OpRefuel> tmpRefuels = new List<OpRefuel>();
                                tmpRefuels.AddRange(dataProvider.GetRefuelFilter(loadNavigationProperties: false, loadCustomData: loadRefuelCustomDataTypes, IdRefuel: refuelIdsToAdd.ToArray(),
                                    IdLocation: idLocations == null ? null : idLocations.ToArray(), IdMeter: idMeters == null ? null : idMeters.ToArray(), customDataTypes: dataTypeIds.Keys.ToList())
                                    .Where(q => q.IdMeter.HasValue && q.IdLocation.HasValue && q.StartTime.HasValue && q.EndTime.HasValue));
                                if (tmpRefuels.Count > 0)
                                {
                                    refuels.AddRange(tmpRefuels);
                                    if (loadRefuelCustomDataTypes)
                                        refuelDatas.AddRange(tmpRefuels.SelectMany(q => q.DataList));
                                    else
                                        refuelDatas.AddRange(dataProvider.GetRefuelDataFilter(loadNavigationProperties: false,
                                            IdRefuel: tmpRefuels.Select(q => q.IdRefuel).Distinct().ToArray(), IdDataType: dataTypeIds.Keys.ToArray(), commandTimeout: 100));
                                    
                                }
                            }
                        }
                    }
                }
            }

            List<OpRefuel> calculatedRefuels = refuels.Where(q => q.DataSource != null && q.DataSource.IdDataSourceType == (int)Enums.DataSourceType.AWSR).ToList();
            List<long> refuelsToSkip = new List<long>();
            Dictionary<Tuple<long, long, DateTime, DateTime>, List<OpRefuel>> duplicates = calculatedRefuels.GroupBy(q => new Tuple<long, long, DateTime, DateTime>(q.IdMeter.Value, q.IdLocation.Value, q.StartTime.Value, q.EndTime.Value)).Where(q => q.Count() > 1).ToDictionary(q => q.Key, q => q.ToList());
            foreach (KeyValuePair<Tuple<long, long, DateTime, DateTime>, List<OpRefuel>> duplicate in duplicates)
            {
                Dictionary<long, long> duplicateIdRefuels = duplicate.Value.Select(w => w.IdRefuel).Distinct().ToDictionary(q => q);
                OpRefuelData refData = refuelDatas.FirstOrDefault(q => duplicateIdRefuels.ContainsKey(q.IdRefuel) && q.IdDataType == DataType.REFUEL_GUID && q.Value != null);
                if (refData != null)
                    refuelsToSkip.AddRange(duplicate.Value.Where(q => q.IdRefuel != refData.IdRefuel).Select(q => q.IdRefuel).Distinct());
                else
                {
                    OpRefuel refu = duplicate.Value.FirstOrDefault();
                    if (refu != null)
                        refuelsToSkip.AddRange(duplicate.Value.Where(q => q.IdRefuel != refu.IdRefuel).Select(q => q.IdRefuel).Distinct());
                }
            }
            if (refuelsToSkip.Count > 0)
            {
                Dictionary<long, long> skipIdRefuels = refuelsToSkip.Distinct().ToDictionary(q => q);
                refuels.RemoveAll(q => skipIdRefuels.ContainsKey(q.IdRefuel));
                refuelDatas.RemoveAll(q => skipIdRefuels.ContainsKey(q.IdRefuel));
            }

            if (refuels.Count > 0)
            {
                List<long> idDataSource = refuels.Where(q => q.DataSource == null && q.IdDataSource != 0).Select(q => q.IdDataSource).Distinct().ToList();
                if (idDataSource.Count > 0)
                {
                    Dictionary<long, OpDataSource> DataSourceDict = dataProvider.GetDataSourceFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDataSource: idDataSource.ToArray()).Distinct(q => q.IdDataSource).ToDictionary(l => l.IdDataSource);
                    refuels.ForEach(q =>
                        {
                            if (q.DataSource == null)
                                q.DataSource = DataSourceDict.TryGetValue(q.IdDataSource);
                        });
                }
            }

            retDict = refuelDatas.GroupBy(q => q.IdRefuel).ToDictionary(q => q.Key, q => q.ToList());

            List<OpLocation> locationList = new List<OpLocation>();
            List<OpMeter> meterList = new List<OpMeter>();
            List<OpMeter> meterParentList = new List<OpMeter>();
            List<long> locationIds = refuels.Select(q => q.IdLocation.Value).Distinct().ToList();
            List<long> meterIds = refuels.Select(q => q.IdMeter.Value).Distinct().ToList();
            if (meterIds.Count > 0)
            {
                List<long> dataTypesList = new List<long>()
                {
                    DataType.ATG_PARAMS_TANK_NUMBER,
                    DataType.ATG_PARAMS_PRODUCT_CODE,
                    DataType.METER_SERIAL_NUMBER,
                    DataType.METER_LOGICAL_PARENT_ID
                };
                meterList = dataProvider.GetMeterFilter(loadNavigationProperties: true, loadCustomData: true, mergeIntoCache: false, IdMeter: meterIds.ToArray(), IdDistributor: new int[0], customDataTypes: dataTypesList);
                if (meterList.Count > 0)
                {
                    List<OpMeter> metersWithParentId = meterList.Where(q => q.DataList.Exists(DataType.METER_LOGICAL_PARENT_ID) && q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue).ToList();
                    List<long> metersParentIds = metersWithParentId.Where(q => q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue).Select(q => q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).Value).ToList();
                    if (metersParentIds.Count > 0)
                        meterParentList = dataProvider.GetMeterFilter(loadNavigationProperties: true, loadCustomData: true, mergeIntoCache: false, IdMeter: metersParentIds.ToArray(), IdDistributor: new int[0], customDataTypes: dataTypesList);
                }
            }
            Dictionary<long, double?> meterTankCapacityDict = new Dictionary<long, double?>();
            if (meterList.Count > 0 || meterParentList.Count > 0)
            {
                DateTime utcNow = DateTime.UtcNow;
                meterTankCapacityDict = Business.Components.CORE.MeterComponent.GetTankCapacity(dataProvider, meterList.Concat(meterParentList).ToArray(), utcNow, utcNow)
                    .ToDictionary(c => c.Key, c =>
                    {
                        Tuple<DateTime, DateTime, double?> foundCapacity = c.Value.FirstOrDefault(v => v.Item1 <= utcNow && v.Item2 >= utcNow);
                        return foundCapacity != null ? foundCapacity.Item3 : (double?)null;
                    });
            }

            meterList.ForEach(q =>
                {
                    if (meterTankCapacityDict.ContainsKey(q.IdMeter))
                        q.TankCapacity = meterTankCapacityDict[q.IdMeter];
                });
            meterParentList.ForEach(q =>
            {
                if (meterTankCapacityDict.ContainsKey(q.IdMeter))
                    q.TankCapacity = meterTankCapacityDict[q.IdMeter];
            });

            if (locationIds.Count > 0)
                locationList = dataProvider.GetLocationFilter(loadNavigationProperties: true, loadCustomData: true, IdLocation: locationIds.ToArray(), IdDistributor: new int[0]);

            PrepareDeliveryReconciliation(dataProvider, refuels != null ? refuels.Where(q => q.IdMeter.HasValue).Distinct(q => q.IdRefuel).ToDictionary(q => q.IdRefuel) : null, retDict,
                meterList != null ? meterList.Distinct(q => q.IdMeter).ToDictionary(q => q.IdMeter) : null,
                meterParentList != null ? meterParentList.Distinct(q => q.IdMeter).ToDictionary(q => q.IdMeter) : null,
                locationList != null ? locationList.Distinct(q => q.IdLocation).ToDictionary(q => q.IdLocation) : null,
                refuelCustomDataTypes,
                returnPerProduct, returnPerTank, returnPerRefuel,
                out listPerProduct, out listPerTank, out listPerRefuel);

            return retDict;
        }
        #endregion
        #region LoadDeliveryReconciliationProperties
        public static void LoadDeliveryReconciliationProperties(OpBindingList<OpDynamicMeasure> opBindingList)
        {
            if (opBindingList == null || opBindingList.Count == 0) return;

            opBindingList.AddProperty(new OpDynamicProperty(MdLocation.IdLocation, typeof(long)));
            opBindingList.AddProperty(new OpDynamicProperty(MdLocation.CID, typeof(long)));
            opBindingList.AddProperty(new OpDynamicProperty(MdLocation.Name, typeof(long)));
            opBindingList.AddProperty(new OpDynamicProperty(MdLocation.City, typeof(long)));
            opBindingList.AddProperty(new OpDynamicProperty(MdLocation.Address, typeof(long)));
            opBindingList.AddProperty(new OpDynamicProperty(MdMeter.IdMeter, typeof(long)));
            opBindingList.AddProperty(new OpDynamicProperty(MdMeter.ProductCode, typeof(string)));
            opBindingList.AddProperty(new OpDynamicProperty(MdMeter.IdProductCode, typeof(int)));
            opBindingList.AddProperty(new OpDynamicProperty(MdMeter.TankCapacity, typeof(double)));
            opBindingList.AddProperty(new OpDynamicProperty(ObjDW.Metadata.MdDataSourceType.IdDataSourceType, typeof(int)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.MeterIds, typeof(string)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.MeterNbrs, typeof(string)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.MeterSerialNbrs, typeof(string)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.CapacityMeterIds, typeof(List<long>)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.Guid, typeof(string)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.DictionaryKey, typeof(string)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.IsRefuelJoined, typeof(bool)));
            opBindingList.AddProperty(new OpDynamicProperty(MdDynamicProperty.JoinType, typeof(string)));
            foreach (int dataSourceType in new List<long>()
                {
                    (int)Enums.DataSourceType.AWSR,
                    (int)Enums.DataSourceType.Estimation,
                    (int)Enums.DataSourceType.RefuelEnhanced,
                    (int)Enums.DataSourceType.File,
                    (int)Enums.DataSourceType.Fitter,
                    (int)Enums.DataSourceType.ManualDataEntry,
                    (int)Enums.DataSourceType.Interface,
                    (int)Enums.DataSourceType.OKO,
                })
            {
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartTime, dataSourceType), typeof(DateTime)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndTime, dataSourceType), typeof(DateTime)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationOrderNbr, dataSourceType), typeof(string)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationTruckNbr, dataSourceType), typeof(string)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationDriverName, dataSourceType), typeof(string)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationTerminalName, dataSourceType), typeof(string)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationCarrierName, dataSourceType), typeof(string)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStatus, dataSourceType), typeof(int)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationRefuelIds, dataSourceType), typeof(string)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationGross, dataSourceType), typeof(double)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationNet, dataSourceType), typeof(double)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationSoldGross, dataSourceType), typeof(double)));
                opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationSoldNet, dataSourceType), typeof(double)));
            }
            opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartLevel, (int)Enums.DataSourceType.AWSR), typeof(double)));
            opBindingList.AddProperty(new OpDynamicProperty(String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndLevel, (int)Enums.DataSourceType.AWSR), typeof(double)));

        }
        #endregion
                
        
        #region ReconciliationByRefuel
        private static List<OpDynamicMeasure> ReconciliationByRefuel(IEnumerable<OpDataTemporal> archiveDataTankMeasure, Dictionary<DateTime, List<OpDataTemporal>> deliveryData,
            Dictionary<long, Tuple<OpUnit, int>> dataTypeUnitConfig, Enums.AggregationType aggregationType, double? outlier)
        {
            List<OpDynamicMeasure> recoData = new List<OpDynamicMeasure>();
            OpCumulativeVariance scaledCV = new OpCumulativeVariance();
            OpCumulativeVariance verifiedCV = new OpCumulativeVariance();
            OpDataTemporal endFuelVolumeFromLastDayNET = null;
            OpDataTemporal endFuelVolumeFromLastDayGROSS = null;
            DateTime? recoDateLastDay = null;
            foreach (IGrouping<Tuple<DateTime?, DateTime?>, OpDataTemporal> dataGroup in archiveDataTankMeasure.GroupBy(d => new Tuple<DateTime?, DateTime?>(d.StartTime, d.EndTime)))
            {
                OpDataTemporal refData = dataGroup.FirstOrDefault();
                if (refData == null) break;
                #region Ładowanie danych do klasy OpReconciliation
                OpReconciliation reco = new OpReconciliation()
                {
                    IdMeter = refData.IdMeter,
                    StartTime = refData.StartTime,
                    EndTime = refData.EndTime,
                    IdAggregationType = refData.IdAggregationType,
                    IdLocation = refData.IdLocation,
                    IdDataSourceType = refData.IdDataSourceType
                };

                bool isManualAggregate = refData.IdDataSourceType.HasValue && refData.IdDataSourceType.Value == (int)Enums.DataSourceType.ManualDataEntry;
                
                OpDataTemporal temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_RAW_VOLUME_DISPENCED);
                reco.SalesDispencedGross = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, temporal);
                temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_RAW_REF_VOLUME_DISPENCED);
                reco.SalesDispencedNet = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, temporal);
                                
                temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_VOLUME_DISPENCED);
                reco.FlowDispencedGross = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, temporal);
                if (isManualAggregate) // Podmiana sprzedazy przy agregacie manualnym. Nie ma wtedy sprzedazy flow.
                    reco.FlowDispencedGross = reco.SalesDispencedGross;
                
                OpDataTemporal FlowDispencedNetTemporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_REF_VOLUME_DISPENCED);
                reco.FlowDispencedNet = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, FlowDispencedNetTemporal);
                if (isManualAggregate)// Podmiana sprzedazy przy agregacie manualnym. Nie ma wtedy sprzedazy flow.
                    reco.FlowDispencedNet = reco.SalesDispencedNet;

                OpDataTemporal fuelDistrDispensedOffset = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_DISPENSED_DISTR_OFFSET);
                if (fuelDistrDispensedOffset != null)
                    HandleDispensedOffsetEvent(fuelDistrDispensedOffset.ValueDouble, reco);

                temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_VOLUME_OUTCOME);
                reco.OutcomeGross = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, temporal);
                temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_REF_VOLUME_OUTCOME);
                reco.OutcomeNet = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, temporal);
                temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.FUEL_DEPTH);
                reco.FuelDepth = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, temporal);
                temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.FUEL_TEMPERATURE);
                reco.FuelTemperature = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, temporal);

                temporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_RECONCILIATION_DAY);
                if (temporal != null && temporal.ValueDateTime.HasValue)
                {
                    reco.Day = temporal.ValueDateTime.Value;
                }
                else
                {
                    reco.Day = reco.EndTime.Value.AddHours(-REFUEL_MATCH_TIME).Date;
                }

                // Dodatkowo sprawdzany jest typ endvolume z popdzedniego dnia
                OpDataTemporal endVolumeGrossTemporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_VOLUME);
                reco.EndVolumeGross = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, endVolumeGrossTemporal);
                OpDataTemporal startVolumeGrossTemporal = GetStartVolume(endFuelVolumeFromLastDayGROSS, endVolumeGrossTemporal, reco.Day, recoDateLastDay);
                reco.StartVolumeGross = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, startVolumeGrossTemporal);
                endFuelVolumeFromLastDayGROSS = endVolumeGrossTemporal;
                
                OpDataTemporal endVolumeNetTemporal = dataGroup.FirstOrDefault(d => d.IdDataType == DataType.AWSR_FUEL_REF_VOLUME);
                reco.EndVolumeNet = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, endVolumeNetTemporal);
                OpDataTemporal startVolumeNetTemporal = GetStartVolume(endFuelVolumeFromLastDayNET, endVolumeNetTemporal, reco.Day, recoDateLastDay);
                reco.StartVolumeNet = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, startVolumeNetTemporal);
                endFuelVolumeFromLastDayNET = endVolumeNetTemporal;

                // Brak outcome (trzeba obliczyc)
                if (reco.OutcomeGross == null)
                {
                    if (reco.StartVolumeGross.HasValue && reco.EndVolumeGross.HasValue)
                        reco.OutcomeGross = reco.StartVolumeGross.Value - reco.EndVolumeGross.Value;
                }
                if (reco.OutcomeNet == null)
                {
                    if (reco.StartVolumeNet.HasValue && reco.EndVolumeNet.HasValue)
                        reco.OutcomeNet = reco.StartVolumeNet.Value - reco.EndVolumeNet.Value;
                }

                List<OpDataTemporal> thisAggrDeliveries = new List<OpDataTemporal>();
                if (reco.IdAggregationType == (int)Enums.AggregationType.Day)
                {
                        deliveryData.TryGetValue(reco.Day, out thisAggrDeliveries);
                }
                else
                {
                    //deliveryData.TryGetValue(reco.EndTime.Value, out thisAggrDeliveries);
                    // Procedura dla dostaw nie działa za dobrze dla 15
                    // Zakomentowane przed audytem
                    //    thisAggrDeliveries = dataGroup.Where(d => d.IdDataType == DataType.AWSR_FUEL_VOLUME_DELIVERED || d.IdDataType == DataType.AWSR_FUEL_REF_VOLUME_DELIVERED).ToList();
                    deliveryData.TryGetValue(reco.EndTime.Value, out thisAggrDeliveries);

                }

                OpDataTemporal deliveryManualTemporalNet = null;
                OpDataTemporal deliveryCalculatedTemporalNet = null;
                if (thisAggrDeliveries != null)
                {
                    #region Delivery Gross
                    OpDataTemporal deliveryManualTemporalGross = thisAggrDeliveries.FirstOrDefault(d => d.IdDataSourceType == (int)Enums.DataSourceType.ManualDataEntry && d.IdDataType == DataType.AWSR_FUEL_VOLUME_DELIVERED);
                    reco.DeliveryManualGross = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, deliveryManualTemporalGross);
                    OpDataTemporal deliveryCalculatedTemporalGross = thisAggrDeliveries.FirstOrDefault(d => d.IdDataSourceType == (int)Enums.DataSourceType.OKO && d.IdDataType == DataType.AWSR_FUEL_VOLUME_DELIVERED);
                    reco.DeliveryCalculatedGross = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, deliveryCalculatedTemporalGross);

                    reco.DeliveryMixedGross = ChoseDelivery(reco.DeliveryManualGross, reco.DeliveryCalculatedGross, reco.OutcomeGross, reco.FlowDispencedGross, reco.SalesDispencedGross);

                    #endregion

                    #region Delivery Net
                    deliveryManualTemporalNet = thisAggrDeliveries.FirstOrDefault(d => d.IdDataSourceType == (int)Enums.DataSourceType.ManualDataEntry && d.IdDataType == DataType.AWSR_FUEL_REF_VOLUME_DELIVERED);
                    reco.DeliveryManualNet = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, deliveryManualTemporalNet);
                    deliveryCalculatedTemporalNet = thisAggrDeliveries.FirstOrDefault(d => d.IdDataSourceType == (int)Enums.DataSourceType.OKO && d.IdDataType == DataType.AWSR_FUEL_REF_VOLUME_DELIVERED);
                    reco.DeliveryCalculatedNet = GetTemporalDoubleValueUnitBased(dataTypeUnitConfig, deliveryCalculatedTemporalNet);

                    reco.DeliveryMixedNet = ChoseDelivery(reco.DeliveryManualNet, reco.DeliveryCalculatedNet, reco.OutcomeNet, reco.FlowDispencedNet, reco.SalesDispencedNet);
                    #endregion
                }
                #endregion

                #region Filtrowanie agregatów
                // Są dostępne dane do stock.
                if (!reco.OutcomeGross.HasValue && !reco.FlowDispencedGross.HasValue && !reco.DeliveryMixedGross.HasValue)
                    continue;

                // Aggregat musi byc nie krótszy niz 22h oraz nie dłuższy niz 26h
                //if (aggregationType == Enums.AggregationType.Day)
                //{
                //    if (!(reco.EndTime.Value - reco.StartTime.Value >= TimeSpan.FromHours(22) && reco.EndTime.Value - reco.StartTime.Value <= TimeSpan.FromHours(26)))
                //        continue;
                //}
                #endregion

                OpVariance scaledVariance = new OpVariance();
                OpVariance verifiedVariance = new OpVariance();
                
                scaledVariance.FlowErrorManualGross = CountVariance(reco.FlowDispencedGross, reco.OutcomeGross, reco.DeliveryManualGross);
                scaledVariance.FlowErrorManualNet = CountVariance(reco.FlowDispencedNet, reco.OutcomeNet, reco.DeliveryManualNet);
                scaledVariance.SalesErrorManualGross = CountVariance(reco.SalesDispencedGross, reco.OutcomeGross, reco.DeliveryManualGross);
                scaledVariance.SalesErrorManualNet = CountVariance(reco.SalesDispencedNet, reco.OutcomeNet, reco.DeliveryManualNet);

                scaledCV.FlowManualGross += scaledVariance.FlowErrorManualGross.HasValue ? scaledVariance.FlowErrorManualGross.Value : 0;
                scaledCV.FlowManualNet += scaledVariance.FlowErrorManualNet.HasValue ? scaledVariance.FlowErrorManualNet.Value : 0;
                scaledCV.SalesManualGross += scaledVariance.SalesErrorManualGross.HasValue ? scaledVariance.SalesErrorManualGross.Value : 0;
                scaledCV.SalesManualNet += scaledVariance.SalesErrorManualNet.HasValue ? scaledVariance.SalesErrorManualNet.Value : 0;
                        
                scaledVariance.FlowErrorCalculatedGross = CountVariance(reco.FlowDispencedGross, reco.OutcomeGross, reco.DeliveryCalculatedGross);
                scaledVariance.FlowErrorCalculatedNet = CountVariance(reco.FlowDispencedNet, reco.OutcomeNet, reco.DeliveryCalculatedNet);
                scaledVariance.SalesErrorCalculatedGross = CountVariance(reco.SalesDispencedGross, reco.OutcomeGross, reco.DeliveryCalculatedGross);
                scaledVariance.SalesErrorCalculatedNet = CountVariance(reco.SalesDispencedNet, reco.OutcomeNet, reco.DeliveryCalculatedNet);

                scaledCV.FlowCalculatedGross += scaledVariance.FlowErrorCalculatedGross.HasValue ? scaledVariance.FlowErrorCalculatedGross.Value : 0;
                scaledCV.FlowCalculatedNet += scaledVariance.FlowErrorCalculatedNet.HasValue ? scaledVariance.FlowErrorCalculatedNet.Value : 0;
                scaledCV.SalesCalculatedGross += scaledVariance.SalesErrorCalculatedGross.HasValue ? scaledVariance.SalesErrorCalculatedGross.Value : 0;
                scaledCV.SalesCalcuatedNet += scaledVariance.SalesErrorCalculatedNet.HasValue ? scaledVariance.SalesErrorCalculatedNet.Value : 0;
                        
                scaledVariance.FlowErrorMixedGross = CountVariance(reco.FlowDispencedGross, reco.OutcomeGross, reco.DeliveryMixedGross);
                scaledVariance.FlowErrorMixedNet = CountVariance(reco.FlowDispencedNet, reco.OutcomeNet, reco.DeliveryMixedNet);
                scaledVariance.SalesErrorMixedGross = CountVariance(reco.SalesDispencedGross, reco.OutcomeGross, reco.DeliveryMixedGross);
                scaledVariance.SalesErrorMixedNet = CountVariance(reco.SalesDispencedNet, reco.OutcomeNet, reco.DeliveryMixedNet);

                scaledCV.FlowMixedGross += scaledVariance.FlowErrorMixedGross.HasValue ? scaledVariance.FlowErrorMixedGross.Value : 0;
                scaledCV.FlowMixedNet += scaledVariance.FlowErrorMixedNet.HasValue ? scaledVariance.FlowErrorMixedNet.Value : 0;
                scaledCV.SalesMixedGross += scaledVariance.SalesErrorMixedGross.HasValue ? scaledVariance.SalesErrorMixedGross.Value : 0;
                scaledCV.SalesMixedNet += scaledVariance.SalesErrorMixedNet.HasValue ? scaledVariance.SalesErrorMixedNet.Value : 0;
                    
                if (refData.DataStatus != Enums.DataStatus.Unknown)
                {
                    verifiedVariance.FlowErrorManualGross = CountVariance(reco.FlowDispencedGross, reco.OutcomeGross, reco.DeliveryManualGross);
                    verifiedVariance.FlowErrorManualNet = CountVariance(reco.FlowDispencedNet, reco.OutcomeNet, reco.DeliveryManualNet);
                    verifiedVariance.SalesErrorManualGross = CountVariance(reco.SalesDispencedGross, reco.OutcomeGross, reco.DeliveryManualGross);
                    verifiedVariance.SalesErrorManualNet = CountVariance(reco.SalesDispencedNet, reco.OutcomeNet, reco.DeliveryManualNet);

                    verifiedCV.FlowManualGross += verifiedVariance.FlowErrorManualGross.HasValue ? verifiedVariance.FlowErrorManualGross.Value : 0;
                    verifiedCV.FlowManualNet += verifiedVariance.FlowErrorManualNet.HasValue ? verifiedVariance.FlowErrorManualNet.Value : 0;
                    verifiedCV.SalesManualGross += verifiedVariance.SalesErrorManualGross.HasValue ? verifiedVariance.SalesErrorManualGross.Value : 0;
                    verifiedCV.SalesManualNet += verifiedVariance.SalesErrorManualNet.HasValue ? verifiedVariance.SalesErrorManualNet.Value : 0;
                         
                    verifiedVariance.FlowErrorCalculatedGross = CountVariance(reco.FlowDispencedGross, reco.OutcomeGross, reco.DeliveryCalculatedGross);
                    verifiedVariance.FlowErrorCalculatedNet = CountVariance(reco.FlowDispencedNet, reco.OutcomeNet, reco.DeliveryCalculatedNet);
                    verifiedVariance.SalesErrorCalculatedGross = CountVariance(reco.SalesDispencedGross, reco.OutcomeGross, reco.DeliveryCalculatedGross);
                    verifiedVariance.SalesErrorCalculatedNet = CountVariance(reco.SalesDispencedNet, reco.OutcomeNet, reco.DeliveryCalculatedNet);

                    verifiedCV.FlowCalculatedGross += verifiedVariance.FlowErrorCalculatedGross.HasValue ? verifiedVariance.FlowErrorCalculatedGross.Value : 0;
                    verifiedCV.FlowCalculatedNet += verifiedVariance.FlowErrorCalculatedNet.HasValue ? verifiedVariance.FlowErrorCalculatedNet.Value : 0;
                    verifiedCV.SalesCalculatedGross += verifiedVariance.SalesErrorCalculatedGross.HasValue ? verifiedVariance.SalesErrorCalculatedGross.Value : 0;
                    verifiedCV.SalesCalcuatedNet += verifiedVariance.SalesErrorCalculatedNet.HasValue ? verifiedVariance.SalesErrorCalculatedNet.Value : 0;
                        
                    verifiedVariance.FlowErrorMixedGross = CountVariance(reco.FlowDispencedGross, reco.OutcomeGross, reco.DeliveryMixedGross);
                    verifiedVariance.FlowErrorMixedNet = CountVariance(reco.FlowDispencedNet, reco.OutcomeNet, reco.DeliveryMixedNet);
                    verifiedVariance.SalesErrorMixedGross = CountVariance(reco.SalesDispencedGross, reco.OutcomeGross, reco.DeliveryMixedGross);
                    verifiedVariance.SalesErrorMixedNet = CountVariance(reco.SalesDispencedNet, reco.OutcomeNet, reco.DeliveryMixedNet);

                    verifiedCV.FlowMixedGross += verifiedVariance.FlowErrorMixedGross.HasValue ? verifiedVariance.FlowErrorMixedGross.Value : 0;
                    verifiedCV.FlowMixedNet += verifiedVariance.FlowErrorMixedNet.HasValue ? verifiedVariance.FlowErrorMixedNet.Value : 0;
                    verifiedCV.SalesMixedGross += verifiedVariance.SalesErrorMixedGross.HasValue ? verifiedVariance.SalesErrorMixedGross.Value : 0;
                    verifiedCV.SalesMixedNet += verifiedVariance.SalesErrorMixedNet.HasValue ? verifiedVariance.SalesErrorMixedNet.Value : 0;                        
                }

                OpDynamicMeasure gridRow = new OpDynamicMeasure();
                gridRow.AddOrUpdateProperty(MdDynamicProperty.IsManual, isManualAggregate);

                #region Tworzenie DynamicMeasure
                gridRow[MdReconciliation.StartTime] = new OpDynamicProperty(MdReconciliation.StartTime, reco.StartTime);
                gridRow[MdReconciliation.EndTime] = new OpDynamicProperty(MdReconciliation.EndTime, reco.EndTime);
                gridRow[MdReconciliation.IdMeter] = new OpDynamicProperty(MdReconciliation.IdMeter, reco.IdMeter);
                gridRow[MdReconciliation.IdLocation] = new OpDynamicProperty(MdReconciliation.IdLocation, reco.IdLocation);
                gridRow[MdReconciliation.IdDataSourceType] = new OpDynamicProperty(MdReconciliation.IdDataSourceType, reco.IdDataSourceType);
                gridRow[MdReconciliation.IdAggregationType] = new OpDynamicProperty(MdReconciliation.IdAggregationType, reco.IdAggregationType);

                gridRow[MdReconciliation.SalesDispencedGross] = new OpDynamicProperty(MdReconciliation.SalesDispencedGross, Round(reco.SalesDispencedGross));
                gridRow[MdReconciliation.SalesDispencedNet] = new OpDynamicProperty(MdReconciliation.SalesDispencedNet, Round(reco.SalesDispencedNet));
                gridRow[MdReconciliation.FlowDispencedGross] = new OpDynamicProperty(MdReconciliation.FlowDispencedGross, Round(reco.FlowDispencedGross));
                gridRow[MdReconciliation.FlowDispencedNet] = new OpDynamicProperty(MdReconciliation.FlowDispencedNet, Round(reco.FlowDispencedNet));
                gridRow[MdReconciliation.OutcomeGross] = new OpDynamicProperty(MdReconciliation.OutcomeGross, Round(reco.OutcomeGross));
                gridRow[MdReconciliation.OutcomeNet] = new OpDynamicProperty(MdReconciliation.OutcomeNet, Round(reco.OutcomeNet));

                gridRow[MdReconciliation.StartVolumeGross] = new OpDynamicProperty(MdReconciliation.StartVolumeGross, Round(reco.StartVolumeGross));
                gridRow[MdReconciliation.EndVolumeGross] = new OpDynamicProperty(MdReconciliation.EndVolumeGross, Round(reco.EndVolumeGross));
                gridRow[MdReconciliation.StartVolumeNet] = new OpDynamicProperty(MdReconciliation.StartVolumeNet, Round(reco.StartVolumeNet));
                gridRow[MdReconciliation.EndVolumeNet] = new OpDynamicProperty(MdReconciliation.EndVolumeNet, Round(reco.EndVolumeNet));

                gridRow[MdReconciliation.FuelDepth] = new OpDynamicProperty(MdReconciliation.FuelDepth, Round(reco.FuelDepth));
                gridRow[MdReconciliation.FuelTemperature] = new OpDynamicProperty(MdReconciliation.FuelTemperature, Round(reco.FuelTemperature));

                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryManualGross, MdDynamicProperty.FlowSufix), Round(reco.DeliveryManualGross)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryManualNet, MdDynamicProperty.FlowSufix), Round(reco.DeliveryManualNet)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryCalculatedGross, MdDynamicProperty.FlowSufix), Round(reco.DeliveryCalculatedGross)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryCalculatedNet, MdDynamicProperty.FlowSufix), Round(reco.DeliveryCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryMixedGross, MdDynamicProperty.FlowSufix), Round(reco.DeliveryMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryMixedNet, MdDynamicProperty.FlowSufix), Round(reco.DeliveryMixedNet, 2)));

                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryManualGross, MdDynamicProperty.SalesSufix), Round(reco.DeliveryManualGross)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryManualNet, MdDynamicProperty.SalesSufix), Round(reco.DeliveryManualNet)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryCalculatedGross, MdDynamicProperty.SalesSufix), Round(reco.DeliveryCalculatedGross)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryCalculatedNet, MdDynamicProperty.SalesSufix), Round(reco.DeliveryCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryMixedGross, MdDynamicProperty.SalesSufix), Round(reco.DeliveryMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdReconciliation.DeliveryMixedNet, MdDynamicProperty.SalesSufix), Round(reco.DeliveryMixedNet, 2)));

                // DOMYŚLNIE FLOW
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdReconciliation.DeliveryManualGross, Round(reco.DeliveryManualGross)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdReconciliation.DeliveryManualNet, Round(reco.DeliveryManualNet)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdReconciliation.DeliveryCalculatedGross, Round(reco.DeliveryCalculatedGross)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdReconciliation.DeliveryCalculatedNet, Round(reco.DeliveryCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdReconciliation.DeliveryMixedGross, Round(reco.DeliveryMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdReconciliation.DeliveryMixedNet, Round(reco.DeliveryMixedNet, 2)));


                #region Scaled
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorManualGross, MdDynamicProperty.ScaledSufix), Round(scaledVariance.FlowErrorManualGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorManualNet, MdDynamicProperty.ScaledSufix), Round(scaledVariance.FlowErrorManualNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorCalculatedGross, MdDynamicProperty.ScaledSufix), Round(scaledVariance.FlowErrorCalculatedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorCalculatedNet, MdDynamicProperty.ScaledSufix), Round(scaledVariance.FlowErrorCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorMixedGross, MdDynamicProperty.ScaledSufix), Round(scaledVariance.FlowErrorMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorMixedNet, MdDynamicProperty.ScaledSufix), Round(scaledVariance.FlowErrorMixedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorManualGross, MdDynamicProperty.ScaledSufix), Round(scaledVariance.SalesErrorManualGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorManualNet, MdDynamicProperty.ScaledSufix), Round(scaledVariance.SalesErrorManualNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorCalculatedGross, MdDynamicProperty.ScaledSufix), Round(scaledVariance.SalesErrorCalculatedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorCalculatedNet, MdDynamicProperty.ScaledSufix), Round(scaledVariance.SalesErrorCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorMixedGross, MdDynamicProperty.ScaledSufix), Round(scaledVariance.SalesErrorMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorMixedNet, MdDynamicProperty.ScaledSufix), Round(scaledVariance.SalesErrorMixedNet, 2)));

                gridRow[string.Concat(MdCumulativeVariance.FlowManualGross, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowManualGross, MdDynamicProperty.ScaledSufix), Round(scaledCV.FlowManualGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowManualNet, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowManualNet, MdDynamicProperty.ScaledSufix), Round(scaledCV.FlowManualNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowCalculatedGross, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowCalculatedGross, MdDynamicProperty.ScaledSufix), Round(scaledCV.FlowCalculatedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowCalculatedNet, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowCalculatedNet, MdDynamicProperty.ScaledSufix), Round(scaledCV.FlowCalculatedNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowMixedGross, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowMixedGross, MdDynamicProperty.ScaledSufix), Round(scaledCV.FlowMixedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowMixedNet, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowMixedNet, MdDynamicProperty.ScaledSufix), Round(scaledCV.FlowMixedNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesManualGross, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesManualGross, MdDynamicProperty.ScaledSufix), Round(scaledCV.SalesManualGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesManualNet, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesManualNet, MdDynamicProperty.ScaledSufix), Round(scaledCV.SalesManualNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesCalculatedGross, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesCalculatedGross, MdDynamicProperty.ScaledSufix), Round(scaledCV.SalesCalculatedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesCalcuatedNet, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesCalcuatedNet, MdDynamicProperty.ScaledSufix), Round(scaledCV.SalesCalcuatedNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesMixedGross, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesMixedGross, MdDynamicProperty.ScaledSufix), Round(scaledCV.SalesMixedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesMixedNet, MdDynamicProperty.ScaledSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesMixedNet, MdDynamicProperty.ScaledSufix), Round(scaledCV.SalesMixedNet, 2));
                #endregion
                #region Verivied
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorManualGross, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.FlowErrorManualGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorManualNet, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.FlowErrorManualNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorCalculatedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.FlowErrorCalculatedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorCalculatedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.FlowErrorCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorMixedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.FlowErrorMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.FlowErrorMixedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.FlowErrorMixedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorManualGross, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.SalesErrorManualGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorManualNet, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.SalesErrorManualNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorCalculatedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.SalesErrorCalculatedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorCalculatedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.SalesErrorCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorMixedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.SalesErrorMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(MdVariance.SalesErrorMixedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedVariance.SalesErrorMixedNet, 2)));

                gridRow[string.Concat(MdCumulativeVariance.FlowManualGross, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowManualGross, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.FlowManualGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowManualNet, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowManualNet, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.FlowManualNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowCalculatedGross, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowCalculatedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.FlowCalculatedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowCalculatedNet, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowCalculatedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.FlowCalculatedNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowMixedGross, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowMixedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.FlowMixedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.FlowMixedNet, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.FlowMixedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.FlowMixedNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesManualGross, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesManualGross, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.SalesManualGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesManualNet, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesManualNet, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.SalesManualNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesCalculatedGross, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesCalculatedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.SalesCalculatedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesCalcuatedNet, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesCalcuatedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.SalesCalcuatedNet, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesMixedGross, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesMixedGross, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.SalesMixedGross, 2));
                gridRow[string.Concat(MdCumulativeVariance.SalesMixedNet, MdDynamicProperty.VerifiedSufix)] = new OpDynamicProperty(string.Concat(MdCumulativeVariance.SalesMixedNet, MdDynamicProperty.VerifiedSufix), Round(verifiedCV.SalesMixedNet, 2));
                #endregion

                #region Default scaled
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.FlowErrorManualGross, Round(scaledVariance.FlowErrorManualGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.FlowErrorManualNet, Round(scaledVariance.FlowErrorManualNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.FlowErrorCalculatedGross, Round(scaledVariance.FlowErrorCalculatedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.FlowErrorCalculatedNet, Round(scaledVariance.FlowErrorCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.FlowErrorMixedGross, Round(scaledVariance.FlowErrorMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.FlowErrorMixedNet, Round(scaledVariance.FlowErrorMixedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.SalesErrorManualGross, Round(scaledVariance.SalesErrorManualGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.SalesErrorManualNet, Round(scaledVariance.SalesErrorManualNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.SalesErrorCalculatedGross, Round(scaledVariance.SalesErrorCalculatedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.SalesErrorCalculatedNet, Round(scaledVariance.SalesErrorCalculatedNet, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.SalesErrorMixedGross, Round(scaledVariance.SalesErrorMixedGross, 2)));
                gridRow.DynamicProperties.AddProperty(new OpDynamicProperty(MdVariance.SalesErrorMixedNet, Round(scaledVariance.SalesErrorMixedNet, 2)));

                gridRow[MdCumulativeVariance.FlowManualGross] = new OpDynamicProperty(MdCumulativeVariance.FlowManualGross, Round(scaledCV.FlowManualGross, 2));
                gridRow[MdCumulativeVariance.FlowManualNet] = new OpDynamicProperty(MdCumulativeVariance.FlowManualNet, Round(scaledCV.FlowManualNet, 2));
                gridRow[MdCumulativeVariance.FlowCalculatedGross] = new OpDynamicProperty(MdCumulativeVariance.FlowCalculatedGross, Round(scaledCV.FlowCalculatedGross, 2));
                gridRow[MdCumulativeVariance.FlowCalculatedNet] = new OpDynamicProperty(MdCumulativeVariance.FlowCalculatedNet, Round(scaledCV.FlowCalculatedNet, 2));
                gridRow[MdCumulativeVariance.FlowMixedGross] = new OpDynamicProperty(MdCumulativeVariance.FlowMixedGross, Round(scaledCV.FlowMixedGross, 2));
                gridRow[MdCumulativeVariance.FlowMixedNet] = new OpDynamicProperty(MdCumulativeVariance.FlowMixedNet, Round(scaledCV.FlowMixedNet, 2));
                gridRow[MdCumulativeVariance.SalesManualGross] = new OpDynamicProperty(MdCumulativeVariance.SalesManualGross, Round(scaledCV.SalesManualGross, 2));
                gridRow[MdCumulativeVariance.SalesManualNet] = new OpDynamicProperty(MdCumulativeVariance.SalesManualNet, Round(scaledCV.SalesManualNet, 2));
                gridRow[MdCumulativeVariance.SalesCalculatedGross] = new OpDynamicProperty(MdCumulativeVariance.SalesCalculatedGross, Round(scaledCV.SalesCalculatedGross, 2));
                gridRow[MdCumulativeVariance.SalesCalcuatedNet] = new OpDynamicProperty(MdCumulativeVariance.SalesCalcuatedNet, Round(scaledCV.SalesCalcuatedNet, 2));
                gridRow[MdCumulativeVariance.SalesMixedGross] = new OpDynamicProperty(MdCumulativeVariance.SalesMixedGross, Round(scaledCV.SalesMixedGross, 2));
                gridRow[MdCumulativeVariance.SalesMixedNet] = new OpDynamicProperty(MdCumulativeVariance.SalesMixedNet, Round(scaledCV.SalesMixedNet, 2));
                #endregion

                #endregion
                gridRow.DynamicProperties.AddOrUpdateProperty(string.Concat(MdReconciliation.FlowDispencedNet, MdDynamicProperty.TemporalSufix), FlowDispencedNetTemporal);                
                gridRow.DynamicProperties.AddOrUpdateProperty(string.Concat(MdReconciliation.DeliveryCalculatedNet, MdDynamicProperty.TemporalSufix), deliveryCalculatedTemporalNet);
                gridRow.DynamicProperties.AddOrUpdateProperty(string.Concat(MdReconciliation.DeliveryManualNet, MdDynamicProperty.TemporalSufix), deliveryManualTemporalNet);
                gridRow.DynamicProperties.AddOrUpdateProperty(string.Concat(MdReconciliation.EndVolumeNet, MdDynamicProperty.TemporalSufix), endVolumeNetTemporal);

                gridRow.AddOrUpdateProperty(MdDynamicProperty.IsManual, isManualAggregate);

                #region Delivery declared calculated diff
                try
                {
                    var diffGross = Convert.ToDouble(reco.DeliveryManualGross) - Convert.ToDouble(reco.DeliveryCalculatedGross);
                    var diffNet = Convert.ToDouble(reco.DeliveryManualNet) - Convert.ToDouble(reco.DeliveryCalculatedNet);
                    var diffPercGross = 0.0;
                    var diffPercNet = 0.0;

                    if (Convert.ToDouble(reco.DeliveryCalculatedGross) != 0)
                        diffPercGross = diffGross / Convert.ToDouble(reco.DeliveryCalculatedGross);
                    if (Convert.ToDouble(reco.DeliveryCalculatedNet) != 0)
                        diffPercNet = diffNet / Convert.ToDouble(reco.DeliveryCalculatedNet);

                    gridRow.AddOrUpdateProperty(MdDynamicProperty.DeliveryReconciliationSoldGross, diffGross);
                    gridRow.AddOrUpdateProperty(MdDynamicProperty.DeliveryReconciliationSoldNet, diffNet);
                    gridRow.AddOrUpdateProperty(MdDynamicProperty.DeliveryReconciliationSoldGross + "perc", diffPercGross);
                    gridRow.AddOrUpdateProperty(MdDynamicProperty.DeliveryReconciliationSoldGross + "perc", diffPercNet);
                }
                catch(Exception)
                {
                    // not available
                }
                #endregion

                recoData.Add(gridRow);

                recoDateLastDay = reco.Day;
            }

            try
            {
                if (outlier.HasValue)
                {
                    double cvGross = 0;
                    double cvNet = 0;
                    foreach (var item in recoData)
                    {
                        double? grossErr = item.GetNullablePropertyValue<double>(MdVariance.FlowErrorMixedGross);
                        if (Math.Abs(grossErr.GetValueOrDefault(0)) > outlier.Value)
                        {
                            item.DynamicProperties.AddOrUpdateProperty(string.Concat(MdVariance.FlowErrorMixedGross, MdDynamicProperty.OutlierSufix), null);
                        }
                        else
                        {
                            cvGross += grossErr.GetValueOrDefault(0);
                            item.DynamicProperties.AddOrUpdateProperty(string.Concat(MdVariance.FlowErrorMixedGross, MdDynamicProperty.OutlierSufix), grossErr);
                        }
                        double? netErr = item.GetNullablePropertyValue<double>(MdVariance.FlowErrorMixedNet);
                        if (Math.Abs(netErr.GetValueOrDefault(0)) > outlier.Value)
                        {
                            item.DynamicProperties.AddOrUpdateProperty(string.Concat(MdVariance.FlowErrorMixedNet, MdDynamicProperty.OutlierSufix), null);
                        }
                        else
                        {
                            cvNet += netErr.GetValueOrDefault(0);
                            item.DynamicProperties.AddOrUpdateProperty(string.Concat(MdVariance.FlowErrorMixedNet, MdDynamicProperty.OutlierSufix), netErr);
                        }

                        item.DynamicProperties.AddOrUpdateProperty(string.Concat(MdCumulativeVariance.FlowMixedGross, MdDynamicProperty.OutlierSufix), Math.Round(cvGross, 2));
                        item.DynamicProperties.AddOrUpdateProperty(string.Concat(MdCumulativeVariance.FlowMixedNet, MdDynamicProperty.OutlierSufix), Math.Round(cvNet, 2));
                    }
                }
            }
            catch (Exception)
            {

            }

            return recoData;
        }
        #endregion

        #region GetCvOutlierRemoved
        private static void GetCvOutlierRemoved(List<OpDynamicMeasure> recoData, double? outlierThreshold, string varianceName)
        {
            try
            {
                for (int i = 0; i < recoData.Count; i++)
                {
                    double? currErr = null;
                    OpDynamicMeasure currRow = recoData[i];
                    if (currRow != null)
                        currErr = currRow.GetNullablePropertyValue<double>(varianceName);

                    if (currErr.HasValue && (Math.Abs(currErr.Value) > outlierThreshold.Value))
                    {
                        //PREVIOUS
                        double? prevErr = null;
                        if (i - 1 > 0)
                        {
                            OpDynamicMeasure prevRow = recoData[i - 1];
                            if (prevRow != null)
                                prevErr = prevRow.GetNullablePropertyValue<double>(varianceName);
                        }

                        // NEXT
                        double? nextErr = null;
                        if (i + 1 < recoData.Count) // Do ostatniego wiersza 
                        {
                            OpDynamicMeasure nextRow = recoData[i + 1];
                            if (nextRow != null)
                                nextErr = nextRow.GetNullablePropertyValue<double>(varianceName);
                        }

                        // AVG
                        double? avgErr = null;
                        if (prevErr.HasValue && nextErr.HasValue)
                        {
                            avgErr = (prevErr.Value + nextErr.Value) / 2;
                            currRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(varianceName, MdDynamicProperty.OutlierSufix), avgErr));
                        }
                        else
                        {
                            currRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(varianceName, MdDynamicProperty.OutlierSufix), currErr));
                        }
                    }
                    else
                    {
                        currRow.DynamicProperties.AddProperty(new OpDynamicProperty(string.Concat(varianceName, MdDynamicProperty.OutlierSufix), currErr));
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                // Ignore
            }
        }
        #endregion

        #region ChoseDelivery
        private static double? ChoseDelivery(double? deliveryDeclared, double? deliveryCalculated, double? outcome, double? flowDisp, double? salesDisp)
        {
            double? theChosenOne = deliveryDeclared;

            if (deliveryDeclared.HasValue)
            {
                if (flowDisp.HasValue && outcome.HasValue)
                {
                    if (deliveryCalculated.HasValue)
                    {
                        double declErr = Math.Abs(CountVariance(flowDisp, outcome, deliveryDeclared).Value);
                        if (declErr >= 1500)
                        {
                            double calcErr = Math.Abs(CountVariance(flowDisp, outcome, deliveryCalculated).Value);
                            theChosenOne = declErr > calcErr ? deliveryCalculated : deliveryDeclared;
                        }
                    }
                }
                else if (salesDisp.HasValue && outcome.HasValue)
                {
                    if (deliveryCalculated.HasValue)
                    {
                        double declErr = Math.Abs(CountVariance(salesDisp, outcome, deliveryDeclared).Value);
                        if (declErr >= 1500)
                        {
                            double calcErr = Math.Abs(CountVariance(salesDisp, outcome, deliveryCalculated).Value);
                            theChosenOne = declErr > calcErr ? deliveryCalculated : deliveryDeclared;
                        }
                    }
                }
            }
            else
                theChosenOne = deliveryCalculated;

            return theChosenOne;
        }
        #endregion

        #region HandleDispensedOffsetEvent
        private static void HandleDispensedOffsetEvent(double? fuelDispensedOffset, OpReconciliation item)
        {
            if (fuelDispensedOffset != null && fuelDispensedOffset.HasValue)
            {
                if (item.FlowDispencedGross.HasValue)
                    item.FlowDispencedGross = item.FlowDispencedGross.Value - fuelDispensedOffset.Value * 1000;
                if (item.FlowDispencedNet.HasValue)
                    item.FlowDispencedNet = item.FlowDispencedNet.Value - fuelDispensedOffset.Value * 1000;
                if (item.SalesDispencedGross.HasValue)
                    item.SalesDispencedGross = item.SalesDispencedGross.Value - fuelDispensedOffset.Value * 1000;
                if (item.SalesDispencedNet.HasValue)
                    item.SalesDispencedNet = item.SalesDispencedNet.Value - fuelDispensedOffset.Value * 1000;
            }
        }
        #endregion

        #region GetStartVolume
        private static OpDataTemporal GetStartVolume(OpDataTemporal endFuelVolumeFromLastDay, OpDataTemporal endFuelVolumeFromToday, DateTime recoDayNow, DateTime? recoDayLastDay)
        {
            if (endFuelVolumeFromLastDay != null && endFuelVolumeFromToday != null &&
                endFuelVolumeFromLastDay.IdDataSourceType == endFuelVolumeFromToday.IdDataSourceType && // czy ten sam typ agregatu (manual/auto)
                recoDayLastDay.HasValue) // czy maja czasy do porownania                    
            {
                int days = (recoDayNow - recoDayLastDay.Value).Days;
                if (days == 1)
                    return endFuelVolumeFromLastDay;
            }
            return null;
        }
        #endregion
        
        #region GetTemporalDoubleRoundedValue
        private static double? GetTemporalDoubleValueUnitBased(Dictionary<long, Tuple<OpUnit, int>> dataTypeUnitConfig, OpDataTemporal temporal)
        {
            if (temporal == null) return null;
            double? valueDisplay = null;
            
            if (dataTypeUnitConfig.ContainsKey(temporal.IdDataType) && dataTypeUnitConfig[temporal.IdDataType] != null && temporal.ValueDouble.HasValue && temporal.DataType != null)
            {
                valueDisplay = (double)CORE.UnitComponent.Evaluate(temporal.ValueDouble, temporal.DataType, dataTypeUnitConfig[temporal.IdDataType].Item1);//(double?)CORE.UnitComponent.Evaluate(temporal, dataTypeUnitConfig[temporal.IdDataType]);
            }
            else
                valueDisplay = temporal.ValueDouble;

            return valueDisplay;
        }
        #endregion
        #region Round
        private static double? Round(object measure, int digits = 2)
        {
            if (measure == null) return null;
            double? m = measure as double?;
            return m != null && m.HasValue ? (double?)Math.Round(m.Value, digits) : null;
        }
        #endregion

        #region PrepareMeterBand
        private static void PrepareMeterBand(OpDynamicMeasure gridRowToEdit, OpMeter opMeter, OpProductCode opProductCode, Dictionary<long, OpMeter> meterParentDict,
            int? idDataSourceType, bool alwaysAdd)
        {
            if (meterParentDict == null)
                meterParentDict = new Dictionary<long, OpMeter>();

            if (opMeter != null && gridRowToEdit != null)
            {
                if (alwaysAdd || (idDataSourceType.HasValue && idDataSourceType.Value == (int)Enums.DataSourceType.AWSR)) // tylko dla Calculated laczymy za pomoca ',' meternbr i idmeters
                {
                    if (opMeter.TankNbr.HasValue)
                    {
                        if (gridRowToEdit.ContainsPropertyKey(MdDynamicProperty.MeterNbrs))
                        {
                            List<string> tankNbrs = gridRowToEdit.DynamicProperties[MdDynamicProperty.MeterNbrs].ToString().Split(',').ToList();
                            if (tankNbrs.Count > 0)
                            {
                                tankNbrs.Add(opMeter.TankNbr.Value.ToString());
                                gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterNbrs, String.Join(",", tankNbrs.ToArray()));
                            }
                            else
                                gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterNbrs, opMeter.TankNbr.Value.ToString());
                        }
                        else
                            gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterNbrs, opMeter.TankNbr.Value.ToString());
                    }
                    if (!String.IsNullOrWhiteSpace(opMeter.MeterSerialNbr))
                    {
                        if (gridRowToEdit.ContainsPropertyKey(MdDynamicProperty.MeterSerialNbrs))
                        {
                            List<string> serialNbrs = gridRowToEdit.DynamicProperties[MdDynamicProperty.MeterSerialNbrs].ToString().Split(',').ToList();
                            if (serialNbrs.Count > 0)
                            {
                                serialNbrs.Add(opMeter.MeterSerialNbr);
                                gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterSerialNbrs, String.Join(",", serialNbrs.ToArray()));
                            }
                            else
                                gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterSerialNbrs, opMeter.MeterSerialNbr);
                        }
                        else
                            gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterSerialNbrs, opMeter.MeterSerialNbr);
                    }

                    if (gridRowToEdit.ContainsPropertyKey(MdDynamicProperty.MeterIds))
                    {
                        List<string> tankNbrs = gridRowToEdit.DynamicProperties[MdDynamicProperty.MeterIds].ToString().Split(',').ToList();
                        if (tankNbrs != null && tankNbrs.Count > 0)
                        {
                            tankNbrs.Add(opMeter.IdMeter.ToString());
                            gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterIds, String.Join(",", tankNbrs.ToArray()));
                        }
                        else
                            gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterIds, opMeter.IdMeter.ToString());
                    }
                    else
                        gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterIds, opMeter.IdMeter.ToString());

                    double? tankCapacity = opMeter.TankCapacity;
                    if (tankCapacity.HasValue)
                    {
                        if (gridRowToEdit.ContainsPropertyKey(MdMeter.TankCapacity))
                        {
                            if (gridRowToEdit.ContainsPropertyKey(MdDynamicProperty.CapacityMeterIds))
                            {
                                if (!(gridRowToEdit.DynamicProperties[MdDynamicProperty.CapacityMeterIds] as List<long>).Contains(opMeter.IdMeter))
                                {
                                    gridRowToEdit.AddOrUpdateProperty(MdMeter.TankCapacity, GenericConverter.Parse<double>(gridRowToEdit.DynamicProperties[MdMeter.TankCapacity]) + tankCapacity.Value);
                                    (gridRowToEdit.DynamicProperties[MdDynamicProperty.CapacityMeterIds] as List<long>).Add(opMeter.IdMeter);
                                }
                            }
                        }
                        else
                        {
                            gridRowToEdit.AddOrUpdateProperty(MdMeter.TankCapacity, tankCapacity.Value);
                            gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.CapacityMeterIds, typeof(List<long>), new List<long>() { opMeter.IdMeter });
                        }
                    }

                    long? parentId = opMeter.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                    if (parentId.HasValue && meterParentDict.ContainsKey(parentId.Value))
                    {
                        OpMeter parentMeter = meterParentDict[parentId.Value];
                        if (parentMeter != null)
                        {
                            if (gridRowToEdit.ContainsPropertyKey(MdDynamicProperty.ParentMeter))
                            {
                                List<string> tankNbrs = gridRowToEdit.DynamicProperties[MdDynamicProperty.ParentMeter].ToString().Split(',').ToList();
                                if (tankNbrs != null && tankNbrs.Count > 0)
                                {
                                    tankNbrs.Add(parentMeter.IdMeter.ToString());
                                    gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.ParentMeter, String.Join(",", tankNbrs.ToArray()));
                                }
                                else
                                    gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.ParentMeter, parentMeter.IdMeter.ToString());
                            }
                            else
                                gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.ParentMeter, parentMeter.IdMeter.ToString());
                        }
                    }
                }
                if (opProductCode != null && !gridRowToEdit.ContainsPropertyKey(MdMeter.ProductCode))
                    gridRowToEdit.AddOrUpdateProperty(MdMeter.ProductCode, opProductCode.Code);
                if (opMeter.IdProductCode.HasValue && !gridRowToEdit.ContainsPropertyKey(MdMeter.IdProductCode))
                    gridRowToEdit.AddOrUpdateProperty(MdMeter.IdProductCode, opMeter.IdProductCode.Value);
                if (!gridRowToEdit.ContainsPropertyKey(MdMeter.IdMeter))
                    gridRowToEdit.AddOrUpdateProperty(MdMeter.IdMeter, opMeter.IdMeter);
                if (!gridRowToEdit.ContainsPropertyKey(MdDynamicProperty.MeterList))
                    gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.MeterList, new List<OpMeter>());
                (gridRowToEdit.DynamicProperties[MdDynamicProperty.MeterList] as List<OpMeter>).Add(opMeter);
                if (!gridRowToEdit.ContainsPropertyKey(ObjDW.Metadata.MdRefuel.Meter))
                    gridRowToEdit.AddOrUpdateProperty(ObjDW.Metadata.MdRefuel.Meter, opMeter);
            }
        }
        #endregion
        #region PrepareLocationBand
        private static void PrepareLocationBand(OpDynamicMeasure gridRowToEdit, OpLocation opLocation)
        {
            if (opLocation != null && gridRowToEdit != null)
            {
                if (!gridRowToEdit.ContainsPropertyKey(MdLocation.IdLocation))
                    gridRowToEdit.AddOrUpdateProperty(MdLocation.IdLocation, opLocation.IdLocation);
                if (!gridRowToEdit.ContainsPropertyKey(MdLocation.CID))
                    gridRowToEdit.AddOrUpdateProperty(MdLocation.CID, opLocation.CID);
                if (!gridRowToEdit.ContainsPropertyKey(MdLocation.Name))
                    gridRowToEdit.AddOrUpdateProperty(MdLocation.Name, opLocation.Name);
                if (!gridRowToEdit.ContainsPropertyKey(MdLocation.City))
                    gridRowToEdit.AddOrUpdateProperty(MdLocation.City, opLocation.City);
                if (!gridRowToEdit.ContainsPropertyKey(MdLocation.Address))
                    gridRowToEdit.AddOrUpdateProperty(MdLocation.Address, opLocation.Address);
                if (!gridRowToEdit.ContainsPropertyKey(ObjDW.Metadata.MdRefuel.Location))
                    gridRowToEdit.AddOrUpdateProperty(ObjDW.Metadata.MdRefuel.Location, opLocation);
            }
        }
        #endregion
        #region PrepareSourceBand
        private static void PrepareSourceBand(OpDynamicMeasure gridRowToEdit, OpMeter opMeter, long idRefuel, Dictionary<long, OpRefuelData> refuelDataDict,
            DateTime? refuelStartTime, DateTime? refuelEndTime, int? idDataSourceType, Dictionary<long, long> dataTypeDict)
        {
            if (!idDataSourceType.HasValue || gridRowToEdit == null || dataTypeDict == null || dataTypeDict.Count == 0 || refuelDataDict == null) return;

            #region Source strings
            string deliveryDayStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationDeliveryDay, idDataSourceType.Value);
            string startTimeStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartTime, idDataSourceType.Value);
            string endTimeStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndTime, idDataSourceType.Value);
            string orderNumberStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationOrderNbr, idDataSourceType.Value);
            string truckNumberStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationTruckNbr, idDataSourceType.Value);
            string driverNameStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationDriverName, idDataSourceType.Value);
            string terminalNameStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationTerminalName, idDataSourceType.Value);
            string carrierNameStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationCarrierName, idDataSourceType.Value);
            string statusStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStatus, idDataSourceType.Value);
            string refuelIdsStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationRefuelIds, idDataSourceType.Value);
            string grossStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationGross, idDataSourceType.Value);
            string netStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationNet, idDataSourceType.Value);
            string grossListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationGrossList, idDataSourceType.Value);
            string netListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationNetList, idDataSourceType.Value);
            string tempStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationLoadingTemperature, idDataSourceType.Value);
            string densStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationLoadingDensity, idDataSourceType.Value);
            string weightStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationTotalWeight, idDataSourceType.Value);
            string tempListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationLoadingTemperatureList, idDataSourceType.Value);
            string densListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationLoadingDensityList, idDataSourceType.Value);
            string weightListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationTotalWeightList, idDataSourceType.Value);
            string soldGrossStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationSoldGross, idDataSourceType.Value);
            string soldNetStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationSoldNet, idDataSourceType.Value);
            string startVolumeStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartVolume, idDataSourceType);
            string endVolumeStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndVolume, idDataSourceType);
            string startTemperatureStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartTemperature, idDataSourceType);
            string endTemperatureStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndTemperature, idDataSourceType);
            string startVolumeListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartVolumeList, idDataSourceType);
            string endVolumeListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndVolumeList, idDataSourceType);
            string startTemperatureListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartTemperatureList, idDataSourceType);
            string endTemperatureListStr = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndTemperatureList, idDataSourceType);
            string awsrStartLevel = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartLevel, (int)Enums.DataSourceType.AWSR);
            string awsrEndLevel = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndLevel, (int)Enums.DataSourceType.AWSR);
            string awsrStartLevelList = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationStartLevelList, (int)Enums.DataSourceType.AWSR);
            string awsrEndLevelList = String.Format("{0}.{1}", MdDynamicProperty.DeliveryReconciliationEndLevelList, (int)Enums.DataSourceType.AWSR);
            #endregion

            double? startLevelPerValue = null;
            double? endLevelPerValue = null;
            double? grossValue = dataTypeDict.ContainsKey(DataType.REFUEL_TOTAL_VOLUME) && refuelDataDict.ContainsKey(DataType.REFUEL_TOTAL_VOLUME) && refuelDataDict[DataType.REFUEL_TOTAL_VOLUME] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_TOTAL_VOLUME].Value) : null;
            double? netValue = dataTypeDict.ContainsKey(DataType.REFUEL_TOTAL_REFERENCE_VOLUME) && refuelDataDict.ContainsKey(DataType.REFUEL_TOTAL_REFERENCE_VOLUME) && refuelDataDict[DataType.REFUEL_TOTAL_REFERENCE_VOLUME] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_TOTAL_REFERENCE_VOLUME].Value) : null;
            double? soldGrossValue = dataTypeDict.ContainsKey(DataType.REFUEL_SOLD_VOLUME_GROSS) && refuelDataDict.ContainsKey(DataType.REFUEL_SOLD_VOLUME_GROSS) && refuelDataDict[DataType.REFUEL_SOLD_VOLUME_GROSS] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_SOLD_VOLUME_GROSS].Value) : null;
            double? soldNetValue = dataTypeDict.ContainsKey(DataType.REFUEL_SOLD_VOLUME_NET) && refuelDataDict.ContainsKey(DataType.REFUEL_SOLD_VOLUME_NET) && refuelDataDict[DataType.REFUEL_SOLD_VOLUME_NET] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_SOLD_VOLUME_NET].Value) : null;
            double? tempValue = dataTypeDict.ContainsKey(DataType.REFUEL_LOADING_TEMPERATURE) && refuelDataDict.ContainsKey(DataType.REFUEL_LOADING_TEMPERATURE) && refuelDataDict[DataType.REFUEL_LOADING_TEMPERATURE] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_LOADING_TEMPERATURE].Value) : null;
            double? weightValue = dataTypeDict.ContainsKey(DataType.REFUEL_TOTAL_WEIGHT) && refuelDataDict.ContainsKey(DataType.REFUEL_TOTAL_WEIGHT) && refuelDataDict[DataType.REFUEL_TOTAL_WEIGHT] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_TOTAL_WEIGHT].Value) : null;
            double? densValue = dataTypeDict.ContainsKey(DataType.REFUEL_LOADING_DENSITY) && refuelDataDict.ContainsKey(DataType.REFUEL_LOADING_DENSITY) && refuelDataDict[DataType.REFUEL_LOADING_DENSITY] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_LOADING_DENSITY].Value) : null;
            string orderValue = dataTypeDict.ContainsKey(DataType.REFUEL_ORDER_NUMBER) && refuelDataDict.ContainsKey(DataType.REFUEL_ORDER_NUMBER) && refuelDataDict[DataType.REFUEL_ORDER_NUMBER] != null ? refuelDataDict[DataType.REFUEL_ORDER_NUMBER].Value as string : null;
            string truckValue = dataTypeDict.ContainsKey(DataType.REFUEL_TRUCK_NUMBER) && refuelDataDict.ContainsKey(DataType.REFUEL_TRUCK_NUMBER) && refuelDataDict[DataType.REFUEL_TRUCK_NUMBER] != null ? refuelDataDict[DataType.REFUEL_TRUCK_NUMBER].Value as string : null;
            string driverValue = dataTypeDict.ContainsKey(DataType.REFUEL_DRIVER_NAME) && refuelDataDict.ContainsKey(DataType.REFUEL_DRIVER_NAME) && refuelDataDict[DataType.REFUEL_DRIVER_NAME] != null ? refuelDataDict[DataType.REFUEL_DRIVER_NAME].Value as string : null;
            string terminalValue = dataTypeDict.ContainsKey(DataType.REFUEL_TERMINAL_NAME) && refuelDataDict.ContainsKey(DataType.REFUEL_TERMINAL_NAME) && refuelDataDict[DataType.REFUEL_TERMINAL_NAME] != null ? refuelDataDict[DataType.REFUEL_TERMINAL_NAME].Value as string : null;
            string carrierValue = dataTypeDict.ContainsKey(DataType.REFUEL_CARRIER_NAME) && refuelDataDict.ContainsKey(DataType.REFUEL_CARRIER_NAME) && refuelDataDict[DataType.REFUEL_CARRIER_NAME] != null ? refuelDataDict[DataType.REFUEL_CARRIER_NAME].Value as string : null;
            double? startVolume = dataTypeDict.ContainsKey(DataType.REFUEL_START_VOLUME) && refuelDataDict.ContainsKey(DataType.REFUEL_START_VOLUME) && refuelDataDict[DataType.REFUEL_START_VOLUME] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_START_VOLUME].Value) : null;
            double? endVolume = dataTypeDict.ContainsKey(DataType.REFUEL_END_VOLUME) && refuelDataDict.ContainsKey(DataType.REFUEL_END_VOLUME) && refuelDataDict[DataType.REFUEL_END_VOLUME] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_END_VOLUME].Value) : null;
            double? startTemperature = dataTypeDict.ContainsKey(DataType.REFUEL_START_TEMPERATURE) && refuelDataDict.ContainsKey(DataType.REFUEL_START_TEMPERATURE) && refuelDataDict[DataType.REFUEL_START_TEMPERATURE] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_START_TEMPERATURE].Value) : null;
            double? endTemperature = dataTypeDict.ContainsKey(DataType.REFUEL_END_TEMPERATURE) && refuelDataDict.ContainsKey(DataType.REFUEL_END_TEMPERATURE) && refuelDataDict[DataType.REFUEL_END_TEMPERATURE] != null ? GenericConverter.ParseNullable<double>(refuelDataDict[DataType.REFUEL_END_TEMPERATURE].Value) : null;
            DateTime? deliveryDayValue = dataTypeDict.ContainsKey(DataType.REFUEL_DELIVERY_DAY) && refuelDataDict.ContainsKey(DataType.REFUEL_DELIVERY_DAY) && refuelDataDict[DataType.REFUEL_DELIVERY_DAY] != null ? GenericConverter.ParseNullable<DateTime>(refuelDataDict[DataType.REFUEL_DELIVERY_DAY].Value) : null;
            int? status = null;
            if (opMeter != null)
            {
                double? tankCapacity = opMeter.TankCapacity;
                if (tankCapacity.HasValue && tankCapacity.Value != 0)
                {
                    if (startVolume.HasValue)
                        startLevelPerValue = ((startVolume.Value * 1000.0) / opMeter.TankCapacity.Value) * 100.0;
                    if (endVolume.HasValue)
                        endLevelPerValue = ((endVolume.Value * 1000.0) / opMeter.TankCapacity.Value) * 100.0;
                }
            }
            OpRefuelData statusData = dataTypeDict.ContainsKey(DataType.REFUEL_TOTAL_VOLUME) && refuelDataDict.ContainsKey(DataType.REFUEL_TOTAL_VOLUME) ? refuelDataDict[DataType.REFUEL_TOTAL_VOLUME] : null;
            if (statusData != null)
                status = statusData.Status;

            if (gridRowToEdit.ContainsPropertyKey(MdDynamicProperty.DeliveryReconciliationRefuelIds))
                (gridRowToEdit.GetPropertyValue(MdDynamicProperty.DeliveryReconciliationRefuelIds) as List<long>).Add(idRefuel);
            else
                gridRowToEdit.AddOrUpdateProperty(MdDynamicProperty.DeliveryReconciliationRefuelIds, new List<long>() { idRefuel });

            bool addGrossNet = true;
            if (gridRowToEdit.ContainsPropertyKey(refuelIdsStr))
            {
                List<string> idRefuelsList = gridRowToEdit.DynamicProperties[refuelIdsStr].ToString().Split(',').ToList();
                if (idRefuelsList != null && idRefuelsList.Count > 0)
                {
                    if (idRefuelsList.Contains(idRefuel.ToString()))
                        addGrossNet = false;
                    else
                    {
                        idRefuelsList.Add(idRefuel.ToString());
                        gridRowToEdit.AddOrUpdateProperty(refuelIdsStr, String.Join(",", idRefuelsList.ToArray()));
                    }
                }
                else
                    gridRowToEdit.AddOrUpdateProperty(refuelIdsStr, idRefuel.ToString());
            }
            else
                gridRowToEdit.AddOrUpdateProperty(refuelIdsStr, idRefuel.ToString());

            if (idDataSourceType.Value == (int)Enums.DataSourceType.AWSR)
            {
                if (startLevelPerValue.HasValue)
                {
                    if (gridRowToEdit.ContainsPropertyKey(awsrStartLevelList))
                        (gridRowToEdit.GetPropertyValue(awsrStartLevelList) as List<double>).Add(startLevelPerValue.Value);
                    else
                        gridRowToEdit.AddOrUpdateProperty(awsrStartLevelList, new List<double>() { startLevelPerValue.Value });
                    gridRowToEdit.AddOrUpdateProperty(awsrStartLevel, (gridRowToEdit.GetPropertyValue(awsrStartLevelList) as List<double>).Average());
                }
                if (endLevelPerValue.HasValue)
                {
                    if (gridRowToEdit.ContainsPropertyKey(awsrEndLevelList))
                        (gridRowToEdit.GetPropertyValue(awsrEndLevelList) as List<double>).Add(endLevelPerValue.Value);
                    else
                        gridRowToEdit.AddOrUpdateProperty(awsrEndLevelList, new List<double>() { endLevelPerValue.Value });
                    gridRowToEdit.AddOrUpdateProperty(awsrEndLevel, (gridRowToEdit.GetPropertyValue(awsrEndLevelList) as List<double>).Average());
                }
            }

            if (startVolume.HasValue && addGrossNet)
            {
                if (gridRowToEdit.ContainsPropertyKey(startVolumeListStr))
                    (gridRowToEdit.GetPropertyValue(startVolumeListStr) as List<double>).Add(startVolume.Value * 1000.0);
                else
                    gridRowToEdit.AddOrUpdateProperty(startVolumeListStr, new List<double>() { startVolume.Value * 1000.0 });
                gridRowToEdit.AddOrUpdateProperty(startVolumeStr, (gridRowToEdit.GetPropertyValue(startVolumeListStr) as List<double>).Average());
            }
            if (endVolume.HasValue && addGrossNet)
            {
                if (gridRowToEdit.ContainsPropertyKey(endVolumeListStr))
                    (gridRowToEdit.GetPropertyValue(endVolumeListStr) as List<double>).Add(endVolume.Value * 1000.0);
                else
                    gridRowToEdit.AddOrUpdateProperty(endVolumeListStr, new List<double>() { endVolume.Value * 1000.0 });
                gridRowToEdit.AddOrUpdateProperty(endVolumeStr, (gridRowToEdit.GetPropertyValue(endVolumeListStr) as List<double>).Average());
            }
            if (startTemperature.HasValue && addGrossNet)
            {
                if (gridRowToEdit.ContainsPropertyKey(startTemperatureListStr))
                    (gridRowToEdit.GetPropertyValue(startTemperatureListStr) as List<double>).Add(startTemperature.Value);
                else
                    gridRowToEdit.AddOrUpdateProperty(startTemperatureListStr, new List<double>() { startTemperature.Value });
                gridRowToEdit.AddOrUpdateProperty(startTemperatureStr, (gridRowToEdit.GetPropertyValue(startTemperatureListStr) as List<double>).Average());
            }
            if (endTemperature.HasValue && addGrossNet)
            {
                if (gridRowToEdit.ContainsPropertyKey(endTemperatureListStr))
                    (gridRowToEdit.GetPropertyValue(endTemperatureListStr) as List<double>).Add(endTemperature.Value);
                else
                    gridRowToEdit.AddOrUpdateProperty(endTemperatureListStr, new List<double>() { endTemperature.Value });
                gridRowToEdit.AddOrUpdateProperty(endTemperatureStr, (gridRowToEdit.GetPropertyValue(endTemperatureListStr) as List<double>).Average());
            }

            if (grossValue.HasValue)
            {
                if (gridRowToEdit.ContainsPropertyKey(grossStr))
                {
                    if (addGrossNet)
                    {
                        gridRowToEdit.AddOrUpdateProperty(grossStr, GenericConverter.Parse<double>(gridRowToEdit.DynamicProperties[grossStr]) + (grossValue.Value * 1000.0));

                        if (gridRowToEdit.ContainsPropertyKey(grossListStr))
                            (gridRowToEdit.DynamicProperties[grossListStr] as List<Tuple<double, long>>).Add(new Tuple<double, long>(grossValue.Value * 1000.0, idRefuel));
                        else
                            gridRowToEdit.AddOrUpdateProperty(grossListStr, new List<Tuple<double, long>>() { new Tuple<double, long>(grossValue.Value * 1000.0, idRefuel) });
                    }
                }
                else
                {
                    gridRowToEdit.AddOrUpdateProperty(grossStr, grossValue.Value * 1000.0);

                    if (gridRowToEdit.ContainsPropertyKey(grossListStr))
                        (gridRowToEdit.DynamicProperties[grossListStr] as List<Tuple<double, long>>).Add(new Tuple<double, long>(grossValue.Value * 1000.0, idRefuel));
                    else
                        gridRowToEdit.AddOrUpdateProperty(grossListStr, new List<Tuple<double, long>>() { new Tuple<double, long>(grossValue.Value * 1000.0, idRefuel) });
                }
            }

            if (netValue.HasValue)
            {
                if (gridRowToEdit.ContainsPropertyKey(netStr))
                {
                    if (addGrossNet)
                    {
                        gridRowToEdit.AddOrUpdateProperty(netStr, GenericConverter.Parse<double>(gridRowToEdit.DynamicProperties[netStr]) + (netValue.Value * 1000.0));

                        if (gridRowToEdit.ContainsPropertyKey(netListStr))
                            (gridRowToEdit.DynamicProperties[netListStr] as List<Tuple<double, long>>).Add(new Tuple<double, long>(netValue.Value * 1000.0, idRefuel));
                        else
                            gridRowToEdit.AddOrUpdateProperty(netListStr, new List<Tuple<double, long>>() { new Tuple<double, long>(netValue.Value * 1000.0, idRefuel) });
                    }
                }
                else
                {
                    gridRowToEdit[netStr] = new OpDynamicProperty(netStr, netValue.Value * 1000.0);

                    if (gridRowToEdit.ContainsPropertyKey(netListStr))
                        (gridRowToEdit.DynamicProperties[netListStr] as List<Tuple<double, long>>).Add(new Tuple<double, long>(netValue.Value * 1000.0, idRefuel));
                    else
                        gridRowToEdit.AddOrUpdateProperty(netListStr, new List<Tuple<double, long>>() { new Tuple<double, long>(netValue.Value * 1000.0, idRefuel) });
                }
            }

            if (tempValue.HasValue && addGrossNet)
            {
                if (gridRowToEdit.ContainsPropertyKey(tempListStr))
                    (gridRowToEdit.DynamicProperties[tempListStr] as List<Tuple<double, long>>).Add(new Tuple<double, long>(tempValue.Value, idRefuel));
                else
                    gridRowToEdit.AddOrUpdateProperty(tempListStr, new List<Tuple<double, long>>() { new Tuple<double, long>(tempValue.Value, idRefuel) });
                gridRowToEdit.AddOrUpdateProperty(tempStr, (gridRowToEdit.DynamicProperties[tempListStr] as List<Tuple<double, long>>).Average(q => q.Item1));
            }

            if (densValue.HasValue && addGrossNet)
            {
                if (gridRowToEdit.ContainsPropertyKey(densListStr))
                    (gridRowToEdit.DynamicProperties[densListStr] as List<Tuple<double, long>>).Add(new Tuple<double, long>(densValue.Value, idRefuel));
                else
                    gridRowToEdit.AddOrUpdateProperty(densListStr, new List<Tuple<double, long>>() { new Tuple<double, long>(densValue.Value, idRefuel) });
                gridRowToEdit.AddOrUpdateProperty(densStr, (gridRowToEdit.DynamicProperties[densListStr] as List<Tuple<double, long>>).Average(q => q.Item1));
            }

            if (weightValue.HasValue && addGrossNet)
            {
                if (gridRowToEdit.ContainsPropertyKey(weightListStr))
                    (gridRowToEdit.DynamicProperties[weightListStr] as List<Tuple<double, long>>).Add(new Tuple<double, long>(weightValue.Value, idRefuel));
                else
                    gridRowToEdit.AddOrUpdateProperty(weightListStr, new List<Tuple<double, long>>() { new Tuple<double, long>(weightValue.Value, idRefuel) });
                gridRowToEdit.AddOrUpdateProperty(weightStr, (gridRowToEdit.DynamicProperties[weightListStr] as List<Tuple<double, long>>).Average(q => q.Item1));
            }

            if (soldGrossValue.HasValue)
            {
                if (gridRowToEdit.ContainsPropertyKey(soldGrossStr))
                {
                    if (addGrossNet)
                        gridRowToEdit.AddOrUpdateProperty(soldGrossStr, GenericConverter.Parse<double>(gridRowToEdit.DynamicProperties[soldGrossStr]) + (soldGrossValue.Value * 1000.0));
                }
                else
                    gridRowToEdit.AddOrUpdateProperty(soldGrossStr, soldGrossValue.Value * 1000.0);
            }

            if (soldNetValue.HasValue)
            {
                if (gridRowToEdit.ContainsPropertyKey(soldNetStr))
                {
                    if (addGrossNet)
                        gridRowToEdit.AddOrUpdateProperty(soldNetStr, GenericConverter.Parse<double>(gridRowToEdit.DynamicProperties[soldNetStr]) + (soldNetValue.Value * 1000.0));
                }
                else
                    gridRowToEdit.AddOrUpdateProperty(soldNetStr, soldNetValue.Value * 1000.0);
            }

            if (status.HasValue)
            {
                if (status.Value == 0)
                    gridRowToEdit.AddOrUpdateProperty(statusStr, 0);
                else
                    gridRowToEdit.AddOrUpdateProperty(statusStr, status.Value);
            }

            if (idDataSourceType.Value != (int)Enums.DataSourceType.AWSR)
            {
                if (refuelStartTime.HasValue)
                    gridRowToEdit.AddOrUpdateProperty(startTimeStr, refuelStartTime.Value);

                if (refuelEndTime.HasValue)
                    gridRowToEdit.AddOrUpdateProperty(endTimeStr, refuelEndTime.Value);
            }
            else
            {
                if (refuelStartTime.HasValue)
                {
                    if (gridRowToEdit.ContainsPropertyKey(startTimeStr))
                    {
                        DateTime startTimeTemp = GenericConverter.Parse<DateTime>(gridRowToEdit.DynamicProperties[startTimeStr]);
                        if (refuelStartTime.Value < startTimeTemp)
                            gridRowToEdit.AddOrUpdateProperty(startTimeStr, refuelStartTime.Value);
                    }
                    else
                        gridRowToEdit.AddOrUpdateProperty(startTimeStr, refuelStartTime.Value);
                }
                if (refuelEndTime.HasValue)
                {
                    if (gridRowToEdit.ContainsPropertyKey(endTimeStr))
                    {
                        DateTime endTimeTemp = GenericConverter.Parse<DateTime>(gridRowToEdit.DynamicProperties[endTimeStr]);
                        if (refuelEndTime.Value > endTimeTemp)
                            gridRowToEdit.AddOrUpdateProperty(endTimeStr, refuelEndTime.Value);
                    }
                    else
                        gridRowToEdit.AddOrUpdateProperty(endTimeStr, refuelEndTime.Value);
                }
            }

            if (!String.IsNullOrWhiteSpace(orderValue))
                gridRowToEdit.AddOrUpdateProperty(orderNumberStr, orderValue);

            if (!String.IsNullOrWhiteSpace(truckValue))
                gridRowToEdit.AddOrUpdateProperty(truckNumberStr, truckValue);

            if (!String.IsNullOrWhiteSpace(driverValue))
                gridRowToEdit.AddOrUpdateProperty(driverNameStr, driverValue);

            if (!String.IsNullOrWhiteSpace(terminalValue))
                gridRowToEdit.AddOrUpdateProperty(terminalNameStr, terminalValue);

            if (!String.IsNullOrWhiteSpace(carrierValue))
                gridRowToEdit.AddOrUpdateProperty(carrierNameStr, carrierValue);

            if (deliveryDayValue.HasValue)
                gridRowToEdit.AddOrUpdateProperty(deliveryDayStr, deliveryDayValue);
        }
        #endregion
        #region PrepareDeliveryReconciliation
        private static void PrepareDeliveryReconciliation(DataProvider dataProvider,
            Dictionary<long, OpRefuel> refuelDict, Dictionary<long, List<OpRefuelData>> refuelDataDict,
            Dictionary<long, OpMeter> meterDict, Dictionary<long, OpMeter> meterParentDict, Dictionary<long, OpLocation> locationDict,
            List<long> customDataTypes,
            bool returnPerProduct, bool returnPerTank, bool returnPerRefuel,
            out OpBindingList<OpDynamicMeasure> listPerProduct,
            out OpBindingList<OpDynamicMeasure> listPerTank,
            out OpBindingList<OpDynamicMeasure> listPerRefuel)
        {
            listPerProduct = new OpBindingList<OpDynamicMeasure>(new List<OpDynamicMeasure>());
            listPerTank = new OpBindingList<OpDynamicMeasure>(new List<OpDynamicMeasure>());
            listPerRefuel = new OpBindingList<OpDynamicMeasure>(new List<OpDynamicMeasure>());

            if (refuelDict == null || refuelDict.Count == 0
                || refuelDataDict == null || refuelDataDict.Count == 0) return;

            if (meterDict == null)
                meterDict = new Dictionary<long, OpMeter>();
            if (meterParentDict == null)
                meterParentDict = new Dictionary<long, OpMeter>();
            if (locationDict == null)
                locationDict = new Dictionary<long, OpLocation>();

            Dictionary<long, long> dataTypeIds = customDataTypes != null ? customDataTypes.Distinct().ToDictionary(q => q) : new Dictionary<long, long>();
            dataTypeIds[DataType.REFUEL_GUID] = DataType.REFUEL_GUID;
            dataTypeIds[DataType.REFUEL_TOTAL_VOLUME] = DataType.REFUEL_TOTAL_VOLUME;
            dataTypeIds[DataType.REFUEL_TOTAL_REFERENCE_VOLUME] = DataType.REFUEL_TOTAL_REFERENCE_VOLUME;

            Dictionary<string, OpDynamicMeasure> dataSourcePerProduct = new Dictionary<string, OpDynamicMeasure>();
            Dictionary<string, OpDynamicMeasure> dataSourcePerTank = new Dictionary<string, OpDynamicMeasure>();
            Dictionary<string, OpDynamicMeasure> dataSourcePerRefuel = new Dictionary<string, OpDynamicMeasure>();
            Dictionary<int, OpProductCode> productCodeDict = dataProvider.GetAllProductCode().Distinct(q => q.IdProductCode).ToDictionary(q => q.IdProductCode);
            DateTime? refStartTime = null;
            DateTime? refEndTime = null;

            Dictionary<long, Dictionary<long, OpRefuelData>> refuelIdDataTypeDict = refuelDataDict.ToDictionary(q => q.Key, q =>
                {
                    return q.Value.Where(d => d.Index == 0).GroupBy(w => w.IdDataType).ToDictionary(w => w.Key, w =>
                        {
                            return w.FirstOrDefault();
                        });
                });

            foreach (KeyValuePair<long, Dictionary<long, OpRefuelData>> item in refuelIdDataTypeDict)
            {
                string refuelGuidStr = null;
                string refuelOrderNbrStr = null;
                string refuelIdMeterStr = null;

                OpMeter opMeter = null;
                OpLocation opLocation = null;
                int? idDataSourceType = null;
                long? idLocation = null;
                int? idProductCode = null;

                OpProductCode opProductCode = null;
                DateTime? startTime = null;

                OpRefuel refuel = refuelDict.ContainsKey(item.Key) ? refuelDict[item.Key] : null;
                if (refuel != null && refuel.IdMeter.HasValue && refuel.IdLocation.HasValue)
                {
                    opMeter = meterDict.ContainsKey(refuel.IdMeter.Value) ? meterDict[refuel.IdMeter.Value] : null;
                    opLocation = locationDict.ContainsKey(refuel.IdLocation.Value) ? locationDict[refuel.IdLocation.Value] : null;
                    idLocation = refuel.IdLocation;
                    if (refuel.DataSource != null)
                        idDataSourceType = refuel.DataSource.IdDataSourceType;
                    refStartTime = refuel.StartTime;
                    refEndTime = refuel.EndTime;
                    startTime = refuel.StartTime;
                }
                else
                    continue;

                if (!idDataSourceType.HasValue)
                    continue;

                if (opMeter != null)
                {
                    if (opMeter.IdProductCode.HasValue)
                        opProductCode = productCodeDict.ContainsKey(opMeter.IdProductCode.Value) ? productCodeDict[opMeter.IdProductCode.Value] : null;
                    refuelIdMeterStr = opMeter.IdMeter.ToString();
                }
                if (opProductCode != null)
                    idProductCode = opProductCode.IdProductCode;

                OpRefuelData opRefuelGuid = item.Value.ContainsKey(DataType.REFUEL_GUID) ? item.Value[DataType.REFUEL_GUID] : null;
                if (opRefuelGuid != null && opRefuelGuid.Value != null)
                    refuelGuidStr = opRefuelGuid.Value.ToString();
                OpRefuelData opRefuelOrderNbr = item.Value.ContainsKey(DataType.REFUEL_ORDER_NUMBER) ? item.Value[DataType.REFUEL_ORDER_NUMBER] : null;
                if (opRefuelOrderNbr != null && opRefuelOrderNbr.Value != null)
                    refuelOrderNbrStr = opRefuelOrderNbr.Value.ToString();

                #region Per product
                if (returnPerProduct)
                {
                    #region Istnieje guid
                    if (!String.IsNullOrEmpty(refuelGuidStr) && idProductCode.HasValue)
                    {
                        #region Laczenie po istniejacym juz guid
                        if (dataSourcePerProduct.ContainsKey(String.Format("GUID_{0}_PRODUCT_{1}", refuelGuidStr, idProductCode.Value)))
                        {
                            OpDynamicMeasure gridRowToEdit = dataSourcePerProduct[String.Format("GUID_{0}_PRODUCT_{1}", refuelGuidStr, idProductCode.Value)];

                            PrepareMeterBand(gridRowToEdit, opMeter, opProductCode, meterParentDict, idDataSourceType, false);
                            PrepareLocationBand(gridRowToEdit, opLocation);
                            PrepareSourceBand(gridRowToEdit, opMeter, item.Key, item.Value,
                                refStartTime, refEndTime, idDataSourceType, dataTypeIds);
                        }
                        #endregion
                        #region Nowe guid
                        else
                        {
                            string key = String.Format("GUID_{0}_PRODUCT_{1}", refuelGuidStr, idProductCode.Value);
                            OpDynamicMeasure gridRowToAdd = new OpDynamicMeasure();
                            gridRowToAdd[MdDynamicProperty.IsRefuelJoined] = new OpDynamicProperty(MdDynamicProperty.IsRefuelJoined, true);
                            gridRowToAdd[MdDynamicProperty.Guid] = new OpDynamicProperty(MdDynamicProperty.Guid, refuelGuidStr);
                            gridRowToAdd[MdDynamicProperty.JoinType] = new OpDynamicProperty(MdDynamicProperty.JoinType, ResourcesText.Guid);
                            gridRowToAdd[MdDynamicProperty.DictionaryKey] = new OpDynamicProperty(MdDynamicProperty.DictionaryKey, key);
                            gridRowToAdd[ObjDW.Metadata.MdDataSourceType.IdDataSourceType] = new OpDynamicProperty(ObjDW.Metadata.MdDataSourceType.IdDataSourceType, idDataSourceType.Value);

                            PrepareMeterBand(gridRowToAdd, opMeter, opProductCode, meterParentDict, idDataSourceType, false);
                            PrepareLocationBand(gridRowToAdd, opLocation);
                            PrepareSourceBand(gridRowToAdd, opMeter, item.Key, item.Value,
                                refStartTime, refEndTime, idDataSourceType, dataTypeIds);

                            dataSourcePerProduct.Add(key, gridRowToAdd);
                        }
                        #endregion
                    }
                    #endregion
                    #region Nie laczymy
                    else
                    {
                        string key = String.Format("NONE_{0}_ID_{1}", Guid.NewGuid().ToString(), item.Key);
                        OpDynamicMeasure gridRowToAdd = new OpDynamicMeasure();
                        gridRowToAdd[MdDynamicProperty.IsRefuelJoined] = new OpDynamicProperty(MdDynamicProperty.IsRefuelJoined, false);
                        gridRowToAdd[MdDynamicProperty.Guid] = new OpDynamicProperty(MdDynamicProperty.Guid, null);
                        gridRowToAdd[MdDynamicProperty.JoinType] = new OpDynamicProperty(MdDynamicProperty.JoinType, ResourcesText.None);
                        gridRowToAdd[MdDynamicProperty.DictionaryKey] = new OpDynamicProperty(MdDynamicProperty.DictionaryKey, key);
                        gridRowToAdd[ObjDW.Metadata.MdDataSourceType.IdDataSourceType] = new OpDynamicProperty(ObjDW.Metadata.MdDataSourceType.IdDataSourceType, idDataSourceType.Value);

                        PrepareMeterBand(gridRowToAdd, opMeter, opProductCode, meterParentDict, idDataSourceType, true);
                        PrepareLocationBand(gridRowToAdd, opLocation);
                        PrepareSourceBand(gridRowToAdd, opMeter, item.Key, item.Value,
                                refStartTime, refEndTime, idDataSourceType, dataTypeIds);

                        dataSourcePerProduct.Add(key, gridRowToAdd);
                    }
                    #endregion
                }
                #endregion
                #region Per tank
                if (returnPerTank)
                {
                    #region Istnieje guid/idmeter
                    if (!String.IsNullOrEmpty(refuelGuidStr) && !String.IsNullOrEmpty(refuelIdMeterStr))
                    {
                        #region Laczenie po istniejacym juz guid/idmeter
                        if (dataSourcePerTank.ContainsKey(String.Format("GUID_{0}_METER_{1}", refuelGuidStr, refuelIdMeterStr)))
                        {
                            OpDynamicMeasure gridRowToEdit = dataSourcePerTank[String.Format("GUID_{0}_METER_{1}", refuelGuidStr, refuelIdMeterStr)];

                            PrepareMeterBand(gridRowToEdit, opMeter, opProductCode, meterParentDict, idDataSourceType, false);
                            PrepareLocationBand(gridRowToEdit, opLocation);
                            PrepareSourceBand(gridRowToEdit, opMeter, item.Key, item.Value,
                                refStartTime, refEndTime, idDataSourceType, dataTypeIds);
                        }
                        #endregion
                        #region Nowe guid/idmeter
                        else
                        {
                            string key = String.Format("GUID_{0}_METER_{1}", refuelGuidStr, refuelIdMeterStr);
                            OpDynamicMeasure gridRowToAdd = new OpDynamicMeasure();
                            gridRowToAdd[MdDynamicProperty.IsRefuelJoined] = new OpDynamicProperty(MdDynamicProperty.IsRefuelJoined, true);
                            gridRowToAdd[MdDynamicProperty.Guid] = new OpDynamicProperty(MdDynamicProperty.Guid, refuelGuidStr);
                            gridRowToAdd[MdDynamicProperty.JoinType] = new OpDynamicProperty(MdDynamicProperty.JoinType, ResourcesText.Guid);
                            gridRowToAdd[MdDynamicProperty.DictionaryKey] = new OpDynamicProperty(MdDynamicProperty.DictionaryKey, key);
                            gridRowToAdd[ObjDW.Metadata.MdDataSourceType.IdDataSourceType] = new OpDynamicProperty(ObjDW.Metadata.MdDataSourceType.IdDataSourceType, idDataSourceType.Value);

                            PrepareMeterBand(gridRowToAdd, opMeter, opProductCode, meterParentDict, idDataSourceType, false);
                            PrepareLocationBand(gridRowToAdd, opLocation);
                            PrepareSourceBand(gridRowToAdd, opMeter, item.Key, item.Value,
                                refStartTime, refEndTime, idDataSourceType, dataTypeIds);

                            dataSourcePerTank.Add(key, gridRowToAdd);
                        }
                        #endregion
                    }
                    #endregion
                    #region Nie laczymy
                    else
                    {
                        string key = String.Format("NONE_{0}_ID_{1}", Guid.NewGuid().ToString(), item.Key);
                        OpDynamicMeasure gridRowToAdd = new OpDynamicMeasure();
                        gridRowToAdd[MdDynamicProperty.IsRefuelJoined] = new OpDynamicProperty(MdDynamicProperty.IsRefuelJoined, false);
                        gridRowToAdd[MdDynamicProperty.Guid] = new OpDynamicProperty(MdDynamicProperty.Guid, null);
                        gridRowToAdd[MdDynamicProperty.JoinType] = new OpDynamicProperty(MdDynamicProperty.JoinType, ResourcesText.None);
                        gridRowToAdd[MdDynamicProperty.DictionaryKey] = new OpDynamicProperty(MdDynamicProperty.DictionaryKey, key);
                        gridRowToAdd[ObjDW.Metadata.MdDataSourceType.IdDataSourceType] = new OpDynamicProperty(ObjDW.Metadata.MdDataSourceType.IdDataSourceType, idDataSourceType.Value);

                        PrepareMeterBand(gridRowToAdd, opMeter, opProductCode, meterParentDict, idDataSourceType, true);
                        PrepareLocationBand(gridRowToAdd, opLocation);
                        PrepareSourceBand(gridRowToAdd, opMeter, item.Key, item.Value,
                                refStartTime, refEndTime, idDataSourceType, dataTypeIds);

                        dataSourcePerTank.Add(key, gridRowToAdd);
                    }
                    #endregion
                }
                #endregion
                #region Per refuel
                if (returnPerRefuel)
                {
                    string key = String.Format("NONE_{0}_ID_{1}", Guid.NewGuid().ToString(), item.Key);
                    OpDynamicMeasure gridRowToAdd = new OpDynamicMeasure();
                    gridRowToAdd[MdDynamicProperty.IsRefuelJoined] = new OpDynamicProperty(MdDynamicProperty.IsRefuelJoined, false);
                    gridRowToAdd[MdDynamicProperty.Guid] = new OpDynamicProperty(MdDynamicProperty.Guid, refuelGuidStr);
                    gridRowToAdd[MdDynamicProperty.JoinType] = new OpDynamicProperty(MdDynamicProperty.JoinType, ResourcesText.None);
                    gridRowToAdd[MdDynamicProperty.DictionaryKey] = new OpDynamicProperty(MdDynamicProperty.DictionaryKey, key);
                    gridRowToAdd[ObjDW.Metadata.MdDataSourceType.IdDataSourceType] = new OpDynamicProperty(ObjDW.Metadata.MdDataSourceType.IdDataSourceType, idDataSourceType.Value);
                    gridRowToAdd[ObjDW.Metadata.MdRefuel.IdRefuel] = new OpDynamicProperty(ObjDW.Metadata.MdRefuel.IdRefuel, item.Key);

                    PrepareMeterBand(gridRowToAdd, opMeter, opProductCode, meterParentDict, idDataSourceType, true);
                    PrepareLocationBand(gridRowToAdd, opLocation);
                    PrepareSourceBand(gridRowToAdd, opMeter, item.Key, item.Value,
                            refStartTime, refEndTime, idDataSourceType, dataTypeIds);

                    dataSourcePerRefuel.Add(key, gridRowToAdd);
                }
                #endregion
            }

            listPerProduct = new OpBindingList<OpDynamicMeasure>(dataSourcePerProduct.Select(q => q.Value).ToList());
            LoadDeliveryReconciliationProperties(listPerProduct);
            listPerTank = new OpBindingList<OpDynamicMeasure>(dataSourcePerTank.Select(q => q.Value).ToList());
            LoadDeliveryReconciliationProperties(listPerTank);
            listPerRefuel = new OpBindingList<OpDynamicMeasure>(dataSourcePerRefuel.Select(q => q.Value).ToList());
            LoadDeliveryReconciliationProperties(listPerRefuel);
        }
        #endregion
    }
}
