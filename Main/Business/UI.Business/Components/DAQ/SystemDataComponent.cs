﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.DAQ
{
    public class SystemDataComponent
    {
        #region Methods
        public static long GetDictTablesVersion(DataProvider dataProvider)
        {
            List<OpSystemData> list = dataProvider.GetSystemDataFilterDAQ(loadNavigationProperties: false, IdDataType: new long[] { DataType.SYSTEM_DICTIONARY_TABLES_VERSION });
            if (list != null && list.Count > 0)
            {
                object value = list[0].Value;
                if (value != null)
                    return Convert.ToInt64(value);
            }
            return 0;
        }
        #endregion
    }
}
