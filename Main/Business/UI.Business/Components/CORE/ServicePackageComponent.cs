﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.IO;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ServicePackageComponent : BaseComponent
    {
        #region Save

        public static OpServicePackage Save(DataProvider dataProvider, OpServicePackage objectToSave)
        {
            if (objectToSave.IdContract == 0)
                objectToSave.IdContract = null;
            long id = dataProvider.SaveServicePackage(objectToSave);
            for (int i = 0; i < objectToSave.DataList.Count; i++)
            {
                objectToSave.DataList[i].IdServicePackage = id;
                if (objectToSave.DataList[i].OpState == Objects.OpChangeState.New || objectToSave.DataList[i].OpState == Objects.OpChangeState.Modified)
                {
                    dataProvider.SaveServicePackageData(objectToSave.DataList[i]);
                    objectToSave.DataList[i].OpState = Objects.OpChangeState.Loaded;
                }
                else if (objectToSave.DataList[i].OpState == Objects.OpChangeState.Delete)
                {
                    dataProvider.DeleteServicePackageData(objectToSave.DataList[i]);
                    objectToSave.DataList.Remove(objectToSave.DataList[i], true);
                    i--;
                }
            }
            return dataProvider.GetServicePackage(id, true);
        }

        #endregion

        #region Delete 

        public static void Delete(DataProvider dataProvider, OpServicePackage objectToDelete)
        {
            if (objectToDelete != null && objectToDelete.IdServicePackage > 0)
            {
                if (objectToDelete.DataList != null)
                {
                    foreach (OpServicePackageData cdItem in objectToDelete.DataList)
                    {
                        if (cdItem.IdServicePackageData > 0)
                            dataProvider.DeleteServicePackageData(cdItem);
                    }
                    objectToDelete.DataList.Clear();
                }
                DeleteByIdServicePackage(dataProvider, objectToDelete.IdServicePackage);
                dataProvider.DeleteServicePackage(objectToDelete);
            }
        }

        public static void DeleteByIdServicePackage(DataProvider dataProvider, long idServicePackage)
        {
            List<OpServiceInPackage> servicesInPackageList = dataProvider.GetServiceInPackageFilter(IdServicePackage: new long[] { idServicePackage });
            foreach (OpServiceInPackage sinItem in servicesInPackageList)
                dataProvider.DeleteServiceInPackage(sinItem);
        }

        #endregion
    }
}
