﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class FileComponent
    {
        public static OpFile GetFile(IDataProvider dataProvider, long idFile)
        {
            OpFile file = dataProvider.GetFile(idFile);
            if (file != null && file.FileBytes == null)
                dataProvider.GetFileContent(file);

            return file;
        }

        [Obsolete("Use IDataProvider override.")]
        public static OpFile GetFile(DataProvider dataProvider, long idFile)
        {
            OpFile file = dataProvider.GetFile(idFile);
            if (file != null && file.FileBytes == null)
                dataProvider.GetFileContent(file);

            return file;
        }

        /// <summary>
        /// Reads file from specific path and save to OpFile object
        /// </summary>
        /// <param name="path">The file path to read</param>
        /// <param name="file">Object to fill data with</param>
        public static void ReadFileContentFromFile(string path, OpFile file)
        {
            using (FileStream fstream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                byte[] fileBytes = new byte[fstream.Length];
                ReadWholeArray(fstream, fileBytes);
                file.FileBytes = fileBytes;
                file.Size = fstream.Length;
                fstream.Close();
            }
        }

        /// <summary>
        /// Writes file from OpFile object to specific file location
        /// </summary>
        /// <param name="path">The file path to write</param>
        /// <param name="file">Object get data from</param>
        public static void SaveFileContentToFile(string path, OpFile file)
        {
            using (FileStream fstream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                byte[] fileBytes = file.FileBytes as byte[];
                fstream.Write(fileBytes, 0, fileBytes.Length);
                fstream.Close();
            }
        }

        /// <summary>
        /// Reads data into a complete array, throwing an EndOfStreamException
        /// if the stream runs out of data first, or if an IOException
        /// naturally occurs.
        /// </summary>
        /// <param name="stream">The stream to read data from</param>
        /// <param name="data">The array to read bytes into. The array
        /// will be completely filled from the stream, so an appropriate
        /// size must be given.</param>
        public static void ReadWholeArray(Stream stream, byte[] data)
        {
            int offset = 0;
            int remaining = data.Length;
            while (remaining > 0)
            {
                int read = stream.Read(data, offset, remaining);
                if (read <= 0)
                    throw new EndOfStreamException
                        (String.Format("End of stream reached with {0} bytes left to read", remaining));
                remaining -= read;
                offset += read;
            }
        }


        public static bool DownloadFileFromImrprod(DataProvider dataProvider, string downloadDirectory, string fileName, string savePath)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                string filePathDownloaded = ftpClient.DownloadFile(savePath, downloadDirectory, fileName);
                if (filePathDownloaded != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Enums.FileExtension GetFileExtension(OpFile file)
        {
            return file.Extension;
        }
    }
}
