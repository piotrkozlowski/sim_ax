﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class PackageComponent : BaseComponent
    {
        #region Methods
        public static List<OpPackage> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllPackage();
        }

        public static OpPackage GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpPackage GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetPackage(Id, queryDatabase);
        }

        public static List<OpPackage> GetPackagesForOperator(DataProvider dataProvider, int idOperator)
        {
            //return dataProvider.GetPackagesForOperator(idOperator);
            return dataProvider.GetPackageFilter(IdOperatorCreator: new int[] { idOperator }).Concat(
                    dataProvider.GetPackageFilter(IdOperatorReceiver: new int[] { idOperator })).Distinct(p => p.IdPackage).ToList();
        }

        public static List<int> GetPackageOrderStatusList()
        {
            List<int> retList = new List<int>();
            retList.Add((int)Enums.PackageStatus.NewOrder);
            retList.Add((int)Enums.PackageStatus.OrderCompleted);
            retList.Add((int)Enums.PackageStatus.Ordered);
            retList.Add((int)Enums.PackageStatus.OrderPartiallyCompleted);
            retList.Add((int)Enums.PackageStatus.ToCompletion);
            retList.Add((int)Enums.PackageStatus.InCompletion);
            return retList;
        }

        public static OpPackage Save(DataProvider dataProvider, OpOperator loggedOperator, OpPackage objectToSave)
        {
            bool isNewItem = objectToSave.IdPackage == 0; //check if it's new issue

            OpPackage originalPackage = null;
            if (objectToSave.IdPackage > 0)
            {
                originalPackage = dataProvider.GetPackage(objectToSave.IdPackage);
            }

            try
            {
                objectToSave.IdPackage = dataProvider.SavePackage(objectToSave);

                if (originalPackage != null)
                {
                    #region OperatorAcceptingPackage
                    if (originalPackage.IdPackageStatus == (int)Enums.PackageStatus.Ordered
                        && objectToSave.IdPackageStatus == (int)Enums.PackageStatus.ToCompletion)
                    {
                        //trzeba zapisac operatora akceptujacego paczke
                        OpPackageData operatorAcceptingData = objectToSave.DataList.Find(d => d.IdDataType == DataType.PACKAGE_OPERATOR_ACCEPTING_ORDER);
                        if (operatorAcceptingData == null)
                        {
                            operatorAcceptingData = new OpPackageData();
                            operatorAcceptingData.IdPackage = objectToSave.IdPackage;
                            operatorAcceptingData.IdDataType = DataType.PACKAGE_OPERATOR_ACCEPTING_ORDER;
                            operatorAcceptingData.Index = 0;
                            operatorAcceptingData.Value = LoggedOperator.IdOperator;
                            operatorAcceptingData.OpState = OpChangeState.New;
                            objectToSave.DataList.Add(operatorAcceptingData);
                        }
                        else
                        {
                            operatorAcceptingData.Value = loggedOperator.IdOperator;
                            operatorAcceptingData.OpState = OpChangeState.Modified;
                        }
                        objectToSave.OperatorAcceptingOrder = loggedOperator;
                    }
                    #endregion
                    #region OperatorCompletingPackage
                    if (originalPackage.IdPackageStatus == (int)Enums.PackageStatus.InCompletion
                        && objectToSave.IdPackageStatus == (int)Enums.PackageStatus.Sent)
                    {
                        //trzeba zapisac operatora kompletującego paczke
                        OpPackageData operatorCompletingData = objectToSave.DataList.Find(d => d.IdDataType == DataType.PACKAGE_OPERATOR_COMPLETING_ORDER);
                        if (operatorCompletingData == null)
                        {
                            operatorCompletingData = new OpPackageData();
                            operatorCompletingData.IdPackage = objectToSave.IdPackage;
                            operatorCompletingData.IdDataType = DataType.PACKAGE_OPERATOR_COMPLETING_ORDER;
                            operatorCompletingData.Index = 0;
                            operatorCompletingData.Value = LoggedOperator.IdOperator;
                            operatorCompletingData.OpState = OpChangeState.New;
                            objectToSave.DataList.Add(operatorCompletingData);
                        }
                        else
                        {
                            operatorCompletingData.Value = loggedOperator.IdOperator;
                            operatorCompletingData.OpState = OpChangeState.Modified;
                        }
                        objectToSave.OperatorCompletingOrder = loggedOperator;
                    }
                    #endregion
                }

                //zapis do tabeli PACKAGE_DATA
                OpPackageData data = null;
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    data.IdPackage = objectToSave.IdPackage;
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        DataComponent.Delete(dataProvider, data);
                    }
                    else if (isNewItem || dataProvider.GetDataType(data.IdDataType).IsEditable)
                    {
                        //zapis do bazy tylko tych ktore sa IsEditable na true lub jesli obiekt ktory dodajemy jest nowym obiektem wiec dodajemy dane po raz pierwszy
                        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                    }
                }

                bool packageStatusChanged = false;

                #region UpdatePackageStatusHistory
                OpPackageStatusHistory packageHistoryStatus = new OpPackageStatusHistory();
                packageHistoryStatus.IdPackage = objectToSave.IdPackage;
                packageHistoryStatus.StartDate = dataProvider.DateTimeNow;
                if (originalPackage == null)//nowa paczka
                {
                    packageHistoryStatus.IdPackageStatus = objectToSave.IdPackageStatus;//(int)Enums.PackageStatus.New;
                    dataProvider.SavePackageStatusHistory(packageHistoryStatus);
                    packageStatusChanged = true;
                }
                else
                {
                    OpPackageStatusHistory lastPackageHistory = dataProvider.GetPackageStatusHistoryFilter(
                        IdPackage: new int[] { objectToSave.IdPackage },
                        customWhereClause: "[END_DATE] is null").FirstOrDefault();
                    if (lastPackageHistory == null || lastPackageHistory.IdPackageStatus != objectToSave.IdPackageStatus)
                    {
                        if (lastPackageHistory != null)
                        {
                            lastPackageHistory.EndDate = dataProvider.DateTimeNow;
                            dataProvider.SavePackageStatusHistory(lastPackageHistory);
                        }
                        packageHistoryStatus.IdPackageStatus = objectToSave.IdPackageStatus;
                        dataProvider.SavePackageStatusHistory(packageHistoryStatus);
                        packageStatusChanged = true;
                    }
                }

                #endregion
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        IMRLog.AddToLog(Module, EventID.Forms.PackageAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdPackage);
                    else
                        IMRLog.AddToLog(Module, EventID.Forms.PackageSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdPackage);
                }

                if (packageStatusChanged)
                {
                    Dictionary<OpPackage, OpPackage> email = new Dictionary<OpPackage, OpPackage>();
                    email.Add(objectToSave, originalPackage);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, email, EmailComponent.OperationType.ChangeStatus);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        IMRLog.AddToLog(Module, EventID.Forms.PackageAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                    else
                        IMRLog.AddToLog(Module, EventID.Forms.PackageSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdPackage, ex.Message);
                }
                throw ex;
            }

            return dataProvider.GetPackage(objectToSave.IdPackage);
        }

        public static OpPackage Copy(DataProvider dataProvider, OpOperator loggedOperator, OpPackage objectToCopy)
        {
            OpPackage newPackage = new OpPackage(objectToCopy);
            newPackage.IdPackage = 0;
            try
            {
                newPackage = Save(dataProvider, loggedOperator, newPackage);
                if (LogCustomEvents) IMRLog.AddToLog(Module, EventID.Forms.PackageAdded, LoggedOperator.Login, ServerCORE, newPackage.IdPackage);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) IMRLog.AddToLog(Module, EventID.Forms.PackageAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                throw ex;
            }

            return dataProvider.GetPackage(newPackage.IdPackage);
        }

        public static void AcceptPackage(DataProvider dataProvider, OpOperator loggedOperator, OpPackage packageToAccept, List<OpDepositoryElement> elementsToAccept = null, bool automaticallyAccept = false)
        {
            List<OpDepositoryElement> selectedRows = elementsToAccept;
            if(selectedRows == null)
                selectedRows = dataProvider.GetDepositoryElementFilter(customWhereClause: "[ID_PACKAGE] = " + packageToAccept.IdPackage);

            #region Urzadzenia bez jakiegokoilwiek numeru seryjnego z nieznanym dystrybutorem
            /*
                        List<OpDepositoryElement> unkownDistrElement = selectedRows.Where(d => !d.Removed && !d.SerialNbr.HasValue && String.IsNullOrEmpty(d.ExternalSn)).ToList();
                        if (unkownDistrElement != null && unkownDistrElement.Count > 0)
                        {
                            var grouppedDepositoryElements = unkownDistrElement.GroupBy(x => new { x.Article },
                            (key, group) => new
                            {
                                Article = key.Article,
                                Count = group.Count()
                            });
                            if (grouppedDepositoryElements.ToList().Exists(i => i.Count > 0))
                            {
                                #region PrepareGrid
                                baseGridControlUnknownDistrDevices = new IMRGridControl() { Dock = DockStyle.Fill, AccessibleName = @"gridUnknownDistrDevices" };
                                pnlUnknowDistributorElements.Controls.Add(baseGridControlUnknownDistrDevices);

                                IMRGridColumnList columns = new IMRGridColumnList();
                                columns.Add(IMRGridColumn.Create("Article.Name", ResourcesText.Article, true, true));
                                columns.Add(IMRGridColumn.Create("Amount", ResourcesText.Amount, true, true));
                                columns.Add(IMRGridColumn.Create(new RepositoryItemComboBox() { TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor }, "Distributor", ResourcesText.Distributor, true, servicePoolLocations.Select(l => l.Distributor).Cast<object>().ToList(), null, false, DevExpress.Data.UnboundColumnType.String, null, null));
                                gvUnknownDistrDevices.LoadColumnsDefinition<OpGridRow>(columns);

                                gvUnknownDistrDevices.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(gvUnknownDistrDevices_CustomUnboundColumnData);
                                #endregion
                                List<OpGridRow> gridRowList = new List<OpGridRow>();
                                int iter = 0;
                                foreach (var vItem in grouppedDepositoryElements.ToList().Where(i => i.Count > 0))
                                {
                                    OpGridRow grItem = new OpGridRow(iter, new Dictionary<string, object>());
                                    grItem["Article"] = vItem.Article;
                                    grItem["Count"] = vItem.Count;
                                    iter++;
                                }
                                gcUnknownDistrDevices.DataSource = gridRowList;
                            }

                            lcgBaseControls.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                        }
                        */
            #endregion

            for (int i = 0; i < selectedRows.Count; i++)
            {
                OpDepositoryElement depositoryElement = selectedRows[i];
                if (depositoryElement.EndDate.HasValue)
                    continue;//obsużony już wczesniej wiersz
                
                //EndDate = DateTime.Now dla elementu w paczce
                depositoryElement.Accepted = true;
                dataProvider.SaveDepositoryElement(depositoryElement);

                if (depositoryElement.DataList != null && depositoryElement.DataList.Exists(t => t.IdDataType == DataType.DEPOSITORY_ELEMENT_PACKAGE_ITEM_ERROR_MSG))
                {
                    //jesli urzadzenie jest accepted to nie potrzebujemy juz erro msg
                    OpDepositoryElementData dedItem = depositoryElement.DataList.Find(t => t.IdDataType == DataType.DEPOSITORY_ELEMENT_PACKAGE_ITEM_ERROR_MSG);
                    if (dedItem.IdDepositoryElementData != 0)
                        dataProvider.DeleteDepositoryElementData(dedItem);
                    for (int j = 0; j < depositoryElement.DataList.Count; j++)
                    {
                        if (depositoryElement.DataList[j].IdDataType == DataType.DEPOSITORY_ELEMENT_PACKAGE_ITEM_ERROR_MSG &&
                           depositoryElement.DataList[j].IdDepositoryElement == depositoryElement.IdDepositoryElement)
                            depositoryElement.DataList.RemoveAt(j, true);
                    }
                }

                DepositoryComponent.RemoveDepositoryElement(dataProvider, depositoryElement);

                if (depositoryElement.IdReference.HasValue)
                {
                    OpDepositoryElement to_remove = DepositoryComponent.GetByID(dataProvider, depositoryElement.IdReference.Value);
                    if (to_remove != null)
                        DepositoryComponent.RemoveDepositoryElement(dataProvider, to_remove);
                }
                else
                {
                    if (depositoryElement.SerialNbr.HasValue || depositoryElement.ExternalSn != null)
                    {
                        if (depositoryElement.SerialNbr.HasValue)
                        {
                            DepositoryComponent.RemoveDeviceFromLocation(dataProvider, depositoryElement.Device, packageToAccept.OperatorCreator.DepositoryLocation);
                        }
                        else
                        {
                            DepositoryComponent.RemoveArticleFromLocation(dataProvider, depositoryElement.Article, depositoryElement.ExternalSn, depositoryElement.Removed, packageToAccept.OperatorCreator.DepositoryLocation);
                        }
                    }
                    else
                    {
                        OpDepositoryElement to_remove = new OpDepositoryElement();
                        List<OpDepositoryElement> nonuniques = DepositoryComponent.GetElements(dataProvider, packageToAccept.OperatorCreator.DepositoryLocation).Where(c => c.IdArticle == depositoryElement.IdArticle).ToList();
                        if (nonuniques.Count > 0)
                        {
                            /*
                            long diffrence = (nonuniques[0].StartDate - depositoryElement.StartDate).Ticks;

                            foreach (OpDepositoryElement dep in nonuniques)
                            {
                                if ((dep.StartDate - depositoryElement.StartDate).Ticks < diffrence)
                                    to_remove = dep;
                            }*/
                            if (nonuniques.Exists(d => !d.Removed))
                            {
                                DateTime minStartDate = nonuniques.Where(d => !d.Removed).Min(d => d.StartDate);
                                to_remove = nonuniques.Find(d => !d.Removed && d.StartDate == minStartDate);
                            }
                            else
                            {
                                DateTime minStartDate = nonuniques.Min(d => d.StartDate);
                                to_remove = nonuniques.Find(d => d.StartDate == minStartDate);
                            }
                            if (to_remove != null && to_remove.IdDepositoryElement > 0)
                                DepositoryComponent.RemoveDepositoryElement(dataProvider, to_remove);

                        }
                    }



                    #region Z_GRUPOWANIEM

                    //Z GRUPOWANIEM
                    /*DepositoryComponent.RemoveDepositoryElement(DataProvider, depositoryElement);
                    for (int j = 0; j < count; j++)
                    {
                        if (depositoryElement.SerialNbr.HasValue)
                            DepositoryComponent.RemoveDeviceFromLocation(DataProvider, depositoryElement.Device, Package.OperatorCreator.DepositoryLocation);
                        else
                        {
                            DepositoryComponent.RemoveArticleFromLocation(DataProvider, depositoryElement.Article, depositoryElement.ExternalSn, depositoryElement.Removed, Package.OperatorCreator.DepositoryLocation);
                        }
                    }*/

                    #endregion
                }

                #region Urządzenia wracajace od SOT'a ktore nie zostaly zdemontowane schodza z puli serwisowej
                /*
                            if (!depositoryElement.Removed)
                            {
                                if (depositoryElement.SerialNbr.HasValue)
                                    DepositoryComponent.RemoveFromServicePool(DataProvider, depositoryElement.Device.Distributor, depositoryElement);
                                else
                                {
                                    if (!String.IsNullOrEmpty(depositoryElement.ExternalSn))
                                    {
                                        List<OpDepositoryElement> depElemToDel = DataProvider.GetDepositoryElementFilter(IdLocation: servicePoolLocations.Select(l => l.IdLocation).ToArray(),
                                                                                    ExternalSn: depositoryElement.ExternalSn,
                                                                                    customWhereClause: "[END_DATE] is null");
                                        if (depElemToDel != null)
                                        {
                                            foreach (OpDepositoryElement dItem in depElemToDel)
                                            {
                                                DepositoryComponent.RemoveFromServicePool(DataProvider, servicePoolLocations.Find(l => l.IdLocation == dItem.IdLocation.Value).Distributor,
                                                    dItem);
                                            }
                                        }
                                    }
                                    else
                                    {
                                    }
                                }
                            }
                            */
                #endregion

                #region Zmiana statusu urządzenia i stanow magazynów serwisowych (lokalnie i na serwerach telemetrycznych)
                //LPIELA SERVICE_MAGAZINE

                //DepositoryComponent.AddToServiceMagazine(DataProvider, depositoryElement, Enums.DeviceState.SERVICED);

                #endregion

                if(depositoryElement.SerialNbr.HasValue)
                {
                    if (depositoryElement.Device == null)
                        depositoryElement.Device = dataProvider.GetDevice(depositoryElement.SerialNbr.Value);
                    depositoryElement.Device.IdDeviceStateType = (int)OpDeviceStateType.Enum.IDLE;
                    depositoryElement.Device.DeviceStateType = dataProvider.GetDeviceStateType((int)OpDeviceStateType.Enum.IDLE);
                    DeviceComponent.Save(dataProvider, depositoryElement.Device);
                }
            }
            if (automaticallyAccept)
            {
                List<OpDepositoryElement> itemsInPackage = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { packageToAccept.IdPackage });
                bool allAccepted = true;
                foreach (OpDepositoryElement depElem in itemsInPackage)
                {
                    if (depElem.EndDate == null || depElem.Accepted == null || depElem.Accepted == false)
                    {
                        allAccepted = false;
                        break;
                    }
                }
                if (allAccepted)//itemsInPackage.Count - selectedRows.Count - gvAcceptedDevices.RowCount == 0)
                {

                    if (itemsInPackage != null)
                    {
                        foreach (OpDepositoryElement dItem in itemsInPackage)
                        {
                            if (dItem.SerialNbr.HasValue)
                            {
                                
                            }
                        }
                    }

                    packageToAccept.Received = true;
                    if (!packageToAccept.ReceiveDate.HasValue)
                        packageToAccept.ReceiveDate = dataProvider.DateTimeNow;
                    packageToAccept.OperatorReceiver = loggedOperator;
                    packageToAccept.PackageStatus = dataProvider.PackageStatus.Where(t => t.IdPackageStatus == (int)Enums.PackageStatus.Accepted).FirstOrDefault();
                    PackageComponent.Save(dataProvider, LoggedOperator, packageToAccept);
                }
            }
        }

        public static void AcceptPackageFitter(DataProvider dataProvider, OpOperator loggedOperator, OpPackage packageToAccept)
        {
            if (packageToAccept != null && packageToAccept.IdPackageStatus == (int)Enums.PackageStatus.Sent && packageToAccept.FromAIUT)
            {
                List<OpDepositoryElement> depositoryElementsInPacakge = dataProvider.GetDepositoryElementFilter(loadDeviceDetails: false, IdPackage: new int[] { packageToAccept.IdPackage });
                AcceptPackageFitter(dataProvider, loggedOperator, packageToAccept, depositoryElementsInPacakge);
            }
        }


        public static void AcceptPackageFitter(DataProvider dataProvider, OpOperator loggedOperator, OpPackage packageToAccept, List<OpDepositoryElement> depositoryElementsInPacakge)
        {
            if (packageToAccept != null && packageToAccept.IdPackageStatus == (int)Enums.PackageStatus.Sent && packageToAccept.FromAIUT)
            {
                foreach (OpDepositoryElement deItem in depositoryElementsInPacakge.Where(d => d.Accepted != true))
                {
                    DepositoryComponent.AcceptPackageElementFitter(dataProvider, loggedOperator, deItem);
                    OpDepositoryElement dePackageItem = depositoryElementsInPacakge.Find(d => d.IdDepositoryElement == deItem.IdDepositoryElement);
                    dePackageItem = deItem;
                    //dePackageItem = model.DepositoryElementsInPackageCustom.Find(d => d.DepositoryElement.IdDepositoryElement == deItem.IdDepositoryElement).DepositoryElement;
                    //dePackageItem = deItem;
                }

                if (!depositoryElementsInPacakge.Exists(d => !d.Accepted.HasValue) && packageToAccept.IdPackageStatus == (int)Enums.PackageStatus.Sent)
                {
                    //Akceptujemy paczkę, jeśli wszystkie elmenty są zaakceptowane
                    packageToAccept.Received = true;
                    packageToAccept.ReceiveDate = dataProvider.DateTimeNow;
                    packageToAccept.IdPackageStatus = (int)Enums.PackageStatus.Accepted;
                    packageToAccept.PackageStatus = dataProvider.GetPackageStatus((int)Enums.PackageStatus.Accepted);
                    PackageComponent.Save(dataProvider, loggedOperator, packageToAccept);
                }
            }
        }

        public static void DeleteData(DataProvider dataProvider, int idPackage)
        {
            foreach (var item in dataProvider.GetPackageDataFilter(IdPackage: new int[1] { idPackage }, IdDataType: DataProvider.PackageDataTypes.ToArray()))
            {
                dataProvider.DeletePackageData(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, OpPackage issue, long idDataType)
        {
            foreach (var item in dataProvider.GetPackageDataFilter(IdPackage: new int[1] { issue.IdPackage }, IdDataType: new long[1] { idDataType }))
            {
                dataProvider.DeletePackageData(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, OpPackage issue, long idDataType, int index)
        {
            long idData = issue.DataList.First(d => d.IdDataType == idDataType && d.Index == index).IdData;
            if (idData > 0)
                dataProvider.DeletePackageData(dataProvider.GetPackageData(idData));
        }

        public static void DeletePackageData(DataProvider dataProvider, int idPackage)
        {
            foreach (var item in dataProvider.GetPackageDataFilter(IdPackage: new int[1] { idPackage }, IdDataType: DataProvider.PackageDataTypes.ToArray()))
            {
                dataProvider.DeletePackageData(item);
            }
        }

        public static void DeletePackageData(DataProvider dataProvider, OpPackage package, long idDataType, int index)
        {
            long idData = package.DataList.First(d => d.IdDataType == idDataType && d.Index == index).IdData;
            if (idData > 0)
                dataProvider.DeletePackageData(dataProvider.GetPackageData(idData));
        }

        public static void DeletePackageHistory(DataProvider dataProvider, int idPackage)
        { 
            List<OpPackageStatusHistory> pshList = dataProvider.GetPackageStatusHistoryFilter(
                IdPackage: new int[]{ idPackage });
            foreach(OpPackageStatusHistory pshItem in pshList)
            {
                dataProvider.DeletePackageStatusHistory(pshItem);
            }
        }

        public static void Delete(DataProvider dataProvider, OpPackage objectToDelete)
        {
            try
            {
                DeleteData(dataProvider, objectToDelete.IdPackage);
                DeletePackageHistory(dataProvider, objectToDelete.IdPackage);
                DepositoryComponent.DeletePackageElements(dataProvider, objectToDelete);
                dataProvider.DeletePackage(objectToDelete);
                if (LogCustomEvents) IMRLog.AddToLog(Module, EventID.Forms.PackageDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdPackage);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) IMRLog.AddToLog(Module, EventID.Forms.PackageDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdPackage, ex.Message);
                throw ex;
            }
        }


        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpPackage package, OpFile file, string savePath)
        {
            return DownloadFile(dataProvider, loggedOperator, package, file, savePath, false);
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpPackage package, OpFile file, string savePath, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                string filePathDownloaded = ftpClient.DownloadFile(savePath, String.Format(@"{0}/{1}/", useTestFolders ? "PackageTest" : "Package", package.IdPackage), file.PhysicalFileName);
                if (filePathDownloaded != null)
                {
                    file.LastDownloaded = dataProvider.DateTimeNow;
                    dataProvider.SaveFile(file);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpPackage package, string filePath, string fileDescription = null)
        {
            return UploadFile(dataProvider, loggedOperator, package, filePath, false, fileDescription);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpPackage package, string filePath, bool useTestFolders, string fileDescription = null)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    bool uploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/", useTestFolders ? "PackageTest" : "Package", package.IdPackage), filePath, true);
                    if (!uploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, "Upload file failed: Unknown error");
                        return null;
                    }
                    string tmpDownloadPath = ftpClient.DownloadFile(Path.GetTempPath(), String.Format(@"{0}/{1}/", useTestFolders ? "PackageTest" : "Package", package.IdPackage), Path.GetFileName(filePath));
                    if (File.Exists(tmpDownloadPath))
                    {
                        File.Delete(tmpDownloadPath);
                        OpFile file = new OpFile();
                        file.PhysicalFileName = Path.GetFileName(filePath);
                        file.Size = new FileInfo(filePath).Length;
                        file.InsertDate = dataProvider.DateTimeNow;
                        file.FileBytes = new byte[1] { 0x00 };
                        file.Description = fileDescription;
                        long idFile = dataProvider.SaveFile(file);
                        if (idFile > 0)
                        {
                            package.DataList.SetValue(DataType.PACKAGE_RECEIPT_CONFIRMATION, package.DataList.GetNextIndex(DataType.PACKAGE_RECEIPT_CONFIRMATION), file.IdFile);
                            Save(dataProvider, loggedOperator, package);
                            if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
                                LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                            return file;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }
            return null;
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpPackage package, OpFile file)
        {
            return DeleteFile(dataProvider, loggedOperator, package, file, false);
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpPackage package, OpFile file, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                ftpClient.DeleteFile(String.Format(@"{0}/{1}/", useTestFolders ? "PackageTest" : "Package", package.IdPackage), file.PhysicalFileName);

                DeleteData(dataProvider, package, DataType.PACKAGE_RECEIPT_CONFIRMATION, 0);
                package.DataList.DeleteValue(DataType.PACKAGE_RECEIPT_CONFIRMATION, 0);
                dataProvider.DeleteFile(file);
                Save(dataProvider, loggedOperator, package);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeleted, LoggedOperator.Login, ServerCORE, file.IdFile);
                    LogSuccess(EventID.Forms.AttachmentDeleted, file.IdFile);
                return true;
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeletionError, LoggedOperator.Login, ServerCORE, file.IdFile, ex.Message);
                    LogError(EventID.Forms.AttachmentDeletionError, file.IdFile, ex.Message);
                throw ex;
            }
        }

        #endregion
    }
}
