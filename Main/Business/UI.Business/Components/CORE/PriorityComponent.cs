﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class PriorityComponent
    {
        #region Methods
        public static OpPriority GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetPriority(Id);
        }

        public static List<OpPriority> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllPriority();
        }

        public static OpPriority Save(DataProvider dataProvider, OpPriority objectToSave)
        {
            dataProvider.SavePriority(objectToSave);

            return dataProvider.GetPriority(objectToSave.IdPriority);
        }

        public static void Delete(DataProvider dataProvider, OpPriority objectToDelete)
        {
            dataProvider.DeletePriority(objectToDelete);
        }
        #endregion
    }
}
