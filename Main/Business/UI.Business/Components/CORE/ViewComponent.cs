﻿using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ViewComponent
    {
        #region Consts

        private const string InvalidValueResourceKey = "InvalidValue";
        private const string DefaultActionView = "ACTION_DETAILS";
        private const string DefaultActorView = "ACTOR_DETAILS";
        private const string DefaultAlarmView = "ALARM_DETAILS";
        private const string DefaultAlarmEventView = "ALARM_EVENT_DETAILS";
        private const string DefaultDeviceView = "DEVICE_DETAILS";
        private const string DefaultDeviceConnectionView = "DEVICE_CONNECTION_DETAILS";
        private const string DefaultDistributorView = "DISTRIBUTOR_DETAILS";
        private const string DefaultIssueView = "ISSUE_DETAILS";
        private const string DefaultLocationView = "LOCATION_DETAILS";
        private const string DefaultMeterView = "METER_DETAILS";
        private const string DefaultOperatorView = "OPERATOR_DETAILS";
        private const string DefaultRouteView = "ROUTE_DETAILS";
        private const string DefaultReportView = "REPORT_DETAILS";
        private const string DefaultTaskView = "TASK_DETAILS";
        private const string DefaultSimCardView = "SIM_CARD_DETAILS";

        #endregion

        #region GetViewConfiguration

        public static List<OpView> GetViewConfiguration(IDataProvider dataProvider)
        {
            return GetViewConfiguration(dataProvider, null);
        }
        public static List<OpView> GetViewConfiguration(IDataProvider dataProvider, int[] idView)
        {
            List<OpView> vList = new List<OpView>();

            List<long> customViewDataType = new List<long>()
            {
                DataType.VIEW_ID_SOURCE_IMR_SERVER,
                DataType.VIEW_ID_HIERARCHY_PARENT_COLUMN,
                DataType.VIEW_ID_HIERARCHY_COLUMN,
                DataType.VIEW_IS_TREE_VIEW,
                DataType.VIEW_IS_SYNCHRONIZABLE,
                DataType.VIEW_ID_DATABASE_TYPE,
                DataType.VIEW_DATABASE_NAME,
                DataType.VIEW_SIMPLE_COLUMN_NAMES,
                DataType.VIEW_IMR_SERVER_WEBSERVICE_ADDRESS,
                DataType.VIEW_IMR_SERVER_WEBSERVICE_BINDING,
                DataType.VIEW_CURRENT_SESSION_GUID
            };
            List<long> customViewColumnDataType = new List<long>()
            {
                DataType.VIEW_COLUMN_COMBINED_ID_DATA_TYPE,
                DataType.VIEW_COLUMN_COMBINED_INDEX_NBR,
                DataType.VIEW_COLUMN_COMBINED_SEPARATOR,
                DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE,
                DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE_ID_DATA_TYPE,
                DataType.VIEW_COLUMN_ASSOCIATIVE_TABLE_INDEX,
                DataType.VIEW_COLUMN_ALLOWED_VALUE
            };

            vList = dataProvider.GetViewFilter(IdView: idView, loadNavigationProperties: true, loadCustomData: true, customDataTypes: customViewDataType, transactionLevel: System.Data.IsolationLevel.ReadUncommitted);

            if (vList != null && vList.Count > 0)
            {
                OpView.LoadViewColumns(ref vList, dataProvider, customViewColumnDataType);

                #region LoadImrServers

                LoadImrServers(dataProvider, vList);

                #endregion
            }

            return vList;
        }
        #endregion

        #region LoadImrServers
        public static void LoadImrServers(IDataProvider dataProvider, List<OpView> vList)
        {
            List<int> idImrServers = new List<int>();

            foreach (OpView vItem in vList)
            {
                idImrServers.AddRange(vItem.DataList.Where(d => d.IdDataType == DataType.VIEW_ID_SOURCE_IMR_SERVER && d.Value != null && d.Value is int).Select(d => Convert.ToInt32(d.Value)));
            }
            idImrServers = idImrServers.Distinct().ToList();
            Dictionary<int, OpImrServer> isDict = dataProvider.GetImrServer(idImrServers.ToArray()).ToDictionary(d => d.IdImrServer);
            foreach (OpView vItem in vList)
            {
                vItem.DataList.Where(d => d.IdDataType == DataType.VIEW_ID_SOURCE_IMR_SERVER && d.Value != null && d.Value is int)
                                .Select(d => Convert.ToInt32(d.Value))
                                .ToList()
                                .ForEach(v => { vItem.ImrServers.Add(isDict[v]); });
            }
        }
        #endregion

        #region GetActiveView
        /// <summary>
        /// Get active view for the operator, distributor, module or default
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="oper"></param>
        /// <param name="idViewDataType"></param>
        /// <param name="module"></param>
        /// <returns></returns>
        /// 
        public static Dictionary<long, OpView> GetActiveView(IDataProvider dataProvider, OpOperator oper, Enums.Module module)
        {
            List<Tuple<string, long>> activeViews = DataType.GetIds("ACTIVE_");
            activeViews = activeViews.Where(t => t.Item1.StartsWith("ACTIVE_") && t.Item1.EndsWith("_VIEW")).ToList();
            BaseComponent.Log(EventID.Forms.ActiveViewToGet, true, new object[] { String.Join(",", activeViews.Select(d => d.Item1)) });
            if (activeViews.Count > 0)
            {
                Dictionary<long, OpView> viewDict = GetActiveView(dataProvider, oper, module, activeViews.Select(t => t.Item2).Distinct().ToArray());
                if (viewDict == null)
                    viewDict = new Dictionary<long, OpView>();
                viewDict.Where(d => d.Value == null).Select(d => d.Key).ToList().ForEach(d => viewDict.Remove(d));
                return viewDict;
            }
            return new Dictionary<long, OpView>();
        }
        public static Dictionary<long, OpView> GetActiveView(IDataProvider dataProvider, OpOperator oper, Enums.Module module, long[] idViewDataType)
        {
            Dictionary<long, OpView> activeViews = new Dictionary<long, OpView>();

            foreach (long lItem in idViewDataType.Distinct())
            {
                activeViews[lItem] = GetActiveView(dataProvider, oper, module, lItem);
            }

            return activeViews;
        }
        public static OpView GetActiveView(IDataProvider dataProvider, OpOperator oper, Enums.Module module, long idViewDataType)
        {
            try
            {
                OpView view = null;

                #region OperatorView
                List<OpOperatorData> operatorViewList = oper.DataList.Where(w => w.IdDataType == idViewDataType).ToList();

                OpOperatorData operatorView = null;
                if (operatorViewList != null && operatorViewList.Count > 1)
                    throw new Exception(ResourcesText.ThereIsMoreThanOneActiveView);
                else if (operatorViewList != null && operatorViewList.Count > 0)
                {
                    operatorView = operatorViewList.First();

                    if (operatorView != null && operatorView.Value != null && !string.IsNullOrEmpty(operatorView.Value.ToString()))
                    {
                        view = GetViewConfiguration(dataProvider, new int[] { Convert.ToInt32(operatorView.Value) }).FirstOrDefault();
                        if (view != null)
                            return view;
                    }
                }
                #endregion
                #region DistributorView
                List<OpDistributorData> distributorViewList = dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { oper.IdDistributor },
                                                                                     IdDataType: new long[] { idViewDataType },
                                                                                     loadNavigationProperties: false,
                                                                                     mergeIntoCache: false,
                                                                                     transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                     autoTransaction: false);

                OpDistributorData distributorView = null;
                if (distributorViewList != null && distributorViewList.Count > 1)
                    throw new Exception(ResourcesText.ThereIsMoreThanOneActiveView);
                else if (distributorViewList != null && distributorViewList.Count > 0)
                {
                    distributorView = distributorViewList.First();
                    if (distributorView != null && distributorView.Value != null && !string.IsNullOrEmpty(distributorView.Value.ToString()))
                    {
                        view = GetViewConfiguration(dataProvider, new int[] { Convert.ToInt32(distributorView.Value) }).FirstOrDefault();
                        if (view != null)
                            return view;
                    }
                }
                #endregion
                #region ModuleView

                OpModule currentModule = dataProvider.GetModule((int)module);
                OpModuleData moduleView = currentModule.DataList.FirstOrDefault(f => f.IdDataType == idViewDataType);
                if (moduleView == null || moduleView.Value == null)
                {
                    List<OpModuleData> moduleViewList = dataProvider.GetModuleDataFilter(IdModule: new int[] { (int)module },
                                                                                         IdDataType: new long[] { idViewDataType },
                                                                                         loadNavigationProperties: false,
                                                                                         mergeIntoCache: false,
                                                                                         transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                         autoTransaction: false);
                    if (moduleViewList != null && moduleViewList.Count > 1)
                        throw new Exception(ResourcesText.ThereIsMoreThanOneActiveView);
                    else if (moduleViewList != null && moduleViewList.Count > 0)
                        moduleView = moduleViewList.First();
                }

                if (moduleView != null && moduleView.Value != null && !string.IsNullOrEmpty(moduleView.Value.ToString()))
                {
                    view = GetViewConfiguration(dataProvider, new int[] { Convert.ToInt32(moduleView.Value) }).FirstOrDefault();
                    if (view != null)
                        return view;
                }

                #endregion
                #region DefaultView
                OpView defaultView = GetDefaultViewDefinition(dataProvider, idViewDataType);

                if (defaultView == null || defaultView.OpState == OpChangeState.Incorrect)
                    return null;
                return defaultView;
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
        #region GetDefaultViewDefinition
        public static OpView GetDefaultViewDefinition(IDataProvider dataProvider, long idViewDataType)
        {
            OpView defaultView = new OpView();
            defaultView.DatabaseType = Enums.DatabaseType.CORE;
            switch (idViewDataType)
            {
                case DataType.ACTIVE_ACTION_VIEW:
                    defaultView.Name = DefaultActionView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdAction;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_ACTION,
                        IdViewColumn = 1,
                        Name =  DataType.HELPER_ID_ACTION.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_ACTION, 0)
                    });
                    break;
                case DataType.ACTIVE_ACTOR_VIEW:
                    defaultView.Name = DefaultActorView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdActor;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_ACTOR,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_ACTOR.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        // Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_ACTOR, 0)
                    });
                    break;
                case DataType.ACTIVE_ALARM_VIEW:
                    defaultView.Name = DefaultAlarmView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdAlarm;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_ALARM,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_ALARM.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_ALARM, 0)
                    });
                    break;
                case DataType.ACTIVE_ALARM_EVENT_VIEW:
                    defaultView.Name = DefaultAlarmEventView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdAlarmEvent;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_ALARM_EVENT,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_ALARM_EVENT.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_ALARM, 0)
                    });
                    break;
                case DataType.ACTIVE_DEVICE_VIEW:
                    defaultView.Name = DefaultDeviceView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.SerialNbr;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_SERIAL_NBR,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_SERIAL_NBR.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_SERIAL_NBR, 0)
                    });
                    break;
                case DataType.ACTIVE_DEVICE_CONNECTION_VIEW:
                    defaultView.Name = DefaultDeviceConnectionView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdDeviceConnection;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_DEVICE_CONNECTION,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_DEVICE_CONNECTION.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_SERIAL_NBR, 0)
                    });
                    break;
                case DataType.ACTIVE_DISTRIBUTOR_VIEW:
                    defaultView.Name = DefaultDistributorView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdDistributor;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_DISTRIBUTOR,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_DISTRIBUTOR.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_DISTRIBUTOR, 0)
                    });
                    break;
                case DataType.ACTIVE_ISSUE_VIEW:
                    defaultView.Name = DefaultIssueView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdIssue;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_ISSUE,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_ISSUE.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_ISSUE, 0)
                    });
                    break;
                case DataType.ACTIVE_LOCATION_VIEW:
                    defaultView.Name = DefaultLocationView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdLocation;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_LOCATION,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_LOCATION.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_LOCATION, 0)
                    });
                    break;
                case DataType.ACTIVE_METER_VIEW:
                    defaultView.Name = DefaultMeterView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdMeter;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_METER,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_METER.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_METER, 0)
                    });
                    break;
                case DataType.ACTIVE_OPERATOR_VIEW:
                    defaultView.Name = DefaultOperatorView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdOperator;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_OPERATOR,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_OPERATOR.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_OPERATOR, 0)
                    });
                    break;
                case DataType.ACTIVE_REPORT_VIEW:
                    defaultView.Name = DefaultReportView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdReport;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_REPORT,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_REPORT.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_REPORT, 0)
                    });
                    break;
                case DataType.ACTIVE_ROUTE_VIEW:
                    defaultView.Name = DefaultRouteView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdRoute;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_ROUTE,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_ROUTE.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_ROUTE, 0)
                    });
                    break;
                case DataType.ACTIVE_TASK_VIEW:
                    defaultView.Name = DefaultTaskView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdTask;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_TASK,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_TASK.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_TASK, 0)
                    });
                    break;
                case DataType.ACTIVE_SIM_CARD_VIEW:
                    defaultView.Name = DefaultSimCardView;
                    defaultView.IdReferenceType = (int)Enums.ReferenceType.IdSimCard;
                    defaultView.Columns.Add(new OpViewColumn()
                    {
                        PrimaryKey = true,
                        IdDataType = DataType.HELPER_ID_SIM_CARD,
                        IdViewColumn = 1,
                        Name = DataType.HELPER_ID_SIM_CARD.ToString(),
                        IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                        //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_SIM_CARD, 0)
                    });
                    break;
                default:
                    throw new Exception(String.Format("Unsupported default view {0}", DataType.GetName(idViewDataType)));
            }
            defaultView.Columns.Add(new OpViewColumn()
            {
                PrimaryKey = true,
                IdDataType = DataType.HELPER_ID_IMR_SERVER,
                IdViewColumn = 2,
                Name = DataType.HELPER_ID_IMR_SERVER.ToString(),
                IdViewColumnType = (int)Enums.ViewColumnType.BaseHelper
                //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), DataType.HELPER_ID_IMR_SERVER, 0)
            });

            DataTable dtItem = null;
            try
            {
                dtItem = Cache.TableSchema.Get(defaultView.Name, dataProvider.dbConnectionCore);
                if (dtItem == null || dtItem.Columns == null)
                    throw new Exception(String.Format("Missing {0} view in DB", defaultView.Name));
                if(!dtItem.Columns.Contains("ID_VIEW_KEY"))
                    throw new Exception(String.Format("Wrong {0} view in DB, missing ID_VIEW_KEY ", defaultView.Name));
            }
            catch (Exception ex)
            {
                BaseComponent.Log(EventID.Forms.ViewNotExistsInDB, true, new object[] { DataType.GetName(idViewDataType) });
                defaultView.OpState = OpChangeState.Incorrect;
                defaultView.Columns.Clear();
                return defaultView;
            }
            if (dtItem != null)
            {
                OpViewColumn primaryKeyColumn = defaultView.PrimaryKeyColumn;
                int idViewColumn = 3;
                foreach (DataColumn dcItem in dtItem.Columns)
                {
                    long idDataType = 0;
                    if (long.TryParse(dcItem.ColumnName, out idDataType) && idDataType > 0)
                    {
                        if (!defaultView.Columns.Exists(c => c.IdDataType == idDataType))
                        {
                            defaultView.Columns.Add(new OpViewColumn()
                            {
                                PrimaryKey = false,
                                IdDataType = idDataType,
                                Name = idDataType.ToString(),
                                IdViewColumnType = DataType.GetHelperDataTypes((Enums.ReferenceType)defaultView.IdReferenceType, true).Exists(d => d == idDataType) ? (int)Enums.ViewColumnType.BaseHelper : (int)Enums.ViewColumnType.Standard,
                                IdViewColumn = idViewColumn,
                                IdKeyViewColumn = primaryKeyColumn.IdViewColumn
                                //Name = String.Format("{0}_{1}_{2}", String.Join("", defaultView.Name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries).Select(d => d.ToUpper().First())), idDataType, 0)
                            });
                            idViewColumn++;
                        }
                    }
                }
            }
            return defaultView;
        }
        #endregion
        #region GetActiveViewDataTypeByReferenceType

        public static long GetActiveViewDataTypeByReferenceType(Enums.ReferenceType referenceType)
        {
            if (referenceType == Enums.ReferenceType.IdActor)
                return DataType.ACTIVE_ACTOR_VIEW;
            else if (referenceType == Enums.ReferenceType.IdAction)
                return DataType.ACTIVE_ACTION_VIEW;
            else if (referenceType == Enums.ReferenceType.IdAlarm)
                return DataType.ACTIVE_ALARM_VIEW;
            else if (referenceType == Enums.ReferenceType.IdAlarmEvent)
                return DataType.ACTIVE_ALARM_EVENT_VIEW;
            else if (referenceType == Enums.ReferenceType.IdDistributor)
                return DataType.ACTIVE_DISTRIBUTOR_VIEW;
            else if (referenceType == Enums.ReferenceType.IdDeviceConnection)
                return DataType.ACTIVE_DEVICE_CONNECTION_VIEW;
            else if (referenceType == Enums.ReferenceType.IdIssue)
                return DataType.ACTIVE_ISSUE_VIEW;
            else if (referenceType == Enums.ReferenceType.IdLocation)
                return DataType.ACTIVE_LOCATION_VIEW;
            else if (referenceType == Enums.ReferenceType.SerialNbr)
                return DataType.ACTIVE_DEVICE_VIEW;
            else if (referenceType == Enums.ReferenceType.IdMeter)
                return DataType.ACTIVE_METER_VIEW;
            else if (referenceType == Enums.ReferenceType.IdOperator)
                return DataType.ACTIVE_OPERATOR_VIEW;
            else if (referenceType == Enums.ReferenceType.IdTask)
                return DataType.ACTIVE_TASK_VIEW;
            else if (referenceType == Enums.ReferenceType.IdRoute)
                return DataType.ACTIVE_ROUTE_VIEW;
            else if (referenceType == Enums.ReferenceType.IdReport)
                return DataType.ACTIVE_REPORT_VIEW;
            else if (referenceType == Enums.ReferenceType.IdSimCard)
                return DataType.ACTIVE_SIM_CARD_VIEW;
            else
                throw new Exception(ResourcesText.UnsupportedReferenceType);
        }

        #endregion
        #region GetViewColumn

        public static OpViewColumn GetViewColumn(OpView view, long idDataType,
            string columnName = null, int indexNbr = 0,
            Enums.ViewColumnType viewColumnType = Enums.ViewColumnType.Unknown,
            OpViewColumn keyColumn = null)
        {
            OpViewColumn column = null;
            if (view != null)
            {
                column = view.GetViewColumn(idDataType, columnName: columnName, indexNbr: indexNbr, viewColumnType: viewColumnType, keyColumn: keyColumn);
            }
            return column;


        }

        public static List<OpViewColumn> GetViewColumns(OpView view, long idDataType,
            string columnName = null, int indexNbr = 0,
            Enums.ViewColumnType viewColumnType = Enums.ViewColumnType.Unknown,
            OpViewColumn keyColumn = null)
        {
            List<OpViewColumn> column = null;
            if (view != null)
            {
                column = view.GetViewColumns(idDataType, columnName: columnName, indexNbr: indexNbr, viewColumnType: viewColumnType, keyColumn: keyColumn);
            }
            return column;


        }

        #endregion

        #region CheckIfDefaultViewDefinition
        public static bool CheckIfDefaultViewDefinition(OpView view)
        {
            return view == null || view.IdView == 0;
        }
        #endregion

    }
}
