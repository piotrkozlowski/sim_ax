﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.IO;
using System.Net;
using IMR.Suite.UI.Business.Objects;
using System.Globalization;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ServiceListComponent : BaseComponent
    {
        #region Delete

        public static void Delete(DataProvider dataProvider, OpServiceList objectToDelete)
        {
            if (objectToDelete != null && objectToDelete.IdServiceList > 0)
            {
                List<OpServiceListData> dataToDelete = dataProvider.GetServiceListDataFilter(IdServiceList: new int[] { objectToDelete.IdServiceList });
                if (dataToDelete != null)
                {
                    foreach (OpServiceListData sldItem in dataToDelete)
                    {
                        dataProvider.DeleteServiceListData(sldItem);
                    }
                }

                dataProvider.DeleteServiceList(objectToDelete);
            }
        }

        public static void DeleteServiceListDevice(DataProvider dataProvider, OpServiceListDevice objectToDelete)
        {
            if (objectToDelete != null && objectToDelete.IdServiceList > 0)
            {
                foreach (OpServiceListDeviceData sldd in objectToDelete.DataList)
                {
                    if (sldd.IdServiceListDeviceData != 0)
                        dataProvider.DeleteServiceListDeviceData(sldd);
                }
                dataProvider.DeleteServiceListDevice(objectToDelete.IdServiceList, objectToDelete.SerialNbr);
            }
        }

        #endregion

        #region Save

        public static OpServiceList Save(DataProvider dataProvider, OpServiceList objectToSave)
        {
            objectToSave.IdServiceList = dataProvider.SaveServiceList(objectToSave);

            if (objectToSave.DataList != null)
            {
                objectToSave.DataList.ForEach(d => d.IdServiceList = objectToSave.IdServiceList);
                OpServiceListData data;
                List<int> indexesToRemove = new List<int>();
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        if(objectToSave.DataList[i].IdServiceListData > 0)
                            dataProvider.DeleteServiceListData(objectToSave.DataList[i]);
                        indexesToRemove.Add(i);
                    }
                    else if ((objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                    {
                        objectToSave.DataList[i].IdData = dataProvider.SaveServiceListData(objectToSave.DataList[i]);
                        objectToSave.DataList[i].IdServiceListData = objectToSave.DataList[i].IdData;
                        if (objectToSave.DataList[i].OpState == OpChangeState.New)
                            objectToSave.DataList[i].AssignReferences(dataProvider);
                        objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                    }
                }
                if (indexesToRemove.Count > 0)
                {
                    foreach (int iItem in indexesToRemove)
                        objectToSave.DataList.RemoveAt(iItem, true);
                }
            }

            return objectToSave;
        }

        public static void SaveAttachment(OpServiceListDevice contract, DataProvider dataProvider, long dataType)
        {
            List<OpServiceListDeviceData> newAttachmentsList = contract.DataList.FindAll(t => t.IdDataType == dataType && t.OpState == Objects.OpChangeState.New);
            foreach (OpServiceListDeviceData sldItem in newAttachmentsList)
            {
                sldItem.IdServiceListDeviceData = dataProvider.SaveServiceListDeviceData(sldItem);
                sldItem.OpState = Objects.OpChangeState.Loaded;
            }
        }

        #endregion

        #region Files
        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpServiceList serviceList, OpServiceListDevice serviceListDevice, OpFile file, string savePath)
        {
            return DownloadFile(dataProvider, loggedOperator, serviceList, serviceListDevice, file, savePath, false);
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpServiceList serviceList, OpServiceListDevice serviceListDevice, OpFile file, string savePath, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                string filePathDownloaded = ftpClient.DownloadFile(savePath, String.Format(@"{0}/{1}/{2}/", useTestFolders ? "ServiceTest" : "Service", serviceList.IdServiceList, serviceListDevice.SerialNbr ), file.PhysicalFileName);
                if (filePathDownloaded != null)
                {
                    file.LastDownloaded = dataProvider.DateTimeNow;
                    dataProvider.SaveFile(file);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpServiceList serviceList, OpServiceListDevice serviceListDevice, string filePath, bool isAditionalAttachment, bool? isPhotoServiced)
        {
            return UploadFile(dataProvider, loggedOperator, serviceList, serviceListDevice, filePath, isAditionalAttachment, isPhotoServiced, false);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpServiceList serviceList, OpServiceListDevice serviceListDevice, string filePath, bool isAditionalAttachment, bool? isPhotoServiced, bool useTestFolders)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    try
                    {
                        ftpClient.MakeDirectory(String.Format(@"{0}/{1}/", useTestFolders ? "ServiceTest" : "Service", serviceList.IdServiceList));
                    }
                    catch (WebException ex)
                    {
                    }
                    bool uploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/{2}/", useTestFolders ? "ServiceTest" : "Service", serviceList.IdServiceList, serviceListDevice.SerialNbr), filePath, true);

                    if (!uploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, "Upload file failed: Unknown error");
                        return null;
                    }
                    string tmpDownloadPath = ftpClient.DownloadFile(Path.GetTempPath(), String.Format(@"{0}/{1}/{2}/", useTestFolders ? "ServiceTest" : "Service", serviceList.IdServiceList, serviceListDevice.SerialNbr), Path.GetFileName(filePath));
                    if (File.Exists(tmpDownloadPath))
                    {
                        OpFile file = new OpFile();
                        file.PhysicalFileName = Path.GetFileName(filePath);
                        file.Size = new FileInfo(filePath).Length;
                        file.InsertDate = dataProvider.DateTimeNow;
                        file.FileBytes = new byte[1] { 0x00 };
                        long idFile = dataProvider.SaveFile(file);
                        File.Delete(tmpDownloadPath);
                        if (idFile > 0)
                        {
                            if (!isAditionalAttachment)
                            {
                                if (isPhotoServiced == true)
                                    serviceListDevice.PhotoServiced = idFile;
                                else
                                    serviceListDevice.Photo = idFile;
                                dataProvider.SaveServiceListDevice(serviceList.IdServiceList, serviceListDevice);
                            }
                            else
                            {
                                OpServiceListDeviceData toAdd = new OpServiceListDeviceData();
                                toAdd.IdServiceList = serviceList.IdServiceList;
                                toAdd.SerialNbr = serviceListDevice.SerialNbr;
                                if (serviceListDevice.DataList != null)
                                    toAdd.Index = serviceListDevice.DataList.GetNextIndex(DataType.SERVICE_LIST_DEVICE_ATTACHMENT);
                                else
                                {
                                    serviceListDevice.DataList = new Objects.OpDataList<OpServiceListDeviceData>();
                                    toAdd.Index = 0;
                                }
                                toAdd.IdDataType = DataType.SERVICE_LIST_DEVICE_ATTACHMENT;
                                toAdd.Value = idFile;
                                toAdd.OpState = Objects.OpChangeState.New;
                                serviceListDevice.DataList.Add(toAdd);

                                SaveAttachment(serviceListDevice, dataProvider, DataType.SERVICE_LIST_DEVICE_ATTACHMENT);
                            }
                            if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
                                LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                            return file;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }
            return null;
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpServiceList serviceList, OpServiceListDevice serviceListDevice, bool isAditionalAttachment, bool? isPhotoServiced)
        {
            return DeleteFile(dataProvider, loggedOperator, serviceList, serviceListDevice, isAditionalAttachment, isPhotoServiced, false);
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpServiceList serviceList, OpServiceListDevice serviceListDevice, bool isAditionalAttachment, bool? isPhotoServiced, bool useTestFolders)
        {
            string idFile = "";
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);

                if (isAditionalAttachment)
                {
                    List<OpServiceListDeviceData> toDeleteList = serviceListDevice.DataList.FindAll(d => d.OpState == Objects.OpChangeState.Delete);

                    foreach (OpServiceListDeviceData slddItem in toDeleteList)
                    {
                        OpFile fileToDelete = dataProvider.GetFile(Convert.ToInt64(slddItem.Value), true);
                        ftpClient.DeleteFile(String.Format(@"{0}/{1}/{2}/", useTestFolders ? "ServiceTest" : "Service", serviceList.IdServiceList, serviceListDevice.SerialNbr), fileToDelete.PhysicalFileName);

                        idFile += fileToDelete.IdFile + ",";

                        dataProvider.DeleteFile(fileToDelete);
                        dataProvider.DeleteServiceListDeviceData(slddItem);
                        serviceListDevice.DataList.Remove(slddItem, true);
                    }
                }
                else
                {
                    OpFile fileToDelete = null;
                    if (isPhotoServiced == true)
                    {
                        fileToDelete = dataProvider.GetFile(serviceListDevice.PhotoServiced.Value);
                        serviceListDevice.PhotoServiced = null;
                    }
                    else
                    {
                        fileToDelete = dataProvider.GetFile(serviceListDevice.Photo.Value);
                        serviceListDevice.Photo = null;
                    }
                    
                    ftpClient.DeleteFile(String.Format(@"{0}/{1}/{2}/", useTestFolders ? "ServiceTest" : "Service", serviceList.IdServiceList, serviceListDevice.SerialNbr), fileToDelete.PhysicalFileName);

                    idFile += fileToDelete.IdFile + ",";

                    dataProvider.DeleteFile(fileToDelete);
                    dataProvider.SaveServiceListDevice(serviceList.IdServiceList, serviceListDevice);
                }
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeleted, LoggedOperator.Login, ServerCORE, file.IdFile);
                    LogSuccess(EventID.Forms.AttachmentDeleted, idFile.Substring(0, idFile.Length - 1));
                return true;
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeletionError, LoggedOperator.Login, ServerCORE, file.IdFile, ex.Message);
                    LogError(EventID.Forms.AttachmentDeletionError, idFile, ex.Message);
                throw ex;
            }
        }

        #endregion

        public static bool ValidateServiceReferenceTypeValue(DataProvider dataProvider, DBCommonRDT radioDeviceTesterNewConnection, Enums.ServiceReferenceType ReferenceType, object Value, out int ErrorCode)
        {
            ErrorCode = 0;
            /*
             * 0 - NoErrors
             * 1 - NullValue
             * 2 - WrongType
             * 3 - WrongValue
             * 4 - Non-existend SIM serial number
            */
            if (Value == null)
                ErrorCode = 1;
            else
            {
                switch (ReferenceType)
                {
                    case Enums.ServiceReferenceType.BatteryVolume://mAh
                        #region BatteryRemainingVolume
                        double result = 0.0;
                        if (!double.TryParse(Value.ToString(), out result))
                            ErrorCode = 2;
                        else
                        {
                            if (result < 0.0)
                                ErrorCode = 3;
                        }
                        #endregion
                        break;
                    case Enums.ServiceReferenceType.SimCardPhone:
                        #region SimCardPhone
                        if (Value != null && !String.IsNullOrEmpty(Value.ToString()))//dopuszczamy brak kartySim
                        {
                            string phoneNbr = Value.ToString().Trim();
                            if (phoneNbr[0] == '+')
                                phoneNbr = phoneNbr.Substring(1);
                            long resultPhone = 0;
                            if (!long.TryParse(phoneNbr, out resultPhone))
                                ErrorCode = 2;
                        }
                        #endregion
                        break;
                    case Enums.ServiceReferenceType.SimCardSerialNbr:
                        #region SimCardSerialNbr
                        if (Value != null && !String.IsNullOrEmpty(Value.ToString()))//dopuszczamy brak kartySim
                        {
                            if (radioDeviceTesterNewConnection != null)
                            {
                                if (!radioDeviceTesterNewConnection.CheckIfSimCardExists(Value.ToString().Trim()))
                                    ErrorCode = 4;
                            }
                        }
                        #endregion
                        break;
                    case Enums.ServiceReferenceType.IdImrServer:
                        #region SimCardPhone
                        if (Value != null && !String.IsNullOrEmpty(Value.ToString()))//dopuszczamy brak kartySim
                        {
                            int resultIdImrServer = 0;
                            if (!int.TryParse(Value.ToString(), out resultIdImrServer))
                                ErrorCode = 2;
                            else
                            {
                                OpImrServer isItem = dataProvider.GetImrServer(resultIdImrServer);
                                if (isItem == null)
                                    ErrorCode = 3;
                            }
                        }
                        else
                            ErrorCode = 1;
                        #endregion
                        break;
                    default:
                        break;
                }
            }
            return ErrorCode == 0;
        }

    }
}
