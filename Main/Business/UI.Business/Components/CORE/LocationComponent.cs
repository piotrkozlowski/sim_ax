﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using IMR.Suite.Services.Common.UTDServiceReference;
using IMR.Suite.Services.Common.Components;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LocationComponent : BaseComponent
    {
        #region Methods
        #region Get

        #region GetByID
        public static OpLocation GetByID(DataProvider dataProvider, long Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpLocation GetByID(DataProvider dataProvider, long Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetLocation(Id, queryDatabase);
        }

        #endregion
        #region GetAll
        public static List<OpLocation> GetAll(DataProvider dataProvider, bool? exludeDeletedAndUnused = null)
        {
            // Metoda została przeniesiona do DataProvider, ponieważ z poziomu UI.Business.Object nie mielibyśmy do niej dostępu.

            return dataProvider.GetAll(exludeDeletedAndUnused);

            //List<OpLocation> locationList = null;
            //if (exludeDeletedAndUnused.HasValue && exludeDeletedAndUnused.Value == true)
            //{
            //    locationList = dataProvider.GetAllLocation().Where(o => o.IdLocationStateType != (int)Enums.LocationState.DELETED && o.IdLocationStateType != (int)Enums.LocationState.UNUSED).ToList();
            //}
            //else
            //    locationList = dataProvider.GetAllLocation();

            //return locationList;
        }

        #endregion
        #region GetPatterns
        public static List<OpLocation> GetPatterns(DataProvider dataProvider, int? idDistributor = null)
        {
            if (idDistributor == null)
                return GetAll(dataProvider, exludeDeletedAndUnused: true).Where(d => d.IdLocationPattern.HasValue && d.IdLocationPattern == d.IdLocation).ToList();
            return GetAll(dataProvider, exludeDeletedAndUnused: true).Where(d => d.IdLocationPattern.HasValue && d.IdLocationPattern == d.IdLocation && d.IdDistributor == idDistributor).ToList();
        }
        #endregion
        #region GetConsumersLocations
        public static List<OpLocation> GetConsumersLocations(DataProvider dataProvider)
        {
            return GetAll(dataProvider, exludeDeletedAndUnused: true).FindAll(l => !l.AllowGrouping);
        }

        #endregion
        #region GetDepositoryLocations
        public static List<OpLocation> GetDepositoryLocations(DataProvider dataProvider)
        {
            return GetAll(dataProvider, exludeDeletedAndUnused: true).FindAll(l => l.IdLocationType == (int)Enums.LocationType.DepositoryLocation);
        }

        #endregion
        #region GetStateHistory
        public static List<OpLocationStateHistory> GetStateHistory(DataProvider dataProvider, long idLocation)
        {
            return dataProvider.GetLocationStateHistoryFilter(IdLocation: new long[] { idLocation });
            //old AF solution:
            //return dataProvider.GetLocationStateHistoryForLocation(idLocation);
            //old BB solution:
            //return dataProvider.LocationStateHistory.Where(lsh => lsh.IdLocation == idLocation).ToList();
        }

        #endregion
        #region GetRecentlyAdded
        public static List<OpLocation> GetRecentlyAdded(DataProvider dataProvider, int topCount)
        {
            //return dataProvider.GetLocationsRecentlyAdded(topCount).ToList();
            return null;
        }

        #endregion
        #region GetIdLocationsByIdProfile
        /// <summary>
        /// Gets locations ids which have provided profile id.
        /// </summary>
        /// <param name="dataProvider">DataProvider reference.</param>
        /// <param name="idProfile">Id profile to search locations which have it.</param>
        /// <returns>List of locations ids.</returns>
        public static List<long> GetIdLocationsByIdProfile(DataProvider dataProvider, int idProfile)
        {
            List<long> idLocations = new List<long>();
            if (dataProvider == null) return idLocations;

            List<OpData> locationsData = dataProvider.GetDataFilter(autoTransaction: false, loadNavigationProperties: false, IdDataType: new[] { DataType.PROFILE_ID });
            if (locationsData.Any())
            {
                var selectedData = locationsData.Where(w => w.IdLocation.HasValue && w.Value != null && Convert.ToInt32(w.Value) == idProfile).Select(s => s.IdLocation.Value);
                idLocations.AddRange(selectedData);
            }

            return idLocations;
        }

        #endregion
        #region GetLocationIdsByMeterIds
        /// <summary>
        /// Gets locations id numbers for provided meter id numbers.
        /// </summary>
        /// <param name="dataProvider">Data provider instance.</param>
        /// <param name="meterIds">Meters id numbers.</param>
        /// <returns>List of location id numbers.</returns>
        public static List<long> GetLocationIdsByMeterIds(DataProvider dataProvider, long[] meterIds)
        {
            var locationEq = dataProvider.GetLocationEquipmentFilter(loadNavigationProperties: false, loadCustomData: false, IdMeter: meterIds, EndTime: TypeDateTimeCode.Null());
            return locationEq.Where(l => l.IdLocation.HasValue).ToList().Select(l => l.IdLocation.Value).Distinct().ToList();
        }
        #endregion
        #region FindLocation
        public static OpLocation FindLocation(DataProvider dataProvider, long id_location, int id_imr_server)
        {
            return GetAll(dataProvider, exludeDeletedAndUnused: false).Find(l => l.IdImrServer == id_imr_server && l.IdLocationOriginal == id_location);
        }
        #endregion
        #region GetIssueHistory
        public static List<OpIssue> GetIssueHistory(DataProvider dataProvider, long idLocation)
        {
            return dataProvider.GetIssueFilter(IdLocation: new long[] { idLocation });
            //old JK solution
            //return dataProvider.GetIssueHistoryForLocation(idLocation);
        }
        #endregion
        #region GetTaskHistory
        public static List<OpTask> GetTaskHistory(DataProvider dataProvider, long idLocation)
        {
            return dataProvider.GetTaskFilter(IdLocation: new long[] { idLocation });
            //old JK solution
            //return dataProvider.GetTaskHistoryForLocation(idLocation);
        }
        #endregion
        #region GetLocationHistory
        public static List<OpHistory> GetLocationHistory(DataProvider dataProvider, OpOperator loggedOperator, long idLocation)
        {
            List<OpHistory> hList = new List<OpHistory>();

            List<OpLocationStateHistory> lshList = dataProvider.GetLocationStateHistoryFilter(transactionLevel: IsolationLevel.ReadUncommitted,
                IdLocation: new long[] { idLocation });
            lshList.ForEach(lsh => hList.Add(new OpHistory(lsh.StartDate, Enums.ObjectHistoryEventType.ChangedStatus, lsh.LocationStateType.ToString(), null) { EventObject = lsh }));

            #region LocationEquipment
            List<OpLocationEquipment> leList = dataProvider.GetLocationEquipmentFilter(loadCustomData: false, transactionLevel: IsolationLevel.ReadUncommitted, IdLocation: new long[] { idLocation });
            if (leList.Count > 0)
            {
                foreach (OpLocationEquipment leItem in leList)
                {
                    OpHistory hInstallItem = null;
                    OpHistory hDeinstallItem = null;
                    if (leItem.SerialNbr.HasValue)
                    {
                        hInstallItem = new OpHistory(leItem.StartTime, Enums.ObjectHistoryEventType.InstalledAtLocation,
                            (leItem.Device != null ? leItem.Device.ToString() : leItem.SerialNbr.ToString()),
                            (!leItem.IdMeter.HasValue ? "" : String.Format("{0}: {1}", ResourcesText.InstalledAt, leItem.Meter != null ? leItem.Meter.ToString() : leItem.IdMeter.ToString())))
                        { EventObject = leItem };
                        if (leItem.EndTime.HasValue)
                        {

                            hInstallItem = new OpHistory(leItem.EndTime.Value, Enums.ObjectHistoryEventType.RemovedFromLocation,
                                (leItem.Device != null ? leItem.Device.ToString() : leItem.SerialNbr.ToString()),
                                (!leItem.IdMeter.HasValue ? "" : String.Format("{0}: {1}", ResourcesText.RemovedFrom, leItem.Meter != null ? leItem.Meter.ToString() : leItem.IdMeter.ToString())))
                            { EventObject = leItem };
                        }
                    }
                    else
                    {

                    }
                    if (hInstallItem != null)
                        hList.Add(hInstallItem);
                    if (hDeinstallItem != null)
                        hList.Add(hDeinstallItem);
                }
            }
            #endregion

            if (loggedOperator.HasPermission(Activity.TASK_LIST))
            {
                #region Task
                List<OpTask> tList = dataProvider.GetTaskFilter(loadCustomData: false, transactionLevel: IsolationLevel.ReadUncommitted,
                    IdLocation: new long[] { idLocation });
                if (tList.Count > 0)
                {
                    foreach (OpTask tItem in tList)
                    {
                        hList.Add(new OpHistory(tItem.CreationDate, Enums.ObjectHistoryEventType.RelatedWithTask,
                            String.Format("{0}: {1}", ResourcesText.IdTask, tItem.IdTask),
                            String.Format("{0}: {1}, {2}: {3}", ResourcesText.OperatorRegistering, tItem.OperatorRegistering, ResourcesText.OperatorPerformer, tItem.OperatorPerformer))
                        { EventObject = tItem });
                    }
                }
                #endregion
            }
            if (loggedOperator.HasPermission(Activity.ISSUE_LIST))
            {
                #region Issue
                List<OpIssue> iList = dataProvider.GetIssueFilter(transactionLevel: IsolationLevel.ReadUncommitted,
                    IdLocation: new long[] { idLocation });
                if (iList.Count > 0)
                {
                    foreach (OpIssue iItem in iList)
                    {
                        hList.Add(new OpHistory(iItem.CreationDate, Enums.ObjectHistoryEventType.RelatedWithIssue,
                            String.Format("{0}: {1}", ResourcesText.IdTask, iItem.IdIssue),
                            String.Format("{0}: {1}, {2}: {3}", ResourcesText.OperatorRegistering, iItem.OperatorRegistering, ResourcesText.OperatorPerformer, iItem.OperatorPerformer))
                        { EventObject = iItem });
                    }
                }
                #endregion
            }
            hList = hList.OrderByDescending(h => h.EventDate).ToList();
            return hList;
        }
        #endregion
        #region GetLocationData
        public static List<T> GetLocationData<T>(DataProvider dataProvider, OpLocation opLocation, long idDataType, bool tryGetFromDistributor = true, bool tryQueryDatabase = true) where T : struct, IConvertible
        {
            List<T> list = new List<T>();
            List<OpData> locationData = opLocation.DataList.Where(q => q.Value != null && q.IdDataType == idDataType).ToList();
            if (locationData.Count == 0 && tryQueryDatabase)
            {
                locationData = dataProvider.GetDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdLocation: new long[] { opLocation.IdLocation }, SerialNbr: new long[0], IdMeter: new long[0],
                    IdDataType: new long[] { idDataType },
                    customWhereClause: "[VALUE] IS NOT NULL ");
            }

            if (locationData.Count > 0)
            {
                foreach (OpData data in locationData)
                {
                    T? value = data.TryGetNullableValue<T>();
                    if (value.HasValue)
                        list.Add(value.Value);
                }
                return list;
            }

            if (tryGetFromDistributor && opLocation.Distributor != null)
            {
                List<OpDistributorData> distributorData = opLocation.Distributor.DataList.Where(q => q.Value != null && q.IdDataType == idDataType).ToList();
                if (distributorData.Count == 0 && tryQueryDatabase)
                {
                    distributorData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDistributor: new int[] { opLocation.Distributor.IdDistributor },
                        IdDataType: new long[] { idDataType }, customWhereClause: "[VALUE] IS NOT NULL ");
                }

                foreach (OpDistributorData data in distributorData)
                {
                    T? value = data.TryGetNullableValue<T>();
                    if (value.HasValue)
                        list.Add(value.Value);
                }
            }

            return list;
        }
        #endregion
        #region GetLocationDistributorNotificationOperators

        public static List<OpOperator> GetLocationDistributorNotificationOperators(DataProvider dataProvider, OpLocation location, List<EmailComponent.OperationType> operationTypesToCheck)
        {
            List<OpOperator> mailingList = new List<OpOperator>();
            string customWhereClause = "[VALUE] is not null";
            customWhereClause += " AND (";
            customWhereClause += "(cast([VALUE] as nvarchar) not like '%,%' AND cast([VALUE] as nvarchar) like '" + location.IdDistributor + "')";
            customWhereClause += " OR";
            customWhereClause += "(CAST(VALUE as nvarchar) like '%,%' and (CAST(VALUE as nvarchar) like '%," + location.IdDistributor + ",%' OR CAST(VALUE as nvarchar) like '%," + location.IdDistributor + "' OR (CAST(VALUE as nvarchar) like '" + location.IdDistributor + ",%')))";
            customWhereClause += ")";
            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, IdDataType: new long[] { DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION_ID_DISTRIBUTOR }, customWhereClause: customWhereClause);
            if (odList != null && odList.Count > 0)
            {
                List<OpOperator> oList = dataProvider.GetOperator(odList.Select(d => d.IdOperator).Distinct().ToArray());
                foreach (OpOperator oItem in oList)
                {
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Add)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.LocationOptions.Add))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Change)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.LocationOptions.Change))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Delete)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.LocationOptions.Delete))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.ChangeStatus)
                        && oItem.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((Enums.LocationState)location.IdLocationStateType)))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                }
            }
            mailingList.RemoveAll(o => o.Actor == null);
            mailingList.RemoveAll(o => String.IsNullOrEmpty(o.Actor.Email));
            mailingList = mailingList.Distinct().ToList();
            return mailingList;
        }

        #endregion
        #region GetLocationDistributorNotificationOperators

        public static List<OpOperator> GetLocationDistributorNotificationOperators(DataProvider dataProvider, OpLocation location, List<SmsComponent.OperationType> operationTypesToCheck)
        {
            List<OpOperator> mailingList = new List<OpOperator>();
            string customWhereClause = "[VALUE] is not null";
            customWhereClause += " AND (";
            customWhereClause += "(cast([VALUE] as nvarchar) not like '%,%' AND cast([VALUE] as nvarchar) like '" + location.IdDistributor + "')";
            customWhereClause += " OR";
            customWhereClause += "(CAST(VALUE as nvarchar) like '%,%' and (CAST(VALUE as nvarchar) like '%," + location.IdDistributor + ",%' OR CAST(VALUE as nvarchar) like '%," + location.IdDistributor + "' OR (CAST(VALUE as nvarchar) like '" + location.IdDistributor + ",%')))";
            customWhereClause += ")";
            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, IdDataType: new long[] { DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_LOCATION_ID_DISTRIBUTOR }, customWhereClause: customWhereClause);
            if (odList != null && odList.Count > 0)
            {
                List<OpOperator> oList = dataProvider.GetOperator(odList.Select(d => d.IdOperator).Distinct().ToArray());
                foreach (OpOperator oItem in oList)
                {
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Add)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Add))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Change)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Change))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Delete)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Delete))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.ChangeStatus)
                        && oItem.SmsNotificationOptions.GetNotify(Business.Objects.SmsNotificationOptions.StatusToOptions((Enums.LocationState)location.IdLocationStateType)))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                }
            }
            mailingList.RemoveAll(o => o.Actor == null);
            mailingList.RemoveAll(o => !o.SerialNbr.HasValue || o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) == null);
            mailingList = mailingList.Distinct().ToList();
            return mailingList;
        }

        #endregion
        #region GetSchema
        public static List<OpSchema> GetSchema(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, long idLocation)
        {
            List<OpSchema> sList = new List<OpSchema>();
            List<OpData> schemaDatas = dataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                       IdLocation: new long[] { idLocation }, IdDataType: new long[] { DataType.LOCATION_SCHEMA_DEFINITION, DataType.LOCATION_SCHEMA_CONFIGURATION });

            Dictionary<long, OpFile> fDict = new Dictionary<long, OpFile>();

            List<long> idFileToGet = new List<long>();
            foreach (object oItem in schemaDatas.Where(d => d.Value != null).Select(d => d.Value))
            {
                long idFile = 0;
                if (long.TryParse(oItem.ToString(), out idFile) && idFile > 0)
                    idFileToGet.Add(idFile);
            }

            if (idFileToGet.Count > 0)
                fDict = dataProvider.GetFile(idFileToGet.Distinct().ToArray()).ToDictionary(f => f.IdFile);


            foreach (int indexNbr in schemaDatas.Select(d => d.Index).Distinct())
            {
                OpData schemaDefinition = schemaDatas.Find(d => d.IdDataType == DataType.LOCATION_SCHEMA_DEFINITION && d.Index == indexNbr);
                OpData schemaConfiguration = schemaDatas.Find(d => d.IdDataType == DataType.LOCATION_SCHEMA_CONFIGURATION && d.Index == indexNbr);

                long idFileSchemaDefinition = 0;
                long idFileSchemaConfiguration = 0;

                
                if (!(schemaDefinition != null && schemaDefinition.Value != null && long.TryParse(schemaDefinition.Value.ToString(), out idFileSchemaDefinition) && idFileSchemaDefinition > 0))
                {
                    BaseComponent.Log(EventID.Forms.MissingLocationSchemaData, true, new object[] { "definition", idLocation, DataType.LOCATION_SCHEMA_DEFINITION, indexNbr });
                    continue;
                }
                if (!(schemaConfiguration != null && schemaConfiguration.Value != null && long.TryParse(schemaConfiguration.Value.ToString(), out idFileSchemaConfiguration) && idFileSchemaConfiguration > 0))
                {
                    BaseComponent.Log(EventID.Forms.MissingLocationSchemaData, true, new object[] { "configuration", idLocation, DataType.LOCATION_SCHEMA_CONFIGURATION, indexNbr });
                    continue;
                }
                if (!fDict.ContainsKey(idFileSchemaDefinition))
                {
                    BaseComponent.Log(EventID.Forms.MissingLocationSchemaFile, true, new object[] { "definition", idLocation, DataType.LOCATION_SCHEMA_DEFINITION, indexNbr, idFileSchemaDefinition });
                    continue;
                }
                if (!fDict.ContainsKey(idFileSchemaConfiguration))
                {
                    BaseComponent.Log(EventID.Forms.MissingLocationSchemaFile, true, new object[] { "configuration", idLocation, DataType.LOCATION_SCHEMA_CONFIGURATION, indexNbr, idFileSchemaConfiguration });
                    continue;
                }

                dataProvider.GetFileContent(fDict[idFileSchemaDefinition]);
                dataProvider.GetFileContent(fDict[idFileSchemaConfiguration]);
                BaseComponent.Log(EventID.Forms.ParameterValues, true, new object[] { "SchemaDefinitionName", fDict[idFileSchemaDefinition].PhysicalFileName });
                BaseComponent.Log(EventID.Forms.ParameterValues, true, new object[] { "SchemaDefinitionSize", (fDict[idFileSchemaDefinition].FileBytes as byte[]).Length });
                BaseComponent.Log(EventID.Forms.ParameterValues, true, new object[] { "SchemaConfigurationName", fDict[idFileSchemaConfiguration].PhysicalFileName });
                BaseComponent.Log(EventID.Forms.ParameterValues, true, new object[] { "SchemaConfigurationSize", (fDict[idFileSchemaConfiguration].FileBytes as byte[]).Length });

                System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(OpSchema));
                System.IO.Stream stream = new System.IO.MemoryStream(fDict[idFileSchemaConfiguration].FileBytes as byte[]);

                OpSchema sItem = xs.Deserialize(stream) as OpSchema;
                if (sItem != null)
                {
                    sItem.Schema = fDict[idFileSchemaDefinition].FileBytes as byte[];
                    stream.Close();
                    if (sItem.SchemaConfiguration == null)
                        sItem.SchemaConfiguration = new List<OpSchemaElement>();
                    else
                    {
                        Dictionary<long, OpDataType> dtTmpDict = dataProvider.GetDataType(sItem.SchemaConfiguration.Select(d => d.IdDataType).Distinct().ToArray()).ToDictionary(d => d.IdDataType);
                        sItem.SchemaConfiguration.ForEach(c => c.DataType = dtTmpDict[c.IdDataType]);
                    }
                    Dictionary<long, OpDataType> dtDict = DataFormatGroupComponent.SetDataTypeUnit(dataProvider, loggedOperator, module,
                                                            dataProvider.GetDataType(sItem.SchemaConfiguration.Select(c => c.IdDataType).Distinct().ToArray()).ToList())
                                                            .ToDictionary(d => d.IdDataType);
                    foreach (OpSchemaElement seItem in sItem.SchemaConfiguration)
                    {
                        seItem.Value = null;
                        if (dtDict.ContainsKey(seItem.IdDataType))
                        {
                            int idDataTypeClass = 0;
                            if (seItem.DataType != null)
                                idDataTypeClass = seItem.DataType.IdDataTypeClass;
                            seItem.DataType = dtDict[seItem.DataType.IdDataType];
                            if (idDataTypeClass > 0)//DataTypy które maja ReferrenceType mogą byc w bazie int a tutaj juz zostaną przetworzone na string
                                seItem.DataType.DataTypeClass = dataProvider.GetDataTypeClass(idDataTypeClass);
                        }
                    }
                }
                else
                {
                    BaseComponent.Log(EventID.Forms.LocationSchemaConfigurationDeserializationFailed, true, new object[] { idLocation });
                }
                BaseComponent.Log(EventID.Forms.ParameterValues, true, new object[] { "SchemaElementCount", sItem != null && sItem.SchemaConfiguration != null ? sItem.SchemaConfiguration.Count.ToString() : "<null>" });
                sList.Add(sItem);
            }
            return sList;
        }

        #endregion
        #endregion
        #region Save
        public static OpLocation Save(DataProvider dataProvider, OpLocation objectToSave, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, objectToSave, useDBCollector: useDBCollector, loggedOperator: null, emailNotification: false, smsNotification: smsNotification);
        }
                
        public static OpLocation Save(DataProvider dataProvider, OpLocation objectToSave, OpOperator loggedOperator, 
            bool emailNotification = false, bool smsNotification = false, bool useDBCollector = false, bool loadNavigationProperties = true)
        {
            bool isNewItem = objectToSave.IdLocation == 0; //check if it's new location

            try
            {
                OpLocation originalLocation = null;
                if (objectToSave.IdLocation > 0)
                {
                    originalLocation = dataProvider.GetLocation(objectToSave.IdLocation, true);
                }

                //zapis do tabeli LOCATION            
                dataProvider.SaveLocation(objectToSave, useDBCollector);
                if (loadNavigationProperties)
                {
                    List<OpLocation> opLocs = new OpLocation[] { objectToSave }.ToList();
                    OpLocation.LoadNavigationProperties(ref opLocs, dataProvider);
                }

                //zapis do tabeli DATA
                OpData data = null;
                for (int i = objectToSave.DataList.Count - 1; i >= 0; i--)
                {
                    data = objectToSave.DataList[i];
                    data.IdLocation = objectToSave.IdLocation;

                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        DataComponent.Delete(dataProvider, data, useDBCollector);
                        objectToSave.DataList.Remove(data, true);
                    }
                    else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                    {
                        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data, useDBCollector);

                        if (objectToSave.DataList[i].OpState == OpChangeState.New)
                        {
                            if(loadNavigationProperties)
                                objectToSave.DataList[i].AssignReferences(dataProvider);
                        }

                        objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                    }
                }

                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.LocationAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdLocation);
                        LogSuccess(EventID.Forms.LocationAdded, objectToSave.IdLocation);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.LocationSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdLocation);
                        LogSuccess(EventID.Forms.LocationSaved, objectToSave.IdLocation);
                }

                if (emailNotification)
                {
                    Dictionary<OpLocation, OpLocation> mail = new Dictionary<OpLocation, OpLocation>();
                    mail.Add(objectToSave, originalLocation);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, mail, isNewItem ? EmailComponent.OperationType.Add : EmailComponent.OperationType.Change);

                    //LocationStateChangeEmail(dataProvider, objectToSave, originalLocation, loggedOperator, 2, string.Empty);
                }
                if (smsNotification)
                {
                    Dictionary<OpLocation, OpLocation> sms = new Dictionary<OpLocation, OpLocation>();
                    sms.Add(objectToSave, originalLocation);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, isNewItem ? SmsComponent.OperationType.Add : SmsComponent.OperationType.Change, useDBCollector);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.LocationAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.LocationAdditionError, ex.Message);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.LocationSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdLocation, ex.Message);
                        LogError(EventID.Forms.LocationSavingError, objectToSave.IdLocation, ex.Message);
                }
                throw ex;
            }

            //wpis do tabeli LOCATION_STATE_HISTORY jeśli zmienił sie LOCATION_STATE
            // !!! TW. to powinno zapisywać sie na żadanie, osobno
            // !!! AddStateHistory(dataProvider, loggedOperator, objectToSave);

            objectToSave.OpState = OpChangeState.Loaded;
            if(loadNavigationProperties)
                return dataProvider.GetLocation(objectToSave.IdLocation);
            return objectToSave;
        }

        public static void AddStateHistory(DataProvider dataProvider, OpLocation location, string notes)
        {
            AddStateHistory(dataProvider, location, location.IdLocationStateType, notes);
        }
        public static void AddStateHistory(DataProvider dataProvider, OpLocation location, int newIdLocationStateType, string notes, DateTime? time = null)
        {
            List<OpLocationStateHistory> history = GetStateHistory(dataProvider, location.IdLocation);
            AddStateHistory(dataProvider, location, history, newIdLocationStateType, location.InKpi, notes, true, time);
        }
        public static void AddStateHistory(DataProvider dataProvider, OpLocation location, List<OpLocationStateHistory> stateHistory, int idLocationStateType, bool inKpi, string notes, bool saveToDataBase, DateTime? time = null, bool useDBCollector = false)
        {
            if (!time.HasValue)
                time = dataProvider.DateTimeNow;

            OpLocationStateHistory lastNewState = null;
            OpLocationStateHistory lastState = null;

            if (!saveToDataBase) // nie ma zadania wpisu do DB, w takim razie sprawdzamy czy sa jakies wpisy z NEW
            {
                lastNewState = stateHistory.LastOrDefault(w => w.OpState == OpChangeState.New);
                if (lastNewState != null)
                {
                    if (lastNewState.IdLocationStateType == idLocationStateType &&
                        lastNewState.InKpi == inKpi &&
                        lastNewState.Notes == notes)
                    {
                        // jest wpis z NEW ale nic sie nie zmienilo (ktos cofnął zmianę przed zapisem??), usuwamy ten wpis z NEW
                        stateHistory.Remove(lastNewState);
                        lastState = stateHistory.LastOrDefault(w => w.OpState == OpChangeState.Modified);
                        if (lastState != null)
                        {
                            lastState.EndDate = null;
                            lastState.OpState = OpChangeState.Loaded;
                        }
                        return;
                    }
                }
            }

            lastState = stateHistory.LastOrDefault(w => w.EndDate == null);
            if (lastState != null && lastNewState == null)
                lastState.EndDate = time.Value;

            OpLocationStateHistory newState = new OpLocationStateHistory();
            if (lastNewState != null)
                newState = lastNewState;

            newState.IdLocation = location.IdLocation;
            newState.IdLocationStateType = idLocationStateType;
            newState.InKpi = inKpi;
            newState.StartDate = (lastState == null || lastState.EndDate == null) ? time.Value : lastState.EndDate.Value;
            newState.Notes = notes;
            newState.OpState = OpChangeState.New;

            newState.AssignReferences(dataProvider);
            if (lastNewState == null)
                stateHistory.Add(newState);

            if (saveToDataBase)
            {
                try
                {
                    LocationStateHistoryComponent.Save(dataProvider, stateHistory, useDBCollector);
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.LocationHistorySaved, LoggedOperator.Login, ServerCORE, location.IdLocation);
                        LogSuccess(EventID.Forms.LocationHistorySaved, location.IdLocation);
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.LocationHistorySavingError, LoggedOperator.Login, ServerCORE, location.IdLocation, ex.Message);
                        LogError(EventID.Forms.LocationHistorySavingError, location.IdLocation, ex.Message);
                    throw ex;
                }
            }
        }

        public static void ChangeLocationStateTelemetricServer(DataProvider dataProvider, OpLocation location)
        {
            if (location != null)
            {
                if(!location.DataList.Exists(d => d.IdDataType == DataType.LOCATION_SOURCE_SERVER_ID && d.Value != null)
                    || !location.DataList.Exists(d => d.IdDataType == DataType.LOCATION_IMR_SERVER_ID && d.Value != null))
                {
                    location.DataList.RemoveAll(d => d.IdDataType.In(new long[]{ DataType.LOCATION_SOURCE_SERVER_ID, DataType.LOCATION_IMR_SERVER_ID }));
                    location.DataList.AddRange(dataProvider.GetDataFilter(customWhereClause: "[ID_LOCATION] = " + location.IdLocation + " and [ID_DATA_TYPE] in (" + DataType.LOCATION_SOURCE_SERVER_ID + "," + DataType.LOCATION_IMR_SERVER_ID + ")"));
                    location.DataList.RemoveAll(d => d.IdDataType.In(new long[] { DataType.LOCATION_SOURCE_SERVER_ID, DataType.LOCATION_IMR_SERVER_ID }) && (d.Value == null || String.IsNullOrEmpty(d.Value.ToString())));
                }
                if (location.IdImrServer.HasValue && location.DataList.Exists(d => d.IdDataType == DataType.LOCATION_SOURCE_SERVER_ID && d.Value != null))
                {
                    OpImrServer server = dataProvider.GetImrServer(location.IdImrServer.Value);
                    if (server.IsGood && server.IsImrlt && server.ImrServerVersion == (int)Enums.ImrServerVersion.IMR4)
                    {
                        #region DBCollector

                        if (!String.IsNullOrEmpty(server.DBCollectorWebServiceAddress))
                        {
                            Services.Common.ImrWcfServiceReference.SESSION dbCollectorSession = null;
                            Services.Common.ImrWcfServiceReference.DBCollectorClient dbCollectorClient = null;


                            try
                            {
                                OpOperator wcfOperator = OperatorComponent.GetOperatorWithCreation(dataProvider, "ImrWcfDBCollector");
                                Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION> tItem =
                                    Services.Common.Components.DBCollectorComponent.CreateWcfConnection(server.DBCollectorWebServiceAddress, dataProvider.dbConnectionCore.DistributorFilter,
                                                                                                        wcfOperator.Login, wcfOperator.Password, Enums.Module.Unknown);
                                dbCollectorSession = tItem.Item2;
                                dbCollectorClient = tItem.Item1;


                                List<OpLocationStateHistory> lshLocal = dataProvider.GetLocationStateHistoryFilter(IdLocation: new long[] { location.IdLocation },
                                                                            autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, loadNavigationProperties: false);
                                DB_LOCATION_STATE_HISTORY[] lshServer = dbCollectorClient.GetLocationStateHistoryFilter(ref dbCollectorSession, null, new long[] { location.IdLocationOriginal }, null, null, null, null, null, null, null, false, System.Data.IsolationLevel.ReadUncommitted, 0);
                                if (dbCollectorSession.Status != Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                                    throw new Exception(dbCollectorSession.Error.Msg);
                                DB_LOCATION lServer = dbCollectorClient.GetLocationFilter(ref dbCollectorSession, new long[] { location.IdLocationOriginal }, null, null, null, null, null, null, null, null, null, null, false, System.Data.IsolationLevel.ReadUncommitted, 0).FirstOrDefault();
                                if (dbCollectorSession.Status != Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                                    throw new Exception(dbCollectorSession.Error.Msg);

                                #region History

                                List<DB_LOCATION_STATE_HISTORY> newestInDBHistory = lshLocal.Except(lshServer, w => new { IdLocationStateType = w.ID_LOCATION_STATE_TYPE, StartDate = w.START_DATE }).ToList();
                                List<DB_LOCATION_STATE_HISTORY> sameRecordsHistory = lshServer.Intersect(lshLocal, w => new { IdLocationStateType = w.ID_LOCATION_STATE_TYPE, StartDate = w.START_DATE }).ToList();
                                
                                foreach (DB_LOCATION_STATE_HISTORY op in sameRecordsHistory)
                                {
                                    DB_LOCATION_STATE_HISTORY founded = lshLocal.Find(w => w.ID_LOCATION_STATE_TYPE == op.ID_LOCATION_STATE_TYPE && w.START_DATE == op.START_DATE);
                                    if ((op.END_DATE != founded.END_DATE)
                                        || !String.Equals(op.NOTES, founded.NOTES)
                                        || (op.IN_KPI != founded.IN_KPI))
                                    {
                                        op.END_DATE = founded.END_DATE;
                                        op.NOTES = founded.NOTES;
                                        op.IN_KPI = founded.IN_KPI;

                                        op.ID_LOCATION_STATE = dbCollectorClient.SaveLocationStateHistory(ref dbCollectorSession, op);
                                        if (dbCollectorSession.Status != Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                                            throw new Exception(dbCollectorSession.Error.Msg);

                                    }
                                }

                                if (newestInDBHistory.Count(h => !h.END_DATE.HasValue) > 0 && lshServer != null && lshServer.Count(h => !h.END_DATE.HasValue) > 0)
                                {
                                    //zabezpieczenie przed niezamkniętymi wierszami
                                    foreach (DB_LOCATION_STATE_HISTORY lshExistingToClose in lshServer.Where(h => !h.END_DATE.HasValue))
                                    {
                                        DB_LOCATION_STATE_HISTORY existingLocal = lshLocal.Find(w => w.ID_LOCATION_STATE_TYPE == lshExistingToClose.ID_LOCATION_STATE_TYPE
                                                                                                  && w.START_DATE == lshExistingToClose.START_DATE);
                                        DateTime? closeDate = null;
                                        if (existingLocal != null && existingLocal.END_DATE.HasValue)
                                            closeDate = existingLocal.END_DATE;
                                        else
                                        {
                                            if (newestInDBHistory.Exists(h => !h.END_DATE.HasValue && h.START_DATE > lshExistingToClose.START_DATE))
                                                closeDate = newestInDBHistory.OrderBy(h => h.START_DATE).First(h => h.START_DATE > lshExistingToClose.START_DATE).START_DATE;
                                        }
                                        if (closeDate.HasValue)
                                        {
                                            lshExistingToClose.END_DATE = closeDate;
                                            dbCollectorClient.SaveLocationStateHistory(ref dbCollectorSession, lshExistingToClose);
                                            if (dbCollectorSession.Status != Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                                                throw new Exception(dbCollectorSession.Error.Msg);
                                        }
                                    }
                                }

                                foreach (DB_LOCATION_STATE_HISTORY op in newestInDBHistory)
                                {
                                    DB_LOCATION_STATE_HISTORY lshNew = new DB_LOCATION_STATE_HISTORY();
                                    lshNew.ID_LOCATION_STATE = 0;
                                    lshNew.ID_LOCATION = location.IdLocationOriginal;
                                    lshNew.ID_LOCATION_STATE_TYPE = op.ID_LOCATION_STATE_TYPE;
                                    lshNew.IN_KPI = op.IN_KPI;
                                    lshNew.START_DATE = op.START_DATE;
                                    lshNew.END_DATE = op.END_DATE;
                                    lshNew.NOTES = op.NOTES;

                                    lshNew.ID_LOCATION_STATE = dbCollectorClient.SaveLocationStateHistory(ref dbCollectorSession, lshNew);
                                        if (dbCollectorSession.Status != Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                                            throw new Exception(dbCollectorSession.Error.Msg);
                                }

                                #endregion

                                lServer.ID_LOCATION_STATE_TYPE = location.ID_LOCATION_STATE_TYPE;
                                dbCollectorClient.SaveLocation(ref dbCollectorSession, lServer);
                                        if (dbCollectorSession.Status != Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                                            throw new Exception(dbCollectorSession.Error.Msg);
                            }
                            catch (Exception ex)
                            {
                                if (dbCollectorClient != null)
                                    dbCollectorClient.Logout(dbCollectorSession);
                                if (dbCollectorClient != null && dbCollectorClient.State != System.ServiceModel.CommunicationState.Closed)
                                    dbCollectorClient.Abort();
                                throw new Exception(ex.Message);
                            }
                        }

                        #endregion
                    }
                }
            }
        }
        public static void SetHelperValues(DataProvider dataProvider, OpLocation location, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdLocation, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_LOCATION:
                        location.DataList.SetValue(DataType.HELPER_ID_LOCATION, 0, location.IdLocation);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        location.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, location.IdDistributor);
                        break;
                    case DataType.HELPER_ID_LOCATION_TYPE:
                        location.DataList.SetValue(DataType.HELPER_ID_LOCATION_TYPE, 0, location.IdLocationType);
                        break;
                    case DataType.HELPER_ID_LOCATION_STATE_TYPE:
                        location.DataList.SetValue(DataType.HELPER_ID_LOCATION_STATE_TYPE, 0, location.IdLocationStateType);
                        break;
                    case DataType.HELPER_LOCATION_IN_KPI:
                        location.DataList.SetValue(DataType.HELPER_LOCATION_IN_KPI, 0, location.InKpi);
                        break;
                    case DataType.HELPER_LOCATION_ALLOW_GROUPING:
                        location.DataList.SetValue(DataType.HELPER_LOCATION_ALLOW_GROUPING, 0, location.AllowGrouping);
                        break;
                    case DataType.HELPER_ID_CONSUMER:
                        location.DataList.SetValue(DataType.HELPER_ID_CONSUMER, 0, location.IdConsumer);
                        break;
                    default:
                        break;
                }
            }
            location.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion
        #region Delete

        public static void DeleteHistory(DataProvider dataProvider, long idLocation)
        {
            foreach (var item in dataProvider.GetLocationStateHistoryFilter(IdLocation: new long[] { idLocation }))
            {
                dataProvider.DeleteLocationStateHistory(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, long idLocation)
        {
            foreach (var item in dataProvider.GetDataFilter(IdLocation: new long[1] { idLocation }))//, IdDataType: DataProvider.LocationDataTypes.ToArray()
            {
                dataProvider.DeleteData(item);
            }
        }

        public static void DeleteLocationHierarchy(DataProvider dataProvider, long idLocation)
        {
            List<OpLocationHierarchy> itemsToDelete = new List<OpLocationHierarchy>();
            foreach (OpLocationHierarchy item in dataProvider.GetLocationHierarchyFilter(IdLocation: new long[] { idLocation }))
            {
                itemsToDelete.Add(item);
            }
            if (itemsToDelete != null && itemsToDelete.Count > 0)
            {
                foreach (OpLocationHierarchy item in dataProvider.GetLocationHierarchyFilter(IdLocationHierarchyParent: itemsToDelete.Select(t => t.IdLocationHierarchy).ToArray()))
                {
                    dataProvider.DeleteLocationHierarchy(item);
                }
            }
            foreach (OpLocationHierarchy item in itemsToDelete)
            {
                dataProvider.DeleteLocationHierarchy(item);
            }
        }

        public static void Delete(DataProvider dataProvider, OpLocation objectToDelete, OpOperator loggedOperator = null, bool emailNotification = false, bool smsNotification = false, bool useDBCollector = false)
        {
            try
            {
                DeleteHistory(dataProvider, objectToDelete.IdLocation);
                DeleteData(dataProvider, objectToDelete.IdLocation);
                DepositoryComponent.DeleteLocationElements(dataProvider, objectToDelete);
                dataProvider.DeleteLocation(objectToDelete);
                DeleteLocationHierarchy(dataProvider, objectToDelete.IdLocation);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.LocationDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdLocation);
                    LogSuccess(EventID.Forms.LocationDeleted, objectToDelete.IdLocation);
                if (emailNotification)
                {
                    Dictionary<OpLocation, OpLocation> email = new Dictionary<OpLocation, OpLocation>();
                    email.Add(objectToDelete, null);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, email, EmailComponent.OperationType.Delete);
                }
                if (smsNotification)
                {
                    Dictionary<OpLocation, OpLocation> sms = new Dictionary<OpLocation, OpLocation>();
                    sms.Add(objectToDelete, null);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, SmsComponent.OperationType.Delete, useDBCollector);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.LocationDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdLocation, ex.Message);
                    LogError(EventID.Forms.LocationDeletionError, objectToDelete.IdLocation, ex.Message);
                throw ex;
            }
        }

        #endregion




        #endregion
    }
}
