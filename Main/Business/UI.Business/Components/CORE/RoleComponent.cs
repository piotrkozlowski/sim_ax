﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.DW;

using OpDataUtil = IMR.Suite.UI.Business.Objects.OpDataUtil;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class RoleComponent
    {
        #region bblach te funkcje trzeba wyrzucic, jest juz kolumna ID_MODULE w tabeli ROLE - AF wyrzucone można usunąć
        /*
        public static Enums.Module GetRoleModule(int idRole)
        {
            if (idRole >= 4000 && idRole <= 4999)
                return Enums.Module.IMRPrepaidManager;

            return Enums.Module.Unknown;
        }*/
        /*
        public static OpRole GetOperatorRoleForModule(DataProvider dataProvider, OpOperator oper, Enums.Module module)
        {
            //int RoleRangeFrom = 0;
            //int RoleRangeTo = 0;

            //switch (module)
            //{
            //    case Enums.Module.IMRServiceTerminal:
            //        RoleRangeFrom = 4000;
            //        RoleRangeTo = 4999;
            //        break;
            //}

            List<OpRole> Roles = dataProvider.GetOperatorRoleFilter(IdOperator: new int[] { oper.IdOperator }).Select(or => or.Role).ToList(); //oper.Roles.FindAll(n => GetRoleModule(n.ID_ROLE) == Enums.Module.IMRPrepaidManager);
            if (Roles.Count > 0)
                return Roles[0];
            else
                return null;
        }*/
        #endregion

        public static OpRole GetOperatorRoleForModule(DataProvider dataProvider, OpOperator oper, Enums.Module module)
        {
            return oper.Roles.Find(n => n.IdModule == (int)module);
        }

        #region AssignNewRoleToOperator
        public static void AssignNewRoleToOperator(DataProvider dataProvider, OpOperator oper, Enums.Module module, OpRole newRole)
        {
            foreach (OpOperatorRole loop in dataProvider.GetOperatorRole(oper.IdOperator, null))
            {
                //Filter roles by module
                if (loop.IdOperator == oper.IdOperator && loop.Role.IdModule == (int)module)
                {
                    dataProvider.DeleteOperatorRole(loop);
                    oper.Roles.Remove(loop.Role);
                }
            }

            OpOperatorRole insert = new OpOperatorRole() { Operator = oper, Role = newRole };
            dataProvider.SaveOperatorRole(insert);

            oper.Roles.Add(newRole);
        }
        #endregion

        #region IsLocationAllowed
        public static bool IsLocationAllowed(DataProvider dataProvider, long idLocation)
        {
            if (dataProvider == null || dataProvider.dbConnectionCore == null || dataProvider.dbConnectionCore.LocationFilterDict.Keys.Count == 0) return true;

            return dataProvider.dbConnectionCore.LocationFilterDict.ContainsKey(idLocation);
        }
        #endregion

        #region AssignFiltersByPermission
        public static void AssignFiltersByPermission(DataProvider dataProvider, OpOperator opOperator)
        {
            if (dataProvider == null || opOperator == null) return;

            AssignAllowedLocationFilters(dataProvider: dataProvider, opOperator: opOperator);
            AssignAllowedTaskGroupFilters(dataProvider: dataProvider, opOperator: opOperator);
        }
        #endregion

        #region AssignAllowedLocationFilters
        private static void AssignAllowedLocationFilters(DataProvider dataProvider, OpOperator opOperator)
        {
            if (dataProvider == null || opOperator == null) return;

            bool containsAll = false;
            List<OpActivity> activites = opOperator.Activities.Where(w => w.IdActivity.In(new int[] { Activity.ALLOWED_LOCATION,
                                                                                                      Activity.ALLOWED_LOCATION_STATE_TYPE,
                                                                                                      Activity.ALLOWED_LOCATION_TYPE })).ToList();
            if (activites.Count == 0)
            {
                // Brak, niewykonifugrowane activites
                // mamy uprawnienie do wszystkich
                dataProvider.LocationFilter = new long[0];
                dataProvider.DeviceFilter = new long[0];
                dataProvider.MeterFilter = new long[0];
                return;
            }
            else if (activites.Any(w => w.ReferenceValue == null && w.Deny))
            {
                // ustawione Deny na All
                // zadna lokalizacja nie jest uprawniona, wiec urzadzenia ani liczniki tez nie sa uprawnione
                // nie mozna utworzyc pustej tablicy albo null, bo to oznacza wszystkie
                // zamiast tego utworzona lista z jednym elementem: 0, dzieki temu zapytanie do bazy nic nie zwroci (nie ma w bazie Id = 0)
                dataProvider.LocationFilter = new long[] { 0 };
                dataProvider.DeviceFilter = new long[] { 0 };
                dataProvider.MeterFilter = new long[] { 0 };
                return;
            }
            else if (activites.All(w => w.ReferenceValue == null) && !activites.Any(w => w.Deny))
            {
                // wszystkie
                dataProvider.LocationFilter = new long[0];
                dataProvider.DeviceFilter = new long[0];
                dataProvider.MeterFilter = new long[0];
                return;
            }
            List<long> allowedLocationList = new List<long>();
            List<long> disallowedFromHierarchy = new List<long>();
            List<long> allowedFromHierarchy = new List<long>();
            int[] allowedLocationType = null;
            int[] allowedLocationState = null;

            #region ALLOWED_LOCATION
            List<OpActivity> allowedLocationActivities = activites.Where(a => a.IdActivity == Activity.ALLOWED_LOCATION).ToList();
            if (allowedLocationActivities.Any(w => w.ReferenceValue == null) && allowedLocationActivities.Any(w => w.Deny))
                containsAll = true;
            else if (!allowedLocationActivities.Any(w => w.ReferenceValue == null) && allowedLocationActivities.All(w => w.Deny))
                containsAll = true;
            if(allowedLocationActivities.Count > 0)
            {
                List<long> disallowedLocationList = disallowedLocationList = allowedLocationActivities.Where(w => w.Deny).Select(q => GenericConverter.Parse<long>(q.ReferenceValue)).ToList();
                disallowedLocationList.RemoveAll(q => q == 0);
                disallowedLocationList = disallowedLocationList.Distinct().ToList();

                allowedLocationList = containsAll ? new List<long>() : allowedLocationActivities.Where(w => !w.Deny).Select(q => GenericConverter.Parse<long>(q.ReferenceValue)).ToList();
                allowedLocationList.RemoveAll(q => q == 0);
                allowedLocationList = allowedLocationList.Distinct().ToList();

                List<OpLocationHierarchy> allLocationHierarchy = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, IdLocation: new long[0]);
                Dictionary<long, List<OpLocationHierarchy>> locationHierarchyByLocationDict = allLocationHierarchy
                    .GroupBy(q => q.IdLocation).ToDictionary(q => q.Key, q => q.ToList());
                Dictionary<long, List<OpLocationHierarchy>> locationHierarchyByHierarchyParentDict = allLocationHierarchy.Where(q => q.IdLocationHierarchyParent.HasValue)
                    .GroupBy(q => q.IdLocationHierarchyParent.Value).ToDictionary(q => q.Key, q => q.ToList());

                disallowedFromHierarchy = LocationHierarchyComponent.GetChildIdLocations(locationHierarchyByLocationDict, locationHierarchyByHierarchyParentDict, disallowedLocationList).Union(disallowedLocationList).Distinct().ToList();
                allowedFromHierarchy = LocationHierarchyComponent.GetChildIdLocations(locationHierarchyByLocationDict, locationHierarchyByHierarchyParentDict, allowedLocationList).Union(allowedLocationList).Distinct().ToList();
            }
            #endregion
            #region ALLOWED_LOCATION_TYPE
            if (activites.Exists(a => a.IdActivity == Activity.ALLOWED_LOCATION_TYPE))
            {
                List<int> allowedLocationTypeTmp = new List<int>();
                if (activites.Any(w => w.IdActivity == Activity.ALLOWED_LOCATION_TYPE && !w.Deny && w.ReferenceValue == null))
                    allowedLocationTypeTmp = dataProvider.GetAllLocationType().Select(t => t.IdLocationType).ToList();
                else
                    allowedLocationTypeTmp = activites.Where(w => w.IdActivity == Activity.ALLOWED_LOCATION_TYPE && !w.Deny).Select(q => GenericConverter.Parse<int>(q.ReferenceValue)).ToList();
                List<int> disallowedLocationTypeTmp = activites.Where(w => w.IdActivity == Activity.ALLOWED_LOCATION_TYPE &&  w.Deny).Select(q => GenericConverter.Parse<int>(q.ReferenceValue)).ToList();//wiemy z wcześniejszychif'ów że nie mamy opcji all

                disallowedLocationTypeTmp.RemoveAll(q => q == 0);
                disallowedLocationTypeTmp = disallowedLocationTypeTmp.Distinct().ToList();

                allowedLocationTypeTmp.RemoveAll(q => q == 0);
                allowedLocationTypeTmp = allowedLocationTypeTmp.Distinct().ToList();

                if (disallowedLocationTypeTmp.Count > 0 && allowedLocationTypeTmp.Count == 0)
                    allowedLocationTypeTmp = dataProvider.GetAllLocationType()
                                                         .Select(t => t.IdLocationType)
                                                         .ToList();
                allowedLocationType = allowedLocationTypeTmp.Except(disallowedLocationTypeTmp).ToArray();

                if (allowedLocationType != null && (allowedLocationType.Length == 0 || allowedLocationType.Length == dataProvider.GetAllLocationType().Count))
                    allowedLocationType = null;
            }
            #endregion
            #region ALLOWED_LOCATION_STATE_TYPE
            if (activites.Exists(a => a.IdActivity == Activity.ALLOWED_LOCATION_STATE_TYPE))
            {
                List<int> allowedLocationStateTypeTmp = new List<int>();
                if (activites.Any(w => w.IdActivity == Activity.ALLOWED_LOCATION_STATE_TYPE && !w.Deny && w.ReferenceValue == null))
                    allowedLocationStateTypeTmp = dataProvider.GetAllLocationType().Select(t => t.IdLocationType).ToList();
                else
                    allowedLocationStateTypeTmp = activites.Where(w => w.IdActivity == Activity.ALLOWED_LOCATION_STATE_TYPE && !w.Deny).Select(q => GenericConverter.Parse<int>(q.ReferenceValue)).ToList();
                List<int> disallowedLocationStateTypeTmp = activites.Where(w => w.IdActivity == Activity.ALLOWED_LOCATION_STATE_TYPE && w.Deny).Select(q => GenericConverter.Parse<int>(q.ReferenceValue)).ToList();//wiemy z wcześniejszychif'ów że nie mamy opcji all

                disallowedLocationStateTypeTmp.RemoveAll(q => q == 0);
                disallowedLocationStateTypeTmp = disallowedLocationStateTypeTmp.Distinct().ToList();

                allowedLocationStateTypeTmp.RemoveAll(q => q == 0);
                allowedLocationStateTypeTmp = allowedLocationStateTypeTmp.Distinct().ToList();

                if (disallowedLocationStateTypeTmp.Count > 0 && allowedLocationStateTypeTmp.Count == 0)
                    allowedLocationStateTypeTmp = dataProvider.GetAllLocationStateType()
                                                         .Select(t => t.IdLocationStateType)
                                                         .ToList();
                allowedLocationState = allowedLocationStateTypeTmp.Except(disallowedLocationStateTypeTmp).ToArray();

                if (allowedLocationState != null && (allowedLocationState.Length == 0 || allowedLocationState.Length == dataProvider.GetAllLocationStateType().Count))
                    allowedLocationState = null;
            }
            #endregion

            bool querryFilterUsed = allowedLocationList.Count == 0;
            //IdLocationType: allowedLocationType, IdLocationStateType: allowedLocationState
            //Jeżeli nie mamy ALLOWED_LOCATION, ammy pewność że nie ma uprawnień per grupa, i mozemy od razu zapytać o określone typy i stany
            //w przeciwnym razie trzeba je odsiać dopiero po analizie hierarchii
            Dictionary<long, OpLocation> allLocationDict = dataProvider.GetLocationFilter(loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false, autoTransaction: false,
                                                                                          IdLocation: new long[0],
                                                                                          IdLocationType: querryFilterUsed ? allowedLocationType : null,
                                                                                          IdLocationStateType: querryFilterUsed ? allowedLocationState : null)
                                                                       .ToDictionary(l => l.IdLocation);
            //TO DO - żeby nie pobierać wszystkich lokalizacji trzeba by sprawdzić czy w ALLOWED_LOCATIONS mamy grupy i pobrać tylko ALLOWED_LOCATION + zawartość tych grup
            
            List<long> allowedDeviceList = new List<long>();
            List<long> allowedMeterList = new List<long>();

            if (containsAll)
                allowedLocationList = allLocationDict.Keys.ToList();
            else
                allowedLocationList = allLocationDict.Keys.Join(allowedLocationList, q => q, w => w, (q, w) => w).ToList();

            if (allowedFromHierarchy.Count > 0)
                allowedLocationList = allowedLocationList.Union(allowedFromHierarchy).Distinct().ToList();
            if (disallowedFromHierarchy.Count > 0)
                allowedLocationList = allowedLocationList.Except(disallowedFromHierarchy).Distinct().ToList();

            if (allowedLocationList.Count > 0 && !querryFilterUsed
                && allowedLocationType != null && allowedLocationType.Length > 0
                && allowedLocationState != null && allowedLocationState.Length > 0)
            {
                List<long> allowedLocationListTmp = new List<long>();
                Dictionary<int, int> allowedLocationTypeDict = allowedLocationType == null ? new Dictionary<int, int>() : allowedLocationType.ToDictionary(k => k, v => v);
                Dictionary<int, int> allowedLocationStateDict = allowedLocationType == null ? new Dictionary<int, int>() : allowedLocationState.ToDictionary(k => k, v => v);
                OpLocation lItem = null;
                foreach (long allowedLocation in allowedLocationList)
                {
                    lItem = allLocationDict.TryGetValue(allowedLocation);
                    if (lItem != null)//jeśli jest równy null to najprawdopodobniej uprawnienia sa tak skonfigurowane że dostajemy ALLOWED_LOCATION na lokalziacje dystrybutorów do których nie mamy uprawnień
                    {
                        if ((allowedLocationTypeDict.Count == 0 || allowedLocationTypeDict.ContainsKey(lItem.IdLocationType))
                            && (allowedLocationStateDict.Count == 0 || allowedLocationStateDict.ContainsKey(lItem.IdLocationStateType)))
                            allowedLocationListTmp.Add(allowedLocation);
                    }
                }
                allowedLocationList = allowedLocationListTmp;
            }

            if (allowedLocationList.Count == 0)
            {
                // zadna lokalizacja nie jest uprawniona, wiec urzadzenia ani liczniki tez nie sa uprawnione
                dataProvider.LocationFilter = new long[] { 0 };
                dataProvider.DeviceFilter = new long[] { 0 };
                dataProvider.MeterFilter = new long[] { 0 };
                return;
            }

            List<OpLocationEquipment> opLocationEquipments = dataProvider.GetLocationEquipmentFilter(loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false, autoTransaction: false,
                IdLocation: new long[0], IdMeter: new long[0], SerialNbr: new long[0],
                //IdLocation: allowedLocationList.ToArray(),
                customWhereClause: "[ID_LOCATION] IS NOT NULL",
                EndTime: Data.DB.TypeDateTimeCode.Null());
            Dictionary<long, long> allowedLocationDict = allowedLocationList.Distinct().ToDictionary(q => q);
            opLocationEquipments = opLocationEquipments.Where(q => q.IdLocation.HasValue
                && allowedLocationDict.ContainsKey(q.IdLocation.Value)).ToList();
            List<long> idMeterList = opLocationEquipments.Where(q => q.IdMeter.HasValue).Select(q => q.IdMeter.Value).Distinct().ToList();
            List<long> idDeviceList = opLocationEquipments.Where(q => q.SerialNbr.HasValue).Select(q => q.SerialNbr.Value).Distinct().ToList();
            if (idMeterList.Count > 0)
            {
                List<long> allIdMeterList = dataProvider.GetMeterFilter(loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false, autoTransaction: false,
                    IdMeter: new long[0]).Select(q => q.IdMeter).Distinct().ToList();
                allowedMeterList = allIdMeterList.Join(idMeterList, q => q, w => w, (q, w) => q).Distinct().ToList();
            }
            if (idDeviceList.Count > 0)
            {
                List<long> allIdDeviceList = dataProvider.GetDeviceFilter(loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false, autoTransaction: false,
                    SerialNbr: new long[0]).Select(q => q.SerialNbr).Distinct().ToList();
                allowedDeviceList = allIdDeviceList.Join(idDeviceList, q => q, w => w, (q, w) => q).Distinct().ToList();
            }
            dataProvider.LocationFilter = allowedLocationList.ToArray();
            if (allowedDeviceList.Count > 0)
                dataProvider.DeviceFilter = allowedDeviceList.ToArray();
            else
                dataProvider.DeviceFilter = new long[] { 0 };
            if (allowedMeterList.Count > 0)
                dataProvider.MeterFilter = allowedMeterList.ToArray();
            else
                dataProvider.MeterFilter = new long[] { 0 };
        }
        #endregion
        #region AssignAllowedTaskGroupFilters
        private static void AssignAllowedTaskGroupFilters(DataProvider dataProvider, OpOperator opOperator)
        {
            if (dataProvider == null || opOperator == null) return;

            List<OpActivity> activites = opOperator.Activities.Where(w => w.IdActivity == Activity.ALLOWED_TASK_GROUP).ToList();
            if (activites.Count == 0)
            {
                // Brak, niewykonifugrowane activites
                dataProvider.TaskGroupFilter = new int[0];
                return;
            }
            else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
            {
                //ustawione Deny na All
                dataProvider.TaskGroupFilter = new int[] { 0 };
                return;
            }
            else
            {
                List<int> idAllTaskGroupList = dataProvider.GetTaskGroupFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                             IdTaskGroup: new int[0], IdDistributor: new int[0]).Select(s => s.IdTaskGroup).ToList();

                if (activites.Any(w => w.ReferenceValue == null) && activites.Any(w => w.Deny == true))
                {
                    // wszystkie + wykluczone                                                           
                    List<int> disallowedTaskGroupList = activites.Where(w => w.Deny).Select(q => GenericConverter.Parse<int>(q.ReferenceValue)).ToList();
                    disallowedTaskGroupList.RemoveAll(q => q == 0);
                    disallowedTaskGroupList = disallowedTaskGroupList.Distinct().ToList();

                    List<int> allowedIdTaskGroup = idAllTaskGroupList.Except(disallowedTaskGroupList).ToList();

                    dataProvider.TaskGroupFilter = allowedIdTaskGroup.ToArray();
                }
                else if (activites.Any(w => w.ReferenceValue == null))
                {
                    // wszystkie
                    dataProvider.TaskGroupFilter = new int[0];
                }
                else
                {
                    // tylko wyspecyfikowane
                    List<int> disallowedObjects = idAllTaskGroupList.Join(activites.Where(w => w.Deny == true), w => w, q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();
                    List<int> allowTaskGroupList = idAllTaskGroupList.Join(activites, w => w, q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).Except(disallowedObjects).ToList();
                    dataProvider.TaskGroupFilter = allowTaskGroupList.ToArray();
                }
            }
        }
        #endregion

        #region HasPermission
        public static bool HasPermission(OpOperator opOperator, int idActivity, object referenceValue = null, bool defaultIfNoPermissionDefined = false)
        {
            // Metoda została przeniesiona do OpOperator tak by mieć do niej dostęp również w UI.Business.Object 

            return opOperator.HasPermission(idActivity, referenceValue, defaultIfNoPermissionDefined);

            //if (opOperator.Activities != null)
            //{
            //    if (referenceValue == null) //szukamy czy operator ma zdefiniowane idActivity i nie jest ustawione Deny na True
            //    {
            //        return opOperator.Activities.Any(a => a.IdActivity == idActivity && !a.Deny) && !opOperator.Activities.Any(a => a.IdActivity == idActivity && a.Deny);
            //    }
            //    else
            //    {
            //        //podano ID obiektu do ktorego mamy sprawdzić uprawnienie
            //        List<OpActivity> activites = opOperator.Activities.Where(w => w.IdActivity == idActivity).ToList();
            //        if (activites.Any(w => OpDataUtil.ValueEquals(w.ReferenceValue, referenceValue) && w.Deny == true))
            //        {
            //            //zdefiniowane uprawnienie z tym referenceValue z Deny = True
            //            return false;
            //        }
            //        else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
            //        {
            //            //wszystkie elementy sa wykluczone
            //            return false;
            //        }
            //        else if (activites.Any(w => w.ReferenceValue == null && w.Deny == false))
            //        {
            //            //wszystkie elementy dozwolone
            //            return true;
            //        }
            //        else if (activites.Any(w => OpDataUtil.ValueEquals(w.ReferenceValue, referenceValue) && w.Deny == false))
            //        {
            //            //zdefiniowane uprawnienie z tym referenceValue z Deny = False
            //            return true;
            //        }
            //    }
            //}
            //return false; //jesli nie ma zdefiniowanych uprawnień
        }
        #endregion
        #region FilterByPermission
        public static List<T> FilterByPermission<T>(List<T> objectList, int idActivity, OpOperator _operator, bool returnAllIfNoActivities = false) where T : class, IReferenceType
        {
            List<T> allowedList = new List<T>();
            if (_operator == null) return allowedList;

            List<OpActivity> activites = _operator.Activities.Where(w => w.IdActivity == idActivity).ToList();
            if (activites.Count == 0)
            {
                // Brak, niewykonifugrowane activites
                if (returnAllIfNoActivities) return objectList;
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
            {
                //ustawione Deny na All
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null) && activites.Any(w => w.Deny == true))
            {
                // wszystkie + wykluczone
                List<OpActivity> disallowedActivites = activites.Where(w => w.Deny == true).ToList();
                List<T> disallowedObjects = objectList.Join(disallowedActivites, w => w.GetReferenceKey(), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();
                allowedList = objectList.Except(disallowedObjects).ToList();
            }
            else if (activites.Any(w => w.ReferenceValue == null))
            {
                // wszystkie
                allowedList = objectList;
            }
            else
            {
                // tylko wyspecyfikowane
                List<T> allowedObjects = new List<T>();
                if (activites.Any(w => w.Deny == false))
                    allowedObjects = objectList.Join(activites.Where(a => a.Deny == false), w => w.GetReferenceKey(), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();
                else if(returnAllIfNoActivities)
                    allowedObjects = objectList;

                List<T> disallowedObjects = objectList.Join(activites.Where(w => w.Deny == true), w => w.GetReferenceKey(), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();

                allowedList = allowedObjects.Except(disallowedObjects).ToList();
                //Zakomentowane, ponieważ nie obsługiwało sytuacji w której mamy uprawnienie na Deny=true z ustawionym ReferenceValue != null, przy braku uprawnień Deny=false
                //allowedList = objectList.Join(activites, w => w.GetReferenceKey(), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).Except(disallowedObjects).ToList();
            }
            return allowedList;
        }
        public static List<T> FilterByPermission<T>(List<T> objectList, string referenceKeyProperty, int idActivity, OpOperator _operator, bool returnAllIfNoActivities = false) where T : class, IReferenceType
        {
            List<T> allowedList = new List<T>();
            if (_operator == null) return allowedList;

            List<OpActivity> activites = _operator.Activities.Where(w => w.IdActivity == idActivity).ToList();
            //           activites = activites.Distinct(d => new { IdActivity = d.IdActivity, Deny = d.Deny, ReferenceType = d.IdReferenceType, ReferenceValue = d.ReferenceValue }).ToList();

            if (activites.Count == 0)
            {
                // Brak, niewykonifugrowane activites
                if (returnAllIfNoActivities) return objectList;
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
            {
                //ustawione Deny na All
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null) && activites.Any(w => w.Deny == true))
            {
                // wszystkie + wykluczone
                List<OpActivity> disallowedActivites = activites.Where(w => w.Deny == true).ToList();
                List<T> disallowedObjects = objectList.Join(disallowedActivites, w => GetValue(w, referenceKeyProperty), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();
                allowedList = objectList.Except(disallowedObjects).ToList();
            }
            else if (activites.Any(w => w.ReferenceValue == null))
            {
                // wszystkie
                allowedList = objectList;
            }
            else
            {
                // tylko wyspecyfikowane
                allowedList = objectList.Join(activites, w => GetValue(w, referenceKeyProperty), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).Distinct().ToList();
            }
            return allowedList;
        }
        public static object GetValue(object objectItem, string propertyName)
        {
            if (objectItem != null && propertyName != null)
            {
                string[] properties = propertyName.Split('.');
                int propertyCount = 0;
                Type typeSource = objectItem.GetType();
                PropertyInfo propertyFounded = null;
                while (propertyCount < properties.Length)
                {
                    propertyFounded = typeSource.GetProperties().FirstOrDefault(p => p.Name == properties[propertyCount]);
                    if (propertyCount < properties.Length - 1 && propertyFounded != null)
                    {
                        objectItem = objectItem.GetType().GetProperty(properties[propertyCount]).GetValue(objectItem, null);
                        typeSource = propertyFounded.PropertyType;
                    }
                    propertyCount++;
                }
                if (propertyFounded != null)
                {
                    return propertyFounded.GetValue(objectItem, null);
                }
            }
            return null;
        }
        #endregion

        #region class ReferenceValueComparer
        internal class ReferenceValueComparer : IEqualityComparer<object>
        {
            bool IEqualityComparer<object>.Equals(object x, object y)
            {
                return OpDataUtil.ValueEquals(x, y);
            }
            public int GetHashCode(object obj)
            {
                return obj.GetHashCode();
            }
        }
        #endregion
    }
}
