﻿using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using System;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DataComponent
    {
        #region Methods
        //public static OpData GetByID(DataProvider dataProvider, int Id)
        //{
        //    if (Id == 0)
        //        return null;

        //    return dataProvider.GetData(Id);
        //}

        //public static List<OpData> GetAll(DataProvider dataProvider)
        //{
        //    return dataProvider.Data;
        //}

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, IOpData objectToSave, bool useDBCollector = false)
        {
            if (objectToSave is OpTaskData)
            {
                return Save(dataProvider, (OpTaskData)objectToSave);
            }
            else if (objectToSave is OpData)
            {
                return Save(dataProvider, (OpData)objectToSave, useDBCollector);
            }
            else if (objectToSave is OpProfileData)
            {
                return Save(dataProvider, (OpProfileData)objectToSave);
            }
            else if (objectToSave is OpOperatorData)
            {
                return Save(dataProvider, (OpOperatorData)objectToSave);
            }

            throw new ArgumentException("The type " + (objectToSave == null ? "<null>" : objectToSave.GetType().ToString()) + " is not supported");
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpProfileData objectToSave)
        {
            return dataProvider.SaveProfileData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpData objectToSave, bool useDBCollector = false)
        {
            return dataProvider.SaveData(objectToSave, useDBCollector);
            //return dataProvider.SaveData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpTaskData objectToSave)
        {
            return dataProvider.SaveTaskData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpRouteData objectToSave)
        {
            return dataProvider.SaveRouteData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpRoutePointData objectToSave)
        {
            return dataProvider.SaveRoutePointData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpIssueData objectToSave)
        {
            return dataProvider.SaveIssueData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpOperatorData objectToSave)
        {
            return dataProvider.SaveOperatorData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpDistributorData objectToSave)
        {
            return dataProvider.SaveDistributorData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpSystemData objectToSave)
        {
            return dataProvider.SaveSystemData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpPackageData objectToSave)
        {
            return dataProvider.SavePackageData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, Objects.DW.OpReportData objectToSave)
        {
            return dataProvider.SaveReportData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpConsumerData objectToSave)
        {
            return dataProvider.SaveConsumerData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpConsumerTypeData objectToSave)
        {
            return dataProvider.SaveConsumerTypeData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpTariffData objectToSave)
        {
            return dataProvider.SaveTariffData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpModuleData objectToSave)
        {
            return dataProvider.SaveModuleData(objectToSave);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static long Save(DataProvider dataProvider, OpSessionData objectToSave)
        {
            return dataProvider.SaveSessionData(objectToSave);
        }

        /* DELETE */

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpData objectToDelete, bool useDBCollector = false)
        {
            dataProvider.DeleteData(objectToDelete, useDBCollector);
            //dataProvider.DeleteData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpIssueData objectToDelete)
        {
            dataProvider.DeleteIssueData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpTaskData objectToDelete)
        {
            dataProvider.DeleteTaskData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpPackageData objectToDelete)
        {
            dataProvider.DeletePackageData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpConsumerData objectToDelete)
        {
            dataProvider.DeleteConsumerData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, Objects.DW.OpReportData objectToDelete)
        {
            dataProvider.DeleteReportData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpRouteData objectToDelete)
        {
            dataProvider.DeleteRouteData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpOperatorData objectToDelete)
        {
            dataProvider.DeleteOperatorData(objectToDelete);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void Delete(DataProvider dataProvider, OpSessionData objectToDelete)
        {
            dataProvider.DeleteSessionData(objectToDelete);
        }
        /*
        public static void Delete(DataProvider dataProvider, OpConsumerTypeData objectToDelete)
        {
            dataProvider.DeleteConsumerTypeData(objectToDelete);
        }*/


        #endregion

        #region Methods overloaded for IDataProvider

        #region Delete
        
        #region OpData

        public static void Delete(IDataProvider dataProvider, OpData objectToDelete, bool useDBCollector = false)
        {
            dataProvider.DeleteData(objectToDelete, useDBCollector);
            //dataProvider.DeleteData(objectToDelete);
        }
        
        #endregion
        #region OpIssueData

        public static void Delete(IDataProvider dataProvider, OpIssueData objectToDelete)
        {
            dataProvider.DeleteIssueData(objectToDelete);
        }
        
        #endregion
        #region OpTaskData

        public static void Delete(IDataProvider dataProvider, OpTaskData objectToDelete)
        {
            dataProvider.DeleteTaskData(objectToDelete);
        }
        
        #endregion
        #region OpPackageData

        public static void Delete(IDataProvider dataProvider, OpPackageData objectToDelete)
        {
            dataProvider.DeletePackageData(objectToDelete);
        }
        
        #endregion
        #region OpConsumerData

        public static void Delete(IDataProvider dataProvider, OpConsumerData objectToDelete)
        {
            dataProvider.DeleteConsumerData(objectToDelete);
        }
        
        #endregion
        #region Objects.DW.OpReportData

        public static void Delete(IDataProvider dataProvider, Objects.DW.OpReportData objectToDelete)
        {
            dataProvider.DeleteReportData(objectToDelete);
        }
        
        #endregion
        #region OpRouteData

        public static void Delete(IDataProvider dataProvider, OpRouteData objectToDelete)
        {
            dataProvider.DeleteRouteData(objectToDelete);
        }
        
        #endregion
        #region OpOperatorData

        public static void Delete(IDataProvider dataProvider, OpOperatorData objectToDelete)
        {
            dataProvider.DeleteOperatorData(objectToDelete);
        }
        
        #endregion
        #region OpSessionData

        public static void Delete(IDataProvider dataProvider, OpSessionData objectToDelete)
        {
            dataProvider.DeleteSessionData(objectToDelete);
        }
        
        #endregion

        #endregion

        #region Save

        #region General

        public static long Save(IDataProvider dataProvider, IOpData objectToSave, bool useDBCollector = false)
        {
            if (objectToSave is OpTaskData)
            {
                return Save(dataProvider, (OpTaskData)objectToSave);
            }
            else if (objectToSave is OpData)
            {
                return Save(dataProvider, (OpData)objectToSave, useDBCollector);
            }
            else if (objectToSave is OpProfileData)
            {
                return Save(dataProvider, (OpProfileData)objectToSave);
            }
            else if (objectToSave is OpOperatorData)
            {
                return Save(dataProvider, (OpOperatorData)objectToSave);
            }

            throw new ArgumentException("The type " + (objectToSave == null ? "<null>" : objectToSave.GetType().ToString()) + " is not supported");
        }

        #endregion

        #region OpprofileData

        public static long Save(IDataProvider dataProvider, OpProfileData objectToSave)
        {
            return dataProvider.SaveProfileData(objectToSave);
        }
        
        #endregion
        #region OpData

        public static long Save(IDataProvider dataProvider, OpData objectToSave, bool useDBCollector = false)
        {
            return dataProvider.SaveData(objectToSave, useDBCollector);
            //return dataProvider.SaveData(objectToSave);
        }
                
        #endregion
        #region OpTaskData

        public static long Save(IDataProvider dataProvider, OpTaskData objectToSave)
        {
            return dataProvider.SaveTaskData(objectToSave);
        }
        
        #endregion
        #region OpRouteData

        public static long Save(IDataProvider dataProvider, OpRouteData objectToSave)
        {
            return dataProvider.SaveRouteData(objectToSave);
        }
        
        #endregion
        #region OpRoutePointData

        public static long Save(IDataProvider dataProvider, OpRoutePointData objectToSave)
        {
            return dataProvider.SaveRoutePointData(objectToSave);
        }
        
        #endregion
        #region OpIssueData

        public static long Save(IDataProvider dataProvider, OpIssueData objectToSave)
        {
            return dataProvider.SaveIssueData(objectToSave);
        }
        
        #endregion
        #region OpOperatorData

        public static long Save(IDataProvider dataProvider, OpOperatorData objectToSave)
        {
            return dataProvider.SaveOperatorData(objectToSave);
        }
        
        #endregion
        #region OpDistributorData

        public static long Save(IDataProvider dataProvider, OpDistributorData objectToSave)
        {
            return dataProvider.SaveDistributorData(objectToSave);
        }
        
        #endregion
        #region OpSystemData

        public static long Save(IDataProvider dataProvider, OpSystemData objectToSave)
        {
            return dataProvider.SaveSystemData(objectToSave);
        }
        
        #endregion
        #region OpPackageData

        public static long Save(IDataProvider dataProvider, OpPackageData objectToSave)
        {
            return dataProvider.SavePackageData(objectToSave);
        }
        
        #endregion
        #region Objects.DW.OpReportData

        public static long Save(IDataProvider dataProvider, Objects.DW.OpReportData objectToSave)
        {
            return dataProvider.SaveReportData(objectToSave);
        }
        
        #endregion
        #region OpConsumerData

        public static long Save(IDataProvider dataProvider, OpConsumerData objectToSave)
        {
            return dataProvider.SaveConsumerData(objectToSave);
        }
        
        #endregion
        #region OpConsumerTypeData

        public static long Save(IDataProvider dataProvider, OpConsumerTypeData objectToSave)
        {
            return dataProvider.SaveConsumerTypeData(objectToSave);
        }
        
        #endregion
        #region OpTariffData

        public static long Save(IDataProvider dataProvider, OpTariffData objectToDelete)
        {
            return dataProvider.SaveTariffData(objectToDelete);
        }
        
        #endregion
        #region OpModuleData

        public static long Save(IDataProvider dataProvider, OpModuleData objectToDelete)
        {
            return dataProvider.SaveModuleData(objectToDelete);
        }
        
        #endregion
        #region OpSessionData

        public static long Save(IDataProvider dataProvider, OpSessionData objectToDelete)
        {
            return dataProvider.SaveSessionData(objectToDelete);
        }
        
        #endregion

        #endregion

        #endregion
    }
}
