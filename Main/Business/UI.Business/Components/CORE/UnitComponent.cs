﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects.DAQ;
using Core = IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public static class UnitComponent
    {
        /*
            * sdudzik - wzor do przeliczenia z jednostki pierwotnej na wybrana jednostke z uwzglednieniem bias obydwu jednostek:
            * 
            * NewValue = (OldScale * OldValue + OldBias - NewBias) / NewScale
            * NewValue - wartosc po przeliczeniu
            * OldValue - wartosc pierwotna
            * NewScale - skala wybranej jednostki
            * OldScale - skala jednostki pierwotnej
            * NewBias - bias wybranej jednostki
            * OldBias - bias jednostki pierwotnej
        */

        #region Evaluate
        /// <summary>
        /// Calculates the value for given unit and rounds a value to specified number of fractional digits
        /// </summary>
        /// <param name="value">Value to convert</param>
        /// <param name="dataType">Source data type</param>
        /// <param name="destUnit">Indicates destination unit used in calculating object value</param>
        /// <param name="digits">Number of fractional digits</param>
        /// <returns></returns>
        public static object Evaluate(object value, OpDataType dataType, OpUnit destUnit, int? digits = null)
        {
            Dictionary<long, OpUnit> dataTypeUnitDict = new Dictionary<long,OpUnit>();
            Dictionary<long, int?> dataTypeFractionalDigitsDict = new Dictionary<long, int?>();
            if (dataType != null)
            {
                if (destUnit != null)
                    dataTypeUnitDict[dataType.IdDataType] = destUnit;
                dataTypeFractionalDigitsDict[dataType.IdDataType] = digits;
            }
            return ChangeUnit(value, dataType, dataTypeUnitDict, dataTypeFractionalDigitsDict);
        }

        /// <summary>
        /// Calculates the value for given unit and rounds a value to specified number of fractional digits
        /// </summary>
        /// <param name="opData">OpData object</param>
        /// <param name="destUnit">Indicates destination unit used in calculating object value</param>
        /// <param name="digits">Number of fractional digits</param>
        /// <returns></returns>
        public static object Evaluate(Objects.CORE.OpData opData, OpUnit destUnit, int? digits = null)
        {
            if (opData == null)
                return null;
            return Evaluate(opData.Value, opData.DataType, destUnit, digits);
        }

        public static object Evaluate(Objects.CORE.OpData opData, System.Tuple<OpUnit, int> destUnitDigits)
        {
            if (opData == null)
                return null;
            if (destUnitDigits == null)
                return opData.Value;
            return Evaluate(opData.Value, opData.DataType, destUnitDigits.Item1, destUnitDigits.Item2);
        }

        /// <summary>
        /// Calculates the value for given unit and rounds a value to specified number of fractional digits
        /// </summary>
        /// <param name="opDataArch">OpDataArch object</param>
        /// <param name="destUnit">Indicates destination unit used in calculating object value</param>
        /// <param name="digits">Number of fractional digits</param>
        /// <returns></returns>
        public static object Evaluate(Objects.DAQ.OpDataArch opDataArch, OpUnit destUnit, int? digits = null)
        {
            if (opDataArch == null)
                return null;
            return Evaluate(opDataArch.Value, opDataArch.DataType, destUnit, digits);
        }
        public static object Evaluate(Objects.DAQ.OpDataArch opDataArch, System.Tuple<OpUnit, int> destUnitDigits)
        {
            if (opDataArch == null)
                return null;
            if (destUnitDigits == null)
                return opDataArch.Value;
            return Evaluate(opDataArch.Value, opDataArch.DataType, destUnitDigits.Item1, destUnitDigits.Item2);
        }

        /// <summary>
        /// Calculates the value for given unit and rounds a value to specified number of fractional digits
        /// </summary>
        /// <param name="opData">OpData object</param>
        /// <param name="destUnit">Indicates destination unit used in calculating object value</param>
        /// <param name="digits">Number of fractional digits</param>
        /// <returns></returns>
        public static object Evaluate(Objects.DW.OpData opData, OpUnit destUnit, int? digits = null)
        {
            if (opData == null)
                return null;
            return Evaluate(opData.Value, opData.DataType, destUnit, digits);
        }
        public static object Evaluate(Objects.DW.OpData opData, System.Tuple<OpUnit, int> destUnitDigits)
        {
            if (opData == null)
                return null;
            if (destUnitDigits == null)
                return opData.Value;
            return Evaluate(opData.Value, opData.DataType, destUnitDigits.Item1, destUnitDigits.Item2);
        }

        /// <summary>
        /// Calculates the value for given unit and rounds a value to specified number of fractional digits
        /// </summary>
        /// <param name="opDataArch">OpDataArch object</param>
        /// <param name="destUnit">Indicates destination unit used in calculating object value</param>
        /// <param name="digits">Number of fractional digits</param>
        /// <returns></returns>
        public static object Evaluate(Objects.DW.OpDataArch opDataArch, OpUnit destUnit, int? digits = null)
        {
            if (opDataArch == null)
                return null;
            return Evaluate(opDataArch.Value, opDataArch.DataType, destUnit, digits);
        }
        public static object Evaluate(Objects.DW.OpDataArch opDataArch, System.Tuple<OpUnit, int> destUnitDigits)
        {
            if (opDataArch == null)
                return null;
            if (destUnitDigits == null)
                return opDataArch.Value;
            return Evaluate(opDataArch.Value, opDataArch.DataType, destUnitDigits.Item1, destUnitDigits.Item2);
        }

        /// <summary>
        /// Calculates the value for given unit and rounds a value to specified number of fractional digits
        /// </summary>
        /// <param name="opDataTemporal">OpDataTemporal object</param>
        /// <param name="destUnit">Indicates destination unit used in calculating object value</param>
        /// <param name="digits">Number of fractional digits</param>
        /// <returns></returns>
        public static object Evaluate(Objects.DW.OpDataTemporal opDataTemporal, OpUnit destUnit, int? digits = null)
        {
            if (opDataTemporal == null)
                return null;
            return Evaluate(opDataTemporal.Value, opDataTemporal.DataType, destUnit, digits);
        }
        public static object Evaluate(Objects.DW.OpDataTemporal opDataTemporal, System.Tuple<OpUnit, int> destUnitDigits)
        {
            if (opDataTemporal == null)
                return null;
            if (destUnitDigits == null)
                return opDataTemporal.Value;
            return Evaluate(opDataTemporal.Value, opDataTemporal.DataType, destUnitDigits.Item1, destUnitDigits.Item2);
        }

        /// <summary>
        /// Calculates the value for given unit and rounds a value to specified number of fractional digits
        /// </summary>
        /// <param name="opRefuelData">OpDataTemporal object</param>
        /// <param name="destUnit">Indicates destination unit used in calculating object value</param>
        /// <param name="digits">Number of fractional digits</param>
        /// <returns></returns>
        public static object Evaluate(Objects.DW.OpRefuelData opRefuelData, OpUnit destUnit, int? digits = null)
        {
            if (opRefuelData == null)
                return null;
            return Evaluate(opRefuelData.Value, opRefuelData.DataType, destUnit, digits);
        }
        public static object Evaluate(Objects.DW.OpRefuelData opRefuelData, System.Tuple<OpUnit, int> destUnitDigits)
        {
            if (opRefuelData == null)
                return null;
            if (destUnitDigits == null)
                return opRefuelData.Value;
            return Evaluate(opRefuelData.Value, opRefuelData.DataType, destUnitDigits.Item1, destUnitDigits.Item2);
        }
        #endregion

        #region GetDataTypeUnitDict
        public static Dictionary<long, OpUnit> GetDataTypeUnitDict(List<OpDataFormatGroupDetails> dataFormatGroupDetails, bool outDataTypeUnit)
        {
            Dictionary<long, OpUnit> dict = new Dictionary<long, OpUnit>();
            if (dataFormatGroupDetails == null || dataFormatGroupDetails.Count == 0) return dict;

            foreach (OpDataFormatGroupDetails dataFormat in dataFormatGroupDetails)
            {
                if (outDataTypeUnit)
                {
                    if (dataFormat.IdUnitOut.HasValue && dataFormat.UnitOut != null)
                        dict[dataFormat.IdDataType] = dataFormat.UnitOut;
                }
                else
                {
                    if (dataFormat.IdUnitIn.HasValue && dataFormat.UnitIn != null)
                        dict[dataFormat.IdDataType] = dataFormat.UnitIn;
                }
            }
            return dict;
        }
        #endregion

        #region ChangeUnit - List of CORE.OpData
        public static List<Objects.CORE.OpData> ChangeUnit(List<Objects.CORE.OpData> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Core.OpData.ChangeUnit(dataSource, dataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        public static List<Objects.CORE.OpData> ChangeUnit(List<Objects.CORE.OpData> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Core.OpData.ChangeUnit(dataSource, fromDataTypeUnitDict, toDataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        #endregion
        #region ChangeUnit - List of DAQ.OpDataArch
        public static List<Objects.DAQ.OpDataArch> ChangeUnit(List<Objects.DAQ.OpDataArch> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DAQ.OpDataArch.ChangeUnit(dataSource, dataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        public static List<Objects.DAQ.OpDataArch> ChangeUnit(List<Objects.DAQ.OpDataArch> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DAQ.OpDataArch.ChangeUnit(dataSource, fromDataTypeUnitDict, toDataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        #endregion
        #region ChangeUnit - List of DW.OpDataArch
        public static List<Objects.DW.OpDataArch> ChangeUnit(List<Objects.DW.OpDataArch> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpDataArch.ChangeUnit(dataSource, dataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        public static List<Objects.DW.OpDataArch> ChangeUnit(List<Objects.DW.OpDataArch> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpDataArch.ChangeUnit(dataSource, fromDataTypeUnitDict, toDataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        #endregion
        #region ChangeUnit - List of DW.OpData
        public static List<Objects.DW.OpData> ChangeUnit(List<Objects.DW.OpData> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpData.ChangeUnit(dataSource, dataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        public static List<Objects.DW.OpData> ChangeUnit(List<Objects.DW.OpData> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpData.ChangeUnit(dataSource, fromDataTypeUnitDict, toDataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        #endregion
        #region ChangeUnit - List of DW.OpDataTemporal
        public static List<Objects.DW.OpDataTemporal> ChangeUnit(List<Objects.DW.OpDataTemporal> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpDataTemporal.ChangeUnit(dataSource, dataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        public static List<Objects.DW.OpDataTemporal> ChangeUnit(List<Objects.DW.OpDataTemporal> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpDataTemporal.ChangeUnit(dataSource, fromDataTypeUnitDict, toDataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        #endregion
        #region ChangeUnit - List of DW.OpRefuelData
        public static List<Objects.DW.OpRefuelData> ChangeUnit(List<Objects.DW.OpRefuelData> dataSource, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpRefuelData.ChangeUnit(dataSource, dataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        public static List<Objects.DW.OpRefuelData> ChangeUnit(List<Objects.DW.OpRefuelData> dataSource, Dictionary<long, OpUnit> fromDataTypeUnitDict, Dictionary<long, OpUnit> toDataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return Objects.DW.OpRefuelData.ChangeUnit(dataSource, fromDataTypeUnitDict, toDataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        #endregion
        #region ChangeUnit
        public static object ChangeUnit(object data, OpDataType dataType, Dictionary<long, OpUnit> dataTypeUnitDict, Dictionary<long, int?> dataTypeFractionalDigitsDict = null)
        {
            return OpUnit.ChangeUnit(data, dataType, dataTypeUnitDict, dataTypeFractionalDigitsDict: dataTypeFractionalDigitsDict);
        }
        public static object ChangeUnit(object data, OpDataType dataType, OpUnit fromUnit, OpUnit toUnit, int? fractionalDigits = null)
        {
            return OpUnit.ChangeUnit(data, dataType, fromUnit, toUnit, fractionalDigits: fractionalDigits);
        }
        #endregion
    }
}
