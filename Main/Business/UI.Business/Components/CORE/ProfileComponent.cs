﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Profile;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ProfileComponent
    {
        #region GetHierarchicalProfiles
        /// <summary>
        /// Finds Profiles with Devices, Meters, Locations and Distributors assigned
        /// </summary>
        /// <param name="dataProvider">required</param>
        /// <param name="devices">optional, result will be logical sum of Devices, Meters, Locations and Distributors provided</param>
        /// <param name="meters">optional, result will be logical sum of Devices, Meters, Locations and Distributors provided</param>
        /// <param name="locations">optional, result will be logical sum of Devices, Meters, Locations and Distributors provided</param>
        /// <param name="distributors">optional, result will be logical sum of Devices, Meters, Locations and Distributors provided</param>
        /// <param name="idProfiles">optional, filters specific Profiles</param>
        /// <param name="queryDatabase">true to get Profiles from DB rather than cache</param>
        /// <returns>List of non-empty Profiles</returns>
        public static List<OpProfile> GetHierarchicalProfiles(DataProvider dataProvider, IEnumerable<OpDevice> devices = null, IEnumerable<OpMeter> meters = null,
                                                              IEnumerable<OpLocation> locations = null, IEnumerable<OpDistributor> distributors = null, int[] idProfiles = null,
                                                              bool queryDatabase = false, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            if (dataProvider == null)
                return new List<OpProfile>();

            List<long> deviceIds = null;
            List<long> meterIds = null;
            List<long> locationIds = null;
            List<int> distributorIds = null;
            if (devices == null) devices = new List<OpDevice>();
            else deviceIds = devices.Select(q => q.SerialNbr).ToList();
            if (meters == null) meters = new List<OpMeter>();
            else meterIds = meters.Select(q => q.IdMeter).ToList();
            if (locations == null) locations = new List<OpLocation>();
            else locationIds = locations.Select(q => q.IdLocation).ToList();
            if (distributors == null) distributors = new List<OpDistributor>();
            else distributorIds = distributors.Select(q => q.IdDistributor).ToList();

            Dictionary<int, List<long[]>> data = dataProvider.GetHierarchicalProfiles(deviceIds, meterIds, locationIds, distributorIds, idProfiles, DataType.PROFILE_ID);
            List<OpProfile> profiles = dataProvider.GetProfile(data.Keys.ToArray(), queryDatabase);
            profiles.ForEach(q => { q.Devices.Clear(); q.Meters.Clear(); q.Locations.Clear(); q.Distributors.Clear(); });
            Dictionary<int, OpProfile> profilesDict = profiles.ToDictionary(q => q.IdProfile);

            List<long[]> allEntries = data.SelectMany(q => q.Value).ToList();
            Dictionary<long, OpDevice> deviceDict = dataProvider.GetDevice(allEntries.Select(q => q[0]).Except(devices.Select(q => q.SerialNbr)).ToArray(), false, loadNavigationProperties, loadCustomData).Union(devices).ToDictionary(q => q.SerialNbr);
            Dictionary<long, OpMeter> meterDict = dataProvider.GetMeter(allEntries.Select(q => q[1]).Except(meters.Select(q => q.IdMeter)).ToArray(), false, loadNavigationProperties, loadCustomData).Union(meters).ToDictionary(q => q.IdMeter);
            Dictionary<long, OpLocation> locationDict = dataProvider.GetLocation(allEntries.Select(q => q[2]).Except(locations.Select(q => q.IdLocation)).ToArray(), false, loadNavigationProperties, loadCustomData).Union(locations).ToDictionary(q => q.IdLocation);
            Dictionary<int, OpDistributor> distributorDict = dataProvider.GetDistributor(allEntries.Select(q => (int)q[3]).Except(distributors.Select(q => q.IdDistributor)).ToArray()).Union(distributors).ToDictionary(q => q.IdDistributor);

            foreach (KeyValuePair<int, List<long[]>> entry in data)
            {
                if (profilesDict.ContainsKey(entry.Key))
                {
                    OpProfile profile = profilesDict[entry.Key];
                    foreach (long[] assignment in entry.Value)
                    {
                        System.Tuple<OpProfile.ProfileSource, object> source;
                        if (assignment[3] > 0)
                            source = new System.Tuple<OpProfile.ProfileSource, object>(OpProfile.ProfileSource.Distributor, distributorDict.ContainsKey((int)assignment[3]) ? (object)distributorDict[(int)assignment[3]] : (object)(int)assignment[3]);
                        else if (assignment[2] > 0)
                            source = new System.Tuple<OpProfile.ProfileSource, object>(OpProfile.ProfileSource.Location, locationDict.ContainsKey(assignment[2]) ? (object)locationDict[assignment[2]] : (object)assignment[2]);
                        else if (assignment[1] > 0)
                            source = new System.Tuple<OpProfile.ProfileSource, object>(OpProfile.ProfileSource.Meter, meterDict.ContainsKey(assignment[1]) ? (object)meterDict[assignment[1]] : (object)assignment[1]);
                        else if (assignment[0] > 0)
                            source = new System.Tuple<OpProfile.ProfileSource, object>(OpProfile.ProfileSource.Device, deviceDict.ContainsKey(assignment[0]) ? (object)deviceDict[assignment[0]] : (object)assignment[0]);
                        else
                            continue;

                        if (assignment[0] > 0 && deviceDict.ContainsKey(assignment[0]))
                        {
                            OpDevice device = deviceDict[assignment[0]];
                            if (profile.Devices.ContainsKey(device))
                            {
                                if ((int)profile.Devices[device].Item1 > (int)source.Item1)
                                    profile.Devices[device] = source;
                            }
                            else
                                profile.Devices.Add(device, source);

                            if (assignment[2] > 0 && locationDict.ContainsKey(assignment[2]))
                                device.CurrentLocations.Add(locationDict[assignment[2]]);
                        }
                        if (assignment[1] > 0 && (int)source.Item1 >= (int)OpProfile.ProfileSource.Meter && meterDict.ContainsKey(assignment[1]))
                        {
                            OpMeter meter = meterDict[assignment[1]];
                            if (profile.Meters.ContainsKey(meter))
                            {
                                if ((int)profile.Meters[meter].Item1 > (int)source.Item1)
                                    profile.Meters[meter] = source;
                            }
                            else
                                profile.Meters.Add(meter, source);

                            if (assignment[2] > 0 && locationDict.ContainsKey(assignment[2]))
                                meter.CurrentLocations.Add(locationDict[assignment[2]]);
                        }
                        if (assignment[2] > 0 && (int)source.Item1 >= (int)OpProfile.ProfileSource.Location && locationDict.ContainsKey(assignment[2]))
                        {
                            OpLocation location = locationDict[assignment[2]];
                            if (profile.Locations.ContainsKey(location))
                            {
                                if ((int)profile.Locations[location].Item1 > (int)source.Item1)
                                    profile.Locations[location] = source;
                            }
                            else
                                profile.Locations.Add(location, source);
                        }
                        if (assignment[3] > 0 && (int)source.Item1 >= (int)OpProfile.ProfileSource.Distributor && distributorDict.ContainsKey((int)assignment[3]))
                        {
                            OpDistributor distributor = distributorDict[(int)assignment[3]];
                            if (!profile.Distributors.ContainsKey(distributor.IdDistributor))
                                profile.Distributors.Add(distributor.IdDistributor, distributor);
                        }
                    }
                }
            }

            return profilesDict.Values.ToList();
        }
        #endregion

        #region AssignParameters
        private static void AddHierarchicalData(IEnumerable<IOpData> dataToAdd, OpDataList<OpData> itemDataList)
        {
            foreach (IOpData data in dataToAdd)
            {
                if (!dataToAdd.Any(q => q.IdDataType == data.IdDataType && !OpDataUtil.ValueEquals(q.Value, data.Value)) &&
                    !itemDataList.Exists(q => q.IdDataType == data.IdDataType))
                    itemDataList.Add(new OpData()
                    {
                        IdDataType = data.IdDataType,
                        Index = data.Index,
                        Value = data.Value,
                        OpState = OpChangeState.Undefined
                    });
            }
        }
        public static void AssignDeviceParameters(IEnumerable<OpProfile> profiles, IEnumerable<OpDevice> devices, IEnumerable<long> profileParamIdDataTypes = null)
        {
            if (profileParamIdDataTypes == null)
                profileParamIdDataTypes = profiles.SelectMany(q => q.Parameters.Select(w => w.IdDataType)).Distinct();

            foreach (OpDevice device in devices)
            {
                device.DataList.RemoveAll(q => profileParamIdDataTypes.Contains(q.IdDataType));
                Dictionary<OpProfile.ProfileSource, List<IOpData>> itemData = new Dictionary<OpProfile.ProfileSource, List<IOpData>>();
                foreach (OpProfile profile in profiles)
                {
                    if (profile.Devices.ContainsKey(device))
                    {
                        OpProfile.ProfileSource source = profile.Devices[device].Item1;
                        if (itemData.ContainsKey(source))
                            itemData[source].AddRange(profile.Parameters);
                        else
                            itemData.Add(source, new List<IOpData>(profile.Parameters));
                    }
                }

                if (itemData.ContainsKey(OpProfile.ProfileSource.Device))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Device], device.DataList);
                if (itemData.ContainsKey(OpProfile.ProfileSource.Meter))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Meter], device.DataList);
                if (itemData.ContainsKey(OpProfile.ProfileSource.Location))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Location], device.DataList);
                if (itemData.ContainsKey(OpProfile.ProfileSource.Distributor))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Distributor], device.DataList);
            }
        }
        public static void AssignMeterParameters(IEnumerable<OpProfile> profiles, IEnumerable<OpMeter> meters, IEnumerable<long> profileParamIdDataTypes = null)
        {
            if (profileParamIdDataTypes == null)
                profileParamIdDataTypes = profiles.SelectMany(q => q.Parameters.Select(w => w.IdDataType)).Distinct();

            foreach (OpMeter meter in meters)
            {
                meter.DataList.RemoveAll(q => profileParamIdDataTypes.Contains(q.IdDataType));
                Dictionary<OpProfile.ProfileSource, List<IOpData>> itemData = new Dictionary<OpProfile.ProfileSource, List<IOpData>>();
                foreach (OpProfile profile in profiles)
                {
                    if (profile.Meters.ContainsKey(meter))
                    {
                        OpProfile.ProfileSource source = profile.Meters[meter].Item1;
                        if (itemData.ContainsKey(source))
                            itemData[source].AddRange(profile.Parameters);
                        else
                            itemData.Add(source, new List<IOpData>(profile.Parameters));
                    }
                }

                if (itemData.ContainsKey(OpProfile.ProfileSource.Meter))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Meter], meter.DataList);
                if (itemData.ContainsKey(OpProfile.ProfileSource.Location))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Location], meter.DataList);
                if (itemData.ContainsKey(OpProfile.ProfileSource.Distributor))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Distributor], meter.DataList);
            }
        }
        public static void AssignLocationParameters(IEnumerable<OpProfile> profiles, IEnumerable<OpLocation> locations, IEnumerable<long> profileParamIdDataTypes = null)
        {
            if (profileParamIdDataTypes == null)
                profileParamIdDataTypes = profiles.SelectMany(q => q.Parameters.Select(w => w.IdDataType)).Distinct();

            foreach (OpLocation location in locations)
            {
                location.DataList.RemoveAll(q => profileParamIdDataTypes.Contains(q.IdDataType));
                Dictionary<OpProfile.ProfileSource, List<IOpData>> itemData = new Dictionary<OpProfile.ProfileSource, List<IOpData>>();
                foreach (OpProfile profile in profiles)
                {
                    if (profile.Locations.ContainsKey(location))
                    {
                        OpProfile.ProfileSource source = profile.Locations[location].Item1;
                        if (itemData.ContainsKey(source))
                            itemData[source].AddRange(profile.Parameters);
                        else
                            itemData.Add(source, new List<IOpData>(profile.Parameters));
                    }
                }

                if (itemData.ContainsKey(OpProfile.ProfileSource.Location))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Location], location.DataList);
                if (itemData.ContainsKey(OpProfile.ProfileSource.Distributor))
                    AddHierarchicalData(itemData[OpProfile.ProfileSource.Distributor], location.DataList);
            }
        }
        public static void AssignDistributorParameters(IEnumerable<OpProfile> profiles, IEnumerable<OpDistributor> distributors, IEnumerable<long> profileParamIdDataTypes = null)
        {
            if (profileParamIdDataTypes == null)
                profileParamIdDataTypes = profiles.SelectMany(q => q.Parameters.Select(w => w.IdDataType)).Distinct();

            foreach (OpDistributor distributor in distributors)
            {
                distributor.DataList.RemoveAll(q => profileParamIdDataTypes.Contains(q.IdDataType));
                List<IOpData> itemData = new List<IOpData>();
                foreach (OpProfile profile in profiles)
                {
                    if (profile.Distributors.ContainsKey(distributor.IdDistributor))
                    {
                        itemData.AddRange(profile.Parameters);
                    }
                }
                foreach (IOpData data in itemData)
                {
                    if (!itemData.Any(q => q.IdDataType == data.IdDataType && !OpDataUtil.ValueEquals(q.Value, data.Value)))
                        distributor.DataList.Add(new OpDistributorData()
                        {
                            IdDataType = data.IdDataType,
                            Index = data.Index,
                            Value = data.Value,
                            OpState = OpChangeState.Undefined
                        });
                }
            }
        }
        #endregion

        #region SaveDevices
        public static void SaveDevices(DataProvider dataProvider, IEnumerable<OpDevice> addedDevices, IEnumerable<OpDevice> removedDevices, OpProfile profile)
        {
            if (addedDevices.Count() > 0 || removedDevices.Count() > 0)
            {
                Dictionary<long, OpDevice> devicesDict = addedDevices.Union(removedDevices).ToDictionary(q => q.SerialNbr);
                List<OpData> devData = dataProvider.GetDataFilter(SerialNbr: devicesDict.Keys.ToArray(), IdDataType: new long[] { DataType.PROFILE_ID }, loadNavigationProperties: false);
                devData.ForEach(q => { if (q.SerialNbr.HasValue && devicesDict.ContainsKey(q.SerialNbr.Value)) devicesDict[q.SerialNbr.Value].DataList.Add(q); });
                foreach (OpDevice device in addedDevices)
                    if (!device.DataList.Exists(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile))
                    {
                        OpData toAdd = new OpData() { SerialNbr = device.SerialNbr, IdDataType = DataType.PROFILE_ID, Value = profile.IdProfile, Index = device.DataList.GetFirstFreeIndex(DataType.PROFILE_ID) };
                        dataProvider.SaveData(toAdd);
                        device.DataList.Add(toAdd);
                    }
                foreach (OpDevice device in removedDevices)
                {
                    OpData toRemove = device.DataList.Find(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile);
                    if (toRemove != null)
                    {
                        dataProvider.DeleteData(toRemove);
                        device.DataList.RemoveAll(q => q == toRemove);
                    }
                }
            }
        }
        #endregion
        #region SaveMeters
        public static void SaveMeters(DataProvider dataProvider, IEnumerable<OpMeter> addedMeters, IEnumerable<OpMeter> removedMeters, OpProfile profile)
        {
            if (addedMeters.Count() > 0 || removedMeters.Count() > 0)
            {
                Dictionary<long, OpMeter> metersDict = addedMeters.Union(removedMeters).ToDictionary(q => q.IdMeter);
                List<OpData> metData = dataProvider.GetDataFilter(IdMeter: metersDict.Keys.ToArray(), IdDataType: new long[] { DataType.PROFILE_ID }, loadNavigationProperties: false);
                metData.ForEach(q => { if (q.IdMeter.HasValue && metersDict.ContainsKey(q.IdMeter.Value)) metersDict[q.IdMeter.Value].DataList.Add(q); });
                foreach (OpMeter meter in addedMeters)
                    if (!meter.DataList.Exists(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile))
                    {
                        OpData toAdd = new OpData() { IdMeter = meter.IdMeter, IdDataType = DataType.PROFILE_ID, Value = profile.IdProfile, Index = meter.DataList.GetFirstFreeIndex(DataType.PROFILE_ID) };
                        dataProvider.SaveData(toAdd);
                        meter.DataList.Add(toAdd);
                    }

                foreach (OpMeter meter in removedMeters)
                {
                    OpData toRemove = meter.DataList.Find(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile);
                    if (toRemove != null)
                    {
                        dataProvider.DeleteData(toRemove);
                        meter.DataList.RemoveAll(q => q == toRemove);
                    }
                }
            }
        }
        #endregion
        #region SaveLocations
        public static void SaveLocations(DataProvider dataProvider, IEnumerable<OpLocation> addedLocations, IEnumerable<OpLocation> removedLocations, OpProfile profile)
        {
            if (addedLocations.Count() > 0 || removedLocations.Count() > 0)
            {
                Dictionary<long, OpLocation> locationsDict = addedLocations.Union(removedLocations).ToDictionary(q => q.IdLocation);
                List<OpData> locData = dataProvider.GetDataFilter(IdLocation: locationsDict.Keys.ToArray(), IdDataType: new long[] { DataType.PROFILE_ID }, loadNavigationProperties: false);
                locData.ForEach(q => { if (q.IdLocation.HasValue && locationsDict.ContainsKey(q.IdLocation.Value)) locationsDict[q.IdLocation.Value].DataList.Add(q); });
                foreach (OpLocation location in addedLocations)
                    if (!location.DataList.Exists(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile))
                    {
                        OpData toAdd = new OpData() { IdLocation = location.IdLocation, IdDataType = DataType.PROFILE_ID, Value = profile.IdProfile, Index = location.DataList.GetFirstFreeIndex(DataType.PROFILE_ID) };
                        dataProvider.SaveData(toAdd);
                        location.DataList.Add(toAdd);
                    }
                foreach (OpLocation location in removedLocations)
                {
                    OpData toRemove = location.DataList.Find(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile);
                    if (toRemove != null)
                    {
                        dataProvider.DeleteData(toRemove);
                        location.DataList.RemoveAll(q => q == toRemove);
                    }
                }
            }
        }
        #endregion
        #region SaveDistributors
        public static void SaveDistributors(DataProvider dataProvider, IEnumerable<OpDistributor> addedDistributors, IEnumerable<OpDistributor> removedDistributors, OpProfile profile)
        {
            if (addedDistributors.Count() > 0 || removedDistributors.Count() > 0)
            {
                Dictionary<int, OpDistributor> distributorDict = addedDistributors.Union(removedDistributors).ToDictionary(q => q.IdDistributor);
                List<OpDistributorData> disData = dataProvider.GetDistributorDataFilter(IdDistributor: distributorDict.Keys.ToArray(), IdDataType: new long[] { DataType.PROFILE_ID }, loadNavigationProperties: false);
                disData.ForEach(q => { if (distributorDict.ContainsKey(q.IdDistributor)) distributorDict[q.IdDistributor].DataList.Add(q); });
                foreach (OpDistributor distributor in addedDistributors)
                    if (!distributor.DataList.Exists(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile))
                    {
                        OpDistributorData toAdd = new OpDistributorData()
                        {
                            IdDistributor = distributor.IdDistributor,
                            IdDataType = DataType.PROFILE_ID,
                            Value = profile.IdProfile,
                            Index = distributor.DataList.GetFirstFreeIndex(DataType.PROFILE_ID)
                        };
                        dataProvider.SaveDistributorData(toAdd);
                        distributor.DataList.Add(toAdd);
                    }
                foreach (OpDistributor distributor in removedDistributors)
                {
                    OpDistributorData toRemove = distributor.DataList.Find(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile);
                    if (toRemove != null)
                    {
                        dataProvider.DeleteDistributorData(toRemove);
                        distributor.DataList.RemoveAll(q => q == toRemove);
                    }
                }
            }
        }
        #endregion
        #region AssignProfiles
        public static void AssignProfiles(DataProvider dataProvider, IEnumerable<OpProfile> profilesToAdd, IEnumerable<OpProfile> profilesToRemove, IEnumerable<OpMeter> meters)
        {
            if (meters.Count() > 0)
            {
                Dictionary<long, OpMeter> meterDict = meters.ToDictionary(q => q.IdMeter);
                List<OpData> metData = dataProvider.GetDataFilter(loadNavigationProperties: false, IdMeter: meterDict.Keys.ToArray(), IdDataType: new long[] { DataType.PROFILE_ID });
                metData.ForEach(q => { if (q.IdMeter.HasValue && meterDict.ContainsKey(q.IdMeter.Value)) meterDict[q.IdMeter.Value].DataList.Add(q); });
                foreach (OpMeter meter in meters)
                {
                    foreach (OpProfile profile in profilesToAdd)
                        if (!meter.DataList.Exists(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile))
                        {
                            OpData toAdd = new OpData() { IdMeter = meter.IdMeter, IdDataType = DataType.PROFILE_ID, Value = profile.IdProfile, Index = meter.DataList.GetFirstFreeIndex(DataType.PROFILE_ID) };
                            dataProvider.SaveData(toAdd);
                            meter.DataList.Add(toAdd);
                        }
                    foreach (OpProfile profile in profilesToRemove)
                    {
                        OpData toRemove = meter.DataList.Find(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile);
                        if (toRemove != null)
                        {
                            dataProvider.DeleteData(toRemove);
                            meter.DataList.RemoveAll(q => q == toRemove);
                        }
                    }
                }
            }
        }
        public static void AssignProfiles(DataProvider dataProvider, IEnumerable<OpProfile> profilesToAdd, IEnumerable<OpProfile> profilesToRemove, IEnumerable<OpLocation> locations)
        {
            if (locations.Count() > 0)
            {
                Dictionary<long, OpLocation> locationDict = locations.ToDictionary(q => q.IdLocation);
                List<OpData> locData = dataProvider.GetDataFilter(loadNavigationProperties: false, IdLocation: locationDict.Keys.ToArray(), IdDataType: new long[] { DataType.PROFILE_ID });
                locData.ForEach(q => { if (q.IdLocation.HasValue && locationDict.ContainsKey(q.IdLocation.Value)) locationDict[q.IdLocation.Value].DataList.Add(q); });
                foreach (OpLocation location in locations)
                {
                    foreach (OpProfile profile in profilesToAdd)
                        if (!location.DataList.Exists(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile))
                        {
                            OpData toAdd = new OpData() { IdLocation = location.IdLocation, IdDataType = DataType.PROFILE_ID, Value = profile.IdProfile, Index = location.DataList.GetFirstFreeIndex(DataType.PROFILE_ID) };
                            dataProvider.SaveData(toAdd);
                            location.DataList.Add(toAdd);
                        }
                    foreach (OpProfile profile in profilesToRemove)
                    {
                        OpData toRemove = location.DataList.Find(q => q.IdDataType == DataType.PROFILE_ID && q.TryGetNullableValue<int>() == profile.IdProfile);
                        if (toRemove != null)
                        {
                            dataProvider.DeleteData(toRemove);
                            location.DataList.RemoveAll(q => q == toRemove);
                        }
                    }
                }
            }
        }
        #endregion
        #region GetAllWithParameter
        public static List<OpProfile> GetAllWithParameter(DataProvider dataProvider, long idDataType, object value)
        {
            return dataProvider.GetAllProfile().FindAll(q => q.Parameters.Exists(w => w.IdDataType == idDataType && OpDataUtil.ValueEquals(w.Value, value)));
        }
        #endregion

        #region GetProfileDataForLocation
        /// <summary>
        /// Finds Profile data for Profiles set in Location or Distributor of Location
        /// </summary>
        /// <param name="dataProvider">Instance of DataProvider</param>
        /// <param name="location">Instance of OpLocation to find profile id parameter</param>
        /// <param name="module">If null find first profile with null module, if not null find first profile for given module</param>
        /// <param name="dataTypes">List of data type ids to search in found profiles</param>
        /// <param name="tryGetFromDistributor">Indicates if distributor of location should be searched for profile id parameter when location lacks of one</param>
        /// <param name="tryQueryDatabase">Indicates if profile id parameter should be searched in database when Location/Distributor data list lack of one</param>
        /// <param name="getHierarchicalProfiles">Indicates if profile data should be searched in first found profile or in profiles hierarchy if module is not null</param>
        /// <param name="allowNullValues">Indicates if profile data with null value should be returned</param>
        /// <returns>List of OpProfileData</returns>
        [Obsolete("Use GetMerged... methods instead!")]
        public static List<OpProfileData> GetProfileDataForLocation(DataProvider dataProvider, OpLocation location, Enums.Module? module, List<long> dataTypes,
            bool tryGetFromDistributor = true, bool tryQueryDatabase = true, bool getHierarchicalProfiles = true, bool allowNullValues = false)
        {
            List<OpProfileData> profileData = new List<OpProfileData>();

            if (location == null || dataProvider == null || dataTypes == null || dataTypes.Count == 0) return profileData;

            List<OpProfile> profileList = new List<OpProfile>();

            List<OpData> profileIdLocationDataList = location.DataList.Where(q => q.Value != null && q.IdDataType == DataType.PROFILE_ID).ToList();
            List<int> profileIdList = new List<int>();
            if (profileIdLocationDataList.Count == 0 && tryQueryDatabase)
            {
                profileIdLocationDataList = dataProvider.GetDataFilter(loadNavigationProperties: false, IdLocation: new long[] { location.IdLocation },
                    IdDataType: new long[] { DataType.PROFILE_ID }, customWhereClause: @" [VALUE] IS NOT NULL ");
            }
            foreach (OpData profileIdLocationData in profileIdLocationDataList)
            {
                int profileId = GenericConverter.Parse<int>(profileIdLocationData.Value);
                if (profileId > 0)
                    profileIdList.Add(profileId);
            }

            if (profileIdList.Count == 0 && tryGetFromDistributor && location.Distributor != null)
            {
                List<OpDistributorData> profileIdDistributorDataList = location.Distributor.DataList.Where(q => q.Value != null && q.IdDataType == DataType.PROFILE_ID).ToList();
                if (profileIdDistributorDataList.Count == 0 && tryQueryDatabase)
                {
                    profileIdDistributorDataList = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, IdDistributor: new int[] { location.IdDistributor },
                        IdDataType: new long[] { DataType.PROFILE_ID }, customWhereClause: @" [VALUE] IS NOT NULL ");
                }
                foreach (OpDistributorData profileIdDistributorData in profileIdDistributorDataList)
                {
                    int profileId = GenericConverter.Parse<int>(profileIdDistributorData.Value);
                    if (profileId > 0)
                        profileIdList.Add(profileId);
                }
            }

            if (profileIdList.Count > 0)
            {
                if (!module.HasValue)
                {
                    profileList = dataProvider.GetProfileFilter(loadNavigationProperties: false, IdProfile: profileIdList.Distinct().ToArray(),
                        customWhereClause: "[ID_MODULE] IS NULL", topCount: 1);
                    OpProfile profile = profileList.FirstOrDefault();
                    if (profile != null)
                    {
                        foreach (OpProfileData data in profile.Parameters.Where(q => dataTypes.Contains(q.IdDataType)
                            && (allowNullValues || q.Value != null)))
                        {
                            profileData.Add(data);
                        }
                    }
                }
                else
                {
                    string customWhere = null;
                    long? topCount = null;
                    if (getHierarchicalProfiles)
                        customWhere = String.Format("([ID_MODULE] = {0} OR [ID_MODULE] IS NULL)", (int)module.Value);
                    else
                    {
                        customWhere = String.Format("[ID_MODULE] = {0}", (int)module.Value);
                        topCount = 1;
                    }
                    profileList = dataProvider.GetProfileFilter(loadNavigationProperties: false, IdProfile: profileIdList.Distinct().ToArray(),
                        customWhereClause: customWhere, topCount: topCount);
                    if (getHierarchicalProfiles)
                    {
                        OpProfile moduleProfile = profileList.FirstOrDefault(q => q.IdModule.HasValue && q.IdModule.Value == (int)module);
                        if (moduleProfile != null)
                        {
                            foreach (OpProfileData data in moduleProfile.Parameters.Where(q => dataTypes.Contains(q.IdDataType)
                                && (allowNullValues || q.Value != null)))
                            {
                                profileData.Add(data);
                            }
                        }
                        OpProfile defaultProfile = profileList.FirstOrDefault(q => !q.IdModule.HasValue);
                        if (defaultProfile != null)
                        {
                            foreach (OpProfileData data in defaultProfile.Parameters.Where(q => dataTypes.Contains(q.IdDataType)
                                && (allowNullValues || q.Value != null)))
                            {
                                if (!profileData.Any(q => q.IdDataType == data.IdDataType))
                                    profileData.Add(data);
                            }
                        }
                    }
                    else
                    {
                        OpProfile profile = profileList.FirstOrDefault();
                        if (profile != null)
                        {
                            foreach (OpProfileData data in profile.Parameters.Where(q => dataTypes.Contains(q.IdDataType)
                                && (allowNullValues || q.Value != null)))
                            {
                                profileData.Add(data);
                            }
                        }
                    }
                }
            }

            return profileData;
        }
        #endregion

        #region GetMergedMeterProfileDataValue
        public static bool GetMergedMeterProfileDataValue<T>(DataProvider dataProvider, long idMeter, long? idLocation, int? idDistributor, long idDataType, out T value, List<Enums.Module> modules = null)
        {
            return GetMergedMeterProfileDataValue(dataProvider, idMeter, idLocation, idDistributor, idDataType, null, out value, modules);
        }
        public static bool GetMergedMeterProfileDataValue<T>(DataProvider dataProvider, long idMeter, long? idLocation, int? idDistributor, long idDataType, int? index, out T value, List<Enums.Module> modules = null)
        {
            return GetMergedObjectProfileDataValueMethod(dataProvider, idMeter, idLocation, idDistributor, idDataType, index, out value, modules);
        }
        #endregion
        #region GetMergedLocationProfileDataValue
        public static bool GetMergedLocationProfileDataValue<T>(DataProvider dataProvider, long idLocation, int? idDistributor, long idDataType, out T value, List<Enums.Module> modules = null)
        {
            return GetMergedLocationProfileDataValue(dataProvider, idLocation, idDistributor, idDataType, null, out value, modules);
        }
        public static bool GetMergedLocationProfileDataValue<T>(DataProvider dataProvider, long idLocation, int? idDistributor, long idDataType, int? index, out T value, List<Enums.Module> modules = null)
        {
            return GetMergedObjectProfileDataValueMethod(dataProvider, null, idLocation, idDistributor, idDataType, index, out value, modules);
        }
        #endregion
        #region GetMergedDistributorProfileDataValue
        public static bool GetMergedDistributorProfileDataValue<T>(DataProvider dataProvider, int idDistributor, long idDataType, out T value, List<Enums.Module> modules = null)
        {
            return GetMergedDistributorProfileDataValue(dataProvider, idDistributor, idDataType, null, out value, modules);
        }
        public static bool GetMergedDistributorProfileDataValue<T>(DataProvider dataProvider, int idDistributor, long idDataType, int? index, out T value, List<Enums.Module> modules = null)
        {
            return GetMergedObjectProfileDataValueMethod(dataProvider, null, null, idDistributor, idDataType, index, out value, modules);
        }
        #endregion
        #region GetMergedObjectProfileDataValueMethod
        private static bool GetMergedObjectProfileDataValueMethod<T>(DataProvider dataProvider, long? idMeter, long? idLocation, int? idDistributor, long idDataType, int? index, out T value, List<Enums.Module> modules = null)
        {
            Dictionary<int, object> dataDict = GetMergedObjectsProfileDataMethod(dataProvider, new List<OpProfileComponentData>() { new OpProfileComponentData(idMeter, idLocation, idDistributor, idDataType, index, null) { Modules = modules } }).GroupBy(d => d.Index.Value).ToDictionary(d => d.Key, d => d.First().Value);
            value = default(T);

            if (dataDict.Count == 0)
                return false;
            object tempValue;
            if (index == null)
                tempValue = dataDict.MinBy(d => d.Key).Value;
            else
            {
                if (!dataDict.ContainsKey(index.Value))
                    return false;
                tempValue = dataDict[index.Value];
            }

            if (tempValue == null)
                return true;

            Type t = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);

            value = (T)Convert.ChangeType(tempValue, t);
            return true;
        }
        #endregion

        #region GetMergedMeterProfileDataValueOrDefault
        public static T GetMergedMeterProfileDataValueOrDefault<T>(DataProvider dataProvider, long idMeter, long? idLocation, int? idDistributor, long idDataType, List<Enums.Module> modules = null)
        {
            return GetMergedMeterProfileDataValueOrDefault(dataProvider, idMeter, idLocation, idDistributor, idDataType, default(T), modules);
        }
        public static T GetMergedMeterProfileDataValueOrDefault<T>(DataProvider dataProvider, long idMeter, long? idLocation, int? idDistributor, long idDataType, T defaultValue, List<Enums.Module> modules = null)
        {
            return GetMergedMeterProfileDataValueOrDefault(dataProvider, idMeter, idLocation, idDistributor, idDataType, null, defaultValue, modules);
        }
        public static T GetMergedMeterProfileDataValueOrDefault<T>(DataProvider dataProvider, long idMeter, long? idLocation, int? idDistributor, long idDataType, int? index, T defaultValue, List<Enums.Module> modules = null)
        {
            return GetMergedObjectProfileDataValueOrDefaultMethod(dataProvider, idMeter, idLocation, idDistributor, idDataType, index, defaultValue, modules);
        }
        #endregion
        #region GetMergedLocationProfileDataValueOrDefault
        public static T GetMergedLocationProfileDataValueOrDefault<T>(DataProvider dataProvider, long idLocation, int? idDistributor, long idDataType, List<Enums.Module> modules = null)
        {
            return GetMergedLocationProfileDataValueOrDefault(dataProvider, idLocation, idDistributor, idDataType, default(T), modules);
        }
        public static T GetMergedLocationProfileDataValueOrDefault<T>(DataProvider dataProvider, long idLocation, int? idDistributor, long idDataType, T defaultValue, List<Enums.Module> modules = null)
        {
            return GetMergedLocationProfileDataValueOrDefault(dataProvider, idLocation, idDistributor, idDataType, null, defaultValue, modules);
        }
        public static T GetMergedLocationProfileDataValueOrDefault<T>(DataProvider dataProvider, long idLocation, int? idDistributor, long idDataType, int? index, T defaultValue, List<Enums.Module> modules = null)
        {
            return GetMergedObjectProfileDataValueOrDefaultMethod(dataProvider, null, idLocation, idDistributor, idDataType, index, defaultValue, modules);
        }
        #endregion
        #region GetMergedDistributorProfileDataValueOrDefault
        public static T GetMergedDistributorProfileDataValueOrDefault<T>(DataProvider dataProvider, int idDistributor, long idDataType, List<Enums.Module> modules = null)
        {
            return GetMergedDistributorProfileDataValueOrDefault(dataProvider, idDistributor, idDataType, default(T), modules);
        }
        public static T GetMergedDistributorProfileDataValueOrDefault<T>(DataProvider dataProvider, int idDistributor, long idDataType, T defaultValue, List<Enums.Module> modules = null)
        {
            return GetMergedDistributorProfileDataValueOrDefault(dataProvider, idDistributor, idDataType, null, defaultValue, modules);
        }
        public static T GetMergedDistributorProfileDataValueOrDefault<T>(DataProvider dataProvider, int idDistributor, long idDataType, int? index, T defaultValue, List<Enums.Module> modules = null)
        {
            return GetMergedObjectProfileDataValueOrDefaultMethod(dataProvider, null, null, idDistributor, idDataType, index, defaultValue, modules);
        }
        #endregion
        #region GetMergedObjectProfileDataValueOrDefaultMethod
        private static T GetMergedObjectProfileDataValueOrDefaultMethod<T>(DataProvider dataProvider, long? idMeter, long? idLocation, int? idDistributor, long idDataType, int? index, T defaultValue, List<Enums.Module> modules = null)
        {
            Dictionary<int, object> dataDict = GetMergedObjectsProfileDataMethod(dataProvider, new List<OpProfileComponentData>() { new OpProfileComponentData(idMeter, idLocation, idDistributor, idDataType, index, null, defaultValue) { Modules = modules } }).GroupBy(d => d.Index.Value).ToDictionary(d => d.Key, d => d.First().Value);

            if (dataDict.Count == 0)
                return defaultValue;
            object value;
            if (index == null)
                value = dataDict.MinBy(d => d.Key).Value;
            else
            {
                if (!dataDict.ContainsKey(index.Value))
                    return defaultValue;
                value = dataDict[index.Value];
            }

            Type t = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);

            return value != null ? (T)Convert.ChangeType(value, t) : defaultValue;
        }
        #endregion

        #region GetMergedMeterProfileData
        public static Dictionary<int, object> GetMergedMeterProfileData(DataProvider dataProvider, long idMeter, long? idLocation, int? idDistributor, long idDataType, List<Enums.Module> modules = null)
        {
            return GetMergedObjectsProfileDataMethod(dataProvider, new List<OpProfileComponentData>() { new OpProfileComponentData(idMeter, idLocation, idDistributor, idDataType, null, null) { Modules = modules } }).GroupBy(d => d.Index.Value).ToDictionary(d => d.Key, d => d.First().Value);
        }
        #endregion
        #region GetMergedLocationProfileData
        public static Dictionary<int, object> GetMergedLocationProfileData(DataProvider dataProvider, long idLocation, int? idDistributor, long idDataType, List<Enums.Module> modules = null)
        {
            return GetMergedObjectsProfileDataMethod(dataProvider, new List<OpProfileComponentData>() { new OpProfileComponentData(null, idLocation, idDistributor, idDataType, null, null) { Modules = modules } }).GroupBy(d => d.Index.Value).ToDictionary(d => d.Key, d => d.First().Value);
        }
        #endregion
        #region GetMergedDistributorProfileData
        public static Dictionary<int, object> GetMergedDistributorProfileData(DataProvider dataProvider, int idDistributor, long idDataType, List<Enums.Module> modules = null)
        {
            return GetMergedObjectsProfileDataMethod(dataProvider, new List<OpProfileComponentData>() { new OpProfileComponentData(null, null, idDistributor, idDataType, null, null) { Modules = modules } }).GroupBy(d => d.Index.Value).ToDictionary(d => d.Key, d => d.First().Value);
        }
        #endregion
        #region GetMergedObjectsProfileDataMethod
        public static List<OpProfileComponentData> GetMergedObjectsProfileDataMethod(DataProvider dataProvider, List<OpProfileComponentData> dataList)
        {
            long[] idMeters = dataList.Where(d => d.IdMeter != null).Select(d => d.IdMeter.Value).Distinct().ToArray();
            long[] idLocations = dataList.Where(d => d.IdLocation != null).Select(d => d.IdLocation.Value).Distinct().ToArray();
            int[] idDistributors = dataList.Where(d => d.IdDistributor != null).Select(d => d.IdDistributor.Value).Distinct().ToArray();
            long[] idDataTypes = dataList.Select(d => d.IdDataType).Distinct().ToArray();

            Dictionary<long, Dictionary<long, Dictionary<int, object>>> idMeterDataFromMeter = null;
            Dictionary<OpProfileComponentData, Dictionary<int, object>> idMeterProfileDataFromMeter = null;
            Dictionary<long, Dictionary<long, Dictionary<int, object>>> idLocationDataFromLocation = null;
            Dictionary<OpProfileComponentData, Dictionary<int, object>> idLocationProfileDataFromLocation = null;
            Dictionary<int, Dictionary<long, Dictionary<int, object>>> idDistributorDataFromDistributor = null;
            Dictionary<OpProfileComponentData, Dictionary<int, object>> idDistributorProfileDataFromDistributor = null;

            if (idMeters.Length > 0)
            {
                idMeterDataFromMeter = dataProvider.GetDataFilter(IdMeter: idMeters, IdDataType: idDataTypes, loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).GroupBy(d => d.IdMeter.Value).ToDictionary(d => d.Key, d => d.GroupBy(t => t.IdDataType).ToDictionary(t => t.Key, t => t.Distinct(v => v.Index).ToDictionary(v => v.Index, v => v.Value)));
            }
            if (idLocations.Length > 0)
            {
                idLocationDataFromLocation = dataProvider.GetDataFilter(IdLocation: idLocations, IdDataType: idDataTypes, loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).GroupBy(d => d.IdLocation.Value).ToDictionary(d => d.Key, d => d.GroupBy(t => t.IdDataType).ToDictionary(t => t.Key, t => t.Distinct(v => v.Index).ToDictionary(v => v.Index, v => v.Value)));
            }
            if (idDistributors.Length > 0)
            {
                idDistributorDataFromDistributor = dataProvider.GetDistributorDataFilter(IdDistributor: idDistributors, IdDataType: idDataTypes, loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).GroupBy(d => d.IdDistributor).ToDictionary(d => d.Key, d => d.GroupBy(t => t.IdDataType).ToDictionary(t => t.Key, t => t.Distinct(v => v.Index).ToDictionary(v => v.Index, v => v.Value)));
            }
            if (idMeters.Length > 0 || idLocations.Length > 0 || idDistributors.Length > 0)
            {
                GetProfileDataForProfileComponentData(dataProvider, dataList, out idMeterProfileDataFromMeter, out idLocationProfileDataFromLocation, out idDistributorProfileDataFromDistributor);
            }

            List<OpProfileComponentData> result = new List<OpProfileComponentData>();

            foreach (OpProfileComponentData data in dataList)
            {
                Dictionary<int, object> currentResult = MethodGetMergedObjectProfileDataSingleDataType(data.IdMeter, data.IdLocation, data.IdDistributor
                    , data.IdMeter != null && idMeterDataFromMeter.ContainsKey(data.IdMeter.Value) && idMeterDataFromMeter[data.IdMeter.Value].ContainsKey(data.IdDataType) ? idMeterDataFromMeter[data.IdMeter.Value][data.IdDataType] : null
                    , idMeterProfileDataFromMeter != null && data.IdMeter != null && idMeterProfileDataFromMeter.ContainsKey(data) ? idMeterProfileDataFromMeter[data] : null
                    , data.IdLocation != null && idLocationDataFromLocation.ContainsKey(data.IdLocation.Value) && idLocationDataFromLocation[data.IdLocation.Value].ContainsKey(data.IdDataType) ? idLocationDataFromLocation[data.IdLocation.Value][data.IdDataType] : null
                    , idLocationProfileDataFromLocation != null && data.IdLocation != null && idLocationProfileDataFromLocation.ContainsKey(data) ? idLocationProfileDataFromLocation[data] : null
                    , data.IdDistributor != null && idDistributorDataFromDistributor.ContainsKey(data.IdDistributor.Value) && idDistributorDataFromDistributor[data.IdDistributor.Value].ContainsKey(data.IdDataType) ? idDistributorDataFromDistributor[data.IdDistributor.Value][data.IdDataType] : null
                    , idDistributorProfileDataFromDistributor != null && idDistributorProfileDataFromDistributor.ContainsKey(data) ? idDistributorProfileDataFromDistributor[data] : null);
                if (currentResult != null)
                    result.AddRange(currentResult.Select(r => new OpProfileComponentData(data.IdMeter, data.IdLocation, data.IdDistributor, data.IdDataType, r.Key, r.Value) { Modules = data.Modules != null ? data.Modules.ToList() : null}));
                else
                    result.Add(new OpProfileComponentData(data.IdMeter, data.IdLocation, data.IdDistributor, data.IdDataType, data.Index ?? 0, data.DefaultValue) { Modules = data.Modules != null ? data.Modules.ToList() : null });
            }
            return result;
        }
        #endregion
        #region GetProfileDataForProfileComponentData
        public static bool GetProfileDataForProfileComponentData(DataProvider dataProvider, List<OpProfileComponentData> componentDatas
                                                                  , out Dictionary<OpProfileComponentData, Dictionary<int, object>> indexProfileDataFromMeter
                                                                  , out Dictionary<OpProfileComponentData, Dictionary<int, object>> indexProfileDataFromLocation
                                                                  , out Dictionary<OpProfileComponentData, Dictionary<int, object>> indexProfileDataFromDistributor)
        {
            indexProfileDataFromMeter = new Dictionary<OpProfileComponentData, Dictionary<int, object>>();
            indexProfileDataFromLocation = new Dictionary<OpProfileComponentData, Dictionary<int, object>>();
            indexProfileDataFromDistributor = new Dictionary<OpProfileComponentData, Dictionary<int, object>>();

            if (componentDatas == null || componentDatas.Count == 0)
                return false;
            List<OpProfileComponentData> dataList = componentDatas.Distinct().ToList();
            long[] idMeters = dataList.Where(d => d.IdMeter != null).Select(d => d.IdMeter.Value).Distinct().ToArray();
            long[] idLocations = dataList.Where(d => d.IdLocation != null).Select(d => d.IdLocation.Value).Distinct().ToArray();
            int[] idDistributors = dataList.Where(d => d.IdDistributor != null).Select(d => d.IdDistributor.Value).Distinct().ToArray();
            long[] idDataTypes = dataList.Select(d => d.IdDataType).Distinct().ToArray();

            Dictionary<long, Dictionary<int, int>> meterProfilesDict = new Dictionary<long, Dictionary<int, int>>();
            Dictionary<long, Dictionary<int, int>> locationProfilesDict = new Dictionary<long, Dictionary<int, int>>();
            Dictionary<int, Dictionary<int, int>> distributorProfilesDict = new Dictionary<int, Dictionary<int, int>>();
            List<int> allProfiles = new List<int>();

            if (idMeters.Length > 0)
            {
                meterProfilesDict = dataProvider.GetDataFilter(IdMeter: idMeters, IdDataType: new long[] { DataType.PROFILE_ID }, loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).Where(d => d.Value != null).GroupBy(d => d.IdMeter.Value).ToDictionary(d => d.Key, d => d.GroupBy(v => v.Index).ToDictionary(v => v.Key, v => Convert.ToInt32(v.First().Value)));
                allProfiles.AddRange(meterProfilesDict.SelectMany(m => m.Value.Select(v => v.Value)));
            }
            if (idLocations.Length > 0)
            {
                locationProfilesDict = dataProvider.GetDataFilter(IdLocation: idLocations, IdDataType: new long[] { DataType.PROFILE_ID }, loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).Where(d => d.Value != null).GroupBy(d => d.IdLocation.Value).ToDictionary(d => d.Key, d => d.GroupBy(v => v.Index).ToDictionary(v => v.Key, v => Convert.ToInt32(v.First().Value)));
                allProfiles.AddRange(locationProfilesDict.SelectMany(m => m.Value.Select(v => v.Value)));
            }
            if (idDistributors.Length > 0)
            {
                distributorProfilesDict = dataProvider.GetDistributorDataFilter(IdDistributor: idDistributors, IdDataType: new long[] { DataType.PROFILE_ID }, loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).Where(d => d.Value != null).GroupBy(d => d.IdDistributor).ToDictionary(d => d.Key, d => d.GroupBy(v => v.Index).ToDictionary(v => v.Key, v => Convert.ToInt32(v.First().Value)));
                allProfiles.AddRange(distributorProfilesDict.SelectMany(m => m.Value.Select(v => v.Value)));
            }

            Dictionary<int, int?> profileModuleDict = dataProvider.GetProfileFilter(IdProfile: allProfiles.Distinct().ToArray(), loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).ToDictionary(p => p.IdProfile, p => p.IdModule);
            Dictionary<long, Dictionary<int, List<Tuple<int, object>>>> allProfileData = dataProvider.GetProfileDataFilter(IdProfile: allProfiles.Distinct().ToArray(), IdDataType: idDataTypes, loadNavigationProperties: false, commandTimeout: dataProvider.dbConnectionCore.LongTimeout, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted).GroupBy(d => d.IdDataType).ToDictionary(d => d.Key, d => d.GroupBy(p => p.IdProfile).ToDictionary(p => p.Key, p => p.Select(v => new Tuple<int, object>(v.IndexNbr, v.Value)).ToList()));

            foreach (OpProfileComponentData data in componentDatas)
            {
                if (!allProfileData.ContainsKey(data.IdDataType))
                    continue;
                if (data.IdMeter != null && meterProfilesDict.ContainsKey(data.IdMeter.Value))
                {
                    Dictionary<int, int> indexProfileId = meterProfilesDict[data.IdMeter.Value].Where(p => profileModuleDict.ContainsKey(p.Value) && (profileModuleDict[p.Value] == null || (data.Modules != null && data.Modules.Contains((Enums.Module)profileModuleDict[p.Value])))).ToDictionary(p => p.Key, p => p.Value);
                    int[] currentProfiles = indexProfileId.Select(d => d.Value).Distinct().ToArray();
                    Dictionary<int, List<Tuple<int, object>>> profileData = allProfileData[data.IdDataType].Where(p => currentProfiles.Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value);
                    indexProfileDataFromMeter.Add(data, MethodGetObjectProfileDataSingleDataType(indexProfileId, profileData));
                }
                if (data.IdLocation != null && locationProfilesDict.ContainsKey(data.IdLocation.Value))
                {
                    Dictionary<int, int> indexProfileId = locationProfilesDict[data.IdLocation.Value].Where(p => profileModuleDict.ContainsKey(p.Value) && (profileModuleDict[p.Value] == null || (data.Modules != null && data.Modules.Contains((Enums.Module)profileModuleDict[p.Value])))).ToDictionary(p => p.Key, p => p.Value);
                    int[] currentProfiles = indexProfileId.Select(d => d.Value).Distinct().ToArray();
                    Dictionary<int, List<Tuple<int, object>>> profileData = allProfileData[data.IdDataType].Where(p => currentProfiles.Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value);
                    indexProfileDataFromLocation.Add(data, MethodGetObjectProfileDataSingleDataType(indexProfileId, profileData));
                }
                if (data.IdDistributor != null && distributorProfilesDict.ContainsKey(data.IdDistributor.Value))
                {
                    Dictionary<int, int> indexProfileId = distributorProfilesDict[data.IdDistributor.Value].Where(p => profileModuleDict.ContainsKey(p.Value) && (profileModuleDict[p.Value] == null || (data.Modules != null && data.Modules.Contains((Enums.Module)profileModuleDict[p.Value])))).ToDictionary(p => p.Key, p => p.Value);
                    int[] currentProfiles = indexProfileId.Select(d => d.Value).Distinct().ToArray();
                    Dictionary<int, List<Tuple<int, object>>> profileData = allProfileData[data.IdDataType].Where(p => currentProfiles.Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value);
                    indexProfileDataFromDistributor.Add(data, MethodGetObjectProfileDataSingleDataType(indexProfileId, profileData));
                }
            }

            return true;
        }
        #endregion

        #region MethodGetMergedObjectProfileDataSingleDataType
        /// <summary>
        /// Method used to calculate merged data from object data and profile data for object, single data type only. All data is calculated from different data from different layers
        /// </summary>
        /// <param name="idMeter">Id meter</param>
        /// <param name="idLocation">Id location</param>
        /// <param name="idDistributor">Id distributor</param>
        /// <param name="indexDataFromMeter">Dictionary with meter data: Key is IndexNbr of DataValue, Value is Value of DataValue</param>
        /// <param name="indexProfileDataFromMeter">Dictionary with meter profile data: Key is IndexNbr of DataValue, Value is Value of DataValue</param>
        /// <param name="indexDataFromLocation">Dictionary with location data: Key is IndexNbr of DataValue, Value is Value of DataValue</param>
        /// <param name="indexProfileDataFromLocation">Dictionary with location profile data: Key is IndexNbr of DataValue, Value is Value of DataValue</param>
        /// <param name="indexDataFromDistributor">Dictionary with distributor data: Key is IndexNbr of DataValue, Value is Value of DataValue</param>
        /// <param name="indexProfileDataFromDistributor">Dictionary with distributor profile data: Key is IndexNbr of DataValue, Value is Value of DataValue</param>
        /// <returns>Dictionary with calculated merged object and profile data for object: Key is IndexNbr of DataValue, Value is Value of DataValue</returns>
        public static Dictionary<int, object> MethodGetMergedObjectProfileDataSingleDataType(long? idMeter, long? idLocation, int? idDistributor
            , Dictionary<int, object> indexDataFromMeter, Dictionary<int, object> indexProfileDataFromMeter
            , Dictionary<int, object> indexDataFromLocation, Dictionary<int, object> indexProfileDataFromLocation
            , Dictionary<int, object> indexDataFromDistributor, Dictionary<int, object> indexProfileDataFromDistributor)
        {
            if (idMeter == null && idLocation == null && idDistributor == null)
                return null;

            List<Tuple<long, int, object>> allDataDict = new List<Tuple<long, int, object>>();
            long tempId = 1;
            if (idMeter != null && indexDataFromMeter != null && indexDataFromMeter.Count > 0)
                allDataDict.AddRange(indexDataFromMeter.Select(p => new Tuple<long, int, object>(tempId++, p.Key, p.Value)));
            if (idMeter != null && indexProfileDataFromMeter != null && indexProfileDataFromMeter.Count > 0)
                allDataDict.AddRange(indexProfileDataFromMeter.Select(p => new Tuple<long, int, object>(tempId++, p.Key, p.Value)));
            if (idLocation != null && indexDataFromLocation != null && indexDataFromLocation.Count > 0)
                allDataDict.AddRange(indexDataFromLocation.Select(p => new Tuple<long, int, object>(tempId++, p.Key, p.Value)));
            if (idLocation != null && indexProfileDataFromLocation != null && indexProfileDataFromLocation.Count > 0)
                allDataDict.AddRange(indexProfileDataFromLocation.Select(p => new Tuple<long, int, object>(tempId++, p.Key, p.Value)));
            if (idDistributor != null && indexDataFromDistributor != null && indexDataFromDistributor.Count > 0)
                allDataDict.AddRange(indexDataFromDistributor.Select(p => new Tuple<long, int, object>(tempId++, p.Key, p.Value)));
            if (idDistributor != null && indexProfileDataFromDistributor != null && indexProfileDataFromDistributor.Count > 0)
                allDataDict.AddRange(indexProfileDataFromDistributor.Select(p => new Tuple<long, int, object>(tempId++, p.Key, p.Value)));

            Dictionary<int, object> result = allDataDict.GroupBy(a => a.Item2).ToDictionary(a => a.Key, a => a.MinBy(b => b.Item1).Item3);
            return result;
        }
        #endregion
        #region MethodGetObjectProfileDataSingleDataType
        /// <summary>
        /// Method used to calculate profile data for object, single data type only. Profile data is calculated from different profiles from different indexes
        /// </summary>
        /// <param name="objectIndexProfileId">Dictionary with IdProfile for object: Key is IndexNbr of profile, Value is IdProfile</param>
        /// <param name="profileData">Dictionary with ProfileData for profiles: Key is IdProfile, Value is List of Tuple, where Item1 is IndexNbr of DataValue and Item2 is Value of DataValue</param>
        /// <returns>Dictionary with calculated profile data for object: Key is IndexNbr of DataValue, Value is Value of DataValue</returns>
        public static Dictionary<int, object> MethodGetObjectProfileDataSingleDataType(Dictionary<int, int> objectIndexProfileId, Dictionary<int, List<Tuple<int, object>>> profileData)
        {
            if (objectIndexProfileId == null || profileData == null)
                return null;

            Dictionary<int, object> result = objectIndexProfileId.Where(p => profileData.ContainsKey(p.Value)).OrderBy(p => p.Key).SelectMany(p => profileData[p.Value].Select(pd => new { IdPriority = p.Key, IndexNbr = pd.Item1, Value = pd.Item2 }))
                .GroupBy(a => a.IndexNbr).ToDictionary(a => a.Key, a => a.MinBy(b => b.IdPriority).Value);

            return result;
        }
        #endregion
    }
}
