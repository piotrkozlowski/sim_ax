﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using System.Collections;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ConsumerComponent
    {
        #region Methods
        public static OpConsumer GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetConsumer(Id);
        }

        public static List<OpConsumer> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllConsumer();
        }

        public static OpConsumer Save(DataProvider dataProvider, OpConsumer objectToSave)
        {
            dataProvider.SaveConsumer(objectToSave);
            objectToSave.DataList.AssignReferences(dataProvider);

            OpConsumerData data = null;
            for (int i = 0; i < objectToSave.DataList.Count; i++)
            {
                data = objectToSave.DataList[i];
                data.Consumer = objectToSave;
                //zapis do bazy tylko tych ktore sa IsEditable na true lub jesli obiekt ktory dodajemy jest nowym obiektem wiec dodajemy dane po raz pierwszy
				if (data.OpState == OpChangeState.Modified || data.OpState == OpChangeState.New)
                {
                    objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                }

				if (data.OpState == OpChangeState.Delete)
                {
                    DataComponent.Delete(dataProvider, objectToSave.DataList[i]);
                }
            }

            return dataProvider.GetConsumer(objectToSave.IdConsumer);
        }
        /*
        public static void AddConsumer(DataProvider dataProvider, OpConsumer insert)
        {
            if (!dataProvider.Consumer.Contains(insert))
            {
                dataProvider.Consumer.Add(insert);
            }
        }*/

        public static void Delete(DataProvider dataProvider, OpConsumer objectToDelete)
        {
            dataProvider.DeleteConsumer(objectToDelete);
        }

        public static List<OpConsumer> GetByTagName(DataProvider dataProvider, string TagNo)
        {
            return dataProvider.GetAllConsumer().FindAll(n => n.DT_CONSUMER_TAG_NO != null && n.DT_CONSUMER_TAG_NO == TagNo);   
        }

        public static int GetLastContactIndex(OpConsumer consumer)
        {
            int ret = 0;
            foreach (var loop in consumer.DataList)
            {
                if (loop.IdDataType == DataType.CONSUMER_MESSAGE_BODY)
                    ret = Math.Max(ret, loop.Index);
            }
            return ret;
        }

        public static void LoadAllData(DataProvider dataProvider,  OpConsumer consumer)
        {
            consumer.DataList.Clear();
            consumer.DataList.AddRange(dataProvider.GetConsumerDataFilter(IdConsumer: new int[] { consumer.IdConsumer }));
        }

        public static string GetCurrentLocation(List<OpLocationConsumerHistory> hist, out bool ok)
        {
            List<OpLocationConsumerHistory> filter = hist.FindAll(l => l.EndTime == null);
            if (filter.Count == 1)
            {
                ok = true;
                return filter[0].Location.ToString();
            }

            if (filter.Count == 0)
            {
                ok = true;
                return "No location has been assigned!";
            }

            if (filter.Count > 1)
            {
                ok = false;
                return String.Format("{0} locations assigned!", filter.Count);
            }

            ok = false;
            return "Internal error!";
        }

        #endregion
    }
}
