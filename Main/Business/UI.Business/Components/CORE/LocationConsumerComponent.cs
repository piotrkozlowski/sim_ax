﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LocationConsumerComponent
    {
        public static List<OpLocationConsumerHistory> GetLocationConsumer(DataProvider dataProvider, OpLocation loc)
        {
            return dataProvider.GetLocationConsumerHistoryFilter(IdLocation: new long[] {loc.IdLocation});
        }
        public static List<OpLocationConsumerHistory> GetConsumerLocations(DataProvider dataProvider, OpConsumer cons)
        {
            return dataProvider.GetLocationConsumerHistoryFilter(IdConsumer: new int[] { cons.IdConsumer });
        }

        public static OpLocation GetConsumerLocationHistoryForDate(List<OpLocationConsumerHistory> list, DateTime date)
        {
            OpLocationConsumerHistory rt = list.Find(n => n.StartTime <= date && (n.EndTime == null || n.EndTime > date));
            return (rt == null) ? null : rt.Location;
        }

        public static void RemoveConsumerFromLocation(DataProvider dataProvider, OpConsumer consumer)
        {
            foreach (var loop in GetConsumerLocations(dataProvider,consumer))
            {
                if (loop.EndTime == null)
                {
                    loop.EndTime = dataProvider.DateTimeNow;
                    dataProvider.SaveLocationConsumerHistory(loop);
                    if (loop.Location != null)
                    {
                        loop.Location.Consumer = null;
                        dataProvider.SaveLocation(loop.Location);
                    }
                }
            }
        }
    }
}
