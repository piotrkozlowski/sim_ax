﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
	public class DataFormatGroupComponent
	{
        #region Methods
        public static List<OpDataFormatGroupDetails> GetHierarchicalDataFormatGroupDetails(DataProvider dataProvider, OpOperator opOperator, Enums.Module module)
        {
            if (dataProvider == null || opOperator == null)
                return new List<OpDataFormatGroupDetails>();

            return dataProvider.GetHierarchicalDataFormatGroupDetails(opOperator.IdOperator, opOperator.IdDistributor, (int)module);
        }
        public static void DeleteDataFormatGroup(DataProvider dataProvider, OpDataFormatGroup dataFormatGroupToRemove)
        {
            if (dataProvider == null) return;

            List<OpDataFormatGroupDetails> dataTypesInGroupsToRemove = dataProvider.GetDataFormatGroupDetails(new int[] { dataFormatGroupToRemove.IdDataFormatGroup });

            foreach (OpDataFormatGroupDetails dataTypeInGroupToRemove in dataTypesInGroupsToRemove)
                dataProvider.DeleteDataFormatGroupDetails(dataTypeInGroupToRemove);

            dataProvider.DeleteDataFormatGroup(dataFormatGroupToRemove);
        }
        public static bool CheckDataTypeFormat(List<OpDataFormatGroupDetails> formatDetails, OpDataType dataType, object value, out string errorMsg)
        {
            errorMsg = String.Empty;
            List<OpDataFormatGroupDetails> dataTypeFormatDetails = formatDetails.Where(d => d.IdDataType == dataType.IdDataType && d.DataTypeFormatOut != null).ToList();
            if (dataTypeFormatDetails.Count == 0)
                return true;
            if(value == null)
            {
                if(dataTypeFormatDetails.Exists(d => d.DataTypeFormatOut.IsRequired == true))
                {
                    errorMsg = ResourcesText.ValueRequired;
                    return false;
                }
                return true;
            }
            switch (dataType.IdDataTypeClass)
            {
                case (int)Enums.DataTypeClass.Text:
                #region Text
                    foreach (OpDataFormatGroupDetails dfgdItem in dataTypeFormatDetails)
                    {
                        if(dfgdItem.DataTypeFormatOut.TextMaxLength != null && value.ToString().Length > dfgdItem.DataTypeFormatOut.TextMaxLength)
                        {
                            errorMsg = String.Format(ResourcesText.MaxPermissibleLenght, dfgdItem.DataTypeFormatOut.TextMaxLength);
                            return false;
                        }
                        if (dfgdItem.DataTypeFormatOut.TextMinLength != null && value.ToString().Length < dfgdItem.DataTypeFormatOut.TextMinLength)
                        {
                            errorMsg = String.Format(ResourcesText.MinPermissibleLenght, dfgdItem.DataTypeFormatOut.TextMinLength);
                            return false;
                        }
                    }
                    return true;
                #endregion
                case (int)Enums.DataTypeClass.Integer:
                #region Integer
                {
                    long intValue;
                    try
                    {
                        intValue = Convert.ToInt64(value);
                    }
                    catch
                    {
                        errorMsg = String.Format(ResourcesText.TheValueIsNotValidIntegerValue);
                        return false;
                    }

                    foreach (OpDataFormatGroupDetails dfgdItem in dataTypeFormatDetails)
                    {
                        // prec, scale, value
                        if (dfgdItem.DataTypeFormatOut.NumberMinValue != null && intValue < dfgdItem.DataTypeFormatOut.NumberMinValue)
                        {
                            errorMsg = String.Format(ResourcesText.MinPermissibleValue, dfgdItem.DataTypeFormatOut.NumberMinValue);
                            return false;
                        }

                        if (dfgdItem.DataTypeFormatOut.NumberMinValue != null && intValue > dfgdItem.DataTypeFormatOut.NumberMaxValue)
                        {
                            errorMsg = String.Format(ResourcesText.MaxPermissibleValue, dfgdItem.DataTypeFormatOut.NumberMinValue);
                            return false;
                        }
                    }
                    return true;
                }
                #endregion
                case (int)Enums.DataTypeClass.Real:
                #region Real
                {
                    double doubleValue;
                    string valueStr = value.ToString().Replace(',', '.');
                    try
                    {
                        doubleValue = Convert.ToDouble(valueStr, System.Globalization.CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        errorMsg = String.Format(ResourcesText.TheValueIsNotValidRealValue);
                        return false;
                    }

                    foreach (OpDataFormatGroupDetails dfgdItem in dataTypeFormatDetails)
                    {
                        // prec, scale, value
                        if (dfgdItem.DataTypeFormatOut.NumberMinValue != null && doubleValue < dfgdItem.DataTypeFormatOut.NumberMinValue)
                        {
                            errorMsg = String.Format(ResourcesText.MinPermissibleValue, dfgdItem.DataTypeFormatOut.NumberMinValue);
                            return false;
                        }

                        if (dfgdItem.DataTypeFormatOut.NumberMinValue != null && doubleValue > dfgdItem.DataTypeFormatOut.NumberMaxValue)
                        {
                            errorMsg = String.Format(ResourcesText.MaxPermissibleValue, dfgdItem.DataTypeFormatOut.NumberMinValue);
                            return false;
                        }
                    }
                    return true;
                }
                #endregion
                default:
                    return false;
            }
        }

        public static List<OpDataType> SetDataTypeUnit(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, List<OpDataType> dataTypes)
        {
            List<OpDataFormatGroupDetails> dataFormatGroupDetails = DataFormatGroupComponent.GetHierarchicalDataFormatGroupDetails((DataProvider)dataProvider, loggedOperator, module).ToList();
            Dictionary<long, OpUnit> unitDict = dataFormatGroupDetails.Distinct(x => x.ID_DATA_TYPE)
                                                                        .Where(x => x.IdUnitOut.HasValue)
                                                                        .ToDictionary(k => k.IdDataType, v => v.UnitOut == null ? dataProvider.GetUnit(v.IdUnitOut.Value) : v.UnitOut);
            if (unitDict.Count > 0)
            {
                Dictionary<long, OpDataType> dtDict = dataTypes.ToDictionary(d => d.IdDataType);
                foreach (KeyValuePair<long, OpUnit> kvItem in unitDict)
                {
                    if (kvItem.Value != null && dtDict.ContainsKey(kvItem.Key))
                        dtDict[kvItem.Key].Unit = kvItem.Value;
                }
            }
            return dataTypes;
        }

        #endregion
        
    }

}
