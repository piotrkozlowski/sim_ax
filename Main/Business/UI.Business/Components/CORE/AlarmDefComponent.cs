﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class AlarmDefComponent
    {
        #region Methods

        static Tuple<long, string>[] arrayAlarmDataTypeEnableToName = {
                new Tuple<long, string>(DataType.ALEVEL_CONFIG_HIHI_EVENT_ENABLE, "HiHi Level"),
                new Tuple<long, string>(DataType.ALEVEL_CONFIG_HI_EVENT_ENABLE, "Hi Level"),
                new Tuple<long, string>(DataType.ALEVEL_CONFIG_LO_EVENT_ENABLE, "Lo Level"),
                new Tuple<long, string>(DataType.ALEVEL_CONFIG_LOLO_EVENT_ENABLE, "LoLo Level"),
                new Tuple<long, string>(DataType.METER_RAPID_ACTIVE, "Rapid Level"),
                //--
                new Tuple<long, string>(DataType.METER_LO_UP_EVENT_ENABLE, "Low pressure" ),
                new Tuple<long, string>(DataType.METER_LO_DOWN_EVENT_ENABLE, "Low pressure to normal" ),
                new Tuple<long, string>(DataType.METER_LOLO_UP_EVENT_ENABLE, "LowLow pressure" ),
                new Tuple<long, string>(DataType.METER_LOLO_DOWN_EVENT_ENABLE, "LowLow pressure to normal" ),

                new Tuple<long, string>(DataType.METER_HI_UP_EVENT_ENABLE, "High pressure" ),
                new Tuple<long, string>(DataType.METER_HI_DOWN_EVENT_ENABLE, "High pressure to normal" ),
                new Tuple<long, string>(DataType.METER_HIHI_UP_EVENT_ENABLE, "HighHigh pressure" ),
                new Tuple<long, string>(DataType.METER_HIHI_DOWN_EVENT_ENABLE, "HighHigh pressure to normal"),

                new Tuple<long, string>(DataType.METER_STEPREL_EVENT_ENABLE, "In alarm range, significant change detected"),
                new Tuple<long, string>(DataType.METER_NVR_UP_EVENT_ENABLE, "Out of range"),
                new Tuple<long, string>(DataType.METER_NVR_DOWN_EVENT_ENABLE, "Out of range return to normal"),

                new Tuple<long, string>(DataType.METER_UDI_UP_EVENT_ENABLE, "Alarm start"),
                new Tuple<long, string>(DataType.METER_UDI_DOWN_EVENT_ENABLE, "Alarm stop"),
        };

        public static string FormatAlarmDefName(string CID, string MeterSerialNbr, long AlarmlevelEnable)
        {
            

            string level="";
            Tuple<long, string> AlarmDataTypeEnableToName = arrayAlarmDataTypeEnableToName.Where(w => w.Item1 == AlarmlevelEnable).FirstOrDefault();
            if (AlarmDataTypeEnableToName != null)
                level = AlarmDataTypeEnableToName.Item2;
            return string.Format("({0})({1}) {2}", CID, MeterSerialNbr, level);

            //switch(  AlarmlevelEnable){
            //    case DataType.ALEVEL_CONFIG_HIHI_EVENT_ENABLE:
            //        level = "HiHi";
            //        break;
            //    case DataType.ALEVEL_CONFIG_HI_EVENT_ENABLE:
            //        level = "Hi";
            //        break;
            //    case DataType.ALEVEL_CONFIG_LO_EVENT_ENABLE:
            //        level = "Lo";
            //        break;
            //    case DataType.ALEVEL_CONFIG_LOLO_EVENT_ENABLE:
            //        level = "LoLo";
            //        break;
            //    case DataType.METER_RAPID_ACTIVE:
            //        level = "Rapid";
            //        break;
            //    /*--------------------*/
            //    case DataType.METER_LO_UP_EVENT_ENABLE:
            //        level = "Low pressure";
            //        break;
            //    case DataType.METER_LO_DOWN_EVENT_ENABLE:
            //        level = "Low pressure to normal";
            //        break;
            //    case DataType.METER_LOLO_UP_EVENT_ENABLE:
            //        level = "LowLow pressure";
            //        break;
            //    case DataType.METER_LOLO_DOWN_EVENT_ENABLE:
            //        level = "LowLow pressure to normal";
            //        break;
            //    case DataType.METER_HI_UP_EVENT_ENABLE:
            //        level = "High pressure";
            //        break;
            //    case DataType.METER_HI_DOWN_EVENT_ENABLE:
            //        level = "High pressure to normal";
            //        break;
            //    case DataType.METER_HIHI_UP_EVENT_ENABLE:
            //        level = "HighHigh pressure";
            //        break;
            //    case DataType.METER_HIHI_DOWN_EVENT_ENABLE:
            //        level = "HighHigh pressure to normal";
            //        break;
            //    case DataType.METER_STEPREL_EVENT_ENABLE:
            //        level = "In alarm range, significant change detected";
            //        break;
            //    case DataType.METER_NVR_UP_EVENT_ENABLE:
            //        level = "Out of range";
            //        break;
            //    case DataType.METER_NVR_DOWN_EVENT_ENABLE:
            //        level = "Out of range return to normal";
            //        break;
            //    case DataType.METER_UDI_UP_EVENT_ENABLE:
            //        level = "Alarm start";
            //        break;
            //    case DataType.METER_UDI_DOWN_EVENT_ENABLE:
            //        level = "Alarm stop";
            //        break;
            //}
            // return string.Format("({0})({1}) {2} ", CID, MeterSerialNbr, level); 
        }

        public static string GetAlarmDefLevelStringFromName(string name) { 

            //if( name.Contains( " HiHi ")) return "HiHi";
            //if( name.Contains( " Hi ")) return "Hi";
            //if( name.Contains( " Lo ")) return "Lo";
            //if( name.Contains( " LoLo ")) return "LoLo";
            //if( name.Contains( " Rapid ")) return "Rapid";

            if( !string.IsNullOrEmpty( name ) )
                return name.Substring(name.LastIndexOf(')')+1).TrimEnd().TrimStart();

            return null;
        }


        public static long GetAlarmEnableIdDataTypeFromName(string name)
        {
            long retDataTypeEnable = 0;

            if (!string.IsNullOrEmpty(name))
            {
                string AlarmDefLevelString = GetAlarmDefLevelStringFromName(name);
                Tuple<long, string> AlarmDataTypeEnableToName = arrayAlarmDataTypeEnableToName.Where(w => w.Item2 == AlarmDefLevelString).FirstOrDefault();
                if (AlarmDataTypeEnableToName != null)
                    retDataTypeEnable = AlarmDataTypeEnableToName.Item1;
            }

            return retDataTypeEnable;
        }

      
        public static OpAlarmDef GetByID(DataProvider dataProvider, int Id)
        {

            return GetByID(dataProvider, Id, false);
        }

        public static OpAlarmDef GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetAlarmDef(Id, queryDatabase);
        }

        public static List<OpAlarmDef> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllAlarmDef();
        }
       
        public static void Delete(DataProvider dataProvider, OpAlarmGroup objectToDelete)
        {
            List<OpAlarmGroupOperator> opOperators = new List<OpAlarmGroupOperator>();
            opOperators = dataProvider.GetAlarmGroupOperatorFilter(IdAlarmGroup: new int[] {objectToDelete.IdAlarmGroup});

            foreach (OpAlarmGroupOperator o in opOperators)
            {
                dataProvider.DeleteAlarmGroupOperator(o);
            }
            dataProvider.DeleteAlarmGroup(objectToDelete);
        }
        
        public static void LoadAllData(DataProvider dataProvider, OpMeter meter)
        {
            meter.DataList.Clear();
            meter.DataList.AddRange(dataProvider.GetDataFilter(IdMeter: new long[] { meter.IdMeter }, IdDataType: new long[0]));
        }

        class AlalrmDefParameters
        {
            public long idDatatypeAlarm;
            public long alarmlevel;
            public long alarmenable;
            public long profiletext;
            public int alarmtype;
            public bool checkLastAlarm;
        }

        private static List<OpAlarmDef> CreateAlarmDefForLPGA(List<OpProfileData> opProfileData, OpMeter opMeter)
        {
            List<OpAlarmDef> opAlarmDef = new List<OpAlarmDef>();
            List<AlalrmDefParameters> AlarmList = new List<AlalrmDefParameters>();
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_HIHI_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_HIHI_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_LPG_HIHI_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_HI_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_HI_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_LPG_HI_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_LO_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_LO_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_LPG_LO_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueSmaller,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_LOLO_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_LOLO_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_LPG_LOLO_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueSmaller,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_LOLO_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_LOLO_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_LPG_LOLO_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueSmaller,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.METER_RAPID_TRIGGER_LEVEL,
                alarmenable = DataType.METER_RAPID_ACTIVE,
                profiletext = DataType.PROFILE_PORTAL_LPG_LEAKAGE_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });


            foreach (AlalrmDefParameters alarm in AlarmList)
            {
                OpData alarmLevelData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmlevel).FirstOrDefault();
                OpData alarmLevelEnableData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmenable).FirstOrDefault();
                OpProfileData alarmProfileTextData = opProfileData.Where(k => k.IdDataType == alarm.profiletext).FirstOrDefault();

                object alarmValue;
                if (alarm.alarmlevel == DataType.METER_RAPID_TRIGGER_LEVEL) alarmValue = true;
                else alarmValue = alarmLevelData != null && alarmLevelData.Value != null ? Convert.ToDouble(alarmLevelData.Value) : 0;

                OpAlarmDef opAlarmdef = new OpAlarmDef()
                {
                    Name = AlarmDefComponent.FormatAlarmDefName(opMeter.CurrentLocation.CID, opMeter.MeterSerialNbr, alarm.alarmenable),
                    IdMeter = opMeter.IdMeter,
                    IdAlarmType = alarm.alarmtype,
                    IdDataTypeAlarm = alarm.alarmlevel == DataType.METER_RAPID_TRIGGER_LEVEL ? DataType.DEVICE_EVENT_RAPID_UP_EVENT : DataType.GAS_LEVEL_PERCENTAGE_VALUE,
                    SystemAlarmValue = alarmLevelData != null && alarmLevelData.Value != null ? Convert.ToDouble(alarmLevelData.Value) : 0,
                    IsEnabled = alarmLevelEnableData != null && alarmLevelEnableData.Value != null ? Convert.ToBoolean(alarmLevelEnableData.Value) : false,

                    IdAlarmText = alarmProfileTextData != null && alarmProfileTextData.Value != null ? Convert.ToInt64(alarmProfileTextData.Value.ToString()) : 0,

                    CheckLastAlarm = true,
                    SuspensionTime = 900,
                    IsConfirmable = false
                };
                opAlarmDef.Add(opAlarmdef);
            }
            return opAlarmDef;
        }

        private static List<OpAlarmDef> CreateAlarmDefForFuel(List<OpProfileData> opProfileData, OpMeter opMeter)
        {
            List<OpAlarmDef> opAlarmDef = new List<OpAlarmDef>();

            List<AlalrmDefParameters> AlarmList = new List<AlalrmDefParameters>();
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_HIHI_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_HIHI_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_FUEL_HIHI_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_HI_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_HI_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_FUEL_HI_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_LO_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_LO_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_FUEL_LO_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueSmaller,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.ALEVEL_CONFIG_LOLO_LEVEL,
                alarmenable = DataType.ALEVEL_CONFIG_LOLO_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_FUEL_LOLO_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueSmaller,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = DataType.METER_RAPID_TRIGGER_LEVEL,
                alarmenable = DataType.METER_RAPID_ACTIVE,
                profiletext = DataType.PROFILE_PORTAL_FUEL_LEAKAGE_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });

            OpData opMeterCapacityData = opMeter.DataList.Where(w => w.DataType != null && w.DataType.IdDataType == DataType.METER_TANK_CAPACITY).FirstOrDefault();

            foreach (AlalrmDefParameters alarm in AlarmList)
            {
                OpData alarmLevelData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmlevel).FirstOrDefault();
                OpData alarmLevelEnableData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmenable).FirstOrDefault();
                OpProfileData alarmProfileTextData = opProfileData.Where(k => k.IdDataType == alarm.profiletext).FirstOrDefault();

                string locationCID = opMeter.CurrentLocation != null ? opMeter.CurrentLocation.CID : "???";

                object alarmValue;
                if (alarm.alarmlevel == DataType.METER_RAPID_TRIGGER_LEVEL) alarmValue = true;
                else alarmValue = alarmLevelData != null && alarmLevelData.Value != null ? FuelVolumeProcentageToM3(opMeterCapacityData, Convert.ToDouble(alarmLevelData.Value)) : 0;

                OpAlarmDef opAlarmdef = new OpAlarmDef()
                {
                    Name = AlarmDefComponent.FormatAlarmDefName(locationCID, opMeter.MeterSerialNbr, alarm.alarmenable),
                    IdMeter = opMeter.IdMeter,
                    IdLocation = opMeter.CurrentLocation != null ? (long?)opMeter.CurrentLocation.IdLocation : null,
                    IdAlarmType = alarm.alarmtype,
                    IdDataTypeAlarm = alarm.alarmlevel == DataType.METER_RAPID_TRIGGER_LEVEL ? DataType.DEVICE_EVENT_RAPID_UP_EVENT : DataType.FUEL_VOLUME,
                    SystemAlarmValue = alarmValue,
                    IsEnabled = alarmLevelEnableData != null && alarmLevelEnableData.Value != null ? Convert.ToBoolean(alarmLevelEnableData.Value) : false,

                    IdAlarmText = alarmProfileTextData != null && alarmProfileTextData.Value != null ? Convert.ToInt64(alarmProfileTextData.Value.ToString()) : 0,

                    CheckLastAlarm = true,
                    SuspensionTime = 900,
                    IsConfirmable = false
                };

                opAlarmDef.Add(opAlarmdef);
            }
            return opAlarmDef;
        }

        //private static List<OpAlarmDef> CreateAlarmDefForPreassureSensor(List<OpProfileData> opProfileData, OpMeter opMeter)
        //{            
        //    List<OpAlarmDef> opAlarmDef = new List<OpAlarmDef>();
        //    List<AlalrmDefParameters> AlarmList = new List<AlalrmDefParameters>();

        //    /**********************  HIHI  ********************************/
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_HIHI_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_HIHI_UP_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_HIHI_UP_ALARM_TEXT,
        //        alarmtype = 2,
        //    });

        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_HIHI_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_HIHI_DOWN_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_HIHI_DOWN_ALARM_TEXT,
        //        alarmtype = 1,
        //    });

        //    /**********************  HI  ******************************/
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_HI_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_HI_UP_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_HI_UP_ALARM_TEXT,
        //        alarmtype = 2,
        //    });

        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_HI_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_HI_DOWN_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_HI_DOWN_ALARM_TEXT,
        //        alarmtype = 1,
        //    });
            
        //    /**************  LO  ***************/
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_LO_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_LO_UP_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_LO_UP_ALARM_TEXT,
        //        alarmtype = 2,
        //    });

        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_LO_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_LO_DOWN_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_LO_DOWN_ALARM_TEXT,
        //        alarmtype = 1,
        //    });

           
        //    /****************  LOLO  *************/
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_LOLO_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_LOLO_UP_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_LOLO_UP_ALARM_TEXT,
        //        alarmtype = 2,
        //    });
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_LOLO_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_LOLO_DOWN_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_LOLO_DOWN_ALARM_TEXT,
        //        alarmtype = 1,
        //    });


        //    /**************** Emergency StepRel ***********************/
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = DataType.METER_STEPREL_PRESSURE_VALUE,
        //        alarmenable = DataType.METER_STEPREL_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_EMERGENCY_STEPREL_ALARM_TEXT,
        //        alarmtype = 1,
        //    });

        //    /****************  NVR   ***********************/
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = 0, 
        //        alarmenable = DataType.METER_NVR_UP_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_NVR_UP_ALARM_TEXT,
        //        alarmtype = 1,
        //    });
        //    AlarmList.Add(new AlalrmDefParameters()
        //    {
        //        alarmlevel = 0,
        //        alarmenable = DataType.METER_NVR_DOWN_EVENT_ENABLE,
        //        profiletext = DataType.PROFILE_PORTAL_PRESSURE_NVR_DOWN_ALARM_TEXT,
        //        alarmtype = 1,
        //    });

            

        //    foreach (AlalrmDefParameters alarm in AlarmList)
        //    {
        //        OpData alarmLevelData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmlevel).FirstOrDefault();
        //        OpData alarmLevelEnableData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmenable).FirstOrDefault();
        //        OpProfileData alarmProfileTextData = opProfileData.Where(k => k.IdDataType == alarm.profiletext).FirstOrDefault();

        //        object alarmValue;
        //        if (alarm.alarmlevel == DataType.METER_RAPID_TRIGGER_LEVEL) alarmValue = true;
        //        else alarmValue = alarmLevelData != null && alarmLevelData.Value != null ? Convert.ToDouble(alarmLevelData.Value) : 0;

        //        OpAlarmDef opAlarmdef = new OpAlarmDef()
        //        {
        //            Name = AlarmDefComponent.FormatAlarmDefName(opMeter.CurrentLocation.CID, opMeter.MeterSerialNbr, alarm.alarmenable),
        //            IdMeter = opMeter.IdMeter,
        //            IdAlarmType = alarm.alarmtype,
        //            IdDataTypeAlarm = DataType.GAS_LEVEL_PERCENTAGE_VALUE,
        //            SystemAlarmValue = alarmValue,
        //            IsEnabled = alarmLevelEnableData != null && alarmLevelEnableData.Value != null ? Convert.ToBoolean(alarmLevelEnableData.Value) : false,

        //            IdAlarmText = alarmProfileTextData != null && alarmProfileTextData.Value != null ? Convert.ToInt64(alarmProfileTextData.Value.ToString()) : 0,

        //            CheckLastAlarm = true,
        //            SuspensionTime = 900,
        //            IsConfirmable = false
        //        };

        //        opAlarmDef.Add(opAlarmdef);
        //    }
        //    return opAlarmDef;
        //}

        private static List<OpAlarmDef> CreateAlarmDefForPreassureSensor(List<OpProfileData> opProfileData, OpMeter opMeter)
        {
            List<OpAlarmDef> opAlarmDef = new List<OpAlarmDef>();
            List<AlalrmDefParameters> AlarmList = new List<AlalrmDefParameters>();

            /**********************  HIHI  ********************************/
            AlarmList.Add(new AlalrmDefParameters()
            {                
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_HIHI_UP_LATCHED_EVENT,
                alarmenable = DataType.METER_HIHI_UP_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_HIHI_UP_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });

            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_HIHI_DOWN_LATCHED_EVENT,
                alarmenable = DataType.METER_HIHI_DOWN_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_HIHI_DOWN_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });

            /**********************  HI  ******************************/
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_HI_UP_LATCHED_EVENT,
                alarmenable = DataType.METER_HI_UP_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_HI_UP_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });

            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_HI_DOWN_LATCHED_EVENT,
                alarmenable = DataType.METER_HI_DOWN_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_HI_DOWN_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });

            /**************  LO  ***************/
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_LO_UP_LATCHED_EVENT,
                alarmenable = DataType.METER_LO_UP_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_LO_UP_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });

            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_LO_DOWN_LATCHED_EVENT,
                alarmenable = DataType.METER_LO_DOWN_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_LO_DOWN_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });


            /****************  LOLO  *************/
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_LOLO_UP_LATCHED_EVENT,
                alarmenable = DataType.METER_LOLO_UP_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_LOLO_UP_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_LOLO_DOWN_LATCHED_EVENT,
                alarmenable = DataType.METER_LOLO_DOWN_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_LOLO_DOWN_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });


            /**************** Emergency StepRel ***********************/            
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_VALUE_STEPREL_LATCHED_EVENT,
                alarmenable = DataType.METER_STEPREL_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_EMERGENCY_STEPREL_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = false
            });
            /****************  NVR   ***********************/
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 1,
                idDatatypeAlarm = DataType.ANALOG_NVR_LATCHED,
                alarmenable = DataType.METER_NVR_UP_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_NVR_UP_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = true
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 0,
                idDatatypeAlarm = DataType.ANALOG_NVR_LATCHED,
                alarmenable = DataType.METER_NVR_DOWN_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_NVR_DOWN_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueEqual,
                checkLastAlarm = true
            });



            foreach (AlalrmDefParameters alarm in AlarmList)
            {

                OpData alarmLevelEnableData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmenable).FirstOrDefault();
                OpProfileData alarmProfileTextData = opProfileData.Where(k => k.IdDataType == alarm.profiletext).FirstOrDefault();
                object alarmValue = Convert.ToBoolean(alarm.alarmlevel);
                

                OpAlarmDef opAlarmdef = new OpAlarmDef()
                {
                    Name = AlarmDefComponent.FormatAlarmDefName(opMeter.CurrentLocation.CID, opMeter.MeterSerialNbr, alarm.alarmenable),
                    IdMeter = opMeter.IdMeter,
                    IdAlarmType = alarm.alarmtype,
                    IdDataTypeAlarm = alarm.idDatatypeAlarm,
                    SystemAlarmValue = alarmValue,
                    IsEnabled = alarmLevelEnableData != null && alarmLevelEnableData.Value != null ? Convert.ToBoolean(alarmLevelEnableData.Value) : false,
                    IdAlarmText = alarmProfileTextData != null && alarmProfileTextData.Value != null ? Convert.ToInt64(alarmProfileTextData.Value.ToString()) : 0,

                    CheckLastAlarm = alarm.checkLastAlarm,
                    SuspensionTime = 0,
                    IsConfirmable = false
                };

                opAlarmDef.Add(opAlarmdef);
            }
            return opAlarmDef;
        }

        private static List<OpAlarmDef> CreateAlarmDefForPressureDigitlInput(List<OpProfileData> opProfileData, OpMeter opMeter)
        {
            List<OpAlarmDef> opAlarmDef = new List<OpAlarmDef>();

            List<AlalrmDefParameters> AlarmList = new List<AlalrmDefParameters>();
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 0,
                alarmenable = DataType.METER_UDI_UP_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_UDI_UP_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });
            AlarmList.Add(new AlalrmDefParameters()
            {
                alarmlevel = 0,
                alarmenable = DataType.METER_UDI_DOWN_EVENT_ENABLE,
                profiletext = DataType.PROFILE_PORTAL_PRESSURE_UDI_DOWN_ALARM_TEXT,
                alarmtype = (int)Enums.AlarmType.ValueGreater,
            });

            foreach (AlalrmDefParameters alarm in AlarmList)
            {
                OpData alarmEnableData = opMeter.DataList.Where(w => w.DataType.IdDataType == alarm.alarmenable).FirstOrDefault();
                OpProfileData alarmProfileTextData = opProfileData.Where(k => k.IdDataType == alarm.profiletext).FirstOrDefault();
                OpAlarmDef opAlarmdef = new OpAlarmDef()
                {
                    Name = AlarmDefComponent.FormatAlarmDefName(opMeter.CurrentLocation.CID, opMeter.MeterSerialNbr, alarm.alarmenable),
                    IdMeter = opMeter.IdMeter,
                    IdAlarmType = alarm.alarmtype,
                    IdDataTypeAlarm = DataType.GAS_LEVEL_PERCENTAGE_VALUE,
                    SystemAlarmValue = 1,
                    IsEnabled = alarmEnableData != null && alarmEnableData.Value != null ? Convert.ToBoolean(alarmEnableData.Value) : false,

                    IdAlarmText = alarmProfileTextData != null && alarmProfileTextData.Value != null ? Convert.ToInt64(alarmProfileTextData.Value.ToString()) : 0,

                    CheckLastAlarm = false,
                    SuspensionTime = 0 ,
                    IsConfirmable = false
                };
                opAlarmDef.Add(opAlarmdef);
            }


            return opAlarmDef;
        }

        private static List<OpAlarmDef> GetListAlrmDefForMeterType(DataProvider dataProvider, List<OpProfileData> opProfileData, OpMeter opMeter)
        {
            List<int> proflieCalibrationMeterMeterTypeGroups = opProfileData.Where(w => w.IdDataType == (long)DataType.PROFILE_PORTAL_CALIBRATION_TABLE_METER_TYPE_GROUP && w.Value != null).Select(s => Convert.ToInt32(s.Value)).ToList();
            List<int> proflieCalibrationMeterTypes = new List<int>();
            if( proflieCalibrationMeterMeterTypeGroups.Count>0)
                proflieCalibrationMeterTypes = dataProvider.GetMeterTypeInGroupFilter(IdMeterTypeGroup: proflieCalibrationMeterMeterTypeGroups.ToArray()).Select(s => s.IdMeterType).ToList();

            List<int> proflieLPGMeterMeterTypeGroups = opProfileData.Where(w => w.IdDataType == (long)DataType.PROFILE_PORTAL_LPG_METER_TYPE_GROUP && w.Value != null).Select(s => Convert.ToInt32(s.Value)).ToList();
            List<int> proflieLPGMeterMeterTypes = new List<int>();
            if(proflieLPGMeterMeterTypeGroups.Count > 0 )
                proflieLPGMeterMeterTypes = dataProvider.GetMeterTypeInGroupFilter(IdMeterTypeGroup: proflieLPGMeterMeterTypeGroups.ToArray()).Select(s => s.IdMeterType).ToList();

            List<int> listPressureMeterTypesClass = new List<int>() 
            {
                (int)Enums.MeterTypeClass.PressureSensor,
                (int)Enums.MeterTypeClass.I2CPressureTemperature,
                (int)Enums.MeterTypeClass.I2CPressure            
            }; // (int)METER_TYPE_CLASS.Enum.PRESSURE_SENSOR enumerator dla AlarmDefComponent ??                        

            List<int> listPressureDigitalInputmeterTypeClass = new List<int>
            {
                (int)Enums.MeterTypeClass.DigitalInput 
            };

            if (proflieLPGMeterMeterTypes.Contains(opMeter.IdMeterType))
                return CreateAlarmDefForLPGA(opProfileData, opMeter);
            
            if (proflieCalibrationMeterTypes.Contains(opMeter.IdMeterType))
                return CreateAlarmDefForFuel(opProfileData, opMeter);

            if (opMeter.MeterType != null && listPressureMeterTypesClass.Contains(opMeter.MeterType.IdMeterTypeClass))
                return CreateAlarmDefForPreassureSensor(opProfileData, opMeter);

            if (opMeter.MeterType != null && listPressureDigitalInputmeterTypeClass.Contains(opMeter.MeterType.IdMeterTypeClass))
                return CreateAlarmDefForPressureDigitlInput(opProfileData, opMeter);

            return new List<OpAlarmDef>();
        }
        /// <summary>
        /// uwaga przelicza procenty na wielkośc w M3, pojemnoc w data podawana jest w l wiec 1000 l = 1m3
        /// </summary>
        /// <param name="capacity"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static double FuelVolumeProcentageToM3( OpData OpDataCapacity, double level)
        {
            if (OpDataCapacity == null || OpDataCapacity.Value == null)
                return 0;

            double capacity = Convert.ToDouble(OpDataCapacity.Value);

            return level * capacity / 1000; // capacity jest w l a poziom alarmu jest w m3.

        }

        /// <summary>
        /// UWAGA zmieniamy, nie korzystamy z krzywej kalibracyjnej ale podajemy poziom w m3
        /// dla metrów w calibrationGroup zamieniamy przeliczenie.
        /// UWAGA! Przed wywołaniem funkcji trzeba pamiętać aby były uzupełnione parametry  DataList oraz CurrentLocations.
        /// CurrentLocation jest ważne ze względu na CID lokalizacji używane definiowania nazwy alarmu. Dane DataList jest istotna, gdyż zawiera definicje progów alarmowych.
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="opProfileData"></param>
        /// <param name="opMeter"></param>
        /// <returns></returns>
        public static List<OpAlarmDef> SaveAlarmDef(DataProvider dataProvider, List<OpProfileData> opProfileData, OpMeter opMeter)
        {
            List<OpAlarmDef> opAlarmDefToSave = GetListAlrmDefForMeterType( dataProvider, opProfileData,  opMeter) ;                       
            List<OpAlarmDef> opAlarmDefFromDBList = dataProvider.GetAlarmDefFilter(IdMeter: new long[] { opMeter.IdMeter });

            // 1. Wszystko zdefiniowane dajemy na Enable == false
            // opAlarmDefFromDBList.ForEach(f => f.IsEnabled = false);
            // 2. Ustawiamy wartości, dodajemy dnowe

            // Pojawiło sie nowe kryterium, poniewaz dl preassure mozna rozróznic alarm po IdDataAlarmType więc 
            // zamiast nawy w tym przypadku sprawdzamy typ.
            List<int> listPressureMeterTypesClass = new List<int>() 
            {
                (int)Enums.MeterTypeClass.PressureSensor,
                (int)Enums.MeterTypeClass.I2CPressureTemperature,
                (int)Enums.MeterTypeClass.I2CPressure            
            };
            bool useDataTypeAsIndex = false;
            if (opMeter.MeterType != null && listPressureMeterTypesClass.Contains(opMeter.MeterType.IdMeterTypeClass)){
                useDataTypeAsIndex = true;
            }
            foreach (OpAlarmDef alarm in opAlarmDefToSave)
            {
                OpAlarmDef alarmDefFromDB = null;
                if (useDataTypeAsIndex)
                {
                    if (alarm.IdDataTypeAlarm == DataType.ANALOG_NVR_LATCHED) 
                        alarmDefFromDB = opAlarmDefFromDBList.Where(w => w.IdDataTypeAlarm == alarm.IdDataTypeAlarm && Convert.ToInt32( w.SystemAlarmValue ) == Convert.ToInt32(alarm.SystemAlarmValue)).FirstOrDefault();
                    else 
                        alarmDefFromDB = opAlarmDefFromDBList.Where(w =>w.IdDataTypeAlarm == alarm.IdDataTypeAlarm).FirstOrDefault();
                }
                else
                {
                    alarmDefFromDB = opAlarmDefFromDBList.Where(w => AlarmDefComponent.GetAlarmDefLevelStringFromName(w.Name) == AlarmDefComponent.GetAlarmDefLevelStringFromName(alarm.Name)).FirstOrDefault();
                }
                if (alarmDefFromDB != null)
                {
                    alarmDefFromDB.Name = alarm.Name;
                    alarmDefFromDB.IdMeter = alarm.IdMeter;
                    alarmDefFromDB.IdLocation = alarm.IdLocation;
                    alarmDefFromDB.IdAlarmType = alarm.IdAlarmType;
                    alarmDefFromDB.IdDataTypeAlarm = alarm.IdDataTypeAlarm;
                    alarmDefFromDB.SystemAlarmValue = alarm.SystemAlarmValue;
                    alarmDefFromDB.IsEnabled = alarm.IsEnabled;
                    alarmDefFromDB.IdAlarmText = alarm.IdAlarmText == 0 ? alarmDefFromDB.IdAlarmText : alarm.IdAlarmText;
                    alarmDefFromDB.CheckLastAlarm = alarm.CheckLastAlarm;
                    alarmDefFromDB.SuspensionTime = alarm.SuspensionTime;
                    alarmDefFromDB.IsConfirmable = alarm.IsConfirmable;

                }
                else
                {
                    opAlarmDefFromDBList.Add(alarm);
                }
            }
            //3 zapisujemy
            opAlarmDefFromDBList.ForEach(f => dataProvider.SaveAlarmDef(f));
            return opAlarmDefFromDBList;
        }
        #endregion
    }
}
