﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class TariffComponent
    {
        #region Methods
        public static OpTariff GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetTariff(Id);
        }

        public static List<OpTariff> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllTariff();
        }

        public static OpTariff Save(DataProvider dataProvider, OpTariff objectToSave)
        {
            bool isNewItem = objectToSave.IdTariff == 0;
            dataProvider.SaveTariff(objectToSave);

            OpTariffData data = null;
            for (int i = 0; i < objectToSave.DataList.Count; i++)
            {
                data = objectToSave.DataList[i];
                data.IdTariff = objectToSave.IdTariff;
                if (isNewItem || ((data.OpState == OpChangeState.New || data.OpState == OpChangeState.Modified) && dataProvider.GetDataType(data.IdDataType).IsEditable))
                {
                    objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                    objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                }
                else if (data.OpState == OpChangeState.Delete)
                {
                    if (!isNewItem)
                        dataProvider.DeleteTariffData(data);
                }
            }
            objectToSave.DataList.RemoveAll(d => d.OpState == OpChangeState.Delete);

            return dataProvider.GetTariff(objectToSave.IdTariff);
        }

        public static void Delete(DataProvider dataProvider, OpTariff objectToDelete)
        {
            dataProvider.DeleteTariff(objectToDelete);
        }

        public static OpTariff GetActiveTariffForLocation(DataProvider dataProvider, long IdLocation)
        {
            foreach (var loop in dataProvider.GetAllLocationTariff())
            {
                if (!loop.END_TIME.HasValue && loop.IdLocation == IdLocation)
                    return loop.Tariff;
            }
            return null;
        }

        #region SetHelperValues

        public static void SetHelperValues(DataProvider dataProvider, OpTariff tariff, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdTariff, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_TARIFF:
                        tariff.DataList.SetValue(DataType.HELPER_ID_TARIFF, 0, tariff.IdTariff);
                        break;
                    default:
                        break;
                }
            }

            tariff.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #endregion

        
    }
}
