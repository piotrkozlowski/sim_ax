﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects;
using System.Data;
using DB = IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ReportComponent
    {
        #region Get
        public static OpReport GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetReport(Id);
        }

        public static List<OpReport> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllReport();
        }

        public static List<OpReportParam> GetParams(DataProvider dataProvider, int idReport)
        {
            return GetParams(dataProvider, new int[] { idReport });
        }

        public static List<OpReportParam> GetParams(DataProvider dataProvider, int[] idReport)
        {
            if (idReport == null || idReport.Length == 0) return new List<OpReportParam>();
            return dataProvider.GetReportParamFilter(IdReport: idReport);
        }

        public static List<OpReportParam> GetParams(DataProvider dataProvider, int idReport, bool isInputParam)
        {
            return GetParams(dataProvider, new int[] { idReport }, isInputParam);
        }

        public static List<OpReportParam> GetParams(DataProvider dataProvider, int[] idReport, bool isInputParam)
        {
            if (idReport == null || idReport.Length == 0) return new List<OpReportParam>();
            return dataProvider.GetReportParamFilter(IdReport: idReport, IsInputParam: isInputParam);
        }        

        public static object GetReport(DataSet QueryResult)
        {
            return QueryResult;
        }        

        public static bool GetHierarchicalUseCOREDatabase(DataProvider dataProvider, OpReport report, Enums.Module module)
        {
            return GetHierarchicalUseCOREDatabase(dataProvider, report.IdReport, module);
        }
        public static bool GetHierarchicalUseCOREDatabase(DataProvider dataProvider, int idReport, Enums.Module module)
        {
            List<OpReportData> reportDatas = dataProvider.GetReportDataFilter(loadNavigationProperties: false,
                IdReport: new int[] { idReport },
                IdDataType: new long[] { DataType.REPORT_USE_CORE_DATABASE },
                customWhereClause: "[VALUE] IS NOT NULL");
            OpReportData reportData = reportDatas.FirstOrDefault();
            if (reportData != null)
                return GenericConverter.Parse<bool>(reportData.Value);
            else
            {
                List<OpModuleData> moduleDatas = dataProvider.GetModuleDataFilter(loadNavigationProperties: false,
                    IdModule: new int[] { (int)module },
                    IdDataType: new long[] { DataType.REPORT_USE_CORE_DATABASE },
                    customWhereClause: "[VALUE] IS NOT NULL");
                OpModuleData moduleData = moduleDatas.FirstOrDefault();
                if (moduleData != null)
                    return GenericConverter.Parse<bool>(moduleData.Value);
            }
            return false;
        }
        #endregion

        #region Set

        #region SetHelperValues

        public static void SetHelperValues(DataProvider dataProvider, OpReport report, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdReport, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_REPORT:
                        report.DataList.SetValue(DataType.HELPER_ID_REPORT, 0, report.IdReport);
                        break;
                    case DataType.HELPER_ID_REPORT_TYPE:
                        report.DataList.SetValue(DataType.HELPER_ID_REPORT_TYPE, 0, report.IdReportType);
                        break;
                    default:
                        break;
                }
            }
            report.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #endregion

        #region GetAllReportDataType
        public static List<OpReportDataType> GetAllReportDataType(DataProvider dataProvider, bool distinct = false)
        {
            List<OpReportDataType> list = dataProvider.GetAllReportDataType();
            if (list != null && distinct)
                list = list.Distinct(d => d.IdReportDataType).ToList();
            if (list != null)
                list = list.OrderBy(d => d.IdReportDataType).ToList();
            return list;
        }
        #endregion

        #region GenerateReport

        public static byte[] GenerateReport(DataProvider dataProvider, OpImrServer reportServer, string reportName, object[] parameters, bool useDBCollector = false)
        {
            try
            {
                byte[] result = null;

                bool? dbCollectorEnabled = dataProvider.IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    dataProvider.dbCollectorClient.InitDBCollectorProperties(ref dataProvider.dbCollectorSession, new object[] { 2, new object[] { true } });
                    result = dataProvider.dbCollectorClient.GenerateReport(ref dataProvider.dbCollectorSession, reportName, parameters);                    
                }
                else
                {
                    List<OpDistributor> distributorFilter = dataProvider.GetAllDistributor();
                    if (distributorFilter == null)
                        distributorFilter = new List<OpDistributor>();

                    OpOperator wcfDBCollectorOperator = IMR.Suite.UI.Business.Components.CORE.OperatorComponent.GetOperatorWithCreation(dataProvider, "ImrWcfDBCollector");
                    Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION> clientDbCollector = Services.Common.Components.DBCollectorComponent.CreateWcfConnection(
                        reportServer.DBCollectorWebServiceAddress,
                        distributorFilter.Select(d => d.IdDistributor).ToArray(),
                        wcfDBCollectorOperator.Login,
                        wcfDBCollectorOperator.Password,
                        Enums.Module.DBCollector);

                    if (clientDbCollector.Item2.Status == Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                    {
                        Services.Common.ImrWcfServiceReference.SESSION clientSession = clientDbCollector.Item2;
                        clientDbCollector.Item1.InitDBCollectorProperties(ref clientSession, new object[] { 2, new object[] { true } });
                        result = clientDbCollector.Item1.GenerateReport(ref clientSession, reportName, parameters);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataSet GenerateReport(DataProvider dataProvider, OpReport report, List<OpReportParam> paramList, int timeout, bool useCOREDatabase = false)
        {
            DB.DB.Parameter[] StoredProcedureParams = new DB.DB.Parameter[paramList.Count];
            OpReportParam reportParam = null;

            for (int i = 0; i < paramList.Count; i++)
            {
                reportParam = paramList[i];

                if (reportParam.IdReference.HasValue
                    && IsListReferenceType(reportParam))
                {
                    if (!useCOREDatabase)
                        StoredProcedureParams[i] = dataProvider.dbConnectionDw.CreateTableParam(reportParam.FieldName, ReferenceTypeComponent.GetReferenceObjectKey((Enums.ReferenceType)reportParam.IdReference.Value, reportParam.Value));
                    else
                        StoredProcedureParams[i] = dataProvider.dbConnectionCore.CreateTableParam(reportParam.FieldName, ReferenceTypeComponent.GetReferenceObjectKey((Enums.ReferenceType)reportParam.IdReference.Value, reportParam.Value));
                }
                else
                {
                    StoredProcedureParams[i] = dataProvider.DataTypeValue((OpDataTypeClass.Enum)reportParam.IdDataTypeClass, reportParam.IdReference.HasValue ? ReferenceTypeComponent.GetReferenceObjectKey((Enums.ReferenceType)reportParam.IdReference.Value, reportParam.Value) : reportParam.Value, reportParam.FieldName);
                }
            }

            if (!useCOREDatabase)
            {
                return (DataSet)dataProvider.dbConnectionDw.DB.ExecuteProcedure(
                    report.StoredProcedure,
                    new DB.DB.AnalyzeDataSet(GetReport),
                    StoredProcedureParams,
                    AutoTransaction: false, CommandTimeout: timeout
                );
            }
            else
            {
                return (DataSet)dataProvider.dbConnectionCore.DB.ExecuteProcedure(
                    report.StoredProcedure,
                    new DB.DB.AnalyzeDataSet(GetReport),
                    StoredProcedureParams,
                    AutoTransaction: false, CommandTimeout: timeout
                );
            }
        }

        #endregion

        #region Save

        [Obsolete("Use IDataProvider override.")]
        public static OpReport Save(DataProvider dataProvider, OpReport objectToSave, bool saveData = true)
        {
            bool isNewItem = objectToSave.IdReport == 0; //check if it's new element

            objectToSave.IdReport = dataProvider.SaveReport(objectToSave);

            if (saveData)
            {
                //zapis do tabeli REPORT_DATA
                OpReportData data = null;
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    data.IdReport = objectToSave.IdReport;
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        DataComponent.Delete(dataProvider, data);
                        objectToSave.DataList.Remove(data, true);
                    }
                    else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                    {
                        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);

                        if (objectToSave.DataList[i].OpState == OpChangeState.New)
                            objectToSave.DataList[i].AssignReferences(dataProvider);

                        objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                    }
                }
            }

            return dataProvider.GetReport(objectToSave.IdReport);
        }

        public static OpReport Save(IDataProvider dataProvider, OpReport objectToSave, bool saveData = true)
        {
            bool isNewItem = objectToSave.IdReport == 0; //check if it's new element

            objectToSave.IdReport = dataProvider.SaveReport(objectToSave);

            if (saveData)
            {
                //zapis do tabeli REPORT_DATA
                OpReportData data = null;
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    data.IdReport = objectToSave.IdReport;
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        DataComponent.Delete(dataProvider, data);
                        objectToSave.DataList.Remove(data, true);
                    }
                    else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                    {
                        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);

                        if (objectToSave.DataList[i].OpState == OpChangeState.New)
                            objectToSave.DataList[i].AssignReferences(dataProvider);

                        objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                    }
                }
            }

            return dataProvider.GetReport(objectToSave.IdReport);
        }

        private static bool IsListReferenceType(OpReportParam reportParam)
        {
            if (reportParam.IdReference.HasValue)
            {
                Enums.ReferenceType referenceType = (Enums.ReferenceType)reportParam.IdReference.Value;

                return referenceType.In(new Enums.ReferenceType[] { Enums.ReferenceType.OperatorList, Enums.ReferenceType.TaskStatusList, Enums.ReferenceType.LocationList,Enums.ReferenceType.DistributorList,Enums.ReferenceType.MeterTypeList,Enums.ReferenceType.DeviceTypeList,Enums.ReferenceType.AreaList });
            }

            return false;
        }

        #endregion

    }
}
