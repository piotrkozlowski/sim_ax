﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class WorkOrderJCIComponent
    {
        #region Methods
        public static OpWorkOrderJci GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpWorkOrderJci GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetWorkOrderJci(Id, queryDatabase);
        }

        public static OpWorkOrderJci Save(DataProvider dataProvider, OpOperator loggedOperator, OpWorkOrderJci objectToSave)
        {
            dataProvider.SaveWorkOrderJci(objectToSave);

            return dataProvider.GetWorkOrderJci(objectToSave.IdWorkOrderJci);
        }

        //public static void AddHistory(DataProvider dataProvider, OpOperator loggedOperator, OpWorkOrderJci objectToSave)
        //{
        //    List<OpIssueHistory> history = GetHistory(dataProvider, objectToSave.IdIssue);
        //    if (history.Count > 0)
        //    {
        //        history.Last().EndDate = DataProvider.DateTimeNow;
        //        dataProvider.SaveIssueHistory(history.Last());
        //    }

        //    OpIssueHistory issueHistory = new OpIssueHistory();
        //    issueHistory.Issue = objectToSave;
        //    issueHistory.StartDate = DataProvider.DateTimeNow;
        //    issueHistory.IssueStatus = objectToSave.IssueStatus;
        //    issueHistory.Operator = loggedOperator;

        //    dataProvider.SaveIssueHistory(issueHistory);
        //}

        //public static OpWorkOrderJci Copy(DataProvider dataProvider, OpWorkOrderJci objectToCopy)
        //{
        //    OpWorkOrderJci newIssue = new OpWorkOrderJci(objectToCopy);
        //    newIssue.ID_ISSUE = 0;
        //    dataProvider.SaveIssue(newIssue);

        //    return dataProvider.GetIssue(newIssue.IdIssue);
        //}

        //public static void DeleteHistory(DataProvider dataProvider, int idIssue)
        //{
        //    foreach (var item in dataProvider.IssueHistory.Where(ih=>ih.IdIssue == idIssue))
        //    {
        //        dataProvider.DeleteIssueHistory(item);
        //    }
        //}

        //public static void Delete(DataProvider dataProvider, OpWorkOrderJci objectToDelete)
        //{
        //    DeleteHistory(dataProvider, objectToDelete.IdIssue);
        //    dataProvider.DeleteIssue(objectToDelete);
        //}
        #endregion


    }
}
