﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ArticleComponent
    {
        #region Methods
        public static OpArticle GetByID(DataProvider dataProvider, long Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpArticle GetByID(DataProvider dataProvider, long Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetArticle(Id, queryDatabase);
        }

        public static List<OpArticle> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllArticle().OrderBy(a => a.Name).ToList();
        }

        public static OpArticle Save(DataProvider dataProvider, OpArticle objectToSave)
        {
            dataProvider.SaveArticle(objectToSave);

            return dataProvider.GetArticle(objectToSave.IdArticle);
        }

        public static void Delete(DataProvider dataProvider, OpArticle objectToDelete)
        {
            dataProvider.DeleteArticle(objectToDelete);
        }

        public static void Synchronize(DataProvider dataProvider, List<SPArticle> articles)
        {
            Dictionary<int, SPArticle> dictCDNArticles = articles.ToDictionary(d => d.Twr_GIDNumer);

            Dictionary<long, OpArticle> dictArticles = dataProvider.GetAllArticle().Where(d => d.ExternalId.HasValue).ToDictionary(d => d.ExternalId.Value);
            foreach (var cdnArticle in dictCDNArticles)
            {
                if (dictArticles.ContainsKey(cdnArticle.Key))
                {
                    OpArticle articleInDB = dictArticles[cdnArticle.Key];
                    if (articleInDB.Name != cdnArticle.Value.KodTowaru ||
                        articleInDB.Description != cdnArticle.Value.NazwaTowaru)
                    {
                        articleInDB.Name = cdnArticle.Value.KodTowaru;
                        articleInDB.Description = cdnArticle.Value.NazwaTowaru;
                        Save(dataProvider, articleInDB);
                    }
                }
                else
                {
                    Save(dataProvider,
                         new OpArticle()
                             {
                                 ExternalId = cdnArticle.Key,
                                 Name = cdnArticle.Value.KodTowaru,
                                 Description = cdnArticle.Value.NazwaTowaru
                             });
                }
            }

        }

        public static List<OpArticle> GetArticleByOrderNumber(DataProvider dataProvider, DBCommonUsok UsokConnection, string serialNumber, bool isAiut, out OpDevice device)
        {
            List<OpArticle> articleList = null;
            device = null;
            if (isAiut)
            {
                if (serialNumber.Length > 0)
                {
                    try
                    {//tu bylo uzyte FindDevices
                        long SN = Convert.ToInt64(serialNumber.Trim());
                        device = dataProvider.GetDevice(SN);
                        if (device == null)
                        {
                            if (serialNumber.Trim().Length == 16)
                            {  
                                long? sn = null;
                                List<OpData> dataList = dataProvider.GetDataFilter(
                                            IdDataType: new long[] { DataType.DEVICE_FACTORY_NUMBER },
                                            customWhereClause: "[VALUE] = '" + serialNumber.Trim() + "' and [SERIAL_NBR] is not null");
                                if (dataList.Count > 0)
                                {
                                    sn = dataList.Where(w => w.IdData == dataList.Max(t => t.IdData)).First().SerialNbr;
                                    if (sn.HasValue)
                                    {
                                        device = dataProvider.GetDevice(sn.Value, true);
                                    }
                                }
                            }
                        }
                        if (device != null)
                        {
                            List<OpDeviceOrderNumber> donList = dataProvider.GetDeviceOrderNumberFilter(Name: device.DeviceOrderNumber.Name,
                                FirstQuarter: device.DeviceOrderNumber.FirstQuarter, SecondQuarter: device.DeviceOrderNumber.SecondQuarter);
                            if (donList != null)
                            {
                                List<OpDeviceOrderNumberInArticle> doniaList = dataProvider.GetDeviceOrderNumberInArticleFilter(IdDeviceOrderNumber: donList.Select(d => d.IdDeviceOrderNumber).ToArray());
                                if (doniaList != null && doniaList.Count > 0)
                                {
                                    articleList = doniaList.Select(t => t.Article).ToList();
                                    
                                }
                                else
                                {
                                    articleList = dataProvider.Article;
                                }
                            }
                            else
                            {
                                articleList = dataProvider.Article;
                            }
                        }
                        else
                        {
                            articleList = dataProvider.Article;
                        }
                    }
                    catch //(Exception ex)
                    {
                        articleList = dataProvider.Article;
                        device = null;
                    }
                }
            }
            else
            {
                articleList = dataProvider.Article;
            }
            return articleList;
        }

        public static OpArticle GetUnnecessaryArticle(DataProvider dataProvider)
        {
            return GetUnnecessaryArticles(dataProvider).First();
        }

        public static List<OpArticle> GetUnnecessaryArticles(DataProvider dataProvider)
        {
            List<OpArticle> retValue = null;

            retValue = dataProvider.GetArticleFilter(customWhereClause: "[NAME] like 'Unnecessary' or [NAME] like 'Niepotrzebny'");
            if (retValue == null || retValue.Count == 0)
            {
                OpArticle item = new OpArticle();
                item.IsUnique = true;
                item.IsAiut = false;
                item.Name = "Unnecessary";
                item.IdArticle = dataProvider.SaveArticle(item);

                retValue = new List<OpArticle>();
                retValue.Add(item);
            }

            return retValue;
        }

        public static List<OpArticle> GetArticleList(DataProvider dataProvider)
        {
            return dataProvider.GetArticleFilter(Visible: true).OrderBy(a => a.Name).ToList();
        }

        public static bool CheckOrdernumberExistanceInArticleName(OpArticle article, List<OpDeviceOrderNumber> orderNumberList)
        {
            bool exists = false;
            foreach (OpDeviceOrderNumber donItem in orderNumberList.Where(d => !String.IsNullOrEmpty(d.Name)))
            {
                if (article.Name.ToUpper().StartsWith(donItem.Name.ToUpper() + " ")
                    || article.Name.ToUpper().StartsWith(donItem.Name.ToUpper() + "-")
                    || article.Name.ToUpper().EndsWith(" " + donItem.Name.ToUpper())
                    || article.Name.ToUpper().Contains(" " + donItem.Name.ToUpper() + " ")
                    || article.Name.ToUpper().Contains(" " + donItem.Name.ToUpper() + "-"))
                {
                    exists = true;
                    break;
                }
            }
            return exists;
        }

        #endregion
    }
}
