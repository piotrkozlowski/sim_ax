﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ImrServerComponent
    {
        public const int LocalHost = 18;

        #region Methods
        public static OpImrServer GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetImrServer(Id);
        }

        public static List<OpImrServer> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllImrServer().Where(i => i.IS_GOOD).ToList();
        }

        public static OpImrServer Save(DataProvider dataProvider, OpImrServer objectToSave)
        {
            objectToSave.IdImrServer = dataProvider.SaveImrServer(objectToSave);

            SaveImrSeverData(dataProvider, objectToSave);

            return objectToSave;
        }

        public static void SaveImrSeverData(DataProvider dataProvider, OpImrServer server)
        {
            for(int i = 0; i < server.DataList.Count; i++)
            {
                server.DataList[i].IdImrServer = server.IdImrServer;
                if (server.DataList[i].OpState == OpChangeState.New || server.DataList[i].OpState == OpChangeState.Modified)
                {
                    server.DataList[i].IdData = dataProvider.SaveImrServerData(server.DataList[i]);
                    server.DataList[i].OpState = OpChangeState.Loaded;
                }
                else if (server.DataList[i].OpState == OpChangeState.Delete)
                {
                    if (server.DataList[i].IdData > 0)
                        dataProvider.DeleteImrServerData(server.DataList[i]);
                    server.DataList.RemoveAt(i, true);
                    i--;
                }
            }
            
        }

        public static bool IsIssueTaskEnabled(DataProvider dataProvider, bool defaultResult = false)
        {
            try
            {
                List<OpImrServerData> imrServerDatas = dataProvider.GetImrServerDataFilter(loadNavigationProperties: false,
                    IdImrServer: new int[] { LocalHost },
                    IdDataType: new long[] { DataType.IMR_SERVER_ISSUES_TASKS_ENABLED });

                if (imrServerDatas == null || imrServerDatas.Count == 0)
                    return defaultResult;

                OpImrServerData imrServerData = imrServerDatas.FirstOrDefault(q => q.Value != null);
                if (imrServerData != null)
                    return GenericConverter.Parse<bool>(imrServerData.Value, defaultResult);
            }
            catch { }
            return defaultResult;
        }

        public static bool IsDataBaseIMRSC(DataProvider dataProvider, bool defaultResult = true)
        {
            try
            {
                //List<OpImrServerData> imrServerDatas = dataProvider.GetImrServerDataFilter(loadNavigationProperties: false,
                //    IdDataType: new long[] { Common.DataType.IMR_SERVER_IS_LOCALHOST },
                //    customWhereClause: " [VALUE] IS NOT NULL AND [VALUE] = cast(1 as bit) ");

                //// bezpieczniej zwrocic true
                //if (imrServerDatas == null || imrServerDatas.Count == 0)
                //{
                //    Common.IMRLog.AddToLog(EventID.Forms.IMRServerDataIsLocalhostNotSet);
                //    return defaultResult;
                //}

                List<OpImrServerData> imrServerDatas = dataProvider.GetImrServerDataFilter(loadNavigationProperties: false,
                    IdImrServer: new int[] { LocalHost },
                    IdDataType: new long[] { Common.DataType.IMR_SERVER_IS_CORE_IMRSC },
                    customWhereClause: " [VALUE] IS NOT NULL ");

                // bezpieczniej zwrocic true
                if (imrServerDatas == null || imrServerDatas.Count == 0)
                {
                    Common.IMRLog.AddToLog(EventID.Forms.IMRServerDataIsCoreIMRSCNotSet);
                    return defaultResult;
                }

                OpImrServerData imrServerData = imrServerDatas.FirstOrDefault(q => q.Value != null);
                if (imrServerData == null) return defaultResult;

                bool isCoreIMRSC;
                if (bool.TryParse(imrServerData.Value.ToString(), out isCoreIMRSC))
                    return isCoreIMRSC;

            }
            catch (Exception ex)
            {
                Common.IMRLog.AddToLog(EventID.Forms.IMRServerIsDataBaseIMRSCError, ex);
            }
            Common.IMRLog.AddToLog(EventID.Forms.IMRServerIsDataBaseIMRSC, defaultResult);
            return defaultResult;
        }

        public static OpDistributor GetServerDistributor(DataProvider dataProvider, int? IdImrServer)
        {
            OpDistributor serverDistributor = null;
            if (IdImrServer != null)
            {
                string customWhereClause = String.Format("[ID_DATA_TYPE] = {0} AND [VALUE] is not null AND CAST([VALUE] as int) = {1}", DataType.DISTRIBUTOR_IMR_SERVER_ID, IdImrServer.Value.ToString());
                List<int> allIdDistributorOnSelectedImrServer = dataProvider.GetDistributorDataFilter(IdDistributor: new int[]{}, 
                                                                                                      customWhereClause: customWhereClause, 
                                                                                                      transactionLevel: System.Data.IsolationLevel.ReadUncommitted, 
                                                                                                      autoTransaction: false, 
                                                                                                      loadNavigationProperties: false)
                                                                            .Select(s => s.IdDistributor).ToList();
                if (allIdDistributorOnSelectedImrServer.Count > 0)
                {
                    customWhereClause = String.Format("[ID_DATA_TYPE] = {0} AND [VALUE] is not null and CAST([VALUE] as bit) = 1", DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR);
                    List<OpDistributorData> serverDistirbutorData = dataProvider.GetDistributorDataFilter(IdDistributor: allIdDistributorOnSelectedImrServer.ToArray(),
                                                                                                          IdDataType: new long[] { DataType.DISTRIBUTOR_PERFORMANCE_HIERARCHY_SERVER_DISTRIBUTOR },
                                                                                                          customWhereClause: customWhereClause,
                                                                                                          loadNavigationProperties: false, 
                                                                                                          autoTransaction: false,
                                                                                                          transactionLevel: System.Data.IsolationLevel.ReadUncommitted);
                    if (serverDistirbutorData.Count > 0)
                    {
                        serverDistributor = dataProvider.GetDistributorFilter(transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                              autoTransaction: false,
                                                                              mergeIntoCache: false,
                                                                              IdDistributor: serverDistirbutorData.Select(d => d.IdDistributor).Distinct().ToArray()).FirstOrDefault();
                    }
                }
            }
            return serverDistributor;
        }

        public static List<string> GetServerSupportedExcelExtensions(DataProvider dataProvider, OpImrServer isItem)
        {
            List<string> retList = new List<string>();

            if (!isItem.DataList.Exists(d => d.IdDataType == DataType.IMR_SERVER_SUPPORTED_EXCEL_EXTENSION))
                isItem.DataList.AddRange(dataProvider.GetImrServerDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                             IdImrServer: new int[] { isItem.IdImrServer }, IdDataType: new long[] { DataType.IMR_SERVER_SUPPORTED_EXCEL_EXTENSION }));
            foreach (OpImrServerData isdItem in isItem.DataList.Where(d => d.IdDataType == DataType.IMR_SERVER_SUPPORTED_EXCEL_EXTENSION && d.Value != null && !String.IsNullOrEmpty(d.Value.ToString().Trim())))
            {
                retList.Add(isdItem.Value.ToString().Trim());
            }

            return retList.Distinct().ToList();
        }

        public static Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION> CreateWcfConnection(DataProvider dataProvider, OpDistributor distributor, bool useDeploymentServer = false)
        {
            OpOperator wcfDBCollectorOperator = dataProvider.GetOperatorFilter(Login: "ImrWcfDBCollector", IdDistributor: new int[] { 1 }).FirstOrDefault();
            if (wcfDBCollectorOperator == null)
                return new Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION>(null, null);
            else
                return CreateWcfConnection(dataProvider, distributor, wcfDBCollectorOperator.Login, RSA.Encrypt(wcfDBCollectorOperator.Password), useDeploymentServer);
        }

        public static Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION> CreateWcfConnection(DataProvider dataProvider, OpDistributor distributor, string wcfOperatorLogin, string wcfOperatorPass, bool useDeploymentServer = false)
        {
            Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION> retValue = new Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION>(null, null);

            if (distributor.IdIMRServer.HasValue)
            {
                OpImrServer serverToCheck = distributor.ImrServer;
                if (useDeploymentServer)
                {
                    if (serverToCheck.DeploymentServerId.HasValue)
                    {
                        serverToCheck = dataProvider.GetImrServer(serverToCheck.DeploymentServerId.Value);
                    }
                    else
                    {
                        BaseComponent.Log(EventID.Forms.ImrServerHasNotDefinedDeploymentImrServer, true, new object[]{ distributor.ImrServer.ToString(), distributor.ImrServer.IdImrServer });
                        return retValue;
                    }
                }
                if (!String.IsNullOrEmpty(serverToCheck.DBCollectorWebServiceAddress))
                {
                    Services.Common.ImrWcfServiceReference.DBCollectorClient wcfDBCollectorClient = new Services.Common.ImrWcfServiceReference.DBCollectorClient("netTcpBinding_IDBCollector", serverToCheck.DBCollectorWebServiceAddress);
                    Services.Common.ImrWcfServiceReference.SESSION session = new Services.Common.ImrWcfServiceReference.SESSION();
                    session.Status = Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK;
                    session.Time = dataProvider.DateTimeUtcNow;
                    string language = "pl-PL"; //"en-US"
                    //string passTm = RSA.Encrypt("aiutaiut"/*this.WcfOperator.Password*/);
                    List<OpDistributor> distributorFilter = null;
                    distributorFilter = dataProvider.GetAllDistributor();
                    if (distributorFilter == null)
                        distributorFilter = new List<OpDistributor>();

                    wcfDBCollectorClient.InitDBCollector(null, null, distributorFilter.Select(d => d.IdDistributor).ToArray(), Enums.Module.DBCollector);
                    int totalOffset = Convert.ToInt32(TimeZoneInfo.Local.GetUtcOffset(DateTime.Now).TotalMinutes);
                    session = wcfDBCollectorClient.Login(session, wcfOperatorLogin, wcfOperatorPass, language, totalOffset);

                    if (session.Status == Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK)
                    {
                        retValue = new Tuple<Services.Common.ImrWcfServiceReference.DBCollectorClient, Services.Common.ImrWcfServiceReference.SESSION>(wcfDBCollectorClient, session);
                    }
                    else
                    {
                        BaseComponent.Log(EventID.Forms.WcfLoginFailed, true, new object[]{ serverToCheck.ToString(), serverToCheck.IdImrServer, (session != null && session.Error != null ? session.Error.Msg : "-") });
                        return retValue;
                    }
                }
                else
                {
                    BaseComponent.Log(EventID.Forms.ImrServerHasNotDefinedDBCollectorConnection, true, new object[]{ serverToCheck.ToString(), serverToCheck.IdImrServer });
                    return retValue;
                }
            }
            else
            {
                BaseComponent.Log(EventID.Forms.DistributorHasNotDefinedImrServer, true, new object[]{ distributor.ToString(), distributor.IdDistributor });
                return retValue;
            }

            return retValue;
        }


        #endregion

        #region GetServerActionMSMQDict
        public static Dictionary<int, Tuple<string, string>> GetServerActionMSMQDict(DataProvider dataProvider, int[] IdImrServer, bool useDBCollector)
        {
            Dictionary<int, Tuple<string, string>> serverActionMSMQDict = new Dictionary<int, Tuple<string, string>>();

            if (useDBCollector)
            {
                List<OpImrServerData> serverDatas = dataProvider.GetImrServerDataFilter(loadNavigationProperties: false,
                    IdImrServer: IdImrServer, IdDataType: new long[] { DataType.IMR_SERVER_MSMQ_HOST, DataType.IMR_SERVER_MSMQ_SEND_ACTION }, IndexNbr: new int[] { 0 });
                if (serverDatas != null && serverDatas.Count > 0)
                {
                    foreach (KeyValuePair<int, List<OpImrServerData>> item in serverDatas.GroupBy(q => q.IdImrServer).ToDictionary(q => q.Key, q => q.ToList()))
                    {
                        OpImrServerData hostData = item.Value.FirstOrDefault(q => q.IdDataType == DataType.IMR_SERVER_MSMQ_HOST && q.Value != null);
                        OpImrServerData sendActionData = item.Value.FirstOrDefault(q => q.IdDataType == DataType.IMR_SERVER_MSMQ_SEND_ACTION && q.Value != null);
                        if (hostData != null && !String.IsNullOrWhiteSpace(hostData.Value.ToString()) && sendActionData != null && !String.IsNullOrWhiteSpace(sendActionData.Value.ToString()))
                            serverActionMSMQDict[item.Key] = new Tuple<string, string>(hostData.Value.ToString(), sendActionData.Value.ToString());
                    }
                }
            }

            return serverActionMSMQDict;
        }
        #endregion

        #region GetReportServer()

        public static OpImrServer GetReportServer(DataProvider dataProvider)
        {
            try
            {
                OpImrServerData reportServerData = dataProvider.GetImrServerDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdDataType: new long[] { DataType.IMR_SERVER_REPORT_GENERATOR }).Where(d => Convert.ToInt32(d.Value) == 1).FirstOrDefault();

                if (reportServerData != null)
                {
                    return dataProvider.GetImrServer(reportServerData.ID_IMR_SERVER);
                }
            }
            catch (Exception ex)
            {
                BaseComponent.Log(EventID.Forms.GetServerReportException, true, new object[] { ex.ToString() });
            }

            return null;
        }

        #endregion
    }
}
