﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;

using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Web;
using IMR.Suite.UI.Business.Objects.Custom;
using System.Globalization;
using IMR.Suite.Services.Common.UTDServiceReference;
using IMR.Suite.Services.Common.Components;
using System.Data;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class TaskComponent : BaseComponent
    {
        #region Methods

        #region Get

        public static OpTask GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpTask GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetTask(Id, queryDatabase);
        }

        public static List<OpTask> GetAll(DataProvider dataProvider)
        {
            return GetAll(dataProvider, false);
        }

        public static List<OpTask> GetAll(DataProvider dataProvider, bool excludeFinished)
        {
            if (excludeFinished)
                return dataProvider.GetAllTask().Where(t => !t.TaskStatus.IsFinished).ToList();
            else
                return dataProvider.GetAllTask();
        }

        public static List<OpFile> GetAttachments(DataProvider DataProvider, OpTask task, OpOperator loggedOperator = null)
        {
            List<OpFile> taskFiles = new List<OpFile>();
            if (task.DataList.Count(d => d.IdDataType == DataType.TASK_ATTACHMENT) > 0)
            {
                long[] fileIds = task.DataList.Where(d => d.IdDataType == DataType.TASK_ATTACHMENT).Select(d => Convert.ToInt64(d.Value)).ToArray();
                if (fileIds.Length > 0)
                {
                    taskFiles = DataProvider.GetFile(fileIds, true);
                    //foreach (var item in task.DataList.Where(d => d.IdDataType == DataType.TASK_ATTACHMENT))
                    //{
                    //    taskFiles.Add(DataProvider.GetFile(task.DataList.TryGetValue<long>(item.IdDataType, item.Index)));
                    //}
                    taskFiles = GetAllowedAttachments(taskFiles, loggedOperator);
                }
            }
            return taskFiles;
        }

        public static List<OpFile> GetAllowedAttachments(List<OpFile> attachments, OpOperator loggedOperator)
        {
            List<long> notAllowedAttachments = new List<long>();
            if (loggedOperator != null && attachments != null && attachments.Count > 0)
            {
                if (loggedOperator.Activities.Exists(a => a.IdActivity == Activity.ALLOWED_TASK_ATTACHMENT_EXTENSION))
                {
                    foreach (OpFile fItem in attachments)
                    {
                        if (!loggedOperator.HasPermission(Activity.ALLOWED_TASK_ATTACHMENT_EXTENSION, (int)fItem.Extension))
                        {
                            notAllowedAttachments.Add(fItem.IdFile);
                        }
                    }
                }
            }

            if (notAllowedAttachments.Count > 0)
                return attachments.Where(f => !f.IdFile.In(notAllowedAttachments.ToArray())).ToList();
            return attachments;
        }

        public static List<OpTaskHistory> GetHistory(DataProvider dataProvider, int idTask)
        {
            return dataProvider.GetTaskHistoryFilter(IdTask: new int[] { idTask }); // .Where(t => t.IdTask == idTask).ToList();
        }

        public static List<OpHistory> GetTaskHistory(DataProvider dataProvider, OpOperator loggedOperator, int idTask, List<Enums.ObjectHistoryEventType> includedEvents = null)
        {
            List<OpHistory> taskHistory = new List<OpHistory>();
            #region StateHistory

            if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.ChangedStatus))
            {
                List<OpTaskHistory> taskStateHistory = GetHistory(dataProvider, idTask);

                foreach (OpTaskHistory stateHistory in taskStateHistory)
                {
                    OpHistory hItem = new OpHistory(stateHistory.StartDate, Enums.ObjectHistoryEventType.ChangedStatus, stateHistory.TaskStatus.ToString(), stateHistory.Notes);
                    hItem.EventObject = stateHistory;
                    taskHistory.Add(hItem);
                }
            }

            #endregion
            #region InstallationProtocol

            if (loggedOperator.HasPermission(Activity.TASK_INSTALLATION_PROTOCOL_VIEW))
            {
                if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.StepPerformed))
                {
                    List<Tuple<Enums.TaskVisitStepType, DateTime, string, object>> installationProtocol = GetIntallationProtocol(dataProvider, idTask);

                    foreach (Tuple<Enums.TaskVisitStepType, DateTime, string, object> tItem in installationProtocol)
                    {
                        OpHistory hItem = new OpHistory(tItem.Item2, Enums.ObjectHistoryEventType.StepPerformed, new OpEnumObject(tItem.Item1).ToString(), tItem.Item3) { EventObject = tItem.Item4 };
                        taskHistory.Add(hItem);
                    }
                }
            }

            #endregion
            return taskHistory; // .Where(t => t.IdTask == idTask).ToList();
        }

        public static OpTaskHistory GetPreviousStatus(DataProvider dataProvider, int idTask)
        {
            return dataProvider.GetTaskHistoryFilter(IdTask: new int[] { idTask }).Where(t => t.END_DATE != null).OrderByDescending(th => th.ID_TASK_HISTORY).FirstOrDefault();
        }

        public static List<OpTask> GetForIssue(DataProvider dataProvider, int idIssue)
        {
            return GetForIssue(dataProvider, idIssue, null);
        }

        public static List<OpTask> GetForIssue(DataProvider dataProvider, int idIssue, bool? myIMRSCVisible)
        {
            return dataProvider.GetTaskFilter(IdIssue: new int[] { idIssue }).Where(t => ((myIMRSCVisible.HasValue && t.IMRMySCVisible == myIMRSCVisible) || !myIMRSCVisible.HasValue)).ToList();
        }

        public static List<OpTask> GetRecentlyAdded(DataProvider dataProvider, int topCount)
        {
            //return dataProvider.GetTasksRecentlyAdded(topCount).ToList();
            return null;
        }

        public static int[] GetFreeServiceActions(DataProvider dataProvider, int[] taskIds)
        {
            //return dataProvider.Task.Where(t => t.TaskType.REMOTE && !t.TaskStatus.IS_FINISHED).Select(t => t.ID_TASK).Where(id => taskIds.Contains(id)).ToArray();
            return dataProvider.GetTask(taskIds).Where(t => t.TaskType.REMOTE && !t.TaskStatus.IS_FINISHED).Select(t => t.IdTask).ToArray();
        }

        public static List<OpTaskOperation> GetAllowedTaskOperation(DataProvider dataProvider, List<int> allowedDistributor)
        {
            /*
                     * Załadowanie dozwolonych predefiniowanych operacji związanych z realizacja zadania, w zależności od:
                     *  - Lokalizacji - okreslane na podstawie meterów jakie znajdują się aktualnie na lokalizacji - można określić dopiero w konkretnym tasku
                     *  - Dystrybutora - okreslane na podstawie ID_DYSTRYBUTORA - można określić globalnie - zawęża dalszą listę dostępnych akcji
                     *  Struktura:
                     *      ->TYP (Lokalizacja, Dystrybutor)
                     *          -> PODTYP (dla Lokalizacji w zaleznosci od meterów, dla dystrybutora Id distributor)
                     *              -> ID_DESCR
                     *   Pojedyncza struktura ma ten sam index
                     *   Struktury zapisywane sa w TASK_DATA dla ID_TASK = 0
                    */
            List<OpTaskData> taskOperations = dataProvider.GetTaskDataFilter(IdTask: new int[] { 0 },
                                                                             IdDataType: new long[]{ DataType.TASK_OPERATION_TYPE,
                                                                                                             DataType.TASK_OPERATION_SUBTYPE,
                                                                                                             DataType.TASK_OPERATION_ID_DESCR },
                                                                             loadNavigationProperties: false);
            List<OpTaskOperation> allowedTaskOperations = new List<OpTaskOperation>();
            if (taskOperations != null)
            {
                foreach (var tdItem in taskOperations.GroupBy(x => new { x.Index },
                                                                (key, group) => new
                                                                {
                                                                    List = group.ToList()
                                                                }))
                {
                    OpTaskData type = tdItem.List.Find(t => t.IdDataType == DataType.TASK_OPERATION_TYPE);
                    int typeId;
                    if (type != null && type.Value != null && !String.IsNullOrEmpty(type.Value.ToString()) && int.TryParse(type.Value.ToString(), out typeId))
                    {
                        OpTaskData subType = tdItem.List.Find(d => d.IdDataType == DataType.TASK_OPERATION_SUBTYPE);
                        OpTaskData descr = tdItem.List.Find(d => d.IdDataType == DataType.TASK_OPERATION_ID_DESCR);
                        long idDescr;
                        if (subType != null && subType.Value != null && !String.IsNullOrEmpty(subType.Value.ToString()) &&
                            descr != null && descr.Value != null && !String.IsNullOrEmpty(descr.Value.ToString()) && long.TryParse(descr.Value.ToString(), out idDescr))
                        {
                            if (typeId == (int)Enums.TaskOperationType.Distributor)
                            {
                                int idDistributor;
                                if (int.TryParse(subType.Value.ToString(), out idDistributor))
                                {
                                    if (!idDistributor.In(allowedDistributor.ToArray()))// != LoggedUser.IdDistributor)
                                        allowedTaskOperations.RemoveAll(o => o.IdDescr == idDescr);
                                    else
                                    {
                                        if (!allowedTaskOperations.Exists(o => o.IdDescr == idDescr))
                                            allowedTaskOperations.Add(new OpTaskOperation(idDescr, "", null));
                                    }
                                }
                            }
                            else if (typeId == (int)Enums.TaskOperationType.Location)
                            {
                                int idMeterLocationType;
                                if (int.TryParse(subType.Value.ToString(), out idMeterLocationType))
                                {
                                    OpTaskOperation taskOperationTmp = allowedTaskOperations.Find(o => o.IdDescr == idDescr);
                                    if (taskOperationTmp != null)
                                    {
                                        if (!taskOperationTmp.AllowedLocationTypes.Exists(t => (int)t == idMeterLocationType))
                                            taskOperationTmp.AllowedLocationTypes.Add((Enums.MeterTypeClass)idMeterLocationType);
                                    }
                                    else
                                    {
                                        if (!allowedTaskOperations.Exists(o => o.IdDescr == idDescr))
                                            allowedTaskOperations.Add(new OpTaskOperation(idDescr, "", new List<Enums.MeterTypeClass>() { (Enums.MeterTypeClass)idMeterLocationType }));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (allowedTaskOperations != null && allowedTaskOperations.Count > 0)
            {
                List<OpDescr> descriptions = dataProvider.GetDescr(allowedTaskOperations.Select(d => d.IdDescr).ToArray());
                foreach (OpDescr dItem in descriptions.Where(d => d.IdLanguage == (int)dataProvider.UserLanguage).ToList())
                    allowedTaskOperations.Find(o => o.IdDescr == dItem.IdDescr).Descr = dItem.Description;

            }
            return allowedTaskOperations;
        }

        public static List<OpTaskStatus> GetPossibleTaskStatusChangeList(DataProvider dataProvider, OpOperator loggedOperator, List<OpTaskStatus> allowedTaskStatusEdit, List<OpTaskStatus> allowedTaskStatusChange, OpTask task)
        {
            OpTaskStatus currentTaskStatus = null;
            if(task != null && task.IdTask > 0)
                currentTaskStatus = dataProvider.GetTaskStatus(task.IdTaskStatus);
            return GetPossibleTaskStatusChangeList(dataProvider, loggedOperator, allowedTaskStatusEdit, allowedTaskStatusChange, currentTaskStatus);
            /*
            if (task == null || task.IdTask == 0)
                currentTaskStatus = dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Not_Started);
            else
                currentTaskStatus = dataProvider.GetTaskStatus(task.IdTaskStatus);
            List<OpTaskStatus> allowedTaskStatusList = new List<OpTaskStatus>();
            if (task == null || task.IdTask == 0)
                allowedTaskStatusList.Add(currentTaskStatus);
            else
            {
                allowedTaskStatusList.AddRange(allowedTaskStatusEdit);
                if (!allowedTaskStatusList.Exists(t => t.IdTaskStatus == task.IdTaskStatus))
                    allowedTaskStatusList.Add(currentTaskStatus);
            }

            List<OpTaskStatus> allowedTaskStatusChangeWorkflow = new List<OpTaskStatus>();
            if (loggedOperator != null && loggedOperator.Distributor != null && loggedOperator.Distributor.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT, DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV })))
            {
                #region DataBaseConfiguration

                List<OpDistributorData> ddList = loggedOperator.Distributor.DataList.Where(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT, DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV })).ToList();
                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT))
                {
                    int? idPrevTaskStatus = null;
                    int? idNextTaskStatus = null;
                    if(ddItem.Value != null)
                    {
                        int idTaskStatusTmp = 0;
                        if(!int.TryParse(ddItem.Value.ToString(), out idTaskStatusTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextTaskStatus = idTaskStatusTmp;
                    }
                    OpDistributorData ddItemPrev  = ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idTaskStatusTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idTaskStatusTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevTaskStatus = idTaskStatusTmp;
                    }
                    if (idPrevTaskStatus == currentTaskStatus.IdTaskStatus && idNextTaskStatus.HasValue)
                    {
                        if (dataProvider.GetTaskStatus(idPrevTaskStatus.Value) != null
                            && !allowedTaskStatusChangeWorkflow.Exists(d => d.IdTaskStatus == idPrevTaskStatus.Value))
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus(idPrevTaskStatus.Value));//dodajemy obecny status żeby nie zamknąć sobie drogi jakiejkolwiek zmiany przy zmianach masowych
                        if (idNextTaskStatus != currentTaskStatus.IdTaskStatus
                            && dataProvider.GetTaskStatus(idNextTaskStatus.Value) != null
                            && !allowedTaskStatusChangeWorkflow.Exists(d => d.IdTaskStatus == idNextTaskStatus.Value))
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus(idNextTaskStatus.Value));
                    }
                }

                #endregion
            }
            else
            {
                #region Default

                switch (currentTaskStatus.IdTaskStatus)
                {
                    case (int)OpTaskStatus.Enum.Not_Started:
                        //jeżeli task nie jest jezcze stworzony to chcemy dodawać tylko początkowe statusy
                        if (task != null && task.IdTask > 0)
                        {
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Planned));
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Suspended));
                        }
                        break;
                    case (int)OpTaskStatus.Enum.Planned:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Not_Started));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Suspended));
                        break;
                    case (int)OpTaskStatus.Enum.In_progress:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Planned));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Suspended));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        break;
                    case (int)OpTaskStatus.Enum.WaitingForAcceptance:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_succesfully));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.RealizationNotPossible));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Verified));
                        break;
                    case (int)OpTaskStatus.Enum.Finished_succesfully:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));//do poprawienia ewentualnych ruchów magazynowych
                        break;
                    case (int)OpTaskStatus.Enum.Finished_with_error:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));//do poprawienia ewentualnych ruchów magazynowych
                        break;
                    case (int)OpTaskStatus.Enum.Suspended:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                        break;
                    case (int)OpTaskStatus.Enum.Verified:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_succesfully));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Rejected));
                        break;
                    case (int)OpTaskStatus.Enum.RealizationNotPossible:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_succesfully));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        break;
                    case (int)OpTaskStatus.Enum.Rejected:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Verified));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.RealizationNotPossible));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        break;
                    default:
                        break;
                }

                #endregion
            }

            allowedTaskStatusChangeWorkflow.RemoveAll(t => t == null);

            List<OpTaskStatus> tsList = allowedTaskStatusList;
            tsList = tsList.Intersect(allowedTaskStatusChangeWorkflow).ToList();
            foreach (OpTaskStatus tsItem in allowedTaskStatusChange)
            {
                if (allowedTaskStatusChangeWorkflow.Exists(t => t.IdTaskStatus == tsItem.IdTaskStatus)
                    && !tsList.Exists(t => t.IdTaskStatus == tsItem.IdTaskStatus))
                    tsList.Add(tsItem);
            }
            if (!tsList.Exists(t => t.IdTaskStatus == currentTaskStatus.IdTaskStatus))
                tsList.Add(currentTaskStatus);

            tsList = tsList.OrderBy(t => t.ComboBoxString).ToList();

            return tsList;
            */
        }

        public static List<OpTaskStatus> GetPossibleTaskStatusChangeList(DataProvider dataProvider, OpOperator loggedOperator, List<OpTaskStatus> allowedTaskStatusEdit = null, List<OpTaskStatus> allowedTaskStatusChange = null, List<OpTaskStatus> currentTaskStatus = null)
        {
            if (currentTaskStatus == null)
                currentTaskStatus = new List<OpTaskStatus>() { dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Not_Started) };
            List<OpTaskStatus> tsList = null;
            foreach (OpTaskStatus tsItem in currentTaskStatus)
            {
                List<OpTaskStatus> tsTmpList = GetPossibleTaskStatusChangeList(dataProvider, loggedOperator, allowedTaskStatusEdit: allowedTaskStatusEdit, allowedTaskStatusChange: allowedTaskStatusChange, currentTaskStatus: tsItem);
                if (tsList == null)
                    tsList = tsTmpList;
                else
                    tsList = tsList.Intersect(tsTmpList).ToList();
            }
            if (tsList == null)
                tsList = new List<OpTaskStatus>();
            return tsList;
        }

        public static List<OpTaskStatus> GetPossibleTaskStatusChangeList(DataProvider dataProvider, OpOperator loggedOperator, List<OpTaskStatus> allowedTaskStatusEdit = null, List<OpTaskStatus> allowedTaskStatusChange = null, OpTaskStatus currentTaskStatus = null)
        {
            bool currentStatusGiven = currentTaskStatus != null;
            if(allowedTaskStatusEdit == null)
                allowedTaskStatusEdit = RoleComponent.FilterByPermission(dataProvider.GetAllTaskStatus(), Activity.ALLOWED_TASK_STATUS_EDIT, loggedOperator);
            if (allowedTaskStatusChange == null)
                allowedTaskStatusChange = RoleComponent.FilterByPermission(dataProvider.GetAllTaskStatus(), Activity.ALLOWED_TASK_STATUS_CHANGE, loggedOperator);
            if (currentTaskStatus == null)
                currentTaskStatus = dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Not_Started);

            List<OpTaskStatus> allowedTaskStatusList = new List<OpTaskStatus>();
            if (!currentStatusGiven)
                allowedTaskStatusList.Add(currentTaskStatus);
            else
            {
                allowedTaskStatusList.AddRange(allowedTaskStatusEdit);
                if (!allowedTaskStatusList.Exists(t => t.IdTaskStatus == currentTaskStatus.IdTaskStatus))
                    allowedTaskStatusList.Add(currentTaskStatus);
            }

            List<OpTaskStatus> allowedTaskStatusChangeWorkflow = new List<OpTaskStatus>();
            if (loggedOperator != null && loggedOperator.Distributor != null && loggedOperator.Distributor.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT, DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV })))
            {
                #region DataBaseConfiguration

                List<OpDistributorData> ddList = loggedOperator.Distributor.DataList.Where(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT, DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV })).ToList();
                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT))
                {
                    int? idPrevTaskStatus = null;
                    int? idNextTaskStatus = null;
                    if (ddItem.Value != null)
                    {
                        int idTaskStatusTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idTaskStatusTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextTaskStatus = idTaskStatusTmp;
                    }
                    OpDistributorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idTaskStatusTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idTaskStatusTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevTaskStatus = idTaskStatusTmp;
                    }
                    if (idPrevTaskStatus == currentTaskStatus.IdTaskStatus && idNextTaskStatus.HasValue)
                    {
                        if (dataProvider.GetTaskStatus(idPrevTaskStatus.Value) != null
                            && !allowedTaskStatusChangeWorkflow.Exists(d => d.IdTaskStatus == idPrevTaskStatus.Value))
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus(idPrevTaskStatus.Value));//dodajemy obecny status żeby nie zamknąć sobie drogi jakiejkolwiek zmiany przy zmianach masowych
                        if (idNextTaskStatus != currentTaskStatus.IdTaskStatus
                            && dataProvider.GetTaskStatus(idNextTaskStatus.Value) != null
                            && !allowedTaskStatusChangeWorkflow.Exists(d => d.IdTaskStatus == idNextTaskStatus.Value))
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus(idNextTaskStatus.Value));
                    }
                }

                #endregion
            }
            else
            {
                #region Default

                switch (currentTaskStatus.IdTaskStatus)
                {
                    case (int)OpTaskStatus.Enum.Not_Started:
                        //jeżeli task nie jest jezcze stworzony to chcemy dodawać tylko początkowe statusy
                        if (currentStatusGiven)
                        {
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Planned));
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                            allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Suspended));
                        }
                        break;
                    case (int)OpTaskStatus.Enum.Planned:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Not_Started));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Suspended));
                        break;
                    case (int)OpTaskStatus.Enum.In_progress:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Planned));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Suspended));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        break;
                    case (int)OpTaskStatus.Enum.WaitingForAcceptance:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_succesfully));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.RealizationNotPossible));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Verified));
                        break;
                    case (int)OpTaskStatus.Enum.Finished_succesfully:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));//do poprawienia ewentualnych ruchów magazynowych
                        break;
                    case (int)OpTaskStatus.Enum.Finished_with_error:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));//do poprawienia ewentualnych ruchów magazynowych
                        break;
                    case (int)OpTaskStatus.Enum.Suspended:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.In_progress));
                        break;
                    case (int)OpTaskStatus.Enum.Verified:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_succesfully));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Rejected));
                        break;
                    case (int)OpTaskStatus.Enum.RealizationNotPossible:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_succesfully));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        break;
                    case (int)OpTaskStatus.Enum.Rejected:
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.WaitingForAcceptance));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Verified));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.RealizationNotPossible));
                        allowedTaskStatusChangeWorkflow.Add(dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_with_error));
                        break;
                    default:
                        break;
                }

                #endregion
            }

            allowedTaskStatusChangeWorkflow.RemoveAll(t => t == null);

            List<OpTaskStatus> tsList = allowedTaskStatusList;
            tsList = tsList.Intersect(allowedTaskStatusChangeWorkflow).ToList();
            foreach (OpTaskStatus tsItem in allowedTaskStatusChange)
            {
                if (allowedTaskStatusChangeWorkflow.Exists(t => t.IdTaskStatus == tsItem.IdTaskStatus)
                    && !tsList.Exists(t => t.IdTaskStatus == tsItem.IdTaskStatus))
                    tsList.Add(tsItem);
            }
            if (!tsList.Exists(t => t.IdTaskStatus == currentTaskStatus.IdTaskStatus))
                tsList.Add(currentTaskStatus);

            tsList = tsList.OrderBy(t => t.ComboBoxString).ToList();

            return tsList;
        }

        public static List<Tuple<OpTaskData, string>> GetAssignedTaskOperations(DataProvider dataProvider, List<int> idTask)
        {
            List<Tuple<OpTaskData, string>> toReturn = new List<Tuple<OpTaskData, string>>();

            if (idTask != null && idTask.Count > 0)
            {

            }
            else
                toReturn = null;

            return toReturn;
        }

        public static List<OpDataType> GetAttributes(DataProvider dataProvider)
        {
            List<OpDataType> attributesList = new List<OpDataType>();

            OpDataTypeGroup taskEditDataTypeGroup = dataProvider.GetDataTypeGroupFilter(Name: "TASK_EDIT_PARAMS").FirstOrDefault();
            if (taskEditDataTypeGroup != null)
            {
                List<OpDataTypeInGroup> taskEditDataType = dataProvider.GetDataTypeInGroup(taskEditDataTypeGroup.IdDataTypeGroup);
                if (taskEditDataType != null && taskEditDataType.Count > 0)
                    attributesList = taskEditDataType.Select(d => d.DataType).ToList();
            }

            return attributesList;
        }

        public static List<OpTaskData> GetAttributesData(DataProvider dataProvider, int idTask)
        {
            List<OpTaskData> attributesList = new List<OpTaskData>();

            OpDataTypeGroup taskEditDataTypeGroup = dataProvider.GetDataTypeGroupFilter(Name: "IMRSC_PARAMS_EDIT_GROUP").FirstOrDefault();
            if (taskEditDataTypeGroup != null)
            {
                List<OpDataTypeInGroup> taskEditDataType = dataProvider.GetDataTypeInGroup(taskEditDataTypeGroup.IdDataTypeGroup);
                if (taskEditDataType != null && taskEditDataType.Count > 0)
                    attributesList = GetAttributesData(dataProvider, idTask, taskEditDataType.Select(d => d.DataType).ToList());
            }
            return attributesList;
        }
        public static List<OpTaskData> GetAttributesData(DataProvider dataProvider, int idTask, List<OpDataType> attributes)
        {
            if (attributes != null && attributes.Count > 0)
            {
                List<OpTaskData> attributesData = dataProvider.GetTaskDataFilter(IdDataType: attributes.Select(d => d.IdDataType).ToArray(), customWhereClause: "[ID_TASK] = " + idTask);
                if (attributesData != null)
                    attributesData.ForEach(a => a.DataType = attributes.Find(d => d.IdDataType == a.IdDataType));
                return attributesData;
            }
            else
                return new List<OpTaskData>();
        }

        public static List<OpTask> GetShortDeadlinePassTasks(DataProvider dataProvider, OpOperator operatorPerformer, List<int> idDistributor = null)
        {
            List<OpTask> retList = new List<OpTask>();
            List<int> idOperatorPerformerList = new List<int>();
            List<int> idDistributorList = new List<int>();
            if (operatorPerformer != null)
                idOperatorPerformerList.Add(operatorPerformer.IdOperator);
            if (idDistributor != null)
                idDistributorList.AddRange(idDistributor);

            retList = dataProvider.GetTaskFilter(loadNavigationProperties: true,
                                                    loadTaskHistory: true,
                                                    IdOperatorPerformer: idOperatorPerformerList.ToArray(),
                                                    IdDistributor: idDistributorList.ToArray(),
                                                    IdTaskStatus: new int[] { (int)OpTaskStatus.Enum.In_progress, (int)OpTaskStatus.Enum.Planned },
                                                    customWhereClause: "[ID_OPERATOR_PERFORMER] IS NOT NULL");

            List<int> idTasksToSkip = new List<int>();
            foreach (OpTask tItem in retList)
            {
                if (tItem.DeadlineDaysLeft > 3)
                    idTasksToSkip.Add(tItem.IdTask);
            }
            if (idTasksToSkip.Count > 0)
            {
                retList.RemoveAll(t => t.IdTask.In(idTasksToSkip.ToArray()));
            }

            return retList;
        }

        public static List<OpTask> GetUnsuspendedTasks(DataProvider dataProvider, OpOperator operatorPerformer, List<int> idDistributor = null, bool loadNavigationProperties = false)
        {
            List<int> idOperatorPerformerList = new List<int>();
            List<int> idDistributorList = new List<int>();
            if (idDistributor != null)
                idDistributorList.AddRange(idDistributor);
            if (operatorPerformer != null)
                idOperatorPerformerList.Add(operatorPerformer.IdOperator);
            List<OpTask> tasksUnsuspended = new List<OpTask>();


            //wczytywać taski ze statusem inprogres z historią
            List<OpTask> tasksInProgres = dataProvider.GetTaskFilter(IdOperatorPerformer: idOperatorPerformerList.ToArray(),
                                                            loadNavigationProperties: false,
                                                            loadTaskHistory: true,
                                                            IdDistributor: idDistributorList.ToArray(),
                                                            IdTaskStatus: new int[] { (int)OpTaskStatus.Enum.In_progress },
                                                            customWhereClause: "[ID_OPERATOR_PERFORMER] IS NOT NULL")
                                                            .OrderByDescending(c => c.CreationDate).ToList();

            //wczytywać taski ze statusem suspended
            List<OpTask> tasksSuspended = dataProvider.GetTaskFilter(IdOperatorPerformer: idOperatorPerformerList.ToArray(),
                                                            loadNavigationProperties: false,
                                                            IdDistributor: idDistributorList.ToArray(),
                                                            IdTaskStatus: new int[] { (int)OpTaskStatus.Enum.Suspended },
                                                            customWhereClause: "[ID_OPERATOR_PERFORMER] IS NOT NULL")
                                                            .OrderByDescending(c => c.CreationDate).ToList();

            //sprawdzać czy w taskach suspended są issue in_progerss
            if (tasksSuspended.Exists(t => t.IdIssue.HasValue))
            {
                List<OpIssue> issueList = dataProvider.GetIssueFilter(IdIssue: tasksSuspended.Where(t => t.IdIssue.HasValue).Select(t => t.IdIssue.Value).ToArray(),
                                                                      loadIssueHistory: false,
                                                                      loadNavigationProperties: false,
                                                                      IdIssueStatus: new int[] { (int)OpIssueStatus.Enum.IN_PROGRESS });
                foreach (OpTask task in tasksSuspended)
                {
                    if (task.IdIssue.HasValue)
                    {
                        task.Issue = issueList.Find(i => i.IdIssue == task.IdIssue);
                    }
                }
            }

            if (tasksInProgres.Count > 0)
            {
                foreach (OpTask task in tasksInProgres)
                {
                    //sprawdzamy czy ostatnio był suspended? 
                    if ((task.TaskHistory.Count - 2 >= 0) && (task.TaskHistory[task.TaskHistory.Count - 2].IdTaskStatus == (int)OpTaskStatus.Enum.Suspended))
                    {
                        tasksUnsuspended.Add(task);
                    }
                }
            }

            if (tasksSuspended.Count > 0)
            {
                foreach (OpTask task in tasksSuspended)
                {
                    if (task.Issue != null)
                    {
                        if (task.Issue.IdIssueStatus == (int)OpIssueStatus.Enum.IN_PROGRESS)
                        {
                            tasksUnsuspended.Add(task);
                        }
                    }
                }
            }



            if (tasksUnsuspended.Count > 0)
            {
                if (loadNavigationProperties)
                {
                    if(tasksUnsuspended.Exists(t => t.IdLocation.HasValue))
                    {
                        List<long> idLocations = tasksUnsuspended.Where(t => t.IdLocation.HasValue).Select(t => t.IdLocation.Value).Distinct().ToList();
                        Dictionary<long, OpLocation> lDict = dataProvider.GetLocation(idLocations.ToArray()).ToDictionary(l => l.IdLocation);
                        foreach (OpTask tItem in tasksUnsuspended.Where(t => t.IdLocation.HasValue))
                        {
                            tItem.Location = lDict.TryGetValue(tItem.IdLocation.Value);
                        }
                    }
                }
                return tasksUnsuspended;
            }
            else
                return null;
        }

        public static string GetTaskPaymentTypeName(Enums.TaskPaymentPartType paymentType)
        {
            switch (paymentType)
            {
                case Enums.TaskPaymentPartType.FitterBase:
                case Enums.TaskPaymentPartType.ClientBase:
                    return ResourcesText.Service2;
                case Enums.TaskPaymentPartType.FitterPremium:
                    return ResourcesText.ServicePremium;
                case Enums.TaskPaymentPartType.FitterRevisit:
                    return ResourcesText.Revisit;
                case Enums.TaskPaymentPartType.ClientSetInstallation:
                case Enums.TaskPaymentPartType.FitterSetInstallation:
                    return ResourcesText.SetInstallation;
                case Enums.TaskPaymentPartType.ClientSecondSetInstallation:
                case Enums.TaskPaymentPartType.FitterSecondSetInstallation:
                    return ResourcesText.SecondSetInstallation;
                case Enums.TaskPaymentPartType.ClientInstallationAberdeenArea:
                case Enums.TaskPaymentPartType.FitterInstallationAberdeenArea:
                    return ResourcesText.SetInstallation + " Aberdeen";
                case Enums.TaskPaymentPartType.ClientServiceAberdeenArea:
                    return ResourcesText.Service + " Aberdeen";
                case Enums.TaskPaymentPartType.ClientAberdeenAreaSecondSetInstallation:
                case Enums.TaskPaymentPartType.FitterAberdeenAreaSecondSetInstallation:
                    return ResourcesText.SecondSetInstallation + " Aberdeen";
                case Enums.TaskPaymentPartType.ClientDevice:
                    return ResourcesText.Device;
                case Enums.TaskPaymentPartType.ClientInstallationTransitAccomoodataion:
                    return ResourcesText.Transit + "/" + ResourcesText.Accomoodation + " (" + ResourcesText.Installation + ")";
                case Enums.TaskPaymentPartType.ClientServiceTransitAccomoodataion:
                    return ResourcesText.Transit + "/" + ResourcesText.Accomoodation + " (" + ResourcesText.Service + ")";
                case Enums.TaskPaymentPartType.ClientOKOX303SmartMeterPlugInUnitRemoval:
                    return ResourcesText.Uninstall + " OKOX303 SmartMeter";
                case Enums.TaskPaymentPartType.ClientTankMonitoringHardwareRemoval:
                    return ResourcesText.UninstallHardware + " TankMonitoring";
                case Enums.TaskPaymentPartType.ClientServiceNotCoveredByFlatFee:
                    return ResourcesText.ServiceNotCoveredByFlatFee;
                case Enums.TaskPaymentPartType.ClientSimCardActivation:
                    return ResourcesText.SimCardActivation;
                case Enums.TaskPaymentPartType.ClientServiceHardwarePenalty:
                    return ResourcesText.ServiceHardwarePenalty;
                case Enums.TaskPaymentPartType.ClientServiceSoftwarePenalty:
                    return ResourcesText.ServiceSoftwarePenalty;
                case Enums.TaskPaymentPartType.ClientSetInstallationPremium:
                    return ResourcesText.SetInstallation + " (Premium)";
                case Enums.TaskPaymentPartType.ClientSecondSetInstallationPremium:
                    return ResourcesText.SecondSetInstallation + " (Premium)";
                case Enums.TaskPaymentPartType.ClientSetInstallationAberdeenAreaPremium:
                    return ResourcesText.SetInstallation + " Aberdeen (Premium)";
                default:
                    return ResourcesText.Unknown;
            }
        }

        public static List<GOObjectServiceDefaultPayment> GetTaskDefaultPayment(DataProvider dataProvider, OpDistributor distributor, List<OpDistributorData> defaultPaymentData, bool loadAllPayments = false)
        {
            if (defaultPaymentData == null || defaultPaymentData.Count == 0)
            {
                List<long> additionalDataTypesList = new List<long>();
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TASK_TYPE);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_SERVICE_OPTION);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_FROM);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_TO);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_AMOUNT);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_CANCELED);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_PART_TYPE);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TYPE);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_ARTICLE);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TASK_GROUP);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_ID);
                additionalDataTypesList.Add(DataType.DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_NAME);
                defaultPaymentData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                            IdDistributor: new int[] { distributor.IdDistributor },
                                                                            IdDataType: additionalDataTypesList.ToArray());
                distributor.DataList.RemoveAll(d => d.IdDataType.In(additionalDataTypesList.ToArray()));
                distributor.DataList.AddRange(defaultPaymentData);
            }

            List<OpDistributorData> distrDataObjectServiceTaskType = defaultPaymentData.FindAll(d => d.Value != null && d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TASK_TYPE);
            List<TaskComponent.GOObjectServiceDefaultPayment> objectServicePaymentDataSource = new List<TaskComponent.GOObjectServiceDefaultPayment>();
            if (distrDataObjectServiceTaskType != null && distrDataObjectServiceTaskType.Count > 0)
            {
                foreach (OpDistributorData dItem in distrDataObjectServiceTaskType)
                {
                    DateTime from = Convert.ToDateTime(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_FROM && d.Index == dItem.Index && d.Value != null).Value);
                    DateTime? to = null;
                    DateTime? cancelationDate = null;
                    int idTaskType = Convert.ToInt32(dItem.Value);
                    long? idArticle = null;
                    Enums.TaskPaymentType paymentType = Enums.TaskPaymentType.Client;//żeby pozostała zgodność ze wcześniejszymi wersjami
                    Enums.TaskPaymentPartType paymentPartType = Enums.TaskPaymentPartType.ClientBase;//żeby pozostała zgodność ze wcześniejszymi wersjami
                    double amount = Convert.ToDouble(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_AMOUNT && d.Index == dItem.Index && d.Value != null).Value);
                    TaskComponent.GOServiceOption serviceOption = null;
                    int? idTaskGroup = null;

                    if (defaultPaymentData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TYPE && d.Index == dItem.Index && d.Value != null))
                        paymentType = (Enums.TaskPaymentType)Convert.ToInt32(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TYPE && d.Index == dItem.Index && d.Value != null).Value);
                    if (defaultPaymentData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_PART_TYPE && d.Index == dItem.Index && d.Value != null))
                        paymentPartType = (Enums.TaskPaymentPartType)Convert.ToInt32(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_PART_TYPE && d.Index == dItem.Index && d.Value != null).Value);
                    if (defaultPaymentData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_ARTICLE && d.Index == dItem.Index && d.Value != null))
                        idArticle = Convert.ToInt64(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_ARTICLE && d.Index == dItem.Index && d.Value != null).Value);
                    if (defaultPaymentData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_TO && d.Index == dItem.Index && d.Value != null))
                        to = Convert.ToDateTime(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_TO && d.Index == dItem.Index && d.Value != null).Value);
                    if (defaultPaymentData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_CANCELED && d.Index == dItem.Index && d.Value != null))
                        cancelationDate = Convert.ToDateTime(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_CANCELED && d.Index == dItem.Index && d.Value != null).Value);
                    if (defaultPaymentData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TASK_GROUP && d.Index == dItem.Index && d.Value != null))
                        idTaskGroup = Convert.ToInt32(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_TASK_GROUP && d.Index == dItem.Index && d.Value != null).Value);


                    if (defaultPaymentData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_SERVICE_OPTION && d.Index == dItem.Index && d.Value != null))
                    {
                        int idServiceOption = Convert.ToInt32(defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_OBJECT_SERVICE_PAYMENT_ID_SERVICE_OPTION && d.Index == dItem.Index && d.Value != null).Value);
                        OpDistributorData ddIdServiceOption = defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_ID && Convert.ToInt32(d.Value) == idServiceOption);

                        string serviceOptionName = defaultPaymentData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_SERVICE_AGREEMENT_OPTION_NAME && d.Index == ddIdServiceOption.Index && d.Value != null).Value.ToString();
                        serviceOption = new TaskComponent.GOServiceOption(idServiceOption, serviceOptionName);
                    }
                    if (!cancelationDate.HasValue || loadAllPayments)
                    {
                        TaskComponent.GOObjectServiceDefaultPayment goospItem = new TaskComponent.GOObjectServiceDefaultPayment(dataProvider.GetTaskType(idTaskType), serviceOption, paymentType, paymentPartType,
                                                                                                                                (idArticle.HasValue ? dataProvider.GetArticle(idArticle.Value) : null),
                                                                                                                                from, to, cancelationDate, amount,
                                                                                                                                (idTaskGroup.HasValue ? dataProvider.GetTaskGroup(idTaskGroup.Value) : null));
                        goospItem.Index = dItem.Index;
                        objectServicePaymentDataSource.Add(goospItem);
                    }
                }
            }
            return objectServicePaymentDataSource;
        }

        public static List<Tuple<Enums.TaskVisitStepType, DateTime, string, object>> GetIntallationProtocol(DataProvider dataProvider, int idTask)
        {
            List<Tuple<Enums.TaskVisitStepType, DateTime, string, object>> retList = new List<Tuple<Enums.TaskVisitStepType, DateTime, string, object>>();
            #region ProtocolDataTypes
            List<long> protocolDataTypes = new List<long>(){
                                            DataType.TASK_PROTOCOL_AIUT_DEVICE_MOUNT_ON_MAGAZINE,
                                            DataType.TASK_PROTOCOL_AIUT_DEVICE_UNMOUNT_FROM_MAGAZINE,
                                            DataType.TASK_PROTOCOL_DEVICE_EXCHANGE,
                                            DataType.TASK_PROTOCOL_DEVICE_ID_ARTICLE,
                                            DataType.TASK_PROTOCOL_DEVICE_MOVE,
                                            DataType.TASK_PROTOCOL_DEVICE_MOVE_DESTINATION,
                                            DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_MOUNT_ON_MAGAZINE,
                                            DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_UNMOUNT_FROM_MAGAZINE,
                                            DataType.TASK_PROTOCOL_ADDED_METER_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_ID_ACTION,
                                            DataType.TASK_PROTOCOL_ACTION_TYPE,
                                            DataType.TASK_PROTOCOL_ACTION_DEVICE,
                                            DataType.TASK_PROTOCOL_ACTION_STATUS,
                                            DataType.TASK_PROTOCOL_ACTION_TEXT,
                                            DataType.TASK_PROTOCOL_TIMESTAMP,
                                            DataType.TASK_ARRANGED_FITTER_VISIT_DATE,
                                            DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR,
                                            DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR,
                                            DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE,
                                            DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT,
                                            DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES,
                                            DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR,
                                            DataType.TASK_PROTOCOL_METER_SERIAL_NBR,
                                            DataType.TASK_CURRENT_METER_READOUT,
                                            DataType.TASK_PROTOCOL_ALEVEL_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_NAME,
                                            DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NAME,
                                            DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NOTE,
                                            DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_NAME,
                                            DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NAME,
                                            DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NOTE,
                                            DataType.TASK_EXPORT_STEP,
                                            DataType.TASK_EXPORT_STEP_STATUS,
                                            DataType.TASK_EXPORT_STEP_NOTE,
                                            DataType.TASK_EXPORT_STATUS,
                                            DataType.TASK_EXPORT_NOTE,
                                            DataType.TASK_EXPORT_ID_ACTION,
                                            DataType.TASK_EXPORT_ARCH_LOCATION_ID_IMR_SERVER,
                                            DataType.TASK_EXPORT_ARCH_LOCATION_SOURCE_SERVER_ID,
                                            DataType.TASK_PROTOCOL_ID_OPERATOR,
                                            //HeatOkoStepLatch
                                            DataType.TAKS_OKO_SERIAL_NBR,
                                            DataType.TASK_T_MOBILE_SIGNAL_LEVEL,
                                            DataType.TASK_PLUS_SIGNAL_LEVEL,
                                            DataType.TASK_ORANGE_SIGNAL_LEVEL,
                                            DataType.TASK_PLAY_SIGNAL_LEVEL,
                                            //WatermeterLatch
                                            DataType.TASK_WATERMETER_DEVICE_SERIAL_NBR,
                                            DataType.TAKS_WATERMETER_SERIAL_NBR,
                                            DataType.TASK_INSTALLATION_PROTOCOL_WATERMETER_INDICATIONS_SUMMARY,
                                            DataType.TASK_REPLACED_WATER_METER_SERIAL_NUMBER,
                                            DataType.TASK_INSTALLATION_PROTOCOL_WATERMETER_NOMINAL_FLOW,
                                            //ConverterLatch
                                            DataType.TASK_CONVERTER_DEVICE_SERIAL_NBR,
                                            DataType.TASK_CONVERTER_SERIAL_NBR,
                                            DataType.TASK_INSTALLATION_PROTOCOL_INDICATIONS_SUMMARY,
                                            //BatteryExchangeLatch
                                            DataType.TASK_PROTOCOL_BATTERY_CHANGE_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_BATTERY_CHANGE_OLD_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_BATTERY_CHANGE_NEW_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_BATTERY_CHANGE_OLD_BATTERY_EFFICIENT,
                                            //ValveTestLatch
                                            DataType.TASK_PROTOCOL_VALVE_OPERATION_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_VALVE_OPERATION_TYPE,
                                            DataType.TASK_PROTOCOL_VALVE_OPERATION_RESULT,
                                            DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS,
                                            DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS_NAME,
                                            //ReprogramLatch
                                            DataType.TASK_PROTOCOL_REPROGRAM_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_REPROGRAM_FILE_NAME,
                                            DataType.TASK_PROTOCOL_REPROGRAM_OLD_VERSION_NUMBER,
                                            DataType.TASK_PROTOCOL_REPROGRAM_NEW_VERSION_NUMBER,
                                            //SimCardTestLatch
                                            DataType.TASK_PROTOCOL_SIM_CARD_TEST_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_SIM_CARD_TEST_PHONE_NBR,
                                            DataType.TASK_PROTOCOL_SIM_CARD_TEST_ERROR_TYPE,
                                            //CriticalErrorLatch
                                            DataType.TASK_PROTOCOL_CRITICAL_ERROR_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_CRITICAL_ERROR_TYPE,
                                            DataType.TASK_PROTOCOL_CRITICAL_ERROR_VALUE,
                                            //IsMeterUnderground
                                            DataType.METER_TANK_IS_UNDERGROUND,
                                            //ArchiveReadStepLatch
                                            DataType.TASK_PROTOCOL_ARCHIVE_READ_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_ARCHIVE_READ_FROM,
                                            DataType.TASK_PROTOCOL_ARCHIVE_READ_TO,
                                            DataType.TASK_PROTOCOL_ARCHIVE_READ_TYPE,
                                            DataType.TASK_PROTOCOL_ARCHIVE_READ_FRAME_COUNT,
                                            //EventLogStepLatch
                                            DataType.TASK_PROTOCOL_EVENT_LOG_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_EVENT_LOG_REFERENCE_DATE,
                                            DataType.TASK_PROTOCOL_EVENT_LOG_ENTRY_COUNT,
                                            //NewMeterCounterResynchronizationStepLatch
                                            DataType.TASK_PROTOCOL_NEW_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR,
                                            DataType.DEVICE_COUNTER_COEFFICIENT,
                                            DataType.GAS_METER_USAGE_VALUE,
                                            DataType.DEVICE_COUNTER_LITER_MAX_VALUE,
                                            //OldMeterCounterResynchronizationStepLatch
                                            DataType.TASK_PROTOCOL_OLD_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR,
                                            DataType.DEVICE_COUNTER_COEFFICIENT,
                                            DataType.GAS_METER_USAGE_VALUE,
                                            DataType.DEVICE_COUNTER_LITER_MAX_VALUE,
                                            //ConfigurationRestoreStepLatch
                                            DataType.TASK_PROTOCOL_CONFIGURATION_RESTORE_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_RESTORED_CONFIGURATION,
                                            //InstalledGasmeterStepLatch
                                            DataType.TASK_PROTOCOL_INSTALLED_GASMETER_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_METER_TYPE,
                                            DataType.METER_SERIES,
                                            DataType.METER_SERIAL_NUMBER,
                                            DataType.DEVICE_COUNTER_COEFFICIENT,
                                            DataType.GAS_METER_USAGE_VALUE,
                                            DataType.DEVICE_COUNTER_LITER_MAX_VALUE,
                                            DataType.LOCATION_CID,
                                            DataType.TASK_PROTOCOL_OPERATION_TYPE,
                                            DataType.MINIMUM_TEMPORARY_FLOW,
                                            DataType.MAXIMUM_TEMPORARY_FLOW,
                                            DataType.TASK_PROTOCOL_GAS_METER_USAGE_VALUE,
                                            DataType.TASK_PROTOCOL_DEVICE_COUNTER_LITER_MAX_VALUE,
                                            DataType.TASK_PROTOCOL_DEVICE_COUNTER_COEFFICIENT,
                                            //DeinstalledGasmeterStepLatch
                                            DataType.TASK_PROTOCOL_DEINSTALLED_GASMETER_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_DEINSTALLED_MAGAZINE_CID,
                                            DataType.TASK_PROTOCOL_OPERATION_TYPE,
                                            //ConfigurationDownloadStepLatch
                                            DataType.TASK_PROTOCOL_CONFIGURATION_DOWNLOAD_DEVICE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_OKO,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_LATITUDE,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_LONGITUDE,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_FILE_NAME,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_STATUS,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_EFFICIENCY,
                                            DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_FRAME_COUNT,
                                            //TaskProceedResynchronisationStepLatch
                                            DataType.TASK_PROTOCOL_INSTALLATION_WITH_SYNCHRONIZATION_SERIAL_NBR,
                                            DataType.LOCATION_CID,
                                            DataType.GAS_METER_USAGE_VALUE,
                                            //TaskProceedPinValueStepLatch
                                            DataType.TASK_PROTOCOL_PIN_VALUE_SERIAL_NBR,
                                            DataType.TASK_PROTOCOL_PIN_NUMBER,
                                            DataType.TASK_PROTOCOL_PIN_TYPE,
                                            DataType.TASK_PROTOCOL_PIN_VALUE,
                                            //TaskProceedDeviceDiagnosticStepLatch
                                            DataType.TASK_PROTOCOL_DIAGNOSTIC_SERIAL_NBR,
                                            //TaskProceedInstalledGasmeterRadioQualityVerificationStepLatch
                                            DataType.TASK_PROTOCOL_INSTALLED_GASMETER_RADIO_QUALITY_VERIFICATION_DEVICE_SERIAL_NBR,
                                            DataType.DEVICE_LATITUDE,
                                            DataType.DEVICE_LONGITUDE,
                                            DataType.RADIO_QUALITY,
                                            //TaskProceedInstalledMeterAdapterStepLatch
                                            DataType.TASK_PROTOCOL_INSTALLED_METER_ADAPTER_SERIAL_NBR,
                                            DataType.METER_SERIAL_NUMBER,
                                            DataType.DEVICE_SERIAL_NBR,
                                            DataType.LOCATION_CID,
                                            //TaskProceedDeviceValidImpulseCountTestStepLatch
                                            DataType.TASK_PROTOCOL_DEVICE_VALID_IMPULSE_COUNT_TEST_SERIAL_NBR,
                                            DataType.METER_SERIAL_NUMBER,
                                            DataType.METER_NAME,
                                            DataType.TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_PRIOR_TEST,
                                            DataType.TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_AFTER_TEST,
                                            DataType.TASK_PROTOCOL_DEVICE_TEST_DURATION,
                                            //TaskProceedInstalledWatermeterStepLatch
                                            DataType.TASK_PROTOCOL_INSTALLED_WATERMETER_DEVICE_SERIAL_NBR,
                                            DataType.DEVICE_COUNTER_COEFFICIENT,
                                            DataType.WATER_COUNTER,
                                            DataType.REVERSE_WATER_COUNTER,
                                            DataType.MAX_HOUR_FLOW,
                                            DataType.DEVICE_CID,
                                            DataType.METER_SERIAL_NUMBER,
                                            DataType.DEVICE_MAX_TEMPERATURE,
                                            DataType.DEVICE_MIN_TEMPERATURE,
                                            DataType.DEVICE_INDUCTIVE_SENSOR_WATER_METER_TYPE,
                                            DataType.DEVICE_UP_EVENT_MAX_HOUR_FLOW,
                                            DataType.DEVICE_UP_EVENT_MAX_TEMPORAL_FLOW,
                                            DataType.DEVICE_UP_EVENT_SABOTAGE_CONTACT,
                                            DataType.DEVICE_UP_EVENT_REMOVE,
                                            DataType.DEVICE_UP_EVENT_REVERSE_FLOW,
                                            DataType.DEVICE_UP_EVENT_BATTERY_LOW,
                                            DataType.DEVICE_UP_EVENT_VALVE_LEAK_TEST_FAILED,
                                            DataType.DEVICE_UP_EVENT_EXTREME_TEMP,
                                            DataType.DEVICE_DOWN_EVENT_MAX_HOUR_FLOW,
                                            DataType.DEVICE_DOWN_EVENT_MAX_TEMPORAL_FLOW,
                                            DataType.DEVICE_DOWN_EVENT_SABOTAGE_CONTACT,
                                            DataType.DEVICE_DOWN_EVENT_REMOVE,
                                            DataType.DEVICE_DOWN_EVENT_REVERSE_FLOW,
                                            DataType.DEVICE_DOWN_EVENT_BATTERY_LOW,
                                            DataType.DEVICE_DOWN_EVENT_VALVE_LEAK_TEST_FAILED,
                                            DataType.DEVICE_DOWN_EVENT_EXTREME_TEMP,
                                            DataType.TASK_PROTOCOL_WATERMETER_MEASURING_DIRECTIVE,
                                            //TaskProceedHubInstallationStepLatch
                                            DataType.TASK_PROTOCOL_HUB_INSTALLATION_DEVICE_SERIAL_NBR,
                                            DataType.LOCATION_CID,
                                            DataType.LOCATION_DEVICE_INSTALLATION_PLACE,
                                            DataType.LOCATION_DEVICE_INSTALLATION_PLACE_NOTE,
                                            DataType.TASK_PROTOCOL_INSTALLATION_FORM_TYPE,
                                            //TaskProceedWalkedByReadoutStepLatch
                                            DataType.TASK_PROTOCOL_WALKED_BY_READOUT_DEVICE_SERIAL_NBR,
                                            DataType.SITA_WALKEDBY_FRAME_TYPE,
                                            DataType.WALKEDBY_LATITUDE,
                                            DataType.WALKEDBY_LONGITUDE,
                                            DataType.SITA_WALKEDBY_FRAME,
                                            DataType.SITA_CONTAINS_ENCRYPTION_KEYS,
                                            DataType.TASK_INSTALLATION_PROTOCOL_WALKEDBY_MEASUREMENT_VALUE,
                        };
            #endregion
            List<OpTaskData> protocolContent = dataProvider.GetTaskDataFilter(IdTask: new int[] { idTask },
                        IdDataType: protocolDataTypes.ToArray());
            if (protocolContent != null && protocolContent.Count > 0)
            {
                foreach (OpTaskData tdTimeStampItem in protocolContent.Where(t => t.IdDataType == DataType.TASK_PROTOCOL_TIMESTAMP && t.Value != null))
                {
                    DateTime timeStamp = dataProvider.DateTimeNow;
                    if (!DateTime.TryParse(tdTimeStampItem.Value.ToString(), out timeStamp))
                        continue;
                    string details = "";
                    Enums.TaskVisitStepType stepType = Enums.TaskVisitStepType.Unknown;
                    object tag = null;

                    List<OpTaskData> relatedProtocolContent = protocolContent.Where(t => t.Index == tdTimeStampItem.Index).ToList();
                    if (tdTimeStampItem.Index == 0)
                    {
                        List<long> idDataTypeToIgnore = new List<long>();//istnieją data typy, które na indeksach 0 zawierają wartości aktualne i nie chcemy żeby nam mieszały
                        idDataTypeToIgnore.Add(DataType.TASK_ARRANGED_FITTER_VISIT_DATE);
                        idDataTypeToIgnore.Add(DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR);
                        idDataTypeToIgnore.Add(DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR);
                        idDataTypeToIgnore.Add(DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE);
                        idDataTypeToIgnore.Add(DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT);
                        idDataTypeToIgnore.Add(DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES);
                        idDataTypeToIgnore.Add(DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR);
                        relatedProtocolContent.RemoveAll(d => d.IdDataType.In(idDataTypeToIgnore.ToArray()));
                    }
                    if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_AIUT_DEVICE_MOUNT_ON_MAGAZINE
                                                        || t.IdDataType == DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_MOUNT_ON_MAGAZINE))
                    {
                        #region DeviceUninstall

                        stepType = Enums.TaskVisitStepType.ElementUninstall;
                        OpTaskData tdSerialNbrItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_AIUT_DEVICE_MOUNT_ON_MAGAZINE
                                                                                   || t.IdDataType == DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_MOUNT_ON_MAGAZINE);
                        OpTaskData tdIdArticleItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_ID_ARTICLE && t.Value != null);
                        OpArticle article = null;
                        if (tdIdArticleItem != null)
                        {
                            long idArticle = 0;
                            if (long.TryParse(tdIdArticleItem.Value.ToString(), out idArticle))
                            {
                                article = dataProvider.GetArticle(idArticle);
                            }

                        }

                        details = String.Format("{0}: {1}, {2}: {3}", ResourcesText.UnistalledDevice,
                                                                (article != null ? article.ToString() : ""),
                                                                ResourcesText.SerialNumber,
                                                                (tdSerialNbrItem != null && tdSerialNbrItem.Value != null ? tdSerialNbrItem.Value.ToString() : ""));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_AIUT_DEVICE_UNMOUNT_FROM_MAGAZINE
                                                             || t.IdDataType == DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_UNMOUNT_FROM_MAGAZINE))
                    {
                        #region DeviceInstallation

                        stepType = Enums.TaskVisitStepType.ElementInstallation;
                        OpTaskData tdSerialNbrItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_AIUT_DEVICE_UNMOUNT_FROM_MAGAZINE
                                                                                   || t.IdDataType == DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_UNMOUNT_FROM_MAGAZINE);
                        OpTaskData tdIdArticleItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_ID_ARTICLE && t.Value != null);
                        OpArticle article = null;
                        if (tdIdArticleItem != null)
                        {
                            long idArticle = 0;
                            if (long.TryParse(tdIdArticleItem.Value.ToString(), out idArticle))
                            {
                                article = dataProvider.GetArticle(idArticle);
                            }

                        }

                        details = String.Format("{0}: {1}, {2}: {3}.", ResourcesText.InstalledDevice,
                                                                (article != null ? article.ToString() : ""),
                                                                ResourcesText.SerialNumber,
                                                                (tdSerialNbrItem != null && tdSerialNbrItem.Value != null ? tdSerialNbrItem.Value.ToString() : ""));

                        long alevelSerialNbr = 0;
                        if (article != null && article.ToString().ToUpper().Contains("ALEVEL")
                            && tdSerialNbrItem != null && tdSerialNbrItem.Value != null && long.TryParse(tdSerialNbrItem.Value.ToString(), out alevelSerialNbr))
                        {
                            OpLocationEquipment leItem = dataProvider.GetLocationEquipmentFilter(loadCustomData: false, loadNavigationProperties: false,
                                                                                                 SerialNbr: new long[] { alevelSerialNbr },
                                                                                                 customWhereClause: "[END_TIME] is null and [ID_METER] is not null").FirstOrDefault();
                            if (leItem != null)
                            {
                                OpMeter mItem = dataProvider.GetMeter(leItem.IdMeter.Value);
                                if (mItem != null)
                                {
                                    details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.MeterTank, mItem.ToString());
                                }
                            }
                        }

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_EXCHANGE))
                    {
                        #region DeviceExchange

                        stepType = Enums.TaskVisitStepType.DeviceExchange;

                        //pary wymienianych urzadzen zapisywane sa na tych samych indeksach, najpierw usuwane potem wprowadzane
                        List<OpTaskData> tdExchangedDevice = relatedProtocolContent.Where(d => d.IdDataType == DataType.TASK_PROTOCOL_DEVICE_EXCHANGE).ToList();
                        OpTaskData tdOldDevice = tdExchangedDevice.First();
                        tdExchangedDevice = tdExchangedDevice.OrderBy(t => t.IdTaskData).ToList();
                        if (tdExchangedDevice.Exists(d => d.IdDataType == DataType.TASK_PROTOCOL_DEVICE_EXCHANGE &&
                                                       d.Index == tdOldDevice.Index &&
                                                       d.IdTaskData > tdOldDevice.IdTaskData))
                        {
                            OpTaskData tdNewDevice = tdExchangedDevice.Find(d => d.IdDataType == DataType.TASK_PROTOCOL_DEVICE_EXCHANGE &&
                                                       d.Index == tdOldDevice.Index &&
                                                       d.IdTaskData > tdOldDevice.IdTaskData);
                            long oldSerialNbr = 0;
                            long newSerialNbr = 0;
                            if (tdOldDevice.Value != null && tdNewDevice.Value != null)
                            {
                                if (long.TryParse(tdOldDevice.Value.ToString(), out oldSerialNbr) && long.TryParse(tdNewDevice.Value.ToString(), out newSerialNbr))
                                {
                                    OpDevice exchanged = dataProvider.GetDevice(oldSerialNbr);
                                    OpDevice newDevice = dataProvider.GetDevice(newSerialNbr);
                                    details = String.Format("{0}: {1} ({2}), {3}: {4} ({5})", ResourcesText.Device, exchanged.SerialNbr, exchanged.DeviceOrderNumber.ToString(),
                                                                                                            ResourcesText.ExchangedTo, newDevice.SerialNbr, newDevice.DeviceOrderNumber.ToString());
                                }
                            }
                        }

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_MOVE && t.Value != null))
                    {
                        #region DeviceMove

                        stepType = Enums.TaskVisitStepType.DeviceMove;
                        OpTaskData tdSerialNbrItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_MOVE && t.Value != null);
                        OpTaskData tdIdDestinationLocationItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_MOVE && t.Value != null);
                        long movedDeviceSerialNbr = 0;
                        long idDestinationLocation = 0;
                        if (tdSerialNbrItem != null && tdIdDestinationLocationItem != null
                            && long.TryParse(tdSerialNbrItem.Value.ToString(), out movedDeviceSerialNbr)
                            && long.TryParse(tdIdDestinationLocationItem.Value.ToString(), out idDestinationLocation))
                        {
                            OpDevice movedDevice = dataProvider.GetDevice(movedDeviceSerialNbr);
                            OpLocation destinationLocation = dataProvider.GetLocation(idDestinationLocation);
                            if (movedDevice != null && destinationLocation != null)
                            {
                                details = String.Format("{0}: {1} ({2}), {3}: {4}", ResourcesText.Device, movedDevice.SerialNbr, movedDevice.DeviceOrderNumber.ToString(),
                                                                                           ResourcesText.MovedTo, destinationLocation.ToString());
                            }
                        }

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_ADDED_METER_SERIAL_NBR && t.Value != null))
                    {
                        #region MeterAddtion

                        stepType = Enums.TaskVisitStepType.MeterAddtion;
                        OpTaskData tdNewMeterItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ADDED_METER_SERIAL_NBR && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.NewTankMeterSerialNbr, tdNewMeterItem.Value.ToString());

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_ID_ACTION && t.Value != null))
                    {
                        #region ActionSending

                        stepType = Enums.TaskVisitStepType.ActionSending;
                        OpTaskData tdIdActionImrServerItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ID_ACTION && t.Value != null);
                        OpTaskData tdActionTypeItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ACTION_TYPE && t.Value != null);
                        OpTaskData tdActionDeviceItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ACTION_DEVICE && t.Value != null);
                        OpTaskData tdActionStatusItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ACTION_STATUS && t.Value != null);
                        OpTaskData tdActionTextItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ACTION_TEXT && t.Value != null);

                        string actionType = (tdActionTypeItem != null ? tdActionTypeItem.Value.ToString() : "");
                        string actionStatus = (tdActionStatusItem != null ? tdActionStatusItem.Value.ToString() : "");
                        string actionText = (tdActionTextItem != null ? tdActionTextItem.Value.ToString() : "");
                        long actionDeviceSerialNbr = 0;
                        int idActionStatus = 0;
                        if (int.TryParse(actionStatus, out idActionStatus))
                        {
                            actionStatus = dataProvider.GetActionStatus(idActionStatus).ToString();
                        }
                        if (tdActionDeviceItem != null)
                        {
                            long.TryParse(tdActionDeviceItem.Value.ToString(), out actionDeviceSerialNbr);
                        }

                        details = (String.IsNullOrEmpty(actionType) ? "" : String.Format("{0} {1}.", ResourcesText.ActionType, actionType) + Environment.NewLine);
                        details += (actionDeviceSerialNbr == 0 ? "" : String.Format("{0} {1}.", ResourcesText.SentTo, actionDeviceSerialNbr) + Environment.NewLine);
                        details += (String.IsNullOrEmpty(actionStatus) ? "" : String.Format("{0} {1} ", ResourcesText.Status, actionStatus) + Environment.NewLine);
                        details += (String.IsNullOrEmpty(actionText) ? "" : String.Format("{0} {1} ", ResourcesText.ReceivedReply, actionText) + Environment.NewLine);

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_DATE && t.Value != null)
                        || relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT && t.Value != null))
                    {
                        #region PlannedVisitDate

                        stepType = Enums.TaskVisitStepType.PlannedVisitDate;
                        OpTaskData tdVisitDate = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_DATE && t.Value != null);
                        OpTaskData tdVisitHourBegin = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR && t.Value != null);
                        OpTaskData tdVisitHourEnd = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR && t.Value != null);
                        OpTaskData tdScheduleMode = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE && t.Value != null);
                        OpTaskData tdScheduleResult = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT && t.Value != null);
                        OpTaskData tdScheduleNotes = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES && t.Value != null);
                        OpTaskData tdScheduleIdOperator = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR && t.Value != null);

                        DateTime? fitterVisitDate = null;
                        int beginHour = -1;
                        int endHour = -1;
                        int scheduleMode = -1;
                        int appointmentResult = -1;
                        string scheduleNotes = null;
                        int scheduleIdOperator = -1;
                        if (tdVisitDate != null && tdVisitDate.Value != null)
                        {
                            DateTime fitterVisitDateTmp = DateTime.MinValue;
                            if (DateTime.TryParse(tdVisitDate.Value.ToString(), out fitterVisitDateTmp))
                                fitterVisitDate = dataProvider.ConvertUtcTimeToTimeZone(fitterVisitDateTmp);
                        }
                        if (tdVisitHourBegin != null && tdVisitHourBegin.Value != null)
                            int.TryParse(tdVisitHourBegin.Value.ToString(), out beginHour);
                        if (tdVisitHourEnd != null && tdVisitHourEnd.Value != null)
                            int.TryParse(tdVisitHourEnd.Value.ToString(), out endHour);
                        if (tdScheduleMode != null && tdScheduleMode.Value != null)
                            int.TryParse(tdScheduleMode.Value.ToString(), out scheduleMode);
                        if (tdScheduleResult != null && tdScheduleResult.Value != null)
                            int.TryParse(tdScheduleResult.Value.ToString(), out appointmentResult);
                        if (tdScheduleNotes != null && tdScheduleNotes.Value != null)
                            scheduleNotes = tdScheduleNotes.Value.ToString();
                        if (tdScheduleIdOperator != null && tdScheduleIdOperator.Value != null)
                            int.TryParse(tdScheduleIdOperator.Value.ToString(), out scheduleIdOperator);

                        details = String.Format("{0}: {1}", ResourcesText.PlannedVisitDate, (fitterVisitDate.HasValue ? fitterVisitDate.Value.ToString("yyyy-MM-dd") : "-")) + Environment.NewLine;
                        if (beginHour != -1 && endHour != -1)
                            details += String.Format("{0}: {1}, {2}: {3}", ResourcesText.FromHour, beginHour, ResourcesText.ToHour, endHour) + Environment.NewLine;
                        if (scheduleMode != -1)
                        {
                            OpEnumObject gotsmItem = new OpEnumObject((Enums.TaskAppointmentScheduleMode)scheduleMode);
                            if (gotsmItem != null)
                                details += String.Format("{0}: {1}", ResourcesText.PlanMode, gotsmItem.ToString()) + Environment.NewLine;
                        }
                        if (appointmentResult != -1)
                        {
                            OpEnumObject gotarItem = new OpEnumObject((Enums.TaskAppointmentResult)appointmentResult);
                            if (gotarItem != null)
                                details += String.Format("{0}: {1}", ResourcesText.Result, gotarItem.ToString()) + Environment.NewLine;
                        }
                        if (!String.IsNullOrEmpty(scheduleNotes))
                            details += String.Format("{0}: {1}", ResourcesText.Notes, scheduleNotes) + Environment.NewLine;
                        if (scheduleIdOperator != -1)
                        {
                            OpOperator oItem = dataProvider.GetOperator(scheduleIdOperator);
                            if (oItem != null)
                                details += String.Format("{0}: {1}", ResourcesText.User, oItem.ToString());
                        }


                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_CURRENT_METER_READOUT && t.Value != null))
                    {
                        #region CurrentMeterReadout

                        stepType = Enums.TaskVisitStepType.CurrentMeterReadout;
                        OpTaskData tdMeterItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_METER_SERIAL_NBR && t.Value != null);
                        OpTaskData tdCurrentReadoutItem = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_CURRENT_METER_READOUT && t.Value != null);
                        OpTaskData tdAlevelSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ALEVEL_SERIAL_NBR && t.Value != null);

                        if (tdMeterItem != null && tdMeterItem.Value != null && !String.IsNullOrEmpty(tdMeterItem.Value.ToString())
                            && tdCurrentReadoutItem != null && tdCurrentReadoutItem.Value != null && !String.IsNullOrEmpty(tdCurrentReadoutItem.Value.ToString()))
                            details = String.Format("{0} {1}: {2}", ResourcesText.CurrentReadoutFor, tdMeterItem.Value.ToString(), tdCurrentReadoutItem.Value.ToString());
                        long alevelSerialnbr = 0;
                        if (tdAlevelSerialNbr != null && tdAlevelSerialNbr.Value != null && long.TryParse(tdAlevelSerialNbr.Value.ToString(), out alevelSerialnbr))
                        {
                            if (alevelSerialnbr > 0)
                            {
                                details += Environment.NewLine + String.Format("ALEVEL: {0}", alevelSerialnbr);
                            }
                        }

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_NAME && t.Value != null))
                    {
                        #region PhotoDone

                        stepType = Enums.TaskVisitStepType.PhotoDone;
                        OpTaskData tdPhotoDoneName = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_NAME && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.PhotoDone, tdPhotoDoneName.Value.ToString());
                        tag = tdPhotoDoneName;

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NAME && t.Value != null))
                    {
                        #region PhotoDoneProblem

                        stepType = Enums.TaskVisitStepType.PhotoDoneProblem;
                        OpTaskData tdPhotoDoneProblemName = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NAME && t.Value != null);
                        OpTaskData tdPhotoDoneProblemNote = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NOTE && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.PhotoDone, tdPhotoDoneProblemName.Value.ToString());
                        if (tdPhotoDoneProblemNote != null && tdPhotoDoneProblemNote.Value != null)
                        {
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.Details, tdPhotoDoneProblemNote.Value.ToString());
                        }

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_NAME && t.Value != null))
                    {
                        #region PhotoUploaded

                        stepType = Enums.TaskVisitStepType.PhotoUploaded;
                        OpTaskData tdPhotoUploadName = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_NAME && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.PhotoUploaded, tdPhotoUploadName.Value.ToString());

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NAME && t.Value != null))
                    {
                        #region PhotoDoneProblem

                        stepType = Enums.TaskVisitStepType.PhotoUploadedProblem;
                        OpTaskData tdPhotoUploadedProblemName = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NAME && t.Value != null);
                        OpTaskData tdPhotoUploadedProblemNote = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NOTE && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.PhotoUploadedProblem, tdPhotoUploadedProblemName.Value.ToString());
                        if (tdPhotoUploadedProblemNote != null && tdPhotoUploadedProblemNote.Value != null)
                        {
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.Details, tdPhotoUploadedProblemNote.Value.ToString());
                        }

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_EXPORT_STATUS && t.Value != null))
                    {
                        #region Export

                        stepType = Enums.TaskVisitStepType.Export;
                        OpTaskData tdExportStatus = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_STATUS && t.Value != null);
                        OpTaskData tdExportNote = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_NOTE && t.Value != null);
                        OpTaskData tdExportOperator = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ID_OPERATOR && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.Export, new OpEnumObject((Enums.TaskExportStepResult)Convert.ToInt32(tdExportStatus.Value)));
                        if (tdExportOperator != null && tdExportOperator.Value != null)
                        {
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.Operator, dataProvider.GetOperator(Convert.ToInt32(tdExportOperator.Value)));
                        }
                        if (tdExportNote != null && tdExportNote.Value != null)
                        {
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.Details, tdExportNote.Value.ToString());
                        }


                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_EXPORT_STEP && t.Value != null))
                    {
                        #region ExportStep

                        stepType = Enums.TaskVisitStepType.ExportStep;
                        OpTaskData tdExportStep = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_STEP && t.Value != null);
                        OpTaskData tdExportStepStatus = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_STEP_STATUS && t.Value != null);
                        OpTaskData tdExportStepNote = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_STEP_NOTE && t.Value != null);
                        OpTaskData tdExportStepIdAction = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_ID_ACTION && t.Value != null);
                        OpTaskData tdExportOperator = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ID_OPERATOR && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.ExportStep, new OpEnumObject((Enums.TaskExportStepType)Convert.ToInt32(tdExportStep.Value))) + Environment.NewLine;
                        details += String.Format("{0}: {1}", ResourcesText.Result, new OpEnumObject((Enums.TaskExportStepResult)Convert.ToInt32(tdExportStepStatus.Value)));
                        if (tdExportOperator != null && tdExportOperator.Value != null)
                        {
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.Operator, dataProvider.GetOperator(Convert.ToInt32(tdExportOperator.Value)));
                        }
                        if (tdExportStepIdAction != null && tdExportStepIdAction.Value != null)
                        {
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.IdAction, tdExportStepIdAction.Value.ToString());
                        }
                        if (tdExportStepNote != null && tdExportStepNote.Value != null)
                        {
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.Details, tdExportStepNote.Value.ToString());
                        }

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_EXPORT_ARCH_LOCATION_ID_IMR_SERVER && t.Value != null))
                    {
                        #region MappingsChange

                        stepType = Enums.TaskVisitStepType.MappingsChanged;
                        OpTaskData tdMappingsIdImrServer = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_ARCH_LOCATION_ID_IMR_SERVER && t.Value != null);
                        OpTaskData tdMappingsIdLocation = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_EXPORT_ARCH_LOCATION_SOURCE_SERVER_ID && t.Value != null);
                        OpTaskData tdExportOperator = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ID_OPERATOR && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.ArchiveServer, dataProvider.GetImrServer(Convert.ToInt32(tdMappingsIdImrServer.Value))) + Environment.NewLine;
                        details += String.Format("{0}: {1}", ResourcesText.ArchiveIdLocation, tdMappingsIdLocation.Value);


                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TAKS_OKO_SERIAL_NBR && t.Value != null))
                    {
                        #region HeatOkoStepLatch

                        stepType = Enums.TaskVisitStepType.HeatOkoInstallation;
                        OpTaskData tdOkoSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TAKS_OKO_SERIAL_NBR && t.Value != null);
                        OpTaskData tdTMobileSignalLevel = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_T_MOBILE_SIGNAL_LEVEL && t.Value != null);
                        OpTaskData tdPlusSignalLevel = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PLUS_SIGNAL_LEVEL && t.Value != null);
                        OpTaskData tdOrangeSignalLevel = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_ORANGE_SIGNAL_LEVEL && t.Value != null);
                        OpTaskData tdPlaySignalLevel = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PLAY_SIGNAL_LEVEL && t.Value != null);

                        details = String.Format("{0} OKO: {1}", ResourcesText.Installed, (tdOkoSerialNbr != null && tdOkoSerialNbr.Value != null ? tdOkoSerialNbr.Value.ToString() : "-"));
                        if (tdTMobileSignalLevel != null)
                            details += Environment.NewLine + String.Format("{0} {1}: {2}", ResourcesText.SignalLevel, "T-Mobile", (tdTMobileSignalLevel != null && tdTMobileSignalLevel.Value != null ? tdTMobileSignalLevel.Value.ToString() : "-"));
                        if (tdPlusSignalLevel != null)
                            details += Environment.NewLine + String.Format("{0} {1}: {2}", ResourcesText.SignalLevel, "Plus", (tdPlusSignalLevel != null && tdPlusSignalLevel.Value != null ? tdPlusSignalLevel.Value.ToString() : "-"));
                        if (tdPlaySignalLevel != null)
                            details += Environment.NewLine + String.Format("{0} {1}: {2}", ResourcesText.SignalLevel, "Play", (tdPlaySignalLevel != null && tdPlaySignalLevel.Value != null ? tdPlaySignalLevel.Value.ToString() : "-"));
                        if (tdOrangeSignalLevel != null)
                            details += Environment.NewLine + String.Format("{0} {1}: {2}", ResourcesText.SignalLevel, "Orange", (tdOrangeSignalLevel != null && tdOrangeSignalLevel.Value != null ? tdOrangeSignalLevel.Value.ToString() : "-"));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_WATERMETER_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region WatermeterInstallation

                        stepType = Enums.TaskVisitStepType.WatermeterInstallation;
                        OpTaskData tdWatermeterDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_WATERMETER_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdWatermeterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TAKS_WATERMETER_SERIAL_NBR && t.Value != null);
                        OpTaskData tdReplacedWatermeterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_REPLACED_WATER_METER_SERIAL_NUMBER && t.Value != null);
                        OpTaskData tdWatermeterNominalFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_INSTALLATION_PROTOCOL_WATERMETER_NOMINAL_FLOW && t.Value != null);
                        OpTaskData tdWatermeterIndications = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_INSTALLATION_PROTOCOL_WATERMETER_INDICATIONS_SUMMARY && t.Value != null);

                        details = String.Format("{0}: {1}, {2}: {3}, Q3: {4}", ResourcesText.Device,
                                                                               (tdWatermeterDeviceSerialNbr != null && tdWatermeterDeviceSerialNbr.Value != null ? tdWatermeterDeviceSerialNbr.Value.ToString() : "-"),
                                                                               ResourcesText.Watermeter,
                                                                               (tdWatermeterSerialNbr != null && tdWatermeterSerialNbr.Value != null ? tdWatermeterSerialNbr.Value.ToString() : "-"),
                                                                               (tdWatermeterNominalFlow != null && tdWatermeterNominalFlow.Value != null ? tdWatermeterNominalFlow.Value.ToString() : "-"));
                        if (tdReplacedWatermeterSerialNbr != null)
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.ReplacedWatermeter, (tdReplacedWatermeterSerialNbr != null && tdReplacedWatermeterSerialNbr.Value != null ? tdReplacedWatermeterSerialNbr.Value.ToString() : "-"));
                        if (tdWatermeterIndications != null)
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.WatermeterIndications, (tdWatermeterIndications != null && tdWatermeterIndications.Value != null ? tdWatermeterIndications.Value.ToString() : "-"));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_CONVERTER_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region ConverterInstallation

                        stepType = Enums.TaskVisitStepType.ConverterInstallation;
                        OpTaskData tdConverterDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_CONVERTER_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdConverterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_CONVERTER_SERIAL_NBR && t.Value != null);
                        OpTaskData tdConverterIndications = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_INSTALLATION_PROTOCOL_INDICATIONS_SUMMARY && t.Value != null);

                        details = String.Format("{0}: {1}, {2}: {3}", ResourcesText.Device,
                                                                               (tdConverterDeviceSerialNbr != null && tdConverterDeviceSerialNbr.Value != null ? tdConverterDeviceSerialNbr.Value.ToString() : "-"),
                                                                               ResourcesText.Converter,
                                                                               (tdConverterSerialNbr != null && tdConverterSerialNbr.Value != null ? tdConverterSerialNbr.Value.ToString() : "-"));
                        if (tdConverterIndications != null)
                            details += Environment.NewLine + String.Format("{0}: {1}", ResourcesText.ConverterIndications, (tdConverterIndications != null && tdConverterIndications.Value != null ? tdConverterIndications.Value.ToString() : "-"));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_BATTERY_CHANGE_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region BatteryChange

                        stepType = Enums.TaskVisitStepType.BatteryChange;
                        OpTaskData tdBatteryChangeDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_BATTERY_CHANGE_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdOldBatterySerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_BATTERY_CHANGE_OLD_SERIAL_NBR);
                        OpTaskData tdNewBatterySerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_BATTERY_CHANGE_NEW_SERIAL_NBR && t.Value != null);
                        OpTaskData tdOldBatteryEfficient = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_BATTERY_CHANGE_OLD_BATTERY_EFFICIENT && t.Value != null);

                        bool oldBatteryEfficient = false;
                        if (!bool.TryParse(tdOldBatteryEfficient.Value.ToString(), out oldBatteryEfficient))
                            oldBatteryEfficient = false;

                        details = String.Format("{0}: {1}", ResourcesText.Device,
                                                            (tdBatteryChangeDeviceSerialNbr != null && tdBatteryChangeDeviceSerialNbr.Value != null ? tdBatteryChangeDeviceSerialNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0}: {1} ({2})", ResourcesText.OldBatterySerialNbr,
                                                                   (tdOldBatterySerialNbr != null && tdOldBatterySerialNbr.Value != null ? tdOldBatterySerialNbr.Value.ToString() : "-"),
                                                                   (tdOldBatteryEfficient != null && tdOldBatteryEfficient.Value != null && oldBatteryEfficient ? ResourcesText.Efficient : ResourcesText.NotEfficient));
                        details += Environment.NewLine;
                        details += String.Format("{0}: {1}", ResourcesText.NewBatterySerialNbr,
                                                            (tdNewBatterySerialNbr != null && tdNewBatterySerialNbr.Value != null ? tdNewBatterySerialNbr.Value.ToString() : "-"));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_RESULT && t.Value != null))
                    {
                        #region ValveTestResult

                        stepType = Enums.TaskVisitStepType.ValveTestResult;
                        OpTaskData tdValveOperationDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdValveOperationResult = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_RESULT && t.Value != null);
                        OpTaskData tdValveOperationStatus = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS_NAME && t.Value != null);

                        int valveOperationResult = 0;
                        if (!int.TryParse(tdValveOperationResult.Value.ToString(), out valveOperationResult))
                            valveOperationResult = 0;

                        details = String.Format("{0}: {1}", ResourcesText.Device,
                                                            (tdValveOperationDeviceSerialNbr != null && tdValveOperationDeviceSerialNbr.Value != null ? tdValveOperationDeviceSerialNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1} {2} {3}", ResourcesText.ValveOperation,
                                                                   (tdValveOperationStatus != null && tdValveOperationStatus.Value != null ? tdValveOperationStatus.Value.ToString() : "-"),
                                                                   ResourcesText.Result,
                                                                   (tdValveOperationResult != null && tdValveOperationResult.Value != null && valveOperationResult == 1 ? ResourcesText.Success : ResourcesText.Failure));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS && t.Value != null))
                    {
                        #region ValveStateChange

                        stepType = Enums.TaskVisitStepType.ValveStateChange;
                        OpTaskData tdValveOperationDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdValveStateChange = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS && t.Value != null);
                        OpTaskData tdValveOperationStatus = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS_NAME && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.Device,
                                                            (tdValveOperationDeviceSerialNbr != null && tdValveOperationDeviceSerialNbr.Value != null ? tdValveOperationDeviceSerialNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1}", ResourcesText.ValveOperation,
                                                                   (tdValveOperationStatus != null && tdValveOperationStatus.Value != null ? tdValveOperationStatus.Value.ToString() : "-"));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_REPROGRAM_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region DeviceReprogram

                        stepType = Enums.TaskVisitStepType.DeviceReprogram;
                        OpTaskData tdReprogramDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_REPROGRAM_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdFileName = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_REPROGRAM_FILE_NAME && t.Value != null);
                        OpTaskData tdOldSoftVersionNumber = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_REPROGRAM_OLD_VERSION_NUMBER && t.Value != null);
                        OpTaskData tdNewSoftVersionNumber = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_REPROGRAM_NEW_VERSION_NUMBER && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.FlashAnalyze,//ResourcesText.ReprogrammedDevice,
                                                            (tdReprogramDeviceSerialNbr != null && tdReprogramDeviceSerialNbr.Value != null ? tdReprogramDeviceSerialNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1}", ResourcesText.OldSoftwareVersion,
                                                                   (tdOldSoftVersionNumber != null && tdOldSoftVersionNumber.Value != null ? tdOldSoftVersionNumber.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1}", ResourcesText.NewSoftwareVersion,
                                                                   (tdNewSoftVersionNumber != null && tdNewSoftVersionNumber.Value != null ? tdNewSoftVersionNumber.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1}", ResourcesText.NewSoftwareVersionFileName,
                                                                   (tdFileName != null && tdFileName.Value != null ? tdFileName.Value.ToString() : "-"));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_SIM_CARD_TEST_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region SimCardTest

                        stepType = Enums.TaskVisitStepType.SimCardTest;
                        OpTaskData tdSimCardTestDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_SIM_CARD_TEST_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdSimCardPhoneNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_SIM_CARD_TEST_PHONE_NBR && t.Value != null);
                        OpTaskData tdSimCardErrorType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_SIM_CARD_TEST_ERROR_TYPE && t.Value != null);
                        int errorType = 0;
                        if (!int.TryParse(tdSimCardErrorType.Value.ToString(), out errorType))
                            errorType = 0;

                        details = String.Format("{0}: {1}", ResourcesText.Device,
                                                            (tdSimCardTestDeviceSerialNbr != null && tdSimCardTestDeviceSerialNbr.Value != null ? tdSimCardTestDeviceSerialNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1}", ResourcesText.PhoneNbr,
                                                                   (tdSimCardPhoneNbr != null && tdSimCardPhoneNbr.Value != null ? tdSimCardPhoneNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        if (errorType != (int)Enums.SIMCardError.NoError)
                        {
                            details += String.Format("{0} {1}", ResourcesText.ErrorType, SimCardComponent.GetSimCardErrorText((Enums.SIMCardError)errorType));
                        }
                        else
                            details += ResourcesText.NoSimCardErrors;

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_CRITICAL_ERROR_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region DeviceCriticalErrorTest

                        stepType = Enums.TaskVisitStepType.DeviceCriticalErrorTest;
                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CRITICAL_ERROR_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdErrorType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CRITICAL_ERROR_TYPE && t.Value != null);
                        OpTaskData tdErrorValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CRITICAL_ERROR_VALUE);

                        long idErrorDataType = 0;
                        if (!long.TryParse(tdErrorType.Value.ToString(), out idErrorDataType))
                            idErrorDataType = 0;
                        OpDataType errorDataType = dataProvider.GetDataType(idErrorDataType);

                        details = String.Format("{0}: {1}", ResourcesText.Device,
                                                            (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null ? tdDeviceSerialNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1}", ResourcesText.Type,
                                                                   (errorDataType != null ? errorDataType.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("{0} {1}", ResourcesText.Value,
                                                                   (tdErrorValue != null && tdErrorValue.Value != null ? tdErrorValue.Value.ToString() : "-"));

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.METER_TANK_IS_UNDERGROUND && t.Value != null))
                    {
                        #region TankPosition

                        stepType = Enums.TaskVisitStepType.TankPosition;
                        OpTaskData tdMeterIsUnderGround = relatedProtocolContent.Find(t => t.IdDataType == DataType.METER_TANK_IS_UNDERGROUND && t.Value != null);
                        OpTaskData tdMeterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_METER_SERIAL_NBR && t.Value != null);
                        OpTaskData tdAlevelSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ALEVEL_SERIAL_NBR && t.Value != null);

                        details = String.Format("{0}: {1}", ResourcesText.TankIsUndergound, (tdMeterIsUnderGround != null && tdMeterIsUnderGround.Value != null && Convert.ToBoolean(tdMeterIsUnderGround.Value.ToString()) ? ResourcesText.Yes : ResourcesText.No));
                        details += Environment.NewLine;
                        details += String.Format("{0}: {1}", ResourcesText.TankSerialNbr,
                                                            (tdMeterSerialNbr != null && tdMeterSerialNbr.Value != null ? tdMeterSerialNbr.Value.ToString() : "-"));
                        details += Environment.NewLine;
                        details += String.Format("ALEVEL {0} {1}", ResourcesText.SerialNBR,
                                                                   (tdAlevelSerialNbr != null && tdAlevelSerialNbr.Value != null ? tdAlevelSerialNbr.Value.ToString() : "-"));


                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_ARCHIVE_READ_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region ArchiveRead

                        stepType = Enums.TaskVisitStepType.ArchiveRead;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ARCHIVE_READ_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdArchiveReadDateFrom = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ARCHIVE_READ_FROM && t.Value != null);
                        OpTaskData tdArchiveReadDateTo = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ARCHIVE_READ_TO && t.Value != null);
                        OpTaskData tdArchiveReadType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ARCHIVE_READ_TYPE && t.Value != null);
                        OpTaskData tdArchiveReadFrameCount = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_ARCHIVE_READ_FRAME_COUNT && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.From, (tdArchiveReadDateFrom != null && tdArchiveReadDateFrom.Value != null) ? DateTime.Parse(tdArchiveReadDateFrom.Value.ToString()).ToString("yyyy-MM-dd HH:mm") : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.To, (tdArchiveReadDateTo != null && tdArchiveReadDateTo.Value != null) ? DateTime.Parse(tdArchiveReadDateTo.Value.ToString()).ToString("yyyy-MM-dd HH:mm") : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Type, (tdArchiveReadType != null && tdArchiveReadType.Value != null) ? IMR.Suite.UI.Business.Components.DW.PerformanceComponent.GetAggregationTypeString((Enums.AggregationType)Convert.ToInt32(tdArchiveReadType.Value)) : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}", ResourcesText.ReadFrameCount, (tdArchiveReadFrameCount != null && tdArchiveReadFrameCount.Value != null) ? tdArchiveReadFrameCount.Value.ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_EVENT_LOG_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region EventLog
                        stepType = Enums.TaskVisitStepType.EventLog;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_EVENT_LOG_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdReferenceDate = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_EVENT_LOG_REFERENCE_DATE && t.Value != null);
                        OpTaskData tdEntryCount = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_EVENT_LOG_ENTRY_COUNT && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.ReferenceDate, (tdReferenceDate != null && tdReferenceDate.Value != null) ? DateTime.Parse(tdReferenceDate.Value.ToString()).ToString("yyyy-MM-dd HH:mm") : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}", ResourcesText.EntryCount, (tdEntryCount != null && tdEntryCount.Value != null) ? tdEntryCount.Value.ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_NEW_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region NewMeterCounterResynchronization

                        stepType = Enums.TaskVisitStepType.NewMeterCounterResynchronization;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_NEW_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdCounterCoefficient = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_COUNTER_COEFFICIENT && t.Value != null);
                        OpTaskData tdGasMeterUsageValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.GAS_METER_USAGE_VALUE && t.Value != null);
                        OpTaskData tdCounterLiterMaxValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_COUNTER_LITER_MAX_VALUE && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Weight, (tdCounterCoefficient != null && tdCounterCoefficient.Value != null) ? tdCounterCoefficient.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Value, (tdGasMeterUsageValue != null && tdGasMeterUsageValue.Value != null) ? tdGasMeterUsageValue.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}", ResourcesText.Format, (tdCounterLiterMaxValue != null && tdCounterLiterMaxValue.Value != null) ? tdCounterLiterMaxValue.Value.ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_OLD_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region OldMeterCounterResynchronization

                        stepType = Enums.TaskVisitStepType.OldMeterCounterResynchronization;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_OLD_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdCounterCoefficient = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_COUNTER_COEFFICIENT && t.Value != null);
                        OpTaskData tdGasMeterUsageValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.GAS_METER_USAGE_VALUE && t.Value != null);
                        OpTaskData tdCounterLiterMaxValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_COUNTER_LITER_MAX_VALUE && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Weight, (tdCounterCoefficient != null && tdCounterCoefficient.Value != null) ? tdCounterCoefficient.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Value, (tdGasMeterUsageValue != null && tdGasMeterUsageValue.Value != null) ? tdGasMeterUsageValue.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}", ResourcesText.Format, (tdCounterLiterMaxValue != null && tdCounterLiterMaxValue.Value != null) ? tdCounterLiterMaxValue.Value.ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_CONFIGURATION_RESTORE_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region ConfigurationRestore

                        stepType = Enums.TaskVisitStepType.ConfigurationRestore;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CONFIGURATION_RESTORE_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdRestoredConfiguration = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_RESTORED_CONFIGURATION && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0} ({1}): {2}", ResourcesText.Configuration, ResourcesText.Bytes, (tdRestoredConfiguration != null && tdRestoredConfiguration.Value != null) ? ((byte[])tdRestoredConfiguration.Value).Length.ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_GASMETER_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region InstalledGasmeter

                        stepType = Enums.TaskVisitStepType.InstalledGasmeter;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_GASMETER_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdMeterType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_METER_TYPE && t.Value != null);
                        OpTaskData tdMeterSeries = relatedProtocolContent.Find(t => t.IdDataType == DataType.METER_SERIES && t.Value != null);
                        OpTaskData tdMeterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.METER_SERIAL_NUMBER && t.Value != null);
                        OpTaskData tdCounterCoefficient = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_COUNTER_COEFFICIENT && t.Value != null);
                        OpTaskData tdGasMeterUsageValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.GAS_METER_USAGE_VALUE && t.Value != null);
                        OpTaskData tdCounterLiterMaxValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_COUNTER_LITER_MAX_VALUE && t.Value != null);
                        OpTaskData tdLocationCID = relatedProtocolContent.Find(t => t.IdDataType == DataType.LOCATION_CID && t.Value != null);
                        OpTaskData tdOperationType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_OPERATION_TYPE && t.Value != null);
                        OpTaskData tdMinimumTemporaryFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.MINIMUM_TEMPORARY_FLOW && t.Value != null);
                        OpTaskData tdMaximumTemporaryFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.MAXIMUM_TEMPORARY_FLOW && t.Value != null);

                        OpTaskData tdTaskProtocolGasMeterUsageValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_GAS_METER_USAGE_VALUE && t.Value != null);
                        OpTaskData tdTaskProtocolCounterLiterMaxValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_COUNTER_LITER_MAX_VALUE && t.Value != null);
                        OpTaskData tdTaskProtocolCounterCoefficient = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_COUNTER_COEFFICIENT && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.MeterType, (tdMeterType != null && tdMeterType.Value != null) ? tdMeterType.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.MeterSeries, (tdMeterSeries != null && tdMeterSeries.Value != null) ? tdMeterSeries.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.MeterSerialNbr, (tdMeterSerialNbr != null && tdMeterSerialNbr.Value != null) ? tdMeterSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.CounterWeight, (tdCounterCoefficient != null && tdCounterCoefficient.Value != null) ? tdCounterCoefficient.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.CounterWeight, (tdTaskProtocolCounterCoefficient != null && tdTaskProtocolCounterCoefficient.Value != null) ? tdTaskProtocolCounterCoefficient.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.CounterValue, (tdGasMeterUsageValue != null && tdGasMeterUsageValue.Value != null) ? tdGasMeterUsageValue.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.CounterWeight, (tdTaskProtocolGasMeterUsageValue != null && tdTaskProtocolGasMeterUsageValue.Value != null) ? tdTaskProtocolGasMeterUsageValue.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.CounterFormat, (tdCounterLiterMaxValue != null && tdCounterLiterMaxValue.Value != null) ? tdCounterLiterMaxValue.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.CounterWeight, (tdTaskProtocolCounterLiterMaxValue != null && tdTaskProtocolCounterLiterMaxValue.Value != null) ? tdTaskProtocolCounterLiterMaxValue.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.LocationCID, (tdLocationCID != null && tdLocationCID.Value != null) ? tdLocationCID.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SynchronizationType, (tdOperationType != null && tdOperationType.Value != null) ? ((Enums.SitaOperationType)Convert.ToInt32(tdOperationType.Value)).ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.MinimumTemporaryFlow, (tdMinimumTemporaryFlow != null && tdMinimumTemporaryFlow.Value != null) ? tdMinimumTemporaryFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}", ResourcesText.MaximumTemporaryFlow, (tdMaximumTemporaryFlow != null && tdMaximumTemporaryFlow.Value != null) ? tdMaximumTemporaryFlow.Value.ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_DEINSTALLED_GASMETER_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region DeinstalledGasmeter
                        stepType = Enums.TaskVisitStepType.DeinstalledGasmeter;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEINSTALLED_GASMETER_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdDeinstalledMagazineCID = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEINSTALLED_MAGAZINE_CID && t.Value != null);
                        OpTaskData tdOperationType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_OPERATION_TYPE && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.MagazineCID, (tdDeinstalledMagazineCID != null && tdDeinstalledMagazineCID.Value != null) ? tdDeinstalledMagazineCID.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}", ResourcesText.DeinstallationType, (tdOperationType != null && tdOperationType.Value != null) ? ((Enums.SitaOperationType)Convert.ToInt32(tdOperationType.Value)).ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_CONFIGURATION_DOWNLOAD_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region ConfigurationDownload

                        stepType = Enums.TaskVisitStepType.ConfigurationDownload;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CONFIGURATION_DOWNLOAD_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdRestoredConfiguration = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_RESTORED_CONFIGURATION && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0} ({1}): {2}", ResourcesText.Configuration, ResourcesText.Bytes, (tdRestoredConfiguration != null && tdRestoredConfiguration.Value != null) ? ((byte[])tdRestoredConfiguration.Value).Length.ToString() : "-");
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_OKO && t.Value != null))
                    {
                        #region ClusterTest

                        stepType = Enums.TaskVisitStepType.ClusterTest;

                        OpTaskData tdOko = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_OKO && t.Value != null);
                        OpTaskData tdFileName = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_FILE_NAME && t.Value != null);
                        OpTaskData tdApulseList = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE && t.Value != null);
                        OpTaskData tdLatitudeList = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_LATITUDE && t.Value != null);
                        OpTaskData tdLongitudeList = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_LONGITUDE && t.Value != null);
                        OpTaskData tdApulseStatusList = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_STATUS && t.Value != null);
                        OpTaskData tdApulseEfficiencyList = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_EFFICIENCY && t.Value != null);
                        OpTaskData tdApulseFrameCountList = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_FRAME_COUNT && t.Value != null);

                        long? okoSn = null;
                        if (tdOko != null)
                        {
                            long okoSnTmp = 0;
                            if (long.TryParse(tdOko.Value.ToString(), out okoSnTmp))
                                okoSn = okoSnTmp;
                        }

                        Dictionary<long, Tuple<bool, Enums.SitaClusterTestApulseStatus, double?, double?, int?, double?, Tuple<int, int, double?>>> deviceCoordinates = new Dictionary<long, Tuple<bool, Enums.SitaClusterTestApulseStatus, double?, double?, int?, double?, Tuple<int, int, double?>>>();
                        string[] apulses = (tdApulseList != null ? tdApulseList.Value.ToString().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries) : new List<string>().ToArray());
                        string[] latitudes = (tdLatitudeList != null ? tdLatitudeList.Value.ToString().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries) : new List<string>().ToArray());
                        string[] longitudes = (tdLongitudeList != null ? tdLongitudeList.Value.ToString().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries) : new List<string>().ToArray());
                        string[] apulseStatuses = (tdApulseStatusList != null ? tdApulseStatusList.Value.ToString().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries) : new List<string>().ToArray());
                        string[] apulseEfficiences = (tdApulseEfficiencyList != null ? tdApulseEfficiencyList.Value.ToString().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries) : new List<string>().ToArray());
                        string[] apulseFramesCount = (tdApulseFrameCountList != null ? tdApulseFrameCountList.Value.ToString().Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries) : new List<string>().ToArray());

                        System.Device.Location.GeoCoordinate okoGeoCoordinate = null;

                        #region DeviceCoordinatesDict

                        double latitudeTmp = 0.0;
                        double longitudeTmp = 0.0;
                        double? fromOKODistance = null;
                        int totalFrames = 1;
                        int framesFrequncy = 0;
                        int okoCnt = (okoSn.HasValue ? 1 : 0);
                        if (apulses.Count() + okoCnt > 0)
                        {
                            for (int i = 0; i < apulses.Count() + okoCnt; i++)
                            {
                                long deviceSn = 0;
                                double? efficiency = null;
                                int? frameCount = null;
                                Enums.SitaClusterTestApulseStatus apulseStatus = Enums.SitaClusterTestApulseStatus.ConfiguredNotReceived;
                                if (i == 0)
                                {
                                    if (!long.TryParse(tdOko.Value.ToString(), out deviceSn))
                                        continue;
                                    int totalFramesTmp = 1;
                                    int framesFrequencyTmp = 0;

                                    if (apulseFramesCount.Count() > 0 && int.TryParse(apulseFramesCount[0], out totalFramesTmp))
                                        totalFrames = totalFramesTmp;

                                    if (apulseEfficiences.Count() > 0 && int.TryParse(apulseEfficiences[0], out framesFrequencyTmp))
                                        framesFrequncy = framesFrequencyTmp;
                                }
                                else
                                {
                                    int frameCountTmp = 0;
                                    if (!long.TryParse(apulses[i - okoCnt], out deviceSn))
                                        continue;
                                    int apulseStatusTmp = (int)Enums.SitaClusterTestApulseStatus.ConfiguredNotReceived;
                                    if (apulseStatuses.Count() > i - okoCnt && int.TryParse(apulseStatuses[i - okoCnt], out apulseStatusTmp))
                                        apulseStatus = (Enums.SitaClusterTestApulseStatus)apulseStatusTmp;
                                    if (apulseFramesCount.Count() > i && int.TryParse(apulseFramesCount[i], out frameCountTmp))//pamiętamy o totalu na 0 indeksie
                                        frameCount = frameCountTmp;

                                    efficiency = Math.Round(100.0 * ((double)(frameCount.HasValue ? frameCount.Value : 0) / (double)totalFrames), 2);
                                    /*
                                    if (apulseEfficiences.Count() > i - okoCnt)
                                    {
                                        try
                                        {
                                            efficiency = Convert.ToDouble(apulseEfficiences[i - okoCnt]);
                                        }
                                        catch (FormatException)
                                        {
                                            try
                                            {
                                                efficiency = Convert.ToDouble(apulseEfficiences[i - okoCnt], System.Globalization.CultureInfo.InvariantCulture);
                                            }
                                            catch{}
                                        }
                                    }
                                    */

                                }
                                if (i > 0 && frameCount == 0 && apulseStatus == Enums.SitaClusterTestApulseStatus.ConfiguredReceived)
                                {
                                    apulseStatus = Enums.SitaClusterTestApulseStatus.ConfiguredNotReceived;
                                }

                                latitudeTmp = 0.0;
                                longitudeTmp = 0.0;
                                #region Latitude
                                if (latitudes.Count() > i)//pierwszy indeks to wspolrzedna OKO
                                {
                                    bool conversionSuccess = true;
                                    try
                                    {
                                        latitudeTmp = IMR.Suite.UI.Business.Components.DW.PerformanceComponent.ConvertValueToDouble(latitudes[i]);//Convert.ToDouble(latitudes[i]);
                                    }
                                    catch (FormatException)
                                    {
                                        try
                                        {
                                            latitudeTmp = Convert.ToDouble(latitudes[i], System.Globalization.CultureInfo.InvariantCulture);
                                        }
                                        catch
                                        {
                                            conversionSuccess = false;
                                        }
                                    }
                                    if (!conversionSuccess)
                                        latitudeTmp = 0.0;
                                }
                                #endregion
                                #region Longitude
                                if (longitudes.Count() > i)//pierwszy indeks to wspolrzedna OKO
                                {
                                    bool conversionSuccess = true;
                                    try
                                    {
                                        longitudeTmp = IMR.Suite.UI.Business.Components.DW.PerformanceComponent.ConvertValueToDouble(longitudes[i]); //Convert.ToDouble(longitudes[i]);
                                    }
                                    catch (FormatException)
                                    {
                                        try
                                        {
                                            longitudeTmp = Convert.ToDouble(longitudes[i], System.Globalization.CultureInfo.InvariantCulture);
                                        }
                                        catch
                                        {
                                            conversionSuccess = false;
                                        }
                                    }
                                    if (!conversionSuccess)
                                        longitudeTmp = 0.0;
                                }
                                #endregion
                                if (i == 0)
                                {
                                    okoGeoCoordinate = new System.Device.Location.GeoCoordinate(latitudeTmp, longitudeTmp);
                                    fromOKODistance = null;
                                }
                                else
                                {
                                    try
                                    {
                                        fromOKODistance = okoGeoCoordinate.GetDistanceTo(new System.Device.Location.GeoCoordinate(latitudeTmp, longitudeTmp));
                                    }
                                    catch (Exception ex)
                                    {
                                        fromOKODistance = null;
                                    }
                                }

                                if (!deviceCoordinates.ContainsKey(deviceSn))
                                    deviceCoordinates.Add(deviceSn, new Tuple<bool, Enums.SitaClusterTestApulseStatus, double?, double?, int?, double?, Tuple<int, int, double?>>(i == 0, apulseStatus, latitudeTmp, longitudeTmp, frameCount, efficiency, new Tuple<int, int, double?>(totalFrames, framesFrequncy, fromOKODistance)));
                            }
                            if (deviceCoordinates.Count > 0)
                            {
                                Dictionary<long, OpDevice> dDict = dataProvider.GetDevice(deviceCoordinates.Keys.Distinct().ToArray()).ToDictionary(d => d.SerialNbr);
                                Dictionary<OpDevice, Tuple<bool, Enums.SitaClusterTestApulseStatus, double?, double?, int?, double?, Tuple<int, int, double?>>> deviceCoordinatesObj = new Dictionary<OpDevice, Tuple<bool, Enums.SitaClusterTestApulseStatus, double?, double?, int?, double?, Tuple<int, int, double?>>>();

                                foreach (KeyValuePair<long, Tuple<bool, Enums.SitaClusterTestApulseStatus, double?, double?, int?, double?, Tuple<int, int, double?>>> kvItem in deviceCoordinates)
                                {
                                    if (dDict.ContainsKey(kvItem.Key))
                                        deviceCoordinatesObj.Add(dDict[kvItem.Key], kvItem.Value);
                                }

                                tag = deviceCoordinatesObj;
                            }
                        }

                        #endregion

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SummaryFile, (tdFileName != null && tdFileName.Value != null) ? tdFileName.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", "OKO", (tdOko != null && tdOko.Value != null) ? tdOko.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0} {1} ({2}: {3}): {4}", "APULSE", ResourcesText.Count, ResourcesText.All, apulses.Count(), String.Join(",", apulses));
                        foreach (Enums.SitaClusterTestApulseStatus eItem in Enum.GetValues(typeof(Enums.SitaClusterTestApulseStatus)))
                        {
                            if (deviceCoordinates.Values.Count(d => d.Item2 == eItem) > 0)
                            {
                                sb.AppendFormat(Environment.NewLine);
                                sb.AppendFormat("{0} {1} ({2}: {3}): {4}", "APULSE", new OpEnumObject(eItem).ToString(), ResourcesText.Count, deviceCoordinates.Count(d => !d.Value.Item1/*IsOko*/ && d.Value.Item2 == eItem), String.Join(",", deviceCoordinates.Where(d => !d.Value.Item1/*IsOko*/ && d.Value.Item2 == eItem).Select(d => d.Key)));
                            }
                        }
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLATION_WITH_SYNCHRONIZATION_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedInstallationWithSynchronizationStepLatch

                        stepType = Enums.TaskVisitStepType.InstallationWithSynchronization;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLATION_WITH_SYNCHRONIZATION_SERIAL_NBR && t.Value != null);
                        OpTaskData tdLocationCid = relatedProtocolContent.Find(t => t.IdDataType == DataType.LOCATION_CID && t.Value != null);
                        OpTaskData tdGasMeterUsageValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.GAS_METER_USAGE_VALUE && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Location, (tdLocationCid != null && tdLocationCid.Value != null) ? tdLocationCid.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Value, (tdGasMeterUsageValue != null && tdGasMeterUsageValue.Value != null) ? tdGasMeterUsageValue.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_PIN_VALUE_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedPinValueStepLatch

                        stepType = Enums.TaskVisitStepType.PinValue;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_PIN_VALUE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdPinNumber = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_PIN_NUMBER && t.Value != null);
                        OpTaskData tdPinType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_PIN_TYPE && t.Value != null);
                        OpTaskData tdPinValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_PIN_VALUE && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.PinNumber, (tdPinNumber != null && tdPinNumber.Value != null) ? tdPinNumber.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.PinType, (tdPinType != null && tdPinType.Value != null) ? ((Enums.SitaOKOPINMenuOption)Convert.ToInt32(tdPinType.Value)).ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.PinValue, (tdPinValue != null && tdPinValue.Value != null) ? tdPinValue.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_DIAGNOSTIC_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedDeviceDiagnosticStepLatch

                        stepType = Enums.TaskVisitStepType.Diagnostic;

                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DIAGNOSTIC_SERIAL_NBR && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_GASMETER_RADIO_QUALITY_VERIFICATION_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedInstalledGasmeterRadioQualityVerificationStepLatch

                        stepType = Enums.TaskVisitStepType.InstalledGasmeterRadioQualityVerificationDeviceSerialNbr;

                        OpTaskData tdInstalledGasmeterRadioQualityVerificationDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_GASMETER_RADIO_QUALITY_VERIFICATION_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdDeviceLatitude = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_LATITUDE && t.Value != null);
                        OpTaskData tdDeviceLongitude = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_LONGITUDE && t.Value != null);
                        OpTaskData tdRadioQuality = relatedProtocolContent.Find(t => t.IdDataType == DataType.RADIO_QUALITY && t.Value != null);

                        OpDevice device = null;
                        if (tdInstalledGasmeterRadioQualityVerificationDeviceSerialNbr != null && tdInstalledGasmeterRadioQualityVerificationDeviceSerialNbr.Value != null)
                            device = dataProvider.GetDevice(Convert.ToInt64(tdInstalledGasmeterRadioQualityVerificationDeviceSerialNbr.Value));

                        double latitude = 0.0;
                        double longitude = 0.0;

                        if (tdDeviceLatitude != null && tdDeviceLatitude.Value != null)
                            latitude = IMR.Suite.UI.Business.Components.DW.PerformanceComponent.ConvertValueToDouble(tdDeviceLatitude.Value);
                        if (tdDeviceLongitude != null && tdDeviceLongitude.Value != null)
                            longitude = IMR.Suite.UI.Business.Components.DW.PerformanceComponent.ConvertValueToDouble(tdDeviceLongitude.Value);

                        tag = new Tuple<OpDevice, double, double>(device, latitude, longitude);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SerialNumber, (tdInstalledGasmeterRadioQualityVerificationDeviceSerialNbr != null && tdInstalledGasmeterRadioQualityVerificationDeviceSerialNbr.Value != null) ? tdInstalledGasmeterRadioQualityVerificationDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Latitude, (tdDeviceLatitude != null && tdDeviceLatitude.Value != null) ? tdDeviceLatitude.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Longitude, (tdDeviceLongitude != null && tdDeviceLongitude.Value != null) ? tdDeviceLongitude.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.RadioQuality, (tdRadioQuality != null && tdRadioQuality.Value != null) ? tdRadioQuality.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_METER_ADAPTER_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedInstalledMeterAdapterStepLatch

                        stepType = Enums.TaskVisitStepType.InstalledMeterAdapterSerialNbr;

                        OpTaskData tdInstalledMeterAdapterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_METER_ADAPTER_SERIAL_NBR && t.Value != null);
                        OpTaskData tdMeterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.METER_SERIAL_NUMBER && t.Value != null);
                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdLocationCid = relatedProtocolContent.Find(t => t.IdDataType == DataType.LOCATION_CID && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SerialNumber, (tdInstalledMeterAdapterSerialNbr != null && tdInstalledMeterAdapterSerialNbr.Value != null) ? tdInstalledMeterAdapterSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Meter, (tdMeterSerialNbr != null && tdMeterSerialNbr.Value != null) ? tdMeterSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Device, (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.LocationCID, (tdLocationCid != null && tdLocationCid.Value != null) ? tdLocationCid.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_VALID_IMPULSE_COUNT_TEST_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedDeviceValidImpulseCountTestStepLatch

                        stepType = Enums.TaskVisitStepType.DeviceValidImpulseCountTest;

                        OpTaskData tdDeviceSerailNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_VALID_IMPULSE_COUNT_TEST_SERIAL_NBR && t.Value != null);
                        OpTaskData tdMeterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.METER_SERIAL_NUMBER && t.Value != null);
                        OpTaskData tdMeterName = relatedProtocolContent.Find(t => t.IdDataType == DataType.METER_NAME && t.Value != null);
                        OpTaskData tdImpulseCountPriorTest = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_PRIOR_TEST && t.Value != null);
                        OpTaskData tdImpulseCountAfterTest = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_AFTER_TEST && t.Value != null);
                        OpTaskData tdTestDuration = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_DEVICE_TEST_DURATION && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SerialNumber, (tdDeviceSerailNbr != null && tdDeviceSerailNbr.Value != null) ? tdDeviceSerailNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Meter, (tdMeterSerialNbr != null && tdMeterSerialNbr.Value != null) ? tdMeterSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.MeterType, (tdMeterName != null && tdMeterName.Value != null) ? tdMeterName.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.ImpulseCountPriorTest, (tdImpulseCountPriorTest != null && tdImpulseCountPriorTest.Value != null) ? tdImpulseCountPriorTest.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.ImpulseCountAfterTest, (tdImpulseCountAfterTest != null && tdImpulseCountAfterTest.Value != null) ? tdImpulseCountAfterTest.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.TestDuration, (tdTestDuration != null && tdTestDuration.Value != null) ? tdTestDuration.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_WATERMETER_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedInstalledWatermeterDeviceStepLatch

                        stepType = Enums.TaskVisitStepType.InstalledWaterMeterDevice;

                        #region PrepareTaskData
                        OpTaskData tdDeviceSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLED_WATERMETER_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdDeviceCounterCoefficient = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_COUNTER_COEFFICIENT && t.Value != null);
                        OpTaskData tdWaterCounter = relatedProtocolContent.Find(t => t.IdDataType == DataType.WATER_COUNTER && t.Value != null);
                        OpTaskData tdReverseWaterCounter = relatedProtocolContent.Find(t => t.IdDataType == DataType.REVERSE_WATER_COUNTER && t.Value != null);
                        OpTaskData tdMaxHourFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.MAX_HOUR_FLOW && t.Value != null);
                        OpTaskData tdCID = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_CID && t.Value != null);
                        OpTaskData tdMeterSerialNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.METER_SERIAL_NUMBER && t.Value != null);
                        OpTaskData tdMaxTemperature = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_MAX_TEMPERATURE && t.Value != null);
                        OpTaskData tdMinTemperature = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_MIN_TEMPERATURE && t.Value != null);
                        OpTaskData tdInductiveSensorWatermeterType = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_INDUCTIVE_SENSOR_WATER_METER_TYPE && t.Value != null);
                        OpTaskData tdMeasuringDirective = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_WATERMETER_MEASURING_DIRECTIVE && t.Value != null);

                        OpTaskData tdUpEventMaxHourFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_MAX_HOUR_FLOW && t.Value != null);
                        OpTaskData tdUpEventMaxTemporalFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_MAX_TEMPORAL_FLOW && t.Value != null);
                        OpTaskData tdUpEventSabotageContact = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_SABOTAGE_CONTACT && t.Value != null);
                        OpTaskData tdUpEventRemove = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_REMOVE && t.Value != null);
                        OpTaskData tdUpEventReverseFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_REVERSE_FLOW && t.Value != null);
                        OpTaskData tdUpEventBatteryLow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_BATTERY_LOW && t.Value != null);
                        OpTaskData tdUpEventValueLeakTestFailed = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_VALVE_LEAK_TEST_FAILED && t.Value != null);
                        OpTaskData tdUpEventExtremeTemp = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_UP_EVENT_EXTREME_TEMP && t.Value != null);
                        OpTaskData tdDownEventMaxHourFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_MAX_HOUR_FLOW && t.Value != null);
                        OpTaskData tdDownEventMaxTemporalFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_MAX_TEMPORAL_FLOW && t.Value != null);
                        OpTaskData tdDownEventSabotageContact = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_SABOTAGE_CONTACT && t.Value != null);
                        OpTaskData tdDownEventRemove = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_REMOVE && t.Value != null);
                        OpTaskData tdDownEventReverseFlow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_REVERSE_FLOW && t.Value != null);
                        OpTaskData tdDownEventBatteryLow = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_BATTERY_LOW && t.Value != null);
                        OpTaskData tdDownEventValueLeakTestFailed = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_VALVE_LEAK_TEST_FAILED && t.Value != null);
                        OpTaskData tdDownEventExtremeTemp = relatedProtocolContent.Find(t => t.IdDataType == DataType.DEVICE_DOWN_EVENT_EXTREME_TEMP && t.Value != null);
                        #endregion
                        #region GetDataType
                        OpDataType dtDeviceSerialNbr = dataProvider.GetDataType(DataType.TASK_PROTOCOL_INSTALLED_WATERMETER_DEVICE_SERIAL_NBR);
                        OpDataType dtDeviceCounterCoefficient = dataProvider.GetDataType(DataType.DEVICE_COUNTER_COEFFICIENT);
                        OpDataType dtWaterCounter = dataProvider.GetDataType(DataType.WATER_COUNTER);
                        OpDataType dtReverseWaterCounter = dataProvider.GetDataType(DataType.REVERSE_WATER_COUNTER);
                        OpDataType dtMaxHourFlow = dataProvider.GetDataType(DataType.MAX_HOUR_FLOW);
                        OpDataType dtCID = dataProvider.GetDataType(DataType.DEVICE_CID);
                        OpDataType dtMeterSerialNbr = dataProvider.GetDataType(DataType.METER_SERIAL_NUMBER);
                        OpDataType dtMaxTemperature = dataProvider.GetDataType(DataType.DEVICE_MAX_TEMPERATURE);
                        OpDataType dtMinTemperature = dataProvider.GetDataType(DataType.DEVICE_MIN_TEMPERATURE);
                        OpDataType dtInductiveSensorWatermeterType = dataProvider.GetDataType(DataType.DEVICE_INDUCTIVE_SENSOR_WATER_METER_TYPE);
                        OpDataType dtMeasuringDirective = dataProvider.GetDataType(DataType.TASK_PROTOCOL_WATERMETER_MEASURING_DIRECTIVE);

                        OpDataType dtUpEventMaxHourFlow = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_MAX_HOUR_FLOW);
                        OpDataType dtUpEventMaxTemporalFlow = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_MAX_TEMPORAL_FLOW);
                        OpDataType dtUpEventSabotageContact = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_SABOTAGE_CONTACT);
                        OpDataType dtUpEventRemove = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_REMOVE);
                        OpDataType dtUpEventReverseFlow = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_REVERSE_FLOW);
                        OpDataType dtUpEventBatteryLow = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_BATTERY_LOW);
                        OpDataType dtUpEventValueLeakTestFailed = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_VALVE_LEAK_TEST_FAILED);
                        OpDataType dtUpEventExtremeTemp = dataProvider.GetDataType(DataType.DEVICE_UP_EVENT_EXTREME_TEMP);
                        OpDataType dtDownEventMaxHourFlow = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_MAX_HOUR_FLOW);
                        OpDataType dtDownEventMaxTemporalFlow = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_MAX_TEMPORAL_FLOW);
                        OpDataType dtDownEventSabotageContact = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_SABOTAGE_CONTACT);
                        OpDataType dtDownEventRemove = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_REMOVE);
                        OpDataType dtDownEventReverseFlow = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_REVERSE_FLOW);
                        OpDataType dtDownEventBatteryLow = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_BATTERY_LOW);
                        OpDataType dtDownEventValueLeakTestFailed = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_VALVE_LEAK_TEST_FAILED);
                        OpDataType dtDownEventExtremeTemp = dataProvider.GetDataType(DataType.DEVICE_DOWN_EVENT_EXTREME_TEMP);
                        #endregion
                        #region PrepareDetails
                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", dtDeviceSerialNbr != null ? dtDeviceSerialNbr.ToString() : DataType.TASK_PROTOCOL_INSTALLED_WATERMETER_DEVICE_SERIAL_NBR.ToString(), (tdDeviceSerialNbr != null && tdDeviceSerialNbr.Value != null) ? tdDeviceSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDeviceCounterCoefficient != null ? dtDeviceCounterCoefficient.ToString() : DataType.DEVICE_COUNTER_COEFFICIENT.ToString(), (tdDeviceCounterCoefficient != null && tdDeviceCounterCoefficient.Value != null) ? tdDeviceCounterCoefficient.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtWaterCounter != null ? dtWaterCounter.ToString() : DataType.WATER_COUNTER.ToString(), (tdWaterCounter != null && tdWaterCounter.Value != null) ? tdWaterCounter.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtReverseWaterCounter != null ? dtReverseWaterCounter.ToString() : DataType.REVERSE_WATER_COUNTER.ToString(), (tdReverseWaterCounter != null && tdReverseWaterCounter.Value != null) ? tdReverseWaterCounter.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtMaxHourFlow != null ? dtMaxHourFlow.ToString() : DataType.MAX_HOUR_FLOW.ToString(), (tdMaxHourFlow != null && tdMaxHourFlow.Value != null) ? tdMaxHourFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtCID != null ? dtCID.ToString() : DataType.DEVICE_CID.ToString(), (tdCID != null && tdCID.Value != null) ? tdCID.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtMeterSerialNbr != null ? dtMeterSerialNbr.ToString() : DataType.METER_SERIAL_NUMBER.ToString(), (tdMeterSerialNbr != null && tdMeterSerialNbr.Value != null) ? tdMeterSerialNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtMaxTemperature != null ? dtMaxTemperature.ToString() : DataType.DEVICE_MAX_TEMPERATURE.ToString(), (tdMaxTemperature != null && tdMaxTemperature.Value != null) ? tdMaxTemperature.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtMinTemperature != null ? dtMinTemperature.ToString() : DataType.DEVICE_MIN_TEMPERATURE.ToString(), (tdMinTemperature != null && tdMinTemperature.Value != null) ? tdMinTemperature.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtInductiveSensorWatermeterType != null ? dtInductiveSensorWatermeterType.ToString() : DataType.DEVICE_INDUCTIVE_SENSOR_WATER_METER_TYPE.ToString(), (tdInductiveSensorWatermeterType != null && tdInductiveSensorWatermeterType.Value != null) ? tdInductiveSensorWatermeterType.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtMeasuringDirective != null ? dtMeasuringDirective.ToString() : DataType.TASK_PROTOCOL_WATERMETER_MEASURING_DIRECTIVE.ToString(), (tdMeasuringDirective != null && tdMeasuringDirective.Value != null) ? tdMeasuringDirective.Value.ToString() : "-", Environment.NewLine);

                        sb.AppendFormat("{0}: {1}{2}", dtUpEventMaxHourFlow != null ? dtUpEventMaxHourFlow.ToString() : DataType.DEVICE_UP_EVENT_MAX_HOUR_FLOW.ToString(), (tdUpEventMaxHourFlow != null && tdUpEventMaxHourFlow.Value != null) ? tdUpEventMaxHourFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtUpEventMaxTemporalFlow != null ? dtUpEventMaxTemporalFlow.ToString() : DataType.DEVICE_UP_EVENT_MAX_TEMPORAL_FLOW.ToString(), (tdUpEventMaxTemporalFlow != null && tdUpEventMaxTemporalFlow.Value != null) ? tdUpEventMaxTemporalFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtUpEventSabotageContact != null ? dtUpEventSabotageContact.ToString() : DataType.DEVICE_UP_EVENT_SABOTAGE_CONTACT.ToString(), (tdUpEventSabotageContact != null && tdUpEventSabotageContact.Value != null) ? tdUpEventSabotageContact.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtUpEventRemove != null ? dtUpEventRemove.ToString() : DataType.DEVICE_UP_EVENT_REMOVE.ToString(), (tdUpEventRemove != null && tdUpEventRemove.Value != null) ? tdUpEventRemove.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtUpEventReverseFlow != null ? dtUpEventReverseFlow.ToString() : DataType.DEVICE_UP_EVENT_REVERSE_FLOW.ToString(), (tdUpEventReverseFlow != null && tdUpEventReverseFlow.Value != null) ? tdUpEventReverseFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtUpEventBatteryLow != null ? dtUpEventBatteryLow.ToString() : DataType.DEVICE_UP_EVENT_BATTERY_LOW.ToString(), (tdUpEventBatteryLow != null && tdUpEventBatteryLow.Value != null) ? tdUpEventBatteryLow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtUpEventValueLeakTestFailed != null ? dtUpEventValueLeakTestFailed.ToString() : DataType.DEVICE_UP_EVENT_VALVE_LEAK_TEST_FAILED.ToString(), (tdUpEventValueLeakTestFailed != null && tdUpEventValueLeakTestFailed.Value != null) ? tdUpEventValueLeakTestFailed.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtUpEventExtremeTemp != null ? dtUpEventExtremeTemp.ToString() : DataType.DEVICE_UP_EVENT_EXTREME_TEMP.ToString(), (tdUpEventExtremeTemp != null && tdUpEventExtremeTemp.Value != null) ? tdUpEventExtremeTemp.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventMaxHourFlow != null ? dtDownEventMaxHourFlow.ToString() : DataType.DEVICE_DOWN_EVENT_MAX_HOUR_FLOW.ToString(), (tdDownEventMaxHourFlow != null && tdDownEventMaxHourFlow.Value != null) ? tdDownEventMaxHourFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventMaxTemporalFlow != null ? dtDownEventMaxTemporalFlow.ToString() : DataType.DEVICE_DOWN_EVENT_MAX_TEMPORAL_FLOW.ToString(), (tdDownEventMaxTemporalFlow != null && tdDownEventMaxTemporalFlow.Value != null) ? tdDownEventMaxTemporalFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventSabotageContact != null ? dtDownEventSabotageContact.ToString() : DataType.DEVICE_DOWN_EVENT_SABOTAGE_CONTACT.ToString(), (tdDownEventSabotageContact != null && tdDownEventSabotageContact.Value != null) ? tdDownEventSabotageContact.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventRemove != null ? dtDownEventRemove.ToString() : DataType.DEVICE_DOWN_EVENT_REMOVE.ToString(), (tdDownEventRemove != null && tdDownEventRemove.Value != null) ? tdDownEventRemove.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventReverseFlow != null ? dtDownEventReverseFlow.ToString() : DataType.DEVICE_DOWN_EVENT_REVERSE_FLOW.ToString(), (tdDownEventReverseFlow != null && tdDownEventReverseFlow.Value != null) ? tdDownEventReverseFlow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventBatteryLow != null ? dtDownEventBatteryLow.ToString() : DataType.DEVICE_DOWN_EVENT_BATTERY_LOW.ToString(), (tdDownEventBatteryLow != null && tdDownEventBatteryLow.Value != null) ? tdDownEventBatteryLow.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventValueLeakTestFailed != null ? dtDownEventValueLeakTestFailed.ToString() : DataType.DEVICE_DOWN_EVENT_VALVE_LEAK_TEST_FAILED.ToString(), (tdDownEventValueLeakTestFailed != null && tdDownEventValueLeakTestFailed.Value != null) ? tdDownEventValueLeakTestFailed.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", dtDownEventExtremeTemp != null ? dtDownEventExtremeTemp.ToString() : DataType.DEVICE_DOWN_EVENT_EXTREME_TEMP.ToString(), (tdDownEventExtremeTemp != null && tdDownEventExtremeTemp.Value != null) ? tdDownEventExtremeTemp.Value.ToString() : "-", Environment.NewLine);

                        details = sb.ToString();
                        #endregion

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_HUB_INSTALLATION_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedHubInstallationStepLatch

                        stepType = Enums.TaskVisitStepType.HubInstallation;

                        OpTaskData tdDeviceSerailNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_HUB_INSTALLATION_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdLocationCID = relatedProtocolContent.Find(t => t.IdDataType == DataType.LOCATION_CID && t.Value != null);
                        OpTaskData tdLocationDeviceInstallationPlace = relatedProtocolContent.Find(t => t.IdDataType == DataType.LOCATION_DEVICE_INSTALLATION_PLACE && t.Value != null);
                        OpTaskData tdLocationDeviceInstallationPlaceNote = relatedProtocolContent.Find(t => t.IdDataType == DataType.LOCATION_DEVICE_INSTALLATION_PLACE_NOTE && t.Value != null);
                        OpTaskData tdInstallationFormType = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_INSTALLATION_FORM_TYPE && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SerialNumber, (tdDeviceSerailNbr != null && tdDeviceSerailNbr.Value != null) ? tdDeviceSerailNbr.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.CID, (tdLocationCID != null && tdLocationCID.Value != null) ? tdLocationCID.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.InstallationPlace, (tdLocationDeviceInstallationPlace != null && tdLocationDeviceInstallationPlace.Value != null) ? tdLocationDeviceInstallationPlace.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.InstallationPlaceNote, (tdLocationDeviceInstallationPlaceNote != null && tdLocationDeviceInstallationPlaceNote.Value != null) ? tdLocationDeviceInstallationPlaceNote.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.InstallationForm, (tdInstallationFormType != null && tdInstallationFormType.Value != null) ? tdInstallationFormType.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }
                    else if (relatedProtocolContent.Exists(t => t.IdDataType == DataType.TASK_PROTOCOL_WALKED_BY_READOUT_DEVICE_SERIAL_NBR && t.Value != null))
                    {
                        #region TaskProceedWalkedByReadoutStepLatch

                        stepType = Enums.TaskVisitStepType.WalkedByReadout;

                        OpTaskData tdDeviceSerailNbr = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_PROTOCOL_WALKED_BY_READOUT_DEVICE_SERIAL_NBR && t.Value != null);
                        OpTaskData tdFrame = relatedProtocolContent.Find(t => t.IdDataType == DataType.SITA_WALKEDBY_FRAME && t.Value != null);
                        OpTaskData tdFrameType = relatedProtocolContent.Find(t => t.IdDataType == DataType.SITA_WALKEDBY_FRAME_TYPE && t.Value != null);
                        OpTaskData tdLatitude = relatedProtocolContent.Find(t => t.IdDataType == DataType.WALKEDBY_LATITUDE && t.Value != null);
                        OpTaskData tdLongitude = relatedProtocolContent.Find(t => t.IdDataType == DataType.WALKEDBY_LONGITUDE && t.Value != null);
                        OpTaskData tdContainsEncryptionKey = relatedProtocolContent.Find(t => t.IdDataType == DataType.SITA_CONTAINS_ENCRYPTION_KEYS && t.Value != null);
                        OpTaskData tdMeasurementValue = relatedProtocolContent.Find(t => t.IdDataType == DataType.TASK_INSTALLATION_PROTOCOL_WALKEDBY_MEASUREMENT_VALUE && t.Value != null);

                        StringBuilder sb = new StringBuilder();
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SerialNumber, (tdDeviceSerailNbr != null && tdDeviceSerailNbr.Value != null) ? tdDeviceSerailNbr.Value.ToString() : "-", Environment.NewLine);
                        //sb.AppendFormat("{0}: {1}{2}", ResourcesText.Frame, (tdFrame != null && tdFrame.Value != null) ? tdFrame.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.FrameType, (tdFrameType != null && tdFrameType.Value != null) ? ((Enums.SITAFrameType)Convert.ToInt32(tdFrameType.Value)).ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Latitude, (tdLatitude != null && tdLatitude.Value != null) ? tdLatitude.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Longitude, (tdLongitude != null && tdLongitude.Value != null) ? tdLongitude.Value.ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.SITAContainsEncryptionKeys, (tdContainsEncryptionKey != null && tdContainsEncryptionKey.Value != null) ? Convert.ToBoolean(tdContainsEncryptionKey.Value).ToString() : "-", Environment.NewLine);
                        sb.AppendFormat("{0}: {1}{2}", ResourcesText.Value, (tdMeasurementValue != null && tdMeasurementValue.Value != null) ? tdMeasurementValue.Value.ToString() : "-", Environment.NewLine);
                        details = sb.ToString();

                        #endregion
                    }

                    if (!String.IsNullOrEmpty(details) && stepType != Enums.TaskVisitStepType.Unknown)
                    {
                        retList.Add(new Tuple<Enums.TaskVisitStepType, DateTime, string, object>(stepType, timeStamp, details, tag));
                       // gotvsDataSource.Add(new GOTaskVisitStep(stepType, timeStamp, details) { Tag = tag });
                    }
                }
            }
            return retList;
        }

        #endregion

        #region Save

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, false, true, null, smsNotification: smsNotification, useDBCollector: useDBCollector);
        }

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, bool finishParentIssue, List<OpDepositoryElement> devicesToChange, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, null, finishParentIssue, true, devicesToChange, smsNotification: smsNotification, useDBCollector: useDBCollector);
        }

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, bool finishParentIssue, bool emailNotification, List<OpDepositoryElement> devicesToChange, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, null, finishParentIssue, emailNotification, devicesToChange, smsNotification: smsNotification, useDBCollector: useDBCollector);
        }

        #region Save with finish related tasks prompt
        public delegate bool AskIfFinishParentIssueDelegate(DataProvider dataProvider, OpTask issue, string question, bool multiple, out bool cancel, out bool applyToAll, OpIssueStatus issueStatus);
        public delegate List<OpDepositoryElement> AskIfChangeDevicesOwnership(DataProvider dataProvider, OpTask task, ref bool applyToAll, List<OpDepositoryElement> devicesToChange = null);
        public delegate bool AskIfChangeStatusWithNoFitterVisitDateDelegate(DataProvider dataProvider, OpTask task, ref DateTime selectedDate, out bool cancel, ref bool applyToAll);

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave,
                                  AskIfFinishParentIssueDelegate askIfFinishParentIssue,
                                  AskIfChangeDevicesOwnership askIfChangeDevicesOwnership,
                                  string customQuestionBase = null, bool emailNotification = true,
                                  bool smsNotification = false, bool useDBCollector = false)
        {
            bool cancel;
            return Save(dataProvider, loggedOperator, objectToSave, null, askIfFinishParentIssue, askIfChangeDevicesOwnership, null, out cancel, customQuestionBase, emailNotification, smsNotification, useDBCollector);
        }

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, string note,
                                  AskIfFinishParentIssueDelegate askIfFinishParentIssue,
                                  AskIfChangeDevicesOwnership askIfChangeDevicesOwnership,
                                  AskIfChangeStatusWithNoFitterVisitDateDelegate askIfChangeStatusWithNoFitterVisitDate,
                                  string customQuestionBase = null, bool emailNotification = true, bool multiple = false,
                                  bool smsNotification = false, bool useDBCollector = false)
        {
            bool cancel;
            return Save(dataProvider, loggedOperator, objectToSave, null, askIfFinishParentIssue, askIfChangeDevicesOwnership, null, out cancel, customQuestionBase, emailNotification, smsNotification, useDBCollector);
        }

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, string note,
                                  AskIfFinishParentIssueDelegate askIfFinishParentIssue,
                                  AskIfChangeDevicesOwnership askIfChangeDevicesOwnership,
                                  AskIfChangeStatusWithNoFitterVisitDateDelegate askIfChangeStatusWithNoFitterVisitDate,
                                  out bool cancel,
                                  string customQuestionBase = null, bool emailNotification = true, bool multiple = false,
                                  bool smsNotification = false, bool useDBCollector = false)
        {
            bool obsolete;
            //bool cancel;
            bool changeStatus = false;

            if (askIfChangeStatusWithNoFitterVisitDate != null)
            {
                bool applyToAll = false; // Do masowej edycji
                DateTime selectedDate = DateTime.Now; // Do masowej edycji
                changeStatus = askIfChangeStatusWithNoFitterVisitDate(dataProvider, objectToSave, ref selectedDate, out cancel, ref applyToAll);
                if (cancel || !changeStatus)
                {
                    cancel = true;
                    return objectToSave;
                }
            }

            bool finishRelatedTasks = askIfFinishParentIssue(dataProvider, objectToSave, customQuestionBase, false, out cancel, out obsolete, MapTaskStatus(dataProvider, objectToSave.TaskStatus));
            if (cancel)
                return objectToSave;
            List<OpDepositoryElement> devicesOwnershipToChange = null;
            if (objectToSave.TaskStatus.IsFinished && askIfChangeDevicesOwnership != null)
            {
                bool devicesOwnershipApplyToAll = false;//wykorzystywane przy masowej edycji
                devicesOwnershipToChange = askIfChangeDevicesOwnership(dataProvider, objectToSave, ref devicesOwnershipApplyToAll);
            }

            return Save(dataProvider, loggedOperator, objectToSave, note, finishRelatedTasks, emailNotification, devicesOwnershipToChange, smsNotification, useDBCollector);
        }

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, string note,
                                  AskIfFinishParentIssueDelegate askIfFinishParentIssue,
                                  out bool applyToAll, out bool finishParentIssue,
                                  AskIfChangeDevicesOwnership askIfChangeDevicesOwnership,
                                  AskIfChangeStatusWithNoFitterVisitDateDelegate askIfChangeStatusWithNoFitterVisitDate,
                                  out bool cancel,
                                  string customQuestionBase = null, bool emailNotification = true,
                                  bool smsNotification = false, bool useDBCollector = false)
        {
            applyToAll = false;
            finishParentIssue = false;
            bool changeStatus = false;
            //bool cancel;

            if (askIfChangeStatusWithNoFitterVisitDate != null)
            {
                applyToAll = false; // Do masowej edycji
                DateTime selectedDate = DateTime.Now; // Do masowej edycji
                changeStatus = askIfChangeStatusWithNoFitterVisitDate(dataProvider, objectToSave, ref selectedDate, out cancel, ref applyToAll);
                if (cancel || !changeStatus)
                    return objectToSave;
            }

            finishParentIssue = askIfFinishParentIssue(dataProvider, objectToSave, customQuestionBase, true, out cancel, out applyToAll, MapTaskStatus(dataProvider, objectToSave.TaskStatus));
            if (cancel)
                return objectToSave;
            List<OpDepositoryElement> devicesOwnershipToChange = null;
            if (objectToSave.TaskStatus.IsFinished && askIfChangeDevicesOwnership != null)
            {
                bool devicesOwnershipApplyToAll = false;//wykorzystywane przy masowej edycji
                devicesOwnershipToChange = askIfChangeDevicesOwnership(dataProvider, objectToSave, ref devicesOwnershipApplyToAll);
            }

            return Save(dataProvider, loggedOperator, objectToSave, note, finishParentIssue, emailNotification, devicesOwnershipToChange, smsNotification, useDBCollector);
        }

        #region SaveMultiple
        public static List<OpTask> SaveMultiple(DataProvider dataProvider, OpOperator loggedOperator, List<OpTask> objectsToSave,
                                                AskIfFinishParentIssueDelegate askIfFinishParentIssue,
                                                AskIfChangeDevicesOwnership askIfChangeDevicesOwnership,
                                                string customQuestionBase = null, bool emailNotification = true,
                                                bool smsNotification = false, bool useDBCollector = false)
        {
            return SaveMultiple(dataProvider, loggedOperator, objectsToSave, null, askIfFinishParentIssue, askIfChangeDevicesOwnership, customQuestionBase, emailNotification, smsNotification, useDBCollector);
        }

        public static List<OpTask> SaveMultiple(DataProvider dataProvider, OpOperator loggedOperator, List<OpTask> objectsToSave, string note,
                                                AskIfFinishParentIssueDelegate askIfFinishParentIssue,
                                                AskIfChangeDevicesOwnership askIfChangeDevicesOwnership,
                                                string customQuestionBase = null, bool emailNotification = true,
                                                bool smsNotification = false, bool useDBCollector = false)
        {
            return SaveMultiple(dataProvider, loggedOperator, objectsToSave, null, askIfFinishParentIssue, askIfChangeDevicesOwnership, null, customQuestionBase, emailNotification, smsNotification, useDBCollector);
        }

        public static List<OpTask> SaveMultiple(DataProvider dataProvider, OpOperator loggedOperator, List<OpTask> objectsToSave, string note,
                                                AskIfFinishParentIssueDelegate askIfFinishParentIssue,
                                                AskIfChangeDevicesOwnership askIfChangeDevicesOwnership,
                                                AskIfChangeStatusWithNoFitterVisitDateDelegate askIfChangeStatusWithNoFitterVisitDate,
                                                string customQuestionBase = null, bool emailNotification = true,
                                                bool smsNotification = false, bool useDBCollector = false)
        {
            bool applyToAll = false;
            bool devicesOwnershipApplyToAll = false;
            bool cancel = false;
            bool[] finishParentIssue = new bool[objectsToSave.Count];
            bool applyToAllStatus = false;
            bool[] applyStatus = new bool[objectsToSave.Count];
            bool? applyToAllStatusChoice = null;
            DateTime selectedDate = DateTime.Now;
            List<int> taskIdsWithDateChanged = new List<int>();

            Dictionary<int, List<OpDepositoryElement>> devicesToChangeOwnership = new Dictionary<int, List<OpDepositoryElement>>();

            for (int i = 0; i < objectsToSave.Count; i++)
            {
                if (!applyToAllStatus)
                {
                    if (askIfChangeStatusWithNoFitterVisitDate != null)
                    {
                        applyStatus[i] = askIfChangeStatusWithNoFitterVisitDate(dataProvider, objectsToSave[i], ref selectedDate, out cancel, ref applyToAllStatus);

                        if (applyStatus[i])
                        {
                            taskIdsWithDateChanged.Add(objectsToSave[i].IdTask);
                        }

                        if (applyToAllStatus && applyToAllStatusChoice == null)
                        {
                            applyToAllStatusChoice = applyStatus[i];
                        }

                        if (cancel)
                        {
                            // Z uwagi, że po wciśnięciu Cancel nie dojdzie do zapisu jakiegokolwiek taska, należy przywrócić
                            // stary stan statusu i daty wizyty serwisanta (tam gdzie została zmieniona).
                            Dictionary<int, OpTask> oldTasks = dataProvider.GetTaskFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                                IdTask: objectsToSave.Select(x => x.IdTask).ToArray()).ToDictionary(k => k.IdTask);

                            int[] changedDateTaskIds = taskIdsWithDateChanged.ToArray();

                            foreach (OpTask task in objectsToSave)
                            {
                                task.TaskStatus = dataProvider.GetTaskStatus(oldTasks[task.IdTask].IdTaskStatus);

                                if (task.IdTask.In(changedDateTaskIds))
                                {
                                    task.FitterVisitDate = oldTasks[task.IdTask].FitterVisitDate;
                                }
                            }

                            return objectsToSave;
                        }
                    }
                    else
                    {
                        applyStatus[i] = true;
                    }
                }
                else
                {
                    applyStatus[i] = applyToAllStatusChoice.Value;
                    if (applyStatus[i] && objectsToSave[i].FitterVisitDate == null)
                    {
                        objectsToSave[i].FitterVisitDate = selectedDate;
                    }
                    else if (objectsToSave[i].FitterVisitDate != null)
                    {
                        applyStatus[i] = true;
                    }
                }

                if (!devicesToChangeOwnership.ContainsKey(objectsToSave[i].IdTask))
                    devicesToChangeOwnership.Add(objectsToSave[i].IdTask, new List<OpDepositoryElement>());

                if (objectsToSave[i].TaskStatus.IsFinished && askIfChangeDevicesOwnership != null)
                {
                    devicesToChangeOwnership[objectsToSave[i].IdTask] = askIfChangeDevicesOwnership(dataProvider, objectsToSave[i], ref devicesOwnershipApplyToAll);
                }

                if (objectsToSave[i].OpState == OpChangeState.Loaded)
                    continue;

                if (!applyToAll)
                {
                    string question = null;
                    if (customQuestionBase != null)
                        question = String.Format("{0} ({1})", customQuestionBase, objectsToSave[i]);

                    if (objectsToSave[i].IdIssue.HasValue)
                    {
                        //pobieramy issue z bazy zeby miec swieze informacje
                        objectsToSave[i].Issue = IssueComponent.GetByID(dataProvider, (int)objectsToSave[i].IdIssue, true);
                        finishParentIssue[i] = askIfFinishParentIssue(dataProvider, objectsToSave[i], question, true, out cancel, out applyToAll, MapTaskStatus(dataProvider, objectsToSave[i].TaskStatus));
                    }
                }
                else
                    finishParentIssue[i] = finishParentIssue[i - 1];

                if (cancel)
                    return objectsToSave;
            }

            List<OpTask> saved = new List<OpTask>(objectsToSave.Count);
            for (int i = 0; i < objectsToSave.Count; i++)
            {
                try
                {
                    if (!applyStatus[i]) continue;

                    saved.Add(Save(dataProvider, loggedOperator, objectsToSave[i], note, finishParentIssue[i], emailNotification, devicesToChangeOwnership.TryGetValue(objectsToSave[i].IdTask), smsNotification, useDBCollector));
                }
                catch (Exception ex)
                {
                    ex.Data.Add("ObjectCausingException", objectsToSave[i]);
                    throw ex;
                }
            }
            return saved;
        }
        #endregion
        #endregion

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, string notes, bool finishParentIssue, List<OpDepositoryElement> devicesToChangeOwner, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, notes, finishParentIssue, true, devicesToChangeOwner, smsNotification, useDBCollector);
        }

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, string notes,
                                  bool finishParentIssue, bool emailNotification,
                                  List<OpDepositoryElement> devicesToChangeOwner,
                                  bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, notes, finishParentIssue, emailNotification, smsNotification, devicesToChangeOwner, useDBCollector);
        }

        public static OpTask Save(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, string notes,
                                  bool finishParentIssue, bool emailNotification, bool smsNotification,
                                  List<OpDepositoryElement> devicesToChangeOwner,
                                  bool useDBCollector = false)
        {           
            bool isNewItem = objectToSave.IdTask == 0; //check if it's new task
            bool dataListChanged = !objectToSave.DataList.All(w => w.OpState == OpChangeState.Loaded);
            bool saved = false;

            OpTask originalTask = null;
            if (objectToSave.IdTask > 0)
            {
                originalTask = dataProvider.GetTask(objectToSave.IdTask, true);
            }

            if (objectToSave.OpState == OpChangeState.Loaded && !dataListChanged && String.IsNullOrEmpty(notes))
                return objectToSave;

            try
            {
                if (isNewItem || objectToSave.OpState == OpChangeState.Modified)
                {
                    objectToSave.IdTask = dataProvider.SaveTask(objectToSave);
                    objectToSave.OpState = OpChangeState.Loaded;
                    saved = true;
                }

                #region RoutePoint
                RouteComponent.GetRoutePoint(dataProvider, objectToSave);
                #endregion

                #region TaskData
                //zapis do tabeli TASK_DATA
                if (dataListChanged || isNewItem)
                {
                    //jeśli zaznaczone, że zapłacone i nie ustawiono daty zapłaty to wstaw dzisiaj
                    if (objectToSave.FitterPaymentDone && !objectToSave.FitterPaymentDate.HasValue)
                        objectToSave.FitterPaymentDate = DateTime.Today;

                    SaveTaskData(dataProvider, objectToSave.IdTask, objectToSave.DataList, isNewItem);//docelow to ma zastąpić pętle poniżej

                    //OpTaskData data;
                    //for (int i = 0; i < objectToSave.DataList.Count; i++)
                    //{
                    //    data = objectToSave.DataList[i];
                    //    data.IdTask = objectToSave.IdTask;
                    //    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    //    {
                    //        DataComponent.Delete(dataProvider, data);
                    //    }
                    //    else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                    //    {
                    //        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                    //        if (objectToSave.DataList[i].OpState == OpChangeState.New)
                    //            objectToSave.DataList[i].AssignReferences(dataProvider);

                    //        objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                    //    }
                    //}
                    //objectToSave.DataList.RemoveAll(d => d.OpState == OpChangeState.Delete);

                    saved = true;
                }

                #endregion
                #region TaskHistory

                if (originalTask == null || (originalTask.IdTaskStatus != objectToSave.IdTaskStatus) ||
                        !String.IsNullOrEmpty(notes))
                {
                    AddTaskHistory(dataProvider, loggedOperator, objectToSave, notes);
                }
                if ((originalTask == null || (originalTask.IdTaskStatus != objectToSave.IdTaskStatus)) && objectToSave.IdLocation.HasValue)
                {
                    if (objectToSave.Location == null)
                    {
                        objectToSave.Location = dataProvider.GetLocation(objectToSave.IdLocation.Value);
                    }
                    ProceedTaskStatusChangeOperation(dataProvider, loggedOperator, originalTask == null ? null : (int?)originalTask.IdTaskStatus, objectToSave.IdTaskStatus, objectToSave.Location);
                }

                #endregion
                #region TaskDataLatch

                //OperatorPerformer change
                if (originalTask != null && originalTask.IdOperatorPerformer.HasValue && objectToSave != null && objectToSave.IdOperatorPerformer.HasValue
                    && originalTask.IdOperatorPerformer != objectToSave.IdOperatorPerformer)
                {
                    TaskProceedOperatorPerformerChangedStepLatch(dataProvider, objectToSave, (originalTask.OperatorPerformer != null ? originalTask.OperatorPerformer : dataProvider.GetOperator(objectToSave.IdOperatorPerformer.Value)), null);
                }

                #endregion
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.TaskAdditionError, ex.Message);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdTask, ex.Message);
                        LogError(EventID.Forms.TaskSavingError, objectToSave.IdTask, ex.Message);
                }
                throw ex;
            }
            Exception logException = null;
            try
            {
                if (saved && LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdTask);
                        LogSuccess(EventID.Forms.TaskAdded, objectToSave.IdTask);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdTask);
                        LogSuccess(EventID.Forms.TaskSaved, objectToSave.IdTask);
                }
                if (saved && emailNotification)
                {
                    Dictionary<OpTask, OpTask> email = new Dictionary<OpTask, OpTask>();
                    email.Add(objectToSave, originalTask);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, email, isNewItem ? EmailComponent.OperationType.Add : EmailComponent.OperationType.Change);
                    try
                    {
                        TaskStatusChangeEmail(dataProvider, objectToSave, originalTask, loggedOperator, 2, notes);
                    }
                    catch (Exception taskStatusException)
                    {
                        logException = taskStatusException;
                    }
                }
                if (saved && smsNotification)
                {
                    Dictionary<OpTask, OpTask> sms = new Dictionary<OpTask, OpTask>();
                    sms.Add(objectToSave, originalTask);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, isNewItem ? SmsComponent.OperationType.Add : SmsComponent.OperationType.Change, useDBCollector);                    
                }
            }
            catch (Exception ex)
            {
                logException = ex;//nie chcemy żeby wyleciał przez to zapis
            }

            #region FinishParentIssue
            //check for data change
            if (originalTask == null || (originalTask.IdTaskStatus != objectToSave.IdTaskStatus) ||
                !String.IsNullOrEmpty(notes))
            {
                //zmieniamy status zgłszenia rodzica, jeśli takowe istnieje i jeśli przyszla taka informacja z gory
                if (objectToSave.IdIssue.HasValue && finishParentIssue)
                {
                    OpIssue parentIssue = null;
                    parentIssue = objectToSave.Issue ?? IssueComponent.GetByID(dataProvider, objectToSave.IdIssue.Value);
                    parentIssue.IssueStatus = dataProvider.GetIssueStatus(MapTaskStatus(dataProvider, objectToSave.TaskStatus).IdIssueStatus);
                    try
                    {
                        IssueComponent.Save(dataProvider, loggedOperator, parentIssue, smsNotification, useDBCollector);
                        if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.IssueSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdIssue);
                            LogSuccess(EventID.Forms.IssueSaved, objectToSave.IdIssue);
                    }
                    catch (Exception ex)
                    {
                        if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.IssueSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdIssue, ex.Message);
                            LogError(EventID.Forms.IssueSavingError, objectToSave.IdIssue, ex.Message);
                        throw ex;
                    }
                }

                if (objectToSave.TaskStatus.IsFinished)
                {
                    //LPIELA SERVICE_MAGAZINE
                    /*
                    List<OpTaskHistory> taskHistory = dataProvider.GetTaskHistoryFilter(IdTask: new int[] { objectToSave.IdTask });
                    if (taskHistory != null)
                    {
                        if (taskHistory.Count(t => t.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_succesfully) == 1)//nie chcemy ponownie tych statusow synchronizowac zeby nie nabalaganić
                        {
                            List<OpDepositoryElement> mountedDevices = DepositoryComponent.GetElements(dataProvider, objectToSave.IdTask, false, false, null);
                            List<OpDepositoryElement> unmountedDevices = DepositoryComponent.GetElements(dataProvider, objectToSave.IdTask., false, true, null);

                            if (unmountedDevices != null)
                            {
                                foreach (OpDepositoryElement depElem in unmountedDevices)
                                {
                                    DepositoryComponent.AddToServiceMagazine(dataProvider, depElem, Enums.DeviceState.SERVICED, objectToSave, DepositoryComponent.MagazineAdd.TaskAcceptation);
                                }
                            }
                            if(mountedDevices != null)
                            {
                                foreach(OpDepositoryElement depElem in mountedDevices)
                                {
                                    if(depElem.SerialNbr.HasValue)
                                        DepositoryComponent.RemoveFromServiceMagazine(dataProvider, depElem, depElem.Device.Distributor);
                                    else
                                        DepositoryComponent.RemoveFromServiceMagazine(dataProvider, depElem, objectToSave.Location.Distributor);
                                }
                            }
                        }
                    }
                    */
                }
            }

            #endregion
            #region ChangeUsedDevicesOwnership

            if (objectToSave.TaskStatus.IsFinished
                && devicesToChangeOwner != null && devicesToChangeOwner.Count > 0
                && objectToSave.Distributor.DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END && d.Value != null && Convert.ToBoolean(d.Value)))
            {
                TaskComponent.ChangeTaskUsedDevicesOwnership(dataProvider, loggedOperator, objectToSave, devicesToChangeOwner);
            }

            #endregion

            if (logException != null)
            {
                throw logException;
            }

            return objectToSave;//dataProvider.GetTask(objectToSave.IdTask);
        }

        #endregion

        #region SaveTaskData

        public static void SaveTaskData(DataProvider dataProvider, int idTask, List<OpTaskData> dataList, bool isNewItem = false)
        {
            if (dataList != null)
            {
                OpDataList<OpTaskData> taskDataList = new OpDataList<OpTaskData>(dataList);
                SaveTaskData(dataProvider, idTask, taskDataList, isNewItem);
            }
        }

        public static void SaveTaskData(DataProvider dataProvider, int idTask, OpDataList<OpTaskData> dataList, bool isNewItem = false)
        {
            OpTaskData data;
            for (int i = 0; i < dataList.Count; i++)
            {
                data = dataList[i];
                data.IdTask = idTask;
                if (dataList[i].OpState == OpChangeState.Delete)
                {
                    DataComponent.Delete(dataProvider, data);
                }
                else if (isNewItem || (dataList[i].OpState == OpChangeState.Modified || dataList[i].OpState == OpChangeState.New))
                {
                    dataList[i].IdData = DataComponent.Save(dataProvider, data);
                    if (dataList[i].OpState == OpChangeState.New)
                        dataList[i].AssignReferences(dataProvider);

                    dataList[i].OpState = OpChangeState.Loaded;
                }
            }
            dataList.RemoveAll(d => d.OpState == OpChangeState.Delete);
        }

        #endregion

        #region GetStatusesThatRequiresFitterVisitDate
        public static List<OpTaskStatus.Enum> GetStatusesThatRequiresFitterVisitDate()
        {
            List<OpTaskStatus.Enum> statuses = new List<OpTaskStatus.Enum>()
            {
                OpTaskStatus.Enum.Finished_succesfully,
                OpTaskStatus.Enum.Finished_with_error,
                OpTaskStatus.Enum.Verified,
                OpTaskStatus.Enum.Rejected,
                OpTaskStatus.Enum.RealizationNotPossible
            };

            return statuses;
        }
        #endregion

        #region StatusRequiresFitterVisitDate
        public static bool StatusRequiresFitterVisitDate(OpTaskStatus.Enum status)
        {
            return status.In(GetStatusesThatRequiresFitterVisitDate().ToArray());
        }
        #endregion

        #region AddTaskHistory

        public static void AddTaskHistory(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave)
        {
            AddTaskHistory(dataProvider, loggedOperator, objectToSave, null);
        }

        public static void AddTaskHistory(DataProvider dataProvider, OpOperator loggedOperator, OpTask objectToSave, string notes)
        {
            try
            {
                List<OpTaskHistory> history = GetHistory(dataProvider, objectToSave.IdTask);
                if (history.Count > 0)
                {
                    history.Last().EndDate = dataProvider.DateTimeNow;
                    dataProvider.SaveTaskHistory(history.Last());
                }

                OpTaskHistory taskHistory = new OpTaskHistory();
                taskHistory.Task = objectToSave;
                taskHistory.StartDate = dataProvider.DateTimeNow;
                taskHistory.TaskStatus = objectToSave.TaskStatus;
                taskHistory.Operator = loggedOperator;
                taskHistory.Notes = notes;

                dataProvider.SaveTaskHistory(taskHistory);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskHistorySaved, LoggedOperator.Login, ServerCORE, objectToSave.IdTask);
                    LogSuccess(EventID.Forms.TaskHistorySaved, objectToSave.IdTask);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskHistorySavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdTask, ex.Message);
                    LogError(EventID.Forms.TaskHistorySavingError, objectToSave.IdTask, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region ClearTaskGroup

        public static void ClearTaskGroup(DataProvider dataProvider, OpOperator loggedOperator, int idTaskGroup)
        {
            try
            {
                foreach (var item in dataProvider.GetTaskFilter(IdTaskGroup: new int[] { idTaskGroup }))
                {
                    item.TaskGroup = null;
                    try
                    {
                        Save(dataProvider, loggedOperator, item);
                        if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskSaved, LoggedOperator.Login, ServerCORE, item.IdTask);
                            LogSuccess(EventID.Forms.TaskSaved, item.IdTask);
                    }
                    catch (Exception ex)
                    {
                        if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskSavingError, LoggedOperator.Login, ServerCORE, item.IdTask, ex.Message);
                            LogError(EventID.Forms.TaskSavingError, item.IdTask, ex.Message);
                        throw ex;
                    }
                }
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupSaved, LoggedOperator.Login, ServerCORE, idTaskGroup);
                    LogSuccess(EventID.Forms.TaskGroupSaved, idTaskGroup);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupSavingError, LoggedOperator.Login, ServerCORE, idTaskGroup, ex.Message);
                    LogError(EventID.Forms.TaskGroupSavingError, idTaskGroup, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region ClearWorkOrder

        public static void ClearWorkOrder(DataProvider dataProvider, OpOperator loggedOperator, long idRoute, AskIfFinishParentIssueDelegate askIfFinishParentIssueDelegate)
        {
            try
            {
                foreach (var item in dataProvider.GetTaskFilter(IdPlannedRoute: new long[] { idRoute }))
                {
                    //ustawiamy status taska na poprzedni
                    OpTaskHistory taskHistory = GetPreviousStatus(dataProvider, item.IdTask);
                    if (taskHistory != null)
                        item.TaskStatus = taskHistory.TaskStatus;
                    item.OperatorPerformer = null;
                    item.Route = null;
                    Save(dataProvider, loggedOperator, item, askIfFinishParentIssueDelegate, null);
                }
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderSaved, LoggedOperator.Login, ServerCORE, idRoute);
                    LogSuccess(EventID.Forms.WorkOrderSaved, idRoute);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderSavingError, LoggedOperator.Login, ServerCORE, idRoute, ex.Message);
                    LogError(EventID.Forms.WorkOrderSavingError, idRoute, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region FindTask

        public static OpTask FindTask(DataProvider dataProvider, long id_location, int idTaskType)
        {
            return dataProvider.GetTaskFilter(IdLocation: new long[] { id_location }, IdTaskType: new int[] { idTaskType }).Find(
                t => (t.IdTaskStatus == (int)OpTaskStatus.Enum.In_progress ||
                t.IdTaskStatus == (int)OpTaskStatus.Enum.Not_Started ||
                t.IdTaskStatus == (int)OpTaskStatus.Enum.WaitingForAcceptance ||
                t.IdTaskStatus == (int)OpTaskStatus.Enum.Planned));
        }

        #endregion

        #region Files
        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, string filePath, string fileDescription = null)
        {
            return UploadFile(dataProvider, loggedOperator, task, filePath, false, fileDescription);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, string filePath, bool useTestFolders, string fileDescription = null, bool onlyFileUpdate = false, Enums.FileType fileType = Enums.FileType.Photo)
        {
            if (File.Exists(filePath))
            {
                bool ftpUploadSuccess = false;
                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    BaseComponent.Log(EventID.Forms.FTPInit, true, new object[] { FTPSettings.Address, FTPSettings.UserName, useTestFolders });

                    bool createDirectory = true;//!task.DataList.Exists(d => d.IdDataType == DataType.TASK_ATTACHMENT && d.IdTaskData != 0);

                    ftpUploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", task.IdTask), filePath, createDirectory/*true*/);
                    if (!ftpUploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, String.Format("FTP Upload file failed: Unknown error. FTP path: {0}, local path: {1}, create directory: {2}",
                                                            String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", task.IdTask),
                                                            filePath,
                                                            createDirectory));
                        //return null;
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    //throw ex;
                }
                try
                {
                    OpFile file = null;
                    if (onlyFileUpdate)
                    {
                        #region TryFindExistingFile

                        bool dataReeloaded = false;
                        if (!task.DataList.Exists(d => d.IdDataType == DataType.TASK_ATTACHMENT && d.Value != null))
                        {
                            task.DataList.RemoveAll(d => d.IdDataType.In(new long[] { DataType.TASK_ATTACHMENT, DataType.TASK_ATTACHMENT_TYPE }));
                            task.DataList.AddRange(dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask }, IdDataType: new long[] { DataType.TASK_ATTACHMENT, DataType.TASK_ATTACHMENT_TYPE }, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted));
                            dataReeloaded = true;
                        }
                        List<long> idFileList = new List<long>();
                        task.DataList.Where(d => d.IdDataType == DataType.TASK_ATTACHMENT && d.Value != null).ToList().ForEach(t => { long idFile = 0; if (long.TryParse(t.Value.ToString(), out  idFile) && idFile > 0) idFileList.Add(idFile); });
                        OpFile fTmp = null;
                        if (idFileList.Count > 0)
                        {
                            List<OpFile> fList = dataProvider.GetFile(idFileList.ToArray());
                            fTmp = fList.Find(f => String.Equals(f.PhysicalFileName, Path.GetFileName(filePath)));
                            if (fTmp == null && !dataReeloaded)
                            {
                                task.DataList.RemoveAll(d => d.IdDataType.In(new long[] { DataType.TASK_ATTACHMENT, DataType.TASK_ATTACHMENT_TYPE }));
                                task.DataList.AddRange(dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask }, IdDataType: new long[] { DataType.TASK_ATTACHMENT, DataType.TASK_ATTACHMENT_TYPE }, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted));
                                List<long> newIdFileList = new List<long>();
                                task.DataList.Where(d => d.IdDataType == DataType.TASK_ATTACHMENT && d.Value != null).ToList().ForEach(t => { long idFile = 0; if (long.TryParse(t.Value.ToString(), out  idFile) && idFile > 0) newIdFileList.Add(idFile); });
                                newIdFileList = newIdFileList.Except(idFileList).ToList();
                                if (newIdFileList.Count > 0)
                                {
                                    fList.AddRange(dataProvider.GetFile(newIdFileList.ToArray()));
                                    fTmp = fList.Find(f => String.Equals(f.PhysicalFileName, Path.GetFileName(filePath)));
                                }
                            }
                        }
                        if (fTmp != null)
                        {
                            file = fTmp;
                            if (file == null)
                                dataProvider.GetFileContent(file);
                        }
                        else
                        {
                            file = new OpFile();
                            file.PhysicalFileName = Path.GetFileName(filePath);
                            file.Size = new FileInfo(filePath).Length;
                            file.InsertDate = dataProvider.DateTimeNow;
                            file.FileBytes = new byte[1] { 0x00 };
                            file.Description = fileDescription;
                        }

                        #endregion
                    }
                    //if (!onlyFileUpdate)//sprawdzenie mamy do czynienia jedynie z aktualizacją pliku o nazwie istniejącej już w ramach zadania
                    //{
                    //string tmpDownloadPath = ftpClient.DownloadFile(Path.GetTempPath(), String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", task.IdTask), Path.GetFileName(filePath));
                    //if (File.Exists(tmpDownloadPath))
                    //{
                    if (file == null)
                    {
                        file = new OpFile();
                        file.PhysicalFileName = Path.GetFileName(filePath);
                        file.Size = new FileInfo(filePath).Length;
                        file.InsertDate = dataProvider.DateTimeNow;
                        file.FileBytes = new byte[1] { 0x00 };
                        file.Description = fileDescription;
                    }
                    if (fileType.In(new Enums.FileType[] { Enums.FileType.ClientSignature, Enums.FileType.FitterSignature }) || !ftpUploadSuccess)
                    {
                        byte[] fileByteArray = File.ReadAllBytes(filePath);
                        if (fileByteArray != null)
                            file.FileBytes = fileByteArray;
                    }
                    bool isNewFile = file.IdFile == 0;
                    file.IdFile = dataProvider.SaveFile(file);
                    if (file.FileBytes != null)
                        dataProvider.SaveFileContent(file);
                    if (isNewFile)
                    {
                        int nextIndex = task.DataList.GetNextIndex(DataType.TASK_ATTACHMENT);
                        task.DataList.SetValue(DataType.TASK_ATTACHMENT, nextIndex, file.IdFile);
                        task.DataList.SetValue(DataType.TASK_ATTACHMENT_TYPE, nextIndex, (int)fileType);
                        Save(dataProvider, loggedOperator, task);
                        if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
                            LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                    }

                    //File.Delete(tmpDownloadPath);//dla bezpieczeństwa umiesczzone po zapisie TASK_DATA

                    if (file.IdFile > 0)
                    {
                        return file;
                    }
                    //}
                    //}
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }
            return null;
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpTaskData taskAttachmentData, string filePath, bool useTestFolders, string fileDescription = null, bool onlyFileUpdate = false, Enums.FileType fileType = Enums.FileType.Photo, DateTime? insertDate = null)
        {
            if (File.Exists(filePath))
            {
                bool ftpUploadSuccess = false;
                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);

                    bool createDirectory = true;//!task.DataList.Exists(d => d.IdDataType == DataType.TASK_ATTACHMENT && d.IdTaskData != 0);

                    ftpUploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", taskAttachmentData.IdTask), filePath, createDirectory/*true*/);
                    if (!ftpUploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, String.Format("FTP Upload file failed: Unknown error. FTP path: {0}, local path: {1}, create directory: {2}",
                                                            String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", taskAttachmentData.IdTask),
                                                            filePath,
                                                            createDirectory));
                        //return null;
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    //throw ex;
                }
                try
                {
                    OpFile file = null;
                    if (taskAttachmentData.Value != null)
                        file = dataProvider.GetFile(Convert.ToInt64(taskAttachmentData.Value));
                    else
                        file = new OpFile();
                    file.PhysicalFileName = Path.GetFileName(filePath);
                    file.Size = new FileInfo(filePath).Length;
                    if (insertDate.HasValue)
                        file.InsertDate = insertDate.Value;
                    else
                        file.InsertDate = dataProvider.DateTimeNow;
                    file.FileBytes = new byte[1] { 0x00 };
                    file.Description = fileDescription;

                    if (fileType.In(new Enums.FileType[] { Enums.FileType.ClientSignature, Enums.FileType.FitterSignature }) || !ftpUploadSuccess)
                    {
                        byte[] fileByteArray = File.ReadAllBytes(filePath);
                        if (fileByteArray != null)
                            file.FileBytes = fileByteArray;
                    }
                    bool isNewFile = file.IdFile == 0;
                    file.IdFile = dataProvider.SaveFile(file);
                    if (file.FileBytes != null)
                        dataProvider.SaveFileContent(file);
                    if (isNewFile)
                    {
                        taskAttachmentData.Value = file.IdFile;
                        if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
                            LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                    }

                    if (file.IdFile > 0)
                    {
                        return file;
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }
            return null;
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, OpFile file, string savePath)
        {
            return DownloadFile(dataProvider, loggedOperator, task, file, savePath, false);
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, OpFile file, string savePath, bool useTestFolders)
        {
            try
            {
                dataProvider.GetFileContent(file);
                byte[] bytes = file.FileBytes as byte[];

                if (bytes == null || bytes.Length == 0 || bytes[0] == 0)
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    string filePathDownloaded = ftpClient.DownloadFile(savePath, String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", task.IdTask), file.PhysicalFileName);
                    if (filePathDownloaded != null)
                    {
                        file.LastDownloaded = dataProvider.DateTimeNow;
                        dataProvider.SaveFile(file);

                        return true;
                    }
                }
                else
                {
                    System.IO.File.WriteAllBytes(savePath + file.PhysicalFileName, bytes);

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, OpFile file, bool useTestFolders = false)
        {
            try
            {
                dataProvider.GetFileContent(file);
                byte[] result = file.FileBytes as byte[];

                if (result == null || result.Length == 0 || result[0] == 0)
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);

                    result = ftpClient.DownloadFile(String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", task.IdTask), file.PhysicalFileName);

                    if (result != null)
                    {
                        file.LastDownloaded = dataProvider.DateTimeNow;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, OpFile file)
        {
            return DeleteFile(dataProvider, loggedOperator, task, file, false);
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, OpFile file, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                ftpClient.DeleteFile(String.Format(@"{0}/{1}/", useTestFolders ? "TaskTest" : "Task", task.IdTask), file.PhysicalFileName);

                int index = task.DataList.First(d => d.IdDataType == DataType.TASK_ATTACHMENT && d.GetValue<long>() == file.IdFile).Index;
                DeleteData(dataProvider, task, DataType.TASK_ATTACHMENT, index);
                task.DataList.DeleteValue(DataType.TASK_ATTACHMENT, index);
                DeleteData(dataProvider, task, DataType.TASK_ATTACHMENT_TYPE, index);
                task.DataList.DeleteValue(DataType.TASK_ATTACHMENT_TYPE, index);
                dataProvider.DeleteFile(file);
                Save(dataProvider, loggedOperator, task);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeleted, LoggedOperator.Login, ServerCORE, file.IdFile);
                    LogSuccess(EventID.Forms.AttachmentDeleted, file.IdFile);
                return true;
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeletionError, LoggedOperator.Login, ServerCORE, file.IdFile, ex.Message);
                    LogError(EventID.Forms.AttachmentDeletionError, file.IdFile, ex.Message);
                throw ex;
            }
        }
        #endregion

        #region Delete

        public static void DeleteHistory(DataProvider dataProvider, int idTask)
        {
            foreach (var item in dataProvider.GetTaskHistoryFilter(IdTask: new int[] { idTask }))
            {
                dataProvider.DeleteTaskHistory(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, int idTask)
        {
            foreach (var item in dataProvider.GetTaskDataFilter(IdTask: new int[1] { idTask }, IdDataType: DataProvider.TaskDataTypes.ToArray()))
            {
                dataProvider.DeleteTaskData(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, OpTask task, long idDataType, int index)
        {
            long idData = task.DataList.First(d => d.IdDataType == idDataType && d.Index == index).IdData;
            if (idData > 0)
                dataProvider.DeleteTaskData(dataProvider.GetTaskData(idData));
        }

        public static void Delete(DataProvider dataProvider, OpTask objectToDelete)
        {
            Delete(dataProvider, objectToDelete, true);
        }

        public static void Delete(DataProvider dataProvider, OpTask objectToDelete, bool emailNotification, OpOperator loggedOperator = null)
        {
            Delete(dataProvider, objectToDelete, emailNotification, false, loggedOperator, useDBCollector: false);
        }

        public static void Delete(DataProvider dataProvider, OpTask objectToDelete, bool emailNotification, bool smsNotification, OpOperator loggedOperator = null, bool useDBCollector = false)
        {
            try
            {
                List<OpRoutePoint> relatedRoutePoints = dataProvider.GetRoutePointFilter(loadCustomData: false, loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, IdTask: new int[] { objectToDelete.IdTask });
                if (relatedRoutePoints != null && relatedRoutePoints.Count > 0)
                {
                    foreach (OpRoutePoint rpItem in relatedRoutePoints)
                        RouteComponent.DeleteRoutePoint(dataProvider, loggedOperator, rpItem);
                }

                DeleteHistory(dataProvider, objectToDelete.IdTask);
                DeleteData(dataProvider, objectToDelete.IdTask);
                DepositoryComponent.DeleteTaskElements(dataProvider, objectToDelete);
                dataProvider.DeleteTask(objectToDelete);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdTask);
                    LogSuccess(EventID.Forms.TaskDeleted, objectToDelete.IdTask);
                if (emailNotification)
                {
                    Dictionary<OpTask, OpTask> email = new Dictionary<OpTask, OpTask>();
                    email.Add(objectToDelete, null);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, email, EmailComponent.OperationType.Delete);
                }
                if (smsNotification)
                {
                    Dictionary<OpTask, OpTask> sms = new Dictionary<OpTask, OpTask>();
                    sms.Add(objectToDelete, null);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, SmsComponent.OperationType.Delete, useDBCollector);                    
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdTask, ex.Message);
                    LogError(EventID.Forms.TaskDeletionError, objectToDelete.IdTask, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region MapTaskStatus

        private static OpIssueStatus MapTaskStatus(DataProvider dataProvider, OpTaskStatus task_status)
        {
            OpIssueStatus issue_status;

            switch (task_status.ID_TASK_STATUS)
            {
                case (int)OpTaskStatus.Enum.Canceled:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.FINISHED);
                    break;
                case (int)OpTaskStatus.Enum.Finished_succesfully:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.FINISHED);
                    break;
                case (int)OpTaskStatus.Enum.Finished_with_error:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.FINISHED);
                    break;
                case (int)OpTaskStatus.Enum.In_progress:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.IN_PROGRESS);
                    break;
                case (int)OpTaskStatus.Enum.Not_Started:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.ACCEPTED);
                    break;
                case (int)OpTaskStatus.Enum.WaitingForAcceptance:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.WAITING_FOR_RESPONSE);
                    break;
                case (int)OpTaskStatus.Enum.Planned:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.QUEUED);
                    break;
                case (int)OpTaskStatus.Enum.Suspended:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.SUSPENDED);
                    break;
                default:
                    issue_status = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.NEW);
                    break;
            }

            return issue_status;
        }

        #endregion

        #region SuspendTask

        public static void SuspendTask(DataProvider dataProvider, OpOperator loggedOperator, int idTask, DateTime expectedReopenDate, string notes, Enums.TaskSuspensionReason suspensionReason, bool smsNotification = false, bool useDBCollector = false)
        {
            SuspendTask(dataProvider, loggedOperator, dataProvider.GetTask(idTask), expectedReopenDate, notes, suspensionReason, smsNotification, useDBCollector);
        }

        public static void SuspendTask(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, DateTime expectedReopenDate, string notes, Enums.TaskSuspensionReason suspensionReason, bool smsNotification = false, bool useDBCollector = false)
        {
            BaseComponent.Log(EventID.Forms.TaskSuspendParams, true, new object[] { (loggedOperator != null ? loggedOperator.ToString() : "<null>"), (task != null ? task.IdTask.ToString() : "<null>"), expectedReopenDate, notes, suspensionReason.ToString() });
            task.DataList.RemoveAll(t => t.IdDataType == DataType.TASK_SUSPENSION_REASON);
            task.DataList.AddRange(dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask }, IdDataType: new long[] { DataType.TASK_SUSPENSION_REASON }, loadNavigationProperties: false));

            if (loggedOperator != null)
            {
                task.IdTaskStatus = (int)OpTaskStatus.Enum.Suspended;
                task.TaskStatus = dataProvider.GetTaskStatus(task.IdTaskStatus);
                TaskComponent.Save(dataProvider, loggedOperator, task, notes, finishParentIssue: true, devicesToChangeOwner: null, smsNotification: smsNotification, useDBCollector: useDBCollector);
                task.TaskHistory = TaskComponent.GetHistory(dataProvider, task.IdTask);

                //zapisanie typu powodu zawieszenia
                OpTaskData suspensionReasonTaskData = new OpTaskData();
                suspensionReasonTaskData.IdTask = task.IdTask;
                suspensionReasonTaskData.IdDataType = DataType.TASK_SUSPENSION_REASON;
                suspensionReasonTaskData.Index = task.TaskHistory.Find(t => !t.EndDate.HasValue).IdTaskHistory; //model.Task.DataList.GetNextIndex(DataType.TASK_SUSPENSION_REASON);
                suspensionReasonTaskData.Value = (int)suspensionReason;
                task.DataList.Add(suspensionReasonTaskData);
                suspensionReasonTaskData.IdTaskData = dataProvider.SaveTaskData(suspensionReasonTaskData);
                //zapisanie spodziewanej daty wznowienia
                OpTaskData expectedReopenDateTaskData = new OpTaskData();
                expectedReopenDateTaskData.IdTask = task.IdTask;
                expectedReopenDateTaskData.IdDataType = DataType.TASK_EXPECTED_REOPEN_DATE;
                expectedReopenDateTaskData.Index = suspensionReasonTaskData.Index;
                expectedReopenDateTaskData.Value = expectedReopenDate;//Convert.ToDateTime(expectedReopenDate);
                task.DataList.Add(expectedReopenDateTaskData);
                expectedReopenDateTaskData.IdTaskData = dataProvider.SaveTaskData(expectedReopenDateTaskData);

                //dodatkowo wyslanie maila do representanta dystrybutora
                List<OpDistributorData> distributorData = dataProvider.GetDistributorDataFilter(
                    IdDistributor: new int[] { task.IdDistributor },
                    IdDataType: new long[] { DataType.DISTRIBUTOR_AUTOMATICALLY_SUSPEND_ISSUE });
                foreach (OpDistributorData distrData in distributorData)
                {
                    if (distrData.IdDataType == DataType.DISTRIBUTOR_AUTOMATICALLY_SUSPEND_ISSUE)
                    {
                        if (Convert.ToBoolean(distrData.Value) == true)
                        {
                            if (task.IdIssue != null)
                            {
                                OpIssue issue = dataProvider.GetIssue(task.IdIssue.Value);
                                if (issue != null)
                                {
                                    BaseComponent.Log(EventID.Forms.TaskSuspendAutoIssueSuspensd, true, new object[] { task.IdTask, task.IdIssue });
                                    issue.IdIssueStatus = (int)OpIssueStatus.Enum.SUSPENDED;
                                    issue.IssueStatus = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.SUSPENDED);
                                    IssueComponent.Save(dataProvider, loggedOperator, issue, notes, false, smsNotification: smsNotification, useDBCollector: useDBCollector);
                                }
                            }
                        }
                    }
                }

            }
        }

        #endregion

        #region ChangeTaskUsedDevicesOwnership

        public static void ChangeTaskUsedDevicesOwnership(DataProvider dataProvider, OpOperator loggedOperator, OpTask task, List<OpDepositoryElement> depositoryItems = null)
        {
            if (task != null && task.TaskStatus.IsFinished)
            {
                List<OpDistributorData> taskDistributorData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false,
                                                                                                    IdDistributor: new int[] { task.IdDistributor },
                                                                                                    IdDataType: new long[]{ DataType.DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END,
                                                                                                                            DataType.DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END_UNINSTALL_ID_DISTRIBUTOR_OWNER });
                bool changeOwnership = false;
                OpDistributor targetOwner = null;
                if (taskDistributorData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END && d.Value != null && Convert.ToBoolean(d.Value)))
                    changeOwnership = Convert.ToBoolean(taskDistributorData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END && d.Value != null && Convert.ToBoolean(d.Value)).Value);
                if (taskDistributorData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END_UNINSTALL_ID_DISTRIBUTOR_OWNER && d.Value != null))
                {
                    int targetOwnerId = Convert.ToInt32(taskDistributorData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_CHANGE_DEVICE_OWNERSHIP_AT_TASK_END_UNINSTALL_ID_DISTRIBUTOR_OWNER && d.Value != null).Value);
                    targetOwner = dataProvider.GetDistributor(targetOwnerId);
                }
                if (changeOwnership && targetOwner != null)
                {
                    List<OpDepositoryElement> taskDepositoryItems = null;
                    if (depositoryItems != null && depositoryItems.Count(t => t.SerialNbr.HasValue && t.IdTask == task.IdTask) > 0)
                        taskDepositoryItems = depositoryItems;
                    else
                        taskDepositoryItems = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false,
                                                                                      IdTask: new int[] { task.IdTask },
                                                                                      customWhereClause: "[SERIAL_NBR] is not null");
                    if (taskDepositoryItems != null && taskDepositoryItems.Count > 0)
                    {
                        List<OpDevice> deviceList = dataProvider.GetDeviceFilter(loadNavigationProperties: false, loadCustomData: false,
                                                                                 SerialNbr: taskDepositoryItems.Select(d => d.SerialNbr.Value).ToArray());
                        List<OpDeviceDetails> deviceDetailsList = dataProvider.GetDeviceDetailsFilter(loadNavigationProperties: false,
                                                                                                      SerialNbr: taskDepositoryItems.Select(d => d.SerialNbr.Value).ToArray());
                        List<OpDepositoryElementData> deviceDistributorLatch = dataProvider.GetDepositoryElementDataFilter(loadNavigationProperties: false,
                                                                                                                           IdDepositoryElement: taskDepositoryItems.Select(d => d.IdDepositoryElement).ToArray(),
                                                                                                                           IdDataType: new long[] { DataType.DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_LATCH,
                                                                                                                                                    DataType.DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_OWNER_LATCH });
                        foreach (OpDepositoryElement taskItem in taskDepositoryItems)
                        {
                            OpDevice dItem = deviceList.Find(d => d.SerialNbr == taskItem.SerialNbr.Value);
                            OpDeviceDetails ddItem = deviceDetailsList.Find(d => d.SerialNbr == taskItem.SerialNbr);
                            #region DistributorLatch

                            OpDepositoryElementData latchData = deviceDistributorLatch.Find(d => d.IdDepositoryElement == taskItem.IdDepositoryElement && d.IdDataType == DataType.DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_LATCH);
                            if (latchData == null)//jeżeli nie ma dodajemy, jeżeli jest zostawiamy zatrzask
                            {
                                latchData = new OpDepositoryElementData();
                                latchData.IdDepositoryElement = taskItem.IdDepositoryElement;
                                latchData.IdDataType = DataType.DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_LATCH;
                                latchData.Index = 0;
                                latchData.Value = dItem.IdDistributor;
                                latchData.IdDepositoryElementData = dataProvider.SaveDepositoryElementData(latchData);
                                deviceDistributorLatch.Add(latchData);
                            }
                            latchData = deviceDistributorLatch.Find(d => d.IdDepositoryElement == taskItem.IdDepositoryElement && d.IdDataType == DataType.DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_OWNER_LATCH);
                            if (latchData == null)//jeżeli nie ma dodajemy, jeżeli jest zostawiamy zatrzask
                            {
                                latchData = new OpDepositoryElementData();
                                latchData.IdDepositoryElement = taskItem.IdDepositoryElement;
                                latchData.IdDataType = DataType.DEPOSITORY_ELEMENT_TASK_DEVICE_ID_DISTRIBUTOR_OWNER_LATCH;
                                latchData.Index = 0;
                                latchData.Value = ddItem.IdDistributorOwner;
                                latchData.IdDepositoryElementData = dataProvider.SaveDepositoryElementData(latchData);
                                deviceDistributorLatch.Add(latchData);
                            }

                            #endregion
                            #region DistributorOwnerChange

                            if (taskItem.Removed)//usuniety, nadajemy mu ownera z parametru
                            {
                                if (ddItem.IdDistributorOwner != targetOwner.IdDistributor)
                                {
                                    DeviceComponent.ChangeDeviceDistributor(dataProvider, ddItem, loggedOperator, dItem.IdDistributor, targetOwner.IdDistributor, "Task end change owner mechanism [Uninstall]", (int)Enums.ServiceReferenceType.IdTask, task.IdTask);
                                }
                            }
                            else//zamontowany dajemy dystrybutora wynikającego z zadania
                            {
                                if (ddItem.IdDistributorOwner != targetOwner.IdDistributor)
                                {
                                    DeviceComponent.ChangeDeviceDistributor(dataProvider, ddItem, loggedOperator, dItem.IdDistributor, task.IdDistributor, "Task end change owner mechanism [Install]", (int)Enums.ServiceReferenceType.IdTask, task.IdTask);
                                }
                            }

                            #endregion
                        }
                    }

                }
            }
        }

        #endregion

        #region TaskDataLatch

        #region TaskDataLatch

        public static OpTaskData TaskDataLatch(DataProvider dataProvider, OpTask task, long idDataType, object value, int? indexNbr = null, int? firstIndexNbr = null)
        {
            OpTaskData newTaskData = null;
            if (task != null)
            {
                newTaskData = new OpTaskData();
                newTaskData.IdTask = task.IdTask;
                newTaskData.IdDataType = idDataType;
                if (!indexNbr.HasValue)
                {
                    if (!task.DataList.Exists(d => d.IdDataType == idDataType))
                        task.DataList.AddRange(dataProvider.GetTaskDataFilter(IdDataType: new long[] { idDataType }, IdTask: new int[] { task.IdTask }));
                    newTaskData.Index = task.DataList.GetNextIndex(idDataType);
                }
                else
                    newTaskData.Index = indexNbr.Value;
                if (firstIndexNbr.HasValue)
                {
                    if (newTaskData.Index < firstIndexNbr)
                        newTaskData.Index = firstIndexNbr.Value;
                }
                newTaskData.Value = value;
                newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
                newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
                task.DataList.Add(newTaskData);
            }
            return newTaskData;
        }

        #endregion
        #region TaskProceedDepositoryStepLatch

        public static void TaskProceedDepositoryStepLatch(DataProvider dataProvider, OpTask task, long? idArticle, long? serialNbr, string externalSn, bool installed, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            if (serialNbr.HasValue && (!idArticle.HasValue || idArticle == 0))
            {
                OpArticle article = DepositoryComponent.GetArticleForDevice(dataProvider, serialNbr.Value);
                idArticle = article.IdArticle;
            }
            if (idArticle.HasValue)
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_ID_ARTICLE, idArticle.Value, tdItem.Index);
            }
            if (serialNbr.HasValue)
            {
                if (installed)
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_AIUT_DEVICE_UNMOUNT_FROM_MAGAZINE, serialNbr, tdItem.Index);
                else
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_AIUT_DEVICE_MOUNT_ON_MAGAZINE, serialNbr, tdItem.Index);
            }
            else
            {
                if (installed)
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_UNMOUNT_FROM_MAGAZINE, externalSn, tdItem.Index);
                else
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_EXTERNAL_DEVICE_MOUNT_ON_MAGAZINE, externalSn, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedDeviceExchangeStepLatch

        public static void TaskProceedDeviceExchangeStepLatch(DataProvider dataProvider, OpTask task, string oldSerialNbr, string newSerialNbr, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_EXCHANGE, oldSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_EXCHANGE, newSerialNbr, tdItem.Index);
        }

        #endregion
        #region TaskProceedActionStepLatch

        public static void TaskProceedActionStepLatch(DataProvider dataProvider, OpTask task, long idAction, string actionType, int? idActionStatus, long? serialNbr, string actionText, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ID_ACTION, idAction, tdItem.Index);
            if (!String.IsNullOrEmpty(actionType))
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ACTION_TYPE, actionType, tdItem.Index);
            if (serialNbr.HasValue)
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ACTION_DEVICE, serialNbr, tdItem.Index);
            if (idActionStatus.HasValue)
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ACTION_STATUS, idActionStatus.Value, tdItem.Index);
            if (!String.IsNullOrEmpty(actionText))
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ACTION_TEXT, actionText, tdItem.Index);
        }

        #endregion
        #region TaskProceedAddMeterStepLatch

        public static void TaskProceedAddMeterStepLatch(DataProvider dataProvider, OpTask task, string meterSerialNbr, DateTime? timeStamp)
        {
            if (!String.IsNullOrEmpty(meterSerialNbr))
            {
                OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ADDED_METER_SERIAL_NBR, meterSerialNbr, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedArrangedFitterVisitDateStepLatch

        public static void TaskProceedArrangedFitterVisitDateStepLatch(DataProvider dataProvider, OpTask task, OpOperator loggedOperator)
        {
            OpTaskData tdArrangedDate = task.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_DATE && d.Index == 0 && d.Value != null && d.Value is DateTime);
            OpTaskData tdBeginHour = task.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR && d.Index == 0 && d.Value != null);
            OpTaskData tdEndHour = task.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR && d.Index == 0 && d.Value != null);
            OpTaskData tdScheduleMode = task.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE && d.Index == 0 && d.Value != null && d.Value is int);
            OpTaskData tdScheduleResult = task.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT && d.Index == 0 && d.Value != null && d.Value is int);
            OpTaskData tdScheduleNotes = task.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES && d.Index == 0 && d.Value != null);

            DateTime? arrangedDate = null;
            int beginHour = 6;
            int endHour = 22;
            int scheduleMode = (int)Enums.TaskAppointmentScheduleMode.Other;
            int scheduleResult = (int)Enums.TaskAppointmentResult.Success;
            string notes = null;
            if (tdArrangedDate != null && tdArrangedDate.Value != null)
            {
                DateTime arrangedDateTmp = DateTime.MinValue;
                if (DateTime.TryParse(tdArrangedDate.Value.ToString(), out arrangedDateTmp))
                    arrangedDate = arrangedDateTmp;
            } 
            
            if (tdBeginHour != null)
                int.TryParse(tdBeginHour.Value.ToString(), out beginHour);
            if (tdEndHour != null)
                int.TryParse(tdEndHour.Value.ToString(), out endHour);
            if (tdScheduleMode != null)
                int.TryParse(tdScheduleMode.Value.ToString(), out scheduleMode);
            if (tdScheduleResult != null)
                int.TryParse(tdScheduleResult.Value.ToString(), out scheduleResult);
            if (tdScheduleNotes != null)
                notes = tdScheduleNotes.Value.ToString();
            TaskProceedArrangedFitterVisitDateStepLatch(dataProvider, task, loggedOperator, arrangedDate, beginHour, endHour, (Enums.TaskAppointmentScheduleMode)scheduleMode, (Enums.TaskAppointmentResult)scheduleResult, notes, null);
        }

        #endregion
        #region TaskProceedArrangedFitterVisitDateStepLatch

        public static void TaskProceedArrangedFitterVisitDateStepLatch(DataProvider dataProvider, OpTask task, OpOperator loggedOperator, DateTime date, int hourFrom, int hourTo, Enums.TaskAppointmentScheduleMode scheduleMode, string note, DateTime? timeStamp)
        {
            TaskProceedArrangedFitterVisitDateStepLatch(dataProvider, task, loggedOperator, date, hourFrom, hourTo, scheduleMode, Enums.TaskAppointmentResult.Success, note, timeStamp);
        }

        public static void TaskProceedArrangedFitterVisitDateStepLatch(DataProvider dataProvider, OpTask task, OpOperator loggedOperator, DateTime? date, int? hourFrom, int? hourTo, Enums.TaskAppointmentScheduleMode scheduleMode, Enums.TaskAppointmentResult appointmentResult, string note, DateTime? timeStamp)
        {
            BaseComponent.Log(EventID.Forms.TaskArrangedFitterVisitDateParams, true, new object[]{ (loggedOperator != null ? loggedOperator.ToString() : "<null>"), 
                                                                                                   (task != null ? task.IdTask.ToString() : "<null>"),
                                                                                                   (date.HasValue ? date.Value.ToString() : "<null>"),
                                                                                                   (hourFrom.HasValue ? hourFrom.ToString() : "<null>"),
                                                                                                   (hourTo.HasValue ? hourTo.ToString() : "<null>"),
                                                                                                   scheduleMode.ToString(), appointmentResult.ToString(), note,
                                                                                                   (timeStamp.HasValue ? timeStamp.Value.ToString() : "<null>") });
            List<OpTaskData> fitterArrangedVisitDate = dataProvider.GetTaskDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                                      IdTask: new int[] { task.IdTask },
                                                                                      IndexNbr: new int[] { 0 },
                                                                                      IdDataType: new long[] { DataType.TASK_ARRANGED_FITTER_VISIT_DATE,
                                                                                                               DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR,
                                                                                                               DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR,
                                                                                                               DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE,
                                                                                                               DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT,
                                                                                                               DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES,
                                                                                                               DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR });

            if (fitterArrangedVisitDate == null)
                fitterArrangedVisitDate = new List<OpTaskData>();

            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow), firstIndexNbr: 1);
            TaskDataLatch(dataProvider, task, DataType.TASK_ARRANGED_FITTER_VISIT_DATE, date, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR, hourFrom, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR, hourTo, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE, (int)scheduleMode, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT, (int)appointmentResult, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES, note, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR, loggedOperator.IdOperator, tdItem.Index);

            OpTaskData tdTmp = null;
            #region TASK_ARRANGED_FITTER_VISIT_DATE

            tdTmp = fitterArrangedVisitDate.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_DATE);
            if (tdTmp == null)
            {
                tdTmp = task.DataList.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_DATE && t.Index == 0);
            }
            if (tdTmp == null)
            {
                tdTmp = new OpTaskData();
                tdTmp.IdDataType = DataType.TASK_ARRANGED_FITTER_VISIT_DATE;
                tdTmp.Index = 0;
                tdTmp.IdTask = task.IdTask;
            }
            DateTime tmpDate = DateTime.MinValue;
            if (tdTmp.Value != null && !String.IsNullOrEmpty(tdTmp.Value.ToString()))
                DateTime.TryParse(tdTmp.Value.ToString(), out tmpDate);
            if (date != tmpDate || tdTmp.IdTaskData == 0)
            {
                tdTmp.Value = date;
                tdTmp.IdTaskData = dataProvider.SaveTaskData(tdTmp);
            }

            tdTmp = null;
            #endregion
            #region TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR

            tdTmp = fitterArrangedVisitDate.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR);
            if (tdTmp == null)
                tdTmp = task.DataList.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR && t.Index == 0);
            if (tdTmp == null)
            {
                tdTmp = new OpTaskData();
                tdTmp.IdDataType = DataType.TASK_ARRANGED_FITTER_VISIT_BEGIN_HOUR;
                tdTmp.Index = 0;
                tdTmp.IdTask = task.IdTask;
            }
            int tmpBeginHour = -1;
            if (tdTmp.Value != null && !String.IsNullOrEmpty(tdTmp.Value.ToString()))
                int.TryParse(tdTmp.Value.ToString(), out tmpBeginHour);
            if (hourFrom != tmpBeginHour || tdTmp.IdTaskData == 0)
            {
                tdTmp.Value = hourFrom;
                tdTmp.IdTaskData = dataProvider.SaveTaskData(tdTmp);
            }

            tdTmp = null;
            #endregion
            #region TASK_ARRANGED_FITTER_VISIT_END_HOUR

            tdTmp = fitterArrangedVisitDate.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR);
            if (tdTmp == null)
                tdTmp = task.DataList.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR && t.Index == 0);
            if (tdTmp == null)
            {
                tdTmp = new OpTaskData();
                tdTmp.IdDataType = DataType.TASK_ARRANGED_FITTER_VISIT_END_HOUR;
                tdTmp.Index = 0;
                tdTmp.IdTask = task.IdTask;
            }
            int tmpEndHour = -1;
            if (tdTmp.Value != null && !String.IsNullOrEmpty(tdTmp.Value.ToString()))
                int.TryParse(tdTmp.Value.ToString(), out tmpEndHour);
            if (hourTo != tmpEndHour || tdTmp.IdTaskData == 0)
            {
                tdTmp.Value = hourTo;
                tdTmp.IdTaskData = dataProvider.SaveTaskData(tdTmp);
            }

            tdTmp = null;
            #endregion
            #region TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE

            tdTmp = fitterArrangedVisitDate.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE);
            if (tdTmp == null)
                tdTmp = task.DataList.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE && t.Index == 0);
            if (tdTmp == null)
            {
                tdTmp = new OpTaskData();
                tdTmp.IdDataType = DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_MODE;
                tdTmp.Index = 0;
                tdTmp.IdTask = task.IdTask;
            }
            int tmpScheduleMode = -1;
            if (tdTmp.Value != null && !String.IsNullOrEmpty(tdTmp.Value.ToString()))
                int.TryParse(tdTmp.Value.ToString(), out tmpScheduleMode);
            if ((int)scheduleMode != tmpScheduleMode || tdTmp.IdTaskData == 0)
            {
                tdTmp.Value = (int)scheduleMode;
                tdTmp.IdTaskData = dataProvider.SaveTaskData(tdTmp);
            }

            tdTmp = null;
            #endregion
            #region TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT

            tdTmp = fitterArrangedVisitDate.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT);
            if (tdTmp == null)
                tdTmp = task.DataList.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT && t.Index == 0);
            if (tdTmp == null)
            {
                tdTmp = new OpTaskData();
                tdTmp.IdDataType = DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT;
                tdTmp.Index = 0;
                tdTmp.IdTask = task.IdTask;
            }
            int tmpScheduleResult = -1;
            if (tdTmp.Value != null && !String.IsNullOrEmpty(tdTmp.Value.ToString()))
                int.TryParse(tdTmp.Value.ToString(), out tmpScheduleResult);
            if ((int)appointmentResult != tmpScheduleResult || tdTmp.IdTaskData == 0)
            {
                tdTmp.Value = (int)appointmentResult;
                tdTmp.IdTaskData = dataProvider.SaveTaskData(tdTmp);
            }

            tdTmp = null;
            #endregion
            #region TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES

            tdTmp = fitterArrangedVisitDate.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES);
            if (tdTmp == null)
                tdTmp = task.DataList.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES && t.Index == 0);
            if (tdTmp == null)
            {
                tdTmp = new OpTaskData();
                tdTmp.IdDataType = DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_NOTES;
                tdTmp.Index = 0;
                tdTmp.IdTask = task.IdTask;
            }
            if (!String.Equals(tdTmp.Value, note) || tdTmp.IdTaskData == 0)
            {
                tdTmp.Value = note;
                tdTmp.IdTaskData = dataProvider.SaveTaskData(tdTmp);
            }

            tdTmp = null;
            #endregion
            #region TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR

            tdTmp = fitterArrangedVisitDate.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR);
            if (tdTmp == null)
                tdTmp = task.DataList.Find(t => t.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR && t.Index == 0);
            if (tdTmp == null)
            {
                tdTmp = new OpTaskData();
                tdTmp.IdDataType = DataType.TASK_ARRANGED_FITTER_VISIT_ID_OPERATOR;
                tdTmp.Index = 0;
                tdTmp.IdTask = task.IdTask;
            }
            int tmpIdOperator = -1;
            if (tdTmp.Value != null && !String.IsNullOrEmpty(tdTmp.Value.ToString()))
                int.TryParse(tdTmp.Value.ToString(), out tmpIdOperator);
            if (loggedOperator.IdOperator != tmpIdOperator || tdTmp.IdTaskData == 0)
            {
                tdTmp.Value = loggedOperator.IdOperator;
                tdTmp.IdTaskData = dataProvider.SaveTaskData(tdTmp);
            }

            tdTmp = null;
            #endregion

            if (Enums.TaskAppointmentResult.Failure == appointmentResult)
            {
                //Bieżąca próba umówienia się na wizytę się nie udała, sprawdzamy wcześniejsze próby
                //Narazie na sztywno przyjęta ilość 6 po rząd, jeśli będzie konieczne można parametryzować np w Dystrybutorze
                int findIndex = tdItem.Index - 1;
                int failuresCount = 1;
                while (findIndex > 0)
                {
                    OpTaskData tdHistoricalAppointmentResult = task.DataList.Find(d => d.IdDataType == DataType.TASK_ARRANGED_FITTER_VISIT_SCHEDULE_RESULT && d.Index == findIndex);
                    if (tdHistoricalAppointmentResult != null && tdHistoricalAppointmentResult.Value != null)//jeśli jest null to przyjmujemy że było ok
                    {
                        int historicalAppointmentResult = -1;
                        if (int.TryParse(tdHistoricalAppointmentResult.Value.ToString(), out historicalAppointmentResult) && historicalAppointmentResult == (int)Enums.TaskAppointmentResult.Failure)//w razie problemów rpzyjmujemy że jest ok
                        {
                            failuresCount++;
                        }
                        else
                            break;//skoro była pozytywna próba nawiązania kontaktu nie mamy dalej nic do sprawdzania
                    }
                    findIndex--;
                }
                BaseComponent.Log(EventID.Forms.TaskArrangedFitterVisitDateFailureCount, true, new object[] { failuresCount });
                if (failuresCount >= 6)
                {
                    SuspendTask(dataProvider, loggedOperator, task.IdTask, dataProvider.DateTimeNow.AddDays(14), String.Format("{0}. {1}: {2}", ResourcesText.AutoSuspensionDueToLackOfContactWithCustomer, ResourcesText.Attempts, failuresCount), Enums.TaskSuspensionReason.ClientFault);
                }
            }
        }

        #endregion
        #region TaskProceedCurrentTankMeterReadingStepLatch

        public static void TaskProceedCurrentTankMeterReadingStepLatch(DataProvider dataProvider, OpTask task, string meterSerialNbr, double currentTankMeterReading, long? alevelSerialNbr, DateTime? timeStamp)
        {
            if (!String.IsNullOrEmpty(meterSerialNbr))
            {
                OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_METER_SERIAL_NBR, meterSerialNbr, tdItem.Index);
                TaskDataLatch(dataProvider, task, DataType.TASK_CURRENT_METER_READOUT, currentTankMeterReading, tdItem.Index);
                if (alevelSerialNbr.HasValue)
                {
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ALEVEL_SERIAL_NBR, alevelSerialNbr.Value, tdItem.Index);
                }
            }
        }

        #endregion
        #region TaskProceedTankMeterIsUndergroundStepLatch

        public static void TaskProceedTankMeterIsUndergroundStepLatch(DataProvider dataProvider, OpTask task, string meterSerialNbr, bool isUnderground, long? alevelSerialNbr, DateTime? timeStamp)
        {
            if (!String.IsNullOrEmpty(meterSerialNbr))
            {
                OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_METER_SERIAL_NBR, meterSerialNbr, tdItem.Index);
                TaskDataLatch(dataProvider, task, DataType.METER_TANK_IS_UNDERGROUND, isUnderground, tdItem.Index);
                if (alevelSerialNbr.HasValue)
                {
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ALEVEL_SERIAL_NBR, alevelSerialNbr.Value, tdItem.Index);
                }
            }
        }

        #endregion
        #region TaskProceedAttachmentPhotoDoneStepLatch

        public static void TaskProceedAttachmentPhotoDoneStepLatch(DataProvider dataProvider, OpTask task, string photoName, DateTime? timeStamp)
        {
            if (!String.IsNullOrEmpty(photoName))
            {
                OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_NAME, photoName, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedAttachmentPhotoDoneProblemStepLatch

        public static void TaskProceedAttachmentPhotoDoneProblemStepLatch(DataProvider dataProvider, OpTask task, string photoName, string note, DateTime? timeStamp)
        {
            if (!String.IsNullOrEmpty(photoName))
            {
                OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NAME, photoName, tdItem.Index);
                if (!String.IsNullOrEmpty(note))
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_DONE_PROBLEM_NOTE, note, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedAttachmentPhotoUploadedStepLatch

        public static void TaskProceedAttachmentPhotoUploadedStepLatch(DataProvider dataProvider, OpTask task, string photoName, DateTime? timeStamp)
        {
            if (!String.IsNullOrEmpty(photoName))
            {
                OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_NAME, photoName, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedAttachmentPhotoUploadedProblemStepLatch

        public static void TaskProceedAttachmentPhotoUploadedProblemStepLatch(DataProvider dataProvider, OpTask task, string photoName, string note, DateTime? timeStamp)
        {
            if (!String.IsNullOrEmpty(photoName))
            {
                OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NAME, photoName, tdItem.Index);
                if (!String.IsNullOrEmpty(note))
                    TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ATTACHMENT_PHOTO_UPLOADED_PROBLEM_NOTE, note, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedProblemStepLatch

        public static void TaskProceedProblemStepLatch(DataProvider dataProvider, OpTask task, Enums.TaskProceedProblem problemType, string note, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROCEED_PROBLEM_TYPE, (int)problemType, tdItem.Index);
            if (!String.IsNullOrEmpty(note))
                TaskDataLatch(dataProvider, task, DataType.TASK_PROCEED_PROBLEM_NOTE, note, tdItem.Index);
        }

        #endregion
        #region TaskProceedHeatInstallationOkoStepLatch

        public static void TaskProceedHeatInstallationOkoStepLatch(DataProvider dataProvider, OpTask task, long okoSerialNbr, string okoSealNumber, int? signalLevel, long? signalIdDataType, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TAKS_OKO_SERIAL_NBR, okoSerialNbr, tdItem.Index);
            if (!String.IsNullOrEmpty(okoSealNumber))
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_OKO_SEAL_NUMBER, okoSealNumber, tdItem.Index);
            }
            if (signalLevel.HasValue && signalIdDataType.HasValue)
            {
                TaskDataLatch(dataProvider, task, signalIdDataType.Value, signalLevel.Value, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedHeatInstallationWatermeterStepLatch

        public static void TaskProceedHeatInstallationWatermeterStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, string watermeterSerialNbr, double? watermeterNominalFlow, double? watermeterPulseWeight, string replacedWatermeterSerialNbr, string indicationSummary, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_WATERMETER_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            if (!String.IsNullOrEmpty(watermeterSerialNbr))
            {
                TaskDataLatch(dataProvider, task, DataType.TAKS_WATERMETER_SERIAL_NBR, watermeterSerialNbr, tdItem.Index);
            }
            if (!String.IsNullOrEmpty(replacedWatermeterSerialNbr))
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_REPLACED_WATER_METER_SERIAL_NUMBER, replacedWatermeterSerialNbr, tdItem.Index);
            }
            if (!String.IsNullOrEmpty(indicationSummary))
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_INSTALLATION_PROTOCOL_WATERMETER_INDICATIONS_SUMMARY, indicationSummary, tdItem.Index);
            }
            if (watermeterNominalFlow.HasValue)
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_INSTALLATION_PROTOCOL_WATERMETER_NOMINAL_FLOW, watermeterNominalFlow.Value, tdItem.Index);
            }
            if (watermeterPulseWeight.HasValue)
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_INSTALLATION_PROTOCOL_WATERMETER_PULSE_WEIGHT, watermeterPulseWeight.Value, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedHeatInstallationConverterStepLatch

        public static void TaskProceedHeatInstallationConverterStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, string converterSerialNbr, string replacedConverterSerialNbr, string indicationSummary, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_CONVERTER_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            if (!String.IsNullOrEmpty(converterSerialNbr))
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_CONVERTER_SERIAL_NBR, converterSerialNbr, tdItem.Index);
            }
            if (!String.IsNullOrEmpty(replacedConverterSerialNbr))
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_CONVERTER_BEFORE_INSTALLATION_SERIAL_NUMBER, replacedConverterSerialNbr, tdItem.Index);
            }
            if (!String.IsNullOrEmpty(indicationSummary))
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_INSTALLATION_PROTOCOL_INDICATIONS_SUMMARY, indicationSummary, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedMeterSerialNbrChangeStepLatch

        public static void TaskProceedMeterSerialNbrChangeStepLatch(DataProvider dataProvider, OpTask task, string newMeterSerialNbr, string oldMeterSerialNbr, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_NEW_METER_SERIAL_NBR, newMeterSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_OLD_METER_SERIAL_NBR, oldMeterSerialNbr, tdItem.Index);
        }

        #endregion
        #region TaskProceedChangeBatteryActionStepLatch

        public static void TaskProceedChangeBatteryActionStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, long newBatterySerialNbr, long? oldBatterySerialNbr, bool oldBatteryWasEfficient, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_BATTERY_CHANGE_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_BATTERY_CHANGE_OLD_SERIAL_NBR, oldBatterySerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_BATTERY_CHANGE_NEW_SERIAL_NBR, newBatterySerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_BATTERY_CHANGE_OLD_BATTERY_EFFICIENT, oldBatteryWasEfficient, tdItem.Index);
        }

        #endregion
        #region TaskProceedValveOperationStepLatch

        public static void TaskProceedValveOperationStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, Enums.ValveState valveOperationType, string statusName, int operationResult, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_VALVE_OPERATION_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_VALVE_OPERATION_TYPE, (int)valveOperationType, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_VALVE_OPERATION_RESULT, operationResult, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS_NAME, statusName, tdItem.Index);
        }

        #endregion
        #region TaskProceedValveStatusStepLatch

        public static void TaskProceedValveStatusStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, Enums.ValveState valveStatus, string statusName, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_VALVE_OPERATION_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS, (int)valveStatus, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_VALVE_OPERATION_STATUS_NAME, statusName, tdItem.Index);
        }

        #endregion
        #region TaskProceedDeviceReprogramStepLatch

        public static void TaskProceedDeviceReprogramStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, string fileName, string oldVersionNumber, string newVersionNumber, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_REPROGRAM_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_REPROGRAM_FILE_NAME, fileName, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_REPROGRAM_OLD_VERSION_NUMBER, oldVersionNumber, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_REPROGRAM_NEW_VERSION_NUMBER, newVersionNumber, tdItem.Index);
        }

        #endregion
        #region TaskProceedSimCardTestStepLatch

        public static void TaskProceedSimCardTestStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, string phoneNbr, Enums.SIMCardError simCardError, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_SIM_CARD_TEST_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_SIM_CARD_TEST_PHONE_NBR, phoneNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_SIM_CARD_TEST_ERROR_TYPE, (int)simCardError, tdItem.Index);
        }

        #endregion
        #region TaskProceedDeviceCriticalErrorStepLatch

        public static void TaskProceedDeviceCriticalErrorStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, long errorType, object errorValue, DateTime? timeStamp)
        {
            errorValue = dataProvider.dbConnectionCore.ParamObject(errorValue, errorType, dataProvider.GetDataType(errorType).IdDataTypeClass);
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CRITICAL_ERROR_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CRITICAL_ERROR_TYPE, errorType, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CRITICAL_ERROR_VALUE, errorValue, tdItem.Index);
        }

        #endregion
        #region TaskProceedArchiveReadStepLatch

        public static void TaskProceedArchiveReadStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, DateTime readFrom, DateTime readTo, IMR.Suite.Common.Enums.AggregationType archiveType, int frameCount, DateTime? timeStamp)
        {            
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ARCHIVE_READ_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ARCHIVE_READ_FROM, readFrom, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ARCHIVE_READ_TO, readTo, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ARCHIVE_READ_TYPE, archiveType, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_ARCHIVE_READ_FRAME_COUNT, frameCount, tdItem.Index);
        }

        #endregion
        #region TaskProceedEventLogStepLatch

        public static void TaskProceedEventLogStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, DateTime referenceDate, int entryCount, DateTime? timeStamp)
        {            
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_EVENT_LOG_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_EVENT_LOG_REFERENCE_DATE, referenceDate, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_EVENT_LOG_ENTRY_COUNT, entryCount, tdItem.Index);
        }

        #endregion
        #region TaskProceedNewMeterCounterResynchronizationStepLatch

        public static void TaskProceedNewMeterCounterResynchronizationStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, int impulseWeight, int counterValue,
            int counterFormat, DateTime? timeStamp)
        {            
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_NEW_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_COEFFICIENT, impulseWeight, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.GAS_METER_USAGE_VALUE, counterValue, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_LITER_MAX_VALUE, counterFormat, tdItem.Index);
        }

        #endregion
        #region TaskProceedOldMeterCounterResynchronizationStepLatch

        public static void TaskProceedOldMeterCounterResynchronizationStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, int impulseWeight, int counterValue,
            int counterFormat, DateTime? timeStamp)
        {            
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_OLD_METER_COUNTER_RESYNCHRONIZATION_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_COEFFICIENT, impulseWeight, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.GAS_METER_USAGE_VALUE, counterValue, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_LITER_MAX_VALUE, counterFormat, tdItem.Index);
        }

        #endregion
        #region TaskProceedConfigurationRestoreStepLatch

        public static void TaskProceedConfigurationRestoreStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, byte[] restoredConfiguration, DateTime? timeStamp)
        {            
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CONFIGURATION_RESTORE_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);

            if (restoredConfiguration != null)
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_RESTORED_CONFIGURATION, restoredConfiguration, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedInstalledGasmeterStepLatch

        public static void TaskProceedInstalledGasmeterStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, MeterType meterType, string meterSeries, int? maxHourFlow,
            string meterSerialNbr, int? impulseWeight, int? counterValue, int? counterFormat, string locationCID, IMR.Suite.Common.Enums.SitaOperationType operationType, int? minimumTemporaryFlow,
            int? maximumTemporaryFlow, int? deviceCounterScaler, DateTime? timeStamp, string taskProtocolCounterValue, string taskProtocolCounterFormat, string taskProtocolImpulseWeight)
        {            
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_INSTALLED_GASMETER_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            if(meterType != null)
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_METER_TYPE, meterType.Id, tdItem.Index);
            if(!String.IsNullOrEmpty(meterSeries))
                TaskDataLatch(dataProvider, task, DataType.METER_SERIES, meterSeries, tdItem.Index);
            if(!String.IsNullOrEmpty(meterSerialNbr))
                TaskDataLatch(dataProvider, task, DataType.METER_SERIAL_NUMBER, meterSerialNbr, tdItem.Index);
            if(impulseWeight.HasValue)
                TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_COEFFICIENT, impulseWeight, tdItem.Index);
            if(counterValue.HasValue)
                TaskDataLatch(dataProvider, task, DataType.GAS_METER_USAGE_VALUE, counterValue, tdItem.Index);
            if(counterFormat.HasValue)
                TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_LITER_MAX_VALUE, counterFormat, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.LOCATION_CID, locationCID, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_OPERATION_TYPE, (int)operationType, tdItem.Index);
            if(minimumTemporaryFlow.HasValue)
                TaskDataLatch(dataProvider, task, DataType.MINIMUM_TEMPORARY_FLOW, minimumTemporaryFlow, tdItem.Index);
            if(maximumTemporaryFlow.HasValue)
                TaskDataLatch(dataProvider, task, DataType.MAXIMUM_TEMPORARY_FLOW, maximumTemporaryFlow, tdItem.Index);
            if (deviceCounterScaler.HasValue)
                TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_SCALER, deviceCounterScaler, tdItem.Index);

            if (!String.IsNullOrEmpty(taskProtocolCounterValue))
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_GAS_METER_USAGE_VALUE, taskProtocolCounterValue, tdItem.Index);
            if (!String.IsNullOrEmpty(taskProtocolCounterFormat))
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_COUNTER_LITER_MAX_VALUE, taskProtocolCounterFormat, tdItem.Index);
            if (!String.IsNullOrEmpty(taskProtocolImpulseWeight))
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_COUNTER_COEFFICIENT, taskProtocolImpulseWeight, tdItem.Index);

            if (maxHourFlow.HasValue)
            {
                TaskDataLatch(dataProvider, task, DataType.MAX_HOUR_FLOW, maxHourFlow, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedDeinstalledGasmeterStepLatch

        public static void TaskProceedDeinstalledGasmeterStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, string magazineCID,
            IMR.Suite.Common.Enums.SitaOperationType operationType, DateTime? timeStamp)
        {            
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEINSTALLED_GASMETER_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEINSTALLED_MAGAZINE_CID, magazineCID, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_OPERATION_TYPE, (int)operationType, tdItem.Index);
        }

        #endregion
        #region TaskProceedConfigurationDownloadStepLatch

        public static void TaskProceedConfigurationDownloadStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, byte[] restoredConfiguration, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CONFIGURATION_DOWNLOAD_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);

            if (restoredConfiguration != null)
            {
                TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_RESTORED_CONFIGURATION, restoredConfiguration, tdItem.Index);
            }
        }

        #endregion
        #region TaskProceedOperatorPerformerChangedStepLatch

        public static void TaskProceedOperatorPerformerChangedStepLatch(DataProvider dataProvider, OpTask task, OpOperator oldOperator, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.OPERATOR_IMR_SERVER_OPERATOR_LOGIN, oldOperator.Login, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.OPERATOR_ID_IMR_SUITE_CORE, oldOperator.IdOperator, tdItem.Index);
        }

        #endregion
        #region TaskProceedClusterTestStepLatch

        public static void TaskProceedClusterTestStepLatch(DataProvider dataProvider, OpTask task, long okoSerialNbr, string apulseList, string latitudeList, string longitudeList, string fileName, string apulseStatusList, string apulseEfficiencyList, string apulseFrameCountList, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_OKO, okoSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE, apulseList, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_LATITUDE, latitudeList, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_LONGITUDE, longitudeList, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_FILE_NAME, fileName, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_STATUS, apulseStatusList, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_EFFICIENCY, apulseEfficiencyList, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_CLUSTER_TEST_APULSE_FRAME_COUNT, apulseFrameCountList, tdItem.Index);
        }

        #endregion
        #region TaskProceedResynchronisationStepLatch

        public static void TaskProceedInstallationWithSynchronizationStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, string locationCid, double gasMeterUsageValue, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_INSTALLATION_WITH_SYNCHRONIZATION_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.LOCATION_CID, locationCid, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.GAS_METER_USAGE_VALUE, gasMeterUsageValue, tdItem.Index);
        }

        #endregion
        #region TaskProceedPinValueStepLatch

        public static void TaskProceedPinValueStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, int pinNumber, Enums.SitaOKOPINMenuOption pinType, string pinValue, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_PIN_VALUE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_PIN_NUMBER, pinNumber, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_PIN_TYPE, (int)pinType, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_PIN_VALUE, pinValue, tdItem.Index);
        }

        #endregion
        #region TaskProceedDeviceDiagnosticStepLatch

        public static void TaskProceedDeviceDiagnosticStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DIAGNOSTIC_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
        }

        #endregion
        #region TaskProceedInstalledGasmeterRadioQualityVerificationStepLatch

        public static void TaskProceedInstalledGasmeterRadioQualityVerificationStepLatch(DataProvider dataProvider, OpTask task, 
            long deviceSerialNbr, double? latitude, double? longitude, int radioQuality, 
            DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_INSTALLED_GASMETER_RADIO_QUALITY_VERIFICATION_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            if (latitude.HasValue)
                TaskDataLatch(dataProvider, task, DataType.DEVICE_LATITUDE, latitude, tdItem.Index);
            if (longitude.HasValue)
                TaskDataLatch(dataProvider, task, DataType.DEVICE_LONGITUDE, longitude, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.RADIO_QUALITY, radioQuality, tdItem.Index);
        }

        #endregion
        #region TaskProceedInstalledMeterAdapterStepLatch

        public static void TaskProceedInstalledMeterAdapterStepLatch(DataProvider dataProvider, OpTask task, long meterAdapterSerialNbr, string meterSerialNbr, long deviceSerialNbr, string locationCid, DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_INSTALLED_METER_ADAPTER_SERIAL_NBR, meterAdapterSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.METER_SERIAL_NUMBER, meterSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.LOCATION_CID, locationCid, tdItem.Index);
        }
        
        #endregion
        #region TaskProceedDeviceValidImpulseCountTestStepLatch

        public static void TaskProceedDeviceValidImpulseCountTestStepLatch(DataProvider dataProvider, OpTask task, long deviceSerialNbr, string meterSerialNbr, string meterName, double impulseCountPriorTest, double impulseCountAfterTest, long testDuration,
            DateTime? timeStamp)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_VALID_IMPULSE_COUNT_TEST_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.METER_SERIAL_NUMBER, meterSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.METER_NAME, meterName, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_PRIOR_TEST, impulseCountPriorTest, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_IMPULSE_COUNT_AFTER_TEST, impulseCountAfterTest, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_DEVICE_TEST_DURATION, testDuration, tdItem.Index);
        }

        #endregion
        #region TaskProceedInstalledWatermeterStepLatch

        public static void TaskProceedInstalledWatermeterStepLatch(DataProvider dataProvider, OpTask task, DateTime? timeStamp, List<DB_TASK_DATA> taskData, long deviceSerialNbr, double impulseWeight,
                double waterCounter, double reverseWaterCounter, double maxHourFlow, string CID, string meterSerialNbr, double maxTemperature, double minTemperature, int inductiveSensorWaterMeterType, string measuringDirective)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_INSTALLED_WATERMETER_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_COUNTER_COEFFICIENT, impulseWeight, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.WATER_COUNTER, waterCounter, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.REVERSE_WATER_COUNTER, reverseWaterCounter, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.MAX_HOUR_FLOW, maxHourFlow, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_CID, CID, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.METER_SERIAL_NUMBER, meterSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_MAX_TEMPERATURE, maxTemperature, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_MIN_TEMPERATURE, minTemperature, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.DEVICE_INDUCTIVE_SENSOR_WATER_METER_TYPE, inductiveSensorWaterMeterType, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_WATERMETER_MEASURING_DIRECTIVE, measuringDirective, tdItem.Index);

            foreach (DB_TASK_DATA td in taskData)
            {
                TaskDataLatch(dataProvider, task, td.ID_DATA_TYPE, td.VALUE, tdItem.INDEX_NBR);
            }
        }

        #endregion
        #region TaskProceedHubInstallationStepLatch

        public static void TaskProceedHubInstallationStepLatch(DataProvider dataProvider, OpTask task, DateTime? timeStamp, long deviceSerialNbr, string locationCID, 
            int locationDeviceInstallationPlace, string locationDeviceInstallationPlaceNote, int installationFormType)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_HUB_INSTALLATION_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.LOCATION_CID, locationCID, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.LOCATION_DEVICE_INSTALLATION_PLACE, locationDeviceInstallationPlace, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.LOCATION_DEVICE_INSTALLATION_PLACE_NOTE, locationDeviceInstallationPlaceNote, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_INSTALLATION_FORM_TYPE, installationFormType, tdItem.Index);
        }

        #endregion
        #region TaskProceedWalkedByReadoutStepLatch

        public static void TaskProceedWalkedByReadoutStepLatch(DataProvider dataProvider, OpTask task, DateTime? timeStamp, long deviceSerialNbr,
            byte[] frame, Enums.SITAFrameType frameType, double latitude, double longitude, bool sitaContainsEncryptionKey, object walkedByMeasurementValue)
        {
            OpTaskData tdItem = TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_TIMESTAMP, (timeStamp.HasValue ? timeStamp.Value : dataProvider.DateTimeUtcNow));

            TaskDataLatch(dataProvider, task, DataType.TASK_PROTOCOL_WALKED_BY_READOUT_DEVICE_SERIAL_NBR, deviceSerialNbr, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.SITA_WALKEDBY_FRAME, frame, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.SITA_WALKEDBY_FRAME_TYPE, (int)frameType, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.WALKEDBY_LATITUDE, latitude, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.WALKEDBY_LONGITUDE, longitude, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.SITA_CONTAINS_ENCRYPTION_KEYS, sitaContainsEncryptionKey, tdItem.Index);
            TaskDataLatch(dataProvider, task, DataType.TASK_INSTALLATION_PROTOCOL_WALKEDBY_MEASUREMENT_VALUE, walkedByMeasurementValue, tdItem.Index);
        }

        #endregion

        #endregion

        #region TaskStatusChangeEmail
        public static void TaskStatusChangeEmail(DataProvider dataProvider, OpTask TaskObject, OpTask OriginalTask, OpOperator LoggedOperator, int SelectedLanguage, string Note)
        {
            if (OriginalTask == null || TaskObject.IdTaskStatus != OriginalTask.IdTaskStatus)
            {
                EmailComponent.HashedPassword = "F2xTCpfOaKFstdRLHwMsC5b02e2XSVsgDnkwhUMYI+lSSy+AzFmL7HvbBhwcsz8loN6G80sMXrqnd3jxgAXO2OQkPa3xppQUKJXbbjtiF4Gt3REIo8YcnCLzkgejGPEIyTYieWjPHIYz1WCAxKUAtbjeAqg3heKsB/GZ/bPTln8=";
                StringBuilder strBody = new StringBuilder();
                string personStr = string.Empty;

                List<OpOperator> mailingList = new List<OpOperator>();

                if (TaskObject.OperatorRegistering != null &&
                    TaskObject.OperatorRegistering.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)TaskObject.IdTaskStatus)) &&
                    !TaskObject.OperatorRegistering.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Change))
                {
                    mailingList.Add(TaskObject.OperatorRegistering);
                    personStr = ResourcesText.YouHaveCreated;
                }

                if (TaskObject.OperatorPerformer != null && (TaskObject.OperatorRegistering != null && TaskObject.IdOperatorRegistering != TaskObject.IdOperatorPerformer) &&
                    TaskObject.OperatorPerformer.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)TaskObject.IdTaskStatus)) &&
                    !TaskObject.OperatorPerformer.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Change))
                {
                    mailingList.Add(TaskObject.OperatorPerformer);
                    personStr = ResourcesText.AssignedToYou;
                }

                strBody.AppendFormat(ResourcesText.TaskStatusChangeExtended,
                    personStr,
                    TaskObject.IdTask,
                    (OriginalTask != null ? OriginalTask.TaskStatus.ToString() : ""),
                    TaskObject.TaskStatus,
                    (TaskObject.Location != null && !String.IsNullOrEmpty(TaskObject.Location.Name) ? (TaskObject.Location.Name + " ") : "") + (TaskObject.Location != null && !String.IsNullOrEmpty(TaskObject.Location.CID) ? TaskObject.Location.CID : ""),
                    (TaskObject.Location != null && !String.IsNullOrEmpty(TaskObject.Location.FullAddress) ? TaskObject.Location.FullAddress : ""),
                    (TaskObject.Location != null && TaskObject.Location.Actor != null && !String.IsNullOrEmpty(TaskObject.Location.Actor.Phone) ? TaskObject.Location.Actor.Phone : "") +
                            (TaskObject.Location != null && TaskObject.Location.Actor != null && !String.IsNullOrEmpty(TaskObject.Location.Actor.Mobile) ? ", " + TaskObject.Location.Actor.Mobile : ""),
                    Note);

                mailingList.AddRange(GetTaskDistributorNotificationOperators(dataProvider, TaskObject, new List<EmailComponent.OperationType>() { EmailComponent.OperationType.ChangeStatus }));

                mailingList = mailingList.Distinct().ToList();
                mailingList.RemoveAll(o => o.Actor == null);
                mailingList.RemoveAll(o => String.IsNullOrEmpty(o.Actor.Email));

                mailingList.RemoveAll(o => o.IdOperator == LoggedOperator.IdOperator);

                foreach (OpOperator oItem in mailingList)
                {
                    SendTaskEmail(oItem.Actor.Email,
                        ResourcesText.TaskStatusChangedTo + " " + TaskObject.TaskStatus.ToString(), strBody.ToString());
                }

            }
        }

        private static void SendTaskEmail(string Email, string Subject, string Message)
        {
            if (!EmailComponent.SendEmail(Email, Subject, Message))
            {
                //ErrorMessageBox.ShowDialog(AssemblyWrapper.Title, ResourcesText.EmailSendFail);
                throw new Exception(ResourcesText.EmailSendFail);
            }
        }
        #endregion

        #region FuelAuditOperations

        public static List<OpTask> FuelAuditAllowedTasks(DataProvider dataProvider, OpOperator loggedOperator)
        {
            List<OpTask> retTaskList = new List<OpTask>();
            List<OpDistributorSupplier> distributorSupplier = dataProvider.GetDistributorSupplierFilter(IdDistributor: new int[] { loggedOperator.IdDistributor });
            if (distributorSupplier != null && distributorSupplier.Count > 0)
            {
                List<OpSupplier> allowedSupplier = RoleComponent.FilterByPermission<OpSupplier>(distributorSupplier.Select(s => s.Supplier).ToList(), Activity.ALLOWED_SUPPLIER, loggedOperator);
                List<OpTask> tList = dataProvider.GetTaskFilter(loadNavigationProperties: false,
                                                                loadTaskHistory: false,
                                                                IdDistributor: new int[] { loggedOperator.IdDistributor });
                if (tList != null && tList.Count > 0)
                {
                    List<OpTaskData> taskDataList = dataProvider.GetTaskDataFilter(loadNavigationProperties: false,
                                                                                      IdTask: tList.Select(d => d.IdTask).ToArray(),
                                                                                      IdDataType: new long[] { DataType.TASK_CARRIER });
                    foreach (OpTaskData tdItem in taskDataList)
                    {
                        if (tdItem != null && tdItem.Value != null && allowedSupplier.Exists(s => String.Equals(s.Name, tdItem.Value.ToString().Trim())))
                        {
                            retTaskList.Add(tList.Find(t => t.IdTask == tdItem.IdTask));
                        }
                    }
                }
            }

            return retTaskList;
        }

        public static List<MeterComponent.GODeliverySubject> GetFuelAuditTaskData(DataProvider dataProvider, OpTask task, List<OpTaskData> neccessaryData = null)
        {
            List<MeterComponent.GODeliverySubject> retList = new List<MeterComponent.GODeliverySubject>();

            if (neccessaryData == null)
            {
                neccessaryData = dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask },
                                                                IdDataType: new long[]{ DataType.TASK_VOLUME_CONTAINTER,
                                                                                        DataType.TASK_OTHER_CONTAINER,
                                                                                        DataType.REFUEL_LOADING_DENSITY,
                                                                                        DataType.REFUEL_TOTAL_WEIGHT,
                                                                                        DataType.REFUEL_COMPARTMENTS,
                                                                                        DataType.REFUEL_TEMPERATURE,
                                                                                        DataType.REFUEL_LOADING_TEMPERATURE,
                                                                                        DataType.ATG_PARAMS_TANK_NUMBER,
                                                                                        DataType.ATG_PARAMS_PRODUCT_CODE,
                                                                                        DataType.TASK_PRODUCT_CODE,
                                                                                        DataType.METER_IMR_SERVER_ID,
                                                                                        DataType.REFUEL_FLOWMETER_SERIAL_NUMBER });
            }

            if (neccessaryData != null && neccessaryData.Count > 0 && neccessaryData.Exists(d => d.IdDataType == DataType.ATG_PARAMS_TANK_NUMBER))
            {
                foreach (OpTaskData tdItem in neccessaryData.Where(d => d.IdDataType == DataType.ATG_PARAMS_TANK_NUMBER))
                {
                    long idMeter = Convert.ToInt64(neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.METER_IMR_SERVER_ID).Value);
                    MeterComponent.GOTank tank = new MeterComponent.GOTank(idMeter);
                    tank.IdProductCode = Convert.ToInt32(neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.ATG_PARAMS_PRODUCT_CODE).Value);
                    tank.ProductCode = neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.TASK_PRODUCT_CODE).Value.ToString();
                    tank.TankNumber = Convert.ToInt32(neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.ATG_PARAMS_TANK_NUMBER).Value);

                    string compartments = neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.REFUEL_COMPARTMENTS).Value.ToString();
                    string[] compartmentsArray = compartments.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<MeterComponent.GOCompartment> compartmentList = new List<MeterComponent.GOCompartment>();
                    compartmentsArray.ToList().ForEach(c => compartmentList.Add(new MeterComponent.GOCompartment(Convert.ToInt32(c))));

                    string volumes = neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.TASK_VOLUME_CONTAINTER).Value.ToString();
                    string[] volumesArray = volumes.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    MeterComponent.GODeliverySubject newDeliveryObject = new MeterComponent.GODeliverySubject(tank, compartmentList, 0.0);
                    newDeliveryObject.Index = tdItem.Index;

                    foreach (string sItem in volumesArray)
                    {
                        string[] singleVolume = sItem.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                        double volumeValue = 0.0;
                        string volumeValueString = singleVolume[1].Replace(',', '.');
                        double.TryParse(volumeValueString, NumberStyles.Any, CultureInfo.InvariantCulture, out volumeValue);

                        switch (Convert.ToInt32(singleVolume[0]))
                        {
                            case (int)Enums.DataSourceType.Estimation:
                                if (singleVolume.Count() == 3)
                                    newDeliveryObject.PlannedVolume = volumeValue * 1000.0;//zamiana m3 na litry
                                else if (singleVolume.Count() == 4)//15C
                                    newDeliveryObject.PlannedVolume15C = volumeValue * 1000.0;//zamiana m3 na litry
                                break;
                            case (int)Enums.DataSourceType.ManualDataEntry:
                                if (singleVolume.Count() == 3)
                                    newDeliveryObject.VolumeAuditor = volumeValue * 1000.0;//zamiana m3 na litry
                                else if (singleVolume.Count() == 4)//15C
                                    newDeliveryObject.VolumeAuditor15C = volumeValue * 1000.0;//zamiana m3 na litry
                                break;
                            case (int)Enums.DataSourceType.OKO:
                                if (singleVolume.Count() == 3)
                                    newDeliveryObject.VolumeATG = volumeValue * 1000.0;//zamiana m3 na litry
                                else if (singleVolume.Count() == 4)//15C
                                    newDeliveryObject.VolumeATG15C = volumeValue * 1000.0;//zamiana m3 na litry
                                break;
                            case (int)Enums.DataSourceType.Fitter:
                                if (singleVolume.Count() == 3)
                                    newDeliveryObject.VolumeFlowmeter = volumeValue * 1000.0;//zamiana m3 na litry
                                else if (singleVolume.Count() == 4)//15C
                                    newDeliveryObject.VolumeFlowmeter15C = volumeValue * 1000.0;//zamiana m3 na litry
                                break;
                            default:
                                break;
                        }
                    }


                    string other = neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.TASK_OTHER_CONTAINER).Value.ToString();
                    string[] otherArray = other.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (string sItem in otherArray)
                    {
                        string[] singleOther = sItem.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                        double otherValue = 0.0;
                        if (singleOther.Count() > 2 && singleOther[2] != null)
                        {
                            string otherValueString = singleOther[2].Replace(',', '.');
                            double.TryParse(otherValueString, NumberStyles.Any, CultureInfo.InvariantCulture, out otherValue);

                            switch (singleOther[0])
                            {
                                case "TEMPERATURE":
                                    if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                                        newDeliveryObject.TemperatureAuditor = otherValue;
                                    else if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                                        newDeliveryObject.TemperatureFlowmeter = otherValue;
                                    break;
                                case "DENSITY":
                                    if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                                        newDeliveryObject.DensityAuditor = otherValue * 1000.0;//m3 na dm3
                                    else if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                                        newDeliveryObject.DensityFlowmeter = otherValue * 1000.0;//m3 na dm3
                                    break;
                                case "WEIGHT":
                                    if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                                        newDeliveryObject.WeightAuditor = otherValue;
                                    else if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                                        newDeliveryObject.WeightFlowmeter = otherValue;
                                    break;
                                case "REMARKS":
                                    if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                                        newDeliveryObject.FlowmeterRemarks = otherValueString;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    OpTaskData flowmeterSerialNumber = neccessaryData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.REFUEL_FLOWMETER_SERIAL_NUMBER);
                    if (flowmeterSerialNumber != null && flowmeterSerialNumber.Value != null)
                        newDeliveryObject.FlowmeterSerialNumber = flowmeterSerialNumber.Value.ToString();

                    retList.Add(newDeliveryObject);
                }
            }
            return retList;
        }

        public static MeterComponent.GODeliverySubject AddFuelAuditDeliverySubject(DataProvider dataProvider,
                                                                                   OpTask task,
                                                                                   MeterComponent.GOTank tank,
                                                                                   List<MeterComponent.GOCompartment> compartments,
                                                                                   double plannedVolume,
                                                                                   double plannedVolume15C,
                                                                                   double auditorVolume, double auditorVolume15C,
                                                                                   double atgVolume, double atgVolume15C,
                                                                                   double flowmeterVolume, double flowmeterVolume15C,
                                                                                   double temperatureAuditorC, double temperatureFlowmeterC,
                                                                                   double densityAuditorKgDm3, double densityFlowmeterKgDm3,
                                                                                   double weightAuditorKg, double weightFlowmeterKg,
                                                                                   string flowmeterRemarks, string flowmeterSerialNumber,
                                                                                   out List<OpTaskData> newTaskDataList)
        {
            newTaskDataList = new List<OpTaskData>();

            if (flowmeterRemarks == "null")
                flowmeterRemarks = null;

            MeterComponent.GODeliverySubject newDeliverySubject = new MeterComponent.GODeliverySubject(tank, compartments, plannedVolume);
            newDeliverySubject.PlannedVolume15C = plannedVolume15C;
            newDeliverySubject.VolumeAuditor = auditorVolume;
            newDeliverySubject.VolumeAuditor15C = auditorVolume15C;
            newDeliverySubject.VolumeATG = atgVolume;
            newDeliverySubject.VolumeATG15C = atgVolume15C;
            newDeliverySubject.VolumeFlowmeter = flowmeterVolume;
            newDeliverySubject.VolumeFlowmeter15C = flowmeterVolume15C;

            newDeliverySubject.WeightAuditor = weightAuditorKg;
            newDeliverySubject.WeightFlowmeter = weightFlowmeterKg;

            newDeliverySubject.DensityAuditor = densityAuditorKgDm3;
            newDeliverySubject.DensityFlowmeter = densityFlowmeterKgDm3;

            newDeliverySubject.TemperatureAuditor = temperatureAuditorC;
            newDeliverySubject.TemperatureFlowmeter = temperatureFlowmeterC;

            newDeliverySubject.FlowmeterRemarks = flowmeterRemarks;
            newDeliverySubject.FlowmeterSerialNumber = flowmeterSerialNumber;

            int nextIndex = 0;
            List<OpTaskData> taskDataTankNumbers = dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask }, IdDataType: new long[] { DataType.ATG_PARAMS_TANK_NUMBER });
            if (taskDataTankNumbers != null && taskDataTankNumbers.Count > 0)
                nextIndex = taskDataTankNumbers.Max(d => d.Index) + 1;
            #region ATG_PARAMS_TANK_NUMBER
            OpTaskData newTaskData = new OpTaskData();
            newTaskData.IdDataType = DataType.ATG_PARAMS_TANK_NUMBER;
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.Task = task;
            newTaskData.Value = newDeliverySubject.Tank.TankNumber;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);
            #endregion
            #region ATG_PARAMS_PRODUCT_CODE
            newTaskData = new OpTaskData();
            newTaskData.IdDataType = DataType.ATG_PARAMS_PRODUCT_CODE;
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.Task = task;
            newTaskData.Value = newDeliverySubject.Tank.IdProductCode;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);
            #endregion
            #region TASK_PRODUCT_CODE
            newTaskData = new OpTaskData();
            newTaskData.IdDataType = DataType.TASK_PRODUCT_CODE;
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.Task = task;
            newTaskData.Value = newDeliverySubject.Tank.ProductCode;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);
            #endregion
            #region REFUEL_COMPARTMENTS
            newTaskData = new OpTaskData();
            newTaskData.IdDataType = DataType.REFUEL_COMPARTMENTS;
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.Task = task;
            newTaskData.Value = newDeliverySubject.CompartmentList;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);
            #endregion
            #region TASK_VOLUME_CONTAINTER
            string volumesToSave = "";
            //wszystkie objetosci jakie dostajemy sa w listrach i musi je przekonvertować na m3
            OpDataType volumeDataType = dataProvider.GetDataType(DataType.REFUEL_TOTAL_VOLUME);
            volumesToSave += (int)Enums.DataSourceType.Estimation + ":" + (newDeliverySubject.PlannedVolume / 1000.0) + ":" + volumeDataType.IdUnit + ";";
            volumesToSave += (int)Enums.DataSourceType.Estimation + ":" + (newDeliverySubject.PlannedVolume15C / 1000.0) + ":" + volumeDataType.IdUnit + ":15C;";
            volumesToSave += (int)Enums.DataSourceType.ManualDataEntry + ":" + (newDeliverySubject.VolumeAuditor / 1000.0) + ":" + volumeDataType.IdUnit + ";";
            volumesToSave += (int)Enums.DataSourceType.ManualDataEntry + ":" + (newDeliverySubject.VolumeAuditor15C / 1000.0) + ":" + volumeDataType.IdUnit + ":15C;";
            volumesToSave += (int)Enums.DataSourceType.OKO + ":" + (newDeliverySubject.VolumeATG / 1000.0) + ":" + volumeDataType.IdUnit + ";";
            volumesToSave += (int)Enums.DataSourceType.OKO + ":" + (newDeliverySubject.VolumeATG15C / 1000.0) + ":" + volumeDataType.IdUnit + ":15C;";
            volumesToSave += (int)Enums.DataSourceType.Fitter + ":" + (newDeliverySubject.VolumeFlowmeter / 1000.0) + ":" + volumeDataType.IdUnit + ";";
            volumesToSave += (int)Enums.DataSourceType.Fitter + ":" + (newDeliverySubject.VolumeFlowmeter15C / 1000.0) + ":" + volumeDataType.IdUnit + ":15C;";

            newTaskData = new OpTaskData();
            newTaskData.IdDataType = DataType.TASK_VOLUME_CONTAINTER;
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.Task = task;
            newTaskData.Value = volumesToSave;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);

            #endregion
            #region TASK_OTHER_CONTAINER
            string otherToSave = "";
            //gestos trzeba przeliczyc z kg/dm3 na kg/m3
            otherToSave += "WEIGHT:" + (int)Enums.DataSourceType.ManualDataEntry + ":" + (newDeliverySubject.WeightAuditor / 1000.0) + ";";
            otherToSave += "DENSITY:" + (int)Enums.DataSourceType.ManualDataEntry + ":" + (newDeliverySubject.DensityAuditor / 1000.0) + ";";
            otherToSave += "TEMPERATURE:" + (int)Enums.DataSourceType.ManualDataEntry + ":" + (newDeliverySubject.TemperatureAuditor / 1000.0) + ";";

            otherToSave += "WEIGHT:" + (int)Enums.DataSourceType.Fitter + ":" + (newDeliverySubject.WeightFlowmeter / 1000.0) + ";";
            if (newDeliverySubject.DensityFlowmeter == 0.0)
            {
                double gestosc = 0.0;
                if (String.Equals(newDeliverySubject.Tank.ProductCode, "SFS Diesel"))
                    gestosc = (double)MeterComponent.GODeliverySubject.FlowmeterDensityKgM3.Diesel;
                else if (String.Equals(newDeliverySubject.Tank.ProductCode, "VPR NITRO+"))
                    gestosc = (double)MeterComponent.GODeliverySubject.FlowmeterDensityKgM3.Racing;
                else if (String.Equals(newDeliverySubject.Tank.ProductCode, "SFS 95"))
                    gestosc = (double)MeterComponent.GODeliverySubject.FlowmeterDensityKgM3.Pb95;
                else if (String.Equals(newDeliverySubject.Tank.ProductCode, "VP95 NITRO+"))
                    gestosc = (double)MeterComponent.GODeliverySubject.FlowmeterDensityKgM3.VP95;
                else if (String.Equals(newDeliverySubject.Tank.ProductCode, "VPD NITRO+"))
                    gestosc = (double)MeterComponent.GODeliverySubject.FlowmeterDensityKgM3.VPDiesel;
                newDeliverySubject.DensityFlowmeter = gestosc * 1000.0;

                otherToSave += "DENSITY:" + (int)Enums.DataSourceType.Fitter + ":" + gestosc + ";";
            }
            else
                otherToSave += "DENSITY:" + (int)Enums.DataSourceType.Fitter + ":" + (newDeliverySubject.DensityFlowmeter / 1000.0) + ";";
            otherToSave += "TEMPERATURE:" + (int)Enums.DataSourceType.Fitter + ":" + (newDeliverySubject.TemperatureFlowmeter / 1000.0) + ";";
            otherToSave += "REMARKS:" + (int)Enums.DataSourceType.Fitter + ":" + flowmeterRemarks + ";";

            newTaskData = new OpTaskData();
            newTaskData.IdDataType = DataType.TASK_OTHER_CONTAINER;
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.Task = task;
            newTaskData.Value = otherToSave;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);

            #endregion
            #region METER_IMR_SERVER_ID
            newTaskData = new OpTaskData();
            newTaskData.IdDataType = DataType.METER_IMR_SERVER_ID;
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.Task = task;
            newTaskData.Value = newDeliverySubject.Tank.IdMeter;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);
            #endregion
            #region REFUEL_FLOWMETER_SERIAL_NUMBER
            newTaskData = new OpTaskData();
            newTaskData.Index = nextIndex;
            newTaskData.OpState = Business.Objects.OpChangeState.Loaded;
            newTaskData.IdDataType = DataType.REFUEL_FLOWMETER_SERIAL_NUMBER;
            newTaskData.Task = task;
            newTaskData.Value = flowmeterSerialNumber;
            newTaskData.IdTaskData = dataProvider.SaveTaskData(newTaskData);
            newTaskDataList.Add(newTaskData);
            #endregion

            newDeliverySubject.Index = nextIndex;

            return newDeliverySubject;
        }

        public static MeterComponent.GODeliverySubject UpdateFuelAuditDelivelySubject(DataProvider dataProvider, MeterComponent.GODeliverySubject deliverySubject,
                                                          List<MeterComponent.GOTank> tankList, OpTask task,
                                                          string tank, string compartmentList,
                                                          double auditorVolume, double auditorVolume15C,
                                                          double atgVolume, double atg15CVolume,
                                                          double flowmeterVolume, double flowmeter15CVolume,
                                                          double temperatureAuditorC, double temperatureFlowmeterC,
                                                          double densityAuditorKgDm3, double densityFlowmeterKgDm3,
                                                          double weightAuditorKg, double weightFlowmeterKg,
                                                          string flowmeterRemarks, string flowmeterSerialNumer)
        {
            MeterComponent.GODeliverySubject deliverySubjectToUpdate = deliverySubject;

            if (flowmeterRemarks == "null")
                flowmeterRemarks = null;

            List<OpTaskData> neccessaryData = dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask },
                                                        IdDataType: new long[]{ DataType.TASK_VOLUME_CONTAINTER,
                                                                                DataType.TASK_OTHER_CONTAINER,
                                                                                DataType.REFUEL_LOADING_DENSITY,
                                                                                DataType.REFUEL_TOTAL_WEIGHT,
                                                                                DataType.REFUEL_COMPARTMENTS,
                                                                                DataType.REFUEL_TEMPERATURE,
                                                                                DataType.REFUEL_LOADING_TEMPERATURE,
                                                                                DataType.ATG_PARAMS_TANK_NUMBER,
                                                                                DataType.ATG_PARAMS_PRODUCT_CODE,
                                                                                DataType.TASK_PRODUCT_CODE,
                                                                                DataType.METER_IMR_SERVER_ID,
                                                                                DataType.REFUEL_FLOWMETER_SERIAL_NUMBER });

            OpTaskData taskDataToSave = null;

            #region Tank
            if (!String.Equals(deliverySubjectToUpdate.Tank.ToString(), tank))
            {
                MeterComponent.GOTank tankObj = tankList.Find(t => String.Equals(t.ToString(), tank));

                #region ATG_PARAMS_TANK_NUMBER
                taskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.ATG_PARAMS_TANK_NUMBER);
                if (taskDataToSave == null)
                {
                    taskDataToSave = new OpTaskData();
                    taskDataToSave.Index = deliverySubjectToUpdate.Index;
                    taskDataToSave.IdDataType = DataType.ATG_PARAMS_TANK_NUMBER;
                    taskDataToSave.IdTask = task.IdTask;
                }
                taskDataToSave.Value = tankObj.TankNumber;
                dataProvider.SaveTaskData(taskDataToSave);
                #endregion
                #region ATG_PARAMS_PRODUCT_CODE
                taskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.ATG_PARAMS_PRODUCT_CODE);
                if (taskDataToSave == null)
                {
                    taskDataToSave = new OpTaskData();
                    taskDataToSave.Index = deliverySubjectToUpdate.Index;
                    taskDataToSave.IdDataType = DataType.ATG_PARAMS_PRODUCT_CODE;
                    taskDataToSave.IdTask = task.IdTask;
                }
                taskDataToSave.Value = tankObj.IdProductCode;
                dataProvider.SaveTaskData(taskDataToSave);
                #endregion
                #region TASK_PRODUCT_CODE
                taskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.TASK_PRODUCT_CODE);
                if (taskDataToSave == null)
                {
                    taskDataToSave = new OpTaskData();
                    taskDataToSave.Index = deliverySubjectToUpdate.Index;
                    taskDataToSave.IdDataType = DataType.TASK_PRODUCT_CODE;
                    taskDataToSave.IdTask = task.IdTask;
                }
                taskDataToSave.Value = tankObj.ProductCode;
                dataProvider.SaveTaskData(taskDataToSave);
                #endregion
                #region METER_IMR_SERVER_ID
                taskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.METER_IMR_SERVER_ID);
                if (taskDataToSave == null)
                {
                    taskDataToSave = new OpTaskData();
                    taskDataToSave.Index = deliverySubjectToUpdate.Index;
                    taskDataToSave.IdDataType = DataType.METER_IMR_SERVER_ID;
                    taskDataToSave.IdTask = task.IdTask;
                }
                taskDataToSave.Value = tankObj.IdMeter;
                dataProvider.SaveTaskData(taskDataToSave);
                #endregion

                deliverySubjectToUpdate.Tank = tankObj;
            }
            #endregion
            #region Compartments
            if (!String.Equals(compartmentList, deliverySubjectToUpdate.CompartmentList))
            {
                string[] compartmentsSplit = compartmentList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                List<MeterComponent.GOCompartment> newCompartments = new List<MeterComponent.GOCompartment>();
                compartmentsSplit.ToList().ForEach(c => newCompartments.Add(new MeterComponent.GOCompartment(Convert.ToInt32(c))));

                #region REFUEL_COMPARTMENTS
                taskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.REFUEL_COMPARTMENTS);
                if (taskDataToSave == null)
                {
                    taskDataToSave = new OpTaskData();
                    taskDataToSave.Index = deliverySubjectToUpdate.Index;
                    taskDataToSave.IdDataType = DataType.REFUEL_COMPARTMENTS;
                    taskDataToSave.IdTask = task.IdTask;
                }
                taskDataToSave.Value = compartmentList;
                dataProvider.SaveTaskData(taskDataToSave);
                #endregion

                deliverySubjectToUpdate.Compartments = newCompartments;
            }
            #endregion

            taskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.TASK_VOLUME_CONTAINTER);
            #region Volume

            string[] volumeContainer = taskDataToSave.Value.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            bool change = false;
            #region VolumeAuditor
            if (deliverySubjectToUpdate.VolumeAuditor != auditorVolume)
            {
                for (int i = 0; i < volumeContainer.Count(); i++)
                {
                    string[] singleVolue = volumeContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Convert.ToInt32(singleVolue[0]) == (int)Enums.DataSourceType.ManualDataEntry)
                    {
                        if (singleVolue.Count() == 3)
                        {
                            volumeContainer[i] = singleVolue[0] + ":" + (auditorVolume / 1000.0) + ":" + singleVolue[2] + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region VolumeAuditor15C
            if (deliverySubjectToUpdate.VolumeAuditor15C != auditorVolume15C)
            {
                for (int i = 0; i < volumeContainer.Count(); i++)
                {
                    string[] singleVolue = volumeContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Convert.ToInt32(singleVolue[0]) == (int)Enums.DataSourceType.ManualDataEntry)
                    {
                        if (singleVolue.Count() == 4)
                        {
                            volumeContainer[i] = singleVolue[0] + ":" + (auditorVolume15C / 1000.0) + ":" + singleVolue[2] + ":" + singleVolue[3] + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region AtgVolume
            if (deliverySubjectToUpdate.VolumeATG != atgVolume)
            {
                for (int i = 0; i < volumeContainer.Count(); i++)
                {
                    string[] singleVolue = volumeContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Convert.ToInt32(singleVolue[0]) == (int)Enums.DataSourceType.OKO)
                    {
                        if (singleVolue.Count() == 3)
                        {
                            volumeContainer[i] = singleVolue[0] + ":" + (atgVolume / 1000.0) + ":" + singleVolue[2] + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region Atg15CVolume
            if (deliverySubjectToUpdate.VolumeATG15C != atg15CVolume)
            {
                for (int i = 0; i < volumeContainer.Count(); i++)
                {
                    string[] singleVolue = volumeContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Convert.ToInt32(singleVolue[0]) == (int)Enums.DataSourceType.OKO)
                    {
                        if (singleVolue.Count() == 4)
                        {
                            volumeContainer[i] = singleVolue[0] + ":" + (atg15CVolume / 1000.0) + ":" + singleVolue[2] + ":" + singleVolue[3] + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region FlowmeterVolume
            if (deliverySubjectToUpdate.VolumeFlowmeter != flowmeterVolume)
            {
                for (int i = 0; i < volumeContainer.Count(); i++)
                {
                    string[] singleVolue = volumeContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Convert.ToInt32(singleVolue[0]) == (int)Enums.DataSourceType.Fitter)
                    {
                        if (singleVolue.Count() == 3)
                        {
                            volumeContainer[i] = singleVolue[0] + ":" + (flowmeterVolume / 1000.0) + ":" + singleVolue[2] + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region Flowmeter15CVolume
            if (deliverySubjectToUpdate.VolumeFlowmeter15C != flowmeter15CVolume)
            {
                for (int i = 0; i < volumeContainer.Count(); i++)
                {
                    string[] singleVolue = volumeContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (Convert.ToInt32(singleVolue[0]) == (int)Enums.DataSourceType.Fitter)
                    {
                        if (singleVolue.Count() == 4)
                        {
                            volumeContainer[i] = singleVolue[0] + ":" + (flowmeter15CVolume / 1000.0) + ":" + singleVolue[2] + ":" + singleVolue[3] + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion

            if (change)
            {
                for (int i = 0; i < volumeContainer.Count(); i++)
                {
                    if (volumeContainer[i].ElementAt(volumeContainer[i].Length - 1) != ';')
                        volumeContainer[i] += ";";
                }

                string toSaveVolumeContainerValue = "";
                volumeContainer.ToList().ForEach(c => toSaveVolumeContainerValue += c);
                taskDataToSave.Value = toSaveVolumeContainerValue;
                dataProvider.SaveTaskData(taskDataToSave);
            }

            deliverySubjectToUpdate.VolumeAuditor = auditorVolume;
            deliverySubjectToUpdate.VolumeAuditor15C = auditorVolume15C;
            deliverySubjectToUpdate.VolumeATG = atgVolume;
            deliverySubjectToUpdate.VolumeATG15C = atg15CVolume;
            deliverySubjectToUpdate.VolumeFlowmeter = flowmeterVolume;
            deliverySubjectToUpdate.VolumeFlowmeter15C = flowmeter15CVolume;

            #endregion
            #region REFUEL_FLOWMETER_SERIAL_NUMBER
            OpTaskData flowmeterSnTaskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.REFUEL_FLOWMETER_SERIAL_NUMBER);
            if (flowmeterSnTaskDataToSave == null)
            {
                flowmeterSnTaskDataToSave = new OpTaskData();
                flowmeterSnTaskDataToSave.Index = deliverySubjectToUpdate.Index;
                flowmeterSnTaskDataToSave.IdDataType = DataType.REFUEL_FLOWMETER_SERIAL_NUMBER;
                flowmeterSnTaskDataToSave.Task = task;
                flowmeterSnTaskDataToSave.OpState = OpChangeState.Loaded;
                flowmeterSnTaskDataToSave.Value = flowmeterSerialNumer;
                flowmeterSnTaskDataToSave.IdTaskData = dataProvider.SaveTaskData(flowmeterSnTaskDataToSave);
            }
            else
            {
                if (flowmeterSnTaskDataToSave.Value == null || !String.Equals(flowmeterSerialNumer, flowmeterSnTaskDataToSave.Value.ToString()))
                {
                    flowmeterSnTaskDataToSave.Value = flowmeterSerialNumer;
                    flowmeterSnTaskDataToSave.IdTaskData = dataProvider.SaveTaskData(flowmeterSnTaskDataToSave);
                }
            }

            deliverySubjectToUpdate.FlowmeterSerialNumber = flowmeterSerialNumer;
            #endregion
            #region Other

            taskDataToSave = neccessaryData.Find(d => d.Index == deliverySubjectToUpdate.Index && d.IdDataType == DataType.TASK_OTHER_CONTAINER);

            string[] otherContainer = taskDataToSave.Value.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            change = false;
            #region TemperatureAuditor
            if (deliverySubjectToUpdate.TemperatureAuditor != temperatureAuditorC)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    string[] singleOther = otherContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.Equals(singleOther[0], "TEMPERATURE"))
                    {
                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                        {
                            otherContainer[i] = "TEMPERATURE:" + ((int)Enums.DataSourceType.ManualDataEntry).ToString() + ":" + temperatureAuditorC + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region TemperatureFlowmeter
            if (deliverySubjectToUpdate.TemperatureFlowmeter != temperatureFlowmeterC)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    string[] singleOther = otherContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.Equals(singleOther[0], "TEMPERATURE"))
                    {
                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                        {
                            otherContainer[i] = "TEMPERATURE:" + ((int)Enums.DataSourceType.Fitter).ToString() + ":" + temperatureFlowmeterC + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region DensityAuditor
            if (deliverySubjectToUpdate.DensityAuditor != densityAuditorKgDm3)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    string[] singleOther = otherContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.Equals(singleOther[0], "DENSITY"))
                    {
                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                        {
                            otherContainer[i] = "DENSITY:" + ((int)Enums.DataSourceType.ManualDataEntry).ToString() + ":" + densityAuditorKgDm3 / 1000.0 + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region DensityFlowmeter
            if (deliverySubjectToUpdate.DensityFlowmeter != densityFlowmeterKgDm3)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    string[] singleOther = otherContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.Equals(singleOther[0], "DENSITY"))
                    {
                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                        {
                            otherContainer[i] = "DENSITY:" + ((int)Enums.DataSourceType.Fitter).ToString() + ":" + densityFlowmeterKgDm3 / 1000.0 + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region WeightAuditor
            if (deliverySubjectToUpdate.WeightAuditor != weightAuditorKg)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    string[] singleOther = otherContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.Equals(singleOther[0], "WEIGHT"))
                    {
                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                        {
                            otherContainer[i] = "WEIGHT:" + ((int)Enums.DataSourceType.ManualDataEntry).ToString() + ":" + weightAuditorKg + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region WeightFlowmeter
            if (deliverySubjectToUpdate.WeightFlowmeter != weightFlowmeterKg)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    string[] singleOther = otherContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.Equals(singleOther[0], "WEIGHT"))
                    {
                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                        {
                            otherContainer[i] = "WEIGHT:" + ((int)Enums.DataSourceType.Fitter).ToString() + ":" + weightFlowmeterKg + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion
            #region FlowmeterRemarks
            if (deliverySubjectToUpdate.FlowmeterRemarks != flowmeterRemarks)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    string[] singleOther = otherContainer[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    if (String.Equals(singleOther[0], "REMARKS"))
                    {
                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                        {
                            otherContainer[i] = "REMARKS:" + ((int)Enums.DataSourceType.Fitter).ToString() + ":" + flowmeterRemarks + ";";
                            break;
                        }
                    }
                }
                change = true;
            }
            #endregion

            if (change)
            {
                for (int i = 0; i < otherContainer.Count(); i++)
                {
                    if (otherContainer[i].ElementAt(otherContainer[i].Length - 1) != ';')
                        otherContainer[i] += ";";
                }

                string toSaveOtherContainerValue = "";
                otherContainer.ToList().ForEach(c => toSaveOtherContainerValue += c);
                taskDataToSave.Value = toSaveOtherContainerValue;
                dataProvider.SaveTaskData(taskDataToSave);
            }

            deliverySubjectToUpdate.VolumeAuditor = auditorVolume;
            deliverySubjectToUpdate.VolumeAuditor15C = auditorVolume15C;
            deliverySubjectToUpdate.VolumeATG = atgVolume;
            deliverySubjectToUpdate.VolumeATG15C = atg15CVolume;
            deliverySubjectToUpdate.VolumeFlowmeter = flowmeterVolume;
            deliverySubjectToUpdate.VolumeFlowmeter15C = flowmeter15CVolume;

            deliverySubject.TemperatureAuditor = temperatureAuditorC;
            deliverySubject.TemperatureFlowmeter = temperatureFlowmeterC;
            deliverySubject.DensityAuditor = densityAuditorKgDm3;
            deliverySubject.DensityFlowmeter = densityFlowmeterKgDm3;
            deliverySubject.WeightAuditor = weightAuditorKg;
            deliverySubject.WeightFlowmeter = weightFlowmeterKg;

            deliverySubject.FlowmeterRemarks = flowmeterRemarks;
            #endregion

            return deliverySubjectToUpdate;
        }

        public static void DeleteFuelAuditDeliverySubject(DataProvider dataProvider, OpTask task, int deliverySubjectIndex)
        {
            List<long> dataTypeToDownload = new List<long>();
            dataTypeToDownload.Add(DataType.TASK_VOLUME_CONTAINTER);
            dataTypeToDownload.Add(DataType.TASK_OTHER_CONTAINER);
            dataTypeToDownload.Add(DataType.REFUEL_LOADING_DENSITY);
            dataTypeToDownload.Add(DataType.REFUEL_TOTAL_WEIGHT);
            dataTypeToDownload.Add(DataType.REFUEL_COMPARTMENTS);
            dataTypeToDownload.Add(DataType.REFUEL_TEMPERATURE);
            dataTypeToDownload.Add(DataType.REFUEL_LOADING_TEMPERATURE);
            dataTypeToDownload.Add(DataType.ATG_PARAMS_TANK_NUMBER);
            dataTypeToDownload.Add(DataType.ATG_PARAMS_PRODUCT_CODE);
            dataTypeToDownload.Add(DataType.TASK_PRODUCT_CODE);
            dataTypeToDownload.Add(DataType.METER_IMR_SERVER_ID);
            dataTypeToDownload.Add(DataType.REFUEL_FLOWMETER_SERIAL_NUMBER);
            List<OpTaskData> taskDataToDelete = dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask },
                                                                               IndexNbr: new int[] { deliverySubjectIndex },
                                                                               IdDataType: dataTypeToDownload.ToArray());

            foreach (OpTaskData tdItem in taskDataToDelete)
            {
                dataProvider.DeleteTaskData(tdItem);
            }
        }


        public static void UploadFuelAuditData(DataProvider dataProvider, OpTask task, List<OpTaskData> extendedTaskData)
        {
            if (task != null)
            {
                OpDistributorData distributorAwsrServer = task.Distributor.DataList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_AWSR_IMR_SERVER_ID);
                if (distributorAwsrServer == null)
                    distributorAwsrServer = dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { task.Distributor.IdDistributor },
                                                                                  IdDataType: new long[] { DataType.DISTRIBUTOR_AWSR_IMR_SERVER_ID },
                                                                                  loadNavigationProperties: false).FirstOrDefault();
                int idImrServer = -1;
                if (distributorAwsrServer != null && distributorAwsrServer.Value != null && int.TryParse(distributorAwsrServer.Value.ToString(), out idImrServer))
                {

                    if (idImrServer == 46) idImrServer = 16; //nowy serwer shella

                    OpImrServer awsrServer = dataProvider.GetImrServer(idImrServer);

                    //List<OpImrServer> serv = dataProvider.GetImrServerFilter();

                    string orderNumber = extendedTaskData.Find(td => td.IdDataType == DataType.TASK_ORDER_NUMBER).Value.ToString();
                    string truckNumber = extendedTaskData.Find(td => td.IdDataType == DataType.TASK_TANK_REGISTRATION_NUMBER).Value.ToString();
                    string trailerNumber = extendedTaskData.Find(td => td.IdDataType == DataType.TASK_TRAILER_REGISTRATION_NUMBER).Value.ToString();
                    string driverName = extendedTaskData.Find(td => td.IdDataType == DataType.TASK_TANK_DRIVER).Value.ToString();
                    string terminalName = extendedTaskData.Find(td => td.IdDataType == DataType.TASK_TANK_LOADING_PLACE).Value.ToString();
                    string carierName = extendedTaskData.Find(td => td.IdDataType == DataType.TASK_CARRIER).Value.ToString();

                    List<OpFile> files = dataProvider.GetFile(task.DataList.Where(w => w.IdDataType == (long)DataType.TASK_ATTACHMENT).Select(s => Convert.ToInt64(s.Value)).ToArray());

                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);

                    UTDWebServiceSoapClient client = null;
                    string errorMsg = "";
                    try
                    {
                        client = UTDComponent.GetUTDClient(awsrServer.WebserviceAddress, awsrServer.WebserviceTimeout);
                        client.Open();
                        DataTable dtResult = null;
                        if (client.GetDataFilter(out dtResult, out errorMsg, null, null, null, null, new long[] { DataType.LOCATION_CID }, null, "[VALUE] is not null AND CAST([VALUE] as nvarchar) like'" + task.Location.CID + "'", null))
                        {
                            if (dtResult != null && dtResult.Rows.Count > 0)
                            {
                                long awsrServerLocationId = Convert.ToInt64(dtResult.Rows[0]["ID_LOCATION"]);
                                dtResult = client.GetLocationEquipmentFilter(out errorMsg, null, new long[] { awsrServerLocationId }, null, null, "[ID_METER] is not null AND [END_TIME] is null");
                                if (errorMsg.Length == 0)
                                {
                                    if (dtResult != null && dtResult.Rows.Count > 0)
                                    {
                                        List<long> awsrServerIdMeterList = new List<long>();
                                        foreach (DataRow drItem in dtResult.Rows)
                                        {
                                            awsrServerIdMeterList.Add(Convert.ToInt64(drItem["ID_METER"]));
                                        }
                                        if (client.GetDataFilter(out dtResult, out errorMsg, null, null, null, awsrServerIdMeterList.ToArray(), new long[] { DataType.ATG_PARAMS_TANK_NUMBER }, null, null, null))
                                        {
                                            if (dtResult != null && dtResult.Rows.Count > 0)
                                            {
                                                List<Tuple<long, int>> meters = new List<Tuple<long, int>>();//ID_METER, METER_NUMBER
                                                foreach (DataRow drItem in dtResult.Rows)
                                                {
                                                    meters.Add(new Tuple<long, int>(Convert.ToInt64(drItem["ID_METER"]), Convert.ToInt32(drItem["VALUE"])));
                                                }

                                                DateTime referenceDate = dataProvider.ConvertTimeZoneToUtcTime(task.FitterVisitDate.Value);
                                                if (idImrServer == 16) //jesli shell, to przelaczanie z CORE na DW
                                                {
                                                    awsrServer = dataProvider.GetImrServer(17);
                                                    client.Close();
                                                    client = UTDComponent.GetUTDClient(awsrServer.WebserviceAddress, awsrServer.WebserviceTimeout);
                                                    client.Open();
                                                }
                                                //tutaj dodać to co poleci do DataArch
                                                OpActor OperatorAccepted = null;
                                                if (task.IdOperatorAccepted != null)
                                                    OperatorAccepted = dataProvider.GetActor((int)task.IdOperatorAccepted);
                                                List<OpTaskData> taskMovie = new List<OpTaskData>();
                                                taskMovie.AddRange(dataProvider.GetTaskDataFilter(IdTask: new int[] { task.IdTask }, IdDataType: new long[] { (long)DataType.TASK_FILE_LINK, (long)DataType.TASK_FILE_NAME }).ToArray());

                                                long? idDataSourceFlowmeter = client.SaveDataSource(0, (int)Enums.DataSourceType.Flowmeter);

                                                if (task.IdTask != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_ID, 0, task.IdTask, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (referenceDate != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_DATE, 0, dataProvider.ConvertUtcTimeToTimeZone(referenceDate), referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (task.CreationDate != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_CREATION_DATE, 0, task.CreationDate, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (task.Deadline != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_DEADLINE, 0, task.Deadline, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (OperatorAccepted != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_ACKNOWLEDGING_PERSON_NAME, 0, OperatorAccepted.FullName, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (task.OperatorPerformer != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_PERFORMING_PERSON_NAME, 0, task.OperatorPerformer.Actor.FullName, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (task.OperatorRegistering != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_REGISTRANT_NAME, 0, task.OperatorRegistering.Actor.FullName, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (task.Priority != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_PRIORITY, 0, task.Priority, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (task.IdTaskStatus != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_STATE, 0, task.IdTaskStatus, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (task.MaintenanceNotes != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_AUDIT_SUMMARY, 0, task.MaintenanceNotes, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (referenceDate != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_START_TIME, 0, dataProvider.ConvertUtcTimeToTimeZone(referenceDate), referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                if (referenceDate != null)
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.REFUEL_END_TIME, 0, dataProvider.ConvertUtcTimeToTimeZone(referenceDate), referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);


                                                string dir = String.Format(@"{0}/{1}/", "Task", task.IdTask);
                                                int index_Nbr = 0;

                                                foreach (var f in files)
                                                {
                                                    Uri uri = ftpClient.GetFtpFileUri(dir, f.PhysicalFileName);
                                                    string path = uri.AbsoluteUri;

                                                    if (path != null)
                                                        client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.ATTACHMENT_FILE_URL, index_Nbr, path, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);
                                                    if (f.Size != null)
                                                        client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.ATTACHMENT_FILE_SIZE, index_Nbr, f.Size, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);
                                                    if (f.Description != null)
                                                        client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.ATTACHMENT_FILE_DESCR, index_Nbr, f.Description, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);
                                                    if (f.InsertDate != null)
                                                        client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.ATTACHMENT_FILE_CREATION_DATE, index_Nbr, f.InsertDate, referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);

                                                    index_Nbr++;
                                                }

                                                if (taskMovie.Count > 0)
                                                {
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.TASK_FILE_LINK, 0, taskMovie.Where(w => w.IdDataType == DataType.TASK_FILE_LINK).Select(s => s.Value).FirstOrDefault(), referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);
                                                    client.SaveDataArchDW(0, null, null, awsrServerLocationId, DataType.TASK_FILE_NAME, 0, taskMovie.Where(w => w.IdDataType == DataType.TASK_FILE_NAME).Select(s => s.Value).FirstOrDefault(), referenceDate, idDataSourceFlowmeter, (int)Enums.DataSourceType.Flowmeter, 0, 1);
                                                }

                                                foreach (int iItem in meters.Select(m => m.Item2))
                                                {
                                                    List<OpTaskData> tmpParamsTankNumber = extendedTaskData.FindAll(td => td.IdDataType == DataType.ATG_PARAMS_TANK_NUMBER && Convert.ToInt32(td.Value) == iItem);
                                                    int meterNumber = iItem;
                                                    long idMeter = meters.Find(t => t.Item2 == meterNumber).Item1;

                                                    Dictionary<string, CompartmentRefuel> refuelDict = new Dictionary<string, CompartmentRefuel>();

                                                    foreach (OpTaskData tdItem in tmpParamsTankNumber)
                                                    {
                                                        string[] volumeContainer = extendedTaskData.Find(d => d.IdDataType == DataType.TASK_VOLUME_CONTAINTER && d.Index == tdItem.Index).Value.ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                                                        string compartments = extendedTaskData.Find(d => d.IdDataType == DataType.REFUEL_COMPARTMENTS && d.Index == tdItem.Index).Value.ToString();
                                                        string flowmeterSerialNumber = null;
                                                        if (extendedTaskData.Exists(d => d.IdDataType == DataType.REFUEL_FLOWMETER_SERIAL_NUMBER && d.Index == tdItem.Index))
                                                            flowmeterSerialNumber = extendedTaskData.Find(d => d.IdDataType == DataType.REFUEL_FLOWMETER_SERIAL_NUMBER && d.Index == tdItem.Index).Value.ToString();

                                                        foreach (string sItem in volumeContainer)
                                                        {
                                                            string[] singleVolume = sItem.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                                                            int idDataSourceType = Convert.ToInt32(singleVolume[0]);
                                                            string volumeValueString = singleVolume[1].Replace(',', '.');
                                                            double volume = 0.0;
                                                            double.TryParse(volumeValueString, NumberStyles.Any, CultureInfo.InvariantCulture, out volume);


                                                            if (!refuelDict.ContainsKey(compartments))
                                                                refuelDict.Add(compartments, new CompartmentRefuel(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                                                                                                                   compartments, Convert.ToInt32(tdItem.Value),
                                                                                                                   0.0, 0.0, 0.0, 0.0, 0.0, 0.0, flowmeterSerialNumber));

                                                            switch (Convert.ToInt32(singleVolume[0]))
                                                            {
                                                                case (int)Enums.DataSourceType.Estimation:
                                                                    if (singleVolume.Count() == 3)
                                                                        refuelDict[compartments].PlannedVolume += volume;//m3
                                                                    break;
                                                                case (int)Enums.DataSourceType.ManualDataEntry:
                                                                    if (singleVolume.Count() == 3)
                                                                        refuelDict[compartments].TerminalVolume += volume;//m3
                                                                    else if (singleVolume.Count() == 4)//15C
                                                                        refuelDict[compartments].Terminal15CVolume += volume;//m3
                                                                    break;
                                                                case (int)Enums.DataSourceType.OKO:
                                                                    if (singleVolume.Count() == 3)
                                                                        refuelDict[compartments].AtgVolume += volume;//m3
                                                                    else if (singleVolume.Count() == 4)//15C
                                                                        refuelDict[compartments].Atg15CVolume += volume;//m3 
                                                                    break;
                                                                case (int)Enums.DataSourceType.Fitter:
                                                                    if (singleVolume.Count() == 3)
                                                                        refuelDict[compartments].FlowmeterVolume += volume;//m3 
                                                                    else if (singleVolume.Count() == 4)//15C
                                                                        refuelDict[compartments].Flowmeter15CVolume += volume;//m3
                                                                    break;
                                                                default:
                                                                    break;
                                                            }
                                                        }

                                                        string other = extendedTaskData.Find(d => d.Index == tdItem.Index && d.IdDataType == DataType.TASK_OTHER_CONTAINER).Value.ToString();
                                                        string[] otherArray = other.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                                                        foreach (string sItem in otherArray)
                                                        {
                                                            string[] singleOther = sItem.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

                                                            double otherValue = 0.0;
                                                            if (singleOther.Count() > 2 && singleOther[2] != null)
                                                            {
                                                                string otherValueString = singleOther[2].Replace(',', '.');
                                                                double.TryParse(otherValueString, NumberStyles.Any, CultureInfo.InvariantCulture, out otherValue);

                                                                switch (singleOther[0])
                                                                {
                                                                    case "TEMPERATURE":
                                                                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                                                                            refuelDict[compartments].TemperatureAuditor = otherValue;
                                                                        else if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                                                                            refuelDict[compartments].TemperatureFlowmeter = otherValue;
                                                                        break;
                                                                    case "DENSITY":
                                                                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                                                                            refuelDict[compartments].DensityAuditor = otherValue;
                                                                        else if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                                                                            refuelDict[compartments].DensityFlowmeter = otherValue;
                                                                        break;
                                                                    case "WEIGHT":
                                                                        if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.ManualDataEntry)
                                                                            refuelDict[compartments].WeightAuditor = otherValue;
                                                                        else if (Convert.ToInt32(singleOther[1]) == (int)Enums.DataSourceType.Fitter)
                                                                            refuelDict[compartments].WeightFlowmeter = otherValue;
                                                                        break;
                                                                    default:
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (tmpParamsTankNumber != null && tmpParamsTankNumber.Count > 0)
                                                    {
                                                        //awsrServerLocationId = 200;//dla testów

                                                        //long? idDataSourcePlanned = client.SaveDataSource(0, (int)Enums.DataSourceType.Estimation);
                                                        //long? refuelIdPlanned = client.SaveRefuel(0, idMeter, awsrServerLocationId, idDataSourcePlanned.Value, referenceDate, referenceDate);

                                                        //long? idDataSourceTerminal = client.SaveDataSource(0, (int)Enums.DataSourceType.ManualDataEntry);
                                                        //long? refuelIdTerminal = client.SaveRefuel(0, idMeter, awsrServerLocationId, idDataSourceTerminal.Value, referenceDate, referenceDate);

                                                        //long? idDataSourceAtg = client.SaveDataSource(0, (int)Enums.DataSourceType.OKO);
                                                        //long? refuelIdAtg = client.SaveRefuel(0, idMeter, awsrServerLocationId, idDataSourceAtg.Value, referenceDate, referenceDate);



                                                        int indexNbr = 0;

                                                        foreach (CompartmentRefuel crItem in refuelDict.Values)
                                                        {
                                                            #region REFUEL_COMPARTMENTS

                                                            long? refuelIdFlowmeter = client.SaveRefuel(0, idMeter, awsrServerLocationId, idDataSourceFlowmeter.Value, referenceDate, referenceDate);

                                                            List<long> idRefuelList = new List<long>();
                                                            //idRefuelList.Add(refuelIdPlanned.Value);
                                                            //idRefuelList.Add(refuelIdTerminal.Value);
                                                            //idRefuelList.Add(refuelIdAtg.Value);
                                                            idRefuelList.Add(refuelIdFlowmeter.Value);

                                                            foreach (long rlItem in idRefuelList)
                                                            {
                                                                client.SaveRefuelData(0, rlItem, DataType.REFUEL_ORDER_NUMBER, 0, orderNumber, referenceDate, referenceDate, 1);
                                                                client.SaveRefuelData(0, rlItem, DataType.REFUEL_TRUCK_NUMBER, 0, truckNumber, referenceDate, referenceDate, 1);
                                                                client.SaveRefuelData(0, rlItem, DataType.REFUEL_DRIVER_NAME, 0, driverName, referenceDate, referenceDate, 1);
                                                                client.SaveRefuelData(0, rlItem, DataType.REFUEL_TERMINAL_NAME, 0, terminalName, referenceDate, referenceDate, 1);
                                                                client.SaveRefuelData(0, rlItem, DataType.REFUEL_CARRIER_NAME, 0, carierName, referenceDate, referenceDate, 1);
                                                                client.SaveRefuelData(0, rlItem, DataType.REFUEL_TRAILER_NUMBER, 0, trailerNumber, referenceDate, referenceDate, 1);
                                                            }


                                                            foreach (long rlItem in idRefuelList)//.Where(l => l != refuelIdAtg))
                                                                client.SaveRefuelData(0, rlItem, DataType.REFUEL_COMPARTMENTS, indexNbr, crItem.Compartments, referenceDate, referenceDate, 1);

                                                            //if (refuelDict.Values.Count(d => d.TankNumber == crItem.TankNumber) > 1 && crItem.AtgVolume > 0.0)
                                                            //{
                                                            //    string atgCompartments = String.Join(",", refuelDict.Values.Where(d => d.TankNumber == crItem.TankNumber).Select(d => d.TankNumber).ToArray());
                                                            //    client.SaveRefuelData(0, refuelIdAtg.Value, DataType.REFUEL_COMPARTMENTS, indexNbr, atgCompartments, referenceDate, referenceDate, 1);
                                                            //    client.SaveRefuelData(0, refuelIdAtg.Value, DataType.REFUEL_TOTAL_VOLUME, indexNbr, crItem.AtgVolume, referenceDate, referenceDate, 1);
                                                            //    client.SaveRefuelData(0, refuelIdAtg.Value, DataType.REFUEL_TOTAL_REFERENCE_VOLUME, indexNbr, crItem.Atg15CVolume, referenceDate, referenceDate, 1);
                                                            //}
                                                            //if (refuelDict.Values.Count(d => d.TankNumber == crItem.TankNumber) == 1)
                                                            //{
                                                            //    client.SaveRefuelData(0, refuelIdAtg.Value, DataType.REFUEL_COMPARTMENTS, indexNbr, crItem.Compartments, referenceDate, referenceDate, 1);
                                                            //    client.SaveRefuelData(0, refuelIdAtg.Value, DataType.REFUEL_TOTAL_VOLUME, indexNbr, crItem.AtgVolume, referenceDate, referenceDate, 1);
                                                            //    client.SaveRefuelData(0, refuelIdAtg.Value, DataType.REFUEL_TOTAL_REFERENCE_VOLUME, indexNbr, crItem.Atg15CVolume, referenceDate, referenceDate, 1);
                                                            //}
                                                            #endregion

                                                            //client.SaveRefuelData(0, refuelIdPlanned.Value, DataType.REFUEL_TOTAL_VOLUME, indexNbr, crItem.PlannedVolume, referenceDate, referenceDate, 1);
                                                            //client.SaveRefuelData(0, refuelIdTerminal.Value, DataType.REFUEL_TOTAL_VOLUME, indexNbr, crItem.TerminalVolume, referenceDate, referenceDate, 1);
                                                            client.SaveRefuelData(0, refuelIdFlowmeter.Value, DataType.REFUEL_TOTAL_VOLUME, indexNbr, crItem.FlowmeterVolume, referenceDate, referenceDate, 1);

                                                            //client.SaveRefuelData(0, refuelIdTerminal.Value, DataType.REFUEL_TOTAL_REFERENCE_VOLUME, indexNbr, crItem.Terminal15CVolume, referenceDate, referenceDate, 1);
                                                            client.SaveRefuelData(0, refuelIdFlowmeter.Value, DataType.REFUEL_TOTAL_REFERENCE_VOLUME, indexNbr, crItem.Flowmeter15CVolume, referenceDate, referenceDate, 1);

                                                            //client.SaveRefuelData(0, refuelIdTerminal.Value, DataType.REFUEL_LOADING_TEMPERATURE, indexNbr, crItem.TemperatureAuditor, referenceDate, referenceDate, 1);
                                                            client.SaveRefuelData(0, refuelIdFlowmeter.Value, DataType.REFUEL_TEMPERATURE, indexNbr, crItem.TemperatureFlowmeter, referenceDate, referenceDate, 1);

                                                            //client.SaveRefuelData(0, refuelIdTerminal.Value, DataType.REFUEL_LOADING_DENSITY, indexNbr, crItem.DensityAuditor, referenceDate, referenceDate, 1);
                                                            client.SaveRefuelData(0, refuelIdFlowmeter.Value, DataType.REFUEL_LOADING_DENSITY, indexNbr, crItem.DensityFlowmeter, referenceDate, referenceDate, 1);

                                                            //client.SaveRefuelData(0, refuelIdTerminal.Value, DataType.REFUEL_TOTAL_WEIGHT, indexNbr, crItem.WeightAuditor, referenceDate, referenceDate, 1);
                                                            client.SaveRefuelData(0, refuelIdFlowmeter.Value, DataType.REFUEL_TOTAL_WEIGHT, indexNbr, crItem.WeightFlowmeter, referenceDate, referenceDate, 1);
                                                            client.SaveRefuelData(0, refuelIdFlowmeter.Value, DataType.REFUEL_FLOWMETER_SERIAL_NUMBER, indexNbr, crItem.FlowmeterSerialNumber, referenceDate, referenceDate, 1);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                                throw new Exception("No meter params found");
                                        }
                                        else
                                            throw new Exception("Error during downloading meter params");
                                    }
                                    else
                                        throw new Exception("Location equipment for location: ID" + awsrServerLocationId.ToString() + ", CID: " + task.Location.CID + " not found");
                                }
                                else
                                    throw new Exception("Error during downloading location equipment");
                            }
                            else
                                throw new Exception("Location with CID: " + task.Location.CID + " not found");
                        }
                        else
                            throw new Exception("Error during downloading location");
                        if (client != null)
                            client.Close();

                    }
                    catch (Exception ex)
                    {
                        if (client != null)
                            client.Close();
                        throw new Exception(ex.Message);
                    }
                }
            }
        }




        private class CompartmentRefuel
        {
            public double PlannedVolume = 0.0;
            public double TerminalVolume = 0.0;
            public double Terminal15CVolume = 0.0;
            public double AtgVolume = 0.0;
            public double Atg15CVolume = 0.0;
            public double FlowmeterVolume = 0.0;
            public double Flowmeter15CVolume = 0.0;

            public double WeightAuditor { get; set; } //kg
            public double DensityAuditor { get; set; } //kg/m3
            public double TemperatureAuditor { get; set; } //C

            public double WeightFlowmeter { get; set; } //kg
            public double DensityFlowmeter { get; set; } //kg/m3
            public double TemperatureFlowmeter { get; set; } //C

            public string Compartments = "";
            public int TankNumber { get; set; }

            public string FlowmeterSerialNumber { get; set; }


            public CompartmentRefuel(double PlannedVolume,
                                     double TerminalVolume, double Terminal15CVolume,
                                     double AtgVolume, double Atg15CVolume,
                                     double FlowmeterVolume, double Flowmeter15CVolume,
                                     string Compartments,
                                     int TankNumber,
                                     double WeightAuditor, double WeightFlowmeter,
                                     double DensityAuditor, double DensityFlowmeter,
                                     double TemperatureAuditor, double TemperatureFlowmeter,
                                     string FlowmeterSerialNumber
                                     )
            {
                this.PlannedVolume = PlannedVolume;
                this.TerminalVolume = TerminalVolume;
                this.AtgVolume = AtgVolume;
                this.Atg15CVolume = Atg15CVolume;
                this.FlowmeterVolume = FlowmeterVolume;
                this.Flowmeter15CVolume = Flowmeter15CVolume;
                this.Compartments = Compartments;
                this.TankNumber = TankNumber;
                this.WeightAuditor = WeightAuditor;
                this.WeightFlowmeter = WeightFlowmeter;
                this.DensityAuditor = DensityAuditor;
                this.DensityFlowmeter = DensityFlowmeter;
                this.TemperatureAuditor = TemperatureAuditor;
                this.TemperatureFlowmeter = TemperatureFlowmeter;
                this.FlowmeterSerialNumber = FlowmeterSerialNumber;
            }
        }
        #endregion

        #region DeinstalledDeviceAdd

        public static void DeinstaledDeviceAdd(DataProvider dataProvider, OpOperator loggedOperator, OpArticle article, OpTask task, long? serialNbr, string externalSN, string notes,
            out OpDepositoryElement elementAssignedToTask, out OpDepositoryElement elementAssignedToDepository,
            OpDevice device, DateTime? timeStamp = null)
        {
            if (loggedOperator.DepositoryLocation == null)
            {
                loggedOperator.DepositoryLocation = DepositoryComponent.GetFitterDepository(dataProvider, loggedOperator);
            }
            if (loggedOperator.DepositoryLocation == null)
                throw new Exception(ResourcesText.FitterDepositoryNotDefined);

            elementAssignedToTask = DepositoryComponent.AssignElementToTask(dataProvider, loggedOperator, article.IdArticle, serialNbr, task, externalSN, false, true,
                        notes, dataProvider.GetDeviceStateType((int)OpDeviceStateType.Enum.REMOVED));

            elementAssignedToDepository = DepositoryComponent.AssignElementToLocation(dataProvider, loggedOperator, article.IdArticle, serialNbr, elementAssignedToTask.IdDepositoryElement,
                loggedOperator.DepositoryLocation, externalSN, true, notes, dataProvider.GetDeviceStateType((int)OpDeviceStateType.Enum.REMOVED));

            if (article.IsAiut && serialNbr.HasValue)
            {//zmiana statusu urzadzenia
                device.DeviceStateType = dataProvider.DeviceStateType.Find(d => d.IdDeviceStateType == (int)Enums.DeviceState.REMOVED);
                device.IdDeviceStateType = (int)Enums.DeviceState.REMOVED;
                DeviceComponent.Save(dataProvider, device);
            }
            #region DataLatch
            TaskComponent.TaskProceedDepositoryStepLatch(dataProvider, task, article.IdArticle, serialNbr, externalSN, false, timeStamp);
            #endregion
        }

        #endregion

        #region InstalledDeviceAdd

        public static void InstalledDeviceAdd(DataProvider dataProvider, OpOperator loggedOperator, OpOperator fitterOperator, OpTask task, long serialNbr, out OpDepositoryElement elementAssignedToTask, DateTime? timeStamp = null)
        {
            elementAssignedToTask = null;
            List<OpDepositoryElement> fitterDepositoryContent = DepositoryComponent.GetSOTDepositoryAvailableElements(dataProvider, fitterOperator);
            if (fitterDepositoryContent.Exists(d => d.SerialNbr == serialNbr))
            {
                InstalledDeviceAdd(dataProvider, loggedOperator, fitterOperator, task, fitterDepositoryContent.Find(d => d.SerialNbr == serialNbr), out elementAssignedToTask, timeStamp);
            }
            else
                throw new Exception(ResourcesText.FitterDepositoryDoesNotContaintSuchElement);
        }

        public static void InstalledDeviceAdd(DataProvider dataProvider, OpOperator loggedOperator, OpOperator fitterOperator, OpTask task, string externalSn, out OpDepositoryElement elementAssignedToTask, DateTime? timeStamp = null)
        {
            elementAssignedToTask = null;
            if (String.IsNullOrEmpty(externalSn))
                throw new Exception(ResourcesText.ExternalSerialNbrCannotBeEmpty);
            List<OpDepositoryElement> fitterDepositoryContent = DepositoryComponent.GetSOTDepositoryAvailableElements(dataProvider, fitterOperator);
            if (fitterDepositoryContent.Exists(d => String.Equals(d.ExternalSn, externalSn)))
            {
                InstalledDeviceAdd(dataProvider, loggedOperator, fitterOperator, task, fitterDepositoryContent.Find(d => String.Equals(d.ExternalSn, externalSn)), out elementAssignedToTask, timeStamp);
            }
            else
                throw new Exception(ResourcesText.FitterDepositoryDoesNotContaintSuchElement);
        }

        public static void InstalledDeviceAdd(DataProvider dataProvider, OpOperator loggedOperator, OpOperator fitterOperator, OpTask task, OpArticle article, out OpDepositoryElement elementAssignedToTask, DateTime? timeStamp = null)
        {
            elementAssignedToTask = null;
            if (article.IsUnique)
                throw new Exception(ResourcesText.IdentifyBySerialNbrArticle);
            List<OpDepositoryElement> fitterDepositoryContent = DepositoryComponent.GetSOTDepositoryAvailableElements(dataProvider, fitterOperator);
            if (fitterDepositoryContent.Exists(d => d.IdArticle == article.IdArticle))
            {
                InstalledDeviceAdd(dataProvider, loggedOperator, fitterOperator, task, fitterDepositoryContent.Find(d => d.IdArticle == article.IdArticle), out elementAssignedToTask, timeStamp);
            }
            else
                throw new Exception(ResourcesText.FitterDepositoryDoesNotContaintSuchElement);
        }

        public static void InstalledDeviceAdd(DataProvider dataProvider, OpOperator loggedOperator, OpOperator fitterOperator, OpTask task, OpDepositoryElement depositoryElement, out OpDepositoryElement elementAssignedToTask, DateTime? timeStamp = null)
        {
            OpDistributor distributor = dataProvider.GetDistributor(task.Location.IdDistributor);
            /*
            OpLocation fitterDepository = fitterOperator.DepositoryLocation;
            if (fitterDepository == null)
            {
                OpOperatorData fitterDepositoryId = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, IdOperator: new int[] { fitterOperator.IdOperator }, IdDataType: new long[] { DataType.DEPOSITORY_ID }, customWhereClause: "[VALUE] is not null").FirstOrDefault();
                long idLocation = 0;
                if (fitterDepositoryId != null && fitterDepositoryId.Value != null && long.TryParse(fitterDepositoryId.Value.ToString(), out idLocation))
                {
                    if (idLocation > 0)
                        fitterDepository = dataProvider.GetLocation(idLocation);
                }
            }
            if (fitterDepository == null)
                throw new Exception(ResourcesText.FitterDepositoryNotDefined);
            if (depositoryElement.IdLocation != fitterDepository.IdLocation
                || depositoryElement.EndDate.HasValue)
                throw new Exception(ResourcesText.ElementDoesNotBelongToFitterDepository);
            */
            //OpDepositoryElement element = model.DepositoryElements.Find(c => c.IdDepositoryElement == id_depository_element);

            elementAssignedToTask = DepositoryComponent.AssignElementToTask(
                dataProvider, loggedOperator, depositoryElement.IdArticle, depositoryElement.SerialNbr, depositoryElement.IdDepositoryElement, task, depositoryElement.ExternalSn, false, false, depositoryElement.Notes);

            #region Zdjęcie z puli serwisowej
            if (distributor != null && distributor.EquipmentLocationId.HasValue)
            {
                OpLocation equipmentPool = dataProvider.GetLocation(distributor.EquipmentLocationId.Value);
                if (equipmentPool != null)
                {
                    if (depositoryElement.Article.IsUnique)
                    {
                        if (depositoryElement.SerialNbr.HasValue)
                        {
                            List<OpDepositoryElement> devices_in_pool = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[] { depositoryElement.SerialNbr.Value }, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null());
                            foreach (OpDepositoryElement equipmentToRemove in devices_in_pool)
                            {
                                DepositoryComponent.RemoveDepositoryElement(dataProvider, equipmentToRemove);
                            }
                        }
                        else if (!String.IsNullOrEmpty(depositoryElement.ExternalSn))
                        {
                            OpDepositoryElement deviceToRemove = dataProvider.GetDepositoryElementFilter(ExternalSn: depositoryElement.ExternalSn, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault();
                            if (deviceToRemove != null)
                                DepositoryComponent.RemoveDepositoryElement(dataProvider, deviceToRemove);
                        }
                    }
                    else
                    {
                        OpDepositoryElement elementToRemove = dataProvider.GetDepositoryElementFilter(IdArticle: new long[] { depositoryElement.IdArticle }, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault(); ;
                        if (elementToRemove != null)
                        {
                            DepositoryComponent.RemoveDepositoryElement(dataProvider, elementToRemove);
                        }
                    }
                }
            }
            #endregion
            DepositoryComponent.RemoveDepositoryElement(dataProvider, depositoryElement);
            //model.DepositoryElements.Remove(element);
            #region DataLatch
            TaskComponent.TaskProceedDepositoryStepLatch(dataProvider, task, depositoryElement.IdArticle, depositoryElement.SerialNbr, depositoryElement.ExternalSn, true, timeStamp);
            #endregion
        }

        #endregion

        #region GetTaskDistributorNotificationOperators

        public static List<OpOperator> GetTaskDistributorNotificationOperators(DataProvider dataProvider, OpTask task, List<EmailComponent.OperationType> operationTypesToCheck)
        {
            List<OpOperator> mailingList = new List<OpOperator>();
            string customWhereClause = "[VALUE] is not null";
            customWhereClause += " AND (";
            customWhereClause += "(cast([VALUE] as nvarchar) not like '%,%' AND cast([VALUE] as nvarchar) like '" + task.IdDistributor + "')";
            customWhereClause += " OR";
            customWhereClause += "(CAST(VALUE as nvarchar) like '%,%' and (CAST(VALUE as nvarchar) like '%," + task.IdDistributor + ",%' OR CAST(VALUE as nvarchar) like '%," + task.IdDistributor + "' OR (CAST(VALUE as nvarchar) like '" + task.IdDistributor + ",%')))";
            customWhereClause += ")";
            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, IdDataType: new long[] { DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_TASK_ID_DISTRIBUTOR }, customWhereClause: customWhereClause);
            if (odList != null && odList.Count > 0)
            {
                List<OpOperator> oList = dataProvider.GetOperator(odList.Select(d => d.IdOperator).Distinct().ToArray());
                foreach (OpOperator oItem in oList)
                {
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Add)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Add))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Change)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Change))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Delete)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Delete))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.ChangeStatus)
                        && oItem.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.IdTaskStatus)))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                }
            }
            mailingList.RemoveAll(o => o.Actor == null);
            mailingList.RemoveAll(o => String.IsNullOrEmpty(o.Actor.Email));
            mailingList = mailingList.Distinct().ToList();
            return mailingList;
        }

        #endregion

        #region GetTaskDistributorNotificationOperators

        public static List<OpOperator> GetTaskDistributorNotificationOperators(DataProvider dataProvider, OpTask task, List<SmsComponent.OperationType> operationTypesToCheck)
        {
            List<OpOperator> mailingList = new List<OpOperator>();
            string customWhereClause = "[VALUE] is not null";
            customWhereClause += " AND (";
            customWhereClause += "(cast([VALUE] as nvarchar) not like '%,%' AND cast([VALUE] as nvarchar) like '" + task.IdDistributor + "')";
            customWhereClause += " OR";
            customWhereClause += "(CAST(VALUE as nvarchar) like '%,%' and (CAST(VALUE as nvarchar) like '%," + task.IdDistributor + ",%' OR CAST(VALUE as nvarchar) like '%," + task.IdDistributor + "' OR (CAST(VALUE as nvarchar) like '" + task.IdDistributor + ",%')))";
            customWhereClause += ")";
            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, IdDataType: new long[] { DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_TASK_ID_DISTRIBUTOR }, customWhereClause: customWhereClause);
            if (odList != null && odList.Count > 0)
            {
                List<OpOperator> oList = dataProvider.GetOperator(odList.Select(d => d.IdOperator).Distinct().ToArray());
                foreach (OpOperator oItem in oList)
                {
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Add)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Add))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Change)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Change))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Delete)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Delete))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.ChangeStatus)
                        && oItem.SmsNotificationOptions.GetNotify(Business.Objects.SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.IdTaskStatus)))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                }
            }
            mailingList.RemoveAll(o => !o.SerialNbr.HasValue || o.DataList.FirstOrDefault(dt => dt.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) == null);
            mailingList = mailingList.Distinct().ToList();
            return mailingList;
        }

        #endregion

        #region SetHelperValues
        public static void SetHelperValues(DataProvider dataProvider, OpTask task, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdTask, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_TASK:
                        task.DataList.SetValue(DataType.HELPER_ID_TASK, 0, task.IdTask);
                        break;
                    case DataType.HELPER_ID_TASK_TYPE:
                        task.DataList.SetValue(DataType.HELPER_ID_TASK_TYPE, 0, task.IdTaskType);
                        break;
                    case DataType.HELPER_ID_TASK_STATUS:
                        task.DataList.SetValue(DataType.HELPER_ID_TASK_STATUS, 0, task.IdTaskStatus);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        task.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, task.IdDistributor);
                        break;
                    case DataType.HELPER_ID_TASK_GROUP:
                        task.DataList.SetValue(DataType.HELPER_ID_TASK_GROUP, 0, task.IdTaskGroup);
                        break;
                    case DataType.HELPER_ID_PLANNED_ROUTE:
                        task.DataList.SetValue(DataType.HELPER_ID_PLANNED_ROUTE, 0, task.IdPlannedRoute);
                        break;
                    case DataType.HELPER_ID_OPERATOR_REGISTERING:
                        task.DataList.SetValue(DataType.HELPER_ID_OPERATOR_REGISTERING, 0, task.IdOperatorRegistering);
                        break;
                    case DataType.HELPER_ID_OPERATOR_PERFORMER:
                        task.DataList.SetValue(DataType.HELPER_ID_OPERATOR_PERFORMER, 0, task.IdOperatorPerformer);
                        break;
                    case DataType.HELPER_CREATION_DATE:
                        task.DataList.SetValue(DataType.HELPER_CREATION_DATE, 0, task.CreationDate);
                        break;
                    case DataType.HELPER_ACCEPTED:
                        task.DataList.SetValue(DataType.HELPER_ACCEPTED, 0, task.IdOperatorAccepted);
                        break;
                    case DataType.HELPER_ACCEPTANCE_DATE:
                        task.DataList.SetValue(DataType.HELPER_ACCEPTANCE_DATE, 0, task.AcceptanceDate);
                        break;
                    case DataType.HELPER_NOTES:
                        task.DataList.SetValue(DataType.HELPER_NOTES, 0, task.Notes);
                        break;
                    case DataType.HELPER_DEADLINE:
                        task.DataList.SetValue(DataType.HELPER_DEADLINE, 0, task.Deadline);
                        break;
                    case DataType.HELPER_TOPIC_NUMBER:
                        task.DataList.SetValue(DataType.HELPER_TOPIC_NUMBER, 0, task.TopicNumber);
                        break;
                    case DataType.HELPER_PRIORITY:
                        task.DataList.SetValue(DataType.HELPER_PRIORITY, 0, task.Priority);
                        break;
                    case DataType.HELPER_OPERATION_CODE:
                        task.DataList.SetValue(DataType.HELPER_OPERATION_CODE, 0, task.OperationCode);
                        break;
                    case DataType.HELPER_ID_ISSUE:
                        task.DataList.SetValue(DataType.HELPER_ID_ISSUE, 0, task.IdIssue);
                        break;
                    case DataType.HELPER_ID_OPERATOR_ACCEPTED:
                        task.DataList.SetValue(DataType.HELPER_ID_OPERATOR_ACCEPTED, 0, task.IdOperatorAccepted);
                        break;
                    case DataType.HELPER_ID_LOCATION:
                        task.DataList.SetValue(DataType.HELPER_ID_LOCATION, 0, task.IdLocation);
                        break;
                    case DataType.HELPER_ID_ACTOR:
                        task.DataList.SetValue(DataType.HELPER_ID_ACTOR, 0, task.IdActor);
                        break;
                    default:
                        break;
                }
            }
            task.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }
        #endregion

        #region ProceedTaskStatusChangeOperation

        private static void ProceedTaskStatusChangeOperation(DataProvider dataProvider, OpOperator loggedOperator, int? prevTaskStatus, int? nextTaskStatus, OpLocation taskLocation)
        {
            if (prevTaskStatus == null)
            {
                prevTaskStatus = (int)OpTaskStatus.Enum.Not_Started;
            }

            if (loggedOperator != null && loggedOperator.Distributor != null && loggedOperator.Distributor.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT, DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV })))
            {
                #region DataBaseConfiguration

                Tuple<OpDistributorData, OpDistributorData> flowPair =
                    loggedOperator.Distributor.DataList.Where(x => x.IdDataType == DataType.DISTRIBUTOR_TASK_STATUS_FLOW_PREV && Convert.ToInt32(x.Value) == prevTaskStatus)
                                                       .Join(loggedOperator.Distributor.DataList.Where(x => x.IdDataType == DataType.DISTRIBUTOR_TASK_STATUS_FLOW_NEXT && Convert.ToInt32(x.Value) == nextTaskStatus),
                                                             o => o.Index,
                                                             o => o.Index,
                                                             (a, b) => Tuple.Create(a, b))
                                                       .FirstOrDefault(x => (x.Item1.Value == null ? null : (int?)Convert.ToInt32(x.Item1.Value)) == prevTaskStatus && (x.Item2 == null ? null : (int?)Convert.ToInt32(x.Item2.Value)) == nextTaskStatus);

                if (flowPair != default(Tuple<OpDistributorData, OpDistributorData>))
                {
                    #region ChangeLocationState
                    OpDistributorData ddLocationStateType = loggedOperator.Distributor.DataList.FirstOrDefault(x => x.IdDataType == DataType.DITRIBUTOR_TASK_STATUS_CHANGE_ID_LOCATION_STATE_TYPE && x.Index == flowPair.Item1.Index);
                    if (ddLocationStateType != null && ddLocationStateType.Value != null)
                    {
                        int idNewLocationStateType = 0;
                        if (int.TryParse(ddLocationStateType.Value.ToString(), out idNewLocationStateType) && idNewLocationStateType > 0)
                        {
                            taskLocation.LocationStateType = dataProvider.GetLocationStateType(idNewLocationStateType);

                            LocationComponent.Save(dataProvider, taskLocation, loggedOperator);
                            LocationComponent.AddStateHistory(dataProvider, taskLocation, idNewLocationStateType, string.Empty);
                            LocationComponent.ChangeLocationStateTelemetricServer(dataProvider, taskLocation);
                        }
                    }
                    #endregion
                }

                #endregion
            }
        }

        #endregion

        #endregion

        #region GUIObjectClass

        public class GOServiceOption : IComparable, IEquatable<GOServiceOption>
        {
            public int IdServiceOption { get; private set; }
            public string ServiceOptionName { get; private set; }

            public GOServiceOption(int IdServiceOption, string ServiceOptionName)
            {
                this.IdServiceOption = IdServiceOption;
                this.ServiceOptionName = ServiceOptionName;
            }

            public override string ToString()
            {
                return this.ServiceOptionName;
            }

            public override bool Equals(object obj)
            {
                if (obj is GOServiceOption)
                {
                    return (obj as GOServiceOption).IdServiceOption == this.IdServiceOption;
                }
                else
                    return false;
            }

            public int CompareTo(object obj)
            {
                if (obj is GOServiceOption)
                {
                    return (obj as GOServiceOption).IdServiceOption == this.IdServiceOption ? 0 : -1;
                }
                else
                    return -1;
            }

            public bool Equals(GOServiceOption other)
            {
                if (other == null)
                    return false;
                return this.IdServiceOption.Equals(other.IdServiceOption);
            }

            public static bool operator ==(GOServiceOption a, GOServiceOption b)
            {
                // If both are null, or both are same instance, return true.
                if (System.Object.ReferenceEquals(a, b))
                {
                    return true;
                }

                // If one is null, but not both, return false.
                if (((object)a == null) || ((object)b == null))
                {
                    return false;
                }

                // Return true if the fields match:
                return a.IdServiceOption == b.IdServiceOption;
            }

            public static bool operator !=(GOServiceOption a, GOServiceOption b)
            {
                // If both are null, or both are same instance, return true.
                if (System.Object.ReferenceEquals(a, b))
                {
                    return false;
                }

                // If one is null, but not both, return false.
                if (((object)a == null) || ((object)b == null))
                {
                    return true;
                }

                // Return true if the fields match:
                return a.IdServiceOption != b.IdServiceOption;
            }
        }

        public class GOObjectServiceDefaultPayment
        {
            public int Index { get; set; }
            public OpTaskType TaskType { get; private set; }
            public GOServiceOption ServiceOption { get; private set; }
            public GOPaymentType TaskPaymentType { get; private set; }
            public GOPaymentPartType TaskPaymentPartType { get; private set; }
            public OpArticle Article { get; private set; }
            public string ServiceOptionStr
            {
                get
                {
                    if (ServiceOption != null)
                        return ServiceOption.ToString();
                    return null;
                }
            }
            public DateTime From { get; set; }
            public DateTime? To { get; set; }
            public double Amount { get; set; }
            public DateTime? CanelationDate { get; set; }
            public OpTaskGroup TaskGroup { get; set; }

            public GOObjectServiceDefaultPayment(OpTaskType TaskType, GOServiceOption ServiceOption,
                                          Enums.TaskPaymentType TaskPaymentType,
                                          Enums.TaskPaymentPartType TaskPaymentPartType,
                                          OpArticle Article,
                                          DateTime From, DateTime? To, DateTime? CanelationDate,
                                          double Amount,
                                          OpTaskGroup taskGroup = null)
            {
                this.TaskType = TaskType;
                this.ServiceOption = ServiceOption;
                this.TaskPaymentType = new GOPaymentType(TaskPaymentType);
                this.TaskPaymentPartType = new GOPaymentPartType(TaskPaymentPartType);
                this.Article = Article;
                this.From = From;
                this.To = To;
                this.CanelationDate = CanelationDate;
                this.Amount = Amount;
                this.TaskGroup = taskGroup;
            }
        }

        public class GOPaymentType
        {
            public Enums.TaskPaymentType PaymentType { get; private set; }
            public string Description { get; private set; }

            public GOPaymentType(Enums.TaskPaymentType PaymentType)
            {
                this.PaymentType = PaymentType;
                if (this.PaymentType == Enums.TaskPaymentType.Fitter)
                    this.Description = ResourcesText.Fitter;
                else if (this.PaymentType == Enums.TaskPaymentType.Client)
                    this.Description = ResourcesText.Client;
            }

            public override string ToString()
            {
                return this.Description;
            }
        }

        public class GOPaymentPartType
        {
            public Enums.TaskPaymentPartType PaymentPartType { get; private set; }
            public string Description { get { return TaskComponent.GetTaskPaymentTypeName(this.PaymentPartType); } }

            public GOPaymentPartType(Enums.TaskPaymentPartType PaymentPartType)
            {
                this.PaymentPartType = PaymentPartType;
            }

            public override string ToString()
            {
                return this.Description;
            }
        }

        #endregion

        #region GetTaskSuspensionReasonText

        public static string GetTaskSuspensionReasonText(int idTaskSuspensionReason)
        {
            string ret = "";

            switch (idTaskSuspensionReason)
            {
                case (int)Enums.TaskSuspensionReason.AiutFault:
                    ret = ResourcesText.AiutFault;
                    break;
                case (int)Enums.TaskSuspensionReason.ClientFault:
                    ret = ResourcesText.ClientFault;
                    break;
                default:
                    ret = "";
                    break;
            }

            return ret;
        }

        #endregion
    }
}
