﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class TaskGroupComponent : BaseComponent
    {
        #region Methods
        public static OpTaskGroup GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpTaskGroup GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetTaskGroup(Id, queryDatabase);
        }

        public static List<OpTaskGroup> GetByParentID(DataProvider dataProvider, int? parentId, List<OpTaskGroup> taskGroupList = null)
        {
            //if (!parentId.HasValue)
            //    return GetAll(dataProvider).Where(tg => !tg.IdParrentGroup.HasValue).ToList();

            //return dataProvider.GetAllTaskGroup().Where(tg => tg.IdParrentGroup == parentId).ToList();
            if (taskGroupList == null)
            {
                if (!parentId.HasValue)
                    return dataProvider.GetTaskGroupFilter(customWhereClause: "[ID_PARRENT_GROUP] is null");

                return dataProvider.GetTaskGroupFilter(customWhereClause: "[ID_PARRENT_GROUP] = " + parentId.Value);
            }
            else
            {
                if (!parentId.HasValue)
                    return taskGroupList.Where(g => !g.IdParrentGroup.HasValue).ToList();
                return taskGroupList.Where(g => g.IdParrentGroup == parentId.Value).ToList();
            }
        }

        public static List<OpTaskGroup> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllTaskGroup();
        }

        public static OpTaskGroup Save(DataProvider dataProvider, OpTaskGroup objectToSave)
        {
            bool isNew = objectToSave.IdTaskGroup == 0;
            try
            {
                dataProvider.SaveTaskGroup(objectToSave);
                if (LogCustomEvents)
                {
                    if (isNew)
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdTaskGroup);
                        LogSuccess(EventID.Forms.TaskGroupAdded, objectToSave.IdTaskGroup);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdTaskGroup);
                        LogSuccess(EventID.Forms.TaskGroupSaved, objectToSave.IdTaskGroup);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNew)
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.TaskGroupAdditionError, ex.Message);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdTaskGroup, ex.Message);
                        LogError(EventID.Forms.TaskGroupSavingError, objectToSave.IdTaskGroup, ex.Message);
                }
                throw ex;
            }

            return dataProvider.GetTaskGroup(objectToSave.IdTaskGroup);
        }

        public static void Delete(DataProvider dataProvider, OpTaskGroup objectToDelete)
        {
            try
            {
                dataProvider.DeleteTaskGroup(objectToDelete);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdTaskGroup);
                    LogSuccess(EventID.Forms.TaskGroupDeleted, objectToDelete.IdTaskGroup);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskGroupDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdTaskGroup, ex.Message);
                    LogError(EventID.Forms.TaskGroupDeletionError, objectToDelete.IdTaskGroup, ex.Message);
                throw ex;
            }
        }
        #endregion
    }
}
