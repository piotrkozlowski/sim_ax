﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Data.DB;
using IMR.Suite.Services.Common.UTDServiceReference;
using IMR.Suite.Services.Common.Components;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DeviceComponent
    {
        #region enum DeviceImportStatus
        public enum DeviceImportStatus
        {
            Failed,
            InvalidSN,
            DeviceNotFound,
            ConnectionFailed,
            Success
        }
        #endregion

        #region Methods
        public static OpDevice GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpDevice GetByID(DataProvider dataProvider, long Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetDevice(Id, queryDatabase);
        }

        public static List<OpDevice> GetAll(DataProvider dataProvider, bool? exludeDeleted = null, bool? exludePattern = null, bool queryDatabase = false)
        {
            List<OpDevice> devicesList = new List<OpDevice>();

            if (queryDatabase)
                devicesList = dataProvider.GetDeviceFilter(loadNavigationProperties: true, loadCustomData: true, mergeIntoCache: true, autoTransaction: false, SerialNbr: null);
            else
                devicesList = dataProvider.GetAllDevice();

            if (exludeDeleted.HasValue && exludeDeleted.Value) //usuwamy zezlomowane
                devicesList.RemoveAll(o => o.IdDeviceStateType == (int)Enums.DeviceState.ERASED);

            if (exludePattern.HasValue && exludePattern.Value) //usuwamy wzorce
                devicesList.RemoveAll(d => d.SerialNbrPattern.HasValue && d.SerialNbr == d.SerialNbrPattern);

            return devicesList;
        }

        public static DeviceImportStatus Import(DataProvider dataProvider, DBCommonUsok usokConnection, string serialNbrString, out OpDevice device)
        {
            long tmpSerialNumber = 0;
            long.TryParse(serialNbrString, out tmpSerialNumber); //probujemy sparsowac SN na long
            device = null;

            if (serialNbrString.Length > 0)
            {
               // long radioAddress; string name; string firstQuarter; string deviceTypeName;
               // string secondQuarter; string thirdQuarter; int idDeviceState; int idCustomer; int idDeviceType;

                //jeśli numer seryjny jest adresem radiowym
                if (serialNbrString.Length <= 8)
                {
                    try
                    {
                        device = dataProvider.GetDevice(tmpSerialNumber, true); //szukamy w bazie
                        if (device != null)
                            return DeviceImportStatus.Success;
                    }
                    catch
                    {
                    }
                }
                //jeśli numer seryjny jest dłuższy od 8 znaków
                else if (serialNbrString.Length > 8 && dataProvider.GetAllDevice().FindIndex(d => d.FactoryNumber != null && d.FactoryNumber.Equals(serialNbrString)) != -1)
                {
                    device = dataProvider.GetAllDevice().FirstOrDefault(d => d.FactoryNumber != null && d.FactoryNumber.Equals(serialNbrString));
                    if (device != null)
                        return DeviceImportStatus.Success;
                }
                return DeviceImportStatus.DeviceNotFound;
                /*
                if (usokConnection != null)
                {
                   // if (usokConnection.CheckIfDeviceExists(serialNbrString, out radioAddress, out name, out firstQuarter,
                                    out secondQuarter, out thirdQuarter, out  idDeviceState, out  idCustomer, out idDeviceType, out deviceTypeName))
                    {
                        //urzadzenie znalezione na bazie usok
                        device = new OpDevice();
                        device.DeviceOrderNumber = GetCreateDeviceOrderNuber(dataProvider, String.Format("{0} {1}-{2}-{3}", name, firstQuarter, secondQuarter, thirdQuarter));
                        device.DeviceStateType = dataProvider.GetDeviceStateType(idDeviceState);

                        if (idDeviceType > 0)
                        {
                            object tmp = dataProvider.GetAllDeviceType(); //pobieramy dane do cache jeśli nie sa pobrane
                            if (dataProvider.DeviceTypeDict != null && !dataProvider.DeviceTypeDict.ContainsKey(idDeviceType)) //jesli typ urzadzenia nie odnaleziony
                            {
                                OpDeviceType deviceType = new OpDeviceType()
                                {
                                    IdDeviceType = idDeviceType,
                                    Name = deviceTypeName,
                                    TwoWayTransAvailable = false
                                };
                                if (name.Contains("OKO"))
                                    deviceType.DeviceTypeClass = dataProvider.GetDeviceTypeClass((int)OpDeviceTypeClass.Enum.OKO);
                                else
                                    deviceType.DeviceTypeClass = dataProvider.GetDeviceTypeClass((int)OpDeviceTypeClass.Enum.RADIO_DEVICE);

                                dataProvider.AddDeviceType(deviceType);
                            }
                            device.DeviceType = dataProvider.GetDeviceType(idDeviceType);
                        }
                        else
                            device.DeviceType = dataProvider.GetDeviceType(10000); //unknown DeviceType

                        if (idCustomer > 0)
                        {
                            object tmp = dataProvider.GetAllDistributor(); //pobieramy dane do cache jeśli nie sa pobrane
                            if (dataProvider.DistributorDict != null && !dataProvider.DistributorDict.ContainsKey(idCustomer)) //jesli dystrybutor nie odnaleziony
                            {
                            }
                            device.Distributor = dataProvider.GetDistributor(idCustomer);
                        }
                        else
                            device.Distributor = dataProvider.GetDistributor(1); //system owner

                        device.SerialNbr = radioAddress;
                        device.FactoryNumber = serialNbrString;
                        Add(dataProvider, device);

                        device = dataProvider.GetDevice(radioAddress);
                        return DeviceImportStatus.Success;
                    }
                    else
                        return DeviceImportStatus.DeviceNotFound;
                }
                else
                    return DeviceImportStatus.ConnectionFailed;
                */
            }
            else
                return DeviceImportStatus.InvalidSN;
        }

        internal static void Add(DataProvider dataProvider, OpDevice objectToSave, bool useDBCollector = false, bool loadNavigationProperties = true)
        {
            dataProvider.AddDevice(objectToSave, useDBCollector);
            SaveDeviceData(dataProvider, objectToSave, useDBCollector: useDBCollector, loadNavigationProperties: loadNavigationProperties);

            //wpis do tabeli DEVICE_STATE_HISTORY jeśli zmienił sie status
            AddStateHistory(dataProvider, objectToSave, useDBCollector: useDBCollector);
        }

        public static OpDevice Save(DataProvider dataProvider, OpDevice objectToSave, string notes = null, bool useDBCollector = false, bool loadNavigationProperties = true)
        {
            bool isNewItem = objectToSave.OpState == OpChangeState.New; //check if it's new item

            //zapis do tabeli DEVICE            
            if (isNewItem)
                Add(dataProvider, objectToSave, useDBCollector);
            else
            {
                dataProvider.SaveDevice(objectToSave, useDBCollector);
                SaveDeviceData(dataProvider, objectToSave, useDBCollector: useDBCollector, loadNavigationProperties: loadNavigationProperties);

                //wpis do tabeli DEVICE_STATE_HISTORY jeśli zmienił sie status
                AddStateHistory(dataProvider, objectToSave, notes, useDBCollector);
            }

            objectToSave.OpState = OpChangeState.Loaded;
            return dataProvider.GetDevice(objectToSave.SerialNbr);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void SaveDeviceData(DataProvider dataProvider, OpDevice objectToSave, bool useDBCollector = false, bool loadNavigationProperties = true)
        {
            //zapis do tabeli DATA
            OpData data = null;
            for (int i = objectToSave.DataList.Count - 1; i >= 0; i--)
            {
                data = objectToSave.DataList[i];
                data.SerialNbr = objectToSave.SerialNbr;

                if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                {
                    DataComponent.Delete(dataProvider, data, useDBCollector);
                    objectToSave.DataList.Remove(data, true);
                }
                else if (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New)
                {
                    objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data, useDBCollector);

                    if (objectToSave.DataList[i].OpState == OpChangeState.New)
                    {
                        if(loadNavigationProperties)
                            objectToSave.DataList[i].AssignReferences(dataProvider);
                    }

                    objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                }
            }
        }

        public static void AddStateHistory(DataProvider dataProvider, OpDevice objectToSave, string notes = null, bool useDBCollector = false)
        {
            OpDeviceStateType currentDeviceStateType = objectToSave.DeviceStateType;
            List<OpDeviceStateHistory> history = GetStateHistory(dataProvider, objectToSave.SerialNbr, useDBCollector: useDBCollector);
            objectToSave.DeviceStateType = currentDeviceStateType;
            if (history.Count > 0)
            {
                OpDeviceStateHistory lastHistoryItem = history.FirstOrDefault(d => !d.EndTime.HasValue);
                if (lastHistoryItem == null)
                    lastHistoryItem = history.FirstOrDefault(d => d.EndTime == history.Max(e => e.EndTime));
                if (lastHistoryItem.IdDeviceStateType != objectToSave.IdDeviceStateType) //jeśli zmienił sie status
                {
                    if (!lastHistoryItem.EndTime.HasValue)
                    {
                        lastHistoryItem.EndTime = dataProvider.DateTimeNow; //ustaw dla ostatniego wpisu date końca
                    }
                    dataProvider.SaveDeviceStateHistory(lastHistoryItem, useDBCollector: useDBCollector);
                }
                else
                    return;
            }

            //jeśli nie bylo zadnego wpisu lub status sie zmienil dodaj kolejny wpis
            OpDeviceStateHistory insert = new OpDeviceStateHistory();
            insert.SerialNbr = objectToSave.SerialNbr;
            insert.StartTime = dataProvider.DateTimeNow;
            insert.EndTime = null;
            insert.DeviceStateType = objectToSave.DeviceStateType;
            insert.Notes = notes;

            dataProvider.SaveDeviceStateHistory(insert, useDBCollector: useDBCollector);
        }

        public static void AddStateHistory(DataProvider dataProvider, OpDevice device, List<OpDeviceStateHistory> stateHistory, int newIdDeviceStateType, string notes, bool saveToDataBase = false, bool useDBCollector = false)
        {
            if (stateHistory != null && stateHistory.Count > 0)
            {
                OpDeviceStateHistory lastHistoryItem = stateHistory.FirstOrDefault(d => !d.EndTime.HasValue);
                if (lastHistoryItem == null)
                    lastHistoryItem = stateHistory.FirstOrDefault(d => d.EndTime == stateHistory.Max(e => e.EndTime));
                if (lastHistoryItem.IdDeviceStateType != newIdDeviceStateType) //jeśli zmienił sie status
                {
                    if (!lastHistoryItem.EndTime.HasValue)
                    {
                        lastHistoryItem.EndTime = dataProvider.DateTimeNow; //ustaw dla ostatniego wpisu date końca
                    }
                    //dataProvider.SaveDeviceStateHistory(lastHistoryItem, useDBCollector: useDBCollector);//ma być zapisane tylko jeśli saveToDataBase = true i sprawi to lastHistoryItem.EndTime = dataProvider.DateTimeNow; bo ustawi flagę OpState na Modified
                }
                else
                {
                    return;
                } 
            }

            OpDeviceStateHistory newState = new OpDeviceStateHistory()
            {
                SerialNbr = device.SerialNbr,
                IdDeviceStateType = newIdDeviceStateType,
                StartTime = dataProvider.DateTimeNow,
                Notes = notes,
                OpState = OpChangeState.New,
            };
            newState.AssignReferences(dataProvider);
            stateHistory.Add(newState);

            if (saveToDataBase)
                DeviceStateHistoryComponent.Save(dataProvider, stateHistory, useDBCollector: useDBCollector);
        }


        public static void AddDistributorHistory(DataProvider dataProvider, long serialNbr, 
                                                 int newDistributor, int? newOwner, OpOperator loggedOperator, string notes,
                                                 int? idServiceReferenceType = null, object referenceValue = null)
        {
            List<OpDeviceDistributorHistory> distributorHistory = dataProvider.GetDeviceDistributorHistoryFilter(SerialNbr: new long[] { serialNbr }, IdDistributor: new int[] { });
            OpDeviceDistributorHistory lastHitory = distributorHistory.LastOrDefault(w => w.EndTime == null);

            bool valueChanged = false;
            if (lastHitory != null)
            {
                if (lastHitory.IdServiceReferenceType == idServiceReferenceType)
                {
                    if (idServiceReferenceType == (int)Enums.ServiceReferenceType.IdServiceList
                        || idServiceReferenceType == (int)Enums.ServiceReferenceType.IdShippingList
                        || idServiceReferenceType == (int)Enums.ServiceReferenceType.IdImrServer)
                    {
                        if (Convert.ToInt64(referenceValue) != Convert.ToInt64(lastHitory.ReferenceValue))
                            valueChanged = true;
                    }
                }
            }

            if (lastHitory == null || (lastHitory != null && (newDistributor != lastHitory.IdDistributor
                                                             || newOwner != lastHitory.IdDistributorOwner
                                                             || idServiceReferenceType != lastHitory.IdServiceReferenceType
                                                             || valueChanged)))
            {
                if (lastHitory != null)
                    lastHitory.EndTime = dataProvider.DateTimeNow;

                OpDeviceDistributorHistory newHistory = new OpDeviceDistributorHistory()
                {
                    SerialNbr = serialNbr,
                    IdDistributor = newDistributor,
                    IdDistributorOwner = newOwner,
                    StartTime = lastHitory == null ? dataProvider.DateTimeNow : lastHitory.EndTime.Value,
                    Notes = notes,
                    Operator = loggedOperator,
                    IdServiceReferenceType = idServiceReferenceType,
                    ReferenceValue = referenceValue
                };
                if (lastHitory != null)
                    dataProvider.SaveDeviceDistributorHistory(lastHitory);
                dataProvider.SaveDeviceDistributorHistory(newHistory);

                distributorHistory.Add(newHistory);
            }
        }

        public static void AddSimCardHistory(DataProvider dataProvider, long deviceSerialNbr, string phone, DBCommonRDT RadioDeviceTesterNewConnection, OpOperator loggedOperator, DateTime? startTime = null)
        {
            OpSimCard simCard = null;
            SimCardComponent.SaveSimCardFromProductionDataBase(dataProvider, phone, RadioDeviceTesterNewConnection, loggedOperator);
            AddSimCardHistory(dataProvider, deviceSerialNbr, simCard != null ? (int?)simCard.IdSimCard : null, startTime);
        }

        public static void AddSimCardHistory(DataProvider dataProvider, long serialNbr, int? idSimCard, DateTime? startTime = null)
        {
            OpDeviceSimCardHistory lastHistory = dataProvider.GetDeviceSimCardHistoryFilter(customWhereClause: "[SERIAL_NBR] = " + serialNbr).LastOrDefault(s => !s.EndTime.HasValue);
            bool createNewHistory = true;

            if (lastHistory != null)
            {
                if (idSimCard != lastHistory.IdSimCard)
                {
                    lastHistory.EndTime = startTime.HasValue ? startTime.Value : dataProvider.DateTimeNow;
                    dataProvider.SaveDeviceSimCardHistory(lastHistory);
                }
                else
                    createNewHistory = false;
            }
            if (createNewHistory)
            {
                OpDeviceSimCardHistory newHistory = new OpDeviceSimCardHistory();
                newHistory.SerialNbr = serialNbr;
                newHistory.IdSimCard = idSimCard;
                newHistory.StartTime = startTime.HasValue ? startTime.Value : dataProvider.DateTimeNow;
                dataProvider.SaveDeviceSimCardHistory(newHistory);
            }
        }

        public static bool CreateSimCardHistory(DataProvider dataProvider, DBCommonRDT radioDeviceTesterNewConnection, List<OpDevice> devices, OpOperator loggedOperator, out List<Exception> exceptions, bool onlyFirtsError = true)
        {
            bool retValue = true;
            exceptions = new List<Exception>();
            if (devices != null && devices.Count > 0)
            {
                UTDWebServiceSoapClient utdClient = null;
                List<OpDeviceDistributorHistory> devicesDistributorHistory = dataProvider.GetDeviceDistributorHistoryFilter(SerialNbr: devices.Select(d => d.SerialNbr).ToArray());

                if (devices.Exists(d => d.Distributor == null))
                {
                    List<OpDistributor> distrList = dataProvider.GetDistributor(devices.Where(d => d.Distributor == null).Select(d => d.IdDistributor).Distinct().ToArray());
                    foreach (OpDevice dItem in devices.Where(d => d.Distributor == null))
                    {
                        dItem.Distributor = distrList.Find(d => d.IdDistributor == dItem.IdDistributor);
                    }
                }

                foreach (OpDevice dItem in devices)
                {
                    List<OpDistributor> deviceDistributors = new List<OpDistributor>();
                    if (devicesDistributorHistory.Exists(d => d.SerialNbr == dItem.SerialNbr))
                        deviceDistributors.AddRange(devicesDistributorHistory.FindAll(d => d.SerialNbr == dItem.SerialNbr).Select(d => d.Distributor));
                    if (!deviceDistributors.Exists(d => d.IdDistributor == dItem.IdDistributor))
                        deviceDistributors.Add(dItem.Distributor);

                    List<OpDistributorData> distributorServerId = dataProvider.GetDistributorDataFilter(IdDistributor: deviceDistributors.Select(d => d.IdDistributor).ToArray(),
                                                                                                        IdDataType: new long[]{ DataType.DISTRIBUTOR_IMR_SERVER_ID });
                    if (DataProvider.ImrSeverDataTypes == null)
                        DataProvider.ImrSeverDataTypes = new List<long>();
                    if (!DataProvider.ImrSeverDataTypes.Exists(d => d == DataType.IMR_SERVER_DW_SERVER_ID))
                        DataProvider.ImrSeverDataTypes.Add(DataType.IMR_SERVER_DW_SERVER_ID);

                    List<long> imrServerDataType = new List<long>();
                    imrServerDataType.Add(DataType.IMR_SERVER_DW_SERVER_ID);
                    List<OpImrServerData> imrServerData = dataProvider.GetImrServerDataFilter(IdImrServer: distributorServerId.Select(s => Convert.ToInt32(s.Value)).ToArray(),
                                                                                              IdDataType: imrServerDataType.ToArray());

                    List<OpImrServer> imrServer = dataProvider.GetImrServer(distributorServerId.Select(s => Convert.ToInt32(s.Value)).ToArray());

                    foreach (OpImrServer isItem in imrServer)
                    {
                        isItem.DataList.RemoveAll(d => d.IdDataType.In(imrServerDataType.ToArray()));
                        isItem.DataList.AddRange(imrServerData.Where(s => s.IdImrServer == isItem.IdImrServer));
                    }

                    List<DB_DATA_ARCH> simCardHistory = new List<DB_DATA_ARCH>();
                    foreach (OpImrServer isItem in imrServer.Where(s => s.IsGood && s.IsImrlt && s.ImrServerVersion == (int)Enums.ImrServerVersion.IMR4))
                    {
                        string errorMsg = "";
                        try
                        {
                            string webserviceAddress = "";
                            int? webserviceTimeout = 0;
                            if (isItem.DwServerId.HasValue)
                            {
                                OpImrServer dwServer = dataProvider.GetImrServer(isItem.DwServerId.Value);
                                webserviceAddress = dwServer.WebserviceAddress;
                                webserviceTimeout = dwServer.WebserviceTimeout;
                            }
                            else
                            {
                                webserviceAddress = isItem.WebserviceAddress;
                                webserviceTimeout = isItem.WebserviceTimeout;
                            }

                            utdClient = UTDComponent.GetUTDClient(webserviceAddress, webserviceTimeout);
                            utdClient.Open();

                            System.Data.DataTable dataArch = utdClient.GetDataArchFilterDW(null, null, null, null, null, null, null, null, null, null, null, "[ID_DATA_TYPE] = " + DataType.DEVICE_PHONE_NUMBER + " and [SERIAL_NBR] = " + dItem.SerialNbr, out errorMsg);
                            if (errorMsg.Length > 0)
                                throw new Exception(errorMsg);
                            if (dataArch != null && dataArch.Rows.Count > 0)
                            {
                                foreach (System.Data.DataRow drItem in dataArch.Rows)
                                {
                                    DB_DATA_ARCH newArchItem = new DB_DATA_ARCH();
                                    newArchItem.SERIAL_NBR = dItem.SerialNbr;
                                    newArchItem.VALUE = drItem["VALUE"];
                                    newArchItem.TIME = Convert.ToDateTime(drItem["TIME"]);
                                    simCardHistory.Add(newArchItem);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            retValue = false;
                            exceptions.Add(ex);
                        }
                        finally
                        {
                            if(utdClient != null)
                                utdClient.Close();
                        }
                        if (retValue == false && onlyFirtsError == true)
                            return retValue;
                    }
                    if (simCardHistory != null && simCardHistory.Count > 0)
                    {
                        List<OpDeviceSimCardHistory> existingHistory = dataProvider.GetDeviceSimCardHistoryFilter(customWhereClause: "[SERIAL_NBR] = " + dItem.SerialNbr);
                        if (existingHistory != null && existingHistory.Count > 0)
                        {
                            foreach (OpDeviceSimCardHistory dschItem in existingHistory)
                                dataProvider.DeleteDeviceSimCardHistory(dschItem);
                        }
                    }
                    foreach (DB_DATA_ARCH daItem in simCardHistory.OrderBy(d => d.TIME))
                    {
                        AddSimCardHistory(dataProvider, daItem.SERIAL_NBR.Value,
                                          daItem.VALUE == null || String.IsNullOrEmpty(daItem.VALUE.ToString()) ? null : daItem.VALUE.ToString(),
                                          radioDeviceTesterNewConnection,
                                          loggedOperator,
                                          daItem.TIME);
                                          
                    }
                }
            }
            return retValue;
        }

        public static List<OpDeviceStateHistory> GetStateHistory(DataProvider dataProvider, long serialNbr, bool useDBCollector = false)
        {
            return dataProvider.GetDeviceStateHistoryFilter(SerialNbr: new[] { serialNbr }, useDBCollector: useDBCollector).ToList();
        }

        public static void Delete(DataProvider dataProvider, OpDevice objectToDelete)
        {
            foreach (OpData data in objectToDelete.DataList)
            {
                DataComponent.Delete(dataProvider, data);
            }
            dataProvider.DeleteDevice(objectToDelete);
        }

        public static void LoadAllData(DataProvider dataProvider, OpDevice device)
        {
            device.DataList.Clear();
            device.DataList.AddRange(dataProvider.GetDataFilter(SerialNbr: new long[] { device.SerialNbr }));
        }

        public static OpDeviceOrderNumber GetCreateDeviceOrderNuber(DataProvider dataProvider, string deviceOrderNumberString)
        {
            OpDeviceOrderNumber DeviceOrderNumber = null;
            if (!String.IsNullOrEmpty(deviceOrderNumberString))
            {
                int DONNameEndIndex = deviceOrderNumberString.IndexOf(' ');
                if (DONNameEndIndex >= 0)
                {
                    string[] DONQuarters = deviceOrderNumberString.Substring(DONNameEndIndex + 1).Split('-');
                    if (DONQuarters.Length == 3)
                    {
                        DeviceOrderNumber = new OpDeviceOrderNumber()
                        {
                            Name = deviceOrderNumberString.Substring(0, DONNameEndIndex),
                            FirstQuarter = DONQuarters[0],
                            SecondQuarter = DONQuarters[1],
                            ThirdQuarter = DONQuarters[2]
                        };
                    }
                }
            }
            if (DeviceOrderNumber == null)
                throw new ArgumentException("Incorrect Device Order Number format!", "deviceOrderNumberString");

            OpDeviceOrderNumber DON = dataProvider.GetAllDeviceOrderNumber().Find(q => q.ToString().Equals(DeviceOrderNumber.ToString()));
            if (DON == null)
                dataProvider.SaveDeviceOrderNumber(DeviceOrderNumber);
            else
                DeviceOrderNumber = DON;

            return DeviceOrderNumber;
        }

        public static string Get8DigitSerialNumberFrom16Digit(string longSerialNbr)
        {
            if (String.IsNullOrEmpty(longSerialNbr))
                return null;
            if (longSerialNbr.Trim().Length == 8)
            { 
                long sn;
                if (long.TryParse(longSerialNbr, out sn))
                    return longSerialNbr;
            }
            if (longSerialNbr.Trim().Length != 16)
                return null;
            return longSerialNbr[0].ToString() + longSerialNbr[1].ToString() +
                longSerialNbr[8].ToString() + longSerialNbr[9].ToString() + longSerialNbr[11].ToString() +
                longSerialNbr[12].ToString() + longSerialNbr[13].ToString() + longSerialNbr[14].ToString();

        }

        public static string GetRadioAddress(string serial)
        {
            if (serial == null)
                return null;

            if (serial.Trim().Length == 16)
                return "" + serial[0] + serial[1] + serial[8] + serial[10] + serial[11] + serial[12] + serial[13] + serial[14];

            return serial.Trim();
        }


        public static List<OpHistory> GetDeviceHistory(DataProvider dataProvider, OpOperator loggedOperator, long serialNbr, List<Enums.ObjectHistoryEventType> includedEvents = null)
        {
            List<OpHistory> deviceHistory = new List<OpHistory>();

            List<OpDeviceWarranty> warranties = new List<OpDeviceWarranty>();
            if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.ShippedToClient)
                                      || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.ServicePerformed))
            {
                if(dataProvider.CheckIfTableExists(Enums.Tables.DEVICE_WARRANTY.ToString()))
                    warranties = dataProvider.GetDeviceWarrantyFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                  SerialNbr: new long[] { serialNbr });
            }

            #region StateHistory

            if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.ChangedStatus))
            {
                List<OpDeviceStateHistory> deviceStateHistory = DeviceComponent.GetStateHistory(dataProvider, serialNbr);

                foreach (OpDeviceStateHistory stateHistory in deviceStateHistory)
                {
                    OpHistory hItem = new OpHistory(stateHistory.StartTime, Enums.ObjectHistoryEventType.ChangedStatus, stateHistory.DeviceStateType.ToString(), stateHistory.Notes);
                    hItem.EventObject = stateHistory;
                    deviceHistory.Add(hItem);
                }
            }

            #endregion
            #region ShippingListHistory

            if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.ShippedToClient))
            {
                if (dataProvider.CheckIfTableExists(Enums.Tables.SHIPPING_LIST.ToString()))
                {
                    //Historia wysyłek i serwisów w IMR SC
                    List<OpShippingListDevice> deviceIMRSCShippingHistory = dataProvider.GetShippingListDeviceFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                                                        SerialNbr: new long[] { serialNbr });
                    if (deviceIMRSCShippingHistory != null && deviceIMRSCShippingHistory.Count > 0)
                    {
                        List<OpShippingList> slList = dataProvider.GetShippingListFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                            IdShippingList: deviceIMRSCShippingHistory.Select(s => s.IdShippingList).ToArray());
                        foreach (OpShippingListDevice sldItem in deviceIMRSCShippingHistory)
                        {
                            OpDeviceWarranty warranty = warranties.Find(d => d.IdShippingList == sldItem.IdShippingList
                                                                             && d.StartDate == (warranties.Where(d1 => d1.IdShippingList == sldItem.IdShippingList)
                                                                                                          .Max(d2 => d2.StartDate)));
                            OpShippingList slItem = slList.Find(s => s.IdShippingList == sldItem.IdShippingList);
                            if (slItem != null && slItem.FinishDate.HasValue)
                            {
                                OpHistory hItem = new OpHistory(slItem.FinishDate.Value, Enums.ObjectHistoryEventType.ShippedToClient,
                                    slItem.ShippingListNbr + " (" + sldItem.IdShippingList + ")",
                                    warranty == null ? "" : String.Format("{0}: {1}", ResourcesText.WarrantyDate, warranty.WarrantyExpiration));

                                hItem.EventObject = sldItem;
                                deviceHistory.Add(hItem);
                            }
                        }
                    }
                }
            }

            #endregion
            #region ShippingHistory

            if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.SendToServer))
            {
                if (loggedOperator.HasPermission(Activity.DEVICE_SEND_TO_SERVER_OP) && dataProvider.CheckIfTableExists(Enums.Tables.DEVICE_DISTRIBUTOR_HISTORY.ToString()))
                {
                    List<OpDeviceDistributorHistory> shippingHistory = dataProvider.GetDeviceDistributorHistoryFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        SerialNbr: new long[] { serialNbr }, IdServiceReferenceType: new int[] { (int)Enums.ServiceReferenceType.IdImrServer });
                    if (shippingHistory != null && shippingHistory.Count(d => d.ReferenceValue != null) > 0)
                    {
                        List<OpImrServer> isList = dataProvider.GetImrServer(shippingHistory.Select(d => Convert.ToInt32(d.ReferenceValue)).ToArray());
                        List<OpOperator> oList = dataProvider.GetOperator(shippingHistory.Select(h => h.IdOperator).ToArray());
                        foreach (OpDeviceDistributorHistory ddhItem in shippingHistory)
                        {
                            string description = null;
                            string longDescription = null;
                            OpImrServer isItem = isList.Find(s => s.IdImrServer == Convert.ToInt32(ddhItem.ReferenceValue));
                            OpOperator oItem = oList.Find(s => s.IdOperator == Convert.ToInt32(ddhItem.IdOperator));
                            if (isItem != null)
                                description = ResourcesText.SentToServer + ": " + isItem.ToString();
                            if (oItem != null)
                                longDescription = ResourcesText.SentBy + ": " + oItem.ToString();
                            OpHistory hItem = new OpHistory(ddhItem.StartTime, Enums.ObjectHistoryEventType.SendToServer, description, longDescription);
                            hItem.EventObject = isItem;

                            deviceHistory.Add(hItem);
                        }
                    }
                }
            }
            #endregion
            #region ServiceHistory

            if (includedEvents == null || includedEvents.Exists(e => e.In(new Enums.ObjectHistoryEventType[] { Enums.ObjectHistoryEventType.ServicePerformed,
                Enums.ObjectHistoryEventType.ApprovedForService })))
            {
                if (dataProvider.CheckIfTableExists(Enums.Tables.SERVICE_LIST_DEVICE.ToString()))
                {
                    //serwisy bierzemy tylko pod uwage te ktore zosaly stworznoe w IMR SC, historyczne nie sa w pełni pobrane
                    List<OpServiceListDevice> deviceIMRServiceListHistory = dataProvider.GetServiceListDeviceFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                                                                                SerialNbr: new long[] { serialNbr },
                                                                                                                customWhereClause: "[ID_SERVICE_LIST] >= 3000");

                    if (deviceIMRServiceListHistory != null && deviceIMRServiceListHistory.Count > 0)
                    {
                        foreach (OpServiceListDevice sldItem in deviceIMRServiceListHistory)
                        {
                            OpDeviceWarranty warranty = null;
                            if (warranties != null)
                            {
                                warranty = warranties.Where(d => d.StartDate < sldItem.StartDate).OrderByDescending(d => d.StartDate).FirstOrDefault();
                            }
                            Enums.ObjectHistoryEventType ohetItem = Enums.ObjectHistoryEventType.ApprovedForService;
                            if (sldItem.EndDate.HasValue)
                                ohetItem = Enums.ObjectHistoryEventType.ServicePerformed;
                            if (includedEvents == null || includedEvents.Exists(e => e == ohetItem))
                            {
                                OpHistory hItem = new OpHistory(sldItem.EndDate.HasValue ? sldItem.EndDate.Value : sldItem.StartDate,
                                    ohetItem,
                                    String.Format("{0}: {1}", ResourcesText.ServiceList, sldItem.IdServiceList),
                                    warranty == null ? "" : String.Format("{0}: {1}", ResourcesText.WarrantyDate, warranty.WarrantyExpiration));
                                hItem.EventObject = sldItem;

                                deviceHistory.Add(hItem);
                            }
                        }
                    }
                }
            }

            #endregion
            #region DepositoryHistory

            if (includedEvents == null || includedEvents.Exists(e => e.In(new Enums.ObjectHistoryEventType[] { Enums.ObjectHistoryEventType.AddedToPackage,
                Enums.ObjectHistoryEventType.RelatedWithTask, Enums.ObjectHistoryEventType.DeinstalledByFitter, Enums.ObjectHistoryEventType.ReceivedByFitter })))
            {
                if (loggedOperator.HasPermission(Activity.TASK_LIST) || loggedOperator.HasPermission(Activity.PACKAGE_LIST) || loggedOperator.HasPermission(Activity.LOCATION_LIST))
                {
                    string customWhereClause = null;
                    if (!loggedOperator.HasPermission(Activity.TASK_LIST) && (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.RelatedWithTask)))
                        customWhereClause = "[ID_TASK] is null";
                    if (!loggedOperator.HasPermission(Activity.PACKAGE_LIST) && (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.AddedToPackage)))
                        customWhereClause = (String.IsNullOrEmpty(customWhereClause) ? "" : customWhereClause + " AND ") + "[ID_PACKAGE] is null";
                    if (!loggedOperator.HasPermission(Activity.LOCATION_LIST) && (includedEvents == null || includedEvents.Exists(e => e.In( new Enums.ObjectHistoryEventType[] { Enums.ObjectHistoryEventType.DeinstalledByFitter, Enums.ObjectHistoryEventType.ReceivedByFitter }))))
                        customWhereClause = (String.IsNullOrEmpty(customWhereClause) ? "" : customWhereClause + " AND ") + "[ID_LOCATION] is null";

                    List<OpDepositoryElement> depositoryHistory = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: true, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        SerialNbr: new long[] { serialNbr }, customWhereClause: customWhereClause);

                    if (depositoryHistory != null && depositoryHistory.Count > 0)
                    {
                        foreach (OpDepositoryElement deItem in depositoryHistory)
                        {
                            string description = "";
                            bool allowToAdd = true;
                            Enums.ObjectHistoryEventType ohetItem = Enums.ObjectHistoryEventType.AddedToPackage;
                            DateTime eventDate = deItem.StartDate;
                            if (deItem.IdPackage.HasValue)
                            {
                                ohetItem = Enums.ObjectHistoryEventType.AddedToPackage;
                                description += ResourcesText.Package + ": " + deItem.Package.Name + " (Id: " + deItem.IdPackage + "), " +
                                               ResourcesText.Sender + ": " + deItem.Package.OperatorCreator + ", " +
                                               ResourcesText.Receiver + ": " + deItem.Package.OperatorReceiver;

                                allowToAdd = loggedOperator.HasPermission(Activity.ALLOWED_DISTRIBUTOR, deItem.Package.IdDistributor);
                            }
                            else if (deItem.IdTask.HasValue)
                            {
                                ohetItem = Enums.ObjectHistoryEventType.RelatedWithTask;
                                description += ResourcesText.Task + ": " + deItem.IdTask + " (" + deItem.Task.Location.ToString().Trim() + ")";

                                allowToAdd = loggedOperator.HasPermission(Activity.ALLOWED_DISTRIBUTOR, deItem.Task.IdDistributor);
                            }
                            else if (deItem.IdLocation.HasValue)
                            {
                                if (deItem.Removed)
                                    ohetItem = Enums.ObjectHistoryEventType.DeinstalledByFitter;
                                else
                                    ohetItem = Enums.ObjectHistoryEventType.ReceivedByFitter;
                                description += ResourcesText.Depository + ": " + deItem.Location.ToString().Trim();

                                allowToAdd = loggedOperator.HasPermission(Activity.ALLOWED_DISTRIBUTOR, deItem.Location.IdDistributor);
                            }
                            if (allowToAdd && (includedEvents == null || includedEvents.Exists(e => e == ohetItem)))
                            {
                                OpHistory hItem = new OpHistory(eventDate, ohetItem, description, deItem.Notes);
                                hItem.EventObject = deItem;
                                deviceHistory.Add(hItem);
                            }
                        }
                    }
                }
            }
            #endregion
            #region SimCardHistory

            if (includedEvents == null || includedEvents.Exists(e => e.In(new Enums.ObjectHistoryEventType[] { Enums.ObjectHistoryEventType.SimCardRemoved,
                Enums.ObjectHistoryEventType.SimCardAssigned })))
            {
                if (loggedOperator.HasPermission(Activity.SIM_CARD_LIST) && dataProvider.CheckIfTableExists(Enums.Tables.DEVICE_SIM_CARD_HISTORY.ToString()))
                {
                    List<OpDeviceSimCardHistory> simCardHistory = dataProvider.GetDeviceSimCardHistoryFilter(loadNavigationProperties: true, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        SerialNbr: new long[] { serialNbr });
                    if (simCardHistory != null && simCardHistory.Count > 0)
                    {
                        foreach (OpDeviceSimCardHistory dschItem in simCardHistory)
                        {
                            Enums.ObjectHistoryEventType ohetItem = Enums.ObjectHistoryEventType.SimCardRemoved;
                            DeviceComponent.GODeviceHistory newHistoryItem = new DeviceComponent.GODeviceHistory();
                            bool allowToAdd = true;
                            string description = "";
                            if (dschItem.IdSimCard.HasValue)
                            {
                                ohetItem = Enums.ObjectHistoryEventType.SimCardAssigned;
                                newHistoryItem.ReferenceObject = dschItem.SimCard;
                                description = ResourcesText.SerialNumber + ": " + dschItem.SimCard.SerialNbr + ", " + ResourcesText.Phone + ": " + dschItem.SimCard.Phone;
                                allowToAdd = loggedOperator.HasPermission(Activity.ALLOWED_DISTRIBUTOR, dschItem.SimCard.IdDistributor);
                            }
                            newHistoryItem.LongDescription = description;
                            newHistoryItem.EventDate = dschItem.StartTime;
                            if (allowToAdd && (includedEvents == null || includedEvents.Exists(e => e == ohetItem)))
                            {
                                OpHistory hitem = new OpHistory(dschItem.StartTime, ohetItem, "", description);
                                hitem.EventObject = dschItem.SimCard;
                                deviceHistory.Add(hitem);
                            }
                        }
                    }
                }
            }

            #endregion
            #region InstallationHistory

            if (includedEvents == null || includedEvents.Exists(e => e.In(new Enums.ObjectHistoryEventType[] { Enums.ObjectHistoryEventType.InstalledAtLocation,
                Enums.ObjectHistoryEventType.RemovedFromLocation })))
            {
                if (loggedOperator.HasPermission(Activity.LOCATION_LIST))
                {
                    List<OpLocationEquipment> leList = dataProvider.GetLocationEquipmentFilter(loadCustomData: false, loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        SerialNbr: new long[] { serialNbr }, customWhereClause: "[ID_LOCATION] is not null");
                    if (leList != null && leList.Count > 0)
                    {
                        List<OpLocation> lList = dataProvider.GetLocation(leList.Select(l => l.IdLocation.Value).Distinct().ToArray())
                                                    .Where(l => l.IdLocationType == (int)Enums.LocationType.CustomerLocation).ToList();

                        foreach (OpLocationEquipment leItem in leList)
                        {
                            OpLocation lItem = lList.Find(l => l.IdLocation == leItem.IdLocation.Value);
                            if (lItem != null)
                            {
                                if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.InstalledAtLocation))
                                {
                                    OpHistory hItem = new OpHistory(leItem.StartTime, Enums.ObjectHistoryEventType.InstalledAtLocation, lItem.ToString(), lItem.FullAddress);
                                    hItem.EventObject = lItem;
                                    deviceHistory.Add(hItem);
                                }
                                if (leItem.EndTime.HasValue && (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.RemovedFromLocation)))
                                {
                                    OpHistory hItem = new OpHistory(leItem.EndTime.Value, Enums.ObjectHistoryEventType.RemovedFromLocation, lItem.ToString(), lItem.FullAddress);
                                    hItem.EventObject = lItem;

                                    deviceHistory.Add(hItem);
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            deviceHistory = deviceHistory.OrderByDescending(d => d.EventDate).ToList();
            
            return deviceHistory;
        }
        public static List<GODeviceHistory> GetDeviceHistory(DataProvider dataProvider, DBCommonUsok usokConnection, long serialNbr)
        {
            List<OpDeviceStateHistory> deviceStateHistory = DeviceComponent.GetStateHistory(dataProvider, serialNbr);
            List<SLDeviceServiceHistory> deviceServiceHistory = new List<SLDeviceServiceHistory>();

            //Historia wysyłek i serwisów w IMR SC
            List<OpShippingListDevice> deviceIMRSCShippingHistory = dataProvider.GetShippingListDeviceFilter(SerialNbr: new long[] { serialNbr });
            //serwisy bierzemy tylko pod uwage te ktore zosaly stworznoe w IMR SC, historyczne nie sa w pełni pobrane
            List<OpServiceListDevice> deviceIMRServiceListHistory = dataProvider.GetServiceListDeviceFilter(SerialNbr: new long[] { serialNbr },
                                                                                                            customWhereClause: "[ID_SERVICE_LIST] >= 3000");

            // List<OpDeviceShippingAndServiceHistoryUser> deviceServiceHistory = new List<OpDeviceShippingAndServiceHistoryUser>();
            if (usokConnection != null && usokConnection.Connected)
            {
                deviceServiceHistory = usokConnection.GetDeviceShippingAndServiceHistory(serialNbr).ToList();
                //deviceServiceHistory = DataProvider.GetDeviceShippingAndServiceHistory(Device.SerialNbr);
            }
            List<GODeviceHistory> goDeviceHistoryList = new List<GODeviceHistory>();
            foreach (OpDeviceStateHistory stateHistory in deviceStateHistory)
            {
                goDeviceHistoryList.Add(new GODeviceHistory()
                {
                    EventDate = stateHistory.StartTime,
                    LongDescription = stateHistory.Notes,
                    State = stateHistory.DeviceStateType.ToString(),
                    OpDeviceStateHistory = stateHistory
                });
            }
            foreach (SLDeviceServiceHistory stateHistory in deviceServiceHistory)
            // foreach(OpDeviceShippingAndServiceHistoryUser stateHistory in deviceServiceHistory)
            {
                //historię wysyłek mamy już w całości w IMRSC
                if (stateHistory.ROW_TYPE != SLDeviceServiceHistory.RowType.ShippedToClient)
                {
                    goDeviceHistoryList.Add(new GODeviceHistory()
                    {
                        RowType = stateHistory.ROW_TYPE,
                        EventDate = stateHistory.EVENT_DATE,
                        LongDescription = String.Format("{0}; {1}", stateHistory.DESCR, stateHistory.LONG_DESCR),
                        WarrantyDate = stateHistory.WARRANTY_DATE
                    });
                }
            }

            List<OpDeviceWarranty> warranties = null;

            if (deviceIMRSCShippingHistory != null && deviceIMRSCShippingHistory.Count > 0)
            {
                warranties = dataProvider.GetDeviceWarrantyFilter(SerialNbr: new long[] { serialNbr },
                                                                                         IdShippingList: deviceIMRSCShippingHistory.Select(d => d.IdShippingList).ToArray());

                foreach (OpShippingListDevice sldItem in deviceIMRSCShippingHistory)
                {
                    OpDeviceWarranty warranty = warranties.Find(d => d.IdShippingList == sldItem.IdShippingList
                                                                     && d.StartDate == (warranties.Where(d1 => d1.IdShippingList == sldItem.IdShippingList)
                                                                                                  .Max(d2 => d2.StartDate)));
                    if (sldItem.ShippingList.FinishDate.HasValue)
                    {
                        GODeviceHistory newHistory = new GODeviceHistory()
                        {
                            RowType = SLDeviceServiceHistory.RowType.ShippedToClient,
                            EventDate = sldItem.ShippingList.FinishDate.Value,
                            LongDescription = sldItem.ShippingList.ShippingListNbr + " (" + sldItem.IdShippingList + ")",
                            WarrantyDate = warranty != null ? (DateTime?)warranty.WarrantyExpiration : null
                        };
                        newHistory.ReferenceObject = sldItem;
                        goDeviceHistoryList.Add(newHistory);
                    }
                }
            }

            if (deviceIMRServiceListHistory != null && deviceIMRServiceListHistory.Count > 0)
            {
                foreach (OpServiceListDevice sldItem in deviceIMRServiceListHistory)
                {
                    OpDeviceWarranty warranty = null;
                    if (warranties != null)
                    {
                        warranty = warranties.Where(d => d.StartDate < sldItem.StartDate).OrderByDescending(d => d.StartDate).FirstOrDefault();
                    }
                    GODeviceHistory newHistory = new GODeviceHistory()
                    {
                        RowType = sldItem.EndDate.HasValue ? SLDeviceServiceHistory.RowType.ServicePerformed : SLDeviceServiceHistory.RowType.ApprovedForService,
                        EventDate = sldItem.EndDate.HasValue ? sldItem.EndDate.Value : sldItem.StartDate,
                        LongDescription = String.Format("{0}; {1}", sldItem.Notes, sldItem.ServiceShortDescr),
                        WarrantyDate = warranty != null ? (DateTime?)warranty.WarrantyExpiration : null
                    };
                    newHistory.ReferenceObject = sldItem;

                    goDeviceHistoryList.Add(newHistory);
                }
            }

            goDeviceHistoryList = goDeviceHistoryList.OrderByDescending(d => d.EventDate).ToList();

            deviceStateHistory.Clear();
            deviceServiceHistory.Clear();

            return goDeviceHistoryList;
        }

        public static void ChangeDeviceDistributor(DataProvider dataProvider, 
                                                   OpDeviceDetails device, OpOperator loggedOperator, 
                                                   int newIdDistributor, int? newIdOwner, string notes,
                                                   int? idServiceReferenceType = null, object referenceValue = null)
        {
            if (device.IdDistributor != newIdDistributor || device.IdDistributorOwner != newIdOwner)
            {
                device.IdDistributorOwner = newIdOwner;
                device.IdDistributor = newIdDistributor;
                dataProvider.SaveDeviceDetails(device);

                if (device.IdDistributor != newIdDistributor)
                {
                    OpDevice devItem = dataProvider.GetDevice(device.SerialNbr.Value, true);
                    devItem.IdDistributor = newIdDistributor;
                    dataProvider.SaveDevice(devItem);
                }
            }

            AddDistributorHistory(dataProvider,
                                    device.SerialNbr.Value,
                                    device.IdDistributor,
                                    device.IdDistributorOwner,
                                    loggedOperator,
                                    notes,
                                    idServiceReferenceType,
                                    referenceValue);
        }
        #region GetSchema
        public static List<OpSchema> GetSchema(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, long serialNbr)
        {
            return new List<OpSchema>();
        }

        #endregion

        #region SetHelperValues OpDevice

        public static void SetHelperValues(DataProvider dataProvider, OpDevice device, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.SerialNbr, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_SERIAL_NBR:
                        device.DataList.SetValue(DataType.HELPER_SERIAL_NBR, 0, device.SerialNbr);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        device.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, device.IdDistributor);
                        break;
                    case DataType.HELPER_ID_DEVICE_TYPE:
                        device.DataList.SetValue(DataType.HELPER_ID_DEVICE_TYPE, 0, device.IdDeviceType);
                        break;
                    case DataType.HELPER_ID_DEVICE_STATE_TYPE:
                        device.DataList.SetValue(DataType.HELPER_ID_DEVICE_STATE_TYPE, 0, device.IdDeviceStateType);
                        break;
                    case DataType.HELPER_ID_DEVICE_ORDER_NUMBER:
                        device.DataList.SetValue(DataType.HELPER_ID_DEVICE_ORDER_NUMBER, 0, device.IdDeviceOrderNumber);
                        break;
                    default:
                        break;
                }
            }
            device.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #region SetHelperValues OpDeviceConnection

        public static void SetHelperValues(DataProvider dataProvider, IMR.Suite.UI.Business.Objects.DW.OpDeviceConnection deviceConnection, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.SerialNbr, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_DEVICE_CONNECTION:
                        deviceConnection.DataList.SetValue(DataType.HELPER_ID_DEVICE_CONNECTION, 0, deviceConnection.IdDeviceConnection);
                        break;
                    case DataType.HELPER_SERIAL_NBR:
                        deviceConnection.DataList.SetValue(DataType.HELPER_SERIAL_NBR, 0, deviceConnection.SerialNbr);
                        break;
                    case DataType.HELPER_SERIAL_NBR_PARENT:
                        deviceConnection.DataList.SetValue(DataType.HELPER_SERIAL_NBR_PARENT, 0, deviceConnection.SerialNbrParent);
                        break;
                    case DataType.HELPER_TIME:
                        deviceConnection.DataList.SetValue(DataType.HELPER_TIME, 0, deviceConnection.Time);
                        break;
                    default:
                        break;
                }
            }
            deviceConnection.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #endregion

        #region Methods overloaded for IDataProvider

        #region SaveDeviceData

        public static void SaveDeviceData(IDataProvider dataProvider, OpDevice objectToSave, bool useDBCollector = false, bool loadNavigationProperties = true)
        {
            //zapis do tabeli DATA
            OpData data = null;
            for (int i = objectToSave.DataList.Count - 1; i >= 0; i--)
            {
                data = objectToSave.DataList[i];
                data.SerialNbr = objectToSave.SerialNbr;

                if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                {
                    DataComponent.Delete(dataProvider, data, useDBCollector);
                    objectToSave.DataList.Remove(data, true);
                }
                else if (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New)
                {
                    objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data, useDBCollector);

                    if (objectToSave.DataList[i].OpState == OpChangeState.New)
                    {
                        if (loadNavigationProperties)
                            objectToSave.DataList[i].AssignReferences(dataProvider);
                    }

                    objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                }
            }
        }
        
        #endregion

        #endregion

        #region GODeviceHistory

        public class GODeviceHistory
        {
            public DateTime EventDate { get; set; }
            public SLDeviceServiceHistory.RowType RowType { get; set; }
            //      public OpDeviceShippingAndServiceHistoryUser.RowTypeEnum RowType { get; set; }
            public string RowTypeString
            {
                get
                {
                    switch (RowType)
                    {
                        case SLDeviceServiceHistory.RowType.ShippedToClient://SLDeviceServiceHistory.RowType.ShippedToClient
                            return "Wysłano do klienta";
                        case SLDeviceServiceHistory.RowType.ApprovedForService://SLDeviceServiceHistory.RowType.ApprovedForService:
                            return "Przyjęto na serwis";
                        case SLDeviceServiceHistory.RowType.ServicePerformed://SLDeviceServiceHistory.RowType.ServicePerformed:
                            return "Wykonano naprawę";
                        case SLDeviceServiceHistory.RowType.InstalledAtLocation://SLDeviceServiceHistory.RowType.MountedInLocation:
                            return "Zamontowano w lokalizacji";
                        case SLDeviceServiceHistory.RowType.RemoveFromLocation://SLDeviceServiceHistory.RowType.RemoveFromLocation:
                            return "Zdemontowano z lokalizacji";
                        case SLDeviceServiceHistory.RowType.InPackage:
                            return "W paczce";
                        case SLDeviceServiceHistory.RowType.ReceivedByFitter:
                            return "Odebrane przez serwisanta";
                        case SLDeviceServiceHistory.RowType.RelatedWithTask:
                            return "Powiązane z zadaniem";
                        case SLDeviceServiceHistory.RowType.DeinstalledByFitter:
                            return "Zdjęte przez serwisanta";
                        case SLDeviceServiceHistory.RowType.SimCardAssigned:
                            return "Przypisano kartę Sim";
                        case SLDeviceServiceHistory.RowType.SimCardRemoved:
                            return "Usunięto kartę Sim";
                    }
                    return null;
                }

            }
            public string LongDescription { get; set; }
            public DateTime? WarrantyDate { get; set; }
            public string State { get; set; }

            public object ReferenceObject { get; set; }

            public string Notes
            {
                get { return this.OpDeviceStateHistory != null ? this.OpDeviceStateHistory.Notes : string.Empty; }
                set
                {
                    if (this.OpDeviceStateHistory != null)
                    {
                        this.OpDeviceStateHistory.Notes = value;
                        this.LongDescription = value;
                    }
                }
            }

            public OpDeviceStateHistory OpDeviceStateHistory { get; set; }
        }
        
        #endregion
    }
}
