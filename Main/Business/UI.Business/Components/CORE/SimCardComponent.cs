﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;
using IMR.Suite.UI.Business.Objects.CORE.Metadata;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class SimCardComponent
    {
        #region Save

        public static OpSimCard Save(IDataProvider dataProvider, OpOperator loggedOperator, OpSimCard objectToSave)
        {

            try
            {
                if (objectToSave.IdSimCard == 0)
                {
                    /* sprawdzenie przeniesione do triggera
                    List<OpSimCard> simCardExistingCheck = dataProvider.GetSimCardFilter(SerialNbr: objectToSave.SerialNbr, Phone: objectToSave.Phone);
                    if (simCardExistingCheck != null && simCardExistingCheck.Count > 0)
                    {
                        objectToSave.IdSimCard = simCardExistingCheck.Find(s => s.IdSimCard == simCardExistingCheck.Max(s1 => s1.IdSimCard)).IdSimCard;
                    }*/
                }
                if (objectToSave != null && objectToSave.Pin == null)
                    objectToSave.Pin = "";

                objectToSave.IdSimCard = dataProvider.SaveSimCard(objectToSave);

                OpSimCardData data = null;
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    data.IdSimCard = objectToSave.IdSimCard;
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        dataProvider.DeleteSimCardData(objectToSave.DataList[i]);
                    }
                    else if (objectToSave.DataList[i].OpState == OpChangeState.New ||
                             objectToSave.DataList[i].OpState == OpChangeState.Modified)
                    {
                        objectToSave.DataList[i].IdData = dataProvider.SaveSimCardData(objectToSave.DataList[i]);
                    }
                }

                objectToSave.DataList.RemoveAll(s => s.OpState == OpChangeState.Delete);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objectToSave;
        }

        #endregion

        #region Save

        [Obsolete("Use IDataProvider override.")]
        public static OpSimCard Save(DataProvider dataProvider, OpOperator loggedOperator, OpSimCard objectToSave)
        {

            try
            {
                if (objectToSave.IdSimCard == 0)
                {
                    /* sprawdzenie przeniesione do triggera
                    List<OpSimCard> simCardExistingCheck = dataProvider.GetSimCardFilter(SerialNbr: objectToSave.SerialNbr, Phone: objectToSave.Phone);
                    if (simCardExistingCheck != null && simCardExistingCheck.Count > 0)
                    {
                        objectToSave.IdSimCard = simCardExistingCheck.Find(s => s.IdSimCard == simCardExistingCheck.Max(s1 => s1.IdSimCard)).IdSimCard;
                    }*/
                }
                if (objectToSave != null && objectToSave.Pin == null)
                    objectToSave.Pin = "";

                objectToSave.IdSimCard = dataProvider.SaveSimCard(objectToSave);

                OpSimCardData data = null;
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    data.IdSimCard = objectToSave.IdSimCard;
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        dataProvider.DeleteSimCardData(objectToSave.DataList[i]);
                    }
                    else if (objectToSave.DataList[i].OpState == OpChangeState.New ||
                             objectToSave.DataList[i].OpState == OpChangeState.Modified)
                    {
                        objectToSave.DataList[i].IdData = dataProvider.SaveSimCardData(objectToSave.DataList[i]);
                    }
                }

                objectToSave.DataList.RemoveAll(s => s.OpState == OpChangeState.Delete);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objectToSave;
        }

        #endregion

        #region Delete

        public static void Delete(DataProvider dataProvider, OpOperator loggedOperator, OpSimCard objectToDelete, List<OpDeviceSimCardHistory> deviceSimCardHistory = null)
        {
            if (deviceSimCardHistory == null)
            {
                deviceSimCardHistory = dataProvider.GetDeviceSimCardHistoryFilter(loadNavigationProperties: false, autoTransaction: false,
                                                                                transactionLevel: System.Data.IsolationLevel.Serializable,
                                                                                IdSimCard: new int[] { objectToDelete.IdSimCard });
            }

            foreach (OpDeviceSimCardHistory history in deviceSimCardHistory)
            {
                dataProvider.DeleteDeviceSimCardHistory(history);
            }

            foreach (OpSimCardData scdItem in objectToDelete.DataList)
                dataProvider.DeleteSimCardData(scdItem);
            dataProvider.DeleteSimCard(objectToDelete);

        }

        #endregion

        #region SuggestNewSimCard

        public static List<OpSimCard> SuggestNewSimCard(DataProvider dataProvider, string simCardSerialNbr, long? deviceSerialNbr, bool onlyFirstPossibleCard = true)
        {
            OpSimCard oldSimCard = null;
            if (!String.IsNullOrEmpty(simCardSerialNbr))
                oldSimCard = dataProvider.GetSimCardFilter(loadNavigationProperties: false, loadCustomData: false, SerialNbr: simCardSerialNbr).FirstOrDefault();
            OpDevice device = null;
            if (deviceSerialNbr.HasValue)
                device = dataProvider.GetDevice(deviceSerialNbr.Value);

            return SuggestNewSimCard(dataProvider, oldSimCard, device, onlyFirstPossibleCard);
        }

        public static List<OpSimCard> SuggestNewSimCard(DataProvider dataProvider, OpSimCard oldSimCard, OpDevice device, bool onlyFirstPossibleCard = true)
        {
            List<OpSimCard> newSimCards = new List<OpSimCard>();

            /*
             * Karty dobieramy pod kątem przynależności do danego klienta, w drugiej kolejności do kraju
             * Karty wyciągamy operając się o taryfy do których przynależą
             * Odrzucamy taryfy które: 
             *  -   są oznaczone jako do wycofania
             *  -   ich umowa wygaśnie za mniej niż pół roku
             *  -   nie posiadaja usługi GPRS jeśli poprzednia karta taką usługę posiadała
             * Odrzucamy karty które:
             *  -   zostały oznaczone do zamówienia duplikatu
            */

            bool simCardToChange = false;
            bool gprs = false;
            int idDistributor = (device != null ? device.IdDistributor : oldSimCard.IdDistributor);
            OpDistributor distributor = dataProvider.GetDistributor(idDistributor);
            int? idDefaultTariff = null;
            string distributorCountry = null;
            string oldSimCardTariffCountry = null;
            List<OpTariff> possibleTariffList = new List<OpTariff>();
            OpTariff oldTariff = null;

            List<long> idDistributorDataTypeToGet = new List<long>();
            if (!distributor.DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_DEFAULT_OPERATOR_TARIFF_ID))
                idDistributorDataTypeToGet.Add(DataType.DISTRIBUTOR_DEFAULT_OPERATOR_TARIFF_ID);

            distributor.DataList.AddRange(dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { idDistributor }, IdDataType: idDistributorDataTypeToGet.ToArray()));
            if (distributor.DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_DEFAULT_OPERATOR_TARIFF_ID))
            {
                OpDistributorData ddItem = distributor.DataList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_DEFAULT_OPERATOR_TARIFF_ID);
                int idTariffTmp = 0;
                if (ddItem != null && ddItem.Value != null && !String.IsNullOrEmpty(ddItem.Value.ToString()) && int.TryParse(ddItem.Value.ToString(), out idTariffTmp))
                    idDefaultTariff = idTariffTmp;
            }

            #region LoadSimCardData

            if (oldSimCard != null)
            {
                List<OpSimCardData> oldSimCardData = dataProvider.GetSimCardDataFilter(loadNavigationProperties: false, customWhereClause: "[ID_SIM_CARD] = " + oldSimCard.IdSimCard);
                if (oldSimCardData != null && oldSimCardData.Count > 0 && oldSimCardData.Exists(d => d.IdDataType == DataType.SIM_CARD_GPRS))
                {
                    OpSimCardData gprsData = oldSimCardData.Find(d => d.IdDataType == DataType.SIM_CARD_GPRS);
                    if (gprsData.Value != null && !String.IsNullOrEmpty(gprsData.Value.ToString()))
                        gprs = Convert.ToBoolean(gprsData.Value);
                }
                if (oldSimCardData != null && oldSimCardData.Count > 0 && oldSimCardData.Exists(d => d.IdDataType == DataType.SIM_CARD_OPERATOR_TARIFF && d.Value != null))
                {
                    OpSimCardData scdItem = oldSimCardData.Find(d => d.IdDataType == DataType.SIM_CARD_OPERATOR_TARIFF && d.Value != null);
                    oldTariff = dataProvider.GetTariffFilter(IdTariff: new int[] { Convert.ToInt32(scdItem.Value) }).FirstOrDefault();
                    if (oldTariff != null)
                    {
                        List<long> oldTariffDataToGet = new List<long>();
                        if (!oldTariff.DataList.Exists(d => d.IdDataType == DataType.TARIFF_DEPRECATED))
                            oldTariffDataToGet.Add(DataType.TARIFF_DEPRECATED);
                        if (!oldTariff.DataList.Exists(d => d.IdDataType == DataType.TARIFF_ACTIVATION_TIME))
                            oldTariffDataToGet.Add(DataType.TARIFF_ACTIVATION_TIME);
                        if (!oldTariff.DataList.Exists(d => d.IdDataType == DataType.TARIFF_DEACTIVATION_TIME))
                            oldTariffDataToGet.Add(DataType.TARIFF_DEACTIVATION_TIME);
                        if (oldTariffDataToGet.Count > 0)
                        {
                            #region Pobranie brakujących danych

                            List<OpTariffData> oldTariffData = dataProvider.GetTariffDataFilter(IdTariff: new int[] { oldTariff.IdTariff },
                                                                                                IdDataType: new long[]{ DataType.TARIFF_ACTIVATION_TIME,
                                                                                                                DataType.TARIFF_DEACTIVATION_TIME,
                                                                                                                DataType.TARIFF_DEPRECATED });
                            if (oldTariffData != null && oldTariffData.Count > 0)
                                oldTariff.DataList.AddRange(oldTariffData);

                            #endregion
                        }
                        OpTariffData tdItemTmp = oldTariff.DataList.Find(d => d.IdDataType == DataType.TARIFF_DEPRECATED && d.Value != null);
                        bool oldTariffDeprecated = tdItemTmp != null ? false : Convert.ToBoolean(tdItemTmp.Value);
                        DateTime? oldTariffActivationTime = (!oldTariff.DataList.Exists(d => d.IdDataType == DataType.TARIFF_ACTIVATION_TIME && d.Value != null) ? null : (DateTime?)Convert.ToDateTime(oldTariff.DataList.Find(d => d.IdDataType == DataType.TARIFF_ACTIVATION_TIME && d.Value != null).Value));
                        DateTime? oldTariffDeactivationTime = (!oldTariff.DataList.Exists(d => d.IdDataType == DataType.TARIFF_DEACTIVATION_TIME && d.Value != null) ? null : (DateTime?)Convert.ToDateTime(oldTariff.DataList.Find(d => d.IdDataType == DataType.TARIFF_DEACTIVATION_TIME && d.Value != null).Value));
                        if (!oldTariffDeprecated)
                        {
                            if (oldTariffActivationTime.HasValue && oldTariffActivationTime.Value < dataProvider.DateTimeNow)
                            {
                                if (oldTariffDeactivationTime.HasValue)
                                {//jeżeli karta ma datę końca obowiązywania taryfy to wycofujemy ją jeżeli okres do końca jest krótszy niż pół roku
                                    if (oldTariffDeactivationTime.Value.AddMonths(-6) < dataProvider.DateTimeNow)
                                        simCardToChange = true;
                                }
                            }
                            else
                                simCardToChange = true;
                        }
                        else
                            simCardToChange = true;
                    }
                    else
                        simCardToChange = true;
                }
                else//jeśli nie ma informacji o taryfie to wymieniamy
                    simCardToChange = true;

                if (!simCardToChange && idDefaultTariff.HasValue)
                {
                    OpSimCardData scdItem = oldSimCardData.Find(d => d.IdDataType == DataType.SIM_CARD_OPERATOR_TARIFF && d.Value != null);
                    if (Convert.ToInt32(scdItem.Value) != idDefaultTariff)
                    {
                        simCardToChange = true;
                    }
                }
            }
            else
                simCardToChange = true;

            #endregion

            if (simCardToChange)
            {

                #region LoadDistributorData

                OpDistributorData distrData = null;

                if (device != null)
                {
                    #region DeviceDistributor

                    if (device.Distributor == null)
                    {
                        device.Distributor = dataProvider.GetDistributor(device.IdDistributor);
                    }
                    if (!device.Distributor.DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_COUNTRY))
                    {
                        device.Distributor.DataList.AddRange(dataProvider.GetDistributorDataFilter(customWhereClause: "[ID_DISTRIBUTOR] = " + device.IdDistributor + " and [ID_DATA_TYPE] = " + DataType.DISTRIBUTOR_COUNTRY));
                    }

                    distrData = device.Distributor.DataList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_COUNTRY);

                    #endregion
                }

                if (oldSimCard != null && distrData == null)
                {
                    #region SimCardDistributor

                    if (oldSimCard.Distributor == null)
                    {
                        oldSimCard.Distributor = dataProvider.GetDistributor(oldSimCard.IdDistributor);
                    }
                    if (!oldSimCard.Distributor.DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_COUNTRY))
                    {
                        oldSimCard.Distributor.DataList.AddRange(dataProvider.GetDistributorDataFilter(customWhereClause: "[ID_DISTRIBUTOR] = " + oldSimCard.IdDistributor + " and [ID_DATA_TYPE] = " + DataType.DISTRIBUTOR_COUNTRY));
                    }

                    if (oldSimCard.Distributor != null && oldSimCard.DataList.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_COUNTRY))
                        distrData = oldSimCard.Distributor.DataList.Find(d => d.IdDataType == DataType.DISTRIBUTOR_COUNTRY);

                    if (distrData == null)
                        distrData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, customWhereClause: "[ID_DISTRIBUTOR] = " + oldSimCard.IdDistributor + " AND [ID_DATA_TYPE] = " + DataType.DISTRIBUTOR_COUNTRY).FirstOrDefault();

                    #endregion
                }

                if (distrData != null && distrData.Value != null && !String.IsNullOrEmpty(distrData.Value.ToString()))
                    distributorCountry = distrData.Value.ToString();

                #endregion

                #region LoadTariffsData

                List<long> tariffsDataTypeToGet = new List<long>();
                tariffsDataTypeToGet.Add(DataType.TARIFF_ACTIVATION_TIME);
                tariffsDataTypeToGet.Add(DataType.TARIFF_ACCOUNT_NUMBER);
                tariffsDataTypeToGet.Add(DataType.TARIFF_DEACTIVATION_TIME);
                tariffsDataTypeToGet.Add(DataType.TARIFF_SERVICE);
                tariffsDataTypeToGet.Add(DataType.TARIFF_COUNTRY);
                tariffsDataTypeToGet.Add(DataType.TARIFF_DEPRECATED);

                List<OpTariff> tariffList = dataProvider.GetTariffFilter(IdDistributor: new int[] { 1, idDistributor });

                if (tariffList != null && tariffList.Count > 0)
                {
                    List<OpTariffData> tariffDataList = dataProvider.GetTariffDataFilter(IdDataType: tariffsDataTypeToGet.ToArray(), IdTariff: tariffList.Select(t => t.IdTariff).ToArray());
                    foreach (OpTariff tItem in tariffList)
                    {
                        tItem.DataList.RemoveAll(d => d.IdDataType.In(tariffsDataTypeToGet.ToArray()));
                        tItem.DataList.AddRange(tariffDataList.Where(t => t.IdTariff == tItem.IdTariff));
                    }
                    //List<int> idTariffToRemove = new List<int>();
                    //wyrzucenie wszystkich taryf, które nie mają zdefiniowanego numeru konta
                    tariffList.RemoveAll(t => !t.DataList.Exists(d => d.IdDataType == DataType.TARIFF_ACCOUNT_NUMBER));
                    //wyrzucenie wszystkich taryf które chcemy wycofać
                    tariffList.RemoveAll(t => t.DataList.Exists(d => d.IdDataType == DataType.TARIFF_DEPRECATED
                                            && d.Value != null
                                            && !String.IsNullOrEmpty(d.Value.ToString())
                                            && Convert.ToBoolean(d.Value)));
                    tariffList.RemoveAll(t => t.ActivationDate > dataProvider.DateTimeNow);//usunięcie wszystkich taryf jeszcze nie aktywnych
                    //usnięcie wszystkich już nieaktywnych
                    tariffList.RemoveAll(t => t.DataList.Exists(d => d.IdDataType == DataType.TARIFF_DEACTIVATION_TIME
                                            && d.Value != null
                                            && !String.IsNullOrEmpty(d.Value.ToString())
                                            && dataProvider.DateTimeNow > Convert.ToDateTime(d.Value)));
                    //usunięcie wszystkich taryf które nie posiadają przypisanego kraju, oprócz tych które są danego dystrybutora
                    //priorytetowym będzie dobór po dystrybutorze, w dalszej kolejności po kraju
                    tariffList.RemoveAll(t => !t.DataList.Exists(d => d.IdDataType == DataType.TARIFF_COUNTRY) && t.IdDistributor != idDistributor);
                    //usunięcie taryf których umowa wygasa w najbliższe pół roku
                    tariffList.RemoveAll(t => t.DataList.Exists(d => d.IdDataType == DataType.TARIFF_DEACTIVATION_TIME
                                            && d.Value != null
                                            && !String.IsNullOrEmpty(d.Value.ToString())
                                            && dataProvider.DateTimeNow.AddMonths(6) > Convert.ToDateTime(d.Value)));

                    if (gprs)
                    {
                        //stara karta miała usługę GPRS nowa więc też musi taką mieć

                        tariffList.RemoveAll(t => !t.DataList.Exists(d => d.IdDataType == DataType.TARIFF_SERVICE));//usuwamy wszystkie w których nie ma informacji o sługach
                        List<int> tariffToSkip = new List<int>();
                        foreach (OpTariff tItem in tariffList)
                        {
                            OpTariffData tdItem = tItem.DataList.Find(t => t.IdDataType == DataType.TARIFF_SERVICE && Convert.ToInt32(t.Value) == (int)Enums.TariffService.GPRS);
                            if (tdItem == null || tdItem.Value == null || !Convert.ToBoolean(tdItem.Value))
                                tariffToSkip.Add(tItem.IdTariff);
                        }
                        if (tariffToSkip.Count > 0)
                            tariffList.RemoveAll(t => t.IdTariff.In(tariffToSkip.ToArray()));
                    }

                    tariffList.ForEach(t => possibleTariffList.Add(t));
                }

                #endregion

                #region LoadPossibleSimCards

                if (possibleTariffList.Count > 0)
                {
                    #region DistributorPossibleMatches

                    List<OpTariff> distributorTariffMatch = new List<OpTariff>();
                    List<OpTariff> oldSimCardDistributorTariffMatch = new List<OpTariff>();

                    foreach (OpTariff tItem in possibleTariffList.Where(d => d.IdDistributor.In(new int[] { idDistributor, 1 })))
                    {
                        #region Znalezienie dostępnych kart sim

                        string customWhereClause = "[ID_DATA_TYPE] = " + DataType.SIM_CARD_OPERATOR_TARIFF;
                        customWhereClause += " AND [VALUE] is not null AND cast([VALUE] as int) = " + tItem.IdTariff;
                        List<OpSimCardData> possibleSimCards = dataProvider.GetSimCardDataFilter(loadNavigationProperties: false, customWhereClause: customWhereClause);
                        if (possibleSimCards.Count > 0)
                        {
                            string customWhereClauseAllowedCards = "[ID_SIM_CARD] in (";
                            customWhereClauseAllowedCards += "select sc.ID_SIM_CARD ";
                            customWhereClauseAllowedCards += "from SIM_CARD sc join @ID_SIM_CARD isc on sc.ID_SIM_CARD = isc.VALUE ";
                            customWhereClauseAllowedCards += "join DATA d on sc.PHONE = CAST(d.VALUE as nvarchar(max)) ";
                            customWhereClauseAllowedCards += "and d.ID_DATA_TYPE = " + DataType.DEVICE_PHONE_NUMBER + " ";
                            customWhereClauseAllowedCards += "and d.VALUE is not null";
                            customWhereClauseAllowedCards += ")";
                            List<OpSimCard> simCardsConnectedWithDevices = dataProvider.GetSimCardFilter(IdSimCard: possibleSimCards.Select(s => s.IdSimCard).ToArray(), loadCustomData: false,
                                                                                        customWhereClause: customWhereClauseAllowedCards);
                            List<int> simCardId = new List<int>();
                            foreach (OpSimCardData scdItem in possibleSimCards)
                            {
                                if (simCardsConnectedWithDevices == null
                                    || !simCardsConnectedWithDevices.Exists(s => s.IdSimCard == scdItem.IdSimCard))
                                    simCardId.Add(scdItem.IdSimCard);
                            }
                            List<OpSimCard> simCards = null;
                            if (simCardId.Count > 0)
                                simCards = dataProvider.GetSimCard(simCardId.ToArray(), false, loadCustomData: false);


                            if (simCards != null && simCards.Count > 0)
                            {
                                //upewniamy się że karty będą posiadały tylko wymaganych dystrybutorów
                                simCards.RemoveAll(s => !s.IdDistributor.In(new int[] { 1, idDistributor }));
                                foreach (OpSimCard scItem in simCards)
                                {
                                    newSimCards.Add(scItem);
                                    if (onlyFirstPossibleCard)
                                        break;
                                }
                            }
                        }
                        if (newSimCards.Count > 0 && onlyFirstPossibleCard)
                            break;

                        #endregion
                    }

                    #endregion

                    #region CountryPossibleMatches

                    if (newSimCards.Count == 0 || !onlyFirstPossibleCard)
                    {
                        List<OpTariff> distributorCountryTariffMatch = new List<OpTariff>();
                        List<OpTariff> oldSimCardCountryTariffMatch = new List<OpTariff>();

                        foreach (OpTariff tItem in possibleTariffList)
                        {
                            OpTariffData tariffCountry = tItem.DataList.Find(t => t.IdDataType == DataType.TARIFF_COUNTRY);
                            if (String.Equals(tariffCountry.Value.ToString(), distributorCountry))
                                distributorCountryTariffMatch.Add(tItem);
                            if (!String.IsNullOrEmpty(oldSimCardTariffCountry) && String.Equals(tariffCountry.Value.ToString(), oldSimCardTariffCountry))
                                oldSimCardCountryTariffMatch.Add(tItem);
                        }
                        if (distributorCountryTariffMatch.Count > 0
                            || oldSimCardCountryTariffMatch.Count > 0)
                        {
                            List<OpTariff> analyzedTariffs = new List<OpTariff>();
                            analyzedTariffs.AddRange(distributorCountryTariffMatch);
                            analyzedTariffs.AddRange(oldSimCardCountryTariffMatch);
                            if (analyzedTariffs.Count > 0)
                            {
                                #region Znalezienie dostępnych kart sim
                                foreach (OpTariff tItem in analyzedTariffs)
                                {
                                    string customWhereClause = "[ID_DATA_TYPE] = " + DataType.SIM_CARD_OPERATOR_TARIFF;
                                    customWhereClause += " AND [VALUE] is not null AND cast([VALUE] as int) = " + tItem.IdTariff;
                                    List<OpSimCardData> possibleSimCards = dataProvider.GetSimCardDataFilter(loadNavigationProperties: false, customWhereClause: customWhereClause);
                                    if (possibleSimCards.Count > 0)
                                    {
                                        string customWhereClauseAllowedCards = "[ID_SIM_CARD] in (";
                                        customWhereClauseAllowedCards += "select sc.ID_SIM_CARD ";
                                        customWhereClauseAllowedCards += "from SIM_CARD sc join @ID_SIM_CARD isc on sc.ID_SIM_CARD = isc.VALUE ";
                                        customWhereClauseAllowedCards += "join DATA d on sc.PHONE = CAST(d.VALUE as nvarchar(max)) ";
                                        customWhereClauseAllowedCards += "and d.ID_DATA_TYPE = 1010 ";
                                        customWhereClauseAllowedCards += "and d.VALUE is not null";
                                        customWhereClauseAllowedCards += ")";
                                        List<OpSimCard> simCardsConnectedWithDevices = dataProvider.GetSimCardFilter(IdSimCard: possibleSimCards.Select(s => s.IdSimCard).ToArray(), loadCustomData: false,
                                                                                                 customWhereClause: customWhereClauseAllowedCards);
                                        List<int> simCardId = new List<int>();
                                        foreach (OpSimCardData scdItem in possibleSimCards)
                                        {
                                            if (simCardsConnectedWithDevices == null
                                                || !simCardsConnectedWithDevices.Exists(s => s.IdSimCard == scdItem.IdSimCard))
                                                simCardId.Add(scdItem.IdSimCard);
                                        }
                                        List<OpSimCard> simCards = null;
                                        if (simCardId.Count > 0)
                                            simCards = dataProvider.GetSimCard(simCardId.ToArray(), false, loadCustomData: false);


                                        if (simCards != null && simCards.Count > 0)
                                        {
                                            //upewniamy się że karty będą posiadały tylko wymaganych dystrybutorów
                                            simCards.RemoveAll(s => !s.IdDistributor.In(new int[] { 1, idDistributor }));
                                            foreach (OpSimCard scItem in simCards)
                                            {
                                                newSimCards.Add(scItem);
                                                if (onlyFirstPossibleCard)
                                                    break;
                                            }
                                        }
                                    }
                                    if (newSimCards.Count > 0 && onlyFirstPossibleCard)
                                        break;
                                }
                                #endregion
                            }
                        }
                    }

                    #endregion
                }

                #endregion



                if (newSimCards.Count > 0)
                {
                    List<int> idSimCardToReject = new List<int>();
                    foreach (OpSimCard scItem in newSimCards)
                    {
                        bool rejected = false;
                        #region RejectCardDueTariff
                        if (idDefaultTariff.HasValue)
                        {
                            OpSimCardData scdItem = scItem.DataList.Find(d => d.IdDataType == DataType.SIM_CARD_OPERATOR_TARIFF);
                            int idOperTarifTmp = 0;
                            if (scdItem != null && scdItem.Value != null && !String.IsNullOrEmpty(scdItem.Value.ToString())
                                && int.TryParse(scdItem.Value.ToString(), out idOperTarifTmp))
                            {
                                if (idOperTarifTmp != idDefaultTariff)
                                {
                                    rejected = true;
                                }
                            }
                            else
                            {
                                rejected = true;
                            }
                        }
                        #endregion
                        #region RejectCardDueNonActive

                        if (!rejected)
                        {
                            if (scItem.DataList.Exists(d => d.IdDataType == DataType.SIM_CARD_OFF_DATE && d.Value != null)
                                || scItem.DataList.Exists(d => d.IdDataType == DataType.SIM_CARD_IS_ACTIVE && d.Value != null && Convert.ToBoolean(d.Value)))
                                rejected = true;
                        }

                        #endregion
                        #region RejectCardDueDuplicate

                        if (!rejected)
                        {
                            if (scItem.DataList.Exists(d => d.IdDataType.In(new long[]{ DataType.SIM_CARD_DUPLICATE_REQUEST_DATE,
                                                                                       DataType.SIM_CARD_DUPLICATE_ORDER_DATE,
                                                                                       DataType.SIM_CARD_DUPLICATE_PRODUCTION_DATE })
                                                            && d.Value != null))
                                rejected = true;
                        }

                        #endregion

                        if (rejected)
                            idSimCardToReject.Add(scItem.IdSimCard);
                    }
                    if (idSimCardToReject.Count < newSimCards.Count)//nie chcemy wyrzucić wszystkich
                    {
                        newSimCards.RemoveAll(s => s.IdSimCard.In(idSimCardToReject.ToArray()));
                    }
                }
            }

            return newSimCards;
        }

        #endregion

        #region SaveSimCardFromProductionDataBase

        public static void SaveSimCardFromProductionDataBase(DataProvider dataProvider, string phone, DBCommonRDT RadioDeviceTesterNewConnection, OpOperator loggedOperator)
        {
            OpSimCard simCard = null;
            if (RadioDeviceTesterNewConnection != null && !String.IsNullOrEmpty(phone))
            {
                IMR.Suite.Data.DB.RDT.RDTNSimCard[] productionSimCards = RadioDeviceTesterNewConnection.GetSimCardByPhone(phone);

                if (productionSimCards != null && productionSimCards.Count() > 0)
                {
                    IMR.Suite.Data.DB.RDT.RDTNSimCard analyzedProductionSimCard = productionSimCards.ToList().Find(s => s.IdSimCard == productionSimCards.Max(s1 => s1.IdSimCard));
                    simCard = dataProvider.GetSimCardFilter(loadCustomData: false, Phone: phone.Trim(), SerialNbr: analyzedProductionSimCard.SerialNbr.Trim()).FirstOrDefault();
                    bool isNew = simCard == null;
                    if (simCard == null)
                    {
                        simCard = new OpSimCard();
                        simCard.Phone = phone;
                        simCard.SerialNbr = analyzedProductionSimCard.SerialNbr;
                    }
                    simCard.Pin = analyzedProductionSimCard.Pin;
                    simCard.Puk = analyzedProductionSimCard.Puk;
                    simCard.IdDistributor = analyzedProductionSimCard.IdDistributor;
                    simCard.ApnLogin = analyzedProductionSimCard.ApnLogin;
                    simCard.ApnPassword = analyzedProductionSimCard.ApnPassword;
                    simCard.Ip = analyzedProductionSimCard.IpNumber;
                    int mobileNetworkCode = 0;
                    if (!String.IsNullOrEmpty(analyzedProductionSimCard.OperatorCode) && int.TryParse(analyzedProductionSimCard.OperatorCode, out mobileNetworkCode))
                        simCard.MobileNetworkCode = mobileNetworkCode;

                    OpSimCardData newSimCardData = null;
                    if (!isNew)
                    {
                        newSimCardData = dataProvider.GetSimCardDataFilter(customWhereClause: "[ID_SIM_CARD] = " + simCard.IdSimCard + " AND [ID_DATA_TYPE] = " + DataType.SIM_CARD_ID_DISTRIBUTOR_OWNER).FirstOrDefault();
                    }
                    if (newSimCardData == null)
                    {
                        newSimCardData = new OpSimCardData();
                        newSimCardData.IdDataType = DataType.SIM_CARD_ID_DISTRIBUTOR_OWNER;
                        newSimCardData.IndexNbr = 0;
                        newSimCardData.OpState = OpChangeState.New;
                        simCard.DataList.Add(newSimCardData);
                    }
                    else
                        newSimCardData.OpState = OpChangeState.Modified;

                    newSimCardData.Value = analyzedProductionSimCard.IdOwner;
                }
            }
            if (simCard == null)
            {
                List<OpSimCard> imrscSimCards = dataProvider.GetSimCardFilter(loadCustomData: false, Phone: phone);
                if (imrscSimCards != null && imrscSimCards.Count > 0)
                {
                    simCard = imrscSimCards.Find(s => s.IdSimCard == imrscSimCards.Max(s1 => s1.IdSimCard));
                }
                if (simCard == null)
                {
                    simCard = new OpSimCard();
                    simCard.Phone = phone;
                    simCard.SerialNbr = "-";
                    simCard.IdDistributor = 1;
                    simCard.Pin = "-";
                    simCard.Puk = "-";
                }
            }
            simCard = SimCardComponent.Save(dataProvider, loggedOperator, simCard);
        }

        public static void SaveSimCardFromProductionDataBase(DataProvider dataProvider, string serialNbr, string phone, string pin, string puk, int idDistributor, int idOwner)
        {
            OpOperator loggedOperator = null;
            serialNbr = serialNbr.Trim();
            phone = phone.Trim();
            pin = pin.Trim();
            puk = puk.Trim();
            if (pin.Length != 4)
                throw new Exception("Wrong PIN lenght");
            if (puk.Length != 8)
                throw new Exception("Wrong PUK lenght");

            OpSimCard simCard = dataProvider.GetSimCardFilter(loadCustomData: false, Phone: phone, SerialNbr: serialNbr).FirstOrDefault();

            bool isNew = simCard == null;
            if (simCard == null)
            {
                simCard = new OpSimCard();
                simCard.Phone = phone;
                simCard.SerialNbr = serialNbr;
            }
            simCard.Pin = pin;
            simCard.Puk = puk;
            simCard.IdDistributor = idDistributor;

            OpSimCardData newSimCardData = null;
            if (!isNew)
            {
                newSimCardData = dataProvider.GetSimCardDataFilter(customWhereClause: "[ID_SIM_CARD] = " + simCard.IdSimCard + " AND [ID_DATA_TYPE] = " + DataType.SIM_CARD_ID_DISTRIBUTOR_OWNER).FirstOrDefault();
            }
            if (newSimCardData == null)
            {
                newSimCardData = new OpSimCardData();
                newSimCardData.IdDataType = DataType.SIM_CARD_ID_DISTRIBUTOR_OWNER;
                newSimCardData.IndexNbr = 0;
                newSimCardData.OpState = OpChangeState.New;
                simCard.DataList.Add(newSimCardData);
            }
            else
                newSimCardData.OpState = OpChangeState.Modified;

            newSimCardData.Value = idOwner;

            simCard = SimCardComponent.Save(dataProvider, loggedOperator, simCard);
        }

        #endregion

        #region RequestSimCardDuplicate

        /// <summary>
        /// Ustawia datatyp SIM_CARD_DUPLICATE_REQUEST_DATE dla podanych kart SIM. 
        /// <para>
        ///     Metoda zwraca wyjątek ArgumentException, jeżeli forceUpdate = FALSE i istnieją karty SIM, 
        ///     które już wcześniej miały ustawioną datę zamówienia duplikatu.
        ///     Metoda zwraca wyjątek ArgumentNullException, gdy nie ma użytkownika z danym id.
        /// </para>
        /// </summary>
        /// <param name="dataProvider">DataProvider z IMRSC.</param>
        /// <param name="simCards">Lista kart SIM dla których ma być ustawiony datatyp.</param>
        /// <param name="orderDate">Data zamówienia duplikatu.</param>
        /// <param name="forceUpdate">
        /// Jeżeli parametr jest ustawiony na TRUE wtedy metoda nie sprawdza, czy karty miały 
        /// wcześniej ustawiony ten datatyp. Jeżeli datatyp był już ustawiony wtedy te karty SIM 
        /// są pomijane (wartość datatypu się nie zmienia).
        /// </param>
        /// <exception cref="System.ArgumentException">
        /// Rzucany, gdy parametr forceUpdate = FALSE i istnieją 
        /// karty SIM, które już mają ustawioną datę zamówienia duplikatu.
        /// Właściwość wyjątku: (List{string})Data["simSerialNumbers"] zawiera listę numerów seryjnych tych kart.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        /// Rzucany, gdy użytkownik TMQ nie jest przypisany do operatora IMRSC.
        /// </exception>
        public static void RequestSimCardDuplicate(DataProvider dataProvider, int RDTNEmployeeId, List<OpSimCard> simCards, DateTime requestDate, bool forceUpdate = false)
        {
            // nie mamy pewności czy OpSimCard ma załadowany DataList. Musimy to zrobić sami
            List<OpSimCardData> scdDupliceRequest = dataProvider.GetSimCardDataFilter(IdSimCard: simCards.Select(s => s.IdSimCard).ToArray(),
                                                                                      IdDataType: new long[] { DataType.SIM_CARD_DUPLICATE_REQUEST_DATE },
                                                                                      loadNavigationProperties: false);

            // załaduj listę kart sim które mają już ustawioną datę zamówienia duplikatu
            List<OpSimCard> simWithDuplicateRequest = scdDupliceRequest.Select(x => x.SimCard).ToList();

            // jeżeli istnieją już karty z zamówieniem duplikatu i ma przerwać
            if (!forceUpdate && simWithDuplicateRequest.Count > 0)
            {
                // utwórz listę numerów seryjnych i załącz do wyjątku
                List<string> serialNumbers = simWithDuplicateRequest.Select(x => x.SerialNbr).ToList();
                ArgumentException ex = new ArgumentException(ResourcesText.SimCardDuplicateRequestExists);
                ex.Data.Add("simSerialNumbers", serialNumbers);
                throw ex;
            }

            OpOperator op = OperatorComponent.GetOperatorByRDTNEmployeeId(dataProvider, RDTNEmployeeId);
            if (op == null) throw new ArgumentNullException(ResourcesText.SimCardDuplicateRequestInvalidOperator);

            // aktualizuj tylko te karty które nie mają ustawionej daty
            foreach (OpSimCard simCard in simCards.Except(simWithDuplicateRequest))
            {
                // dodaj nowe datatypy do karty sim
                simCard.DataList.Add(new OpSimCardData()
                {
                    IdSimCard = simCard.IdSimCard,
                    IdDataType = DataType.SIM_CARD_DUPLICATE_REQUEST_DATE,
                    IndexNbr = 0,
                    OpState = OpChangeState.New,
                    Value = requestDate
                });
                simCard.DataList.Add(new OpSimCardData()
                {
                    IdSimCard = simCard.IdSimCard,
                    IdDataType = DataType.SIM_CARD_DUPLICATE_REQUEST_OPERATOR_ID,
                    IndexNbr = 0,
                    OpState = OpChangeState.New,
                    Value = op.IdOperator
                });

                SimCardComponent.Save(dataProvider, null, simCard);
            }
        }

        /// <summary>
        /// Ustawia datatyp SIM_CARD_DUPLICATE_REQUEST_DATE dla kart SIM z podanymi numerami seryjnymi. 
        /// <para>
        ///     Metoda zwraca wyjątek ArgumentException, jeżeli forceUpdate = FALSE i istnieją karty SIM, 
        ///     które już wcześniej miały ustawioną datę zamówienia duplikatu.
        ///     Metoda zwraca wyjątek ArgumentNullException, gdy nie ma użytkownika z danym id.
        /// </para>
        /// </summary>
        /// <param name="dataProvider">DataProvider z IMRSC.</param>
        /// <param name="simSerialNumbers">Lista numerów seryjnych kart SIM dla których ma być ustawiony datatyp.</param>
        /// <param name="orderDate">Data zamówienia duplikatu.</param>
        /// <param name="forceUpdate">
        /// Jeżeli parametr jest ustawiony na TRUE wtedy metoda nie sprawdza, czy karty miały 
        /// wcześniej ustawiony ten datatyp. Jeżeli datatyp był już ustawiony wtedy te karty SIM 
        /// są pomijane (wartość datatypu się nie zmienia).
        /// </param>
        /// <exception cref="System.ArgumentException">
        /// Rzucany, gdy parametr forceUpdate = FALSE i istnieją 
        /// karty SIM, które już mają ustawioną datę zamówienia duplikatu.
        /// Właściwość wyjątku: (List{string})Data["simSerialNumbers"] zawiera listę numerów seryjnych tych kart.
        /// </exception>
        /// <exception cref="System.ArgumentNullException">
        /// Rzucany, gdy użytkownik TMQ nie jest przypisany do operatora IMRSC.
        /// </exception>
        public static void RequestSimCardDuplicate(DataProvider dataProvider, int RDTNEmployeeId, List<string> simSerialNumbers, DateTime requestDate, bool forceUpdate = false)
        {
            List<OpSimCard> simCards = new List<OpSimCard>();
            foreach (string simSN in simSerialNumbers)
            {
                OpSimCard simCard = dataProvider.GetSimCardFilter(loadCustomData: false, SerialNbr: simSN).First();
                simCards.Add(simCard);
            }

            RequestSimCardDuplicate(dataProvider, RDTNEmployeeId, simCards, requestDate, forceUpdate);
        }

        #endregion

        #region GetSimCardErrorText

        public static string GetSimCardErrorText(Enums.SIMCardError errorType)
        {
            switch (errorType)
            {
                case Enums.SIMCardError.Failure: return ResourcesText.Failure;
                case Enums.SIMCardError.NoConnectionWithGSM: return ResourcesText.NoConnectionWithGSM;
                case Enums.SIMCardError.NoError: return ResourcesText.NoError;
                case Enums.SIMCardError.NotInserted: return ResourcesText.NotInserted;
                case Enums.SIMCardError.PINError: return ResourcesText.PINError;
                case Enums.SIMCardError.PUKRequired: return ResourcesText.PUKRequired;
                case Enums.SIMCardError.TimeoutException: return ResourcesText.TimeoutException;
                default:
                    return errorType.ToString();
            }
        }

        #endregion

        #region GetSimCardByPermission

        public static List<OpSimCard> GetSimCardByPermission(DataProvider dataProvider, OpOperator loggedOperator,
                                                             bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false,
                                                             int[] IdSimCard = null, int[] IdDistributor = null, string SerialNbr = null, string Phone = null,
                                                             string Pin = null, string Puk = null, int[] MobileNetworkCode = null, string Ip = null, string ApnLogin = null,
                                                             string ApnPassword = null, long? topCount = null, string customWhereClause = null,
                                                             bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                                                             bool loadLocation = false, bool loadDevice = false)
        {
            List<OpSimCard> scList = dataProvider.GetSimCardFilter(loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, mergeIntoCache: mergeIntoCache,
                                                                   IdSimCard: IdSimCard, IdDistributor: IdDistributor, SerialNbr: SerialNbr, Phone: Phone,
                                                                   Pin: Pin, Puk: Puk, MobileNetworkCode: MobileNetworkCode, Ip: Ip, ApnLogin: ApnLogin,
                                                                   ApnPassword: ApnPassword, topCount: topCount, customWhereClause: customWhereClause,
                                                                   autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);


            return GetSimCardByPermission(dataProvider, loggedOperator, scList,
                                          autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout,
                                          loadLocation: loadLocation, loadDevice: loadDevice);
        }

        public static List<OpSimCard> GetSimCardByPermission(DataProvider dataProvider, OpOperator loggedOperator, List<OpSimCard> scList,
                                                             bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0,
                                                             bool loadLocation = false, bool loadDevice = false)
        {
            if (scList == null || scList.Count == 0)
                return new List<OpSimCard>();
            Dictionary<int, OpSimCard> scDict = scList.ToDictionary(s => s.IdSimCard);
            List<int> idSimCardToFilter = new List<int>();
            if (loadDevice || loadLocation)
            {
                int[] idDistributor = scList.Select(s => s.IdDistributor).Distinct().ToArray();
                if (dataProvider.DistributorFilter != null)
                    idDistributor = idDistributor.Intersect(dataProvider.DistributorFilter).ToArray();
                Data.DB.DB_DEVICE_SIM_CARD[] deviceSimCard = null;
                if (idDistributor != null && idDistributor.Count() > 0)
                    deviceSimCard = dataProvider.dbConnectionCore.GetSimCardDeviceByDistributor(idDistributor, autoTransaction: autoTransaction, transactionLevel: transactionLevel);
                Dictionary<long, OpLocation> lDict = new Dictionary<long, OpLocation>();
                Dictionary<long, OpDevice> dDict = new Dictionary<long, OpDevice>();
                if (deviceSimCard != null)
                {
                    if (loadLocation)
                    {
                        List<long> idLocations = deviceSimCard.Where(s => s.ID_LOCATION.HasValue).Select(s => s.ID_LOCATION.Value).Distinct().ToList();
                        if (idLocations.Count > 0)
                        {
                            lDict = RoleComponent.FilterByPermission<OpLocation>(dataProvider.GetLocation(idLocations.ToArray()), MdLocation.IdDistributor, Activity.ALLOWED_DISTRIBUTOR, loggedOperator).ToDictionary(l => l.IdLocation);
                            if (loggedOperator.Activities.Exists(a => a.IdActivity == Activity.ALLOWED_LOCATION))
                                lDict = RoleComponent.FilterByPermission<OpLocation>(lDict.Values.ToList(), Activity.ALLOWED_LOCATION, loggedOperator).ToDictionary(l => l.IdLocation);
                        }
                    }
                    if (loadDevice)
                    {
                        if (deviceSimCard.Count() > 0)
                        {
                            dDict = RoleComponent.FilterByPermission<OpDevice>(dataProvider.GetDevice(deviceSimCard.Select(d => d.DEVICE_SERIAL_NBR).Distinct().ToArray()), MdDevice.IdDistributor, Activity.ALLOWED_DISTRIBUTOR, loggedOperator).ToDictionary(d => d.SerialNbr);
                        }
                    }

                    foreach (Data.DB.DB_DEVICE_SIM_CARD dscItem in deviceSimCard.Where(s => s.ID_SIM_CARD.HasValue))
                    {
                        if (scDict.ContainsKey(dscItem.ID_SIM_CARD.Value))
                        {
                            OpSimCard scItem = scDict[dscItem.ID_SIM_CARD.Value];

                            if (loadDevice && dDict.ContainsKey(dscItem.DEVICE_SERIAL_NBR))
                                scItem.Devices.Add(dDict[dscItem.DEVICE_SERIAL_NBR]);
                            else
                                idSimCardToFilter.Add(dscItem.ID_SIM_CARD.Value);

                            if (loadLocation && dscItem.ID_LOCATION.HasValue)
                            {
                                if (lDict.ContainsKey(dscItem.ID_LOCATION.Value))
                                    scItem.Locations.Add(lDict[dscItem.ID_LOCATION.Value]);
                                else
                                    idSimCardToFilter.Add(dscItem.ID_SIM_CARD.Value);
                            }
                        }
                    }
                }
            }
            if (idSimCardToFilter.Count > 0)
            {
                idSimCardToFilter = idSimCardToFilter.Distinct().ToList();
                idSimCardToFilter.ForEach(s => scDict.Remove(s));
            }

            return scDict.Values.ToList();
        }

        #endregion

        #region SetHelperValues

        public static void SetHelperValues(DataProvider dataProvider, OpSimCard simCard, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdSimCard, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_SIM_CARD:
                        simCard.DataList.SetValue(DataType.HELPER_ID_SIM_CARD, 0, simCard.IdSimCard);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        simCard.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, simCard.IdDistributor);
                        break;
                    case DataType.HELPER_SIM_CARD_SERIAL_NBR:
                        simCard.DataList.SetValue(DataType.HELPER_SIM_CARD_SERIAL_NBR, 0, simCard.SerialNbr);
                        break;
                    case DataType.HELPER_PHONE:
                        simCard.DataList.SetValue(DataType.HELPER_PHONE, 0, simCard.Phone);
                        break;
                    case DataType.HELPER_PIN:
                        simCard.DataList.SetValue(DataType.HELPER_PIN, 0, simCard.Pin);
                        break;
                    case DataType.HELPER_PUK:
                        simCard.DataList.SetValue(DataType.HELPER_PUK, 0, simCard.Puk);
                        break;
                    case DataType.HELPER_MOBILE_NETWORK_CODE:
                        simCard.DataList.SetValue(DataType.HELPER_MOBILE_NETWORK_CODE, 0, simCard.MobileNetworkCode);
                        break;
                    case DataType.HELPER_IP:
                        simCard.DataList.SetValue(DataType.HELPER_IP, 0, simCard.Ip);
                        break;
                    case DataType.HELPER_APN_LOGIN:
                        simCard.DataList.SetValue(DataType.HELPER_APN_LOGIN, 0, simCard.ApnLogin);
                        break;
                    case DataType.HELPER_APN_PASSWORD:
                        simCard.DataList.SetValue(DataType.HELPER_APN_PASSWORD, 0, simCard.ApnPassword);
                        break;
                    default:
                        break;
                }
            }

            simCard.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #region GetSimCardHistory

        public static List<OpHistory> GetSimCardHistory(DataProvider dataProvider, OpOperator loggedOperator, long idSimCard)
        {
            List<OpHistory> hList = new List<OpHistory>();

            if (loggedOperator.HasPermission(Activity.SIM_CARD_LIST) && dataProvider.CheckIfTableExists(Enums.Tables.DEVICE_SIM_CARD_HISTORY.ToString()))
            {
                List<OpDeviceSimCardHistory> simCardHistory = dataProvider.GetDeviceSimCardHistoryFilter(loadNavigationProperties: true, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdSimCard: new int[] { (int)idSimCard });
                if (simCardHistory != null && simCardHistory.Count > 0)
                {
                    foreach (OpDeviceSimCardHistory dschItem in simCardHistory)
                    {
                        bool allowToAdd = loggedOperator.HasPermission(Activity.ALLOWED_DISTRIBUTOR, dschItem.SimCard.IdDistributor);
                        OpDevice device = dataProvider.GetDevice(dschItem.SerialNbr);
                        if (allowToAdd && dschItem.SimCard != null && device != null)
                        {                            
                            Enums.ObjectHistoryEventType ohetItem = Enums.ObjectHistoryEventType.SimCardAssigned;
                            if (dschItem.EndTime.HasValue)
                                ohetItem = Enums.ObjectHistoryEventType.SimCardRemoved;                            
                            string description = "";
                            description = device.ToString();
                            allowToAdd = loggedOperator.HasPermission(Activity.ALLOWED_DISTRIBUTOR, dschItem.SimCard.IdDistributor);
                            
                            OpHistory hitem = new OpHistory(dschItem.EndTime.HasValue ? dschItem.EndTime.Value : dschItem.StartTime, ohetItem, "", description);
                            hitem.EventObject = device;
                            hList.Add(hitem);                            
                        }                      
                    }
                }
            }

            return hList;
        }

        #endregion
    }
}
