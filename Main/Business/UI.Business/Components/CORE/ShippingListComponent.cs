﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ShippingListComponent : BaseComponent
    {
        #region Save

        public static OpShippingList Save(DataProvider dataProvider, OpShippingList objectToSave, bool emailNotifications = true)
        {
            OpShippingList orginalShippingList = null;
            if (objectToSave.IdShippingList > 0)
                orginalShippingList = dataProvider.GetShippingList(objectToSave.IdShippingList, true);
            objectToSave.IdShippingList = dataProvider.SaveShippingList(objectToSave);

            //zapis do tabeli SHIPPING_LIST_DATA
            OpShippingListData data = null;
            for (int i = 0; i < objectToSave.DataList.Count; i++)
            {
                data = objectToSave.DataList[i];
                if(data.DataType == null)
                    data.DataType = dataProvider.GetDataType(data.IdDataType);
                if(data.DataType.IsEditable)
                {
                    data.IdShippingList = objectToSave.IdShippingList;
                    if (data.OpState == OpChangeState.Delete)
                    {
                        if (data.IdShippingListData > 0)
                            dataProvider.DeleteShippingListData(data);
                    }
                    else if (data.OpState == OpChangeState.New || data.OpState == OpChangeState.Modified)
                    {
                        data.IdData = dataProvider.SaveShippingListData(data);
                    }
                }
            }

            if (emailNotifications)
            {
                if (orginalShippingList != null && objectToSave.FinishDate.HasValue && !orginalShippingList.FinishDate.HasValue)
                {
                    EmailComponent.CreateAndSendEmail(dataProvider, LoggedOperator, new Tuple<OpShippingList, OpShippingList>(orginalShippingList, objectToSave), EmailComponent.ShippingListOperationType.Close);
                }
            }

            return objectToSave;
        }

        #endregion

        #region Delete

        public static void Delete(DataProvider dataProvider, OpShippingList objectToDelete)
        {
            if (objectToDelete != null && objectToDelete.IdShippingList > 0)
            {
                if (objectToDelete.DataList != null)
                {
                    foreach (OpShippingListData sldItem in objectToDelete.DataList)
                    {
                        dataProvider.DeleteShippingListData(sldItem);
                    }
                    List<OpShippingListData> stillExistingData = dataProvider.GetShippingListDataFilter(IdShippingList: new int[] { objectToDelete.IdShippingList });
                    if (stillExistingData != null)
                    {
                        foreach (OpShippingListData sldItem in stillExistingData)
                        {
                            dataProvider.DeleteShippingListData(sldItem);
                        }
                    }

                }

                dataProvider.DeleteShippingList(objectToDelete);
            }
        }

        #endregion

        #region GetArticles
        
        /// <summary>
        /// Zwraca słownik artykułów w liście wysyłkowej, gdzie wartość klucza to ilość. Artykuł znajduje się we właściwości Article.
        /// </summary>
        /// <param name="dataProvider">Data Provider</param>
        /// <param name="shippingList">Lista wysyłkowa dla której chcemy wyciągnąć artykuły.</param>
        /// <returns></returns>
        public static Dictionary<OpShippingListDevice, int> GetArticles(DataProvider dataProvider, OpShippingList shippingList)
        {
            Dictionary<OpShippingListDevice, int> resultDict = new Dictionary<OpShippingListDevice, int>();

            List<OpShippingListData> articleIdDataList = shippingList.DataList.FindAll(x => x.DataType.IdDataType == DataType.SHIPPING_LIST_ID_ARTICLE).ToList();
            foreach(OpShippingListData articleIdData in articleIdDataList)
            {
                OpShippingListData articleCountData = shippingList.DataList.Find(x => x.DataType.IdDataType == DataType.SHIPPING_LIST_ARTICLE_COUNT
                                                                                      && x.IndexNbr == articleIdData.IndexNbr);

                if (articleCountData == null)
                {
                    throw new InvalidOperationException("Cannot find amount for the article.");
                }
                else
                {
                    OpShippingListDevice dummyDevice = new OpShippingListDevice()
                    {
                        Article = dataProvider.GetArticleFilter(IdArticle: new long[] { (long)articleIdData.Value }).First(),
                        Device = new OpDevice()
                    };

                    resultDict[dummyDevice] = (int)articleCountData.Value;
                }
            }

            return resultDict;
        }

        #endregion

        #region CreateReportDataTable
        /// <summary>
        /// Creates the report data table of devices and articles in orders from shipping list.
        /// </summary>
        /// <param name="idShippingList">The identifier shipping list.</param>
        /// <param name="dataProvider">The data provider.</param>
        /// <param name="serialNbr">The serial NBR.</param>
        /// <returns></returns>
        public static DataTable CreateReportDataTable(int idShippingList, Business.DataProvider dataProvider, long[] serialNbr = null)
        {
            DataTable result = new DataTable();

            result.Columns.Add("INDEX_NBR", typeof(int));
            result.Columns.Add("DEVICE_ORDER_NUMBER", typeof(string));
            result.Columns.Add("DEVICE_SERIAL_NBR", typeof(string));
            result.Columns.Add("METER_NAME", typeof(string));
            result.Columns.Add("METER_SERIAL_NBR", typeof(string));
            result.Columns.Add("WARRANTY_DATE", typeof(DateTime));
            result.Columns.Add("IMEI_NBR", typeof(string));
            result.Columns.Add("PACKAGE_NBR", typeof(string));
            result.Columns.Add("DEVICE_TYPE", typeof(int));
            result.Columns.Add("RDT_PACKAGE_NBR", typeof(string));
            //List<GOShippingListReport> reportData = new List<GOShippingListReport>();


            #region ReportBuilder
            List<OpShippingListDevice> sld = dataProvider.GetShippingListDeviceFilter(IdShippingList: new int[] { idShippingList }, SerialNbr: serialNbr);
            List<OpDevice> devices = new List<OpDevice>();
            if (sld.Select(v => v.SerialNbr).ToArray().Length > 0)
                devices = dataProvider.GetDeviceFilter(SerialNbr: sld.Select(v => v.SerialNbr).ToArray());
            DataTable dt = dataProvider.GetShippingListDeviceRadioDeviceTesterData(idShippingList);
            //List<OpDeviceWarranty> warranty = DataProvider.GetDeviceWarrantyFilter(SerialNbr: devices.Select(d => d.SerialNbr).ToArray());
            List<OpData> warranty = dataProvider.GetDataFilter(SerialNbr: devices.Select(v => v.SerialNbr).ToArray(), IdDataType: new long[] { DataType.DEVICE_WARRANTY_DATE });
            int index = 1;
            List<OpArticle> unnecessaryArticles = ArticleComponent.GetUnnecessaryArticles(dataProvider);
            foreach (OpDevice device in devices.OrderBy(v => v.IdDeviceOrderNumber))
            {
                int rowId = -1;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].ToString().Trim() == device.FactoryNumber.Trim())
                    {
                        rowId = i;
                        break;
                    }
                }

                OpShippingListDevice shippingDevice = sld.FirstOrDefault(v => v.SerialNbr == device.SerialNbr);
                /**/
                //GOShippingListReport toAdd = new GOShippingListReport();
                //toAdd.INDEX_NBR = index;
                //toAdd.DEVICE_SERIAL_NBR = device.FactoryNumber.Trim();
                //toAdd.DEVICE_ORDER_NUMBER = device.DeviceType.IdDeviceType != 10000 ? device.DeviceOrderNumber.Name.Trim() + " " + device.DeviceOrderNumber.FirstQuarter.Trim() + "-" + device.DeviceOrderNumber.SecondQuarter.Trim() + "-" + device.DeviceOrderNumber.ThirdQuarter.Trim() : shippingDevice.Article.Name.Trim();
                //toAdd.WARRANTY_DATE = (DateTime)warranty.FirstOrDefault(v => v.SerialNbr == device.SerialNbr).Value;//.WarrantyExpiration;
                //if (shippingDevice.Meter != null)
                //{
                //    toAdd.METER_NAME = shippingDevice.Meter.MeterType.Name;
                //    toAdd.METER_SERIAL_NBR = shippingDevice.Meter.MeterSerialNbr;
                //}
                //if (rowId != -1)
                //{
                //    toAdd.IMEI_NUMBER = dt.Rows[rowId][1].ToString();
                //    toAdd.PACKAGE_NUMBER = dt.Rows[rowId][2].ToString();
                //    toAdd.DEVICE_TYPE = int.Parse(dt.Rows[rowId][3].ToString());
                //    toAdd.RDT_PACKAGE_NBR = dt.Rows[rowId][4].ToString();
                //}
                //if(shippingDevice.IdArticle.In(new long?[] {971, 1320 }))
                //{
                //    toAdd.IMEI_NUMBER = device.FactoryNumber.Trim();
                //}


                //reportData.Add(toAdd);
                /**/
                int INDEX_NBR = index;
                string DEVICE_SERIAL_NBR = device.FactoryNumber.Trim();
                string DEVICE_ORDER_NUMBER = "";
                if (!shippingDevice.IdArticle.HasValue || unnecessaryArticles.Exists(v => v.IdArticle == shippingDevice.IdArticle))
                    DEVICE_ORDER_NUMBER = device.DeviceOrderNumber.Name.Trim() + " " + device.DeviceOrderNumber.FirstQuarter.Trim() + "-" + device.DeviceOrderNumber.SecondQuarter.Trim() + "-" + device.DeviceOrderNumber.ThirdQuarter.Trim();
                else
                    DEVICE_ORDER_NUMBER = shippingDevice.Article.Name.Trim();
                DateTime WARRANTY_DATE = (DateTime)warranty.FirstOrDefault(v => v.SerialNbr == device.SerialNbr).Value;
                string METER_NAME = "-";
                string METER_SERIAL_NBR = "-";
                string IMEI_NUMBER = "";
                string PACKAGE_NUMBER = "";
                int DEVICE_TYPE = 0;
                string RDT_PACKAGE_NBR = "";
                if (shippingDevice.Meter != null)
                {
                    METER_NAME = shippingDevice.Meter.MeterType.Name;
                    METER_SERIAL_NBR = shippingDevice.Meter.MeterSerialNbr;
                }
                if (rowId != -1)
                {
                    IMEI_NUMBER = dt.Rows[rowId][1].ToString();
                    PACKAGE_NUMBER = dt.Rows[rowId][2].ToString();
                    DEVICE_TYPE = int.Parse(dt.Rows[rowId][3].ToString());
                    RDT_PACKAGE_NBR = dt.Rows[rowId][4].ToString();
                }
                if (shippingDevice.IdArticle.In(new long?[] { 971, 1320 }))
                {
                    IMEI_NUMBER = device.FactoryNumber.Trim();
                }



                result.Rows.Add(INDEX_NBR, DEVICE_ORDER_NUMBER, DEVICE_SERIAL_NBR, METER_NAME.ToUpper(), METER_SERIAL_NBR.ToUpper(), WARRANTY_DATE,
                    IMEI_NUMBER, PACKAGE_NUMBER, DEVICE_TYPE, RDT_PACKAGE_NBR);
                index++;
            }
            #endregion


            //foreach (GOShippingListReport rpt_item in reportData)
            //    result.Rows.Add(rpt_item.INDEX_NBR, rpt_item.DEVICE_ORDER_NUMBER, rpt_item.DEVICE_SERIAL_NBR, rpt_item.METER_NAME.ToUpper(), rpt_item.METER_SERIAL_NBR.ToUpper(), rpt_item.WARRANTY_DATE,
            //        rpt_item.IMEI_NUMBER, rpt_item.PACKAGE_NUMBER, rpt_item.DEVICE_TYPE, rpt_item.RDT_PACKAGE_NBR);

            return result;
        }
        #endregion
    }
}
