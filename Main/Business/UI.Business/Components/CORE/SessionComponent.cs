﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using System;
using System.Collections.Generic;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class SessionComponent
    {
        public static OpSession Save(IDataProvider dataProvider, OpOperator loggedOperator, OpSession objectToSave)
        {
            bool isNewItem = false;

            try
            {
                isNewItem = objectToSave.IdSession == 0;
                objectToSave.IdSession = dataProvider.SaveSession(objectToSave);
                
                OpSessionData data = null;

                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    data.IdSession = objectToSave.IdSession;

                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        DataComponent.Delete(dataProvider, data);
                    }
                    else if (isNewItem || (dataProvider.GetDataType(data.IdDataType) != null && dataProvider.GetDataType(data.IdDataType).IsEditable))
                    {
                        //zapis do bazy tylko tych ktore sa IsEditable na true lub jesli obiekt ktory dodajemy jest nowym obiektem wiec dodajemy dane po raz pierwszy
                        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                    }
                }                

                #region UpdateSessionStatusHistory
                
                #endregion                                
            }
            catch (Exception ex)
            {
                if (isNewItem)
                    IMRLog.AddToLog(BaseComponent.Module, EventID.Forms.SessionAdditionError, loggedOperator == null ? "Unknown" : loggedOperator.Login, BaseComponent.ServerCORE, ex.Message);
                else
                    IMRLog.AddToLog(BaseComponent.Module, EventID.Forms.SessionSavingError, loggedOperator == null ? "Unknown" : loggedOperator.Login, BaseComponent.ServerCORE, objectToSave.IdSession, ex.Message);
                throw ex;
            }
            
            return objectToSave;
        }

        #region TryGetSession
        public static bool TryGetSession(IDataProvider dataProvider, long[] idSession, int[] idOperator, int[] idModule, int[] idSessionStatus, int retry,
            long? topCount, bool loadNavigationProperties, bool mergeIntoCache, bool autoTransaction, System.Data.IsolationLevel transactionLevel,
            ref List<OpSession> sessionList)
        {
            bool ret = false;
            sessionList = new List<OpSession>();
            try
            {
                if (retry < 3)
                {
                    sessionList = dataProvider.GetSessionFilter(loadNavigationProperties: loadNavigationProperties, mergeIntoCache: mergeIntoCache,
                        autoTransaction: autoTransaction, transactionLevel: transactionLevel,
                        IdSession: idSession,
                        IdOperator: idOperator,
                        IdModule: idModule,
                        IdSessionStatus: idSessionStatus,
                        topCount: topCount);
                    ret = true;
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                ret = TryGetSession(dataProvider, idSession, idOperator, idModule, idSessionStatus, retry + 1, topCount, loadNavigationProperties, mergeIntoCache, autoTransaction, transactionLevel, ref sessionList);
            }
            return ret;
        }
        #endregion
        #region TryGetSessionData
        public static bool TryGetSessionData(IDataProvider dataProvider, long[] idSession, long[] idDataType, int retry,
            bool loadNavigationProperties, bool mergeIntoCache, bool autoTransaction, System.Data.IsolationLevel transactionLevel,
            ref List<OpSessionData> sessionDataList)
        {
            bool ret = false;
            sessionDataList = new List<OpSessionData>();

            try
            {
                if (retry < 3)
                {
                    sessionDataList = dataProvider.GetSessionDataFilter(loadNavigationProperties: loadNavigationProperties, mergeIntoCache: mergeIntoCache,
                        autoTransaction: autoTransaction, transactionLevel: transactionLevel,
                        IdSession: idSession,
                        IdDataType: idDataType);
                    ret = true;
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                ret = TryGetSessionData(dataProvider, idSession, idDataType, retry + 1, loadNavigationProperties, mergeIntoCache, autoTransaction, transactionLevel, ref sessionDataList);
            }

            return ret;
        }
        #endregion

        #region TrySaveSession
        public static bool TrySaveSession(IDataProvider dataProvider, OpSession session, int retry, ref long? idSession)
        {
            bool ret = false;

            try
            {
                if (retry < 3)
                {
                    idSession = dataProvider.SaveSession(session);
                    ret = true;
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                ret = TrySaveSession(dataProvider, session, retry + 1, ref idSession);
            }

            return ret;
        }
        #endregion
        #region TrySaveSessionData
        public static bool TrySaveSessionData(IDataProvider dataProvider, OpSessionData sessionData, int retry, ref long? idSessionData)
        {
            bool ret = false;

            try
            {
                if (retry < 3)
                {
                    idSessionData = dataProvider.SaveSessionData(sessionData);
                    ret = true;
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                ret = TrySaveSessionData(dataProvider, sessionData, retry + 1, ref idSessionData);
            }

            return ret;
        }
        #endregion
    }
}
