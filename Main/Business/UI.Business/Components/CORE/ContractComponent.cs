﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.IO;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ContractComponent : BaseComponent
    {
        #region Files 
        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpContract contract, OpFile file, string savePath)
        {
            return DownloadFile(dataProvider, loggedOperator, contract, file, savePath, false);
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpContract contract, OpFile file, string savePath, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                string filePathDownloaded = ftpClient.DownloadFile(savePath, String.Format(@"{0}/{1}/", useTestFolders ? "ContractTest" : "Contract", contract.IdContract), file.PhysicalFileName);
                if (filePathDownloaded != null)
                {
                    file.LastDownloaded = dataProvider.DateTimeNow;
                    dataProvider.SaveFile(file);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpContract contract, string filePath, bool isAttachment)
        {
            return UploadFile(dataProvider, loggedOperator, contract, filePath, isAttachment, false);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpContract contract, string filePath, bool isAttachment, bool useTestFolders)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    bool uploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/", useTestFolders ? "ContractTest" : "Contract", contract.IdContract), filePath, true);
                    if (!uploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, "Upload file failed: Unknown error");
                        return null;
                    }
                    string tmpDownloadPath = ftpClient.DownloadFile(Path.GetTempPath(), String.Format(@"{0}/{1}/", useTestFolders ? "ContractTest" : "Contract", contract.IdContract), Path.GetFileName(filePath));
                    if (File.Exists(tmpDownloadPath))
                    { 
                        OpFile file = new OpFile();
                        file.PhysicalFileName = Path.GetFileName(filePath);
                        file.Size = new FileInfo(filePath).Length;
                        file.InsertDate = dataProvider.DateTimeNow;
                        file.FileBytes = new byte[1] { 0x00 };
                        long idFile = dataProvider.SaveFile(file);
                        File.Delete(tmpDownloadPath);
                        if (idFile > 0)
                        {
                            OpContractData toAdd = new OpContractData();
                            toAdd.IdContract = contract.IdContract;
                            toAdd.Value = idFile;
                            if (contract.DataList != null)
                                toAdd.Index = (isAttachment ? contract.DataList.GetNextIndex(DataType.CONTRACT_ATTACHMENT) : 0);
                            else
                            {
                                contract.DataList = new Objects.OpDataList<OpContractData>();
                                toAdd.Index = 0;
                            }
                            toAdd.IdDataType = (isAttachment ? DataType.CONTRACT_ATTACHMENT : DataType.CONTRACT_FILE);
                            toAdd.OpState = Objects.OpChangeState.New;
                            contract.DataList.Add(toAdd);
                            
                            SaveAttachment(contract, dataProvider, (isAttachment ? DataType.CONTRACT_ATTACHMENT : DataType.CONTRACT_FILE ));
                            if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
                                LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                            return file;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }
            return null;
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpContract contract)
        {
            return DeleteFile(dataProvider, loggedOperator, contract, false);
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpContract contract, bool useTestFolders)
        {
            string idFile = "";
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);

                List<OpContractData> toDeleteList = contract.DataList.FindAll(d => d.OpState == Objects.OpChangeState.Delete);

                foreach (OpContractData cdItem in toDeleteList)
                {
                    OpFile fileToDelete = dataProvider.GetFile(Convert.ToInt64(cdItem.Value), true);
                    ftpClient.DeleteFile(String.Format(@"{0}/{1}/", useTestFolders ? "ContractTest" : "Contract", contract.IdContract), fileToDelete.PhysicalFileName);



                    
                    idFile += fileToDelete.IdFile+",";

                    dataProvider.DeleteFile(fileToDelete);
                    dataProvider.DeleteContractData(cdItem);
                    contract.DataList.Remove(cdItem, true);
                }

                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeleted, LoggedOperator.Login, ServerCORE, file.IdFile);
                    LogSuccess(EventID.Forms.AttachmentDeleted, idFile.Substring(0, idFile.Length-1) );
                return true;
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeletionError, LoggedOperator.Login, ServerCORE, file.IdFile, ex.Message);
                    LogError(EventID.Forms.AttachmentDeletionError, idFile, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region Save

        public static OpContract Save(DataProvider dataProvider, OpContract objectToSave)
        {
            int id = dataProvider.SaveContract(objectToSave);
            for (int i = 0; i < objectToSave.DataList.Count; i++ )
            {
                if (objectToSave.DataList[i].OpState == Objects.OpChangeState.New || objectToSave.DataList[i].OpState == Objects.OpChangeState.Modified)
                {
                    dataProvider.SaveContractData(objectToSave.DataList[i]);
                    objectToSave.DataList[i].OpState = Objects.OpChangeState.Loaded;
                }
                else if (objectToSave.DataList[i].OpState == Objects.OpChangeState.Delete)
                {
                    dataProvider.DeleteContractData(objectToSave.DataList[i]);
                    objectToSave.DataList.Remove(objectToSave.DataList[i], true);
                    i--;
                }
            }
            return dataProvider.GetContract(id, true);
        }

        public static void SaveAttachment(OpContract contract, DataProvider dataProvider, long dataType)
        {
            List<OpContractData> newAttachmentsList = contract.DataList.FindAll(t => t.IdDataType == dataType && t.OpState == Objects.OpChangeState.New);
            foreach (OpContractData cdItem in newAttachmentsList)
            {
                cdItem.IdContractData =  dataProvider.SaveContractData(cdItem);
                cdItem.OpState = Objects.OpChangeState.Loaded;
            }
        }

        #endregion

        #region Delete

        public static void Delete(DataProvider dataProvider, OpContract objectToDelete)
        {
            if (objectToDelete != null && objectToDelete.IdContract > 0)
            {
                if (objectToDelete.DataList != null)
                {
                    List<OpWarrantyContract> warrantyContractList = dataProvider.GetWarrantyContractFilter(IdContract: new int[] { objectToDelete.IdContract });
                    if (warrantyContractList != null)
                    {
                        foreach (OpWarrantyContract wcItem in warrantyContractList)
                            dataProvider.DeleteWarrantyContract(wcItem.IdDeviceOrderNumber, wcItem.IdContract, wcItem.IdContract);
                    }
                    foreach (OpContractData cdItem in objectToDelete.DataList)
                    {
                        if (cdItem.IdContractData > 0)
                            dataProvider.DeleteContractData(cdItem);
                    }
                    objectToDelete.DataList.Clear();
                }
                dataProvider.DeleteContract(objectToDelete);
            }
        }

        #endregion
    }
}
