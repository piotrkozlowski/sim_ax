﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DeviceTypeGroupComponent
    {
        public static bool? IsDeviceInGroup(DataProvider dataProvider, Enums.DeviceTypeGroup deviceTypeGroup, int idDeviceType)
        {
            OpDeviceTypeGroup group = dataProvider.GetAllDeviceTypeGroup().Find(n => n.Name == deviceTypeGroup.ToString());
            if (group == null)
                return null;

            return dataProvider.GetDeviceTypeInGroup(group.IdDeviceTypeGroup).Find(n => n.IdDeviceType == idDeviceType) != null;
        }

        public static List<OpDeviceType> GetForGroup(DataProvider dataProvider, Enums.DeviceTypeGroup deviceTypeGroup)
        {
            OpDeviceTypeGroup group = dataProvider.GetAllDeviceTypeGroup().Find(n => n.Name == deviceTypeGroup.ToString());
            if (group == null)
                throw new Exception(string.Format(@"DeviceTypeGroup name '{0}' was not found in database.", deviceTypeGroup.ToString()));

            return dataProvider.GetDeviceTypeInGroup(group.IdDeviceTypeGroup).Select(s => s.DeviceType).ToList();
        }
    }
}
