﻿using System;
using System.Collections.Generic;
using System.Linq;

using IMR.Suite.Common;

using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LocationEquipmentComponent
    {
        #region Get methods
        #region Gets for Location
        public static OpDevice GetDeviceForLocation(DataProvider dataProvider, OpLocation location)
        {
            return GetDevicesForLocation(dataProvider, location).FirstOrDefault();
        }
        public static List<OpDevice> GetDevicesForLocation(DataProvider dataProvider, OpLocation location)
        {
            return GetDevicesForLocation(dataProvider, new List<OpLocation>() { location });
        }
        public static List<OpDevice> GetDevicesForLocation(DataProvider dataProvider, List<OpLocation> locations)
        {
            return dataProvider.GetLocationEquipmentFilter
                    (
                        IdLocation: locations.Select(q => q.IdLocation).Distinct().ToArray(),
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).Where(w => w.Device != null).Select(s => s.Device).ToList();
        }
        public static OpMeter GetMeterForLocation(DataProvider dataProvider, OpLocation location)
        {
            return GetMetersForLocation(dataProvider, location).FirstOrDefault();
        }
        public static List<OpMeter> GetMetersForLocation(DataProvider dataProvider, OpLocation location)
        {
            return GetMetersForLocation(dataProvider, new List<OpLocation>() { location });
        }
        public static List<OpMeter> GetMetersForLocation(DataProvider dataProvider, List<OpLocation> locations)
        {
            return dataProvider.GetLocationEquipmentFilter
                    (
                        IdLocation: locations.Select(q => q.IdLocation).Distinct().ToArray(),
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).Where(w => w.Meter != null).Select(s => s.Meter).ToList();
        }

        public static OpMeter GetHistoryMeterForLocation(List<OpLocationEquipment> entries, OpLocation location, DateTime date)
        {
            var entry = 
                entries.Find(
                    n =>
                    (n.IdLocation == location.IdLocation) && (n.StartTime <= date) && (n.EndTime > date || n.EndTime == null));
            return (entry == null) ? null : entry.Meter;
        }

        public static List<OpLocationEquipment> GetForLocation(DataProvider dataProvider, OpLocation location, bool useDBCollector = false)
		{
			return dataProvider.GetLocationEquipmentFilter
				(
					IdLocation: new[] { location.IdLocation },
					EndTime: Data.DB.TypeDateTimeCode.Null(),
					mergeIntoCache: useDBCollector == false,
                    useDBCollector: useDBCollector
				);
		}
		public static List<OpLocationEquipment> GetEntriesForLocation(DataProvider dataProvider, OpLocation location, bool useDBCollector = false)
		{
			return dataProvider.GetLocationEquipmentFilter
				(
					IdLocation: new[] { location.IdLocation },
                    mergeIntoCache: useDBCollector == false,
                    useDBCollector: useDBCollector
				);
		}

        /// <summary>
        /// Gets location equipement for provided locations ids.
        /// </summary>
        /// <param name="dataProvider">DataProvider reference.</param>
        /// <param name="locationsIds">List of location ids.</param>
        /// <param name="useDBCollector">Indicator to use DBCollector.</param>
        /// <returns>List of location equipement for provided locations ids.</returns>
        public static List<OpLocationEquipment> GetEntriesForLocationsIds(DataProvider dataProvider, List<long> locationsIds, bool useDBCollector = false)
        {
            return dataProvider.GetLocationEquipmentFilter(
                    IdLocation: locationsIds.ToArray(),
                    mergeIntoCache: useDBCollector == false,
                    useDBCollector: useDBCollector
                );
        }

        #endregion
        #region Gets for Device
        public static OpLocation GetLocationForDevice(DataProvider dataProvider, OpDevice device)
        {
            return GetLocationsForDevice(dataProvider, device).FirstOrDefault();

            // Ponizej wersja Artura
            //OpLocationEquipment LE = dataProvider.GetAllLocationEquipment().Find(i => i.SerialNbr == device.SerialNbr);
            //return (LE == null) ? null : LE.Location;
        }
        public static List<OpLocation> GetLocationsForDevice(DataProvider dataProvider, OpDevice device)
        {
            return GetLocationsForDevice(dataProvider, new List<OpDevice>() { device });
        }
        public static List<OpLocation> GetLocationsForDevice(DataProvider dataProvider, List<OpDevice> devices)
        {
            return dataProvider.GetLocationEquipmentFilter
                    (
                        SerialNbr: devices.Select(q => q.SerialNbr).Distinct().ToArray(),
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).Where(w => w.Location != null).Select(s => s.Location).ToList();
            // Ponizej wersja Artura
            // TW: pobranie wszystkich Equipment wraz z NavigationProperties (choć i tak poprawiłem OpLocationEquipment.ConvertList)
            //     trwa bradzo długo
            //return dataProvider.GetAllLocationEquipment().Where(le => le.SerialNbr == device.SerialNbr && !le.EndTime.HasValue).Select(le => le.Location).ToList();
        }
        public static OpMeter GetMeterForDevice(DataProvider dataProvider, OpDevice device)
        {
            return GetMetersForDevice(dataProvider, device).FirstOrDefault();

            // Ponizej wersja Artura
            //OpLocationEquipment LE = dataProvider.GetAllLocationEquipment().Find(l => l.Device == device);
            //return (LE == null) ? null : LE.Meter;
        }
        public static List<OpMeter> GetMetersForDevice(DataProvider dataProvider, OpDevice device)
        {
            return GetMetersForDevice(dataProvider, new List<OpDevice>() { device });
        }
        public static List<OpMeter> GetMetersForDevice(DataProvider dataProvider, List<OpDevice> devices)
        {
            return dataProvider.GetLocationEquipmentFilter
                    (
                        SerialNbr: devices.Select(q => q.SerialNbr).Distinct().ToArray(),
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).Where(w => w.Meter != null).Select(s => s.Meter).ToList();

            // Ponizej wersja Artura 
            // TW: pobranie wszystkich Equipment wraz z NavigationProperties (choć i tak poprawiłem OpLocationEquipment.ConvertList)
            //     trwa bradzo długo
            //List<OpMeter> ret = new List<OpMeter>();
            //foreach (var loop in dataProvider.GetAllLocationEquipment().FindAll(l => l.Device == device))
            //{
            //    ret.Add(loop.Meter);
            //}
            //return ret;
        }
		public static List<OpLocationEquipment> GetForDevice(DataProvider dataProvider, OpDevice device, bool useDBCollector = false)
		{
			return dataProvider.GetLocationEquipmentFilter
				(
					SerialNbr: new[] { device.SerialNbr },
					EndTime: Data.DB.TypeDateTimeCode.Null(),
					mergeIntoCache: useDBCollector == false,
                    useDBCollector: useDBCollector
				);
		}
		public static List<OpLocationEquipment> GetEntriesForDevice(DataProvider dataProvider, OpDevice device)
		{
			return dataProvider.GetLocationEquipmentFilter
				(
					SerialNbr: new[] { device.SerialNbr },
					mergeIntoCache: true
				);
		}
        #endregion
        #region Gets for Meter
        public static OpLocation GetLocationForMeter(DataProvider dataProvider, OpMeter meter)
        {
            return GetLocationsForMeter(dataProvider, meter).FirstOrDefault();
        }
        public static List<OpLocation> GetLocationsForMeter(DataProvider dataProvider, OpMeter meter)
        {
            return GetLocationsForMeter(dataProvider, new List<OpMeter>() { meter });
        }
        public static List<OpLocation> GetLocationsForMeter(DataProvider dataProvider, List<OpMeter> meters)
        {
            return dataProvider.GetLocationEquipmentFilter
                    (
                        IdMeter: meters.Select(q => q.IdMeter).Distinct().ToArray(),
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).Where(w => w.Location != null).Select(s => s.Location).ToList();
        }
        public static OpDevice GetDeviceForMeter(DataProvider dataProvider, OpMeter meter)
        {
            return GetDevicesForMeter(dataProvider, meter).FirstOrDefault();
        }
        public static List<OpDevice> GetDevicesForMeter(DataProvider dataProvider, OpMeter meter)
        {
            return GetDevicesForMeter(dataProvider, new List<OpMeter>() { meter });
        }
        public static List<OpDevice> GetDevicesForMeter(DataProvider dataProvider, List<OpMeter> meters)
        {
            return dataProvider.GetLocationEquipmentFilter
                    (
                        IdMeter: meters.Select(q => q.IdMeter).Distinct().ToArray(),
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).Where(w => w.Device != null).Select(s => s.Device).ToList();
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
		public static List<OpLocationEquipment> GetForMeter(DataProvider dataProvider, OpMeter meter, bool useDBCollector = false)
		{
			return dataProvider.GetLocationEquipmentFilter
				(
					IdMeter: new[] { meter.IdMeter },
					EndTime: Data.DB.TypeDateTimeCode.Null(),
					mergeIntoCache: useDBCollector == false,
                    useDBCollector: useDBCollector
				);
		}
		public static List<OpLocationEquipment> GetEntriesForMeter(DataProvider dataProvider, OpMeter meter)
		{
			return dataProvider.GetLocationEquipmentFilter
				(
					IdMeter: new[] { meter.IdMeter },
					mergeIntoCache: true
				);
		}
        #endregion

		// wykorzystywane przez Artura w PrepaidManager
        public static List<OpLocationEquipment> GetCurrentEntriesForDevice(DataProvider dataProvider, OpDevice device)
        {
            return dataProvider.GetAllLocationEquipment().FindAll(i => i.SERIAL_NBR == device.SERIAL_NBR);
        }
		// wykorzystywane przez Artura w PrepaidManager
        public static List<OpLocationEquipment> GetCurrentEntriesForLocation(DataProvider dataProvider, OpLocation location)
        {
            return dataProvider.GetAllLocationEquipment().FindAll(i => i.IdLocation == location.IdLocation);
        }
        #endregion

		#region Install methods
		#region InstallDeviceToLocation
        public static void InstallDeviceToLocation(DataProvider dataProvider, OpDevice device, OpLocation location, bool bWithMeter = true, DateTime? startTime = null, bool useDBCollector = false)
		{
			// wszystkie otwarte wpisy dla tego urzadzenia
			List<OpLocationEquipment> installedEquipments = GetForDevice(dataProvider, device, useDBCollector);

			if (installedEquipments.Where(w => w.Meter != null).Count() > 0)
			{
				// Urzadzenie jest zainstalowane w Meter
				if (bWithMeter)
				{
					// insttalujemy caly meter (ze wszystkimi urzadzeniami) w docelowej lokalizacji
					InstallMeterToLocation(dataProvider, installedEquipments.Where(w => w.Meter != null).First().Meter, location, useDBCollector: useDBCollector);
					return;
				}
				else
				{
					// "odczepiamy" urzadzenie od Metera
                    UninstallDeviceFromMeter(dataProvider, device, installedEquipments.Where(w => w.Meter != null).First().Meter, useDBCollector: useDBCollector);
				}
			}

			// odinstalowywujemy w "starej(ych)" lokalizacji(ach) [oldL,null,SN] tworząc tylko wpis koncowy
			foreach (OpLocationEquipment installedEquipment in installedEquipments.Where(w => w.IdLocation != null))
				UninstallDeviceFromLocation(dataProvider, device, installedEquipment.Location, bWithMeter: false, bCreateNewEntry: false, useDBCollector: useDBCollector);

            NewEntry(dataProvider, location, null, device, startTime, useDBCollector);

			if (device.DataList.Exists(DataType.DEVICE_CID) && !location.IdLocationType.In((int)Enums.LocationType.DepositoryLocation, (int)Enums.LocationType.ServiceLocation))
			{
				device.DataList.SetValue(DataType.DEVICE_CID, location.CID);
				DeviceComponent.SaveDeviceData(dataProvider, device, useDBCollector);
			}
			// !!! Skopiować parametry z Location do Device?? (Timery itp)
		}
		#endregion
		#region InstallMeterToLocation

        [Obsolete("Use overloaded method with IDataProvider", false)]
		public static void InstallMeterToLocation(DataProvider dataProvider, OpMeter meter, OpLocation location, DateTime? startTime = null, bool useDBCollector = false)
		{
			// wszystkie otwarte wpisy dla tego metera
			List<OpLocationEquipment> installedEquipments = GetForMeter(dataProvider, meter, useDBCollector);

			if (installedEquipments.Count > 0)
			{
				// odinstalowywujemy w "starej(ych)" lokalizacji(ach) [oldL,M,xxx] tworząc tylko wpis koncowy
				foreach (OpLocationEquipment equipment in installedEquipments.Where(w => w.Location != null))
					UninstallMeterFromLocation(dataProvider, meter, equipment.Location, bCreateNewEntry: false, useDBCollector: useDBCollector);

				// konczymy wpisy jeśli meter ma urzadzenie a nie jest w lokalizacji [null,M,xxx]
				foreach (OpLocationEquipment equipment in installedEquipments.Where(w => w.Location == null && w.Device != null))
					CloseEntry(dataProvider, equipment, useDBCollector);

				foreach (OpLocationEquipment equipment in installedEquipments)
				{
					NewEntry(dataProvider, location, equipment.Meter, equipment.Device, startTime, useDBCollector);

					if (equipment.Device != null && equipment.Device.DataList.Exists(DataType.DEVICE_CID) &&
					    !location.IdLocationType.In((int) Enums.LocationType.DepositoryLocation,
					                                (int) Enums.LocationType.ServiceLocation))
					{
						equipment.Device.DataList.SetValue(DataType.DEVICE_CID, location.CID);
						DeviceComponent.SaveDeviceData(dataProvider, equipment.Device, useDBCollector);
					}

					// !!! Skopiować parametry z Location do Device?? (Timery itp)
				}
			}
			else
			{
				// Meter nie jest zainstalowany w ani jednej lokalizacji
				// Tworzymy nowy wpis instalujac tylko metera w lokalizacji [L,M,null]
				NewEntry(dataProvider, location, meter, null, startTime, useDBCollector);
			}
		}
		#endregion
		#region InstallDeviceToMeter
		public static void InstallDeviceToMeter(DataProvider dataProvider, OpDevice device, OpMeter meter, DateTime? startTime = null)
		{
			// wszystkie otwarte wpisy dla tego urzadzenia
			List<OpLocationEquipment> deviceEquipments = GetForDevice(dataProvider, device);

			// wszystkie otwarte wpisy dla metera
			List<OpLocationEquipment> meterEquipments = GetForMeter(dataProvider, meter);

			foreach (OpLocationEquipment equipment in deviceEquipments.Where(w => w.Meter != null))
				UninstallDeviceFromMeter(dataProvider, device, equipment.Meter, bCreateNewEntry: false);

			foreach (OpLocationEquipment equipment in deviceEquipments.Where(w => w.Meter == null && w.Location != null))
				UninstallDeviceFromLocation(dataProvider, device, equipment.Location, bWithMeter: false, bCreateNewEntry: false);

			List<OpLocationEquipment> meterInLocation = meterEquipments.Where(w => w.Location != null).ToList();
			if (meterInLocation.Count > 0)
			{
				foreach (OpLocationEquipment equipment in meterInLocation.Distinct(w => w.Location))
				{
					UninstallMeterFromLocation(dataProvider, meter, equipment.Location, bCreateNewEntry: false);

                    NewEntry(dataProvider, equipment.Location, meter, device, startTime);

					if (equipment.Device != null && equipment.Device.DataList.Exists(DataType.DEVICE_CID) && !equipment.Location.IdLocationType.In((int)Enums.LocationType.DepositoryLocation, (int)Enums.LocationType.ServiceLocation))
					{
						equipment.Device.DataList.SetValue(DataType.DEVICE_CID, equipment.Location.CID);
						DeviceComponent.SaveDeviceData(dataProvider, equipment.Device);
					}
					// !!! Skopiować parametry z Location do Device?? (Timery itp)
				}
			}
			else
                NewEntry(dataProvider, null, meter, device, startTime);
		}
		#endregion
		#endregion

		#region Uninstall methods
		#region UninstallDeviceFromLocation
		/// <param name="device">Device to uninstall</param>
		/// <param name="location">Location from device should be uninstalled</param>
		/// <param name="bWithMeter">true if assigned Meter(s) should be uinstalled also</param>
		/// <param name="bCreateNewEntry">false if do not create beginig entry</param>
        public static void UninstallDeviceFromLocation(DataProvider dataProvider, OpDevice device, OpLocation location, bool bWithMeter = true, bool bCreateNewEntry = true, bool useDBCollector = false)
		{
			OpLocationEquipment equipment = dataProvider.GetLocationEquipmentFilter(SerialNbr: new long[] { device.SerialNbr },
																					IdLocation: new long[] { location.IdLocation },
																					EndTime: Data.DB.TypeDateTimeCode.Null(),
                                                                                    useDBCollector: useDBCollector).FirstOrDefault();
			if (equipment != null)
			{
				if (equipment.Meter != null)
				{
					// Urzadzenie jest zainstalowane w meter (wraz z meterem) [L,M,SN]
					List<OpLocationEquipment> meterEquipments = dataProvider.GetLocationEquipmentFilter(IdMeter: new long[] { equipment.Meter.IdMeter },
																										IdLocation: new long[] { location.IdLocation },
                                                                                                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                                                                                                        useDBCollector: useDBCollector);
					if (bWithMeter)
					{
						// poniewaz meter moze miec wiele SN, wiec trzeba usunac cały meter (ze wszystkimi SN) [L,M,xxx]
						foreach (OpLocationEquipment meterEquipment in meterEquipments)
						{
							CloseEntry(dataProvider, meterEquipment, useDBCollector); // konczymy wpisy dla [L,M,xxx]

							// rozpoczynamy wpis [null,M,SN]
							if (meterEquipment.Device != null) // zabezpieczamy sie przed wpisem [L,M,null] bo go nie powinno byc przy istniejacym, choc jednym wpisem [L,M,SN]
							{
								if (bCreateNewEntry) NewEntry(dataProvider, null, meterEquipment.Meter, meterEquipment.Device, useDBCollector: useDBCollector);
							}
						}
					}
					else
					{
						CloseEntry(dataProvider, equipment, useDBCollector); //konczymy wpis [L,M,SN]

						// usuwamy samo urzadzenie, pozostawiajac meter w lokalizacji [L,M,xxx] (meter moze miec jeszcze inne urzadzenia)
						if (meterEquipments.Where(w => w.Device != device).Count() == 0)
						{
							// meter w tej lokalizacji ma tylko jedno(usuwane) urzadzenie wiec nalezy rozpoczac wpis [L,M,null]
                            if (bCreateNewEntry) NewEntry(dataProvider, equipment.Location, equipment.Meter, null, useDBCollector: useDBCollector);
						}
					}
				}
				else
				{
					// Urzadzenie bez metera [L,null,SN], konczymy wpis
					CloseEntry(dataProvider, equipment, useDBCollector);
				}
			}
		}
		#endregion
		#region UninstallMeterFromLocation
		/// <param name="meter">Meter to uninstall</param>
		/// <param name="location">Location from meter should be uninstalled</param>
		/// <param name="bCreateNewEntry">false if do not create beginig entry</param>
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void UninstallMeterFromLocation(DataProvider dataProvider, OpMeter meter, OpLocation location, bool bCreateNewEntry = true, bool useDBCollector = false)
		{
			List<OpLocationEquipment> equipments = dataProvider.GetLocationEquipmentFilter(IdMeter: new long[] { meter.IdMeter },
																						   IdLocation: new long[] { location.IdLocation },
																						   EndTime: Data.DB.TypeDateTimeCode.Null(),
                                                                                           useDBCollector: useDBCollector);
			foreach (OpLocationEquipment meterEquipment in equipments)
			{
				// konczymy wpis(y) [L,M,xxx]
				CloseEntry(dataProvider, meterEquipment, useDBCollector);

				if (meterEquipment.Device != null)
				{
					// w meterze zainstalowane jest urządzenie, wiec musimy zostawic otwarty wpis [null,M,SN]
					if (bCreateNewEntry) NewEntry(dataProvider, null, meter, meterEquipment.Device, useDBCollector: useDBCollector);
				}
			}
		}
		#endregion
		#region UninstallDeviceFromMeter
		/// <param name="device">Device to uninstall</param>
		/// <param name="meter">meter from device should be uninstalled</param>
		/// <param name="bCreateNewEntry">false if do not create beginig entry</param>
		public static void UninstallDeviceFromMeter(DataProvider dataProvider, OpDevice device, OpMeter meter, bool bCreateNewEntry = true, bool useDBCollector = false)
		{
			List<OpLocationEquipment> equipments = GetForMeter(dataProvider, meter, useDBCollector).Where(w => w.Device != null).ToList();

			if (equipments.Count == 0)
				return; // nie ma takiego metera lub ten meter nie ma zainstalowanych urzadzen

			if (equipments.Count > 1)
			{
				// meter ma zainstalowanych wiele urzadzen
				// szukamy tego ktorego odinstalowywujemy
				OpLocationEquipment equipmentForDevice = equipments.Where(w => w.Device == device).FirstOrDefault();
				if (equipmentForDevice == null)
				{
					// cos nie tak, bo nie ma takiego urzadzenia w meterze
					return;
				}
				// dla tego wiersza z tym urzadzeniem w EQUIPMENT zamykamy tylko wpis, reszte wpisów dla tego metera zostawiamy otwarte
				CloseEntry(dataProvider, equipmentForDevice, useDBCollector);
			}
			else
			{
				// jest tylko jeden wpis z tym meterem
				// czy jest to wpis z odinstalowywanym urzadzeniem?
				OpLocationEquipment equipmentForDevice = equipments.Where(w => w.Device == device).FirstOrDefault();
				if (equipmentForDevice == null)
				{
					// nie wiec, nie ma co odinstalowywac
					return;
				}
				// zamykamy wpis w EQUIPMENT
				CloseEntry(dataProvider, equipmentForDevice, useDBCollector);

				// jesli meter jest zainstalowany w lokalizacji to zostawiamy wpis [L,M,null]
				// Uwaga: sytucja (stan na dzien 31.08.2011) gdy jest zalozenie ze meter moze byc tylko w jednej lokalizacji
				if (equipmentForDevice.Location != null)
				{
					if (bCreateNewEntry) NewEntry(dataProvider, equipmentForDevice.Location, meter, null, useDBCollector: useDBCollector);
				}
			}
		}
		#endregion
		#endregion

		#region Other methods
        [Obsolete("Use overloaded method with IDataProvider", false)]
		public static OpLocationEquipment NewEntry(DataProvider dataProvider, OpLocation location, OpMeter meter, OpDevice device, DateTime? startTime = null, bool useDBCollector = false)
		{
            if (!startTime.HasValue)
                startTime = dataProvider.DateTimeNow;
            OpLocationEquipment newEquipment = new OpLocationEquipment() { Location = location, Meter = meter, Device = device, StartTime = startTime.Value };
			return Save(dataProvider, newEquipment, useDBCollector);
		}

        [Obsolete("Use overloaded method with IDataProvider", false)]
		public static void CloseEntry(DataProvider dataProvider, OpLocationEquipment equipment, bool useDBCollector = false)
        {
            equipment.EndTime = dataProvider.DateTimeNow;
            Save(dataProvider, equipment, useDBCollector);

			// To jest Artura, chyba jeszcze do poprawki (inne kombinacje??)
            if (equipment.Device != null && equipment.Location != null)
                equipment.Device.CurrentLocations.Remove(equipment.Location);
        }

        [Obsolete("Use overloaded method with IDataProvider", false)]
		public static OpLocationEquipment Save(DataProvider dataProvider, OpLocationEquipment objectToSave, bool useDBCollector = false)
		{
			dataProvider.SaveLocationEquipment(objectToSave, useDBCollector);
			return dataProvider.GetLocationEquipment(objectToSave.IdLocationEquipment, useDBCollector);
		}

		public static void ReplaceDevice(DataProvider dataProvider, OpLocation Location, OpDevice NewDevice, OpMeter meter = null)
		{
            //OpMeter meter = null;
            bool fixedMeter = meter != null;

			foreach (var loop in GetCurrentEntriesForLocation(dataProvider, Location))
			{
                if (!fixedMeter && loop.Meter != null)
					meter = loop.Meter;
				if (loop.EndTime == null)
					CloseEntry(dataProvider, loop);
			}

			foreach (var loop in GetCurrentEntriesForDevice(dataProvider, NewDevice))
			{
                if (!fixedMeter && loop.Meter != null)
					meter = loop.Meter;
				if (loop.EndTime == null)
					CloseEntry(dataProvider, loop);
			}

            if (fixedMeter)
                foreach (var loop in GetForMeter(dataProvider, meter))
                    CloseEntry(dataProvider, loop);

			OpLocationEquipment insert = new OpLocationEquipment();
			insert.Location = Location;
			insert.Device = NewDevice;
			insert.Meter = meter;
            insert.StartTime = dataProvider.DateTimeNow;

			dataProvider.SaveLocationEquipment(insert);

            NewDevice.CurrentLocations.Clear();
            NewDevice.CurrentLocations.Add(Location);
            NewDevice.CurrentMeters.Clear();
            NewDevice.CurrentMeters.Add(meter);

            Location.CurrentDevices.Clear();
            Location.CurrentDevices.Add(NewDevice);
            Location.CurrentMeters.Clear();
            Location.CurrentMeters.Add(meter);
		}
        //public static OpLocationEquipment GetByID(DataProvider dataProvider, long Id)
        //{
        //    if (Id == 0)
        //        return null;

        //    return dataProvider.GetLocationEquipment(Id);
        //}

        //public static List<OpLocationEquipment> GetAll(DataProvider dataProvider)
        //{
        //    return dataProvider.GetAllLocationEquipment().
        //        Where(le => !le.EndTime.HasValue && le.Device != null &&
        //            (!le.Device.SerialNbrPattern.HasValue || le.Device.SerialNbr != le.Device.SerialNbrPattern)).ToList();
        //}

        /* NOT ALLOWED!!
        public static void Delete(DataProvider dataProvider, OpLocationEquipment objectToDelete)
        {
            dataProvider.DeleteLocationEquipment(objectToDelete);
        }*/

        //public static OpLocationEquipment AssignDeviceToLocation(DataProvider dataProvider, OpDevice device, OpLocation location)
        //{
        //    RemoveDeviceFromLocationsExcept(dataProvider, device, location);

        //    OpLocationEquipment locationEquipment = dataProvider.LocationEquipment.FirstOrDefault(
        //        lo => lo.IdLocation.HasValue && lo.IdLocation == location.IdLocation &&
        //            lo.SerialNbr.HasValue && lo.SerialNbr == device.SerialNbr &&
        //            !lo.EndTime.HasValue);

        //    if (locationEquipment == null)
        //    {
        //        locationEquipment = new OpLocationEquipment();
        //        locationEquipment.Location = location;
        //        locationEquipment.Device = device;
        //        locationEquipment.StartTime = DataProvider.DateTimeNow;

        //        locationEquipment = Save(dataProvider, locationEquipment);
        //    }

        //    return locationEquipment;
        //}

        //public static void RemoveDeviceFromAnyLocation(DataProvider dataProvider, OpDevice device)
        //{
        //    List<OpLocationEquipment> leList = new List<OpLocationEquipment>();
        //    foreach (var locationEquipment in dataProvider.LocationEquipment.Where(le => le.SerialNbr.HasValue && le.SerialNbr == device.SerialNbr && !le.EndTime.HasValue).ToList())
        //    {
        //        locationEquipment.EndTime = DataProvider.DateTimeNow;
        //        Save(dataProvider, locationEquipment);
        //    }
        //}

        //public static void RemoveDeviceFromLocationsExcept(DataProvider dataProvider, OpDevice device, OpLocation exceptLocation)
        //{
        //    List<OpLocationEquipment> leList = new List<OpLocationEquipment>();
        //    foreach (var locationEquipment in dataProvider.LocationEquipment.Where(
        //        le => le.IdLocation.HasValue && le.IdLocation != exceptLocation.IdLocation &&
        //            le.SerialNbr.HasValue && le.SerialNbr == device.SerialNbr && !le.EndTime.HasValue).ToList())
        //    {
        //        locationEquipment.EndTime = DataProvider.DateTimeNow;
        //        Save(dataProvider, locationEquipment);
        //    }
        //}

        //public static void RemoveDeviceFromLocation(DataProvider dataProvider, OpDevice device, OpLocation location)
        //{
        //    OpLocationEquipment locationEquipment = dataProvider.LocationEquipment.LastOrDefault(
        //        lo => lo.IdLocation.HasValue && lo.IdLocation == location.IdLocation &&
        //            lo.SerialNbr.HasValue && lo.SerialNbr == device.SerialNbr &&
        //            !lo.EndTime.HasValue);

        //    if (locationEquipment != null)
        //    {
        //        locationEquipment.EndTime = DataProvider.DateTimeNow;
        //        Save(dataProvider, locationEquipment);
        //    }
        //}
        #endregion

        #region Methods overloads for IDataProvider

        #region InstallMeterToLocation
        public static void InstallMeterToLocation(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpMeter meter, OpLocation location, DateTime? startTime = null, bool useDBCollector = false)
        {
            // wszystkie otwarte wpisy dla tego metera
            List<OpLocationEquipment> installedEquipments = GetForMeter(dataProvider, meter, useDBCollector);

            if (installedEquipments.Count > 0)
            {
                // odinstalowywujemy w "starej(ych)" lokalizacji(ach) [oldL,M,xxx] tworząc tylko wpis koncowy
                foreach (OpLocationEquipment equipment in installedEquipments.Where(w => w.Location != null))
                    UninstallMeterFromLocation(dataProvider, meter, equipment.Location, bCreateNewEntry: false, useDBCollector: useDBCollector);

                // konczymy wpisy jeśli meter ma urzadzenie a nie jest w lokalizacji [null,M,xxx]
                foreach (OpLocationEquipment equipment in installedEquipments.Where(w => w.Location == null && w.Device != null))
                    CloseEntry(dataProvider, equipment, useDBCollector);

                foreach (OpLocationEquipment equipment in installedEquipments)
                {
                    NewEntry(dataProvider, location, equipment.Meter, equipment.Device, startTime, useDBCollector);

                    if (equipment.Device != null && equipment.Device.DataList.Exists(DataType.DEVICE_CID) &&
                        !location.IdLocationType.In((int)Enums.LocationType.DepositoryLocation,
                                                    (int)Enums.LocationType.ServiceLocation))
                    {
                        equipment.Device.DataList.SetValue(DataType.DEVICE_CID, location.CID);
                        DeviceComponent.SaveDeviceData(dataProvider, equipment.Device, useDBCollector);
                    }

                    // !!! Skopiować parametry z Location do Device?? (Timery itp)
                }
            }
            else
            {
                // Meter nie jest zainstalowany w ani jednej lokalizacji
                // Tworzymy nowy wpis instalujac tylko metera w lokalizacji [L,M,null]
                NewEntry(dataProvider, location, meter, null, startTime, useDBCollector);
            }
        }
        #endregion

        #region GetForMeter

        public static List<OpLocationEquipment> GetForMeter(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpMeter meter, bool useDBCollector = false)
        {
            return dataProvider.GetLocationEquipmentFilter
                (
                    IdMeter: new[] { meter.IdMeter },
                    EndTime: Data.DB.TypeDateTimeCode.Null(),
                    mergeIntoCache: useDBCollector == false,
                    useDBCollector: useDBCollector
                );
        }
        
        #endregion

        #region UninstallMeterFromLocation
        /// <param name="meter">Meter to uninstall</param>
        /// <param name="location">Location from meter should be uninstalled</param>
        /// <param name="bCreateNewEntry">false if do not create beginig entry</param>
        public static void UninstallMeterFromLocation(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpMeter meter, OpLocation location, bool bCreateNewEntry = true, bool useDBCollector = false)
        {
            List<OpLocationEquipment> equipments = dataProvider.GetLocationEquipmentFilter(IdMeter: new long[] { meter.IdMeter },
                                                                                           IdLocation: new long[] { location.IdLocation },
                                                                                           EndTime: Data.DB.TypeDateTimeCode.Null(),
                                                                                           useDBCollector: useDBCollector);
            foreach (OpLocationEquipment meterEquipment in equipments)
            {
                // konczymy wpis(y) [L,M,xxx]
                CloseEntry(dataProvider, meterEquipment, useDBCollector);

                if (meterEquipment.Device != null)
                {
                    // w meterze zainstalowane jest urządzenie, wiec musimy zostawic otwarty wpis [null,M,SN]
                    if (bCreateNewEntry) NewEntry(dataProvider, null, meter, meterEquipment.Device, useDBCollector: useDBCollector);
                }
            }
        }
        #endregion

        #region CloseEntry

        public static void CloseEntry(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocationEquipment equipment, bool useDBCollector = false)
        {
            equipment.EndTime = dataProvider.DateTimeNow;
            Save(dataProvider, equipment, useDBCollector);

            // To jest Artura, chyba jeszcze do poprawki (inne kombinacje??)
            if (equipment.Device != null && equipment.Location != null)
                equipment.Device.CurrentLocations.Remove(equipment.Location);
        }
        
        #endregion

        #region NewEntry

        public static OpLocationEquipment NewEntry(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocation location, OpMeter meter, OpDevice device, DateTime? startTime = null, bool useDBCollector = false)
        {
            if (!startTime.HasValue)
                startTime = dataProvider.DateTimeNow;
            OpLocationEquipment newEquipment = new OpLocationEquipment() { Location = location, Meter = meter, Device = device, StartTime = startTime.Value };
            return Save(dataProvider, newEquipment, useDBCollector);
        }

        #endregion

        #region Save

        public static OpLocationEquipment Save(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocationEquipment objectToSave, bool useDBCollector = false)
        {
            dataProvider.SaveLocationEquipment(objectToSave, useDBCollector);
            return dataProvider.GetLocationEquipment(objectToSave.IdLocationEquipment, useDBCollector);
        }

        #endregion

        #endregion
    }
}
