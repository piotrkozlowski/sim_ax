using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common.MSMQ;
using IMR.Suite.UI.Business.Objects;
using System.Resources;
using System.Globalization;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class SmsComponent : BaseComponent
    {
        public enum OperationType { Add, Delete, Change, ChangeStatus }
        public enum ShippingListOperationType { Close = 1 }

        private static ResourceManager ResourceManager = new ResourceManager(typeof(ResourcesText));

        public static string Host;
        public static string Queue;
        public static int IdActionType;

        public const int SmsThreshold = 10;

        #region CreateAndSendSms

        #region Task

        /// <summary>
        /// Constructs and sends SMS
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendSms(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpTask, OpTask> changedItems, OperationType operationType, bool useDBCollector)
        {
            List<OpOperator> operatorsToSms = ConstructSmsRecipientList(dataProvider, loggedOperator, changedItems);
            Dictionary<OpOperator, List<string>> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToSms, operationType, changedItems);
            return SendSms(dataProvider, addressesAndMessages, useDBCollector);
        }

        #endregion
        #region Issue

        /// <summary>
        /// Constructs and sends SMS
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendSms(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpIssue, OpIssue> changedItems, OperationType operationType, bool useDBCollector)
        {
            if (operationType != OperationType.ChangeStatus)
            {
                List<OpOperator> operatorsToSms = ConstructSmsRecipientList(dataProvider, loggedOperator, changedItems);
                Dictionary<OpOperator, List<string>> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToSms, operationType, changedItems);
                return SendSms(dataProvider, addressesAndMessages, useDBCollector);
            }

            return true;
        }

        #endregion
        #region WorkOrder

        /// <summary>
        /// Constructs and sends Sms
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendSms(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpRoute, OpRoute> changedItems, OperationType operationType, bool useDBCollector)
        {
            List<OpOperator> operatorsToSms = ConstructSmsRecipientList(loggedOperator, changedItems);
            Dictionary<OpOperator, List<string>> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToSms, operationType, changedItems);
            return SendSms(dataProvider, addressesAndMessages, useDBCollector);
        }

        #endregion
        #region Location

        /// <summary>
        /// Constructs and sends SMS
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendSms(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpLocation, OpLocation> changedItems, OperationType operationType, bool useDBCollector)
        {
            List<OpOperator> operatorsToSms = ConstructSmsRecipientList(dataProvider, loggedOperator, changedItems);
            Dictionary<OpOperator, List<string>> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToSms, operationType, changedItems);
            return SendSms(dataProvider, addressesAndMessages, useDBCollector);
        }

        #endregion

        #region NYI

        //#region Package

        ///// <summary>
        ///// Constructs and sends Sms
        ///// </summary>
        ///// <param name="dataProvider">DataProvider instance</param>
        ///// <param name="LoggedOperator">Operator currently logged</param>
        ///// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        ///// <param name="operationType">Type of changes made</param>
        ///// <returns>true if message sent successfully, false otherwise</returns>
        //public static bool CreateAndSendSms(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpPackage, OpPackage> changedItems, OperationType operationType, bool useDBCollector)
        //{
        //    List<OpOperator> operatorsToSms = ConstructSmsRecipientList(loggedOperator, changedItems);
        //    Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToSms, operationType, changedItems);
        //    return SendSms(dataProvider, addressesAndMessages, useDBCollector);
        //}

        //#endregion
        //#region ShippingList

        ///// <summary>
        ///// Constructs and sends Sms
        ///// </summary>
        ///// <param name="dataProvider">DataProvider instance</param>
        ///// <param name="LoggedOperator">Operator currently logged</param>
        ///// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        ///// <param name="operationType">Type of changes made</param>
        ///// <returns>true if message sent successfully, false otherwise</returns>
        //public static bool CreateAndSendSms(DataProvider dataProvider, OpOperator loggedOperator, Tuple<OpShippingList, OpShippingList> changedItems, ShippingListOperationType operationType, bool useDBCollector)
        //{
        //    List<OpOperator> operatorsToSms = ConstructSmsRecipientList(dataProvider, loggedOperator, changedItems, operationType);
        //    Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToSms, operationType, changedItems);
        //    return SendSms(dataProvider, addressesAndMessages, useDBCollector);
        //}

        //#endregion

        #endregion
        
        #endregion

        #region ConstructSmsRecipientList

        #region Location

        /// <summary>
        /// Constructs lists of operators to SMS
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to SMS</returns>
        public static List<OpOperator> ConstructSmsRecipientList(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpLocation, OpLocation> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;

            List<OpOperator> operatorsToSms = dataProvider.GetAllOperator().Where(x => x.DataList.Any(y => y.IdDataType == DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_LOCATION)).ToList();

            foreach (OpLocation location in changedItems.Keys)
            {
                List<OpOperator> addtionallRecipients = LocationComponent.GetLocationDistributorNotificationOperators(dataProvider, location, new List<OperationType>(){ OperationType.Add, 
                                                                                                                                                                         OperationType.Change,
                                                                                                                                                                         OperationType.Delete });
                addtionallRecipients.RemoveAll(o => o.IdOperator == loggedOperator.IdOperator);
                operatorsToSms.AddRange(addtionallRecipients);
            }

            operatorsToSms.RemoveAll(o => !o.SerialNbr.HasValue || o.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) == null);

            return operatorsToSms.Distinct().ToList();
        }

        #endregion
        #region Task

        /// <summary>
        /// Constructs lists of operators to SMS
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to SMS</returns>
        public static List<OpOperator> ConstructSmsRecipientList(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpTask, OpTask> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;
            List<OpTask> recipients;
            if (changedItems.Values.Contains(null))
                recipients = changedItems.Keys.ToList();
            else
                recipients = changedItems.Values.Concat(changedItems.Keys.Where(k => k.IdOperatorPerformer != changedItems.TryGetValue<OpTask, OpTask>(k).IdOperatorPerformer)).ToList();
            List<OpOperator> operatorsToSms = new List<OpOperator>();
            foreach (OpTask task in recipients)
            {
                if (loggedOperator != null && loggedOperator.IdOperator == task.IdOperatorRegistering)
                {
                    if (task.OperatorPerformer != null && task.OperatorPerformer.SerialNbr.HasValue && task.OperatorPerformer.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(task.OperatorPerformer);
                }
                else if (loggedOperator != null && loggedOperator.IdOperator == task.IdOperatorPerformer)
                {
                    if (task.OperatorRegistering != null && task.OperatorRegistering.SerialNbr.HasValue && task.OperatorRegistering.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(task.OperatorRegistering);
                }
                else
                {
                    if (task.OperatorRegistering != null && task.OperatorRegistering.SerialNbr.HasValue && task.OperatorRegistering.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(task.OperatorRegistering);
                    if (task.OperatorPerformer != null && task.OperatorPerformer.SerialNbr.HasValue && task.OperatorPerformer.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(task.OperatorPerformer);
                }

                List<OpOperator> addtionallRecipients = TaskComponent.GetTaskDistributorNotificationOperators(dataProvider, task, new List<OperationType>(){ OperationType.Add, 
                                                                                                                                                             OperationType.Change,
                                                                                                                                                             OperationType.Delete });
                addtionallRecipients.RemoveAll(o => o.IdOperator == loggedOperator.IdOperator);
                operatorsToSms.AddRange(addtionallRecipients);
            }

            return operatorsToSms.Distinct().ToList();
        }

        #endregion
        #region Issue

        /// <summary>
        /// Constructs lists of operators to SMS
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to SMS</returns>
        public static List<OpOperator> ConstructSmsRecipientList(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpIssue, OpIssue> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;
            List<OpIssue> recipients;
            if (changedItems.Values.Contains(null))
                recipients = changedItems.Keys.ToList();
            else
                recipients = changedItems.Values.Concat(changedItems.Keys.Where(k => k.IdOperatorPerformer != changedItems.TryGetValue<OpIssue, OpIssue>(k).IdOperatorPerformer)).ToList();
            List<OpOperator> operatorsToSms = new List<OpOperator>();
            List<OpOperator> issueTypeAdditionalConstraint = new List<OpOperator>();
            foreach (OpIssue issue in recipients)
            {
                if (loggedOperator != null && loggedOperator.IdOperator == issue.IdOperatorRegistering)
                {
                    if (issue.OperatorPerformer != null && issue.OperatorPerformer.SerialNbr.HasValue && issue.OperatorPerformer.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(issue.OperatorPerformer);
                }
                else if (loggedOperator != null && loggedOperator.IdOperator == issue.IdOperatorPerformer)
                {
                    if (issue.OperatorRegistering != null && issue.OperatorRegistering.SerialNbr.HasValue && issue.OperatorRegistering.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(issue.OperatorRegistering);
                }
                else
                {
                    if (issue.OperatorRegistering != null && issue.OperatorRegistering.SerialNbr.HasValue && issue.OperatorRegistering.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(issue.OperatorRegistering);
                    if (issue.OperatorPerformer != null && issue.OperatorPerformer.SerialNbr.HasValue && issue.OperatorPerformer.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(issue.OperatorPerformer);
                }
                List<OperationType> operationTypeList = new List<OperationType>(){ OperationType.Add,
                                                                               OperationType.Change,
                                                                               OperationType.Delete };
                List<OpOperator> addtionallRecipients = IssueComponent.GetIssueDistributorNotificationOperators(dataProvider, issue, operationTypeList);
                operatorsToSms.AddRange(addtionallRecipients);
            }
            return operatorsToSms.Distinct().ToList();
        }

        #endregion
        #region WorkOrder

        /// <summary>
        /// Constructs lists of operators to SMS
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to SMS</returns>
        public static List<OpOperator> ConstructSmsRecipientList(OpOperator loggedOperator, Dictionary<OpRoute, OpRoute> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;
            List<OpRoute> recipients;
            if (changedItems.Values.Contains(null))
                recipients = changedItems.Keys.ToList();
            else
                recipients = changedItems.Values.Concat(changedItems.Keys.Where(k => k.IdOperatorExecutor != changedItems.TryGetValue<OpRoute, OpRoute>(k).IdOperatorExecutor)).ToList();
            List<OpOperator> operatorsToSms = new List<OpOperator>();
            foreach (OpRoute route in recipients)
            {
                if (loggedOperator != null && loggedOperator.IdOperator == route.IdOperatorApproved)
                {
                    if (route.OperatorExecutor != null && route.OperatorExecutor.SerialNbr.HasValue && route.OperatorExecutor.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(route.OperatorExecutor);
                }
                else if (loggedOperator != null && loggedOperator.IdOperator == route.IdOperatorExecutor)
                {
                    if (route.OperatorApproved != null && route.OperatorApproved.SerialNbr.HasValue && route.OperatorApproved.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(route.OperatorApproved);
                }
                else
                {
                    if (route.OperatorApproved != null && route.OperatorApproved.SerialNbr.HasValue && route.OperatorApproved.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(route.OperatorApproved);
                    if (route.OperatorExecutor != null && route.OperatorExecutor.SerialNbr.HasValue && route.OperatorExecutor.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                        operatorsToSms.Add(route.OperatorExecutor);
                }
            }
            return operatorsToSms.Distinct().ToList();
        }

        #endregion

        #region NYI

        //#region Package

        ///// <summary>
        ///// Constructs lists of operators to Sms
        ///// </summary>
        ///// <param name="loggedOperator">Operator currently logged</param>
        ///// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        ///// <returns>List of operators to Sms</returns>
        //public static List<OpOperator> ConstructSmsRecipientList(OpOperator loggedOperator, Dictionary<OpPackage, OpPackage> changedItems)
        //{
        //    throw new NotImplementedException();

        //    //if (loggedOperator == null)
        //    //    loggedOperator = LoggedOperator;

        //    //List<OpOperator> operatorsToSms = new List<OpOperator>();

        //    //foreach (OpPackage pItem in changedItems.Keys)
        //    //{
        //    //    operatorsToSms.Add(pItem.OperatorCreator);
        //    //    if (pItem.OperatorReceiver != null)
        //    //        operatorsToSms.Add(pItem.OperatorReceiver);
        //    //}

        //    //foreach (OpPackage pItem in changedItems.Values.Where(v => v != null))
        //    //{
        //    //    operatorsToSms.Add(pItem.OperatorCreator);
        //    //    if (pItem.OperatorReceiver != null)
        //    //        operatorsToSms.Add(pItem.OperatorReceiver);
        //    //}

        //    //operatorsToSms = operatorsToSms.Distinct().ToList();
        //    //operatorsToSms.RemoveAll(o => !o.SerialNbr.HasValue || o.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) == null);

        //    //return operatorsToSms;
        //}

        //#endregion
        //#region ShippingList

        ///// <summary>
        ///// Constructs lists of operators to SMS
        ///// </summary>
        ///// <param name="loggedOperator">Operator currently logged</param>
        ///// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        ///// <returns>List of operators to SMS</returns>
        //public static List<OpOperator> ConstructSmsRecipientList(DataProvider dataProvider, OpOperator loggedOperator, Tuple<OpShippingList, OpShippingList> changedItems, ShippingListOperationType operationType)
        //{
        //    throw new NotImplementedException();

        //    //if (loggedOperator == null)
        //    //    loggedOperator = LoggedOperator;

        //    //List<OpOperator> operatorsToSms = new List<OpOperator>();

        //    ////operatorsToSms.AddRange(dataProvider.GetOperatorShippingListMailingList(changedItems.Item2.Distributor.IdDistributor, (int)operationType));
        //    //operatorsToSms.AddRange(dataProvider.GetOperatorShippingListSmsList(changedItems.Item2.Distributor.IdDistributor, (int)operationType));

        //    //operatorsToSms.RemoveAll(o => !o.SerialNbr.HasValue || o.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) == null);

        //    //return operatorsToSms.Distinct().ToList();
        //}

        //#endregion

        #endregion

        #endregion

        #region ConstructMessageBody

        #region Task
        /// <summary>
        /// Constructs mail body and returns it along with Sms address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToSms">List of operators to Sms</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, List<string>> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToSms, OperationType operationType, Dictionary<OpTask, OpTask> changedItems)
        {
            Dictionary<OpOperator, List<string>> ret = new Dictionary<OpOperator, List<string>>();

            #region Message

            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Delete);
                    bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;

                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_Change_3__4_By_5_", recipient.Actor),
                                            GetTranslatedString("Task", recipient.Actor),
                                            task.IdTask,
                                            GetTranslatedString("Status", recipient.Actor),
                                            task.TaskStatus,
                                            changedItems.TryGetValue<OpTask, OpTask>(task).TaskStatus,
                                            LoggedOperator.ToString());

                                    ret.Add(recipient, text);
                                }
                            }

                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_Change_3__4_By_5_", recipient.Actor),
                                            GetTranslatedString("Task", recipient.Actor),
                                            task.IdTask,
                                            GetTranslatedString("Status", recipient.Actor),
                                            task.TaskStatus,
                                            changedItems.TryGetValue<OpTask, OpTask>(task).TaskStatus,
                                            LoggedOperator.ToString());

                                    ret.Add(recipient, text);
                                }
                            }

                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;
                            foreach (OpTask task in changedItems.Keys)
                            {
                                string text = "";
                                if (task.IdOperatorPerformer == recipient.IdOperator)
                                {
                                    text = string.Format(GetTranslatedString("IMRSMSYouHaveCreated_0_ID_1_", recipient.Actor),
                                        GetTranslatedString("Task", recipient.Actor),
                                        task.IdTask);
                                }
                                if (task.IdOperatorRegistering == recipient.IdOperator)
                                {
                                    text = string.Format(GetTranslatedString("IMRSMS_0_ID_1_AssignedToYouBy_2_", recipient.Actor),
                                        GetTranslatedString("Task", recipient.Actor),
                                        task.IdTask,
                                        LoggedOperator.ToString());
                                }

                                ret.Add(recipient, text);
                            }
                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator || changedItems.TryGetValue<OpTask, OpTask>(t).IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)) &&
                                    (change || (assigned && changedItems.TryGetValue<OpTask, OpTask>(task).IdOperatorPerformer != recipient.IdOperator)))
                                {
                                    OpTask oldVal = changedItems.TryGetValue<OpTask, OpTask>(task);

                                    int paramCount = 0;
                                    StringBuilder sb = new StringBuilder();

                                    sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor), GetTranslatedString("Task", recipient.Actor), task.IdTask);

                                    if (oldVal.OperatorPerformer != task.OperatorPerformer) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("OperatorPerformer", recipient.Actor), oldVal.OperatorPerformer, task.OperatorPerformer); }
                                    if (oldVal.IdIssue != task.IdIssue) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Id", recipient.Actor), oldVal.IdIssue, task.IdIssue); }
                                    if (oldVal.TaskType != task.TaskType) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Type", recipient.Actor), oldVal.TaskType.Name, task.TaskType.Name); }
                                    if (oldVal.Deadline != task.Deadline) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Deadline", recipient.Actor), oldVal.Deadline, task.Deadline); }
                                    if (oldVal.Priority != task.Priority) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Priority", recipient.Actor), oldVal.Priority, task.Priority); }
                                    if (oldVal.Location != task.Location) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Location", recipient.Actor), oldVal.Location, task.Location); }
                                    if (oldVal.Route != task.Route) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Route", recipient.Actor), oldVal.Route, task.Route); }

                                    if (paramCount > 1)
                                    {
                                        sb.Clear();
                                        sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor) + ".", GetTranslatedString("Task", recipient.Actor), task.IdTask);
                                    }

                                    if (oldVal.TaskStatus != task.TaskStatus) { sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Status", recipient.Actor), oldVal.TaskStatus, task.TaskStatus); }

                                    sb.AppendFormat("\n" + GetTranslatedString("by_0_", recipient.Actor), LoggedOperator);

                                    ret.Add(recipient, sb.ToString());
                                }
                            }
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (change && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    OpTask oldVal = changedItems.TryGetValue<OpTask, OpTask>(task);

                                    int paramCount = 0;
                                    StringBuilder sb = new StringBuilder();

                                    sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor), GetTranslatedString("Task", recipient.Actor), task.IdTask);

                                    if (oldVal.OperatorPerformer != task.OperatorPerformer) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("OperatorPerformer", recipient.Actor), oldVal.OperatorPerformer, task.OperatorPerformer); }
                                    if (oldVal.IdIssue != task.IdIssue) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Id", recipient.Actor), oldVal.IdIssue, task.IdIssue); }
                                    
                                    if (oldVal.TaskType != task.TaskType) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Type", recipient.Actor), oldVal.TaskType.Name, task.TaskType.Name); }
                                    if (oldVal.Deadline != task.Deadline) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Deadline", recipient.Actor), oldVal.Deadline, task.Deadline); }
                                    if (oldVal.Priority != task.Priority) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Priority", recipient.Actor), oldVal.Priority, task.Priority); }
                                    if (oldVal.Location != task.Location) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Location", recipient.Actor), oldVal.Location, task.Location); }
                                    if (oldVal.Route != task.Route) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Route", recipient.Actor), oldVal.Route, task.Route); }

                                    if (paramCount > 1)
                                    {
                                        sb.Clear();
                                        sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor) + ".", GetTranslatedString("Task", recipient.Actor), task.IdTask);
                                    }

                                    if (oldVal.TaskStatus != task.TaskStatus) { sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Status", recipient.Actor), oldVal.TaskStatus, task.TaskStatus); }

                                    sb.AppendFormat("\n" + GetTranslatedString("by_0_", recipient.Actor), LoggedOperator);

                                    ret.Add(recipient, sb.ToString());
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;

                            foreach (OpTask task in changedItems.Keys)
                            {
                                string text = "";
                                if (task.IdOperatorPerformer == recipient.IdOperator)
                                {
                                    text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_HasBeenDeleted", recipient.Actor),
                                        GetTranslatedString("Task", recipient.Actor),
                                        task.IdTask,
                                        GetTranslatedString("AssignedToYou", recipient.Actor));
                                }
                                if (task.IdOperatorRegistering == recipient.IdOperator)
                                {
                                    text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_HasBeenDeleted", recipient.Actor),
                                        GetTranslatedString("Task", recipient.Actor),
                                        task.IdTask,
                                        GetTranslatedString("YouHaveCreated", recipient.Actor));
                                }

                                ret.Add(recipient, text);
                            }
                            break;
                        #endregion
                    }
                    
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            } 

            #endregion

            return ret;
        }
        #endregion
        #region Issue
        /// <summary>
        /// Constructs mail body and returns it along with Sms address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToSms">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, List<string>> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToSms, OperationType operationType, Dictionary<OpIssue, OpIssue> changedItems)
        {
            Dictionary<OpOperator, List<string>> ret = new Dictionary<OpOperator, List<string>>();
            #region AdditionalConstraints
            List<long> dataTypeToGet = new List<long>() { DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR,
                                                          DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE };

            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, IdDataType: dataTypeToGet.ToArray(), IdOperator: operatorsToSms.Select(s => s.IdOperator).ToArray());
            #endregion
            #region msg

            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Delete);
                    bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_Change_3__4_By_5_", recipient.Actor),
                                            GetTranslatedString("Issue", recipient.Actor),
                                            issue.IdIssue,
                                            GetTranslatedString("Status", recipient.Actor),
                                            issue.IssueStatus,
                                            changedItems.TryGetValue<OpIssue, OpIssue>(issue).IssueStatus,
                                            LoggedOperator.ToString());

                                    ret.Add(recipient, text);
                                }
                            }
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_Change_3__4_By_5_", recipient.Actor),
                                            GetTranslatedString("Issue", recipient.Actor),
                                            issue.IdIssue,
                                            GetTranslatedString("Status", recipient.Actor),
                                            issue.IssueStatus,
                                            changedItems.TryGetValue<OpIssue, OpIssue>(issue).IssueStatus,
                                            LoggedOperator.ToString());

                                    ret.Add(recipient, text);
                                }
                            }
                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;

                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1_AssignedToYouBy_2_", recipient.Actor),
                                        GetTranslatedString("Issue", recipient.Actor),
                                        issue.IdIssue,
                                        LoggedOperator.ToString());

                                    ret.Add(recipient, text);
                                }
                            }

                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (add && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMSYouHaveCreated_0_ID_1_", recipient.Actor),
                                        GetTranslatedString("Issue", recipient.Actor),
                                        issue.IdIssue);

                                    ret.Add(recipient, text);
                                }
                            }

                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator || changedItems.TryGetValue<OpIssue, OpIssue>(t).IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)) &&
                                    (change || (assigned && changedItems.TryGetValue<OpIssue, OpIssue>(issue).IdOperatorPerformer != recipient.IdOperator)))
                                {
                                    OpIssue oldVal = changedItems.TryGetValue<OpIssue, OpIssue>(issue);

                                    int paramCount = 0;
                                    StringBuilder sb = new StringBuilder();

                                    sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor), GetTranslatedString("Issue", recipient.Actor), issue.IdIssue);

                                    if (oldVal.OperatorPerformer != issue.OperatorPerformer) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("OperatorPerformer", recipient.Actor), oldVal.OperatorPerformer, issue.OperatorPerformer); }
                                    if (oldVal.IssueType != issue.IssueType) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Type", recipient.Actor), oldVal.IssueType.Description, issue.IssueType.Description); }
                                    if (oldVal.Deadline != issue.Deadline) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Deadline", recipient.Actor), oldVal.Deadline, issue.Deadline); }
                                    if (oldVal.Priority != issue.Priority) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Priority", recipient.Actor), oldVal.Priority, issue.Priority); }
                                    if (oldVal.Location != issue.Location) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Location", recipient.Actor), oldVal.Location, issue.Location); }

                                    if (paramCount > 1)
                                    {
                                        sb.Clear();
                                        sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor) + ".", GetTranslatedString("Issue", recipient.Actor), issue.IdIssue);
                                    }

                                    if (oldVal.IssueStatus != issue.IssueStatus) { sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Status", recipient.Actor), oldVal.IssueStatus, issue.IssueStatus); }

                                    sb.AppendFormat("\n"+GetTranslatedString("by_0_", recipient.Actor), LoggedOperator);

                                    ret.Add(recipient, sb.ToString());
                                }
                            }
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (change && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssue oldVal = changedItems.TryGetValue<OpIssue, OpIssue>(issue);

                                    int paramCount = 0;
                                    StringBuilder sb = new StringBuilder();
                                    
                                    sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor), GetTranslatedString("Issue", recipient.Actor), issue.IdIssue);

                                    if (oldVal.OperatorPerformer != issue.OperatorPerformer) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("OperatorPerformer", recipient.Actor), oldVal.OperatorPerformer, issue.OperatorPerformer); }
                                    if (oldVal.IssueType != issue.IssueType) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Type", recipient.Actor), oldVal.IssueType.Description, issue.IssueType.Description); }
                                    if (oldVal.Deadline != issue.Deadline) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Deadline", recipient.Actor), oldVal.Deadline, issue.Deadline); }
                                    if (oldVal.Priority != issue.Priority) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Priority", recipient.Actor), oldVal.Priority, issue.Priority); }
                                    if (oldVal.Location != issue.Location) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Location", recipient.Actor), oldVal.Location, issue.Location); }

                                    if (paramCount > 1)
                                    {
                                        sb.Clear();
                                        sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor) + ".", GetTranslatedString("Issue", recipient.Actor), issue.IdIssue);
                                    }

                                    if (oldVal.IssueStatus != issue.IssueStatus) { sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Status", recipient.Actor), oldVal.IssueStatus, issue.IssueStatus); }

                                    sb.AppendFormat("\n" + GetTranslatedString("by_0_", recipient.Actor), LoggedOperator);

                                    ret.Add(recipient, sb.ToString());
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_HasBeenDeleted", recipient.Actor),
                                        GetTranslatedString("Issue", recipient.Actor),
                                        issue.IdIssue,
                                        GetTranslatedString("AssignedToYou", recipient.Actor));

                                    ret.Add(recipient, text);
                                }
                            }
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_HasBeenDeleted", recipient.Actor),
                                        GetTranslatedString("Issue", recipient.Actor),
                                        issue.IdIssue,
                                        GetTranslatedString("YouHaveCreated", recipient.Actor));

                                    ret.Add(recipient, text);
                                }
                            }
                            break;
                        #endregion
                    }
                    if (strBody.Length > 0)
                        ret.Add(recipient, strBody.ToString());
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            } 
            #endregion

            return ret;
        }
        #endregion
        #region Location
        /// <summary>
        /// Constructs mail body and returns it along with Sms address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToSms">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, List<string>> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToSms, OperationType operationType, Dictionary<OpLocation, OpLocation> changedItems)
        {
            #region msg

            Dictionary<OpOperator, List<string>> ret = new Dictionary<OpOperator, List<string>>();
            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Delete);
                    //bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            {
                                if (!change) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((Enums.LocationState)location.LocationStateType.IdLocationStateType)))
                                    {
                                        OpLocation oldVal = changedItems.TryGetValue(location);

                                        ret.Add(recipient, string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_Change_3__4_By_5_", recipient.Actor),
                                            GetTranslatedString("Location", recipient.Actor),
                                            location.IdLocation,
                                            GetTranslatedString("State", recipient.Actor),
                                            oldVal != null ? oldVal.LocationStateType.ToString() : string.Empty,
                                            location.LocationStateType.ToString(),
                                            LoggedOperator.ToString()));
                                    }
                                }

                                break;
                            }
                        #endregion
                        #region Add
                        case OperationType.Add:
                            {
                                //if (!add && !assigned) break;
                                if (!add) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    OpLocationStateHistory lastHist = LocationComponent.GetStateHistory(dataProvider, location.IdLocation).LastOrDefault();

                                    ret.Add(recipient, string.Format(GetTranslatedString("IMRSMSAdded_0_ID_1_By_2_", recipient.Actor),
                                        GetTranslatedString("Location", recipient.Actor),
                                        location.IdLocation,
                                        LoggedOperator.ToString()));
                                }

                                break;
                            }
                        #endregion
                        #region Change
                        case OperationType.Change:
                            {
                                //if (!change && !assigned) break;
                                if (!change) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((Enums.LocationState)location.LocationStateType.IdLocationStateType)) && change)
                                    {
                                        OpLocation oldVal = changedItems.TryGetValue<OpLocation, OpLocation>(location);

                                        int paramCount = 0;
                                        StringBuilder sb = new StringBuilder();

                                        sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor), GetTranslatedString("Location", recipient.Actor), location.IdLocation);

                                        if (oldVal.IdLocationPattern != location.IdLocationPattern) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.IdLocationPattern, oldVal.IdLocationPattern.HasValue ? oldVal.IdLocationPattern.ToString() : string.Empty, location.IdLocationPattern.HasValue ? location.IdLocationPattern.ToString() : string.Empty); }
                                        if (oldVal.City != location.City) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.City, oldVal.City != null ? oldVal.City : string.Empty, location.City != null ? location.City : string.Empty); }
                                        if (oldVal.Name != location.Name) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Name, oldVal.Name != null ? oldVal.Name : string.Empty, location.Name != null ? location.Name : string.Empty); }
                                        if (oldVal.CID != location.CID) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.CID, oldVal.CID != null ? oldVal.CID : string.Empty, location.CID != null ? location.CID : string.Empty); }
                                        if (oldVal.InKpi != location.InKpi) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.InKpi, oldVal.InKpi, location.InKpi); }
                                        if (oldVal.Address != location.Address) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Address, oldVal.Address != null ? oldVal.Address : string.Empty, location.Address != null ? location.Address : string.Empty); }
                                        if (oldVal.PostalCode != location.PostalCode) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.PostalCode, oldVal.PostalCode != null ? oldVal.PostalCode : string.Empty, location.PostalCode != null ? location.PostalCode : string.Empty); }
                                        if (oldVal.Country != location.Country) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Country, oldVal.Country != null ? oldVal.Country : string.Empty, location.Country != null ? location.Country : string.Empty); }
                                        if (oldVal.Actor != location.Actor) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Actor, oldVal.Actor != null ? oldVal.Actor.ToString() : string.Empty, location.Actor != null ? location.Actor.ToString() : string.Empty); }
                                        if (oldVal.Consumer != location.Consumer) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Consumer, oldVal.Consumer != null ? oldVal.Consumer.ToString() : string.Empty, location.Consumer != null ? location.Consumer.ToString() : string.Empty); }
                                        if (oldVal.CreationDate != location.CreationDate) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.CreationDate, oldVal.CreationDate != null ? oldVal.CreationDate.Value.ToString("yyyy-MM-dd") : string.Empty, location.CreationDate != null ? location.CreationDate.Value.ToString("yyyy-MM-dd") : string.Empty); }
                                        if (oldVal.LocationType != location.LocationType) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.LocationType, oldVal.LocationType != null ? oldVal.LocationType.ToString() : string.Empty, location.LocationType != null ? location.LocationType.ToString() : string.Empty); }
                                        if (oldVal.Distributor != location.Distributor) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Distributor, oldVal.Distributor != null ? oldVal.Distributor.ToString() : string.Empty, location.Distributor != null ? location.Distributor.ToString() : string.Empty); }
                                        if (oldVal.Latitude != location.Latitude) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Latitude, oldVal.Latitude != null ? oldVal.Latitude.ToString() : string.Empty, location.Latitude != null ? location.Latitude.ToString() : string.Empty); }
                                        if (oldVal.Longitude != location.Longitude) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.Longitude, oldVal.Longitude != null ? oldVal.Longitude.ToString() : string.Empty, location.Longitude != null ? location.Longitude.ToString() : string.Empty); }
                                        if (oldVal.HostCID != location.HostCID) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.HostCID, oldVal.HostCID != null ? oldVal.HostCID.ToString() : string.Empty, location.HostCID != null ? location.HostCID.ToString() : string.Empty); }
                                        if (oldVal.ImrServer != location.ImrServer) { sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.ImrServer, oldVal.ImrServer != null ? oldVal.ImrServer.ToString() : string.Empty, location.ImrServer != null ? location.ImrServer.ToString() : string.Empty); }
                                        if (oldVal.MaxTruckSize != location.MaxTruckSize) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.MaxTruckSize, oldVal.MaxTruckSize != null ? oldVal.MaxTruckSize.ToString() : string.Empty, location.MaxTruckSize != null ? location.MaxTruckSize.ToString() : string.Empty); }

                                        if (paramCount > 1)
                                        {
                                            sb.Clear();
                                            sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor) + ".", GetTranslatedString("Location", recipient.Actor), location.IdLocation);
                                        }

                                        if (oldVal.LocationStateType != location.LocationStateType) { sb.AppendFormat("\n{0}:{1}-{2}", ResourcesText.LocationStateType, oldVal.LocationStateType != null ? oldVal.LocationStateType.ToString() : string.Empty, location.LocationStateType != null ? location.LocationStateType.ToString() : string.Empty); }

                                        sb.AppendFormat("\n" + GetTranslatedString("by_0_", recipient.Actor), LoggedOperator);

                                        ret.Add(recipient, sb.ToString());
                                    }
                                }

                                break;
                            }
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            {
                                if (!delete) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    ret.Add(recipient, string.Format(GetTranslatedString("IMRSMSDeleted_0_ID_1_By_2_", recipient.Actor),
                                        GetTranslatedString("Location", recipient.Actor),
                                        location.IdLocation,
                                        LoggedOperator.ToString()));
                                }
                                break;
                            }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            } 
            #endregion

            return ret;
        }
        #endregion        
        #region WorkOrder
        /// <summary>
        /// Constructs mail body and returns it along with Sms address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToSms">List of operators to Sms</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="subject">returned Email subject</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, List<string>> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToSms, OperationType operationType, Dictionary<OpRoute, OpRoute> changedItems)
        {
            Dictionary<OpOperator, List<string>> ret = new Dictionary<OpOperator, List<string>>();

            #region msg

            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Delete);
                    bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;

                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {       
                                    OpRoute oldVal = changedItems.TryGetValue<OpRoute, OpRoute>(workOrder);

                                    ret.Add(recipient, string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_Change_3__4_By_5_", recipient.Actor),
                                            GetTranslatedString("Route", recipient.Actor),
                                            GetTranslatedString("Status", recipient.Actor),
                                            workOrder.IdRoute,
                                            oldVal != null ? oldVal.RouteStatus.ToString() : string.Empty,
                                            workOrder.RouteStatus,
                                            LoggedOperator.ToString()));
                                }
                            }
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    OpRoute oldVal = changedItems.TryGetValue<OpRoute, OpRoute>(workOrder);

                                    ret.Add(recipient, string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_Change_3__4_By_5_", recipient.Actor),
                                            GetTranslatedString("Route", recipient.Actor),
                                            GetTranslatedString("Status", recipient.Actor),
                                            workOrder.IdRoute,
                                            oldVal != null ? oldVal.RouteStatus.ToString() : string.Empty,
                                            workOrder.RouteStatus.ToString(),
                                            LoggedOperator.ToString()));
                                }
                            }
                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator))
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1_HasBeenCreatedAnd_2_", recipient.Actor),
                                        GetTranslatedString("Route", recipient.Actor),
                                        workOrder.IdRoute,
                                        GetTranslatedString("AssignedToYou", recipient.Actor));

                                    ret.Add(recipient, text);
                                }
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                                if (add && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1_HasBeenCreatedAnd_2_", recipient.Actor),
                                        GetTranslatedString("Route", recipient.Actor),
                                        workOrder.IdRoute,
                                        GetTranslatedString("YouHaveApproved", recipient.Actor));

                                    ret.Add(recipient, text);
                                }
                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator || changedItems.TryGetValue<OpRoute, OpRoute>(t).IdOperatorExecutor == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)) &&
                                    (change || (assigned && changedItems.TryGetValue<OpRoute, OpRoute>(workOrder).IdOperatorExecutor != recipient.IdOperator)))
                                {
                                    OpRoute oldVal = changedItems.TryGetValue<OpRoute, OpRoute>(workOrder);

                                    int paramCount = 0;
                                    StringBuilder sb = new StringBuilder();

                                    sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor), GetTranslatedString("Route", recipient.Actor), workOrder.IdRoute);

                                    if (oldVal.OperatorExecutor != workOrder.OperatorExecutor) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("OperatorExecutor", recipient.Actor), oldVal.OperatorExecutor, workOrder.OperatorExecutor); }
                                    if (oldVal.Symbol != workOrder.Symbol) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Symbol", recipient.Actor), oldVal.Symbol, workOrder.Symbol); }

                                    if (paramCount > 1)
                                    {
                                        sb.Clear();
                                        sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor) + ".", GetTranslatedString("Route", recipient.Actor), workOrder.IdRoute);
                                    }

                                    if (oldVal.RouteStatus != workOrder.RouteStatus) { sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Status", recipient.Actor), oldVal.RouteStatus.ToString(), workOrder.RouteStatus.ToString()); }

                                    sb.AppendFormat("\n" + GetTranslatedString("by_0_", recipient.Actor), LoggedOperator);

                                    ret.Add(recipient, sb.ToString());
                                }
                            }
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                            {
                                if (change && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    OpRoute oldVal = changedItems.TryGetValue<OpRoute, OpRoute>(workOrder);

                                    int paramCount = 0;
                                    StringBuilder sb = new StringBuilder();

                                    sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor), GetTranslatedString("Route", recipient.Actor), workOrder.IdRoute);

                                    if (oldVal.OperatorExecutor != workOrder.OperatorExecutor) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("OperatorExecutor", recipient.Actor), oldVal.OperatorExecutor, workOrder.OperatorExecutor); }
                                    if (oldVal.Symbol != workOrder.Symbol) { paramCount++; sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Symbol", recipient.Actor), oldVal.Symbol, workOrder.Symbol); }

                                    if (paramCount > 1)
                                    {
                                        sb.Clear();
                                        sb.AppendFormat(GetTranslatedString("IMRSMS_0_ID_1_ParamsChange", recipient.Actor) + ".", GetTranslatedString("Route", recipient.Actor), workOrder.IdRoute);
                                    }

                                    if (oldVal.RouteStatus != workOrder.RouteStatus) { sb.AppendFormat("\n{0}:{1}-{2}", GetTranslatedString("Status", recipient.Actor), oldVal.RouteStatus.ToString(), workOrder.RouteStatus.ToString()); }

                                    sb.AppendFormat("\n" + GetTranslatedString("by_0_", recipient.Actor), LoggedOperator);

                                    ret.Add(recipient, sb.ToString());
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator))
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_HasBeenDeleted", recipient.Actor),
                                        GetTranslatedString("Route", recipient.Actor),
                                        workOrder.IdRoute,
                                        GetTranslatedString("AssignedToYou", recipient.Actor));

                                    ret.Add(recipient, text);
                                }
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    string text = string.Format(GetTranslatedString("IMRSMS_0_ID_1__2_HasBeenDeleted", recipient.Actor),
                                        GetTranslatedString("Route", recipient.Actor),
                                        workOrder.IdRoute,
                                        GetTranslatedString("YouHaveApproved", recipient.Actor));

                                    ret.Add(recipient, text);
                                }
                            break;
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            }
            #endregion

            return ret;
        }
        #endregion

        #region NYI
        
        //#region ShippingList
        ///// <summary>
        ///// Constructs mail body and returns it along with Email address
        ///// </summary>
        ///// <param name="dataProvider">DataProvider instance</param>
        ///// <param name="operatorsToEmail">List of operators to Email</param>
        ///// <param name="operationType">Type of changes made</param>
        ///// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        ///// <param name="subject">returned Email subject</param>
        ///// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        //public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail, ShippingListOperationType operationType, Tuple<OpShippingList, OpShippingList> changedItems)
        //{
        //    throw new NotImplementedException();

        //    //#region Subject
        //    //switch (operationType)
        //    //{
        //    //    case ShippingListOperationType.Close:
        //    //        subject = String.Format(ResourcesText.ShippinListHasBeenClosed, changedItems.Item2.IdShippingList);
        //    //        break;
        //    //    default:
        //    //        subject = "";
        //    //        break;
        //    //}
        //    //#endregion
        //    //#region msg

        //    //Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
        //    //List<OpDistributorData> distrData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, IdDistributor: new int[] { changedItems.Item2.IdDistributor }, IdDataType: new long[] { DataType.DISTRIBUTOR_SHIPPING_MAIL_NOTE });
        //    //string Note = "";
        //    //if (distrData.Count > 0)
        //    //    Note = (string)distrData[0].Value;
        //    //foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
        //    //{
        //    //    try
        //    //    {
        //    //        StringBuilder strBody = new StringBuilder();

        //    //        switch (operationType)
        //    //        {
        //    //            case ShippingListOperationType.Close:
        //    //                strBody.AppendLine("<b>ID:</b> " + changedItems.Item2.IdShippingList + "<br/>");
        //    //                strBody.AppendLine("<b>" + ResourcesText.Distributor + ":</b> " + changedItems.Item2./*Location.*/Distributor + "<br/>");
        //    //                strBody.AppendLine("<b>" + ResourcesText.ShippingListType + ":</b> " + changedItems.Item2.ShippingListType + "<br/>");
        //    //                strBody.AppendLine("<b>" + ResourcesText.ClosingPerson + ":</b> " + LoggedOperator.ToString() + "<br/>");
        //    //                strBody.AppendLine("<b>" + ResourcesText.DevicesAmount + ":</b> " + changedItems.Item2.DevicesAmount + "<br/>");
        //    //                strBody.AppendLine("<b>" + ResourcesText.DistributorAdditionalNote + "</b> " + Note.Replace("\r\n", "<br/>") + "<br/>");
        //    //                break;
        //    //            default:
        //    //                break;
        //    //        }
        //    //        if (strBody.Length > 0)
        //    //            ret.Add(recipient, strBody.ToString());
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
        //    //    }
        //    //} 
        //    //#endregion
        //    //return ret;
        //}
        //#endregion
        //#region Package
        ///// <summary>
        ///// Constructs mail body and returns it along with Email address
        ///// </summary>
        ///// <param name="dataProvider">DataProvider instance</param>
        ///// <param name="operatorsToEmail">List of operators to Email</param>
        ///// <param name="operationType">Type of changes made</param>
        ///// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        ///// <param name="subject">returned Email subject</param>
        ///// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        //public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail,
        //    OperationType operationType, Dictionary<OpPackage, OpPackage> changedItems,
        //    Dictionary<OpPackage, string> AdditionallInfo = null)
        //{
        //    throw new NotImplementedException();

        //    //#region Subject
        //    //switch (operationType)
        //    //{
        //    //    case OperationType.ChangeStatus:
        //    //        subject = String.Format(ResourcesText.SubjectPackageStatusChange, String.Join(", ", changedItems.Keys.Select(item => item.ToString())));
        //    //        break;
        //    //    default:
        //    //        subject = "";
        //    //        break;
        //    //}
        //    //#endregion
        //    //#region msg

        //    //Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
        //    //foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
        //    //{
        //    //    try
        //    //    {
        //    //        StringBuilder strBody = new StringBuilder();

        //    //        switch (operationType)
        //    //        {
        //    //            #region ChangeStatus
        //    //            case OperationType.ChangeStatus:
        //    //                foreach (OpPackage package in changedItems.Keys.Where(t => t.IdOperatorReceiver == recipient.IdOperator))
        //    //                {
        //    //                    if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((Enums.PackageStatus)package.IdPackageStatus)))
        //    //                    {
        //    //                        string lastPackageStatus = "-";
        //    //                        string currentPackageStaus = package.PackageStatus.ToString();
        //    //                        if (changedItems.ContainsKey(package) && changedItems[package] != null)
        //    //                            lastPackageStatus = changedItems[package].PackageStatus.ToString();
        //    //                        strBody.AppendFormat(ResourcesText.PackageStatusChange, ResourcesText.AssignedToYou, package.IdPackage, lastPackageStatus, currentPackageStaus);

        //    //                        if (AdditionallInfo != null && AdditionallInfo.ContainsKey(package) && !String.IsNullOrEmpty(AdditionallInfo[package]))
        //    //                        {
        //    //                            strBody.AppendLine("<br>" + AdditionallInfo[package]);
        //    //                        }
        //    //                    }
        //    //                }
        //    //                foreach (OpPackage package in changedItems.Keys.Where(t => t != null && t.IdOperatorCreator == recipient.IdOperator))
        //    //                {
        //    //                    if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((Enums.PackageStatus)package.IdPackageStatus)))
        //    //                    {
        //    //                        string lastPackageStatus = "-";
        //    //                        string currentPackageStaus = package.PackageStatus.ToString();
        //    //                        if (changedItems.ContainsKey(package) && changedItems[package] != null)
        //    //                            lastPackageStatus = changedItems[package].PackageStatus.ToString();
        //    //                        if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions(((Enums.PackageStatus)package.IdPackageStatus))))
        //    //                            strBody.AppendFormat(ResourcesText.PackageStatusChange, ResourcesText.YouHaveCreated, package.IdPackage, lastPackageStatus, currentPackageStaus);

        //    //                        if (AdditionallInfo != null && AdditionallInfo.ContainsKey(package) && !String.IsNullOrEmpty(AdditionallInfo[package]))
        //    //                        {
        //    //                            strBody.AppendLine("<br>" + AdditionallInfo[package]);
        //    //                        }
        //    //                    }
        //    //                }
        //    //                break;
        //    //            #endregion
        //    //        }
        //    //        if (strBody.Length > 0)
        //    //            ret.Add(recipient, strBody.ToString());
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
        //    //    }
        //    //} 
        //    //#endregion
        //    //return ret;
        //}
        //#endregion
        
        #endregion

        #endregion

        #region CountSMS

        #region Task

        public static int CountSMS(DataProvider dataProvider, OpOperator loggedOperator, OperationType operationType, List<OpTask> changedItems)
        {
            int count = 0;

            List<OpTask> recipients = changedItems;
            List<OpOperator> operatorsToSms = new List<OpOperator>();
            foreach (OpTask task in recipients)
            {
                if (task.OperatorPerformer != null && task.OperatorPerformer.SerialNbr.HasValue && task.OperatorPerformer.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                    operatorsToSms.Add(task.OperatorPerformer);
                if (task.OperatorRegistering != null && task.OperatorRegistering.SerialNbr.HasValue && task.OperatorRegistering.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                    operatorsToSms.Add(task.OperatorRegistering);

                List<OpOperator> addtionallRecipients = TaskComponent.GetTaskDistributorNotificationOperators(dataProvider, task, new List<OperationType>(){ OperationType.Add, 
                                                                                                                                                             OperationType.Change,
                                                                                                                                                             OperationType.Delete });
                addtionallRecipients.RemoveAll(o => o.IdOperator == loggedOperator.IdOperator);
                operatorsToSms.AddRange(addtionallRecipients);
            }

            operatorsToSms = operatorsToSms.Distinct().ToList();


            #region Message

            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Delete);
                    bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.TaskOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;

                            foreach (OpTask task in changedItems.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    count++;
                                }
                            }

                            foreach (OpTask task in changedItems.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    count++;
                                }
                            }

                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;
                            foreach (OpTask task in changedItems)
                            {
                                count++;
                            }
                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpTask task in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)) && (change || assigned))
                                {
                                    count++;
                                }
                            }
                            foreach (OpTask task in changedItems.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (change && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    count++;
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;

                            foreach (OpTask task in changedItems)
                            {
                                count++;
                            }
                            break;
                        #endregion
                    }

                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            }

            #endregion

            return count;
        }
        #endregion
        #region Issue

        public static int CountSMS(DataProvider dataProvider, OpOperator loggedOperator, OperationType operationType, List<OpIssue> changedItems)
        {
            int count = 0;

            List<OpIssue> recipients = changedItems;
            List<OpOperator> operatorsToSms = new List<OpOperator>();
            foreach (OpIssue issue in recipients)
            {
                if (issue.OperatorRegistering != null && issue.OperatorRegistering.SerialNbr.HasValue && issue.OperatorRegistering.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                    operatorsToSms.Add(issue.OperatorRegistering);
                if (issue.OperatorPerformer != null && issue.OperatorPerformer.SerialNbr.HasValue && issue.OperatorPerformer.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                    operatorsToSms.Add(issue.OperatorPerformer);

                List<OpOperator> addtionallRecipients = IssueComponent.GetIssueDistributorNotificationOperators(dataProvider, issue, new List<OperationType>(){ OperationType.Add, 
                                                                                                                                                             OperationType.Change,
                                                                                                                                                             OperationType.Delete });
                operatorsToSms.AddRange(addtionallRecipients);
            }

            operatorsToSms = operatorsToSms.Distinct().ToList();

            #region msg

            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Delete);
                    bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;
                            foreach (OpIssue issue in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    count++;
                                }
                            }
                            foreach (OpIssue issue in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    count++;
                                }
                            }
                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;

                            foreach (OpIssue issue in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    count++;
                                }
                            }

                            foreach (OpIssue issue in changedItems)
                            {
                                if (add && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    count++;
                                }
                            }

                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpIssue issue in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)) && (change || (assigned)))
                                {
                                    count++;
                                }
                            }
                            foreach (OpIssue issue in changedItems)
                            {
                                if (change && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    count++;
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;
                            foreach (OpIssue issue in changedItems)
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    count++;
                                }
                            foreach (OpIssue issue in changedItems)
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    count++;
                                }
                            break;
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            }
            #endregion

            return count;
        }
        #endregion
        #region Location

        public static int CountSMS(DataProvider dataProvider, OpOperator loggedOperator, OperationType operationType, List<OpLocation> changedItems)
        {
            #region msg

            List<OpOperator> operatorsToSms = dataProvider.GetAllOperator().Where(x => x.DataList.Any(y => y.IdDataType == DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_LOCATION)).ToList();
            foreach (OpLocation location in changedItems)
            {
                List<OpOperator> addtionallRecipients = LocationComponent.GetLocationDistributorNotificationOperators(dataProvider, location, new List<OperationType>(){ OperationType.Add, 
                                                                                                                                                                         OperationType.Change,
                                                                                                                                                                         OperationType.Delete });
                addtionallRecipients.RemoveAll(o => o.IdOperator == loggedOperator.IdOperator);
                operatorsToSms.AddRange(addtionallRecipients);
            }
            operatorsToSms.RemoveAll(o => !o.SerialNbr.HasValue || o.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) == null);
            operatorsToSms = operatorsToSms.Distinct().ToList();

            int count = 0;

            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Delete);
                    //bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.LocationOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            {
                                if (!change) break;

                                foreach (OpLocation location in changedItems)
                                {
                                    if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((Enums.LocationState)location.LocationStateType.IdLocationStateType)))
                                    {
                                        count++;
                                    }
                                }

                                break;
                            }
                        #endregion
                        #region Add
                        case OperationType.Add:
                            {
                                //if (!add && !assigned) break;
                                if (!add) break;

                                foreach (OpLocation location in changedItems)
                                {
                                    count++;
                                }

                                break;
                            }
                        #endregion
                        #region Change
                        case OperationType.Change:
                            {
                                //if (!change && !assigned) break;
                                if (!change) break;

                                foreach (OpLocation location in changedItems)
                                {
                                    if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((Enums.LocationState)location.LocationStateType.IdLocationStateType)) && change)
                                    {
                                        count++;
                                    }
                                }

                                break;
                            }
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            {
                                if (!delete) break;

                                foreach (OpLocation location in changedItems)
                                {
                                    count++;
                                }
                                break;
                            }
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            }
            #endregion

            return count;
        }
        #endregion
        #region WorkOrder

        public static int CountSMS(OpOperator loggedOperator, OperationType operationType, List<OpRoute> changedItems)
        {
            List<OpRoute> recipients = changedItems;
            List<OpOperator> operatorsToSms = new List<OpOperator>();
            foreach (OpRoute route in recipients)
            {
                if (route.OperatorExecutor != null && route.OperatorExecutor.SerialNbr.HasValue && route.OperatorExecutor.DataList.FirstOrDefault(d => d.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null)
                    operatorsToSms.Add(route.OperatorExecutor);
            }
            operatorsToSms = operatorsToSms.Distinct().ToList();

            int count = 0;

            #region msg

            foreach (OpOperator recipient in operatorsToSms.Where(o => o.SerialNbr.HasValue && o.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) != null))
            {
                try
                {
                    bool change = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Change);
                    bool add = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Add);
                    bool delete = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Delete);
                    bool assigned = recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.WorkOrderOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;

                            foreach (OpRoute workOrder in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    count++;
                                }
                            }
                            foreach (OpRoute workOrder in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    count++;
                                }
                            }
                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;
                            foreach (OpRoute workOrder in changedItems)
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    count++;
                                }
                            foreach (OpRoute workOrder in changedItems)
                                if (add && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    count++;
                                }
                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpRoute workOrder in changedItems)
                            {
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)) && (change || (assigned)))
                                {
                                    count++;
                                }
                            }
                            foreach (OpRoute workOrder in changedItems)
                            {
                                if (change && recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    count++;
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;
                            foreach (OpRoute workOrder in changedItems)
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    count++;
                                }
                            foreach (OpRoute workOrder in changedItems)
                                if (recipient.SmsNotificationOptions.GetNotify(SmsNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    count++;
                                }
                            break;
                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Login, ex.Message);
                }
            }
            #endregion

            return count;
        }
        #endregion

        #endregion

        #region SendSms

        public static bool SendSms(DataProvider dataProvider, Dictionary<OpOperator, List<string>> messages, bool useDBCollector)
        {
            var serverActionMSMQDict = ImrServerComponent.GetServerActionMSMQDict(dataProvider, null, useDBCollector);

            if (messages == null || messages.Count == 0)
            {
                IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, "-", "No messages to send at all.");
            }

            foreach (KeyValuePair<OpOperator, List<string>> recipient in messages)
            {
                if (recipient.Value == null || recipient.Value.Count == 0)
                {
                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Key.Login, "No messages to send for this operator.");
                }

                foreach (string msg in recipient.Value)
                {
                    try
                    {
                        OpOperatorData mobileOperator = recipient.Key.DataList.FirstOrDefault(x => x.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE);

                        // Jak nie ma numeru seryjnego operatora lub zdefiniowanego operatora sieci komorkowej to nie wysy�amy sms
                        if (!recipient.Key.SerialNbr.HasValue || mobileOperator == null || mobileOperator.Value == null) continue;

                        OpAction newAction = new OpAction();
                        newAction.IdActionData = 0;
                        newAction.IdActionType = IdActionType;
                        newAction.IdActionStatus = (int)Enums.ActionStatus.New;
                        newAction.SerialNbr = Common.Consts.SystemSerialNumber;
                        newAction.CreationDate = DateTime.MinValue; // sdudzik - MinValue po to, aby w parametrze procedury poszedl null, a wtedy czas zostanie ustawiony w procedurze na GETUTCDATE
                        newAction.IdModule = (int)Module;
                        newAction.IdOperator = LoggedOperator.IdOperator;
                        newAction.Location = null;
                        newAction.Meter = null;

                        List<DataValue> data = new List<DataValue>();
                        data.Add(new DataValue(DataType.ACTION_SMS_TEXT_BODY_TYPE, (int)BodyType.Text));
                        data.Add(new DataValue(DataType.ACTION_SMS_REQUEST_TEXT, msg));
                        data.Add(new DataValue(DataType.ACTION_OPERATOR_ID, recipient.Key.IdOperator));

                        IMRLog.AddToLog(Module, false, EventID.Sms.TryingToSendSms, recipient.Key.Login, "SaveAction");

                        OpAction retAction = ActionComponent.SaveAction(dataProvider, newAction, true, useDBCollector);
                        if (retAction != null && retAction.IdAction != 0)
                        {
                            if (retAction.ID_SERVER.HasValue) // wyslij do kolejki zdefiniowanej dla konkretnego serwera
                            {
                                IMRLog.AddToLog(Module, false, EventID.Sms.TryingToSendSms, recipient.Key.Login, "Sending through server: " + retAction.ID_SERVER.Value);

                                int? idOperatorOnServer = OperatorComponent.GetMappedIdOperator(dataProvider, recipient.Key, retAction.ID_SERVER.Value);

                                if (idOperatorOnServer.HasValue)
                                {
                                    if (serverActionMSMQDict.ContainsKey(retAction.ID_SERVER.Value))
                                    {
                                        int? serverIdActionType = null;
                                        bool? dbCollectorEnabled = dataProvider.IsDBCollectorEnabled(useDBCollector);
                                        if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                                        {
                                            serverIdActionType = dataProvider.dbCollectorClient.GetServerIdActionType(ref dataProvider.dbCollectorSession, retAction.ID_SERVER.Value, (int)retAction.IdActionType);
                                        }

                                        if (serverIdActionType.HasValue)
                                        {
                                            // Aktualizujemy id operatora na zmapowane id z serwera docelowego
                                            data.RemoveAll(x => x.IdDataType == DataType.ACTION_OPERATOR_ID);
                                            data.Add(new DataValue(DataType.ACTION_OPERATOR_ID, idOperatorOnServer.Value));

                                            IMR.Suite.Common.Action action = new IMR.Suite.Common.Action((Enums.ActionType)serverIdActionType.Value, SN.FromDBValue((ulong?)retAction.SerialNbr))
                                            {
                                                Status = Enums.ActionStatus.New,
                                                ActionData = data.ToArray()
                                            };

                                            action.IdAction = retAction.IdAction;

                                            string serverHost = serverActionMSMQDict[retAction.ID_SERVER.Value].Item1;
                                            string serverQueue = serverActionMSMQDict[retAction.ID_SERVER.Value].Item2;

                                            ActionComponent.RunAction(dataProvider, action, retAction.ID_SERVER.Value, String.Format("{0}\\private$\\{1}", serverHost, serverQueue), useDBCollector);
                                            //SendAction(serverHost, serverQueue, action); // zmiana sposobu uruchamiania akcji
                                        }
                                        else
                                        {
                                            IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Key.Login, "No action type mappings for server: " + retAction.ID_SERVER.Value);
                                        }
                                    }
                                    else
                                    {
                                        IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Key.Login, "Action-MSMQ dictionary does not contain value for server: " + retAction.ID_SERVER.Value);
                                    }
                                }
                                else
                                {
                                    IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Key.Login, "No recipient mappings for server: " + retAction.ID_SERVER.Value);
                                }
                            }
                            else  // wyslij do kolejki zdefiniowanej w konfiguracji aplikacji
                            {
                                IMRLog.AddToLog(Module, false, EventID.Sms.TryingToSendSms, recipient.Key.Login, "Sending through configured queue (Host: " + Host + ", Queue: " + Queue + ")");

                                IMR.Suite.Common.Action action = new IMR.Suite.Common.Action((int)newAction.IdActionType, SN.FromDBValue((ulong?)retAction.SerialNbr))
                                {
                                    Status = Enums.ActionStatus.New,
                                    ActionData = data.ToArray()
                                };

                                action.IdAction = retAction.IdAction;

                                ActionComponent.RunAction(dataProvider, action, null, String.Format("{0}\\private$\\{1}", Host, Queue), false);
                                //SendAction(Host, Queue, action); // zmiana sposobu uruchamiania akcji
                            }
                        }
                        else
                        {
                            IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Key.Login, "Did not receive retAction.");
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(Module, false, EventID.Sms.SmsSendingError, recipient.Key.Login, ex.Message);

                        return false;
                    }
                }
            }

            return true;
        }

        #endregion

        #region SendAction
        [Obsolete("Use ActionComponent.RunAction", true)]
        private static void SendAction(string host, string queue, IMR.Suite.Common.Action act)
        {
            //prepate action string
            string QueueInitString = String.Format(
                "{0}\\private$\\{1}",
                host,
                queue
                );

            //send action
            MSMQ MyQueue = new MSMQ(QueueInitString, true);
            MyQueue.Send(act);
        }

        #endregion

        #region GetTranslatedString

        public static string GetTranslatedString(string key, OpActor actor = null)
        {
            if (actor == null)
            {
                return ResourceManager.GetString(key);
            }

            try
            {
                CultureInfo info = CultureInfo.GetCultureInfo(LanguageComponent.GetCultureCode((Enums.Language)actor.IdLanguage));
                return ResourceManager.GetString(key, info);
            }
            catch
            {}

            return ResourceManager.GetString(key);
        }

        #endregion
    }
}