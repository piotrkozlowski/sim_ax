﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ModuleDataComponent
    {
        #region Methods
        public static bool GetFTPEnabled(DataProvider dataProvider, Enums.Module module)
        {
            OpModuleData systemData = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_ENABLED, (int)module);
            BaseComponent.Log(EventID.Forms.FTPEnabled, true, new object[] { module.ToString(), (systemData != null ? systemData.TryGetValue<bool>() : false) });
            if (systemData != null)
                return systemData.TryGetValue<bool>();

            return false;
        }

        public static void SaveFTPEnabled(DataProvider dataProvider, bool enabled, Enums.Module module)
        {
            OpModuleData data = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_ENABLED, (int)module);
            if (data == null)
            {
                data = new OpModuleData();
                data.IdDataType = DataType.SERVICE_CENTRE_FTP_ENABLED;
                data.Value = enabled;
                data.IdModule = (int)module;
            }
            else
            {
                data.Value = enabled;
            }

            DataComponent.Save(dataProvider, data);
        }

        public static string GetFTPAddress(DataProvider dataProvider, Enums.Module module)
        {
            OpModuleData systemData = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_ADDRESS, (int)module);
            BaseComponent.Log(EventID.Forms.FTPAddress, true, new object[] { module.ToString(), (systemData != null ? systemData.TryGetValue<string>() : "<null>") });
            if (systemData != null)
                return systemData.TryGetValue<string>();

            return null;
        }

        public static void SaveFTPAddress(DataProvider dataProvider, string address, Enums.Module module)
        {
            OpModuleData data = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_ADDRESS, (int)module);
            if (data == null)
            {
                data = new OpModuleData();
                data.IdDataType = DataType.SERVICE_CENTRE_FTP_ADDRESS;
                data.Value = address;
                data.IdModule = (int)module;
            }
            else
            {
                data.Value = address;
            }

            DataComponent.Save(dataProvider, data);
        }

        public static string GetFTPLogin(DataProvider dataProvider, Enums.Module module)
        {
            OpModuleData systemData = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_LOGIN, (int)module);
            BaseComponent.Log(EventID.Forms.FTPLogin, true, new object[] { module.ToString(), (systemData != null ? systemData.TryGetValue<string>() : "<null>") });
            if (systemData != null)
                return systemData.TryGetValue<string>();

            return null;
        }

        public static void SaveFTPLogin(DataProvider dataProvider, string login, Enums.Module module)
        {
            OpModuleData data = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_LOGIN, (int)module);
            if (data == null)
            {
                data = new OpModuleData();
                data.IdDataType = DataType.SERVICE_CENTRE_FTP_LOGIN;
                data.Value = login;
                data.IdModule = (int)module;
            }
            else
            {
                data.Value = login;
            }

            DataComponent.Save(dataProvider, data);
        }

        public static string GetFTPPassword(DataProvider dataProvider, Enums.Module module)
        {
            OpModuleData systemData = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_PASSWORD, (int)module);
            if (systemData != null)
                return systemData.TryGetValue<string>();

            return null;
        }

        public static void SaveFTPPassword(DataProvider dataProvider, string password, Enums.Module module)
        {
            OpModuleData data = GetModuleData(dataProvider, DataType.SERVICE_CENTRE_FTP_PASSWORD, (int)module);
            if (data == null)
            {
                data = new OpModuleData();
                data.IdDataType = DataType.SERVICE_CENTRE_FTP_PASSWORD;
                data.Value = password;
                data.IdModule = (int)module;
            }
            else
            {
                data.Value = password;
            }

            DataComponent.Save(dataProvider, data);
        }

        public static OpModuleData GetModuleData(DataProvider dataProvider, long idDataType, int idModule)
        {
            return dataProvider.GetModuleDataFilter(IdDataType: new long[] { idDataType }, IdModule: new int[] { idModule }).FirstOrDefault();
        }

        #endregion
    }
}
