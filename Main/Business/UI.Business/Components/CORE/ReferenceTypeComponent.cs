﻿using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ReferenceTypeComponent
    {
        #region GetReferenceObject
        public static object GetReferenceObject(DataProvider dataProvider, Enums.ReferenceType referenceType, object referenceValue, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            try
            {
                if (referenceValue != null)
                {
                    switch (referenceType)
                    {
                        case Enums.ReferenceType.None:
                            break;
                        // dodajac nowe - zachowaj porzadek alfabetyczny!
                        case Enums.ReferenceType.ActionType:
                            return dataProvider.GetActionType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.DistributorList:
                            if (!string.IsNullOrEmpty(referenceValue.ToString()))
                            {
                                string[] ids = referenceValue.ToString().Split(';');
                                int[] listIds = ids.Select(id => Convert.ToInt32(id)).ToArray();
                                return dataProvider.GetDistributor(listIds);
                            }
                            break;
                        case Enums.ReferenceType.DeviceTypeList:
                            if (!string.IsNullOrEmpty(referenceValue.ToString()))
                            {
                                string[] ids = referenceValue.ToString().Split(';');
                                int[] listIds = ids.Select(id => Convert.ToInt32(id)).ToArray();
                                return dataProvider.GetDeviceType(listIds);
                            }
                            break;
                        case Enums.ReferenceType.IdAction:
                            return dataProvider.GetAction(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdActionDef:
                            return dataProvider.GetActionDef(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdActionSmsText:
                            return dataProvider.GetActionSmsText(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdActionStatus:
                            return dataProvider.GetActionStatus(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdActor:
                            return dataProvider.GetActor(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdActorGroup:
                            return dataProvider.GetActorGroup(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdActivity:
                            return dataProvider.GetActivity(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdAlarm:
                            return dataProvider.GetAlarm(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdAlarmEvent:
                            return dataProvider.GetAlarmEvent(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdAlarmDef:
                            return dataProvider.GetAlarmDef(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdAlarmGroup:
                            return dataProvider.GetAlarmGroup(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdAlarmStatus:
                            return dataProvider.GetAlarmStatus(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdAlarmText:
                            return dataProvider.GetAlarmText(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdAlarmType:
                            return dataProvider.GetAlarmType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdAtgType:
                            return dataProvider.GetAtgType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdArticle:
                            return dataProvider.GetArticle(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdChartType:
                            int idChartTypeInt = Convert.ToInt32(referenceValue);

                            if (Enum.IsDefined(typeof(Enums.ChartType), idChartTypeInt))
                                return (Enums.ChartType)idChartTypeInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdCommandCodeType:
                            return dataProvider.GetCommandCodeType(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdConfigurationProfile:
                            return dataProvider.GetConfigurationProfile(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdCurrency:
                            return dataProvider.GetCurrency(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDataArch:
                            return dataProvider.GetDataArch(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdDatabaseType:
                            int idDatabaseInt = Convert.ToInt32(referenceValue);

                            if (Enum.IsDefined(typeof(Enums.DatabaseType), idDatabaseInt))
                                return (Enums.ChartType)idDatabaseInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdDataFormatGroup:
                            return dataProvider.GetDataFormatGroup(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDataSourceType:
                            return dataProvider.GetDataSourceType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDataType:
                            return dataProvider.GetDataType(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdDelivery:
                            return dataProvider.GetDelivery(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDeliveryAdvice:
                            return dataProvider.GetDeliveryAdvice(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDepositoryElement:
                            return dataProvider.GetDepositoryElement(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDescr:
                            OpDescr descr = dataProvider.GetDescr(Convert.ToInt64(referenceValue)); // w cachu sa tylko descr dla wybranego jezyka

                            if (descr == null) // chcemy sprobowac zaladowac jeszcze dla domyslnego jezyka angielskiego
                            {
                                bool descrIsFullCacheChanged = false;
                                if (dataProvider.DescrIsFullCache)
                                {
                                    descrIsFullCacheChanged = true;
                                    dataProvider.DescrIsFullCache = false; // bez tego w ogole bysmy nie poszukali nic wiecej
                                }
                                descr = dataProvider.GetDescr(Convert.ToInt64(referenceValue), Enums.Language.English);
                                if (descrIsFullCacheChanged)
                                    dataProvider.DescrIsFullCache = true;
                            }
                            return descr;
                        case Enums.ReferenceType.IdDetailView:
                            int refValueInt = Convert.ToInt32(referenceValue);

                            if (Enum.IsDefined(typeof(Enums.DetailView), refValueInt))
                                return new Objects.Custom.OpEnumObject((Enums.DetailView)refValueInt);
                            else
                                return null;
                        case Enums.ReferenceType.IdDeviceConnection:
                            return dataProvider.GetDeviceConnection(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDeviceDashboardAdditionalChartView:
                            int refIdAddValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.DeviceDashboardAdditionalChartView), refIdAddValueInt))
                                return (Enums.DeviceDashboardAdditionalChartView)refIdAddValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdDeviceDashboardMainChartView:
                            int refIdMainValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.DeviceDashboardMainChartView), refIdMainValueInt))
                                return (Enums.DeviceDashboardMainChartView)refIdMainValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdDeviceDriver:
                            return dataProvider.GetDeviceDriver(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDeviceInstalationPlace:
                            int refDeviceInstalationPlace = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.DeviceInstalationPlace), refDeviceInstalationPlace))
                                return (Enums.DeviceInstalationPlace)refDeviceInstalationPlace;
                            else
                                return null;
                        case Enums.ReferenceType.IdDeviceLocationCompassDirection:
                            int refDeviceLocationCompassDirection = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.DeviceLocationCompassDirection), refDeviceLocationCompassDirection))
                                return (Enums.DeviceLocationCompassDirection)refDeviceLocationCompassDirection;
                            else
                                return null;
                        case Enums.ReferenceType.IdDeviceOrderNumber:
                            return dataProvider.GetDeviceOrderNumber(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDeviceStateType:
                            return dataProvider.GetDeviceStateType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDeviceType:
                            return dataProvider.GetDeviceType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDeviceTypeGroup:
                            return dataProvider.GetDeviceTypeGroup(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdDispenserPumpType:
                            int refDispenserPumpTypeInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.DispenserPumpType), refDispenserPumpTypeInt))
                                return new Objects.Custom.OpEnumObject((Enums.DispenserPumpType)refDispenserPumpTypeInt);
                            else
                                return null;
                        case Enums.ReferenceType.IdDistributor:
                            return dataProvider.GetDistributor(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdEmailSendingMode:
                            int refIdEmailSendingModeAddValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.EmailSendingMode), refIdEmailSendingModeAddValueInt))
                                return (Enums.EmailSendingMode)refIdEmailSendingModeAddValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdEtl:
                            return dataProvider.GetEtl(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdFaultCode:
                            return dataProvider.GetFaultCode(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdFile:
                            return dataProvider.GetFile(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdFileExtension:
                            int refIdFileExtAddValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.FileExtension), refIdFileExtAddValueInt))
                                return (Enums.FileExtension)refIdFileExtAddValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdFuelFillingPointType:
                            int refFuelFillingPointTypeInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.FuelFillingPointType), refFuelFillingPointTypeInt))
                                return new Objects.Custom.OpEnumObject((Enums.FuelFillingPointType)refFuelFillingPointTypeInt);
                            else
                                return null;
                        case Enums.ReferenceType.IdIMRServer:
                            return dataProvider.GetImrServer(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdIssue:
                            return dataProvider.GetIssue(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdIssueGroup:
                            return dataProvider.GetIssueGroup(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdIssueStatus:
                            return dataProvider.GetIssueStatus(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdIssueType:
                            return dataProvider.GetIssueType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdLanguage:
                            return dataProvider.GetLanguage(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdLocation:
                            return dataProvider.GetLocation(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdLocationStateType:
                            return dataProvider.GetLocationStateType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdLocationType:
                            return dataProvider.GetLocationType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdMapMarkerTextType:
                            int refIdTextTypeInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.MapMarkerTextType), refIdTextTypeInt))
                                return (Enums.MapMarkerTextType)refIdTextTypeInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdMeter:
                            return dataProvider.GetMeter(new long[] { Convert.ToInt64(referenceValue) }, false, loadNavigationProperties, loadCustomData).FirstOrDefault();
                        case Enums.ReferenceType.IdMeterType:
                            return dataProvider.GetMeterType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdMeterTypeClass:
                            return dataProvider.GetMeterTypeClass(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdMeterTypeGroup:
                            return dataProvider.GetMeterTypeGroup(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdModule:
                            return dataProvider.GetModule(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdOperator:
                            return dataProvider.GetOperator(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdOperatorLoginStatus:
                            int refIdOLSValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.OperatorLoginStatus), refIdOLSValueInt))
                                return new Objects.Custom.OpEnumObject((Enums.OperatorLoginStatus)refIdOLSValueInt);
                            else
                                return null;
                        case Enums.ReferenceType.IdPriority:
                            return dataProvider.GetPriority(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdProductCode:
                            return dataProvider.GetProductCode(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdProfile:
                            return dataProvider.GetProfile(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdProtocol:
                            return dataProvider.GetProtocol(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdReferenceType:
                            return dataProvider.GetReferenceType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdRefuel:
                            return dataProvider.GetRefuel(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdReport:
                            return dataProvider.GetReport(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdReportDataType:
                            return dataProvider.GetReportDataType(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdReportType:
                            int refIdReportTypeValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.ReportType), refIdReportTypeValueInt))
                                return (Enums.ReportType)refIdReportTypeValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdRole:
                            return dataProvider.GetRole(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdRoute:
                            return dataProvider.GetRoute(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdRouteDef:
                            return dataProvider.GetRouteDef(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdRouteStatus:
                            return dataProvider.GetRouteStatus(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdRouteType:
                            return dataProvider.GetRouteType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdSimCard:
                            return dataProvider.GetSimCard(new int[] { Convert.ToInt32(referenceValue) }, false, loadCustomData: loadCustomData);
                        case Enums.ReferenceType.IdSitaCommandType:
                            int refIdSitaCommandAddValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.SitaCommandType), refIdSitaCommandAddValueInt))
                                return (Enums.SitaCommandType)refIdSitaCommandAddValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdSitaFitterDeposiotryMode:
                            int refSitaFitterDeposiotryModeAddValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.SitaFitterDeposiotryMode), refSitaFitterDeposiotryModeAddValueInt))
                                return (Enums.SitaFitterDeposiotryMode)refSitaFitterDeposiotryModeAddValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.IdSitaInstallationFormType:
                            int refSitaInstallationFormType = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.SitaInstallationFormType), refSitaInstallationFormType))
                                return (Enums.SitaInstallationFormType)refSitaInstallationFormType;
                            else
                                return null;
                        case Enums.ReferenceType.IdSlotType:
                            return dataProvider.GetSlotType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdStationType:
                            int refStationTypeInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.StationType), refStationTypeInt))
                                return new Objects.Custom.OpEnumObject((Enums.StationType)refStationTypeInt);
                            else
                                return null;
                        case Enums.ReferenceType.IdStationProfileType:
                            int refStationProfileTypeInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.StationProfileType), refStationProfileTypeInt))
                                return new Objects.Custom.OpEnumObject((Enums.StationProfileType)refStationProfileTypeInt);
                            else
                                return null;
                        case Enums.ReferenceType.IdSupplier:
                            return dataProvider.GetSupplier(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdTariff:
                            return dataProvider.GetTariff(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdTask:
                            return dataProvider.GetTask(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdTaskGroup:
                            return dataProvider.GetTaskGroup(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdTaskStatus:
                            return dataProvider.GetTaskStatus(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdTaskType:
                            return dataProvider.GetTaskType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdTransmissionDriver:
                            return dataProvider.GetTransmissionDriverDW(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdTransmissionType:
                            return dataProvider.GetTransmissionType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdUIObjectProperties:
                            int refIdUIValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.UIObjectProperties), refIdUIValueInt))
                                return new Objects.Custom.OpEnumObject((Enums.UIObjectProperties)refIdUIValueInt);
                            else
                                return null;
                        case Enums.ReferenceType.IdValveState:
                            int refIdValveState = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.ValveState), refIdValveState))
                                return new Objects.Custom.OpEnumObject((Enums.ValveState)refIdValveState);
                            else
                                return null;
                        case Enums.ReferenceType.IdVersion:
                            return dataProvider.GetVersion(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.IdVersionElement:
                            return dataProvider.GetVersionElement(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdView:
                            return dataProvider.GetView(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdViewColumn:
                            return dataProvider.GetViewColumn(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.IdWMBUSTransmissionType:
                            return dataProvider.GetWmbusTransmissionType(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.TextValue:
                            return referenceValue;
                        case Enums.ReferenceType.IdVersionState:
                            return dataProvider.GetVersionState(Convert.ToInt32(referenceValue));
                        case Enums.ReferenceType.LocationList:
                        case Enums.ReferenceType.AreaList:
                            if (!string.IsNullOrEmpty(referenceValue.ToString()))
                            {
                                string[] ids = referenceValue.ToString().Split(';');
                                long[] listIds = ids.Select(id => Convert.ToInt64(id)).ToArray();
                                return dataProvider.GetLocation(listIds);
                            }
                            break;
                        case Enums.ReferenceType.MeterTypeList:
                            if (!string.IsNullOrEmpty(referenceValue.ToString()))
                            {
                                string[] ids = referenceValue.ToString().Split(';');
                                int[] listIds = ids.Select(id => Convert.ToInt32(id)).ToArray();
                                return dataProvider.GetMeterType(listIds);
                            }
                            break;
                        case Enums.ReferenceType.OperatorList:
                            if (!string.IsNullOrEmpty(referenceValue.ToString()))
                            {
                                string[] ids = referenceValue.ToString().Split(';');
                                int[] listIds = ids.Select(id => Convert.ToInt32(id)).ToArray();
                                return dataProvider.GetOperator(listIds);
                            }
                            break;   
                        case Enums.ReferenceType.SerialNbr:
                            return dataProvider.GetDevice(Convert.ToInt64(referenceValue));
                        case Enums.ReferenceType.SITAUIObjectProperties:
                            int refSITAUIObjectPropertiesAddValueInt = Convert.ToInt32(referenceValue);
                            if (Enum.IsDefined(typeof(Enums.SITAUIObjectProperties), refSITAUIObjectPropertiesAddValueInt))
                                return (Enums.SITAUIObjectProperties)refSITAUIObjectPropertiesAddValueInt;
                            else
                                return null;
                        case Enums.ReferenceType.TaskStatusList:
                            if (!string.IsNullOrEmpty(referenceValue.ToString()))
                            {
                                string[] ids = referenceValue.ToString().Split(';');
                                int[] listIds = ids.Select(id => Convert.ToInt32(id)).ToArray();
                                return dataProvider.GetTaskStatus(listIds).OrderBy(t => t.ComboBoxString).Cast<object>().ToList();
                            }
                            break;
                        default:
                            throw new ApplicationException("Reference type not supported: " + referenceType);
                    }
                }
            }
            catch
            {

            }
            return null;
        }
        #endregion
        #region GetReferenceObjectKey
        public static object GetReferenceObjectKey(Enums.ReferenceType referenceType, object referenceValue)
        {
            if (referenceType.In(new Enums.ReferenceType[] { Enums.ReferenceType.OperatorList, Enums.ReferenceType.TaskStatusList, Enums.ReferenceType.LocationList, Enums.ReferenceType.DistributorList, Enums.ReferenceType.MeterTypeList, Enums.ReferenceType.DeviceTypeList, Enums.ReferenceType.AreaList })
                && (referenceValue != null && !string.IsNullOrEmpty(referenceValue.ToString())))
            {
                string[] ids = referenceValue.ToString().Split(';');
                int[] listIds = ids.Select(id => Convert.ToInt32(id)).ToArray();
                return listIds;
            }
            else if (referenceType == Enums.ReferenceType.TextValue)
                return referenceValue;
            else if (referenceValue != null)
            {
                if (referenceValue is IReferenceType)
                    return (referenceValue as IReferenceType).GetReferenceKey();
                else if (referenceValue is Enums.ChartType)
                    return (int)(Enums.ChartType)referenceValue;
                else if (referenceValue is Enums.ValveState)
                    return (long)(Enums.ValveState)referenceValue;
                else if (referenceValue is Enums.ProfileType)
                    return (int)(Enums.ProfileType)referenceValue;
                else if (referenceValue is Enums.DetailView)
                    return (int)(Enums.DetailView)referenceValue;
                else if (referenceValue is Enums.DeviceDashboardMainChartView)
                    return (int)(Enums.DeviceDashboardMainChartView)referenceValue;
                else if (referenceValue is Enums.DeviceDashboardAdditionalChartView)
                    return (int)(Enums.DeviceDashboardAdditionalChartView)referenceValue;
                else if (referenceValue is Enums.MapMarkerTextType)
                    return (int)(Enums.MapMarkerTextType)referenceValue;
                else if (referenceValue is Enums.UIObjectProperties)
                    return (int)(Enums.UIObjectProperties)referenceValue;
                else if (referenceValue is Enums.FileExtension)
                    return (int)(Enums.FileExtension)referenceValue;
                else if (referenceValue is Enums.FileType)
                    return (int)(Enums.FileType)referenceValue;
                else if (referenceValue is Enums.ReportType)
                    return (int)(Enums.ReportType)referenceValue;
                else if (referenceValue is Enums.SitaCommandType)
                    return (int)(Enums.SitaCommandType)referenceValue;
                else if (referenceValue is Enums.SitaFitterDeposiotryMode)
                    return (int)(Enums.SitaFitterDeposiotryMode)referenceValue;
                else if (referenceValue is Enums.SitaInstallationFormType)
                    return (int)(Enums.SitaInstallationFormType)referenceValue;
                else if (referenceValue is Objects.Custom.OpEnumObject)
                    return (referenceValue as Objects.Custom.OpEnumObject).EnumKey;
                else if (referenceValue is Enums.EmailSendingMode)
                    return (int)(Enums.EmailSendingMode)referenceValue;
                else if (referenceValue is Enums.DatabaseType)
                    return (int)(Enums.DatabaseType)referenceValue;
            }
            return null;
        }
        #endregion
        #region GetReferenceObjectList
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="referenceType"></param>
        /// <param name="opOperator">if Operator specified filter items by permission</param>
        /// <returns></returns>
        public static List<object> GetReferenceObjectList(DataProvider dataProvider, Enums.ReferenceType referenceType, OpOperator opOperator = null)
        {
            try
            {
                switch (referenceType)
                {
                    case Enums.ReferenceType.None:
                        break;
                    // dodajac nowe - zachowaj porzadek alfabetyczny!
                    case Enums.ReferenceType.ActionType:
                        if (opOperator != null)
                            return RoleComponent.FilterByPermission<OpActionType>(dataProvider.GetAllActionType(), Activity.ALLOWED_ACTION_TYPE, opOperator).OrderBy(o => o.Descr).ThenBy(t => t.Name).Cast<object>().ToList();
                        else
                            return dataProvider.GetAllActionType().OrderBy(o => o.Descr).ThenBy(t => t.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.AreaList:
                        return dataProvider.Location.FindAll(t => t.IdLocationType.In<int>((int)Enums.LocationType.PrepaidAreaLocation)
                             && t.IdLocationStateType != (int)Enums.LocationState.DELETED && dataProvider.DistributorFilterDict.ContainsKey(t.IdDistributor)).Cast<object>().ToList();
                    case Enums.ReferenceType.DeviceTypeList:
                        return dataProvider.DeviceType.FindAll(t => t.IdDeviceTypeClass.In<int>((int)Enums.DeviceTypeClass.OKO, (int)Enums.DeviceTypeClass.RADIO_DEVICE, (int)Enums.DeviceTypeClass.OTHER)).Cast<object>().ToList();
                    case Enums.ReferenceType.DistributorList:
                        return dataProvider.Distributor.FindAll(t=> dataProvider.DistributorFilterDict.ContainsKey(t.IdDistributor)).Cast<object>().ToList();
                    case Enums.ReferenceType.IdActionDef:
                        List<int> allowedActionTypes = new List<int>();
                        if (opOperator != null)
                            allowedActionTypes = RoleComponent.FilterByPermission<OpActionType>(dataProvider.GetAllActionType(), Activity.ALLOWED_ACTION_TYPE, opOperator).OrderBy(o => o.Descr).ThenBy(t => t.Name).Select(a => a.IdActionType).ToList();
                        else
                            allowedActionTypes = dataProvider.GetAllActionType().OrderBy(o => o.Descr).ThenBy(t => t.Name).Select(a => a.IdActionType).ToList();

                        return dataProvider.GetAllActionDef().Where(ad => allowedActionTypes.Contains(ad.ActionType.IdActionType)).OrderBy(q => q.IdActionDef).Cast<object>().ToList();

                    case Enums.ReferenceType.IdAction:
                        return dataProvider.GetAllAction().OrderBy(d => d.IdAction).Cast<object>().ToList();
                    case Enums.ReferenceType.IdActionSmsText:
                        return dataProvider.GetAllActionSmsText().OrderBy(o => o.SmsText).Cast<object>().ToList();
                    case Enums.ReferenceType.IdActionStatus:
                        return dataProvider.GetAllActionStatus().OrderBy(o => o.IdActionStatus).Cast<object>().ToList();
                    case Enums.ReferenceType.IdActor:
                        return dataProvider.GetAllActor().Cast<object>().ToList();
                    case Enums.ReferenceType.IdActorGroup: 
                        return dataProvider.GetAllActorGroup().Cast<object>().ToList();
                    case Enums.ReferenceType.IdActivity:
                        if (opOperator != null)
                            return ActivityComponent.FilterByPermission<OpActivity>(dataProvider.GetAllActivity(), Activity.ALLOWED_ACTIVITES, opOperator).OrderBy(o => o.Descr).ThenBy(t => t.Name).Cast<object>().ToList();
                        else
                            return dataProvider.GetAllActivity().OrderBy(o => o.Descr).ThenBy(n => n.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdAlarm:
                        return dataProvider.GetAllAlarm().OrderBy(d => d.IdAlarm).Cast<object>().ToList();
                    case Enums.ReferenceType.IdAlarmEvent:
                        return dataProvider.GetAllAlarmEvent().OrderBy(d => d.IdAlarmEvent).Cast<object>().ToList();
                    case Enums.ReferenceType.IdAlarmDef: 
                        return dataProvider.GetAllAlarmDef().Cast<object>().ToList();
                    case Enums.ReferenceType.IdAlarmGroup:
                        return dataProvider.GetAllAlarmGroup().OrderBy(d => d.IdAlarmGroup).Cast<object>().ToList();
                    case Enums.ReferenceType.IdAlarmStatus:
                        return dataProvider.GetAllAlarmStatus().OrderBy(d => d.IdAlarmStatus).Cast<object>().ToList();
                    case Enums.ReferenceType.IdAlarmType: 
                        return dataProvider.GetAllAlarmType().Cast<object>().ToList();
                    case Enums.ReferenceType.IdAlarmText:
                        return dataProvider.GetAllAlarmText().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdAtgType:
                        return dataProvider.GetAllAtgType().OrderBy(o => o.Descr).Cast<object>().ToList();
                    case Enums.ReferenceType.IdArticle:
                        return dataProvider.GetAllArticle().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdChartType: 
                        return Enum.GetValues(typeof(Enums.ChartType)).Cast<object>().ToList();
                    case Enums.ReferenceType.IdCommandCodeType:
                        return dataProvider.GetAllCommandCodeType().OrderBy(o => o.IdCommandCode).Cast<object>().ToList();
                    case Enums.ReferenceType.IdConfigurationProfile:
                        return dataProvider.GetAllConfigurationProfile().OrderBy(o => o.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdCurrency:
                        return dataProvider.GetAllCurrency().OrderBy(q => q.Symbol).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDataArch: 
                        return new List<IMR.Suite.UI.Business.Objects.CORE.OpDataArch>().Cast<object>().ToList(); // Z uwagi, że może być bardzo dużo wpisów w tej tabelce, zwracamy pustą listę.
                    case Enums.ReferenceType.IdDatabaseType:
                        return Enum.GetValues(typeof(Enums.DatabaseType)).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDataFormatGroup:
                        return dataProvider.GetAllDataFormatGroup().OrderBy(o => o.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDataSourceType:
                        return dataProvider.GetAllDataSourceType().OrderBy(d => d.IdDataSourceType).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDataType:
                        return dataProvider.GetAllDataType().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDelivery:
                        return dataProvider.GetAllDelivery().Cast<object>().ToList();
                    case Enums.ReferenceType.IdDeliveryAdvice:
                        return dataProvider.GetAllDeliveryAdvice().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDepositoryElement:
                        return dataProvider.GetAllDepositoryElement().Cast<object>().ToList();
                    case Enums.ReferenceType.IdDescr:
                        return dataProvider.GetAllDescr().OrderBy(o => o.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDetailView:
                        return Enum.GetValues(typeof(Enums.DetailView)).OfType<Enum>().Select(q => new Objects.Custom.OpEnumObject(q)).OrderBy(q => q.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDeviceConnection:
                        return dataProvider.GetAllDeviceConnection().OrderBy(o => o.IdDeviceConnection).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDeviceDashboardAdditionalChartView:
                        return new List<object>(Enum.GetValues(typeof(Enums.DeviceDashboardAdditionalChartView)).Cast<object>());
                    case Enums.ReferenceType.IdDeviceDashboardMainChartView:
                        return new List<object>(Enum.GetValues(typeof(Enums.DeviceDashboardMainChartView)).Cast<object>());
                    case Enums.ReferenceType.IdDeviceDriver:
                        return dataProvider.GetAllDeviceDriver().OrderBy(o => o.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDeviceInstalationPlace:
                        return new List<object>(Enum.GetValues(typeof(Enums.DeviceInstalationPlace)).Cast<object>());
                    case Enums.ReferenceType.IdDeviceLocationCompassDirection:
                        return new List<object>(Enum.GetValues(typeof(Enums.DeviceLocationCompassDirection)).Cast<object>());
                    case Enums.ReferenceType.IdDeviceOrderNumber:
                        return dataProvider.GetAllDeviceOrderNumber().Cast<object>().ToList();
                    case Enums.ReferenceType.IdDeviceStateType:
                        return dataProvider.GetAllDeviceStateType().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDeviceType:
                        return dataProvider.GetAllDeviceType().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDeviceTypeGroup:
                        return dataProvider.GetAllDeviceTypeGroup().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDispenserPumpType:
                        return Enum.GetValues(typeof(Enums.DispenserPumpType)).OfType<Enum>().Select(q => new Objects.Custom.OpEnumObject(q)).OrderBy(q => q.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdDistributor:
                        if (opOperator != null)
                            return RoleComponent.FilterByPermission<OpDistributor>(dataProvider.GetAllDistributor(), Activity.ALLOWED_DISTRIBUTOR, opOperator).OrderBy(d => d.Name).Cast<object>().ToList();
                        else
                            return dataProvider.GetAllDistributor().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdEmailSendingMode:
                        return new List<object>(Enum.GetValues(typeof(Enums.EmailSendingMode)).Cast<object>());
                    case Enums.ReferenceType.IdEtl:
                        return dataProvider.GetAllEtl().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdFaultCode:
                        return dataProvider.GetAllFaultCode().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdFile:
                        return dataProvider.GetAllFile().OrderBy(o => o.PhysicalFileName).Cast<object>().ToList();
                    case Enums.ReferenceType.IdFileExtension:
                        return new List<object>(Enum.GetValues(typeof(Enums.FileExtension)).Cast<object>());
                    case Enums.ReferenceType.IdIMRServer:
                        if (opOperator != null)
                            return RoleComponent.FilterByPermission<OpImrServer>(dataProvider.GetAllImrServer(), Activity.ALLOWED_IMR_SERVER, opOperator).OrderBy(o => o.StandardDescription).Cast<object>().ToList();
                        else
                            return dataProvider.GetAllImrServer().OrderBy(o => o.StandardDescription).Cast<object>().ToList();
                    case Enums.ReferenceType.IdFuelFillingPointType:
                        return Enum.GetValues(typeof(Enums.FuelFillingPointType)).OfType<Enum>().Select(q => new Objects.Custom.OpEnumObject(q)).OrderBy(q => q.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdIssue:
                        return dataProvider.GetAllIssue().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdIssueGroup:
                        return dataProvider.GetAllIssueGroup().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdIssueStatus:
                        return dataProvider.GetAllIssueStatus().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdIssueType:
                        return dataProvider.GetAllIssueType().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdLanguage:
                        return dataProvider.GetAllLanguage().OrderBy(o => o.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdLocation:
                        return LocationComponent.GetAll(dataProvider, exludeDeletedAndUnused: true).OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdLocationStateType:
                        return dataProvider.GetAllLocationStateType().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdLocationType:
                        return dataProvider.GetAllLocationType().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdMapMarkerTextType:
                        return new List<object>(Enum.GetValues(typeof(Enums.MapMarkerTextType)).Cast<object>());
                    case Enums.ReferenceType.IdMeter:
                        return dataProvider.GetAllMeter().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdMeterType:
                        return dataProvider.GetAllMeterType().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdMeterTypeClass:
                        return dataProvider.GetAllMeterTypeClass().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdMeterTypeGroup:
                        return dataProvider.GetAllMeterTypeGroup().OrderBy(o => o.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdModule:
                        return dataProvider.GetAllModule().OrderBy(o => o.Descr).ThenBy(n => n.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdOperator:
                        return OperatorComponent.GetAll(dataProvider, isBlocked: false, order: true).OrderBy(o => o.ComboBoxString).Cast<object>().ToList();
                    case Enums.ReferenceType.IdOperatorLoginStatus:
                        return Enum.GetValues(typeof(Enums.OperatorLoginStatus)).OfType<Enum>().Select(q => new Objects.Custom.OpEnumObject(q)).OrderBy(q => q.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdPriority:
                        return dataProvider.GetAllPriority().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdProductCode:
                        return dataProvider.GetAllProductCode().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdProfile:
                        return dataProvider.GetAllProfile().OrderBy(o => o.Descr).ThenBy(n => n.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdProtocol:
                        return dataProvider.GetAllProtocol().OrderBy(d => d.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdReferenceType:
                        return dataProvider.GetAllReferenceType().OrderBy(r => r.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdRefuel:
                        return dataProvider.GetAllRefuel().OrderBy(r => r.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdReport:
                        if (opOperator != null)
                            return RoleComponent.FilterByPermission<OpReport>(dataProvider.GetAllReport(), Activity.ALLOWED_REPORT, opOperator).Where(w => w.IdReportType.In((int)(Enums.ReportType.PredefinedOnStoredProcedure))).OrderBy(o => o.Name).Cast<object>().ToList();
                        //return RoleComponent.FilterByPermission<OpReport>(dataProvider.GetAllReport(), Activity.ALLOWED_REPORT, opOperator).Where(w => w.IdReportType.In((int)(Enums.ReportType.PredefinedOnStoredProcedure), (int)(Enums.ReportType.MeasuresReport))).OrderBy(o => o.Name).Cast<object>().ToList();
                        else
                            return dataProvider.GetAllReport().Where(w => w.IdReportType.In((int)(Enums.ReportType.PredefinedOnStoredProcedure))).OrderBy(o => o.Name).Cast<object>().ToList();
                    //return dataProvider.GetAllReport().Where(w => w.IdReportType.In((int)(Enums.ReportType.PredefinedOnStoredProcedure), (int)(Enums.ReportType.MeasuresReport))).OrderBy(o => o.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdReportDataType:
                        return ReportComponent.GetAllReportDataType(dataProvider, true).Cast<object>().ToList();
                    case Enums.ReferenceType.IdReportType:
                        return new List<object>(Enum.GetValues(typeof(Enums.ReportType)).Cast<object>());
                    case Enums.ReferenceType.IdRole:
                        if (opOperator != null)
                            return RoleComponent.FilterByPermission<OpRole>(dataProvider.GetAllRole(), Activity.ALLOWED_ROLE, opOperator).OrderBy(o => o.Descr).ThenBy(t => t.Name).Cast<object>().ToList();
                        else
                            return dataProvider.GetAllRole().OrderBy(o => o.Descr).ThenBy(t => t.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdRoute:
                        return dataProvider.GetAllRoute().OrderByDescending(r => r.Year).ThenBy(r => r.Month).ThenBy(r => r.Week).Cast<object>().ToList();
                    case Enums.ReferenceType.IdRouteDef:
                        return dataProvider.GetAllRouteDef().Cast<object>().ToList();
                    case Enums.ReferenceType.IdRouteStatus:
                        return dataProvider.GetAllRouteStatus().Cast<object>().ToList();
                    case Enums.ReferenceType.IdRouteType:
                        return dataProvider.GetAllRouteType().Cast<object>().ToList();
                    case Enums.ReferenceType.IdSimCard:
                        return dataProvider.GetAllSimCard().Cast<object>().ToList();
                    case Enums.ReferenceType.IdSitaCommandType:
                        return new List<object>(Enum.GetValues(typeof(Enums.SitaCommandType)).Cast<object>());
                    case Enums.ReferenceType.IdSitaFitterDeposiotryMode:
                        return new List<object>(Enum.GetValues(typeof(Enums.SitaFitterDeposiotryMode)).Cast<object>());
                    case Enums.ReferenceType.IdSitaInstallationFormType:
                        return Enum.GetValues(typeof(Enums.SitaInstallationFormType)).Cast<object>().ToList();
                    case Enums.ReferenceType.IdSlotType:
                        return dataProvider.GetAllSlotType().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdStationType:
                        return Enum.GetValues(typeof(Enums.StationType)).OfType<Enum>().Select(q => new Objects.Custom.OpEnumObject(q)).OrderBy(q => q.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdStationProfileType:
                        return Enum.GetValues(typeof(Enums.StationProfileType)).OfType<Enum>().Select(q => new Objects.Custom.OpEnumObject(q)).OrderBy(q => q.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdSupplier:
                        return dataProvider.GetAllSupplier().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdTariff:
                        return dataProvider.GetAllTariff().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdTask:
                        return TaskComponent.GetAll(dataProvider).OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdTaskGroup:
                        return TaskGroupComponent.GetAll(dataProvider).OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdTaskStatus:
                        return dataProvider.GetAllTaskStatus().OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.IdTaskType:
                        return dataProvider.GetAllTaskType().OrderBy(o => o.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdTransmissionDriver:
                        return dataProvider.GetAllTransmissionDriverDW().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdTransmissionType:
                        return dataProvider.GetAllTransmissionType().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.IdUIObjectProperties:
                        return Enum.GetValues(typeof(Enums.UIObjectProperties)).OfType<Enum>().Select(q => new Objects.Custom.OpEnumObject(q)).OrderBy(q => q.Description).Cast<object>().ToList();
                    case Enums.ReferenceType.IdValveState:
                        break;
                    case Enums.ReferenceType.IdVersion:
                        return dataProvider.GetAllVersion().Cast<object>().ToList();
                    case Enums.ReferenceType.IdVersionElement:
                        return dataProvider.GetAllVersionElement().Cast<object>().ToList();
                    case Enums.ReferenceType.IdVersionState:
                        return dataProvider.GetAllVersionState().Cast<object>().ToList();
                    case Enums.ReferenceType.IdView: 
                        return dataProvider.GetAllView().Cast<object>().ToList();
                    case Enums.ReferenceType.IdViewColumn: 
                        return dataProvider.GetAllViewColumn().Cast<object>().ToList();
                    case Enums.ReferenceType.IdWMBUSTransmissionType:
                        return dataProvider.GetAllWmbusTransmissionType().OrderBy(q => q.Name).Cast<object>().ToList();
                    case Enums.ReferenceType.LocationList:
                        return dataProvider.Location.FindAll(t => t.IdLocationType.In<int>((int)Enums.LocationType.CustomerLocation)
                             && t.IdLocationStateType != (int)Enums.LocationState.DELETED).Cast<object>().ToList();
                    case Enums.ReferenceType.MeterTypeList:
                        return dataProvider.MeterType.FindAll(t => t.IdMeterTypeClass.In<int>((int)Enums.MeterTypeClass.GasMeter)).Cast<object>().ToList();
                    case Enums.ReferenceType.TextValue:
                        return new List<object>();
                    case Enums.ReferenceType.OperatorList:
                        return OperatorComponent.GetAll(dataProvider, isBlocked: false, order: true).OrderBy(o => o.ComboBoxString).Cast<object>().ToList();
                    case Enums.ReferenceType.SerialNbr:
                        return DeviceComponent.GetAll(dataProvider, exludeDeleted: true).OrderBy(o => o.ToString()).Cast<object>().ToList();
                    case Enums.ReferenceType.SITAUIObjectProperties:
                        return new List<object>(Enum.GetValues(typeof(Enums.SITAUIObjectProperties)).Cast<object>());
                    case Enums.ReferenceType.TaskStatusList:
                        return dataProvider.GetAllTaskStatus().OrderBy(o => o.ComboBoxString).Cast<object>().ToList();
                }
            }
            catch
            {
            }

            return new List<object>();
        }

        #endregion
        #region GetReferenceObjectType
        public static Type GetReferenceObjectType(Enums.ReferenceType referenceType)
        {
            switch (referenceType)
            {
                case Enums.ReferenceType.None:
                    break;
                // dodajac nowe - zachowaj porzadek alfabetyczny!
                case Enums.ReferenceType.ActionType:
                    return typeof(OpActionType);
                case Enums.ReferenceType.IdAction:
                    return typeof(Objects.CORE.OpAction);
                case Enums.ReferenceType.IdActionDef:
                    return typeof(OpActionDef);
                case Enums.ReferenceType.IdActionSmsText:
                    return typeof(OpActionSmsText);
                case Enums.ReferenceType.IdActionStatus:
                    return typeof(OpActionStatus);
                case Enums.ReferenceType.IdActor:
                    return typeof(OpActor);
                case Enums.ReferenceType.IdActorGroup: 
                    return typeof(OpActorGroup);
                case Enums.ReferenceType.IdActivity:
                    return typeof(OpActivity);
                case Enums.ReferenceType.IdAlarm:
                    return typeof(Objects.CORE.OpAlarm);
                case Enums.ReferenceType.IdAlarmEvent:
                    return typeof(Objects.CORE.OpAlarmEvent);
                case Enums.ReferenceType.IdAlarmDef: 
                    return typeof(OpAlarmDef);
                case Enums.ReferenceType.IdAlarmGroup:
                    return typeof(Objects.CORE.OpAlarmGroup);
                case Enums.ReferenceType.IdAlarmStatus:
                    return typeof(Objects.CORE.OpAlarmStatus);
                case Enums.ReferenceType.IdAlarmText:
                    return typeof(OpAlarmText);
                case Enums.ReferenceType.IdAlarmType: 
                    return typeof(OpAlarmType);
                case Enums.ReferenceType.IdAtgType:
                    return typeof(OpAtgType);
                case Enums.ReferenceType.IdArticle:
                    return typeof(OpArticle);
                case Enums.ReferenceType.IdChartType: 
                    return typeof(Enums.ChartType);
                case Enums.ReferenceType.IdCommandCodeType:
                    return typeof(OpCommandCodeType);
                case Enums.ReferenceType.IdConfigurationProfile:
                    return typeof(OpConfigurationProfile);
                case Enums.ReferenceType.IdCurrency:
                    return typeof(OpCurrency);
                case Enums.ReferenceType.IdDataArch: 
                    return typeof(IMR.Suite.UI.Business.Objects.CORE.OpDataArch);
                case Enums.ReferenceType.IdDatabaseType:
                    return typeof(Enums.DatabaseType);
                case Enums.ReferenceType.IdDataFormatGroup:
                    return typeof(OpDataFormatGroup);
                case Enums.ReferenceType.IdDataSourceType:
                    return typeof(OpDataSourceType);
                case Enums.ReferenceType.IdDataType:
                    return typeof(OpDataType);
                case Enums.ReferenceType.IdDelivery:
                    return typeof(OpDelivery);
                case Enums.ReferenceType.IdDeliveryAdvice:
                    return typeof(OpDeliveryAdvice);
                case Enums.ReferenceType.IdDepositoryElement:
                    return typeof(OpDepositoryElement);
                case Enums.ReferenceType.IdDescr:
                    return typeof(OpDescr);
                case Enums.ReferenceType.IdDetailView:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdDeviceConnection:
                    return typeof(OpDeviceConnection);
                case Enums.ReferenceType.IdDeviceDashboardAdditionalChartView:
                    return typeof(Enums.DeviceDashboardAdditionalChartView);
                case Enums.ReferenceType.IdDeviceDashboardMainChartView:
                    return typeof(Enums.DeviceDashboardMainChartView);
                case Enums.ReferenceType.IdDeviceDriver:
                    return typeof(OpDeviceDriver);
                case Enums.ReferenceType.IdDeviceInstalationPlace:
                    return typeof(Enums.DeviceInstalationPlace);
                case Enums.ReferenceType.IdDeviceLocationCompassDirection:
                    return typeof(Enums.DeviceLocationCompassDirection);
                case Enums.ReferenceType.IdDeviceOrderNumber:
                    return typeof(OpDeviceOrderNumber);
                case Enums.ReferenceType.IdDeviceStateType:
                    return typeof(OpDeviceStateType);
                case Enums.ReferenceType.IdDeviceType:
                    return typeof(OpDeviceType);
                case Enums.ReferenceType.IdDeviceTypeGroup:
                    return typeof(OpDeviceTypeGroup);
                case Enums.ReferenceType.IdDispenserPumpType:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdDistributor:
                    return typeof(OpDistributor);
                case Enums.ReferenceType.IdEmailSendingMode:
                    return typeof(Enums.EmailSendingMode);
                case Enums.ReferenceType.IdEtl:
                    return typeof(Objects.CORE.OpEtl);
                case Enums.ReferenceType.IdFaultCode:
                    return typeof(OpFaultCode);
                case Enums.ReferenceType.IdFile:
                    return typeof(OpFile);
                case Enums.ReferenceType.IdFileExtension:
                    return typeof(Enums.FileExtension);
                case Enums.ReferenceType.IdFileType:
                    return typeof(Enums.FileType);
                case Enums.ReferenceType.IdFuelFillingPointType:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdIMRServer:
                    return typeof(OpImrServer);
                case Enums.ReferenceType.IdIssue:
                    return typeof(OpIssue);
                case Enums.ReferenceType.IdIssueGroup:
                    return typeof(OpIssueGroup);
                case Enums.ReferenceType.IdIssueStatus:
                    return typeof(OpIssueStatus);
                case Enums.ReferenceType.IdIssueType:
                    return typeof(OpIssueType);
                case Enums.ReferenceType.IdLanguage:
                    return typeof(OpLanguage);
                case Enums.ReferenceType.IdLocation:
                    return typeof(OpLocation);
                case Enums.ReferenceType.IdLocationStateType:
                    return typeof(OpLocationStateType);
                case Enums.ReferenceType.IdLocationType:
                    return typeof(OpLocationType);
                case Enums.ReferenceType.IdMapMarkerTextType:
                    return typeof(Enums.MapMarkerTextType);
                case Enums.ReferenceType.IdMeter:
                    return typeof(OpMeter);
                case Enums.ReferenceType.IdMeterType:
                    return typeof(OpMeterType);
                case Enums.ReferenceType.IdMeterTypeClass:
                    return typeof(OpMeterTypeClass);
                case Enums.ReferenceType.IdMeterTypeGroup:
                    return typeof(OpMeterTypeGroup);
                case Enums.ReferenceType.IdModule:
                    return typeof(OpModule);
                case Enums.ReferenceType.IdOperator:
                    return typeof(OpOperator);
                case Enums.ReferenceType.IdOperatorLoginStatus:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdPriority:
                    return typeof(OpPriority);
                case Enums.ReferenceType.IdProductCode:
                    return typeof(OpProductCode);
                case Enums.ReferenceType.IdProfile:
                    return typeof(OpProfile);
                case Enums.ReferenceType.IdProtocol:
                    return typeof(OpProtocol);
                case Enums.ReferenceType.IdRefuel:
                    return typeof(OpRefuel);
                case Enums.ReferenceType.IdReferenceType:
                    return typeof(OpReferenceType);
                case Enums.ReferenceType.IdReport:
                    return typeof(OpReport);
                case Enums.ReferenceType.IdReportDataType:
                    return typeof(OpReportDataType);
                case Enums.ReferenceType.IdReportType:
                    return typeof(Enums.ReportType);
                case Enums.ReferenceType.IdRole:
                    return typeof(OpRole);
                case Enums.ReferenceType.IdRoute:
                    return typeof(OpRoute);
                case Enums.ReferenceType.IdRouteDef:
                    return typeof(OpRouteDef);
                case Enums.ReferenceType.IdRouteStatus:
                    return typeof(OpRouteStatus);
                case Enums.ReferenceType.IdRouteType:
                    return typeof(OpRouteType);
                case Enums.ReferenceType.IdSimCard:
                    return typeof(OpSimCard);
                case Enums.ReferenceType.IdSitaCommandType:
                    return typeof(Enums.SitaCommandType);
                case Enums.ReferenceType.IdSitaFitterDeposiotryMode:
                    return typeof(Enums.SitaFitterDeposiotryMode);
                case Enums.ReferenceType.IdSitaInstallationFormType:
                    return typeof(Enums.SitaInstallationFormType);
                case Enums.ReferenceType.IdSlotType:
                    return typeof(OpSlotType);
                case Enums.ReferenceType.IdStationType:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdStationProfileType:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdSupplier:
                    return typeof(OpSupplier);
                case Enums.ReferenceType.IdTariff:
                    return typeof(OpTariff);
                case Enums.ReferenceType.IdTask:
                    return typeof(OpTask);
                case Enums.ReferenceType.IdTaskGroup:
                    return typeof(OpTaskGroup);
                case Enums.ReferenceType.IdTaskStatus:
                    return typeof(OpTaskStatus);
                case Enums.ReferenceType.IdTaskType:
                    return typeof(OpTaskType);
                case Enums.ReferenceType.IdTransmissionDriver:
                    return typeof(OpTransmissionDriver);
                case Enums.ReferenceType.IdTransmissionType:
                    return typeof(OpTransmissionType);
                case Enums.ReferenceType.IdUIObjectProperties:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdValveState:
                    return typeof(Objects.Custom.OpEnumObject);
                case Enums.ReferenceType.IdVersion:
                    return typeof(OpVersion);
                case Enums.ReferenceType.IdVersionElement: 
                    return typeof(OpVersionElement);
                case Enums.ReferenceType.IdVersionState:
                    return typeof(OpVersionState);
                case Enums.ReferenceType.IdView: 
                    return typeof(OpView);
                case Enums.ReferenceType.IdViewColumn: 
                    return typeof(OpViewColumn);
                case Enums.ReferenceType.IdWMBUSTransmissionType:
                    return typeof(OpWmbusTransmissionType);
                case Enums.ReferenceType.TextValue:
                    return typeof(string);
                case Enums.ReferenceType.OperatorList:
                    break;
                case Enums.ReferenceType.SerialNbr:
                    return typeof(OpDevice);
                case Enums.ReferenceType.SITAUIObjectProperties:
                    return typeof(Enums.SITAUIObjectProperties);
                case Enums.ReferenceType.TaskStatusList:
                    break;
            }
            return null;
        }
        #endregion       
    }
}
