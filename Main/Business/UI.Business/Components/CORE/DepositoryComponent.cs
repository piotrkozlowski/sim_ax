﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using IMR.Suite.Services.Common.UTDServiceReference;
using IMR.Suite.Services.Common.Components;
using System.Data;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DepositoryComponent
    {
        public const int DeviceOrderNumberNotProducedByAiut = 10000;

        #region Get functions
        public static OpDepositoryElement GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetDepositoryElement(Id);
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpLocation location)
        {
            return GetElements(dataProvider, location, null);
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpLocation location, bool? removed,
            bool loadNavigationProperties = true, bool autoTransaction = true)
        {
            return dataProvider.GetDepositoryElementFilter(IdLocation: new long[] { location.IdLocation },
                                                           Removed: removed,
                                                           EndDate: Data.DB.TypeDateTimeCode.Null(),
                                                           loadNavigationProperties: loadNavigationProperties,
                                                           autoTransaction: autoTransaction);
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpPackage package, bool loadNavigationProperties = true, bool autoTransaction = true)
        {
            return dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { package.IdPackage });
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpTask task)
        {
            return GetElements(dataProvider, task, null, null, null);
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpTask task, bool ordered)
        {
            return GetElements(dataProvider, task, ordered, null, null);
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpTask task, bool ordered, bool removed)
        {
            return GetElements(dataProvider, task, ordered, removed, null);
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpTask task, bool? ordered, bool? removed, bool? accepted)
        {
            return GetElements(dataProvider, task.IdTask, ordered, removed, accepted);
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, int idTask, bool? ordered, bool? removed, bool? accepted)
        {
            return dataProvider.GetDepositoryElementFilter(IdTask: new int[] { idTask }, Ordered: ordered,
                                                           Removed: removed, Accepted: accepted,
                                                           EndDate: Data.DB.TypeDateTimeCode.Null());
        }

        public static List<OpDepositoryElement> GetElements(DataProvider dataProvider, OpRoute route)
        {
            List<int> taskIds = RouteComponent.GetTasks(dataProvider, route.IdRoute).Select(t => t.IdTask).ToList();
            
            List<OpDepositoryElement> all = null;
            if (taskIds.Count > 0)
            {
                all = dataProvider.GetDepositoryElementFilter(IdTask: taskIds.ToArray(), Ordered: true, EndDate: Data.DB.TypeDateTimeCode.Null());
            }

            return all;
        }

        public static OpDepositoryElement GetElement(DataProvider dataProvider, long? serialNbr, string externalSN,
            int idPackage, long idArticle, bool? removed, int? idReference)
        {
            OpDepositoryElement depElemToUpdate = null;
            int[] idRefTab = null;
            if(idReference.HasValue)
                idRefTab = new int[1]{ idReference.Value };
            if (serialNbr.HasValue)
            {
                depElemToUpdate = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { idPackage },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                SerialNbr: new long[]{ serialNbr.Value },
                                                                                IdReference: idRefTab
                                                                                    ).FirstOrDefault();
            }
            else if (externalSN != null && externalSN.Trim().Length > 0)
            {
                depElemToUpdate = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { idPackage },
                                                                            IdArticle: new long[] { idArticle },
                                                                            Removed: removed,
                                                                            ExternalSn: externalSN,
                                                                            IdReference: idRefTab
                                                                                ).FirstOrDefault();
            }else
            {
                depElemToUpdate = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { idPackage },
                                                                            IdArticle: new long[] { idArticle },
                                                                            Removed: removed,
                                                                            IdReference: idRefTab
                                                                                ).FirstOrDefault();
            }
                
            return depElemToUpdate;
        }

        public static List<OpDepositoryElement> GetSOTDepositoryAvailableElements(DataProvider dataProvider, OpOperator operatorSOT,
            bool loadNavigationProperties = true, bool autoTransaction = true)
        {
            if (operatorSOT.DepositoryLocation == null)
            {
                operatorSOT.DepositoryLocation = DepositoryComponent.GetFitterDepository(dataProvider, operatorSOT);
            }
            if (operatorSOT.DepositoryLocation == null)
                throw new Exception(ResourcesText.FitterDepositoryNotDefined);

            List<OpDepositoryElement> depositoryContent = DepositoryComponent.GetElements(dataProvider, operatorSOT.DepositoryLocation, null, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction);
            depositoryContent.RemoveAll(c => c.EndDate.HasValue);

            List<OpPackage> packagesForOperator = PackageComponent.GetPackagesForOperator(dataProvider, operatorSOT.IdOperator);
            packagesForOperator.RemoveAll(c => c.FromAIUT == true); //paczki od AIUT
            packagesForOperator.RemoveAll(c => c.Received == true); //paczki odebrane
            packagesForOperator.RemoveAll(c => c.IdPackageStatus == (int)Enums.PackageStatus.Removed);
            packagesForOperator.RemoveAll(c => c.IdPackageStatus == (int)Enums.PackageStatus.Expired);
            packagesForOperator.RemoveAll(c => c.IdPackageStatus.In(PackageComponent.GetPackageOrderStatusList().ToArray()));

            //usuniecie z magazynku urzadzen ktore znajduja sie w paczkach utworzonych przez operatora
            foreach (OpPackage package in packagesForOperator)
            {
                List<OpDepositoryElement> elemList = DepositoryComponent.GetElements(dataProvider, package, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction);
                //elemList.RemoveAll(d => d.EndDate.HasValue);

                foreach (OpDepositoryElement elem in elemList)
                {
                    bool correctReference = false;
                    /*
                     * Znalazły się przypadki że referencaj wskazuje na nie istniejący już wiersz, oprócz tego że trzeba znaleźć tego powody
                     * zabezpieczamy się tutaj na takie sytuacje
                    */
                    if (elem.IdReference.HasValue)
                    {
                        int cntBefore = depositoryContent.Count;
                        depositoryContent.RemoveAll(c => c.IdDepositoryElement == elem.IdReference.Value);
                        if (depositoryContent.Count < cntBefore)
                            correctReference = true;
                    }
                    if(!correctReference)
                    {
                        int cntBefore = depositoryContent.Count;
                        if (elem.Article.IsUnique && (elem.SerialNbr.HasValue || (elem.ExternalSn != null && !String.IsNullOrEmpty(elem.ExternalSn.Trim()))))
                        {
                            if (elem.SerialNbr != null)
                                depositoryContent.RemoveAll(c => c.SerialNbr == elem.SerialNbr);
                            if (elem.ExternalSn != null)
                                depositoryContent.RemoveAll(c => c.ExternalSn == elem.ExternalSn);
                        }
                        else
                        {
                            OpDepositoryElement tmp = depositoryContent.FirstOrDefault(c => c.IdArticle == elem.IdArticle);
                            if (tmp != null)
                                depositoryContent.Remove(tmp);
                        }
                        /*
                        if (depositoryContent.Count == cntBefore)
                        {
                        }
                        */
                    }

                }
            }
            return depositoryContent;
        }

        public static OpLocation GetFitterDepository(DataProvider dataProvider, OpOperator fitter)
        {
            OpLocation fitterDepository = fitter.DepositoryLocation;
            if (fitterDepository == null)
            {
                OpOperatorData fitterDepositoryId = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, IdOperator: new int[] { fitter.IdOperator }, IdDataType: new long[] { DataType.DEPOSITORY_ID }, customWhereClause: "[VALUE] is not null").FirstOrDefault();
                long idLocation = 0;
                if (fitterDepositoryId != null && fitterDepositoryId.Value != null && long.TryParse(fitterDepositoryId.Value.ToString(), out idLocation))
                {
                    if (idLocation > 0)
                        fitterDepository = dataProvider.GetLocation(idLocation);
                }
            }
          //  if (fitterDepository == null)
          //      throw new Exception(ResourcesText.FitterDepositoryNotDefined);
            return fitterDepository;
            
        }

        public static OpOperator GetFitterByDepository(DataProvider dataProvider, long idDepositoryLocation)
        {
            OpOperatorData fitterDepositoryId = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, customWhereClause: String.Format("[ID_DATA_TYPE] = {0} and [VALUE] is not null and cast([VALUE] as bigint) = {1}", DataType.DEPOSITORY_ID, idDepositoryLocation)).FirstOrDefault();
            if (fitterDepositoryId == null)
                return null;
            return dataProvider.GetOperator(fitterDepositoryId.IdOperator);
        }

        public static List<OpDepositoryElement> GetOrderedElements(DataProvider dataProvider, OpOperator contracting)
        {
            List<OpDepositoryElement> retList = new List<OpDepositoryElement>();

            string customWhereClause = "[ID_OPERATOR_RECEIVER] " + (contracting != null ? "= " + contracting.IdOperator.ToString() : "is not null"); 

            List<OpPackage> packagesInCompletion = dataProvider.GetPackageFilter(loadNavigationProperties: true,
                                                                                 IdPackageStatus: new int[] { (int)Enums.PackageStatus.ToCompletion, 
                                                                                                              (int)Enums.PackageStatus.InCompletion, 
                                                                                                              (int)Enums.PackageStatus.OrderPartiallyCompleted },
                                                                                 customWhereClause: customWhereClause);
            if (packagesInCompletion != null && packagesInCompletion.Count > 0)
            {
                retList = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: true,
                                                                  IdPackage: packagesInCompletion.Select(p => p.IdPackage).ToArray(),
                                                                  customWhereClause: "[END_DATE] is null");
            }

            return retList;
        }

        public static OpArticle GetArticleForDevice(DataProvider dataProvider, long serialNbr)
        {
            OpArticle retValue = null;

            List<OpDepositoryElement> deList = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, SerialNbr: new long[] { serialNbr });
            if (deList != null && deList.Count > 0)
            {
                long idArticle = deList.Find(d => d.IdDepositoryElement == deList.Max(d1 => d1.IdDepositoryElement)).IdArticle;
                retValue = dataProvider.GetArticle(idArticle);
            }
            else
            {
                OpDevice dItem = dataProvider.GetDevice(serialNbr);
                if (dItem != null)
                {
                    retValue = dataProvider.GetArticleFilter(loadNavigationProperties: false, customWhereClause: "[NAME] like '" + dItem.DeviceOrderNumber.Name + " "
                                                                                                                                 + dItem.DeviceOrderNumber.FirstQuarter
                                                                                                                                 + "-XXXX-XXXX'").FirstOrDefault();
                    if (retValue == null)
                    {
                        retValue = new OpArticle();
                        retValue.Name = dItem.DeviceOrderNumber.Name + " " + dItem.DeviceOrderNumber.FirstQuarter + "-XXXX-XXXX";
                        retValue.Description = "Automatically generated";
                        retValue.Visible = true;
                        retValue.IsAiut = true;
                        retValue.IsUnique = true;
                        retValue.IdArticle = dataProvider.SaveArticle(retValue);
                    }
                }
            }

            return retValue;
        }

        #endregion

        #region Save|Assign|Remove functions

        #region Save
        public static OpDepositoryElement Save(DataProvider dataProvider, OpDepositoryElement objectToSave)
        {
            dataProvider.SaveDepositoryElement(objectToSave);

            return dataProvider.GetDepositoryElement(objectToSave.IdDepositoryElement);
        }

        public static List<OpDepositoryElementData> SaveData(DataProvider dataProvider, List<OpDepositoryElementData> objectToSave)
        {
            int idDepositoryElem = 0;
            if(objectToSave.Count == 0)
                return null;
            else
                idDepositoryElem = objectToSave[0].IdDepositoryElement;
            List<long> itemsToRemoveFromDataList = new List<long>();
            foreach (OpDepositoryElementData deditem in objectToSave)
            {
                if (deditem.OpState == OpChangeState.New ||
                        deditem.OpState == OpChangeState.Modified)
                {
                    deditem.IdDepositoryElementData = dataProvider.SaveDepositoryElementData(deditem);
                }
                else if(deditem.OpState == OpChangeState.Delete)
                {
                    if (deditem.IdDepositoryElementData > 0)
                        dataProvider.DeleteDepositoryElementData(deditem);
                    itemsToRemoveFromDataList.Add(deditem.IdDepositoryElementData);
                }
            }
            if (itemsToRemoveFromDataList.Count > 0)
                objectToSave.RemoveAll(d => d.IdDepositoryElementData.In(itemsToRemoveFromDataList.ToArray()));
            return dataProvider.GetDepositoryElementDataFilter(IdDepositoryElement: new int[]{idDepositoryElem});
        }
        #endregion

        #region Assign functions

        #region Task

        public static OpDepositoryElement AssignElementToTask(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, OpTask task, string externalSN, bool ordered, bool removed, string notes)
        {
            return AssignElementToTask(dataProvider, LoggedOperator, idArticle, serialNbr, null, task, externalSN, ordered, removed, notes, null);
        }

        public static OpDepositoryElement AssignElementToTask(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, OpTask task, string externalSN, bool ordered, bool removed, string notes, OpDeviceStateType stateType)
        {
            return AssignElementToTask(dataProvider, LoggedOperator, idArticle, serialNbr, null, task, externalSN, ordered, removed, notes, stateType);
        }

        public static OpDepositoryElement AssignElementToTask(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, int? id_reference, OpTask task, string externalSN, bool ordered, bool removed, string notes)
        {
            return AssignElementToTask(dataProvider, LoggedOperator, idArticle, serialNbr, id_reference, task, externalSN, ordered, removed, notes, null);
        }

        public static OpDepositoryElement AssignElementToTask(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, int? id_reference, OpTask task, string externalSN, bool ordered, bool removed, string notes, OpDeviceStateType stateType)
        {
            //OpDepositoryElement depositoryElement = dataProvider.DepositoryElement.FirstOrDefault(
            //    lo => lo.IdTask.HasValue && lo.IdTask == task.IdTask &&
            //        lo.IdArticle == idArticle &&
            //        lo.Removed == removed &&
            //        lo.Ordered == ordered &&
            //        ((serialNbr.HasValue && lo.SerialNbr == serialNbr) || !serialNbr.HasValue) &&
            //        !lo.EndDate.HasValue);

            OpDepositoryElement depositoryElement = null;

            if (id_reference.HasValue)
            {
                if (serialNbr.HasValue)
                {
                    depositoryElement = dataProvider.GetDepositoryElementFilter(IdTask: new int[] { task.IdTask },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                Ordered: ordered,
                                                                                IdReference: new int[] { id_reference.Value },
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null(),
                                                                                SerialNbr: new long[] { serialNbr.Value }).FirstOrDefault();
                }
                else
                {
                    if (externalSN != null)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdTask: new int[] { task.IdTask },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                Ordered: ordered,
                                                                                ExternalSn: externalSN,
                                                                                IdReference: new int[] { id_reference.Value },
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
                    }
                }
            }
            else
            {
                if (serialNbr.HasValue)
                {
                    depositoryElement = dataProvider.GetDepositoryElementFilter(IdTask: new int[] { task.IdTask },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                Ordered: ordered,
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null(),
                                                                                SerialNbr: new long[] { serialNbr.Value }).FirstOrDefault();
                }
                else
                {
                    if (externalSN != null)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdTask: new int[] { task.IdTask },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                Ordered: ordered,
                                                                                ExternalSn: externalSN,
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
                    }
                }
            }

            if (depositoryElement == null)
            {
                //depositoryElement = new OpDepositoryElement(dataProvider, task.IdTask, idArticle, 
                //    serialNbr.HasValue ? OpDeviceTypeClass.Enum.RADIO_DEVICE : OpDeviceTypeClass.Enum.OTHER, 
                //    OpDeviceStateType.Enum.IDLE, serialNbr, externalSN, ordered, removed, notes);

                depositoryElement = new OpDepositoryElement(dataProvider, null, task.IdTask, null, idArticle,
                    serialNbr.HasValue ? OpDeviceTypeClass.Enum.RADIO_DEVICE : OpDeviceTypeClass.Enum.OTHER,
     OpDeviceStateType.Enum.IDLE, serialNbr, externalSN, removed, ordered, null, notes, id_reference);
            }

            if (stateType != null)
            {
                depositoryElement.DeviceStateType = stateType;
                depositoryElement.IdDeviceStateType = stateType.IdDeviceStateType;
            }

            depositoryElement = Save(dataProvider, depositoryElement);

            return depositoryElement;
        }

        #endregion
        #region Package

        public static OpDepositoryElement AssignElementToPackage(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, OpPackage package, string externalSN, bool removed, string notes)
        {
            return AssignElementToPackage(dataProvider, LoggedOperator, idArticle, serialNbr, null, package, externalSN, removed, notes);
        }

        public static OpDepositoryElement AssignElementToPackage(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, int? id_reference, OpPackage package, string externalSN, bool removed, string notes)
        {
            //OpDepositoryElement depositoryElement = dataProvider.GetAllDepositoryElement().FirstOrDefault(
            //    lo => lo.IdPackage.HasValue && lo.IdPackage == package.IdPackage &&
            //        lo.IdArticle == idArticle &&
            //        lo.Removed == removed &&
            //        ((serialNbr.HasValue && lo.SerialNbr == serialNbr) || (!serialNbr.HasValue && lo.Article.IsUnique && externalSN !=null && externalSN == lo.ExternalSn)
            //        || (!serialNbr.HasValue && !lo.Article.IsUnique && externalSN == null)) &&
            //        !lo.EndDate.HasValue);

            OpDepositoryElement depositoryElement = null;

            if (id_reference.HasValue)
            {
                if (serialNbr.HasValue)
                {
                    depositoryElement = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { package.IdPackage },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null(),
                                                                                IdReference: new int[] { id_reference.Value },
                                                                                SerialNbr: new long[] { serialNbr.Value }).FirstOrDefault();
                }
                else
                {
                    if (externalSN != null)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { package.IdPackage },
                                                                                    IdArticle: new long[] { idArticle },
                                                                                    Removed: removed,
                                                                                    ExternalSn: externalSN,
                                                                                    IdReference: new int[] { id_reference.Value },
                                                                                    EndDate: Data.DB.TypeDateTimeCode.Null()
                                                                                     ).FirstOrDefault();
                    }
                }
            }
            else
            {
                if (serialNbr.HasValue)
                {
                    depositoryElement = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { package.IdPackage },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null(),
                                                                                SerialNbr: new long[] { serialNbr.Value }).FirstOrDefault();
                }
                else
                {
                    if (externalSN != null)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { package.IdPackage },
                                                                                    IdArticle: new long[] { idArticle },
                                                                                    Removed: removed,
                                                                                    ExternalSn: externalSN,
                                                                                    EndDate: Data.DB.TypeDateTimeCode.Null()
                                                                                     ).FirstOrDefault();
                    }
                }
            }

            if (depositoryElement == null)
            {
                //depositoryElement = new OpDepositoryElement(dataProvider, package.IdPackage, idArticle, 
                //    serialNbr.HasValue ? OpDeviceTypeClass.Enum.RADIO_DEVICE : OpDeviceTypeClass.Enum.OTHER, 
                //    OpDeviceStateType.Enum.IDLE, serialNbr, externalSN, removed, notes);

                depositoryElement = new OpDepositoryElement(dataProvider, null, null, package.IdPackage, idArticle,
    serialNbr.HasValue ? OpDeviceTypeClass.Enum.RADIO_DEVICE : OpDeviceTypeClass.Enum.OTHER,
OpDeviceStateType.Enum.IDLE, serialNbr, externalSN, removed, false, null, notes, id_reference);
                //depositoryElement = Save(dataProvider, depositoryElement);
            }

            depositoryElement = Save(dataProvider, depositoryElement);
            /*else
            {
                if (!serialNbr.HasValue) //jeśli probojemy dodac artykuł (bez SN) ktory juz istnieje aktualizujeym tylko count
                {
                    //depositoryElement.Count += count;
                    if (notes != null)
                    {
                        if (!String.IsNullOrEmpty(depositoryElement.Notes))
                            depositoryElement.Notes += ", " + notes;
                        else
                            depositoryElement.Notes = notes;
                    }
                    depositoryElement = Save(dataProvider, depositoryElement);
                }
            }*/

            return depositoryElement;
        }

        public static OpDepositoryElement AssignValidatedElementToPackage(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, int? id_reference, OpPackage package, string externalSN, bool removed, string notes)
        {
            OpDepositoryElement depositoryElement = new OpDepositoryElement(dataProvider, null, null, package.IdPackage, idArticle,
                                                                            serialNbr.HasValue ? OpDeviceTypeClass.Enum.RADIO_DEVICE : OpDeviceTypeClass.Enum.OTHER,
                                                                            OpDeviceStateType.Enum.IDLE, serialNbr, externalSN, removed, false, null, notes, id_reference);
                                                                                        //depositoryElement = Save(dataProvider, depositoryElement);
            

            depositoryElement = Save(dataProvider, depositoryElement);

            return depositoryElement;
        }

        public static void AcceptPackageElementFitter(DataProvider dataProvider, OpOperator loggedOperator, OpDepositoryElement PackageElement)
        {
            //Przypisanie do lokalizacji
            AssignElementToLocation(dataProvider, loggedOperator, PackageElement.IdArticle, PackageElement.SerialNbr,
                            loggedOperator.DepositoryLocation, PackageElement.ExternalSn, false, PackageElement.Notes, dataProvider.GetDeviceStateType((int)OpDeviceStateType.Enum.SOT_RECEIVED));
            if (PackageElement.SerialNbr.HasValue)
            {
                OpDevice device = dataProvider.GetDevice(PackageElement.SerialNbr.Value, true);
                if (device != null)
                {
                    device.DeviceStateType = dataProvider.GetDeviceStateType((int)OpDeviceStateType.Enum.SOT_RECEIVED);
                    DeviceComponent.Save(dataProvider, device);
                }
            }

            /*
                     * Rezygnujemy z tego ponieważ powoduje to sytuacje w ktorych jeśli paczka wyszła
                     * dzięki innym uprawnieniom to urządzenia nie zejdą z magazynu nadawcy co jest 
                     * niedopuszczalne
                     * Zdejmujemu więc zawsze jesli występuje w OpDepositoryElement wartosc w polu
                     * IdReference, poniważ paczki generowane z IMR SC mają w tym polu null
                     */
            //Dla przesyłania urządzeń między serwisantami, trzeba zrobić demontaż z magazynu nadawcy
            if (PackageElement.IdReference.HasValue)
            {
                List<OpDepositoryElement> itemsUnmount = dataProvider.GetDepositoryElementFilter(
                                    IdDepositoryElement: new int[] { PackageElement.IdReference.Value });
                foreach (OpDepositoryElement deItem in itemsUnmount)
                {
                    deItem.EndDate = DateTime.UtcNow;
                    DepositoryComponent.Save(dataProvider, deItem);
                }
            }
            //Akceptacja elementu
            PackageElement.EndDate = dataProvider.DateTimeNow;
            PackageElement.Accepted = true;
            Save(dataProvider, PackageElement);
        }

        public static void AssignOrderedElementsToPackage(DataProvider dataProvider, OpOperator loggedOperator, OpPackage package, List<OpDepositoryElement> elementsToAssign, List<OpDepositoryElement> elementsToClose = null)
        {
            if (elementsToClose == null)
            {
                //ID_REFERENCE z elementsToAssign wskazuje na elementy do zamknięcia
                List<int> idReferenceList = elementsToAssign.Where(d => d.IdReference.HasValue).Select(d => d.IdReference.Value).ToList();
                if (idReferenceList != null && idReferenceList.Count > 0)
                {
                    elementsToClose = dataProvider.GetDepositoryElementFilter(loadDeviceDetails: true, IdDepositoryElement: idReferenceList.ToArray());
                }
            }

            List<OpPackage> fitterOrderPackages = new List<OpPackage>();

            for (int i = 0; i < elementsToAssign.Count; i++)
            {
                OpDepositoryElement orderItemToClose = elementsToClose.Find(d => d.IdDepositoryElement == elementsToAssign[i].IdReference.Value);
                orderItemToClose.SerialNbr = elementsToAssign[i].SerialNbr;
                orderItemToClose.ExternalSn = elementsToAssign[i].ExternalSn;
                orderItemToClose.EndDate = dataProvider.DateTimeNow;

                if (!fitterOrderPackages.Exists(d => d.IdPackage == orderItemToClose.IdPackage))
                    fitterOrderPackages.Add(orderItemToClose.Package);

                DepositoryComponent.Save(dataProvider, orderItemToClose);
                elementsToAssign[i].Package = package;
                elementsToAssign[i] = DepositoryComponent.Save(dataProvider, elementsToAssign[i]);
            }
            if (fitterOrderPackages.Count > 0)
            {
                List<OpDepositoryElement> orderPackageElements = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, IdPackage: fitterOrderPackages.Select(p => p.IdPackage).ToArray());
                foreach (OpPackage pLoopItem in fitterOrderPackages.Where(p => p.IdPackageStatus.In(new int[] { (int)Enums.PackageStatus.ToCompletion, (int)Enums.PackageStatus.InCompletion, (int)Enums.PackageStatus.OrderPartiallyCompleted })))
                {
                    int targetStatus = (int)Enums.PackageStatus.OrderPartiallyCompleted;
                    if (pLoopItem.IdPackageStatus == (int)Enums.PackageStatus.ToCompletion)
                    {
                        //dla ułatwienai dopuszcamy możlwiość reazlicji elementów ze statusu do kompletacji
                        if (!orderPackageElements.Exists(d => d.IdPackage == pLoopItem.IdPackage && !d.EndDate.HasValue))
                        {
                            targetStatus = (int)Enums.PackageStatus.OrderCompleted;
                        }
                        else
                        {
                            targetStatus = (int)Enums.PackageStatus.OrderPartiallyCompleted;
                        }
                    }
                    else
                    {
                        if (pLoopItem.IdPackageStatus != (int)Enums.PackageStatus.OrderCompleted && !orderPackageElements.Exists(d => d.IdPackage == pLoopItem.IdPackage
                                                                                                                                         && !d.EndDate.HasValue))
                        {
                            targetStatus = (int)Enums.PackageStatus.OrderCompleted;
                        }
                        if (pLoopItem.IdPackageStatus != (int)Enums.PackageStatus.OrderPartiallyCompleted && orderPackageElements.Exists(d => d.IdPackage == pLoopItem.IdPackage
                                                                                                                                         && !d.EndDate.HasValue))
                        {
                            targetStatus = (int)Enums.PackageStatus.OrderPartiallyCompleted;
                        }
                    }
                    if (targetStatus != pLoopItem.IdPackageStatus)
                    {
                        pLoopItem.IdPackageStatus = targetStatus;
                        if (targetStatus != (int)Enums.PackageStatus.OrderCompleted)
                            pLoopItem.RealizationDate = null;
                        else
                        {
                            if (!pLoopItem.RealizationDate.HasValue)
                                pLoopItem.RealizationDate = dataProvider.DateTimeNow;
                        }
                        PackageComponent.Save(dataProvider, loggedOperator, pLoopItem);
                    }
                }
            }
        }

        #endregion
        #region Location

        public static OpDepositoryElement AssignElementToLocation(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, OpLocation location, string externalSN, bool removed, string notes)
        {
            return AssignElementToLocation(dataProvider, LoggedOperator, idArticle, serialNbr, null, location, externalSN, removed, notes, null);
        }

        public static OpDepositoryElement AssignElementToLocation(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, int? id_reference, OpLocation location, string externalSN, bool removed, string notes)
        {
            return AssignElementToLocation(dataProvider, LoggedOperator, idArticle, serialNbr, id_reference, location, externalSN, removed, notes, null);
        }

        public static OpDepositoryElement AssignElementToLocation(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, OpLocation location, string externalSN, bool removed, string notes, OpDeviceStateType stateType)
        {
            return AssignElementToLocation(dataProvider, LoggedOperator, idArticle, serialNbr, null, location, externalSN, removed, notes, stateType);
        }

        public static OpDepositoryElement AssignElementToLocation(DataProvider dataProvider, OpOperator LoggedOperator, long idArticle, long? serialNbr, int? id_reference, OpLocation location, string externalSN, bool removed, string notes, OpDeviceStateType stateType)
        {
            //OpDepositoryElement depositoryElement = dataProvider.DepositoryElement.FirstOrDefault(
            //    lo => lo.IdLocation.HasValue && lo.IdLocation == location.IdLocation &&
            //        lo.IdArticle == idArticle &&
            //        lo.Removed == removed &&
            //        ((serialNbr.HasValue && lo.SerialNbr == serialNbr) || !serialNbr.HasValue) &&
            //        !lo.EndDate.HasValue);


            OpDepositoryElement depositoryElement = null;

            if (id_reference.HasValue)
            {
                if (serialNbr.HasValue)
                {
                    if (id_reference.HasValue)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdLocation: new long[] { location.IdLocation },
                                                                                    IdArticle: new long[] { idArticle },
                                                                                    Removed: removed,
                                                                                    EndDate: Data.DB.TypeDateTimeCode.Null(),
                                                                                    IdReference: new int[] { id_reference.Value },
                                                                                    SerialNbr: new long[] { serialNbr.Value }).FirstOrDefault();
                    }
                }
                else
                {
                    if (externalSN != null)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdLocation: new long[] { location.IdLocation },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                ExternalSn: externalSN,
                                                                                IdReference: new int[] { id_reference.Value },
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
                    }
                }
            }
            else
            {
                if (serialNbr.HasValue)
                {
                    if (id_reference.HasValue)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdLocation: new long[] { location.IdLocation },
                                                                                    IdArticle: new long[] { idArticle },
                                                                                    Removed: removed,
                                                                                    EndDate: Data.DB.TypeDateTimeCode.Null(),
                                                                                    SerialNbr: new long[] { serialNbr.Value }).FirstOrDefault();
                    }
                }
                else
                {
                    if (externalSN != null)
                    {
                        depositoryElement = dataProvider.GetDepositoryElementFilter(IdLocation: new long[] { location.IdLocation },
                                                                                IdArticle: new long[] { idArticle },
                                                                                Removed: removed,
                                                                                ExternalSn: externalSN,
                                                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
                    }
                }
            }

            if (depositoryElement == null)
            {
                depositoryElement = new OpDepositoryElement(dataProvider, location.IdLocation, null, null, idArticle, serialNbr.HasValue ? OpDeviceTypeClass.Enum.RADIO_DEVICE : OpDeviceTypeClass.Enum.OTHER,
                     OpDeviceStateType.Enum.IDLE, serialNbr, externalSN, removed, false, null, notes, id_reference);
            }

            if (stateType != null)
            {
                depositoryElement.DeviceStateType = stateType;
                depositoryElement.IdDeviceStateType = stateType.IdDeviceStateType;
            }

            depositoryElement = Save(dataProvider, depositoryElement);

            return depositoryElement;
        }

        public static OpDepositoryElement AssignElementToLocation(DataProvider dataProvider, OpOperator loggedOperator, OpDepositoryElement depositoryElement, 
            bool fullValidation = true, bool allowCreateObjects = false)
        {
            if (depositoryElement == null)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationError, true, new object[]{ "Object not set to and instance of an object (depositoryElement)" });
                throw new Exception("Object not set to and instance of an object (depositoryElement)");
            }
            BaseComponent.Log(EventID.Forms.AssignToLocationInfo, true, new object[]{ (depositoryElement.SerialNbr.HasValue ? depositoryElement.SerialNbr.Value.ToString() : "-"),
                                                                                      (!String.IsNullOrEmpty(depositoryElement.ExternalSn) ? depositoryElement.ExternalSn : "-"),
                                                                                      (depositoryElement.IdLocation.HasValue ? depositoryElement.IdLocation.Value.ToString() : "-"),
                                                                                      depositoryElement.IdArticle,
                                                                                      depositoryElement.StartDate });
            if (depositoryElement.IdArticle == 0)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationArticleNotValid, true, new object[]{  });
                throw new Exception("Article is not valid");
            }
            if (depositoryElement.EndDate.HasValue)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationClosedElement, true, new object[]{  });
                throw new Exception("Cannot assign closed element");
            }
            if (depositoryElement.IdTask.HasValue)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationWrongContextTask, true, new object[]{ depositoryElement.IdTask.Value });
                throw new Exception("Location assign wrong context (IdTask not valid)");
            }
            if(depositoryElement.IdPackage.HasValue)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationWrongContextPackage, true, new object[]{ depositoryElement.IdPackage.Value });
                throw new Exception("Location assign wrong context (IdPackage not valid)");
            }
            if (!depositoryElement.IdLocation.HasValue)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationNoDepository, true, new object[]{  });
                throw new Exception("Depository id not given");
            }
            OpArticle aItem = dataProvider.GetArticle(depositoryElement.IdArticle);
            if (aItem == null)
            {
                // Wszystko fajnie ze tu zapiszemy article ale go nie pobierzemy wiec
                // wyleci null reference potem przy if(aItem.IsUnique wiec dodalem pobranie ponownie
                if (depositoryElement.Article != null && allowCreateObjects)
                    dataProvider.SaveArticle(depositoryElement.Article);
                else
                {
                    BaseComponent.Log(EventID.Forms.AssignToLocationCannotCreateArticle, true, new object[]{  });
                    throw new Exception("New article object not given, cannot create in database");
                }
            }
            OpLocation lItem = dataProvider.GetLocation(depositoryElement.IdLocation.Value);
            if (lItem == null)
            {
                if (depositoryElement.Location != null && allowCreateObjects)
                    dataProvider.SaveLocation(depositoryElement.Location);
                else
                {
                    BaseComponent.Log(EventID.Forms.AssignToLocationCannotCreateLocation, true, new object[]{  });
                    throw new Exception("New location object not given, cannot create in database");
                }
            }

            // Jesli po tym pobraniu nadal bedzie null to nic z tym nie zrobimy chyba
            if (aItem == null)
            {
                aItem = dataProvider.GetArticle(depositoryElement.IdArticle);
            }

            if(aItem.IsUnique != (depositoryElement.SerialNbr.HasValue || !String.IsNullOrEmpty(depositoryElement.ExternalSn)))
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationIncosistentArticle, true, new object[]{ aItem.IdArticle, aItem.IsUnique });
                throw new Exception(String.Format("Article (Id: {0}) properties (IsUnique: {1}) is inconsistent with given depository element", aItem.IdArticle, aItem.IsUnique));
            }

            List<OpDepositoryElement> existingItems = new List<OpDepositoryElement>();
            if (depositoryElement.SerialNbr.HasValue)
            {
                existingItems = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, 
                                                                        SerialNbr: new long[] { depositoryElement.SerialNbr.Value }, customWhereClause: "[END_DATE] is null");
            }
            else if (depositoryElement.ExternalSn != null && !String.IsNullOrEmpty(depositoryElement.ExternalSn.Trim()))
            {
                existingItems = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                        ExternalSn: depositoryElement.ExternalSn, customWhereClause: "[END_DATE] is null");
            }
            if (existingItems.Count(d => d.IdLocation != depositoryElement.IdLocation) > 0)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationExistingOnAnotherDepository, true, new object[]{ (depositoryElement.SerialNbr.HasValue ? depositoryElement.SerialNbr.Value.ToString() : "-"),
                                                                                                        (!String.IsNullOrEmpty(depositoryElement.ExternalSn) ? depositoryElement.ExternalSn : "-"),
                                                                                                        String.Join(",", existingItems.Where(d => d.IdLocation != depositoryElement.IdLocation).Select(d => d.IdLocation.Value)) });
                throw new Exception(String.Format("Element (Sn: {0}, ExtSn: {1}) already exists at another depository (Ids: {2})",
                                                  (depositoryElement.SerialNbr.HasValue ? depositoryElement.SerialNbr.Value.ToString() : "-"),
                                                  (!String.IsNullOrEmpty(depositoryElement.ExternalSn) ? depositoryElement.ExternalSn : "-"),
                                                  String.Join(",", existingItems.Where(d => d.IdLocation != depositoryElement.IdLocation).Select(d => d.IdLocation.Value))));
            }
            if(existingItems.Count(d => d.IdLocation == depositoryElement.IdLocation) != 0)
            {
                BaseComponent.Log(EventID.Forms.AssignToLocationExistingOnDepository, true, new object[]{ (depositoryElement.SerialNbr.HasValue ? depositoryElement.SerialNbr.Value.ToString() : "-"),
                                                                                                        (!String.IsNullOrEmpty(depositoryElement.ExternalSn) ? depositoryElement.ExternalSn : "-"),
                                                                                                        String.Join(",", existingItems.Where(d => d.IdLocation == depositoryElement.IdLocation).Select(d => d.IdLocation.Value)) });
                throw new Exception(String.Format("Element (Sn: {0}, ExtSn: {1}) already exists at depository (Ids: {2})",
                                                  (depositoryElement.SerialNbr.HasValue ? depositoryElement.SerialNbr.Value.ToString() : "-"),
                                                  (!String.IsNullOrEmpty(depositoryElement.ExternalSn) ? depositoryElement.ExternalSn : "-"),
                                                  String.Join(",", existingItems.Where(d => d.IdLocation != depositoryElement.IdLocation).Select(d => d.IdLocation.Value))));
            }
            depositoryElement.IdDepositoryElement = 0;
            depositoryElement.IdDepositoryElement = dataProvider.SaveDepositoryElement(depositoryElement);

            BaseComponent.Log(EventID.Forms.AssignToLocationReturnInfo, true, new object[]{ (depositoryElement.SerialNbr.HasValue ? depositoryElement.SerialNbr.Value.ToString() : "-"),
                                                                                      (!String.IsNullOrEmpty(depositoryElement.ExternalSn) ? depositoryElement.ExternalSn : "-"),
                                                                                      (depositoryElement.IdLocation.HasValue ? depositoryElement.IdLocation.Value.ToString() : "-"),
                                                                                      depositoryElement.IdArticle,
                                                                                      depositoryElement.StartDate,
                                                                                      depositoryElement.IdDepositoryElement });

            return depositoryElement;
        }

        #endregion

        #endregion

        #region Remove functions

        public static void RemoveDepositoryElement(DataProvider dataProvider, OpDepositoryElement element)
        {
            OpDepositoryElement depositoryElement = dataProvider.GetDepositoryElement(element.IdDepositoryElement, true);

            if (depositoryElement != null)
            {
                depositoryElement.EndDate = dataProvider.DateTimeNow;
                Save(dataProvider, depositoryElement);
            }
        }

        #region Location

        public static void RemoveDeviceFromLocation(DataProvider dataProvider, OpDevice device, OpLocation location)
        {
            //OpDepositoryElement depositoryElement = dataProvider.DepositoryElement.LastOrDefault(
            //    lo => lo.IdLocation.HasValue && lo.IdLocation == location.IdLocation &&
            //        lo.SerialNbr.HasValue && lo.SerialNbr == device.SerialNbr &&
            //        !lo.EndDate.HasValue);

            OpDepositoryElement depositoryElement = dataProvider.GetDepositoryElementFilter(
                                                IdLocation: new long[] { location.IdLocation },
                                                SerialNbr: new long[] { device.SerialNbr },
                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();

            if (depositoryElement != null)
            {
                depositoryElement.EndDate = dataProvider.DateTimeNow;
                Save(dataProvider, depositoryElement);
            }
        }

        public static void RemoveArticleFromLocation(DataProvider dataProvider, OpArticle article, string externalSN, bool removed, OpLocation location)
        {
            //OpDepositoryElement depositoryElement = dataProvider.DepositoryElement.LastOrDefault(
            //    lo => lo.IdLocation.HasValue && lo.IdLocation == location.IdLocation && lo.Removed == removed &&
            //        lo.IdArticle == article.IdArticle &&
            //        !lo.EndDate.HasValue);

            OpDepositoryElement depositoryElement = dataProvider.GetDepositoryElementFilter(
                                                IdLocation: new long[] { location.IdLocation },
                                                IdArticle: new long[] { article.IdArticle },
                                                ExternalSn: externalSN,
                                                Removed: removed,
                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();

            if (depositoryElement != null)
            {
                depositoryElement.EndDate = dataProvider.DateTimeNow;
                Save(dataProvider, depositoryElement);
            }

        }

        public static void RemoveElementFromLocation(DataProvider dataProvider, OpOperator loggedOperator, OpDepositoryElement depositoryElement, 
            bool fullValidation = true, bool allowCreateObjects = false)
        {
            if (depositoryElement == null)
            {
                BaseComponent.Log(EventID.Forms.RemoveFromLocationError, true, new object[] { "Object not set to and instance of an object (depositoryElement)" });
                throw new Exception("Object not set to and instance of an object (depositoryElement)");
            }
            BaseComponent.Log(EventID.Forms.RemoveFromLocationInfo, true, new object[]{ (depositoryElement.SerialNbr.HasValue ? depositoryElement.SerialNbr.Value.ToString() : "-"),
                                                                                      (!String.IsNullOrEmpty(depositoryElement.ExternalSn) ? depositoryElement.ExternalSn : "-"),
                                                                                      (depositoryElement.IdLocation.HasValue ? depositoryElement.IdLocation.Value.ToString() : "-"),
                                                                                      depositoryElement.IdArticle,
                                                                                      depositoryElement.StartDate });
            if (depositoryElement.IdArticle == 0)
            {
                BaseComponent.Log(EventID.Forms.RemoveFromLocationArticleNotValid, true, new object[] { });
                throw new Exception("Article is not valid");
            }
            if (depositoryElement.IdTask.HasValue)
            {
                BaseComponent.Log(EventID.Forms.RemoveFromLocationWrongContextTask, true, new object[] { depositoryElement.IdTask.Value });
                throw new Exception("Location assign wrong context (IdTask not valid)");
            }
            if (depositoryElement.IdPackage.HasValue)
            {
                BaseComponent.Log(EventID.Forms.RemoveFromLocationWrongContextPackage, true, new object[] { depositoryElement.IdPackage.Value });
                throw new Exception("Location assign wrong context (IdPackage not valid)");
            }
            if (!depositoryElement.IdLocation.HasValue)
            {
                BaseComponent.Log(EventID.Forms.RemoveFromLocationNoDepository, true, new object[] { });
                throw new Exception("Depository id not given");
            }
            OpArticle aItem = dataProvider.GetArticle(depositoryElement.IdArticle);
            if (aItem == null)
            {
                if (depositoryElement.Article != null && allowCreateObjects)
                    dataProvider.SaveArticle(depositoryElement.Article);
                else
                {
                    BaseComponent.Log(EventID.Forms.AssignToLocationCannotCreateArticle, true, new object[]{  });
                    throw new Exception("New article object not given, cannot create in database");
                }
            }
            else
                depositoryElement.Article = aItem;
            OpLocation lItem = dataProvider.GetLocation(depositoryElement.IdLocation.Value);
            if (lItem == null)
            {
                if (depositoryElement.Location != null && allowCreateObjects)
                    dataProvider.SaveLocation(depositoryElement.Location);
                else
                {
                    BaseComponent.Log(EventID.Forms.RemoveFromLocationCannotCreateLocation, true, new object[]{  });
                    throw new Exception("New location object not given, cannot create in database");
                }
            }
            else
                depositoryElement.Location = lItem;

            if(aItem.IsUnique != (depositoryElement.SerialNbr.HasValue || !String.IsNullOrEmpty(depositoryElement.ExternalSn)))
            {
                BaseComponent.Log(EventID.Forms.RemoveFromLocationIncosistentArticle, true, new object[]{ aItem.IdArticle, aItem.IsUnique });
                throw new Exception(String.Format("Article (Id: {0}) properties (IsUnique: {1}) is inconsistent with given depository element", aItem.IdArticle, aItem.IsUnique));
            }


            List<OpDepositoryElement> existingItems = new List<OpDepositoryElement>();
            if (depositoryElement.SerialNbr.HasValue)
            {
                existingItems = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                        IdLocation: new long[]{ depositoryElement.IdLocation.Value },
                                                                        SerialNbr: new long[] { depositoryElement.SerialNbr.Value }, customWhereClause: "[END_DATE] is null");
            }
            else if (depositoryElement.ExternalSn != null && !String.IsNullOrEmpty(depositoryElement.ExternalSn.Trim()))
            {
                existingItems = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                        IdLocation: new long[]{ depositoryElement.IdLocation.Value },
                                                                        ExternalSn: depositoryElement.ExternalSn, customWhereClause: "[END_DATE] is null");
            }
            else
            {
                existingItems = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                        IdLocation: new long[]{ depositoryElement.IdLocation.Value },
                                                                        IdArticle: new long[]{ depositoryElement.IdArticle },
                                                                        customWhereClause: "[END_DATE] is null");
                if(existingItems.Count > 1)
                    existingItems = existingItems.Where(d => d.StartDate == existingItems.Select(d1 => d1.StartDate).Min()).ToList();
                if (existingItems.Count > 1)//jakby daty był takie same
                    existingItems = new List<OpDepositoryElement>() { existingItems.First() };
            }
            
            BaseComponent.Log(EventID.Forms.RemoveFromLocationElementsToClose, true, new object[]{ existingItems.Count, String.Join(",", existingItems.Select(d => d.IdDepositoryElement)) });
            DateTime endDate = depositoryElement.EndDate.HasValue ? depositoryElement.EndDate.Value : dataProvider.DateTimeNow;
            foreach (OpDepositoryElement deItem in existingItems)
            {
                deItem.EndDate = endDate;

                RemoveElementFromLocation(dataProvider, aItem, deItem.SerialNbr, deItem.ExternalSn, deItem.Removed, lItem);
            }
            depositoryElement.EndDate = endDate;//jeśli nie była wcześniej ustawiona
            BaseComponent.Log(EventID.Forms.RemoveFromLocationElementsClosed, true, new object[]{ existingItems.Count, String.Join(",", existingItems.Select(d => d.IdDepositoryElement)) });
        }

        public static void RemoveElementFromLocation(DataProvider dataProvider, OpArticle article, long? serialNbr, string externalSN, bool removed, OpLocation location)
        {
            //OpDepositoryElement depositoryElement = dataProvider.DepositoryElement.LastOrDefault(
            //    lo => lo.IdLocation.HasValue && lo.IdLocation == location.IdLocation && lo.Removed == removed &&
            //        lo.IdArticle == article.IdArticle &&
            //        !lo.EndDate.HasValue);
            OpDepositoryElement depositoryElement;

            if (serialNbr.HasValue)
            {
                depositoryElement = dataProvider.GetDepositoryElementFilter(
                                                IdLocation: new long[] { location.IdLocation },
                                                IdArticle: new long[] { article.IdArticle },
                                                SerialNbr: new long[] { serialNbr.Value },
                                                Removed: removed,
                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
            }
            else
            {
                depositoryElement = dataProvider.GetDepositoryElementFilter(
                                                IdLocation: new long[] { location.IdLocation },
                                                IdArticle: new long[] { article.IdArticle },
                                                ExternalSn: externalSN,
                                                Removed: removed,
                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
            }

            if (depositoryElement != null)
            {
                depositoryElement.EndDate = dataProvider.DateTimeNow;
                Save(dataProvider, depositoryElement);
            }

        }

        public static void RemoveFromServicePool(DataProvider dataProvider, OpDistributor distributor, OpDevice device)
        {
            if (!distributor.EquipmentLocationId.HasValue)
            {
                OpDistributorData distrData = dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { distributor.IdDistributor }, IdDataType: new long[] { DataType.DEPOSITORY_ID }).FirstOrDefault();
                if (distrData == null || distrData.Value == null || String.IsNullOrEmpty(distrData.Value.ToString()))
                    return;
                distributor.EquipmentLocationId = Convert.ToInt64(distrData.Value);

                List<OpDepositoryElement> depElemList = dataProvider.GetDepositoryElementFilter(IdLocation: new long[] { distributor.EquipmentLocationId.Value },
                                                                                                SerialNbr: new long[] { device.SerialNbr },
                                                                                                customWhereClause: "[END_DATE] is null");
                if (depElemList != null)
                {
                    foreach (OpDepositoryElement deItem in depElemList)
                    {
                        RemoveFromServicePool(dataProvider, distributor, deItem);
                    }
                }
            }
        }

        public static void RemoveFromServicePool(DataProvider dataProvider, OpDistributor distributor, OpDepositoryElement element)
        {
            if (!distributor.EquipmentLocationId.HasValue)
            {
                OpDistributorData distrData = dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { distributor.IdDistributor }, IdDataType: new long[] { DataType.DEPOSITORY_ID }).FirstOrDefault();
                if (distrData == null || distrData.Value == null || String.IsNullOrEmpty(distrData.Value.ToString()))
                    return;
                distributor.EquipmentLocationId = Convert.ToInt64(distrData.Value);
            }
            OpLocation equipmentPool = dataProvider.GetLocation(distributor.EquipmentLocationId.Value);
            if (equipmentPool != null)
            {
                if (element.Article.IsUnique)
                {
                    if (element.SerialNbr.HasValue)
                    {
                        List<OpDepositoryElement> devices_in_pool = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[] { element.SerialNbr.Value }, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null());
                        foreach (OpDepositoryElement equipmentToRemove in devices_in_pool)
                        {
                            DepositoryComponent.RemoveDepositoryElement(dataProvider, equipmentToRemove);
                        }
                    }
                    else if (!String.IsNullOrEmpty(element.ExternalSn))
                    {
                        OpDepositoryElement deviceToRemove = dataProvider.GetDepositoryElementFilter(ExternalSn: element.ExternalSn, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault();
                        if (deviceToRemove != null)
                            DepositoryComponent.RemoveDepositoryElement(dataProvider, deviceToRemove);
                    }
                }
                else
                {
                    OpDepositoryElement elementToRemove = dataProvider.GetDepositoryElementFilter(IdArticle: new long[] { element.IdArticle }, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault(); ;
                    if (elementToRemove != null)
                    {
                        DepositoryComponent.RemoveDepositoryElement(dataProvider, elementToRemove);
                    }
                }
            }
        }

        public static void UninstalElementFromDepository(DataProvider dataProvider, OpDepositoryElement element)
        {
            element.EndDate = dataProvider.DateTimeNow;
            dataProvider.SaveDepositoryElement(element);
        }

        #endregion
        #region Task

        public static void RemoveElementFromTask(DataProvider dataProvider, OpArticle article, long? serialNbr, string externalSN, bool removed, OpTask task)
        {
            //OpDepositoryElement depositoryElement = dataProvider.DepositoryElement.LastOrDefault(
            //    lo => lo.IdLocation.HasValue && lo.IdLocation == location.IdLocation && lo.Removed == removed &&
            //        lo.IdArticle == article.IdArticle &&
            //        !lo.EndDate.HasValue);
            OpDepositoryElement depositoryElement;

            if (serialNbr.HasValue)
            {
                depositoryElement = dataProvider.GetDepositoryElementFilter(
                                                IdTask: new int[] { task.IdTask },
                                                IdArticle: new long[] { article.IdArticle },
                                                SerialNbr: new long[] { serialNbr.Value },
                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
            }
            else
            {
                depositoryElement = dataProvider.GetDepositoryElementFilter(
                                                IdTask: new int[] { task.IdTask },
                                                IdArticle: new long[] { article.IdArticle },
                                                ExternalSn: externalSN,
                                                EndDate: Data.DB.TypeDateTimeCode.Null()).FirstOrDefault();
            }

            if (depositoryElement != null)
            {
                depositoryElement.EndDate = dataProvider.DateTimeNow;
                Save(dataProvider, depositoryElement);
            }

        }

        #endregion
        #region Package

        #region RemovePackageWithOrderedElementsIfExists

        public static void RemovePackageWithOrderedElementsIfExists(DataProvider dataProvider, OpOperator loggedOperator, int idPackage)
        {
            List<OpDepositoryElement> deList = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, IdPackage: new int[] { idPackage });
            if (deList != null && deList.Count(d => d.Ordered && d.IdReference.HasValue) > 0)
            {
                OrderElementsWithdrawal(dataProvider, loggedOperator, deList.Where(d => d.Ordered && d.IdReference.HasValue).ToList());
            }
        }

        #endregion

        #region OrderElementWithdrawal

        public static void OrderElementsWithdrawal(DataProvider dataProvider, OpOperator loggedOperator, List<OpDepositoryElement> elementsToWithdraw)
        {
            List<OpDepositoryElement> elementsToOpen = dataProvider.GetDepositoryElementFilter(IdDepositoryElement: elementsToWithdraw.Select(d => d.IdReference.Value).ToArray());
            List<OpPackage> fitterOrderPackages = new List<OpPackage>();
            foreach (OpDepositoryElement deItem in elementsToWithdraw)
            {
                OpDepositoryElement elementToOpen = elementsToOpen.Find(d => d.IdDepositoryElement == deItem.IdReference.Value);

                if (!fitterOrderPackages.Exists(d => d.IdPackage == elementToOpen.IdPackage))
                    fitterOrderPackages.Add(elementToOpen.Package);

                elementToOpen.EndDate = null;
                elementToOpen.SerialNbr = null;
                elementToOpen.ExternalSn = null;
                DepositoryComponent.Save(dataProvider, elementToOpen);
                DepositoryComponent.Delete(dataProvider, deItem);
            }
            if (fitterOrderPackages.Count > 0)
            {
                List<OpDepositoryElement> orderPackageElements = dataProvider.GetDepositoryElementFilter(loadNavigationProperties: false, IdPackage: fitterOrderPackages.Select(p => p.IdPackage).ToArray());
                foreach (OpPackage pLoopItem in fitterOrderPackages.Where(p => p.IdPackageStatus.In(new int[] { (int)Enums.PackageStatus.InCompletion, (int)Enums.PackageStatus.OrderPartiallyCompleted, (int)Enums.PackageStatus.OrderCompleted })))
                {
                    int targetStatus = (int)Enums.PackageStatus.OrderPartiallyCompleted;

                    if (!orderPackageElements.Exists(d => d.IdPackage == pLoopItem.IdPackage && d.EndDate.HasValue))
                    {
                        //w zamówieniu nie został ani jeden zrealizowny element
                        List<OpPackageStatusHistory> packageStatusHistory = dataProvider.GetPackageStatusHistoryFilter(IdPackage: new int[] { pLoopItem.IdPackage });
                        packageStatusHistory = packageStatusHistory.OrderByDescending(p => p.StartDate).ToList();
                        foreach (OpPackageStatusHistory pshItem in packageStatusHistory)
                        {
                            if (pshItem.IdPackageStatus.In(new int[] { (int)Enums.PackageStatus.ToCompletion, (int)Enums.PackageStatus.InCompletion }))
                            {
                                targetStatus = pshItem.IdPackageStatus;
                                break;
                            }
                        }
                    }
                    else
                    {
                        if (orderPackageElements.Exists(d => d.IdPackage == pLoopItem.IdPackage && !d.EndDate.HasValue))
                        {
                            //w paczce istnieją elementy, które nie są zrealizowane
                            targetStatus = (int)Enums.PackageStatus.OrderPartiallyCompleted;
                        }
                        else
                        {
                            targetStatus = (int)Enums.PackageStatus.OrderCompleted;
                        }
                    }

                    if (targetStatus != pLoopItem.IdPackageStatus)
                    {
                        pLoopItem.IdPackageStatus = targetStatus;
                        if (targetStatus != (int)Enums.PackageStatus.OrderCompleted)
                            pLoopItem.RealizationDate = null;
                        else
                        {
                            if (!pLoopItem.RealizationDate.HasValue)
                                pLoopItem.RealizationDate = dataProvider.DateTimeNow;
                        }

                        PackageComponent.Save(dataProvider, loggedOperator, pLoopItem);
                    }
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #region Delete functions
        /// <summary>
        /// Deletes Depository Element from table
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="objectToDelete"></param>
        public static void Delete(DataProvider dataProvider, OpDepositoryElement objectToDelete)
        {
            List<OpDepositoryElementData> dedItemToDelete = dataProvider.GetDepositoryElementDataFilter(IdDepositoryElement: new int[] { objectToDelete.IdDepositoryElement }, loadNavigationProperties: false);
            if (dedItemToDelete != null && dedItemToDelete.Count > 0)
            {
                foreach (OpDepositoryElementData dedItem in dedItemToDelete)
                {
                    dataProvider.DeleteDepositoryElementData(dedItem);
                }
            }

            dataProvider.DeleteDepositoryElement(objectToDelete);
        }

        public static void DeletePackageElements(DataProvider dataProvider, OpPackage package)
        {
            foreach (OpDepositoryElement depositoryElement in dataProvider.GetDepositoryElementFilter(IdPackage: new int[] { package.IdPackage }))//.Where(de => de.IdPackage == package.IdPackage).ToList())
            {
                Delete(dataProvider, depositoryElement);
            }
        }

        public static void DeletePackageElementsData(DataProvider dataProvider, OpPackage package)
        {
            List<OpDepositoryElement> deList = dataProvider.GetDepositoryElementFilter(IdPackage: new int[]{ package.IdPackage });
            foreach (OpDepositoryElement depositoryElement in deList)
            {
                foreach (OpDepositoryElementData dedItem in depositoryElement.DataList)
                {
                    dataProvider.DeleteDepositoryElementData(dedItem);
                }
            }
        }

        public static void DeleteTaskElements(DataProvider dataProvider, OpTask task)
        {
            foreach (OpDepositoryElement depositoryElement in dataProvider.GetDepositoryElementFilter(IdTask: new int[] { task.IdTask }))//Where(de => de.IdTask == task.IdTask).ToList())
            {
                Delete(dataProvider, depositoryElement);
            }
        }

        public static void DeleteLocationElements(DataProvider dataProvider, OpLocation location)
        {
            foreach (OpDepositoryElement depositoryElement in dataProvider.GetDepositoryElementFilter(IdLocation: new long[] { location.IdLocation }))//Where(de => de.IdLocation == location.IdLocation).ToList())
            {
                Delete(dataProvider, depositoryElement);
            }
        }
        #endregion

        #endregion

        #region GroupDepositoryElements

        public static List<OpDepositoryElement> GroupDepositoryElements(List<OpDepositoryElement> all)
        {
            //List<OpDepositoryElement> dep_elem = new List<OpDepositoryElement>();

            //IEnumerable<IGrouping<OpArticle, OpDepositoryElement>> grouping = all.GroupBy(c => c.Article);

            //foreach (IGrouping<OpArticle, OpDepositoryElement> group in grouping)
            //{
            //    if (group.Key.IsUnique)
            //        foreach (OpDepositoryElement dep in group)
            //        {
            //            dep.Count = 1;
            //            dep_elem.Add(dep);
            //        }
            //    else
            //    {
            //        string notes = "";
            //        int count = 0;
            //        OpDepositoryElement tmp = new OpDepositoryElement();
            //        foreach (OpDepositoryElement dep in group)
            //        {
            //            if (!String.IsNullOrEmpty(dep.Notes))
            //                notes += dep.Notes + ", ";
            //            count++;
            //            tmp = dep;
            //        }
            //        tmp.Count = count;
            //        tmp.Notes = notes;
            //        dep_elem.Add(tmp);
            //    }

            //}
            //return dep_elem;
            return GroupDepositoryElements(all, true, false);
        }
        public static List<OpDepositoryElement> GroupDepositoryElements(List<OpDepositoryElement> all, bool group_removed, bool group_unique)
        {
            List<OpDepositoryElement> dep_elem = new List<OpDepositoryElement>();

            IEnumerable<IGrouping<OpArticle, OpDepositoryElement>> grouping = all.GroupBy(c => c.Article);

            foreach (IGrouping<OpArticle, OpDepositoryElement> group in grouping)
            {
                if (!group_unique && group.Key.IsUnique)
                    foreach (OpDepositoryElement dep in group)
                    {
                        dep.Count = 1;
                        dep_elem.Add(dep);
                    }
                else
                {
                    int count = 0;
                    OpDepositoryElement tmp = new OpDepositoryElement();

                    foreach (OpDepositoryElement dep in group.Where(c => c.Removed == false).ToList())
                    {
                        //if (!String.IsNullOrEmpty(dep.Notes))
                        //    notes += dep.Notes + ", ";
                        count++;
                        tmp = dep;
                    }
                    if (count > 0)
                    {
                        tmp.Count = count;
                        //tmp.Notes = notes;
                        dep_elem.Add(tmp);
                    }

                    if (group_removed)
                    {
                        count = 0;
                        foreach (OpDepositoryElement dep in group.Where(c => c.Removed == true).ToList())
                        {
                            //if (!String.IsNullOrEmpty(dep.Notes))
                            //    notes += dep.Notes + ", ";
                            count++;
                            tmp = dep;
                        }
                        if (count > 0)
                        {
                            tmp.Count = count;
                            //tmp.Notes = notes;
                            dep_elem.Add(tmp);
                        }
                    }
                }

            }
            return dep_elem.OrderBy(c => c.StartDate).ToList();
        }



        #endregion

        #region GetDevicesByBarCodes

        public static List<OpDevice> GetDevicesByBarCodes(DataProvider dataProvider, string[] barCodes)
        {
            List<OpDevice> retValue = new List<OpDevice>();

            List<string> customWhereClauseList = new List<string>();
            string customWhereClauseTmp = "cast([VALUE] as nvarchar) in (";
            int elementsInWhereClauseCnt = 0;
            for (int i = 0; i < barCodes.Count(); i++)
            {
                if ((customWhereClauseTmp + "'" + barCodes[i] + "',").Length > 3999)
                {
                    customWhereClauseTmp = customWhereClauseTmp.Substring(0, customWhereClauseTmp.Length - 1);
                    customWhereClauseTmp += ")";
                    customWhereClauseList.Add(customWhereClauseTmp);
                    customWhereClauseTmp = "cast([VALUE] as nvarchar) in (";
                    elementsInWhereClauseCnt = 0;
                }
                customWhereClauseTmp += "'" + barCodes[i] + "',";
                elementsInWhereClauseCnt++;
            }
            if (elementsInWhereClauseCnt > 0)
            {
                customWhereClauseTmp = customWhereClauseTmp.Substring(0, customWhereClauseTmp.Length - 1);
                customWhereClauseTmp += ")";
                customWhereClauseList.Add(customWhereClauseTmp);
            }

            List<OpData> deviceBarCodeData = new List<OpData>();
            foreach (string sItem in customWhereClauseList)
            {
                deviceBarCodeData.AddRange(dataProvider.GetDataFilter(loadNavigationProperties: false, IdDataType: new long[] { DataType.DEVICE_BARCODE }, customWhereClause: sItem));
            }
            if (deviceBarCodeData.Count > 0)
                retValue = dataProvider.GetDevice(deviceBarCodeData.Select(d => d.SerialNbr.Value).Distinct().ToArray());
            if (retValue.Count > 0)
            {
                Dictionary<long, OpDevice> deviceDictTmp = retValue.ToDictionary(d => d.SerialNbr);
                foreach (OpData dItem in deviceBarCodeData)
                {
                    deviceDictTmp[dItem.SerialNbr.Value].DataList.RemoveAll(d => d.IdDataType == DataType.DEVICE_BARCODE);
                    deviceDictTmp[dItem.SerialNbr.Value].DataList.Add(dItem);
                }
            }
            return retValue;
        }

        #endregion

        #region GetDevicesByFactoryNbrs

        public static List<OpDevice> GetDevicesByFactoryNbrs(DataProvider dataProvider, string[] factoryNbrs)
        {
            List<OpDevice> retValue = new List<OpDevice>();

            List<string> customWhereClauseList = new List<string>();
            string customWhereClauseTmp = String.Format("[ID_DATA_TYPE] = {0} AND cast([VALUE] as nvarchar) in (", DataType.DEVICE_FACTORY_NUMBER);
            int elementsInWhereClauseCnt = 0;
            for (int i = 0; i < factoryNbrs.Count(); i++)
            {
                if ((customWhereClauseTmp + "'" + factoryNbrs[i] + "',").Length > 3999)
                {
                    customWhereClauseTmp = customWhereClauseTmp.Substring(0, customWhereClauseTmp.Length - 1);
                    customWhereClauseTmp += ")";
                    customWhereClauseList.Add(customWhereClauseTmp);
                    customWhereClauseTmp = String.Format("[ID_DATA_TYPE] = {0} AND cast([VALUE] as nvarchar) in (", DataType.DEVICE_FACTORY_NUMBER);
                    elementsInWhereClauseCnt = 0;
                }
                customWhereClauseTmp += "'" + factoryNbrs[i] + "',";
                elementsInWhereClauseCnt++;
            }
            if (elementsInWhereClauseCnt > 0)
            {
                customWhereClauseTmp = customWhereClauseTmp.Substring(0, customWhereClauseTmp.Length - 1);
                customWhereClauseTmp += ")";
                customWhereClauseList.Add(customWhereClauseTmp);
            }

            List<OpData> deviceBarCodeData = new List<OpData>();
            foreach (string sItem in customWhereClauseList)
            {
                deviceBarCodeData.AddRange(dataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 60,
                                                                      customWhereClause: sItem));
            }
            if (deviceBarCodeData.Count > 0)
                retValue = dataProvider.GetDevice(deviceBarCodeData.Select(d => d.SerialNbr.Value).Distinct().ToArray());
            if (retValue.Count > 0)
            {
                Dictionary<long, OpDevice> deviceDictTmp = retValue.ToDictionary(d => d.SerialNbr);
                foreach (OpData dItem in deviceBarCodeData)
                {
                    deviceDictTmp[dItem.SerialNbr.Value].DataList.RemoveAll(d => d.IdDataType == DataType.DEVICE_FACTORY_NUMBER);
                    deviceDictTmp[dItem.SerialNbr.Value].DataList.Add(dItem);
                }
            }
            return retValue;
        }

        #endregion

        #region ValidateDevice


        public static int ValidateDevice(
            DataProvider dataProvider, 
            List<Tuple<string, OpArticle>> SnList, 
            bool IsAIUT, 
            DBCommonRDT RadioDeviceTesterNewConnection, 
            string DeviceOrderNumber,
            List<string> DevicesList,
            out string Msg,
            out List<OpDevice> Device,
            out Dictionary<string, int> FailedList,
            out Dictionary<string, string> AdditionalErrorData,
            bool checkRepeatability = true,
            bool onlyFirstError = true,
            bool checkExistingOnShippingList = false,
            bool checkExistingOnOpenShippingList = false,
            bool checkExistingOnUSOK = true,
            bool checkDeviceState = true,
            bool addStrangeDevice = false,
            bool checkExistingOnDepositoryLocation = false,
            bool checkIfAssignedToTask = false,
            bool checkSerialNbrLenght = true,
            bool checkIfTestedOnUSOK = true,
            bool checkIfAllDeviceOrderNumbersAreTheSame = true,
            bool checkExistingInPackage = false,
            bool checkExistingOnDepositoryLocationExceptServicePool = false,
            bool checkProductionValidation = false,
            bool checkArticle = true,
            bool checkDeviceExistingInServiceList = false,
            bool checkDeviceServiced = false,
            int statusValidateOption = 0,
            object statusValidateParam = null,
            bool forceUseIsAiutFlag = false)
        {
            //-1 - Errors (if onlyFirstError = false)
            //0 - NoErrors
            //1 - FieldRequired
            //2 - CorrectSerialNumber
            //3 - NotAllDevicesAreTheSameType
            //4 - UnexpectedErrorOccurs
            //5 - DeviceDoesntExistsOnUsok
            //6 - WrongDeviceState
            //7 - DeviceDoesntExistsOnRadioDeviceTester
            //8 - ElementExiststOnList
            //9 - ArticleNotUnique
            //10 - AIUTDevice
            //11 - ErrorDuringDeviceImport
            //12 - ImportFailed
            //13 - InvalidSN
            //14 - DeviceNotFound
            //15 - DeviceIsNotTestedCorrectly
            //16 - NoShippingList
            //17 - ExistsOnDepositoryLocation
            //18 - WrongArticle
            //19 - AlreadyAssignedToTask
            //20 - DeviceExistingInPackage
            //21 - WrongProductionState
            //22 - DeviceExistingInServiceList
            //23 - DeviceServiced
            //24 - CannotUseDeviceWithThisDistributorInThisContext
            //25 - RadioDeviceTesterNewConnectionRequired
            //26 - OpenShippingList

            //przy listach wyslkowych nie da sie rozroznic tylko po artykule czy urzadzenie nasze czy nie
            //przy listach serwisowych też nie ma takiego rozróżnienia
            bool unnecessaryArticle = statusValidateOption == 4 || SnList.Exists(s => String.Equals(s.Item2.Name.ToUpper(), "UNNECESSARY") || String.Equals(s.Item2.Name.ToUpper(), "NIEPOTRZEBNY"));

            Msg = "";
            Device = new List<OpDevice>();
            FailedList = new Dictionary<string, int>();
            AdditionalErrorData = new Dictionary<string, string>();
            List<long> servicePoolsIds = new List<long>();
            if (checkExistingOnDepositoryLocationExceptServicePool)
            {
                List<OpDistributorData> distributorServicePools = dataProvider.GetDistributorDataFilter(IdDataType: new long[] { DataType.DEPOSITORY_ID });
                if (distributorServicePools != null && distributorServicePools.Count > 0)
                    servicePoolsIds.AddRange(distributorServicePools.Where(d => d.Value != null && !String.IsNullOrEmpty(d.Value.ToString()))
                                                                    .Select(d => Convert.ToInt64(d.Value))
                                                                    .ToArray());
            }

            bool isAiut = false;
            if (unnecessaryArticle)
                isAiut = IsAIUT;
            else
                isAiut = SnList.Exists(s => s.Item2.IsUnique);

            if (/*IsAIUT*/isAiut && (SnList == null || SnList.Count == 0))
            {
                return 1;//FieldRequired
            }
            SnList = SnList.Distinct(d => d.Item1).ToList();

            List<OpDevice> validationDeviceList = new List<OpDevice>();
            List<OpDepositoryElement> validationDepositoryElement = new List<OpDepositoryElement>();
            List<OpShippingListDevice> validationShpippingListDevice = new List<OpShippingListDevice>();
            List<OpDeviceOrderNumber> validationDeviceOrderNumbers = new List<OpDeviceOrderNumber>();

            #region DataInitialization

            //przy listach wyslkowych nie da sie rozroznic tylko po artykule czy urzadzenie nasze czy nie
            if (unnecessaryArticle)
                isAiut = IsAIUT;
            else
                isAiut = SnList.Exists(s => s.Item2.IsAiut && s.Item2.IsUnique);

            if (SnList.Exists(s => s.Item2 != null && !s.Item2.IsAiut && s.Item2.IsUnique))
                validationDeviceOrderNumbers = dataProvider.GetAllDeviceOrderNumber();

            if (/*IsAIUT*/isAiut)
            {
                long snTmp = 0;
                List<long> validationSnListTmp = new List<long>();
                List<string> barCodes = new List<string>();
                List<OpData> barCodesData = new List<OpData>();

                //jeśli mamy do czynienia z artykułem unnecessary to wszystkie urządzenia są identyfikowane po numerze seryjnym
                List<Tuple<string, OpArticle>> snToAnalyze = new List<Tuple<string, OpArticle>>();
                if (unnecessaryArticle)
                    snToAnalyze.AddRange(SnList);
                else
                    snToAnalyze.AddRange(SnList.Where(d => d.Item2.IsAiut && d.Item2.IsUnique));

                foreach (Tuple<string, OpArticle> snItem in snToAnalyze)
                {
                    if (snItem.Item1.Trim().Length == 22)
                        barCodes.Add(snItem.Item1.Trim());
                    else
                    {
                        if (!String.IsNullOrEmpty(snItem.Item1) && long.TryParse(snItem.Item1, out snTmp))
                            validationSnListTmp.Add(snTmp);
                    }
                }
                if (barCodes.Count > 0)
                {
                    #region BarCodes

                    List<string> customWhereClauseList = new List<string>();
                    string customWhereClauseTmp = "cast([VALUE] as nvarchar) in (";
                    int elementsInWhereClauseCnt = 0;
                    for(int i = 0; i < barCodes.Count; i++)
                    {
                        if ((customWhereClauseTmp + "'" + barCodes[i] + "',").Length > 3999)
                        {
                            customWhereClauseTmp = customWhereClauseTmp.Substring(0, customWhereClauseTmp.Length - 1);
                            customWhereClauseTmp += ")";
                            customWhereClauseList.Add(customWhereClauseTmp);
                            customWhereClauseTmp = "cast([VALUE] as nvarchar) in (";
                            elementsInWhereClauseCnt = 0;
                        }
                        customWhereClauseTmp += "'" + barCodes[i] + "',";
                        elementsInWhereClauseCnt++;
                    }
                    if (elementsInWhereClauseCnt > 0)
                    {
                        customWhereClauseTmp = customWhereClauseTmp.Substring(0, customWhereClauseTmp.Length - 1);
                        customWhereClauseTmp += ")";
                        customWhereClauseList.Add(customWhereClauseTmp);
                    }

                    foreach(string sItem in customWhereClauseList)
                    {
                        barCodesData.AddRange(dataProvider.GetDataFilter(loadNavigationProperties: false, IdDataType: new long[] { DataType.DEVICE_BARCODE }, customWhereClause: sItem));
                    }

                    validationSnListTmp.AddRange(barCodesData.Select(d => d.SerialNbr.Value));
                    validationSnListTmp = validationSnListTmp.Distinct().ToList();

                    #endregion
                }
                if (validationSnListTmp.Count > 0)
                {
                    validationDeviceList = dataProvider.GetDevice(validationSnListTmp.ToArray());
                    if (barCodesData.Count > 0)
                    {
                        #region LoadBarCodesData
                        Dictionary<long, OpDevice> deviceDictTmp = validationDeviceList.ToDictionary(d => d.SerialNbr);
                        foreach (OpData dItem in barCodesData)
                        {
                            deviceDictTmp[dItem.SerialNbr.Value].DataList.RemoveAll(d => d.IdData == dItem.IdData);
                            deviceDictTmp[dItem.SerialNbr.Value].DataList.Add(dItem);
                        }
                        #endregion
                    }
                    if (checkExistingInPackage || checkExistingOnDepositoryLocation || checkIfAssignedToTask)
                    {
                        string customWhereClause = "";
                        if (checkExistingInPackage)
                            customWhereClause = "[ID_PACKAGE] is not null";
                        if (checkExistingOnDepositoryLocation || checkIfAssignedToTask)
                            customWhereClause += (customWhereClause.Length == 0 ? "" : " OR ") + "([END_DATE] is null and [ID_LOCATION] is not null)";
                        if (customWhereClause.Length > 0)
                        {
                            customWhereClause = "(" + customWhereClause + ")";
                            validationDepositoryElement = dataProvider.GetDepositoryElementFilter(SerialNbr: validationSnListTmp.ToArray(),
                                customWhereClause: customWhereClause);
                            if (checkIfAssignedToTask && validationDepositoryElement.Exists(d => d.IdLocation.HasValue && d.IdReference.HasValue))
                            {
                                validationDepositoryElement.AddRange(dataProvider.GetDepositoryElement(validationDepositoryElement.Where(d => d.IdLocation.HasValue && d.IdReference.HasValue)
                                                                                                                                  .Select(d => d.IdReference.Value)
                                                                                                                                  .ToArray()));
                            }
                        }
                    }
                    if (checkExistingOnShippingList || checkExistingOnOpenShippingList)
                    {
                        validationShpippingListDevice = dataProvider.GetShippingListDeviceFilter(SerialNbr: validationSnListTmp.ToArray());
                    }
                }
            }
            #endregion


            foreach (Tuple<string, OpArticle> snItem in SnList)
            {
                //przy listach wyslkowych nie da sie rozroznic tylko po artykule czy urzadzenie nasze czy nie
                //przy listach serwisowych też nie ma takiego rozróżnienia
                if (statusValidateOption == 4 || String.Equals(snItem.Item2.Name.ToUpper(), "UNNECESSARY") || String.Equals(snItem.Item2.Name.ToUpper(), "NIEPOTRZEBNY"))
                    isAiut = IsAIUT;
                else
                    isAiut = snItem.Item2.IsAiut;
                if (forceUseIsAiutFlag)
                    isAiut = IsAIUT;

                if (isAiut/*IsAIUT*/)
                {
                    OpDevice deviceCheck = null;
                    #region AIUTDevice
                    #region checkEmptyEntries - ABAT
                    if (String.IsNullOrEmpty(snItem.Item1.Trim()))
                    {
                        if (snItem.Item2 != null && snItem.Item2.Name.ToUpper().Contains("ABAT")) //(Article.IdArticle == 151 || Article.IdArticle == 152))
                        {
                            //moga wystapic ABAT'y bez numerow seryjnych
                            continue;
                        }
                        if (snItem.Item2.IsUnique)
                        {
                            if (onlyFirstError)
                                return 1;//FieldRequired
                            else
                            {
                                FailedList.Add(snItem.Item1, 1);
                                continue;
                            }
                        }
                        else
                            continue;//tak jak te nasze ABATY stare
                    }
                    #endregion
                    #region checkSerialNbrLenght
                    if (checkSerialNbrLenght)
                    {
                        if (snItem.Item1.Trim().Length != 8
                            && snItem.Item1.Trim().Length != 16
                            && snItem.Item1.Trim().Length != 22/*BARCODE*/)
                        {
                            Msg = " (" + snItem.Item1 + ")";
                            if (onlyFirstError)
                                return 2;//CorrectSerialNumber
                            else
                            {
                                FailedList.Add(snItem.Item1, 2);
                                continue;
                            }
                        }
                    }
                    #endregion
                    #region checkIfSnIsLong
                    long? insertedSerialNBR = null;
                    try
                    {
                        if (snItem.Item1.Trim().Length != 22)
                        {
                            insertedSerialNBR = Convert.ToInt64(snItem.Item1.Trim());
                            /*
                             * BARCODE nie musi być tylko numeryczny, ale może być poprawny - weryfikacja w barCodeCheck
                            */
                        }
                        else
                            insertedSerialNBR = -1;
                    }
                    catch (Exception ex)
                    {
                        if (onlyFirstError)
                            return 1;//FieldRequired
                        else
                        {
                            FailedList.Add(snItem.Item1, 1);
                            continue;
                        }
                    }
                    #endregion
                    deviceCheck = validationDeviceList.Find(d => d.SerialNbr == insertedSerialNBR.Value);
                    #region barCodeCheck
                    if (deviceCheck == null && snItem.Item1.Trim().Length == 22)
                    {
                            deviceCheck = validationDeviceList.Find(d => d.DataList.Exists(dl => dl.IdDataType == DataType.DEVICE_BARCODE
                                                                                              && dl.Value != null
                                                                                              && String.Equals(dl.Value.ToString().Trim(), snItem.Item1.Trim())));
                    }
                    #endregion
                    #region factoryNumberCheck
                    if (deviceCheck == null)
                    {
                        //to trzeba zrobic w przyszlosci tak jak BARCODE
                        long? sn = null;
                        List<OpData> dataList = dataProvider.GetDataFilter(
                                    IdDataType: new long[] { IMR.Suite.Common.DataType.DEVICE_FACTORY_NUMBER },
                                    customWhereClause: "[VALUE] = '" + snItem.Item1.Trim() + "' and [SERIAL_NBR] is not null");
                        if (dataList.Count > 0)
                        {
                            sn = dataList.Where(w => w.IdData == dataList.Max(t => t.IdData)).First().SerialNbr;
                            if (sn.HasValue)
                            {
                                deviceCheck = dataProvider.GetDevice(sn.Value, true);
                                if (deviceCheck != null)
                                {
                                    validationShpippingListDevice.AddRange(dataProvider.GetShippingListDeviceFilter(SerialNbr: new long[] { deviceCheck.SerialNbr }));
                                    if (checkExistingInPackage || checkExistingOnDepositoryLocation || checkIfAssignedToTask)
                                    {
                                        string customWhereClause = "";
                                        if (checkExistingInPackage)
                                            customWhereClause = "[ID_PACKAGE] is not null";
                                        if (checkExistingOnDepositoryLocation || checkIfAssignedToTask)
                                            customWhereClause += (customWhereClause.Length == 0 ? "" : " OR ") + "([END_DATE] is null and [ID_LOCATION] is not null)";
                                        if (customWhereClause.Length > 0)
                                        {
                                            customWhereClause = "(" + customWhereClause + ")";
                                            validationDepositoryElement = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[]{ deviceCheck.SerialNbr },
                                                customWhereClause: customWhereClause);
                                            if (checkIfAssignedToTask && validationDepositoryElement.Exists(d => d.IdLocation.HasValue && d.IdReference.HasValue))
                                            {
                                                validationDepositoryElement.AddRange(dataProvider.GetDepositoryElement(validationDepositoryElement.Where(d => d.IdLocation.HasValue && d.IdReference.HasValue)
                                                                                                                                                  .Select(d => d.IdReference.Value)
                                                                                                                                                  .ToArray()));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (deviceCheck == null)
                    {
                        Msg = " (" + snItem.Item1 + ")";
                        if (onlyFirstError)
                            return 2;//CorrectSerialNumber
                        else
                        {
                            FailedList.Add(snItem.Item1, 2);
                            continue;
                        }
                    }
                    #endregion
                    #region checkIfAssignedToTask
                    if (checkIfAssignedToTask)
                    {
                        if (validationDepositoryElement != null && validationDepositoryElement.Count > 0 &&
                            validationDepositoryElement.Exists(d => d.IdTask.HasValue && d.SerialNbr == deviceCheck.SerialNbr))
                        {//z montowanymi nie ma problemu
                            string additinallMsg = "";
                            foreach (OpDepositoryElement deItem in validationDepositoryElement.Where(l => l.IdTask.HasValue &&
                                                                                                          l.SerialNbr == deviceCheck.SerialNbr))
                                additinallMsg += deItem.Location + ", ";
                            AdditionalErrorData.Add(snItem.Item1, "(" + additinallMsg.Substring(0, additinallMsg.Length - 2) + ")");

                            Msg = " (" + snItem.Item1 + ")";
                            if (onlyFirstError)
                                return 19;//AlreadyAssignedToTask
                            else
                            {
                                FailedList.Add(snItem.Item1, 19);
                                continue;
                            }
                        }
                    }
                    #endregion
                    #region ExistingInPackage
                    if (checkExistingInPackage)
                    {
                        // existsOn = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[] { deviceCheck.SerialNbr },
                        //                                         customWhereClause: "[ID_PACKAGE] is not null");
                        if (validationDepositoryElement != null && validationDepositoryElement.Count > 0 &&
                            validationDepositoryElement.Exists(d => d.IdPackage.HasValue && d.SerialNbr == deviceCheck.SerialNbr))
                        {
                            string tmpMsg = "";
                            foreach (OpDepositoryElement deItem in validationDepositoryElement.Where(d => d.IdPackage.HasValue && d.SerialNbr == deviceCheck.SerialNbr))
                            {
                                if (deItem.Package.IdPackageStatus.In(new int[] { (int)Enums.PackageStatus.New, (int)Enums.PackageStatus.InCompletion, (int)Enums.PackageStatus.Sent, (int)Enums.PackageStatus.Wrong }))
                                {
                                    bool packagePresenceError = true;
                                    if ((deItem.Package.IdPackageStatus == (int)Enums.PackageStatus.Wrong || deItem.Package.IdPackageStatus == (int)Enums.PackageStatus.Sent)
                                        && deItem.Accepted == true && deItem.EndDate != null)
                                    {
                                        //skorzystano z czesciowej akceptacji paczki, urzadzenia juz w niej nie ma
                                        packagePresenceError = false;
                                    }
                                    if(packagePresenceError)
                                    {
                                        tmpMsg = deItem.Package.ToString();
                                        break;
                                    }
                                }
                            }
                            if (tmpMsg.Length > 0)
                            {
                                Msg = " (" + snItem.Item1 + ")";
                                AdditionalErrorData.Add(snItem.Item1, "(" + tmpMsg + ")");
                                if (onlyFirstError)
                                    return 20;//DeviceExistingInPackage
                                else
                                {
                                    FailedList.Add(snItem.Item1, 20);
                                    continue;
                                }
                            }
                        }
                    }
                    #endregion
                    #region checkExistingOnDepositoryLocation
                    // List<OpDepositoryElement> existsOn = null;
                    /*if (checkExistingOnDepositoryLocation || checkIfAssignedToTask)
                    {
                        existsOn = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[] { deviceCheck.SerialNbr },
                                                                customWhereClause: "[END_DATE] is null and [ID_LOCATION] is not null");
                    }*/
                    if (checkExistingOnDepositoryLocation)
                    {
                        if (validationDepositoryElement != null && validationDepositoryElement.Count > 0 &&
                            validationDepositoryElement.Exists(d => d.IdLocation.HasValue && 
                                                                    d.SerialNbr == deviceCheck.SerialNbr &&
                                                                    !String.Equals(d.Location.Name, "Urządzenia zdjęte na gwarancji")))
                        {
                            if (!checkExistingOnDepositoryLocationExceptServicePool)
                            {
                                Msg = " (" + snItem.Item1 + ")";
                                string additinallMsg = "";
                                foreach (OpDepositoryElement deItem in validationDepositoryElement.Where(d => d.IdLocation.HasValue &&
                                                                                                         d.SerialNbr == deviceCheck.SerialNbr &&
                                                                                                         !String.Equals(d.Location.Name, "Urządzenia zdjęte na gwarancji")).ToList())
                                    additinallMsg += deItem.Location + ", ";
                                AdditionalErrorData.Add(snItem.Item1, "(" + additinallMsg.Substring(0, additinallMsg.Length - 2) + ")");
                                if (onlyFirstError)
                                    return 17;//ExistsOnDepositoryLocation
                                else
                                {
                                    FailedList.Add(snItem.Item1, 17);
                                    continue;
                                }
                            }
                            else
                            {
                                if (validationDepositoryElement.Exists(l => l.IdLocation.HasValue &&
                                                                            l.SerialNbr == deviceCheck.SerialNbr &&
                                                                            !l.IdLocation.Value.In(servicePoolsIds.ToArray()) &&
                                                                            !String.Equals(l.Location.Name, "Urządzenia zdjęte na gwarancji")))
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    string additinallMsg = "";
                                    foreach (OpDepositoryElement deItem in validationDepositoryElement.Where(l => l.IdLocation.HasValue &&
                                                                                                                  l.SerialNbr == deviceCheck.SerialNbr &&
                                                                                                                  !l.IdLocation.Value.In(servicePoolsIds.ToArray()) &&
                                                                                                                  !String.Equals(l.Location.Name, "Urządzenia zdjęte na gwarancji")))
                                        additinallMsg += deItem.Location + ", ";
                                    AdditionalErrorData.Add(snItem.Item1, "(" + additinallMsg.Substring(0, additinallMsg.Length - 2) + ")");
                                    if (onlyFirstError)
                                        return 17;//ExistsOnDepositoryLocation
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 17);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region checkIfAllDeviceOrderNumbersAreTheSame
                    if (checkIfAllDeviceOrderNumbersAreTheSame)
                    {
                        if (!String.IsNullOrEmpty(DeviceOrderNumber))
                        {
                            if (!String.Equals(DeviceOrderNumber, deviceCheck.DeviceOrderNumber.ToString()))
                            {
                                Msg = " (" + deviceCheck.SerialNbr + " - " + deviceCheck.DeviceOrderNumber.ToString() + ")";
                                if (onlyFirstError)
                                    return 3;//NotAllDevicesAreTheSameType
                                else
                                {
                                    FailedList.Add(snItem.Item1, 3);
                                    continue;
                                }
                            }
                        }
                    }
                    #endregion
                    #region checkDeviceState
                    try
                    {
                        string deviceSnToCheck = snItem.Item1.Trim();
                        if (snItem.Item1.Trim().Length == 22)//BARCODE
                        {
                            deviceSnToCheck = deviceCheck.SerialNbr.ToString();
                            while (deviceSnToCheck.Length < 8)
                            {
                                deviceSnToCheck = "0" + deviceSnToCheck;
                            }
                        }

                        int outputCode = dataProvider.CheckDeviceState(deviceSnToCheck, statusValidateOption, statusValidateParam);
                        #region CheckDeviceState
                        switch (outputCode)
                        {
                            case -1:
                                Msg = " (" + snItem.Item1 + ")";
                                if (onlyFirstError)
                                    return 4;//UnexpectedErrorOccurs
                                else
                                {
                                    FailedList.Add(snItem.Item1, 4);
                                    continue;
                                }
                                break;
                            case 2:
                                if (checkExistingOnUSOK)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 5;//DeviceDoesntExistsOnUsok
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 5);
                                        continue;
                                    }
                                }
                                break;
                            case 3:
                                if (checkDeviceState)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 6;//WrongDeviceState
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 6);
                                        continue;
                                    }
                                }
                                break;
                            case 4:
                                if (deviceCheck.IdDeviceOrderNumber != 2852//CPG
                                    && deviceCheck.IdDeviceOrderNumber != 2853)//MacR
                                /*
                                    * Dwa szczególne typy urządzeń które nie są AIUT ale traktowane jako AIUT 
                                    * - nie pojawią się więc w bazie RadioDeviceTester
                                */
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 7;//DeviceDoesntExistsOnRadioDeviceTester
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 7);
                                        continue;
                                    }
                                }
                                break;
                            case 5:
                                if (checkIfTestedOnUSOK)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 15;//DeviceIsNotTestedCorrectly
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 15);
                                        continue;
                                    }
                                }
                                break;
                            case 6:
                                if (checkDeviceExistingInServiceList)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 22;//DeviceExistingInServiceList
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 22);
                                        continue;
                                    }
                                }
                                break;
                            case 7:
                                if (checkDeviceServiced)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 23;//DeviceServiced
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 23);
                                        continue;
                                    }
                                }
                                break;
                            case 8:
                                Msg = " (" + snItem.Item1 + ")";
                                if (onlyFirstError)
                                    return 24;//CannotUseDeviceWithThisDistributorInThisContext
                                else
                                {
                                    FailedList.Add(snItem.Item1, 24);
                                    continue;
                                }
                                break;
                            case 9:
                                Msg = " (" + snItem.Item1 + ")";
                                if (onlyFirstError)
                                    return 24;//CannotUseDeviceWithThisDistributorInThisContext
                                else
                                {
                                    FailedList.Add(snItem.Item1, 24);
                                    continue;
                                }
                                break;
                            default:
                                break;
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Msg = " (" + snItem.Item1 + ")";
                        if (onlyFirstError)
                            return 4;//UnexpectedErrorOccurs
                        else
                        {
                            FailedList.Add(snItem.Item1, 4);
                            continue;
                        }
                    }
                    
                    #endregion
                    #region checkRepeatability
                    if (DevicesList != null && checkRepeatability)
                    {
                        //string insertedSerialNBRTmp = insertedSerialNBR.HasValue ? insertedSerialNBR.ToString() : null;
                        foreach (string item in DevicesList)
                        {
                            long snTmp = 0;
                            if (long.TryParse(item, out snTmp))
                            {
                                if (snTmp == deviceCheck.SerialNbr)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 8;//ElementExiststOnList
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 8);
                                        continue;
                                    }
                                }
                            }
                            else
                            {
                                if (String.Equals(item, deviceCheck.SerialNbr.ToString()))
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 8;//ElementExiststOnList
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 8);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region checkArticle
                    if (checkArticle)
                    {
                        if (snItem.Item2 == null)
                        {
                            if (onlyFirstError)
                                return 18;//WrongArticle
                            else
                            {
                                FailedList.Add(snItem.Item1, 18);
                                continue;
                            }
                        }

                        if (snItem.Item2 != null && !snItem.Item2.IsUnique)
                        {
                            if (onlyFirstError)
                                return 9;//ArticleNotUnique
                            else
                            {
                                FailedList.Add(snItem.Item1, 9);
                                continue;
                            }
                        }

                        if (snItem.Item2 != null)
                        {
                            if (!snItem.Item2.Name.ToUpper().Contains(deviceCheck.DeviceOrderNumber.Name.ToUpper()) && deviceCheck.IdDeviceOrderNumber != DeviceOrderNumberNotProducedByAiut)
                            {
                                if (onlyFirstError)
                                    return 18;//WrongArticle
                                else
                                {
                                    FailedList.Add(snItem.Item1, 18);
                                    continue;
                                }
                            }
                            else
                            {
                                if (!snItem.Item2.IsAiut)// dataProvider.GetArticleFilter(IsAiut: true, customWhereClause: "[NAME] like '" + Article.Name + "'").FirstOrDefault() == null)
                                {
                                    if (onlyFirstError)
                                        return 18;//WrongArticle
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 18);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region checkProductionValidation
                    if (checkProductionValidation)
                    {
                        int errorCode;
                        string errorMsg;

                        if (RadioDeviceTesterNewConnection == null)
                        {
                            if (onlyFirstError)
                            {
                                AdditionalErrorData.Add(snItem.Item1, "");
                                return 25;//WrongProductionState
                            }
                            else
                            {
                                FailedList.Add(snItem.Item1, 25);
                                AdditionalErrorData.Add(snItem.Item1, "");
                                continue;
                            }
                        }

                        string deviceSnToCheck = snItem.Item1.Trim();
                        if (snItem.Item1.Trim().Length == 22)//BARCODE
                        {
                            deviceSnToCheck = deviceCheck.SerialNbr.ToString();
                            if (!String.IsNullOrEmpty(deviceCheck.FactoryNumber.Trim()) || deviceCheck.FactoryNumber.Trim().Length != 16)
                            {
                                while (deviceSnToCheck.Length < 8)
                                {
                                    deviceSnToCheck = "0" + deviceSnToCheck;
                                }
                            }
                            else
                                deviceSnToCheck = deviceCheck.FactoryNumber.Trim();
                        }

                        if (!RadioDeviceTesterNewConnection.ValidateDevice(deviceSnToCheck, out errorCode, out errorMsg))
                        {
                            if (onlyFirstError)
                            {
                                AdditionalErrorData.Add(snItem.Item1, "(" + errorMsg + ")");
                                return 21;//WrongProductionState
                            }
                            else
                            {
                                FailedList.Add(snItem.Item1, 21);
                                AdditionalErrorData.Add(snItem.Item1, "(" + errorMsg + ")");
                                continue;
                            }
                        }
                    }
                    #endregion
                    #region checkExistingOnShippingList
                    if (checkExistingOnShippingList)
                    {
                        bool hasShippingList = false;

                        List<OpShippingListDevice> slDevice = validationShpippingListDevice.Where(d => d.SerialNbr == deviceCheck.SerialNbr).ToList();// dataProvider.GetShippingListDeviceFilter(SerialNbr: new long[] { Convert.ToInt64(snItem.Trim()) });
                        if (slDevice != null && slDevice.Count > 0)
                            hasShippingList = true;

                        if (!hasShippingList)
                        {
                            if (onlyFirstError)
                                return 16;//NoShippingList
                            else
                            {
                                FailedList.Add(snItem.Item1, 16);
                                continue;
                            }
                        }
                    }
                    #endregion
                    #region checkExistingOnOpenShippingList
                    if (checkExistingOnOpenShippingList)
                    {
                        bool hasOpenShippingList = false;

                        List<OpShippingListDevice> slDevice = validationShpippingListDevice.Where(d => d.SerialNbr == deviceCheck.SerialNbr && !d.ShippingList.FinishDate.HasValue).ToList();// dataProvider.GetShippingListDeviceFilter(SerialNbr: new long[] { Convert.ToInt64(snItem.Trim()) });
                        if (slDevice != null && slDevice.Count > 0)
                            hasOpenShippingList = true;

                        if (hasOpenShippingList)
                        {
                            AdditionalErrorData.Add(snItem.Item1, "(" + slDevice[0].IdShippingList + ")");
                            if (onlyFirstError)
                                return 26;//OpenShippingList
                            else
                            {
                                FailedList.Add(snItem.Item1, 26);
                                continue;
                            }
                        }
                    }
                    #endregion
                    Device.Add(deviceCheck);
                    #endregion
                }
                else
                {
                    #region NotAIUTDevice
                    #region Article
                    if (checkArticle)
                    {
                        if (snItem.Item2 != null && snItem.Item2.IsUnique && String.IsNullOrEmpty(snItem.Item1))
                        {
                            if (onlyFirstError)
                                return 1;//FieldRequired
                            else
                            {
                                FailedList.Add(snItem.Item1, 1);
                                continue;
                            }
                        }
                        if (snItem.Item2 != null && !snItem.Item2.IsUnique && !String.IsNullOrEmpty(snItem.Item1))
                        {
                            if (onlyFirstError)
                                return 9;//ArticleNotUnique
                            else
                            {
                                FailedList.Add(snItem.Item1, 9);
                                continue;
                            }
                        }

                        if (snItem.Item2 != null)
                        {
                            if (snItem.Item2.IsAiut)//dataProvider.GetArticleFilter(IsAiut: false, customWhereClause: "[NAME] like '" + Article.Name + "'").FirstOrDefault() == null)
                            {
                                if (onlyFirstError)
                                    return 18;//WrongArticle
                                else
                                {
                                    FailedList.Add(snItem.Item1, 18);
                                    continue;
                                }
                            }
                        }
                    }
                    #endregion
                    if (!String.IsNullOrEmpty(snItem.Item1))
                    {
                        #region checkRepeatability
                        if (DevicesList != null && checkRepeatability)
                        {
                            bool repeatabilityError = false;
                            //string insertedSerialNBRTmp = insertedSerialNBR.HasValue ? insertedSerialNBR.ToString() : null;
                            foreach (string item in DevicesList)
                            {
                                if (String.Equals(item.Trim(), snItem.Item1.Trim()))
                                {
                                    repeatabilityError = true;
                                    break;
                                }
                            }
                            if (repeatabilityError)
                            {
                                Msg = " (" + snItem.Item1 + ")";
                                if (onlyFirstError)
                                    return 8;//ElementExiststOnList
                                else
                                {
                                    FailedList.Add(snItem.Item1, 8);
                                    continue;
                                }
                            }
                        }
                        #endregion
                        #region CheckIfDeviceIsNotAIUT
                        
                        bool checkIfDeviceIsNotAIUT = ArticleComponent.CheckOrdernumberExistanceInArticleName(snItem.Item2, validationDeviceOrderNumbers);
                        
                        if (checkIfDeviceIsNotAIUT)
                        {
                            long? sn = null;
                            try
                            {
                                sn = Convert.ToInt64(snItem.Item1.Trim());
                            }
                            catch (Exception ex)
                            {
                                sn = null;
                            }
                            if (sn != null)
                            {
                                OpDevice device = null;
                                if (sn.HasValue)
                                    device = dataProvider.GetDevice(sn.Value, true);
                                if (device != null)
                                {
                                    if (device.SerialNbr.ToString().Length < 8)
                                    {
                                        for (int i = 0; i < 8 - device.SerialNbr.ToString().Length; i++)
                                        {
                                        }
                                    }
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return 10;//AIUTDevice
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 10);
                                        continue;
                                    }
                                }
                                else
                                {
                                    List<OpData> dataList = dataProvider.GetDataFilter(
                                                    IdDataType: new long[] { IMR.Suite.Common.DataType.DEVICE_FACTORY_NUMBER },
                                                    customWhereClause: "[VALUE] = '" + snItem.Item1.Trim() + "' and [SERIAL_NBR] is not null");
                                    if (dataList.Count > 0)
                                    {
                                        sn = dataList.Where(w => w.IdData == dataList.Max(t => t.IdData)).First().SerialNbr;
                                        if (sn.HasValue)
                                        {
                                            device = dataProvider.GetDevice(sn.Value, true);
                                        }
                                    }
                                    if (device != null)
                                    {
                                        Msg = " (" + snItem.Item1 + ")";
                                        if (onlyFirstError)
                                            return 10;//AIUTDevice
                                        else
                                        {
                                            FailedList.Add(snItem.Item1, 10);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Existing InLocation || Assigned to task
                        List<OpDepositoryElement> existsOn = null;
                        if (checkExistingOnDepositoryLocation || checkIfAssignedToTask)
                        {
                            existsOn = dataProvider.GetDepositoryElementFilter(//ExternalSn: snItem.Item1,
                                                                    customWhereClause: "[EXTERNAL_SN] like '" + snItem.Item1 + "' AND [END_DATE] is null and [ID_LOCATION] is not null");
                        }
                        if (checkExistingOnDepositoryLocation)
                        {

                            if (existsOn != null && existsOn.Count > 0)
                            {
                                if (!checkExistingOnDepositoryLocationExceptServicePool)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    string additinallMsg = "";
                                    foreach (OpDepositoryElement deItem in existsOn)
                                        additinallMsg += deItem.Location + ", ";
                                    AdditionalErrorData.Add(snItem.Item1, "(" + additinallMsg.Substring(0, additinallMsg.Length - 2) + ")");
                                    if (onlyFirstError)
                                        return 17;//ExistsOnDepositoryLocation
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 17);
                                        continue;
                                    }
                                }
                                else
                                {
                                    if (existsOn.Exists(l => !l.IdLocation.Value.In(servicePoolsIds.ToArray())))
                                    {
                                        Msg = " (" + snItem.Item1 + ")";
                                        string additinallMsg = "";
                                        foreach (OpDepositoryElement deItem in existsOn.Where(l => !l.IdLocation.Value.In(servicePoolsIds.ToArray())))
                                            additinallMsg += deItem.Location + ", ";
                                        AdditionalErrorData.Add(snItem.Item1, "(" + additinallMsg.Substring(0, additinallMsg.Length - 2) + ")");
                                        if (onlyFirstError)
                                            return 17;//ExistsOnDepositoryLocation
                                        else
                                        {
                                            FailedList.Add(snItem.Item1, 17);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        if (checkIfAssignedToTask)
                        {
                            if (existsOn != null && existsOn.Count > 0)
                            {//z montowanymi nie ma problemu
                                if (existsOn[0].Removed && existsOn[0].IdReference.HasValue)
                                {
                                    OpDepositoryElement possiblyTask = dataProvider.GetDepositoryElement(existsOn[0].IdReference.Value, true);
                                    if (possiblyTask != null && possiblyTask.IdTask.HasValue)
                                    {
                                        Msg = " (" + snItem.Item1 + ")";
                                        if (onlyFirstError)
                                            return 19;//AlreadyAssignedToTask
                                        else
                                        {
                                            FailedList.Add(snItem.Item1, 19);
                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #region Existing InPackage
                        if (checkExistingInPackage && !String.IsNullOrEmpty(snItem.Item1))
                        {
                            existsOn = dataProvider.GetDepositoryElementFilter(//ExternalSn: snItem.Item1,
                                                                    customWhereClause: "[EXTERNAL_SN] like '" + snItem.Item1 + "' AND [ID_PACKAGE] is not null");
                            if (existsOn != null && existsOn.Count > 0 &&
                                existsOn.Exists(d => d.IdPackage.HasValue && String.Equals(d.ExternalSn.Trim(), snItem.Item1.Trim())))
                            {
                                string tmpMsg = "";
                                foreach (OpDepositoryElement deItem in existsOn.Where(d => d.IdPackage.HasValue && String.Equals(d.ExternalSn.Trim(), snItem.Item1.Trim())))
                                {
                                    if (deItem.Package.IdPackageStatus.In(new int[] { (int)Enums.PackageStatus.New, (int)Enums.PackageStatus.InCompletion, (int)Enums.PackageStatus.Sent, (int)Enums.PackageStatus.Wrong }))
                                    {
                                        bool packagePresenceError = true;
                                        if ((deItem.Package.IdPackageStatus == (int)Enums.PackageStatus.Wrong || deItem.Package.IdPackageStatus == (int)Enums.PackageStatus.Sent)
                                            && deItem.Accepted == true && deItem.EndDate != null)
                                        {
                                            //skorzystano z czesciowej akceptacji paczki, urzadzenia juz w niej nie ma
                                            packagePresenceError = false;
                                        }
                                        if (packagePresenceError)
                                        {
                                            tmpMsg = deItem.Package.ToString();
                                            break;
                                        }
                                    }
                                }
                                if (tmpMsg.Length > 0)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    AdditionalErrorData.Add(snItem.Item1, "(" + tmpMsg + ")");
                                    if (onlyFirstError)
                                        return 20;//DeviceExistingInPackage
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, 20);
                                        continue;
                                    }
                                }
                            }
                        }

                        #endregion
                        
                        if (addStrangeDevice)
                        {
                            /* Mamy do czynienia z jakimś dziwnym zewnętrznym urządzeniem, które musi wejść na listę wsyłkową/serwisową 
                             * i trzeba mu nadać nuemr seryjny, takie numery będą się zaczynać od cyfr 90, nazwy będą umieszczane w FACTORY_NUMBER  
                             */
                            OpData checkIfExists = dataProvider.GetDataFilter(IdDataType: new long[] { DataType.DEVICE_FACTORY_NUMBER }, customWhereClause: "[VALUE] = '" + snItem.Item1 + "'").FirstOrDefault();
                            bool isStrangeDevice = false;
                            if (checkIfExists != null && checkIfExists.SerialNbr.ToString().Length == 8 && checkIfExists.SerialNbr.ToString().StartsWith("90"))
                                isStrangeDevice = true;
                            if (checkIfExists == null || (checkIfExists != null && !isStrangeDevice))
                            {
                                OpDevice device = dataProvider.GetDeviceFilter(customWhereClause: "SERIAL_NBR = (SELECT MAX(SERIAL_NBR) FROM DEVICE WHERE LEN(SERIAL_NBR) = 8 AND cast(SERIAL_NBR as nvarchar) like '90%')").FirstOrDefault();
                                long nextSerialNbr = -1;
                                if (device == null)
                                    nextSerialNbr = 90000000;
                                else
                                    nextSerialNbr = device.SerialNbr + 1;
                                device = new OpDevice();
                                device.SerialNbr = nextSerialNbr;
                                device.DeviceType = dataProvider.GetDeviceTypeFilter(Name: "Unknown").FirstOrDefault();
                                device.DeviceOrderNumber = dataProvider.GetDeviceOrderNumberFilter(Name: "NOT_PRODUCED_BY_AIUT").FirstOrDefault();
                                device.IdDistributor = 1;
                                device.DeviceStateType = dataProvider.GetDeviceStateType(2);
                                OpData deviceData = new OpData();
                                deviceData.SerialNbr = nextSerialNbr;
                                deviceData.IdDataType = DataType.DEVICE_FACTORY_NUMBER;
                                deviceData.Index = 0;
                                deviceData.OpState = OpChangeState.New;
                                deviceData.Value = snItem.Item1.Trim();
                                deviceData.STATUS = 1;
                                device.DataList.Add(deviceData);
                                DeviceComponent.Save(dataProvider, device);
                                Device.Add(device);
                            }
                            else
                            {
                                OpDevice device = dataProvider.GetDevice(checkIfExists.SerialNbr.Value);
                                int retValue = ValidateDevice(dataProvider, 
                                    new List<Tuple<string,OpArticle>>(){ new Tuple<string, OpArticle>(checkIfExists.SerialNbr.ToString(), snItem.Item2) }, //new string[] { checkIfExists.SerialNbr.ToString() }, 
                                    true, 
                                    RadioDeviceTesterNewConnection, 
                                    device.DeviceOrderNumber.ToString(), 
                                    DevicesList,
                                    out Msg, 
                                    out Device, 
                                    out FailedList, 
                                    out AdditionalErrorData, 
                                    checkRepeatability, 
                                    onlyFirstError, 
                                    checkExistingOnShippingList, 
                                    false, 
                                    checkDeviceState, 
                                    false,
                                    checkExistingOnDepositoryLocation,
                                    checkIfAssignedToTask,
                                    checkSerialNbrLenght,
                                    checkIfTestedOnUSOK,
                                    checkIfAllDeviceOrderNumbersAreTheSame,
                                    checkExistingInPackage,
                                    checkExistingOnDepositoryLocationExceptServicePool,
                                    checkProductionValidation,
                                    checkArticle);
                                if (retValue != 0)
                                {
                                    Msg = " (" + snItem.Item1 + ")";
                                    if (onlyFirstError)
                                        return retValue;//AIUTDevice
                                    else
                                    {
                                        FailedList.Add(snItem.Item1, retValue);
                                        continue;
                                    }
                                }
                            }
                            
                        }
                        // }
                    }
                    #endregion
                }
            }
            if (FailedList.Count == 0)
                return 0;
            else
                return -1;
        }




        #endregion

        #region GetDeinstalledElements

        public static List<OpDepositoryElement> GetDeinstalledElements(DataProvider dataProvider, int idFitterOperator)
        {
            List<OpDepositoryElement> depElemList = null;

            OpOperator oItem = dataProvider.GetOperatorFilter(loadNavigationProperties: false, IdOperator: new int[] { idFitterOperator }).FirstOrDefault();
            if (oItem != null)
            {
                depElemList = GetDeinstalledElements(dataProvider, oItem);
            }

            return depElemList;
        }

        public static List<OpDepositoryElement> GetDeinstalledElements(DataProvider dataProvider, OpOperator fitterOperator)
        {
            List<OpDepositoryElement> depElemList = null;
            if (!fitterOperator.DataList.Exists(d => d.IdDataType == DataType.DEPOSITORY_ID))
            {
                OpOperatorData odItem = dataProvider.GetOperatorDataFilter(IdOperator: new int[] { fitterOperator.IdOperator },
                                                                           IdDataType: new long[] { DataType.DEPOSITORY_ID }).FirstOrDefault();
                long idDepositoryLocation = 0;
                if (odItem != null && odItem.Value != null
                    && !String.IsNullOrEmpty(odItem.Value.ToString())
                    && long.TryParse(odItem.Value.ToString(), out idDepositoryLocation))
                {
                    fitterOperator.DataList.Add(odItem);
                }
            }
            if (fitterOperator.DataList.Exists(d => d.IdDataType == DataType.DEPOSITORY_ID))
            {
                fitterOperator.DepositoryLocation = dataProvider.GetLocation(Convert.ToInt64(fitterOperator.DataList.Find(d => d.IdDataType == DataType.DEPOSITORY_ID).Value));
                if (fitterOperator.DepositoryLocation != null)
                {
                    depElemList = GetElements(dataProvider, fitterOperator.DepositoryLocation, true);
                }
            }
            return depElemList;
        }

        #endregion

        #region Get8DigitsSerialNumber

        public static long? Get8DigitsSerialNumber(DataProvider dataProvider, string serialNbr16Digits)
        {
            if (String.IsNullOrEmpty(serialNbr16Digits))
                return null;
            List<OpData> dataList = dataProvider.GetDataFilter(
                                        IdDataType: new long[] { IMR.Suite.Common.DataType.DEVICE_FACTORY_NUMBER },
                                        customWhereClause: "[VALUE] = '" + serialNbr16Digits.Trim() + "' and [SERIAL_NBR] is not null");
            if (dataList.Count > 0)
                return dataList.Where(w => w.IdData == dataList.Max(t => t.IdData)).First().SerialNbr;
            else
                return null;
        }

        #endregion

        #region TaskSettlement

        public static void TaskSettlementInstalledDeviceAdd(DataProvider dataProvider, OpTask task, OpOperator loggedSot, List<OpDepositoryElement> depositoryContent, List<int> toAddIdDepositoryList,
            bool executedFromTerminal, out List<OpDepositoryElement> installedToRoutePointModel, out List<OpDepositoryElement> toRemoveFromDepositoryModel)
        {
            //uzycie tych dwoch parametrow po wywolaniu metody mam sens tylko jesli jest ona wywolywana z IMR ST
            //dzieki temu mozna wprowadzic zainy w modelu
            installedToRoutePointModel = new List<OpDepositoryElement>();
            toRemoveFromDepositoryModel = new List<OpDepositoryElement>();
            foreach (int id_depository_element in toAddIdDepositoryList)
            {
                OpDistributor distributor = dataProvider.GetDistributor(task.Location.IdDistributor);
                OpDepositoryElement element = depositoryContent.Find(c => c.IdDepositoryElement == id_depository_element);//model.DepositoryElements.Find(c => c.IdDepositoryElement == id_depository_element);
                OpDepositoryElement elementAssignedToTask = DepositoryComponent.AssignElementToTask(
                    dataProvider, loggedSot, element.IdArticle, element.SerialNbr, element.IdDepositoryElement, task, element.ExternalSn, false, false, element.Notes);
                if (executedFromTerminal)
                {
                    //model.DepositoryElementsInstalledInRoutePoint.Add();
                    installedToRoutePointModel.Add(elementAssignedToTask);
                }

                #region Zdjęcie z puli serwisowej
                if (distributor != null && distributor.EquipmentLocationId.HasValue)
                {
                    OpLocation equipmentPool = dataProvider.GetLocation(distributor.EquipmentLocationId.Value);
                    if (equipmentPool != null)
                    {
                        if (element.Article.IsUnique)
                        {
                            if (element.SerialNbr.HasValue)
                            {
                                List<OpDepositoryElement> devices_in_pool = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[] { element.SerialNbr.Value }, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null());
                                foreach (OpDepositoryElement equipmentToRemove in devices_in_pool)
                                {
                                    DepositoryComponent.RemoveDepositoryElement(dataProvider, equipmentToRemove);
                                }
                                
                            }
                            else if (!String.IsNullOrEmpty(element.ExternalSn))
                            {
                                OpDepositoryElement deviceToRemove = dataProvider.GetDepositoryElementFilter(ExternalSn: element.ExternalSn, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault();
                                if (deviceToRemove != null)
                                    DepositoryComponent.RemoveDepositoryElement(dataProvider, deviceToRemove);
                                
                            }
                        }
                        else
                        {
                            OpDepositoryElement elementToRemove = dataProvider.GetDepositoryElementFilter(IdArticle: new long[] { element.IdArticle }, IdLocation: new long[] { equipmentPool.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault(); ;
                            if (elementToRemove != null)
                            {
                                DepositoryComponent.RemoveDepositoryElement(dataProvider, elementToRemove);
                            }
                            
                        }
                    }
                }
                #endregion
                #region Zdjęcie z magazynu serwisowego
                //LPIELA SERVICE_MAGAZINE
                // DepositoryComponent.RemoveFromServiceMagazine(dataProvider, element, task.Distributor);
                // string emailMessage = "DeviceInstalledAdd " + DataProvider.DateTimeNow + ", Task: " + task.IdTask + ", IdDepositoryElemenent: " + element.IdDepositoryElement;
                // EmailComponent.SendEmail("lpiela@aiut.com.pl", emailMessage,emailMessage);
                #endregion

                DepositoryComponent.RemoveDepositoryElement(dataProvider, element);
                if (executedFromTerminal)
                {
                    toRemoveFromDepositoryModel.Add(element);
                   // model.DepositoryElements.Remove(element);
                }
            }
        }

        public static void TaskSettlementInstalledDeviceRemove(DataProvider dataProvider, OpTask task, OpOperator loggedSot, OpDepositoryElement toRemove,
            bool executedFromTerminal, out OpDepositoryElement toAddToDepositoryModel)
        {
           // OpDepositoryElement toRemove = model.DepositoryElementsInstalledInRoutePoint.ElementAt(id_depository_element);
            //uzycie parametru po wywolaniu metody mam sens tylko jesli jest ona wywolywana z IMR ST
            //dzieki temu mozna wprowadzic zmiany w modelu
            toAddToDepositoryModel = null;
            if (toRemove != null)
            {
                DepositoryComponent.Delete(dataProvider, toRemove);
                //model.DepositoryElementsInstalledInRoutePoint.Remove(to_removed); to do outside of method

                if (toRemove.IdReference.HasValue)
                {
                    OpDepositoryElement old_element = dataProvider.GetDepositoryElement(toRemove.IdReference.Value);
                    if (old_element != null)
                    {
                        toAddToDepositoryModel = DepositoryComponent.AssignElementToLocation(dataProvider, loggedSot, old_element.IdArticle,
                                    old_element.SerialNbr, loggedSot.DepositoryLocation, old_element.ExternalSn, old_element.Removed, old_element.Notes);
                       // model.DepositoryElements.Add(DepositoryComponent.AssignElementToLocation(dataProvider, loggedOperator, old_element.IdArticle,
                                    //old_element.SerialNbr, loggedOperator.DepositoryLocation, old_element.ExternalSn, old_element.Removed, old_element.Notes));
                    }
                }
                else
                {
                    toAddToDepositoryModel = DepositoryComponent.AssignElementToLocation(dataProvider, loggedSot, toRemove.IdArticle,
                                       toRemove.SerialNbr, loggedSot.DepositoryLocation, toRemove.ExternalSn, toRemove.Removed, toRemove.Notes);
                   // model.DepositoryElements.Add(DepositoryComponent.AssignElementToLocation(dataProvider, loggedOperator, to_removed.IdArticle,
                                      // to_removed.SerialNbr, loggedOperator.DepositoryLocation, to_removed.ExternalSn, to_removed.Removed, to_removed.Notes));
                }
                if (!executedFromTerminal)
                    toAddToDepositoryModel = null;

                //kolejnosc przy montowaniu urzadzenia
                //1.AssignToTask
                //2.Remove DepositoryElement (pula)
                //3.Remove DepositoryElement (magazyn SOT'a)                            

                #region przywrócenie do puli serwisowej urzadzenia
                OpDistributor distributor = dataProvider.GetDistributor(task.Location.IdDistributor);
                if (distributor != null && distributor.EquipmentLocationId.HasValue)
                {
                    OpLocation equipmentPool = dataProvider.GetLocation(distributor.EquipmentLocationId.Value);
                    if (equipmentPool != null)
                    {
                        if (toRemove.Article.IsUnique)
                        {
                            if (toRemove.SerialNbr.HasValue)
                            {
                                string customWhereClause = "[END_DATE] is not null";
                                List<OpDepositoryElement> devices = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[] { toRemove.SerialNbr.Value }, IdLocation: new long[] { equipmentPool.IdLocation }, customWhereClause: customWhereClause).ToList();
                                if (devices.Count > 0)
                                {
                                    //znajdujemy pierwszy usuniety element po dodaniu to_removed. 
                                    OpDepositoryElement elementToRestore = devices.OrderBy(c => c.EndDate).FirstOrDefault(d => d.EndDate > toRemove.StartDate);
                                    if (elementToRestore != null)
                                    {
                                        DepositoryComponent.AssignElementToLocation(dataProvider, loggedSot, elementToRestore.IdArticle, elementToRestore.SerialNbr, elementToRestore.Location, null, false, elementToRestore.Notes);
                                    }
                                }
                            }
                            else if (!String.IsNullOrEmpty(toRemove.ExternalSn))
                            {
                                string customWhereClause = "[END_DATE] is not null";
                                List<OpDepositoryElement> devices = dataProvider.GetDepositoryElementFilter(ExternalSn: toRemove.ExternalSn, IdLocation: new long[] { equipmentPool.IdLocation }, customWhereClause: customWhereClause).ToList();
                                if (devices.Count > 0)
                                {
                                    //znajdujemy pierwszy usuniety element po dodaniu to_removed. 
                                    OpDepositoryElement elementToRestore = devices.OrderBy(c => c.EndDate).FirstOrDefault(d => d.EndDate > toRemove.StartDate);
                                    if (elementToRestore != null)
                                    {
                                        DepositoryComponent.AssignElementToLocation(dataProvider, loggedSot, elementToRestore.IdArticle, null, elementToRestore.Location, elementToRestore.ExternalSn, false, elementToRestore.Notes);
                                    }
                                }
                            }
                        }
                        else
                        {
                            string customWhereClause = "[END_DATE] is not null";
                            List<OpDepositoryElement> devices = dataProvider.GetDepositoryElementFilter(IdArticle: new long[] { toRemove.IdArticle }, IdLocation: new long[] { equipmentPool.IdLocation }, customWhereClause: customWhereClause);

                            OpDepositoryElement elementToRestore = devices.OrderBy(c => c.EndDate).FirstOrDefault(d => d.EndDate > toRemove.StartDate);
                            if (elementToRestore != null)
                            {
                                DepositoryComponent.AssignElementToLocation(dataProvider, loggedSot, elementToRestore.IdArticle, null, elementToRestore.Location, null, false, elementToRestore.Notes);
                            }
                        }
                    }
                }
                #endregion
            }
        }

        public static bool TaskSettlementDeinstalledDeviceAdd(DataProvider dataProvider, DBCommonRDT rdtnConnection, OpOperator loggedSot, OpTask task,
            string deinstalledSerialNbr, List<OpDepositoryElement> depositoryElementsDeinstalledInRoutePoint,
            bool fromAiut, OpArticle article, int articleCount, string notes,
            bool executedFromTerminal,
            out int validateResult, out List<OpDepositoryElement> deinstalledInRoutePointModel, out List<OpDepositoryElement> addToDepositoryModel,
            bool checkExistingOnDepositoryLocation = true,
            object[] additionalValidationData = null)
        {
            deinstalledInRoutePointModel = new List<OpDepositoryElement>();
            addToDepositoryModel = new List<OpDepositoryElement>();
            string errorMsgDeinstaled = "";
            long? serialNbr = null;
            string externalSN = "";
            List<OpDevice> devicesList = null;
            Dictionary<string, int> dict = new Dictionary<string, int>();
            Dictionary<string, string> additionallErrorData = new Dictionary<string, string>();

            List<string> devicesListStr = null;
            if (depositoryElementsDeinstalledInRoutePoint != null && depositoryElementsDeinstalledInRoutePoint.Count > 0)
            {
                devicesListStr = new List<string>();
                foreach (OpDepositoryElement deItem in depositoryElementsDeinstalledInRoutePoint)
                {
                    if (deItem.SerialNbr.HasValue)
                        devicesListStr.Add(deItem.SerialNbr.ToString());
                    else
                    {
                        if (!String.IsNullOrEmpty(deItem.ExternalSn))
                            devicesListStr.Add(deItem.ExternalSn);
                    }
                }
            }
            /*
            validateResult = DepositoryComponent.ValidateDevice(dataProvider, new string[] { deinstalledSerialNbr.Trim() },
                fromAiut, usokConnection, rdtnConnection,  null, devicesListStr,
                article, out errorMsgDeinstaled, out devicesList, out dict, out additionallErrorData,
                true, true, false, false, false, false, checkExistingOnDepositoryLocation, true);*/
            List<Tuple<string, OpArticle>> snList = new List<Tuple<string, OpArticle>>();
            snList.Add(new Tuple<string, OpArticle>(deinstalledSerialNbr.Trim(), article));
            validateResult = DepositoryComponent.ValidateDevice(dataProvider, snList,
                fromAiut, rdtnConnection, null, devicesListStr,
                out errorMsgDeinstaled, out devicesList, out dict, out additionallErrorData,
                checkRepeatability: true,
                onlyFirstError: true,
                checkExistingOnShippingList: false,
                checkExistingOnUSOK: false,
                checkDeviceState: false,
                addStrangeDevice: false,
                checkExistingOnDepositoryLocation: checkExistingOnDepositoryLocation,
                checkIfAssignedToTask: false,
                checkSerialNbrLenght: true,
                checkIfTestedOnUSOK: false,
                checkArticle: true,
                checkExistingInPackage: true);
            if (validateResult != 0)
            {
                #region FillAdditionalErrorData
                if (additionalValidationData != null)
                {
                    if (additionalValidationData.Count() > 0 && additionalValidationData[0] is Dictionary<string, int>)
                    {
                        foreach (KeyValuePair<string, int> kvItem in dict)
                        {
                            (additionalValidationData[0] as Dictionary<string, int>).Add(kvItem.Key, kvItem.Value);
                        }
                    }
                    if (additionalValidationData.Count() > 1 && additionalValidationData[1] is Dictionary<string, string>)
                    {
                        foreach (KeyValuePair<string, string> kvItem in additionallErrorData)
                        {
                            (additionalValidationData[1] as Dictionary<string, string>).Add(kvItem.Key, kvItem.Value);
                        }
                    }
                }
                #endregion
                //string errorText = DecodeDeviceErrorCode(result);
                // ModelState.AddModelError("DeviceError", errorText);
                #region Z_GRUPOWANIEM
                //Z GRUPOWANIEM
                //model.DepositoryElementsDeinstalledInRoutePoint = DepositoryComponent.GroupDepositoryElements(DepositoryComponent.GetElements(dataProvider, task, false, true));
                //model.DepositoryElementsInstalledInRoutePoint = DepositoryComponent.GroupDepositoryElements(DepositoryComponent.GetElements(dataProvider, task, false, false));
                #endregion
                // return View(model);
                return false;
            }
            

            if (fromAiut)
            {
                articleCount = 1;
            }
            else
            {
                if (article.IsUnique)
                {
                    if (!string.IsNullOrEmpty(deinstalledSerialNbr))
                    {
                        externalSN = deinstalledSerialNbr;
                        articleCount = 1;
                    }
                }
            }

            if (devicesList != null && devicesList.Count > 0)
                serialNbr = devicesList[0].SerialNbr;

            for (int i = 0; i < articleCount; i++)
            {
                OpDepositoryElement deinstalled = DepositoryComponent.AssignElementToTask(dataProvider, loggedSot, article.IdArticle, serialNbr, task, externalSN, false, true,
                    notes, dataProvider.GetDeviceStateType((int)OpDeviceStateType.Enum.REMOVED));
                if (executedFromTerminal)
                {
                    //model.DepositoryElementsDeinstalledInRoutePoint.Add(deinstalled);
                    deinstalledInRoutePointModel.Add(deinstalled);
                }

                if (executedFromTerminal)
                {//wywolujac spoza terminala tylko korygujemy przypisanie, urzadzenie juz jest na magazynie
                    OpDepositoryElement added_element = DepositoryComponent.AssignElementToLocation(dataProvider, loggedSot, article.IdArticle, serialNbr, deinstalled.IdDepositoryElement,
                        loggedSot.DepositoryLocation, externalSN, true, notes, dataProvider.GetDeviceStateType((int)OpDeviceStateType.Enum.REMOVED));
                
                    //model.DepositoryElements.Add(added_element);
                    addToDepositoryModel.Add(added_element);
                }

                if (fromAiut && serialNbr.HasValue)
                {//zmiana statusu urzadzenia
                    devicesList[i].DeviceStateType = dataProvider.DeviceStateType.Find(d => d.IdDeviceStateType == (int)Enums.DeviceState.REMOVED);
                    devicesList[i].IdDeviceStateType = (int)Enums.DeviceState.REMOVED;
                    DeviceComponent.Save(dataProvider, devicesList[i]);
                }
                //LPIELA SERVICE_MAGAZINE - zrezygnowano z pomyslu uzycia tego tutaj, magazyny modyfikowane przy akceptacji taska
                //Zmiana statusu urządzenia i stanow magazynów serwisowych (lokalnie i na serwerach telemetrycznych)
                //AddToServiceMagazine(dataProvider, added_element, Enums.DeviceState.REMOVED);
            }
            #region Z_GRUPOWANIEM
            //Z GRUPOWANIEM
            //model.DepositoryElementsDeinstalledInRoutePoint = DepositoryComponent.GroupDepositoryElements(DepositoryComponent.GetElements(dataProvider, task, false, true));
            //model.DepositoryElementsInstalledInRoutePoint = DepositoryComponent.GroupDepositoryElements(DepositoryComponent.GetElements(dataProvider, task, false, false));
            //model.DepositoryElements = DepositoryComponent.GroupDepositoryElements(GetElementsToInstall(loggedOperator), true);
            #endregion

            if (fromAiut && serialNbr.HasValue)
            {
                #region Warranty
                // zapisanie informacji: czy urzadzenie w momencie deinstalacji było na gwarancji
                OpData warrantyData = dataProvider.GetDataFilter(
                        loadNavigationProperties: false, autoTransaction: false,
                        SerialNbr: new long[] { serialNbr.Value },
                        IdDataType: new long[] { DataType.DEVICE_WARRANTY_DATE }).FirstOrDefault();

                if (warrantyData != null)
                {
                    OpDepositoryElement depositoryElement = dataProvider.GetDepositoryElementFilter(
                        loadNavigationProperties: false, autoTransaction: false,
                        SerialNbr: new long[] { serialNbr.Value },
                        IdTask: new int[] { task.IdTask },
                        IdArticle: new long[] { article.IdArticle }).FirstOrDefault();

                    OpDepositoryElementData depositoryElementData = new OpDepositoryElementData();
                    depositoryElementData.IdDataType = DataType.DEVICE_WARRANTY_DATE_DURING_DEINSTALLATION;
                    depositoryElementData.IdDepositoryElement = depositoryElement.IdDepositoryElement;
                    depositoryElementData.Value = warrantyData.Value;
                    dataProvider.SaveDepositoryElementData(depositoryElementData);


                    depositoryElementData = new OpDepositoryElementData();
                    depositoryElementData.IdDataType = DataType.DEVICE_WARRANTY_IS_VALID_DURING_DEINSTALLATION;
                    depositoryElementData.IdDepositoryElement = depositoryElement.IdDepositoryElement;
                    depositoryElementData.Value = ((DateTime)warrantyData.Value > dataProvider.DateTimeNow) ? true : false;
                    depositoryElementData.IdDepositoryElementData = 0;
                    dataProvider.SaveDepositoryElementData(depositoryElementData);

                    // zapisanie do CREDIT_LOCATION urządzen na gwarancji
                    if ((DateTime)warrantyData.Value > dataProvider.DateTimeNow)
                    {
                        OpDevice devCheck = dataProvider.GetDevice(serialNbr.Value);
                        OpDistributorData distributorData = dataProvider.GetDistributorDataFilter(
                            loadNavigationProperties: false, autoTransaction: false,
                            IdDataType: new long[] { DataType.CREDIT_DEPOSITORY_ID },
                            IdDistributor: new int[] { devCheck.IdDistributor }).FirstOrDefault();
                        if (distributorData == null)
                            distributorData = dataProvider.GetDistributorDataFilter(
                            loadNavigationProperties: false, autoTransaction: false,
                            IdDataType: new long[] { DataType.CREDIT_DEPOSITORY_ID },
                            IdDistributor: new int[] { 1 }).FirstOrDefault();

                        if (distributorData != null)
                        {
                            OpDepositoryElement op = new OpDepositoryElement();
                            op.IdArticle = article.IdArticle;
                            op.IdDeviceStateType = (int)OpDeviceStateType.Enum.REMOVED;
                            op.IdLocation = Convert.ToInt64(distributorData.Value);
                            op.StartDate = dataProvider.DateTimeNow;
                            op.SerialNbr = serialNbr.Value;
                            if (devicesList[0].DeviceType == null)
                                devicesList[0].DeviceType = dataProvider.GetDeviceType(devicesList[0].IdDeviceType);

                            op.IdDeviceTypeClass = devicesList[0].DeviceType.IdDeviceTypeClass;
                            dataProvider.SaveDepositoryElement(op);

                            string emailAddress = "mfrydryk@aiut.com.pl";
                            OpDistributorData distrProtector = dataProvider.GetDistributorDataFilter(IdDataType: new long[] { DataType.DISTRIBUTOR_PROTECTOR_OPERATOR_ID }).FirstOrDefault();
                            if (distrProtector != null && distrProtector.Value != null && !String.IsNullOrEmpty(distrProtector.Value.ToString()))
                            {
                                OpOperator operProtector = dataProvider.GetOperator(Convert.ToInt32(distrProtector.Value));
                                if (operProtector != null && operProtector.Actor != null && !String.IsNullOrEmpty(operProtector.Actor.Email))
                                    emailAddress = operProtector.Actor.Email;
                            }
                            string subject = "Zdemontowano urządzenie na gwarancji";
                            string body = "Id zadania: " + task.IdTask.ToString() + "<br/>Urządzenie: " + devicesList[0].SerialNbr.ToString() + "<br/>Data demontażu: " + op.StartDate.ToString() + ", Data gwarancji: " + warrantyData.Value.ToString() + "<br/>";
                            // EmailComponent.HashedPassword = "F2xTCpfOaKFstdRLHwMsC5b02e2XSVsgDnkwhUMYI+lSSy+AzFmL7HvbBhwcsz8loN6G80sMXrqnd3jxgAXO2OQkPa3xppQUKJXbbjtiF4Gt3REIo8YcnCLzkgejGPEIyTYieWjPHIYz1WCAxKUAtbjeAqg3heKsB/GZ/bPTln8=";
                            EmailComponent.SendEmail(emailAddress, subject, body);
                        }
                    }
                }
                #endregion
            }
            
            return true;
        }

        public static void TaskSettlementDeinstalledDeviceRemove(DataProvider dataProvider, OpDepositoryElement toRemoved, List<OpDepositoryElement> depositoryContent,
            bool executedFromTerminal, /*bool serviceMagazines,*/ out OpDepositoryElement removeFromDeinstalledInRoutePointModel, out OpDepositoryElement removeFromDepositoryModel)
        {
            //OpDepositoryElement to_removed = model.DepositoryElementsDeinstalledInRoutePoint.ElementAt(id_depository_element);
            removeFromDeinstalledInRoutePointModel = null;
            removeFromDepositoryModel = null;
            if (toRemoved != null)
            {
                #region Lokalnie Depository Element
                OpDepositoryElement toDeleteFromLocation = depositoryContent.FirstOrDefault(c => c.IdReference == toRemoved.IdDepositoryElement && c.Removed == true);//model.DepositoryElements.FirstOrDefault(c => c.IdReference == to_removed.IdDepositoryElement && c.Removed == true);

                if (toDeleteFromLocation == null)
                {
                    toDeleteFromLocation = new OpDepositoryElement();
                    if (String.Equals(toRemoved.ExternalSn, ""))
                        toRemoved.ExternalSn = null;
                    if (toRemoved.SerialNbr != null || toRemoved.ExternalSn != null)
                    {
                        toDeleteFromLocation = depositoryContent.Find(c => c.SerialNbr == toRemoved.SerialNbr && c.ExternalSn == toRemoved.ExternalSn && c.IdArticle == toRemoved.IdArticle);//model.DepositoryElements.Find(c => c.SerialNbr == toRemoved.SerialNbr && c.ExternalSn == toRemoved.ExternalSn && c.IdArticle == toRemoved.IdArticle);
                    }
                    else
                    {

                        List<OpDepositoryElement> nonuniques = depositoryContent.Where(c => c.IdArticle == toRemoved.IdArticle && c.Removed == true && c.StartDate.TimeOfDay > toRemoved.StartDate.TimeOfDay).ToList();//model.DepositoryElements.Where(c => c.IdArticle == toRemoved.IdArticle && c.Removed == true && c.StartDate.TimeOfDay > toRemoved.StartDate.TimeOfDay).ToList();

                        //test
                        nonuniques = depositoryContent.Where(c => c.IdArticle == toRemoved.IdArticle).ToList();//model.DepositoryElements.Where(c => c.IdArticle == to_removed.IdArticle).ToList();
                        nonuniques = depositoryContent.Where(c => c.Removed == toRemoved.Removed).ToList();//model.DepositoryElements.Where(c => c.Removed == to_removed.Removed).ToList();

                        TimeSpan diffrence = (nonuniques[0].StartDate.TimeOfDay - toRemoved.StartDate.TimeOfDay);
                        toDeleteFromLocation = nonuniques[0];

                        foreach (OpDepositoryElement dep in nonuniques)
                        {
                            TimeSpan diff = dep.StartDate.TimeOfDay - toRemoved.StartDate.TimeOfDay;
                            TimeSpan zero = new TimeSpan(0);
                            if (TimeSpan.Compare(diff, zero) > 0 && TimeSpan.Compare(diff, diffrence) < 0)
                            {
                                diffrence = dep.StartDate.TimeOfDay - toRemoved.StartDate.TimeOfDay;
                                toDeleteFromLocation = dep;
                            }
                        }
                    }
                }
                if (toDeleteFromLocation != null)
                {
                    if (executedFromTerminal)
                    {//wywolujac nie z termnalal nie chcemy usuwac elementu z magazynu SOT, tylko przypisanie do taska
                        DepositoryComponent.Delete(dataProvider, toDeleteFromLocation);
                        //model.DepositoryElements.Remove(to_delete);
                    }
                    else
                    {
                        toDeleteFromLocation.IdReference = null;
                        dataProvider.SaveDepositoryElement(toDeleteFromLocation);
                    }
                    DepositoryComponent.Delete(dataProvider, toRemoved);
                    if (executedFromTerminal)
                    {
                        //model.DepositoryElementsDeinstalledInRoutePoint.Remove(to_removed);
                        removeFromDeinstalledInRoutePointModel = toRemoved;
                    }

                    if (toDeleteFromLocation.SerialNbr.HasValue)
                    {
                        OpDeviceStateHistory lastDeviceStateHistory = dataProvider.GetDeviceStateHistoryFilter(SerialNbr: new long[] { toDeleteFromLocation.SerialNbr.Value }).LastOrDefault();
                        if (lastDeviceStateHistory != null)
                        {
                            if (toDeleteFromLocation.DeviceStateType.IdDeviceStateType == lastDeviceStateHistory.IdDeviceStateType)
                                DeviceStateHistoryComponent.UndoLastHistoryEntry(dataProvider, toDeleteFromLocation.SerialNbr.Value);
                        }
                    }
                }
                #endregion
                #region Lokalnie Magazyny Serwisowe
                /*if (serviceMagazines)
                {
                    if (toRemoved.SerialNbr.HasValue)
                    {
                        List<OpLocationEquipment> toRemoveFromMagazine = dataProvider.GetLocationEquipmentFilter(IdLocation: new long[] { toRemoved.Device.Distributor.ServiceMagazine.IdLocation },
                            SerialNbr: new long[] { toRemoved.SerialNbr.Value }, customWhereClause: "[END_DATE] is null");
                        if (toRemoveFromMagazine != null)
                        {
                            foreach (OpLocationEquipment leItem in toRemoveFromMagazine)
                            {
                                dataProvider.DeleteLocationEquipment(leItem);
                            }
                        }
                       // List<OpLocationEquipment> toReopen = dataProvider.GetLocationEquipmentFilter(SerialNbr:
                    }
                }*/
                #endregion
                #region Serwer trelemetryczny Magazyny Serwisowe
                /*if (serviceMagazines)
                {

                }*/
                #endregion
            }
        }

        #endregion

        #region AddToServiceMagazine

        public static void AddToServiceMagazine(DataProvider dataProvider, OpDepositoryElement depositoryElement, Enums.DeviceState changeDeviceStateTo)
        {
           // OpTask task = null;
          //  if (executedFrom == MagazineAdd.TaskAcceptation && arg == null && !(arg is OpTask))
          //      throw new Exception("Missing argument arg");
          //  if (executedFrom == MagazineAdd.TaskAcceptation)
          //      task = arg as OpTask;
            
            if (depositoryElement.SerialNbr.HasValue)
            {
                UTDWebServiceSoapClient client = UTDComponent.GetUTDClient(depositoryElement.Device.Distributor.ImrServer.WebserviceAddress,
                        depositoryElement.Device.Distributor.ImrServer.WebserviceTimeout);
                #region AIUT
                OpDevice deviceToService = depositoryElement.Device;
                deviceToService.DeviceStateType = dataProvider.DeviceStateType.Where(d => d.IdDeviceStateType == (int)changeDeviceStateTo).First();
                dataProvider.SaveDevice(deviceToService);
                try
                {
                    string errorMsg = "";
                    client.Open();
                    if (!client.ChangeDeviceState(out errorMsg, depositoryElement.SerialNbr.Value, (int)changeDeviceStateTo, depositoryElement.Device.IdDistributor, depositoryElement.Device.Distributor.ImrServer.ImrServerVersion))
                        throw new Exception(errorMsg);
                    if (client != null)
                        client.Close();
                }
                catch (Exception ex)
                {
                    if (client != null)
                        client.Close();
                    throw new Exception(ex.Message);
                }
                
                if (depositoryElement.Device.Distributor.ImrServer.ToString().Contains("localhost"))
                {
                    #region localhost
                    //nie trzeba zmieniac statusu na innym serwerze, ale trzeba ewentualnie uzupełnić odpowiedni magazyn
                    List<OpLocationEquipment> existingOpenedEntries = dataProvider.GetLocationEquipmentFilter(
                            SerialNbr: new long[] { depositoryElement.Device.SerialNbr }, customWhereClause: "[END_TIME] is null AND ID_LOCATION != " + depositoryElement.Device.Distributor.ServiceMagazine.IdLocation);
                    if (existingOpenedEntries != null)
                    {
                        foreach (OpLocationEquipment leItem in existingOpenedEntries)
                        {
                            leItem.EndTime = dataProvider.DateTimeNow;
                            dataProvider.SaveLocationEquipment(leItem);
                        }
                    }

                    OpLocationEquipment magazineEq = dataProvider.GetLocationEquipmentFilter(IdLocation: new long[] { depositoryElement.Device.Distributor.ServiceMagazine.IdLocation },
                        SerialNbr: new long[] { depositoryElement.Device.SerialNbr }, customWhereClause: "[END_TIME] is null").FirstOrDefault();
                    if (magazineEq == null)
                    {
                        OpLocationEquipment newEquipment = new OpLocationEquipment();
                        newEquipment.IdLocation = depositoryElement.Device.Distributor.ServiceMagazine.IdLocation;
                        newEquipment.SerialNbr = depositoryElement.Device.SerialNbr;
                        newEquipment.StartTime = dataProvider.DateTimeNow;
                        dataProvider.SaveLocationEquipment(newEquipment);
                    }
                    #endregion
                }
                else
                {
                    try
                    {
                        client = UTDComponent.GetUTDClient(depositoryElement.Device.Distributor.ImrServer.WebserviceAddress,
                                        depositoryElement.Device.Distributor.ImrServer.WebserviceTimeout);
                        client.Open();
                        string errorMsg = "";
                        //stanow magazynowych nie sciaga collector, trzeba je tez ustawic lokalnie
                        #region Operacje lokalne
                        List<OpLocationEquipment> existingOpenedEntriesLocal = dataProvider.GetLocationEquipmentFilter(
                                                                SerialNbr: new long[] { depositoryElement.SerialNbr.Value },
                                                                customWhereClause: "[END_TIME] is null AND ID_LOCATION != " + depositoryElement.Device.Distributor.ServiceMagazine.IdLocation);
                        if (existingOpenedEntriesLocal != null)
                        {
                            foreach (OpLocationEquipment leItem in existingOpenedEntriesLocal)
                            {
                                leItem.EndTime = dataProvider.DateTimeNow;
                                dataProvider.SaveLocationEquipment(leItem);
                            }
                        }

                        List<OpLocationEquipment> magazineContentLocal = dataProvider.GetLocationEquipmentFilter(IdLocation: new long[] { depositoryElement.Device.Distributor.ServiceMagazine.IdLocation },
                                                                SerialNbr: new long[] { depositoryElement.SerialNbr.Value },
                                                                customWhereClause: "[END_TIME] is null");
                        DataTable magazineContent = null;
                        if (magazineContentLocal == null || magazineContentLocal.Count == 0)
                        {
                            if (depositoryElement.Device.Distributor.ImrServer.IsImrlt)
                            {
                                magazineContent = client.GetLocationEquipmentFilter(out errorMsg, null, new long[] { depositoryElement.Device.Distributor.ServiceMagazine.IdLocationOriginal },
                                    null, new long[] { depositoryElement.SerialNbr.Value }, "[END_TIME] is null");
                            }
                            OpLocationEquipment leNewItem = new OpLocationEquipment();
                            leNewItem.IdLocation = depositoryElement.Device.Distributor.ServiceMagazine.IdLocation;
                            leNewItem.SerialNbr = depositoryElement.SerialNbr.Value;
                            if (magazineContent == null || magazineContent.Rows.Count == 0)
                                leNewItem.StartTime = dataProvider.DateTimeNow;
                            else
                            {
                                DateTime? startTime = null;
                                foreach (DataRow dr in magazineContent.Rows)
                                {
                                    if (!startTime.HasValue || startTime.Value < Convert.ToDateTime(dr["START_TIME"]))
                                        startTime = Convert.ToDateTime(dr["START_TIME"]);
                                }
                                if (!startTime.HasValue)
                                    startTime = dataProvider.DateTimeNow;
                                leNewItem.StartTime = startTime.Value;
                            }

                            dataProvider.SaveLocationEquipment(leNewItem);
                        }
                        #endregion
                        //analogiczne zmiany na serwerze telemetrycznym
                        #region Operacje na serwerze telemetrycznym
                        if (depositoryElement.Device.Distributor.ImrServer.IsImrlt)
                        {
                            DataTable existingOpenedEntries = client.GetLocationEquipmentFilter(out errorMsg, null, null,
                                    null, new long[] { depositoryElement.SerialNbr.Value }, "[END_TIME] is null AND ID_LOCATION != " + depositoryElement.Device.Distributor.ServiceMagazine.IdLocationOriginal);
                            if (errorMsg.Length > 0)
                                throw new Exception(errorMsg);
                            if (existingOpenedEntries != null)
                            {
                                foreach (DataRow dr in existingOpenedEntries.Rows)
                                {
                                    if (!client.EndLocationEquipment(out errorMsg, Convert.ToInt64(dr["ID_LOCATION_EQUIPMENT"])))
                                        throw new Exception(errorMsg);
                                }
                            }

                            if (magazineContent == null)
                                magazineContent = client.GetLocationEquipmentFilter(out errorMsg, null, new long[] { depositoryElement.Device.Distributor.ServiceMagazine.IdLocationOriginal },
                                    null, new long[] { depositoryElement.SerialNbr.Value }, "[END_TIME] is null");
                            if (errorMsg.Length > 0 || magazineContent == null)
                                throw new Exception(errorMsg);
                            if (magazineContent.Rows.Count == 0)
                            {
                                //element nie znajduje sie wtedy magazynie, wiec go tam wprowadzamy
                                client.SaveLocationEquipment(0, depositoryElement.Device.Distributor.ServiceMagazine.IdLocationOriginal, null, depositoryElement.SerialNbr.Value,
                                    out errorMsg);
                                if (errorMsg.Length > 0)
                                    throw new Exception(errorMsg);
                            }

                        }
                        #endregion
                        // }
                        if (client != null)
                            client.Close();
                    }
                    catch (Exception ex)
                    {
                        if (client != null)
                            client.Close();
                        throw new Exception(ex.Message);
                    }
                }
                #endregion
            }
            else
            {
                #region Other
                OpDepositoryElement depElemTask = null;
                if (depositoryElement.IdLocation.HasValue && depositoryElement.IdReference.HasValue)
                {
                    depElemTask = dataProvider.GetDepositoryElement(depositoryElement.IdReference.Value);
                }
                else if (depositoryElement.IdPackage.HasValue && depositoryElement.IdReference.HasValue)
                {
                    OpDepositoryElement temp = dataProvider.GetDepositoryElement(depositoryElement.IdReference.Value);
                    if (temp.IdReference.HasValue)
                        depElemTask = dataProvider.GetDepositoryElement(temp.IdReference.Value);
                }
                else if (depositoryElement.IdTask.HasValue)
                    depElemTask = depositoryElement;
                OpLocation distributorMagazine = null;
                if (depElemTask != null)
                    distributorMagazine = depElemTask.Task.Distributor.ServiceMagazine;
                else
                {
                    OpDistributorData systemUserMagazineDistrData = dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { 1 },
                        IdDataType: new long[] { DataType.DISTRIBUTOR_SERVICE_MAGAZINE_LOCATION_ID }).FirstOrDefault();
                    distributorMagazine = dataProvider.GetLocation(Convert.ToInt64(systemUserMagazineDistrData.Value));
                }
                OpDepositoryElement depElem = new OpDepositoryElement();
                depElem.Location = distributorMagazine;
                depElem.ExternalSn = depositoryElement.ExternalSn;
                depElem.Removed = depositoryElement.Removed;
                depElem.Ordered = depositoryElement.Ordered;
                depElem.Accepted = depositoryElement.Accepted;
                depElem.IdReference = depositoryElement.IdDepositoryElement;
                depElem.IdArticle = depositoryElement.IdArticle;
                depElem.IdDeviceTypeClass = depositoryElement.IdDeviceTypeClass;
                depElem.IdDeviceStateType = (int)changeDeviceStateTo;
                depElem.Notes = depositoryElement.Notes;
                depElem.StartDate = dataProvider.DateTimeNow;
                dataProvider.SaveDepositoryElement(depElem);
                #endregion
            }
        }

        #endregion

        #region RemoveFromServiceMagazine

        public static void RemoveFromServiceMagazine(DataProvider dataProvider, OpDepositoryElement depositoryElement, OpDistributor distributor)
        {
            #region Synchronizacja DepositoryElement magazynu (lokalnie)
            if (depositoryElement != null)
            {
                if (depositoryElement.Article.IsUnique && !(depositoryElement.Article.IsUnique && depositoryElement.Article.Name.ToUpper().Contains("ABAT") && !depositoryElement.SerialNbr.HasValue))
                {
                    if (depositoryElement.SerialNbr.HasValue)//magazyny erwisowe nie trzymaja urzadzen AIUT w DEPOSITORY_ELEMENT tylko w LOCATION_EQUIPMENT
                    {
                        //LPIELA SERVICE_MAGAZINE
                        /*
                        OpDepositoryElement deviceToRemove = dataProvider.GetDepositoryElementFilter(SerialNbr: new long[]{ depositoryElement.SerialNbr.Value },  
                            IdLocation: new long[] { distributor.ServiceMagazine.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault();
                        if (deviceToRemove != null)
                            DepositoryComponent.RemoveDepositoryElement(dataProvider, deviceToRemove);
                        */
                    }
                    else if (!String.IsNullOrEmpty(depositoryElement.ExternalSn))
                    {
                        //LPIELA SERVICE_MAGAZINE
                        OpDepositoryElement deviceToRemove = dataProvider.GetDepositoryElementFilter(ExternalSn: depositoryElement.ExternalSn, IdLocation: new long[] { distributor.ServiceMagazine.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault();
                        if (deviceToRemove != null)
                            DepositoryComponent.RemoveDepositoryElement(dataProvider, deviceToRemove);
                    }
                }
                else
                {
                    //LPIELA SERVICE_MAGAZINE
                    OpDepositoryElement elementToRemove = dataProvider.GetDepositoryElementFilter(IdArticle: new long[] { depositoryElement.IdArticle }, IdLocation: new long[] { distributor.ServiceMagazine.IdLocation }, EndDate: TypeDateTimeCode.Null()).FirstOrDefault(); ;
                    if (elementToRemove != null)
                    {
                        DepositoryComponent.RemoveDepositoryElement(dataProvider, elementToRemove);
                    }
                }
            }
            #endregion
            #region  Synchronizacja LocationEquipment magazynu dystrybutora (lokalnie i na serwerze telemetrycznym)
            //LPIELA SERVICE_MAGAZINE
            if (distributor != null && distributor.IdServiceMagazine.HasValue && distributor.ServiceMagazine != null)
            {
                if (depositoryElement.Article.IsUnique)
                {
                    if (depositoryElement.SerialNbr.HasValue)
                    {
                        //LPIELA SERVICE_MAGAZINE
                        List<OpLocationEquipment> devicesInServiceMagazine = dataProvider.GetLocationEquipmentFilter(IdLocation: new long[] { depositoryElement.Device.Distributor.ServiceMagazine.IdLocation },
                            SerialNbr: new long[] { depositoryElement.SerialNbr.Value }, customWhereClause: "[END_TIME] is null");
                        if (devicesInServiceMagazine != null)
                        {
                            foreach (OpLocationEquipment leItem in devicesInServiceMagazine)
                            {
                                LocationEquipmentComponent.CloseEntry(dataProvider, leItem);
                            }
                        }
                        if (!depositoryElement.Device.Distributor.ImrServer.ToString().Contains("localhost"))
                        {
                            UTDWebServiceSoapClient client = UTDComponent.GetUTDClient(depositoryElement.Device.Distributor.ImrServer.WebserviceAddress,
                                                                            depositoryElement.Device.Distributor.ImrServer.WebserviceTimeout);
                            try
                            {
                                string errorMsg = "";
                                client.Open();
                                DataTable notClosedEntries = null;
                                notClosedEntries = client.GetLocationEquipmentFilter(out errorMsg, null, new long[] { depositoryElement.Device.Distributor.ServiceMagazine.IdLocationOriginal }, null,
                                    new long[] { depositoryElement.SerialNbr.Value }, "[END_TIME] is null");
                                if (errorMsg.Length > 0)
                                    throw new Exception("GetLocationEquipmentFilter - " + errorMsg);
                                if (notClosedEntries != null)
                                {
                                    foreach(DataRow dr in notClosedEntries.Rows)
                                    {
                                        if (!client.EndLocationEquipment(out errorMsg, Convert.ToInt64(dr["ID_LOCATION_EQUIPMENT"])))
                                            throw new Exception("EndLocationEquipment - " + errorMsg);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                EmailComponent.SendEmail("lpiela@aiut.com.pl", "IMR ST error occured", "Error during operation on service magazine, server: " + distributor.ImrServer.ToString() + ", device: " + depositoryElement.SerialNbr + ". Details: " + ex.Message);
                            }
                            finally
                            {
                                if (client != null)
                                    client.Close();
                            }
                        }
                            
                    }
                }
            }
            #endregion
        }

        #endregion

        #region ReWarehousing

        public static void ReWarehousing(DataProvider dataProvider, OpLocation newDepository, OpDepositoryElement deToReWarehousing, OpLocationEquipment leToReWarehousing, Dictionary<long, bool> transferMeterWithDeviceDict, bool useDBCollector = false)
        {
            ReWarehousing(dataProvider, newDepository,
                deToReWarehousing != null ? new List<OpDepositoryElement>() { deToReWarehousing } : new List<OpDepositoryElement>(),
                leToReWarehousing != null ? new List<OpLocationEquipment>() { leToReWarehousing } : new List<OpLocationEquipment>(),
                transferMeterWithDeviceDict, useDBCollector);
        }

        public static void ReWarehousing(DataProvider dataProvider, OpLocation newDepository, OpDepositoryElement deToReWarehousing, List<OpLocationEquipment> leToReWarehousing, Dictionary<long, bool> transferMeterWithDeviceDict, bool useDBCollector = false)
        {
            ReWarehousing(dataProvider, newDepository, deToReWarehousing != null ? new List<OpDepositoryElement>() { deToReWarehousing } : new List<OpDepositoryElement>(), leToReWarehousing, transferMeterWithDeviceDict, useDBCollector);
        }

        public static void ReWarehousing(DataProvider dataProvider, OpLocation newDepository, List<OpDepositoryElement> deToReWarehousing, OpLocationEquipment leToReWarehousing, Dictionary<long, bool> transferMeterWithDeviceDict, bool useDBCollector = false)
        {
            ReWarehousing(dataProvider, newDepository, deToReWarehousing, leToReWarehousing != null ? new List<OpLocationEquipment>() { leToReWarehousing } : new List<OpLocationEquipment>(), transferMeterWithDeviceDict, useDBCollector);
        }

        public static void ReWarehousing(DataProvider dataProvider, OpLocation newDepository, List<OpDepositoryElement> deToReWarehousing, List<OpLocationEquipment> leToReWarehousing, Dictionary<long, bool> transferMeterWithDeviceDict, bool useDBCollector = false)
        {
            // transferMeterWithDeviceDict - jeśli do urządzenia przypisany jest meter słownik zawiera decyzje czy przenosimy meter wraz z urządzeniem na nową lokalizacje (true)
            // bądź czy otweiramy nowy wpis osobno dla metera i osobno dla urządzenia (false lub brak)
            try
            {
                if (newDepository != null && newDepository.IdLocationType == (int)OpLocationType.Enum.DEPOSITORY)
                {
                    #region DepositoryElement
                    if (deToReWarehousing != null && deToReWarehousing.Count > 0)
                    {
                        foreach (OpDepositoryElement deItem in deToReWarehousing)
                        {
                            if (!deItem.EndDate.HasValue) // przenosimy tylko aktualnie znajdujące się w magazynie elementy
                            {
                                //zamykamy stary wpis
                                deItem.EndDate = dataProvider.DateTimeNow;
                                deItem.OpState = OpChangeState.Modified;
                                Save(dataProvider, deItem);

                                // klonujemy obiekt i otwieramy nowy wpis
                                OpDepositoryElement newDepositoryElement = new OpDepositoryElement(deItem);
                                newDepositoryElement.IdDepositoryElement = 0;
                                newDepositoryElement.EndDate = null;
                                newDepositoryElement.StartDate = dataProvider.DateTimeNow;
                                newDepositoryElement.Location = newDepository;
                                newDepositoryElement.OpState = OpChangeState.New;
                                Save(dataProvider, newDepositoryElement);
                            }
                        }
                    }
                    #endregion
                    #region LocationEquipment
                    if (leToReWarehousing != null && leToReWarehousing.Count > 0)
                    {
                        foreach (OpLocationEquipment leItem in leToReWarehousing)
                        {
                            if (leItem.IdLocation.HasValue && !leItem.EndTime.HasValue)
                            {
                                OpDevice device = leItem.Device != null ? leItem.Device : dataProvider.GetDevice(leItem.SerialNbr.Value);
                                OpMeter meter = null;
                                if (leItem.IdMeter.HasValue)                                
                                    meter = leItem.Meter != null ? leItem.Meter : dataProvider.GetMeter(leItem.IdMeter.Value);
                                
                                if (leItem.IdMeter.HasValue && leItem.SerialNbr.HasValue && transferMeterWithDeviceDict.ContainsKey(leItem.SerialNbr.Value) && transferMeterWithDeviceDict[leItem.SerialNbr.Value])
                                {
                                    // Mamy powiązany Meter z Urządzeniem i chcemy je przenieść w całosci                               
                                    LocationEquipmentComponent.CloseEntry(dataProvider, leItem, useDBCollector: useDBCollector);                               
                                    LocationEquipmentComponent.NewEntry(dataProvider, newDepository, meter, device, useDBCollector: useDBCollector);
                                }
                                else
                                {
                                    if (leItem.IdMeter.HasValue && leItem.SerialNbr.HasValue)
                                    {
                                        // Mamy powiązany Meter z Urzadzeniem, ale chcemy je rozdzielić
                                        // Urządzenie trafia na nową lokalizacje, a meter zostaje na starej.
                                        LocationEquipmentComponent.CloseEntry(dataProvider, leItem, useDBCollector: useDBCollector);
                                        LocationEquipmentComponent.NewEntry(dataProvider, newDepository, null, device, useDBCollector: useDBCollector);

                                        if (leItem.Location == null && leItem.IdLocation.HasValue)
                                            leItem.Location = dataProvider.GetLocation(leItem.IdLocation.Value);

                                        LocationEquipmentComponent.NewEntry(dataProvider, leItem.Location, meter, null, useDBCollector: useDBCollector);
                                    }
                                    else if (!leItem.IdMeter.HasValue && leItem.SerialNbr.HasValue)
                                    {
                                        // Mamy samo urządzenie bez Metera
                                        LocationEquipmentComponent.CloseEntry(dataProvider, leItem, useDBCollector: useDBCollector);
                                        LocationEquipmentComponent.NewEntry(dataProvider, newDepository, null, device, useDBCollector: useDBCollector);
                                    }
                                    else if (leItem.IdMeter.HasValue && !leItem.SerialNbr.HasValue)
                                    {
                                        // Mamy meter bez urządzenia
                                        LocationEquipmentComponent.CloseEntry(dataProvider, leItem, useDBCollector: useDBCollector);
                                        LocationEquipmentComponent.NewEntry(dataProvider, newDepository, meter, null, useDBCollector: useDBCollector);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }
}
