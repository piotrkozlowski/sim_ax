﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
	public class DataTypeFormatComponent
	{
        #region Methods
        public static bool ValidateRange(object data, OpDataType dataType, OpDataTypeFormat dataTypeFormat)
        {
            if (dataType != null && dataTypeFormat != null)
            {
                // wartosc wymagana, a jest null - zwroc false
                if (dataTypeFormat.IsRequired.HasValue && dataTypeFormat.IsRequired.Value && data == null)
                    return false;

                if (data != null)
                {
                    switch (dataType.IdDataTypeClass)
                    {
                        case (int)Enums.DataTypeClass.Integer:
                            // zdefiniowana wartosc minimalna, a jest < od niej
                            if (dataTypeFormat.NumberMinValue.HasValue && Convert.ToInt64(data) < dataTypeFormat.NumberMinValue.Value)
                                return false;
                            // zdefiniowana wartosc maksymalna, a jest > od niej
                            if (dataTypeFormat.NumberMaxValue.HasValue && Convert.ToInt64(data) > dataTypeFormat.NumberMaxValue.Value)
                                return false;
                            break;
                        case (int)Enums.DataTypeClass.Real:
                            // zdefiniowana wartosc minimalna, a jest < od niej
                            if (dataTypeFormat.NumberMinValue.HasValue && Convert.ToDouble(data) < dataTypeFormat.NumberMinValue.Value)
                                return false;
                            // zdefiniowana wartosc maksymalna, a jest > od niej
                            if (dataTypeFormat.NumberMaxValue.HasValue && Convert.ToDouble(data) > dataTypeFormat.NumberMaxValue.Value)
                                return false;
                            break;
                        case (int)Enums.DataTypeClass.Decimal:
                            // zdefiniowana wartosc minimalna, a jest < od niej
                            if (dataTypeFormat.NumberMinValue.HasValue && Convert.ToDecimal(data) < Convert.ToDecimal(dataTypeFormat.NumberMinValue.Value))
                                return false;
                            // zdefiniowana wartosc maksymalna, a jest > od niej
                            if (dataTypeFormat.NumberMaxValue.HasValue && Convert.ToDecimal(data) > Convert.ToDecimal(dataTypeFormat.NumberMaxValue.Value))
                                return false;
                            break;
                        case (int)Enums.DataTypeClass.Text:
                            // zdefiniowana minimalna dlugosc tekstu, a jest < od niej
                            if (dataTypeFormat.TextMinLength.HasValue && data.ToString().Length < dataTypeFormat.TextMinLength.Value)
                                return false;
                            // zdefiniowana maksymalna dlugosc tekstu, a jest > od niej
                            if (dataTypeFormat.TextMaxLength.HasValue && data.ToString().Length > dataTypeFormat.TextMaxLength.Value)
                                return false;
                            break;
                    }
                }
            }
            return true;
        }

        public static Dictionary<long, int?> GetDataTypePrecisionDict(List<OpDataFormatGroupDetails> dataFormatGroupDetails, bool outDataTypeFormat)
        {
            Dictionary<long, int?> dict = new Dictionary<long, int?>();
            if (dataFormatGroupDetails == null || dataFormatGroupDetails.Count == 0) return dict;

            foreach (OpDataFormatGroupDetails dataFormat in dataFormatGroupDetails)
            {
                if (outDataTypeFormat)
                {
                    if (dataFormat.IdDataTypeFormatOut.HasValue && dataFormat.DataTypeFormatOut != null)
                    {
                        if (dataFormat.DataTypeFormatOut.NumberMaxPrecision.HasValue)
                            dict[dataFormat.IdDataType] = dataFormat.DataTypeFormatOut.NumberMaxPrecision.Value;
                        else if (dataFormat.DataTypeFormatOut.NumberMinPrecision.HasValue)
                            dict[dataFormat.IdDataType] = dataFormat.DataTypeFormatOut.NumberMinPrecision.Value;
                    }
                }
                else
                {
                    if (dataFormat.IdDataTypeFormatIn.HasValue && dataFormat.DataTypeFormatIn != null)
                    {
                        if (dataFormat.DataTypeFormatIn.NumberMaxPrecision.HasValue)
                            dict[dataFormat.IdDataType] = dataFormat.DataTypeFormatIn.NumberMaxPrecision.Value;
                        else if (dataFormat.DataTypeFormatIn.NumberMinPrecision.HasValue)
                            dict[dataFormat.IdDataType] = dataFormat.DataTypeFormatIn.NumberMinPrecision.Value;
                    }
                }
            }
            return dict;
        }

        public static Dictionary<long, string> GetFormatPatternDict(List<OpDataFormatGroupDetails> dataFormatGroupDetails, bool outDataTypeFormat, string pattern, string specifier, bool useBraces)
        {
            return GetFormatPatternDict(GetDataTypePrecisionDict(dataFormatGroupDetails, outDataTypeFormat), pattern, specifier, useBraces);
        }
        public static Dictionary<long, string> GetFormatPatternDict(Dictionary<long, int?> dataTypePrecisionDict, string pattern, string specifier, bool useBraces)
        {
            Dictionary<long, string> dict = new Dictionary<long, string>();
            if (dataTypePrecisionDict == null || dataTypePrecisionDict.Count == 0) return dict;

            foreach (var keyValue in dataTypePrecisionDict)
            {
                if (keyValue.Value.HasValue && keyValue.Value.Value >= 0)
                {
                    StringBuilder sb = new StringBuilder();
                    if (useBraces)
                        sb.Append("{");

                    if (!String.IsNullOrWhiteSpace(pattern))
                        sb.Append(pattern).Append(":");

                    if (String.IsNullOrWhiteSpace(specifier))
                    {
                        sb.Append("0");
                        if (keyValue.Value.Value > 0)
                            sb.Append(".");
                        for (int i = 0; i < keyValue.Value.Value; i++)
                            sb.Append("0");
                    }
                    else
                        sb.Append(specifier).Append(keyValue.Value.Value);

                    if (useBraces)
                        sb.Append("}");
                    dict[keyValue.Key] = sb.ToString();
                }
            }

            return dict;
        }

        public static Dictionary<long, OpDataTypeFormat> GetDataTypeFormatDict(List<OpDataFormatGroupDetails> dataFormatGroupDetails, bool outDataTypeFormat)
        {
            Dictionary<long, OpDataTypeFormat> dict = new Dictionary<long, OpDataTypeFormat>();
            if (dataFormatGroupDetails == null || dataFormatGroupDetails.Count == 0) return dict;

            foreach (OpDataFormatGroupDetails dataFormat in dataFormatGroupDetails)
            {
                if (outDataTypeFormat)
                {
                    if (dataFormat.IdDataTypeFormatOut.HasValue && dataFormat.DataTypeFormatOut != null)
                        dict[dataFormat.IdDataType] = dataFormat.DataTypeFormatOut;
                }
                else
                {
                    if (dataFormat.IdDataTypeFormatIn.HasValue && dataFormat.DataTypeFormatIn != null)
                        dict[dataFormat.IdDataType] = dataFormat.DataTypeFormatIn;
                }
            }
            return dict;
        }

        public static int? GetPrecision(OpDataTypeFormat dataTypeFormat)
        {
            return OpDataTypeFormat.GetPrecision(dataTypeFormat);
        }
	    #endregion
	}
}
