﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Drawing;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class OperatorComponent : BaseComponent
    {
        #region Get | Save | Delete

        #region Get

        #region GetByID
        public static OpOperator GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpOperator GetByID(DataProvider dataProvider, int id, bool queryDatabase, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            if (id == 0)
                return null;

            OpOperator operatorObj = dataProvider.GetOperator(new int[] { id }, queryDatabase, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData).FirstOrDefault();
            if (operatorObj != null && queryDatabase)
                operatorObj.Actor = dataProvider.GetActor(operatorObj.IdActor, true);

            return operatorObj;
        }

        #endregion
        #region GetAll

        public static List<OpOperator> GetAll(DataProvider dataProvider, bool? isBlocked = null, bool? order = null)
        {
            List<OpOperator> operatorList = null;
            if (isBlocked.HasValue)
                operatorList = dataProvider.GetAllOperator().Where(o => o.IsBlocked == isBlocked.Value).ToList();
            else
                operatorList = dataProvider.GetAllOperator();

            if (order.HasValue)
                operatorList = operatorList.OrderBy(o => o.ComboBoxString).ToList();
            return operatorList;
        }
        #endregion
        #region GetFitters

        public static List<OpOperator> GetFitters(DataProvider dataProvider)
        {
            List<OpOperator> fitters = new List<OpOperator>();
            List<int> tmpOperatorIdList = new List<int>();

            List<OpRoleActivity> operatorRoleActivity = dataProvider.GetRoleActivityFilter(IdActivity: new int[] { Activity.OPERATOR_IMRSC_SOT }, loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted);
            if (operatorRoleActivity != null && operatorRoleActivity.Count > 0)
            {
                List<OpOperatorRole> operatorInFitterRole = dataProvider.GetOperatorRoleFilter(IdRole: operatorRoleActivity.Select(r => r.IdRole).Distinct().ToArray(), loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted);
                if (operatorInFitterRole != null && operatorInFitterRole.Count > 0)
                    tmpOperatorIdList.AddRange(operatorInFitterRole.Select(d => d.IdOperator).Distinct());
            }
            List<OpOperatorActivity> operatorWithFitterActivity = dataProvider.GetOperatorActivityFilter(IdActivity: new int[] { Activity.OPERATOR_IMRSC_SOT }, loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted);
            if (operatorWithFitterActivity != null && operatorWithFitterActivity.Count(o => o.IdOperator.HasValue && !o.Deny) > 0)
                tmpOperatorIdList.AddRange(operatorWithFitterActivity.Where(o => o.IdOperator.HasValue && !o.Deny).Select(o => o.IdOperator.Value).Distinct());
            tmpOperatorIdList = tmpOperatorIdList.Distinct().ToList();

            if (tmpOperatorIdList.Count > 0)
            {
                fitters = dataProvider.GetOperator(tmpOperatorIdList.ToArray());
                fitters = fitters.Where(f => !f.IsBlocked && f.IdDistributor.In(dataProvider.DistributorFilter)).ToList();
            }

            return fitters;
        }
        #endregion

        #region GetOperatorWithCreation

        public static OpOperator GetOperatorWithCreation(IDataProvider dataProvider, string login, string password = null, int? idDistributor = null)
        {
            OpOperator retValue = null;
            retValue = dataProvider.GetOperatorFilter(Login: login, IdDistributor: new List<int>().ToArray()).FirstOrDefault();
            if (retValue == null)
            {
                OpOperator oper = new OpOperator();
                oper.Login = login;
                if (String.IsNullOrEmpty(password))
                    oper.PASSWORD = DataConverter.EncodePassword("aiutaiut");
                else
                    oper.PASSWORD = DataConverter.EncodePassword(password);
                oper.IsBlocked = false;
                if (idDistributor.HasValue)
                    oper.IdDistributor = idDistributor.Value;
                else
                    oper.IdDistributor = 1;
                oper.Actor = dataProvider.GetActorFilter(Name: login).FirstOrDefault();
                if (oper.Actor == null)
                {
                    oper.Actor = new OpActor();
                    oper.Actor.Name = login;
                    oper.Actor.IdLanguage = (int)Enums.Language.English;
                    oper.Actor.IdActor = dataProvider.SaveActor(oper.Actor);
                    oper.IdActor = oper.Actor.IdActor;
                }
                oper.IdOperator = dataProvider.SaveOperator(oper);
                retValue = oper;
            }
            return retValue;
        }

        #endregion

        #region GetImrServerOperator

        public static OpOperator GetImrServerOperator(IDataProvider dataProvider)
        {
            return GetOperatorWithCreation(dataProvider, "IMR Server");
        }

        #endregion

        #region GetFitterBorderCircleRange

        public static double GetFitterBorderCircleRange(OpOperator fitter)
        {
            double radius = 100.0;
            OpOperatorData fitterAreaRadius = fitter.DataList.Find(d => d.IdDataType == DataType.OPERATOR_FITTER_AREA_RADIUS);
            if (fitterAreaRadius != null && fitterAreaRadius.Value != null)
            {
                double radiusTmp = 0.0;
                if (double.TryParse(fitterAreaRadius.Value.ToString(), out radiusTmp))
                    radius = radiusTmp;
            }

            return radius;
        }

        #endregion

        #region GetGoogleMapXXX
        public static System.Tuple<double, double> GetGoogleMapCoordinate(DataProvider dataProvider, OpOperator opOperator)
        {
            System.Tuple<double, double> coords = System.Tuple.Create<double, double>(Consts.MapLatitudeAIUT, Consts.MapLongitudeAIUT);

            double? latitude = opOperator.DataList.TryGetNullableValue<double>(DataType.GOOGLE_MAP_LATITUDE);
            double? longitude = opOperator.DataList.TryGetNullableValue<double>(DataType.GOOGLE_MAP_LONGITUDE);
            if (latitude != null && longitude != null)
            {
                coords = System.Tuple.Create<double, double>(latitude.Value, longitude.Value);
            }
            else if (opOperator.Distributor != null)
            {
                latitude = opOperator.Distributor.DataList.TryGetNullableValue<double>(DataType.GOOGLE_MAP_LATITUDE);
                longitude = opOperator.Distributor.DataList.TryGetNullableValue<double>(DataType.GOOGLE_MAP_LONGITUDE);
                if (latitude != null && longitude != null)
                    coords = System.Tuple.Create<double, double>(latitude.Value, longitude.Value);
            }
            return coords;
        }

        public static int GetGoogleMapZoomLevel(DataProvider dataProvider, OpOperator opOperator)
        {
            int zoomLevel = Consts.MapZoomAIUT;

            int? zl = opOperator.DataList.TryGetNullableValue<int>(DataType.GOOGLE_MAP_ZOOM);
            if (zl != null)
            {
                zoomLevel = zl.Value;
            }
            else if (opOperator.Distributor != null)
            {
                zl = opOperator.Distributor.DataList.TryGetNullableValue<int>(DataType.GOOGLE_MAP_ZOOM);
                if (zl != null)
                    zoomLevel = zl.Value;
            }
            return zoomLevel;
        }

        public static string GetGoogleMapUrl(DataProvider dataProvider, OpOperator opOperator, Enums.Module module)
        {
            string url = opOperator.DataList.TryGetValue<string>(DataType.GOOGLE_MAP_URL);
            if (!string.IsNullOrEmpty(url))
                return url;

            url = opOperator.Distributor.DataList.TryGetValue<string>(DataType.GOOGLE_MAP_URL);
            if (!string.IsNullOrEmpty(url))
                return url;

            OpModule mod = dataProvider.GetModule((int)module);
            if (mod != null)
            {
                url = mod.DataList.TryGetValue<string>(DataType.GOOGLE_MAP_URL);
                if (!string.IsNullOrEmpty(url))
                    return url;

            }

            return string.Empty;
        }
        #endregion

        #region GetProfileData
        public static List<OpProfileData> GetProfileData(DataProvider dataProvider, OpOperator opOperator, Enums.Module? module = null)
        {
            List<OpProfileData> opProfileData = new List<OpProfileData>();
            int? idProfile = opOperator.DataList.TryGetNullableValue<int>(DataType.PROFILE_ID);
            if (idProfile == null && opOperator.Distributor != null)
                idProfile = opOperator.Distributor.DataList.TryGetNullableValue<int>(DataType.PROFILE_ID);
            if (idProfile != null)
            {
                OpProfile opProfile;
                if (module != null)
                    opProfile = dataProvider.GetProfileFilter(IdProfile: new int[] { idProfile.Value }, IdModule: new int[] { (int)module.Value }).FirstOrDefault();
                else
                    opProfile = dataProvider.GetProfileFilter(IdProfile: new int[] { idProfile.Value }).FirstOrDefault();

                if (opProfile != null)
                    opProfileData = opProfile.Parameters;
            }
            return opProfileData;
        }
        #endregion

        #region GetOperatorByRDTNEmployeeId

        /// <summary>
        /// Zwraca operatora z IMR SC który jest połączony z użytkownikiem TMQ Studio o podanym Id.
        /// </summary>
        /// <param name="dataProvider">Data provider.</param>
        /// <param name="RDTNEmployeeId">Id użytkownika TMQ Studio z bazy RadioDeviceTesterNew.</param>
        /// <returns>Obiekt operatora lub null jeżeli nie można go znaleźć.</returns>
        public static OpOperator GetOperatorByRDTNEmployeeId(DataProvider dataProvider, int RDTNEmployeeId)
        {
            List<OpOperatorData> opDataList = dataProvider.GetOperatorDataFilter(IdDataType: new long[] { DataType.OPERATOR_RADIO_DEVICE_TESTER_NEW_ID });
            OpOperatorData opData = opDataList.Find(x => x.GetValue<int>() == RDTNEmployeeId);

            return (opData == null) ? null : dataProvider.GetOperator(opData.IdOperator);
        }

        #endregion

        #region GetMappedIdOperator

        public static int? GetMappedIdOperator(DataProvider dataProvider, OpOperator operatorToMap, int idServer)
        {
            bool getIdFromDb = true;
            int? idOperatorOnServer = null;

            long[] operatorIdOnServerDataTypes = new long[] { DataType.OPERATOR_IMR_SERVER_OPERATOR_ID, DataType.OPERATOR_IMR_SERVER_ID };

            IMRLog.AddToLog(Module, false, EventID.Operator.TryingToMapOperator, operatorToMap.Login, "Checking operator mappings for server: " + idServer);

            getIdFromDb = operatorToMap.DataList.Where(x => x.IdDataType.In(operatorIdOnServerDataTypes)).Count() == 0; // Sprawdzamy czy w ogole sa te datatypy
            if (!getIdFromDb) // Jakies datatypy sa, probujemy sprawdzic czy to te ktorych szukamy
            {
                // Sprawdzamy czy id servera akcji i operatora sie zgadzaja
                OpOperatorData serverId = operatorToMap.DataList.FirstOrDefault(x => x.IdDataType == DataType.OPERATOR_IMR_SERVER_ID && Convert.ToInt32(x.Value) == idServer);
                if (serverId != null)
                {
                    // Sprawdzamy czy mamy id on server operatora
                    OpOperatorData idOnServer = operatorToMap.DataList.FirstOrDefault(x => x.IdDataType == DataType.OPERATOR_IMR_SERVER_OPERATOR_ID && x.Index == serverId.Index);
                    if (idOnServer != null)
                    {
                        getIdFromDb = false;
                        idOperatorOnServer = Convert.ToInt32(idOnServer.Value);
                    }
                    else
                    {
                        getIdFromDb = true;
                    }
                }
                else
                {
                    getIdFromDb = true;
                }
            }

            if (getIdFromDb) // Musimy pobrac z bazy, smutek
            {
                IMRLog.AddToLog(Module, false, EventID.Operator.TryingToMapOperator, operatorToMap.Login, "Trying to get operator mappings for server: " + idServer);

                List<OpOperatorData> idOnServerData = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdOperator: new int[] { operatorToMap.IdOperator }, IdDataType: operatorIdOnServerDataTypes);

                // Sprawdzamy czy operator "teoretycznie" istnieje na serwerze
                OpOperatorData serverId = idOnServerData.FirstOrDefault(x => x.IdDataType == DataType.OPERATOR_IMR_SERVER_ID && Convert.ToInt32(x.Value) == idServer);
                if (serverId != null)
                {
                    // Pobieramy jego id na serwerze
                    OpOperatorData idOnServer = idOnServerData.FirstOrDefault(x => x.IdDataType == DataType.OPERATOR_IMR_SERVER_OPERATOR_ID && x.Index == serverId.Index);

                    if (idOnServer != null)
                    {
                        idOperatorOnServer = Convert.ToInt32(idOnServer.Value);
                    }
                }
            }

            return idOperatorOnServer;
        }

        #endregion

        #region GetOperatorData
        public static List<T> GetOperatorData<T>(DataProvider dataProvider, OpOperator opOperator, long idDataType, bool tryGetFromDistributor = true, bool tryQueryDatabase = true) where T : struct, IConvertible
        {
            List<T> list = new List<T>();
            List<OpOperatorData> operatorData = opOperator.DataList.Where(q => q.Value != null && q.IdDataType == idDataType).ToList();
            if (operatorData.Count == 0 && tryQueryDatabase)
            {
                operatorData = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, IdOperator: new int[] { opOperator.IdOperator },
                    IdDataType: new long[] { idDataType }, IdDistributor: new int[0],
                    customWhereClause: "[VALUE] IS NOT NULL ");
            }

            if (operatorData.Count > 0)
            {
                foreach (OpOperatorData data in operatorData)
                {
                    T? value = data.TryGetNullableValue<T>();
                    if (value.HasValue)
                        list.Add(value.Value);
                }
                return list;
            }

            if (tryGetFromDistributor && opOperator.Distributor != null)
            {
                List<OpDistributorData> distributorData = opOperator.Distributor.DataList.Where(q => q.Value != null && q.IdDataType == idDataType).ToList();
                if (distributorData.Count == 0 && tryQueryDatabase)
                {
                    distributorData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, IdDistributor: new int[] { opOperator.Distributor.IdDistributor },
                        IdDataType: new long[] { idDataType }, customWhereClause: "[VALUE] IS NOT NULL ");
                }

                foreach (OpDistributorData data in distributorData)
                {
                    T? value = data.TryGetNullableValue<T>();
                    if (value.HasValue)
                        list.Add(value.Value);
                }
            }

            return list;
        }

        public static List<IOpData> GetOperatorData(DataProvider dataProvider, OpOperator opOperator, long[] idDataTypes, bool tryGetFromDistributor = true, bool tryQueryDatabase = true)
        {
            if (idDataTypes == null || idDataTypes.Length == 0) return new List<IOpData>();

            List<IOpData> list = new List<IOpData>();
            List<OpOperatorData> operatorData = opOperator.DataList.Where(q => q.Value != null && q.IdDataType.In(idDataTypes)).ToList();
            List<OpDistributorData> distributorData = new List<OpDistributorData>();
            if (tryQueryDatabase)
            {
                List<long> idDataTypeNotInDataList = idDataTypes.Where(q => !operatorData.Any(w => w.IdDataType == q)).ToList();
                if (idDataTypeNotInDataList.Count > 0)
                {
                    operatorData.AddRange(dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdOperator: new int[] { opOperator.IdOperator }, IdDataType: idDataTypeNotInDataList.ToArray(), IdDistributor: new int[0]));
                }
            }

            if (tryGetFromDistributor && opOperator.Distributor != null)
            {
                List<long> idDataTypeNotInDataList = idDataTypes.Where(q => !operatorData.Any(w => w.IdDataType == q)).ToList();
                if (idDataTypeNotInDataList.Count > 0)
                {
                    distributorData.AddRange(opOperator.Distributor.DataList.Where(q => q.Value != null && idDataTypeNotInDataList.Contains(q.IdDataType)));
                    idDataTypeNotInDataList = idDataTypes.Where(q => !distributorData.Any(w => w.IdDataType == q)).ToList();
                    if (idDataTypeNotInDataList.Count > 0)
                    {
                        distributorData.AddRange(dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                            IdDistributor: new int[] { opOperator.Distributor.IdDistributor }, IdDataType: idDataTypeNotInDataList.ToArray()));
                    }
                }
            }

            foreach (long idDataType in idDataTypes.Distinct())
            {
                if (operatorData.Any(q => q.IdDataType == idDataType))
                    list.AddRange(operatorData.Where(q => q.IdDataType == idDataType));
                else if (distributorData.Any(q => q.IdDataType == idDataType))
                    list.AddRange(distributorData.Where(q => q.IdDataType == idDataType));
            }

            return list;
        }
        #endregion

        #endregion

        #region Save

        #region Save
        /// <summary>
        /// Saves Operator with some advanced options.
        /// </summary>
        /// <param name="dataProvider">DataProvider object.</param>
        /// <param name="objectToSave">OpOperator object to be saved.</param>
        /// <param name="addToDefaultAddressBook"></param>
        /// <param name="saveOperatorData">Indicates if list of OperatorData should be saved.</param>
        /// <param name="setOperatorDataIdDistributor">Indicates if IdDistributor of saved OpOperator object should be set in OpOperatorData objects.</param>
        /// <param name="generatePasswordSalt">Indicates if password salt should be generated if configured in Database.</param>
        /// <param name="loadNavigationProperties">Indicates if navigation properties should be loaded.</param>
        /// <param name="loggedOperator">OpOperator object of Operator logged in aplication. Ugly solution added for FuelPrime.</param>
        /// <param name="applicationModule">Id module of application that calls this method. Ugly solution added for FuelPrime.</param>
        /// <returns>Saved Operator.</returns>
        public static OpOperator Save(DataProvider dataProvider, OpOperator objectToSave, bool addToDefaultAddressBook = false, bool saveOperatorData = true, bool setOperatorDataIdDistributor = true, bool generatePasswordSalt = true, bool loadNavigationProperties = true,
            OpOperator loggedOperator = null, int? applicationModule = null)
        {
            bool isNewItem = objectToSave.IdOperator == 0; //check if it's new operator

            try
            {
                int actorId = dataProvider.SaveActor(objectToSave.Actor);
                if (actorId != null)
                {
                    OpActor opActorFromDB = dataProvider.GetActor(actorId);
                    if (opActorFromDB != null)
                    {
                        objectToSave.Actor = opActorFromDB;
                    }
                }
                if (isNewItem && objectToSave.Actor.IdActor > 0 && addToDefaultAddressBook)
                {
                    OpActorGroup builtInActorGroup = ActorGroupComponent.GetBuiltInForDistributor(dataProvider, objectToSave.Distributor);
                    if (builtInActorGroup == null)
                    {
                        OpDistributor distributor = null;
                        if (objectToSave.Distributor != null)
                            distributor = objectToSave.Distributor;
                        else
                            distributor = dataProvider.GetDistributor(objectToSave.IdDistributor);
                        builtInActorGroup = new OpActorGroup();
                        builtInActorGroup.BuiltIn = true;
                        builtInActorGroup.Distributor = distributor;
                        builtInActorGroup.Name = String.Format("{0} {1}", distributor.ToString(), "Address Book");
                        builtInActorGroup.OpState = OpChangeState.New;
                        builtInActorGroup.IdActorGroup = dataProvider.SaveActorGroup(builtInActorGroup);
                    }
                    if (builtInActorGroup != null)
                        ActorComponent.AddToGroup(dataProvider, objectToSave.Actor, builtInActorGroup);
                }
                //else if (objectToSave.Actor.IdActor > 0 && addToDefaultAddressBook)
                //{
                //    //Jeśli chodzi o zmiane operatora to dodac tam ifa, sprawdzić czy zmienił się dystr, usunąć ze starej grupy i dodać do nowej?
                //    //Pytanie: Co jeśli nowy dystrybutor nie ma grupy, zostawić w starej czy nie dodawać?

                //    OpOperator oldOperator = dataProvider.GetOperator(objectToSave.IdOperator);

                //    if (oldOperator != null && oldOperator.IdDistributor != objectToSave.IdDistributor)
                //    {
                //        OpActorGroup oldBuiltInActorGroup = ActorGroupComponent.GetBuiltInForDistributor(dataProvider, oldOperator.Distributor);
                //        OpActorGroup newBuiltInActorGroup = ActorGroupComponent.GetBuiltInForDistributor(dataProvider, objectToSave.Distributor);

                //        if (oldBuiltInActorGroup != null)
                //        {
                //            OpActorInGroup actorInGroup = dataProvider.GetActorInGroupFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                //                                                                         IdActor: new int[] { objectToSave.Actor.IdActor }, IdActorGroup: new int[] { oldBuiltInActorGroup.IdActorGroup }).FirstOrDefault();
                //            if (actorInGroup != null)
                //            {
                //                dataProvider.DeleteActorInGroup(actorInGroup);
                //            }
                //        }

                //        if (newBuiltInActorGroup != null)
                //        {
                //            ActorComponent.AddToGroup(dataProvider, objectToSave.Actor, newBuiltInActorGroup);
                //        }
                //    }
                //}
                objectToSave.IdActor = objectToSave.Actor.IdActor;
                objectToSave.IdDistributor = objectToSave.Distributor.IdDistributor;

                List<long> availableDataTypes = dataProvider.GetDataType(new long[] { DataType.OPERATOR_PASSWORD_SALT, DataType.OPERATOR_PASSWORD_CREATION_TIME }).Select(q => q.IdDataType).ToList();

                bool tryToGenerateSalt = false;
                bool tryCreationTime = false;
                bool deletePasswordSalt = false;
                List<OpOperatorData> operatorDataList = new List<OpOperatorData>();
                List<long> operatorIdDataTypeList = new List<long>();
                if (generatePasswordSalt
                    && availableDataTypes.Contains(DataType.OPERATOR_PASSWORD_SALT)
                    && !String.IsNullOrEmpty(objectToSave.Password)
                    && objectToSave.NewPasswordSet)
                {
                    operatorIdDataTypeList.Add(DataType.OPERATOR_PASSWORD_SALT);
                    tryToGenerateSalt = true;
                }

                if (availableDataTypes.Contains(DataType.OPERATOR_PASSWORD_CREATION_TIME)
                    && objectToSave.NewPasswordSet)
                {
                    operatorIdDataTypeList.Add(DataType.OPERATOR_PASSWORD_CREATION_TIME);
                    tryCreationTime = true;
                }

                string salt = null;

                // 1. dostajemy na wejsciu md5(password)
                // 2. sprawdzamy czy generowac salt
                //      2a. jak ma byc wygenerowane to sprawdzamy czy zostalo ustawione nowe haslo - jesli nie zostalo ustawione nowe haslo to nic wiecej nie robimy z salt!!! bo moze sie okazac, ze haslo na wejsciu zawiera juz salt!!! a nie mozemy wygenerowac nowego hasla jako MD5(md5(md5(password)+salt)+SALT)
                //      2b. jesli bylo nowe haslo to robimy nowe haslo MD5(md5(password)+SALT)
                if (tryToGenerateSalt)
                {
                    bool? generateSalt = null;
                    OpDataList<OpDistributorData> distributorDataList = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDistributor: new int[] { objectToSave.IdDistributor }, IdDataType: new long[] { DataType.GENERATE_OPERATOR_PASSWORD_SALT }, customWhereClause: "[VALUE] IS NOT NULL ").ToOpDataList();
                    generateSalt = distributorDataList.TryGetNullableValue<bool>(DataType.GENERATE_OPERATOR_PASSWORD_SALT);
                    if (!generateSalt.HasValue)
                    {
                        OpDataList<OpSystemData> systemDataList = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                            IdDataType: new long[] { DataType.GENERATE_OPERATOR_PASSWORD_SALT }).ToOpDataList();
                        generateSalt = systemDataList.TryGetNullableValue<bool>(DataType.GENERATE_OPERATOR_PASSWORD_SALT);
                    }
                    if (generateSalt.HasValue && generateSalt.Value)
                    {
                        Random rand = new Random();
                        string pool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        salt = new string(Enumerable.Repeat(pool, 16).Select(q => q[rand.Next(q.Length)]).ToArray());
                        objectToSave.NewPassword = String.Concat(objectToSave.Password, salt);
                    }
                    else if (!generateSalt.HasValue || !generateSalt.Value)
                        deletePasswordSalt = true;
                    objectToSave.NewPasswordSet = false;
                }

                dataProvider.SaveOperator(objectToSave);

                if (operatorIdDataTypeList.Count > 0)
                    operatorDataList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdOperator: new int[] { objectToSave.IdOperator }, IdDistributor: new int[0], IdDataType: operatorIdDataTypeList.ToArray());

                if (!String.IsNullOrEmpty(salt))
                {
                    List<OpOperatorData> saltOperatorData = new List<OpOperatorData>();
                    if (!isNewItem)
                        saltOperatorData = operatorDataList.Where(q => q.IdDataType == DataType.OPERATOR_PASSWORD_SALT).ToList();
                    if (saltOperatorData.Count == 0)
                        saltOperatorData.Add(new OpOperatorData()
                        {
                            IdOperatorData = 0,
                            IdOperator = objectToSave.IdOperator,
                            IdDistributor = null,
                            IdDataType = DataType.OPERATOR_PASSWORD_SALT,
                            Index = 0
                        });
                    foreach (OpOperatorData saltData in saltOperatorData)
                    {
                        saltData.Value = salt;
                        long? idOperatorData = null;
                        TrySaveOperatorData(dataProvider, saltData, 0, ref idOperatorData);
                    }
                }
                if (deletePasswordSalt)
                {
                    foreach (OpOperatorData saltData in operatorDataList.Where(q => q.IdDataType == DataType.OPERATOR_PASSWORD_SALT))
                    {
                        TryDeleteOperatorData(dataProvider, saltData, 0);
                    }
                }

                if (tryCreationTime)
                {
                    List<OpOperatorData> timeOperatorData = new List<OpOperatorData>();
                    if (!isNewItem)
                        timeOperatorData = operatorDataList.Where(q => q.IdDataType == DataType.OPERATOR_PASSWORD_CREATION_TIME).ToList();
                    if (timeOperatorData.Count == 0)
                        timeOperatorData.Add(new OpOperatorData()
                        {
                            IdOperatorData = 0,
                            IdOperator = objectToSave.IdOperator,
                            IdDistributor = null,
                            IdDataType = DataType.OPERATOR_PASSWORD_CREATION_TIME,
                            Index = 0
                        });
                    DateTime utcNow = dataProvider.DateTimeUtcNow;
                    foreach (OpOperatorData timeData in timeOperatorData)
                    {
                        timeData.Value = utcNow;
                        long? idOperatorData = null;
                        TrySaveOperatorData(dataProvider, timeData, 0, ref idOperatorData);
                    }
                }

                if (saveOperatorData)
                {
                    //zapis do tabeli OPERATOR_DATA
                    OpOperatorData operatorData = null;
                    //uniknięcie duplikatów w OPERATOR_DATA dla DataType.OPERATOR_PASSWORD_CREATION_TIME i DataType.OPERATOR_PASSWORD_SALT
                    objectToSave.DataList.RemoveAll(q=>q.IdDataType == DataType.OPERATOR_PASSWORD_CREATION_TIME || q.IdDataType == DataType.OPERATOR_PASSWORD_SALT);

                    for (int i = 0; i < objectToSave.DataList.Count; i++)
                    {
                        operatorData = objectToSave.DataList[i];
                        operatorData.IdOperator = objectToSave.IdOperator;
                        if (setOperatorDataIdDistributor)
                            operatorData.IdDistributor = objectToSave.IdDistributor;
                        if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                        {
                            DataComponent.Delete(dataProvider, operatorData);
                            objectToSave.DataList.Remove(operatorData, true);
                            if (i >= 0)
                                i--;
                        }
                        else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                        {
                            if (operatorData.Distributor == null)
                                operatorData.Distributor = objectToSave.Distributor;
                            objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, operatorData);

                            if (objectToSave.DataList[i].OpState == OpChangeState.New)
                                objectToSave.DataList[i].AssignReferences(dataProvider);

                            objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                        }
                    }

                    if (objectToSave.DepositoryLocation != null && objectToSave.DepositoryLocation.DataList.Count > 0)
                    {
                        OpData data = null;
                        for (int i = 0; i < objectToSave.DepositoryLocation.DataList.Count; i++)
                        {
                            data = objectToSave.DepositoryLocation.DataList[i];
                            data.IdLocation = objectToSave.IdDepositoryLocation;

                            if (objectToSave.DepositoryLocation.DataList[i].OpState == OpChangeState.Delete)
                            {
                                DataComponent.Delete(dataProvider, data);
                                objectToSave.DepositoryLocation.DataList.Remove(data, true);
                                if (i >= 0)
                                    i--;
                            }
                            else if (objectToSave.DepositoryLocation.DataList[i].OpState == OpChangeState.Modified || objectToSave.DepositoryLocation.DataList[i].OpState == OpChangeState.New)
                            {
                                objectToSave.DepositoryLocation.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                                if (objectToSave.DepositoryLocation.DataList[i].OpState == OpChangeState.New)
                                    objectToSave.DepositoryLocation.DataList[i].AssignReferences(dataProvider);
                                objectToSave.DepositoryLocation.DataList[i].OpState = OpChangeState.Loaded;
                            }
                        }
                    }
                }
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.OperatorAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdOperator);
                        LogSuccess(EventID.Forms.OperatorAdded, objectToSave.IdOperator);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.OperatorSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdOperator);
                        LogSuccess(EventID.Forms.OperatorSaved, objectToSave.IdOperator);
                }

                
                if (loggedOperator != null &&  applicationModule.HasValue)
                {
                    OpActivity activity = loggedOperator.Activities.Where(a => a.IdActivity == Activity.OPERATOR_LDAP_SYNCHRONIZATION_ACTION_TYPE && a.ReferenceValue != null && a.Allow).FirstOrDefault();
                    if (activity != null)
                    {
                        List<DataValue> data = new List<DataValue>()
                        {
                            new DataValue(DataType.ACTION_OPERATOR_LDAP_SYNCHRONIZATION, 0, 1),
                            new DataValue(DataType.OPERATOR_IMR_SERVER_OPERATOR_LOGIN, 0, objectToSave.Login),
                            new DataValue(DataType.OPERATOR_IMR_SERVER_OPERATOR_PASSWORD, 0, objectToSave.CryptedPassword)
                        };

                        int refVal = GenericConverter.Parse<int>(activity.ReferenceValue, 0);
                        if (refVal > 0)
                        {
                            OpAction action;
                            Exception ex;
                            ActionComponent.SaveAndRunAction(
                                dataProvider: dataProvider,
                                idActionType: refVal,
                                idActionStatus: (int)Enums.ActionStatus.New,
                                idModule: applicationModule.Value,
                                idOperator: loggedOperator.IdOperator,
                                dataValues: data,
                                idActionData: null,
                                idMeter: null,
                                idLocation: null,
                                serialNbr: Consts.SystemSerialNumber,
                                queuePath: null,
                                action: out action,
                                exception: out ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.OperatorAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.OperatorAdditionError, ex.Message);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.OperatorSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdOperator, ex.Message);
                        LogError(EventID.Forms.OperatorSavingError, objectToSave.IdOperator, ex.Message);
                }
                throw ex;
            }
            if (loadNavigationProperties)
                return dataProvider.GetOperator(objectToSave.IdOperator);
            else
                return objectToSave;
        }
        #endregion

        #region TrySaveOperator
        public static bool TrySaveOperator(DataProvider dataProvider, OpOperator opOperator, int retry, ref int? idOperator)
        {
            bool ret = false;
            try
            {
                if (retry < 3)
                {
                    idOperator = dataProvider.SaveOperator(opOperator);
                    ret = true;
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                ret = TrySaveOperator(dataProvider, opOperator, retry + 1, ref idOperator);
            }
            return ret;
        }
        #endregion
        #region TrySaveOperatorData
        public static bool TrySaveOperatorData(DataProvider dataProvider, OpOperatorData opOperatorData, int retry, ref long? idOperatorData)
        {
            bool ret = false;
            try
            {
                if (retry < 3)
                {
                    idOperatorData = dataProvider.SaveOperatorData(opOperatorData);
                    ret = true;
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                ret = TrySaveOperatorData(dataProvider, opOperatorData, retry + 1, ref idOperatorData);
            }
            return ret;
        }
        #endregion

        #endregion

        #region Delete

        #region DeleteData
        public static void DeleteData(DataProvider dataProvider, int idOperator)
        {
            List<OpOperatorData> listaOp = dataProvider.GetOperatorDataFilter(IdOperator: new int[] { idOperator }, IdDataType: DataProvider.OperatorDataTypes.ToArray());
            foreach (var item in dataProvider.GetOperatorDataFilter(IdOperator: new int[1] { idOperator }, IdDataType: DataProvider.OperatorDataTypes.ToArray()))
            {
                dataProvider.DeleteOperatorData(item);
            }
        }
        #endregion

        #region DeleteOperatorActivity
        public static void DeleteOperatorActivity(DataProvider dataProvider, int idOperator)
        {
            List<OpOperatorActivity> activityList = dataProvider.GetOperatorActivityFilter(IdOperator: new int[] { idOperator });
            foreach (OpOperatorActivity activity in activityList)
            {
                dataProvider.DeleteOperatorActivity(activity);
            }
        }
        #endregion

        #region Delete
        public static void Delete(DataProvider dataProvider, OpOperator objectToDelete)
        {
            try
            {
                DeleteData(dataProvider, objectToDelete.IdOperator);
                DeleteOperatorActivity(dataProvider, objectToDelete.IdOperator);
                dataProvider.DeleteOperator(objectToDelete);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.OperatorDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdOperator);
                    LogSuccess(EventID.Forms.OperatorDeleted, objectToDelete.IdOperator);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.OperatorDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdOperator, ex.Message);
                    LogError(EventID.Forms.OperatorDeletionError, objectToDelete.IdOperator, ex.Message);
                throw ex;
            }
            if (objectToDelete.Actor != null)
                ActorComponent.Delete(dataProvider, objectToDelete.Actor);
        }
        #endregion

        #region TryDeleteOperatorData
        public static bool TryDeleteOperatorData(DataProvider dataProvider, OpOperatorData opOperatorData, int retry)
        {
            bool ret = false;
            try
            {
                if (retry < 3)
                {
                    dataProvider.DeleteOperatorData(opOperatorData);
                    ret = true;
                }
            }
            catch
            {
                System.Threading.Thread.Sleep(500);
                ret = TryDeleteOperatorData(dataProvider, opOperatorData, retry + 1);
            }
            return ret;
        }
        #endregion

        #endregion

        #endregion

        #region Login | Authenticate

        #region Login
        [Obsolete("Use more complex method Login", false)]
        public static int? Login(DataProvider dataProvider, string UserName, string Password, bool hasedPassword = false)
        {
            string PassHash = Password;
            if (!hasedPassword)
                PassHash = DataConverter.EncodePassword(Password);
            return dataProvider.dbConnectionCore.LogInUser(UserName, PassHash);
        }

        /// <summary>
        /// Advanced Login method.
        /// </summary>
        /// <param name="dataProvider">Instance of DataProvider</param>
        /// <param name="userName">User login</param>
        /// <param name="password">User password</param>
        /// <param name="hashedPassword">Indicates if user password is already hashed</param>
        /// <param name="idModule">Module id</param>
        /// <param name="idSession">Session id if new entry was added to Session</param>
        /// <param name="idOperator">Operator id if Operator was found</param>
        /// <param name="permissionsToCheck">List of permissions to validate, Tuple: int - IdActivity, object - reference value, bool - default value to return if permission is not defined</param>
        /// <param name="checkValidity">Indicates if Operator account validity times should be checked and validated</param>
        /// <param name="checkBlockedOperator">Indicates if blocked time should be checked for blocked Operator and validated</param>
        /// <param name="checkFailedLogin">Indicates if failed login operation retries should be checked and validated</param>
        /// <param name="checkPasswordSalt">Indicates if Operator password salt should be checked and validated</param>
        /// <param name="checkLogInSession">Indicates if logging in Session should be checked and validated</param>
        /// <returns>Status of login operation.</returns>
        public static Enums.OperatorLoginStatus Login(DataProvider dataProvider, string userName, string password, bool hashedPassword,
            int idModule, ref long? idSession, ref int? idOperator,
            List<Tuple<int, object, bool>> permissionsToCheck,
            bool checkValidity = true, bool checkBlockedOperator = true, bool checkFailedLogin = true, bool checkPasswordSalt = true, bool checkLogInSession = true)
        {
            if (String.IsNullOrWhiteSpace(userName)) return Enums.OperatorLoginStatus.Unknown;

            Enums.OperatorLoginStatus result = Enums.OperatorLoginStatus.Unknown;

            List<long> customDataTypes = new List<long>();
            if (checkValidity)
            {
                customDataTypes.Add(DataType.OPERATOR_VALIDITY_FROM_TIME);
                customDataTypes.Add(DataType.OPERATOR_VALIDITY_TO_TIME);
            }
            if (checkBlockedOperator)
                customDataTypes.Add(DataType.OPERATOR_BLOCKED_TO_TIME);
            if (checkPasswordSalt)
                customDataTypes.Add(DataType.OPERATOR_PASSWORD_SALT);
            if (checkFailedLogin)
            {
                customDataTypes.Add(DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME);
                customDataTypes.Add(DataType.OPERATOR_BLOCKED_RETRY_TIME);
            }

            List<OpOperator> operatorList = dataProvider.GetOperatorFilter(loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false,
                autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                IdDistributor: new int[0],
                customWhereClause: String.Format(@" [LOGIN] = '{0}' ", userName));

            if (operatorList.Count == 0) // nie znaleziono loginu
                return Enums.OperatorLoginStatus.Unknown;

            if (operatorList.Count > 1) // znaleziono wiecej takich loginow
            {
                if (operatorList.Any(q => q.IsBlocked)) //q.Password == passHash && !q.IsBlocked))
                    operatorList = operatorList.Where(q => q.IsBlocked).ToList(); //q.Password == passHash && q.IsBlocked).ToList();
                else
                    operatorList = operatorList.Where(q => !q.IsBlocked).ToList(); //q.Password == passHash && !q.IsBlocked).ToList();
            }
            OpOperator opOperator = operatorList.FirstOrDefault();
            if (opOperator == null)
                return Enums.OperatorLoginStatus.Unknown;

            if (customDataTypes.Count > 0)
            {
                foreach (OpOperatorData opOperatorData in dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdOperator: new int[] { opOperator.IdOperator }, IdDistributor: new int[0],
                    IdDataType: customDataTypes.ToArray(), IndexNbr: new int[0]))
                {
                    opOperator.DataList.Add(opOperatorData);
                }
            }

            string passHash = password;
            if (password != null && !hashedPassword)
                passHash = DataConverter.EncodePassword(password);

            if (checkPasswordSalt)
            {
                string passwordSalt = opOperator.DataList.TryGetValue<string>(DataType.OPERATOR_PASSWORD_SALT);
                if (!String.IsNullOrEmpty(passwordSalt))
                    passHash = DataConverter.EncodePassword(String.Concat(passHash, passwordSalt));
            }

            int? retryCount = null;
            int? blockTime = null;
            int? retryTime = null;
            int? retryBlockedCount = null;
            bool? logInSession = null;
            List<long> systemDataCustomDataTypes = new List<long>();
            if (checkFailedLogin)
            {
                OpDataList<OpDistributorData> distributorDataList = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdDistributor: new int[] { opOperator.IdDistributor },
                    IdDataType: new long[] { DataType.FAILED_LOGIN_BLOCK_TIME, DataType.FAILED_LOGIN_RETRY_CHECK_TIME, DataType.FAILED_LOGIN_RETRY_COUNT, DataType.FAILED_LOGIN_BLOCKED_TO_TIME_RETRY_COUNT }, IndexNbr: new int[] { 0 }).ToOpDataList();
                retryCount = distributorDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_RETRY_COUNT, 0);
                blockTime = distributorDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_BLOCK_TIME, 0);
                retryTime = distributorDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_RETRY_CHECK_TIME, 0);
                retryBlockedCount = distributorDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_BLOCKED_TO_TIME_RETRY_COUNT, 0);
                if (!retryCount.HasValue)
                    systemDataCustomDataTypes.Add(DataType.FAILED_LOGIN_RETRY_COUNT);
                if (!blockTime.HasValue)
                    systemDataCustomDataTypes.Add(DataType.FAILED_LOGIN_BLOCK_TIME);
                if (!retryTime.HasValue)
                    systemDataCustomDataTypes.Add(DataType.FAILED_LOGIN_RETRY_CHECK_TIME);
                if (!retryBlockedCount.HasValue)
                    systemDataCustomDataTypes.Add(DataType.FAILED_LOGIN_BLOCKED_TO_TIME_RETRY_COUNT);
            }

            if (checkLogInSession)
            {
                OpDataList<OpModuleData> moduleDataList = dataProvider.GetModuleDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdModule: new int[] { idModule }, IdDataType: new long[] { DataType.LOG_OPERATOR_LOGIN_IN_SESSION }).ToOpDataList();
                logInSession = moduleDataList.TryGetNullableValue<bool>(DataType.LOG_OPERATOR_LOGIN_IN_SESSION, 0);
                if (!logInSession.HasValue)
                    systemDataCustomDataTypes.Add(DataType.LOG_OPERATOR_LOGIN_IN_SESSION);
            }

            if (systemDataCustomDataTypes.Count > 0)
            {
                OpDataList<OpSystemData> systemDataList = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdDataType: systemDataCustomDataTypes.ToArray(), IndexNbr: new int[] { 0 }).ToOpDataList();
                if (!retryCount.HasValue && checkFailedLogin)
                    retryCount = systemDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_RETRY_COUNT);
                if (!blockTime.HasValue && checkFailedLogin)
                    blockTime = systemDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_BLOCK_TIME);
                if (!retryTime.HasValue && checkFailedLogin)
                    retryTime = systemDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_RETRY_CHECK_TIME);
                if (!retryBlockedCount.HasValue && checkFailedLogin)
                    retryBlockedCount = systemDataList.TryGetNullableValue<int>(DataType.FAILED_LOGIN_BLOCKED_TO_TIME_RETRY_COUNT);
                if (!logInSession.HasValue && checkLogInSession)
                    logInSession = systemDataList.TryGetNullableValue<bool>(DataType.LOG_OPERATOR_LOGIN_IN_SESSION);
            }

            DateTime utcNow = DateTime.UtcNow;

            idOperator = opOperator.IdOperator;

            bool accountValid = false;
            if (checkValidity)
            {
                DateTime? fromTime = opOperator.DataList.TryGetNullableValue<DateTime>(DataType.OPERATOR_VALIDITY_FROM_TIME, 0);
                DateTime? toTime = opOperator.DataList.TryGetNullableValue<DateTime>(DataType.OPERATOR_VALIDITY_TO_TIME, 0);
                if (fromTime.HasValue && !toTime.HasValue)
                    accountValid = utcNow >= fromTime.Value;
                else if (!fromTime.HasValue && toTime.HasValue)
                    accountValid = utcNow <= toTime.Value;
                else if (fromTime.HasValue && toTime.HasValue)
                    accountValid = utcNow >= fromTime.Value && utcNow <= toTime.Value;
                else
                    accountValid = true;
            }
            else
                accountValid = true;

            List<long> availableIdDataTypes = dataProvider.GetDataType(new long[] { DataType.OPERATOR_BLOCKED_TO_TIME, DataType.SESSION_TIME_STARTED, DataType.SESSION_OPERATOR_LOGIN_STATUS, DataType.SESSION_COMPUTER_NAME,
                DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME, DataType.OPERATOR_BLOCKED_RETRY_TIME }, false,
                loadNavigationProperties: false, mergeIntoCache: false).Select(q => q.IdDataType).ToList(); // lista data typow dostepnych w danej bazie, sprawdzana w miejscach gdzie jest zapis OpXXXData z tym data typem

            if (accountValid)
            {
                if (opOperator.IsBlocked && checkBlockedOperator)
                {
                    OpOperatorData opOperatorData = opOperator.DataList.FirstOrDefault(q => q.IdDataType == DataType.OPERATOR_BLOCKED_TO_TIME && q.Index == 0);
                    DateTime? blockToTime = opOperatorData != null ? opOperatorData.TryGetNullableValue<DateTime>() : null;
                    if (blockToTime.HasValue && blockToTime.Value < DateTime.UtcNow)
                    {
                        opOperator.IsBlocked = false;
                        opOperatorData.Value = null;
                        int? updatedIdOperator = null;
                        long? updatedIdOperatorData = null;
                        TrySaveOperator(dataProvider, opOperator, 0, ref updatedIdOperator);
                        if (availableIdDataTypes.Contains(DataType.OPERATOR_BLOCKED_TO_TIME)) // gdy na bazie nie ma tego data typu to proba zapisu wygeneruje blad klucza na ID_DATA_TYPE
                            TrySaveOperatorData(dataProvider, opOperatorData, 0, ref updatedIdOperatorData);
                    }
                    else if (blockToTime.HasValue)
                    {
                        int maxIndex = -1;
                        bool addRetryTime = true;
                        if (retryBlockedCount.HasValue && retryBlockedCount.Value > 0
                            && IsLoginRetriesExceeded(opOperator.DataList.Where(q => q.IdDataType == DataType.OPERATOR_BLOCKED_RETRY_TIME).ToList(), retryBlockedCount.Value, retryTime, out maxIndex))
                        {
                            opOperatorData.Value = null;
                            long? updatedIdOperatorData = null;
                            if (availableIdDataTypes.Contains(DataType.OPERATOR_BLOCKED_TO_TIME)) // gdy na bazie nie ma tego data typu to proba zapisu wygeneruje blad klucza na ID_DATA_TYPE
                                TrySaveOperatorData(dataProvider, opOperatorData, 0, ref updatedIdOperatorData);

                            foreach (OpOperatorData operatorData in opOperator.DataList.Where(q => q.IdDataType == DataType.OPERATOR_BLOCKED_RETRY_TIME))
                            {
                                TryDeleteOperatorData(dataProvider, operatorData, 0);
                            }
                            opOperator.DataList.RemoveAll(q => q.IdDataType == DataType.OPERATOR_BLOCKED_RETRY_TIME);
                            addRetryTime = false;
                        }
                        if (addRetryTime && availableIdDataTypes.Contains(DataType.OPERATOR_BLOCKED_RETRY_TIME))
                        {
                            OpOperatorData operatorData = new OpOperatorData();
                            operatorData.IdOperatorData = 0;
                            operatorData.IdOperator = opOperator.IdOperator;
                            operatorData.IdDataType = DataType.OPERATOR_BLOCKED_RETRY_TIME;
                            operatorData.Index = maxIndex + 1;
                            operatorData.IdDistributor = null;
                            operatorData.Value = DateTime.UtcNow;
                            long? insertedIdOperatorData = null;
                            TrySaveOperatorData(dataProvider, operatorData, 0, ref insertedIdOperatorData);
                        }
                    }
                }
                if (opOperator.IsBlocked)
                    result = Enums.OperatorLoginStatus.Blocked;
                else
                {
                    if (String.CompareOrdinal(opOperator.Password, passHash) != 0)
                    {
                        result = Enums.OperatorLoginStatus.InvalidPassword;
                        int maxIndex = -1;
                        if (retryCount.HasValue && retryCount.Value > 0)
                        {
                            bool addRetryTime = true;
                            if (IsLoginRetriesExceeded(opOperator.DataList.Where(q => q.IdDataType == DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME).ToList(), retryCount.Value, retryTime, out maxIndex))
                            {
                                OpOperatorData opOperatorData = opOperator.DataList.FirstOrDefault(q => q.IdDataType == DataType.OPERATOR_BLOCKED_TO_TIME && q.Index == 0);
                                if (opOperatorData == null)
                                {
                                    opOperatorData = new OpOperatorData();
                                    opOperatorData.IdOperatorData = 0;
                                    opOperatorData.IdOperator = opOperator.IdOperator;
                                    opOperatorData.IdDataType = DataType.OPERATOR_BLOCKED_TO_TIME;
                                    opOperatorData.Index = 0;
                                }
                                opOperator.IsBlocked = true;
                                opOperatorData.Value = blockTime.HasValue && blockTime.Value > 0 ? DateTime.UtcNow.AddMinutes(blockTime.Value) : (DateTime?)null;
                                int? updatedIdOperator = null;
                                long? updatedIdOperatorData = null;
                                result = Enums.OperatorLoginStatus.Blocked;
                                TrySaveOperator(dataProvider, opOperator, 0, ref updatedIdOperator);
                                if (availableIdDataTypes.Contains(DataType.OPERATOR_BLOCKED_TO_TIME)) // gdy na bazie nie ma tego data typu to proba zapisu wygeneruje blad klucza na ID_DATA_TYPE
                                    TrySaveOperatorData(dataProvider, opOperatorData, 0, ref updatedIdOperatorData);

                                foreach (OpOperatorData operatorData in opOperator.DataList.Where(q => q.IdDataType == DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME))
                                {
                                    TryDeleteOperatorData(dataProvider, operatorData, 0);
                                }
                                opOperator.DataList.RemoveAll(q => q.IdDataType == DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME);
                                addRetryTime = false;
                            }
                            if (addRetryTime && availableIdDataTypes.Contains(DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME))
                            {
                                OpOperatorData operatorData = new OpOperatorData();
                                operatorData.IdOperatorData = 0;
                                operatorData.IdOperator = opOperator.IdOperator;
                                operatorData.IdDataType = DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME;
                                operatorData.Index = maxIndex + 1;
                                operatorData.IdDistributor = null;
                                operatorData.Value = DateTime.UtcNow;
                                long? insertedIdOperatorData = null;
                                TrySaveOperatorData(dataProvider, operatorData, 0, ref insertedIdOperatorData);
                            }
                        }
                    }
                    else
                    {
                        if (permissionsToCheck != null && permissionsToCheck.Count > 0)
                        {
                            List<int> roleIds = new List<int>();
                            List<OpOperatorRole> roles = dataProvider.GetOperatorRoleFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                IdOperator: new int[] { opOperator.IdOperator });
                            roleIds.AddRange(roles.Select(q => q.IdRole));
                            if (roles.Count > 0)
                            {
                                List<OpRoleGroup> roleGroups = dataProvider.GetRoleGroupFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                    IdRoleParent: roles.Select(q => q.IdRole).Distinct().ToArray());
                                roleIds.AddRange(roleGroups.Select(q => q.IdRole));
                            }
                            if (idModule != (int)Enums.Module.Unknown && roleIds.Count > 0)
                                roleIds = dataProvider.GetRoleFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                    IdRole: roleIds.Distinct().ToArray(), IdModule: new int[] { idModule }).Select(q => q.IdRole).ToList();
                            List<OpRoleActivity> roleActivities = new List<OpRoleActivity>();
                            if (roleIds.Count > 0)
                                roleActivities.AddRange(dataProvider.GetRoleActivityFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                    IdRole: roleIds.Distinct().ToArray(), IdActivity: permissionsToCheck.Select(q => q.Item1).Distinct().ToArray()));
                            List<OpOperatorActivity> operatorActivities = dataProvider.GetOperatorActivityFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                IdOperator: new int[] { opOperator.IdOperator }, IdActivity: permissionsToCheck.Select(q => q.Item1).Distinct().ToArray(),
                                IdModule: idModule != (int)Enums.Module.Unknown ? new int[] { idModule } : null);
                            Dictionary<int, OpActivity> ActivityDict = dataProvider.GetActivityFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                IdActivity: permissionsToCheck.Select(q => q.Item1).Distinct().ToArray()).Distinct(q => q.IdActivity).ToDictionary(l => l.ID_ACTIVITY);
                            foreach (OpRoleActivity roleActivity in roleActivities)
                            {
                                roleActivity.Activity = ActivityDict.TryGetValue(roleActivity.IdActivity);
                                if (roleActivity.Activity != null)
                                {
                                    roleActivity.Activity.ReferenceValue = roleActivity.ReferenceValue;
                                    roleActivity.Activity.Deny = roleActivity.Deny;
                                }
                            }
                            foreach (OpOperatorActivity operatorActivity in operatorActivities)
                            {
                                operatorActivity.Activity = ActivityDict.TryGetValue(operatorActivity.IdActivity);
                                if (operatorActivity.Activity != null)
                                {
                                    operatorActivity.Activity.ReferenceValue = operatorActivity.ReferenceValue;
                                    operatorActivity.Activity.Deny = operatorActivity.Deny;
                                }
                            }
                            opOperator.Activities = roleActivities.Where(q => q.Activity != null).Select(q => q.Activity).Concat(operatorActivities.Where(q => q.Activity != null).Select(q => q.Activity)).ToList();
                            result = Enums.OperatorLoginStatus.Valid;
                            foreach (Tuple<int, object, bool> permissionToCheck in permissionsToCheck)
                            {
                                try
                                {
                                    if (!opOperator.HasPermission(permissionToCheck.Item1, permissionToCheck.Item2, permissionToCheck.Item3))
                                    {
                                        result = Enums.OperatorLoginStatus.NoPermission;
                                        break;
                                    }
                                }
                                catch { }
                            }
                        }
                        else
                            result = Enums.OperatorLoginStatus.Valid;
                    }
                }
            }
            else
                result = Enums.OperatorLoginStatus.AccountNotValid;

            if (result == Enums.OperatorLoginStatus.Valid)
            {
                foreach (OpOperatorData operatorData in opOperator.DataList.Where(q => q.IdDataType == DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME || q.IdDataType == DataType.OPERATOR_BLOCKED_RETRY_TIME))
                {
                    TryDeleteOperatorData(dataProvider, operatorData, 0);
                }
                opOperator.DataList.RemoveAll(q => q.IdDataType == DataType.OPERATOR_INVALID_PASSWORD_RETRY_TIME || q.IdDataType == DataType.OPERATOR_BLOCKED_RETRY_TIME);
            }

            idSession = null;
            if (logInSession.HasValue && logInSession.Value)
            {
                int idSessionStatus = (int)Enums.SessionStatus.Validated;
                if (result == Enums.OperatorLoginStatus.AccountNotValid || result == Enums.OperatorLoginStatus.Blocked
                    || result == Enums.OperatorLoginStatus.InvalidPassword || result == Enums.OperatorLoginStatus.NoPermission)
                    idSessionStatus = (int)Enums.SessionStatus.NotValid;

                OpSession session = new OpSession();
                session.IdSession = 0;
                session.SessionGuid = Guid.NewGuid();
                session.IdModule = idModule;
                session.IdOperator = opOperator.IdOperator;
                session.IdSessionStatus = idSessionStatus;
                SessionComponent.TrySaveSession(dataProvider, session, 0, ref idSession);
                if (idSession.HasValue)
                {
                    long? idSessionData = null;
                    OpSessionData sessionData = null;
                    if (availableIdDataTypes.Contains(DataType.SESSION_TIME_STARTED)) // gdy na bazie nie ma tego data typu to proba zapisu wygeneruje blad klucza na ID_DATA_TYPE
                    {
                        sessionData = new OpSessionData();
                        sessionData.IdSessionData = 0;
                        sessionData.IdSession = idSession.Value;
                        sessionData.IdDataType = DataType.SESSION_TIME_STARTED;
                        sessionData.Value = utcNow;
                        SessionComponent.TrySaveSessionData(dataProvider, sessionData, 0, ref idSessionData);
                    }
                    if (availableIdDataTypes.Contains(DataType.SESSION_OPERATOR_LOGIN_STATUS)) // gdy na bazie nie ma tego data typu to proba zapisu wygeneruje blad klucza na ID_DATA_TYPE
                    {
                        idSessionData = null;
                        sessionData = new OpSessionData();
                        sessionData.IdSessionData = 0;
                        sessionData.IdSession = idSession.Value;
                        sessionData.IdDataType = DataType.SESSION_OPERATOR_LOGIN_STATUS;
                        sessionData.Value = (int)result;
                        SessionComponent.TrySaveSessionData(dataProvider, sessionData, 0, ref idSessionData);
                    }
                    if (availableIdDataTypes.Contains(DataType.SESSION_COMPUTER_NAME))
                    {
                        idSessionData = null;
                        sessionData = new OpSessionData();
                        sessionData.IdSessionData = 0;
                        sessionData.IdSession = idSession.Value;
                        sessionData.IdDataType = DataType.SESSION_COMPUTER_NAME;
                        sessionData.Value = Environment.MachineName;
                        SessionComponent.TrySaveSessionData(dataProvider, sessionData, 0, ref idSessionData);
                    }
                }
            }

            return result;
        }

        #endregion
        #region Authenticate

        [Obsolete("User Login method instead", true)]
        public static OpOperator Authenticate(DataProvider dataProvider, string UserName, string Password)
        {
            string PassHash = DataConverter.EncodePassword(Password);
            if (dataProvider.OperatorCacheEnabled)
            {
                return dataProvider.GetAllOperator().Find(n => n.Login == UserName && n.Password == PassHash);
            }
            else
            {
                int? IdUser = dataProvider.dbConnectionCore.LogInUser(UserName, PassHash);
                return (IdUser.HasValue) ? dataProvider.GetOperator(IdUser.Value, true) : null;
            }
        }
        #endregion
        #region IsLoginRetriesExceeded
        public static bool IsLoginRetriesExceeded(List<OpOperatorData> operatorDataToCheck, int retryCount, int? retryTime, out int maxIndex)
        {
            maxIndex = -1;
            if (operatorDataToCheck == null || operatorDataToCheck.Count == 0
                || retryCount < 1) return false;

            List<DateTime> retryTimes = operatorDataToCheck.Where(q => GenericConverter.Parse<DateTime>(q.Value) != default(DateTime)).Select(q => GenericConverter.Parse<DateTime>(q.Value)).ToList();
            maxIndex = operatorDataToCheck.Max(q => q.Index);
            if (retryTime.HasValue && retryTime.Value > 0)
            {
                DateTime retryDateTime = DateTime.UtcNow.AddMinutes(-retryTime.Value);
                return retryTimes.Count(q => q >= retryDateTime) >= retryCount;
            }
            else
                return retryTimes.Count >= retryCount;
        }
        #endregion
        #region ChangePassword
        public static void ChangePassword(string pass, OpOperator MyOperator)
        {
            MyOperator.PASSWORD = DataConverter.EncodePassword(pass);
        }
        #endregion
        #region CheckIfLoginExists
        public bool CheckIfLoginExists(DataProvider dataProvider, string login)
        {
            return true;
        }

        #endregion

        #endregion

        #region Roles | Activities

        #region GetRoles

        public static List<OpRole> GetRoles(DataProvider dataProvider, OpOperator _operator, Enums.Module module = Enums.Module.Unknown)
        {
            return GetRoles(dataProvider, _operator.IdOperator, module: module);
        }

        public static List<OpRole> GetRoles(DataProvider dataProvider, int idOperator, Enums.Module module = Enums.Module.Unknown)
        {
            List<OpRole> operatorRoles = dataProvider.GetOperatorRoleFilter(IdOperator: new int[] { idOperator }).Select(op => op.Role).ToList();
            if (operatorRoles.Count > 0)
            {
                List<OpRoleGroup> roleGroups = dataProvider.GetRoleGroupFilter(IdRoleParent: operatorRoles.Select(a => a.IdRole).ToArray());
                operatorRoles.AddRange(roleGroups.Select(rg => rg.Role));
            }
            if (module != Enums.Module.Unknown)
                operatorRoles.RemoveAll(r => r.IdModule != (int)module);
            return operatorRoles;
        }

        #endregion
        #region GetAllRoles

        public static List<OpRole> GetAllRoles(DataProvider dataProvider, OpOperator loggedOperator, OpOperator _operator)
        {
            List<OpRole> operatorRoles = dataProvider.GetOperatorRoleFilter(IdOperator: new int[] { _operator.IdOperator }).Select(op => op.Role).ToList();
            if (operatorRoles.Count > 0)
            {
                List<OpRoleGroup> roleGroups = dataProvider.GetRoleGroupFilter(IdRoleParent: operatorRoles.Select(a => a.IdRole).ToArray());
                operatorRoles.AddRange(roleGroups.Select(rg => rg.Role));
            }
            operatorRoles = RoleComponent.FilterByPermission<OpRole>(operatorRoles, Activity.ALLOWED_ROLE, loggedOperator);
            if (operatorRoles.Count > 0)
            {
                List<OpModule> allowedModule = RoleComponent.FilterByPermission<OpModule>(dataProvider.GetAllModule(), Activity.ALLOWED_MODULE, loggedOperator);

                if (allowedModule.Select(m => m.IdModule).Distinct().Count() != dataProvider.GetAllModule().Count())
                    operatorRoles.RemoveAll(r => !r.IdModule.HasValue);//jeśli nie mamy uprawnień do wszystkich modułów to usuwamy role przypisane do All
                operatorRoles.RemoveAll(r => r.IdModule.HasValue && !r.IdModule.Value.In(allowedModule.Select(m => m.IdModule).ToArray()));
            }
            return operatorRoles;
        }

        #endregion
        #region GetActivities

        public static List<OpActivity> GetActivities(DataProvider dataProvider, OpOperator _operator, Enums.Module module = Enums.Module.Unknown)
        {
            return GetActivities(dataProvider, _operator.IdOperator, module: module);
        }

        public static List<OpActivity> GetActivities(DataProvider dataProvider, int idOperator, Enums.Module module = Enums.Module.Unknown)
        {
            List<OpRole> roles = GetRoles(dataProvider, idOperator, module: module);
            List<OpActivity> activities = GetActivities(dataProvider, idOperator, roles, module: module);

            return activities;
        }

        public static List<OpActivity> GetActivities(DataProvider dataProvider, OpOperator _operator, List<OpRole> roles, Enums.Module module = Enums.Module.Unknown)
        {
            return GetActivities(dataProvider, _operator.IdOperator, roles, module: module);
        }

        public static List<OpActivity> GetActivities(DataProvider dataProvider, int idOperator, List<OpRole> roles, Enums.Module module = Enums.Module.Unknown)
        {
            List<OpActivity> activities = new List<OpActivity>();
            if (roles != null && roles.Count > 0)
            {
                List<OpRoleActivity> roleActivities = dataProvider.GetRoleActivityFilter(IdRole: roles.Select(r => r.IdRole).ToArray()).ToList();
                activities.AddRange(roleActivities.Select(oa => oa.Activity));
            }

            List<OpOperatorActivity> operatorActivities = dataProvider.GetOperatorActivityFilter(IdOperator: new int[] { idOperator }, IdModule: (module != Enums.Module.Unknown) ? new int[] { (int)module } : null).ToList();
            activities.AddRange(operatorActivities.Select(oa => oa.Activity));

            return activities;
        }

        #endregion
        #region GetAllActivities

        public static List<OpActivity> GetAllActivities(DataProvider dataProvider, OpOperator loggedOperator, OpOperator _operator)
        {
            return GetAllActivities(dataProvider, loggedOperator, _operator, GetAllRoles(dataProvider, loggedOperator, _operator));
        }

        public static List<OpActivity> GetAllActivities(DataProvider dataProvider, OpOperator loggedOperator, OpOperator _operator, List<OpRole> roles)
        {
            List<OpActivity> activities = new List<OpActivity>();
            if (roles != null && roles.Count > 0)
            {
                List<OpRoleActivity> roleActivities = dataProvider.GetRoleActivityFilter(IdRole: roles.Select(r => r.IdRole).ToArray()).ToList();
                activities.AddRange(roleActivities.Select(oa => oa.Activity));
            }

            List<OpOperatorActivity> operatorActivities = dataProvider.GetOperatorActivityFilter(IdOperator: new int[] { _operator.IdOperator }).ToList();
            activities.AddRange(operatorActivities.Select(oa => oa.Activity));

            activities = RoleComponent.FilterByPermission<OpActivity>(activities, Activity.ALLOWED_ACTIVITES, loggedOperator);
            if (activities.Count > 0)
            {
                List<OpModule> allowedModule = RoleComponent.FilterByPermission<OpModule>(dataProvider.GetAllModule(), Activity.ALLOWED_MODULE, loggedOperator);

                if (allowedModule.Select(m => m.IdModule).Distinct().Count() != dataProvider.GetAllModule().Count())
                    activities.RemoveAll(r => !r.IdModule.HasValue);//jeśli nie mamy uprawnień do wszystkich modułów to usuwamy Activity przypisane do All
                activities.RemoveAll(r => r.IdModule.HasValue && !r.IdModule.Value.In(allowedModule.Select(m => m.IdModule).ToArray()));
            }
            return activities;
        }

        #endregion

        public static bool SaveRoles(DataProvider dataProvider, OpOperator _operator, List<OpRole> roles, List<Enums.Module> modules)
        {
            List<OpRole> existingRoles = dataProvider.GetOperatorRoleFilter(IdOperator: new int[] { _operator.IdOperator }).Select(r => r.Role).ToList();
            for (int i = 0; i < roles.Count; i++)
            {
                bool found = false;
                for (int j = 0; j < modules.Count; j++)
                {
                    if (roles[i].IdModule == (int)modules[j])
                        found = true;
                }
                if (!found)
                {
                    roles.RemoveAt(i);
                    if (i > 0)
                        i--;
                }
            }
            for (int i = 0; i < existingRoles.Count; i++)
            {
                bool found = false;
                for (int j = 0; j < modules.Count; j++)
                {
                    if (existingRoles[i].IdModule == (int)modules[j])
                        found = true;
                }
                if (!found)
                {
                    existingRoles.RemoveAt(i);
                    if (i > 0)
                        i--;
                }
            }
            List<OpRole> toDelete = existingRoles.Except(roles, r => new { IdRole = r.IdRole }).ToList();
            List<OpRole> intersect = roles.Intersect(existingRoles, r => new { IdRole = r.IdRole }).ToList();
            List<OpRole> toAdd = roles.Except(intersect, r => new { IdRole = r.IdRole }).ToList();
            foreach (OpRole roleItem in toDelete)
            {
                OpOperatorRole itemToDelete = new OpOperatorRole();
                itemToDelete.Operator = _operator;
                itemToDelete.Role = roleItem;
                dataProvider.DeleteOperatorRoleByOperatorAndRole(itemToDelete.IdOperator, itemToDelete.IdRole);
            }
            foreach (OpRole roleItem in toAdd)
            {
                OpOperatorRole itemToAdd = new OpOperatorRole();
                itemToAdd.Operator = _operator;
                itemToAdd.Role = roleItem;
                dataProvider.SaveOperatorRole(itemToAdd);
            }
            return true;
        }

        [Obsolete("Use GetRoles and GetActivities instead", true)]
        public static void LoadActivities(DataProvider dataProvider, OpOperator oper)
        {
            oper.Activities = OpOperatorActivity.ConvertList(dataProvider.dbConnectionCore.GetOperatorActivity(oper.IdOperator), dataProvider).Select(d => d.Activity).ToList();
        }

        [Obsolete("Use GetRoles and GetActivities instead", true)]
        public static List<OpRole> LoadApplicationRoles(DataProvider dataProvider, OpOperator oper)
        {
            List<OpRole> roles = new List<OpRole>();
            foreach (OpActivity loop in oper.Activities)
            {
                if (loop.IdActivity == Activity.ALLOWED_ROLE && loop.ReferenceValue != null)
                {
                    OpRole insert = dataProvider.GetRole(Convert.ToInt32(loop.ReferenceValue));
                    if (insert != null)
                    {
                        roles.Add(insert);
                    }
                }
            }
            return roles;
        }
        #endregion

        #region DownloadOperatorSignature
        public static Image DownloadOperatorSignature(DataProvider dataProvider, OpOperator opOperator)
        {
            try
            {
                long? idFile = null;
                if (!opOperator.DataList.Any(x => x.IdDataType == DataType.OPERATOR_SIGNATURE))
                {
                    List<OpOperatorData> opData = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdOperator: new int[] { opOperator.IdOperator }, IdDataType: new long[] { DataType.OPERATOR_SIGNATURE });

                    if (opData != null && opData.Count > 0)
                    {
                        idFile = Convert.ToInt64(opData.FirstOrDefault(x => x.Index == opData.Max(y => y.Index)).Value);
                    }

                    opOperator.DataList.AddRange(opData);
                }
                else
                {
                    int maxIdx = opOperator.DataList.Where(x => x.IdDataType == DataType.OPERATOR_SIGNATURE).Max(y => y.Index);
                    idFile = Convert.ToInt64(opOperator.DataList.Where(x => x.IdDataType == DataType.OPERATOR_SIGNATURE && maxIdx == x.Index).First().Value);
                }

                if (idFile.HasValue)
                {
                    byte[] bytes = FileComponent.GetFile(dataProvider, idFile.Value).FileBytes as byte[];

                    if (bytes != null && bytes.Length > 0 && bytes[0] != 0x00)
                    {
                        Image image = null;
                        using (System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes))
                        {
                            image = Image.FromStream(ms);
                        }

                        return image;
                    }
                }
            }
            catch
            {
            }

            return null;
        }
        #endregion

        #region UploadOperatorSignature
        public static void UploadOperatorSignature(DataProvider dataProvider, OpOperator opOperator, Image image, string fileDescription = "")
        {
            try
            {
                long? idFile = null;
                if (!opOperator.DataList.Any(x => x.IdDataType == DataType.OPERATOR_SIGNATURE))
                {
                    List<OpOperatorData> opData = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdOperator: new int[] { opOperator.IdOperator }, IdDataType: new long[] { DataType.OPERATOR_SIGNATURE });

                    if (opData != null && opData.Count > 0)
                    {
                        idFile = Convert.ToInt64(opData.FirstOrDefault(x => x.Index == opData.Max(y => y.Index)).Value);
                    }

                    opOperator.DataList.AddRange(opData);
                }
                else
                {
                    int maxIdx = opOperator.DataList.Where(x => x.IdDataType == DataType.OPERATOR_SIGNATURE).Max(y => y.Index);
                    idFile = Convert.ToInt64(opOperator.DataList.Where(x => x.IdDataType == DataType.OPERATOR_SIGNATURE && maxIdx == x.Index).First().Value);
                }

                OpFile currentFile = null;
                if (idFile.HasValue)
                    currentFile = FileComponent.GetFile(dataProvider, idFile.Value);

                byte[] bytes = null;

                if (image == null)
                {
                    bytes = new byte[] { 0x00 };
                }
                else
                {
                    using (var ms = new System.IO.MemoryStream())
                    {
                        image.Save(ms, image.RawFormat);
                        bytes = ms.ToArray();
                    }
                }

                string extension = image == null ? string.Empty : new ImageFormatConverter().ConvertToString(image.RawFormat);

                if (!string.IsNullOrWhiteSpace(extension)) extension = "." + extension;

                if (currentFile == null)
                {
                    OpFile file = new OpFile();
                    file.PhysicalFileName = (opOperator.Actor == null ? opOperator.IdOperator.ToString() : opOperator.Actor.FullName) + extension;
                    file.Size = bytes.Length;
                    file.InsertDate = dataProvider.DateTimeNow;
                    file.FileBytes = bytes;
                    file.Description = fileDescription;

                    file.IdFile = dataProvider.SaveFile(file);
                    dataProvider.SaveFileContent(file);

                    OpOperatorData opOperatorData = new OpOperatorData();
                    opOperatorData.Value = file.IdFile;
                    opOperatorData.IdDataType = DataType.OPERATOR_SIGNATURE;
                    opOperatorData.IdOperator = opOperator.IdOperator;
                    opOperatorData.IdDistributor = opOperator.IdDistributor;
                    opOperatorData.Index = opOperator.DataList.GetNextIndex(DataType.OPERATOR_SIGNATURE);

                    opOperatorData.IdOperatorData = dataProvider.SaveOperatorData(opOperatorData);

                    opOperator.DataList.Add(opOperatorData);
                }
                else
                {
                    if (currentFile != null)
                    {
                        if (image != null)
                        {                          
                            byte[] currentFileBytes = currentFile.FileBytes != null ? currentFile.FileBytes as byte[] : null;                            
                            if (!currentFile.PhysicalFileName.Equals((opOperator.Actor == null ? opOperator.IdOperator.ToString() : opOperator.Actor.FullName) + extension) && (currentFileBytes == null || !bytes.SequenceEqual(currentFileBytes)))
                            {
                                currentFile.PhysicalFileName = (opOperator.Actor == null ? opOperator.IdOperator.ToString() : opOperator.Actor.FullName) + extension;
                                currentFile.Size = bytes.Length;
                                currentFile.InsertDate = dataProvider.DateTimeNow;
                                currentFile.FileBytes = bytes;
                                currentFile.Description = fileDescription;

                                currentFile.IdFile = dataProvider.SaveFile(currentFile);
                                dataProvider.SaveFileContent(currentFile);
                            }
                        }
                        else
                        {
                            dataProvider.DeleteFile(currentFile);  
                                                    
                            foreach(OpOperatorData odItem in opOperator.DataList.Where(x => x.IdDataType == DataType.OPERATOR_SIGNATURE)) 
                                dataProvider.DeleteOperatorData(odItem);
                        }
                    }
                }
            }
            catch
            {
            }
        }
        #endregion

        #region CheckIfOperatorExistOnServer

        public static bool CheckIfOperatorExistOnServer(DataProvider dataProvider, OpOperator opOperator, OpImrServer imrServer)
        {
            if (opOperator.DataList != null && opOperator.DataList.Exists(e => e.IdDataType == DataType.OPERATOR_IMR_SERVER_ID && Convert.ToInt32(e.Value) == imrServer.IdImrServer))
                return true;
            else
                return false;
        }

        #endregion

        #region GetPasswordValidityConfiguration
        public static void GetPasswordValidityConfiguration(DataProvider dataProvider, OpOperator opOperator,
            ref DateTime? passwordValidToUtcTime,
            ref DateTime? notifyExpiryFromUtcTime,
            ref DateTime? passwordCreationUtcTime,
            bool checkForceChange = false)
        {
            DateTime? previousCreationUtcTime = passwordCreationUtcTime;
            DateTime? creationUtcTime = null;
            passwordValidToUtcTime = null;
            notifyExpiryFromUtcTime = null;
            passwordCreationUtcTime = null;
            bool? forceChange = null;

            if (dataProvider == null || opOperator == null) return;

            try
            {
                OpDataList<OpOperatorData> operatorDataList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdOperator: new int[] { opOperator.IdOperator }, IndexNbr: new int[] { 0 }, IdDistributor: new int[0],
                    IdDataType: new long[] { DataType.OPERATOR_PASSWORD_CREATION_TIME, DataType.OPERATOR_CHANGE_PASSWORD_DURING_NEXT_LOGIN }).ToOpDataList();
                creationUtcTime = operatorDataList.TryGetNullableValue<DateTime>(DataType.OPERATOR_PASSWORD_CREATION_TIME);
                forceChange = operatorDataList.TryGetNullableValue<bool>(DataType.OPERATOR_CHANGE_PASSWORD_DURING_NEXT_LOGIN);
            }
            catch { }

            if (!creationUtcTime.HasValue && previousCreationUtcTime.HasValue)
                creationUtcTime = previousCreationUtcTime;

            passwordCreationUtcTime = creationUtcTime;

            if (checkForceChange && forceChange.HasValue && forceChange.Value)
            {
                passwordValidToUtcTime = notifyExpiryFromUtcTime = dataProvider.DateTimeUtcNow.AddSeconds(-1);
                return;
            }

            int? validDays = null;
            int? expiryDays = null;
            try
            {
                OpDataList<OpDistributorData> distributorDataList = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdDistributor: new int[] { opOperator.IdDistributor }, IndexNbr: new int[] { 0 }, IdDataType: new long[] { DataType.PASSWORD_VALIDITY_DAYS, DataType.PASSWORD_EXPIRY_NOTIFICATION_DAYS }).ToOpDataList();
                validDays = distributorDataList.TryGetNullableValue<int>(DataType.PASSWORD_VALIDITY_DAYS);
                expiryDays = distributorDataList.TryGetNullableValue<int>(DataType.PASSWORD_EXPIRY_NOTIFICATION_DAYS);
            }
            catch { }

            List<long> systemIdDataTypeList = new List<long>();
            if (!validDays.HasValue)
                systemIdDataTypeList.Add(DataType.PASSWORD_VALIDITY_DAYS);
            if (!expiryDays.HasValue)
                systemIdDataTypeList.Add(DataType.PASSWORD_EXPIRY_NOTIFICATION_DAYS);

            try
            {
                if (systemIdDataTypeList.Count > 0)
                {
                    OpDataList<OpSystemData> systemDataList = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDataType: systemIdDataTypeList.ToArray(), IndexNbr: new int[] { 0 }).ToOpDataList();
                    if (!validDays.HasValue)
                        validDays = systemDataList.TryGetNullableValue<int>(DataType.PASSWORD_VALIDITY_DAYS);
                    if (!expiryDays.HasValue)
                        expiryDays = systemDataList.TryGetNullableValue<int>(DataType.PASSWORD_EXPIRY_NOTIFICATION_DAYS);
                }
            }
            catch { }

            if (validDays.HasValue && validDays.Value > 0)
            {
                if (creationUtcTime.HasValue)
                    passwordValidToUtcTime = creationUtcTime.Value.AddDays(validDays.Value);
            }

            if (passwordValidToUtcTime.HasValue && expiryDays.HasValue && expiryDays.Value > 0)
                notifyExpiryFromUtcTime = passwordValidToUtcTime.Value.AddDays(-expiryDays.Value);
        }
        #endregion

        #region ClearChangePasswordFlag
        public static void ClearChangePasswordFlag(DataProvider dataProvider, OpOperator opOperator)
        {
            if (dataProvider == null || opOperator == null) return;

            try
            {
                foreach (OpOperatorData changeData in dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                            IdOperator: new int[] { opOperator.IdOperator }, IdDistributor: new int[0], IdDataType: new long[] { DataType.OPERATOR_CHANGE_PASSWORD_DURING_NEXT_LOGIN },
                            customWhereClause: " [VALUE] IS NOT NULL "))
                {
                    changeData.Value = null;
                    long? idOperatorData = null;
                    TrySaveOperatorData(dataProvider, changeData, 0, ref idOperatorData);
                }
            }
            catch { }
        }
        #endregion

        #region SetHelperValues
        public static void SetHelperValues(DataProvider dataProvider, OpOperator oper, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdOperator, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_OPERATOR:
                        oper.DataList.SetValue(DataType.HELPER_ID_OPERATOR, 0, oper.IdOperator);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        oper.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, oper.IdDistributor);
                        break;
                    case DataType.HELPER_ID_ACTOR:
                        oper.DataList.SetValue(DataType.HELPER_ID_ACTOR, 0, oper.IdActor);
                        break;
                    case DataType.HELPER_LOGIN:
                        oper.DataList.SetValue(DataType.HELPER_LOGIN, 0, oper.Login);
                        break;
                    case DataType.HELPER_PASSWORD:
                        oper.DataList.SetValue(DataType.HELPER_PASSWORD, 0, oper.Password);
                        break;
                    case DataType.HELPER_SERIAL_NBR:
                        oper.DataList.SetValue(DataType.HELPER_SERIAL_NBR, 0, oper.SerialNbr);
                        break;
                    case DataType.HELPER_DESCRIPTION:
                        oper.DataList.SetValue(DataType.HELPER_DESCRIPTION, 0, oper.Description);
                        break;
                    case DataType.HELPER_IS_BLOCKED:
                        oper.DataList.SetValue(DataType.HELPER_IS_BLOCKED, 0, oper.IsBlocked);
                        break;
                    default:
                        break;
                }
            }
            oper.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }
        #endregion

        #region GenerateRandomPassword

        public static string GenerateRandomPassword(DataProvider dataProvider, OpDistributor distributor)
        {
            try
            {
                string newRandomPassword = "";

                const string upperLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                const string lowerLetters = "abcdefghijklmnopqrstuvwxyz";
                const string digits = "0123456789";
                const string specialSymbols = "!@#$%^&*()[]";

                Tuple<int, int, int, int, int> imrPasswordValidationRule = GetIMRPasswordValidationRule(dataProvider, distributor);
                int passwordMinLength = imrPasswordValidationRule.Item1;
                int passwordMinLowerLettersCount = imrPasswordValidationRule.Item2;
                int passwordMinUpperLettersCount = imrPasswordValidationRule.Item3;
                int passwordMinDigitsCount = imrPasswordValidationRule.Item4;
                int passwordMinSpecialSymbolsCount = imrPasswordValidationRule.Item5;

                if (passwordMinLength == 0)
                    passwordMinLength = 10; // Jesli nie będzie datatypu to domyslnie hasło ma 10 znaków

                StringBuilder sbStringAcceptValidationRule = new StringBuilder();
                StringBuilder sbNewPassword = new StringBuilder();
                System.Security.Cryptography.RNGCryptoServiceProvider cryptRNG = null;
                byte[] tokenBuffer = null;

                cryptRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
                tokenBuffer = new byte[passwordMinLowerLettersCount];
                cryptRNG.GetBytes(tokenBuffer);
                for (int i = 0; i < passwordMinLowerLettersCount; i++)
                {
                    int pos = tokenBuffer[i] % lowerLetters.Length;
                    sbStringAcceptValidationRule.Append(lowerLetters[pos]);
                }

                cryptRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
                tokenBuffer = new byte[passwordMinUpperLettersCount];
                cryptRNG.GetBytes(tokenBuffer);
                for (int i = 0; i < passwordMinUpperLettersCount; i++)
                {
                    int pos = tokenBuffer[i] % upperLetters.Length;
                    sbStringAcceptValidationRule.Append(upperLetters[pos]);
                }

                cryptRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
                tokenBuffer = new byte[passwordMinDigitsCount];
                cryptRNG.GetBytes(tokenBuffer);
                for (int i = 0; i < passwordMinDigitsCount; i++)
                {
                    int pos = tokenBuffer[i] % digits.Length;
                    sbStringAcceptValidationRule.Append(digits[pos]);
                }

                cryptRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
                tokenBuffer = new byte[passwordMinSpecialSymbolsCount];
                cryptRNG.GetBytes(tokenBuffer);
                for (int i = 0; i < passwordMinSpecialSymbolsCount; i++)
                {
                    int pos = tokenBuffer[i] % specialSymbols.Length;
                    sbStringAcceptValidationRule.Append(specialSymbols[pos]);
                }

                if (sbStringAcceptValidationRule.Length < passwordMinLength)
                {
                    int missingLettersCount = passwordMinLength - sbStringAcceptValidationRule.Length;
                    cryptRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
                    tokenBuffer = new byte[missingLettersCount];
                    cryptRNG.GetBytes(tokenBuffer);
                    string allSymbols = upperLetters + lowerLetters + digits + specialSymbols;
                    for (int i = 0; i < missingLettersCount; i++)
                    {
                        int pos = tokenBuffer[i] % allSymbols.Length;
                        sbStringAcceptValidationRule.Append(allSymbols[pos]);
                    }
                }

                //Random num = new Random();        
                cryptRNG = new System.Security.Cryptography.RNGCryptoServiceProvider();
                newRandomPassword = new string(sbStringAcceptValidationRule.ToString().ToCharArray().
                                OrderBy(s => GetNextInt32(cryptRNG)).ToArray());


                return newRandomPassword;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static int GetNextInt32(System.Security.Cryptography.RNGCryptoServiceProvider rnd)
        {
            byte[] randomInt = new byte[4];
            rnd.GetBytes(randomInt);
            return Convert.ToInt32(randomInt[0]);
        }

        #endregion

        #region GetIMRPasswordValidationRule

        public static Tuple<int, int, int, int, int> GetIMRPasswordValidationRule(DataProvider dataProvider, OpDistributor distributor)
        {
            try
            {
                int? passwordMinLength = null;
                int? passwordMinLowerLettersCount = null;
                int? passwordMinUpperLettersCount = null;
                int? passwordMinDigitsCount = null;
                int? passwordMinSpecialSymbolsCount = null;

                if (distributor != null)
                {
                    List<OpDistributorData> distributorList = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDistributor: new int[] { distributor.IdDistributor },
                        IdDataType: new long[] { DataType.PASSWORD_MIN_LENGTH, DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT, DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT, DataType.PASSWORD_MIN_DIGITS_COUNT, DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT },
                        IndexNbr: new int[] { 0 });

                    if (distributorList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_LENGTH && Convert.ToInt32(d.Value) > 0))
                        passwordMinLength = Convert.ToInt32(distributorList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_LENGTH && Convert.ToInt32(d.Value) > 0).Value);

                    if (distributorList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0))
                        passwordMinLowerLettersCount = Convert.ToInt32(distributorList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0).Value);

                    if (distributorList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0))
                        passwordMinUpperLettersCount = Convert.ToInt32(distributorList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0).Value);

                    if (distributorList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_DIGITS_COUNT && Convert.ToInt32(d.Value) > 0))
                        passwordMinDigitsCount = Convert.ToInt32(distributorList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_DIGITS_COUNT && Convert.ToInt32(d.Value) > 0).Value);

                    if (distributorList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT && Convert.ToInt32(d.Value) > 0))
                        passwordMinSpecialSymbolsCount = Convert.ToInt32(distributorList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT && Convert.ToInt32(d.Value) > 0).Value);
                }

                List<long> systemDataTypes = new List<long>();
                if (!passwordMinLength.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_LENGTH);
                if (!passwordMinLowerLettersCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT);
                if (!passwordMinUpperLettersCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT);
                if (!passwordMinDigitsCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_DIGITS_COUNT);
                if (!passwordMinSpecialSymbolsCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT);

                if (systemDataTypes.Count > 0)
                {
                    List<OpSystemData> systemDataList = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDataType: systemDataTypes.ToArray(), IndexNbr: new int[] { 0 });
                    if (!passwordMinLength.HasValue)
                    {
                        if (systemDataList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_LENGTH && Convert.ToInt32(d.Value) > 0))
                            passwordMinLength = Convert.ToInt32(systemDataList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_LENGTH && Convert.ToInt32(d.Value) > 0).Value);
                        else
                            passwordMinLength = 0;
                    }
                    if (!passwordMinLowerLettersCount.HasValue)
                    {
                        if (systemDataList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0))
                            passwordMinLowerLettersCount = Convert.ToInt32(systemDataList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0).Value);
                        else
                            passwordMinLowerLettersCount = 0;
                    }
                    if (!passwordMinUpperLettersCount.HasValue)
                    {
                        if (systemDataList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0))
                            passwordMinUpperLettersCount = Convert.ToInt32(systemDataList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT && Convert.ToInt32(d.Value) > 0).Value);
                        else
                            passwordMinUpperLettersCount = 0;
                    }
                    if (!passwordMinDigitsCount.HasValue)
                    {
                        if (systemDataList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_DIGITS_COUNT && Convert.ToInt32(d.Value) > 0))
                            passwordMinDigitsCount = Convert.ToInt32(systemDataList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_DIGITS_COUNT && Convert.ToInt32(d.Value) > 0).Value);
                        else
                            passwordMinDigitsCount = 0;
                    }
                    if (!passwordMinSpecialSymbolsCount.HasValue)
                    {
                        if (systemDataList.Exists(d => d.IdDataType == DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT && Convert.ToInt32(d.Value) > 0))
                            passwordMinSpecialSymbolsCount = Convert.ToInt32(systemDataList.FirstOrDefault(d => d.IdDataType == DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT && Convert.ToInt32(d.Value) > 0).Value);
                        else
                            passwordMinSpecialSymbolsCount = 0;
                    }
                }

                return new Tuple<int, int, int, int, int>(passwordMinLength.Value, passwordMinUpperLettersCount.Value, passwordMinLowerLettersCount.Value, passwordMinDigitsCount.Value, passwordMinSpecialSymbolsCount.Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ValidatePassword

        public static Tuple<bool, string> ValidatePassword(string passwordToConfirm, OpDistributor distributor, DataProvider dataProvider)
        {
            string error = "";
            bool isGood = true;
            try
            {
                int? minLength = null;
                int? minLowerCount = null;
                int? minUpperCount = null;
                int? minDigitsCount = null;
                int? minSpecialCount = null;

                if (distributor != null)
                {
                    OpDataList<OpDistributorData> distributorDataList = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDistributor: new int[] { distributor.IdDistributor },
                        IdDataType: new long[] { DataType.PASSWORD_MIN_LENGTH, DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT, DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT, DataType.PASSWORD_MIN_DIGITS_COUNT, DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT },
                        IndexNbr: new int[] { 0 }).ToOpDataList();
                    minLength = distributorDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_LENGTH, 0);
                    minLowerCount = distributorDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT, 0);
                    minUpperCount = distributorDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT, 0);
                    minDigitsCount = distributorDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_DIGITS_COUNT, 0);
                    minSpecialCount = distributorDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT, 0);
                }

                List<long> systemDataTypes = new List<long>();
                if (!minLength.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_LENGTH);
                if (!minLowerCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT);
                if (!minUpperCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT);
                if (!minDigitsCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_DIGITS_COUNT);
                if (!minSpecialCount.HasValue)
                    systemDataTypes.Add(DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT);
                if (systemDataTypes.Count > 0)
                {
                    OpDataList<OpSystemData> systemDataList = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdDataType: systemDataTypes.ToArray(), IndexNbr: new int[] { 0 }).ToOpDataList();
                    if (!minLength.HasValue)
                        minLength = systemDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_LENGTH, 0);
                    if (!minLowerCount.HasValue)
                        minLowerCount = systemDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_LOWER_LETTERS_COUNT, 0);
                    if (!minUpperCount.HasValue)
                        minUpperCount = systemDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_UPPER_LETTERS_COUNT, 0);
                    if (!minDigitsCount.HasValue)
                        minDigitsCount = systemDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_DIGITS_COUNT, 0);
                    if (!minSpecialCount.HasValue)
                        minSpecialCount = systemDataList.TryGetNullableValue<int>(DataType.PASSWORD_MIN_SPECIAL_SYMBOLS_COUNT, 0);
                }

                if (!minLength.HasValue)
                    minLength = 8;

                if (minLength.HasValue && minLength.Value > 0 && passwordToConfirm.Length < minLength.Value)
                {
                    error = String.Format(ResourcesText.MinimumCharactersRequired_0, minLength.Value);
                    isGood = false;
                }
                if (minLowerCount.HasValue && minLowerCount.Value > 0 && passwordToConfirm.Count(c => char.IsLower(c)) < minLowerCount.Value)
                {
                    error = String.Format(ResourcesText.MinimumLowerLettersRequired_0, minLowerCount.Value);
                    isGood = false;
                }
                if (minUpperCount.HasValue && minUpperCount.Value > 0 && passwordToConfirm.Count(c => char.IsUpper(c)) < minUpperCount.Value)
                {
                    error = String.Format(ResourcesText.MinimumUpperLettersRequired_0, minUpperCount.Value);
                    isGood = false;
                }
                if (minDigitsCount.HasValue && minDigitsCount.Value > 0 && passwordToConfirm.Count(c => char.IsDigit(c)) < minDigitsCount.Value)
                {
                    error = String.Format(ResourcesText.MinimumDigitsRequired_0, minDigitsCount.Value);
                    isGood = false;
                }
                if (minSpecialCount.HasValue && minSpecialCount.Value > 0 && passwordToConfirm.Count(c => !char.IsLetterOrDigit(c)) < minSpecialCount.Value)
                {
                    error = String.Format(ResourcesText.MinimumSpecialCharactersRequired_0, minSpecialCount.Value);
                    isGood = false;
                }
            }
            catch
            {
                error = ResourcesText.ErrorDuringValidatingPassword;
            }
            return new Tuple<bool, string>(isGood, error);
        }

        #endregion
    }
}
