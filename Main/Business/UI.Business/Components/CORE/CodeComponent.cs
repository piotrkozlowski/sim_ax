﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
	public class CodeComponent
	{
		#region Methods
		public static void Save(DataProvider dataProvider, List<OpCode> codes)
		{
			for (int i = codes.Count - 1; i >= 0; i--)
			{
				if (codes[i].OpState == OpChangeState.Delete)
				{
					dataProvider.DeleteCode(codes[i]);
					codes.RemoveAt(i);
				}
			}

			foreach (OpCode code in codes.Where(w => w.OpState == Objects.OpChangeState.New || w.OpState == OpChangeState.Modified))
			{
				code.IdCode = dataProvider.SaveCode(code);
				if (code.OpState == OpChangeState.New)
					code.AssignReferences(dataProvider);
				code.OpState = OpChangeState.Loaded;
			}
		}
		#endregion
	}
}
