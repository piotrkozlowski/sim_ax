﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DistributorSupplierComponent
    {
        #region Methods
        public static OpDistributorSupplier GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetDistributorSupplier(Id);
        }

        public static List<OpDistributorSupplier> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllDistributorSupplier();
        }

        public static List<OpDistributorSupplier> GetByFuelType(DataProvider dataProvider, OpProductCode productCode)
        {
            return dataProvider.GetAllDistributorSupplier().FindAll(n => n.ProductCode == productCode);
        }

        public static OpDistributorSupplier Save(DataProvider dataProvider, OpDistributorSupplier objectToSave)
        {
            bool isNewItem = objectToSave.IdDistributor == 0; //check if it's new distributor
            dataProvider.SaveDistributorSupplier(objectToSave);
            return dataProvider.GetDistributorSupplier(objectToSave.IdDistributor);
        }

        public static void Delete(DataProvider dataProvider, OpDistributorSupplier objectToDelete)
        {
            dataProvider.DeleteDistributorSupplier(objectToDelete);
        }
        #endregion
    }
}
