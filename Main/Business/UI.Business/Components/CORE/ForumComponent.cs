﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.IO;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ForumComponent : BaseComponent
    {
        #region Save

        public static OpMessage Save(DataProvider dataProvider, OpMessage objectToSave, bool onlyData = false)
        {
            try
            {
                if (!onlyData)
                {
                    objectToSave.IdMessage = dataProvider.SaveMessage(objectToSave);
                    objectToSave.Hierarchy += objectToSave.IdMessage + ".";
                    dataProvider.SaveMessage(objectToSave);
                }
                foreach (OpMessageData mdItem in objectToSave.DataList)
                {
                    if (mdItem.OpState == Objects.OpChangeState.New || mdItem.OpState == Objects.OpChangeState.Modified)
                    {
                        if (mdItem.IdMessage == 0)
                            mdItem.IdMessage = objectToSave.IdMessage;
                        dataProvider.SaveMessageData(mdItem);
                    }
                    else if (mdItem.OpState == Objects.OpChangeState.Delete)
                    {
                        if (mdItem.IdMessage != 0)
                            dataProvider.DeleteMessageData(mdItem);
                        objectToSave.DataList.Remove(mdItem, true);
                    }

                }
                return objectToSave;
            }
            catch //(Exception ex)
            {
                return null;
            }
        }

        public static void SaveAttachment(OpMessage message, DataProvider dataProvider, long dataType)
        {
            List<OpMessageData> newAttachmentsList = message.DataList.FindAll(t => t.IdDataType == dataType && t.OpState == Objects.OpChangeState.New);
            foreach (OpMessageData mItem in newAttachmentsList)
            {
                mItem.IdMessageData = dataProvider.SaveMessageData(mItem);
                mItem.OpState = Objects.OpChangeState.Loaded;
            }
        }

        #endregion

        #region Delete

        public static void Delete(DataProvider dataProvider, OpMessage objectToDelete, bool withHierarchy)
        {
            if (!withHierarchy)
            {
                foreach (OpMessageData mdItem in objectToDelete.DataList)
                {
                    if (mdItem.IdMessageData > 0)
                    {
                        dataProvider.DeleteMessageData(mdItem);
                    }
                }
                dataProvider.DeleteMessage(objectToDelete);
            }
            else
            {
                List<OpMessage> childList = dataProvider.GetMessageFilter(customWhereClause: "[HIERARCHY] like '%." + objectToDelete.IdMessage + ".%'");
                childList = childList.OrderBy(m => m.Hierarchy.Length).ToList();
                for (int i = childList.Count - 1; i >= 0; i--)
                {
                    foreach (OpMessageData mdItem in childList[i].DataList)
                    {
                        if (mdItem.IdMessageData > 0)
                        {
                            dataProvider.DeleteMessageData(mdItem);
                        }
                    }
                    dataProvider.DeleteMessage(childList[i]);
                }
            }
                
            
        }

        #endregion

        #region ChangeHierarchy

        public static void ChangeHierarchy(DataProvider dataProvider, OpMessage objectToChange, OpMessage newParent)
        {
            if (objectToChange != null && newParent != null)
            {
                List<OpMessage> changeList = dataProvider.GetMessageFilter(customWhereClause: "[HIERARCHY] like '%." + objectToChange.IdMessage + ".%'");
                foreach (OpMessage mItem in changeList)
                {
                    if(mItem.IdMessage == objectToChange.IdMessage)
                        mItem.IdMessageParent = newParent.IdMessage;
                    mItem.Hierarchy.Replace("." + objectToChange.IdMessageParent.Value + ".", "." + newParent.IdMessage + ".");
                    dataProvider.SaveMessage(mItem);
                }
            }
        }

        #endregion

        #region Files
        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpMessage message, OpFile file, string savePath)
        {
            return DownloadFile(dataProvider, loggedOperator, message, file, savePath, false);
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpMessage message, OpFile file, string savePath, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                string filePathDownloaded = ftpClient.DownloadFile(savePath, String.Format(@"{0}/{1}/", useTestFolders ? "MessageTest" : "Message", message.IdMessage), file.PhysicalFileName);
                if (filePathDownloaded != null)
                {
                    file.LastDownloaded = dataProvider.DateTimeNow;
                    dataProvider.SaveFile(file);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpMessage message, string filePath, bool onlyAssign, OpMessageData existingFile, string md5)
        {
            return UploadFile(dataProvider, loggedOperator, message, filePath, onlyAssign, md5, existingFile, false);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpMessage message, string filePath, bool onlyAssign, string md5, OpMessageData existingFile, bool useTestFolders)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    bool uploadSuccess = false;
                    if (onlyAssign == false)
                        uploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/", useTestFolders ? "MessageTest" : "Message"), filePath, true);
                    else
                        uploadSuccess = true;
                    if (!uploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, "Upload file failed: Unknown error");
                        return null;
                    }
                  //  string tmpDownloadPath = ftpClient.DownloadFile(Path.GetTempPath(), String.Format(@"{0}/{1}/", useTestFolders ? "MessageTest" : "Message", message.IdMessage), Path.GetFileName(filePath));
                    if (!onlyAssign)
                    {
                        if (File.Exists(filePath))
                        {
                            OpFile file = new OpFile();
                            file.PhysicalFileName = Path.GetFileName(filePath);
                            file.Size = new FileInfo(filePath).Length;
                            file.InsertDate = dataProvider.DateTimeNow;
                            file.FileBytes = new byte[1] { 0x00 };
                            long idFile = dataProvider.SaveFile(file);
                            File.Delete(filePath);
                            if (idFile > 0)
                            {

                                OpMessageData toAdd = new OpMessageData();
                                toAdd.IdMessage = message.IdMessage;
                                toAdd.Value = idFile;
                                if (message.DataList != null)
                                    toAdd.Index = message.DataList.GetNextIndex(DataType.MESSAGE_ATTACHMENT);
                                else
                                {
                                    message.DataList = new Objects.OpDataList<OpMessageData>();
                                    toAdd.Index = 0;
                                }
                                toAdd.IdDataType = DataType.MESSAGE_ATTACHMENT;
                                toAdd.OpState = Objects.OpChangeState.New;
                                message.DataList.Add(toAdd);

                                SaveAttachment(message, dataProvider, DataType.MESSAGE_ATTACHMENT);

                                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
                                    LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                                return file;
                            }
                        }
                    }
                    else
                    {
                        long idFile = Convert.ToInt64(existingFile.Value);

                        OpMessageData msgData = new OpMessageData();
                        msgData.IdMessage = message.IdMessage;
                        msgData.IdDataType = DataType.MESSAGE_ATTACHMENT;
                        msgData.IndexNbr = message.DataList.GetNextIndex(DataType.MESSAGE_ATTACHMENT);
                        msgData.OpState = Objects.OpChangeState.New;
                        msgData.Value = idFile;
                        message.DataList.Add(msgData);

                        msgData = new OpMessageData();
                        msgData.IdMessage = message.IdMessage;
                        msgData.IdDataType = DataType.MESSAGE_ATTACHMENT_MD5;
                        msgData.IndexNbr = 0;
                        msgData.OpState = Objects.OpChangeState.New;
                        msgData.Value = md5;

                        ForumComponent.Save(dataProvider, message, true);
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }
            return null;
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpMessage message, string saveFolder, bool deleteFile)
        {
            return DeleteFile(dataProvider, loggedOperator, message, saveFolder, deleteFile, false);
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpMessage message, string saveFolder, bool deleteFile, bool useTestFolders)
        {
            string idFile = "";
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);

                List<OpMessageData> toDeleteList = message.DataList.FindAll(d => d.OpState == Objects.OpChangeState.Delete);

                foreach (OpMessageData mItem in toDeleteList)
                {
                    OpFile fileToDelete = dataProvider.GetFile(Convert.ToInt64(mItem.Value), true);
                    if (deleteFile)
                        ftpClient.DeleteFile(String.Format(@"{0}/{1}/", useTestFolders ? "MessageTest" : "Message", message.IdMessage), fileToDelete.PhysicalFileName);

                    idFile += fileToDelete.IdFile + ",";

                    if (deleteFile)
                        dataProvider.DeleteFile(fileToDelete);
                    dataProvider.DeleteMessageData(mItem);
                    message.DataList.Remove(mItem, true);
                }

                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeleted, LoggedOperator.Login, ServerCORE, file.IdFile);
                    LogSuccess(EventID.Forms.AttachmentDeleted, idFile.Substring(0, idFile.Length - 1));
                return true;
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeletionError, LoggedOperator.Login, ServerCORE, file.IdFile, ex.Message);
                    LogError(EventID.Forms.AttachmentDeletionError, idFile, ex.Message);
                throw ex;
            }
        }

        #endregion
    }
}
