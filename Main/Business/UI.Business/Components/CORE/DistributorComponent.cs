﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DistributorComponent
    {
        #region Methods

        #region Get

        #region GetByID

        public static OpDistributor GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetDistributor(Id);
        }

        #endregion

        #region GetAll

        public static List<OpDistributor> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllDistributor();
        }

        #endregion

        #region LoadData

        public static void LoadData(DataProvider dataProvider, OpDistributor distr)
        {
            if (distr != null)
            {
                distr.DataList.Clear();
                distr.DataList.AddRange(dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { distr.IdDistributor }));
            }
        } 

        #endregion

        #region GetMainDistributor
        public static bool GetMainDistributor(DataProvider dataProvider, int distributorID, out int mainDistributorID, out string errorMsg)
        {
            errorMsg = String.Empty;
            mainDistributorID = 0;

            IMR.Suite.Data.DB.DB_DISTRIBUTOR_HIERARCHY[] hierarchyList;
            try
            {
                hierarchyList = dataProvider.dbConnectionCore.GetDistributorHierarchyFilter(null, null, new int[] { distributorID }, null, null, null);
            }
            catch (Exception ex)
            {
                errorMsg = "Wystąpił wyjątek podczas pobierania hierarchii dystrybutorów. Szczegóły: " + ex.Message;
                return false;
            }
            if (hierarchyList == null || hierarchyList.Length == 0)
            {
                mainDistributorID = distributorID;
                return true;
            }
            //Zwróci nam się hierarchia - np. .128.375.376. - pierwszy element to distributor stanowiący korzeń hierarchii
            mainDistributorID = int.Parse(hierarchyList[0].HIERARCHY.Split('.')[1]);
            return true;
        }

        #endregion
        #region GetDistributorPriority

        public static int? GetDistributorPriority(DataProvider dataProvider, int idDistributor, DateTime? priorityForDate = null, List<OpDistributorData> existingPriorityData = null)
        {
            int? distributorPriority = null;
            if (!priorityForDate.HasValue)
                priorityForDate = dataProvider.DateTimeNow;
            
            if(existingPriorityData == null)
                existingPriorityData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                                                  IdDistributor: new int[]{ idDistributor },
                                                                                                  IdDataType: new long[] { DataType.DISTRIBUTOR_PRIORITY, DataType.DISTRIBUTOR_PRIORITY_FROM, DataType.DISTRIBUTOR_PRIORITY_TO });
            foreach (OpDistributorData ddItem in existingPriorityData.Where(d => d.IdDataType == DataType.DISTRIBUTOR_PRIORITY && d.Value != null))
            {
                OpDistributorData ddFrom = existingPriorityData.Find(d => d.Index == ddItem.Index && d.Value != null && d.IdDataType == DataType.DISTRIBUTOR_PRIORITY_FROM);
                OpDistributorData ddTo = existingPriorityData.Find(d => d.Index == ddItem.Index && d.Value != null && d.IdDataType == DataType.DISTRIBUTOR_PRIORITY_TO);
                DateTime from = DateTime.MinValue;
                DateTime to = DateTime.MinValue;
                if (ddFrom != null && ddTo != null && DateTime.TryParse(ddFrom.Value.ToString(), out from) && DateTime.TryParse(ddTo.Value.ToString(), out to))
                {
                    if (priorityForDate.Value.Month >= from.Month && priorityForDate.Value.Month <= to.Month
                        && priorityForDate.Value.Day >= from.Day && priorityForDate.Value.Day <= to.Day)
                    {
                        int distrPriority = 0;
                        if (int.TryParse(ddItem.Value.ToString(), out distrPriority))
                        {
                            distributorPriority = distrPriority;
                            break;
                        }
                    }
                }
            }


            return distributorPriority;
        }

        #endregion
        #region GetNextIdDistributor

        public static int GetNextIdDistributor(IDataProvider dataProvider)
        {
            Tuple<int, int> distributorIdPool = SystemDataComponent.GetIdDistributorPool(dataProvider);
            OpDistributor dItem = dataProvider.GetDistributorFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted, topCount: 1,
                                                    IdDistributor: new int[] { },
                                                    customWhereClause: String.Format("[ID_DISTRIBUTOR] = (SELECT MAX([ID_DISTRIBUTOR]) FROM [DISTRIBUTOR] WHERE [ID_DISTRIBUTOR] > {0})", distributorIdPool.Item1 - 1))
                                              .FirstOrDefault();

            int nextIdDistributor = dItem == null ? distributorIdPool.Item1 : dItem.IdDistributor + 1;
            if (nextIdDistributor > distributorIdPool.Item2)
                throw new Exception(String.Format("Cannot reserver Id out of range ({0}-{1})", distributorIdPool.Item1, distributorIdPool.Item2));
            return nextIdDistributor;
        }

        #endregion

        #region GetImportDllName

        public static string GetImportDllName(DataProvider dataProvider, int idDistributor, int idReferenceType, string dllName)
        {
            OpDistributor distributor = dataProvider.GetDistributor(idDistributor);

            // Nie ma dystrybutora więc nie możemy zwrócić jakiegokolwiek modułu bo nie wiemy jaki.
            if (distributor == null) { return null; }
            if (distributor.DataList == null) { distributor.DataList = new OpDataList<OpDistributorData>(); }

            List<OpDistributorData> importDllDataList = distributor.DataList.Where(x => x.IdDataType == DataType.DISTRIBUTOR_IMPORT_DLL_NAME || x.IdDataType == DataType.DISTRIBUTOR_IMPORT_DLL_TYPE).ToList();

            // Co jeśli ma załadowaną tylko część?
            if (importDllDataList == null || importDllDataList.Count == 0 || importDllDataList.Count % 2 == 1)
            {
                // Dociągamy te data jeśli ich nie ma lub nie ma odpowiednich par
                List<OpDistributorData> downloadedData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                                                           IdDistributor: new int[] { idDistributor },
                                                                                                           IdDataType: new long[] { DataType.DISTRIBUTOR_IMPORT_DLL_NAME, DataType.DISTRIBUTOR_IMPORT_DLL_TYPE });

                // Uniknięcie duplikatów
                distributor.DataList.RemoveAll(x => x.IdDataType == DataType.DISTRIBUTOR_IMPORT_DLL_NAME || x.IdDataType == DataType.DISTRIBUTOR_IMPORT_DLL_TYPE);
                if (downloadedData != null && downloadedData.Count > 0)
                {
                    distributor.DataList.AddRange(downloadedData);
                }
            }

            string dllNameToLoad = dllName;
            if (string.IsNullOrWhiteSpace(dllNameToLoad))
            {
                OpDistributorData dllRefTypeDistrData = distributor.DataList.FirstOrDefault(x => x.IdDataType == DataType.DISTRIBUTOR_IMPORT_DLL_TYPE && x.Value != null && Convert.ToInt32(x.Value) == idReferenceType);
                OpDistributorData dllNameDistrData = null;

                if (dllRefTypeDistrData != null)
                {
                    dllNameDistrData = distributor.DataList.FirstOrDefault(x => x.IdDataType == DataType.DISTRIBUTOR_IMPORT_DLL_NAME && x.INDEX_NBR == dllRefTypeDistrData.INDEX_NBR);
                }

                if (dllNameDistrData != null && dllNameDistrData.Value != null)
                {
                    dllNameToLoad = dllNameDistrData.Value.ToString();
                }
            }

            if (string.IsNullOrWhiteSpace(dllNameToLoad))
            {
                throw new Exception(ResourcesText.CouldNotFindPluginToImportData);
            }

            return dllNameToLoad;
        }

        #endregion

        #region GetExportDllName

        public static string GetExportDllName(DataProvider dataProvider, int idDistributor, int idReferenceType, string dllName)
        {
            OpDistributor distributor = dataProvider.GetDistributor(idDistributor);

            // Nie ma dystrybutora więc nie możemy zwrócić jakiegokolwiek modułu bo nie wiemy jaki.
            if (distributor == null) { return null; }
            if (distributor.DataList == null) { distributor.DataList = new OpDataList<OpDistributorData>(); }

            List<OpDistributorData> exportDllDataList = distributor.DataList.Where(x => x.IdDataType == DataType.DISTRIBUTOR_EXPORT_DLL_NAME || x.IdDataType == DataType.DISTRIBUTOR_EXPORT_DLL_TYPE).ToList();

            // Co jeśli ma załadowaną tylko część?
            if (exportDllDataList == null || exportDllDataList.Count == 0 || exportDllDataList.Count % 2 == 1)
            {
                // Dociągamy te data jeśli ich nie ma lub nie ma odpowiednich par
                List<OpDistributorData> downloadedData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                                                                                           IdDistributor: new int[] { idDistributor },
                                                                                                           IdDataType: new long[] { DataType.DISTRIBUTOR_EXPORT_DLL_NAME, DataType.DISTRIBUTOR_EXPORT_DLL_TYPE });

                // Uniknięcie duplikatów
                distributor.DataList.RemoveAll(x => x.IdDataType == DataType.DISTRIBUTOR_EXPORT_DLL_NAME || x.IdDataType == DataType.DISTRIBUTOR_EXPORT_DLL_TYPE);
                if (downloadedData != null && downloadedData.Count > 0)
                {
                    distributor.DataList.AddRange(downloadedData);
                }
            }

            string dllNameToLoad = dllName;
            if (string.IsNullOrWhiteSpace(dllNameToLoad))
            {
                OpDistributorData dllRefTypeDistrData = distributor.DataList.FirstOrDefault(x => x.IdDataType == DataType.DISTRIBUTOR_EXPORT_DLL_TYPE && x.Value != null && Convert.ToInt32(x.Value) == idReferenceType);
                OpDistributorData dllNameDistrData = null;

                if (dllRefTypeDistrData != null)
                {
                    dllNameDistrData = distributor.DataList.FirstOrDefault(x => x.IdDataType == DataType.DISTRIBUTOR_EXPORT_DLL_NAME && x.INDEX_NBR == dllRefTypeDistrData.INDEX_NBR);
                }

                if (dllNameDistrData != null && dllNameDistrData.Value != null)
                {
                    dllNameToLoad = dllNameDistrData.Value.ToString();
                }
            }

            if (string.IsNullOrWhiteSpace(dllNameToLoad))
            {
                throw new Exception(ResourcesText.CouldNotFindPluginToExportData);
            }

            return dllNameToLoad;
        }

        #endregion

        #region GetDistributorDepository

        public static long GetDistributorDepositoryId(DataProvider dataProvider, int idDistributor)
        {
            OpDistributor distributor = dataProvider.GetDistributor(idDistributor);
            if (distributor == null)
            {
                throw new ArgumentException(ResourcesText.YouCannotChangeDeviceDistributorIfDistributorHasNoDepositoryAssigned);
            }

            if (!distributor.IdDepositoryMagazine.HasValue || distributor.IdDepositoryMagazine == 0)
            {
                OpDistributorData distributorDepository = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                    IdDataType: new long[] { DataType.DISTRIBUTOR_DEPOSITORY_MAGAZINE_LOCATION_ID },
                    IdDistributor: new int[] { idDistributor }).FirstOrDefault();

                if (distributorDepository == null || distributorDepository.Value == null || Convert.ToInt64(distributorDepository.Value) == 0)
                {
                    throw new ArgumentException(ResourcesText.YouCannotChangeDeviceDistributorIfDistributorHasNoDepositoryAssigned);
                }

                distributor.DataList.Add(distributorDepository);
            }

            return distributor.IdDepositoryMagazine.Value;
        }

        #endregion

        #endregion

        #region Set

        #region SetHelperValues

        public static void SetHelperValues(DataProvider dataProvider, OpDistributor distributor, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdDistributor, false);
            foreach(long lItem in helpersToSet)
            {
                switch(lItem)
                {
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        distributor.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, distributor.IdDistributor);
                        break;
                    case DataType.HELPER_NAME:
                        distributor.DataList.SetValue(DataType.HELPER_NAME, 0, distributor.Name);
                        break;
                    case DataType.HELPER_ADDRESS:
                        distributor.DataList.SetValue(DataType.HELPER_ADDRESS, 0, distributor.Address);
                        break;
                    case DataType.HELPER_CITY:
                        distributor.DataList.SetValue(DataType.HELPER_CITY, 0, distributor.City);
                        break;
                    case DataType.HELPER_ID_DESCR:
                        distributor.DataList.SetValue(DataType.HELPER_ID_DESCR, 0, distributor.IdDescr);
                        break;
                    default:
                        break;
                }
            }
            distributor.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #endregion

        #region Save

        public static bool Add(DataProvider dataProvider, OpDistributor objectToSave, bool addBuiltInActorGroup = false)
        {
            if (dataProvider.AddDistributor(objectToSave))
            {
                if (addBuiltInActorGroup)
                {
                    if (dataProvider.GetActorGroupFilter(IdDistributor: new int[] { objectToSave.IdDistributor }, BuiltIn: true).Count == 0)
                    {
                        OpActorGroup defaultActorGroup = new OpActorGroup();
                        defaultActorGroup.IdDistributor = objectToSave.IdDistributor;
                        defaultActorGroup.Name = String.Format("{0} Address Book", objectToSave.Name);
                        defaultActorGroup.BuiltIn = true;
                        dataProvider.SaveActorGroup(defaultActorGroup);
                    }
                }
                return true;
            }
            return false;
        }

        public static OpDistributor Save(DataProvider dataProvider, OpDistributor objectToSave)
        {
            //check if it's new distributor
            if(objectToSave.IdDistributor == 0)
            {
                objectToSave.IdDistributor = GetNextIdDistributor(dataProvider);
            }
            dataProvider.SaveDistributor(objectToSave);
            SaveData(dataProvider, objectToSave);
            return dataProvider.GetDistributor(objectToSave.IdDistributor);
        }

        public static OpDistributor USave(DataProvider dataProvider, OpDistributor objectToSave)
        {
            //check if it's new distributor
            dataProvider.SaveDistributor(objectToSave);
            DeleteData(dataProvider, objectToSave);
            SaveData(dataProvider, objectToSave);
            dataProvider.ClearDistributorDataCache();
            dataProvider.ClearDistributorCache();
            int idDistr = objectToSave.IdDistributor;
            objectToSave = null;
            return dataProvider.GetDistributor(idDistr, true);
        }

        public static void SaveData(DataProvider dataProvider, OpDistributor objectToSave)
        {
            bool isNewItem = objectToSave.IdDistributor == 0;
            OpDistributorData data = null;
            for (int i = 0; i < objectToSave.DataList.Count; i++)
            {
                data = objectToSave.DataList[i];
                data.IdDistributor = objectToSave.IdDistributor;
                if (isNewItem || dataProvider.GetDataType(data.IdDataType).IsEditable)
                {
                    objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                }
            }
        }

        #endregion

        #region Delete

        public static void DeleteData(DataProvider dataProvider, OpDistributor objectToSave)
        {
            bool isNewItem = objectToSave.IdDistributor == 0;
            if (!isNewItem)
            {
                OpDistributorData data = null;
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    { 
                        data = objectToSave.DataList[i];
                        if (dataProvider.GetDataType(data.IdDataType).IsEditable)
                            dataProvider.DeleteDistributorData(data);
                    }
                }
            }
            for (int i = 0; i < objectToSave.DataList.Count; i++)
            {
                if (dataProvider.GetDataType(objectToSave.DataList[i].IdDataType).IsEditable)
                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                        objectToSave.DataList.RemoveAt(i, true);
            }
        }

        public static void Delete(DataProvider dataProvider, OpDistributor objectToDelete)
        {
            if (objectToDelete.DataList != null && objectToDelete.DataList.Count > 0)
                objectToDelete.DataList.ForEach(d => d.OpState = OpChangeState.Delete);
            DeleteData(dataProvider, objectToDelete);
            dataProvider.DeleteDistributor(objectToDelete);
        }

        #endregion

        #endregion
    }
}
