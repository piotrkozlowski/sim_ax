﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LocationSupplierComponent
    {
        #region Methods

        public static OpLocationSupplier Save(DataProvider dataProvider, OpLocationSupplier objectToSave)
        {
            dataProvider.SaveLocationSupplier(objectToSave);
            return dataProvider.GetLocationSupplier(objectToSave.IdLocationSupplier);
        }

        public static void Delete(DataProvider dataProvider, OpLocationSupplier objectToDelete)
        {
            dataProvider.DeleteLocationSupplier(objectToDelete);
        }

        public static List<OpLocationSupplier> GetSuppliersForLocation(DataProvider dataProvider, OpLocation location)
        {
            return dataProvider.GetAllLocationSupplier().FindAll(e => e.ID_LOCATION == location.ID_LOCATION);
        }

        public static List<OpLocationSupplier> GetLocationsForSupplier(DataProvider dataProvider, OpDistributorSupplier supplier)
        {
            return dataProvider.GetAllLocationSupplier().FindAll(e => e.ID_DISTRIBUTOR_SUPPLIER == supplier.ID_DISTRIBUTOR_SUPPLIER);
        }

        public static OpLocationSupplier GetCurrentLocationSupplier(DataProvider dataProvider, OpLocation location)
        {
            return dataProvider.GetAllLocationSupplier().Find(e => e.Location == location && e.EndTime == null);
        }

        public static int TerminateActiveTariffsForLocation(DataProvider dataProvider, OpLocation location)
        {
            int i = 0;
            foreach (var loop in dataProvider.GetAllLocationSupplier())
            {
                if (loop.Location == location && loop.EndTime == null)
                {
                    loop.EndTime = dataProvider.DateTimeNow;
                    dataProvider.SaveLocationSupplier(loop);
                    i++;
                }
            }
            return i;
        }

        #endregion
    }
    /*
    static class EnumerableExtensions
    {
        public static OpLocationSupplier GetCurrent<T>(this List<T> list) where T : OpLocationSupplier
        {
            OpLocationSupplier ret = null;

            foreach (T data in list)
            {
                if ((T as OpLocationSupplier).EndTime == null)
            }
            return String.Join(delimiter, items.ToArray());
        }
    }*/
}


