﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.DW;

using OpDataUtil = IMR.Suite.UI.Business.Objects.OpDataUtil; 
//using OpDataUtil = IMR.Suite.UI.Business.Objects.Domain.OpDataUtil;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ActivityComponent
    {

        //#region HasPermission
        //public static bool HasPermission(OpOperator opOperator, int idActivity, object referenceValue = null)
        //{
        //    if (opOperator.Activities != null)
        //    {
        //        if (referenceValue == null) //szukamy czy operator ma zdefiniowane idActivity i nie jest ustawione Deny na True
        //        {
        //            return opOperator.Activities.Any(a => a.IdActivity == idActivity && !a.Deny) && !opOperator.Activities.Any(a => a.IdActivity == idActivity && a.Deny);
        //        }
        //        else
        //        {
        //            //podano ID obiektu do ktorego mamy sprawdzić uprawnienie
        //            List<OpActivity> activites = opOperator.Activities.Where(w => w.IdActivity == idActivity).ToList();
        //            if (activites.Any(w => OpDataUtil.ValueEquals(w.ReferenceValue, referenceValue) && w.Deny == true))
        //            {
        //                //zdefiniowane uprawnienie z tym referenceValue z Deny = True
        //                return false;
        //            }
        //            else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
        //            {
        //                //wszystkie elementy sa wykluczone
        //                return false;
        //            }
        //            else if (activites.Any(w => w.ReferenceValue == null && w.Deny == false))
        //            {
        //                //wszystkie elementy dozwolone
        //                return true;
        //            }
        //            else if (activites.Any(w => OpDataUtil.ValueEquals(w.ReferenceValue, referenceValue) && w.Deny == false))
        //            {
        //                //zdefiniowane uprawnienie z tym referenceValue z Deny = False
        //                return true;
        //            }
        //        }
        //    }
        //    return false; //jesli nie ma zdefiniowanych uprawnień
        //}
        //#endregion

        #region GetAllowedActivities
        public static List<OpActivity> GetAllowedActivities(List<OpActivity> activities)
        {
            if (activities == null) return new List<OpActivity>();
            if (activities.Count == 0) return activities;

            Dictionary<int, int> allowedIdActivities = new Dictionary<int, int>();
            foreach (var groupedActivity in activities.Where(q => !q.IdReferenceType.HasValue).GroupBy(q => q.IdActivity))
            {
                if (groupedActivity.Any(q => !q.Deny) && !groupedActivity.Any(q => q.Deny))
                    allowedIdActivities[groupedActivity.Key] = groupedActivity.Key;
            }
            foreach (var groupedActivity in activities.Where(q => q.IdReferenceType.HasValue).GroupBy(q => q.IdActivity))
            {
                if (!groupedActivity.Any(q => q.ReferenceValue == null && q.Deny) 
                    && (
                        groupedActivity.Any(q => q.ReferenceValue == null && !q.Deny)
                        || groupedActivity.Any(q => q.ReferenceValue != null && !q.Deny))
                    )
                    allowedIdActivities[groupedActivity.Key] = groupedActivity.Key;
            }
            return activities.Where(q => allowedIdActivities.ContainsKey(q.IdActivity)).ToList();
        }
        #endregion

        #region FilterByPermission
        public static List<T> FilterByPermission<T>(List<T> objectList, int idActivity, OpOperator _operator) where T : class, IReferenceType
        {
            List<T> allowedList = new List<T>();

            List<OpActivity> activites = _operator.Activities.Where(w => w.IdActivity == idActivity).ToList();
            if (activites.Count == 0)
            {
                // Brak, niewykonifugrowane activites
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
            {
                //ustawione Deny na All
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null) && activites.Any(w => w.Deny == true))
            {
                // wszystkie + wykluczone
                List<OpActivity> disallowedActivites = activites.Where(w => w.Deny == true).ToList();
                List<T> disallowedObjects = objectList.Join(disallowedActivites, w => w.GetReferenceKey(), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();
                allowedList = objectList.Except(disallowedObjects).ToList();
            }
            else if (activites.Any(w => w.ReferenceValue == null))
            {
                // wszystkie
                allowedList = objectList;
            }
            else
            {
                // tylko wyspecyfikowane
                allowedList = objectList.Join(activites, w => w.GetReferenceKey(), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();
            }
            return allowedList;
        }
        public static List<T> FilterByPermission<T>(List<T> objectList, string referenceKeyProperty, int idActivity, OpOperator _operator) where T : class, IReferenceType
        {
            List<T> allowedList = new List<T>();

            List<OpActivity> activites = _operator.Activities.Where(w => w.IdActivity == idActivity).ToList();
 //           activites = activites.Distinct(d => new { IdActivity = d.IdActivity, Deny = d.Deny, ReferenceType = d.IdReferenceType, ReferenceValue = d.ReferenceValue }).ToList();

            if (activites.Count == 0)
            {
                // Brak, niewykonifugrowane activites
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null && w.Deny == true))
            {
                //ustawione Deny na All
                return allowedList;
            }
            else if (activites.Any(w => w.ReferenceValue == null) && activites.Any(w => w.Deny == true))
            {
                // wszystkie + wykluczone
                List<OpActivity> disallowedActivites = activites.Where(w => w.Deny == true).ToList();
                List<T> disallowedObjects = objectList.Join(disallowedActivites, w => GetValue(w, referenceKeyProperty), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).ToList();
                allowedList = objectList.Except(disallowedObjects).ToList();
            }
            else if (activites.Any(w => w.ReferenceValue == null))
            {
                // wszystkie
                allowedList = objectList;
            }
            else
            {
                // tylko wyspecyfikowane
                allowedList = objectList.Join(activites, w => GetValue(w, referenceKeyProperty), q => q.ReferenceValue, (w, q) => w, new ReferenceValueComparer()).Distinct().ToList();
            }
            return allowedList;
        }
        public static object GetValue(object objectItem, string propertyName)
        {
            if (objectItem != null && propertyName != null)
            {
                string[] properties = propertyName.Split('.');
                int propertyCount = 0;
                Type typeSource = objectItem.GetType();
                PropertyInfo propertyFounded = null;
                while (propertyCount < properties.Length)
                {
                    propertyFounded = typeSource.GetProperties().FirstOrDefault(p => p.Name == properties[propertyCount]);
                    if (propertyCount < properties.Length - 1 && propertyFounded != null)
                    {
                        objectItem = objectItem.GetType().GetProperty(properties[propertyCount]).GetValue(objectItem, null);
                        typeSource = propertyFounded.PropertyType;
                    }
                    propertyCount++;
                }
                if (propertyFounded != null)
                {
                    return propertyFounded.GetValue(objectItem, null);
                }
            }
            return null;
        }
        #endregion 

        #region class ReferenceValueComparer
        internal class ReferenceValueComparer : IEqualityComparer<object>
        {
            bool IEqualityComparer<object>.Equals(object x, object y)
            {
                return OpDataUtil.ValueEquals(x, y);
            }
            public int GetHashCode(object obj)
            {
                return obj.GetHashCode();
            }
        }
        #endregion
    }
}
