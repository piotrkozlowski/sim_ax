﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LocationTariffComponent
    {
        #region Methods

        public static OpLocationTariff Save(DataProvider dataProvider, OpLocationTariff objectToSave)
        {
            dataProvider.SaveLocationTariff(objectToSave);
            return dataProvider.GetLocationTariff(objectToSave.IdLocationTariff);
        }

        public static void Delete(DataProvider dataProvider, OpLocationTariff objectToDelete)
        {
            dataProvider.DeleteLocationTariff(objectToDelete);
        }

        public static List<OpLocationTariff> GetTarrifsForLocation(DataProvider dataProvider, OpLocation location)
        {
            return dataProvider.GetLocationTariffFilter(IdLocation: new long[] {location.IdLocation});
        }

        public static List<OpLocationTariff> GetLocationsForTariff(DataProvider dataProvider, OpTariff tariff)
        {
            return dataProvider.GetLocationTariffFilter(IdTariff: new int[] { tariff.IdTariff});
        }

        public static OpLocationTariff GetCurrentLocationTariff(DataProvider dataProvider, OpLocation location)
        {
            return dataProvider.GetAllLocationTariff().Find(e => e.Location == location && e.EndTime == null);
        }

        public static OpTariff GetHistoryTariffForLocation(List<OpLocationTariff> hierarchy, OpLocation location, DateTime date)
        {
            OpLocationTariff tl = hierarchy.Find(n => n.Location == location && n.StartTime < date && n.EndTime >= date);

            if (tl == null)
            {
                tl = hierarchy.Find(n => n.Location == location && n.StartTime < date && n.EndTime == null);
            }

            return (tl == null) ? null : tl.Tariff;
        }

        public static int TerminateActiveTariffsForLocation(DataProvider dataProvider, OpLocation location, DateTime endTime)
        {
            int i = 0;
            foreach (var loop in dataProvider.GetAllLocationTariff())
            {
                if (loop.Location == location && loop.EndTime == null)
                {
                    loop.EndTime = endTime;
                    dataProvider.SaveLocationTariff(loop);
                    i++;
                }
            }
            return i;
        }
        #endregion
    }
}
