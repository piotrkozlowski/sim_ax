﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
	public class DataTypeGroupComponent
	{
        #region Methods
        public static List<OpDataTypeInGroup> GetDataTypeInGroup(DataProvider dataProvider, string dataTypeGroupName, int? ReferenceType = null, long? ReferenceValue = null, bool queryDatabase = false)
		{
			List<OpDataTypeInGroup> dataTypeInGroup = new List<OpDataTypeInGroup>();
            List<OpDataTypeInGroup> dataTypeInParentGroup = new List<OpDataTypeInGroup>();

			OpDataTypeGroup group = null;
            if (ReferenceType == null || ReferenceValue == null)
            {
                if (!queryDatabase)
                    group = dataProvider.GetAllDataTypeGroup().Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == null && w.ReferenceValue == null);
                else
                    group = dataProvider.GetDataTypeGroupFilter(loadNavigationProperties: true, mergeIntoCache: true, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted)
                                                                 .Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == null && w.ReferenceValue == null);
            }
            else
            {
                if(!queryDatabase)
                    group = dataProvider.GetAllDataTypeGroup().Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == ReferenceType && ReferenceValue != null && Convert.ToInt64(w.ReferenceValue) == ReferenceValue.Value);
                else
                    group = dataProvider.GetDataTypeGroupFilter(loadNavigationProperties: true, mergeIntoCache: true, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted)
                                                                .Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == ReferenceType && ReferenceValue != null && Convert.ToInt64(w.ReferenceValue) == ReferenceValue.Value);
                if (group == null)
                {
                    if (!queryDatabase)
                        group = dataProvider.GetAllDataTypeGroup().Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == null && w.ReferenceValue == null);
                    else
                        group = dataProvider.GetDataTypeGroupFilter(loadNavigationProperties: true, mergeIntoCache: true, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted)
                                                                 .Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == null && w.ReferenceValue == null);
                }
            }
			if (group == null)
				return dataTypeInGroup;

			dataTypeInGroup = dataProvider.GetDataTypeInGroup(group.IdDataTypeGroup, queryDatabase);
			if (group.IdParentGroup == null)
				return dataTypeInGroup;

			dataTypeInParentGroup = dataProvider.GetDataTypeInGroup(group.IdParentGroup.Value, queryDatabase);

			foreach (OpDataTypeInGroup item in dataTypeInGroup)
			{
				OpDataTypeInGroup itemInParent = dataTypeInParentGroup.Find(w => w.IdDataType == item.IdDataType);
				if (itemInParent != null) // jest w grupie i w parent grupie
				{
					// uaaktualniamy tego co jest w parent gruop, jesli w przyszlosci w OpDataTypeInGroup bedzie wiecej niz IdDataType
					int idx = dataTypeInParentGroup.IndexOf(itemInParent);
					dataTypeInParentGroup.Remove(itemInParent);
					dataTypeInParentGroup.Insert(idx, item);
				}
				else
				{
					dataTypeInParentGroup.Add(item);
				}
			}

			return dataTypeInParentGroup;
		}
        /// <summary>
        /// Gets common data types from data type groups for specified Reference type and specified Reference values and data types from parent data type groups
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="dataTypeGroupName"></param>
        /// <param name="ReferenceType"></param>
        /// <param name="ReferenceValues"></param>
        /// <returns></returns>
        public static List<OpDataType> GetCommonDataTypes(DataProvider dataProvider, string dataTypeGroupName, int ReferenceType, long[] ReferenceValues)
        {
            List<OpDataType> dataTypes = new List<OpDataType>();

            List<OpDataTypeInGroup> dataTypeInGroup = new List<OpDataTypeInGroup>();
            
            List<OpDataTypeGroup> groups = null;

            // szukanie po name, reference type oraz wartosciach dla tego reference type
            if (ReferenceValues != null && ReferenceValues.Length > 0)
                groups = dataProvider.GetAllDataTypeGroup().Where(w => w.Name == dataTypeGroupName && w.IdReferenceType == ReferenceType && w.ReferenceValue != null && ReferenceValues.Contains(Convert.ToInt64(w.ReferenceValue))).ToList();
            // szukanie po name i reference type
            else
                groups = dataProvider.GetAllDataTypeGroup().Where(w => w.Name == dataTypeGroupName && w.IdReferenceType == ReferenceType).ToList();

            // nie ma wynikow dla danego reference type to szukamy parent group po name i reference type = null
            if (groups == null || groups.Count == 0)
                groups = dataProvider.GetAllDataTypeGroup().Where(w => w.Name == dataTypeGroupName && !w.IdReferenceType.HasValue).ToList();

            if (groups == null || groups.Count == 0)
                return dataTypes;

            dataTypeInGroup = dataProvider.GetDataTypeInGroup(groups.Select(q => q.IdDataTypeGroup).ToArray());
            if (groups.Count > 1)
                dataTypes = dataTypeInGroup.GroupBy(q => q.IdDataType).Where(q => q.Count() > 1).Select(q => q.First().DataType).ToList();
            else
                dataTypes = dataTypeInGroup.Select(q => q.DataType).ToList();

            if (groups.All(q => !q.IdParentGroup.HasValue))
                return dataTypes;

            List<OpDataType> parentDataTypes = new List<OpDataType>();
            parentDataTypes = dataProvider.GetDataTypeInGroup(groups.Where(q => q.IdParentGroup.HasValue).Select(q => q.IdParentGroup.Value).Distinct().ToArray()).Select(q => q.DataType).Distinct(q => q.IdDataType).ToList();

            foreach (OpDataType item in dataTypes)
            {
                OpDataType itemInParent = parentDataTypes.Find(w => w.IdDataType == item.IdDataType);
                if (itemInParent != null) // jest w grupie i w parent grupie
                {
                    // uaaktualniamy tego co jest w parent gruop, jesli w przyszlosci w OpDataTypeInGroup bedzie wiecej niz IdDataType
                    int idx = parentDataTypes.IndexOf(itemInParent);
                    parentDataTypes.Remove(itemInParent);
                    parentDataTypes.Insert(idx, item);
                }
                else
                {
                    parentDataTypes.Add(item);
                }
            }

            return parentDataTypes;
        }
        public static List<OpDataType> GetDataTypes(DataProvider dataProvider, string dataTypeGroupName, int? ReferenceType = null, long? ReferenceValue = null)
        {
            return GetDataTypeInGroup(dataProvider, dataTypeGroupName, ReferenceType, ReferenceValue).Select(d => d.DataType).ToList();
        }
        public static void DeleteDataTypeGroup(DataProvider dataProvider, OpDataTypeGroup dataTypeGroupToRemove)
        {
            List<OpDataTypeGroup> subGroupsToRemove = dataProvider.GetDataTypeGroupFilter(IdParentGroup: new int[] { dataTypeGroupToRemove.IdDataTypeGroup });
            List<OpDataTypeInGroup> dataTypesInGroupsToRemove = dataProvider.GetDataTypeInGroup(subGroupsToRemove.Select(s => s.IdDataTypeGroup).ToArray());
            dataTypesInGroupsToRemove.AddRange(dataProvider.GetDataTypeInGroup(dataTypeGroupToRemove.IdDataTypeGroup));

            foreach (OpDataTypeInGroup dataTypeInGroupToRemove in dataTypesInGroupsToRemove)
                dataProvider.DeleteDataTypeInGroup(dataTypeInGroupToRemove);

            foreach(OpDataTypeGroup groupToRemove in subGroupsToRemove)
                dataProvider.DeleteDataTypeGroup(groupToRemove);

            dataProvider.DeleteDataTypeGroup(dataTypeGroupToRemove);
        }
        public static void DeleteDataTypeGroup(DataProvider dataProvider, string dataTypeGroupName, int? ReferenceType = null, long? ReferenceValue = null)
        {
            OpDataTypeGroup groupToRemove = null;
            if (ReferenceType == null || ReferenceValue == null)
                groupToRemove = dataProvider.GetAllDataTypeGroup().Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == null && w.ReferenceValue == null);
            else
                groupToRemove = dataProvider.GetAllDataTypeGroup().Find(w => w.Name == dataTypeGroupName && w.IdReferenceType == ReferenceType && ReferenceValue != null && Convert.ToInt64(w.ReferenceValue) == ReferenceValue.Value);

            if (groupToRemove != null)
                DeleteDataTypeGroup(dataProvider, groupToRemove);
        }

        /// <summary>
        /// Gets data types in group allowing alternative reference types and/or values
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="dataTypeGroupName"></param>
        /// <param name="referenceTypeValue">Dictionary of IdReferenceType + ReferenceValue, first item with matching group will be treated as base search element</param>
        /// <returns></returns>
        public static List<OpDataTypeInGroup> GetDataTypeInGroup(DataProvider dataProvider, string dataTypeGroupName, Dictionary<int, long> referenceTypeValue)
        {
            List<OpDataTypeInGroup> result = new List<OpDataTypeInGroup>();

            List<OpDataTypeGroup> allGroups = dataProvider.GetAllDataTypeGroup().FindAll(q => q.Name.Equals(dataTypeGroupName));
            OpDataTypeGroup baseGroup = null;
            if (referenceTypeValue != null)
                foreach (var option in referenceTypeValue)
                {
                    try
                    {
                        baseGroup = allGroups.Find(q => q.IdReferenceType == option.Key && Convert.ToInt64(q.ReferenceValue) == option.Value);
                        if (baseGroup != null)
                            break;
                    }
                    catch { continue; }
                }
            if (baseGroup == null)
                baseGroup = allGroups.Find(q => !q.IdReferenceType.HasValue && q.ReferenceValue == null);

            List<OpDataTypeGroup> groups = new List<OpDataTypeGroup>();
            while (baseGroup != null)
            {
                groups.Add(baseGroup);
                baseGroup = allGroups.Find(q => q.IdDataTypeGroup == baseGroup.IdParentGroup);
            }
            List<OpDataTypeInGroup> allDTiG = dataProvider.GetDataTypeInGroup(groups.Select(q => q.IdDataTypeGroup).ToArray());
            foreach (OpDataTypeGroup group in groups)
            {
                if (dataProvider.DataTypeInGroupDict.ContainsKey(group.IdDataTypeGroup))
                {
                    foreach (OpDataTypeInGroup dtig in dataProvider.DataTypeInGroupDict[group.IdDataTypeGroup])
                        if (!result.Exists(q => q.IdDataType == dtig.IdDataType))
                            result.Add(dtig);
                }
            }

            return result;
        }
	    #endregion
	}
}
