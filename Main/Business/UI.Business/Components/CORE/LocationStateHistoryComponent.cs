﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LocationStateHistoryComponent
    {
		public static void Save(DataProvider dataProvider, List<OpLocationStateHistory> states, bool useDBCollector = false)
		{
			for (int i = states.Count - 1; i >= 0; i--)
			{
				if (states[i].OpState == OpChangeState.Delete)
				{
					dataProvider.DeleteLocationStateHistory(states[i], useDBCollector);
					states.RemoveAt(i);
				}
			}

			foreach(OpLocationStateHistory state in states.Where(w => w.OpState == Objects.OpChangeState.New || w.OpState == OpChangeState.Modified))
			{
				state.IdLocationState = dataProvider.SaveLocationStateHistory(state, useDBCollector);
				if (state.OpState == OpChangeState.New)
					state.AssignReferences(dataProvider);
				state.OpState = OpChangeState.Loaded;
			}
		}

        public static void Save(DataProvider dataProvider, OpLocationStateHistory state, bool useDBCollector = false)
        {
            dataProvider.SaveLocationStateHistory(state, useDBCollector);
			state.OpState = OpChangeState.Loaded;
        }


        public static void CloseActiveEntries(DataProvider dataProvider, long idLocation, bool useDBCollector = false)
        {
            foreach (var loop in dataProvider.GetLocationStateHistoryFilter(IdLocation: new[] { idLocation }))//  GetLocationStateHistoryForLocation(idLocation))
            {
                if (!loop.EndDate.HasValue)
                {
                    loop.EndDate = dataProvider.DateTimeNow;
                    Save(dataProvider, loop, useDBCollector);
                }
            }
        }
    }
}
