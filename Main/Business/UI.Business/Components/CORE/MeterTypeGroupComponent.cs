﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class MeterTypeGroupComponent
    {
        public static bool IsMeterInGroup(DataProvider dataProvider, Enums.MeterTypeGroup groupName, int idMeter)
        {
            OpMeterTypeGroup group = dataProvider.GetAllMeterTypeGroup().Find(n => n.Name == groupName.ToString());
            if (group == null)
                throw new Exception("Group name " + groupName + " was not found in database.");

            return dataProvider.GetMeterTypeInGroup(group.IdMeterTypeGroup).Find(n => n.IdMeterType == idMeter) != null;
        }
    }
}
