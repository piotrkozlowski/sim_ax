﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ProductCodeComponent
    {
        #region Methods
        public static OpProductCode GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetProductCode(Id);
        }

        public static List<OpProductCode> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllProductCode();
        }

        public static OpProductCode Save(DataProvider dataProvider, OpProductCode objectToSave)
        {
            bool isNewItem = objectToSave.IdProductCode == 0;
            dataProvider.SaveProductCode(objectToSave);
            return dataProvider.GetProductCode(objectToSave.IdProductCode);
        }

        public static void Delete(DataProvider dataProvider, OpProductCode objectToDelete)
        {
            dataProvider.DeleteProductCode(objectToDelete);
        }
        #endregion
    }
}
