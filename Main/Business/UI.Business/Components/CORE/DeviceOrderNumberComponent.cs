﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using System.IO;
using System.Security.Cryptography;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DeviceOrderNumberComponent : BaseComponent
    {
        #region Files
        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpDeviceOrderNumber deviceOrderNumber, OpFile file, string savePath, string saveFolder)
        {
            return DownloadFile(dataProvider, loggedOperator, deviceOrderNumber, file, savePath, saveFolder, false);
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpDeviceOrderNumber deviceOrderNumber, OpFile file, string savePath, string saveFolder, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                string filePathDownloaded = ftpClient.DownloadFile(savePath, String.Format(@"{0}/{1}/", useTestFolders ? "DeviceOrderNumberTest" : "DeviceOrderNumber", saveFolder), file.PhysicalFileName);
                if (filePathDownloaded != null)
                {
                    file.LastDownloaded = dataProvider.DateTimeNow;
                    dataProvider.SaveFile(file);

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, List<OpDeviceOrderNumber> deviceOrderNumber, string filePath, string saveFolder, string md5)
        {
            return UploadFile(dataProvider, loggedOperator, deviceOrderNumber, filePath, saveFolder, md5, false);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, List<OpDeviceOrderNumber> deviceOrderNumber, string filePath, string saveFolder, string md5, bool useTestFolders)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    bool uploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/", useTestFolders ? "DeviceOrderNumberTest" : "DeviceOrderNumber", saveFolder), filePath, true);
                    if (!uploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, "Upload file failed: Unknown error");
                        return null;
                    }
                    string tmpDownloadPath = ftpClient.DownloadFile(Path.GetTempPath(), String.Format(@"{0}/{1}/", useTestFolders ? "DeviceOrderNumberTest" : "DeviceOrderNumber", saveFolder), Path.GetFileName(filePath));
                    

                    if (File.Exists(tmpDownloadPath))
                    {
                        

                        OpFile file = new OpFile();
                        file.PhysicalFileName = Path.GetFileName(filePath);
                        file.Size = new FileInfo(filePath).Length;
                        file.InsertDate = dataProvider.DateTimeNow;
                        file.FileBytes = new byte[1] { 0x00 };
                        long idFile = dataProvider.SaveFile(file);
                        File.Delete(tmpDownloadPath);
                        if (idFile > 0)
                        {
                            foreach (OpDeviceOrderNumber donItem in deviceOrderNumber)
                            {
                                int indexNbr = 0;
                                OpDeviceOrderNumberData toAdd = new OpDeviceOrderNumberData();
                                toAdd.IdDeviceOrderNumber = donItem.IdDeviceOrderNumber;
                                toAdd.Value = idFile;
                                if (donItem.DataList != null)
                                    toAdd.IndexNbr = donItem.DataList.GetNextIndex(DataType.DEVICE_ORDER_NUMBER_ATTACHMENT);
                                else
                                {
                                    donItem.DataList = new Objects.OpDataList<OpDeviceOrderNumberData>();
                                    toAdd.IndexNbr = 0;
                                }
                                toAdd.IdDataType = DataType.DEVICE_ORDER_NUMBER_ATTACHMENT;
                                toAdd.OpState = Objects.OpChangeState.New;
                                donItem.DataList.Add(toAdd);
                                indexNbr = toAdd.IndexNbr;

                                toAdd = new OpDeviceOrderNumberData();
                                toAdd.IdDeviceOrderNumber = donItem.IdDeviceOrderNumber;
                                toAdd.IndexNbr = indexNbr;
                                toAdd.IdDataType = DataType.DEVICE_ORDER_NUMBER_FILE_TYPE;
                                toAdd.OpState = Business.Objects.OpChangeState.Loaded;
                                toAdd.Value = saveFolder.ToUpper();
                                toAdd.IdDeviceOrderNumberData = dataProvider.SaveDeviceOrderNumberData(toAdd);
                                donItem.DataList.Add(toAdd);

                                toAdd = new OpDeviceOrderNumberData();
                                toAdd.IdDeviceOrderNumber = donItem.IdDeviceOrderNumber;
                                toAdd.IndexNbr = indexNbr;
                                toAdd.IdDataType = DataType.DEVICE_ORDER_NUMBER_FILE_MD5;
                                toAdd.OpState = Business.Objects.OpChangeState.Loaded;
                                toAdd.Value = md5;
                                toAdd.IdDeviceOrderNumberData = dataProvider.SaveDeviceOrderNumberData(toAdd);
                                donItem.DataList.Add(toAdd);

                                SaveAttachment(donItem, dataProvider, DataType.DEVICE_ORDER_NUMBER_ATTACHMENT);
                            }
                            if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
                                LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                            return file;
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }
            return null;
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpDeviceOrderNumber deviceOrderNumber, string saveFolder, bool deleteFile)
        {
            return DeleteFile(dataProvider, loggedOperator, deviceOrderNumber, saveFolder, deleteFile, false);
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpDeviceOrderNumber deviceOrderNumber, string saveFolder, bool deleteFile, bool useTestFolders)
        {
            string idFile = "";
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);

                List<OpDeviceOrderNumberData> toDeleteList = deviceOrderNumber.DataList.FindAll(d => d.OpState == Objects.OpChangeState.Delete);

                foreach (OpDeviceOrderNumberData donItem in toDeleteList)
                {
                    OpFile fileToDelete = dataProvider.GetFile(Convert.ToInt64(donItem.Value), true);
                    if(deleteFile)
                        ftpClient.DeleteFile(String.Format(@"{0}/{1}/", useTestFolders ? "DeviceOrderNumberTest" : "DeviceOrderNumber", saveFolder), fileToDelete.PhysicalFileName);

                    idFile += fileToDelete.IdFile + ",";

                    if (deleteFile)
                        dataProvider.DeleteFile(fileToDelete);
                    dataProvider.DeleteDeviceOrderNumberData(donItem);
                    List<OpDeviceOrderNumberData> dondListToDelete = dataProvider.GetDeviceOrderNumberDataFilter(IdDeviceOrderNumber: new int[] { donItem.IdDeviceOrderNumber },
                        IndexNbr: new int[] { donItem.IndexNbr }, IdDataType: new long[] { DataType.DEVICE_ORDER_NUMBER_FILE_TYPE, DataType.DEVICE_ORDER_NUMBER_FILE_MD5 });
                    if (dondListToDelete != null)
                    {
                        foreach (OpDeviceOrderNumberData dondToDel in dondListToDelete)
                            dataProvider.DeleteDeviceOrderNumberData(dondToDel);
                    }
                    deviceOrderNumber.DataList.Remove(donItem, true);
                }

                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeleted, LoggedOperator.Login, ServerCORE, file.IdFile);
                    LogSuccess(EventID.Forms.AttachmentDeleted, idFile.Substring(0, idFile.Length - 1));
                return true;
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeletionError, LoggedOperator.Login, ServerCORE, file.IdFile, ex.Message);
                    LogError(EventID.Forms.AttachmentDeletionError, idFile, ex.Message);
                throw ex;
            }
        }

        public static List<long> FindFile(DataProvider dataProvider, string folderName, string fileName, bool useTestFolders)
        {
            List<long> retList = new List<long>();
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                List<string> fileList = ftpClient.GetFileList(String.Format(@"{0}/{1}/", useTestFolders ? "DeviceOrderNumberTest" : "DeviceOrderNumber", folderName));
                if (fileList != null && fileList.Count > 0)
                {
                    foreach (string sItem in fileList)
                    {
                        if(sItem.ToUpper().Contains(fileName.ToUpper()))
                        //if (String.Equals(sItem, fileName))
                        {
                            List<OpFile> fileLiest = dataProvider.GetFileFilter(PhysicalFileName: sItem);
                            foreach (OpFile fItem in fileLiest)
                            {
                                List<OpDeviceOrderNumberData> existingConnections = dataProvider.GetDeviceOrderNumberDataFilter(IdDataType: new long[] { DataType.DEVICE_ORDER_NUMBER_ATTACHMENT },
                                    customWhereClause: "[VALUE] = " + fItem.IdFile);
                                if (existingConnections != null && existingConnections.Count > 0)
                                    retList.Add(fItem.IdFile);
                            }
                        }
                    }
                }
                return retList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<OpFile> FindFileAllDirectories(DataProvider dataProvider, string fileName, bool useTestFolders)
        {
            List<long> existngConnections = null;
            List<OpFile> fileList = new List<OpFile>();
            List<OpFile> fileListTmp = new List<OpFile>();
#if DEBUG
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "ATEX", fileName, useTestFolders);
#else
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "ATEX", fileName, useTestFolders);
#endif
            if (existngConnections != null && existngConnections.Count > 0)
            {
                fileListTmp = dataProvider.GetFile(existngConnections.ToArray(), true);
                fileList.AddRange(fileListTmp);
            }
#if DEBUG
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "Leaflet", fileName, useTestFolders);
#else
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "Leaflet", fileName, useTestFolders);
#endif
            if (existngConnections != null && existngConnections.Count > 0)
            {
                fileListTmp = dataProvider.GetFile(existngConnections.ToArray(), true);
                fileList.AddRange(fileListTmp);
            }
#if DEBUG
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "DataSheet", fileName, useTestFolders);
#else
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "DataSheet", fileName, useTestFolders);
#endif
            if (existngConnections != null && existngConnections.Count > 0)
            {
                fileListTmp = dataProvider.GetFile(existngConnections.ToArray(), true);
                fileList.AddRange(fileListTmp);
            }

#if DEBUG
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "DZWE", fileName, useTestFolders);
#else
            existngConnections = DeviceOrderNumberComponent.FindFile(dataProvider, "DZWE", fileName, useTestFolders);
#endif
            if (existngConnections != null && existngConnections.Count > 0)
            {
                fileListTmp = dataProvider.GetFile(existngConnections.ToArray(), true);
                fileList.AddRange(fileListTmp);
            }

            return fileList;
        }

        #endregion

        #region Save
        public static OpDeviceOrderNumber AddDeviceOrderNumber(DataProvider dataProvider, string name, string firstQuarter, string secondQuarter, string thirdQuarter)
        {
            OpDeviceOrderNumber donItem = dataProvider.GetDeviceOrderNumberFilter(loadCustomData: false, loadNavigationProperties: false,
                                                                                  Name: name,
                                                                                  FirstQuarter: firstQuarter,
                                                                                  SecondQuarter: secondQuarter,
                                                                                  ThirdQuarter: thirdQuarter).FirstOrDefault();
            if (donItem == null)
            {
                donItem = new OpDeviceOrderNumber();
                donItem.Name = name;
                donItem.FirstQuarter = firstQuarter;
                donItem.SecondQuarter = secondQuarter;
                donItem.ThirdQuarter = thirdQuarter;
                donItem.IdDeviceOrderNumber = dataProvider.SaveDeviceOrderNumber(donItem);
            }

           return donItem;
        }

        public static void SaveAttachment(OpDeviceOrderNumber deviceOrderNumber, DataProvider dataProvider, long dataType)
        {
            List<OpDeviceOrderNumberData> newAttachmentsList = deviceOrderNumber.DataList.FindAll(t => t.IdDataType == dataType && t.OpState == Objects.OpChangeState.New);
            foreach (OpDeviceOrderNumberData donItem in newAttachmentsList)
            {
                donItem.IdDeviceOrderNumberData = dataProvider.SaveDeviceOrderNumberData(donItem);
                donItem.OpState = Objects.OpChangeState.Loaded;
            }
        }

        #endregion
    }
}
