﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.Custom.Metadata;
using IMR.Suite.Services.Common.UTDServiceReference;
using IMR.Suite.Services.Common.Components;
using System.Data;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class MeterComponent
    {
        #region Methods
        public static OpMeter GetByID(DataProvider dataProvider, long Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpMeter GetByID(DataProvider dataProvider, long Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetMeter(Id, queryDatabase);
        }

        public static List<OpMeter> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllMeter();
        }

        public static OpMeter Save(DataProvider dataProvider, OpMeter objectToSave, bool loadNavigationProperties = true)
        {
            bool isNewItem = objectToSave.IdMeter == 0; //check if it's new item

            //zapis do tabeli METER            
            dataProvider.SaveMeter(objectToSave);

            SaveMeterData(dataProvider, objectToSave, isNewItem, loadNavigationProperties: loadNavigationProperties);

            objectToSave.OpState = OpChangeState.Loaded;
            return dataProvider.GetMeter(objectToSave.IdMeter);
        }

        public static void SaveMeterData(DataProvider dataProvider, OpMeter objectToSave, bool isNewItem = false, bool loadNavigationProperties = true)
        {
            //zapis do tabeli DATA
            OpData data = null;
            for (int i = objectToSave.DataList.Count - 1; i >= 0; i--)
            {
                try
                {
                    data = objectToSave.DataList[i];
                    data.IdMeter = objectToSave.IdMeter;

                    if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                    {
                        DataComponent.Delete(dataProvider, data);
                        objectToSave.DataList.Remove(data, true);
                    }
                    else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                    {
                        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);

                        if (objectToSave.DataList[i].OpState == OpChangeState.New)
                        {
                            if(loadNavigationProperties)
                                objectToSave.DataList[i].AssignReferences(dataProvider);
                        }

                        objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                    }
                }
                catch (Exception ex)
                {
                }
            }
        }

        public static void Delete(DataProvider dataProvider, OpMeter objectToDelete)
        {
            foreach (OpData data in objectToDelete.DataList)
            {
                DataComponent.Delete(dataProvider, data);
            }
            dataProvider.DeleteMeter(objectToDelete);
        }


        public static void LoadAllData(DataProvider dataProvider, OpMeter meter)
        {
            meter.DataList.Clear();
            meter.DataList.AddRange(dataProvider.GetDataFilter(IdMeter: new long[] { meter.IdMeter }, IdDataType: new long[0]));
        }

        #region FuelSupplyAudit

        public static List<GOTank> GetStationTanks(OpLocation location)
        {
            List<GOTank> tankList = new List<GOTank>();
            UTDWebServiceSoapClient client = null;
            string errorMsg = "";
            try
            {
                client = UTDComponent.GetUTDClient(location.Distributor.ImrServer.WebserviceAddress, location.Distributor.ImrServer.WebserviceTimeout);
                client.Open();

                DataTable dtLocationEquipment = client.GetLocationEquipmentFilter(out errorMsg, null, new long[] { location.IdLocationOriginal }, null, null, "[END_TIME] is null and [ID_METER] is not null");
                if (errorMsg.Length > 0)
                    throw new Exception(errorMsg);
                if (dtLocationEquipment != null && dtLocationEquipment.Rows.Count > 0)
                {
                    List<long> idMeterList = new List<long>();
                    List<long> idMeterDataTypes = new List<long>();
                    foreach (DataRow drItem in dtLocationEquipment.Rows)
                        idMeterList.Add(Convert.ToInt64(drItem["ID_METER"]));
                    idMeterDataTypes.Add(DataType.ATG_PARAMS_TANK_NUMBER);
                    idMeterDataTypes.Add(DataType.ATG_PARAMS_PRODUCT_CODE);

                    DataTable meterData = null;
                    client.GetDataFilter(out meterData, out errorMsg, null, null, null, idMeterList.ToArray(), idMeterDataTypes.ToArray(), null, null, null);
                    if (errorMsg.Length > 0)
                        throw new Exception(errorMsg);

                    idMeterList.ForEach(m => tankList.Add(new GOTank(m)));

                    List<int> productCodesToGet = new List<int>();
                    foreach (DataRow drItem in meterData.Rows)
                    {
                        long idMeter = Convert.ToInt64(drItem["ID_METER"]);
                        if (Convert.ToInt64(drItem["ID_DATA_TYPE"]) == DataType.ATG_PARAMS_PRODUCT_CODE)
                        {
                            productCodesToGet.Add(Convert.ToInt32(drItem["VALUE"]));
                            if (tankList.Exists(m => m.IdMeter == idMeter))
                                tankList.Find(m => m.IdMeter == idMeter).IdProductCode = Convert.ToInt32(drItem["VALUE"]);
                        }
                        else if (Convert.ToInt64(drItem["ID_DATA_TYPE"]) == DataType.ATG_PARAMS_TANK_NUMBER)
                        {
                            if (tankList.Exists(m => m.IdMeter == idMeter))
                                tankList.Find(m => m.IdMeter == idMeter).TankNumber = Convert.ToInt32(drItem["VALUE"]);
                        }
                    }
                    DataTable productCodes = null;
                    productCodes = client.GetProductCodeFilter(productCodesToGet.ToArray(), null, null, null, null, null, null, null, ref errorMsg);
                    if (errorMsg.Length > 0)
                        throw new Exception(errorMsg);

                    foreach (DataRow drItem in productCodes.Rows)
                    {
                        int idProductCode = Convert.ToInt32(drItem["ID_PRODUCT_CODE"]);
                        tankList.Where(t => t.IdProductCode == idProductCode).ToList().ForEach(t => t.ProductCode = drItem["CODE"].ToString());
                    }

                }

                if (client != null)
                    client.Close();
            }
            catch (Exception ex)
            {
                if (client != null)
                    client.Close();
                throw new Exception("FuelSupplyAuditProtocol error", ex);
            }
            return tankList;
        }
        public class GOTank
        {
            public int IdProductCode { get; set; }
            public string ProductCode { get; set; }

            public int TankNumber { get; set; }
            public long IdMeter { get; private set; }

            public string ComboBoxString
            {
                get { return this.ToString(); }
            }

            public GOTank(long IdMeter)
            {
                this.IdMeter = IdMeter;
            }

            public override string ToString()
            {
                if (!String.IsNullOrEmpty(this.ProductCode))
                    return this.TankNumber + " " + this.ProductCode;
                else
                    return this.IdMeter.ToString();
            }
        }
        public class GOCompartment
        {
            public int Compartment { get; private set; }

            public GOCompartment(int Compartment)
            {
                this.Compartment = Compartment;
            }

            public override string ToString()
            {
                return this.Compartment.ToString();
            }

            public override bool Equals(object obj)
            {
                if (obj is GOCompartment)
                {
                    GOCompartment objecComp = obj as GOCompartment;
                    if (this.Compartment != objecComp.Compartment)
                        return false;
                    return true;
                }
                else
                    return base.Equals(obj);
            }
        }
        public class GODeliverySubject
        {
            public int Index { get; set; }

            public GOTank Tank { get; set; }
            public string TankComboBoxString { get { return this.Tank.ComboBoxString; } }
            public List<GOCompartment> Compartments { get; set; }
            public string CompartmentList
            {
                get
                {
                    string retString = "";
                    if (this.Compartments != null && this.Compartments.Count > 0)
                        retString = String.Join(", ", this.Compartments.Select(c => c.Compartment.ToString()));
                    return retString;
                }
            }
            public double PlannedVolume15C { get; set; }
            public double PlannedVolume { get; set; }
            public double VolumeAuditor15C { get; set; }
            public double VolumeAuditor { get; set; }
            public double VolumeATG15C { get; set; }
            public double VolumeATG { get; set; }
            public double VolumeFlowmeter15C { get; set; }
            public double VolumeFlowmeter { get; set; }

            public double WeightAuditor { get; set; } //kg
            public double DensityAuditor { get; set; } //kg/dm3
            public double TemperatureAuditor { get; set; } //C

            public double WeightFlowmeter { get; set; } //kg
            public double DensityFlowmeter { get; set; } //kg/dm3
            public double TemperatureFlowmeter { get; set; } //C

            public string FlowmeterRemarks { get; set; }
            public string FlowmeterSerialNumber { get; set; }

            public enum FlowmeterDensityKgM3
            {
                Diesel = 834,
                Pb95 = 750,
                VP95 = 751,
                VPDiesel = 835,
                Racing = 755,
            }

            public GODeliverySubject(GOTank Tank, List<GOCompartment> Compartments, double PlannedVolume)
            {
                this.Tank = Tank;
                this.Compartments = Compartments;
                this.PlannedVolume = PlannedVolume;
            }

            public override bool Equals(object obj)
            {
                if (obj is GODeliverySubject)
                {
                    GODeliverySubject objectDelSbj = obj as GODeliverySubject;
                    if (this.Tank.IdMeter != objectDelSbj.Tank.IdMeter)
                        return false;
                    if (this.Tank.IdProductCode != objectDelSbj.Tank.IdProductCode)
                        return false;
                    if (this.Tank.TankNumber != objectDelSbj.Tank.TankNumber)
                        return false;
                    return true;
                }
                else
                    return base.Equals(obj);
            }
        }
        #endregion

        #region GetInstallationEquipment
        public static void GetInstallationEquipment(DataProvider dataProvider, OpLocation location,
            out List<OpMeter> tanks, out List<OpMeter> gasMeters, out List<OpMeter> nozzles)
        {
            tanks = new List<OpMeter>();
            gasMeters = new List<OpMeter>();
            nozzles = new List<OpMeter>();

            if (location == null || dataProvider == null) return;

            List<long> installationMeterIds = new List<long>();

            List<long> meterDataTypes = new List<long>() { 
                    DataType.METER_SERIAL_NUMBER,
                    DataType.METER_TANK_CAPACITY,
                    DataType.ATG_PARAMS_PRODUCT_CODE,
                    DataType.ATG_PARAMS_TANK_NUMBER,
                    DataType.METER_NAME,
                    DataType.METER_LOGICAL_PARENT_ID,
                    DataType.ALEVEL_CONFIG_LO_LEVEL,
                    DataType.ALEVEL_CONFIG_LOLO_LEVEL,
                    DataType.ALEVEL_CONFIG_HI_LEVEL,
                    DataType.ALEVEL_CONFIG_HIHI_LEVEL,
                    DataType.METER_NOZZLE_ID
                };
            List<OpMeter> meters = new List<OpMeter>();

            List<OpLocationEquipment> locationEquipmentList = dataProvider.GetLocationEquipmentFilter(loadNavigationProperties: false,
                loadCustomData: false, IdLocation: new long[] { location.IdLocation },
                IdMeter: new long[0], SerialNbr: new long[0], // specjalnie, aby nie ustawil sie filtr z DBCommon
                customWhereClause: " [ID_METER] IS NOT NULL ");
            List<long> meterIds = locationEquipmentList.Where(q => q.IdMeter.HasValue).Select(q => q.IdMeter.Value).Distinct().ToList();
            if (meterIds.Count > 0)
            {
                installationMeterIds.AddRange(meterIds);
                List<OpData> dataList = dataProvider.GetDataFilter(loadNavigationProperties: false,
                    IndexNbr: new int[] { 0 },
                    IdLocation: new long[0], IdMeter: new long[0], SerialNbr: new long[0], // specjalnie, aby nie ustawil sie filtr z DBCommon
                    IdDataType: new long[] { DataType.METER_LOGICAL_PARENT_ID },
                    Status: new int[] { (int)Enums.DataStatus.Normal },
                    customWhereClause: " [VALUE] IS NOT NULL AND [ID_METER] IS NOT NULL ");
                List<Tuple<long, long>> dataTupleList = dataList.Where(q => q.IdMeter.HasValue && q.Value != null).Select(q => new Tuple<long, long>(q.IdMeter.Value, GenericConverter.Parse<long>(q.Value))).ToList();
                List<long> parentMeterIds = dataTupleList
                    .Where(q => q.Item1.In<long>(meterIds.ToArray()))
                    .Select(q => q.Item2).Distinct().ToList();
                installationMeterIds.AddRange(parentMeterIds);
                List<long> childMeterIds = dataTupleList
                    .Where(q => q.Item2.In<long>(meterIds.ToArray()))
                    .Select(q => q.Item1).Distinct().ToList();
                installationMeterIds.AddRange(childMeterIds);
                // child meter ids from parents meter ids
                if (parentMeterIds.Count > 0)
                    installationMeterIds.AddRange(dataTupleList
                        .Where(q => q.Item2.In<long>(parentMeterIds.ToArray()))
                        .Select(q => q.Item1).Distinct());
            }

            installationMeterIds = installationMeterIds.Where(q => q != 0).Distinct().ToList();
            if (installationMeterIds.Count > 0)
            {
                locationEquipmentList = dataProvider.GetLocationEquipmentFilter(IdMeter: installationMeterIds.ToArray(),
                    IdLocation: new long[0], SerialNbr: new long[0], // specjalnie, aby nie ustawil sie filtr z DBCommon
                    customWhereClause: " [ID_LOCATION] IS NOT NULL ");
                meters = dataProvider.GetMeterFilter(IdMeter: installationMeterIds.ToArray(), IdDistributor: new int[0],
                    customDataTypes: meterDataTypes);
                List<OpLocationEquipment> meterLocationEquipment = null;
                int[] tankMeterTypes = new int[] { (int)Enums.MeterType.LPGTank, (int)Enums.MeterType.FuelTank, (int)Enums.MeterType.TankInstallation };
                bool isInstalled = false;
                bool isFromDifferentLocation = false;
                foreach (OpMeter meter in meters)
                {
                    meterLocationEquipment = locationEquipmentList.Where(q => q.IdMeter.HasValue && q.IdMeter.Value == meter.IdMeter).ToList();
                    if (meterLocationEquipment != null)
                    {
                        isInstalled = meterLocationEquipment.Any(q => !q.EndTime.HasValue);
                        isFromDifferentLocation = meterLocationEquipment.Any(q => !q.EndTime.HasValue && q.IdLocation.HasValue && q.IdLocation.Value != location.IdLocation);
                        meter.DynamicProperties.AddOrUpdateProperty(new OpDynamicProperty(MdDynamicProperty.LocationEquipmentList, meterLocationEquipment));
                        OpLocationEquipment locationEquipment = meterLocationEquipment.FirstOrDefault(w => w.IdLocation.HasValue && !w.EndTime.HasValue);
                        if (locationEquipment != null)
                            meter.DynamicProperties.AddOrUpdateProperty(new OpDynamicProperty(MdDynamicProperty.InstalledLocationId, locationEquipment.IdLocation));
                    }
                    meter.DynamicProperties.AddOrUpdateProperty(new OpDynamicProperty(MdDynamicProperty.IsInstalled, isInstalled));
                    meter.DynamicProperties.AddOrUpdateProperty(new OpDynamicProperty(MdDynamicProperty.InAnotherLocation, isFromDifferentLocation));
                    if (meter.IdMeterType.In<int>(tankMeterTypes))
                        tanks.Add(meter);
                    else if (meter.MeterType != null && meter.MeterType.IdMeterTypeClass == (int)Enums.MeterTypeClass.GasMeter)
                        gasMeters.Add(meter);
                    else if (meter.MeterType != null && meter.MeterType.IdMeterTypeClass == (int)Enums.MeterTypeClass.Nozzle)
                        nozzles.Add(meter);
                }
            }
        }
        #endregion
        #region SortInstallationEquipment
        public static List<OpMeter> SortInstallationEquipment(List<OpMeter> tanks, List<OpMeter> gasMeters, List<OpMeter> nozzles, out bool hasChildren, bool reverse = false)
        {
            hasChildren = false;
            if (tanks == null)
                tanks = new List<OpMeter>();
            if (gasMeters == null)
                gasMeters = new List<OpMeter>();
            if (nozzles == null)
                nozzles = new List<OpMeter>();
            List<OpMeter> meters = tanks.Concat(gasMeters).Concat(nozzles).ToList();
            if (meters == null || meters.Count == 0) return new List<OpMeter>();

            List<OpMeter> sortedMeters = new List<OpMeter>();
            Dictionary<long, OpMeter> metersDict = meters.Distinct(q => q.IdMeter).ToDictionary(q => q.IdMeter);
            Dictionary<long, List<long>> sortedIdsDict = new Dictionary<long, List<long>>();
            #region Parentless tanks
            List<OpMeter> installationMeters = tanks.Where(q => q.IdMeterType == (int)Enums.MeterType.TankInstallation
                        || ((q.IdMeterType.In<int>(new int[] { (int)Enums.MeterType.LPGTank, (int)Enums.MeterType.FuelTank })) && !q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue)).ToList();
            if (!reverse)
                installationMeters = installationMeters.OrderByDescending(q => q.IdMeter).ToList();
            else
                installationMeters = installationMeters.OrderBy(q => q.IdMeter).ToList();
            // installed, from location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // installed, from different location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // not installed, from location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // not installed, from different location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            #endregion
            #region Parentless gas meters
            installationMeters = gasMeters.Where(q => !q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue).ToList();
            if (!reverse)
                installationMeters = installationMeters.OrderByDescending(q => q.IdMeter).ToList();
            else
                installationMeters = installationMeters.OrderBy(q => q.IdMeter).ToList();
            // installed, from location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // installed, from different location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // not installed, from location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // not installed, from different location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            #endregion
            #region Parentless nozzles
            installationMeters = nozzles.Where(q => !q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue).ToList();
            if (!reverse)
                installationMeters = installationMeters.OrderByDescending(q => q.IdMeter).ToList();
            else
                installationMeters = installationMeters.OrderBy(q => q.IdMeter).ToList();
            // installed, from location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // installed, from different location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // not installed, from location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            // not installed, from different location
            foreach (OpMeter installationMeter in installationMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                sortedIdsDict[installationMeter.IdMeter] = new List<long>();
            }
            #endregion
            #region Child tanks
            List<OpMeter> childMeters = tanks.Where(q => q.IdMeterType == (int)Enums.MeterType.TankInstallation
                        || ((q.IdMeterType.In<int>(new int[] { (int)Enums.MeterType.LPGTank, (int)Enums.MeterType.FuelTank })) && q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue)).ToList();
            if (!reverse)
                childMeters = childMeters.OrderByDescending(q => q.IdMeter).ToList();
            else
                childMeters = childMeters.OrderBy(q => q.IdMeter).ToList();
            // installed, from location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // installed, from different location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // not installed, from location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // not installed, from different location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            #endregion
            #region Child gas meters
            childMeters = gasMeters.Where(q => q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue).ToList();
            if (!reverse)
                childMeters = childMeters.OrderByDescending(q => q.IdMeter).ToList();
            else
                childMeters = childMeters.OrderBy(q => q.IdMeter).ToList();
            // installed, from location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // installed, from different location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // not installed, from location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // not installed, from different location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            #endregion
            #region Child nozzles
            childMeters = nozzles.Where(q => q.DataList.TryGetNullableValue<long>(DataType.METER_LOGICAL_PARENT_ID).HasValue).ToList();
            if (!reverse)
                childMeters = childMeters.OrderByDescending(q => q.IdMeter).ToList();
            else
                childMeters = childMeters.OrderBy(q => q.IdMeter).ToList();
            // installed, from location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // installed, from different location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // not installed, from location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            // not installed, from different location
            foreach (OpMeter childMeter in childMeters.Where(q => q.DynamicProperties.ContainsKey(MdDynamicProperty.IsInstalled) && q.DynamicProperties[MdDynamicProperty.IsInstalled] is bool && !Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.IsInstalled])
                && q.DynamicProperties.ContainsKey(MdDynamicProperty.InAnotherLocation) && q.DynamicProperties[MdDynamicProperty.InAnotherLocation] is bool && Convert.ToBoolean(q.DynamicProperties[MdDynamicProperty.InAnotherLocation])))
            {
                long parentId = childMeter.DataList.TryGetValue<long>(DataType.METER_LOGICAL_PARENT_ID);
                if (parentId > 0 && sortedIdsDict.ContainsKey(parentId))
                    sortedIdsDict[parentId].Add(childMeter.IdMeter);
            }
            #endregion

            foreach (KeyValuePair<long, List<long>> installationId in sortedIdsDict)
            {
                OpMeter installationMeter = metersDict.TryGetValue(installationId.Key);
                OpMeter childMeter = null;
                if (installationMeter != null)
                {
                    sortedMeters.Add(installationMeter);
                    foreach (long childId in installationId.Value)
                    {
                        childMeter = metersDict.TryGetValue(childId);
                        if (childMeter != null)
                        {
                            if (childMeter.MeterType != null && childMeter.MeterType.IdMeterTypeClass == (int)Enums.MeterTypeClass.Nozzle)
                                childMeter.ProductCode = installationMeter.ProductCode;
                            sortedMeters.Add(childMeter);
                            hasChildren = true;
                        }
                    }
                }
            }

            if (reverse)
                sortedMeters.Reverse();
            return sortedMeters;
        }
        #endregion
        #region GetTankCapacity
        public static Dictionary<long, List<Tuple<DateTime, DateTime, double?>>> GetTankCapacity(DataProvider dataProvider, OpMeter[] meters, DateTime startTime, DateTime endTime)
        {
            Enums.LocationType[] acceptedChildLocationTypes = new Enums.LocationType[] { Enums.LocationType.CustomerLocation, Enums.LocationType.DraftLocation, Enums.LocationType.RelatedLocation };

            Dictionary<long, List<Tuple<DateTime, DateTime, double?>>> result =new Dictionary<long, List<Tuple<DateTime, DateTime, double?>>>();
            List<long> idMetersTankInstallation = meters.Where(m => m.IdMeterType == (int)Enums.MeterType.TankInstallation).Select(m => m.IdMeter).Distinct().ToList();
            List<long> idNormalMeters = meters.Where(m => !idMetersTankInstallation.Contains(m.IdMeter)).Select(m => m.IdMeter).Distinct().ToList();

            ILookup<long, OpData> parentChildMeters = null;
            if (idMetersTankInstallation.Count > 0)
            {
                List<OpData> parentIds = dataProvider.GetDataFilter(loadNavigationProperties: false, IdDataType: new long[] { DataType.METER_LOGICAL_PARENT_ID }, customWhereClause: "ID_METER IS NOT NULL").Where(d => d.IdMeter != null).ToList();
                parentChildMeters = parentIds.Where(v => v.Value != null).ToLookup(v => Convert.ToInt64(v.Value));
            }
            List<long> idChildMeters = new List<long>();
            List<OpLocationEquipment> childMeterEquipments = new List<OpLocationEquipment>();
            if (parentChildMeters != null)
                idChildMeters.AddRange(parentChildMeters.SelectMany(p => p).Select(o => o.IdMeter.Value));

            List<long> idMetersToQuery = idNormalMeters.ToList();
            if (idChildMeters.Count > 0)
            {
                idMetersToQuery.AddRange(idChildMeters);
                childMeterEquipments = dataProvider.GetLocationEquipmentFilter(IdMeter: idChildMeters.ToArray()).Where(e => e.Location != null && acceptedChildLocationTypes.Contains((Enums.LocationType)e.Location.IdLocationType)).ToList(); //tylko z tych typów lokalizacji moga byc childy dla Logical Tank
            }

            List<OpData> metersCapacity = new List<OpData>();
            if (idMetersToQuery.Count > 0)
                metersCapacity = dataProvider.GetDataFilter(loadNavigationProperties: false, IdDataType: new long[] { DataType.METER_TANK_CAPACITY }, IdMeter: idMetersToQuery.ToArray()).Where(c => c.Value != null).ToList();

            Dictionary<long, List<Tuple<DateTime, DateTime, double?>>> resultNormalMeters = idNormalMeters.ToDictionary(m => m, m =>
                                                   {
                                                       OpData currentMeterCapacity = metersCapacity.FirstOrDefault(o => o.IdMeter == m);
                                                       return new List<Tuple<DateTime, DateTime, double?>>() { new Tuple<DateTime, DateTime, double?>(startTime, endTime, currentMeterCapacity != null ? Convert.ToDouble(currentMeterCapacity.Value) : (double?)null) };
                                                   });

            Dictionary<long, List<Tuple<DateTime, DateTime, double?>>> resultTankInstallations = idMetersTankInstallation.ToDictionary(m => m, m =>
                                                                                        {
                                                                                            if (parentChildMeters == null || !parentChildMeters.Contains(m))
                                                                                                return new List<Tuple<DateTime, DateTime, double?>>() { new Tuple<DateTime, DateTime, double?>(startTime, endTime, (double?)null) };
                                                                                            List<long> currentChildMeters = parentChildMeters[m].Select(c => c.IdMeter.Value).ToList();
                                                                                            OpLocationEquipment[] currentEquipments = childMeterEquipments.Where(e => currentChildMeters.Contains(e.IdMeter.Value) && (e.EndTime == null || e.EndTime.Value >= startTime) && (e.StartTime <= endTime)).OrderBy(e => e.StartTime).ThenBy(e => e.EndTime ?? DateTime.MaxValue).ToArray();
                                                                                            Tuple<long, DateTime, DateTime, double?>[] tempMeterEquipmentCapacity = currentEquipments.Where(e => metersCapacity.Any(c => c.IdMeter == e.IdMeter)).Select(e => new Tuple<long, DateTime, DateTime, double?>(e.IdMeter.Value, e.StartTime < startTime ? startTime : e.StartTime, e.EndTime == null || e.EndTime.Value > endTime ? endTime : e.EndTime.Value, Convert.ToDouble(metersCapacity.Find(c => c.IdMeter == e.IdMeter).Value))).ToArray();
                                                                                            if (tempMeterEquipmentCapacity.Length == 0)
                                                                                                return new List<Tuple<DateTime, DateTime, double?>>() { new Tuple<DateTime, DateTime, double?>(startTime, endTime, (double?)null) };

                                                                                            DateTime[] sectionTimes = tempMeterEquipmentCapacity.Select(s => s.Item2).Concat(tempMeterEquipmentCapacity.Select(s => s.Item3)).Distinct().OrderBy(s => s).ToArray();
                                                                                            if (sectionTimes.Length == 0)
                                                                                                return new List<Tuple<DateTime, DateTime, double?>>() { new Tuple<DateTime, DateTime, double?>(startTime, endTime, (double?)null) };

                                                                                            if (sectionTimes.Length == 1)
                                                                                                return new List<Tuple<DateTime, DateTime, double?>>() { new Tuple<DateTime, DateTime, double?>(sectionTimes[0], sectionTimes[0], tempMeterEquipmentCapacity.Where(t => sectionTimes[0] >= t.Item2 && sectionTimes[0] <= t.Item3).GroupBy(t => t.Item1).Select(g => g.First().Item4).Sum()) };
                                                                                            List<Tuple<DateTime, DateTime, double?>> tempResult = new List<Tuple<DateTime, DateTime, double?>>();
                                                                                            for (int i = 0; i < sectionTimes.Length-1; i++)
                                                                                            {
                                                                                                tempResult.Add(new Tuple<DateTime, DateTime, double?>(sectionTimes[i], sectionTimes[i+1],  tempMeterEquipmentCapacity.Where(t => sectionTimes[i] >= t.Item2 && sectionTimes[i] <= t.Item3).GroupBy(t => t.Item1).Select(g => g.First().Item4).Sum() ));
                                                                                                if (i == sectionTimes.Length - 2)
                                                                                                    break;
                                                                                            }

                                                                                            return tempResult;
                                                                                        });

            result = resultNormalMeters.Concat(resultTankInstallations).ToDictionary(d => d.Key, d => d.Value);

            return result;
        }
        #endregion

        #region GetMeterHistory

        public static List<OpHistory> GetMeterHistory(IDataProvider dataProvider, OpOperator loggedOperator, long idMeter, List<Enums.ObjectHistoryEventType> includedEvents = null)
        {
            List<OpHistory> meterHistory = new List<OpHistory>();

            #region InstallationHistory

            if (includedEvents == null || includedEvents.Exists(e => e.In(new Enums.ObjectHistoryEventType[] { Enums.ObjectHistoryEventType.InstalledAtLocation,
                Enums.ObjectHistoryEventType.RemovedFromLocation })))
            {
                if (loggedOperator.HasPermission(Activity.LOCATION_LIST))
                {
                    List<OpLocationEquipment> leList = dataProvider.GetLocationEquipmentFilter(loadCustomData: false, loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                        IdMeter: new long[] { idMeter }, customWhereClause: "[ID_LOCATION] is not null");
                    if (leList != null && leList.Count > 0)
                    {
                        List<OpLocation> lList = dataProvider.GetLocation(leList.Select(l => l.IdLocation.Value).Distinct().ToArray())
                                                    .Where(l => l.IdLocationType == (int)Enums.LocationType.CustomerLocation).ToList();
                        leList = leList.OrderBy(l => l.StartTime).ToList();
                        for(int i = 0; i < leList.Count; i++)
                        {
                            OpLocation lItem = lList.Find(l => l.IdLocation == leList[i].IdLocation.Value);
                            if (lItem != null)
                            {
                                if (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.InstalledAtLocation))
                                {
                                    OpLocationEquipment lePrevItem = (i - 1) < 0 ? null : leList[i - 1];
                                    if (lePrevItem == null || lePrevItem.IdLocation != leList[i].IdLocation)
                                    {
                                        OpHistory hItem = new OpHistory(leList[i].StartTime, Enums.ObjectHistoryEventType.InstalledAtLocation, lItem.ToString(), lItem.FullAddress);
                                        hItem.EventObject = lItem;
                                        meterHistory.Add(hItem);
                                    }
                                }
                                if (leList[i].EndTime.HasValue && (includedEvents == null || includedEvents.Exists(e => e == Enums.ObjectHistoryEventType.RemovedFromLocation)))
                                {
                                    //sprawdzamy czy meter nie został w kolejnyhc wpisach zainstalowany/zdemontowany na tej samej lokalziacji
                                    //takich przypadków nie pokazujemy w historii bo to najczęściej tyllko przypięcie/zmiana urządzenia
                                    OpLocationEquipment leNextItem = (i + 1) >= leList.Count ? null : leList[i + 1];
                                    if (leNextItem == null || leNextItem.IdLocation != leList[i].IdLocation)
                                    {
                                        OpHistory hItem = new OpHistory(leList[i].EndTime.Value, Enums.ObjectHistoryEventType.RemovedFromLocation, lItem.ToString(), lItem.FullAddress);
                                        hItem.EventObject = lItem;

                                        meterHistory.Add(hItem);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            meterHistory = meterHistory.OrderByDescending(h => h.EventDate).ToList();

            return meterHistory;
        }

        #endregion

        #region SetHelperValues
        public static void SetHelperValues(DataProvider dataProvider, OpMeter meter, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdMeter, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_METER:
                        meter.DataList.SetValue(DataType.HELPER_ID_METER, 0, meter.IdMeter);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        meter.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, meter.IdDistributor);
                        break;
                    case DataType.HELPER_ID_METER_TYPE:
                        meter.DataList.SetValue(DataType.HELPER_ID_METER_TYPE, 0, meter.IdMeterType);
                        break;
                    default:
                        break;
                }
            }
            meter.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }
        #endregion

        #region GetSchema
        public static List<OpSchema> GetSchema(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, long idMeter)
        {
            OpMeter mItem = dataProvider.GetMeter(idMeter);
            return GetSchema(dataProvider, loggedOperator, module, mItem);
        }
        public static List<OpSchema> GetSchema(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, OpMeter meter)
        {
            return GetSchema(dataProvider, loggedOperator, module, new List<OpMeter>() { meter });
        }
        public static List<OpSchema> GetSchema(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, List<long> idMeter)
        {
            List<OpMeter> mItem = dataProvider.GetMeter(idMeter.ToArray());
            return GetSchema(dataProvider, loggedOperator, module, mItem);
        }
        public static List<OpSchema> GetSchema(IDataProvider dataProvider, OpOperator loggedOperator, Enums.Module module, List<OpMeter> meter)
        {
            List<OpSchema> sList = new List<OpSchema>();
            Dictionary<int, List<OpMeter>> mDict = meter.GroupBy(m => m.MeterType.IdMeterTypeClass).ToDictionary(m => m.Key, m => m.ToList());
            if (mDict.ContainsKey((int)Enums.MeterTypeClass.Tank) && mDict[(int)Enums.MeterTypeClass.Tank] != null && mDict[(int)Enums.MeterTypeClass.Tank].Count > 0)
            {
                #region Tank
                
                List<OpMeter> meters = mDict[(int)Enums.MeterTypeClass.Tank];
                List<OpTankSchema> tsList = new List<OpTankSchema>();

                Dictionary<long, int> level = new Dictionary<long, int>();
                //Key: IdMeter, Value: isSquareShape, isUnderground, isVertical
                Dictionary<long, Tuple<bool, bool, bool>> tankShape = new Dictionary<long, Tuple<bool, bool, bool>>();
                meters.ForEach(m => {
                    level.Add(m.IdMeter, 0);
                    tankShape.Add(m.IdMeter, new Tuple<bool, bool, bool>(false, false, false));
                });
                TimeSpan? getDataConfigTime = null;
                TimeSpan? getDataMeasurementTime = null;
                TimeSpan? getDataSchemaElementsTime = null;
                #region LoadData
                DateTime dataConfigTime = dataProvider.DateTimeNow;
                List<OpData> dList = dataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                    IdMeter: meters.Select(m => m.IdMeter).ToArray(), IdDataType: new long[] { DataType.METER_TANK_IS_UNDERGROUND, DataType.METER_TANK_IS_VERTICAL, DataType.METER_SHAPE_AND_POSITION });
                if (dList != null && dList.Count > 0)
                {
                    foreach (OpMeter mItem in meters)
                    {
                        bool isSquareShape = false;
                        bool isUnderground = false;
                        bool isVertical = false;
                        OpData dItem = dList.LastOrDefault(d => d.IdMeter == mItem.IdMeter && d.IdDataType == DataType.METER_TANK_IS_UNDERGROUND && d.Value != null);
                        if (dItem != null)
                        {
                            try
                            {
                                isUnderground = Convert.ToBoolean(dItem.Value);
                            }
                            catch (Exception ex)
                            {
                                isUnderground = false;
                                BaseComponent.Log(EventID.Forms.MeterException, new object[] { "-", ex.Message, ex.StackTrace });
                            }
                        }
                        dItem = dList.LastOrDefault(d => d.IdMeter == mItem.IdMeter && d.IdDataType == DataType.METER_TANK_IS_VERTICAL && d.Value != null);
                        if (dItem != null)
                        {
                            try
                            {
                                isVertical = Convert.ToBoolean(dItem.Value);
                            }
                            catch (Exception ex)
                            {
                                isVertical = false;
                                BaseComponent.Log(EventID.Forms.MeterException, new object[] { "-", ex.Message, ex.StackTrace });
                            }
                        }
                        //Taki sposób przyjęty w starym portalu - słaby
                        dItem = dList.LastOrDefault(d => d.IdMeter == mItem.IdMeter && d.IdDataType == DataType.METER_SHAPE_AND_POSITION && d.Value != null);
                        if (dItem != null)
                        {
                            try
                            {
                                isSquareShape = String.Equals(dItem.Value.ToString().Trim().ToUpper(), "SQUARE");
                            }
                            catch (Exception ex)
                            {
                                isSquareShape = false;
                                BaseComponent.Log(EventID.Forms.MeterException, new object[] { "-", ex.Message, ex.StackTrace });
                            }
                        }
                        tankShape[mItem.IdMeter] = new Tuple<bool, bool, bool>(isSquareShape, isUnderground, isVertical);
                        BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", String.Format("Tank {0} shape", mItem.IdMeter), String.Format("IsVertical: {0}, IsUnderground: {1}, IsSquare: {2}", isVertical, isUnderground, isSquareShape) });
                    }
                    getDataConfigTime = (dataProvider.DateTimeNow - dataConfigTime);
                }
                #endregion
                #region SchemaFiles
                List<string> fileNameList = new List<string>();
                //Key: IdMeter, Value: isSquareShape, isUnderground, isVertical
                //Horizontal
                if (tankShape.Values.Any(s => !s.Item1 && !s.Item2 && !s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankHorizontalContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalConfigurationFileName);
                }
                //HorizontalSquare
                if (tankShape.Values.Any(s => s.Item1 && !s.Item2 && !s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankHorizontalSquareContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalSquareMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalSquareSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalSquareConfigurationFileName);
                }
                //HorizontalUnderground
                if (tankShape.Values.Any(s => !s.Item1 && s.Item2 && !s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundConfigurationFileName);
                }
                //HorizontalUndergroundSquare
                if (tankShape.Values.Any(s => s.Item1 && s.Item2 && !s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundSquareContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundSquareMaskFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundSquareSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankHorizontalUndergroundSquareConfigurationFileName);
                }
                //Vertical
                if (tankShape.Values.Any(s => !s.Item1 && !s.Item2 && s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankVerticalContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalConfigurationFileName);
                }
                //VerticalSquare
                if (tankShape.Values.Any(s => s.Item1 && !s.Item2 && s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankVerticalSquareContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalSquareMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalSquareSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalSquareConfigurationFileName);
                }
                //VerticalUnderground
                if (tankShape.Values.Any(s => !s.Item1 && s.Item2 && s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundConfigurationFileName);
                }
                //VerticalUndergroundSquare
                if (tankShape.Values.Any(s => s.Item1 && s.Item2 && s.Item3))
                {
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundSquareContourMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundSquareMaskFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundSquareSchemaFileName);
                    fileNameList.Add(OpTankSchema.TankVerticalUndergroundSquareConfigurationFileName);
                }

                foreach (string fileName in fileNameList)
                {
                    if (!OpTankSchema.TankFile.ContainsKey(fileName) || OpTankSchema.TankFile[fileName] == null)
                    {
                        OpFile fItem = dataProvider.GetFileFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                            PhysicalFileName: fileName).FirstOrDefault();
                        if (fItem != null)
                        {
                            dataProvider.GetFileContent(fItem);
                            OpTankSchema.TankFile[fileName] = fItem.FileBytes as byte[];
                        }
                        else
                            OpTankSchema.TankFile[fileName] = null;
                        BaseComponent.Log(EventID.Forms.MeterLoadedSchemaFromDB, new object[] { "-", fileName, OpTankSchema.TankFile[fileName] == null ? "<null>" : OpTankSchema.TankFile[fileName].Length.ToString() });
                    }
                }
                #endregion
                foreach (OpMeter mItem in meters)
                {
                    OpTankSchema tsItem = new OpTankSchema(mItem.IdMeter, Enums.ReferenceType.IdMeter, level[mItem.IdMeter],
                        tankShape[mItem.IdMeter].Item3, tankShape[mItem.IdMeter].Item2, tankShape[mItem.IdMeter].Item1,
                        new List<OpSchemaElement>())
                    {
                        ReferenceType = Enums.ReferenceType.IdMeter,
                        IdObject = mItem.IdMeter
                    };
                    tsList.Add(tsItem);
                }
                #region LoadMeasurementData

                List<long> measurementDataType = new List<long>();
                measurementDataType.Add(DataType.GAS_LEVEL_PERCENTAGE_VALUE);
                measurementDataType.Add(DataType.ALEVEL_CONFIG_LO_LEVEL);
                measurementDataType.Add(DataType.ALEVEL_CONFIG_LOLO_LEVEL);
                measurementDataType.Add(DataType.ALEVEL_CONFIG_HI_LEVEL);
                measurementDataType.Add(DataType.ALEVEL_CONFIG_HIHI_LEVEL);
                
                foreach (OpTankSchema tsItem in tsList)
                {
                    if (tsItem.SchemaConfiguration != null && tsItem.SchemaConfiguration.Count > 0)
                        measurementDataType.AddRange(tsItem.SchemaConfiguration.Select(c => c.DataType.IdDataType).Distinct());
                    if (tsItem.SchemaConfiguration != null)
                    {
                        foreach (OpSchemaElement seItem in tsItem.SchemaConfiguration)
                        {
                            seItem.Value = null;
                        }
                    }
                }
                measurementDataType = measurementDataType.Distinct().ToList();

                DateTime dataMeasurementTime = dataProvider.DateTimeNow;
                string readoutsCustomWhereClause = "[ID_DATA_SOURCE] is not null";
                List<Objects.DW.OpData> dDWItem = dataProvider.GetDataFilterDW(IdMeter: meters.Select(m => m.IdMeter).ToArray(), IdDataType: measurementDataType.ToArray(), 
                    IndexNbr: new int[] { 0 }, customWhereClause: readoutsCustomWhereClause,
                    transactionLevel: IsolationLevel.ReadUncommitted, loadNavigationProperties: false);
                getDataMeasurementTime = (dataProvider.DateTimeNow - dataMeasurementTime);
                BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "DWData", dDWItem.Count});
                if (dDWItem == null)
                    dDWItem = new List<Objects.DW.OpData>();
                List<OpData> dCOREItem = new List<OpData>();

                List<long> missingDataTypes = measurementDataType.Except(dDWItem.Select(d => d.IdDataType).Distinct()).ToList();
                DateTime dataSchemaElementsTime = dataProvider.DateTimeNow;
                if (missingDataTypes.Count > 0)
                {
                    dCOREItem = dataProvider.GetDataFilter(IdMeter: meters.Select(m => m.IdMeter).ToArray(), IdDataType: missingDataTypes.ToArray(),
                        IndexNbr: new int[] { 0 },
                        transactionLevel: IsolationLevel.ReadUncommitted, loadNavigationProperties: false);
                }
                getDataSchemaElementsTime = (dataProvider.DateTimeNow - dataSchemaElementsTime);
                if (dCOREItem == null)
                    dCOREItem = new List<OpData>();
                BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "COREData", dCOREItem.Count });
                
                List<OpDataFormatGroupDetails> dataFormatGroupDetails = DataFormatGroupComponent.GetHierarchicalDataFormatGroupDetails(dataProvider as DataProvider, loggedOperator, module);
                Dictionary<long, int?> precisionDict = DataTypeFormatComponent.GetDataTypePrecisionDict(dataFormatGroupDetails, true);
                Dictionary<long, OpUnit> unitDict = dataFormatGroupDetails.Distinct(x => x.ID_DATA_TYPE)
                                                                            .Where(x => x.IdUnitOut.HasValue)
                                                                            .ToDictionary(k => k.IdDataType, v => v.UnitOut == null ? dataProvider.GetUnit(v.IdUnitOut.Value) : v.UnitOut);

                Dictionary<long, OpDataType> dtDict = dataProvider.GetDataType(dCOREItem.Select(d => d.IdDataType).Distinct().Union(dDWItem.Select(d => d.IdDataType).Distinct()).ToArray()).ToDictionary(d => d.IdDataType);
                Dictionary<int, OpUnit> uDict = dataProvider.GetUnit(dtDict.Select(d => d.Value.IdUnit).Distinct().ToArray()).ToDictionary(d => d.IdUnit);
                foreach (OpDataType dtItem in dtDict.Values)
                    dtItem.Unit = uDict[dtItem.IdUnit];

                if (dCOREItem.Count > 0)
                {
                    dCOREItem.ForEach(d => { d.DataType = dtDict[d.IdDataType]; d.Unit = uDict[d.DataType.IdUnit]; });
                    dCOREItem = UnitComponent.ChangeUnit(dCOREItem, unitDict, precisionDict);
                }
                if (dDWItem.Count > 0)
                {
                    dDWItem.ForEach(d => { d.DataType = dtDict[d.IdDataType]; d.Unit = uDict[d.DataType.IdUnit]; });
                    dDWItem = UnitComponent.ChangeUnit(dDWItem, unitDict, precisionDict);
                }
                #endregion
                #region FillingSchema
                DateTime dt = dataProvider.DateTimeNow;
                foreach (OpTankSchema tsItem in tsList)
                {
                    long idMeter = Convert.ToInt64(tsItem.IdObject);
                    if (dDWItem != null && dDWItem.Count > 0)
                    {
                        #region DW Data

                        foreach (Objects.DW.OpData dItem in dDWItem.Where(d => d.IdMeter == idMeter))
                        {
                            if (dItem.IdDataType == DataType.GAS_LEVEL_PERCENTAGE_VALUE)
                            {
                                double levelDouble = 0.0;
                                if (double.TryParse(dItem.Value.ToString(), out levelDouble))
                                    level[dItem.IdMeter.Value] = (int)levelDouble;
                            }
                            OpSchemaElement seItem = tsItem.SchemaConfiguration.Find(c => c.DataType.IdDataType == dItem.IdDataType);
                            if (seItem != null)
                            {
                                seItem.Value = dItem.Value;
                            }
                        }
                        #endregion
                    }
                    if (dCOREItem != null && dCOREItem.Count > 0)
                    {
                        #region CORE Data

                        foreach (OpData dItem in dCOREItem.Where(d => d.IdMeter == idMeter))
                        {
                            OpSchemaElement seItem = tsItem.SchemaConfiguration.Find(c => c.DataType.IdDataType == dItem.IdDataType);
                            if (seItem != null && seItem.Value == null)
                            {//Na wszelki wypadek nie chcemy nadpisać już pobranych danych z DW
                                OpDataType dtItem = dataProvider.GetDataType(dItem.IdDataType);
                                object value = dItem.Value;
                                if (dtItem.IdReferenceType.HasValue)
                                    value = ReferenceTypeComponent.GetReferenceObject(dataProvider as DataProvider, (Enums.ReferenceType)dtItem.IdReferenceType.Value, dItem.Value, loadCustomData: false);
                                seItem.Value = value == null ? null : dtItem.IdReferenceType.HasValue ? value.ToString() : value;
                            }
                        }
                        #endregion
                    }
                    #region Tresholds
                    int? hiLevel = null;
                    int? hihiLevel = null;
                    int? loLevel = null;
                    int? loloLevel = null;

                    double tresholdLevelTmp = 0.0;
                    IOpData dLevelItem = null;
                    #region Lo
                    dLevelItem = dCOREItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_LO_LEVEL);
                    if (dLevelItem == null)
                        dLevelItem = dDWItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_LO_LEVEL);
                    if (dLevelItem != null && dLevelItem.Value != null && double.TryParse(dLevelItem.Value.ToString(), out tresholdLevelTmp))
                    {
                        loLevel = Convert.ToInt32(tresholdLevelTmp);
                    }
                    #endregion
                    #region LoLo
                    dLevelItem = dCOREItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_LOLO_LEVEL);
                    if (dLevelItem == null)
                        dLevelItem = dDWItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_LOLO_LEVEL);
                    if (dLevelItem != null && dLevelItem.Value != null && double.TryParse(dLevelItem.Value.ToString(), out tresholdLevelTmp))
                    {
                        loloLevel = Convert.ToInt32(tresholdLevelTmp);
                    }
                    #endregion
                    #region Hi
                    dLevelItem = dCOREItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_HI_LEVEL);
                    if (dLevelItem == null)
                        dLevelItem = dDWItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_HI_LEVEL);
                    if (dLevelItem != null && dLevelItem.Value != null && double.TryParse(dLevelItem.Value.ToString(), out tresholdLevelTmp))
                    {
                        hiLevel = Convert.ToInt32(tresholdLevelTmp);
                    }
                    #endregion
                    #region HiHi
                    dLevelItem = dCOREItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_HIHI_LEVEL);
                    if (dLevelItem == null)
                        dLevelItem = dDWItem.Find(d => d.IdDataType == DataType.ALEVEL_CONFIG_HIHI_LEVEL);
                    if (dLevelItem != null && dLevelItem.Value != null && double.TryParse(dLevelItem.Value.ToString(), out tresholdLevelTmp))
                    {
                        hihiLevel = Convert.ToInt32(tresholdLevelTmp);
                    }
                    #endregion
                    if(loLevel > 0)
                        tsItem.LoLevel = loLevel;
                    if (loloLevel > 0)
                        tsItem.LoLoLevel = loloLevel;
                    if (hiLevel > 0)
                        tsItem.HiLevel = hiLevel;
                    if (hihiLevel > 0)
                        tsItem.HiHiLevel = hihiLevel;

                    #endregion

                    DateTime drawingStartTime = DateTime.Now;
                    //współrzędne tymczasowe, jesli sie ich nie poda algortm postara się je znaleźć
                    tsItem.DrawSchema(level: level[idMeter], levelWidth: 1731, levelHeight: 792, levelX: 96, levelY: 240);
                    TimeSpan drawSchemaTime = (DateTime.Now - drawingStartTime);
                    BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", String.Format("Tank {0} params", idMeter),
                        String.Format("ConvertedTankLevel: {0}, LoLevel: {1}, LoLoLevel: {2}, HiLevel: {3}, HiHiLevel: {4}, DrawingTime: {5}[s]", level[idMeter], loLevel, loloLevel, hiLevel, hihiLevel, Math.Round(drawSchemaTime.TotalSeconds, 2)) });
                    //string fileNameTmp = tsItem.GetFileName(OpTankSchema.TankFileType.Schema, tsItem.IsVertical, tsItem.IsUnderground, tsItem.IsSquareShape);
                    //BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "Schema", fileNameTmp });
                    //fileNameTmp = tsItem.GetFileName(OpTankSchema.TankFileType.Contour, tsItem.IsVertical, tsItem.IsUnderground, tsItem.IsSquareShape);
                    //BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "Contour", fileNameTmp });
                    //fileNameTmp = tsItem.GetFileName(OpTankSchema.TankFileType.Mask, tsItem.IsVertical, tsItem.IsUnderground, tsItem.IsSquareShape);
                    //BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "Mask", fileNameTmp });
                    //fileNameTmp = tsItem.GetFileName(OpTankSchema.TankFileType.Configuration, tsItem.IsVertical, tsItem.IsUnderground, tsItem.IsSquareShape);
                    //BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "Configuration", fileNameTmp });
                    sList.Add(tsItem);

                    if (tsItem.SchemaConfiguration != null)
                    {
                        foreach (OpSchemaElement seItem in tsItem.SchemaConfiguration)
                        {
                            if (String.IsNullOrEmpty(seItem.Description))
                            {
                                seItem.Description = dataProvider.GetDataType(seItem.DataType.IdDataType).ToString();
                            }
                            if (seItem.Value == null || seItem.Value == default(object) || String.IsNullOrEmpty(seItem.Value.ToString()))
                                seItem.Value = "-";
                        }
                    }

                    BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "Loaded configuration count", tsItem.SchemaConfiguration == null ? "<null>" : tsItem.SchemaConfiguration.Count.ToString() });
                }
                TimeSpan ts = dataProvider.DateTimeNow - dt;
                double time = Math.Round(ts.TotalSeconds, 3);
                #endregion

                BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", String.Format("Tank schema load time (Count: {0})", meters.Count),
                                                                        String.Format("Config: {0}[s], Measurement: {1}[s], SchemaElement: {2}[s]", 
                                                                                        (getDataConfigTime.HasValue ? Math.Round(getDataConfigTime.Value.TotalSeconds, 2).ToString() : "<null>"),
                                                                                        (getDataMeasurementTime.HasValue ? Math.Round(getDataMeasurementTime.Value.TotalSeconds, 2).ToString() : "<null>"),
                                                                                        (getDataSchemaElementsTime.HasValue ? Math.Round(getDataSchemaElementsTime.Value.TotalSeconds, 2).ToString() : "<null>"))});
                #endregion
            }
            if (mDict.ContainsKey((int)Enums.MeterTypeClass.Switcher) && mDict[(int)Enums.MeterTypeClass.Switcher] != null && mDict[(int)Enums.MeterTypeClass.Switcher].Count > 0)
            {
                #region ACO
                List<OpMeter> meters = mDict[(int)Enums.MeterTypeClass.Switcher];
                //Key: IdMeter, Value: LEFT_BANK_ACTIVE, RIGHT_BANK_ACTIVE, RESERVE_BANK_ACTIVE
                Dictionary<long, Tuple<bool, bool, bool>> switcherPosition = new Dictionary<long, Tuple<bool, bool, bool>>();
                meters.ForEach(m => {
                    switcherPosition.Add(m.IdMeter, new Tuple<bool, bool, bool>(false, false, false));
                });
                #region LoadData
                List<long> idDataType = new List<long>();
                idDataType.Add(DataType.LEFT_BANK_ACTIVE);
                idDataType.Add(DataType.RIGHT_BANK_ACTIVE);
                idDataType.Add(DataType.RESERVE_BANK_ACTIVE);
                string readoutsCustomWhereClause = "[ID_DATA_SOURCE] is not null";
                List<Objects.DW.OpData> dDWList = dataProvider.GetDataFilterDW(IdMeter: meters.Select(m => m.IdMeter).ToArray(), IdDataType: idDataType.ToArray(), IndexNbr: new int[] { 0 }, customWhereClause: readoutsCustomWhereClause, transactionLevel: IsolationLevel.ReadUncommitted);

                List<OpDataFormatGroupDetails> dataFormatGroupDetails = DataFormatGroupComponent.GetHierarchicalDataFormatGroupDetails(dataProvider as DataProvider, loggedOperator, module);
                Dictionary<long, int?> precisionDict = DataTypeFormatComponent.GetDataTypePrecisionDict(dataFormatGroupDetails, true);
                Dictionary<long, OpUnit> unitDict = dataFormatGroupDetails.Distinct(x => x.ID_DATA_TYPE)
                                                                            .Where(x => x.IdUnitOut.HasValue)
                                                                            .ToDictionary(k => k.IdDataType, v => v.UnitOut == null ? dataProvider.GetUnit(v.IdUnitOut.Value) : v.UnitOut);

                if (dDWList != null && dDWList.Count > 0)
                {
                    foreach (OpMeter mItem in meters)
                    {
                        bool rightBankActive = false;
                        bool leftBankActive = false;
                        bool reserveBankActive = false;
                        Objects.DW.OpData rightBank = dDWList.LastOrDefault(d => d.IdDataType == DataType.RIGHT_BANK_ACTIVE);
                        Objects.DW.OpData leftBank = dDWList.LastOrDefault(d => d.IdDataType == DataType.LEFT_BANK_ACTIVE);
                        Objects.DW.OpData reserveBank = dDWList.LastOrDefault(d => d.IdDataType == DataType.RESERVE_BANK_ACTIVE);

                        if (rightBank != null && rightBank.Value != null)
                        {
                            try { rightBankActive = Convert.ToBoolean(rightBank.Value); } catch (Exception ex) { }
                        }
                        if (leftBank != null && leftBank.Value != null)
                        {
                            try { leftBankActive = Convert.ToBoolean(leftBank.Value); } catch (Exception ex) { }
                        }
                        if (reserveBank != null && reserveBank.Value != null)
                        {
                            try { reserveBankActive = Convert.ToBoolean(reserveBank.Value); } catch (Exception ex) { }
                        }
                        BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", String.Format("Switcher {0} position", mItem.IdMeter), String.Format("LeftBank: {0}, RightBank: {1}, ReserveBank: {2}", leftBankActive, rightBankActive, reserveBankActive) });
                    }
                }
                #endregion
                #region SchemaFiles
                if (OpACOSchema.AcoSchema == null)
                {
                    OpFile fItem = dataProvider.GetFileFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                        PhysicalFileName: OpACOSchema.AcoSchemaFileName).FirstOrDefault();
                    if (fItem != null)
                    {
                        dataProvider.GetFileContent(fItem);
                        OpACOSchema.AcoSchema = fItem.FileBytes as byte[];
                    }
                    BaseComponent.Log(EventID.Forms.MeterLoadedSchemaFromDB, new object[] { "-", OpACOSchema.AcoSchemaFileName, OpACOSchema.AcoSchema == null ? "<null>" : OpACOSchema.AcoSchema.Length.ToString() });
                }
                if (OpACOSchema.AcoLeftMask == null)
                {
                    OpFile fItem = dataProvider.GetFileFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                        PhysicalFileName: OpACOSchema.AcoLeftMaskFileName).FirstOrDefault();
                    if (fItem != null)
                    {
                        dataProvider.GetFileContent(fItem);
                        OpACOSchema.AcoLeftMask = fItem.FileBytes as byte[];
                    }
                    BaseComponent.Log(EventID.Forms.MeterLoadedSchemaFromDB, new object[] { "-", OpACOSchema.AcoLeftMaskFileName, OpACOSchema.AcoLeftMask == null ? "<null>" : OpACOSchema.AcoLeftMask.Length.ToString() });
                }
                if (OpACOSchema.AcoLeftReserveMask == null)
                {
                    OpFile fItem = dataProvider.GetFileFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                        PhysicalFileName: OpACOSchema.AcoLeftReserveFileName).FirstOrDefault();
                    if (fItem != null)
                    {
                        dataProvider.GetFileContent(fItem);
                        OpACOSchema.AcoLeftReserveMask = fItem.FileBytes as byte[];
                    }
                    BaseComponent.Log(EventID.Forms.MeterLoadedSchemaFromDB, new object[] { "-", OpACOSchema.AcoLeftReserveFileName, OpACOSchema.AcoLeftReserveMask == null ? "<null>" : OpACOSchema.AcoLeftReserveMask.Length.ToString() });
                }
                if (OpACOSchema.AcoRightMask == null)
                {
                    OpFile fItem = dataProvider.GetFileFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                        PhysicalFileName: OpACOSchema.AcoRightMaskFileName).FirstOrDefault();
                    if (fItem != null)
                    {
                        dataProvider.GetFileContent(fItem);
                        OpACOSchema.AcoRightMask = fItem.FileBytes as byte[];
                    }
                    BaseComponent.Log(EventID.Forms.MeterLoadedSchemaFromDB, new object[] { "-", OpACOSchema.AcoRightMaskFileName, OpACOSchema.AcoRightMask == null ? "<null>" : OpACOSchema.AcoRightMask.Length.ToString() });
                }
                if (OpACOSchema.AcoRightReserveMask == null)
                {
                    OpFile fItem = dataProvider.GetFileFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                        PhysicalFileName: OpACOSchema.AcoRightReserveFileName).FirstOrDefault();
                    if (fItem != null)
                    {
                        dataProvider.GetFileContent(fItem);
                        OpACOSchema.AcoRightReserveMask = fItem.FileBytes as byte[];
                    }
                    BaseComponent.Log(EventID.Forms.MeterLoadedSchemaFromDB, new object[] { "-", OpACOSchema.AcoRightReserveFileName, OpACOSchema.AcoRightReserveMask == null ? "<null>" : OpACOSchema.AcoRightReserveMask.Length.ToString() });
                }
                #endregion
                foreach (OpMeter mItem in meters)
                {
                    //Key: IdMeter, Value: LEFT_BANK_ACTIVE, RIGHT_BANK_ACTIVE, RESERVE_BANK_ACTIVE
                    OpACOSchema asItem = new OpACOSchema(mItem.IdMeter, Enums.ReferenceType.IdMeter, switcherPosition[mItem.IdMeter].Item1, switcherPosition[mItem.IdMeter].Item2, switcherPosition[mItem.IdMeter].Item3, new List<OpSchemaElement>())
                    {
                        ReferenceType = Enums.ReferenceType.IdMeter,
                        IdObject = mItem.IdMeter
                    };
                    asItem.DrawSchema();
                    sList.Add(asItem);
                }

                #endregion
            }
            if (mDict.ContainsKey((int)Enums.MeterTypeClass.GasMeter) && mDict[(int)Enums.MeterTypeClass.GasMeter] != null && mDict[(int)Enums.MeterTypeClass.GasMeter].Count > 0)
            {
                #region GasMeter

                List<OpMeter> meters = mDict[(int)Enums.MeterTypeClass.GasMeter];
                List<OpGasMeterSchema> gmsList = new List<OpGasMeterSchema>();
                Dictionary<long, double> usageValueDict = new Dictionary<long, double>();

                TimeSpan? getDataConfigTime = null;
                TimeSpan? getDataMeasurementTime = null;
                TimeSpan? getDataSchemaElementsTime = null;

                #region LoadData
                DateTime dataConfigTime = dataProvider.DateTimeNow;
                List<long> idDataType = new List<long>();
                // na przyszłość
                if (idDataType != null && idDataType.Count > 0)
                {
                    List<OpData> dList = dataProvider.GetDataFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                    IdMeter: meters.Select(m => m.IdMeter).ToArray(), IdDataType: idDataType.ToArray());

                    if (dList != null && dList.Count > 0)
                    {
                       /* foreach (OpMeter mItem in meters)
                        {

                        }*/
                    }
                }
                getDataConfigTime = (dataProvider.DateTimeNow - dataConfigTime);
                #endregion
                #region SchemaFiles

                if (OpGasMeterSchema.GetAllFileName().Count > 0)
                {
                    foreach (string fileName in OpGasMeterSchema.GetAllFileName())
                    {
                        if (!OpGasMeterSchema.GasMeterFile.ContainsKey(fileName) || OpGasMeterSchema.GasMeterFile[fileName] == null)
                        {
                            OpFile fItem = dataProvider.GetFileFilter(loadNavigationProperties: false, transactionLevel: IsolationLevel.ReadUncommitted,
                                PhysicalFileName: fileName).FirstOrDefault();
                            if (fItem != null)
                            {
                                dataProvider.GetFileContent(fItem);
                                OpGasMeterSchema.GasMeterFile[fileName] = fItem.FileBytes as byte[];
                            }
                            else
                                OpGasMeterSchema.GasMeterFile[fileName] = null;
                            BaseComponent.Log(EventID.Forms.MeterLoadedSchemaFromDB, new object[] { "-", fileName, OpGasMeterSchema.GasMeterFile[fileName] == null ? "<null>" : OpGasMeterSchema.GasMeterFile[fileName].Length.ToString() });
                        }
                    }
                }

                #endregion
                foreach (OpMeter mItem in meters)
                {
                    OpGasMeterSchema tsItem = new OpGasMeterSchema(mItem.IdMeter, Enums.ReferenceType.IdMeter, new List<OpSchemaElement>())
                    {
                        ReferenceType = Enums.ReferenceType.IdMeter,
                        IdObject = mItem.IdMeter
                    };
                    gmsList.Add(tsItem);
                }
                #region LoadMeasurementData
                List<long> measurementDataType = new List<long>();
                measurementDataType.Add(DataType.GAS_METER_USAGE_VALUE);

                foreach (OpGasMeterSchema gmsItem in gmsList)
                {
                    if (gmsItem.SchemaConfiguration != null && gmsItem.SchemaConfiguration.Count > 0)
                        measurementDataType.AddRange(gmsItem.SchemaConfiguration.Select(c => c.IdDataType).Distinct());
                    if (gmsItem.SchemaConfiguration != null)
                    {
                        foreach (OpSchemaElement seItem in gmsItem.SchemaConfiguration)
                        {
                            seItem.Value = null;
                        }
                    }
                }
                measurementDataType = measurementDataType.Distinct().ToList();

                DateTime dataMeasurementTime = dataProvider.DateTimeNow;
                string readoutsCustomWhereClause = "[ID_DATA_SOURCE] is not null";
                List<Objects.DW.OpData> dDWItem = dataProvider.GetDataFilterDW(IdMeter: meters.Select(m => m.IdMeter).ToArray(), IdDataType: measurementDataType.ToArray(),
                    IndexNbr: new int[] { 0 }, customWhereClause: readoutsCustomWhereClause,
                    transactionLevel: IsolationLevel.ReadUncommitted, loadNavigationProperties: false);
                getDataMeasurementTime = (dataProvider.DateTimeNow - dataMeasurementTime);
                BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "DWData", dDWItem.Count });
                if (dDWItem == null)
                    dDWItem = new List<Objects.DW.OpData>();

                List<OpDataFormatGroupDetails> dataFormatGroupDetails = DataFormatGroupComponent.GetHierarchicalDataFormatGroupDetails(dataProvider as DataProvider, loggedOperator, module);
                Dictionary<long, int?> precisionDict = DataTypeFormatComponent.GetDataTypePrecisionDict(dataFormatGroupDetails, true);
                Dictionary<long, OpUnit> unitDict = dataFormatGroupDetails.Distinct(x => x.ID_DATA_TYPE)
                                                                            .Where(x => x.IdUnitOut.HasValue)
                                                                            .ToDictionary(k => k.IdDataType, v => v.UnitOut == null ? dataProvider.GetUnit(v.IdUnitOut.Value) : v.UnitOut);

                Dictionary<long, OpDataType> dtDict = dataProvider.GetDataType(dDWItem.Select(d => d.IdDataType).Distinct().ToArray()).ToDictionary(d => d.IdDataType);
                Dictionary<int, OpUnit> uDict = dataProvider.GetUnit(dtDict.Select(d => d.Value.IdUnit).Distinct().ToArray()).ToDictionary(d => d.IdUnit);
                foreach (OpDataType dtItem in dtDict.Values)
                    dtItem.Unit = uDict[dtItem.IdUnit];

                if (dDWItem.Count > 0)
                {
                    dDWItem.ForEach(d => { d.DataType = dtDict[d.IdDataType]; d.Unit = uDict[d.DataType.IdUnit]; });
                    dDWItem = UnitComponent.ChangeUnit(dDWItem, unitDict, precisionDict);
                }

                #endregion
                #region FillingSchema
                DateTime dt = dataProvider.DateTimeNow;
                foreach (OpGasMeterSchema gmsItem in gmsList)
                {
                    long idMeter = Convert.ToInt64(gmsItem.IdObject);
                    if (dDWItem != null && dDWItem.Count > 0)
                    {
                        #region DW Data

                        foreach (Objects.DW.OpData dItem in dDWItem.Where(d => d.IdMeter == idMeter))
                        {
                            if (dItem.IdDataType == DataType.GAS_METER_USAGE_VALUE)
                            {
                                double levelDouble = 0.0;
                                if (double.TryParse(dItem.Value.ToString(), out levelDouble))
                                    usageValueDict[dItem.IdMeter.Value] = levelDouble;
                            }
                            OpSchemaElement seItem = gmsItem.SchemaConfiguration.Find(c => c.IdDataType == dItem.IdDataType);
                            if (seItem != null)
                            {
                                seItem.Value = dItem.Value;
                            }
                        }
                        #endregion
                    }

                    //DateTime drawingStartTime = DateTime.Now;
                    //gmsItem.DrawSchema(usageValue: usageValueDict[idMeter], x: 50, y: 90);
                    //TimeSpan drawSchemaTime = (DateTime.Now - drawingStartTime);
                    //BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", String.Format("GasMeter {0} params", idMeter),
                    //    String.Format("ConvertedGasMeterUsageValue: {0}, DrawingTime: {1}[s]", usageValueDict[idMeter], Math.Round(drawSchemaTime.TotalSeconds, 2)) });

                    sList.Add(gmsItem);

                    if (gmsItem.SchemaConfiguration != null)
                    {
                        foreach (OpSchemaElement seItem in gmsItem.SchemaConfiguration)
                        {
                            if (seItem.DataType == null)
                                seItem.DataType = dataProvider.GetDataType(seItem.IdDataType);
                            if (String.IsNullOrEmpty(seItem.Description))
                            {
                                seItem.Description = dataProvider.GetDataType(seItem.DataType.IdDataType).ToString();
                            }
                            if (seItem.Value == null || seItem.Value == default(object) || String.IsNullOrEmpty(seItem.Value.ToString()))
                                seItem.Value = "-";
                        }
                    }

                    BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", "Loaded configuration count", gmsItem.SchemaConfiguration == null ? "<null>" : gmsItem.SchemaConfiguration.Count.ToString() });
                }
                TimeSpan ts = dataProvider.DateTimeNow - dt;
                #endregion

                BaseComponent.Log(EventID.Forms.MeterParameterValues, new object[] { "-", String.Format("GasMeter schema load time (Count: {0})", meters.Count),
                                                                        String.Format("Config: {0}[s], Measurement: {1}[s], SchemaElement: {2}[s]",
                                                                                        (getDataConfigTime.HasValue ? Math.Round(getDataConfigTime.Value.TotalSeconds, 2).ToString() : "<null>"),
                                                                                        (getDataMeasurementTime.HasValue ? Math.Round(getDataMeasurementTime.Value.TotalSeconds, 2).ToString() : "<null>"),
                                                                                        (getDataSchemaElementsTime.HasValue ? Math.Round(getDataSchemaElementsTime.Value.TotalSeconds, 2).ToString() : "<null>"))});
                #endregion
            }
            return sList;
        }

        #endregion
        #endregion
    }
}
