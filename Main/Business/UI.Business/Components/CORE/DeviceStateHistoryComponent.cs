﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
	public class DeviceStateHistoryComponent
	{
        public static void Save(DataProvider dataProvider, List<OpDeviceStateHistory> states, bool useDBCollector = false)
		{
			for (int i = states.Count - 1; i >= 0; i--)
			{
				if (states[i].OpState == OpChangeState.Delete)
				{
					dataProvider.DeleteDeviceStateHistory(states[i], useDBCollector);
					states.RemoveAt(i);
				}
			}

			foreach (OpDeviceStateHistory state in states.Where(w => w.OpState == Objects.OpChangeState.New || w.OpState == OpChangeState.Modified))
			{
                state.IdDeviceState = dataProvider.SaveDeviceStateHistory(state, useDBCollector: useDBCollector);
				if (state.OpState == OpChangeState.New)
					state.AssignReferences(dataProvider);
				state.OpState = OpChangeState.Loaded;
			}
		}

		public static void Save(DataProvider dataProvider, OpDeviceStateHistory state)
		{
			dataProvider.SaveDeviceStateHistory(state);
		}


		public static void CloseActiveEntries(DataProvider dataProvider, long serialNbr)
		{
			foreach (var loop in dataProvider.GetDeviceStateHistoryFilter(SerialNbr: new[] { serialNbr }))
			{
				if (!loop.EndTime.HasValue)
				{
                    loop.EndTime = dataProvider.DateTimeNow;
					Save(dataProvider, loop);
				}
			}
		}

        public static void UndoLastHistoryEntry(DataProvider dataProvider, long serialNbr, bool useDBCollector = false)
        {
            List<OpDeviceStateHistory> deviceStateHistory = dataProvider.GetDeviceStateHistoryFilter(SerialNbr: new long[] { serialNbr }, useDBCollector: useDBCollector);
            if (deviceStateHistory != null && deviceStateHistory.Count > 0)
            {
                OpDeviceStateHistory toDelete = deviceStateHistory.Last();
                dataProvider.DeleteDeviceStateHistory(toDelete, useDBCollector: useDBCollector);
                deviceStateHistory.Remove(toDelete);
                if (deviceStateHistory.Count > 0)
                {
                    OpDeviceStateHistory toUpdate = deviceStateHistory.Last();
                    toUpdate.EndTime = null; 
                    dataProvider.SaveDeviceStateHistory(toUpdate, useDBCollector: useDBCollector);
                }
            }
        }
	}
}
