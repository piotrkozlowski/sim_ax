﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using System.Globalization;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class BaseComponent
    {
        public enum DataBase { CORE = 1, DW = 2, DAQ = 3 }

        /// <summary>
        /// set only if Module, LoggedOperator and ServerCORE are set
        /// </summary>
        public static bool LogCustomEvents = false;

        public static Enums.Module Module { get; set; }
        public static OpOperator LoggedOperator { get; set; }
        public static string ServerCORE { get; set; }
        public static string ServerDAQ { get; set; }
        public static string ServerDW { get; set; }

        private static CultureInfo _CultureInfo { get; set; }
        public static CultureInfo CultureInfo { get { return _CultureInfo; } set { _CultureInfo = value; ResourcesText.Culture = value; } }

        #region Log

        public static void Log(LogData logData, object[] paramsObj)
        {
            Log(logData, DataBase.DW, true, paramsObj);
        }

        public static void Log(LogData logData, bool useDefaultLogLevel, object[] paramsObj)
        {
            Log(logData, DataBase.DW, useDefaultLogLevel, paramsObj);
        }

        public static void Log(LogData logData, DataBase dataBase, bool useDefaultLogLevel, object[] paramsObj)
        {
            string server;
            switch (dataBase)
            {
                case DataBase.CORE:
                    server = ServerCORE;
                    break;
                case DataBase.DW:
                    server = ServerDW;
                    break;
                case DataBase.DAQ:
                    server = ServerDAQ;
                    break;
                default:
                    server = "";
                    break;
            }
            IMRLog.AddToLog(Module, useDefaultLogLevel, logData, paramsObj);
        }

        public static void LogSuccess(LogData logData, object objectID)
        {
            LogSuccess(logData, objectID, DataBase.DW);
        }

        public static void LogSuccess(LogData logData, object objectID, DataBase dataBase)
        {
            LogSuccess(logData, objectID, dataBase, false);
        }

        public static void LogSuccess(LogData logData, object objectID, DataBase dataBase, bool useDefaultLogLevel)
        {
            string server;
            switch (dataBase)
            {
                case DataBase.CORE:
                    server = ServerCORE;
                    break;
                case DataBase.DW:
                    server = ServerDW;
                    break;
                case DataBase.DAQ:
                    server = ServerDAQ;
                    break;
                default:
                    server = "";
                    break;
            }
            IMRLog.AddToLog(Module, useDefaultLogLevel, logData, LoggedOperator.Login, server, objectID);
        }

        public static void LogError(LogData logData, string errorMessage)
        {
            LogError(logData, errorMessage, DataBase.DW);
        }

        public static void LogError(LogData logData, string errorMessage, DataBase dataBase)
        {
            LogError(logData, null, errorMessage, dataBase);
        }

        public static void LogError(LogData logData, object objectID, string errorMessage)
        {
            LogError(logData, objectID, errorMessage, DataBase.DW);
        }

        public static void LogError(LogData logData, object objectID, string errorMessage, DataBase dataBase)
        {
            LogError(logData, objectID, errorMessage, dataBase, false);
        }

        public static void LogError(LogData logData, object objectID, string errorMessage, DataBase dataBase, bool useDefaultLogLevel)
        {
            string server;
            switch (dataBase)
            {
                case DataBase.CORE:
                    server = ServerCORE;
                    break;
                case DataBase.DW:
                    server = ServerDW;
                    break;
                case DataBase.DAQ:
                    server = ServerDAQ;
                    break;
                default:
                    server = "";
                    break;
            }
            if (objectID == null)
                IMRLog.AddToLog(Module, useDefaultLogLevel, logData, LoggedOperator.Login, server, errorMessage);
            else
                IMRLog.AddToLog(Module, useDefaultLogLevel, logData, LoggedOperator.Login, server, objectID, errorMessage);
        }
        #endregion

        #region GetSystemType

        public static Type GetSystemType(string type)
        {
            switch (type.ToLower())
            {
                case "integer":
                    return typeof(int);
                case "datetime":
                    return typeof(DateTime);
                case "float":
                    return typeof(float);
                case "boolean":
                    return typeof(bool);
                case "string":
                default:
                    return typeof(string);
            }
        }

        #endregion

        #region CreateInCustomWhereClause

        public static List<string> CreateInCustomWhereClause(string fieldName, List<object> customWhereClauseList)
        {
            List<string> retList = new List<string>();
            if (!String.IsNullOrEmpty(fieldName) && customWhereClauseList != null && customWhereClauseList.Count(c => c != null) > 0)
            {
                customWhereClauseList = customWhereClauseList.Where(c => c != null).ToList();
                bool isString = customWhereClauseList[0].GetType() == typeof(string);
                fieldName = fieldName.Trim();
                if (!fieldName.StartsWith("["))
                    fieldName = "[" + fieldName;
                if (!fieldName.EndsWith("]"))
                    fieldName = fieldName + "]";
                string customWhereClauseItem = fieldName + " IN (";
                foreach (object oItem in customWhereClauseList)
                {
                    string nextEntry = oItem.ToString();
                    if (isString)
                        nextEntry = "'" + nextEntry + "'";
                    nextEntry += ",";
                    if ((customWhereClauseItem + nextEntry).Length > 3500)//customWhereClause max lenght = 4000
                    {
                        customWhereClauseItem = customWhereClauseItem.Substring(0, customWhereClauseItem.Length - 1);//pozbywamy się przecinka
                        customWhereClauseItem += ")";
                        retList.Add(customWhereClauseItem);
                        customWhereClauseItem = fieldName + " IN (";
                    }
                    customWhereClauseItem += nextEntry;
                }
                customWhereClauseItem = customWhereClauseItem.Substring(0, customWhereClauseItem.Length - 1);//pozbywamy się przecinka
                customWhereClauseItem += ")";
                retList.Add(customWhereClauseItem);
            }
            return retList;
        }

        #endregion
    }
}
