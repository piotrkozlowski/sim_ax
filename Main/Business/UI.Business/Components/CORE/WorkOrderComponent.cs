﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class RouteComponent : BaseComponent
    {
        #region Methods
        #region GetByID

        public static OpRoute GetByID(DataProvider dataProvider, long Id)
        {
            return GetByID(dataProvider, Id, false);
        }
        
        

        public static OpRoute GetByID(DataProvider dataProvider, long Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetRoute(Id, queryDatabase);
        }

        #endregion
        #region GetAll

        public static List<OpRoute> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllRoute();
        }
        
        #endregion
        #region GetRoutePointForTask

        public static OpRoutePoint GetRoutePointForTask(DataProvider dataProvider, int idTask)
        {
            return dataProvider.GetRoutePointFilter(IdTask: new int[] { idTask }).FirstOrDefault(); //RoutePoint.FirstOrDefault(rp => rp.IdTask == idTask);
        }

        public static List<OpRoutePoint> GetRoutePointsForTasks(DataProvider dataProvider, OpTask[] tasks)
        {
            List<OpRoutePoint> routePointList = dataProvider.GetRoutePointFilter(IdTask: tasks.Select(x => x.IdTask).ToArray());

            if (routePointList == null)
            {
                routePointList = new List<OpRoutePoint>();
            }

            int[] existingRoutePointTaskIds = routePointList.Select(x => x.IdTask.Value).ToArray();
            routePointList.AddRange(CreateRoutePointsForTasks(dataProvider, tasks.Where(x => !x.IdTask.In(existingRoutePointTaskIds)).ToArray()));

            return routePointList;
        }
        
        #endregion
        #region GetRoutePoints

        public static List<OpRoutePoint> GetRoutePoints(DataProvider dataProvider)
        {
            return GetRoutePoints(dataProvider, null);
        }
        
        
        public static List<OpRoutePoint> GetRoutePoints(DataProvider dataProvider, long? idRoute)
        {
            if (idRoute.HasValue)
            {
                if (dataProvider.RoutePointCacheEnabled)
                    return dataProvider.GetAllRoutePoint().Where(rp => rp.IdRoute == idRoute).OrderBy(rp => rp.StartDateTime).ToList();
                else
                    return dataProvider.GetRoutePointFilter(IdRoute: new long[] { idRoute.Value }).OrderBy(rp => rp.StartDateTime).ToList();
            }
            else
                return dataProvider.GetAllRoutePoint().ToList();
        }

        public static List<OpRoutePoint> GetRoutePoints(DataProvider dataProvider, long idRoute, OpRoutePoint.RoutePointType type)
        {
            //return dataProvider.GetAllRoutePoint().Where(rp => rp.IdRoute == idRoute && rp.Type == (int)type).OrderBy(rp => rp.StartDateTime).ToList();
            return dataProvider.GetRoutePointFilter(IdRoute: new long[] { idRoute }, Type: new int[] { (int)type }).OrderBy(rp => rp.StartDateTime).ToList();
        }
        #endregion
        #region CreateRoutePointsForTasks

        public static List<OpRoutePoint> CreateRoutePointsForTasks(DataProvider dataProvider, OpTask[] tasks)
        {
            OpTaskStatus.Enum[] validStatuses = Enum.GetValues(typeof(OpTaskStatus.Enum))
                .Cast<OpTaskStatus.Enum>().Where(x => x != OpTaskStatus.Enum.Not_Started && x != OpTaskStatus.Enum.Planned).ToArray();

            OpTask[] modifiedTasks = tasks.Where(x => ((OpTaskStatus.Enum)x.IdTaskStatus).In(validStatuses)).ToArray();

            List<OpRoutePoint> rpList = new List<OpRoutePoint>();

            Dictionary<long, OpLocation> lDict = new Dictionary<long, OpLocation>();
            if (modifiedTasks.ToList().Exists(t => t.IdLocation.HasValue && t.Location == null))
            {
                List<long> idLocationToGet = modifiedTasks.Where(t => t.IdLocation.HasValue && t.Location == null).Select(t => t.IdLocation.Value).Distinct().ToList();
                lDict = dataProvider.GetLocationFilter(autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, loadNavigationProperties: false, mergeIntoCache: false,
                                                        loadCustomData: false, customDataTypes: new List<long>() { DataType.LOCATION_LATITUDE, DataType.LOCATION_LONGITUDE },
                                                        IdLocation: idLocationToGet.ToArray()).ToDictionary(l => l.IdLocation);

            }

            foreach (OpTask tItem in modifiedTasks)
            {
                OpRoutePoint rpItem = rpList.Find(r => r.IdTask == tItem.IdTask);
                if (rpItem == null)//!rpList.Exists(r => r.IdTask == tItem.IdTask))
                {
                    rpItem = new OpRoutePoint();
                    rpItem.IdRoute = tItem.IdPlannedRoute.HasValue ? tItem.IdPlannedRoute.Value : 0;
                    rpItem.Type = 2;

                    if (tItem.IdLocation.HasValue)
                    {
                        if (tItem.Location != null)
                        {
                            rpItem.Latitude = tItem.Location.Latitude;
                            rpItem.Longitude = tItem.Location.Longitude;
                        }
                        else if (lDict.ContainsKey(tItem.IdLocation.Value))
                        {
                            rpItem.Latitude = lDict[tItem.IdLocation.Value].Latitude;
                            rpItem.Longitude = lDict[tItem.IdLocation.Value].Longitude;
                        }
                    }

                    rpItem.IdTask = tItem.IdTask;
                    rpItem.StartDateTime = tItem.CreationDate;
                    rpItem.EndDateTime = tItem.CreationDate.AddHours(1);
                    rpItem.Distributor = tItem.Distributor;
                    rpItem.IdDistributor = tItem.IdDistributor;
                    rpItem.IdRoutePoint = dataProvider.SaveRoutePoint(rpItem);
                    rpList.Add(rpItem);

                }

                if (rpItem.StartDateTime == tItem.CreationDate)
                {
                    if (tItem.FitterVisitDate.HasValue)
                    {
                        rpItem.StartDateTime = tItem.FitterVisitDate.Value;
                        rpItem.EndDateTime = rpItem.StartDateTime.Value.AddHours(1);
                        dataProvider.SaveRoutePoint(rpItem);
                    }
                    else
                    {
                        if (tItem.IdTaskStatus == (int)OpTaskStatus.Enum.WaitingForAcceptance
                            || tItem.IdTaskStatus == (int)OpTaskStatus.Enum.Verified
                            || tItem.IdTaskStatus == (int)OpTaskStatus.Enum.Rejected
                            || tItem.IdTaskStatus == (int)OpTaskStatus.Enum.RealizationNotPossible
                            || tItem.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_succesfully
                            || tItem.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_with_error)
                        {
                            rpItem.StartDateTime = dataProvider.DateTimeNow;
                            rpItem.EndDateTime = rpItem.StartDateTime.Value.AddHours(1);
                            dataProvider.SaveRoutePoint(rpItem);
                        }
                    }
                }
            }

            return rpList;
        }

        public static OpRoutePoint CreateRoutePointForTask(DataProvider dataProvider, OpTask task)
        {
            /* OpTaskStatus.Enum[] validStatuses = Enum.GetValues(typeof(OpTaskStatus.Enum))
                 .Cast<OpTaskStatus.Enum>().Where(x => x != OpTaskStatus.Enum.Not_Started && x != OpTaskStatus.Enum.Planned).ToArray();
             if (!((OpTaskStatus.Enum)task.IdTaskStatus).In((validStatuses))) return null;*/

            OpRoutePoint routePoint = new OpRoutePoint();
            routePoint.IdRoute = task.IdPlannedRoute.HasValue ? task.IdPlannedRoute.Value : 0;
            routePoint.Type = 2;

            if (task.Location != null)
            {
                routePoint.Latitude = task.Location.Latitude;
                routePoint.Longitude = task.Location.Longitude;
            }
            else if (task.IdLocation.HasValue)
            {
                List<OpLocation> locationList = dataProvider.GetLocationFilter(autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, loadNavigationProperties: false, mergeIntoCache: false,
                                                                            loadCustomData: false, customDataTypes: new List<long>() { DataType.LOCATION_LATITUDE, DataType.LOCATION_LONGITUDE },
                                                                            IdLocation: new long[] { task.IdLocation.Value }).ToList();

                if (locationList != null && locationList.Count > 0)
                {
                    routePoint.Latitude = locationList.First().Latitude;
                    routePoint.Longitude = locationList.First().Longitude;
                }
            }

            routePoint.IdTask = task.IdTask;
            routePoint.StartDateTime = task.CreationDate;
            routePoint.EndDateTime = task.CreationDate.AddHours(1);
            routePoint.Distributor = task.Distributor;
            routePoint.IdDistributor = task.IdDistributor;
            routePoint.IdRoutePoint = dataProvider.SaveRoutePoint(routePoint);

            if (routePoint.StartDateTime == task.CreationDate)
            {
                if (task.FitterVisitDate.HasValue)
                {
                    routePoint.StartDateTime = task.FitterVisitDate.Value;
                    routePoint.EndDateTime = routePoint.StartDateTime.Value.AddHours(1);
                    dataProvider.SaveRoutePoint(routePoint);
                }
                else
                {
                    if (task.IdTaskStatus == (int)OpTaskStatus.Enum.WaitingForAcceptance
                        || task.IdTaskStatus == (int)OpTaskStatus.Enum.Verified
                        || task.IdTaskStatus == (int)OpTaskStatus.Enum.Rejected
                        || task.IdTaskStatus == (int)OpTaskStatus.Enum.RealizationNotPossible
                        || task.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_succesfully
                        || task.IdTaskStatus == (int)OpTaskStatus.Enum.Finished_with_error)
                    {
                        routePoint.StartDateTime = dataProvider.DateTimeNow;
                        routePoint.EndDateTime = routePoint.StartDateTime.Value.AddHours(1);
                        dataProvider.SaveRoutePoint(routePoint);
                    }
                }
            }

            return routePoint;
        }

        #endregion
        #region GetRoutePoint

        public static OpRoutePoint GetRoutePoint(DataProvider dataProvider, OpTask task)
        {
            List<OpRoutePoint> routePointList = dataProvider.GetRoutePointFilter(IdTask: new int[] { task.IdTask });
            OpRoutePoint routePoint = null;

            if (routePointList == null || routePointList.Count == 0)
            {
                routePoint = CreateRoutePointForTask(dataProvider, task);
            }
            else
            {
                routePoint = routePointList.First();
            }

            return routePoint;
        }
        
        #endregion
        #region GetRoutePlannedTasks

        //public static List<OpTask> GetRoutePlannedTasks(DataProvider dataProvider, long idRoute)
        //{
        //    //return dataProvider.GetAllTask().Where(t => t.IdPlannedRoute == idRoute && !dataProvider.GetAllRoutePoint().Any(rp => rp.IdTask == t.IdTask)).ToList();
        //    return dataProvider.GetTaskFilter(IdPlannedRoute: new long[] {idRoute},  ).Where(t => t.IdPlannedRoute == idRoute && !dataProvider.GetAllRoutePoint().Any(rp => rp.IdTask == t.IdTask)).ToList();
        //}
        
        #endregion
        #region GetTasks

        public static List<OpTask> GetTasks(DataProvider dataProvider, long IdRoute)
        {
            //dataProvider.TaskCacheEnabled = false;
            return dataProvider.GetTaskFilter(IdPlannedRoute: new long[] { IdRoute });//.Where(t => t.IdPlannedRoute == IdRoute).ToList();
        }
        
        #endregion
        #region GetRouteStatus

        public static List<OpRouteStatus> GetRouteStatus(DataProvider dataProvider)
        {
            List<int> idExcluded = new List<int>() { 5, 6, 7, 8 };
            return dataProvider.GetAllRouteStatus().Where(rs => !idExcluded.Contains(rs.IdRouteStatus)).ToList();
        }
        
        #endregion
        #region GetRecentlyAdded

        public static List<OpRoute> GetRecentlyAdded(DataProvider dataProvider, int topCount)
        {
            //return dataProvider.GetWorkOrdersRecentlyAdded(topCount).ToList();
            return null;
        }
        
        #endregion
        #region GetForOperator

        public static List<OpRoute> GetForOperator(DataProvider dataProvider, int idOperator)
        {
            return dataProvider.GetRouteFilter(IdOperatorExecutor: new int[] { idOperator }); // Route.Where(t => t.IdOperatorExecutor == idOperator).ToList();
        }

        #endregion
        #region MapRouteType
        public static OpTaskType MapRouteType(DataProvider dataProvider, OpRouteType routeType)
        {
            return dataProvider.GetTaskType((int)MapRouteType((Enums.RouteType)routeType.IdRouteType));
        }
        public static OpTaskType.Enum MapRouteType(Enums.RouteType routeType)
        {
            OpTaskType.Enum taskType = OpTaskType.Enum.Maintenance;

            switch(routeType)
            {
                case Enums.RouteType.INSTALLATION:
                    taskType = OpTaskType.Enum.Installation;
                    break;
                case Enums.RouteType.SERVICE:
                    taskType = OpTaskType.Enum.Service;
                    break;
                case Enums.RouteType.READOUT:
                    taskType = OpTaskType.Enum.Readout;
                    break;
                default:
                    taskType = OpTaskType.Enum.Maintenance;
                    break;
            }

            return taskType;
        }

        #endregion
        #region MapRouteStatus
        public static OpTaskStatus MapRouteStatus(DataProvider dataProvider, OpRouteStatus routeStatus)
        {
            return dataProvider.GetTaskStatus((int)MapRouteStatus((Enums.RouteStatus)routeStatus.IdRouteStatus));
        }
        public static OpTaskStatus.Enum MapRouteStatus(Enums.RouteStatus routeStatus)
        {
            OpTaskStatus.Enum taskStatus = OpTaskStatus.Enum.Unknown;

            switch (routeStatus)
            {
                case Enums.RouteStatus.PLANNED:
                    taskStatus = OpTaskStatus.Enum.Planned;
                    break;
                case Enums.RouteStatus.IN_PROGRESS:
                    taskStatus = OpTaskStatus.Enum.In_progress;
                    break;
                default:
                    taskStatus = OpTaskStatus.Enum.Unknown;
                    break;
            }

            return taskStatus;
        }

        #endregion
        #region FilterByTasks

        public static List<OpRoute> FilterByTasks(DataProvider dataProvider, List<OpRoute> routes, List<int> allowedDistributors, List<OpTask> tasksInRoutes = null)
        {
            List<OpRoute> allowedList = new List<OpRoute>();
            if (tasksInRoutes == null || tasksInRoutes.Count == 0)
                tasksInRoutes = dataProvider.GetTaskFilter(IdPlannedRoute: routes.Select(r => r.IdRoute).ToArray(),
                                                           autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                                                           loadNavigationProperties: false);
            Dictionary<long, List<OpTask>> routeTasksDict = new Dictionary<long, List<OpTask>>();
            foreach (OpRoute opRoute in routes) //inicjalizacja slownika
            {
                routeTasksDict.Add(opRoute.IdRoute, new List<OpTask>());
            }
            /* foreach (OpRoute opRoute in routes) //uzupelniamy slownik
             {
                 routeTasksDict[opRoute.IdRoute].AddRange(tasksInRoutes.FindAll(t => t.IdPlannedRoute.HasValue && t.IdPlannedRoute.Value == opRoute.IdRoute));
             }*/
            foreach (OpTask tItem in tasksInRoutes)//szybsze uzupełnianie słownika
            {
                if (tItem.IdPlannedRoute.HasValue && routeTasksDict.ContainsKey(tItem.IdPlannedRoute.Value))
                {
                    routeTasksDict[tItem.IdPlannedRoute.Value].Add(tItem);
                }
            }
            foreach (OpRoute route in routes)
            {
                if (allowedDistributors.Contains(route.IdDistributor))
                {
                    allowedList.Add(route);
                }
                else if (routeTasksDict.ContainsKey(route.IdRoute) && routeTasksDict[route.IdRoute].Any(t => allowedDistributors.Contains(t.IdDistributor)))
                {
                    allowedList.Add(route);
                }
            }
            return allowedList;
        }
        
        #endregion
        #region Save

        public static OpRoute Save(DataProvider dataProvider, OpRoute objectToSave)
        {
            return Save(dataProvider, objectToSave, true);
        }

        public static OpRoute Save(DataProvider dataProvider, OpRoute objectToSave, bool emailNotification, OpOperator loggedOperator = null)
        {
            return Save(dataProvider, objectToSave, emailNotification, false, loggedOperator, false);
        }

        public static OpRoute Save(DataProvider dataProvider, OpRoute objectToSave, bool emailNotification, bool smsNotification, OpOperator loggedOperator = null, bool useDBCollector = false)
        {
            bool isNewItem = objectToSave.IdRoute == 0; //check if it's new work order
            bool dataListChanged = !objectToSave.DataList.All(w => w.OpState == OpChangeState.Loaded);

            if (objectToSave.OpState == OpChangeState.Loaded && !dataListChanged)
                return objectToSave;

            try
            {
                OpRoute originalRoute = null;
                if (objectToSave.IdRoute > 0)
                    originalRoute = dataProvider.GetRoute(objectToSave.IdRoute);

                if (isNewItem || objectToSave.OpState == OpChangeState.Modified)
                {
                    //zapisz date zamkniecia trasy (jesli nie jest ustawiona recznie) jeśli zamykamy trase
                    if (objectToSave.IdRouteStatus == (int)OpRouteStatus.Enum.Finished && !objectToSave.DateFinished.HasValue)
                        objectToSave.DateFinished = dataProvider.DateTimeNow;

                    //zapisz date zaakceptowania trasy (jesli nie jest ustawiona recznie) jeśli ackteptujemy trase
                    if (objectToSave.IdRouteStatus == (int)OpRouteStatus.Enum.Accepted && !objectToSave.DateApproved.HasValue)
                        objectToSave.DateApproved = dataProvider.DateTimeNow;

                    dataProvider.SaveRoute(objectToSave);

                    objectToSave.OpState = OpChangeState.Loaded;
                }

                //zapis do tabeli ROUTE_DATA
                if (isNewItem || dataListChanged)
                {
                    OpRouteData data = null;
                    for (int i = 0; i < objectToSave.DataList.Count; i++)
                    {
                        data = objectToSave.DataList[i];
                        data.IdRoute = objectToSave.IdRoute;
                        if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                        {
                            DataComponent.Delete(dataProvider, data);
                        }
                        else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                        {
                            objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                            if (objectToSave.DataList[i].OpState == OpChangeState.New)
                                objectToSave.DataList[i].AssignReferences(dataProvider);

                            objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                        }
                    }
                }
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdRoute);
                        LogSuccess(EventID.Forms.WorkOrderAdded, objectToSave.IdRoute);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdRoute);
                        LogSuccess(EventID.Forms.WorkOrderSaved, objectToSave.IdRoute);
                }
                if (emailNotification)
                {
                    Dictionary<OpRoute, OpRoute> email = new Dictionary<OpRoute, OpRoute>();
                    email.Add(objectToSave, originalRoute);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, email, isNewItem ? EmailComponent.OperationType.Add : EmailComponent.OperationType.Change);
                }
                if (smsNotification)
                {
                    Dictionary<OpRoute, OpRoute> sms = new Dictionary<OpRoute, OpRoute>();
                    sms.Add(objectToSave, originalRoute);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, isNewItem ? SmsComponent.OperationType.Add : SmsComponent.OperationType.Change, useDBCollector);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.WorkOrderAdditionError, ex.Message);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdRoute, ex.Message);
                        LogError(EventID.Forms.WorkOrderSavingError, objectToSave.IdRoute, ex.Message);
                }
                throw ex;
            }

            //return dataProvider.GetRoute(objectToSave.IdRoute);
            return objectToSave;
        }

        #endregion
        #region SaveRoutePoint

        public static OpRoutePoint SaveRoutePoint(DataProvider dataProvider, OpRoutePoint objectToSave)
        {
            bool isNewItem = objectToSave.IdRoutePoint == 0; //check if it's new routePoint

            try
            {
                dataProvider.SaveRoutePoint(objectToSave);

                //zapis do tabeli ROUTE_POINT_DATA
                OpRoutePointData data = null;
                for (int i = 0; i < objectToSave.DataList.Count; i++)
                {
                    data = objectToSave.DataList[i];
                    data.IdRoutePoint = objectToSave.IdRoutePoint;
                    //zapis do bazy tylko tych ktore sa IsEditable na true lub jesli obiekt ktory dodajemy jest nowym obiektem wiec dodajemy dane po raz pierwszy
                    if (isNewItem || dataProvider.GetDataType(data.IdDataType).IsEditable)
                    {
                        objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                    }
                }
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdRoute);
                    LogSuccess(EventID.Forms.WorkOrderSaved, objectToSave.IdRoute);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdRoute, ex.Message);
                    LogError(EventID.Forms.WorkOrderSavingError, objectToSave.IdRoute, ex.Message);
                throw ex;
            }

            return dataProvider.GetRoutePoint(objectToSave.IdRoutePoint);
        }
        
        #endregion
        #region DeleteData

        public static void DeleteData(DataProvider dataProvider, long idRoute)
        {
            foreach (var item in dataProvider.GetRouteDataFilter(IdRoute: new long[1] { idRoute }, IdDataType: DataProvider.RouteDataTypes.ToArray()))
            {
                dataProvider.DeleteRouteData(item);
            }
        }
        
        #endregion
        #region DeleteRoutePoint

        public static void DeleteRoutePoint(DataProvider dataProvider, OpOperator loggedOperator, OpRoutePoint objectToDelete)
        {
            try
            {

                DeleteRoutePointData(dataProvider, objectToDelete.IdRoutePoint);
                dataProvider.DeleteRoutePoint(objectToDelete);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdTask);
                    Log(EventID.Forms.RoutePointDeleted, true, new object[] { loggedOperator, "localhost", objectToDelete.IdRoutePoint });
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdTask, ex.Message);
                    Log(EventID.Forms.RoutePointDeletionError, true, new object[] { loggedOperator, "localhost", objectToDelete.IdRoutePoint, ex.Message });
                throw ex;
            }
        }
        
        #endregion
        #region DeleteRoutePointData

        public static void DeleteRoutePointData(DataProvider dataProvider, int idRoutePoint)
        {
            foreach (var item in dataProvider.GetRoutePointDataFilter(IdRoutePoint: new int[1] { idRoutePoint }, IdDataType: DataProvider.RoutePointDataTypes.ToArray()))
            {
                dataProvider.DeleteRoutePointData(item);
            }
        }

        public static void DeleteRoutePointData(DataProvider dataProvider, OpRoutePoint routePoint, long idDataType, int index)
        {
            long idData = routePoint.DataList.First(d => d.IdDataType == idDataType && d.Index == index).IdData;
            if (idData > 0)
                dataProvider.DeleteRoutePointData(dataProvider.GetRoutePointData(idData));
        }
        
        #endregion
        #region Delete
		
        public static void Delete(DataProvider dataProvider, OpRoute objectToDelete)
        {
            Delete(dataProvider, objectToDelete, true, false, useDBCollector: false);
        }

        public static void Delete(DataProvider dataProvider, OpRoute objectToDelete, bool emailNotification, OpOperator loggedOperator = null)
        {
            Delete(dataProvider, objectToDelete, true, false, loggedOperator, useDBCollector: false);
        }

        public static void Delete(DataProvider dataProvider, OpRoute objectToDelete, bool emailNotification, bool smsNotification, OpOperator loggedOperator = null, bool useDBCollector = false)
        {
            try
            {
                DeleteData(dataProvider, objectToDelete.IdRoute);
                dataProvider.DeleteRoute(objectToDelete);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdRoute);
                    LogSuccess(EventID.Forms.WorkOrderDeleted, objectToDelete.IdRoute);
                if (emailNotification)
                {
                    Dictionary<OpRoute, OpRoute> email = new Dictionary<OpRoute, OpRoute>();
                    email.Add(objectToDelete, null);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, email, EmailComponent.OperationType.Delete);
                }
                if (smsNotification)
                {
                    Dictionary<OpRoute, OpRoute> sms = new Dictionary<OpRoute, OpRoute>();
                    sms.Add(objectToDelete, null);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, SmsComponent.OperationType.Delete, useDBCollector);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.WorkOrderDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdRoute, ex.Message);
                    LogError(EventID.Forms.WorkOrderDeletionError, objectToDelete.IdRoute, ex.Message);
                throw ex;
            }
        }
        
 
	    #endregion

        #region SetHelperValues

        public static void SetHelperValues(DataProvider dataProvider, OpRoute route, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdRoute, false);

            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_ROUTE:
                        route.DataList.SetValue(DataType.HELPER_ID_ROUTE, 0, route.IdRoute);
                        break;
                    case DataType.HELPER_ID_ROUTE_DEF:
                        route.DataList.SetValue(DataType.HELPER_ID_ROUTE_DEF, 0, route.IdRouteDef);
                        break;
                    case DataType.HELPER_YEAR:
                        route.DataList.SetValue(DataType.HELPER_YEAR, 0, route.Year);
                        break;
                    case DataType.HELPER_MONTH:
                        route.DataList.SetValue(DataType.HELPER_MONTH, 0, route.Month);
                        break;
                    case DataType.HELPER_WEEK:
                        route.DataList.SetValue(DataType.HELPER_WEEK, 0, route.Week);
                        break;
                    case DataType.HELPER_ID_OPERATOR_EXECUTOR:
                        route.DataList.SetValue(DataType.HELPER_ID_OPERATOR_EXECUTOR, 0, route.IdOperatorExecutor);
                        break;
                    case DataType.HELPER_ID_OPERATOR_APPROVED:
                        route.DataList.SetValue(DataType.HELPER_ID_OPERATOR_APPROVED, 0, route.IdOperatorApproved);
                        break;
                    case DataType.HELPER_DATE_UPLOADED:
                        route.DataList.SetValue(DataType.HELPER_DATE_UPLOADED, 0, route.DateUploaded);
                        break;
                    case DataType.HELPER_DATE_APPROVED:
                        route.DataList.SetValue(DataType.HELPER_DATE_APPROVED, 0, route.DateApproved);
                        break;
                    case DataType.HELPER_DATE_FINISHED:
                        route.DataList.SetValue(DataType.HELPER_DATE_FINISHED, 0, route.DateFinished);
                        break;
                    case DataType.HELPER_ID_ROUTE_STATUS:
                        route.DataList.SetValue(DataType.HELPER_ID_ROUTE_STATUS, 0, route.IdRouteStatus);
                        break;
                    case DataType.HELPER_ID_ROUTE_TYPE:
                        route.DataList.SetValue(DataType.HELPER_ID_ROUTE_TYPE, 0, route.IdRouteType);
                        break;
                    case DataType.HELPER_CREATION_DATE:
                        route.DataList.SetValue(DataType.HELPER_CREATION_DATE, 0, route.CreationDate);
                        break;
                    case DataType.HELPER_EXPIRATION_DATE:
                        route.DataList.SetValue(DataType.HELPER_EXPIRATION_DATE, 0, route.ExpirationDate);
                        break;
                    case DataType.HELPER_NAME:
                        route.DataList.SetValue(DataType.HELPER_NAME, 0, route.Name);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        route.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, route.IdDistributor);
                        break;
                }
            }

            route.DataList.ForEach(d => { if (d.DataType == null) { d.DataType = dataProvider.GetDataType(d.IdDataType); } });
        }

        #endregion

        #endregion        
    }
}
