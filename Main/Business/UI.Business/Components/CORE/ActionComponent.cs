﻿// simax_todo - na okres przejsciowy, potem do usuniecia (zarowno obsluga uruchamiania akcji, referencje, jak i plik w Main/References)
// Zrobione, aby obsluzyc uruchomienie akcji w przypadku nowaAplikacja-stareModuly:
// nowa aplikacja ma juz dll bez "AIUT." w nazwie, stare moduly oczekuja dll z "AIUT." w nazwie, dlatego uruchamiamy akcje uzywajac statycznej dll z "AIUT." w nazwie.
// Po okresie przejsciowym, kiedy wszystkie serwery beda mialy zaktualizowane moduly (i dll bez "AIUT." w nazwie) nalezy sie tego rozwiazania pozbyc.
#if !OLD_NAME
extern alias AIUT_COMMON;
#endif

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using IMR.Suite.Common;
using IMR.Suite.Common.MSMQ;
using IMR.Suite.UI.Business.Objects.CORE;

using IMRAction = IMR.Suite.Common.Action;

#if OLD_NAME
using AIUT = IMR.Suite.Common;
#else
using AIUT = AIUT_COMMON.IMR.Suite.Common;
#endif

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ActionComponent
    {
        #region SaveAction
        /// <summary>
        /// Saves action to database.
        /// </summary>
        /// <param name="dataProvider">Instance of data provider.</param>
        /// <param name="toSave">Instance of OpAction.</param>
        /// <param name="dbCreationDate">Indicates whether CreationDate will be set in database stored procedure as UTC Now.</param>
        /// <returns>Action id.</returns>
        public static long SaveAction(DataProvider dataProvider, OpAction toSave, bool dbCreationDate)
        {
            if (dbCreationDate)
                toSave.CreationDate = DateTime.MinValue; // sdudzik - MinValue po to, aby w parametrze procedury poszedl null, a wtedy czas zostanie ustawiony w procedurze na GETUTCDATE

            return dataProvider.SaveAction(toSave);
        }

        /// <summary>
        /// Saves action to database.
        /// </summary>
        /// <param name="dataProvider">Instance of data provider.</param>
        /// <param name="toSave">Instance of OpAction</param>
        /// <param name="dbCreationDate">Indicates whether CreationDate will be set in database stored procedure as UTC Now.</param>
        /// <param name="useDBCollector">Indicates whether DBCollector will be used to save action.</param>
        /// <returns>OpAction object.</returns>
        public static OpAction SaveAction(DataProvider dataProvider, OpAction toSave, bool dbCreationDate, bool useDBCollector = false)
        {
            if (dbCreationDate)
                toSave.CreationDate = DateTime.MinValue;

            return dataProvider.SaveAction(toSave, useDBCollector);
        }
        #endregion

        #region SaveAndRunAction
        /// <summary>
        /// Saves action to database and runs this action.
        /// </summary>
        /// <param name="dataProvider">Instance of data provider.</param>
        /// <param name="actionType">Instance of OpActionType.</param>
        /// <param name="actionStatus">Instance of OpActionStatus.</param>
        /// <param name="device">Instance of OpDevice.</param>
        /// <param name="meter">Instance of OpMeter.</param>
        /// <param name="location">Instance of OpLocation.</param>
        /// <param name="module">Module enum.</param>
        /// <param name="opOperator">Instance of OpOperator.</param>
        /// <param name="idActionData">Action data id.</param>
        /// <param name="dataValues">List of DataValue - Action parameters.</param>
        /// <param name="hostMSMQ">MSMQ host name.</param>
        /// <param name="queueMSMQ">MSMQ queue name.</param>
        /// <param name="action">Saved action.</param>
        /// <param name="exception">Occurred exception.</param>
        /// <param name="useDBCollector">Indicates whether DBCollector will be used. This flag HAS TO BE true if application is connected to _IMRSC databases (Action should not be saved on _IMRSC database).</param>
        /// <param name="serverMSMQDict">Dictionary of servers MSMQ parameters. Key - IdImrServer, Value - tuple of host name (Item1) and queue name (Item2). If null, it will be loaded from database.</param>
        /// <returns>Result of running action operation.</returns>
        public static Enums.RunActionResult SaveAndRunAction(DataProvider dataProvider, OpActionType actionType, OpActionStatus actionStatus, 
            OpDevice device, OpMeter meter, OpLocation location, Enums.Module module, OpOperator opOperator, long? idActionData, List<DataValue> dataValues,
            string hostMSMQ, string queueMSMQ, out OpAction action, out Exception exception, bool useDBCollector = false, Dictionary<int, Tuple<string, string>> serverMSMQDict = null, bool forceQueue = false)
        {
            return SaveAndRunAction(dataProvider, actionType.IdActionType, actionStatus.IdActionStatus, 
                device != null ? device.SerialNbr : (long?)null, meter != null ? meter.IdMeter : (long?)null, location != null ? location.IdLocation : (long?)null,
                (int)module, opOperator.IdOperator, idActionData, dataValues, String.Format("{0}\\private$\\{1}", hostMSMQ, queueMSMQ), out action, out exception, useDBCollector, serverMSMQDict,forceQueue);
        }
        /// <summary>
        /// Saves action to database and runs this action.
        /// </summary>
        /// <param name="dataProvider">Instance of data provider.</param>
        /// <param name="actionType">Instance of OpActionType.</param>
        /// <param name="actionStatus">Instance of OpActionStatus.</param>
        /// <param name="device">Instance of OpDevice.</param>
        /// <param name="meter">Instance of OpMeter.</param>
        /// <param name="location">Instance of OpLocation.</param>
        /// <param name="module">Module enum.</param>
        /// <param name="opOperator">Instance of OpOperator.</param>
        /// <param name="idActionData">Action data id.</param>
        /// <param name="dataValues">List of DataValue - Action parameters.</param>
        /// <param name="queuePath">MSMQ queue path.</param>
        /// <param name="action">Saved action.</param>
        /// <param name="exception">Occurred exception.</param>
        /// <param name="useDBCollector">Indicates whether DBCollector will be used. This flag HAS TO BE true if application is connected to _IMRSC databases (Action should not be saved on _IMRSC database).</param>
        /// <param name="serverMSMQDict">Dictionary of servers MSMQ parameters. Key - IdImrServer, Value - tuple of host name (Item1) and queue name (Item2). If null, it will be loaded from database.</param>
        /// <returns>Result of running action operation.</returns>
        public static Enums.RunActionResult SaveAndRunAction(DataProvider dataProvider, OpActionType actionType, OpActionStatus actionStatus, 
            OpDevice device, OpMeter meter, OpLocation location, Enums.Module module, OpOperator opOperator, long? idActionData, List<DataValue> dataValues,
            string queuePath, out OpAction action, out Exception exception, bool useDBCollector = false, Dictionary<int, Tuple<string, string>> serverMSMQDict = null,bool forceQueue = false)
        {
            return SaveAndRunAction(dataProvider, actionType.IdActionType, actionStatus.IdActionStatus, 
                device != null ? device.SerialNbr : (long?)null, meter != null ? meter.IdMeter : (long?)null, location != null ? location.IdLocation : (long?)null,
                (int)module, opOperator.IdOperator, idActionData, dataValues, queuePath, out action, out exception, useDBCollector, serverMSMQDict, forceQueue);
        }
        /// <summary>
        /// Saves action to database and runs this action.
        /// </summary>
        /// <param name="dataProvider">Instance of data provider.</param>
        /// <param name="idActionType">Action type id.</param>
        /// <param name="idActionStatus">Action status id.</param>
        /// <param name="serialNbr">Device serial number.</param>
        /// <param name="idMeter">Meter id.</param>
        /// <param name="idLocation">Location id.</param>
        /// <param name="idModule">Module id.</param>
        /// <param name="idOperator">Operator id.</param>
        /// <param name="idActionData">Action data id.</param>
        /// <param name="dataValues">List of DataValue - Action parameters.</param>
        /// <param name="hostMSMQ">MSMQ host name.</param>
        /// <param name="queueMSMQ">MSMQ queue name.</param>
        /// <param name="action">Saved action.</param>
        /// <param name="exception">Occurred exception.</param>
        /// <param name="useDBCollector">Indicates whether DBCollector will be used. This flag HAS TO BE true if application is connected to _IMRSC databases (Action should not be saved on _IMRSC database).</param>
        /// <param name="serverMSMQDict">Dictionary of servers MSMQ parameters. Key - IdImrServer, Value - tuple of host name (Item1) and queue name (Item2). If null, it will be loaded from database.</param>
        /// <returns>Result of running action operation.</returns>
        public static Enums.RunActionResult SaveAndRunAction(DataProvider dataProvider, int idActionType, int idActionStatus, 
            long? serialNbr, long? idMeter, long? idLocation, int idModule, int idOperator, long? idActionData, List<DataValue> dataValues,
            string hostMSMQ, string queueMSMQ, out OpAction action, out Exception exception, bool useDBCollector = false, Dictionary<int, Tuple<string, string>> serverMSMQDict = null,bool forceQueue = false)
        {
            return SaveAndRunAction(dataProvider, idActionType, idActionStatus,  serialNbr, idMeter, idLocation, idModule, idOperator, idActionData, dataValues,
                String.Format("{0}\\private$\\{1}", hostMSMQ, queueMSMQ), out action, out exception, useDBCollector, serverMSMQDict,forceQueue);
        }
        /// <summary>
        /// Saves action to database and runs this action.
        /// </summary>
        /// <param name="dataProvider">Instance of data provider.</param>
        /// <param name="idActionType">Action type id.</param>
        /// <param name="idActionStatus">Action status id.</param>
        /// <param name="serialNbr">Device serial number.</param>
        /// <param name="idMeter">Meter id.</param>
        /// <param name="idLocation">Location id.</param>
        /// <param name="idModule">Module id.</param>
        /// <param name="idOperator">Operator id.</param>
        /// <param name="idActionData">Action data id.</param>
        /// <param name="dataValues">List of DataValue - Action parameters.</param>
        /// <param name="queuePath">MSMQ queue path.</param>
        /// <param name="action">Saved action.</param>
        /// <param name="exception">Occurred exception.</param>
        /// <param name="useDBCollector">Indicates whether DBCollector will be used. This flag HAS TO BE true if application is connected to _IMRSC databases (Action should not be saved on _IMRSC database).</param>
        /// <param name="serverMSMQDict">Dictionary of servers MSMQ parameters. Key - IdImrServer, Value - tuple of host name (Item1) and queue name (Item2). If null, it will be loaded from database.</param>
        /// <returns>Result of running action operation.</returns>
        public static Enums.RunActionResult SaveAndRunAction(DataProvider dataProvider, int idActionType, int idActionStatus, 
            long? serialNbr, long? idMeter, long? idLocation, int idModule, int idOperator, long? idActionData, List<DataValue> dataValues,
            string queuePath, out OpAction action, out Exception exception, bool useDBCollector = false, Dictionary<int, Tuple<string, string>> serverMSMQDict = null,bool forceQueue = false)
        {
            action = null;
            exception = null;

            if (!MSMQ.IsInstalled()) return Enums.RunActionResult.MSMQNotInstalled;

            Enums.RunActionResult code = Enums.RunActionResult.OK;

            OpAction newAction = new OpAction();
            newAction.IdActionData = idActionData ?? 0;
            newAction.IdActionType = idActionType;
            newAction.IdActionStatus = idActionStatus;
            newAction.SerialNbr = serialNbr;
            newAction.IdMeter = idMeter;
            newAction.IdLocation = idLocation;
            newAction.IdModule = idModule;
            newAction.IdOperator = idOperator;
            newAction.CreationDate = DateTime.MinValue; // sdudzik - MinValue po to, aby w parametrze procedury poszedl null, a wtedy czas zostanie ustawiony w procedurze na GETUTCDATE
            try
            {
                // zwracany obiekt, a w nim informacja m.in. o serwerze, na ktorym zostala dodana akcja
                // po tym id serwera powinnismy znalezc parametry kolejki z danego serwera
                // useDBCollector MUSI miec wbite true jesli aplikacja jest podlaczona do baz _IMRSC
                // nie chcemy, zeby akcja zostala zapisana na bazie _IMRSC
                action = ActionComponent.SaveAction(dataProvider, newAction, true, useDBCollector);
                if (action != null && action.IdAction != 0)
                {
                    if (!action.ID_SERVER.HasValue) // wyslij do kolejki zdefiniowanej w konfiguracji aplikacji
                    {
                        IMRAction imrAction = new IMRAction(
                            IdActionType: idActionType,
                            SerialNbr: serialNbr.HasValue ? SN.FromDBValue((ulong?)serialNbr.Value) : null,
                            IdMeter: idMeter,
                            IdLocation: idLocation)
                        {
                            Status = (Enums.ActionStatus)idActionStatus,
                            IdActionData = idActionData,
                            ActionData = dataValues != null ? dataValues.ToArray() : new DataValue[0],
                            Invoker = (Enums.Module)idModule,
                            IdOperator = idOperator
                        };
                        imrAction.IdAction = action.IdAction;
                        RunAction(dataProvider, imrAction, null, queuePath, false,forceQueue);
                    }
                    else // wyslij do kolejki zdefiniowanej dla konkretnego serwera
                    {
                        #region Get serverMSMQDict
                        if (serverMSMQDict == null)
                        {
                            try
                            {
                                serverMSMQDict = ImrServerComponent.GetServerActionMSMQDict(dataProvider, new int[] { action.ID_SERVER.Value }, useDBCollector);
                            }
                            catch
                            {
                                serverMSMQDict = new Dictionary<int, Tuple<string, string>>();
                            }
                        }
                        #endregion
                        if (serverMSMQDict.ContainsKey(action.ID_SERVER.Value))
                        {
                            int? serverIdActionType = null;
                            long? serverIdLocation = null;
                            long? serverIdMeter = null;
                            int? serverIdOperator = null;
                            bool? dbCollectorEnabled = dataProvider.IsDBCollectorEnabled(true);
                            if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                            {
                                serverIdActionType = dataProvider.dbCollectorClient.GetServerIdActionType(ref dataProvider.dbCollectorSession, action.ID_SERVER.Value, idActionType);
                                serverIdOperator = dataProvider.dbCollectorClient.GetServerIdOperator(ref dataProvider.dbCollectorSession, action.ID_SERVER.Value, idOperator);
                                if (idLocation.HasValue)
                                    serverIdLocation = dataProvider.dbCollectorClient.GetServerIdLocation(ref dataProvider.dbCollectorSession, action.ID_SERVER.Value, idLocation.Value);
                                if (idMeter.HasValue)
                                    serverIdMeter = dataProvider.dbCollectorClient.GetServerIdMeter(ref dataProvider.dbCollectorSession, action.ID_SERVER.Value, idMeter.Value);
                            }

                            if (serverIdActionType.HasValue
                                && serverIdOperator.HasValue
                                && (!idLocation.HasValue || serverIdLocation.HasValue)
                                && (!idMeter.HasValue || serverIdMeter.HasValue))
                            {
                                IMRAction imrAction = new IMRAction(
                                    IdActionType: serverIdActionType.Value,
                                    SerialNbr: serialNbr.HasValue ? SN.FromDBValue((ulong?)serialNbr.Value) : null,
                                    IdMeter: idMeter.HasValue ? serverIdMeter : null,
                                    IdLocation: idLocation.HasValue ? serverIdLocation : null)
                                {

                                    Status = (Enums.ActionStatus)idActionStatus,
                                    IdActionData = idActionData,
                                    ActionData = dataValues != null ? dataValues.ToArray() : new DataValue[0],
                                    Invoker = (Enums.Module)idModule,
                                    IdOperator = serverIdOperator.Value
                                };

                                imrAction.IdAction = action.IdAction;

                                string host = serverMSMQDict[action.ID_SERVER.Value].Item1;
                                string queue = serverMSMQDict[action.ID_SERVER.Value].Item2;
                                RunAction(dataProvider, imrAction, action.ID_SERVER.Value, String.Format("{0}\\private$\\{1}", host, queue), useDBCollector,forceQueue);
                            }
                            else
                                code = Enums.RunActionResult.MappingNotFound;
                        }
                        else
                            code = Enums.RunActionResult.MSMQParamsNotFound;
                    }
                }
                else
                    code = Enums.RunActionResult.CouldNotRunAction;
            }
            catch (Exception ex)
            {
                exception = ex;
                code = Enums.RunActionResult.Exception;
            }
            return code;
        }
        #endregion
        #region RunAction
        public static void RunAction(DataProvider dataProvider, IMRAction action, int? idServer, string hostName, string queueName, bool useDBCollector = false, bool forceQueue = false)
        {
            RunAction(dataProvider, action, idServer, String.Format("{0}\\private$\\{1}", hostName, queueName), useDBCollector,forceQueue);
        }
        public static void RunAction(DataProvider dataProvider, IMRAction action, int? idServer, string queuePath, bool useDBCollector = false,bool forceQueue = false)
        {
            bool spProcExists = dataProvider.CheckIfStoredProcedureExists("sp_AddActionWithActionDataListToQueue", Enums.DatabaseType.CORE, idServer, useDBCollector);
            System.Version currentVersion = dataProvider.GetIMRServerVersion(Enums.DatabaseType.CORE, idServer, useDBCollector);
            System.Version previousVersion = null;
            System.Version.TryParse(Enums.IMRServerReleaseVersion.Release_4_3_16326_1, out previousVersion);
            if (spProcExists && !forceQueue) // uruchom akcje za pomoca sp_proc jesli taka istnieje
            {
                string xmlActionData = SerializeActionData(action.ActionData);
                dataProvider.AddActionWithActionDataListToQueue(action.IdAction, (long)action.Type, (int)action.Status, action.IdActionData,
                    action.SerialNbr != null ? (long?)action.SerialNbr.DBValue : null, action.IdMeter, action.IdLocation,
                    (int)action.Invoker, action.IdOperator, xmlActionData, queuePath, idServer, useDBCollector: useDBCollector);
            }
            else if (currentVersion != null && previousVersion != null && currentVersion > previousVersion) // dodaj do kolejki wiadomosc Dictionary<string, object> jesli aktualna wersja systemu na to pozwala
                MSMQ.FromPath(queuePath).Send(IMRAction.EncodeMessage(action));
            else // uzyj statycznej dll AIUT.IMR.Suite.Common.dll
            {
                AIUT.Action aiutAction = new AIUT.Action(
                    IdActionType: (int)action.Type,
                    SerialNbr: action.SerialNbr == null ? null : AIUT.SN.FromDBValue(action.SerialNbr.DBValue),
                    IdMeter: action.IdMeter,
                    IdLocation: action.IdLocation)
                {
                    Status = (AIUT.Enums.ActionStatus)action.Status,
                    IdActionData = action.IdActionData,
                    ActionData = action.ActionData != null ? action.ActionData.Select(q => new AIUT.DataValue(q.IdDataType, q.Index, q.Value)).ToArray() : new AIUT.DataValue[0],
                    Invoker = (AIUT.Enums.Module)action.Invoker,
                    IdOperator = action.IdOperator
                };
                aiutAction.IdAction = action.IdAction;
                MSMQ.FromPath(queuePath).Send(aiutAction);
            }
        }
        #endregion

        #region private SerializeActionData
        private static string SerializeActionData(DataValue[] actionData)
        {
            if (actionData == null || actionData.Length == 0) return null;

            string xmlActionData = null;
            using (StringWriter stringWriter = new StringWriter())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = true;
                settings.Indent = false;
                using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, settings))
                {
                    xmlWriter.WriteStartElement("ROOT");
                    xmlWriter.WriteAttributeString("xmlns", "xsd", null, @"http://www.w3.org/2001/XMLSchema");
                    xmlWriter.WriteAttributeString("xmlns", "xsi", null, @"http://www.w3.org/2001/XMLSchema-instance");
                    foreach (Common.DataValue item in actionData)
                    {
                        string xmlType = GetTypeAttribute(item.Value);
                        if (xmlType != null)
                        {
                            xmlWriter.WriteStartElement("ACTION_DATA");
                            xmlWriter.WriteStartElement("ID_DATA_TYPE");
                            xmlWriter.WriteValue(item.IdDataType);
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("INDEX_NBR");
                            xmlWriter.WriteValue(item.Index);
                            xmlWriter.WriteEndElement();
                            if (item.Value != null)
                            {
                                xmlWriter.WriteStartElement("VALUE");
                                xmlWriter.WriteAttributeString("xsi", "type", null, xmlType);
                                xmlWriter.WriteValue(item.Value);
                                xmlWriter.WriteFullEndElement();
                            }
                            xmlWriter.WriteEndElement();
                        }
                    }
                    xmlWriter.WriteEndElement();
                }
                xmlActionData = stringWriter.ToString();
            }
            return xmlActionData;
        }
        #endregion
        #region private GetTypeAttribute
        private static string GetTypeAttribute(object item)
        {
            if (item == null) return "";
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = false;
            settings.OmitXmlDeclaration = true;
            List<object> list = new List<object>() { item };
            using (StringWriter textWriter = new StringWriter())
            using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(list.GetType(), new XmlRootAttribute("ROOT"));
                    serializer.Serialize(xmlWriter, list, new XmlSerializerNamespaces(new XmlQualifiedName[] { 
                    new XmlQualifiedName("xsd", @"http://www.w3.org/2001/XMLSchema"),
                    new XmlQualifiedName("xsi", @"http://www.w3.org/2001/XMLSchema-instance")
                }));
                    string stringText = textWriter.ToString();
                    int indexOfType = stringText.IndexOf("xsi:type");
                    if (indexOfType != -1)
                    {
                        stringText = stringText.Remove(0, indexOfType);
                        int indexOfClose = stringText.IndexOf(">");
                        if (indexOfClose != -1)
                        {
                            stringText = stringText.Remove(indexOfClose);
                            stringText = stringText.Replace("\"", "").Replace("xsi:type=", "");
                            return stringText;
                        }
                    }
                }
                catch { }
            }
            return null;
        }
        #endregion

        #region PopulateData

        public static Tuple<bool?, List<int?>, List<OpActionData>> PopulateActionData<T>(DataProvider dataProvider, List<T> targetList, int idActionType, bool useDBCollector = false)
        {
            bool? oneDeviceDriverSelected = null;
            List<int?> idServers = new List<int?>();

            List<OpDataType> dataTypeToEdit = new List<OpDataType>();
            List<OpActionData> actionData = new List<OpActionData>();
            List<OpActionDef> actionDefForActionType = dataProvider.GetActionDefFilter(IdActionType: new int[] { idActionType }, useDBCollector: useDBCollector);
            if (targetList is List<OpDevice>)
            {
                #region Device
                List<OpDevice> devices = targetList.Cast<OpDevice>().ToList();

                List<int> idDeviceTypes = devices.Select(q => q.IdDeviceType).Distinct(q => q).ToList();
                if (idDeviceTypes != null && idDeviceTypes.Count > 1)
                    oneDeviceDriverSelected = false;
                else if (idDeviceTypes != null && idDeviceTypes.Count == 1)
                    oneDeviceDriverSelected = true;
                else
                    oneDeviceDriverSelected = null;

                if (idDeviceTypes != null && idDeviceTypes.Count > 0)
                    dataTypeToEdit = DataTypeGroupComponent.GetCommonDataTypes(dataProvider, Enums.DataTypeGroup.SIMA_DEVICE_PARAMS_TO_EDIT, (int)Enums.ReferenceType.IdDeviceType, idDeviceTypes.Select(q => Convert.ToInt64(q)).ToArray());
                else
                    dataTypeToEdit = new List<OpDataType>();

                if (devices.Count == 1)
                {
                    int? idServer = dataProvider.GetObjectIdServer(devices[0].SerialNbr, Enums.ReferenceType.SerialNbr, useDBCollector);
                    idServers.Add(idServer);
                    OpActionDef actionDef;
                    if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.SerialNbr == devices[0].SerialNbr && d.ID_SERVER == idServer))
                        actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.SerialNbr == devices[0].SerialNbr && d.ID_SERVER == idServer);
                    else
                        actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType
                            && !d.SerialNbr.HasValue && !d.IdMeter.HasValue && !d.IdLocation.HasValue && !d.IdDataType.HasValue
                            && d.ID_SERVER == idServer);
                    if (actionDef != null)
                    {
                        List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                        actionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                    }
                }
                else if (devices.Count > 1)
                {                    
                    foreach (OpDevice device in devices)
                    {
                        idServers.Add(dataProvider.GetObjectIdServer(device.SerialNbr, Enums.ReferenceType.SerialNbr, useDBCollector));
                    }
                    idServers = idServers.Distinct().ToList();
                    if (idServers.Count == 1)
                    {
                        int? idServer = idServers[0];
                        OpActionDef actionDef;
                        OpActionDef actionDefDefault = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType
                            && !d.SerialNbr.HasValue && !d.IdMeter.HasValue && !d.IdLocation.HasValue && !d.IdDataType.HasValue
                            && d.ID_SERVER == idServer);
                        if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.SerialNbr == devices[0].SerialNbr && d.ID_SERVER == idServer))
                            actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.SerialNbr == devices[0].SerialNbr && d.ID_SERVER == idServer);
                        else
                            actionDef = actionDefDefault;

                        if (actionDef != null)
                        {
                            List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                            actionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                        }
                        for (int i = 1; i < devices.Count; i++)
                        {
                            if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.SerialNbr == devices[i].SerialNbr && d.ID_SERVER == idServer))
                                actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.SerialNbr == devices[i].SerialNbr && d.ID_SERVER == idServer);
                            else
                                actionDef = actionDefDefault;

                            if (actionDef != null)
                            {
                                List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                                tmpActionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                                actionData = actionData.Join(tmpActionData, w => w, q => q, (w, q) => w).ToList();
                            }
                        }
                    }                    
                }
                if (actionData == null)
                    actionData = new List<OpActionData>();

                return new Tuple<bool?, List<int?>, List<OpActionData>>(oneDeviceDriverSelected, idServers, actionData);
                #endregion
            }
            else if (targetList is List<OpMeter>)
            {
                #region Meter
                List<OpMeter> meters = targetList.Cast<OpMeter>().ToList();

                List<int> idMeterTypes = meters.Select(q => q.IdMeterType).Distinct(q => q).ToList();

                if (idMeterTypes != null && idMeterTypes.Count > 0)
                    dataTypeToEdit = DataTypeGroupComponent.GetCommonDataTypes(dataProvider, Enums.DataTypeGroup.SIMA_METER_PARAMS_TO_EDIT, (int)Enums.ReferenceType.IdMeterType, idMeterTypes.Select(q => Convert.ToInt64(q)).ToArray());
                else
                    dataTypeToEdit = new List<OpDataType>();

                if (meters.Count == 1)
                {
                    int? idServer = dataProvider.GetObjectIdServer(meters[0].IdMeter, Enums.ReferenceType.IdMeter, useDBCollector);
                    idServers.Add(idServer);
                    OpActionDef actionDef;
                    if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.IdMeter == meters[0].IdMeter && d.ID_SERVER == idServer))
                        actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.IdMeter == meters[0].IdMeter && d.ID_SERVER == idServer);
                    else
                        actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType
                            && !d.SerialNbr.HasValue && !d.IdMeter.HasValue && !d.IdLocation.HasValue && !d.IdDataType.HasValue
                            && d.ID_SERVER == idServer);

                    if (actionDef != null)
                    {
                        List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                        actionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                    }
                }
                else if (meters.Count > 1)
                {
                    foreach (OpMeter meter in meters)
                    {
                        idServers.Add(dataProvider.GetObjectIdServer(meter.IdMeter, Enums.ReferenceType.IdMeter, useDBCollector));
                    }
                    idServers = idServers.Distinct().ToList();
                    if (idServers.Count == 1)
                    {
                        int? idServer = idServers[0];
                        OpActionDef actionDef;
                        OpActionDef actionDefDefault = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType
                            && !d.SerialNbr.HasValue && !d.IdMeter.HasValue && !d.IdLocation.HasValue && !d.IdDataType.HasValue
                            && d.ID_SERVER == idServer);
                        if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.IdMeter == meters[0].IdMeter && d.ID_SERVER == idServer))
                            actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.IdMeter == meters[0].IdMeter && d.ID_SERVER == idServer);
                        else
                            actionDef = actionDefDefault;

                        if (actionDef != null)
                        {
                            List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                            actionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                        }
                        for (int i = 1; i < meters.Count; i++)
                        {
                            if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.IdMeter == meters[i].IdMeter && d.ID_SERVER == idServer))
                                actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.IdMeter == meters[i].IdMeter && d.ID_SERVER == idServer);
                            else
                                actionDef = actionDefDefault;

                            if (actionDef != null)
                            {
                                List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                                tmpActionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                                actionData = actionData.Join(tmpActionData, w => w, q => q, (w, q) => w).ToList();
                            }
                        }
                    }
                    
                }
                if (actionData == null)
                    actionData = new List<OpActionData>();

                return new Tuple<bool?, List<int?>, List<OpActionData>>(oneDeviceDriverSelected, idServers, actionData);
                #endregion
            }
            else if (targetList is List<OpLocation>)
            {
                #region Location
                List<OpLocation> locations = targetList.Cast<OpLocation>().ToList();

                List<int> idLocationTypes = locations.Select(q => q.IdLocationType).Distinct(q => q).ToList();

                if (idLocationTypes != null && idLocationTypes.Count > 0)
                    dataTypeToEdit = DataTypeGroupComponent.GetCommonDataTypes(dataProvider, Enums.DataTypeGroup.SIMA_LOCATION_PARAMS_TO_EDIT, (int)Enums.ReferenceType.IdLocationType, idLocationTypes.Select(q => Convert.ToInt64(q)).ToArray());
                else
                    dataTypeToEdit = new List<OpDataType>();

                if (locations.Count == 1)
                {
                    int? idServer = dataProvider.GetObjectIdServer(locations[0].IdLocation, Enums.ReferenceType.IdLocation, useDBCollector);
                    idServers.Add(idServer);
                    OpActionDef actionDef;
                    if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.IdLocation == locations[0].IdLocation && d.ID_SERVER == idServer))
                        actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.IdLocation == locations[0].IdLocation && d.ID_SERVER == idServer);
                    else
                        actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType
                            && !d.SerialNbr.HasValue && !d.IdMeter.HasValue && !d.IdLocation.HasValue && !d.IdDataType.HasValue
                            && d.ID_SERVER == idServer);

                    if (actionDef != null)
                    {
                        List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                        actionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                    }
                }
                else if (locations.Count > 1)
                {
                    foreach (OpLocation location in locations)
                    {
                        idServers.Add(dataProvider.GetObjectIdServer(location.IdLocation, Enums.ReferenceType.IdLocation, useDBCollector));
                    }
                    idServers = idServers.Distinct().ToList();
                    if (idServers.Count == 1)
                    {
                        int? idServer = idServers[0];
                        OpActionDef actionDef;
                        OpActionDef actionDefDefault = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType
                            && !d.SerialNbr.HasValue && !d.IdMeter.HasValue && !d.IdLocation.HasValue && !d.IdDataType.HasValue
                            && d.ID_SERVER == idServer);
                        if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.IdLocation == locations[0].IdLocation && d.ID_SERVER == idServer))
                            actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.IdLocation == locations[0].IdLocation && d.ID_SERVER == idServer);
                        else
                            actionDef = actionDefDefault;

                        if (actionDef != null)
                        {
                            List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                            actionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                        }
                        for (int i = 1; i < locations.Count; i++)
                        {
                            if (actionDefForActionType.Any(d => d.IdActionType == idActionType && d.IdLocation == locations[i].IdLocation && d.ID_SERVER == idServer))
                                actionDef = actionDefForActionType.FirstOrDefault(d => d.IdActionType == idActionType && d.IdLocation == locations[i].IdLocation && d.ID_SERVER == idServer);
                            else
                                actionDef = actionDefDefault;

                            if (actionDef != null)
                            {
                                List<OpActionData> tmpActionData = dataProvider.GetActionDataFilter(IdActionData: new long[] { actionDef.IdActionData }, useDBCollector: useDBCollector);
                                tmpActionData = tmpActionData.Where(q => q.ID_SERVER == idServer).ToList();
                                actionData = actionData.Join(tmpActionData, w => w, q => q, (w, q) => w).ToList();
                            }
                        }
                    }                    
                }
                if (actionData == null)
                    actionData = new List<OpActionData>();

                return new Tuple<bool?, List<int?>, List<OpActionData>>(oneDeviceDriverSelected, idServers, actionData);

                #endregion
            }
            return null;
        }

        #endregion

        #region Set

        #region SetHelperValues

        public static void SetHelperValues(DataProvider dataProvider, OpAction action, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdAction, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_ACTION:
                        action.DataList.SetValue(DataType.HELPER_ID_ACTION, 0, action.IdAction);
                        break;
                    case DataType.HELPER_SERIAL_NBR:
                        action.DataList.SetValue(DataType.HELPER_SERIAL_NBR, 0, action.SerialNbr);
                        break;
                    case DataType.HELPER_ID_METER:
                        action.DataList.SetValue(DataType.HELPER_ID_METER, 0, action.IdMeter);
                        break;
                    case DataType.HELPER_ID_LOCATION:
                        action.DataList.SetValue(DataType.HELPER_ID_LOCATION, 0, action.IdLocation);
                        break;
                    case DataType.HELPER_ID_ACTION_TYPE:
                        action.DataList.SetValue(DataType.HELPER_ID_ACTION_TYPE, 0, action.IdActionType);
                        break;
                    case DataType.HELPER_ID_ACTION_STATUS:
                        action.DataList.SetValue(DataType.HELPER_ID_ACTION_STATUS, 0, action.IdActionStatus);
                        break;
                    case DataType.HELPER_ID_ACTION_DATA:
                        action.DataList.SetValue(DataType.HELPER_ID_ACTION_DATA, 0, action.IdActionData);
                        break;
                    case DataType.HELPER_ID_ACTION_PARENT:
                        action.DataList.SetValue(DataType.HELPER_ID_ACTION_PARENT, 0, action.IdActionParent);
                        break;
                    case DataType.HELPER_ID_DATA_ARCH:
                        action.DataList.SetValue(DataType.HELPER_ID_DATA_ARCH, 0, action.IdDataArch);
                        break;
                    case DataType.HELPER_ID_MODULE:
                        action.DataList.SetValue(DataType.HELPER_ID_MODULE, 0, action.IdModule);
                        break;
                    case DataType.HELPER_ID_OPERATOR:
                        action.DataList.SetValue(DataType.HELPER_ID_OPERATOR, 0, action.IdOperator);
                        break;
                    case DataType.HELPER_CREATION_DATE:
                        action.DataList.SetValue(DataType.HELPER_CREATION_DATE, 0, action.CreationDate);
                        break;
                    default:
                        break;
                }
            }
            action.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #endregion
    }
}
