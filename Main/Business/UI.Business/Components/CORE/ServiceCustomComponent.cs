﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ServiceCustomComponent : BaseComponent
    {
        #region Delete

        public static void Delete(DataProvider dataProvider, OpServiceCustom objectToDelete)
        {
            if (objectToDelete != null && objectToDelete.IdServiceCustom > 0)
            {
                dataProvider.DeleteServiceCustom(objectToDelete);
            }
        }

        #endregion
    }
}
