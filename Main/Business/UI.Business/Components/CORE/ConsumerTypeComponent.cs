﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ConsumerTypeComponent
    {
        #region Methods
        public static OpConsumerType GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetConsumerType(Id);
        }

        public static List<OpConsumerType> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllConsumerType();
        }

        public static OpConsumerType Save(DataProvider dataProvider, OpConsumerType objectToSave)
        {
            bool isNewItem = objectToSave.IdConsumerType == 0;

            dataProvider.SaveConsumerType(objectToSave);

            OpConsumerTypeData data = null;
            for (int i = 0; i < objectToSave.DataList.Count; i++)
            {
                data = objectToSave.DataList[i];
                data.IdConsumerType = objectToSave.IdConsumerType;
                //zapis do bazy tylko tych ktore sa IsEditable na true lub jesli obiekt ktory dodajemy jest nowym obiektem wiec dodajemy dane po raz pierwszy
                if (isNewItem || dataProvider.GetDataType(data.IdDataType).IsEditable)
                {
                    objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                }
            }

            return dataProvider.GetConsumerType(objectToSave.IdConsumerType);
        }

        public static void Delete(DataProvider dataProvider, OpConsumerType objectToDelete)
        {
            dataProvider.DeleteConsumerType(objectToDelete);
        }
        #endregion
    }
}
