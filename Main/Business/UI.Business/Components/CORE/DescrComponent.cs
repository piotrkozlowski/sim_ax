﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DescrComponent
    {
        #region Methods

        #region GetByID
        public static OpDescr GetByID(DataProvider dataProvider, int Id)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetDescr(Id);
        }
        #endregion
        #region GetAll
        public static List<OpDescr> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllDescr();
        }
        #endregion
        #region GetResource
        private static System.Resources.ResourceManager ResourceManager = null;
        public static string GetResource(string key)
        {
            string resourceString = String.Empty;
            if (ResourceManager == null)
                ResourceManager = new System.Resources.ResourceManager(typeof(ResourcesText));
            resourceString = ResourceManager.GetString(key, BaseComponent.CultureInfo);
            if (String.IsNullOrEmpty(resourceString))
                resourceString = key;
            return resourceString;
        }
        #endregion
        #endregion
    }
}
