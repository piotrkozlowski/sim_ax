﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ActorGroupComponent
    {
        #region Get

        #region GetByID

        public static OpActorGroup GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }
        
        #endregion
        #region GetByID

        public static OpActorGroup GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetActorGroup(Id, queryDatabase);
        }
        
        #endregion
        #region GetAll

        public static List<OpActorGroup> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllActorGroup();
        }
        
        #endregion

        #region GetBuiltInForDistributor

        [Obsolete("Use IDataProvider overload", false)]
        public static OpActorGroup GetBuiltInForDistributor(DataProvider dataProvider, OpDistributor distributor)
        {
            return GetBuiltInForDistributor(dataProvider, distributor.IdDistributor);
        }

        public static OpActorGroup GetBuiltInForDistributor(IDataProvider dataProvider, OpDistributor distributor)
        {
            return GetBuiltInForDistributor(dataProvider, distributor.IdDistributor);
        }
        
        #endregion
        #region GetBuiltInForDistributor

        [Obsolete("Use IDataProvider overload", false)]
        public static OpActorGroup GetBuiltInForDistributor(DataProvider dataProvider, int distributor)
        {
            return dataProvider.GetActorGroupFilter(IdDistributor: new int[] { distributor }, BuiltIn: true).FirstOrDefault();
        }

        public static OpActorGroup GetBuiltInForDistributor(IDataProvider dataProvider, int distributor)
        {
            return dataProvider.GetActorGroupFilter(IdDistributor: new int[] { distributor }, BuiltIn: true).FirstOrDefault();
        }
        
        #endregion
        #region GetForDistributor

        public static List<OpActorGroup> GetForDistributor(DataProvider dataProvider, List<OpDistributor> distributor)
        {
            return dataProvider.GetActorGroupFilter(IdDistributor: distributor.Select(d => d.IdDistributor).ToArray());
        }
        
        #endregion
        
        #endregion

        #region Save

        public static OpActorGroup Save(DataProvider dataProvider, OpActorGroup objectToSave)
        {
            dataProvider.SaveActorGroup(objectToSave);

            return dataProvider.GetActorGroup(objectToSave.IdActorGroup);
        }
        
        #endregion

        #region Delete

        public static void Delete(DataProvider dataProvider, OpActorGroup objectToDelete)
        {
            dataProvider.DeleteActorGroup(objectToDelete);
        }
        
        #endregion
    }
}
