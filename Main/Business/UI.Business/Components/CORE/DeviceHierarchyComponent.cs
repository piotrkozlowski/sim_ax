﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Components.CORE
{
	public class DeviceHierarchyComponent
	{
        #region Get

        public static List<OpDeviceHierarchy> GetForParentDevice(DataProvider dataProvider, OpDevice device)
        {
            return GetForParentDevice(dataProvider, device.SerialNbr);
        }
        public static List<OpDeviceHierarchy> GetForParentDevice(DataProvider dataProvider, long serialNbr)
        {
            return dataProvider.GetDeviceHierarchyFilter
                    (
                        SerialNbrParent: new[] { serialNbr },
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).ToList();
        }
        public static List<OpDeviceHierarchy> GetForDevice(DataProvider dataProvider, OpDevice device)
        {
            return GetForDevice(dataProvider, device.SerialNbr);
        }
        public static List<OpDeviceHierarchy> GetForDevice(DataProvider dataProvider, long serialNbr)
        {
            return dataProvider.GetDeviceHierarchyFilter
                    (
                        SerialNbr: new[] { serialNbr },
                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                        mergeIntoCache: true
                    ).ToList();
        }

        public static List<OpDeviceHierarchy> GetEntriesForParentDevice(DataProvider dataProvider, OpDevice device)
        {
            return dataProvider.GetDeviceHierarchyFilter
                    (
                        SerialNbrParent: new[] { device.SerialNbr },
                        mergeIntoCache: true
                    ).ToList();
        }
        public static List<OpDeviceHierarchy> GetEntriesForDevice(DataProvider dataProvider, OpDevice device)
        {
            return dataProvider.GetDeviceHierarchyFilter
                    (
                        SerialNbr: new[] { device.SerialNbr },
                        mergeIntoCache: true
                    ).ToList();
        }

        public static List<OpDeviceHierarchy> GetHierarchySlotsForParent(DataProvider dataProvider, long serialNbrParent, bool dbCollectorEnabled = false)
        {
            // Uwaga!
            // Ta metoda ściąga hierarchie urządzeń i uzupełnia puste sloty wg. patternu.
            // Jeśli jednak użytkownikowi brakuje uprawnień do urządzeń które są podpięte,
            // to te sloty są ustawiane jako wolne!

            List<OpDeviceHierarchy> retList = new List<OpDeviceHierarchy>();            

            #region Pattern

            OpDevice device = dataProvider.GetDevice(serialNbrParent);
            if (device != null)
            {
                List<OpDeviceHierarchyPattern> deviceHierarchyPattern = null;

                int? deviceIdServer = dataProvider.GetObjectIdServer(serialNbrParent, IMR.Suite.Common.Enums.ReferenceType.SerialNbr, dbCollectorEnabled);
                deviceHierarchyPattern = dataProvider.GetDeviceHierarchyPatternFilter(loadNavigationProperties: true, autoTransaction: false,
                    transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdDeviceTypeParent: new int[] { device.IdDeviceType })
                    .Where(d => (!dbCollectorEnabled || !deviceIdServer.HasValue) ? !d.ID_SERVER.HasValue : (d.ID_SERVER == deviceIdServer.Value)).ToList();                

                foreach (OpDeviceHierarchyPattern pattern in deviceHierarchyPattern)
                {
                    for (int slotNbr = pattern.SlotNbrMin; slotNbr <= pattern.SlotNbrMax; slotNbr++)
                    {
                        int idx = retList.FindIndex(w => w.SlotNbr == slotNbr && w.IdSlotType == pattern.IdSlotType);
                        if (idx == -1)
                        {
                            OpDeviceHierarchy deviceHierarchy = new OpDeviceHierarchy() { SerialNbrParent = device.SerialNbr, SlotNbr = slotNbr, IdSlotType = pattern.IdSlotType };
                            deviceHierarchy.AssignReferences(dataProvider);
                            deviceHierarchy.OpState = OpChangeState.Undefined;
                            deviceHierarchy.ProtocolIn = pattern.ProtocolIn;
                            deviceHierarchy.ProtocolOut = pattern.ProtocolOut;

                            retList.Add(deviceHierarchy);
                        }
                        else
                        {
                            retList[idx].ProtocolIn = null;
                            retList[idx].ProtocolOut = null;
                        }
                    }
                }
            }

            #endregion

            List<OpDeviceHierarchy> childDevices = DeviceHierarchyComponent.GetForParentDevice(dataProvider, serialNbrParent);
            if (childDevices != null)
            {
                foreach (OpDeviceHierarchy dhItem in childDevices)
                {
                    #region Assigning to pattern

                    int idx = retList.FindIndex(w => w.SlotNbr == dhItem.SlotNbr && w.IdSlotType == dhItem.IdSlotType);
                    if (idx != -1)
                    {
                        if (retList[idx].StartTime == DateTime.MinValue)
                        {
                            retList[idx] = dhItem.Clone() as OpDeviceHierarchy;
                            retList[idx].AssignReferences(dataProvider);
                        }
                        else
                        {
                            OpDeviceHierarchy dh = dhItem.Clone() as OpDeviceHierarchy;
                            dh.AssignReferences(dataProvider);
                            retList.Add(dh);
                        }
                    }
                    else
                    {
                        OpDeviceHierarchy notInPatternDevice = dhItem.Clone() as OpDeviceHierarchy;
                        notInPatternDevice.AssignReferences(dataProvider);
                        notInPatternDevice.OpState = OpChangeState.Undefined;
                        retList.Add(notInPatternDevice);
                    }

                    #endregion
                }
            }

            return retList;
        }

        #endregion

        #region Install

        public static OpDeviceHierarchy InstallDevice(DataProvider dataProvider, OpDevice deviceParent, OpDevice device, int slotNbr = 0, int idSlotType = 0, int? idProtocolIn = null, int? idProtocolOut = null, bool isActive = true)
        {
            OpDeviceHierarchy newDeviceHierarchy = new OpDeviceHierarchy()
            {
                IdDeviceHierarchy = 0,
                SerialNbrParent = deviceParent.SerialNbr,
                SerialNbr = device.SerialNbr,
                IdSlotType = idSlotType,
                SlotNbr = slotNbr,
                IdProtocolIn = idProtocolIn,
                IdProtocolOut = idProtocolOut,
                IsActive = isActive,
                StartTime = dataProvider.DateTimeNow,
                EndTime = null
            };

            return Save(dataProvider, newDeviceHierarchy);
        }
        public static OpDeviceHierarchy Install(DataProvider dataProvider, OpDeviceHierarchy newHierarchy, bool uninstallFromOtherDevices = false, bool useDBCollector = false)
        {
            if (uninstallFromOtherDevices && newHierarchy.SerialNbr != null)
                GetForDevice(dataProvider, newHierarchy.SerialNbr.Value).ForEach(dh => Uninstall(dataProvider, dh));
            newHierarchy.IdDeviceHierarchy = 0;
            newHierarchy.StartTime = dataProvider.DateTimeNow;
            newHierarchy.EndTime = null;
            return Save(dataProvider, newHierarchy, useDBCollector);
        }
        
        #endregion

        #region Uninstall

        public static void UninstallDevice(DataProvider dataProvider, OpDevice deviceParent, OpDevice device, bool useDBCollector = false)
        {
            List<OpDeviceHierarchy> hierachyToUninstall = dataProvider.GetDeviceHierarchyFilter(
                                                                        SerialNbrParent: new long[] { deviceParent.SerialNbr },
                                                                        SerialNbr: new long[] { device.SerialNbr },
                                                                        EndTime: Data.DB.TypeDateTimeCode.Null(),
                                                                        mergeIntoCache: true);
            hierachyToUninstall.ForEach(w =>
            {
                CloseEntry(dataProvider, w, useDBCollector);
            });
        }
        public static void Uninstall(DataProvider dataProvider, OpDeviceHierarchy hierarchy, bool useDBCollector = false)
        {
            OpDeviceHierarchy hierarchyToUninstall = dataProvider.GetDeviceHierarchy(hierarchy.IdDeviceHierarchy);
            if (hierarchyToUninstall == null)
                return;

            CloseEntry(dataProvider, hierarchyToUninstall, useDBCollector);
        }

        #endregion

        #region CloseEntry

        internal static void CloseEntry(DataProvider dataProvider, OpDeviceHierarchy hierarchy, bool useDBCollector = false)
        {
            hierarchy.EndTime = dataProvider.DateTimeNow;
            Save(dataProvider, hierarchy, useDBCollector);
        }

        #endregion
        #region Save

        internal static OpDeviceHierarchy Save(DataProvider dataProvider, OpDeviceHierarchy objectToSave, bool useDBCollector = false)
        {
            dataProvider.SaveDeviceHierarchy(objectToSave, useDBCollector);
            objectToSave.OpState = OpChangeState.Loaded;
            return dataProvider.GetDeviceHierarchy(objectToSave.IdDeviceHierarchy);
        }
        
        #endregion
	}
}
