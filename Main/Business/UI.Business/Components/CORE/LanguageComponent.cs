﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LanguageComponent
    {
        public static string GetCultureCode(Enums.Language language)
        {
            switch (language)
            {
                case Enums.Language.Polish:
                    return "pl-PL";
                case Enums.Language.Russian:
                    return "rU-RU";
                case Enums.Language.Spanish:
                    return "es-ES";
                case Enums.Language.Serbian:
                    return "sr-Latn-RS";
                case Enums.Language.English:
                default:
                    return "en-GB";
            }
        }

    }
}
