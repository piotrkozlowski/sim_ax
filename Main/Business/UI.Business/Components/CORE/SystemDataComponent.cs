﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class SystemDataComponent
    {
        #region Methods
        public static long GetDictTablesVersion(DataProvider dataProvider)
        {
            List<OpSystemData> list = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, IdDataType: new long[] { DataType.SYSTEM_DICTIONARY_TABLES_VERSION });
            if (list != null && list.Count > 0)
            {
                object value = list[0].Value;
                if (value != null)
                    return Convert.ToInt64(value);
            }
            return 0;
        }

        public static bool IsRunningActionEnabled(DataProvider dataProvider, bool defaultResult = false)
        {
            try
            {
                List<OpSystemData> list = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                    IdDataType: new long[] { DataType.SYSTEM_RUNNING_ACTION_ENABLED });

                if (list == null || list.Count == 0)
                {
                    Common.IMRLog.AddToLog(EventID.Forms.SystemRunningActionEnabledNotSet, defaultResult);
                    return defaultResult;
                }

                OpSystemData systemData = list.FirstOrDefault(q => q.Value != null);
                if (systemData != null)
                    return GenericConverter.Parse<bool>(systemData.Value, defaultResult);
            }
            catch (Exception ex)
            {
                Common.IMRLog.AddToLog(EventID.Forms.SystemRunningActionEnabledError, ex);
            }
            Common.IMRLog.AddToLog(EventID.Forms.SystemRunningActionEnabledDefault, defaultResult);
            return defaultResult;
        }
        public static Tuple<int, int> GetIdDistributorPool(IDataProvider dataProvider)
        {
            int? startId = null;
            int? endId = null;
            List<OpSystemData> list = dataProvider.GetSystemDataFilter(loadNavigationProperties: false, IdDataType: new long[] { DataType.SYSTEM_ID_DISTRIBUTOR_POOL_BEGIN, DataType.SYSTEM_ID_DISTRIBUTOR_POOL_END });
            if (list != null && list.Count > 0)
            {
                int maxIndex = list.Max(l => l.Index);
                if (list.Exists(d => d.IdDataType == DataType.SYSTEM_ID_DISTRIBUTOR_POOL_BEGIN && d.Index == maxIndex && d.Value != null))
                    startId = Convert.ToInt32(list.Find(d => d.IdDataType == DataType.SYSTEM_ID_DISTRIBUTOR_POOL_BEGIN && d.Index == maxIndex && d.Value != null).Value);
                if (list.Exists(d => d.IdDataType == DataType.SYSTEM_ID_DISTRIBUTOR_POOL_END && d.Index == maxIndex && d.Value != null))
                    endId = Convert.ToInt32(list.Find(d => d.IdDataType == DataType.SYSTEM_ID_DISTRIBUTOR_POOL_END && d.Index == maxIndex && d.Value != null).Value);
            }
            if (!startId.HasValue || !endId.HasValue)
                throw new Exception("IdDistributor pool not defined");
            return new Tuple<int, int>(startId.Value, endId.Value);
        }
        #endregion
    }
}
