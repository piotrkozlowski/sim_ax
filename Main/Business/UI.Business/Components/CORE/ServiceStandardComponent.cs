﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ServiceStandardComponent
    {
        public static OpService Save(DataProvider dataProvider, OpService objectToSave)
        {
            if (objectToSave.IdDeviceOrderNumber == 0)
                objectToSave.IdDeviceOrderNumber = null;
            int id = dataProvider.SaveService(objectToSave);

            foreach (OpServiceData dadItem in objectToSave.DataList)
            {
                if (dadItem.OpState == Objects.OpChangeState.New || dadItem.OpState == Objects.OpChangeState.Modified)
                {
                    dadItem.IdService = id;
                    dadItem.IdServiceData = dataProvider.SaveServiceData(dadItem);
                }
                if (dadItem.OpState == Objects.OpChangeState.Delete)
                {
                    if (dadItem.IdServiceData > 0)
                        dataProvider.DeleteServiceData(dadItem);
                }
            }
            objectToSave.DataList.RemoveAll(d => d.OpState == Objects.OpChangeState.Delete);

            return dataProvider.GetService(id);
        }

        public static void DeleteByDiagnosticActionInServiceByIdService(DataProvider dataProvider, int idService)
        {
            dataProvider.DeleteDiagnosticActionInService(null, idService);
        }

        public static void Delete(DataProvider dataProvider, OpService objectToDel)
        {
            if (objectToDel.IdService > 0)
            {
                List<OpDiagnosticActionInService> actionInServices = dataProvider.GetDiagnosticActionInServiceFilter(loadNavigationProperties: false, customWhereClause: "[ID_SERVICE] = " + objectToDel.IdService);
                foreach (OpDiagnosticActionInService daisItem in actionInServices)
                {
                    dataProvider.DeleteDiagnosticActionInService(daisItem.IdDiagnosticAction, daisItem.IdService);
                }
                List<OpServiceInPackage> servicePackages = dataProvider.GetServiceInPackageFilter(loadNavigationProperties: false, customWhereClause: "[ID_SERVICE] = " + objectToDel.IdService);
                foreach (OpServiceInPackage sinItem in servicePackages)
                {
                    dataProvider.DeleteServiceInPackage(sinItem);
                }
                List<OpServiceData> serviceData = dataProvider.GetServiceDataFilter(loadNavigationProperties: false, customWhereClause: "[ID_SERVICE] = " + objectToDel.IdService);
                foreach (OpServiceData sdItem in serviceData)
                {
                    dataProvider.DeleteServiceData(sdItem);
                }
                dataProvider.DeleteService(objectToDel);
            }
        }
    }
}
