﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ActionTypeComponent
    {
        public static OpActionType GetActionTypeByName(DataProvider dataProvider, string Name)
        {
            return dataProvider.GetAllActionType().Find(a => a.Name == Name);
        }
    }
}
