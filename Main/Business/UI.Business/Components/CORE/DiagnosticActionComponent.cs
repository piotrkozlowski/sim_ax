﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DiagnosticActionComponent
    {
        public static OpDiagnosticAction Save(DataProvider dataProvider, OpDiagnosticAction objectToSave)
        {
            int id = dataProvider.SaveDiagnosticAction(objectToSave);
            foreach (OpDiagnosticActionData dadItem in objectToSave.DataList)
            {
                if (dadItem.OpState == Objects.OpChangeState.New || dadItem.OpState == Objects.OpChangeState.Modified)
                {
                    dadItem.IdDiagnosticAction = id;
                    dadItem.IdDiagnosticActionData = dataProvider.SaveDiagnosticActionData(dadItem);
                }
                if (dadItem.OpState == Objects.OpChangeState.Delete)
                {
                    if (dadItem.IdDiagnosticActionData > 0)
                        dataProvider.DeleteDiagnosticActionData(dadItem);
                }
            }
            objectToSave.DataList.RemoveAll(d => d.OpState == Objects.OpChangeState.Delete);
            return dataProvider.GetDiagnosticAction(id);
        }

        public static void Delete(DataProvider dataProvider, OpDiagnosticAction objectToDelete)
        {
            List<OpDiagnosticActionInService> daisList = dataProvider.GetDiagnosticActionInServiceFilter(IdDiagnosticAction: new int[] { objectToDelete.IdDiagnosticAction });
            foreach (OpDiagnosticActionInService daisItem in daisList)
            {
                dataProvider.DeleteDiagnosticActionInService(daisItem.IdDiagnosticAction, daisItem.IdService);
            }
            List<OpDiagnosticActionResult> darList = dataProvider.GetDiagnosticActionResultFilter(customWhereClause: "[ID_DIAGNOSTIC_ACTION] = " + objectToDelete.IdDiagnosticAction);
            foreach (OpDiagnosticActionResult darItem in darList)
            {
                dataProvider.DeleteDiagnosticActionResult(darItem);
            }
            objectToDelete.DataList.Clear();
            objectToDelete.DataList.AddRange(dataProvider.GetDiagnosticActionDataFilter(customWhereClause: "[ID_DIAGNOSTIC_ACTION] = " + objectToDelete.IdDiagnosticAction));
            foreach (OpDiagnosticActionData dadItem in objectToDelete.DataList)
                dataProvider.DeleteDiagnosticActionData(dadItem);
            dataProvider.DeleteDiagnosticAction(objectToDelete);
        }
    }
}
