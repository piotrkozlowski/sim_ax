﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DeviceDriverComponent
    {
        public static OpFile GetDriverFileObject(OpDeviceDriver driver, DataProvider dataProvider)
        {
            int IdFile = driver.DataList.TryGetValue<int>(DataType.DRIVER_FILE);
            if (IdFile == 0)
                return null;

            return dataProvider.GetFile(IdFile);
        }
    }
}
