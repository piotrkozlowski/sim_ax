﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ActorComponent : BaseComponent
    {
        #region Methods
        #region Get

        public static OpActor GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpActor GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetActor(Id, queryDatabase);
        }

        public static List<OpActor> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllActor();
        }
        
        #endregion

        #region Save

        public static OpActor Save(DataProvider dataProvider, OpActor objectToSave)
        {
            bool isNew = objectToSave.IdActor == 0;
            try
            {
                dataProvider.SaveActor(objectToSave);
                if (LogCustomEvents)
                {
                    if (isNew)
                        //IMRLog.AddToLog(Module, EventID.Forms.ActorAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdActor);
                        LogSuccess(EventID.Forms.ActorAdded, objectToSave.IdActor);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.ActorSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdActor);
                        LogSuccess(EventID.Forms.ActorSaved, objectToSave.IdActor);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNew)
                        //IMRLog.AddToLog(Module, EventID.Forms.ActorAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.ActorAdditionError, ex.Message);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.ActorSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdActor, ex.Message);
                        LogError(EventID.Forms.ActorSavingError, objectToSave.IdActor, ex.Message);
                }
                throw ex;
            }

            return dataProvider.GetActor(objectToSave.IdActor);
        }
        
        #endregion

        #region AddToGroup

        public static OpActorInGroup AddToGroup(DataProvider dataProvider, OpActor actor, OpActorGroup actorGroup)
        {
            OpActorInGroup actorINGroup = dataProvider.GetActorInGroupFilter(IdActor: new int[] { actor.IdActor }, IdActorGroup: new int[] { actorGroup.IdActorGroup }).FirstOrDefault();
            if (actorINGroup == null)
            {
                actorINGroup = new OpActorInGroup();
                actorINGroup.Actor = actor;
                actorINGroup.ActorGroup = actorGroup;
                try
                {
                    dataProvider.SaveActorInGroup(actorINGroup);
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.ActorSaved, LoggedOperator.Login, ServerCORE, actor.IdActor);
                        LogSuccess(EventID.Forms.ActorSaved, actor.IdActor);
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.ActorSavingError, LoggedOperator.Login, ServerCORE, actor.IdActor, ex.Message);
                        LogError(EventID.Forms.ActorSavingError, actor.IdActor, ex.Message);
                    throw ex;
                }
            }
            return actorINGroup;
        }
        
        #endregion

        #region GetAllAvailable

        public static List<OpActor> GetAllAvailable(DataProvider dataProvider, OpOperator loggedOperator)
        {
            List<OpDistributor> distributorList = RoleComponent.FilterByPermission<OpDistributor>(dataProvider.Distributor, Activity.ALLOWED_DISTRIBUTOR, loggedOperator);
            List<OpActorGroup> actorGroupList = ActorGroupComponent.GetForDistributor(dataProvider, distributorList);
            if (actorGroupList.Count > 0)
            {
                return dataProvider.GetActorInGroupFilter(IdActorGroup: actorGroupList.Select(d => d.IdActorGroup).ToArray()).Select(d => d.Actor).ToList();
            }
            return null;
        }
        
        #endregion

        #region IsInActorGroup

        public static bool IsInActorGroup(DataProvider dataProvider, OpActor actor, List<OpActorGroup> actorGroupList)
        {
            if (actorGroupList.Count > 0)
                return dataProvider.GetActorInGroupFilter(IdActor: new int[] { actor.IdActor }, IdActorGroup: actorGroupList.Select(d => d.IdActorGroup).ToArray()).Count > 0;
            return false;
        }
        
        #endregion

        #region IsInActorGroupForDistributor

        public static bool IsInActorGroupForDistributor(DataProvider dataProvider, OpActor actor, List<OpDistributor> distributorList)
        {
            List<OpActorGroup> actorGroupList = ActorGroupComponent.GetForDistributor(dataProvider, distributorList);
            if (actorGroupList.Count > 0)
            {
                return dataProvider.GetActorInGroupFilter(IdActor: new int[] { actor.IdActor }, IdActorGroup: actorGroupList.Select(d => d.IdActorGroup).ToArray()).Count > 0;
            }
            return false;
        }
        
        #endregion

        #region Delete

        public static void Delete(DataProvider dataProvider, OpActor objectToDelete)
        {
            foreach (OpActorInGroup actorInGroup in dataProvider.GetActorInGroupFilter(IdActor: new int[] { objectToDelete.IdActor }))
            {
                dataProvider.DeleteActorInGroup(actorInGroup);
            }
            try
            {
                dataProvider.DeleteActor(objectToDelete);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.ActorDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdActor);
                    LogSuccess(EventID.Forms.ActorDeleted, objectToDelete.IdActor);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.ActorDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdActor, ex.Message);
                    LogError(EventID.Forms.ActorDeletionError, objectToDelete.IdActor, ex.Message);
                throw ex;
            }
        }
        
        #endregion

        #region SetHelperValues
        public static void SetHelperValues(DataProvider dataProvider, OpActor actor, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdActor, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_ACTOR:
                        actor.DataList.SetValue(DataType.HELPER_ID_ACTOR, 0, actor.IdActor);
                        break;
                    case DataType.HELPER_NAME:
                        actor.DataList.SetValue(DataType.HELPER_NAME, 0, actor.Name);
                        break;
                    case DataType.HELPER_SURNAME:
                        actor.DataList.SetValue(DataType.HELPER_SURNAME, 0, actor.Surname);
                        break;
                    case DataType.HELPER_CITY:
                        actor.DataList.SetValue(DataType.HELPER_CITY, 0, actor.City);
                        break;
                    case DataType.HELPER_ADDRESS:
                        actor.DataList.SetValue(DataType.HELPER_ADDRESS, 0, actor.Address);
                        break;
                    case DataType.HELPER_POSTCODE:
                        actor.DataList.SetValue(DataType.HELPER_POSTCODE, 0, actor.Postcode);
                        break;
                    case DataType.HELPER_EMAIL:
                        actor.DataList.SetValue(DataType.HELPER_EMAIL, 0, actor.Email);
                        break;
                    case DataType.HELPER_MOBILE:
                        actor.DataList.SetValue(DataType.HELPER_MOBILE, 0, actor.Mobile);
                        break;
                    case DataType.HELPER_PHONE:
                        actor.DataList.SetValue(DataType.HELPER_PHONE, 0, actor.Phone);
                        break;
                    case DataType.HELPER_ID_LANGUAGE:
                        actor.DataList.SetValue(DataType.HELPER_ID_LANGUAGE, 0, actor.IdLanguage);
                        break;
                    case DataType.HELPER_DESCRIPTION:
                        actor.DataList.SetValue(DataType.HELPER_DESCRIPTION, 0, actor.Description);
                        break;
                    case DataType.HELPER_AVATAR:
                        actor.DataList.SetValue(DataType.HELPER_AVATAR, 0, actor.Avatar);
                        break;
                    default:
                        break;
                }
            }
            actor.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }
        #endregion

        #region AssignDefaultGroup

        public static OpActorInGroup AssignDefaultGroup(IDataProvider dataProvider, OpOperator loggedOperator, OpActor actor, OpDistributor distributor)
        {
            if (distributor == null)
            {
                if (loggedOperator.HasPermission(Activity.ALLOWED_DISTRIBUTOR, dataProvider.SystemOwner.IdDistributor))
                    distributor = dataProvider.SystemOwner;
                else
                    distributor = loggedOperator.Distributor;
            }

            OpActorInGroup defaultGroup = new OpActorInGroup();
            defaultGroup.Actor = actor;
            defaultGroup.ActorGroup = ActorGroupComponent.GetBuiltInForDistributor(dataProvider, distributor);
            
            if (defaultGroup.ActorGroup == null)
            {
                defaultGroup.ActorGroup = new OpActorGroup();
                defaultGroup.ActorGroup.Name = ResourcesText.Group + " " + distributor.ToString();
                defaultGroup.ActorGroup.BuiltIn = true;
                defaultGroup.ActorGroup.Distributor = distributor;
                defaultGroup.ActorGroup.IdActorGroup = dataProvider.SaveActorGroup(defaultGroup.ActorGroup);
            }
            
            return defaultGroup;
        }

        #endregion

        #endregion
    }
}
