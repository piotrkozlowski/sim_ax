﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class FaultCodeComponent
    {
        public static int SaveFaultCode(DataProvider dataProvider, OpFaultCode toBeSaved)
        {
            toBeSaved.IdFaultCode = dataProvider.SaveFaultCode(toBeSaved);

            SaveFaultCodeData(dataProvider, toBeSaved);

            return toBeSaved.IdFaultCode;
        }

        public static void DeleteFaultCode(DataProvider dataProvider, OpFaultCode toBeDelete)
        {
            foreach (OpFaultCodeData dataToDelete in dataProvider.GetFaultCodeDataFilter(IdFaultCode: new int[] { toBeDelete.IdFaultCode }))
                dataProvider.DeleteFaultCodeData(dataToDelete);
            foreach (OpFaultCodeSummary summaryToDelete in dataProvider.GetFaultCodeSummaryFilter(IdFaultCode: new int[] { toBeDelete.IdFaultCode }))
                dataProvider.DeleteFaultCodeSummary(summaryToDelete);
            List<long> descrIds = new List<long>();
            if (toBeDelete.ID_CLIENT_DESCR.HasValue) descrIds.Add(toBeDelete.ID_CLIENT_DESCR.Value);
            if (toBeDelete.ID_COMPANY_DESCR.HasValue) descrIds.Add(toBeDelete.ID_COMPANY_DESCR.Value);
            if (descrIds.Count > 0)
            {
                foreach (OpDescr descrToDelete in dataProvider.GetDescrFilter(IdDescr: descrIds.ToArray()))
                    dataProvider.DeleteDescr(descrToDelete);
            }
            dataProvider.DeleteFaultCode(toBeDelete);
        }

        public static void SaveFaultCodeData(DataProvider dataProvider, OpFaultCode faultCode)
        {
            for (int i = 0; i < faultCode.DataList.Count; i++)
            {
                if (faultCode.DataList[i].OpState == OpChangeState.New || faultCode.DataList[i].OpState == OpChangeState.Modified)
                {
                    faultCode.DataList[i].IdFaultCode = faultCode.IdFaultCode;
                    faultCode.DataList[i].IdData = dataProvider.SaveFaultCodeData(faultCode.DataList[i]);
                    faultCode.DataList[i].OpState = OpChangeState.Loaded;
                }
                else if (faultCode.DataList[i].OpState == OpChangeState.Delete)
                {
                    if (faultCode.DataList[i].IdData > 0)
                        dataProvider.DeleteFaultCodeData(faultCode.DataList[i]);
                    faultCode.DataList.RemoveAt(i, true);
                    i--;
                }
            }

        }

        public static int SaveFaultCodeSummary(DataProvider dataProvider, OpFaultCodeSummary toBeSaved)
        {
            try
            {
                if (toBeSaved.OpState == Objects.OpChangeState.New || toBeSaved.OpState == Objects.OpChangeState.Modified)
                {
                    switch (toBeSaved.IdServiceReferenceType)
                    {
                        case (int)Enums.ServiceReferenceType.IdShippingList:
                        case (int)Enums.ServiceReferenceType.IdDeviceOrderNumber:
                            toBeSaved.ReferenceValue = Convert.ToInt32(toBeSaved.ReferenceValue);
                            break;
                        case (int)Enums.ServiceReferenceType.BatchNumber:
                            toBeSaved.ReferenceValue = toBeSaved.ReferenceValue.ToString();
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Incorrect ReferenceValue type", ex);
            }
            if (toBeSaved.OpState == Objects.OpChangeState.New || toBeSaved.OpState == Objects.OpChangeState.Modified)
                toBeSaved.IdFaultCodeSummary = dataProvider.SaveFaultCodeSummary(toBeSaved);

            return toBeSaved.IdFaultCodeSummary;
        }

        public static void DeleteFaultCodeSummary(DataProvider dataProvider, OpFaultCodeSummary toDelete)
        {
            if(toDelete.IdFaultCodeSummary > 0)
                dataProvider.DeleteFaultCodeSummary(toDelete);  
        }

        public static bool IsFaultCodeSummaryReferenceValueEquals(Enums.ServiceReferenceType referenceType, object value1, object value2)
        {
            switch (referenceType)
            {
                case Enums.ServiceReferenceType.IdDeviceOrderNumber:
                case Enums.ServiceReferenceType.IdShippingList:
                    return Convert.ToInt32(value1) == Convert.ToInt32(value2);
                default:
                    return String.Equals(value1.ToString(), value2.ToString());
            }
        }
    }
}
