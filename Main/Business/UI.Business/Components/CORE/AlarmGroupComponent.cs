﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class AlarmGroupComponent
    {
        #region Methods
        public static OpAlarmGroup GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpAlarmGroup GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetAlarmGroup(Id, queryDatabase);
        }

        public static List<OpAlarmGroup> GetAll(DataProvider dataProvider)
        {
            return dataProvider.GetAllAlarmGroup();
        }

        public static OpAlarmGroup Save(DataProvider dataProvider, OpAlarmGroup objectToSave, List<int> idOperators)
        {            
            //zapis do tabeli ALARM_GROUP           
            int idAlarmGroup = dataProvider.SaveAlarmGroup(objectToSave);

            List<OpAlarmGroupOperator> allAlarmGroupsOperator = dataProvider.GetAlarmGroupOperatorFilter(IdAlarmGroup: new int[] { objectToSave.IdAlarmGroup }).ToList();

            //zapis do tabeli ALARM_GROUP_OPERATOR
            if (idOperators != null)
            {
                List<OpAlarmGroupOperator> opOperatorsToRemove = allAlarmGroupsOperator.Where(w => w.IdOperator != null && !idOperators.Contains((int)w.IdOperator)).ToList();
                List<OpAlarmGroupOperator> opOperatorsToAdd = idOperators.Where(w => allAlarmGroupsOperator.Select(s => s.ID_OPERATOR).ToList().Contains(w)==false).
                                                Select(s => new OpAlarmGroupOperator()
                                                {
                                                    IdAlarmGroup = idAlarmGroup,
                                                    IdOperator = s
                                                }).ToList();

                foreach (OpAlarmGroupOperator opOperator in opOperatorsToRemove)
                {
                    dataProvider.DeleteAlarmGroupOperator(opOperator);
                }
                foreach (OpAlarmGroupOperator opOperator in opOperatorsToAdd)
                {
                    dataProvider.SaveAlarmGroupOperator(opOperator);
                }
            }

            return dataProvider.GetAlarmGroup(idAlarmGroup);
        }

        /// <summary>
        /// Funkcja usuwa grupe alarmową z tabeli ALARM_GROUP
        /// Przed usunieciem grupy sprawdza i usuwa 
        ///       - powiązania z alarmami ( ALARM_DEF_GROUP)
        ///       - powiazania z operatorem ( OPERATOR )
        /// </summary>
        /// <param name="dataProvider"> </param>
        /// <param name="objectToDelete"></param>
        public static void Delete(DataProvider dataProvider, OpAlarmGroup objectToDelete)
        {
            List<OpAlarmGroupOperator> opAlarmGroupOperators = dataProvider.GetAlarmGroupOperatorFilter(IdAlarmGroup: new int[] { objectToDelete.IdAlarmGroup });
            List<OpAlarmDefGroup> opAlarmDefGroups = dataProvider.GetAlarmDefGroupFilter(IdAlarmGroup: new int[] { objectToDelete.IdAlarmGroup });

            foreach (OpAlarmGroupOperator opAlarmGroupOperator in opAlarmGroupOperators)
            {
                dataProvider.DeleteAlarmGroupOperator(opAlarmGroupOperator);
            }

            foreach (OpAlarmDefGroup opAlarmDefGroup in opAlarmDefGroups)
            {
                dataProvider.DeleteAlarmDefGroup(opAlarmDefGroup);
            }

            dataProvider.DeleteAlarmGroup(objectToDelete);
        }
        
        public static void LoadAllData(DataProvider dataProvider, OpMeter meter)
        {
            meter.DataList.Clear();
            meter.DataList.AddRange(dataProvider.GetDataFilter(IdMeter: new long[] { meter.IdMeter }, IdDataType: new long[0]));
        }

        #endregion
    }
}
