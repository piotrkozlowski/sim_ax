﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.Common;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class ServiceListDeviceComponent : BaseComponent
    {
        #region ComputeServiceOverdue
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataprovider"></param>
        /// <param name="device"></param>
        /// <param name="amountDevicesInServiceGroup"></param>
        /// <returns>Less than 0 number means than service is after deadline</returns>
        public static int ComputeServiceOverdue(DataProvider dataProvider, OpServiceListDevice device, int? amountDevicesInServiceGroup = null)
        {
            int overdue = 0;

            int idDistributor = -1;
            if(device.Device != null)
                idDistributor = device.Device.IdDistributor;
            else if(device.DeviceDetails != null)
                idDistributor = device.DeviceDetails.IdDistributor;

            if(idDistributor != -1)
            {
                List<OpDistributorData> distributorData = dataProvider.GetDistributorDataFilter(IdDistributor: new int[] { idDistributor },
                                                                                                IdDataType: new long[]{ DataType.DISTRIBUTOR_AIUT_SERVICE_DEVICES_PER_WEEK,
                                                                                                                        DataType.DISTRIBUTOR_AIUT_SERVICE_LEGALIZATION_TIME,
                                                                                                                        DataType.DISTRIBUTOR_AIUT_SERVICE_STANDARD_PERIOD });
                if (distributorData != null && distributorData.Count > 0)
                {
                    int totalDaysToService = 0;
                    if (distributorData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_AIUT_SERVICE_STANDARD_PERIOD))
                        totalDaysToService += Convert.ToInt32(distributorData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_AIUT_SERVICE_STANDARD_PERIOD).Value);
                    if (distributorData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_AIUT_SERVICE_LEGALIZATION_TIME))
                        totalDaysToService += Convert.ToInt32(distributorData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_AIUT_SERVICE_LEGALIZATION_TIME).Value);
                    if (distributorData.Exists(d => d.IdDataType == DataType.DISTRIBUTOR_AIUT_SERVICE_DEVICES_PER_WEEK))
                    {
                        int devicesPerWeek = Convert.ToInt32(distributorData.Find(d => d.IdDataType == DataType.DISTRIBUTOR_AIUT_SERVICE_DEVICES_PER_WEEK).Value);
                        int devicesInServiceGroup = amountDevicesInServiceGroup.HasValue ? amountDevicesInServiceGroup.Value : dataProvider.GetServiceListDeviceFilterCustom(loadNavigationProperties: false, IdServiceList: new int[] { device.IdServiceList }).Count;
                        int additonalWeeks = (int)Math.Ceiling(((double)devicesInServiceGroup / (double)devicesPerWeek));
                        totalDaysToService += additonalWeeks * 7;
                    }
                    int currentServiceDays = 0;
                    if(device.EndDate.HasValue)
                        currentServiceDays = (int)(device.EndDate.Value - device.StartDate).TotalDays;
                    else
                        currentServiceDays = (int)(dataProvider.DateTimeNow - device.StartDate).TotalDays;
                    overdue = totalDaysToService - currentServiceDays;
                }
            }
            return overdue;
        }

        #endregion

        #region ComputeServiceKpi

        public static bool ComputeServiceKpi(DataProvider dataprovider, OpServiceListDevice device, out int overdue, int? amountDevicesInServiceGroup = null)
        {
            overdue = ComputeServiceOverdue(dataprovider, device, amountDevicesInServiceGroup);

            return overdue >= 0;
        }

        #endregion

        #region ForwardDevice

        public static void ForwardDevice(DataProvider dataProvider, OpServiceListDevice device, int forwardingOperatorId, int recipientOperatorId)
        {
            int idServiceList = device.IdServiceList;
            long serialNbr = device.SerialNbr;
            DateTime forwardDate = DateTime.Now;


            var foiDataNew = new OpServiceListDeviceData(null, idServiceList, serialNbr, DataType.SERVICE_LIST_DEVICE_FORWARDING_OPERATOR_ID, 0, forwardingOperatorId);
            var foiDataOld = device.DataList.Find(x => x.IdDataType == DataType.SERVICE_LIST_DEVICE_FORWARDING_OPERATOR_ID && x.IndexNbr == 0);
            if (foiDataOld != null)
            {
                foiDataOld.IndexNbr = device.DataList.GetFirstFreeIndex(DataType.SERVICE_LIST_DEVICE_FORWARDING_OPERATOR_ID);
                dataProvider.SaveServiceListDeviceData(foiDataOld);
            }
            dataProvider.SaveServiceListDeviceData(foiDataNew);
            device.DataList.Add(foiDataNew);


            var fdDataNew = new OpServiceListDeviceData(null, idServiceList, serialNbr, DataType.SERVICE_LIST_DEVICE_FORWARD_DATE, 0, forwardDate);
            var fdDataOld = device.DataList.Find(x => x.IdDataType == DataType.SERVICE_LIST_DEVICE_FORWARD_DATE && x.IndexNbr == 0);
            if (fdDataOld != null)
            {
                fdDataOld.IndexNbr = device.DataList.GetFirstFreeIndex(DataType.SERVICE_LIST_DEVICE_FORWARD_DATE);
                dataProvider.SaveServiceListDeviceData(fdDataOld);
            }
            dataProvider.SaveServiceListDeviceData(fdDataNew);
            device.DataList.Add(fdDataNew);


            var roiDataNew = new OpServiceListDeviceData(null, idServiceList, serialNbr, DataType.SERVICE_LIST_DEVICE_RECIPIENT_OPERATOR_ID, 0, recipientOperatorId);
            var roiDataOld = device.DataList.Find(x => x.IdDataType == DataType.SERVICE_LIST_DEVICE_RECIPIENT_OPERATOR_ID && x.IndexNbr == 0);
            if (roiDataOld != null)
            {
                roiDataOld.IndexNbr = device.DataList.GetFirstFreeIndex(DataType.SERVICE_LIST_DEVICE_RECIPIENT_OPERATOR_ID);
                dataProvider.SaveServiceListDeviceData(roiDataOld);
            }
            dataProvider.SaveServiceListDeviceData(roiDataNew);
            device.DataList.Add(roiDataNew);


            // przesunięcie starych danych odbioru jeśli istnieją
            var sroData = device.DataList.Find(x => x.IdDataType == DataType.SERVICE_LIST_DEVICE_SERVICE_RECIPIENT_OPERATOR_ID && x.IndexNbr == 0);
            if (sroData != null)
            {
                sroData.IndexNbr = device.DataList.GetFirstFreeIndex(DataType.SERVICE_LIST_DEVICE_SERVICE_RECIPIENT_OPERATOR_ID);
                dataProvider.SaveServiceListDeviceData(sroData);
            }

            var drdData = device.DataList.Find(x => x.IdDataType == DataType.SERVICE_LIST_DEVICE_RECEPTION_DATE && x.IndexNbr == 0);
            if (drdData != null)
            {
                drdData.IndexNbr = device.DataList.GetFirstFreeIndex(DataType.SERVICE_LIST_DEVICE_RECEPTION_DATE);
                dataProvider.SaveServiceListDeviceData(drdData);
            }
        }

        #endregion

        #region ReceiveDevice
        
        public static void ReceiveDevice(DataProvider dataProvider, OpServiceListDevice device, int receivingOperatorId)
        {
            int idServiceList = device.IdServiceList;
            long serialNbr = device.SerialNbr;
            DateTime receptionDate = DateTime.Now;

            var sroiDataNew = new OpServiceListDeviceData(null, idServiceList, serialNbr, DataType.SERVICE_LIST_DEVICE_SERVICE_RECIPIENT_OPERATOR_ID, 0, receivingOperatorId);
            dataProvider.SaveServiceListDeviceData(sroiDataNew);
            device.DataList.Add(sroiDataNew);

            var rdDataNew = new OpServiceListDeviceData(null, idServiceList, serialNbr, DataType.SERVICE_LIST_DEVICE_RECEPTION_DATE, 0, receptionDate);
            dataProvider.SaveServiceListDeviceData(rdDataNew);
            device.DataList.Add(rdDataNew);
        }

        #endregion
    }
}
