﻿using System;
using System.Linq;
using System.Collections.Generic;

using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class WorkHistoryComponent
    {
        #region SaveWorkHistory
        public static void SaveWorkHistory(IDataProvider dataProvider, OpWorkHistory[] toSave, bool saveHistoryData = true)
        {
            List<Tuple<OpWorkHistory, List<OpWorkHistoryData>>> worksHistory = toSave.Select(s => Tuple.Create<OpWorkHistory, List<OpWorkHistoryData>>(s, s.DataList)).ToList();
            dataProvider.SaveWorkHistory(worksHistory.Select(s => s.Item1).ToArray());

            if (saveHistoryData)
            {
                List<OpWorkHistoryData> worksHistoryData = new List<OpWorkHistoryData>();
                foreach (Tuple<OpWorkHistory, List<OpWorkHistoryData>> tuple in worksHistory.Where(w => w.Item2 != null && w.Item2.Count > 0))
                {
                    tuple.Item2.ForEach(a => a.IdWorkHistory = tuple.Item1.IdWorkHistory);
                    worksHistoryData.AddRange(tuple.Item2);
                }
                if (worksHistoryData.Count > 0)
                    dataProvider.SaveWorkHistoryData(worksHistoryData.ToArray());
            }
        } 
        #endregion
    }
}