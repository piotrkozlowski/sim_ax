﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using System.Net.Mail;
using System.Web;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class EmailComponent : BaseComponent
    {
        #region Members

        public static DataProvider DataProvider;

        public enum OperationType { Add, Delete, Change, ChangeStatus }
        public enum ShippingListOperationType { Close = 1 }

        public static Enums.EmailSendingMode EmailSendingMode = Enums.EmailSendingMode.SMTP;

        public static long SMTPAttachmentMaxSizeInBytes = 5 * 1000 * 1000;
        public static string SMTPServer = "poczta.aiut.com.pl";
        public static string SMTPSender = "noreply@aiut.com.pl";
        public static string HashedPassword = null;
        public static bool EnableSsl = true;
        public static int Port = 587;

        public static long DBMailAttachmentMaxSizeInBytes = 1 * 1000 * 1000;
        public static string DBMailSender = "";
        public static string DBMailProfile = "";

        public static List<MailAddress> InfoMailAddreses = new List<MailAddress>();
        public static string ApplicationName { get; set; }
        public static string ApplicationAdditionalInfo { get; set; }
        public static string SystemInfoText = null;

        #endregion

        #region BussinessObjectNotifications

        #region CreateAndSendEmail
        #region Task
        /// <summary>
        /// Constructs and sends Email
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendEmail(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpTask, OpTask> changedItems, OperationType operationType)
        {
            List<OpOperator> operatorsToEmail = ConstructMailingList(dataProvider, loggedOperator, changedItems);
            string subject;
            Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToEmail, operationType, changedItems, out subject);
            return SendEmail(addressesAndMessages, subject);
        }
        #endregion
        #region Issue
        /// <summary>
        /// Constructs and sends Email
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendEmail(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpIssue, OpIssue> changedItems, OperationType operationType)
        {
            if (operationType != OperationType.ChangeStatus)
            {
                List<OpOperator> operatorsToEmail = ConstructMailingList(dataProvider, loggedOperator, changedItems);
                string subject;
                Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToEmail, operationType, changedItems, out subject);
                return SendEmail(addressesAndMessages, subject);
            }
            else
                return true;
        }
        #endregion
        #region WorkOrder
        /// <summary>
        /// Constructs and sends Email
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendEmail(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpRoute, OpRoute> changedItems, OperationType operationType)
        {
            List<OpOperator> operatorsToEmail = ConstructMailingList(loggedOperator, changedItems);
            string subject;
            Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToEmail, operationType, changedItems, out subject);
            return SendEmail(addressesAndMessages, subject);
        }
        #endregion
        #region Package
        /// <summary>
        /// Constructs and sends Email
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendEmail(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpPackage, OpPackage> changedItems, OperationType operationType)
        {
            List<OpOperator> operatorsToEmail = ConstructMailingList(loggedOperator, changedItems);
            string subject;
            Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToEmail, operationType, changedItems, out subject);
            return SendEmail(addressesAndMessages, subject);
        }
        #endregion
        #region ShippingList
        /// <summary>
        /// Constructs and sends Email
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendEmail(DataProvider dataProvider, OpOperator loggedOperator, Tuple<OpShippingList, OpShippingList> changedItems, ShippingListOperationType operationType)
        {
            List<OpOperator> operatorsToEmail = ConstructMailingList(dataProvider, loggedOperator, changedItems, operationType);
            string subject;
            Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToEmail, operationType, changedItems, out subject);
            return SendEmail(addressesAndMessages, subject);
        }
        #endregion
        #region Location
        /// <summary>
        /// Constructs and sends Email
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="LoggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="operationType">Type of changes made</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool CreateAndSendEmail(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpLocation, OpLocation> changedItems, OperationType operationType)
        {
            List<OpOperator> operatorsToEmail = ConstructMailingList(dataProvider, loggedOperator, changedItems);
            string subject;
            Dictionary<OpOperator, string> addressesAndMessages = ConstructMessageBody(dataProvider, operatorsToEmail, operationType, changedItems, out subject);
            return SendEmail(addressesAndMessages, subject);
        }
        #endregion
        #endregion

        #region ConstructMailingList
        #region Task
        /// <summary>
        /// Constructs lists of operators to Email
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to Email</returns>
        public static List<OpOperator> ConstructMailingList(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpTask, OpTask> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;
            List<OpTask> recipients;
            if (changedItems.Values.Contains(null))
                recipients = changedItems.Keys.ToList();
            else
                recipients = changedItems.Values.Concat(changedItems.Keys.Where(k => k.IdOperatorPerformer != changedItems.TryGetValue<OpTask, OpTask>(k).IdOperatorPerformer)).ToList();
            List<OpOperator> operatorsToEmail = new List<OpOperator>();
            foreach (OpTask task in recipients)
            {
                if (loggedOperator != null && loggedOperator.IdOperator == task.IdOperatorRegistering)
                {
                    if (task.OperatorPerformer != null && !String.IsNullOrEmpty(task.OperatorPerformer.Actor.Email))
                        operatorsToEmail.Add(task.OperatorPerformer);
                }
                else if (loggedOperator != null && loggedOperator.IdOperator == task.IdOperatorPerformer)
                {
                    if (task.OperatorRegistering != null && !String.IsNullOrEmpty(task.OperatorRegistering.Actor.Email))
                        operatorsToEmail.Add(task.OperatorRegistering);
                }
                else
                {
                    if (task.OperatorRegistering != null && !String.IsNullOrEmpty(task.OperatorRegistering.Actor.Email))
                        operatorsToEmail.Add(task.OperatorRegistering);
                    if (task.OperatorPerformer != null && !String.IsNullOrEmpty(task.OperatorPerformer.Actor.Email))
                        operatorsToEmail.Add(task.OperatorPerformer);
                }

                List<OpOperator> addtionallRecipients = TaskComponent.GetTaskDistributorNotificationOperators(dataProvider, task, new List<OperationType>(){ OperationType.Add,
                                                                                                                                                             OperationType.Change,
                                                                                                                                                             OperationType.Delete });
                addtionallRecipients.RemoveAll(o => o.IdOperator == loggedOperator.IdOperator);
                operatorsToEmail.AddRange(addtionallRecipients);
            }

            return operatorsToEmail.Distinct().ToList();
        }
        #endregion
        #region Issue
        /// <summary>
        /// Constructs lists of operators to Email
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to Email</returns>
        public static List<OpOperator> ConstructMailingList(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpIssue, OpIssue> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;
            List<OpIssue> recipients;
            if (changedItems.Values.Contains(null))
                recipients = changedItems.Keys.ToList();
            else
                recipients = changedItems.Values.Concat(changedItems.Keys.Where(k => k.IdOperatorPerformer != changedItems.TryGetValue<OpIssue, OpIssue>(k).IdOperatorPerformer)).ToList();
            List<OpOperator> operatorsToEmail = new List<OpOperator>();
            List<OpOperator> issueTypeAdditionalConstraint = new List<OpOperator>();
            foreach (OpIssue issue in recipients)
            {
                if (loggedOperator != null && loggedOperator.IdOperator == issue.IdOperatorRegistering)
                {
                    if (issue.OperatorPerformer != null && !String.IsNullOrEmpty(issue.OperatorPerformer.Actor.Email))
                        operatorsToEmail.Add(issue.OperatorPerformer);
                }
                else if (loggedOperator != null && loggedOperator.IdOperator == issue.IdOperatorPerformer)
                {
                    if (issue.OperatorRegistering != null && !String.IsNullOrEmpty(issue.OperatorRegistering.Actor.Email))
                        operatorsToEmail.Add(issue.OperatorRegistering);
                }
                else
                {
                    if (issue.OperatorRegistering != null && !String.IsNullOrEmpty(issue.OperatorRegistering.Actor.Email))
                        operatorsToEmail.Add(issue.OperatorRegistering);
                    if (issue.OperatorPerformer != null && !String.IsNullOrEmpty(issue.OperatorPerformer.Actor.Email))
                        operatorsToEmail.Add(issue.OperatorPerformer);
                }
                List<OperationType> operationTypeList = new List<OperationType>(){ OperationType.Add,
                                                                               OperationType.Change,
                                                                               OperationType.Delete };
                List<OpOperator> addtionallRecipients = IssueComponent.GetIssueDistributorNotificationOperators(dataProvider, issue, operationTypeList);
                //addtionallRecipients.RemoveAll(o => o.IdOperator == loggedOperator.IdOperator);
                operatorsToEmail.AddRange(addtionallRecipients);
            }
            return operatorsToEmail.Distinct().ToList();
        }
        #endregion
        #region WorkOrder
        /// <summary>
        /// Constructs lists of operators to Email
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to Email</returns>
        public static List<OpOperator> ConstructMailingList(OpOperator loggedOperator, Dictionary<OpRoute, OpRoute> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;
            List<OpRoute> recipients;
            if (changedItems.Values.Contains(null))
                recipients = changedItems.Keys.ToList();
            else
                recipients = changedItems.Values.Concat(changedItems.Keys.Where(k => k.IdOperatorExecutor != changedItems.TryGetValue<OpRoute, OpRoute>(k).IdOperatorExecutor)).ToList();
            List<OpOperator> operatorsToEmail = new List<OpOperator>();
            foreach (OpRoute route in recipients)
            {
                if (loggedOperator != null && loggedOperator.IdOperator == route.IdOperatorApproved)
                {
                    if (route.OperatorExecutor != null && !String.IsNullOrEmpty(route.OperatorExecutor.Actor.Email))
                        operatorsToEmail.Add(route.OperatorExecutor);
                }
                else if (loggedOperator != null && loggedOperator.IdOperator == route.IdOperatorExecutor)
                {
                    if (route.OperatorApproved != null && !String.IsNullOrEmpty(route.OperatorApproved.Actor.Email))
                        operatorsToEmail.Add(route.OperatorApproved);
                }
                else
                {
                    if (route.OperatorApproved != null && !String.IsNullOrEmpty(route.OperatorApproved.Actor.Email))
                        operatorsToEmail.Add(route.OperatorApproved);
                    if (route.OperatorExecutor != null && !String.IsNullOrEmpty(route.OperatorExecutor.Actor.Email))
                        operatorsToEmail.Add(route.OperatorExecutor);
                }
            }
            return operatorsToEmail.Distinct().ToList();
        }
        #endregion
        #region Package
        /// <summary>
        /// Constructs lists of operators to Email
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to Email</returns>
        public static List<OpOperator> ConstructMailingList(OpOperator loggedOperator, Dictionary<OpPackage, OpPackage> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;
            List<OpOperator> operatorsToEmail = new List<OpOperator>();
            foreach (OpPackage pItem in changedItems.Keys)
            {
                operatorsToEmail.Add(pItem.OperatorCreator);
                if (pItem.OperatorReceiver != null)
                    operatorsToEmail.Add(pItem.OperatorReceiver);
            }
            foreach (OpPackage pItem in changedItems.Values.Where(v => v != null))
            {
                operatorsToEmail.Add(pItem.OperatorCreator);
                if (pItem.OperatorReceiver != null)
                    operatorsToEmail.Add(pItem.OperatorReceiver);
            }
            operatorsToEmail = operatorsToEmail.Distinct().ToList();
            operatorsToEmail.RemoveAll(d => d == null || d.Actor == null || String.IsNullOrEmpty(d.Actor.Email));

            return operatorsToEmail;
        }
        #endregion
        #region ShippingList
        /// <summary>
        /// Constructs lists of operators to Email
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to Email</returns>
        public static List<OpOperator> ConstructMailingList(DataProvider dataProvider, OpOperator loggedOperator, Tuple<OpShippingList, OpShippingList> changedItems, ShippingListOperationType operationType)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;

            List<OpOperator> operatorsToEmail = new List<OpOperator>();

            operatorsToEmail.AddRange(dataProvider.GetOperatorShippingListMailingList(changedItems.Item2.Distributor.IdDistributor/*Location.IdDistributor*/, (int)operationType));

            return operatorsToEmail.Distinct().ToList();
        }
        #endregion

        #region Location
        /// <summary>
        /// Constructs lists of operators to Email
        /// </summary>
        /// <param name="loggedOperator">Operator currently logged</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <returns>List of operators to Email</returns>
        public static List<OpOperator> ConstructMailingList(DataProvider dataProvider, OpOperator loggedOperator, Dictionary<OpLocation, OpLocation> changedItems)
        {
            if (loggedOperator == null)
                loggedOperator = LoggedOperator;

            List<OpOperator> operatorsToEmail = dataProvider.GetAllOperator().Where(x => x.DataList.Any(y => y.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_LOCATION)).ToList();

            foreach (OpLocation location in changedItems.Keys)
            {
                List<OpOperator> addtionallRecipients = LocationComponent.GetLocationDistributorNotificationOperators(dataProvider, location, new List<OperationType>(){ OperationType.Add,
                                                                                                                                                                         OperationType.Change,
                                                                                                                                                                         OperationType.Delete });
                addtionallRecipients.RemoveAll(o => o.IdOperator == loggedOperator.IdOperator);
                operatorsToEmail.AddRange(addtionallRecipients);
            }

            return operatorsToEmail.Distinct().ToList();
        }
        #endregion
        #endregion

        #region ConstructMessageBody
        #region Task
        /// <summary>
        /// Constructs mail body and returns it along with Email address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToEmail">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="subject">returned Email subject</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail, OperationType operationType, Dictionary<OpTask, OpTask> changedItems, out string subject)
        {
            #region Subject
            switch (operationType)
            {
                case OperationType.Add:
                    subject = String.Format(ResourcesText.SubjectTaskAdd, String.Join(", ", changedItems.Keys.Select(item => item.IdTask)));
                    break;
                case OperationType.Change:
                    subject = String.Format(ResourcesText.SubjectTaskChange, String.Join(", ", changedItems.Keys.Select(item => item.IdTask)));
                    break;
                case OperationType.ChangeStatus:
                    subject = String.Format(ResourcesText.SubjectTaskStatusChange, String.Join(", ", changedItems.Keys.Select(item => item.IdTask)));
                    break;
                case OperationType.Delete:
                    subject = String.Format(ResourcesText.SubjectTaskDelete, String.Join(", ", changedItems.Keys.Select(item => item.IdTask)));
                    break;
                default:
                    subject = "";
                    break;
            }
            #endregion
            Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
            foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();
                    bool change = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Change);
                    bool add = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Add);
                    bool delete = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Delete);
                    bool assigned = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.TaskOptions.Assigned);
                    /*bool changeStatus = false;
                    foreach (OpTask tItem in changedItems.Keys)
                    {
                        if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)tItem.IdTaskStatus)))
                            changeStatus = true;
                    }*/

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    OpTaskHistory lastHist = TaskComponent.GetHistory(dataProvider, task.IdTask).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.TaskStatusChange, ResourcesText.AssignedToYou, task.IdTask, changedItems.TryGetValue<OpTask, OpTask>(task).TaskStatus,
                                        task.TaskStatus, lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    OpTaskHistory lastHist = TaskComponent.GetHistory(dataProvider, task.IdTask).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.TaskStatusChange, ResourcesText.YouHaveCreated, task.IdTask, changedItems.TryGetValue<OpTask, OpTask>(task).TaskStatus,
                                        task.TaskStatus, lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;
                            foreach (OpTask task in changedItems.Keys)
                            {
                                string text = "";
                                if (task.IdOperatorPerformer == recipient.IdOperator)
                                    text = ResourcesText.AssignedToYou;
                                if (task.IdOperatorRegistering == recipient.IdOperator)
                                    text = ResourcesText.YouHaveCreated;

                                OpTaskHistory lastHist = TaskComponent.GetHistory(dataProvider, task.IdTask).LastOrDefault();
                                strBody.AppendFormat(ResourcesText.TaskAdd, text, task.IdTask,
                                    task.OperatorRegistering, task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                    task.IdIssue == null ? "" : task.IdIssue.ToString(), task.TaskStatus, task.TaskType,
                                    task.Deadline == null ? "" : task.Deadline.ToString(), task.Priority,
                                    (task.Location != null && !String.IsNullOrEmpty(task.Location.Name) ? (task.Location.Name + " ") : "") + (task.Location != null && !String.IsNullOrEmpty(task.Location.CID) ? task.Location.CID : ""),
                                    HttpUtility.HtmlEncode(task.Notes),
                                    lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                            }
                            /*
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                                //if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    OpTaskHistory lastHist = TaskComponent.GetHistory(dataProvider, task.IdTask).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.TaskAdd, ResourcesText.AssignedToYou, task.IdTask,
                                        task.OperatorRegistering, task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                        task.IdIssue == null ? "" : task.IdIssue.ToString(), task.TaskStatus, task.TaskType,
                                        task.Deadline == null ? "" : task.Deadline.ToString(), task.Priority,
                                        task.Location == null ? "" : task.Location.ToString(),
                                        HttpUtility.HtmlEncode(task.Notes),
                                        lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                                //if (add && recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    OpTaskHistory lastHist = TaskComponent.GetHistory(dataProvider, task.IdTask).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.TaskAdd, ResourcesText.YouHaveCreated, task.IdTask,
                                        task.OperatorRegistering, task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                        task.IdIssue == null ? "" : task.IdIssue.ToString(), task.TaskStatus, task.TaskType,
                                        task.Deadline == null ? "" : task.Deadline.ToString(), task.Priority,
                                        task.Location == null ? "" : task.Location.ToString(),
                                        HttpUtility.HtmlEncode(task.Notes),
                                        lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }*/
                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator || changedItems.TryGetValue<OpTask, OpTask>(t).IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)) &&
                                    (change || (assigned && changedItems.TryGetValue<OpTask, OpTask>(task).IdOperatorPerformer != recipient.IdOperator)))
                                {
                                    OpTaskHistory lastHist = TaskComponent.GetHistory(dataProvider, task.IdTask).LastOrDefault();
                                    OpTask oldVal = changedItems.TryGetValue<OpTask, OpTask>(task);
                                    strBody.AppendFormat(ResourcesText.TaskChange, ResourcesText.AssignedToYou, task.IdTask,
                                        oldVal.OperatorPerformer == null ? "" : oldVal.OperatorPerformer.ToString(), task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                        oldVal.IdIssue == null ? "" : oldVal.IdIssue.ToString(), task.IdIssue == null ? "" : task.IdIssue.ToString(),
                                        oldVal.TaskStatus, task.TaskStatus,
                                        oldVal.TaskType, task.TaskType,
                                        oldVal.Deadline == null ? "" : oldVal.Deadline.ToString(), task.Deadline == null ? "" : task.Deadline.ToString(),
                                        oldVal.Priority, task.Priority,
                                        (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.Name) ? (oldVal.Location.Name + " ") : "") + (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.CID) ? oldVal.Location.CID : ""),
                                        (task.Location != null && !String.IsNullOrEmpty(task.Location.Name) ? (task.Location.Name + " ") : "") + (task.Location != null && !String.IsNullOrEmpty(task.Location.CID) ? task.Location.CID : ""),                                        
                                        oldVal.Route == null ? "" : oldVal.Route.Symbol, task.Route == null ? "" : task.Route.Symbol,
                                        HttpUtility.HtmlEncode(oldVal.Notes), HttpUtility.HtmlEncode(task.Notes),
                                        lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (change && recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    OpTaskHistory lastHist = TaskComponent.GetHistory(dataProvider, task.IdTask).LastOrDefault();
                                    OpTask oldVal = changedItems.TryGetValue<OpTask, OpTask>(task);
                                    strBody.AppendFormat(ResourcesText.TaskChange, ResourcesText.YouHaveCreated, task.IdTask,
                                        oldVal.OperatorPerformer == null ? "" : oldVal.OperatorPerformer.ToString(), task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                        oldVal.IdIssue == null ? "" : oldVal.IdIssue.ToString(), task.IdIssue == null ? "" : task.IdIssue.ToString(),
                                        oldVal.TaskStatus, task.TaskStatus,
                                        oldVal.TaskType, task.TaskType,
                                        oldVal.Deadline == null ? "" : oldVal.Deadline.ToString(), task.Deadline == null ? "" : task.Deadline.ToString(),
                                        oldVal.Priority, task.Priority,
                                        (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.Name) ? (oldVal.Location.Name + " ") : "") + (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.CID) ? oldVal.Location.CID : ""),
                                        (task.Location != null && !String.IsNullOrEmpty(task.Location.Name) ? (task.Location.Name + " ") : "") + (task.Location != null && !String.IsNullOrEmpty(task.Location.CID) ? task.Location.CID : ""),  
                                        oldVal.Route == null ? "" : oldVal.Route.Symbol, task.Route == null ? "" : task.Route.Symbol,
                                        HttpUtility.HtmlEncode(oldVal.Notes), HttpUtility.HtmlEncode(task.Notes),
                                        lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;

                            foreach (OpTask task in changedItems.Keys)
                            {
                                string text = "";
                                if (task.IdOperatorPerformer == recipient.IdOperator)
                                    text = ResourcesText.AssignedToYou;
                                if (task.IdOperatorRegistering == recipient.IdOperator)
                                    text = ResourcesText.YouHaveCreated;
                                strBody.AppendFormat(ResourcesText.TaskDelete, text, task.IdTask,
                                        task.OperatorRegistering, task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                        task.IdIssue == null ? "" : task.IdIssue.ToString(), task.TaskStatus, task.TaskType,
                                        task.Deadline == null ? "" : task.Deadline.ToString(), task.Priority,
                                        (task.Location != null && !String.IsNullOrEmpty(task.Location.Name) ? (task.Location.Name + " ") : "") + (task.Location != null && !String.IsNullOrEmpty(task.Location.CID) ? task.Location.CID : ""),
                                        HttpUtility.HtmlEncode(task.Notes));
                            }
                            /*
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    strBody.AppendFormat(ResourcesText.TaskDelete, ResourcesText.AssignedToYou, task.IdTask,
                                        task.OperatorRegistering, task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                        task.IdIssue == null ? "" : task.IdIssue.ToString(), task.TaskStatus, task.TaskType,
                                        task.Deadline == null ? "" : task.Deadline.ToString(), task.Priority,
                                        task.Location == null ? "" : task.Location.ToString(),
                                        HttpUtility.HtmlEncode(task.Notes));
                                }
                            foreach (OpTask task in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpTaskStatus.Enum)task.TaskStatus.IdTaskStatus)))
                                {
                                    strBody.AppendFormat(ResourcesText.TaskDelete, ResourcesText.YouHaveCreated, task.IdTask,
                                        task.OperatorRegistering, task.OperatorPerformer == null ? "" : task.OperatorPerformer.ToString(),
                                        task.IdIssue == null ? "" : task.IdIssue.ToString(), task.TaskStatus, task.TaskType,
                                        task.Deadline == null ? "" : task.Deadline.ToString(), task.Priority,
                                        task.Location == null ? "" : task.Location.ToString(),
                                        HttpUtility.HtmlEncode(task.Notes));
                                }*/
                            break;
                            #endregion
                    }
                    if (strBody.Length > 0)
                        ret.Add(recipient, strBody.ToString());
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
                }
            }
            return ret;
        }
        #endregion
        #region Issue
        /// <summary>
        /// Constructs mail body and returns it along with Email address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToEmail">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="subject">returned Email subject</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail, OperationType operationType, Dictionary<OpIssue, OpIssue> changedItems, out string subject)
        {
            #region AdditionalConstraints
            List<long> dataTypeToGet = new List<long>() { DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR,
                                                          DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE };

            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, IdDataType: dataTypeToGet.ToArray(), IdOperator: operatorsToEmail.Select(s => s.IdOperator).ToArray());
            #endregion
            #region Subject
            switch (operationType)
            {
                case OperationType.Add:
                    subject = String.Format(ResourcesText.SubjectIssueAdd, String.Join(", ", changedItems.Keys.Select(item => item.IdIssue)));
                    break;
                case OperationType.Change:
                    subject = String.Format(ResourcesText.SubjectIssueChange, String.Join(", ", changedItems.Keys.Select(item => item.IdIssue)));
                    break;
                case OperationType.ChangeStatus:
                    subject = String.Format(ResourcesText.SubjectIssueStatusChange, String.Join(", ", changedItems.Keys.Select(item => item.IdIssue)));
                    break;
                case OperationType.Delete:
                    subject = String.Format(ResourcesText.SubjectIssueDelete, String.Join(", ", changedItems.Keys.Select(item => item.IdIssue)));
                    break;
                default:
                    subject = "";
                    break;
            }
            #endregion
            Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
            foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();
                    bool change = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Change);
                    bool add = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Add);
                    bool delete = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Delete);
                    bool assigned = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.IssueStatusChange, ResourcesText.AssignedToYou, issue.IdIssue, changedItems.TryGetValue<OpIssue, OpIssue>(issue).IssueStatus,
                                        issue.IssueStatus, lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.IssueStatusChange, ResourcesText.YouHaveCreated, issue.IdIssue, changedItems.TryGetValue<OpIssue, OpIssue>(issue).IssueStatus,
                                        issue.IssueStatus, lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }                            
                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.IssueAdd, ResourcesText.AssignedToYou, issue.IdIssue,
                                        issue.OperatorRegistering, issue.OperatorPerformer == null ? "" : issue.OperatorPerformer.ToString(),
                                        issue.IssueStatus, issue.IssueType,
                                        issue.Deadline == null ? "" : issue.Deadline.ToString(), issue.Priority,
                                        (issue.Location != null && !String.IsNullOrEmpty(issue.Location.Name) ? (issue.Location.Name + " ") : "") + (issue.Location != null && !String.IsNullOrEmpty(issue.Location.CID) ? issue.Location.CID : ""),
                                        HttpUtility.HtmlEncode(issue.ShortDescr), lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (add && recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.IssueAdd, ResourcesText.YouHaveCreated, issue.IdIssue,
                                        issue.OperatorRegistering, issue.OperatorPerformer == null ? "" : issue.OperatorPerformer.ToString(),
                                        issue.IssueStatus, issue.IssueType,
                                        issue.Deadline == null ? "" : issue.Deadline.ToString(), issue.Priority,
                                        (issue.Location != null && !String.IsNullOrEmpty(issue.Location.Name) ? (issue.Location.Name + " ") : "") + (issue.Location != null && !String.IsNullOrEmpty(issue.Location.CID) ? issue.Location.CID : ""),
                                        HttpUtility.HtmlEncode(issue.ShortDescr), lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }
                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator || changedItems.TryGetValue<OpIssue, OpIssue>(t).IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)) &&
                                    (change || (assigned && changedItems.TryGetValue<OpIssue, OpIssue>(issue).IdOperatorPerformer != recipient.IdOperator)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    OpIssue oldVal = changedItems.TryGetValue<OpIssue, OpIssue>(issue);
                                    strBody.AppendFormat(ResourcesText.IssueChange, ResourcesText.AssignedToYou, issue.IdIssue,
                                        oldVal.OperatorPerformer == null ? "" : oldVal.OperatorPerformer.ToString(), issue.OperatorPerformer == null ? "" : issue.OperatorPerformer.ToString(),
                                        oldVal.IssueStatus, issue.IssueStatus,
                                        oldVal.IssueType, issue.IssueType,
                                        oldVal.Deadline == null ? "" : oldVal.Deadline.ToString(), issue.Deadline == null ? "" : issue.Deadline.ToString(),
                                        oldVal.Priority, issue.Priority,
                                        (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.Name) ? (oldVal.Location.Name + " ") : "") + (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.CID) ? oldVal.Location.CID : ""),
                                        (issue.Location != null && !String.IsNullOrEmpty(issue.Location.Name) ? (issue.Location.Name + " ") : "") + (issue.Location != null && !String.IsNullOrEmpty(issue.Location.CID) ? issue.Location.CID : ""),
                                        HttpUtility.HtmlEncode(oldVal.ShortDescr), HttpUtility.HtmlEncode(issue.ShortDescr),
                                        lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (change && recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    OpIssue oldVal = changedItems.TryGetValue<OpIssue, OpIssue>(issue);
                                    strBody.AppendFormat(ResourcesText.IssueChange, ResourcesText.YouHaveCreated, issue.IdIssue,
                                        oldVal.OperatorPerformer == null ? "" : oldVal.OperatorPerformer.ToString(), issue.OperatorPerformer == null ? "" : issue.OperatorPerformer.ToString(),
                                        oldVal.IssueStatus, issue.IssueStatus,
                                        oldVal.IssueType, issue.IssueType,
                                        oldVal.Deadline == null ? "" : oldVal.Deadline.ToString(), issue.Deadline == null ? "" : issue.Deadline.ToString(),
                                        oldVal.Priority, issue.Priority,
                                        (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.Name) ? (oldVal.Location.Name + " ") : "") + (oldVal.Location != null && !String.IsNullOrEmpty(oldVal.Location.CID) ? oldVal.Location.CID : ""),
                                        (issue.Location != null && !String.IsNullOrEmpty(issue.Location.Name) ? (issue.Location.Name + " ") : "") + (issue.Location != null && !String.IsNullOrEmpty(issue.Location.CID) ? issue.Location.CID : ""),  
                                        HttpUtility.HtmlEncode(oldVal.ShortDescr), HttpUtility.HtmlEncode(issue.ShortDescr),
                                        lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }
                            }                            
                                break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorPerformer == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.IssueDelete, ResourcesText.AssignedToYou, issue.IdIssue,
                                        issue.OperatorRegistering, issue.OperatorPerformer == null ? "" : issue.OperatorPerformer.ToString(),
                                        issue.IssueStatus, issue.IssueType,
                                        issue.Deadline == null ? "" : issue.Deadline.ToString(), issue.Priority,
                                        (issue.Location != null && !String.IsNullOrEmpty(issue.Location.Name) ? (issue.Location.Name + " ") : "") + (issue.Location != null && !String.IsNullOrEmpty(issue.Location.CID) ? issue.Location.CID : ""),
                                        HttpUtility.HtmlEncode(issue.ShortDescr));
                                }
                            }
                            foreach (OpIssue issue in changedItems.Keys.Where(t => t.IdOperatorRegistering == recipient.IdOperator))
                            {
                                if (!IssueComponent.CheckIssueAdditionalConstraints(dataProvider, issue, recipient, odList))
                                    continue;

                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IssueStatus.IdIssueStatus)))
                                {
                                    OpIssueHistory lastHist = IssueComponent.GetHistory(dataProvider, issue.IdIssue).LastOrDefault();
                                    strBody.AppendFormat(ResourcesText.IssueDelete, ResourcesText.YouHaveCreated, issue.IdIssue,
                                        issue.OperatorRegistering, issue.OperatorPerformer == null ? "" : issue.OperatorPerformer.ToString(),
                                        issue.IssueStatus, issue.IssueType,
                                        issue.Deadline == null ? "" : issue.Deadline.ToString(), issue.Priority,
                                        (issue.Location != null && !String.IsNullOrEmpty(issue.Location.Name) ? (issue.Location.Name + " ") : "") + (issue.Location != null && !String.IsNullOrEmpty(issue.Location.CID) ? issue.Location.CID : ""),
                                        HttpUtility.HtmlEncode(issue.ShortDescr));
                                }
                            }
                            break;
                            #endregion
                    }
                    if (strBody.Length > 0)
                        ret.Add(recipient, strBody.ToString());
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
                }
            }
            return ret;
        }
        #endregion
        #region Package
        /// <summary>
        /// Constructs mail body and returns it along with Email address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToEmail">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="subject">returned Email subject</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail,
            OperationType operationType, Dictionary<OpPackage, OpPackage> changedItems, out string subject,
            Dictionary<OpPackage, string> AdditionallInfo = null)
        {
            #region Subject
            switch (operationType)
            {
                case OperationType.ChangeStatus:
                    subject = String.Format(ResourcesText.SubjectPackageStatusChange, String.Join(", ", changedItems.Keys.Select(item => item.ToString())));
                    break;
                default:
                    subject = "";
                    break;
            }
            #endregion
            Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
            foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            foreach (OpPackage package in changedItems.Keys.Where(t => t.IdOperatorReceiver == recipient.IdOperator))
                            {
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((Enums.PackageStatus)package.IdPackageStatus)))
                                {
                                    string lastPackageStatus = "-";
                                    string currentPackageStaus = package.PackageStatus.ToString();
                                    if (changedItems.ContainsKey(package) && changedItems[package] != null)
                                        lastPackageStatus = changedItems[package].PackageStatus.ToString();
                                    strBody.AppendFormat(ResourcesText.PackageStatusChange, ResourcesText.AssignedToYou, package.IdPackage, lastPackageStatus, currentPackageStaus);

                                    if (AdditionallInfo != null && AdditionallInfo.ContainsKey(package) && !String.IsNullOrEmpty(AdditionallInfo[package]))
                                    {
                                        strBody.AppendLine("<br>" + AdditionallInfo[package]);
                                    }
                                }
                            }
                            foreach (OpPackage package in changedItems.Keys.Where(t => t != null && t.IdOperatorCreator == recipient.IdOperator))
                            {
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((Enums.PackageStatus)package.IdPackageStatus)))
                                {
                                    string lastPackageStatus = "-";
                                    string currentPackageStaus = package.PackageStatus.ToString();
                                    if (changedItems.ContainsKey(package) && changedItems[package] != null)
                                        lastPackageStatus = changedItems[package].PackageStatus.ToString();
                                    if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions(((Enums.PackageStatus)package.IdPackageStatus))))
                                        strBody.AppendFormat(ResourcesText.PackageStatusChange, ResourcesText.YouHaveCreated, package.IdPackage, lastPackageStatus, currentPackageStaus);

                                    if (AdditionallInfo != null && AdditionallInfo.ContainsKey(package) && !String.IsNullOrEmpty(AdditionallInfo[package]))
                                    {
                                        strBody.AppendLine("<br>" + AdditionallInfo[package]);
                                    }
                                }
                            }
                            break;
                            #endregion
                    }
                    if (strBody.Length > 0)
                        ret.Add(recipient, strBody.ToString());
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
                }
            }
            return ret;
        }
        #endregion
        #region WorkOrder
        /// <summary>
        /// Constructs mail body and returns it along with Email address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToEmail">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="subject">returned Email subject</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail, OperationType operationType, Dictionary<OpRoute, OpRoute> changedItems, out string subject)
        {
            #region Subject
            switch (operationType)
            {
                case OperationType.Add:
                    subject = String.Format(ResourcesText.SubjectWorkOrderAdd, String.Join(", ", changedItems.Keys.Select(item => item.Symbol)));
                    break;
                case OperationType.Change:
                    subject = String.Format(ResourcesText.SubjectWorkOrderChange, String.Join(", ", changedItems.Keys.Select(item => item.Symbol)));
                    break;
                case OperationType.ChangeStatus:
                    subject = String.Format(ResourcesText.SubjectWorkOrderStatusChange, String.Join(", ", changedItems.Keys.Select(item => item.Symbol)));
                    break;
                case OperationType.Delete:
                    subject = String.Format(ResourcesText.SubjectWorkOrderDelete, String.Join(", ", changedItems.Keys.Select(item => item.Symbol)));
                    break;
                default:
                    subject = "";
                    break;
            }
            #endregion
            Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
            foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();
                    bool change = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.WorkOrderOptions.Change);
                    bool add = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.WorkOrderOptions.Add);
                    bool delete = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.WorkOrderOptions.Delete);
                    bool assigned = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.WorkOrderOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            if (!change) break;
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                    strBody.AppendFormat(ResourcesText.WorkOrderStatusChange, ResourcesText.AssignedToYou, workOrder.IdRoute, changedItems.TryGetValue<OpRoute, OpRoute>(workOrder).RouteStatus, workOrder.RouteStatus);
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                    strBody.AppendFormat(ResourcesText.WorkOrderStatusChange, ResourcesText.YouHaveApproved, workOrder.IdRoute, changedItems.TryGetValue<OpRoute, OpRoute>(workOrder).RouteStatus, workOrder.RouteStatus);
                            break;
                        #endregion
                        #region Add
                        case OperationType.Add:
                            if (!add && !assigned) break;
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    strBody.AppendFormat(ResourcesText.WorkOrderAdd, ResourcesText.AssignedToYou, workOrder.IdRoute,
                                        workOrder.Symbol, workOrder.OperatorApproved,
                                        workOrder.OperatorExecutor == null ? "" : workOrder.OperatorExecutor.ToString(), workOrder.RouteStatus);
                                }
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                                if (add && recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    strBody.AppendFormat(ResourcesText.WorkOrderAdd, ResourcesText.YouHaveApproved, workOrder.IdRoute,
                                        workOrder.Symbol, workOrder.OperatorApproved,
                                        workOrder.OperatorExecutor == null ? "" : workOrder.OperatorExecutor.ToString(), workOrder.RouteStatus);
                                }
                            break;
                        #endregion
                        #region Change
                        case OperationType.Change:
                            if (!change && !assigned) break;
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator || changedItems.TryGetValue<OpRoute, OpRoute>(t).IdOperatorExecutor == recipient.IdOperator))
                            {
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)) &&
                                    (change || (assigned && changedItems.TryGetValue<OpRoute, OpRoute>(workOrder).IdOperatorExecutor != recipient.IdOperator)))
                                {
                                    OpRoute oldVal = changedItems.TryGetValue<OpRoute, OpRoute>(workOrder);
                                    strBody.AppendFormat(ResourcesText.WorkOrderChange, ResourcesText.AssignedToYou, workOrder.IdRoute,
                                        oldVal.OperatorExecutor == null ? "" : oldVal.OperatorExecutor.ToString(),
                                        workOrder.OperatorExecutor == null ? "" : workOrder.OperatorExecutor.ToString(),
                                        oldVal.Symbol, workOrder.Symbol,
                                        oldVal.RouteStatus, workOrder.RouteStatus);
                                }
                            }
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                            {
                                if (change && recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    OpRoute oldVal = changedItems.TryGetValue<OpRoute, OpRoute>(workOrder);
                                    strBody.AppendFormat(ResourcesText.WorkOrderChange, ResourcesText.YouHaveApproved, workOrder.IdRoute,
                                        oldVal.OperatorExecutor == null ? "" : oldVal.OperatorExecutor.ToString(),
                                        workOrder.OperatorExecutor == null ? "" : workOrder.OperatorExecutor.ToString(),
                                        oldVal.Symbol, workOrder.Symbol,
                                        oldVal.RouteStatus, workOrder.RouteStatus);
                                }
                            }
                            break;
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            if (!delete) break;
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorExecutor == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    strBody.AppendFormat(ResourcesText.WorkOrderDelete, ResourcesText.AssignedToYou, workOrder.IdRoute,
                                        workOrder.Symbol, workOrder.OperatorApproved,
                                        workOrder.OperatorExecutor == null ? "" : workOrder.OperatorExecutor.ToString(), workOrder.RouteStatus);
                                }
                            foreach (OpRoute workOrder in changedItems.Keys.Where(t => t.IdOperatorApproved == recipient.IdOperator))
                                if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((OpRouteStatus.Enum)workOrder.RouteStatus.IdRouteStatus)))
                                {
                                    strBody.AppendFormat(ResourcesText.WorkOrderDelete, ResourcesText.YouHaveApproved, workOrder.IdRoute,
                                        workOrder.Symbol, workOrder.OperatorApproved,
                                        workOrder.OperatorExecutor == null ? "" : workOrder.OperatorExecutor.ToString(), workOrder.RouteStatus);
                                }
                            break;
                            #endregion
                    }
                    if (strBody.Length > 0)
                        ret.Add(recipient, strBody.ToString());
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
                }
            }
            return ret;
        }
        #endregion
        #region ShippingList
        /// <summary>
        /// Constructs mail body and returns it along with Email address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToEmail">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="subject">returned Email subject</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail, ShippingListOperationType operationType, Tuple<OpShippingList, OpShippingList> changedItems, out string subject)
        {
            #region Subject
            switch (operationType)
            {
                case ShippingListOperationType.Close:
                    subject = String.Format(ResourcesText.ShippinListHasBeenClosed, changedItems.Item2.IdShippingList);
                    break;
                default:
                    subject = "";
                    break;
            }
            #endregion
            Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
            List<OpDistributorData> distrData = dataProvider.GetDistributorDataFilter(loadNavigationProperties: false, IdDistributor: new int[] { changedItems.Item2.IdDistributor }, IdDataType: new long[] { DataType.DISTRIBUTOR_SHIPPING_MAIL_NOTE });
            string Note = "";
            if (distrData.Count > 0)
                Note = (string)distrData[0].Value;
            foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();

                    switch (operationType)
                    {
                        case ShippingListOperationType.Close:
                            strBody.AppendLine("<b>ID:</b> " + changedItems.Item2.IdShippingList + "<br/>");
                            strBody.AppendLine("<b>" + ResourcesText.Distributor + ":</b> " + changedItems.Item2./*Location.*/Distributor + "<br/>");
                            strBody.AppendLine("<b>" + ResourcesText.ShippingListType + ":</b> " + changedItems.Item2.ShippingListType + "<br/>");
                            strBody.AppendLine("<b>" + ResourcesText.ClosingPerson + ":</b> " + LoggedOperator.ToString() + "<br/>");
                            strBody.AppendLine("<b>" + ResourcesText.DevicesAmount + ":</b> " + changedItems.Item2.DevicesAmount + "<br/>");
                            strBody.AppendLine("<b>" + ResourcesText.DistributorAdditionalNote + "</b> " + Note.Replace("\r\n", "<br/>") + "<br/>");
                            break;
                        default:
                            break;
                    }
                    if (strBody.Length > 0)
                        ret.Add(recipient, strBody.ToString());
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
                }
            }
            return ret;
        }
        #endregion

        #region Location
        /// <summary>
        /// Constructs mail body and returns it along with Email address
        /// </summary>
        /// <param name="dataProvider">DataProvider instance</param>
        /// <param name="operatorsToEmail">List of operators to Email</param>
        /// <param name="operationType">Type of changes made</param>
        /// <param name="changedItems">Dictionary of items being changed, keys - items after changes, values - items before changes</param>
        /// <param name="subject">returned Email subject</param>
        /// <returns>Dictionary of addresses(keys) and messages(values)</returns>
        public static Dictionary<OpOperator, string> ConstructMessageBody(DataProvider dataProvider, List<OpOperator> operatorsToEmail, OperationType operationType, Dictionary<OpLocation, OpLocation> changedItems, out string subject)
        {
            #region Subject
            switch (operationType)
            {
                case OperationType.Add:
                    subject = String.Format(ResourcesText.SubjectLocationAdd, String.Join(", ", changedItems.Keys.Select(item => item.IdLocation)));
                    break;
                case OperationType.Change:
                    subject = String.Format(ResourcesText.SubjectLocationChange, String.Join(", ", changedItems.Keys.Select(item => item.IdLocation)));
                    break;
                case OperationType.ChangeStatus:
                    subject = String.Format(ResourcesText.SubjectLocationStateChange, String.Join(", ", changedItems.Keys.Select(item => item.IdLocation)));
                    break;
                case OperationType.Delete:
                    subject = String.Format(ResourcesText.SubjectLocationDelete, String.Join(", ", changedItems.Keys.Select(item => item.IdLocation)));
                    break;
                default:
                    subject = "";
                    break;
            }
            #endregion
            Dictionary<OpOperator, string> ret = new Dictionary<OpOperator, string>();
            foreach (OpOperator recipient in operatorsToEmail.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
            {
                try
                {
                    StringBuilder strBody = new StringBuilder();
                    bool change = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.LocationOptions.Change);
                    bool add = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.LocationOptions.Add);
                    bool delete = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.LocationOptions.Delete);
                    //bool assigned = recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.LocationOptions.Assigned);

                    switch (operationType)
                    {
                        #region ChangeStatus
                        case OperationType.ChangeStatus:
                            {
                                if (!change) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((Enums.LocationState)location.LocationStateType.IdLocationStateType)))
                                    {
                                        OpLocation oldVal = changedItems.TryGetValue(location);

                                        //OpLocationStateHistory lastHist = LocationComponent.GetStateHistory(dataProvider, location.IdLocation).LastOrDefault();
                                        strBody.AppendFormat(ResourcesText.LocationStateChange,
                                            location.IdLocation,
                                            oldVal != null ? oldVal.LocationStateType.ToString() : string.Empty,
                                            location.LocationStateType);
                                    }
                                }

                                break;
                            }
                        #endregion
                        #region Add
                        case OperationType.Add:
                            {
                                //if (!add && !assigned) break;
                                if (!add) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    OpLocationStateHistory lastHist = LocationComponent.GetStateHistory(dataProvider, location.IdLocation).LastOrDefault();

                                    strBody.AppendFormat(ResourcesText.LocationAdd,
                                        location.IdLocation,
                                        location.ToString(),
                                        location.LocationStateType.ToString(),
                                        lastHist == null ? "" : HttpUtility.HtmlEncode(lastHist.Notes));
                                }

                                break;
                            }
                        #endregion
                        #region Change
                        case OperationType.Change:
                            {
                                //if (!change && !assigned) break;
                                if (!change) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    if (recipient.EmailNotificationOptions.GetNotify(EmailNotificationOptions.StatusToOptions((Enums.LocationState)location.LocationStateType.IdLocationStateType)) && change)
                                    {
                                        OpLocationStateHistory lastHist = LocationComponent.GetStateHistory(dataProvider, location.IdLocation).LastOrDefault();
                                        OpLocation oldVal = changedItems.TryGetValue<OpLocation, OpLocation>(location);

                                        StringBuilder rows = new StringBuilder();

                                        if (oldVal.IdLocationPattern != location.IdLocationPattern) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.IdLocationPattern, oldVal.IdLocationPattern.HasValue ? oldVal.IdLocationPattern.ToString() : string.Empty, location.IdLocationPattern.HasValue ? location.IdLocationPattern.ToString() : string.Empty); }
                                        if (oldVal.City != location.City) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.City, oldVal.City != null ? oldVal.City : string.Empty, location.City != null ? location.City : string.Empty); }
                                        if (oldVal.Name != location.Name) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Name, oldVal.Name != null ? oldVal.Name : string.Empty, location.Name != null ? location.Name : string.Empty); }
                                        if (oldVal.CID != location.CID) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.CID, oldVal.CID != null ? oldVal.CID : string.Empty, location.CID != null ? location.CID : string.Empty); }
                                        if (oldVal.InKpi != location.InKpi) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.InKpi, oldVal.InKpi, location.InKpi); }
                                        if (oldVal.Address != location.Address) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Address, oldVal.Address != null ? oldVal.Address : string.Empty, location.Address != null ? location.Address : string.Empty); }
                                        if (oldVal.PostalCode != location.PostalCode) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.PostalCode, oldVal.PostalCode != null ? oldVal.PostalCode : string.Empty, location.PostalCode != null ? location.PostalCode : string.Empty); }
                                        if (oldVal.Country != location.Country) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Country, oldVal.Country != null ? oldVal.Country : string.Empty, location.Country != null ? location.Country : string.Empty); }
                                        if (oldVal.Actor != location.Actor) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Actor, oldVal.Actor != null ? oldVal.Actor.ToString() : string.Empty, location.Actor != null ? location.Actor.ToString() : string.Empty); }
                                        if (oldVal.Consumer != location.Consumer) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Consumer, oldVal.Consumer != null ? oldVal.Consumer.ToString() : string.Empty, location.Consumer != null ? location.Consumer.ToString() : string.Empty); }
                                        if (oldVal.LocationStateType != location.LocationStateType) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.LocationStateType, oldVal.LocationStateType != null ? oldVal.LocationStateType.ToString() : string.Empty, location.LocationStateType != null ? location.LocationStateType.ToString() : string.Empty); }
                                        if (oldVal.CreationDate != location.CreationDate) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.CreationDate, oldVal.CreationDate != null ? oldVal.CreationDate.Value.ToString("yyyy-MM-dd") : string.Empty, location.CreationDate != null ? location.CreationDate.Value.ToString("yyyy-MM-dd") : string.Empty); }
                                        if (oldVal.LocationType != location.LocationType) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.LocationType, oldVal.LocationType != null ? oldVal.LocationType.ToString() : string.Empty, location.LocationType != null ? location.LocationType.ToString() : string.Empty); }
                                        if (oldVal.Distributor != location.Distributor) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Distributor, oldVal.Distributor != null ? oldVal.Distributor.ToString() : string.Empty, location.Distributor != null ? location.Distributor.ToString() : string.Empty); }
                                        if (oldVal.Latitude != location.Latitude) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Latitude, oldVal.Latitude != null ? oldVal.Latitude.ToString() : string.Empty, location.Latitude != null ? location.Latitude.ToString() : string.Empty); }
                                        if (oldVal.Longitude != location.Longitude) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Longitude, oldVal.Longitude != null ? oldVal.Longitude.ToString() : string.Empty, location.Longitude != null ? location.Longitude.ToString() : string.Empty); }
                                        if (oldVal.HostCID != location.HostCID) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.HostCID, oldVal.HostCID != null ? oldVal.HostCID.ToString() : string.Empty, location.HostCID != null ? location.HostCID.ToString() : string.Empty); }
                                        if (oldVal.ImrServer != location.ImrServer) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.ImrServer, oldVal.ImrServer != null ? oldVal.ImrServer.ToString() : string.Empty, location.ImrServer != null ? location.ImrServer.ToString() : string.Empty); }
                                        if (oldVal.MaxTruckSize != location.MaxTruckSize) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.MaxTruckSize, oldVal.MaxTruckSize != null ? oldVal.MaxTruckSize.ToString() : string.Empty, location.MaxTruckSize != null ? location.MaxTruckSize.ToString() : string.Empty); }
                                        if (oldVal.Memo != location.Memo) { rows.AppendFormat(ResourcesText.EmailTableRow, ResourcesText.Notes, oldVal.Memo != null ? oldVal.Memo.ToString() : string.Empty, location.Memo != null ? location.Memo.ToString() : string.Empty); }

                                        strBody.AppendFormat(ResourcesText.LocationChange, location.IdLocation, rows.ToString());
                                    }
                                }

                                break;
                            }
                        #endregion
                        #region Delete
                        case OperationType.Delete:
                            {
                                if (!delete) break;

                                foreach (OpLocation location in changedItems.Keys)
                                {
                                    strBody.AppendFormat(ResourcesText.LocationDel,
                                        location.IdLocation,
                                        location.ToString(),
                                        location.LocationStateType);
                                }
                                break;
                            }
                            #endregion
                    }

                    if (strBody.Length > 0)
                    {
                        ret.Add(recipient, strBody.ToString());
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.Message);
                }
            }
            return ret;

        }
        #endregion
        #endregion

        #endregion
        #region IssueNotifier
        public static void IssueNotifier(Exception exception, string messageInfo, string additionalInfo, bool attachPrintScreen = false)
        {
            bool result = false;
            IssueNotifier(exception, messageInfo, additionalInfo, "Error - Issue notifier", out result, attachPrintScreen);
        }

        public static void IssueNotifier(Exception exception, string messageInfo, string additionalInfo, string subject, out bool result, bool attachPrintScreen = false)
        {
            try
            {
                List<Attachment> attachmentList = new List<Attachment>();

                string zipPath = null;
                string logFilePath = null;
                if (attachPrintScreen)
                {
                    #region Screen

                    //robimy printscreena - jesli uzytkownik wyrazi zgode
                    //dolaczamy jako załacznik
                    try
                    {
                        int printScreenWidth = 0;
                        int printScreenHeight = 0;
                        foreach (Screen screen in Screen.AllScreens)
                        {
                            printScreenWidth += screen.Bounds.Width;
                            if (printScreenHeight < screen.Bounds.Height)
                                printScreenHeight = screen.Bounds.Height;
                        }
                        Bitmap printscreen = new Bitmap(printScreenWidth, printScreenHeight);
                        Graphics graphics = Graphics.FromImage(printscreen as Image);
                        graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);
                        string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.InternetCache), "printscreen.jpg");
                        printscreen.Save(fileName, ImageFormat.Jpeg);
                        Attachment attachPrintScr = new Attachment(fileName);
                        attachmentList.Add(attachPrintScr);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, "IssueNotifier", "Failed to create printscreen", ex.Message);
                    }

                    #endregion
                    #region Log

                    string logsDirectoryPath = null;
                    try
                    {
                        if (IMR.Suite.Common.AssemblyWrapper.ExecutingAssembly != null)
                        {
                            string appDir = System.IO.Path.GetDirectoryName(new System.Uri(IMR.Suite.Common.AssemblyWrapper.ExecutingAssembly.CodeBase).LocalPath);
                            if (!string.IsNullOrWhiteSpace(appDir))
                            {
                                logsDirectoryPath = Path.Combine(appDir, "Logs/");

                                DirectoryInfo logsDirectory = new DirectoryInfo(logsDirectoryPath);
                                FileInfo logsFile = logsDirectory.GetFiles().Where(f => f.Extension == ".log").OrderByDescending(f => f.LastWriteTime).FirstOrDefault();

                                if (logsFile != null && logsFile.Exists)
                                {
                                    logFilePath = Path.GetFileNameWithoutExtension(logsFile.FullName) + "_copy" + Path.GetExtension(logsFile.FullName);
                                    System.IO.File.Copy(logsFile.FullName, logFilePath);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, "IssueNotifier", "Failed to create log temp copy", ex.Message);
                    }

                    try
                    {
                        // Probujemy robic zipa
                        if (!string.IsNullOrWhiteSpace(logFilePath))
                        {
                            zipPath = System.IO.Path.Combine(logsDirectoryPath, "log.log.gz");

                            FileInfo fileToCompress = new FileInfo(logFilePath);
                            using (FileStream originalFileStream = fileToCompress.OpenRead())
                            {
                                if ((File.GetAttributes(fileToCompress.FullName) &
                                    FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
                                {
                                    using (FileStream compressedFileStream = File.Create(zipPath))
                                    {
                                        using (System.IO.Compression.GZipStream compressionStream = new System.IO.Compression.GZipStream(compressedFileStream,
                                            System.IO.Compression.CompressionMode.Compress))
                                        {
                                            originalFileStream.CopyTo(compressionStream);

                                        }
                                    }
                                }
                            }
                            Attachment logZipFile = new Attachment(new System.Uri(zipPath).LocalPath);
                            attachmentList.Add(logZipFile);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, "IssueNotifier", "Failed to create zip attachment", ex.Message);

                        zipPath = null;

                        try
                        {
                            // Nie udalo sie z zipem, dajemy zywcem loga
                            if (!string.IsNullOrWhiteSpace(logFilePath))
                            {
                                Attachment logZipFile = new Attachment(new System.Uri(logFilePath).LocalPath);
                                attachmentList.Add(logZipFile);
                            }
                        }
                        catch (Exception innerEx)
                        {
                            IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, "IssueNotifier", "Failed to add log file as attachment", ex.Message);
                        }
                    }

                    #endregion
                }

                subject = String.Format(subject);
                StringBuilder strBody = new StringBuilder();
                string details = String.Empty;
                if (exception != null)
                {
                    details = String.Format("Message:<font color=\"red\">{0}</font>{1}{3}{3}StackTrace:{3}{2}",
                                                    exception.Message,
                                                    exception.InnerException == null ? "" : string.Format("{1}{1}InnerMessage:<font color=\"red\">{0}</font>{1}", exception.InnerException.Message, "<br>"),
                                                    exception.StackTrace,
                                                   "<br>");
                }
                StringBuilder additionalSystemInfo = null;
                if (!String.IsNullOrEmpty(SystemInfoText))
                {
                    additionalSystemInfo = new StringBuilder();
                    additionalSystemInfo.Append("<b>Additional system info</b>:<br>");
                    additionalSystemInfo.Append(SystemInfoText.Replace(Environment.NewLine, "<br>"));
                }

                strBody.AppendFormat("<html><body><font size = \"2\">");
                strBody.AppendFormat("<b>Application name</b>: {0}<br>", ApplicationName);
                strBody.AppendFormat("<b>Assembly version</b>: {0}<br>", Application.ProductVersion);
                strBody.AppendFormat("<b>User name</b>: {0}<br>", Environment.UserName);
                strBody.AppendFormat("<b>Message info</b>: {0}<br>", messageInfo);
                if (additionalSystemInfo != null)
                    strBody.Append(additionalSystemInfo.ToString());
                strBody.AppendFormat("<b>Additional info</b>: {0}<br><br>", additionalInfo);

                if (!String.IsNullOrEmpty(ApplicationAdditionalInfo))
                    strBody.AppendLine(ApplicationAdditionalInfo);
                strBody.AppendLine(String.Format("<b>Operating system</b>: {0}<br>", Environment.OSVersion));
                strBody.AppendLine(String.Format("<b>System is 64bit</b>: {0}<br>", Environment.Is64BitOperatingSystem ? "YES" : "NO"));
                strBody.AppendLine(String.Format("<b>Computer name</b>: {0}<br>", Environment.MachineName));
                strBody.AppendLine(String.Format("<b>Domain</b>: {0}<br>", Environment.UserDomainName));
                strBody.AppendLine(String.Format("<b>Framework version</b>: {0}<br>", Environment.Version));
                strBody.AppendLine(String.Format("<b>Processors</b>: {0}<br>", Environment.ProcessorCount));

                strBody.AppendFormat("<br><b>Details</b>:<br>{0}<br>", details);
                strBody.AppendFormat("</font></body></html>");
                foreach (MailAddress maItem in InfoMailAddreses)
                {
                    EmailComponent.SendEmail(maItem.ToString(), subject, strBody.ToString(), attachmentList);
                }

                #region Usuniecie tempów

                try
                {
                    if (!string.IsNullOrWhiteSpace(zipPath))
                    {
                        System.IO.File.Delete(zipPath);
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, "IssueNotifier", "Failed to delete zip", ex.Message);
                }

                try
                {
                    if (!string.IsNullOrWhiteSpace(logFilePath))
                    {
                        System.IO.File.Delete(logFilePath);
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, "IssueNotifier", "Failed to delete temp log", ex.Message);
                }

                #endregion

                result = true;
            }
            catch
            {
                //nie chcemy tutaj żadnych wyjatkow
                result = false;
            }
        }
        #endregion

        #region SendEmail

        public static bool SendEmail(Dictionary<OpOperator, string> messages, string subject, List<Attachment> attachments = null)
        {
            return SendEmail(messages, subject, SMTPServer, SMTPSender, attachments);
        }

        /// <summary>
        /// Sends an Email(s)
        /// </summary>
        /// <param name="messages">Dictionary of operators to be Emailed(keys) and messages(values)</param>
        /// <param name="subject">Email(s) subject</param>
        /// <returns>true if message sent successfully, false otherwise</returns>
        public static bool SendEmail(Dictionary<OpOperator, string> messages, string subject, string SMTPServer, string SMTPSender, List<Attachment> attachments = null)
        {
            foreach (OpOperator recipient in messages.Keys.Where(o => !String.IsNullOrEmpty(o.Actor.Email)))
            {
                try
                {
                    IMRLog.AddToLog(Module, false, EventID.Email.EmailArguments, EmailSendingMode.ToString(), attachments == null ? "<null>" : attachments.Count.ToString());

                    if (EmailSendingMode == Enums.EmailSendingMode.DBMail)
                    {
                        #region Attachments

                        bool isAttachment = false;
                        List<long> fileIds = new List<long>();

                        IMRLog.AddToLog(Module, false, EventID.Email.EmailDBMailPreparingAttachments);

                        if (attachments != null && attachments.Count > 0)
                        {
                            attachments = attachments.Where(x => x.ContentStream.Length <= DBMailAttachmentMaxSizeInBytes).ToList();

                            foreach (Attachment att in attachments)
                            {
                                string fullFilePath = (att.ContentStream as System.IO.FileStream).Name;
                                string filePath = System.IO.Path.GetDirectoryName(fullFilePath);
                                string fileName = att.Name;

                                byte[] byteArray = null;
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    att.ContentStream.CopyTo(ms);
                                    byteArray = ms.ToArray();
                                }

                                OpFile newFile = new OpFile();
                                newFile.PhysicalFileName = fileName;
                                newFile.Size = new FileInfo(fullFilePath).Length;
                                newFile.InsertDate = DataProvider.DateTimeNow;
                                newFile.FileBytes = byteArray;
                                newFile.Description = string.Empty;

                                newFile.IdFile = DataProvider.SaveFile(newFile);
                                DataProvider.SaveFileContent(newFile);

                                fileIds.Add(newFile.IdFile);

                                isAttachment = true;
                            }
                        }

                        IMRLog.AddToLog(Module, false, EventID.Email.EmailDBMailAttachmentsPrepared);

                        #endregion

                        if (DataProvider != null && DataProvider.dbConnectionCore != null)
                        {
                            isAttachment = fileIds.Count > 0;

                            IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingDBMail, recipient.Login, recipient.Actor.Email, DBMailSender, DBMailProfile, subject, isAttachment, fileIds.ToArray());

                            DataProvider.dbConnectionCore.SendMail(recipient.Actor.Email, DBMailSender, DBMailProfile, subject, messages.TryGetValue(recipient), isAttachment, fileIds.ToArray());

                            IMRLog.AddToLog(Module, false, EventID.Email.EmailSent, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject);
                        }
                    }
                    else
                    {
                        using (MailMessage m = new MailMessage())
                        {
                            m.From = new MailAddress(/*"noreply@aiut.com.pl"*/SMTPSender);
                            m.To.Add(new MailAddress(recipient.Actor.Email));
                            if (attachments != null && attachments.Count > 0)
                            {
                                attachments = attachments.Where(x => x.ContentStream.Length <= SMTPAttachmentMaxSizeInBytes).ToList();
                                attachments.ForEach(a => m.Attachments.Add(a));
                            }
                            m.Subject = subject;
                            m.Body = messages.TryGetValue(recipient);
                            m.IsBodyHtml = true;
                            using (SmtpClient client = new SmtpClient(SMTPServer))
                            {
                                if (HashedPassword != null || !String.IsNullOrEmpty(SMTPSender))
                                {
                                    client.UseDefaultCredentials = false;
                                    client.Credentials = new System.Net.NetworkCredential(SMTPSender/*"noreply"*/, RSA.Decrypt(HashedPassword));
                                }
                                else
                                    client.UseDefaultCredentials = true;

                                /*if (HashedPassword != null)
                                    client = new SmtpClient(SMTPServer) { UseDefaultCredentials = false, Credentials = new System.Net.NetworkCredential("noreply", RSA.Decrypt(HashedPassword)) };
                                else
                                    client = new SmtpClient(SMTPServer) { UseDefaultCredentials = true };*/
                                client.Port = Port;
                                client.EnableSsl = EnableSsl;

                                IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingSMTPMail, recipient.Login, recipient.Actor.Email, subject, m.Attachments.Count, m.IsBodyHtml, Port, EnableSsl);

                                client.Send(m);

                                IMRLog.AddToLog(Module, false, EventID.Email.EmailSent, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject);

#if DEBUG
                                System.Diagnostics.Debug.WriteLine("Send mail - OK");
#endif
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    /*
                    System.Windows.Forms.MessageBox.Show(ex.Message + "      " + Environment.NewLine +
                                                         "Stack trace: " + (ex.StackTrace != null ? ex.StackTrace.ToString() : "") + "      " + Environment.NewLine +
                                                         "Inner exception: " + (ex.InnerException != null ? ex.InnerException.ToString() : "") + "      " + Environment.NewLine +
                                                         "Data: " + (ex.Data != null ? ex.Data.ToString() : ""));*/

                    IMRLog.AddToLog(Module, false, EventID.Email.EmailSendingError, String.Format("{0}({1})", recipient.Login, recipient.Actor.Email), subject, ex.ToString());
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("Send mail - ERROR:" + ex.Message);
#endif
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Sends an Email for selected e-mail address (do not need to create Operator object)
        /// </summary>
        /// <param name="emailAddress">E-mail address to send message</param>
        /// <param name="subject">Message subject</param>
        /// <param name="message">Message body</param>
        /// <returns></returns>
        public static bool SendEmail(string emailAddress, string subject, string message, List<Attachment> attachments = null)
        {
            OpOperator operatorObj = new OpOperator();
            operatorObj.Actor = new OpActor();
            operatorObj.Actor.Email = emailAddress;

            Dictionary<OpOperator, string> messages = new Dictionary<OpOperator, string>();
            messages.Add(operatorObj, message);
            return SendEmail(messages, subject, attachments);
        }

        public static bool SendEmail(string emailAddress, string subject, string message, string SMTPServer, string SMTPSender, List<Attachment> attachments = null)
        {
            OpOperator operatorObj = new OpOperator();
            operatorObj.Actor = new OpActor();
            operatorObj.Actor.Email = emailAddress;

            Dictionary<OpOperator, string> messages = new Dictionary<OpOperator, string>();
            messages.Add(operatorObj, message);
            return SendEmail(messages, subject, SMTPServer, SMTPSender, attachments);
        }

        #endregion
    }
}
