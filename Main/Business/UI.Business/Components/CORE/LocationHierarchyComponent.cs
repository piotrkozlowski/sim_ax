﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.Common;

using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class LocationHierarchyComponent
    {
        public static void SaveLocationArea(DataProvider dataProvider, OpLocation loc, OpArea area, bool useDBCollector = false)
        {
            OpLocationHierarchy insert_parent = null;
            //foreach (var loop in dataProvider.GetAllLocationHierarchy())
            //{
            //    /*if (loop.Location == loc)
            //        DeleteLocationHierarchy(dataProvider,loop);
            //    else*/ 
            //    if (area != null && loop.Location == area.Location)
            //        insert_parent = loop;
            //}
            if (area != null)
            {
                var hierarchyList = dataProvider.GetLocationHierarchyFilter(IdLocation: new long[]{area.Location.IdLocation}, mergeIntoCache: true);
                if (hierarchyList != null && hierarchyList.Count > 0)
                {
                    insert_parent = hierarchyList.Find(t => t.IdLocation == area.Location.IdLocation);
                }
            }
            if (insert_parent == null)
            {
                insert_parent = new OpLocationHierarchy();
                insert_parent.Location = area.Location;
                insert_parent.Hierarchy = String.Format(".{0}.", area.Location.IdLocation);
                dataProvider.SaveLocationHierarchy(insert_parent,useDBCollector);
            }

            OpLocationHierarchy insert_child = null;
            if (loc.CurrentArea != null && loc.CurrentArea.Location.IdLocation != area.Location.IdLocation)
            {
                insert_child = dataProvider.GetLocationHierarchyFilter(IdLocation: new long[] { loc.IdLocation }).First();
            }
            else if (insert_child == null)
            {
                insert_child = new OpLocationHierarchy();
            }
            insert_child.Location = loc;
            insert_child.LocationHierarchyParent = insert_parent;
            insert_child.Hierarchy = String.Format(".{0}.{1}.", area.Location.IdLocation, loc.IdLocation);
            dataProvider.SaveLocationHierarchy(insert_child,useDBCollector);

            loc.CurrentArea = area;
            if (!area.Locations.Contains(loc))
                area.Locations.Add(loc);
        }

        public static void DeleteLocationHierarchy(DataProvider dataProvider, OpLocationHierarchy del, bool useDBCollector = false)
        {
            dataProvider.DeleteLocationHierarchy(del,useDBCollector);
            
            if (del.Location.CurrentArea != null)
                del.Location.CurrentArea.Locations.Remove(del.Location);

            del.Location.CurrentArea = null;
        }

        public static List<OpLocation> GetChildLocations(DataProvider dataProvider, List<OpLocation> parentLocations, bool useCache, bool recursionChilds)
        {
            List<OpLocationHierarchy> opParentHierarchy = new List<OpLocationHierarchy>();
            List<OpLocationHierarchy> opChildHierarchy = new List<OpLocationHierarchy>();

            if (useCache)
                opParentHierarchy = dataProvider.GetAllLocationHierarchy().Join(parentLocations, p => p.IdLocation, q => q.IdLocation, (p, q) => p).ToList();
            else if (parentLocations.Count > 0)
                opParentHierarchy = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: true, mergeIntoCache: true, autoTransaction: false,
                                                                            IdLocation: parentLocations.Select(s => s.IdLocation).ToArray());

            if (useCache)
                opChildHierarchy = dataProvider.GetAllLocationHierarchy().Join(opParentHierarchy, p => p.IdLocationHierarchyParent, q => q.IdLocationHierarchy, (p, q) => p).ToList();
            else if (opParentHierarchy.Count > 0)
                opChildHierarchy = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: true, mergeIntoCache: true, autoTransaction: false,
                                                                           IdLocationHierarchyParent: opParentHierarchy.Select(s => s.IdLocationHierarchy).ToArray());

            List<OpLocation> opLocations = opChildHierarchy.Select(s => s.Location).ToList();
            List<OpLocation> opParentLocations = opLocations.Where(w => w.IdLocationType.In((int)Enums.LocationType.GroupingLocation)).ToList();
            List<OpLocation> opChildLocations = opLocations.Except(opParentLocations).ToList();

            if (recursionChilds && opParentLocations.Count > 0)
                return opChildLocations.Union(GetChildLocations(dataProvider, opParentLocations, useCache, recursionChilds)).ToList();
            else
                return opChildLocations;
        }

        #region GetParentIdLocations
        public static List<long> GetParentIdLocations(Dictionary<long, List<OpLocationHierarchy>> allLocationHierarchyByLocationDict,
            Dictionary<long, List<OpLocationHierarchy>> allLocationHierarchyByHierarchyDict,
            List<long> idLocationList)
        {
            if (allLocationHierarchyByLocationDict == null || allLocationHierarchyByLocationDict.Count == 0
                || allLocationHierarchyByHierarchyDict == null || allLocationHierarchyByHierarchyDict.Count == 0
                || idLocationList == null || idLocationList.Count == 0) return new List<long>();

            List<OpLocationHierarchy> opParentHierarchy = new List<OpLocationHierarchy>();
            //List<OpLocationHierarchy> opChildHierarchy = new List<OpLocationHierarchy>();
            foreach (long idLocation in idLocationList.Distinct())
            {
                if (allLocationHierarchyByLocationDict.ContainsKey(idLocation))
                {
                    List<OpLocationHierarchy> lhWithParents = allLocationHierarchyByLocationDict[idLocation].Where(l => l.IdLocationHierarchyParent.HasValue).ToList();
                    if(lhWithParents.Count > 0)
                    {
                        lhWithParents.ForEach(l => { if (allLocationHierarchyByHierarchyDict.ContainsKey(l.IdLocationHierarchyParent.Value)) opParentHierarchy.AddRange(allLocationHierarchyByHierarchyDict[l.IdLocationHierarchyParent.Value]); });
                    }
                }
            }
            /*
            foreach (OpLocationHierarchy lh in opParentHierarchy)
            {
                if (lh != null && allLocationHierarchyByHierarchyParentDict.ContainsKey(lh.IdLocationHierarchy))
                    opChildHierarchy.AddRange(allLocationHierarchyByHierarchyParentDict[lh.IdLocationHierarchy]);
            }*/
            List<long> hierarchyIdLocationList = opParentHierarchy.Where(q => q != null).Select(q => q.IdLocation).Distinct().ToList();
            return hierarchyIdLocationList.Union(
                GetParentIdLocations(allLocationHierarchyByLocationDict, allLocationHierarchyByHierarchyDict, hierarchyIdLocationList)).Distinct().ToList();
        }
        #endregion
        #region GetChildIdLocations
        public static List<long> GetChildIdLocations(Dictionary<long, List<OpLocationHierarchy>> allLocationHierarchyByLocationDict,
            Dictionary<long, List<OpLocationHierarchy>> allLocationHierarchyByHierarchyParentDict,
            List<long> idLocationList)
        {
            if (allLocationHierarchyByLocationDict == null || allLocationHierarchyByLocationDict.Count == 0
                || allLocationHierarchyByHierarchyParentDict == null || allLocationHierarchyByHierarchyParentDict.Count == 0
                || idLocationList == null || idLocationList.Count == 0) return new List<long>();

            List<OpLocationHierarchy> opParentHierarchy = new List<OpLocationHierarchy>();
            List<OpLocationHierarchy> opChildHierarchy = new List<OpLocationHierarchy>();
            foreach (long idLocation in idLocationList.Distinct())
            {
                if (allLocationHierarchyByLocationDict.ContainsKey(idLocation))
                    opParentHierarchy.AddRange(allLocationHierarchyByLocationDict[idLocation]);
            }

            foreach (OpLocationHierarchy lh in opParentHierarchy)
            {
                if (lh != null && allLocationHierarchyByHierarchyParentDict.ContainsKey(lh.IdLocationHierarchy))
                    opChildHierarchy.AddRange(allLocationHierarchyByHierarchyParentDict[lh.IdLocationHierarchy]);
            }
            List<long> hierarchyIdLocationList = opChildHierarchy.Where(q => q != null).Select(q => q.IdLocation).Distinct().ToList();
            return hierarchyIdLocationList.Union(
                GetChildIdLocations(allLocationHierarchyByLocationDict, allLocationHierarchyByHierarchyParentDict, hierarchyIdLocationList)).Distinct().ToList();
        }
        #endregion

        /*public static List<OpLocationHierarchy> GetLocationAreaHistory(DataProvider dataProvider, OpLocation loc)
        {
            return dataProvider.GetLocationHierarchyFilter(IdLocation: new long[] {loc.IdLocation});
        }*/

        #region RecursiveCopyChildren
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void RecursiveCopyChildren(DataProvider dataProvider, OpLocationHierarchy copyTo, OpLocationHierarchy copyFrom, bool useDBCollector = false)
        {
            if (copyFrom == null) return;
            List<OpLocationHierarchy> childList = dataProvider.GetLocationHierarchyFilter(IdLocationHierarchyParent: new long [] {copyFrom.IdLocationHierarchy}).ToList();
            //foreach (OpLocationHierarchy child in dataProvider.GetAllLocationHierarchy()  <- poprawione
            //                                                  .FindAll(q => q.IdLocationHierarchyParent == copyFrom.IdLocationHierarchy))
            foreach (OpLocationHierarchy child in childList)
            {
                if (copyTo.Hierarchy.Contains(child.IdLocation.ToString())) continue;    //prevent infinite loops
                string sHierarchy = copyTo.Hierarchy + child.IdLocation + ".";
                //OpLocationHierarchy lh = dataProvider.GetAllLocationHierarchy().Find(q => q.Hierarchy.Equals(sHierarchy)); <- poprawione

                OpLocationHierarchy lh = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, customWhereClause: String.Format(" HIERARCHY like '{0}' ", sHierarchy)).FirstOrDefault();
                if (lh == null)
                {
                    lh = new OpLocationHierarchy()
                    {
                        Location = child.Location,
                        LocationHierarchyParent = copyTo,
                        Hierarchy = sHierarchy
                    };
                    dataProvider.SaveLocationHierarchy(lh,useDBCollector);
                }
                RecursiveCopyChildren(dataProvider, lh, child,useDBCollector);
            }
        }
        #endregion
        #region RecursiveDeleteChildren
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void RecursiveDeleteChildren(DataProvider dataProvider, OpLocationHierarchy itemToClear, bool useDBCollector = false)
        {
            //foreach (OpLocationHierarchy child in dataProvider.GetAllLocationHierarchy().Where(q => q.IdLocationHierarchyParent == itemToClear.IdLocationHierarchy)) <- poprawione
            List<OpLocationHierarchy> hierarchyList = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocationHierarchyParent: new long[] { itemToClear.IdLocationHierarchy }).ToList();
            foreach (OpLocationHierarchy child in hierarchyList)
            {
                RecursiveDeleteChildren(dataProvider, child,useDBCollector);
                dataProvider.DeleteLocationHierarchy(child,useDBCollector);
            }
        }
        #endregion
        #region GetLocationHierarchyForLocation
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static List<OpLocationHierarchy> GetLocationHierarchyForLocation(DataProvider dataProvider, long idLocation, bool loadLocationNavigationProperties, bool loadLocationCustomData)
        {
            return GetLocationHierarchyForLocation(dataProvider, new List<long>() { idLocation }, loadLocationNavigationProperties, loadLocationCustomData);
        }
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static List<OpLocationHierarchy> GetLocationHierarchyForLocation(DataProvider dataProvider, List<long> idLocationList, bool loadLocationNavigationProperties, bool loadLocationCustomData)
        {
            List<OpLocationHierarchy> ret = new List<OpLocationHierarchy>();
            ret = GetOriginalHierarchy(dataProvider, idLocationList);
            
            #region LoadParentHierarchy
            List<long> idParetnHierarchy = new List<long>(); // uzupełnienie parentHierarchy, nawet gdy loadLocationNavigationProperties = true, nie wszytsko jest pobierane 
            ret.ForEach(q => { 
                if (q.IdLocationHierarchyParent != null) idParetnHierarchy.Add((long)q.IdLocationHierarchyParent);
            });
            List<OpLocationHierarchy> parentList = new List<OpLocationHierarchy>();
            if (idParetnHierarchy.Count > 0)
            {
                parentList = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: true, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                IdLocationHierarchy: idParetnHierarchy.Distinct().ToArray()).ToList(); // pobranie OpLocationHierarchy dla parent 
            }
                Dictionary<long, OpLocationHierarchy> parentDict = new Dictionary<long,OpLocationHierarchy>();

            foreach (OpLocationHierarchy item in parentList) // przepisanie do słownika OpLocationHierarchy dla parent
            {
                if(!parentDict.ContainsKey(item.IdLocationHierarchy))
                    parentDict.Add((long)item.IdLocationHierarchy,item);
            }
            #endregion

            #region LoadLocation

            List<long> idLocationListForFilter = new List<long>();
            idLocationListForFilter.AddRange(ret.Where(q => q != null).Select(q => q.IdLocation).Distinct().ToList());
            idLocationListForFilter.AddRange(parentList.Where(q => q.IdLocation != null).Select(q => q.IdLocation).Distinct().ToList());
            Dictionary<long, OpLocation> locationDict = new Dictionary<long, OpLocation>();
            if (idLocationListForFilter.Count > 0)
            {
                locationDict = dataProvider.GetLocationFilter(loadNavigationProperties: loadLocationNavigationProperties, loadCustomData: loadLocationCustomData, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                IdLocation: ret.Where(q => q != null).Select(q => q.IdLocation).Distinct().ToArray(), IdDistributor: new int[0])
                .Where(q => (dataProvider.LocationFilterDict.Count == 0 || dataProvider.LocationFilterDict.ContainsKey(q.IdLocation))
                 && (dataProvider.DistributorFilterDict.Count == 0 || dataProvider.DistributorFilterDict.ContainsKey(q.IdDistributor)))
                .Distinct(q => q.IdLocation).ToDictionary(q => q.IdLocation);

            }
            #endregion
            foreach (OpLocationHierarchy item in ret) // usupełnienie Location i ParentHierarchy 
            {
                if (item != null && locationDict.ContainsKey(item.IdLocation) && locationDict[item.IdLocation] != null)
                    item.Location = locationDict[item.IdLocation];
                if(item!=null && item.IdLocationHierarchyParent != null && parentDict.ContainsKey((long)item.IdLocationHierarchyParent) && parentDict[(long)item.IdLocationHierarchyParent] != null)
                    item.LocationHierarchyParent = parentDict[(long)item.IdLocationHierarchyParent];

                if (item != null && item.LocationHierarchyParent != null && locationDict.ContainsKey(item.LocationHierarchyParent.IdLocation) && locationDict[item.LocationHierarchyParent.IdLocation] != null)
                    item.LocationHierarchyParent.Location = locationDict[item.LocationHierarchyParent.IdLocation];
            }
            return ret;
        }  
        #endregion

        #region GetAssignedGroups
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static List<long> GetAssignedGroups(DataProvider dataProvider, long idLocation, bool newFromPattern, long? idLocationPattern = null, bool querryDatabase = false, int? idDistributor = null)
        {
            List<long> ret = new List<long>();
            List<OpLocation> lList = GetAssignedGroups(dataProvider, dataProvider.GetLocation(idLocation), newFromPattern);
            if (lList != null)
                ret = lList.Select(l => l.IdLocation).ToList();
            return ret;
        }
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static List<OpLocation> GetAssignedGroups(DataProvider dataProvider, OpLocation editedLocation, bool newFromPattern, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpLocation> ret = new List<OpLocation>();
            List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();
            if (newFromPattern && editedLocation.IdLocationPattern.HasValue)
                originalHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, editedLocation.IdLocationPattern.Value, loadNavigationProperties, loadCustomData));
            else if (!newFromPattern)
                originalHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, editedLocation.IdLocation, loadNavigationProperties, loadCustomData));
            List<OpLocationHierarchy> hier = originalHierarchy;
            hier = hier.Where(q => q.IdLocation == editedLocation.IdLocation).Select(q => q.LocationHierarchyParent).ToList();

            if (hier.Exists(q => q == null))
                ret.Add(Root);
            ret.AddRange(hier.Where(q => q != null && q.Location != null && q.Location.IdDistributor == editedLocation.IdDistributor).Select(q => q.Location).Distinct());

            return ret;
        }

        #endregion

        #region GetUnassignedGroups
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static List<OpLocation> GetUnassignedGroups(DataProvider dataProvider, OpLocation editedLocation, List<OpLocation> assigned)
        {
            StringBuilder customWhere = new StringBuilder();
            customWhere.Append("([ALLOW_GROUPING] = 1)");
            return dataProvider.GetLocationFilter(customWhereClause: customWhere.ToString()).Except(assigned)
                .Where(q => q.IdLocation != editedLocation.IdLocation && q.IdDistributor == editedLocation.IdDistributor).ToList();
        }
        #endregion

        #region GetAssignedGroupsList
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static List<OpLocation> GetAssignedGroupsList(DataProvider dataProvider, List<OpLocation> editedLocationList, bool newFromPattern, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<long> ret = new List<long>();
            List<OpLocationHierarchy> locationList = new List<OpLocationHierarchy>();
            List<OpLocation> returnListOfLocation = new List<OpLocation>();
            List<string> h = new List<string>();
            List<long> idLocationList = editedLocationList.Select(q => q.IdLocation).Distinct().ToList();
            List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();

            originalHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, idLocationList, loadNavigationProperties, loadCustomData));
            bool isAllNull = true;
            if (!newFromPattern)
            {
                List<OpLocationHierarchy> hier = originalHierarchy;
                h = originalHierarchy.Select(q => q.Hierarchy).ToList();

                foreach (OpLocation item in editedLocationList) //check if root has to be added
                {
                    List<OpLocationHierarchy> checkIfnullHierarchy = originalHierarchy
                        .Where(q => q.IdLocation == item.IdLocation).Select(q => q.LocationHierarchyParent).ToList();
                    if (!checkIfnullHierarchy.Exists(w => w == null))
                    {
                        isAllNull = false;
                        break;
                    }
                }
                ret.AddRange(hier.Where(q => q != null && q.IdLocationHierarchyParent != null).Select(q => q.LocationHierarchyParent.IdLocation).Distinct());

                foreach (long group in ret)
                {
                    bool isCommon = true;
                    foreach (long localization in idLocationList)
                    {
                        if (!h.Any(q => q.Contains(String.Format(".{0}.{1}.", group, localization))))// check if localizations have something common 
                        {
                            isCommon = false;
                            break;
                        }
                    }
                    if (isCommon)// dodanie tych hierarchi w których występuje ten sam idLocation w LocationHierarchyParent (posiadają tą sama hierarchie nadrzędną)
                    {
                        locationList.AddRange(originalHierarchy.Where(q => q.LocationHierarchyParent!=null && q.LocationHierarchyParent.IdLocation == group).Select(q => q.LocationHierarchyParent).ToList()); 
                    }
                }
            }
            locationList = locationList.Distinct().ToList();
            List<int> depositoryIdList = editedLocationList.Select(q => q.IdDistributor).ToList();
            if (depositoryIdList != null && depositoryIdList.Count > 0)
                locationList = locationList.Where(q => q.Location!=null && depositoryIdList.Contains(q.Location.IdDistributor)).ToList();
            
            returnListOfLocation = locationList.Select(q=>q.Location).ToList();
            if (isAllNull) // jesli warunek spełniony znaczy ze wszytskie przynależa do ROOT, dlatego trzeba go dodać
                returnListOfLocation.Add(Root);
            return returnListOfLocation;
        }
        #endregion
        #region GetOriginalHierarchy
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static List<OpLocationHierarchy> GetOriginalHierarchy(DataProvider dataProvider, List<long> editedLocationList)
        {
            List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();
            if (editedLocationList.Count > 0)
            {
                originalHierarchy = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                    /*customWhereClause: String.Format("[HIERARCHY] like '%.{0}.%'", idLocation),*/ IdLocation: editedLocationList.ToArray());//dataProvider.GetAllLocationHierarchy(); 
            }
            return originalHierarchy;
        }
        #endregion
        #region GetUnassignedGroupsList
        public static List<OpLocation> GetUnassignedGroupsList(DataProvider dataProvider, List<OpLocation> editedLocationList, List<OpLocation> assigned)
        {
            StringBuilder customWhere = new StringBuilder();
            customWhere.Append("([ALLOW_GROUPING] = 1)");
            List<long> idLocationList = editedLocationList.Select(q => q.IdLocation).Distinct().ToList();
            List<int> idDistributorList = editedLocationList.Select(q => q.IdDistributor).Distinct().ToList();
            if (idDistributorList != null && idDistributorList.Count > 0)
                return dataProvider.GetLocationFilter(customWhereClause: customWhere.ToString()).Except(assigned).Where(q => !idLocationList.Contains(q.IdLocation) && idDistributorList.Contains(q.IdDistributor)).ToList();
            else
                return dataProvider.GetLocationFilter(customWhereClause: customWhere.ToString()).Except(assigned).Where(q => !idLocationList.Contains(q.IdLocation)).ToList();
        }
        #endregion

        #region ChangeLocationHierarchy

        private static OpLocation Root = new OpLocation() { Name = "ROOT" };
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static bool ChangeLocationHierarchy(DataProvider dataProvider, OpLocation Location, List<OpLocation> assignedGroups, bool allowGroupping, bool querryDatabase, bool useDBCollector = false) 
        {
            return ChangeLocationHierarchy(dataProvider, new List<OpLocation> { Location }, assignedGroups, allowGroupping, querryDatabase,useDBCollector);
        }
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static bool ChangeLocationHierarchy(DataProvider dataProvider, List<long> idLocationList, List<OpLocation> assignedGroups, bool allowGroupping, bool querryDatabase, bool useDBCollector = false)
        {
            List<OpLocation> locationList = dataProvider.GetLocation(idLocationList.ToArray());
            return ChangeLocationHierarchy(dataProvider, locationList, assignedGroups, allowGroupping, querryDatabase, useDBCollector);

        }
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static bool ChangeLocationHierarchy(DataProvider dataProvider, long idLocation, List<OpLocation> assignedGroups, bool allowGroupping,bool querryDatabase, bool useDBCollector = false) 
        {
            OpLocation location = dataProvider.GetLocation(idLocation);
            return ChangeLocationHierarchy(dataProvider, new List<OpLocation> { location }, assignedGroups, allowGroupping,querryDatabase, useDBCollector);
        }
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static void ChangeLocationHierarchy(DataProvider dataProvider, long idLocation, List<OpLocation> assignedGroups, bool allowGroupping, bool useDBCollector = false) 
        {
            bool querryDatabase = false;
            ChangeLocationHierarchy(dataProvider, idLocation, assignedGroups, allowGroupping, querryDatabase, useDBCollector);
        }
        [Obsolete("Use overloaded method with IDataProvider", false)]
        public static bool ChangeLocationHierarchy(DataProvider dataProvider, List<OpLocation> locationList, List<OpLocation> assignedGroups, bool allowGroupping, bool querryDatabase, bool useDBCollector = false)
        {
            try
            {
                List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();

                OpLocation editedLocation = new OpLocation();

                List<OpLocation> originalAssignedGroups = new List<OpLocation>();
                if (locationList.Count == 1)
                    originalAssignedGroups = LocationHierarchyComponent.GetAssignedGroups(dataProvider, locationList[0], false);
                else if (locationList.Count > 1)
                    originalAssignedGroups = LocationHierarchyComponent.GetAssignedGroupsList(dataProvider, locationList, false);

                List<long> idLocationList = locationList.Select(q => q.IdLocation).ToList(); // załadowanie hierarchi po id_location edytowanych grup, grup do ktorych zostala dodana/dodane nowe lokalizacje oraz po Id_location_hierarchy
                idLocationList.AddRange(assignedGroups.Except(originalAssignedGroups).Select(q => q.IdLocation).ToList());
                originalHierarchy = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocation: idLocationList.ToArray());
                originalHierarchy.AddRange(dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocationHierarchyParent: originalHierarchy.Select(q => q.IdLocationHierarchy).ToArray()));

                if (allowGroupping == true)
                {
                    foreach (OpLocation location in locationList)
                    {
                        if (!originalHierarchy.Exists(q => q.IdLocation == location.IdLocation && q.IdLocationHierarchyParent != null) &&
                            !originalHierarchy.Exists(q => q.IdLocation == location.IdLocation))
                        {
                            OpLocationHierarchy lh = new OpLocationHierarchy()
                            {
                                Location = location,
                                IdLocation = location.IdLocation,
                                IdLocationHierarchyParent = null,
                                LocationHierarchyParent = null,
                                Hierarchy = String.Format(".{0}.", location.IdLocation)
                            };
                            dataProvider.SaveLocationHierarchy(lh, useDBCollector);
                        }
                    }
                }

                #region Assign groups
                List<OpLocationHierarchy> copyFromList = new List<OpLocationHierarchy>();
                foreach (OpLocation item in locationList)
                {
                    copyFromList.Add(originalHierarchy.Find(q => q.IdLocation == item.IdLocation));
                }
                int index = 0;
                foreach (OpLocationHierarchy copyFrom in copyFromList)
                {
                    editedLocation = locationList[index];
                    index++;
                    foreach (OpLocation item in assignedGroups.Except(originalAssignedGroups))
                    {
                        if (item == Root) continue;
                        List<OpLocationHierarchy> parents = originalHierarchy.FindAll(q => q.IdLocation == item.IdLocation);
                        if (parents.Count == 0)     //if no parents
                        {
                            OpLocationHierarchy lhp = lhp = new OpLocationHierarchy() //create root
                            {
                                Location = item,
                                LocationHierarchyParent = null,
                                Hierarchy = "." + item.IdLocation + "."
                            };
                            lhp.IdLocationHierarchy = dataProvider.SaveLocationHierarchy(lhp,useDBCollector);

                            OpLocationHierarchy lh = new OpLocationHierarchy()  //assign to root
                            {
                                Location = editedLocation,
                                LocationHierarchyParent = lhp,
                                Hierarchy = (lhp == null ? "." : lhp.Hierarchy) + editedLocation.IdLocation + "."
                            };
                            dataProvider.SaveLocationHierarchy(lh,useDBCollector);

                            if (copyFrom != null)
                                LocationHierarchyComponent.RecursiveCopyChildren(dataProvider, lh, copyFrom,useDBCollector);
                        }
                        else
                        {
                            if (parents.Any(q => q.Hierarchy.Contains("." + editedLocation.IdLocation.ToString() + ".")))
                            {
                                /*WarningMessageBox.ShowDialog(ResourcesText.CycleDetected,
                                        String.Format(ResourcesText.AssigningLocationsToOnesLowerInHierarchyIsNotAllowed_0_Group_1_WillNotBeAssigned,
                                                        Environment.NewLine, item.Name));*/
                                continue;
                            }

                            foreach (OpLocationHierarchy parent in parents)
                            {
                                OpLocationHierarchy lh = new OpLocationHierarchy()
                                {
                                    Location = editedLocation,
                                    LocationHierarchyParent = parent,
                                    Hierarchy = parent.Hierarchy + editedLocation.IdLocation + "."
                                };
                                dataProvider.SaveLocationHierarchy(lh, useDBCollector);

                                if (copyFrom != null)
                                    LocationHierarchyComponent.RecursiveCopyChildren(dataProvider, lh, copyFrom, useDBCollector);
                            }
                        }
                    }
                }
                #endregion
                #region Remove root
                if (originalAssignedGroups.Contains(Root) && !assignedGroups.Contains(Root))
                {
                    //List<OpLocationHierarchy> newHierarchy = dataProvider.GetAllLocationHierarchy();
                    List<OpLocationHierarchy> newHierarchy = new List<OpLocationHierarchy>();
                    foreach (OpLocation item in locationList)
                        newHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, item.IdLocation, true, true));

                    foreach (OpLocation item in locationList)
                    {
                        editedLocation = item;
                        OpLocationHierarchy locationRoot = newHierarchy.Find(q => q.IdLocation == editedLocation.IdLocation && q.IdLocationHierarchyParent == null);

                        List<OpLocationHierarchy> rootChildren = newHierarchy.FindAll(q => q.IdLocationHierarchyParent == locationRoot.IdLocationHierarchy);
                        List<OpLocationHierarchy> locationParents = newHierarchy.FindAll(q => q.IdLocation == editedLocation.IdLocation &&
                                                                                                q.IdLocationHierarchyParent != null);
                        if (locationParents.Count > 0)
                        {
                            foreach (OpLocationHierarchy parent in locationParents)
                                LocationHierarchyComponent.RecursiveCopyChildren(dataProvider, parent, locationRoot);

                            LocationHierarchyComponent.RecursiveDeleteChildren(dataProvider, locationRoot,useDBCollector);

                            dataProvider.DeleteLocationHierarchy(locationRoot,useDBCollector);
                        }
                        if (rootChildren.Count == 0)
                        {
                            dataProvider.DeleteLocationHierarchy(locationRoot,useDBCollector);
                        }
                    }
                }
                #endregion
                foreach (OpLocationHierarchy item in originalHierarchy)
                {
                    if (item.LocationHierarchyParent == null && item.IdLocationHierarchyParent != null)
                        item.LocationHierarchyParent = dataProvider.GetLocationHierarchy((long)item.IdLocationHierarchyParent);
                }
                #region Unassign groups
                foreach (OpLocation location in locationList)
                {
                    editedLocation = location;
                    foreach (OpLocation item in originalAssignedGroups.Except(assignedGroups))
                    {
                        List<OpLocationHierarchy> parents = originalHierarchy.FindAll(q => q.IdLocation == editedLocation.IdLocation &&
                                                                                            q.LocationHierarchyParent != null &&
                                                                                            q.LocationHierarchyParent.IdLocation == item.IdLocation);

                        foreach (OpLocationHierarchy parent in parents) //parents empty for root entry
                        {
                            List<OpLocationHierarchy> children = originalHierarchy.FindAll(q => q.IdLocationHierarchyParent == parent.IdLocationHierarchy);
                            if (children.Count > 0)
                            {
                                OpLocationHierarchy backupParentParent = parent.LocationHierarchyParent;
//                                OpLocationHierarchy root = dataProvider.GetAllLocationHierarchy().Find(q => q.IdLocation == parent.IdLocation && q.IdLocationHierarchyParent == null); <- poprawione
                                OpLocationHierarchy root = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocation: new long[] { parent.IdLocation }).FirstOrDefault();

                                if (root != null && root.IdLocationHierarchyParent == null)   //exists root entry for parent
                                {
                                    foreach (OpLocationHierarchy child in children)
                                    {
                                        if (originalHierarchy.Except(children).Count(q => q.IdLocation == child.IdLocation) > 0 &&       //such entry exists
                                            !originalHierarchy.Exists(q => q.IdLocationHierarchyParent == child.IdLocationHierarchy))    //and has no children
                                            dataProvider.DeleteLocationHierarchy(child, useDBCollector);
                                        else
                                            dataProvider.SaveLocationHierarchy(new OpLocationHierarchy(child) { IdLocationHierarchyParent = root.IdLocationHierarchy },useDBCollector);    //assign to root
                                    }

                                    dataProvider.DeleteLocationHierarchy(parent,useDBCollector);
                                }
                                else
                                {   //assign as root
                                    parent.LocationHierarchyParent = null;
                                    parent.Hierarchy = "." + parent.IdLocation + ".";
                                    dataProvider.SaveLocationHierarchy(parent,useDBCollector);
                                }
                                //if (!dataProvider.GetAllLocationHierarchy().Exists(q => q.IdLocationHierarchyParent == backupParentParent.IdLocationHierarchy)) <-poprawione 
                                if(dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false,IdLocationHierarchyParent: new long []{backupParentParent.IdLocationHierarchy}).Count == 0)
                                    dataProvider.DeleteLocationHierarchy(backupParentParent,useDBCollector);
                            }
                            else
                            {   //entry with no children                            
                                if (assignedGroups.Count == 0)
                                {
                                    OpLocationHierarchy backupParentParentWithouChild = parent.LocationHierarchyParent;
                                    //OpLocationHierarchy rootwithoutChild = dataProvider.GetAllLocationHierarchy().Find(q => q.IdLocation == parent.IdLocation && q.IdLocationHierarchyParent == null); <- poprawione
                                    OpLocationHierarchy rootwithoutChild = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocation: new long[] { parent.IdLocation }).FirstOrDefault();
                                    if (rootwithoutChild != null && rootwithoutChild.IdLocationHierarchyParent == null)   //exists root entry for parent
                                    {
                                        dataProvider.DeleteLocationHierarchy(parent,useDBCollector);
                                    }
                                    dataProvider.DeleteLocationHierarchy(parent,useDBCollector);

                                }
                                else
                                {
                                    dataProvider.DeleteLocationHierarchy(parent,useDBCollector);

                                    if (parent.IdLocationHierarchyParent != null && parent.LocationHierarchyParent.IdLocationHierarchyParent == null &&
                                        !originalHierarchy.Exists(q => q.IdLocationHierarchyParent == parent.IdLocationHierarchyParent &&
                                                                        q.IdLocationHierarchy != parent.IdLocationHierarchy)) //no more children, delete root
                                        dataProvider.DeleteLocationHierarchy(originalHierarchy.Find(q => q.IdLocationHierarchy == parent.IdLocationHierarchyParent &&
                                                                                                            q.IdLocationHierarchyParent == null),useDBCollector);
                                }
                            }
                        }
                    }
                }
                #endregion
                return true;
            }catch(Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region AddRootIfEmpty
        public static void AddRootIfEmpty(DataProvider dataProvider, List<OpLocation> currentLocationGroups, long idLocation, bool allowGroupingEnabled)
        {
            bool ceAllowGroupingEnabled = dataProvider.GetAllLocationHierarchy().Where(q => q.IdLocation == idLocation)
                                                      .Join(dataProvider.GetAllLocationHierarchy(), q => q.IdLocationHierarchy, w => w.IdLocationHierarchyParent, (q, w) => w)
                                                      .Count() == 0;
            bool hasChildren = !allowGroupingEnabled;
            if (currentLocationGroups.Count == 0 && hasChildren)
                currentLocationGroups.Add(Root);
        }
        #endregion

        #region LoadLocationHierarchy

        public static void LoadLocationHierarchy(DataProvider dataProvider, List<OpLocation> locationsToFill)
        {
            List<OpLocationHierarchy> hierarchy = dataProvider.GetLocationHierarchyFilter(IdLocationHierarchy: new long[0], IdLocationHierarchyParent: new long[0], IdLocation: new long[0],
                                                       loadNavigationProperties: false, mergeIntoCache: false,
                                                       autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted);

            if (hierarchy != null)
            {
                Dictionary<long, OpLocation> locactionsDict = new Dictionary<long, OpLocation>(locationsToFill.Distinct().ToDictionary(d => d.IdLocation));

                DateTime dt1 = DateTime.Now;

                Dictionary<long, List<OpLocationHierarchy>> locationHierarchyByLocationDict = hierarchy
                    .GroupBy(q => q.IdLocation).ToDictionary(q => q.Key, q => q.ToList());

                Dictionary<long, List<OpLocationHierarchy>> locationHierarchyByHierarchyParentDict = hierarchy
                    .GroupBy(q => q.IdLocationHierarchy).ToDictionary(q => q.Key, q => q.ToList());

                DateTime dt2 = DateTime.Now;

                List<long> groupsToLoad = Components.CORE.LocationHierarchyComponent.GetParentIdLocations(locationHierarchyByLocationDict, locationHierarchyByHierarchyParentDict, hierarchy.Select(l => l.IdLocation).Distinct().ToList());
                if (groupsToLoad.Count > 0)
                {
                    Dictionary<long, OpLocation> locationGroups = dataProvider.GetLocation(groupsToLoad.ToArray()).ToDictionary(l => l.IdLocation);
                    foreach (OpLocationHierarchy hier in hierarchy)
                    {
                        if (locactionsDict.ContainsKey(hier.IdLocation))
                        {
                            List<long> locationParents = Components.CORE.LocationHierarchyComponent.GetParentIdLocations(locationHierarchyByLocationDict, locationHierarchyByHierarchyParentDict, new List<long>() { hier.IdLocation });
                            if (locationParents.Count > 0)
                            {
                                locationParents.ForEach(l => { if (locationGroups.ContainsKey(l) && !locactionsDict[hier.IdLocation].ParentGroups.Exists(pl => pl.IdLocation == l)) locactionsDict[hier.IdLocation].ParentGroups.Add(locationGroups[l]); });
                            }
                        }
                    }
                }

                TimeSpan ts1 = dt2 - dt1;
                TimeSpan ts2 = DateTime.Now - dt2;
            } 
        }

        #endregion

        #region Override for IDataProvider

        #region GetLocationHierarchyForLocation

        public static List<OpLocationHierarchy> GetLocationHierarchyForLocation(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, long idLocation, bool loadLocationNavigationProperties, bool loadLocationCustomData)
        {
            return GetLocationHierarchyForLocation(dataProvider, new List<long>() { idLocation }, loadLocationNavigationProperties, loadLocationCustomData);
        }

        #endregion
        #region GetLocationHierarchyForLocation

        public static List<OpLocationHierarchy> GetLocationHierarchyForLocation(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, List<long> idLocationList, bool loadLocationNavigationProperties, bool loadLocationCustomData)
        {
            List<OpLocationHierarchy> ret = new List<OpLocationHierarchy>();
            ret = GetOriginalHierarchy(dataProvider, idLocationList);

            #region LoadParentHierarchy
            List<long> idParetnHierarchy = new List<long>(); // uzupełnienie parentHierarchy, nawet gdy loadLocationNavigationProperties = true, nie wszytsko jest pobierane 
            ret.ForEach(q =>
            {
                if (q.IdLocationHierarchyParent != null) idParetnHierarchy.Add((long)q.IdLocationHierarchyParent);
            });
            List<OpLocationHierarchy> parentList = new List<OpLocationHierarchy>();
            if (idParetnHierarchy.Count > 0)
            {
                parentList = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: true, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                IdLocationHierarchy: idParetnHierarchy.Distinct().ToArray()).ToList(); // pobranie OpLocationHierarchy dla parent 
            }
            Dictionary<long, OpLocationHierarchy> parentDict = new Dictionary<long, OpLocationHierarchy>();

            foreach (OpLocationHierarchy item in parentList) // przepisanie do słownika OpLocationHierarchy dla parent
            {
                if (!parentDict.ContainsKey(item.IdLocationHierarchy))
                    parentDict.Add((long)item.IdLocationHierarchy, item);
            }
            #endregion

            #region LoadLocation

            List<long> idLocationListForFilter = new List<long>();
            idLocationListForFilter.AddRange(ret.Where(q => q != null).Select(q => q.IdLocation).Distinct().ToList());
            idLocationListForFilter.AddRange(parentList.Where(q => q.IdLocation != null).Select(q => q.IdLocation).Distinct().ToList());
            Dictionary<long, OpLocation> locationDict = new Dictionary<long, OpLocation>();
            if (idLocationListForFilter.Count > 0)
            {
                locationDict = dataProvider.GetLocationFilter(loadNavigationProperties: loadLocationNavigationProperties, loadCustomData: loadLocationCustomData, mergeIntoCache: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted,
                IdLocation: ret.Where(q => q != null).Select(q => q.IdLocation).Distinct().ToArray(), IdDistributor: new int[0])
                .Where(q => (dataProvider.LocationFilterDict.Count == 0 || dataProvider.LocationFilterDict.ContainsKey(q.IdLocation))
                 && (dataProvider.DistributorFilterDict.Count == 0 || dataProvider.DistributorFilterDict.ContainsKey(q.IdDistributor)))
                .Distinct(q => q.IdLocation).ToDictionary(q => q.IdLocation);

            }
            #endregion
            foreach (OpLocationHierarchy item in ret) // usupełnienie Location i ParentHierarchy 
            {
                if (item != null && locationDict.ContainsKey(item.IdLocation) && locationDict[item.IdLocation] != null)
                    item.Location = locationDict[item.IdLocation];
                if (item != null && item.IdLocationHierarchyParent != null && parentDict.ContainsKey((long)item.IdLocationHierarchyParent) && parentDict[(long)item.IdLocationHierarchyParent] != null)
                    item.LocationHierarchyParent = parentDict[(long)item.IdLocationHierarchyParent];

                if (item != null && item.LocationHierarchyParent != null && locationDict.ContainsKey(item.LocationHierarchyParent.IdLocation) && locationDict[item.LocationHierarchyParent.IdLocation] != null)
                    item.LocationHierarchyParent.Location = locationDict[item.LocationHierarchyParent.IdLocation];
            }
            return ret;
        }

        #endregion

        #region GetAssignedGroupsList
        public static List<OpLocation> GetAssignedGroupsList(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, List<OpLocation> editedLocationList, bool newFromPattern, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<long> ret = new List<long>();
            List<OpLocationHierarchy> locationList = new List<OpLocationHierarchy>();
            List<OpLocation> returnListOfLocation = new List<OpLocation>();
            List<string> h = new List<string>();
            List<long> idLocationList = editedLocationList.Select(q => q.IdLocation).Distinct().ToList();
            List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();

            originalHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, idLocationList, loadNavigationProperties, loadCustomData));
            bool isAllNull = true;
            if (!newFromPattern)
            {
                List<OpLocationHierarchy> hier = originalHierarchy;
                h = originalHierarchy.Select(q => q.Hierarchy).ToList();

                foreach (OpLocation item in editedLocationList) //check if root has to be added
                {
                    List<OpLocationHierarchy> checkIfnullHierarchy = originalHierarchy
                        .Where(q => q.IdLocation == item.IdLocation).Select(q => q.LocationHierarchyParent).ToList();
                    if (!checkIfnullHierarchy.Exists(w => w == null))
                    {
                        isAllNull = false;
                        break;
                    }
                }
                ret.AddRange(hier.Where(q => q != null && q.IdLocationHierarchyParent != null).Select(q => q.LocationHierarchyParent.IdLocation).Distinct());

                foreach (long group in ret)
                {
                    bool isCommon = true;
                    foreach (long localization in idLocationList)
                    {
                        if (!h.Any(q => q.Contains(String.Format(".{0}.{1}.", group, localization))))// check if localizations have something common 
                        {
                            isCommon = false;
                            break;
                        }
                    }
                    if (isCommon)// dodanie tych hierarchi w których występuje ten sam idLocation w LocationHierarchyParent (posiadają tą sama hierarchie nadrzędną)
                    {
                        locationList.AddRange(originalHierarchy.Where(q => q.LocationHierarchyParent != null && q.LocationHierarchyParent.IdLocation == group).Select(q => q.LocationHierarchyParent).ToList());
                    }
                }
            }
            locationList = locationList.Distinct().ToList();
            List<int> depositoryIdList = editedLocationList.Select(q => q.IdDistributor).ToList();
            if (depositoryIdList != null && depositoryIdList.Count > 0)
                locationList = locationList.Where(q => q.Location != null && depositoryIdList.Contains(q.Location.IdDistributor)).ToList();

            returnListOfLocation = locationList.Select(q => q.Location).ToList();
            if (isAllNull) // jesli warunek spełniony znaczy ze wszytskie przynależa do ROOT, dlatego trzeba go dodać
                returnListOfLocation.Add(Root);
            return returnListOfLocation;
        }

        #endregion

        #region ChangeLocationHierarchy

        public static bool ChangeLocationHierarchy(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocation Location, List<OpLocation> assignedGroups, bool allowGroupping, bool querryDatabase, bool useDBCollector = false)
        {
            return ChangeLocationHierarchy(dataProvider, new List<OpLocation> { Location }, assignedGroups, allowGroupping, querryDatabase,useDBCollector);
        }

        #endregion
        #region ChangeLocationHierarchy

        public static bool ChangeLocationHierarchy(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, List<long> idLocationList, List<OpLocation> assignedGroups, bool allowGroupping, bool querryDatabase, bool useDBCollector = false)
        {
            List<OpLocation> locationList = dataProvider.GetLocation(idLocationList.ToArray());
            return ChangeLocationHierarchy(dataProvider, locationList, assignedGroups, allowGroupping, querryDatabase,useDBCollector);

        }

        #endregion
        #region ChangeLocationHierarchy

        public static bool ChangeLocationHierarchy(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, long idLocation, List<OpLocation> assignedGroups, bool allowGroupping, bool querryDatabase, bool useDBCollector = false)
        {
            OpLocation location = dataProvider.GetLocation(idLocation);
            return ChangeLocationHierarchy(dataProvider, new List<OpLocation> { location }, assignedGroups, allowGroupping, querryDatabase,useDBCollector);
        }

        #endregion
        #region ChangeLocationHierarchy

        public static void ChangeLocationHierarchy(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, long idLocation, List<OpLocation> assignedGroups, bool allowGroupping, bool useDBCollector = false)
        {
            bool querryDatabase = false;
            ChangeLocationHierarchy(dataProvider, idLocation, assignedGroups, allowGroupping, querryDatabase,useDBCollector);
        }

        #endregion
        #region ChangeLocationHierarchy

        public static bool ChangeLocationHierarchy(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, List<OpLocation> locationList, List<OpLocation> assignedGroups, bool allowGroupping, bool querryDatabase, bool useDBCollector = false)
        {
            try
            {
                List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();

                OpLocation editedLocation = new OpLocation();

                List<OpLocation> originalAssignedGroups = new List<OpLocation>();
                if (locationList.Count == 1)
                    originalAssignedGroups = LocationHierarchyComponent.GetAssignedGroups(dataProvider, locationList[0], false);
                else if (locationList.Count > 1)
                    originalAssignedGroups = LocationHierarchyComponent.GetAssignedGroupsList(dataProvider, locationList, false);

                List<long> idLocationList = locationList.Select(q => q.IdLocation).ToList(); // załadowanie hierarchi po id_location edytowanych grup, grup do ktorych zostala dodana/dodane nowe lokalizacje oraz po Id_location_hierarchy
                idLocationList.AddRange(assignedGroups.Except(originalAssignedGroups).Select(q => q.IdLocation).ToList());
                originalHierarchy = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocation: idLocationList.ToArray());
                originalHierarchy.AddRange(dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocationHierarchyParent: originalHierarchy.Select(q => q.IdLocationHierarchy).ToArray()));

                if (allowGroupping == true)
                {
                    foreach (OpLocation location in locationList)
                    {
                        if (!originalHierarchy.Exists(q => q.IdLocation == location.IdLocation && q.IdLocationHierarchyParent != null) &&
                            !originalHierarchy.Exists(q => q.IdLocation == location.IdLocation))
                        {
                            OpLocationHierarchy lh = new OpLocationHierarchy()
                            {
                                Location = location,
                                IdLocation = location.IdLocation,
                                IdLocationHierarchyParent = null,
                                LocationHierarchyParent = null,
                                Hierarchy = String.Format(".{0}.", location.IdLocation)
                            };
                            dataProvider.SaveLocationHierarchy(lh,useDBCollector);
                        }
                    }
                }

                #region Assign groups
                List<OpLocationHierarchy> copyFromList = new List<OpLocationHierarchy>();
                foreach (OpLocation item in locationList)
                {
                    copyFromList.Add(originalHierarchy.Find(q => q.IdLocation == item.IdLocation));
                }
                int index = 0;
                foreach (OpLocationHierarchy copyFrom in copyFromList)
                {
                    editedLocation = locationList[index];
                    index++;
                    foreach (OpLocation item in assignedGroups.Except(originalAssignedGroups))
                    {
                        if (item == Root) continue;
                        List<OpLocationHierarchy> parents = originalHierarchy.FindAll(q => q.IdLocation == item.IdLocation);
                        if (parents.Count == 0)     //if no parents
                        {
                            OpLocationHierarchy lhp = lhp = new OpLocationHierarchy() //create root
                            {
                                Location = item,
                                LocationHierarchyParent = null,
                                Hierarchy = "." + item.IdLocation + "."
                            };
                            lhp.IdLocationHierarchy = dataProvider.SaveLocationHierarchy(lhp,useDBCollector);

                            OpLocationHierarchy lh = new OpLocationHierarchy()  //assign to root
                            {
                                Location = editedLocation,
                                LocationHierarchyParent = lhp,
                                Hierarchy = (lhp == null ? "." : lhp.Hierarchy) + editedLocation.IdLocation + "."
                            };
                            dataProvider.SaveLocationHierarchy(lh,useDBCollector);

                            if (copyFrom != null)
                                LocationHierarchyComponent.RecursiveCopyChildren(dataProvider, lh, copyFrom,useDBCollector);
                        }
                        else
                        {
                            if (parents.Any(q => q.Hierarchy.Contains("." + editedLocation.IdLocation.ToString() + ".")))
                            {
                                /*WarningMessageBox.ShowDialog(ResourcesText.CycleDetected,
                                        String.Format(ResourcesText.AssigningLocationsToOnesLowerInHierarchyIsNotAllowed_0_Group_1_WillNotBeAssigned,
                                                        Environment.NewLine, item.Name));*/
                                continue;
                            }

                            foreach (OpLocationHierarchy parent in parents)
                            {
                                OpLocationHierarchy lh = new OpLocationHierarchy()
                                {
                                    Location = editedLocation,
                                    LocationHierarchyParent = parent,
                                    Hierarchy = parent.Hierarchy + editedLocation.IdLocation + "."
                                };
                                dataProvider.SaveLocationHierarchy(lh,useDBCollector);

                                if (copyFrom != null)
                                    LocationHierarchyComponent.RecursiveCopyChildren(dataProvider, lh, copyFrom,useDBCollector);
                            }
                        }
                    }
                }
                #endregion
                #region Remove root
                if (originalAssignedGroups.Contains(Root) && !assignedGroups.Contains(Root))
                {
                    //List<OpLocationHierarchy> newHierarchy = dataProvider.GetAllLocationHierarchy();
                    List<OpLocationHierarchy> newHierarchy = new List<OpLocationHierarchy>();
                    foreach (OpLocation item in locationList)
                        newHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, item.IdLocation, true, true));

                    foreach (OpLocation item in locationList)
                    {
                        editedLocation = item;
                        OpLocationHierarchy locationRoot = newHierarchy.Find(q => q.IdLocation == editedLocation.IdLocation && q.IdLocationHierarchyParent == null);

                        List<OpLocationHierarchy> rootChildren = newHierarchy.FindAll(q => q.IdLocationHierarchyParent == locationRoot.IdLocationHierarchy);
                        List<OpLocationHierarchy> locationParents = newHierarchy.FindAll(q => q.IdLocation == editedLocation.IdLocation &&
                                                                                                q.IdLocationHierarchyParent != null);
                        if (locationParents.Count > 0)
                        {
                            foreach (OpLocationHierarchy parent in locationParents)
                                LocationHierarchyComponent.RecursiveCopyChildren(dataProvider, parent, locationRoot,useDBCollector);

                            LocationHierarchyComponent.RecursiveDeleteChildren(dataProvider, locationRoot,useDBCollector);

                            dataProvider.DeleteLocationHierarchy(locationRoot,useDBCollector);
                        }
                        if (rootChildren.Count == 0)
                        {
                            dataProvider.DeleteLocationHierarchy(locationRoot,useDBCollector);
                        }
                    }
                }
                #endregion
                foreach (OpLocationHierarchy item in originalHierarchy)
                {
                    if (item.LocationHierarchyParent == null && item.IdLocationHierarchyParent != null)
                        item.LocationHierarchyParent = dataProvider.GetLocationHierarchy((long)item.IdLocationHierarchyParent);
                }
                #region Unassign groups
                foreach (OpLocation location in locationList)
                {
                    editedLocation = location;
                    foreach (OpLocation item in originalAssignedGroups.Except(assignedGroups))
                    {
                        List<OpLocationHierarchy> parents = originalHierarchy.FindAll(q => q.IdLocation == editedLocation.IdLocation &&
                                                                                            q.LocationHierarchyParent != null &&
                                                                                            q.LocationHierarchyParent.IdLocation == item.IdLocation);

                        foreach (OpLocationHierarchy parent in parents) //parents empty for root entry
                        {
                            List<OpLocationHierarchy> children = originalHierarchy.FindAll(q => q.IdLocationHierarchyParent == parent.IdLocationHierarchy);
                            if (children.Count > 0)
                            {
                                OpLocationHierarchy backupParentParent = parent.LocationHierarchyParent;
                                //                                OpLocationHierarchy root = dataProvider.GetAllLocationHierarchy().Find(q => q.IdLocation == parent.IdLocation && q.IdLocationHierarchyParent == null); <- poprawione
                                OpLocationHierarchy root = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocation: new long[] { parent.IdLocation }).FirstOrDefault();

                                if (root != null && root.IdLocationHierarchyParent == null)   //exists root entry for parent
                                {
                                    foreach (OpLocationHierarchy child in children)
                                    {
                                        if (originalHierarchy.Except(children).Count(q => q.IdLocation == child.IdLocation) > 0 &&       //such entry exists
                                            !originalHierarchy.Exists(q => q.IdLocationHierarchyParent == child.IdLocationHierarchy))    //and has no children
                                            dataProvider.DeleteLocationHierarchy(child,useDBCollector);
                                        else
                                            dataProvider.SaveLocationHierarchy(new OpLocationHierarchy(child) { IdLocationHierarchyParent = root.IdLocationHierarchy },useDBCollector);    //assign to root
                                    }

                                    dataProvider.DeleteLocationHierarchy(parent,useDBCollector);
                                }
                                else
                                {   //assign as root
                                    parent.LocationHierarchyParent = null;
                                    parent.Hierarchy = "." + parent.IdLocation + ".";
                                    dataProvider.SaveLocationHierarchy(parent,useDBCollector);
                                }
                                //if (!dataProvider.GetAllLocationHierarchy().Exists(q => q.IdLocationHierarchyParent == backupParentParent.IdLocationHierarchy)) <-poprawione 
                                if (dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocationHierarchyParent: new long[] { backupParentParent.IdLocationHierarchy }).Count == 0)
                                    dataProvider.DeleteLocationHierarchy(backupParentParent,useDBCollector);
                            }
                            else
                            {   //entry with no children                            
                                if (assignedGroups.Count == 0)
                                {
                                    OpLocationHierarchy backupParentParentWithouChild = parent.LocationHierarchyParent;
                                    //OpLocationHierarchy rootwithoutChild = dataProvider.GetAllLocationHierarchy().Find(q => q.IdLocation == parent.IdLocation && q.IdLocationHierarchyParent == null); <- poprawione
                                    OpLocationHierarchy rootwithoutChild = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocation: new long[] { parent.IdLocation }).FirstOrDefault();
                                    if (rootwithoutChild != null && rootwithoutChild.IdLocationHierarchyParent == null)   //exists root entry for parent
                                    {
                                        dataProvider.DeleteLocationHierarchy(parent,useDBCollector);
                                    }
                                    dataProvider.DeleteLocationHierarchy(parent,useDBCollector);

                                }
                                else
                                {
                                    dataProvider.DeleteLocationHierarchy(parent,useDBCollector);

                                    if (parent.IdLocationHierarchyParent != null && parent.LocationHierarchyParent.IdLocationHierarchyParent == null &&
                                        !originalHierarchy.Exists(q => q.IdLocationHierarchyParent == parent.IdLocationHierarchyParent &&
                                                                        q.IdLocationHierarchy != parent.IdLocationHierarchy)) //no more children, delete root
                                        dataProvider.DeleteLocationHierarchy(originalHierarchy.Find(q => q.IdLocationHierarchy == parent.IdLocationHierarchyParent &&
                                                                                                            q.IdLocationHierarchyParent == null),useDBCollector);
                                }
                            }
                        }
                    }
                }
                #endregion
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        #endregion

        #region RecursiveCopyChildren

        public static void RecursiveCopyChildren(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocationHierarchy copyTo, OpLocationHierarchy copyFrom, bool useDBCollector = false)
        {
            if (copyFrom == null) return;
            List<OpLocationHierarchy> childList = dataProvider.GetLocationHierarchyFilter(IdLocationHierarchyParent: new long[] { copyFrom.IdLocationHierarchy }).ToList();
            //foreach (OpLocationHierarchy child in dataProvider.GetAllLocationHierarchy()  <- poprawione
            //                                                  .FindAll(q => q.IdLocationHierarchyParent == copyFrom.IdLocationHierarchy))
            foreach (OpLocationHierarchy child in childList)
            {
                if (copyTo.Hierarchy.Contains(child.IdLocation.ToString())) continue;    //prevent infinite loops
                string sHierarchy = copyTo.Hierarchy + child.IdLocation + ".";
                //OpLocationHierarchy lh = dataProvider.GetAllLocationHierarchy().Find(q => q.Hierarchy.Equals(sHierarchy)); <- poprawione

                OpLocationHierarchy lh = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, customWhereClause: String.Format(" HIERARCHY like '{0}' ", sHierarchy)).FirstOrDefault();
                if (lh == null)
                {
                    lh = new OpLocationHierarchy()
                    {
                        Location = child.Location,
                        LocationHierarchyParent = copyTo,
                        Hierarchy = sHierarchy
                    };
                    dataProvider.SaveLocationHierarchy(lh,useDBCollector);
                }
                RecursiveCopyChildren(dataProvider, lh, child,useDBCollector);
            }
        }

        #endregion
        #region RecursiveDeleteChildren

        public static void RecursiveDeleteChildren(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocationHierarchy itemToClear, bool useDBCollector = false)
        {
            //foreach (OpLocationHierarchy child in dataProvider.GetAllLocationHierarchy().Where(q => q.IdLocationHierarchyParent == itemToClear.IdLocationHierarchy)) <- poprawione
            List<OpLocationHierarchy> hierarchyList = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, IdLocationHierarchyParent: new long[] { itemToClear.IdLocationHierarchy }).ToList();
            foreach (OpLocationHierarchy child in hierarchyList)
            {
                RecursiveDeleteChildren(dataProvider, child,useDBCollector);
                dataProvider.DeleteLocationHierarchy(child,useDBCollector);
            }
        }

        #endregion

        #region GetOriginalHierarchy

        public static List<OpLocationHierarchy> GetOriginalHierarchy(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, List<long> editedLocationList)
        {
            List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();
            if (editedLocationList.Count > 0)
            {
                originalHierarchy = dataProvider.GetLocationHierarchyFilter(loadNavigationProperties: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, autoTransaction: false,
                    /*customWhereClause: String.Format("[HIERARCHY] like '%.{0}.%'", idLocation),*/ IdLocation: editedLocationList.ToArray());//dataProvider.GetAllLocationHierarchy(); 
            }
            return originalHierarchy;
        }

        #endregion

        #region GetAssignedGroups

        public static List<long> GetAssignedGroups(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, long idLocation, bool newFromPattern, long? idLocationPattern = null, bool querryDatabase = false, int? idDistributor = null)
        {
            List<long> ret = new List<long>();
            List<OpLocation> lList = GetAssignedGroups(dataProvider, dataProvider.GetLocation(idLocation), newFromPattern);
            if (lList != null)
                ret = lList.Select(l => l.IdLocation).ToList();
            return ret;
        }

        #endregion
        #region GetAssignedGroups

        public static List<OpLocation> GetAssignedGroups(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocation editedLocation, bool newFromPattern, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpLocation> ret = new List<OpLocation>();
            List<OpLocationHierarchy> originalHierarchy = new List<OpLocationHierarchy>();
            if (newFromPattern && editedLocation.IdLocationPattern.HasValue)
                originalHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, editedLocation.IdLocationPattern.Value, loadNavigationProperties, loadCustomData));
            else if (!newFromPattern)
                originalHierarchy.AddRange(GetLocationHierarchyForLocation(dataProvider, editedLocation.IdLocation, loadNavigationProperties, loadCustomData));
            List<OpLocationHierarchy> hier = originalHierarchy;
            hier = hier.Where(q => q.IdLocation == editedLocation.IdLocation).Select(q => q.LocationHierarchyParent).ToList();

            if (hier.Exists(q => q == null))
                ret.Add(Root);
            ret.AddRange(hier.Where(q => q != null && q.Location != null && q.Location.IdDistributor == editedLocation.IdDistributor).Select(q => q.Location).Distinct());

            return ret;
        }

        #endregion

        #region GetUnassignedGroups

        public static List<OpLocation> GetUnassignedGroups(IMR.Suite.UI.Business.Objects.IDataProvider dataProvider, OpLocation editedLocation, List<OpLocation> assigned)
        {
            StringBuilder customWhere = new StringBuilder();
            customWhere.Append("([ALLOW_GROUPING] = 1)");
            return dataProvider.GetLocationFilter(customWhereClause: customWhere.ToString()).Except(assigned)
                .Where(q => q.IdLocation != editedLocation.IdLocation && q.IdDistributor == editedLocation.IdDistributor).ToList();
        }

        #endregion

        #endregion
    }
}
