﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using System.Text.RegularExpressions;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class VersionElementComponent : BaseComponent
    {
        #region CompareVersions
        /// <summary>
        /// Porównuje dwie wersje używając OpVersionNumberFormat.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns>-1 jeśli left ma nowszą wersje, 0 jeśli wersje są te same, 1 jeśli right ma nowszą wersję</returns>
        /// <exception cref="ArgumentException">Jeśli któryś z elementów nie ma VersionNbrFormat lub oba elementy mają różny IdVersionNbrFormat lub format nie jest wspierany lub oba OpVersionElement są null</exception>
        /// <exception cref="FormatException">Jeśli OpVersion.VersionNbr ma niewłaściwy format.</exception>
        public static int CompareVersions(OpVersionElement left, OpVersionElement right)
        {
            if (left == null && right == null)
            {
                throw new ArgumentException(string.Format("You need atleas one version element to make a comprasion."));
            }
            else if (left == null && right != null)
            {
                return 1;
            }
            else if (left != null && right == null)
            {
                return -1;
            }

            if (left.VersionNbrFormat == null || right.VersionNbrFormat == null || left.VersionNbrFormat.IdVersionNbrFormat != right.VersionNbrFormat.IdVersionNbrFormat)
            {
                throw new ArgumentException(string.Format("Invalid version number format: left {0}, right {1}, equal: {2}",
                    left.VersionNbrFormat == null ? "<null>" : left.VersionNbrFormat.ToString(),
                    right.VersionNbrFormat == null ? "<null>" : right.VersionNbrFormat.ToString(),
                    left.IdVersionNbrFormat == right.IdVersionNbrFormat));
            }

            switch ((Enums.VersionNbrFormat)left.IdVersionNbrFormat)
            {
                case Enums.VersionNbrFormat.AssemblyVersion4:
                    {
                        Regex stringRegex = new Regex(left.VersionNbrFormat.RegularExpression);

                        if (!stringRegex.IsMatch(left.Version.VersionNbr)) throw new FormatException(string.Format("Left version is not in valid format, got '{0}', expecting '{1}'", left.Version.VersionNbr, left.VersionNbrFormat.RegularExpression));
                        if (!stringRegex.IsMatch(right.Version.VersionNbr)) throw new FormatException(string.Format("Left version is not in valid format, got '{0}', expecting '{1}'", right.Version.VersionNbr, right.VersionNbrFormat.RegularExpression));

                        int[] leftVersion = left.Version.VersionNbr.Split('.').Select(x => int.Parse(x)).ToArray();
                        int[] rightVersion = right.Version.VersionNbr.Split('.').Select(x => int.Parse(x)).ToArray();

                        for (int i = 0; i < leftVersion.Length; ++i)
                        {
                            if (leftVersion[i] > rightVersion[i]) return -1; else if (leftVersion[i] < rightVersion[i]) return 1;
                        }

                        return 0;
                    }

            }

            throw new ArgumentException("Version number format not supported");
        } 
        #endregion
                
        #region Delete

        public static void DeleteHistory(DataProvider dataProvider, int idVersionElement)
        {
            foreach (var item in dataProvider.GetVersionElementHistoryFilter(IdVersionElement: new int[] { idVersionElement }))
            {
                dataProvider.DeleteVersionElementHistory(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, int idVersionElement)
        {
            foreach (var item in dataProvider.GetVersionElementDataFilter(IdVersionElement: new int[1] { idVersionElement }))
            {
                dataProvider.DeleteVersionElementData(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, OpVersionElement versionElement, long idDataType, int index)
        {
            long idData = versionElement.DataList.First(d => d.IdDataType == idDataType && d.Index == index).IdData;
            if (idData > 0)
                dataProvider.DeleteVersionElementData(dataProvider.GetVersionElementData(idData));
        }

        public static void Delete(DataProvider dataProvider, OpVersionElement objectToDelete)
        {
            try
            {
                DeleteHistory(dataProvider, objectToDelete.IdVersionElement);
                DeleteData(dataProvider, objectToDelete.IdVersionElement);
                dataProvider.DeleteVersionElement(objectToDelete);
                //if (LogCustomEvents)
                //    LogSuccess(EventID.Forms.VersionElemenetDeleted, objectToDelete.IdVersionElement);
            }
            catch (Exception ex)
            {
                //if (LogCustomEvents)
                //    LogError(EventID.Forms.VersionElemenetDeletionError, objectToDelete.IdVersionElement, ex.Message);
                throw ex;
            }
        }

        #endregion


        #region GetPreviousNextVersionStateFlow

        public static void GetPreviousNextVersionStateFlow(DataProvider dataProvider, OpOperator loggedOperator, List<OpVersionState> allowedVersionStateEdit, List<OpVersionState> allowedVersionStateChange, OpVersion version, List<OpVersionState> prevList, List<OpVersionState> nextList)
        {
            OpVersionState currentVersionState = null;
            if (version == null || version.IdVersion == 0)
                currentVersionState = dataProvider.GetVersionState((int)Enums.VersionState.RequestForChange);
            else
                currentVersionState = dataProvider.GetVersionState(version.IdVersionState);

            List<OpVersionState> allowedVersionStateList = new List<OpVersionState>();
            if (version == null || version.IdVersion == 0)
                allowedVersionStateList.Add(currentVersionState);
            else
            {
                allowedVersionStateList.AddRange(allowedVersionStateEdit);
                if (!allowedVersionStateList.Exists(t => t.IdVersionState == version.IdVersionState))
                    allowedVersionStateList.Add(currentVersionState);
            }

            if (loggedOperator != null && loggedOperator.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.OPERATOR_VERSION_STATE_FLOW_PREV, DataType.OPERATOR_VERSION_STATE_FLOW_NEXT })))
            {
                #region DataBaseConfiguration

                List<OpOperatorData> ddList = loggedOperator.DataList.Where(d => d.IdDataType.In(new long[] { DataType.OPERATOR_VERSION_STATE_FLOW_PREV, DataType.OPERATOR_VERSION_STATE_FLOW_NEXT })).ToList();
                foreach (OpOperatorData ddItem in ddList.Where(d => d.IdDataType == DataType.OPERATOR_VERSION_STATE_FLOW_NEXT))
                {
                    int? idPrevVersionState = null;
                    int? idNextVersionState = null;
                    if (ddItem.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextVersionState = idVersionStateTmp;
                    }
                    OpOperatorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.OPERATOR_VERSION_STATE_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevVersionState = idVersionStateTmp;
                    }

                    if (idNextVersionState == currentVersionState.IdVersionState && idPrevVersionState.HasValue)
                    {
                        prevList.Add(dataProvider.GetVersionState(idPrevVersionState.Value));
                    }

                    if (idPrevVersionState == currentVersionState.IdVersionState && idNextVersionState.HasValue)
                    {
                        nextList.Add(dataProvider.GetVersionState(idNextVersionState.Value));
                    }
                }

                #endregion
            }
            else if (loggedOperator != null && loggedOperator.Distributor != null && loggedOperator.Distributor.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV, DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT })))
            {
                #region DataBaseConfiguration

                List<OpDistributorData> ddList = loggedOperator.Distributor.DataList.Where(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV, DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT })).ToList();
                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT))
                {
                    int? idPrevVersionState = null;
                    int? idNextVersionState = null;
                    if (ddItem.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextVersionState = idVersionStateTmp;
                    }
                    OpDistributorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevVersionState = idVersionStateTmp;
                    }

                    if (idNextVersionState == currentVersionState.IdVersionState && idPrevVersionState.HasValue)
                    {
                        prevList.Add(dataProvider.GetVersionState(idPrevVersionState.Value));
                    }

                    if (idPrevVersionState == currentVersionState.IdVersionState && idNextVersionState.HasValue)
                    {
                        nextList.Add(dataProvider.GetVersionState(idNextVersionState.Value));
                    }
                }

                #endregion
            }
            else
            {
                #region Default

                switch ((Enums.VersionState)currentVersionState.IdVersionState)
                {
                    case Enums.VersionState.ChangeDeployment:
                        {
                            prevList.Add(dataProvider.GetVersionState((int)Enums.VersionState.Validation));
                            nextList.Add(dataProvider.GetVersionState((int)Enums.VersionState.ChangeRollback));
                            nextList.Add(dataProvider.GetVersionState((int)Enums.VersionState.EarlyPostDeploymentSupport));
                            break;
                        }
                    case Enums.VersionState.ChangeRollback:
                        {
                            prevList.Add(dataProvider.GetVersionState((int)Enums.VersionState.Validation));
                            //nextList.Add(dataProvider.GetVersionState((int)Enums.VersionState.ChangeDeployment));
                            break;
                        }
                    case Enums.VersionState.EarlyPostDeploymentSupport:
                        {
                            prevList.Add(dataProvider.GetVersionState((int)Enums.VersionState.ChangeDeployment));
                            prevList.Add(dataProvider.GetVersionState((int)Enums.VersionState.ChangeRollback));
                            break;
                        }
                    case Enums.VersionState.Validation:
                        {
                            prevList.Add(dataProvider.GetVersionState((int)Enums.VersionState.Testing));
                            nextList.Add(dataProvider.GetVersionState((int)Enums.VersionState.ChangeDeployment));
                            nextList.Add(dataProvider.GetVersionState((int)Enums.VersionState.ChangeRollback));
                            break;
                        }
                    default:
                        {
                            int[] enumValues = Enum.GetValues(typeof(Enums.VersionState)).Cast<int>().ToArray();

                            if ((version.IdVersionState + 1) <= enumValues.Max())
                            {
                                nextList.Add(dataProvider.GetVersionState(version.IdVersionState + 1));
                            }

                            if ((version.IdVersionState - 1) >= enumValues.Min())
                            {
                                prevList.Add(dataProvider.GetVersionState(version.IdVersionState - 1));
                            }

                            break;
                        }
                }

                #endregion
            }

            nextList.RemoveAll(x => x == null);
            prevList.RemoveAll(x => x == null);

            nextList = nextList.Intersect(allowedVersionStateList).OrderBy(x => x.IdVersionState).ToList();
            prevList = prevList.Intersect(allowedVersionStateList).OrderBy(x => x.IdVersionState).ToList();
        }
        
        #endregion

        #region GetFlowOrder

        public static List<OpVersionState> GetFlowOrder(DataProvider dataProvider, OpOperator loggedOperator)
        {
            List<OpVersionState> flowOrder = new List<OpVersionState>();

            if (loggedOperator != null && loggedOperator.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.OPERATOR_VERSION_STATE_FLOW_PREV, DataType.OPERATOR_VERSION_STATE_FLOW_NEXT })))
            {
                #region DataBaseConfiguration
                List<OpOperatorData> ddList = loggedOperator.DataList.Where(d => d.IdDataType.In(new long[] { DataType.OPERATOR_VERSION_STATE_FLOW_PREV, DataType.OPERATOR_VERSION_STATE_FLOW_NEXT })).ToList();
                int currentVersionState = int.Parse(ddList.Where(x => x.Index == 0).First().Value.ToString());
                foreach (OpOperatorData ddItem in ddList.Where(d => d.IdDataType == DataType.OPERATOR_VERSION_STATE_FLOW_NEXT))
                {
                    int? idPrevVersionState = null;
                    int? idNextVersionState = null;
                    if (ddItem.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextVersionState = idVersionStateTmp;
                    }
                    OpOperatorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.OPERATOR_VERSION_STATE_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevVersionState = idVersionStateTmp;
                    }
                    if (idPrevVersionState == currentVersionState && idNextVersionState.HasValue)
                    {
                        if (dataProvider.GetVersionState(idPrevVersionState.Value) != null && !flowOrder.Exists(d => d.IdVersionState == idPrevVersionState.Value))
                        {
                            flowOrder.Add(dataProvider.GetVersionState(idPrevVersionState.Value));//dodajemy obecny status żeby nie zamknąć sobie drogi jakiejkolwiek zmiany przy zmianach masowych
                        }

                        if (idNextVersionState != currentVersionState
                            && dataProvider.GetVersionState(idNextVersionState.Value) != null
                            && !flowOrder.Exists(d => d.IdVersionState == idNextVersionState.Value))
                        {
                            flowOrder.Add(dataProvider.GetVersionState(idNextVersionState.Value));
                            currentVersionState = idNextVersionState.Value;
                        }
                    }
                }

                #endregion
            }
            else if (loggedOperator != null && loggedOperator.Distributor != null && loggedOperator.Distributor.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV, DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT })))
            {
                #region DataBaseConfiguration

                List<OpDistributorData> ddList = loggedOperator.Distributor.DataList.Where(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV, DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT })).ToList();
                int currentVersionState = int.Parse(ddList.Where(x => x.Index == 0).First().Value.ToString());
                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT))
                {
                    int? idPrevVersionState = null;
                    int? idNextVersionState = null;
                    if (ddItem.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextVersionState = idVersionStateTmp;
                    }
                    OpDistributorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevVersionState = idVersionStateTmp;
                    }
                    if (idPrevVersionState == currentVersionState && idNextVersionState.HasValue)
                    {
                        if (dataProvider.GetVersionState(idPrevVersionState.Value) != null && !flowOrder.Exists(d => d.IdVersionState == idPrevVersionState.Value))
                        {
                            flowOrder.Add(dataProvider.GetVersionState(idPrevVersionState.Value));//dodajemy obecny status żeby nie zamknąć sobie drogi jakiejkolwiek zmiany przy zmianach masowych
                        }

                        if (idNextVersionState != currentVersionState
                            && dataProvider.GetVersionState(idNextVersionState.Value) != null
                            && !flowOrder.Exists(d => d.IdVersionState == idNextVersionState.Value))
                        {
                            flowOrder.Add(dataProvider.GetVersionState(idNextVersionState.Value));
                            currentVersionState = idNextVersionState.Value;
                        }
                    }
                }

                #endregion
            }
            else
            {
                #region Default

                flowOrder.AddRange(dataProvider.GetVersionState(Enum.GetValues(typeof(Enums.VersionState)).Cast<int>().ToArray()));

                #endregion
            }

            flowOrder.RemoveAll(t => t == null);

            return flowOrder;
        }

        #endregion

        #region GetPossibleVersionStateFlow

        public static List<OpVersionState> GetPossibleVersionStateFlow(DataProvider dataProvider, OpOperator loggedOperator, List<OpVersionState> allowedVersionStateEdit, List<OpVersionState> allowedVersionStateChange, OpVersion version)
        {
            OpVersionState currentVersionState = null;
            if (version == null || version.IdVersion == 0)
                currentVersionState = dataProvider.GetVersionState((int)Enums.VersionState.RequestForChange);
            else
                currentVersionState = dataProvider.GetVersionState(version.IdVersionState);

            List<OpVersionState> allowedVersionStateList = new List<OpVersionState>();
            if (version == null || version.IdVersion == 0)
                allowedVersionStateList.Add(currentVersionState);
            else
            {
                allowedVersionStateList.AddRange(allowedVersionStateEdit);
                if (!allowedVersionStateList.Exists(t => t.IdVersionState == version.IdVersionState))
                    allowedVersionStateList.Add(currentVersionState);
            }

            List<OpVersionState> allowedVersionStateChangeWorkflow = new List<OpVersionState>();

            if (loggedOperator != null && loggedOperator.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.OPERATOR_VERSION_STATE_FLOW_PREV, DataType.OPERATOR_VERSION_STATE_FLOW_NEXT })))
            {
                #region DataBaseConfiguration

                List<OpOperatorData> ddList = loggedOperator.DataList.Where(d => d.IdDataType.In(new long[] { DataType.OPERATOR_VERSION_STATE_FLOW_PREV, DataType.OPERATOR_VERSION_STATE_FLOW_NEXT })).ToList();
                foreach (OpOperatorData ddItem in ddList.Where(d => d.IdDataType == DataType.OPERATOR_VERSION_STATE_FLOW_NEXT))
                {
                    int? idPrevVersionState = null;
                    int? idNextVersionState = null;
                    if (ddItem.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextVersionState = idVersionStateTmp;
                    }
                    OpOperatorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.OPERATOR_VERSION_STATE_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevVersionState = idVersionStateTmp;
                    }
                    if (idPrevVersionState == currentVersionState.IdVersionState && idNextVersionState.HasValue)
                    {
                        if (dataProvider.GetVersionState(idPrevVersionState.Value) != null && !allowedVersionStateChangeWorkflow.Exists(d => d.IdVersionState == idPrevVersionState.Value))
                        {
                            allowedVersionStateChangeWorkflow.Add(dataProvider.GetVersionState(idPrevVersionState.Value));//dodajemy obecny status żeby nie zamknąć sobie drogi jakiejkolwiek zmiany przy zmianach masowych
                        }
                        
                        if (idNextVersionState != currentVersionState.IdVersionState
                            && dataProvider.GetVersionState(idNextVersionState.Value) != null
                            && !allowedVersionStateChangeWorkflow.Exists(d => d.IdVersionState == idNextVersionState.Value))
                        {
                            allowedVersionStateChangeWorkflow.Add(dataProvider.GetVersionState(idNextVersionState.Value));
                        }
                    }
                }

                #endregion
            }
            else if (loggedOperator != null && loggedOperator.Distributor != null && loggedOperator.Distributor.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV, DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT })))
            {
                #region DataBaseConfiguration

                List<OpDistributorData> ddList = loggedOperator.Distributor.DataList.Where(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV, DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT })).ToList();
                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_VERSION_STATE_FLOW_NEXT))
                {
                    int? idPrevVersionState = null;
                    int? idNextVersionState = null;
                    if (ddItem.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextVersionState = idVersionStateTmp;
                    }
                    OpDistributorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_VERSION_STATE_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idVersionStateTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idVersionStateTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevVersionState = idVersionStateTmp;
                    }
                    if (idPrevVersionState == currentVersionState.IdVersionState && idNextVersionState.HasValue)
                    {
                        if (dataProvider.GetVersionState(idPrevVersionState.Value) != null && !allowedVersionStateChangeWorkflow.Exists(d => d.IdVersionState == idPrevVersionState.Value))
                        {
                            allowedVersionStateChangeWorkflow.Add(dataProvider.GetVersionState(idPrevVersionState.Value));//dodajemy obecny status żeby nie zamknąć sobie drogi jakiejkolwiek zmiany przy zmianach masowych
                        }
                        
                        if (idNextVersionState != currentVersionState.IdVersionState
                            && dataProvider.GetVersionState(idNextVersionState.Value) != null
                            && !allowedVersionStateChangeWorkflow.Exists(d => d.IdVersionState == idNextVersionState.Value))
                        {
                            allowedVersionStateChangeWorkflow.Add(dataProvider.GetVersionState(idNextVersionState.Value));
                        }
                    }
                }

                #endregion
            }
            else
            {
                #region Default

                switch (currentVersionState.IdVersionState)
                {
                    default:
                    {
                        allowedVersionStateChangeWorkflow.AddRange(dataProvider.GetVersionState(Enum.GetValues(typeof(Enums.VersionState)).Cast<int>().ToArray()));
                        break;
                    }
                }

                #endregion
            }

            allowedVersionStateChangeWorkflow.RemoveAll(t => t == null);

            List<OpVersionState> isList = allowedVersionStateList;
            isList = isList.Intersect(allowedVersionStateChangeWorkflow).ToList();

            foreach (OpVersionState isItem in allowedVersionStateChange)
            {
                if (allowedVersionStateChangeWorkflow.Exists(t => t.IdVersionState == isItem.IdVersionState) && !isList.Exists(t => t.IdVersionState == isItem.IdVersionState))
                    isList.Add(isItem);
            }

            if (!isList.Exists(t => t.IdVersionState == currentVersionState.IdVersionState))
                isList.Add(currentVersionState);

            isList = isList.OrderBy(t => t.IdVersionState).ToList();

            return isList;
        }

        #endregion
    }
}