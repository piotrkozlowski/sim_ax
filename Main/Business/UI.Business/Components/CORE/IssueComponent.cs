﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using System.Data;
using DW_OP = IMR.Suite.UI.Business.Objects.DW;


namespace IMR.Suite.UI.Business.Components.CORE
{
    public class IssueComponent : BaseComponent
    {
        #region Methods
        #region Get

        public static OpIssue GetByID(DataProvider dataProvider, int Id)
        {
            return GetByID(dataProvider, Id, false);
        }

        public static OpIssue GetByID(DataProvider dataProvider, int Id, bool queryDatabase)
        {
            if (Id == 0)
                return null;

            return dataProvider.GetIssue(Id, queryDatabase);
        }

        public static List<OpIssue> GetAll(DataProvider dataProvider, bool excludeFinished)
        {
            if (excludeFinished)
                return dataProvider.GetAllIssue().Where(t => t.IdIssueStatus != (int)OpIssueStatus.Enum.FINISHED).OrderByDescending(i => i.CreationDate).ToList();
            else
                return dataProvider.GetAllIssue();
        }

        public static List<OpIssueHistory> GetHistory(DataProvider dataProvider, int Id)
        {
            return dataProvider.GetIssueHistoryFilter(IdIssue: new int[] { Id });// .Where(ih => ih.IdIssue == Id).ToList();
        }

        public static List<OpFile> GetAttachments(DataProvider DataProvider, OpIssue issue)
        {
            List<OpFile> issueFiles = new List<OpFile>();
            if (issue.DataList.Count(d => d.IdDataType == DataType.ISSUE_ATTACHMENT) > 0)
            {
                long[] fileIds = issue.DataList.Where(d => d.IdDataType == DataType.ISSUE_ATTACHMENT).Select(d => Convert.ToInt64(d.Value)).ToArray();
                if (fileIds.Length > 0)
                {
                    issueFiles = DataProvider.GetFile(fileIds, true);
                }
            }
            return issueFiles;
        }

        public static List<OpTask> GetNotFinishedTasks(DataProvider dataProvider, int idIssue)
        {
            //return dataProvider.GetAllTask().Where(
            //    t => t.IdIssue.HasValue && t.IdIssue == idIssue && (t.ID_TASK_STATUS != (int)OpTaskStatus.Enum.Finished_with_error &&
            //        t.ID_TASK_STATUS != (int)OpTaskStatus.Enum.Canceled &&
            //        t.ID_TASK_STATUS != (int)OpTaskStatus.Enum.Finished_succesfully)).ToList();
            StringBuilder strCustomWhereClause = new StringBuilder();
            strCustomWhereClause.AppendFormat(" [ID_ISSUE] is not null AND [ID_ISSUE] = {0}", idIssue);
            strCustomWhereClause.AppendFormat(" AND [ID_TASK_STATUS] not in ({0},{1},{2})", (int)OpTaskStatus.Enum.Finished_with_error, (int)OpTaskStatus.Enum.Canceled, (int)OpTaskStatus.Enum.Finished_succesfully);
            return dataProvider.GetTaskFilter(customWhereClause: strCustomWhereClause.ToString(), autoTransaction: false);
        }

        public static List<OpDataType> GetAttributes(DataProvider dataProvider, int idIssue)
        {
            List<OpDataType> attributesList = new List<OpDataType>();

            OpDataTypeGroup issueEditDataTypeGroup = dataProvider.GetDataTypeGroupFilter(Name: "ISSUE_EDIT_PARAMS").FirstOrDefault();
            if (issueEditDataTypeGroup != null)
            {
                List<OpDataTypeInGroup> issueEditDataType = dataProvider.GetDataTypeInGroup(issueEditDataTypeGroup.IdDataTypeGroup);
                if (issueEditDataType != null && issueEditDataType.Count > 0)
                    attributesList = issueEditDataType.Select(d => d.DataType).ToList();
            }

            return attributesList;
        }

        public static List<OpIssueData> GetAttributesData(DataProvider dataProvider, int idIssue)
        {
            List<OpIssueData> attributesList = new List<OpIssueData>();

            OpDataTypeGroup issueEditDataTypeGroup = dataProvider.GetDataTypeGroupFilter(Name: "IMRSC_PARAMS_EDIT_GROUP").FirstOrDefault();
            if (issueEditDataTypeGroup != null)
            {
                List<OpDataTypeInGroup> issueEditDataType = dataProvider.GetDataTypeInGroup(issueEditDataTypeGroup.IdDataTypeGroup);
                if (issueEditDataType != null && issueEditDataType.Count > 0)
                    attributesList = GetAttributesData(dataProvider, idIssue, issueEditDataType.Select(d => d.DataType).ToList());
            }
            return attributesList;
        }
        public static List<OpIssueData> GetAttributesData(DataProvider dataProvider, int idIssue, List<OpDataType> attributes)
        {
            if (attributes != null && attributes.Count > 0)
            {
                List<OpIssueData> attributesData = dataProvider.GetIssueDataFilter(IdDataType: attributes.Select(d => d.IdDataType).ToArray(), customWhereClause: "[ID_ISSUE] = " + idIssue);
                if (attributesData != null)
                    attributesData.ForEach(a => a.DataType = attributes.Find(d => d.IdDataType == a.IdDataType));
                return attributesData;
            }
            else
                return new List<OpIssueData>();
        }

        #endregion

        #region Save

        public static OpIssue Save(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, false, true, smsNotification, useDBCollector);
        }

        public static OpIssue Save(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave, bool finishRelatedTasks, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, null, finishRelatedTasks, true, changeRelatedLocatioState: false, newLocationStateType: null, smsNotification: smsNotification, useDBCollector: useDBCollector);
        }

        public static OpIssue Save(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave, bool finishRelatedTasks, bool emailNotification, bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, null, finishRelatedTasks, emailNotification, changeRelatedLocatioState: false, newLocationStateType: null, smsNotification: smsNotification, useDBCollector: useDBCollector);
        }

        #region Save with finish related tasks prompt
        public delegate bool AskIfFinishRelatedTasksDelegate(DataProvider dataProvider, OpIssue issue, string question, bool multiple, out bool cancel, out bool applyForAll);
        public delegate bool AskIfChangeRelatedLocationState(DataProvider dataProvider, OpIssue issue, OpOperator loggedOperator, bool askIfApplyToAll, out OpLocationStateType newLocationState, out bool cancel, out bool applyToAll);
        public delegate void SendJCIDelegate(OpIssue issue, OpOperator loggedOperator, bool issueStatusChanged);

        public static OpIssue Save(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave,
            AskIfFinishRelatedTasksDelegate askIfFinishRelatedTasks, string customQuestionBase = null, SendJCIDelegate sendJCI = null, bool emailNoticifation = true,
            AskIfChangeRelatedLocationState askIfChangeRelatedLocationState = null,
            bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, null, askIfFinishRelatedTasks, customQuestionBase, sendJCI, emailNoticifation, askIfChangeRelatedLocationState, changeRelatedLocationStateInitial: false, newLocationStateInitial: null, smsNotification: smsNotification, useDBCollector: useDBCollector);
        }

        public static OpIssue Save(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave, string note,
            AskIfFinishRelatedTasksDelegate askIfFinishRelatedTasks, string customQuestionBase = null, SendJCIDelegate sendJCI = null, bool emailNoticifation = true,
            AskIfChangeRelatedLocationState askIfChangeRelatedLocationState = null, bool changeRelatedLocationStateInitial = false, OpLocationStateType newLocationStateInitial = null,
            bool smsNotification = false, bool useDBCollector = false)
        {
            bool obsolete;
            bool cancel = false;
            bool issueStatusChanged = objectToSave.IssueStatusChanged;

            bool finishRelatedTasks = false;
            if (askIfFinishRelatedTasks != null)
                finishRelatedTasks = askIfFinishRelatedTasks(dataProvider, objectToSave, customQuestionBase, false, out cancel, out obsolete);
            if (cancel)
                return objectToSave;
            bool changeRelatedLocationState = changeRelatedLocationStateInitial;//false;
            bool applyLocationStateChangeToAll = false;
            OpLocationStateType newLocationState = newLocationStateInitial;
            if (askIfChangeRelatedLocationState != null && issueStatusChanged)
            {
                changeRelatedLocationState = askIfChangeRelatedLocationState(dataProvider, objectToSave, loggedOperator, false, out newLocationState, out cancel, out applyLocationStateChangeToAll);
                if (cancel)
                    return objectToSave;
            }

            OpIssue saved = Save(dataProvider, loggedOperator, objectToSave, note, finishRelatedTasks, emailNoticifation,
                                 changeRelatedLocatioState: changeRelatedLocationState, newLocationStateType: newLocationState,
                                 smsNotification: smsNotification, useDBCollector: useDBCollector);

            if (sendJCI != null)
                sendJCI(saved, loggedOperator, issueStatusChanged);

            return saved;
        }

        #region SaveMultiple
        public static List<OpIssue> SaveMultiple(DataProvider dataProvider, OpOperator loggedOperator, List<OpIssue> objectsToSave,
                                                 AskIfFinishRelatedTasksDelegate askIfFinishRelatedTasks, string customQuestionBase = null,
                                                 SendJCIDelegate sendJCI = null, bool emailNoticifation = true,
                                                 AskIfChangeRelatedLocationState askIfChangeRelatedLocationState = null,
                                                 bool smsNotification = false, bool useDBCollector = false)
        {
            return SaveMultiple(dataProvider, loggedOperator, objectsToSave, null, askIfFinishRelatedTasks, customQuestionBase,
                                sendJCI, emailNoticifation, askIfChangeRelatedLocationState, smsNotification, useDBCollector);
        }

        public static List<OpIssue> SaveMultiple(DataProvider dataProvider, OpOperator loggedOperator, List<OpIssue> objectsToSave, string note,
                                                 AskIfFinishRelatedTasksDelegate askIfFinishRelatedTasks, string customQuestionBase = null,
                                                 SendJCIDelegate sendJCI = null, bool emailNoticifation = true,
                                                 AskIfChangeRelatedLocationState askIfChangeRelatedLocationState = null,
                                                 bool smsNotification = false, bool useDBCollector = false)
        {
            bool applyToAll = false;
            bool cancel = false;
            bool[] finishRelatedTasks = new bool[objectsToSave.Count];
            bool[] changeRelatedLocationState = new bool[objectsToSave.Count];
            OpLocationStateType[] changeRelatedLocationStateStateType = new OpLocationStateType[objectsToSave.Count];

            for (int i = 0; i < objectsToSave.Count; i++)
            {
                if (objectsToSave[i].IdIssueStatus != (int)OpIssueStatus.Enum.FINISHED || objectsToSave[i].OpState == OpChangeState.Loaded)
                    continue;

                if (!applyToAll)
                {
                    string question = null;
                    if (customQuestionBase != null)
                        question = String.Format("{0} ({1})", customQuestionBase, objectsToSave[i]);

                    finishRelatedTasks[i] = askIfFinishRelatedTasks(dataProvider, objectsToSave[i], question, true, out cancel, out applyToAll);
                }
                else
                    finishRelatedTasks[i] = finishRelatedTasks[i - 1];

                if (cancel)
                    return objectsToSave;
            }

            applyToAll = false;
            cancel = false;
            //finishRelatedTasks = new bool[objectsToSave.Count];//bezsensowna linijka zamazująca efekty poprzedniej pętli
            for (int i = 0; i < objectsToSave.Count; i++)
            {
                if (objectsToSave[i].IdIssueStatus != (int)OpIssueStatus.Enum.CANCELLED || objectsToSave[i].OpState == OpChangeState.Loaded)
                    continue;

                if (!applyToAll)
                {
                    string question = null;
                    if (customQuestionBase != null)
                        question = String.Format("{0} ({1})", customQuestionBase, objectsToSave[i]);

                    finishRelatedTasks[i] = askIfFinishRelatedTasks(dataProvider, objectsToSave[i], question, true, out cancel, out applyToAll);
                }
                else
                    finishRelatedTasks[i] = finishRelatedTasks[i - 1];

                if (cancel)
                    return objectsToSave;
            }

            if (askIfChangeRelatedLocationState != null)
            {
                applyToAll = false;
                cancel = false;
                for (int i = 0; i < objectsToSave.Count; i++)
                {
                    if (!applyToAll)
                    {
                        OpLocationStateType newLocationState = null;
                        changeRelatedLocationState[i] = askIfChangeRelatedLocationState(dataProvider, objectsToSave[i], loggedOperator, true, out newLocationState, out cancel, out applyToAll);
                        changeRelatedLocationStateStateType[i] = newLocationState;
                    }
                    else
                    {
                        changeRelatedLocationState[i] = changeRelatedLocationState[i - 1];
                        changeRelatedLocationStateStateType[i] = changeRelatedLocationStateStateType[i - 1];
                    }

                    if (cancel)
                        return objectsToSave;
                }
            }

            List<OpIssue> saved = new List<OpIssue>(objectsToSave.Count);
            for (int i = 0; i < objectsToSave.Count; i++)
            {
                try
                {
                    bool issueStatusChanged = objectsToSave[i].IssueStatusChanged;
                    bool issueChangeRelatedLocationStateType = changeRelatedLocationState[i];
                    OpLocationStateType issueRelatedLocationStateType = changeRelatedLocationStateStateType[i];
                    saved.Add(Save(dataProvider, loggedOperator, objectsToSave[i], note, finishRelatedTasks[i], emailNoticifation,
                                    changeRelatedLocatioState: issueChangeRelatedLocationStateType,
                                    newLocationStateType: issueRelatedLocationStateType,
                                    smsNotification: smsNotification, useDBCollector: useDBCollector));
                    if (sendJCI != null)
                        sendJCI(objectsToSave[i], loggedOperator, issueStatusChanged);
                }
                catch (Exception ex)
                {
                    ex.Data.Add("ObjectCausingException", objectsToSave[i]);
                    throw ex;
                }
            }
            return saved;
        }
        #endregion
        #endregion

        public static OpIssue Save(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave, string note, bool finishRelatedTasks,
                                   bool changeRelatedLocatioState = false, OpLocationStateType newLocationStateType = null,
                                   bool smsNotification = false, bool useDBCollector = false)
        {
            return Save(dataProvider, loggedOperator, objectToSave, note, finishRelatedTasks, true, smsNotification, changeRelatedLocatioState, newLocationStateType, useDBCollector);
        }

        public static OpIssue Save(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave,
                                   string note, bool finishRelatedTasks, bool emailNotification, bool smsNotification,
                                   bool changeRelatedLocatioState = false, OpLocationStateType newLocationStateType = null,
                                   bool useDBCollector = false)
        {
            bool isNewItem = objectToSave.IdIssue == 0; //check if it's new issue
            bool dataListChanged = !objectToSave.DataList.All(w => w.OpState == OpChangeState.Loaded);
            bool saved = false;

            OpIssue originalIssue = null;
            if (objectToSave.IdIssue > 0)
            {
                originalIssue = dataProvider.GetIssue(objectToSave.IdIssue, true);
            }

            if (objectToSave.OpState == OpChangeState.Loaded && !dataListChanged && String.IsNullOrEmpty(note))
                return objectToSave;

            try
            {
                if (isNewItem || objectToSave.OpState == OpChangeState.Modified)
                {
                    //zapisz date realizacji (jesli nie jest ustawiona recznie) jeśli zamykamy zgłoszenie
                    if (objectToSave.IdIssueStatus == (int)OpIssueStatus.Enum.FINISHED && !objectToSave.RealizationDate.HasValue)
                        objectToSave.RealizationDate = dataProvider.DateTimeNow;

                    objectToSave.IdIssue = dataProvider.SaveIssue(objectToSave);
                    objectToSave.OpState = OpChangeState.Loaded;
                    saved = true;
                }

                //zapis do tabeli ISSUE_DATA
                if (isNewItem || dataListChanged)
                {
                    OpIssueData data = null;
                    for (int i = 0; i < objectToSave.DataList.Count; i++)
                    {
                        data = objectToSave.DataList[i];
                        data.IdIssue = objectToSave.IdIssue;
                        if (objectToSave.DataList[i].OpState == OpChangeState.Delete)
                        {
                            DataComponent.Delete(dataProvider, data);
                        }
                        //else if (isNewItem || dataProvider.GetDataType(data.IdDataType).IsEditable)
                        else if (isNewItem || (objectToSave.DataList[i].OpState == OpChangeState.Modified || objectToSave.DataList[i].OpState == OpChangeState.New))
                        {
                            //zapis do bazy tylko tych ktore sa IsEditable na true lub jesli obiekt ktory dodajemy jest nowym obiektem wiec dodajemy dane po raz pierwszy
                            objectToSave.DataList[i].IdData = DataComponent.Save(dataProvider, data);
                            objectToSave.DataList[i].OpState = OpChangeState.Loaded;
                        }
                    }
                    saved = true;
                }

                if (saved && LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.IssueAdded, LoggedOperator.Login, ServerCORE, objectToSave.IdIssue);
                        LogSuccess(EventID.Forms.IssueAdded, objectToSave.IdIssue);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.IssueSaved, LoggedOperator.Login, ServerCORE, objectToSave.IdIssue);
                        LogSuccess(EventID.Forms.IssueSaved, objectToSave.IdIssue);
                }
                if (saved && emailNotification)
                {
                    Dictionary<OpIssue, OpIssue> mail = new Dictionary<OpIssue, OpIssue>();
                    mail.Add(objectToSave, originalIssue);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, mail, isNewItem ? EmailComponent.OperationType.Add : EmailComponent.OperationType.Change);

                    IssueStatusChangeEmail(dataProvider, objectToSave, originalIssue, loggedOperator, 2, note);
                }
                if (saved && smsNotification)
                {
                    Dictionary<OpIssue, OpIssue> sms = new Dictionary<OpIssue, OpIssue>();
                    sms.Add(objectToSave, originalIssue);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, isNewItem ? SmsComponent.OperationType.Add : SmsComponent.OperationType.Change, useDBCollector);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents)
                {
                    if (isNewItem)
                        //IMRLog.AddToLog(Module, EventID.Forms.IssueAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.IssueAdditionError, ex.Message);
                    else
                        //IMRLog.AddToLog(Module, EventID.Forms.IssueSavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdIssue, ex.Message);
                        LogError(EventID.Forms.IssueSavingError, objectToSave.IdIssue, ex.Message);
                }
                throw ex;
            }

            //check for status change
            if (originalIssue == null || (originalIssue.IdIssueStatus != objectToSave.IdIssueStatus)
                || (originalIssue.Deadline != objectToSave.Deadline) ||
                (originalIssue.ShortDescr != objectToSave.ShortDescr) ||
                (originalIssue.Priority != objectToSave.Priority) ||
                !String.IsNullOrEmpty(note))
            {
                AddHistory(dataProvider, loggedOperator, objectToSave, note);

                //zamykamy powiazane zadania
                if ((objectToSave.IdIssueStatus == (int)OpIssueStatus.Enum.FINISHED || objectToSave.IdIssueStatus == (int)OpIssueStatus.Enum.CANCELLED) && finishRelatedTasks)
                {
                    foreach (var task in GetNotFinishedTasks(dataProvider, objectToSave.IdIssue))
                    {
                        if (objectToSave.IdIssueStatus == (int)OpIssueStatus.Enum.FINISHED)
                            task.TaskStatus = dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Finished_succesfully);
                        if (objectToSave.IdIssueStatus == (int)OpIssueStatus.Enum.CANCELLED)
                            task.TaskStatus = dataProvider.GetTaskStatus((int)OpTaskStatus.Enum.Canceled);
                        try
                        {
                            TaskComponent.Save(dataProvider, loggedOperator, task, smsNotification, useDBCollector);
                            if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskSaved, LoggedOperator.Login, ServerCORE, task.IdTask);
                                LogSuccess(EventID.Forms.TaskGroupSaved, task.IdTask);
                        }
                        catch (Exception ex)
                        {
                            if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.TaskSavingError, LoggedOperator.Login, ServerCORE, task.IdTask, ex.Message);
                                LogError(EventID.Forms.TaskSavingError, task.IdTask, ex.Message);
                            throw ex;
                        }
                    }
                }
            }

            //zmieniamy status powiązanej lokalziacji
            if (changeRelatedLocatioState && newLocationStateType != null)
            {
                ChangeRelatedLocationState(dataProvider, loggedOperator, objectToSave, newLocationStateType);
            }

            return objectToSave;
        }

        #endregion

        #region AddHistory

        public static void AddHistory(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave)
        {
            AddHistory(dataProvider, loggedOperator, objectToSave, null);
        }

        public static void AddHistory(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToSave, string note)
        {
            try
            {
                List<OpIssueHistory> history = GetHistory(dataProvider, objectToSave.IdIssue);
                if (history.Count > 0)
                {
                    history.Last().EndDate = dataProvider.DateTimeNow;
                    dataProvider.SaveIssueHistory(history.Last());
                }

                OpIssueHistory issueHistory = new OpIssueHistory();
                issueHistory.Issue = objectToSave;
                issueHistory.StartDate = dataProvider.DateTimeNow;
                issueHistory.IssueStatus = objectToSave.IssueStatus;
                issueHistory.Operator = loggedOperator;
                issueHistory.Notes = note;
                issueHistory.Deadline = objectToSave.Deadline;
                issueHistory.ShortDescr = objectToSave.ShortDescr;
                issueHistory.Priority = objectToSave.Priority;

                dataProvider.SaveIssueHistory(issueHistory);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.IssueHistorySaved, LoggedOperator.Login, ServerCORE, objectToSave.IdIssue);
                    LogSuccess(EventID.Forms.IssueHistorySaved, objectToSave.IdIssue);
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.IssueHistorySavingError, LoggedOperator.Login, ServerCORE, objectToSave.IdIssue, ex.Message);
                    LogError(EventID.Forms.IssueHistorySavingError, objectToSave.IdIssue, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region ChangeRelatedLocationState

        public static void ChangeRelatedLocationState(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, OpLocationStateType newLocationStateType)
        {
            bool saveInfoInDataTransfer = true;//na przyszłość zintegrowac z DBCollector
            int idOldLocationStateType = issue.Location.IdLocationStateType;

            List<OpLocationStateHistory> oldHistory = null;
            if (saveInfoInDataTransfer)
                oldHistory = dataProvider.GetLocationStateHistoryFilter(IdLocation: new long[] { issue.IdLocation.Value }, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted);

            issue.Location.LocationStateType = newLocationStateType;
            LocationComponent.Save(dataProvider, issue.Location);
            LocationComponent.AddStateHistory(dataProvider, issue.Location, loggedOperator.Login);

            #region DataTransfer

            if (saveInfoInDataTransfer)
            {
                OpLocation oldLocation = new OpLocation(issue.Location) { IdLocationStateType = idOldLocationStateType, Distributor = issue.Location.Distributor };
                OpLocation newLocation = new OpLocation(issue.Location) { IdLocationStateType = issue.Location.IdLocationStateType, Distributor = issue.Location.Distributor };
                List<OpLocationStateHistory> newHistory = dataProvider.GetLocationStateHistoryFilter(IdLocation: new long[] { issue.IdLocation.Value }, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted);

                Dictionary<long, OpLocation> lDict = new Dictionary<long, OpLocation>();
                lDict.Add(newLocation.IdLocation, issue.Location);

                DataTransferComponent.SaveDataTransferInfo(dataProvider, loggedOperator, System.Windows.Forms.Application.ProductName, new List<OpLocation>() { oldLocation }, new List<OpLocation>() { newLocation });
                DataTransferComponent.SaveDataTransferInfo(dataProvider, loggedOperator, System.Windows.Forms.Application.ProductName, oldHistory, newHistory, lDict);
            }

            #endregion

            LocationComponent.ChangeLocationStateTelemetricServer(dataProvider, issue.Location);
        }
        //public static OpIssue Clone(DataProvider dataProvider, OpOperator loggedOperator, OpIssue objectToClone)
        //{
        //    OpIssue newIssue = objectToClone.Clone(dataProvider);
        //    Save(dataProvider, loggedOperator, newIssue);

        //    return dataProvider.GetIssue(newIssue.IdIssue);
        //}

        #endregion

        #region Files
        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, string filePath)
        {
            return UploadFile(dataProvider, loggedOperator, issue, filePath, false);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, string filePath, bool useTestFolders)
        {
            return UploadFile(dataProvider, loggedOperator, issue, filePath, useTestFolders, false);
        }

        public static OpFile UploadFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, string filePath, bool useTestFolders, bool onlyFileUpdate, string fileDescription = null)
        {
            #region old

            //if (File.Exists(filePath))
            //{

            //    try
            //    {
            //        FTPClient ftpClient = new FTPClient();
            //        ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
            //        bool uploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/", useTestFolders ? "IssueTest" : "Issue", issue.IdIssue), filePath, true);
            //        if (!uploadSuccess)
            //        {
            //            if (LogCustomEvents)
            //                LogError(EventID.Forms.AttachmentAdditionError, "Upload file failed: Unknown error");
            //            return null;
            //        }
            //        string tmpDownloadPath = ftpClient.DownloadFile(Path.GetTempPath(), String.Format(@"{0}/{1}/", useTestFolders ? "IssueTest" : "Issue", issue.IdIssue), Path.GetFileName(filePath));
            //        if (File.Exists(tmpDownloadPath))
            //        {
            //            File.Delete(tmpDownloadPath);
            //            OpFile file = new OpFile();
            //            file.PhysicalFileName = Path.GetFileName(filePath);
            //            file.Size = new FileInfo(filePath).Length;
            //            file.InsertDate = dataProvider.DateTimeNow;
            //            file.FileBytes = new byte[1] { 0x00 };
            //            long idFile = dataProvider.SaveFile(file);
            //            if (idFile > 0)
            //            {
            //                issue.DataList.SetValue(DataType.ISSUE_ATTACHMENT, issue.DataList.GetNextIndex(DataType.ISSUE_ATTACHMENT), file.IdFile);
            //                Save(dataProvider, loggedOperator, issue);
            //                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdded, LoggedOperator.Login, ServerCORE, file.IdFile);
            //                    LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
            //                return file;
            //            }
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
            //            LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
            //        throw ex;
            //    }
            //}
            //return null;

            #endregion


            if (File.Exists(filePath))
            {
                bool uploadSuccess = false;

                try
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    uploadSuccess = ftpClient.UploadFile(String.Format(@"{0}/{1}/", useTestFolders ? "IssueTest" : "Issue", issue.IdIssue), filePath, true);
                    if (!uploadSuccess)
                    {
                        if (LogCustomEvents)
                            LogError(EventID.Forms.AttachmentAdditionError, "Upload file failed: Unknown error");
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents)
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                }

                try
                {
                    OpFile file = null;
                    if (onlyFileUpdate)
                    {
                        bool dataReeloaded = false;

                        if (!issue.DataList.Exists(d => d.IdDataType == DataType.ISSUE_ATTACHMENT && d.Value != null))
                        {
                            issue.DataList.RemoveAll(d => d.IdDataType.In(new long[] { DataType.ISSUE_ATTACHMENT }));
                            issue.DataList.AddRange(dataProvider.GetIssueDataFilter(IdIssue: new int[] { issue.IdIssue }, IdDataType: new long[] { DataType.ISSUE_ATTACHMENT }, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted));
                            dataReeloaded = true;
                        }

                        List<long> idFileList = new List<long>();
                        issue.DataList.Where(d => d.IdDataType == DataType.ISSUE_ATTACHMENT && d.Value != null).ToList().ForEach(t => { long idFile = 0; if (long.TryParse(t.Value.ToString(), out  idFile) && idFile > 0) idFileList.Add(idFile); });
                        OpFile fTmp = null;
                        if (idFileList.Count > 0)
                        {
                            List<OpFile> fList = dataProvider.GetFile(idFileList.ToArray());
                            fTmp = fList.Find(f => String.Equals(f.PhysicalFileName, Path.GetFileName(filePath)));
                            if (fTmp == null && !dataReeloaded)
                            {
                                issue.DataList.RemoveAll(d => d.IdDataType.In(new long[] { DataType.ISSUE_ATTACHMENT }));
                                issue.DataList.AddRange(dataProvider.GetIssueDataFilter(IdIssue: new int[] { issue.IdIssue }, IdDataType: new long[] { DataType.ISSUE_ATTACHMENT }, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted));
                                List<long> newIdFileList = new List<long>();
                                issue.DataList.Where(d => d.IdDataType == DataType.ISSUE_ATTACHMENT && d.Value != null).ToList().ForEach(t => { long idFile = 0; if (long.TryParse(t.Value.ToString(), out  idFile) && idFile > 0) newIdFileList.Add(idFile); });
                                newIdFileList = newIdFileList.Except(idFileList).ToList();
                                if (newIdFileList.Count > 0)
                                {
                                    fList.AddRange(dataProvider.GetFile(newIdFileList.ToArray()));
                                    fTmp = fList.Find(f => String.Equals(f.PhysicalFileName, Path.GetFileName(filePath)));
                                }
                            }
                        }

                        if (fTmp != null)
                        {
                            file = fTmp;
                            if (file == null)
                            {
                                dataProvider.GetFileContent(file);
                            }
                        }
                        else
                        {
                            file = new OpFile();
                            file.PhysicalFileName = Path.GetFileName(filePath);
                            file.Size = new FileInfo(filePath).Length;
                            file.InsertDate = dataProvider.DateTimeNow;
                            file.FileBytes = new byte[1] { 0x00 };
                        }
                    }

                    if (file == null)
                    {
                        file = new OpFile();
                        file.PhysicalFileName = Path.GetFileName(filePath);
                        file.Size = new FileInfo(filePath).Length;
                        file.InsertDate = dataProvider.DateTimeNow;
                        file.FileBytes = new byte[1] { 0x00 };
                    }

                    if (!uploadSuccess)
                    {
                        byte[] fileByteArray = File.ReadAllBytes(filePath);
                        if (fileByteArray != null)
                        {
                            file.FileBytes = fileByteArray;
                        }
                    }

                    bool isNewFile = file.IdFile == 0;

                    file.Description = fileDescription;

                    file.IdFile = dataProvider.SaveFile(file);

                    if (file.FileBytes != null)
                    {
                        dataProvider.SaveFileContent(file);
                    }

                    if (isNewFile)
                    {
                        int nextIndex = issue.DataList.GetNextIndex(DataType.ISSUE_ATTACHMENT);
                        issue.DataList.SetValue(DataType.ISSUE_ATTACHMENT, nextIndex, file.IdFile);
                        Save(dataProvider, loggedOperator, issue);

                        if (LogCustomEvents)
                        {
                            LogSuccess(EventID.Forms.AttachmentAdded, file.IdFile);
                        }
                    }

                    if (file.IdFile > 0)
                    {
                        return file;
                    }
                }
                catch (Exception ex)
                {
                    if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentAdditionError, LoggedOperator.Login, ServerCORE, ex.Message);
                        LogError(EventID.Forms.AttachmentAdditionError, ex.Message);
                    throw ex;
                }
            }

            return null;
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, OpFile file, string savePath)
        {
            return DownloadFile(dataProvider, loggedOperator, issue, file, savePath, false);
        }

        public static bool DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, OpFile file, string savePath, bool useTestFolders)
        {
            try
            {
                dataProvider.GetFileContent(file);
	            byte[] bytes = file.FileBytes as byte[];

                if (bytes == null || bytes.Length == 0 || bytes[0] == 0)
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    string filePathDownloaded = ftpClient.DownloadFile(savePath, String.Format(@"{0}/{1}/", useTestFolders ? "IssueTest" : "Issue", issue.IdIssue), file.PhysicalFileName);
                    if (filePathDownloaded != null)
                    {
                        file.LastDownloaded = dataProvider.DateTimeNow;
                        dataProvider.SaveFile(file);

                        return true;
                    }
                }
                else
                {
                    System.IO.File.WriteAllBytes(savePath + file.PhysicalFileName, bytes);

                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static byte[] DownloadFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, OpFile file, bool useTestFolders = false)
        {
            try
            {
                dataProvider.GetFileContent(file);
                byte[] result = file.FileBytes as byte[];

                if (result == null || result.Length == 0 || result[0] == 0)
                {
                    FTPClient ftpClient = new FTPClient();
                    ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                    result = ftpClient.DownloadFile(String.Format(@"{0}/{1}/", useTestFolders ? "IssueTest" : "Issue", issue.IdIssue), file.PhysicalFileName);

                    if (result != null)
                    {
                        file.LastDownloaded = dataProvider.DateTimeNow;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, OpFile file)
        {
            return DeleteFile(dataProvider, loggedOperator, issue, file, false);
        }

        public static bool DeleteFile(DataProvider dataProvider, OpOperator loggedOperator, OpIssue issue, OpFile file, bool useTestFolders)
        {
            try
            {
                FTPClient ftpClient = new FTPClient();
                ftpClient.Initialize(FTPSettings.Address, FTPSettings.UserName, FTPSettings.Password);
                ftpClient.DeleteFile(String.Format(@"{0}/{1}/", useTestFolders ? "IssueTest" : "Issue", issue.IdIssue), file.PhysicalFileName);

                int index = issue.DataList.First(d => d.IdDataType == DataType.ISSUE_ATTACHMENT && d.GetValue<long>() == file.IdFile).Index;
                DeleteData(dataProvider, issue, DataType.ISSUE_ATTACHMENT, index);
                issue.DataList.DeleteValue(DataType.ISSUE_ATTACHMENT, index);
                dataProvider.DeleteFile(file);
                Save(dataProvider, loggedOperator, issue);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.AttachmentDeleted, LoggedOperator.Login, ServerCORE, file.IdFile);
                    LogSuccess(EventID.Forms.AttachmentDeleted, file.IdFile);
                return true;
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.IssueDeleted, LoggedOperator.Login, ServerCORE, file.IdFile, ex.Message);
                    LogError(EventID.Forms.AttachmentDeletionError, file.IdFile, ex.Message);
                throw ex;
            }
        }
        #endregion

        #region Delete

        public static void DeleteHistory(DataProvider dataProvider, int idIssue)
        {
            foreach (var item in dataProvider.GetIssueHistoryFilter(IdIssue: new int[] { idIssue }))//.Where(ih => ih.IdIssue == idIssue))
            {
                dataProvider.DeleteIssueHistory(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, int idIssue)
        {
            foreach (var item in dataProvider.GetIssueDataFilter(IdIssue: new int[1] { idIssue }))
            {
                dataProvider.DeleteIssueData(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, OpIssue issue, long idDataType)
        {
            foreach (var item in dataProvider.GetIssueDataFilter(IdIssue: new int[1] { issue.IdIssue }, IdDataType: new long[1] { idDataType }))
            {
                dataProvider.DeleteIssueData(item);
            }
        }

        public static void DeleteData(DataProvider dataProvider, OpIssue issue, long idDataType, int index)
        {
            long idData = issue.DataList.First(d => d.IdDataType == idDataType && d.Index == index).IdData;
            if (idData > 0)
                dataProvider.DeleteIssueData(dataProvider.GetIssueData(idData));
        }

        public static void Delete(DataProvider dataProvider, OpIssue objectToDelete)
        {
            Delete(dataProvider, objectToDelete, true);
        }

        public static void Delete(DataProvider dataProvider, OpIssue objectToDelete, bool emailNotification, OpOperator loggedOperator = null)
        {
            Delete(dataProvider, objectToDelete, emailNotification, false, loggedOperator, useDBCollector: false);
        }

        public static void Delete(DataProvider dataProvider, OpIssue objectToDelete, bool emailNotification, bool smsNotification, OpOperator loggedOperator = null, bool useDBCollector = false)
        {
            try
            {
                DeleteHistory(dataProvider, objectToDelete.IdIssue);
                DeleteData(dataProvider, objectToDelete.IdIssue);
                dataProvider.DeleteIssue(objectToDelete);
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.IssueDeleted, LoggedOperator.Login, ServerCORE, objectToDelete.IdIssue);
                    LogSuccess(EventID.Forms.IssueDeleted, objectToDelete.IdIssue);
                if (emailNotification)
                {
                    Dictionary<OpIssue, OpIssue> email = new Dictionary<OpIssue, OpIssue>();
                    email.Add(objectToDelete, null);
                    EmailComponent.CreateAndSendEmail(dataProvider, loggedOperator, email, EmailComponent.OperationType.Delete);
                }
                if (smsNotification)
                {
                    Dictionary<OpIssue, OpIssue> sms = new Dictionary<OpIssue, OpIssue>();
                    sms.Add(objectToDelete, null);
                    SmsComponent.CreateAndSendSms(dataProvider, loggedOperator, sms, SmsComponent.OperationType.Delete, useDBCollector);
                }
            }
            catch (Exception ex)
            {
                if (LogCustomEvents) //IMRLog.AddToLog(Module, EventID.Forms.IssueDeletionError, LoggedOperator.Login, ServerCORE, objectToDelete.IdIssue, ex.Message);
                    LogError(EventID.Forms.IssueDeletionError, objectToDelete.IdIssue, ex.Message);
                throw ex;
            }
        }

        #endregion

        #region GetIssueDistributorNotificationOperators

        public static List<OpOperator> GetIssueDistributorNotificationOperators(DataProvider dataProvider, OpIssue issue, List<EmailComponent.OperationType> operationTypesToCheck)
        {
            List<OpOperator> mailingList = new List<OpOperator>();
            string customWhereClause = "[VALUE] is not null";
            customWhereClause += " AND (";
            customWhereClause += "(cast([VALUE] as nvarchar(max)) not like '%,%' AND cast([VALUE] as nvarchar(max)) like '" + issue.IdDistributor + "')";
            customWhereClause += " OR";
            customWhereClause += "(CAST(VALUE as nvarchar(max)) like '%,%' and (CAST(VALUE as nvarchar(max)) like '%," + issue.IdDistributor + ",%' OR CAST(VALUE as nvarchar(max)) like '%," + issue.IdDistributor + "' OR (CAST(VALUE as nvarchar(max)) like '" + issue.IdDistributor + ",%')))";
            customWhereClause += ")";
            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, IdDataType: new long[] { DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR }, customWhereClause: customWhereClause);
            if (odList != null && odList.Count > 0)
            {
                List<OpOperator> oList = dataProvider.GetOperator(odList.Select(d => d.IdOperator).Distinct().ToArray());
                foreach (OpOperator oItem in oList)
                {
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Add)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Add))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Change)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Change))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.Delete)
                        && oItem.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Delete))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == EmailComponent.OperationType.ChangeStatus)
                        && oItem.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IdIssueStatus)))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                }
            }
            mailingList.RemoveAll(o => o.Actor == null);
            mailingList.RemoveAll(o => String.IsNullOrEmpty(o.Actor.Email));
            mailingList = mailingList.Distinct().ToList();
            return mailingList;
        }

        #endregion

        #region GetIssueDistributorNotificationOperators

        public static List<OpOperator> GetIssueDistributorNotificationOperators(DataProvider dataProvider, OpIssue issue, List<SmsComponent.OperationType> operationTypesToCheck)
        {
            List<OpOperator> mailingList = new List<OpOperator>();
            string customWhereClause = "[VALUE] is not null";
            customWhereClause += " AND (";
            customWhereClause += "(cast([VALUE] as nvarchar(max)) not like '%,%' AND cast([VALUE] as nvarchar(max)) like '" + issue.IdDistributor + "')";
            customWhereClause += " OR";
            customWhereClause += "(CAST(VALUE as nvarchar(max)) like '%,%' and (CAST(VALUE as nvarchar(max)) like '%," + issue.IdDistributor + ",%' OR CAST(VALUE as nvarchar(max)) like '%," + issue.IdDistributor + "' OR (CAST(VALUE as nvarchar(max)) like '" + issue.IdDistributor + ",%')))";
            customWhereClause += ")";
            List<OpOperatorData> odList = dataProvider.GetOperatorDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: System.Data.IsolationLevel.ReadUncommitted, IdDataType: new long[] { DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR }, customWhereClause: customWhereClause);
            if (odList != null && odList.Count > 0)
            {
                List<OpOperator> oList = dataProvider.GetOperator(odList.Select(d => d.IdOperator).Distinct().ToArray());
                foreach (OpOperator oItem in oList)
                {
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Add)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Add))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Change)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Change))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.Delete)
                        && oItem.SmsNotificationOptions.GetNotify(SmsNotificationOptions.IssueOptions.Delete))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                    if (operationTypesToCheck.Exists(s => s == SmsComponent.OperationType.ChangeStatus)
                        && oItem.SmsNotificationOptions.GetNotify(Business.Objects.SmsNotificationOptions.StatusToOptions((OpIssueStatus.Enum)issue.IdIssueStatus)))
                    {
                        mailingList.Add(oItem);
                        continue;
                    }
                }
            }
            mailingList.RemoveAll(o => !o.SerialNbr.HasValue || o.DataList.FirstOrDefault(dt => dt.IdDataType == DataType.DEVICE_MOBILE_NETWORK_CODE) == null);
            mailingList = mailingList.Distinct().ToList();
            return mailingList;
        }

        #endregion

        #region CheckIssueAdditionalConstraints

        public static bool CheckIssueAdditionalConstraints(DataProvider dataProvider, OpIssue issue, OpOperator recipient, List<OpOperatorData> odList)
        {
            OpOperatorData odItem = null;
            if (odList != null && odList.Count > 0)
            {
                #region Distributor
                odItem = odList.Find(d => (d.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR ||
                                           d.IdDataType == DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE_ID_DISTRIBUTOR) && d.IdOperator == recipient.IdOperator);
                if (odItem != null && odItem.Value != null && !String.IsNullOrEmpty(odItem.Value.ToString()))
                {
                    List<int> idDistributors = odItem.Value.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Cast<int>().ToList();
                    if (idDistributors != null && !idDistributors.Exists(e => e.Equals(issue.IdDistributor)))
                        return false;
                }
                #endregion

                #region IssueType
                odItem = odList.Find(d => (d.IdDataType == DataType.OPERATOR_EMAIL_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE ||
                                           d.IdDataType == DataType.OPERATOR_SMS_NOTIFICATION_OPTIONS_ISSUE_ID_ISSUE_TYPE) && d.IdOperator == recipient.IdOperator);
                if (odItem != null && odItem.Value != null && !String.IsNullOrEmpty(odItem.Value.ToString()))
                {
                    List<int> idIssueType = odItem.Value.ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Cast<int>().ToList();
                    if (idIssueType != null && !idIssueType.Exists(e => e.Equals(issue.IdIssueType)))
                        return false;
                }
                #endregion
            }
            return true;
        }

        #endregion        

        #region IssueStatusChangeEmail
        public static void IssueStatusChangeEmail(DataProvider dataProvider, OpIssue IssueObject, OpIssue OriginalIssue, OpOperator LoggedOperator, int SelectedLanguage, string Note)
        {
            List<Tuple<OpOperator, string, string>> mailinglist = new List<Tuple<OpOperator, string, string>>();
            if (OriginalIssue == null || IssueObject.IdIssueStatus != OriginalIssue.IdIssueStatus || !String.IsNullOrEmpty(Note))
            {
                string baseIssueInfo = "";
                List<OpTask> tasksConnectedWithIssue = null;
                if (OriginalIssue == null || IssueObject.IdIssueStatus != OriginalIssue.IdIssueStatus)
                {
                    #region ChangeStatus
                    EmailComponent.HashedPassword = "F2xTCpfOaKFstdRLHwMsC5b02e2XSVsgDnkwhUMYI+lSSy+AzFmL7HvbBhwcsz8loN6G80sMXrqnd3jxgAXO2OQkPa3xppQUKJXbbjtiF4Gt3REIo8YcnCLzkgejGPEIyTYieWjPHIYz1WCAxKUAtbjeAqg3heKsB/GZ/bPTln8=";

                    #region Stare tworzenie baseIssueInfo
                    //baseIssueInfo = "ID: " + IssueObject.ID_ISSUE + "<br>" +
                    //                        ResourcesText.CreationDate + ": " + IssueObject.CREATION_DATE + "<br>" +
                    //                        ResourcesText.Type + ": " + ((IssueObject.IssueType != null && IssueObject.IssueType.Descr != null) ? IssueObject.IssueType.Descr.DESCRIPTION : string.Empty) + "<br>" +
                    //                        ResourcesText.Status + ": " + ((IssueObject.IssueStatus != null && IssueObject.IssueStatus.Descr != null) ? IssueObject.IssueStatus.Descr.DESCRIPTION : string.Empty) + "<br>" +
                    //                        ResourcesText.Registrant + ": " + IssueObject.OperatorRegistering.ToString() + "<br>" +
                    //                        ResourcesText.Performer + ": " + (IssueObject.OperatorPerformer != null ? IssueObject.OperatorPerformer.ToString() : "-") + "<br>" +
                    //                        ResourcesText.Priority + ": " + ((IssueObject.Priority != null && IssueObject.Priority.Descr != null) ? IssueObject.Priority.Descr.DESCRIPTION : string.Empty) + "<br>" +
                    //                        ResourcesText.Deadline + ": " + (IssueObject.Deadline.HasValue ? IssueObject.Deadline.Value.ToString() : "-") + "<br>" +
                    //                        ResourcesText.Location + ": " + (IssueObject.Location != null ? IssueObject.Location.CID : "-") + "<br>" +
                    //                        ResourcesText.Address + ": " + (IssueObject.Location != null ? IssueObject.Location.Address : "-") + "<br>" +
                    //                        ResourcesText.City + ": " + (IssueObject.Location != null ? IssueObject.Location.City : "-") + "<br>" +
                    //                        ResourcesText.Postcode + ": " + (IssueObject.Location != null ? IssueObject.Location.PostalCode : "-") + "<br>" +
                    //                        (IssueObject.Location != null ? IssueObject.Location.Latitude.ToString() : "") + " " +
                    //                        (IssueObject.Location != null ? IssueObject.Location.Longitude.ToString() : "") + "<br>" +
                    //                        ResourcesText.ContactPerson + ": " + ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.FullName != null) ? IssueObject.Location.Actor.FullName : "") + "<br>" +
                    //                        ResourcesText.ContactPersonPhones + ": " + ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.Phone != null) ? IssueObject.Location.Actor.Phone : "") + ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.Phone != null && IssueObject.Location.Actor.Mobile != null) ? "," : "") + ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.Mobile != null) ? IssueObject.Location.Actor.Mobile : "") + "<br>" +
                    //                        ResourcesText.Description + ": " + IssueObject.SHORT_DESCR + "<br>" +
                    //                        IssueObject.LONG_DESCR;
                    #endregion

                    #region Wersja StringBuilder
                    baseIssueInfo = new StringBuilder()
                        .AppendFormat("ID: {0}<br>", IssueObject.ID_ISSUE)
                        .AppendFormat("{0}: {1}<br>", ResourcesText.CreationDate, IssueObject.CREATION_DATE)
                        .AppendFormat("{0}: {1}<br>", ResourcesText.Type, ((IssueObject.IssueType != null && IssueObject.IssueType.Descr != null) ? IssueObject.IssueType.Descr.DESCRIPTION : string.Empty))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.Status, ((IssueObject.IssueStatus != null && IssueObject.IssueStatus.Descr != null) ? IssueObject.IssueStatus.Descr.DESCRIPTION : string.Empty))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.Registrant, (IssueObject.OperatorRegistering != null ? IssueObject.OperatorRegistering.ToString() : string.Empty))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.Performer, (IssueObject.OperatorPerformer != null ? IssueObject.OperatorPerformer.ToString() : "-"))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.Priority, ((IssueObject.Priority != null && IssueObject.Priority.Descr != null) ? IssueObject.Priority.Descr.DESCRIPTION : string.Empty))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.Deadline, (IssueObject.Deadline.HasValue ? IssueObject.Deadline.Value.ToString() : "-"))
                        .AppendFormat("{0}: {1} ({2})<br>", ResourcesText.Location, (IssueObject.Location != null ? IssueObject.Location.CID : "-"), (IssueObject.Location != null ? IssueObject.Location.Name : "-"))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.Address, (IssueObject.Location != null ? IssueObject.Location.Address : "-"))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.City, (IssueObject.Location != null ? IssueObject.Location.City : "-"))
                        .AppendFormat("{0}: {1}<br>{2}: {3} {4}: {5}<br>", ResourcesText.Postcode,
                                                               (IssueObject.Location != null ? IssueObject.Location.PostalCode : "-"),
                                                               ResourcesText.Latitude,
                                                               (IssueObject.Location != null ? IssueObject.Location.Latitude.ToString() : ""),
                                                               ResourcesText.Longitude,
                                                               (IssueObject.Location != null ? IssueObject.Location.Longitude.ToString() : ""))
                        .AppendFormat("{0}: {1}<br>", ResourcesText.ContactPerson, ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.FullName != null) ? IssueObject.Location.Actor.FullName : ""))
                        .AppendFormat("{0}: {1}{2}{3}<br>", ResourcesText.ContactPersonPhones,
                                                          ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.Phone != null) ? IssueObject.Location.Actor.Phone : ""),
                                                          ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.Phone != null && IssueObject.Location.Actor.Mobile != null) ? "," : ""),
                                                          ((IssueObject.Location != null && IssueObject.Location.Actor != null && IssueObject.Location.Actor.Mobile != null) ? IssueObject.Location.Actor.Mobile : ""))
                        .AppendFormat("{0}: {1}<br>{2}", ResourcesText.Description, IssueObject.SHORT_DESCR, IssueObject.LONG_DESCR)
                        .ToString();
                    #endregion

                    string changeStatus = "";
                    if (IssueObject.IdPrevoiusIssueStatus != 0)
                    {
                        List<OpIssueHistory> issueHistory = GetHistory(dataProvider, IssueObject.IdIssue);
                        if (issueHistory != null && issueHistory.Count > 0)
                        {
                            //changeStatus = "[" + issueHistory.Last<OpIssueHistory>().StartDate.ToString() + "] <b>" + String.Format(ResourcesText.IssueStatusChangedFromTo, dataProvider.GetDescr((long)dataProvider.GetIssueStatus(IssueObject.IdPrevoiusIssueStatus).IdDescr).Description,
                            //                                                                              dataProvider.GetDescr((long)dataProvider.GetIssueStatus(IssueObject.IdIssueStatus).IdDescr).Description) + "</b><br><br>";

                            OpIssueStatus prevIssueStatus = dataProvider.GetIssueStatus(IssueObject.IdPrevoiusIssueStatus);

                            string oldStatus = prevIssueStatus != null ? prevIssueStatus.ToString() : string.Empty;
                            string newStatus = IssueObject.IssueStatus != null ? IssueObject.IssueStatus.ToString() : string.Empty;

                            changeStatus = "[" + issueHistory.Last<OpIssueHistory>().StartDate.ToString() + "] <b>"
                                + String.Format(ResourcesText.IssueStatusChangedFromTo, oldStatus, newStatus)
                                + "</b><br><br>";
                        }
                    }
                    string message = changeStatus + "<b>" + ResourcesText.ChangedBy + ": </b>" +
                                        LoggedOperator.Actor.FullName + "<br>" +
                                        ResourcesText.Email + ": " + (LoggedOperator.Actor.Email != null && LoggedOperator.Actor.Email.Length == 0 ? "-" : LoggedOperator.Actor.Email) + "<br>" +
                                        ResourcesText.Mobile + ": " + (LoggedOperator.Actor.Mobile != null && LoggedOperator.Actor.Mobile.Length == 0 ? "-" : LoggedOperator.Actor.Mobile) + "<br>" +
                                        ResourcesText.Phone + ": " + (LoggedOperator.Actor.Phone != null && LoggedOperator.Actor.Phone.Length == 0 ? "-" : LoggedOperator.Actor.Phone) +
                                        "<br><br><b>" + ResourcesText.Notes + ":</b> " + Note +
                                        "<br><br><b>" + ResourcesText.InformationAboutIssue + "</b><br>" + baseIssueInfo;





                    if (IssueObject.OperatorRegistering != null && IssueObject.OperatorRegistering.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)IssueObject.IdIssueStatus)))
                        //SendIssueEmail(IssueObject.OperatorRegistering.Actor.Email,
                        //     ResourcesText.IssueStatusChangeTo + " " + IssueObject.IssueStatus.ToString(), message);
                        mailinglist.Add(new Tuple<OpOperator, string, string>(IssueObject.OperatorRegistering, ResourcesText.IssueStatusChangeTo + " " + IssueObject.IssueStatus.ToString(), message));
                    if (IssueObject.OperatorPerformer != null && (IssueObject.OperatorRegistering != null && IssueObject.IdOperatorRegistering != IssueObject.IdOperatorPerformer) &&
                        IssueObject.OperatorPerformer.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)IssueObject.IdIssueStatus)))
                        //SendIssueEmail(IssueObject.OperatorPerformer.Actor.Email,
                        //    ResourcesText.IssueStatusChangeTo + " " + IssueObject.IssueStatus.ToString(), message);
                        mailinglist.Add(new Tuple<OpOperator, string, string>(IssueObject.OperatorPerformer, ResourcesText.IssueStatusChangeTo + " " + IssueObject.IssueStatus.ToString(), message));

                    tasksConnectedWithIssue = dataProvider.GetTaskFilter(IdIssue: new int[] { IssueObject.IdIssue }, IdTaskStatus: new int[] { (int)OpTaskStatus.Enum.In_progress });
                    if (tasksConnectedWithIssue != null)
                    {//chcemy miec mozliwosc wyslania maili do sotow przypisanych do taskow z issue
                        tasksConnectedWithIssue = tasksConnectedWithIssue.Where(t => t.OperatorPerformer != null).ToList();
                        foreach (OpTask task in tasksConnectedWithIssue)
                        {
                            if ((IssueObject.OperatorRegistering != null && task.OperatorPerformer.IdOperator != IssueObject.OperatorRegistering.IdOperator) &&
                                       (IssueObject.OperatorPerformer != null && task.OperatorPerformer.IdOperator != IssueObject.OperatorPerformer.IdOperator))
                            {
                                if (task.OperatorPerformer.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.StatusToOptions((OpIssueStatus.Enum)IssueObject.IdIssueStatus)))
                                    //SendIssueEmail(task.OperatorPerformer.Actor.Email,
                                    //    ResourcesText.IssueStatusChangeTo + " " + IssueObject.IssueStatus.ToString(), message);
                                    mailinglist.Add(new Tuple<OpOperator, string, string>(task.OperatorPerformer, ResourcesText.IssueStatusChangeTo + " " + IssueObject.IssueStatus.ToString(), message));
                            }
                        }
                    }


                    if ((int)OpIssueStatus.Enum.REOPENED == IssueObject.IdIssueStatus)
                    {
                        #region Reopened

                        List<OpDistributorData> distributorProtectorOperatorId = dataProvider.GetDistributorDataFilter(IdDataType: new long[] { DataType.DISTRIBUTOR_PROTECTOR_OPERATOR_ID },
                            IdDistributor: new int[] { IssueObject.IdDistributor });
                        if (distributorProtectorOperatorId.Count > 0)
                        {
                            OpOperator distributorProtectorOperator = dataProvider.GetOperator(Convert.ToInt32(distributorProtectorOperatorId[0].Value));

                            if (distributorProtectorOperator != null)
                            {
                                if ((IssueObject.OperatorRegistering != null && distributorProtectorOperator.IdOperator != IssueObject.OperatorRegistering.IdOperator) &&
                                   (IssueObject.OperatorPerformer != null && distributorProtectorOperator.IdOperator != IssueObject.OperatorPerformer.IdOperator))
                                {
                                    //sprawdzenie czy operator ma wykonfigurowane że chce otrzymywac takie maile
                                    if (distributorProtectorOperator.EmailNotificationOptions.GetNotify(Business.Objects.EmailNotificationOptions.IssueOptions.Status_REOPENED))
                                    {
                                        //SendIssueEmail(distributorProtectorOperator.Actor.Email,
                                        //    ResourcesText.IssueStatusChangeToReopened, message);
                                        mailinglist.Add(new Tuple<OpOperator, string, string>(distributorProtectorOperator, ResourcesText.IssueStatusChangeToReopened, message));
                                        // return;
                                    }
                                }
                            }

                        }
                        #endregion
                    }
                    if (IssueObject.IdPrevoiusIssueStatus == (int)OpIssueStatus.Enum.SUSPENDED)
                    {
                        #region Suspended

                        List<OpDistributorData> distributorProtectorOperatorId = dataProvider.GetDistributorDataFilter(IdDataType: new long[] { DataType.DISTRIBUTOR_PROTECTOR_OPERATOR_ID },
                            IdDistributor: new int[] { IssueObject.IdDistributor });
                        if (distributorProtectorOperatorId.Count > 0)
                        {
                            OpOperator distributorProtectorOperator = dataProvider.GetOperator(Convert.ToInt32(distributorProtectorOperatorId[0].Value));
                            if (distributorProtectorOperator != null)
                            {
                                IMR.Suite.UI.Business.Objects.EmailNotificationOptions.IssueOptions issueOptionsStatus = 0;
                                #region ChooseCurrentIssueOptionStatus
                                switch (IssueObject.IdIssueStatus)
                                {
                                    case (int)OpIssueStatus.Enum.ACCEPTED:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_ACCEPTED;
                                        break;
                                    case (int)OpIssueStatus.Enum.FINISHED:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_FINISHED;
                                        break;
                                    case (int)OpIssueStatus.Enum.IN_PROGRESS:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_IN_PROGRESS;
                                        break;
                                    case (int)OpIssueStatus.Enum.NEW:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_NEW;
                                        break;
                                    case (int)OpIssueStatus.Enum.QUEUED:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_QUEUED;
                                        break;
                                    case (int)OpIssueStatus.Enum.REOPENED:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_REOPENED;
                                        break;
                                    case (int)OpIssueStatus.Enum.SUSPENDED:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_SUSPENDED;
                                        break;
                                    case (int)OpIssueStatus.Enum.WAITING_FOR_RESPONSE:
                                        issueOptionsStatus = Business.Objects.EmailNotificationOptions.IssueOptions.Status_WAITING_FOR_RESPONSE;
                                        break;
                                }
                                #endregion
                                if ((IssueObject.OperatorRegistering != null && distributorProtectorOperator.IdOperator != IssueObject.OperatorRegistering.IdOperator) &&
                                    (IssueObject.OperatorPerformer != null && distributorProtectorOperator.IdOperator != IssueObject.OperatorPerformer.IdOperator))
                                {
                                    if (distributorProtectorOperator.EmailNotificationOptions.GetNotify(issueOptionsStatus))
                                    {
                                        string subject = String.Format(ResourcesText.SuspendedIssueChangedStatus,
                                            dataProvider.GetDescrFilter(IdDescr: new long[] { (long)dataProvider.GetIssueStatus(IssueObject.IdIssueStatus).IdDescr },
                                            IdLanguage: new int[] { (int)SelectedLanguage })[0].Description);

                                        //SendIssueEmail(distributorProtectorOperator.Actor.Email, subject, message);
                                        mailinglist.Add(new Tuple<OpOperator, string, string>(distributorProtectorOperator, subject, message));
                                    }
                                }
                            }
                        }

                        #endregion
                    }

                    List<OpOperator> additionalOperators = IssueComponent.GetIssueDistributorNotificationOperators(dataProvider, IssueObject, new List<EmailComponent.OperationType>() { EmailComponent.OperationType.ChangeStatus });
                    additionalOperators.ForEach(o => mailinglist.Add(new Tuple<OpOperator, string, string>(o, ResourcesText.IssueStatusChangeTo + " " + IssueObject.IssueStatus.ToString(), message)));

                    #endregion
                }
                else
                {
                    if (!String.IsNullOrEmpty(Note))
                    {
                        #region ChangeNote
                        EmailComponent.HashedPassword = "F2xTCpfOaKFstdRLHwMsC5b02e2XSVsgDnkwhUMYI+lSSy+AzFmL7HvbBhwcsz8loN6G80sMXrqnd3jxgAXO2OQkPa3xppQUKJXbbjtiF4Gt3REIo8YcnCLzkgejGPEIyTYieWjPHIYz1WCAxKUAtbjeAqg3heKsB/GZ/bPTln8=";
                        baseIssueInfo = "ID: " + IssueObject.ID_ISSUE + "<br>" +
                                        ResourcesText.Location + ": " + (IssueObject.Location != null ? IssueObject.Location.CID : "-") + "<br>" +
                                        ResourcesText.Status + ": " + (IssueObject.IssueStatus.Descr == null ? IssueObject.IssueStatus.ToString() : IssueObject.IssueStatus.Descr.DESCRIPTION) + "<br>" +
                                        ResourcesText.NewNote + ": " + Note;

                        if (IssueObject.OperatorRegistering != null && IssueObject.OperatorRegistering.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Note_Change))
                            //SendIssueEmail(IssueObject.OperatorRegistering.Actor.Email,
                            //    ResourcesText.NewNoteRelatedWithIssue + " (" + IssueObject.Location.ToString() + ")", baseIssueInfo);
                            mailinglist.Add(new Tuple<OpOperator, string, string>(IssueObject.OperatorRegistering, ResourcesText.NewNoteRelatedWithIssue + " (" + (IssueObject.Location != null && !String.IsNullOrEmpty(IssueObject.Location.Name) ? (IssueObject.Location.Name + " ") : "") + (IssueObject.Location != null && !String.IsNullOrEmpty(IssueObject.Location.CID) ? IssueObject.Location.CID : "") + ")", baseIssueInfo));
                        if (IssueObject.OperatorPerformer != null && (IssueObject.OperatorRegistering != null && IssueObject.IdOperatorRegistering != IssueObject.IdOperatorPerformer) &&
                            IssueObject.OperatorPerformer.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Note_Change))
                            //SendIssueEmail(IssueObject.OperatorPerformer.Actor.Email,
                            //    ResourcesText.NewNoteRelatedWithIssue + " (" + IssueObject.Location.ToString() + ")", baseIssueInfo);
                            mailinglist.Add(new Tuple<OpOperator, string, string>(IssueObject.OperatorPerformer, ResourcesText.NewNoteRelatedWithIssue + " (" + (IssueObject.Location != null && !String.IsNullOrEmpty(IssueObject.Location.Name) ? (IssueObject.Location.Name + " ") : "") + (IssueObject.Location != null && !String.IsNullOrEmpty(IssueObject.Location.CID) ? IssueObject.Location.CID : "") + ")", baseIssueInfo));

                        tasksConnectedWithIssue = dataProvider.GetTaskFilter(IdIssue: new int[] { IssueObject.IdIssue });
                        if (tasksConnectedWithIssue != null)
                        {//chcemy miec mozliwosc wyslania maili do sotow przypisanych do taskow z issue
                            tasksConnectedWithIssue = tasksConnectedWithIssue.Where(t => t.OperatorPerformer != null).ToList();
                            foreach (OpTask task in tasksConnectedWithIssue)
                            {
                                if ((IssueObject.OperatorRegistering != null && task.OperatorPerformer.IdOperator != IssueObject.OperatorRegistering.IdOperator) &&
                                           (IssueObject.OperatorPerformer != null && task.OperatorPerformer.IdOperator != IssueObject.OperatorPerformer.IdOperator))
                                {
                                    if (task.OperatorPerformer.EmailNotificationOptions.GetNotify(EmailNotificationOptions.IssueOptions.Note_Change))
                                        //SendIssueEmail(task.OperatorPerformer.Actor.Email,
                                        //    ResourcesText.NewNoteRelatedWithIssue + " (" + ResourcesText.TaskRelatedWithIssue + ": " + task.Location.ToString() + ")", baseIssueInfo);
                                        mailinglist.Add(new Tuple<OpOperator, string, string>(task.OperatorPerformer, ResourcesText.NewNoteRelatedWithIssue + " (" + ResourcesText.TaskRelatedWithIssue + ": " + (task.Location != null && !String.IsNullOrEmpty(task.Location.Name) ? (task.Location.Name + " ") : "") + (task.Location != null && !String.IsNullOrEmpty(task.Location.CID) ? task.Location.CID : "") + ")", baseIssueInfo));
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            mailinglist.RemoveAll(t => t.Item1.Actor == null);
            mailinglist.RemoveAll(t => String.IsNullOrEmpty(t.Item1.Actor.Email));
            List<int> sentOperatorsId = new List<int>();
            foreach (Tuple<OpOperator, string, string> tItem in mailinglist)
            {
                if (!sentOperatorsId.Exists(o => o == tItem.Item1.IdOperator))
                {
                    SendIssueEmail(tItem.Item1.Actor.Email, tItem.Item2, tItem.Item3);
                    sentOperatorsId.Add(tItem.Item1.IdOperator);
                }
            }
        }

        private static void SendIssueEmail(string Email, string Subject, string Message)
        {
            if (!EmailComponent.SendEmail(Email, Subject, Message))
            {
                //ErrorMessageBox.ShowDialog(AssemblyWrapper.Title, ResourcesText.EmailSendFail);
                throw new Exception(ResourcesText.EmailSendFail);
            }
        }
        #endregion

        #region GetIssueByPermission

        public static List<OpIssue> GetIssueByPermission(DataProvider dataProvider, OpOperator loggedOperator,
            bool loadNavigationProperties = true, bool mergeIntoCache = false,
            int[] IdIssue = null, int[] IdActor = null, int[] IdOperatorRegistering = null, int[] IdOperatorPerformer = null,
            int[] IdIssueType = null, int[] IdIssueStatus = null,
            int[] IdDistributor = null,
            TypeDateTimeCode RealizationDate = null, TypeDateTimeCode CreationDate = null,
            string ShortDescr = null, string LongDescr = null,
            long[] IdLocation = null, TypeDateTimeCode Deadline = null, int[] IdPriority = null,
            long? topCount = null, string customWhereClause = null,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool loadIssueHistory = false,
            List<DW_OP.OpReport> allowedReport = null)
        {
            Dictionary<int, OpIssue> iDict = dataProvider.GetIssueFilter(loadNavigationProperties: loadNavigationProperties, mergeIntoCache: mergeIntoCache,
                                                              IdIssue: IdIssue, IdActor: IdActor, IdOperatorRegistering: IdOperatorRegistering, IdOperatorPerformer: IdOperatorPerformer,
                                                              IdIssueType: IdIssueType, IdIssueStatus: IdIssueStatus,
                                                              IdDistributor: IdDistributor,
                                                              RealizationDate: RealizationDate, CreationDate: CreationDate,
                                                              ShortDescr: ShortDescr, LongDescr: LongDescr,
                                                              IdLocation: IdLocation != null ? IdLocation : new List<long>().ToArray(),
                                                              Deadline: Deadline, IdPriority: IdPriority,
                                                              customWhereClause: customWhereClause,
                                                              autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout,
                                                              loadIssueHistory: loadIssueHistory).ToDictionary(d => d.IdIssue);
            List<int> idIssueToFilter = new List<int>();            

            if (LoggedOperator.Activities.Exists(a => a.IdActivity == Activity.ALLOWED_LOCATION)
                 && iDict.Values.ToList().Exists(i => i.IdLocation.HasValue))
            {
                #region ALLOWED_LOCATION
                //gdyby do procedury przekazac null i na jej poziomie odfiltrowac lokalziacje po LocationFilter, stracilibyśmy Issue nie związane z lokalizacją
                foreach (OpIssue iItem in iDict.Values.Where(i => i.IdLocation.HasValue))
                {
                    if (!loggedOperator.HasPermission(Activity.ALLOWED_LOCATION, iItem.IdLocation.Value))
                        idIssueToFilter.Add(iItem.IdIssue);
                }

                #endregion
            }
            if (LoggedOperator.Activities.Exists(a => a.IdActivity == Activity.ALLOWED_REPORT)
                && iDict.Values.ToList().Exists(i => i.IdReport.HasValue))
            {
                #region ALLOWED_REPORT

                Dictionary<int, List<int>> reportAssignedToIssueDict = new Dictionary<int, List<int>>(); // Key: IdReport, Value: IdIssue
                // iList = RoleComponent.FilterByPermission<OpIssue>(iList, "IdReport", Activity.ALLOWED_REPORT, loggedOperator);

                foreach (OpIssue iItem in iDict.Values.Where(i => i.IdIssueType == (int)OpIssueType.Enum.REPORT || i.IdReport.HasValue))
                {
                    if (iItem.DataList.Exists(d => d.IdDataType == DataType.ISSUE_REPORT_ID && d.Value != null))
                    {
                        if (!loggedOperator.HasPermission(Activity.ALLOWED_REPORT, iItem.IdReport.Value))
                            idIssueToFilter.Add(iItem.IdIssue);
                        else
                        {
                            if(!reportAssignedToIssueDict.ContainsKey(iItem.IdReport.Value))
                                reportAssignedToIssueDict.Add(iItem.IdReport.Value, new List<int>());
                            reportAssignedToIssueDict[iItem.IdReport.Value].Add(iItem.IdIssue);
                        }
                    }
                    else
                        idIssueToFilter.Add(iItem.IdIssue);
                }
                                
                if (reportAssignedToIssueDict != null && reportAssignedToIssueDict.Count > 0)
                {
                    Dictionary<int, DW_OP.OpReport> reportDict = allowedReport != null ? allowedReport.ToDictionary(d => d.IdReport) : null;
                    if (reportDict == null)
                        reportDict = dataProvider.GetReport(reportAssignedToIssueDict.Keys.ToArray()).ToDictionary(d => d.IdReport);

                    if (reportDict != null && reportDict.Count > 0)
                    {
                        foreach (KeyValuePair<int, List<int>> kvItem in reportAssignedToIssueDict)
                        {
                            if (!reportDict.ContainsKey(kvItem.Key))
                            {
                                foreach(int iItem in  kvItem.Value)
                                    iDict.Remove(iItem);
                            }
                        }
                    }
                    else
                        reportAssignedToIssueDict.Values.ToList().ForEach(d => d.ForEach(f => iDict.Remove(f)));
                }

                #endregion
            }
            if (LoggedOperator.Activities.Exists(a => a.IdActivity == Activity.ALLOWED_IMR_SERVER)
                && iDict.Values.ToList().Exists(i => i.IdImrServer.HasValue))
            {
                #region ALLOWED_IMR_SERVER

                //  iList = RoleComponent.FilterByPermission<OpIssue>(iList, "IdImrServer", Activity.ALLOWED_IMR_SERVER, loggedOperator);

                foreach (OpIssue iItem in iDict.Values.Where(i => i.IdIssueType.In(new int[] { (int)OpIssueType.Enum.SERVER, (int)OpIssueType.Enum.CLIENT_PORTAL })
                                                                    || i.IdImrServer.HasValue))
                {
                    if (iItem.DataList.Exists(d => d.IdDataType == DataType.ISSUE_SERVER_ID && d.Value != null))
                    {
                        if (!loggedOperator.HasPermission(Activity.ALLOWED_IMR_SERVER, iItem.IdImrServer.Value))
                            idIssueToFilter.Add(iItem.IdIssue);
                    }
                    else
                        idIssueToFilter.Add(iItem.IdIssue);
                }

                #endregion
            }
            if (idIssueToFilter != null && idIssueToFilter.Count > 0)
            {
                idIssueToFilter.ForEach(i => iDict.Remove(i));
            }
            
            return iDict.Values.ToList();
        }

        #endregion

        #region GetPossibleIssueStatusChangeList

        public static List<OpIssueStatus> GetPossibleIssueStatusChangeList(DataProvider dataProvider, OpOperator loggedOperator, List<OpIssueStatus> allowedIssueStatusEdit, List<OpIssueStatus> allowedIssueStatusChange, OpIssue issue)
        {
            OpIssueStatus currentIssueStatus = null;
            if (issue == null || issue.IdIssue == 0)
                currentIssueStatus = dataProvider.GetIssueStatus((int)OpIssueStatus.Enum.NEW);
            else
                currentIssueStatus = dataProvider.GetIssueStatus(issue.IdIssueStatus);

            List<OpIssueStatus> allowedIssueStatusList = new List<OpIssueStatus>();
            if (issue == null || issue.IdIssue == 0)
                allowedIssueStatusList.Add(currentIssueStatus);
            else
            {
                allowedIssueStatusList.AddRange(allowedIssueStatusEdit);
                if (!allowedIssueStatusList.Exists(t => t.IdIssueStatus == issue.IdIssueStatus))
                    allowedIssueStatusList.Add(currentIssueStatus);
            }

            List<OpIssueStatus> allowedIssueStatusChangeWorkflow = new List<OpIssueStatus>();

            if (loggedOperator != null && loggedOperator.Distributor != null && loggedOperator.Distributor.DataList.Exists(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_ISSUE_STATUS_FLOW_NEXT, DataType.DISTRIBUTOR_ISSUE_STATUS_FLOW_PREV })))
            {
                #region DataBaseConfiguration

                List<OpDistributorData> ddList = loggedOperator.Distributor.DataList.Where(d => d.IdDataType.In(new long[] { DataType.DISTRIBUTOR_ISSUE_STATUS_FLOW_NEXT, DataType.DISTRIBUTOR_ISSUE_STATUS_FLOW_PREV })).ToList();
                foreach (OpDistributorData ddItem in ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_ISSUE_STATUS_FLOW_NEXT))
                {
                    int? idPrevIssueStatus = null;
                    int? idNextIssueStatus = null;
                    if (ddItem.Value != null)
                    {
                        int idIssueStatusTmp = 0;
                        if (!int.TryParse(ddItem.Value.ToString(), out idIssueStatusTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idNextIssueStatus = idIssueStatusTmp;
                    }
                    OpDistributorData ddItemPrev = ddList.Where(d => d.IdDataType == DataType.DISTRIBUTOR_ISSUE_STATUS_FLOW_PREV && d.Index == ddItem.Index).FirstOrDefault();
                    if (ddItemPrev.Value != null)
                    {
                        int idIssueStatusTmp = 0;
                        if (!int.TryParse(ddItemPrev.Value.ToString(), out idIssueStatusTmp))
                            continue;//jak nie można sparsować to tej pary wogole nie analizujemy
                        idPrevIssueStatus = idIssueStatusTmp;
                    }
                    if (idPrevIssueStatus == currentIssueStatus.IdIssueStatus && idNextIssueStatus.HasValue)
                    {
                        if (dataProvider.GetIssueStatus(idPrevIssueStatus.Value) != null
                            && !allowedIssueStatusChangeWorkflow.Exists(d => d.IdIssueStatus == idPrevIssueStatus.Value))
                            allowedIssueStatusChangeWorkflow.Add(dataProvider.GetIssueStatus(idPrevIssueStatus.Value));//dodajemy obecny status żeby nie zamknąć sobie drogi jakiejkolwiek zmiany przy zmianach masowych
                        if (idNextIssueStatus != currentIssueStatus.IdIssueStatus
                            && dataProvider.GetIssueStatus(idNextIssueStatus.Value) != null
                            && !allowedIssueStatusChangeWorkflow.Exists(d => d.IdIssueStatus == idNextIssueStatus.Value))
                            allowedIssueStatusChangeWorkflow.Add(dataProvider.GetIssueStatus(idNextIssueStatus.Value));
                    }
                }

                #endregion
            }
            else
            {
                #region Default

                switch (currentIssueStatus.IdIssueStatus)
                {
                    default:
                        allowedIssueStatusChangeWorkflow.AddRange(dataProvider.GetIssueStatus(Enum.GetValues(typeof(OpIssueStatus.Enum)).Cast<int>().ToArray()));
                        break;
                }

                #endregion
            }

            allowedIssueStatusChangeWorkflow.RemoveAll(t => t == null);

            List<OpIssueStatus> isList = allowedIssueStatusList;
            isList = isList.Intersect(allowedIssueStatusChangeWorkflow).ToList();

            foreach (OpIssueStatus isItem in allowedIssueStatusChange)
            {
                if (allowedIssueStatusChangeWorkflow.Exists(t => t.IdIssueStatus == isItem.IdIssueStatus) && !isList.Exists(t => t.IdIssueStatus == isItem.IdIssueStatus))
                    isList.Add(isItem);
            }

            if (!isList.Exists(t => t.IdIssueStatus == currentIssueStatus.IdIssueStatus))
                isList.Add(currentIssueStatus);

            isList = isList.OrderBy(t => t.ComboBoxString).ToList();

            return isList;
        }

        #endregion

        #region SetHelperValues

        public static void SetHelperValues(DataProvider dataProvider, OpIssue issue, List<long> helpersToSet = null)
        {
            if (helpersToSet == null || helpersToSet.Count == 0)
                helpersToSet = DataType.GetHelperDataTypes(Enums.ReferenceType.IdIssue, false);
            foreach (long lItem in helpersToSet)
            {
                switch (lItem)
                {
                    case DataType.HELPER_ID_ISSUE:
                        issue.DataList.SetValue(DataType.HELPER_ID_ISSUE, 0, issue.ID_ISSUE);
                        break;
                    case DataType.HELPER_ID_ISSUE_TYPE:
                        issue.DataList.SetValue(DataType.HELPER_ID_ISSUE_TYPE, 0, issue.IdIssueType);
                        break;
                    case DataType.HELPER_ID_ISSUE_STATUS:
                        issue.DataList.SetValue(DataType.HELPER_ID_ISSUE_STATUS, 0, issue.IdIssueStatus);
                        break;
                    case DataType.HELPER_ID_DISTRIBUTOR:
                        issue.DataList.SetValue(DataType.HELPER_ID_DISTRIBUTOR, 0, issue.IdDistributor);
                        break;
                    case DataType.HELPER_REALIZATION_DATE:
                        issue.DataList.SetValue(DataType.HELPER_REALIZATION_DATE, 0, issue.RealizationDate);
                        break;
                    case DataType.HELPER_SHORT_DESCR:
                        issue.DataList.SetValue(DataType.HELPER_SHORT_DESCR, 0, issue.ShortDescr);
                        break;
                    case DataType.HELPER_LONG_DESCR:
                        issue.DataList.SetValue(DataType.HELPER_LONG_DESCR, 0, issue.LongDescr);
                        break;
                    case DataType.HELPER_ID_ACTOR:
                        issue.DataList.SetValue(DataType.HELPER_ID_ACTOR, 0, issue.IdActor);
                        break;
                    case DataType.HELPER_ID_OPERATOR_REGISTERING:
                        issue.DataList.SetValue(DataType.HELPER_ID_OPERATOR_REGISTERING, 0, issue.IdOperatorRegistering);
                        break;
                    case DataType.HELPER_ID_OPERATOR_PERFORMER:
                        issue.DataList.SetValue(DataType.HELPER_ID_OPERATOR_PERFORMER, 0, issue.IdOperatorPerformer);
                        break;
                    case DataType.HELPER_CREATION_DATE:
                        issue.DataList.SetValue(DataType.HELPER_CREATION_DATE, 0, issue.CreationDate);
                        break;
                    case DataType.HELPER_ID_LOCATION:
                        issue.DataList.SetValue(DataType.HELPER_ID_LOCATION, 0, issue.IdLocation);
                        break;
                    case DataType.HELPER_DEADLINE:
                        issue.DataList.SetValue(DataType.HELPER_DEADLINE, 0, issue.Deadline);
                        break;
                    case DataType.HELPER_PRIORITY:
                        issue.DataList.SetValue(DataType.HELPER_PRIORITY, 0, issue.IdPriority);
                        break;
                    default:
                        break;
                }
            }
            issue.DataList.ForEach(d => { if (d.DataType == null) d.DataType = dataProvider.GetDataType(d.IdDataType); });
        }

        #endregion

        #endregion
    }
}
