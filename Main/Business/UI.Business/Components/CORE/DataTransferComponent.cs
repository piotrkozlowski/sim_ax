﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.CORE;
using System.Reflection;
using IMR.Suite.Data.DB;

namespace IMR.Suite.UI.Business.Components.CORE
{
    public class DataTransferComponent
    {
        #region Methods

        #region InsertDataTransferBatch

        public static void InsertDataTransferBatch(DataProvider dataProvider, OpOperator loggedOperator,
                                                   Enums.ImrServerTableSynchronizationDirection direction, Enums.DataChange changeType,
                                                   Enums.Tables table,
                                                   int? idSourceServer, int? idDestinationServer,
                                                   string applicationName,
                                                   List<Tuple<object, object>> batch)
        {
            if (batch != null)
            {
                List<Tuple<List<Tuple<string, long?>>, Tuple<object, object>>> universalBatch = new List<Tuple<List<Tuple<string, long?>>, Tuple<object, object>>>();
                foreach (Tuple<object, object> tItem in batch)
                {
                    Type batchObjectType = null;
                    List<Tuple<string, long?>> keyList = new List<Tuple<string, long?>>();
                    if (tItem.Item1 != null)
                        batchObjectType = tItem.Item1.GetType();
                    if (batchObjectType == null && tItem.Item2 != null)
                        batchObjectType = tItem.Item2.GetType();
                    if (batchObjectType == typeof(OpData))
                    {
                        #region OpData

                        OpData oldItem = tItem.Item1 as OpData;
                        OpData newItem = tItem.Item2 as OpData;

                        keyList.Add(new Tuple<string, long?>("SERIAL_NBR", (oldItem != null ? (oldItem.SerialNbr.HasValue ? (long?)oldItem.SerialNbr.Value : null) : (newItem.SerialNbr.HasValue ? (long?)newItem.SerialNbr.Value : null))));
                        keyList.Add(new Tuple<string, long?>("ID_LOCATION", (oldItem != null ? (oldItem.IdLocation.HasValue ? (long?)oldItem.IdLocation.Value : null) : (newItem.IdLocation.HasValue ? (long?)newItem.IdLocation.Value : null))));
                        keyList.Add(new Tuple<string, long?>("ID_METER", (oldItem != null ? (oldItem.IdMeter.HasValue ? (long?)oldItem.IdMeter.Value : null) : (newItem.IdMeter.HasValue ? (long?)newItem.IdMeter.Value : null))));

                        keyList.Add(new Tuple<string, long?>("ID_DATA_TYPE", (oldItem != null ? (long?)oldItem.IdDataType : (long?)newItem.IdDataType)));
                        keyList.Add(new Tuple<string, long?>("INDEX_NBR", (oldItem != null ? (long?)oldItem.Index : (long?)newItem.Index)));
                        
                        #endregion
                    }
                    if (batchObjectType == typeof(OpDevice))
                    {
                        #region OpDevice

                        OpDevice oldItem = tItem.Item1 as OpDevice;
                        OpDevice newItem = tItem.Item2 as OpDevice;

                        keyList.Add(new Tuple<string, long?>("SERIAL_NBR", (oldItem != null ? oldItem.SerialNbr : newItem.SerialNbr)));

                        #endregion
                    }
                    if (batchObjectType == typeof(OpDeviceHierarchy))
                    {
                        #region OpDeviceHierarchy

                        OpDeviceHierarchy oldItem = tItem.Item1 as OpDeviceHierarchy;
                        OpDeviceHierarchy newItem = tItem.Item2 as OpDeviceHierarchy;

                        keyList.Add(new Tuple<string, long?>("ID_DEVICE_HIERARCHY", (oldItem != null ? oldItem.IdDeviceHierarchy : newItem.IdDeviceHierarchy)));

                        #endregion
                    }
                    if (batchObjectType == typeof(OpDeviceStateHistory))
                    {
                        #region OpDeviceStateHistory

                        OpDeviceStateHistory oldItem = tItem.Item1 as OpDeviceStateHistory;
                        OpDeviceStateHistory newItem = tItem.Item2 as OpDeviceStateHistory;

                        keyList.Add(new Tuple<string, long?>("ID_DEVICE_STATE", (oldItem != null ? oldItem.IdDeviceState : newItem.IdDeviceState)));

                        #endregion
                    }
                    if (batchObjectType == typeof(OpLocation))
                    {
                        #region OpLocation

                        OpLocation oldItem = tItem.Item1 as OpLocation;
                        OpLocation newItem = tItem.Item2 as OpLocation;

                        keyList.Add(new Tuple<string, long?>("ID_LOCATION", (oldItem != null ? oldItem.IdLocation : newItem.IdLocation)));

                        #endregion
                    }
                    if (batchObjectType == typeof(OpLocationEquipment))
                    {
                        #region OpLocationEquipment

                        OpLocationEquipment oldItem = tItem.Item1 as OpLocationEquipment;
                        OpLocationEquipment newItem = tItem.Item2 as OpLocationEquipment;

                        keyList.Add(new Tuple<string, long?>("ID_LOCATION_EQUIPMENT", (oldItem != null ? oldItem.IdLocationEquipment : newItem.IdLocationEquipment)));

                        #endregion
                    }
                    if (batchObjectType == typeof(OpLocationStateHistory))
                    {
                        #region OpLocationStateHistory

                        OpLocationStateHistory oldItem = tItem.Item1 as OpLocationStateHistory;
                        OpLocationStateHistory newItem = tItem.Item2 as OpLocationStateHistory;

                        keyList.Add(new Tuple<string, long?>("ID_LOCATION_STATE", (oldItem != null ? oldItem.IdLocationState : newItem.IdLocationState)));

                        #endregion
                    }
                    if (batchObjectType == typeof(OpSimCard))
                    {
                        #region OpSimCard

                        OpSimCard oldItem = tItem.Item1 as OpSimCard;
                        OpSimCard newItem = tItem.Item2 as OpSimCard;

                        keyList.Add(new Tuple<string, long?>("ID_SIM_CARD", (oldItem != null ? oldItem.IdSimCard : newItem.IdSimCard)));

                        #endregion
                    }
                    universalBatch.Add(new Tuple<List<Tuple<string, long?>>, Tuple<object, object>>(keyList, tItem));
                }
                InsertDataTransferBatch(dataProvider, loggedOperator, direction, changeType, table, idSourceServer, idDestinationServer, applicationName, universalBatch);
            }
        }

        public static void InsertDataTransferBatch(DataProvider dataProvider, OpOperator loggedOperator,
                                                   Enums.ImrServerTableSynchronizationDirection direction, Enums.DataChange changeType, 
                                                   Enums.Tables table,
                                                   int? idSourceServer, int? idDestinationServer,
                                                   string applicationName,
                                                   List<Tuple<List<Tuple<string, long?>>, Tuple<object, object>>> batch)
        {
            if (batch != null && batch.Count > 0)
            {
                Dictionary<Tuple<long?, long?, long?, long?, long?, long?>, List<Tuple<string, object, object>>> batchDataTransfer = new Dictionary<Tuple<long?, long?, long?, long?, long?, long?>, List<Tuple<string, object, object>>>();
                //tItem.Item1 - kolejność kluczy w liście ma znaczenie!
                foreach (Tuple<List<Tuple<string, long?>>, Tuple<object, object>> tItem in batch)
                {
                    FieldInfo[] fiList = (tItem.Item2.Item1 != null ? tItem.Item2.Item1.GetType() : tItem.Item2.Item2.GetType()).GetFields();
                    foreach (FieldInfo fiItem in fiList)
                    {
                        if (tItem.Item1.Exists(k => String.Equals(fiItem.Name, k.Item1)))
                            continue;//nie dodajemy kluczy
                        object oldValue = null;
                        if(tItem.Item2.Item1 != null)
                            oldValue = fiItem.GetValue(tItem.Item2.Item1);
                        object newValue = fiItem.GetValue(tItem.Item2.Item2);
                        bool addData = false;
                        if ((oldValue != null && newValue == null)
                            || (oldValue == null && newValue != null))
                        {
                            addData = true;
                        }
                        else
                        {
                            if (oldValue != null && newValue != null)
                            {
                                if (!String.Equals(oldValue.ToString(), newValue.ToString()))
                                    addData = true;
                            }
                        }
                        if (addData)
                        {
                            Tuple<long?, long?, long?, long?, long?, long?> tKeyItem = new Tuple<long?, long?, long?, long?, long?, long?>
                                ((tItem.Item1.Count > 0 ? tItem.Item1[0].Item2 : null),
                                 (tItem.Item1.Count > 1 ? tItem.Item1[1].Item2 : null),
                                 (tItem.Item1.Count > 2 ? tItem.Item1[2].Item2 : null),
                                 (tItem.Item1.Count > 3 ? tItem.Item1[3].Item2 : null),
                                 (tItem.Item1.Count > 4 ? tItem.Item1[4].Item2 : null),
                                 (tItem.Item1.Count > 5 ? tItem.Item1[5].Item2 : null));
                            if (!batchDataTransfer.ContainsKey(tKeyItem))
                                batchDataTransfer.Add(tKeyItem, new List<Tuple<string, object, object>>());
                            batchDataTransfer[tKeyItem].Add(new Tuple<string, object, object>(fiItem.Name, oldValue, newValue));
                        }
                    }
                } 

                long batchId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);
                List<OpDataTransfer> dtList = new List<OpDataTransfer>();
                foreach (KeyValuePair<Tuple<long?, long?, long?, long?, long?, long?>, List<Tuple<string, object, object>>> kvItem in batchDataTransfer)
                {
                    foreach (Tuple<string, object, object> tItem in kvItem.Value)
                    {
                        object convertedOldValue = tItem.Item2;
                        object convertedNewValue = tItem.Item3;
                        if (convertedOldValue is DateTime)
                            convertedOldValue = dataProvider.ConvertTimeZoneToUtcTime(Convert.ToDateTime(convertedOldValue));
                        if (convertedNewValue is DateTime)
                            convertedNewValue = dataProvider.ConvertTimeZoneToUtcTime(Convert.ToDateTime(convertedNewValue));
                        dtList.Add(new OpDataTransfer()
                        {
                            BatchId = batchId,
                            ChangeType = (int)changeType,
                            ChangeDirection = (int)direction,
                            TableName = table.ToString(),
                            ColumnName = tItem.Item1,
                            IdSourceServer = idSourceServer,
                            IdDestinationServer = idDestinationServer,
                            OldValue = convertedOldValue,
                            NewValue = convertedNewValue,
                            User = loggedOperator.Login,
                            Application = applicationName,
                            Host = Environment.MachineName,

                            Key1 = kvItem.Key.Item1,
                            Key2 = kvItem.Key.Item2,
                            Key3 = kvItem.Key.Item3,
                            Key4 = kvItem.Key.Item4,
                            Key5 = kvItem.Key.Item5,
                            Key6 = kvItem.Key.Item6
                        });
                    }
                }
                foreach (OpDataTransfer dtItem in dtList)
                {
                    dtItem.InsertTime = dataProvider.DateTimeNow;
                    dataProvider.SaveDataTransfer(dtItem);
                }
            }
        }

        #endregion

        #region SaveDataTransferInfo

        public static void SaveDataTransferInfo(DataProvider dataProvider, OpOperator loggedOperator, string productName, List<OpDeviceStateHistory> oldHistory, List<OpDeviceStateHistory> newHistory, Dictionary<long, OpDevice> devices)
        {
            if (oldHistory == null)
                oldHistory = new List<OpDeviceStateHistory>();
            if (newHistory == null)
                newHistory = new List<OpDeviceStateHistory>();
            List<long> serialNbrList = new List<long>();
            serialNbrList.AddRange(newHistory.Select(h => h.SerialNbr).Distinct());
            serialNbrList.AddRange(oldHistory.Select(h => h.SerialNbr).Distinct());
            serialNbrList = serialNbrList.Distinct().ToList();
            Dictionary<long, List<OpDeviceStateHistory>> oldDict = new Dictionary<long, List<OpDeviceStateHistory>>();
            Dictionary<long, List<OpDeviceStateHistory>> newDict = new Dictionary<long, List<OpDeviceStateHistory>>();
            serialNbrList.ForEach(d => { oldDict.Add(d, new List<OpDeviceStateHistory>()); newDict.Add(d, new List<OpDeviceStateHistory>()); });
            newHistory.ForEach(d => newDict[d.SerialNbr].Add(d));
            oldHistory.Where(d => d.SerialNbr.In(serialNbrList.ToArray())).ToList().ForEach(d => oldDict[d.SerialNbr].Add(d));
            foreach (long lItem in serialNbrList)
            {
                List<OpDeviceStateHistory> newestInDBHistory = newDict[lItem].Except(oldDict[lItem], w => new { IdDeviceStateType = w.IdDeviceStateType, StartTime = w.StartTime }).ToList();
                List<OpDeviceStateHistory> closedRecordsHistory = new List<OpDeviceStateHistory>();
                if (oldDict[lItem].Exists(d => !d.EndTime.HasValue))
                    closedRecordsHistory.Add(newDict[lItem].Find(n => n.IdDeviceState == oldDict[lItem].Find(o => !o.EndTime.HasValue).IdDeviceState));

                int? idDestinationServer = (devices.ContainsKey(lItem) && devices[lItem].Distributor != null ? devices[lItem].Distributor.IdIMRServer : null);

                List<Tuple<object, object>> batch = new List<Tuple<object, object>>();
                if (closedRecordsHistory.Count > 0)
                {
                    closedRecordsHistory.ForEach(d => batch.Add(new Tuple<object, object>(new OpDeviceStateHistory((DB_DEVICE_STATE_HISTORY)d) { EndTime = null }, d)));
                    DataTransferComponent.InsertDataTransferBatch(dataProvider, loggedOperator,
                        Enums.ImrServerTableSynchronizationDirection.Upload, Enums.DataChange.Update, Enums.Tables.DEVICE_STATE_HISTORY,
                        null, idDestinationServer, productName, batch);

                }
                if (newestInDBHistory.Count > 0)
                {
                    batch = new List<Tuple<object, object>>();
                    newestInDBHistory.ForEach(d => batch.Add(new Tuple<object, object>(null, d)));
                    DataTransferComponent.InsertDataTransferBatch(dataProvider, loggedOperator,
                        Enums.ImrServerTableSynchronizationDirection.Upload, Enums.DataChange.Insert, Enums.Tables.DEVICE_STATE_HISTORY,
                        null, idDestinationServer, productName, batch);
                }
            }
        }
        public static void SaveDataTransferInfo(DataProvider dataProvider, OpOperator loggedOperator, string productName, List<OpLocationStateHistory> oldHistory, List<OpLocationStateHistory> newHistory, Dictionary<long, OpLocation> locations)
        {
            if (oldHistory == null)
                oldHistory = new List<OpLocationStateHistory>();
            if (newHistory == null)
                newHistory = new List<OpLocationStateHistory>();
            List<long> idLocationList = new List<long>();
            idLocationList.AddRange(newHistory.Select(h => h.IdLocation).Distinct());
            idLocationList.AddRange(oldHistory.Select(h => h.IdLocation).Distinct());
            idLocationList = idLocationList.Distinct().ToList();
            Dictionary<long, List<OpLocationStateHistory>> oldDict = new Dictionary<long, List<OpLocationStateHistory>>();
            Dictionary<long, List<OpLocationStateHistory>> newDict = new Dictionary<long, List<OpLocationStateHistory>>();
            idLocationList.ForEach(d => { oldDict.Add(d, new List<OpLocationStateHistory>()); newDict.Add(d, new List<OpLocationStateHistory>()); });
            newHistory.ForEach(d => newDict[d.IdLocation].Add(d));
            oldHistory.Where(d => d.IdLocation.In(idLocationList.ToArray())).ToList().ForEach(d => oldDict[d.IdLocation].Add(d));
            foreach (long lItem in idLocationList)
            {
                List<OpLocationStateHistory> newestInDBHistory = newDict[lItem].Except(oldDict[lItem], w => new { IdLocationState = w.IdLocationState, StartDate = w.StartDate }).ToList();
                List<OpLocationStateHistory> closedRecordsHistory = new List<OpLocationStateHistory>();
                if (oldDict[lItem].Exists(d => !d.EndDate.HasValue))
                    closedRecordsHistory.Add(newDict[lItem].Find(n => n.IdLocationState == oldDict[lItem].Find(o => !o.EndDate.HasValue).IdLocationState));

                int? idDestinationServer = (locations.ContainsKey(lItem) && locations[lItem].Distributor != null ? locations[lItem].Distributor.IdIMRServer : null);

                List<Tuple<object, object>> batch = new List<Tuple<object, object>>();
                if (closedRecordsHistory.Count > 0)
                {
                    closedRecordsHistory.ForEach(d => batch.Add(new Tuple<object, object>(new OpLocationStateHistory((DB_LOCATION_STATE_HISTORY)d) { EndDate = null }, d)));
                    DataTransferComponent.InsertDataTransferBatch(dataProvider, loggedOperator,
                        Enums.ImrServerTableSynchronizationDirection.Upload, Enums.DataChange.Update, Enums.Tables.LOCATION_STATE_HISTORY,
                        null, idDestinationServer, productName, batch);

                }
                if (newestInDBHistory.Count > 0)
                {
                    batch = new List<Tuple<object, object>>();
                    newestInDBHistory.ForEach(d => batch.Add(new Tuple<object, object>(null, d)));
                    DataTransferComponent.InsertDataTransferBatch(dataProvider, loggedOperator,
                        Enums.ImrServerTableSynchronizationDirection.Upload, Enums.DataChange.Insert, Enums.Tables.LOCATION_STATE_HISTORY,
                        null, idDestinationServer, productName, batch);
                }
            }
        }
        public static void SaveDataTransferInfo(DataProvider dataProvider, OpOperator loggedOperator, string productName, List<OpDevice> oldItems, List<OpDevice> newItems)
        {
            if (oldItems == null)
                oldItems = new List<OpDevice>();
            if (newItems == null)
                newItems = new List<OpDevice>();
            List<long> serialNbrList = new List<long>();
            serialNbrList.AddRange(newItems.Select(h => h.SerialNbr).Distinct());
            serialNbrList.AddRange(oldItems.Select(h => h.SerialNbr).Distinct());
            serialNbrList = serialNbrList.Distinct().ToList();

            foreach (long lItem in serialNbrList)
            {
                OpDevice newDevice = newItems.Find(d => d.SerialNbr == lItem);
                OpDevice oldDevice = oldItems.Find(d => d.SerialNbr == lItem);
                int? idDestinationServer = (newDevice.Distributor != null ? newDevice.Distributor.IdIMRServer : null);
                Enums.DataChange changeType = oldDevice != null ? Enums.DataChange.Update : Enums.DataChange.Insert;

                DataTransferComponent.InsertDataTransferBatch(dataProvider, loggedOperator,
                        Enums.ImrServerTableSynchronizationDirection.Upload, changeType, Enums.Tables.DEVICE,
                        null, idDestinationServer, productName, new List<Tuple<object, object>>() { new Tuple<object, object>(oldDevice, newDevice) });

            }
        }
        public static void SaveDataTransferInfo(DataProvider dataProvider, OpOperator loggedOperator, string productName, List<OpLocation> oldItems, List<OpLocation> newItems)
        {
            if (oldItems == null)
                oldItems = new List<OpLocation>();
            if (newItems == null)
                newItems = new List<OpLocation>();
            List<long> idLocationList = new List<long>();
            idLocationList.AddRange(newItems.Select(h => h.IdLocation).Distinct());
            idLocationList.AddRange(oldItems.Select(h => h.IdLocation).Distinct());
            idLocationList = idLocationList.Distinct().ToList();

            foreach (long lItem in idLocationList)
            {
                OpLocation newItem = newItems.Find(d => d.IdLocation == lItem);
                OpLocation oldItem = oldItems.Find(d => d.IdLocation == lItem);
                int? idDestinationServer = (newItem.Distributor != null ? newItem.Distributor.IdIMRServer : null);
                Enums.DataChange changeType = oldItem != null ? Enums.DataChange.Update : Enums.DataChange.Insert;

                DataTransferComponent.InsertDataTransferBatch(dataProvider, loggedOperator,
                        Enums.ImrServerTableSynchronizationDirection.Upload, changeType, Enums.Tables.LOCATION,
                        null, idDestinationServer, productName, new List<Tuple<object, object>>() { new Tuple<object, object>(oldItem, newItem) });

            }
        }
        public static void SaveDataTransferInfo(DataProvider dataProvider, OpOperator loggedOperator, string productName, List<OpData> oldItems, List<OpData> newItems, int? idIMRServer)
        {
            if (oldItems == null)
            {
                oldItems = new List<OpData>();
            }

            if (newItems == null)
            {
                newItems = new List<OpData>();
            }

            List<long> idDataList = new List<long>();
            idDataList.AddRange(newItems.Select(h => h.IdData).Distinct());
            idDataList.AddRange(oldItems.Select(h => h.IdData).Distinct());
            idDataList = idDataList.Distinct().ToList();

            foreach (long lItem in idDataList)
            {
                OpData newItem = newItems.Find(d => d.IdData == lItem);
                OpData oldItem = oldItems.Find(d => d.IdData == lItem);
                int? idDestinationServer = idIMRServer;
                Enums.DataChange changeType = oldItem != null ? Enums.DataChange.Update : Enums.DataChange.Insert;

                DataTransferComponent.InsertDataTransferBatch(dataProvider, loggedOperator,
                        Enums.ImrServerTableSynchronizationDirection.Upload, changeType, Enums.Tables.DATA,
                        null, idDestinationServer, productName, new List<Tuple<object, object>>() { new Tuple<object, object>(oldItem, newItem) });

            }
        }

        #endregion

        #region GetDataTypeClass

        public static Enums.DataTypeClass GetDataTypeClass(Type systemType)
        {
            if (systemType == typeof(int) || systemType == typeof(int?) || systemType == typeof(long) || systemType == typeof(long?))
                return Enums.DataTypeClass.Integer;
            else if (systemType == typeof(DateTime) || systemType == typeof(DateTime?))
                return Enums.DataTypeClass.Datetime;
            else if (systemType == typeof(bool) || systemType == typeof(bool?) || systemType == typeof(Boolean) || systemType == typeof(Boolean?))
                return Enums.DataTypeClass.Boolean;
            else if (systemType == typeof(double) || systemType == typeof(double?) || systemType == typeof(float) || systemType == typeof(float?))
                return Enums.DataTypeClass.Real;
            else if (systemType == typeof(decimal) || systemType == typeof(decimal?))
                return Enums.DataTypeClass.Decimal;
            else if (systemType == typeof(System.Drawing.Color))
                return Enums.DataTypeClass.Color;
            else if (systemType == typeof(byte[]))
                return Enums.DataTypeClass.Binary;
            else if (systemType == typeof(object))
                return Enums.DataTypeClass.Object;
            else if (systemType == typeof(string))
                return Enums.DataTypeClass.Text;
            return Enums.DataTypeClass.Unknown;
        }

        #endregion

        #endregion
    }
}
