﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;


namespace IMR.Suite.UI.Business
{
    class EventID
    {
        public class DataProvider
        {
            public static readonly LogData StoredProcedure = new LogData(21010, LogLevel.Error, "An error has occured in data provider layer. You may continue using application, however data may not be displayed correctly. Stored procedure, which failed to execute: '{0}'. More details: {1}");
            public static readonly LogData CustomWhereClauseOutOfRange = new LogData(21011, LogLevel.Error, "Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}");
            public static readonly LogData BulkSave = new LogData(21012, LogLevel.Error, "An error has occured in data provider layer. You may continue using application, however data may not be displayed correctly. Bulk save, which failed on table: '{0}'. More details: {1}");
            public static readonly LogData PreparedToExecuteTSql = new LogData(21013, LogLevel.Trace, "Prepared to  execute TSql. Operation: {0}, Statement: {1}.");
            public static readonly LogData DBCollectorClient = new LogData(21014, LogLevel.Error, "An error has occurred in data provider layer. DBCollector method '{0}' failed to execute. More details: {1}");
        }

        public class Forms
        {
            #region Custom Events
            #region Operator
            public static readonly LogData OperatorLoggedIn = new LogData(1000, LogLevel.Info, "Operator {0} logged in at {1}.");
            public static readonly LogData OperatorAdded = new LogData(1001, LogLevel.Info, "Operator has been added. Operator: {0}, Server: {1}, IdOperator(added): {2}.");
            public static readonly LogData OperatorAdditionError = new LogData(1002, LogLevel.Error, "Error during Operato additionr. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData OperatorSaved = new LogData(1003, LogLevel.Info, "Operator has been saved. Operator: {0}, Server: {1}, IdOperator(saved): {2}.");
            public static readonly LogData OperatorSavingError = new LogData(1004, LogLevel.Error, "Error during saving Operator. Operator: {0}, Server: {1}, IdOperator(to save): {2}, Error message: {3}.");
            public static readonly LogData OperatorDeleted = new LogData(1005, LogLevel.Info, "Operator has been deleted. Operator: {0}, Server: {1}, IdOperator(deleted): {2}.");
            public static readonly LogData OperatorDeletionError = new LogData(1006, LogLevel.Error, "Error during deleting Operator. Operator: {0}, Server: {1}, IdOperator(to delete): {2}, Error message: {3}.");
            #endregion
            #region Actor
            public static readonly LogData ActorAdded = new LogData(1101, LogLevel.Info, "Actor has been added. Operator: {0}, Server: {1}, IdActor: {2}.");
            public static readonly LogData ActorAdditionError = new LogData(1102, LogLevel.Error, "Error during Actor addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData ActorSaved = new LogData(1103, LogLevel.Info, "Actor has been saved. Operator: {0}, Server: {1}, IdActor: {2}.");
            public static readonly LogData ActorSavingError = new LogData(1104, LogLevel.Error, "Error during saving Actor. Operator: {0}, Server: {1}, IdActor: {2}, Error message: {3}.");
            public static readonly LogData ActorDeleted = new LogData(1105, LogLevel.Info, "Actor has been deleted. Operator: {0}, Server: {1}, IdActor: {2}.");
            public static readonly LogData ActorDeletionError = new LogData(1106, LogLevel.Error, "Error during deleting Actor. Operator: {0}, Server: {1}, IdActor: {2}, Error message: {3}.");
            #endregion
            #region Issue
            public static readonly LogData IssueAdded = new LogData(1201, LogLevel.Info, "Issue has been added. Operator: {0}, Server: {1}, IdIssue: {2}.");
            public static readonly LogData IssueAdditionError = new LogData(1202, LogLevel.Error, "Error during Issue addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData IssueSaved = new LogData(1203, LogLevel.Info, "Issue has been saved. Operator: {0}, Server: {1}, IdIssue: {2}.");
            public static readonly LogData IssueSavingError = new LogData(1204, LogLevel.Error, "Error during saving Issue. Operator: {0}, Server: {1}, IdIssue: {2}, Error message: {3}.");
            public static readonly LogData IssueDeleted = new LogData(1205, LogLevel.Info, "Issue has been deleted. Operator: {0}, Server: {1}, IdIssue: {2}.");
            public static readonly LogData IssueDeletionError = new LogData(1206, LogLevel.Error, "Error during deleting Issue. Operator: {0}, Server: {1}, IdIssue: {2}, Error message: {3}.");
            public static readonly LogData IssueHistorySaved = new LogData(1207, LogLevel.Info, "IssueHistory has been saved. Operator: {0}, Server: {1}, IdIssue: {2}.");
            public static readonly LogData IssueHistorySavingError = new LogData(1208, LogLevel.Error, "Error during saving IssueHistory. Operator: {0}, Server: {1}, IdIssue: {2}, Error message: {3}.");
            #endregion
            #region Task
            public static readonly LogData TaskAdded = new LogData(1301, LogLevel.Info, "Task has been added. Operator: {0}, Server: {1}, IdTask: {2}.");
            public static readonly LogData TaskAdditionError = new LogData(1302, LogLevel.Error, "Error during Task addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData TaskSaved = new LogData(1303, LogLevel.Info, "Task has been saved. Operator: {0}, Server: {1}, IdTask: {2}.");
            public static readonly LogData TaskSavingError = new LogData(1304, LogLevel.Error, "Error during saving Task. Operator: {0}, Server: {1}, IdTask: {2}, Error message: {3}.");
            public static readonly LogData TaskDeleted = new LogData(1305, LogLevel.Info, "Task has been deleted. Operator: {0}, Server: {1}, IdTask: {2}.");
            public static readonly LogData TaskDeletionError = new LogData(1306, LogLevel.Error, "Error during deleting Task. Operator: {0}, Server: {1}, IdTask: {2}, Error message: {3}.");
            public static readonly LogData TaskHistorySaved = new LogData(1307, LogLevel.Info, "TaskHistory has been saved. Operator: {0}, Server: {1}, IdTask: {2}.");
            public static readonly LogData TaskHistorySavingError = new LogData(1308, LogLevel.Error, "Error during saving TaskHistory. Operator: {0}, Server: {1}, IdTask: {2}, Error message: {3}.");
            public static readonly LogData TaskSuspendParams = new LogData(1309, LogLevel.Trace, "Task suspend params. LoggedOperator: {0}, IdTask: {1}, ExpectedReopenDate: {2}, Notes: {3}, SuspensionReason: {4}");
            public static readonly LogData TaskSuspendAutoIssueSuspensd = new LogData(1310, LogLevel.Trace, "Task suspend auto issue suspend for IdTask: {0}, IdIssue: {1}");
            public static readonly LogData TaskArrangedFitterVisitDateParams = new LogData(1311, LogLevel.Trace, "Task arranged fitter visit date params. LoggedOperator: {0}, IdTask: {1}, Date: {2}, BeginHour: {3}, EndHour: {4}, ScheduleMode: {5}, AppointmentResult: {6}, Note: {7}, TimeStamp: {8}");
            public static readonly LogData TaskArrangedFitterVisitDateFailureCount = new LogData(1312, LogLevel.Trace, "Task arranged fitter visit date failure count {0}.");

            #endregion
            #region Attachment
            public static readonly LogData AttachmentAdded = new LogData(1501, LogLevel.Info, "Attachment has been added. Operator: {0}, Server: {1}, IdAttachment: {2}.");
            public static readonly LogData AttachmentAdditionError = new LogData(1502, LogLevel.Error, "Error during Attachment addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData AttachmentDeleted = new LogData(1503, LogLevel.Info, "Attachment has been deleted. Operator: {0}, Server: {1}, IdAttachment: {2}.");
            public static readonly LogData AttachmentDeletionError = new LogData(1504, LogLevel.Error, "Error during deleting Attachment. Operator: {0}, Server: {1}, IdAttachment: {2}, Error message: {3}.");
            #endregion
            #region TaskGroup
            public static readonly LogData TaskGroupAdded = new LogData(1401, LogLevel.Info, "TaskGroup has been added. Operator: {0}, Server: {1}, IdTaskGroup: {2}.");
            public static readonly LogData TaskGroupAdditionError = new LogData(1402, LogLevel.Error, "Error during TaskGroup addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData TaskGroupSaved = new LogData(1403, LogLevel.Info, "TaskGroup has been saved. Operator: {0}, Server: {1}, IdTaskGroup: {2}.");
            public static readonly LogData TaskGroupSavingError = new LogData(1404, LogLevel.Error, "Error during saving TaskGroup. Operator: {0}, Server: {1}, IdTaskGroup: {2}, Error message: {3}.");
            public static readonly LogData TaskGroupDeleted = new LogData(1405, LogLevel.Info, "TaskGroup has been deleted. Operator: {0}, Server: {1}, IdTaskGroup: {2}.");
            public static readonly LogData TaskGroupDeletionError = new LogData(1406, LogLevel.Error, "Error during deleting TaskGroup. Operator: {0}, Server: {1}, IdTaskGroup: {2}, Error message: {3}.");
            #endregion
            #region WorkOrder
            public static readonly LogData WorkOrderAdded = new LogData(1601, LogLevel.Info, "WorkOrder has been added. Operator: {0}, Server: {1}, IdWorkOrder: {2}.");
            public static readonly LogData WorkOrderAdditionError = new LogData(1602, LogLevel.Error, "Error during WorkOrder addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData WorkOrderSaved = new LogData(1603, LogLevel.Info, "WorkOrder has been saved. Operator: {0}, Server: {1}, IdWorkOrder: {2}.");
            public static readonly LogData WorkOrderSavingError = new LogData(1604, LogLevel.Error, "Error during saving WorkOrder. Operator: {0}, Server: {1}, IdWorkOrder: {2}, Error message: {3}.");
            public static readonly LogData WorkOrderDeleted = new LogData(1605, LogLevel.Info, "WorkOrder has been deleted. Operator: {0}, Server: {1}, IdWorkOrder: {2}.");
            public static readonly LogData WorkOrderDeletionError = new LogData(1606, LogLevel.Error, "Error during deleting WorkOrder. Operator: {0}, Server: {1}, IdWorkOrder: {2}, Error message: {3}.");
            #endregion
            #region Package
            public static readonly LogData PackageAdded = new LogData(1701, LogLevel.Info, "Package has been added. Operator: {0}, Server: {1}, IdPackage: {2}.");
            public static readonly LogData PackageAdditionError = new LogData(1702, LogLevel.Error, "Error during Package addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData PackageSaved = new LogData(1703, LogLevel.Info, "Package has been saved. Operator: {0}, Server: {1}, IdPackage: {2}.");
            public static readonly LogData PackageSavingError = new LogData(1704, LogLevel.Error, "Error during saving Package. Operator: {0}, Server: {1}, IdPackage: {2}, Error message: {3}.");
            public static readonly LogData PackageDeleted = new LogData(1705, LogLevel.Info, "Package has been deleted. Operator: {0}, Server: {1}, IdPackage: {2}.");
            public static readonly LogData PackageDeletionError = new LogData(1706, LogLevel.Error, "Error during deleting Package. Operator: {0}, Server: {1}, IdPackage: {2}, Error message: {3}.");
            #endregion
            #region Location
            public static readonly LogData LocationAdded = new LogData(1801, LogLevel.Info, "Location has been added. Operator: {0}, Server: {1}, IdLocation: {2}.");
            public static readonly LogData LocationAdditionError = new LogData(1802, LogLevel.Error, "Error during Location addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData LocationSaved = new LogData(1803, LogLevel.Info, "Location has been saved. Operator: {0}, Server: {1}, IdLocation: {2}.");
            public static readonly LogData LocationSavingError = new LogData(1804, LogLevel.Error, "Error during saving Location. Operator: {0}, Server: {1}, IdLocation: {2}, Error message: {3}.");
            public static readonly LogData LocationDeleted = new LogData(1805, LogLevel.Info, "Location has been deleted. Operator: {0}, Server: {1}, IdLocation: {2}.");
            public static readonly LogData LocationDeletionError = new LogData(1806, LogLevel.Error, "Error during deleting Location. Operator: {0}, Server: {1}, IdLocation: {2}, Error message: {3}.");
            public static readonly LogData LocationHistorySaved = new LogData(1807, LogLevel.Info, "LocationHistory has been saved. Operator: {0}, Server: {1}, IdLocation: {2}.");
            public static readonly LogData LocationHistorySavingError = new LogData(1808, LogLevel.Error, "Error during saving LocationHistory. Operator: {0}, Server: {1}, IdLocation: {2}, Error message: {3}.");

            public static readonly LogData MissingLocationSchemaData = new LogData(1001609, LogLevel.Warn, "Missing location schema {0}. IdLocation: {1}, IdDataType: {2}, IndexNbr: {3}.");
            public static readonly LogData MissingLocationSchemaFile = new LogData(1001610, LogLevel.Warn, "Missing location schema {0} file. IdLocation: {1}, IdDataType: {2}, IndexNbr: {3}, IdFile: {4}.");
            public static readonly LogData LocationSchemaConfigurationDeserializationFailed = new LogData(1001611, LogLevel.Error, "Location schema configuration deserialization failed. IdLocation: {0}.");

            #endregion
            #region IMRServer
            public static readonly LogData IMRServerAdded = new LogData(1901, LogLevel.Info, "IMRServer has been added. Operator: {0}, Server: {1}, IdImrServer: {2}.");
            public static readonly LogData IMRServerAdditionError = new LogData(1902, LogLevel.Error, "Error during IMRServer addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData IMRServerSaved = new LogData(1903, LogLevel.Info, "IMRServer has been saved. Operator: {0}, Server: {1}, IdImrServer: {2}.");
            public static readonly LogData IMRServerSavingError = new LogData(1904, LogLevel.Error, "Error during saving IMRServer. Operator: {0}, Server: {1}, IdImrServer: {2}, Error message: {3}.");
            public static readonly LogData IMRServerDeleted = new LogData(1905, LogLevel.Info, "IMRServer has been deleted. Operator: {0}, Server: {1}, IdImrServer: {2}.");
            public static readonly LogData IMRServerDeletionError = new LogData(1906, LogLevel.Error, "Error during deleting IMRServer. Operator: {0}, Server: {1}, IdImrServer: {2}, Error message: {3}.");
            public static readonly LogData IMRServerIsDataBaseIMRSC = new LogData(1907, LogLevel.Error, "Default {0} value returned for IsDataBaseIMRSC.");
            public static readonly LogData IMRServerIsDataBaseIMRSCError = new LogData(1908, LogLevel.Error, "Error during IsDataBaseIMRSC. Error message: {0}.");
            public static readonly LogData IMRServerDataIsLocalhostNotSet = new LogData(1909, LogLevel.Error, "Parameter IMR_SERVER_IS_LOCALHOST is not set to True in any IMRServer.");
            public static readonly LogData IMRServerDataIsCoreIMRSCNotSet = new LogData(1910, LogLevel.Error, "Parameter IMR_SERVER_IS_CORE_IMRSC is not set in any IMRServer.");
            public static readonly LogData ImrServerHasNotDefinedDeploymentImrServer = new LogData(1911, LogLevel.Error, "IMR server {0}(Id: {1}) has not defined deployment IMR server");
            public static readonly LogData ImrServerHasNotDefinedDBCollectorConnection = new LogData(1912, LogLevel.Error, "IMR server {0}(Id: {1}) has not DBCollector connection");
            public static readonly LogData WcfLoginFailed = new LogData(1913, LogLevel.Error, "Wcf login failed on server: {0} (Id:{1}), msg: {2}");
            public static readonly LogData DistributorHasNotDefinedImrServer = new LogData(1914, LogLevel.Error, "Distributor {0}(Id: {1}) has not defined IMR server");
            public static readonly LogData GetServerReportException = new LogData(1915, LogLevel.Error, "Failed to get report server: {0}");
            public static readonly LogData SystemRunningActionEnabledNotSet = new LogData(1916, LogLevel.Error, "Parameter SYSTEM_RUNNING_ACTION_ENABLED is not set in SystemData, default {0} value returned.");
            public static readonly LogData SystemRunningActionEnabledError = new LogData(1917, LogLevel.Error, "Error during SystemRunningActionEnabled. Error message: {0}.");
            public static readonly LogData SystemRunningActionEnabledDefault = new LogData(1918, LogLevel.Error, "Default {0} value returned for SystemRunningActionEnabled.");
            #endregion
            #region Performance
            public static readonly LogData PerformanceIndicatorCreateDistributorPerformanceHierarchyStart = new LogData(2001, LogLevel.Debug, "Create distributor performance hierarchy start for distributor: {0}");
            public static readonly LogData PerformanceIndicatorCheckListCount = new LogData(2002, LogLevel.Trace, "List name: {0} Count: {1}");
            public static readonly LogData PerformanceIndicatorIsServerDistributor = new LogData(2003, LogLevel.Debug, "Is server distributor: {0}");
            #endregion
            #region LocationHierarchy
            
            public static readonly LogData ChangeLocationHierarchyParams = new LogData(2101, LogLevel.Debug, "Change location hierachy params. IdLocation: {0}, AssignedGroups: {1}, AllowGroupping: {2}.");

            #endregion
            #region ModuleData

            public static readonly LogData FTPEnabled = new LogData(2201, LogLevel.Debug, "FTP enabled. Module: {0}, result: {1}.");
            public static readonly LogData FTPAddress = new LogData(2202, LogLevel.Debug, "FTP address. Module: {0}, address: {1}.");
            public static readonly LogData FTPLogin = new LogData(2203, LogLevel.Debug, "FTP login. Module: {0}, login: {1}.");
            public static readonly LogData FTPInit = new LogData(2204, LogLevel.Debug, "FTP init. Address: {0}, login: {1}, test mode: {2}.");

            #endregion
            #region RoutePoint

            public static readonly LogData RoutePointDeleted = new LogData(2301, LogLevel.Info, "RoutePoint has been deleted. Operator: {0}, Server: {1}, IdRoutePoint: {2}.");
            public static readonly LogData RoutePointDeletionError = new LogData(2302, LogLevel.Error, "Error during deleting RoutePoint. Operator: {0}, Server: {1}, IdRoutePoint: {2}, Error message: {3}.");

            #endregion
            #region Login

            public static readonly LogData DBCollectorInitializedInfo = new LogData(2401, LogLevel.Info, "DBCollector initialized: {0}.");
            public static readonly LogData DBCollectorInitializeError = new LogData(2402, LogLevel.Fatal, "DBCollector initialize error. Msg: {0}, StackTrace: {1}.");
            public static readonly LogData DBCollectorClosed = new LogData(2403, LogLevel.Info, "DBCollector closed.");
            public static readonly LogData DBCollectorCloseError = new LogData(2404, LogLevel.Fatal, "DBCollector close error. Msg: {0}, StackTrace: {1}.");
            public static readonly LogData DBCollectorReconnectInfo = new LogData(2405, LogLevel.Warn, "DBCollector reconnect. Retry: {0}.");
            public static readonly LogData DBCollectorIsDBCollectorEnabledNull = new LogData(2406, LogLevel.Error, "Is DBCollector enabled returned <null>.");
            public static readonly LogData DBCollectorNotConnected = new LogData(2407, LogLevel.Error, "DBCollector not connected. Client instance: {0}, session instance: {1}, client state: {2}.");
            public static readonly LogData DBCollectorKeepAliveNotConnected = new LogData(2408, LogLevel.Error, "DBCollector not connected when trying to KeepAlive. Reconnect started.");
            public static readonly LogData DBCollectorTimerInfo = new LogData(2409, LogLevel.Info, "DBCollector timer: {0}.");
            #endregion
            #region DepositoryElement

            public static readonly LogData AssignToLocationError = new LogData(2501, LogLevel.Error, "Assign to location error. Details: {0}");
            public static readonly LogData AssignToLocationArticleNotValid = new LogData(2502, LogLevel.Error, "Assign to location error. Article not valid");
            public static readonly LogData AssignToLocationClosedElement = new LogData(2503, LogLevel.Error, "Assign to location error. Closed element");
            public static readonly LogData AssignToLocationWrongContextTask = new LogData(2504, LogLevel.Error, "Assign to location error. Wrong context (IdTask: {0})");
            public static readonly LogData AssignToLocationWrongContextPackage = new LogData(2505, LogLevel.Error, "Assign to location error. Wrong context (IdPackage: {0})");
            public static readonly LogData AssignToLocationNoDepository = new LogData(2506, LogLevel.Error, "Assign to location error. Depository not given");
            public static readonly LogData AssignToLocationCannotCreateArticle = new LogData(2507, LogLevel.Error, "Assign to location error. Cannot create article");
            public static readonly LogData AssignToLocationCannotCreateLocation = new LogData(2508, LogLevel.Error, "Assign to location error. Cannot create location");
            public static readonly LogData AssignToLocationIncosistentArticle = new LogData(2509, LogLevel.Error, "Assign to location error. Article (Id: {0}) properties (IsUnique: {1}), SerialNbr: {2}, ExtSn: {3}.");
            public static readonly LogData AssignToLocationExistingOnAnotherDepository = new LogData(2510, LogLevel.Error, "Assign to location error. Element (Sn: {0}, ExtSn: {1}) already exists at another depository (Ids: {2})");
            public static readonly LogData AssignToLocationExistingOnDepository = new LogData(2511, LogLevel.Error, "Assign to location error. Element (Sn: {0}, ExtSn: {1}) already exists at depository (Ids: {2})");
            public static readonly LogData AssignToLocationInfo = new LogData(2512, LogLevel.Info, "Assign element to location info. SerialNbr: {0}, ExtSn: {1}, IdLocation: {2}, IdArticle: {3}, StartDate: {4}");
            public static readonly LogData AssignToLocationReturnInfo = new LogData(2513, LogLevel.Info, "Assign element to location info. SerialNbr: {0}, ExtSn: {1}, IdLocation: {2}, IdArticle: {3}, StartDate: {4}, IdDepositoryElement: {5}");

            public static readonly LogData RemoveFromLocationError = new LogData(2521, LogLevel.Error, "Remove from location error. Details: {0}");
            public static readonly LogData RemoveFromLocationArticleNotValid = new LogData(2522, LogLevel.Error, "Remove from location error. Article not valid");
            public static readonly LogData RemoveFromLocationClosedElement = new LogData(2523, LogLevel.Error, "Remove from location error. Closed element");
            public static readonly LogData RemoveFromLocationWrongContextTask = new LogData(2524, LogLevel.Error, "Remove from location error. Wrong context (IdTask: {0})");
            public static readonly LogData RemoveFromLocationWrongContextPackage = new LogData(2525, LogLevel.Error, "Remove from location error. Wrong context (IdPackage: {0})");
            public static readonly LogData RemoveFromLocationNoDepository = new LogData(2526, LogLevel.Error, "Remove from location error. Depository not given");
            public static readonly LogData RemoveFromLocationCannotCreateArticle = new LogData(2527, LogLevel.Error, "Remove from to location error. Cannot create article");
            public static readonly LogData RemoveFromLocationCannotCreateLocation = new LogData(2528, LogLevel.Error, "Remove from location error. Cannot create location");
            public static readonly LogData RemoveFromLocationIncosistentArticle = new LogData(2529, LogLevel.Error, "Remove from location error. Article (Id: {0}) properties (IsUnique: {1}), SerialNbr: {2}, ExtSn: {3}.");
            public static readonly LogData RemoveFromLocationElementsToClose = new LogData(2530, LogLevel.Info, "Remove element from location info.  Elements to close count: {0} (IdDepositoryElement: {1})");
            public static readonly LogData RemoveFromLocationElementsClosed = new LogData(2531, LogLevel.Info, "Remove element from location info. Elements closed count: {0} (IdDepositoryElement: {1})");
            public static readonly LogData RemoveFromLocationInfo = new LogData(2532, LogLevel.Info, "Remove element from location info. SerialNbr: {0}, ExtSn: {1}, IdLocation: {2}, IdArticle: {3}, StartDate: {4}");

            #endregion
            #region Session
            public static readonly LogData SessionAdded = new LogData(2601, LogLevel.Info, "Session has been added. Operator: {0}, Server: {1}, IdSession: {2}.");
            public static readonly LogData SessionAdditionError = new LogData(2602, LogLevel.Error, "Error during Session addition. Operator: {0}, Server: {1}, Error message: {2}.");
            public static readonly LogData SessionSaved = new LogData(2603, LogLevel.Info, "Session has been saved. Operator: {0}, Server: {1}, IdSession: {2}.");
            public static readonly LogData SessionSavingError = new LogData(2604, LogLevel.Error, "Error during saving Session. Operator: {0}, Server: {1}, IdSession: {2}, Error message: {3}.");
            public static readonly LogData SessionDeleted = new LogData(2605, LogLevel.Info, "Session has been deleted. Operator: {0}, Server: {1}, IdSession: {2}.");
            public static readonly LogData SessionDeletionError = new LogData(2606, LogLevel.Error, "Error during deleting Session. Operator: {0}, Server: {1}, IdSession: {2}, Error message: {3}.");
            #endregion
            #region View
            public static readonly LogData ViewNotExistsInDB = new LogData(2701, LogLevel.Warn, "View {0} does not exist in DataBase.");
            public static readonly LogData ActiveViewToGet = new LogData(2702, LogLevel.Trace, "Active views to get: {0}.");
            #endregion
            #region Meter
            public static readonly LogData MeterEnterFunction = new LogData(2801, LogLevel.Info, "[{0}]-EnterFunction. Is IMRSC database:{1}");
            public static readonly LogData MeterExitFunction = new LogData(2802, LogLevel.Info, "[{0}]-Exit function. Time={1}[s]");
            public static readonly LogData MeterException = new LogData(2803, LogLevel.Fatal, "[{0}]-Message: {1}, StackTrace: {2}");
            public static readonly LogData MeterParametersInfo = new LogData(2804, LogLevel.Debug, "[{0}]-Parameters: ObjectReferenceType: {1}, SchemaReferenceType: {2}, IdObject(Count: {3}), IdObject: {4}.");
            public static readonly LogData MeterLoadedFromDBCount = new LogData(2805, LogLevel.Debug, "[{0}]-Loaded from DB: Dict(Count:{1}), LoadTime: {2}[s]. IdServer:{3}");
            public static readonly LogData MeterReturnValue = new LogData(2806, LogLevel.Info, "[{0}]-Return list: Schema(Count:{1})");
            public static readonly LogData MeterExistingCheck = new LogData(2807, LogLevel.Trace, "[{0}]-Variable: {1} - {2}");
            public static readonly LogData MeterParameterValues = new LogData(2808, LogLevel.Trace, "[{0}]-Parameter: {1}, Values: {2}");
            public static readonly LogData MeterExecutionExceeded = new LogData(2809, LogLevel.Warn, "[{0}]-Execution EXCEEDED: {1}");
            public static readonly LogData MeterMissingLocationSchemaData = new LogData(2810, LogLevel.Warn, "[{0}]-Missing location schema {1}. IdLocation: {2}, IdDataType: {3}, IndexNbr: {4}.");
            public static readonly LogData MeterMissingLocationSchemaFile = new LogData(2811, LogLevel.Warn, "[{0}]-Missing location schema {1} file. IdLocation: {2}, IdDataType: {3}, IndexNbr: {4}, IdFile: {5}.");
            public static readonly LogData MeterLoadedSchemaFromDB = new LogData(2812, LogLevel.Info, "[{0}]-Loaded {1} schema from DB (Bytes: {2}).");
            #endregion

            #region Common 2901 - 3000

            public static readonly LogData ParameterValues = new LogData(2901, LogLevel.Trace, "Parameter: {0}, Values: {1}");

            #endregion

            #endregion
        }

        public class Email
        {
            public static readonly LogData EmailSent = new LogData(2000, LogLevel.Info, "An Email has been successfully sent to: {0} ; subject: {1}");
            public static readonly LogData EmailSendingError = new LogData(2001, LogLevel.Error, "Error during sending an Email to: {0} ; subject: {1} ; Error message: {2}");
            public static readonly LogData ConstructingEmailError = new LogData(2002, LogLevel.Error, "Error during sending an Email to: {0} ; Error message: {1}");
            public static readonly LogData EmailSendingSMTPMail = new LogData(2003, LogLevel.Info, "Trying to send email by SMTP. Recipient: {0} ({1}), Subject: {2}, Attachment count: {3}, IsBodyHtml: {4}, Port: {5}, EnableSSL: {6}");
            public static readonly LogData EmailSendingDBMail = new LogData(2004, LogLevel.Info, "Trying to send email by DBMail. Recipient: {0} ({1}), Sender: {2}, Profile: {3}, Subject: {4}, IsAttachment: {5}, FileIds: {6}");
            public static readonly LogData EmailArguments = new LogData(2005, LogLevel.Info, "Email arguments: Mode: {0}, Attachments: {1}");
            public static readonly LogData EmailDBMailPreparingAttachments = new LogData(2006, LogLevel.Info, "Preparing DBMail attachments.");
            public static readonly LogData EmailDBMailAttachmentsPrepared = new LogData(2007, LogLevel.Info, "DBMail attachments prepared.");
        }

        public class Sms
        {
            public static readonly LogData SmsSendingError = new LogData(3000, LogLevel.Error, "Error during sending an Sms to: {0} ; Error message: {1}");
            public static readonly LogData TryingToSendSms = new LogData(3001, LogLevel.Info, "Sending an Sms to: {0} , Message: {1}");
        }

        public class Operator
        {
            public static readonly LogData TryingToMapOperator = new LogData(4000, LogLevel.Info, "Trying to map operator: {0} , Message: {1}");
        }
    }
}
