//#define BASE_DATA_ARCH

using IMR.Suite.UI.Business.Objects.DW;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using FP = IMR.Suite.Services.WEsbProvider.FP;

namespace IMR.Suite.UI.Business
{
    public partial class DataProviderFP
    {
        #region Refuel
        
        /*
         * Warto spr�bowa� pobra�, mo�e kto� przed chwil� u�y� GetRefuelFilter, kt�ry za�adowa� do cache
         * 
         * Nie ma senusu �adowa� wszystkich refueli z wszystkich dostaw.
         * Na szynie nie ma metod do pobierania tylko konkretnych refueli.
         * 
         * Metody te nie s� na razie nigdzie u�ywane.
         * */

        public override List<OpRefuel> GetAllRefuel()
        {
            return RefuelDict.Values.ToList();
        }
        public override OpRefuel GetRefuel(long Id)
        {
            OpRefuel refuel = null;
            RefuelDict.TryGetValue(Id, out refuel);
            return refuel;
        }
        public override List<OpRefuel> GetRefuel(long[] Ids)
        {
            List<OpRefuel> refuels = new List<OpRefuel>();
            if (RefuelDict.Count > 0)
            {
                foreach (var id in Ids)
                {
                    OpRefuel refuel = GetRefuel(id);
                    if (refuel != null) refuels.Add(refuel);
                }
            }
            return refuels;
        }
        public override OpRefuel GetRefuel(long Id, bool queryDatabase)
        {
            return GetRefuel(Id);
        }
        public override List<OpRefuel> GetRefuel(long[] Ids, bool queryDatabase)
        {
            return GetRefuel(Ids);
        }
        public override long SaveRefuel(OpRefuel toBeSaved)
        {
            throw new NotImplementedException("Saving refuel to fuel prime database is not supported!");
        }
        public override void DeleteRefuel(OpRefuel toBeDeleted)
        {
            throw new NotImplementedException("Deleting refuel from fuel prime database is not supported!");
        }
        
        #endregion

        #region DataTemporal
        /// <summary>
        /// Gets all DataTemporal objects
        /// </summary>
        public override List<OpDataTemporal> GetAllDataTemporal()
        {
            return base.GetAllDataTemporal();
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Id">DataTemporal Id</param>
        /// <returns>DataTemporal object</returns>
        public override OpDataTemporal GetDataTemporal(long Id)
        {
            return base.GetDataTemporal(Id);
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Ids">DataTemporal Ids</param>
        /// <returns>DataTemporal list</returns>
        public override List<OpDataTemporal> GetDataTemporal(long[] Ids)
        {
            return base.GetDataTemporal(Ids);
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Id">DataTemporal Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTemporal object</returns>
        public override OpDataTemporal GetDataTemporal(long Id, bool queryDatabase)
        {
            return base.GetDataTemporal(Id, queryDatabase);
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Ids">DataTemporal Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTemporal list</returns>
        public override List<OpDataTemporal> GetDataTemporal(long[] Ids, bool queryDatabase)
        {
            return base.GetDataTemporal(Ids, queryDatabase);
        }

        /// <summary>
        /// Gets DataTemporal list of GLTC Data for all meters.
        /// </summary>
        /// <returns>DataTemporal list</returns>
        public override List<OpDataTemporal> GetGLTCDataForAllMeters()
        {
            return base.GetGLTCDataForAllMeters();
        }

        /// <summary>
        /// Saves the DataTemporal object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTemporal to save</param>
        /// <returns>DataTemporal Id</returns>
        public override long SaveDataTemporal(OpDataTemporal toBeSaved)
        {
            if (toBeSaved == null) return -1;

            #region fuel_balance_sir2
            if (toBeSaved.IdDataType == Common.DataType.AWSR_IRC_SIR2_OFFSET || toBeSaved.IdDataType == Common.DataType.AWSR_IRC_SIR2_OUTLIER_THRESHOLD)
            {
                List<FP.Objects.FuelBalanceSir> fbSirList = WEsbFP.GetFuelBalanceSir2(new[] { toBeSaved.IdLocation.Value }, 
                    ConvertTimeZoneToUtcTime(toBeSaved.EndTime), ConvertTimeZoneToUtcTime(toBeSaved.EndTime));
                if (fbSirList != null)
                {
                    FP.Objects.FuelBalanceSir fbSir = fbSirList.FirstOrDefault(s => s.TankId == toBeSaved.IdMeter.Value);
                    if (fbSir != null)
                    {
                        if (toBeSaved.IdDataType == Common.DataType.AWSR_IRC_SIR2_OUTLIER_THRESHOLD)
                            fbSir.OutlierThreshold = toBeSaved.ValueDouble;
                        else if (toBeSaved.IdDataType == Common.DataType.AWSR_IRC_SIR2_OFFSET)
                            fbSir.Sir2Offset = toBeSaved.ValueDouble;

                        bool success = WEsbFP.SaveFuelBalancesSir(fbSir);
                        return success ? 0 : -1;
                    }
                }
            }
            #endregion

            #region data_temporal

            #endregion

            //return base.SaveDataTemporal(toBeSaved); // zakomentowane, zeby dla bezpieczenstwa nie zapisywac do bazy IMR obiektu DataTemporal pochodzacego z FP
            return -1;
        }

        /// <summary>
        /// Deletes the DataTemporal object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTemporal to delete</param>
        public override void DeleteDataTemporal(OpDataTemporal toBeDeleted)
        {
            #region data_temporal

            #endregion

            //base.DeleteDataTemporal(toBeDeleted); // zakomentowane, zeby dla bezpieczenstwa nie usuwac z bazy IMR obiektu DataTemporal pochodzacego z FP
        }        

        #endregion

        #region DataArch
        public override List<OpDataArch> DataArch
        {
            get
            {
                return GetAllDataArch();
            }
        }

        public override List<OpDataArch> GetAllDataArch()
        {
            return base.GetAllDataArch();
        }

        public override OpDataArch GetDataArch(long Id)
        {
            return base.GetDataArch(Id);
        }

        public override OpDataArch GetDataArch(long Id, bool queryDatabase)
        {
            return base.GetDataArch(Id, queryDatabase);
        }

        public override List<OpDataArch> GetDataArch(long[] Ids)
        {
            return base.GetDataArch(Ids);
        }

        public override List<OpDataArch> GetDataArch(long[] Ids, bool queryDatabase)
        {
            return base.GetDataArch(Ids, queryDatabase);
        }

        public override void DeleteDataArch(OpDataArch toBeDeleted)
        {
#if BASE_DATA_ARCH
            base.DeleteDataArch(toBeDeleted);
#else
            // base.DeleteDataArch(toBeDeleted); // zakomentowane, zeby dla bezpieczenstwa nie usuwac z bazy IMR obiektu DataArch pochodzacego z FP
#endif
        }
        #endregion
    }
}