//#define BASE_DATA_ARCH

using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;

using FP = IMR.Suite.Services.WEsbProvider.FP;
using Common = IMR.Suite.Common;
using IMR.Suite.UI.Business.Components.DW;
using IMR.Suite.UI.Business.Objects.CORE;
using OpDataArch = IMR.Suite.UI.Business.Objects.DW.OpDataArch;

namespace IMR.Suite.UI.Business
{
    public partial class DataProviderFP
    {
        #region Refuel
        #region GetRefuelFilter
        public override List<OpRefuel> GetRefuelFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdRefuel = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataSource = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
            long? topCount = null, string customWhereClause = null,
            List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            List<OpRefuel> refuelsFP = new List<OpRefuel>();

            #region Refuel start/end time
            DateTime? rangeStart = null, rangeEnd = null;
            DateTime? startTimeEqual = null;
            if (StartTime != null)
            {
                rangeStart = StartTime.DateTime1;
                rangeEnd = StartTime.DateTime2;

                if (StartTime.Mode == TypeDateTimeCode.ModeType.Equal)
                    startTimeEqual = StartTime.DateTime1;
            }

            DateTime? endTimeEqual = null;
            if (EndTime != null)
            {
                rangeStart = EndTime.DateTime1;
                rangeEnd = EndTime.DateTime2;

                if (EndTime.Mode == TypeDateTimeCode.ModeType.Equal)
                    endTimeEqual = EndTime.DateTime1;
            }

            if (startTimeEqual != null && endTimeEqual != null)
            {
                rangeStart = startTimeEqual;
                rangeEnd = endTimeEqual;
            }
            #endregion

            // Wyszukanie w cache 
            if (IdRefuel != null && IdRefuel.Length > 0)
            {
                foreach (var id in IdRefuel)
                {
                    OpRefuel refuel = RefuelDict.TryGetValue(id);
                    if (refuel != null)
                    {
                        refuelsFP.Add(refuel);
                    }
                }

                // Wyrzucenie wszystkich id ktore sie znalazly
                IdRefuel = IdRefuel.Where(id => refuelsFP.Select(r => r.IdRefuel).Contains(id) == false).ToArray();
            }

            // Je�eli nie byly podane id lub po wyciagnieciu z cache nadal co� zosta�odo do pobrania, to pobierze wszystko i znadzie tu tego biedaka
            if (IdRefuel == null || (IdRefuel != null && IdRefuel.Length > 0))
            {
                // Pobieranie z szyny
                List<List<FP.Objects.Delivery>> deliveries = new List<List<FP.Objects.Delivery>>();
                if (IdLocation != null && IdLocation.Length > 0)
                {
                    foreach (var item in IdLocation.Partition(100))
                    {
                        var del = WEsbFP.GetDeliveryFilter(item.ToArray(), rangeStart, rangeEnd, commandTimeout);
                        if (del != null) deliveries.AddRange(del);
                    }
                }
                else
                {
                    deliveries = WEsbFP.GetDeliveryFilter(null, rangeStart, rangeEnd, commandTimeout);
                }

                // Przygotowanie s�ownik�w
                Dictionary<long, FP.Objects.Driver> drivers = WEsbFP.GetDrivers().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
                Dictionary<long, FP.Objects.Carrier> carriers = WEsbFP.GetCarriers().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
                Dictionary<long, FP.Objects.Terminal> terminals = WEsbFP.GetTerminals().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
                Dictionary<long, FP.Objects.TruckTrailer> trucksAndTrilers = WEsbFP.GetTrucksTrailers().Where(d => d.Id.HasValue).Distinct(d => d.Id.Value).ToDictionary(k => k.Id.Value);
                List<FP.Objects.Tank> tanks = WEsbFP.GetTanks(null, null).Where(d => d.Id.HasValue).ToList();
                // Wyszukanie meter�w na ktore przyszy dostawy
                List<long> idMetersList = new List<long>();
                idMetersList.AddRange(deliveries.SelectMany(d => d).SelectMany(d => d.AtgRefuels).Where(r => r.TankId.HasValue).Select(r => r.TankId.Value));
                idMetersList.AddRange(deliveries.SelectMany(d => d).SelectMany(d => d.Refuels).Where(r => r.TankId.HasValue).Select(r => r.TankId.Value));
                long[] idMeters = idMetersList.Distinct().ToArray();
                Dictionary<long, OpMeter> metersDict = this.GetMeterFilter(loadNavigationProperties: false, loadCustomData: true, IdMeter: idMeters,
                    customDataTypes: new List<long>() { Common.DataType.ATG_PARAMS_PRODUCT_CODE }).ToDictionary(k => k.IdMeter);

                // Przetwarzanie ka�dej dostawy FP na refuele Op po��czone guidami
                foreach (List<FP.Objects.Delivery> delivery in deliveries)
                {
                    if (delivery.Count == 1)
                    {
                        FP.Objects.Delivery singleDelivery = delivery.FirstOrDefault();
                        if (singleDelivery != null)
                        {
                            Enums.DataSourceType deliverySourceType = FP.EnumsConverter.FpDeliveryTypeToDataSourceType(singleDelivery.DeliveryType);
                            if (deliverySourceType != Enums.DataSourceType.AWSR)
                                continue;
                        }
                        else
                            continue;
                    }
                    // 2017-03-07 - Chcemy widziec niepolaczone calculated, inne sa niepoprawne

                    // Przetwarzanie na list� refueli
                    List<OpRefuel> convertedDeliveries = FP.Objects.Delivery.ToOpRefuelList(WEsbFP, delivery, this, metersDict,
                        drivers, carriers, terminals, trucksAndTrilers, tanks, loadCustomData, customDataTypes);
                    refuelsFP.AddRange(convertedDeliveries);
                }
            }

            // Uzupe�nienie Refuel cache
            foreach (var refuel in refuelsFP)
            {
                if (RefuelDict.ContainsKey(refuel.IdRefuel) == false)
                    RefuelDict[refuel.IdRefuel] = refuel;
            }

            // Uzupe�nienie RefuelData Cache
            foreach (var refuelData in refuelsFP.SelectMany(d => d.DataList))
            {
                if (RefuelDataDict.ContainsKey(refuelData.IdRefuelData) == false)
                    RefuelDataDict[refuelData.IdRefuelData] = refuelData;
            }

            #region Filtrowanie wynik�w
            IEnumerable<OpRefuel> refuelFilter = refuelsFP.AsEnumerable();

            if (IdRefuel != null && IdRefuel.Length > 0)
                refuelFilter = refuelFilter.Where(d => IdRefuel.Contains(d.IdRefuel));
            if (IdMeter != null && IdMeter.Length > 0)
                refuelFilter = refuelFilter.Where(d => d.IdMeter.HasValue && IdMeter.Contains(d.IdMeter.Value));
            if (IdLocation != null && IdLocation.Length > 0)
                refuelFilter = refuelFilter.Where(d => d.IdLocation.HasValue && IdLocation.Contains(d.IdLocation.Value));
            if (IdDataSource != null && IdDataSource.Length > 0)
                refuelFilter = refuelFilter.Where(d => IdDataSource.Contains(d.IdDataSource));
            if (StartTime != null)
            {
                switch (StartTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Null:
                        refuelFilter = refuelFilter.Where(d => !d.StartTime.HasValue);
                        break;
                    case TypeDateTimeCode.ModeType.Between:
                        if (StartTime.DateTime1.HasValue && StartTime.DateTime2.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value >= StartTime.DateTime1.Value && d.StartTime.Value <= StartTime.DateTime2.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Different:
                        if (StartTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value != StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        if (StartTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value == StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                        if (StartTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value > StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        if (StartTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value >= StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                        if (StartTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value < StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        if (StartTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value <= StartTime.DateTime1.Value);
                        break;
                }
            }
            if (EndTime != null)
            {
                switch (EndTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Null:
                        refuelFilter = refuelFilter.Where(d => !d.EndTime.HasValue);
                        break;
                    case TypeDateTimeCode.ModeType.Between:
                        if (EndTime.DateTime1.HasValue && EndTime.DateTime2.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value >= EndTime.DateTime1.Value && d.EndTime.Value <= EndTime.DateTime2.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Different:
                        if (EndTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value != EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        if (EndTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value == EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                        if (EndTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value > EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        if (EndTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value >= EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                        if (EndTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value < EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        if (EndTime.DateTime1.HasValue)
                            refuelFilter = refuelFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value <= EndTime.DateTime1.Value);
                        break;
                }
            }

            List<OpRefuel> returnList = refuelFilter.ToList();

            #endregion

            // Pobieranie dodatkowych warto�ci. (OpMeter, OpLocation, OpDataSource)
            if (loadNavigationProperties && returnList.Count > 0)
                OpRefuel.LoadNavigationProperties(ref returnList, this);

            return returnList;
        }
        #endregion

        #region GetRefuelDataTemporals
        public override Dictionary<DateTime, List<OpDataTemporal>> GetRefuelDataTemporals(long[] idLocation, long[] idMeter,
            DateTime rangeStart, DateTime rangeEnd, string orderNumber, Enums.AggregationType aggrType, bool onlyJoinedRefuels = true, int commandTimeout = 0)
        {
            Dictionary<DateTime, List<OpDataTemporal>> returnDict = new Dictionary<DateTime, List<OpDataTemporal>>();

            if (idLocation == null || idLocation.Length == 0)
                idLocation = Components.CORE.LocationComponent.GetLocationIdsByMeterIds(this, idMeter).ToArray();
            if (idMeter == null)
                idMeter = new long[0];

            Dictionary<DateTime, List<OpDataTemporal>> notJoinedDict = new Dictionary<DateTime, List<OpDataTemporal>>();

            List<FP.Objects.Tank> tanks = null;
            if (idLocation == null || idLocation.Length == 0)
                tanks = WEsbFP.GetTanks(idLocation, null);
            else
            {
                tanks = new List<FP.Objects.Tank>();
                foreach (IEnumerable<long> id in idLocation.Partition(500))
                {
                    List<FP.Objects.Tank> tmpTanks = WEsbFP.GetTanks(id.ToArray(), null);
                    if (tmpTanks != null)
                        tanks.AddRange(tmpTanks);
                }
            }

            List<List<FP.Objects.Delivery>> deliveries = null;
            if (idLocation == null || idLocation.Length == 0)
                deliveries = WEsbFP.GetDeliveries(idLocation, rangeStart, rangeEnd, orderNumber, commandTimeout);
            else
            {
                deliveries = new List<List<FP.Objects.Delivery>>();
                foreach (IEnumerable<long> id in idLocation.Partition(500))
                {
                    List<List<FP.Objects.Delivery>> tmpDeliveries = WEsbFP.GetDeliveries(id.ToArray(), rangeStart, rangeEnd, orderNumber, commandTimeout);
                    if (tmpDeliveries != null)
                        deliveries.AddRange(tmpDeliveries);
                }
            }
            if (deliveries == null) return returnDict;

            // Filtrowanie tylko potwierdzonych dostaw
            // v 2.0 bierzemy niepotwierdzone calculated i potwierdzone deklarowane
            // deliveries = deliveries.Where(d => d.Any(r => r.Confirmed.HasValue && r.Confirmed.Value == true)).ToList();

            // Hierarchia dostaw
            /* Automatyczne
             * 1. AWSR
             * 2. OKO
             * 
             * Manualne
             * 1. Manual
             * 2. File
             * 3. Terminal
             * 4. Planned
             * */

            foreach (List<FP.Objects.Delivery> deliveryList in deliveries)
            {
                if (deliveryList.Count > 1)
                {
                    List<OpDataTemporal> singleDelivery = new List<OpDataTemporal>();
                    DateTime? deliveryDate = null;
                    Dictionary<DateTime, List<OpDataTemporal>> calculatedDeliveryDates = new Dictionary<DateTime, List<OpDataTemporal>>();

                    FP.Objects.Delivery refereneceDelivery = deliveryList.FirstOrDefault(d => d.DeliveryDate.HasValue);
                    if (refereneceDelivery == null) refereneceDelivery = deliveryList.FirstOrDefault(d => d.EndTime.HasValue);

                    if (refereneceDelivery != null)
                    {
                        if (refereneceDelivery.DeliveryDate.HasValue)
                            deliveryDate = DateTimer.GetDtLocalFromMsUtc(refereneceDelivery.DeliveryDate);
                        else if (refereneceDelivery.EndTime.HasValue)
                            deliveryDate = DateTimer.GetDtLocalFromMsUtc(refereneceDelivery.EndTime.Value).Date;
                    }

                    // long - idmeter
                    // dateTime - nowa data refuela dla deklarowanego
                    Dictionary<long, DateTime> tankUpdatedRefuelDate = new Dictionary<long, DateTime>();

                    // Wa�na jest hierarchia typ�w dostaw
                    /*
                     * Dostawy s� zwracane wg ustalonej hierarhii i z kazdego typu (Automatyczna, Manualna)
                     * jest zwracana tylko jedna, ta, kt�ra jest wy�ej w hierarchii. St�d dostawy typu AWSR s� np. wyrzucane jako OKO, 
                     * poniewa� tak dzia�a�a procedura pobierania dostaw w IMR. 
                     * Analogiczna systuacja w dostawach manualnych - pierwsza w hierarchii jest pobierana jako ManualDataEntry.
                     * 
                     * */
                    // Automatyczne
                    #region AWSR
                    if (deliveryList.Any(d => FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.AWSR))
                    {
                        var awsrDelivery = deliveryList.FirstOrDefault(d =>
                            FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.AWSR
                            && d.Canceled.HasValue && d.Canceled.Value == false && d.Confirmed.HasValue && d.Confirmed.Value == true);

                        if (awsrDelivery != null)
                        {
                            IEnumerable<IGrouping<long, FP.Objects.AtgRefuel>> refuelsForMeters = awsrDelivery.AtgRefuels.Where(r => r.TankId.HasValue).GroupBy(r => r.TankId.Value);

                            // Filtrowanie po meterze z parametru funkcji
                            refuelsForMeters = refuelsForMeters.Where(d => idMeter.Contains(d.Key));

                            foreach (var refuelForMeterGroup in refuelsForMeters)
                            {
                                double? valueGross = null;
                                double? valueNet = null;
                                FP.Objects.AtgRefuel awsrRefuel = null;

                                // Mo�e sie zdazyc ze bedzie dostawa rozdzielona na dwa awsry, wiec trzeba zsumowac dostawy w jedna
                                if (refuelForMeterGroup.Count() > 1)
                                {
                                    awsrRefuel = refuelForMeterGroup.FirstOrDefault();

                                    valueGross = refuelForMeterGroup.Sum(r => r.Volume);
                                    valueNet = refuelForMeterGroup.Sum(r => r.Volume);
                                }
                                else if (refuelForMeterGroup.Count() > 0)
                                {
                                    awsrRefuel = refuelForMeterGroup.FirstOrDefault();

                                    valueGross = awsrRefuel.Volume;
                                    valueNet = awsrRefuel.Volume15c;
                                }

                                if (awsrRefuel != null)
                                {
                                    if (valueGross.HasValue)
                                    {
                                        OpDataTemporal deliveryGross = new OpDataTemporal()
                                        {
                                            IdDataSourceType = (int)Enums.DataSourceType.OKO, // procedura zwraca�a AWSR jako OKO inaczej Calculated
                                            IdAggregationType = (int)aggrType,
                                            StartTime = DateTimer.GetDtLocalFromMsUtc(awsrRefuel.StartTime),
                                            EndTime = DateTimer.GetDtLocalFromMsUtc(awsrRefuel.EndTime),
                                            IdDataType = Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                                            IdMeter = awsrRefuel.TankId,
                                            IdLocation = awsrDelivery.LocationId,
                                            IndexNbr = 0,
                                            Value = valueGross / 1000,
                                            DataStatus = Enums.DataStatus.Normal
                                        };
                                        deliveryGross.DataType = GetDataType(deliveryGross.IdDataType);
                                        //singleDelivery.Add(deliveryGross);
                                        if (deliveryGross.EndTime.HasValue)
                                        {
                                            // Refuel AWSR zamkn�� si� p�niej ni� doba manualna, wi�c trzeba manualn� przenie��, by nadal by�y po��czone
                                            if (awsrRefuel.TankId.HasValue && deliveryDate.HasValue && deliveryDate.Value != deliveryGross.EndTime.Value.Date)
                                                tankUpdatedRefuelDate[awsrRefuel.TankId.Value] = deliveryGross.EndTime.Value.Date;

                                            //
                                            if (calculatedDeliveryDates.ContainsKey(deliveryGross.EndTime.Value.Date))
                                            {
                                                calculatedDeliveryDates[deliveryGross.EndTime.Value.Date].Add(deliveryGross);
                                            }
                                            else
                                            {
                                                calculatedDeliveryDates[deliveryGross.EndTime.Value.Date] = new List<OpDataTemporal>() { deliveryGross };
                                            }
                                        }
                                    }

                                    if (valueNet.HasValue)
                                    {
                                        OpDataTemporal deliveryNet = new OpDataTemporal()
                                        {
                                            IdDataSourceType = (int)Enums.DataSourceType.OKO, // procedura zwraca�a AWSR jako OKO inaczej Calculated
                                            IdAggregationType = (int)aggrType,
                                            StartTime = DateTimer.GetDtLocalFromMsUtc(awsrRefuel.StartTime),
                                            EndTime = DateTimer.GetDtLocalFromMsUtc(awsrRefuel.EndTime),
                                            IdDataType = Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                                            IdMeter = awsrRefuel.TankId,
                                            IdLocation = awsrDelivery.LocationId,
                                            IndexNbr = 0,
                                            Value = valueNet / 1000,
                                            DataStatus = Enums.DataStatus.Normal
                                        };
                                        deliveryNet.DataType = GetDataType(deliveryNet.IdDataType);
                                        //singleDelivery.Add(deliveryNet);

                                        if (deliveryNet.EndTime.HasValue)
                                        {
                                            if (calculatedDeliveryDates.ContainsKey(deliveryNet.EndTime.Value.Date))
                                            {
                                                calculatedDeliveryDates[deliveryNet.EndTime.Value.Date].Add(deliveryNet);
                                            }
                                            else
                                            {
                                                calculatedDeliveryDates[deliveryNet.EndTime.Value.Date] = new List<OpDataTemporal>() { deliveryNet };
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region OKO
                    else if (deliveryList.Any(d => FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.OKO))
                    {
                        var delivery = deliveryList.FirstOrDefault(d =>
                            FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.OKO
                            && d.Confirmed.HasValue && d.Confirmed.Value == true
                            && d.Canceled.HasValue && d.Canceled.Value == false);
                        if (delivery != null)
                        {
                            // Pobranie refuela juz dla konkretnych zbiornik�w
                            var refuels = delivery.AtgRefuels.Where(r => r.TankId.HasValue && idMeter.Contains(r.TankId.Value));
                            /*
                             * Do dostawy OKO nalezy doliczy� warto�� sprzeda�y z tabelki ktora znajduje sie w delivery powy�ej. W przypadku tej tabelki dane sprzeda�y s� tam podane per product,
                             * wi�c trzeba warto�� sprzeda�y rozdzieli� na wszystkie zbiorniki tego produktu.
                             * */
                            var refuelsTanks = refuels.Select(s => s.TankId).ToList();
                            var productsWithTheirTanks = tanks.Where(t => refuelsTanks.Contains(t.Id)).GroupBy(t => t.ProductId);//.ToDictionary(k => k.Key);

                            foreach (var refuel in refuels)
                            {
                                // warto�� sprzeda�y do doliczenia
                                double meterSaleGross = 0;
                                double meterSaleNet = 0;

                                // obliczanie wartosci sprzedazy do doliczenia dla kazdego zbiornika, poniewaz sprzedaz jest per product.
                                FP.Objects.Tank tank = tanks.FirstOrDefault(t => t.Id == refuel.TankId);
                                if (tank != null)
                                {
                                    FP.Objects.Sale sale = delivery.Sales.FirstOrDefault(s => s.ProductId == tank.ProductId && s.StartTime == refuel.StartTime && s.EndTime == refuel.EndTime);
                                    if (sale != null)
                                    {
                                        // Lista id zbiornikow dla tego produktu
                                        var tanksOnThatProduct = productsWithTheirTanks.FirstOrDefault(p => p.Key == tank.ProductId);
                                        if (tanksOnThatProduct != null)
                                        {
                                            // Na ile zbiornikow sprzeda� powinna zosta� rozdzielona
                                            int tankCount = tanksOnThatProduct.ToList().Count;
                                            if (tankCount != 0)
                                            {
                                                // Sprzeda� gorss per product
                                                double productSaleGross = sale.PosVolumeDispenced ?? 0;
                                                if (productSaleGross != 0)
                                                {
                                                    meterSaleGross = productSaleGross / tankCount;
                                                }
                                                // Sprzeda� net per product 
                                                double productSaleNet = sale.PosVolumeDispenced15c ?? 0;
                                                if (productSaleNet != 0)
                                                {
                                                    meterSaleNet = productSaleNet / tankCount;
                                                }
                                            }
                                        }
                                    }
                                }

                                double? valueGross = refuel.Volume;
                                double? valueNet = refuel.Volume15c;

                                if (valueGross.HasValue)
                                {
                                    OpDataTemporal deliveryGross = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.OKO,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueGross / 1000 + meterSaleGross / 1000, // doliczona warto�� sprzedazy
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryGross.DataType = GetDataType(deliveryGross.IdDataType);
                                    singleDelivery.Add(deliveryGross);
                                }

                                if (valueNet.HasValue)
                                {
                                    OpDataTemporal deliveryNet = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.OKO,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueNet / 1000 + meterSaleNet / 1000, // doliczona warto�� sprzedazy
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryNet.DataType = GetDataType(deliveryNet.IdDataType);
                                    singleDelivery.Add(deliveryNet);
                                }
                            }
                        }
                    }
                    #endregion

                    // Manualne
                    // 2.0 Manualne bierzemy tylko potwierdzone, nie anulowane 
                    #region ManualDataEntry
                    if (deliveryList.Any(d => FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.ManualDataEntry))
                    {
                        var delivery = deliveryList.FirstOrDefault(d =>
                            FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.ManualDataEntry &&
                            d.Confirmed.HasValue && d.Confirmed.Value == true && d.Canceled.HasValue && d.Canceled.Value == false);
                        if (delivery != null)
                        {
                            // Pobranie refueli juz dla konkretnych zbiornik�w
                            var refuels = delivery.Refuels.Where(r => r.TankId.HasValue && idMeter.Contains(r.TankId.Value));

                            foreach (var refuel in refuels)
                            {
                                double? valueGross = refuel.Volume;
                                double? valueNet = refuel.Volume15c;

                                if (valueGross.HasValue)
                                {
                                    OpDataTemporal deliveryGross = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueGross / 1000,
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryGross.DataType = GetDataType(deliveryGross.IdDataType);

                                    // Jesli wykryty byl przeniesiony na inny dzien to ten deklarowany tez musi byc przeniesiony
                                    if (refuel.TankId.HasValue && tankUpdatedRefuelDate.ContainsKey(refuel.TankId.Value))
                                    {
                                        if (calculatedDeliveryDates.ContainsKey(tankUpdatedRefuelDate[refuel.TankId.Value]))
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]].Add(deliveryGross);
                                        else // to pewnie nigdy sie nie zdazy
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]] = new List<OpDataTemporal>() { deliveryGross };
                                    }
                                    else
                                    {
                                        singleDelivery.Add(deliveryGross);
                                    }
                                }

                                if (valueNet.HasValue)
                                {
                                    OpDataTemporal deliveryNet = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueNet / 1000,
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryNet.DataType = GetDataType(deliveryNet.IdDataType);
                                    // Jesli wykryty byl przeniesiony na inny dzien to ten deklarowany tez musi byc przeniesiony
                                    if (refuel.TankId.HasValue && tankUpdatedRefuelDate.ContainsKey(refuel.TankId.Value))
                                    {
                                        if (calculatedDeliveryDates.ContainsKey(tankUpdatedRefuelDate[refuel.TankId.Value]))
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]].Add(deliveryNet);
                                        else // to pewnie nigdy sie nie zdazy
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]] = new List<OpDataTemporal>() { deliveryNet };
                                    }
                                    else
                                    {
                                        singleDelivery.Add(deliveryNet);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region File
                    else if (deliveryList.Any(d => FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.File))
                    {
                        var delivery = deliveryList.FirstOrDefault(d =>
                            FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.File
                            && d.Confirmed.HasValue && d.Confirmed.Value == true && d.Canceled.HasValue && d.Canceled.Value == false);
                        if (delivery != null)
                        {

                            // Pobranie refuela juz dla konkretnych zbiornik�w
                            var refuels = delivery.Refuels.Where(r => r.TankId.HasValue && idMeter.Contains(r.TankId.Value));

                            foreach (var refuel in refuels)
                            {
                                double? valueGross = refuel.Volume;
                                double? valueNet = refuel.Volume15c;

                                if (valueGross.HasValue)
                                {
                                    OpDataTemporal deliveryGross = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueGross / 1000,
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryGross.DataType = GetDataType(deliveryGross.IdDataType);
                                    // Jesli wykryty byl przeniesiony na inny dzien to ten deklarowany tez musi byc przeniesiony
                                    if (refuel.TankId.HasValue && tankUpdatedRefuelDate.ContainsKey(refuel.TankId.Value))
                                    {
                                        if (calculatedDeliveryDates.ContainsKey(tankUpdatedRefuelDate[refuel.TankId.Value]))
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]].Add(deliveryGross);
                                        else // to pewnie nigdy sie nie zdazy
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]] = new List<OpDataTemporal>() { deliveryGross };
                                    }
                                    else
                                    {
                                        singleDelivery.Add(deliveryGross);
                                    }
                                }

                                if (valueNet.HasValue)
                                {
                                    OpDataTemporal deliveryNet = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueNet / 1000,
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryNet.DataType = GetDataType(deliveryNet.IdDataType);
                                    // Jesli wykryty byl przeniesiony na inny dzien to ten deklarowany tez musi byc przeniesiony
                                    if (refuel.TankId.HasValue && tankUpdatedRefuelDate.ContainsKey(refuel.TankId.Value))
                                    {
                                        if (calculatedDeliveryDates.ContainsKey(tankUpdatedRefuelDate[refuel.TankId.Value]))
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]].Add(deliveryNet);
                                        else // to pewnie nigdy sie nie zdazy
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]] = new List<OpDataTemporal>() { deliveryNet };
                                    }
                                    else
                                    {
                                        singleDelivery.Add(deliveryNet);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region Interface
                    else if (deliveryList.Any(d => FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.Interface))
                    {
                        var delivery = deliveryList.FirstOrDefault(d =>
                            FP.EnumsConverter.FpDeliveryTypeToDataSourceType(d.DeliveryType) == Enums.DataSourceType.Interface
                            && d.Confirmed.HasValue && d.Confirmed.Value == true && d.Canceled.HasValue && d.Canceled.Value == false);
                        if (delivery != null)
                        {
                            // Pobranie refuela juz dla konkretnych zbiornik�w
                            var refuels = delivery.Refuels.Where(r => r.TankId.HasValue && idMeter.Contains(r.TankId.Value));

                            foreach (var refuel in refuels)
                            {
                                double? valueGross = refuel.Volume;
                                double? valueNet = refuel.Volume15c;

                                if (valueGross.HasValue)
                                {
                                    OpDataTemporal deliveryGross = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueGross / 1000,
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryGross.DataType = GetDataType(deliveryGross.IdDataType);
                                    // Jesli wykryty byl przeniesiony na inny dzien to ten deklarowany tez musi byc przeniesiony
                                    if (refuel.TankId.HasValue && tankUpdatedRefuelDate.ContainsKey(refuel.TankId.Value))
                                    {
                                        if (calculatedDeliveryDates.ContainsKey(tankUpdatedRefuelDate[refuel.TankId.Value]))
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]].Add(deliveryGross);
                                        else // to pewnie nigdy sie nie zdazy
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]] = new List<OpDataTemporal>() { deliveryGross };
                                    }
                                    else
                                    {
                                        singleDelivery.Add(deliveryGross);
                                    }
                                }

                                if (valueNet.HasValue)
                                {
                                    OpDataTemporal deliveryNet = new OpDataTemporal()
                                    {
                                        IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                        IdAggregationType = (int)aggrType,
                                        StartTime = DateTimer.GetDtLocalFromMsUtc(refuel.StartTime),
                                        EndTime = DateTimer.GetDtLocalFromMsUtc(refuel.EndTime),
                                        IdDataType = Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                                        IdMeter = refuel.TankId,
                                        IdLocation = delivery.LocationId,
                                        IndexNbr = 0,
                                        Value = valueNet / 1000,
                                        DataStatus = Enums.DataStatus.Normal
                                    };
                                    deliveryNet.DataType = GetDataType(deliveryNet.IdDataType);
                                    // Jesli wykryty byl przeniesiony na inny dzien to ten deklarowany tez musi byc przeniesiony
                                    if (refuel.TankId.HasValue && tankUpdatedRefuelDate.ContainsKey(refuel.TankId.Value))
                                    {
                                        if (calculatedDeliveryDates.ContainsKey(tankUpdatedRefuelDate[refuel.TankId.Value]))
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]].Add(deliveryNet);
                                        else // to pewnie nigdy sie nie zdazy
                                            calculatedDeliveryDates[tankUpdatedRefuelDate[refuel.TankId.Value]] = new List<OpDataTemporal>() { deliveryNet };
                                    }
                                    else
                                    {
                                        singleDelivery.Add(deliveryNet);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    if (singleDelivery.Any() && deliveryDate.HasValue)
                    {
                        if (returnDict.ContainsKey(deliveryDate.Value))
                            returnDict[deliveryDate.Value].AddRange(singleDelivery);
                        else
                            returnDict.Add(deliveryDate.Value, singleDelivery);
                    }

                    foreach (var calcRefuel in calculatedDeliveryDates)
                    {
                        if (returnDict.ContainsKey(calcRefuel.Key))
                        {
                            returnDict[calcRefuel.Key].AddRange(calcRefuel.Value);
                        }
                        else
                        {
                            returnDict[calcRefuel.Key] = new List<OpDataTemporal>(calcRefuel.Value);
                        }
                    }
                }
                else if (!onlyJoinedRefuels && deliveryList.Count(q => q.Canceled.HasValue && !q.Canceled.Value) == 1)
                {
                    FP.Objects.Delivery notJoinedDelivery = deliveryList.FirstOrDefault();
                    if (notJoinedDelivery != null && notJoinedDelivery.DeliveryDate.HasValue)
                    {
                        DateTime deliveryDate = DateTimer.GetDtLocalFromMsUtc(notJoinedDelivery.DeliveryDate.Value);
                        Enums.DataSourceType dataSourceType = FP.EnumsConverter.FpDeliveryTypeToDataSourceType(notJoinedDelivery.DeliveryType);
                        #region AWSR | OKO
                        if (dataSourceType == Enums.DataSourceType.AWSR || dataSourceType == Enums.DataSourceType.OKO)
                        {
                            foreach (var refuelForMeterGroup in notJoinedDelivery.AtgRefuels.Where(r => r.TankId.HasValue && idMeter.Contains(r.TankId.Value)).GroupBy(r => r.TankId.Value))
                            {
                                double? valueGross = null;
                                double? valueNet = null;
                                FP.Objects.AtgRefuel autoRefuel = refuelForMeterGroup.FirstOrDefault();
                                if (refuelForMeterGroup.Count(q => q.Volume.HasValue) > 0)
                                    valueGross = refuelForMeterGroup.Where(q => q.Volume.HasValue).Sum(q => q.Volume.Value);
                                if (refuelForMeterGroup.Count(q => q.Volume15c.HasValue) > 0)
                                    valueNet = refuelForMeterGroup.Where(q => q.Volume15c.HasValue).Sum(q => q.Volume15c.Value);
                                if (autoRefuel != null)
                                {
                                    if (valueGross.HasValue)
                                    {
                                        OpDataTemporal deliveryGross = new OpDataTemporal()
                                        {
                                            IdDataSourceType = (int)Enums.DataSourceType.OKO,
                                            IdAggregationType = (int)aggrType,
                                            StartTime = DateTimer.GetDtLocalFromMsUtc(autoRefuel.StartTime),
                                            EndTime = DateTimer.GetDtLocalFromMsUtc(autoRefuel.EndTime),
                                            IdDataType = Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                                            IdMeter = autoRefuel.TankId,
                                            IdLocation = notJoinedDelivery.LocationId,
                                            IndexNbr = 0,
                                            Value = valueGross / 1000,
                                            DataStatus = Enums.DataStatus.Unknown
                                        };
                                        deliveryGross.DataType = GetDataType(deliveryGross.IdDataType);
                                        if (notJoinedDict.ContainsKey(deliveryDate.Date))
                                            notJoinedDict[deliveryDate.Date].Add(deliveryGross);
                                        else
                                            notJoinedDict[deliveryDate.Date] = new List<OpDataTemporal>() { deliveryGross };
                                    }
                                    if (valueNet.HasValue)
                                    {
                                        OpDataTemporal deliveryNet = new OpDataTemporal()
                                        {
                                            IdDataSourceType = (int)Enums.DataSourceType.OKO,
                                            IdAggregationType = (int)aggrType,
                                            StartTime = DateTimer.GetDtLocalFromMsUtc(autoRefuel.StartTime),
                                            EndTime = DateTimer.GetDtLocalFromMsUtc(autoRefuel.EndTime),
                                            IdDataType = Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                                            IdMeter = autoRefuel.TankId,
                                            IdLocation = notJoinedDelivery.LocationId,
                                            IndexNbr = 0,
                                            Value = valueNet / 1000,
                                            DataStatus = Enums.DataStatus.Unknown
                                        };
                                        deliveryNet.DataType = GetDataType(deliveryNet.IdDataType);
                                        if (notJoinedDict.ContainsKey(deliveryDate.Date))
                                            notJoinedDict[deliveryDate.Date].Add(deliveryNet);
                                        else
                                            notJoinedDict[deliveryDate.Date] = new List<OpDataTemporal>() { deliveryNet };
                                    }
                                }
                            }
                        }
                        #endregion
                        #region ManualDataEntry | File | Interface
                        else if (dataSourceType == Enums.DataSourceType.ManualDataEntry || dataSourceType == Enums.DataSourceType.File || dataSourceType == Enums.DataSourceType.Interface)
                        {
                            foreach (var refuelForMeterGroup in notJoinedDelivery.Refuels.Where(r => r.TankId.HasValue && idMeter.Contains(r.TankId.Value)).GroupBy(r => r.TankId.Value))
                            {
                                double? valueGross = null;
                                double? valueNet = null;
                                FP.Objects.Refuel manRefuel = refuelForMeterGroup.FirstOrDefault();
                                if (refuelForMeterGroup.Count(q => q.Volume.HasValue) > 0)
                                    valueGross = refuelForMeterGroup.Where(q => q.Volume.HasValue).Sum(q => q.Volume.Value);
                                if (refuelForMeterGroup.Count(q => q.Volume15c.HasValue) > 0)
                                    valueNet = refuelForMeterGroup.Where(q => q.Volume15c.HasValue).Sum(q => q.Volume15c.Value);
                                if (manRefuel != null)
                                {
                                    if (valueGross.HasValue)
                                    {
                                        OpDataTemporal deliveryGross = new OpDataTemporal()
                                        {
                                            IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                            IdAggregationType = (int)aggrType,
                                            StartTime = DateTimer.GetDtLocalFromMsUtc(manRefuel.StartTime),
                                            EndTime = DateTimer.GetDtLocalFromMsUtc(manRefuel.EndTime),
                                            IdDataType = Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                                            IdMeter = manRefuel.TankId,
                                            IdLocation = notJoinedDelivery.LocationId,
                                            IndexNbr = 0,
                                            Value = valueGross / 1000,
                                            DataStatus = Enums.DataStatus.Unknown
                                        };
                                        deliveryGross.DataType = GetDataType(deliveryGross.IdDataType);
                                        if (notJoinedDict.ContainsKey(deliveryDate.Date))
                                            notJoinedDict[deliveryDate.Date].Add(deliveryGross);
                                        else
                                            notJoinedDict[deliveryDate.Date] = new List<OpDataTemporal>() { deliveryGross };
                                    }
                                    if (valueNet.HasValue)
                                    {
                                        OpDataTemporal deliveryNet = new OpDataTemporal()
                                        {
                                            IdDataSourceType = (int)Enums.DataSourceType.ManualDataEntry,
                                            IdAggregationType = (int)aggrType,
                                            StartTime = DateTimer.GetDtLocalFromMsUtc(manRefuel.StartTime),
                                            EndTime = DateTimer.GetDtLocalFromMsUtc(manRefuel.EndTime),
                                            IdDataType = Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                                            IdMeter = manRefuel.TankId,
                                            IdLocation = notJoinedDelivery.LocationId,
                                            IndexNbr = 0,
                                            Value = valueNet / 1000,
                                            DataStatus = Enums.DataStatus.Unknown
                                        };
                                        deliveryNet.DataType = GetDataType(deliveryNet.IdDataType);
                                        if (notJoinedDict.ContainsKey(deliveryDate.Date))
                                            notJoinedDict[deliveryDate.Date].Add(deliveryNet);
                                        else
                                            notJoinedDict[deliveryDate.Date] = new List<OpDataTemporal>() { deliveryNet };
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                }
            }

            // Wiele dostaw z jednego dnia trzeba po��czy�...
            foreach (var day in returnDict)
            {
                List<OpDataTemporal> summedRefuels = new List<OpDataTemporal>();
                foreach (var meterRefuelsForTank in day.Value.GroupBy(d => new { d.IdDataSourceType, d.IdMeter, d.IdDataType, d.IndexNbr, d.IdLocation, d.IdAggregationType }))
                {
                    var firstRefuelForDay = meterRefuelsForTank.ToList().FirstOrDefault();
                    if (firstRefuelForDay == null) continue;

                    DateTime? firstRefuelStartTime = firstRefuelForDay.StartTime;

                    var lastRefuelForDay = meterRefuelsForTank.ToList().LastOrDefault();
                    DateTime? lastRefuleEndTime = lastRefuelForDay != null ? lastRefuelForDay.EndTime : null;

                    double? refuelsSum = meterRefuelsForTank.ToList().Where(d => d.ValueDouble.HasValue).Select(d => d.ValueDouble.Value).Sum();

                    OpDataTemporal summedRefuelsForDay = new OpDataTemporal()
                    {
                        IdDataSourceType = firstRefuelForDay.IdDataSourceType,
                        IdAggregationType = firstRefuelForDay.IdAggregationType,
                        StartTime = firstRefuelStartTime,
                        EndTime = lastRefuleEndTime,
                        IdDataType = firstRefuelForDay.IdDataType,
                        IdMeter = firstRefuelForDay.IdMeter,
                        IdLocation = firstRefuelForDay.IdLocation,
                        IndexNbr = firstRefuelForDay.IndexNbr,
                        Value = refuelsSum,
                        DataType = firstRefuelForDay.DataType,
                        DataStatus = Enums.DataStatus.Normal
                    };
                    summedRefuels.Add(summedRefuelsForDay);
                }
                day.Value.Clear();
                day.Value.AddRange(summedRefuels);
            }

            if (!onlyJoinedRefuels && notJoinedDict.Count > 0)
            {
                foreach (var keyValue in notJoinedDict)
                {
                    List<OpDataTemporal> summedRefuels = new List<OpDataTemporal>();
                    foreach (var deliveryGrouped in keyValue.Value.GroupBy(q => new { q.IdDataSourceType, q.IdMeter, q.IdDataType, q.IndexNbr, q.IdLocation, q.IdAggregationType }))
                    {
                        if (deliveryGrouped.Count(q => q.StartTime.HasValue) == 0 || deliveryGrouped.Count(q => q.ValueDouble.HasValue) == 0) continue;

                        DateTime? endTime = null;
                        if (deliveryGrouped.Count(q => q.EndTime.HasValue) > 0)
                            endTime = deliveryGrouped.Where(q => q.EndTime.HasValue).Max(q => q.EndTime.Value);

                        summedRefuels.Add(new OpDataTemporal()
                        {
                            IdDataSourceType = deliveryGrouped.Key.IdDataSourceType,
                            IdAggregationType = deliveryGrouped.Key.IdAggregationType,
                            StartTime = deliveryGrouped.Where(q => q.StartTime.HasValue).Min(q => q.StartTime.Value),
                            EndTime = endTime,
                            IdDataType = deliveryGrouped.Key.IdDataType,
                            IdMeter = deliveryGrouped.Key.IdMeter,
                            IdLocation = deliveryGrouped.Key.IdLocation,
                            IndexNbr = deliveryGrouped.Key.IndexNbr,
                            Value = deliveryGrouped.Where(q => q.ValueDouble.HasValue).Sum(q => q.ValueDouble.Value),
                            DataType = GetDataType(deliveryGrouped.Key.IdDataType),
                            DataStatus = Enums.DataStatus.Unknown
                        });
                    }
                    if (returnDict.ContainsKey(keyValue.Key))
                        returnDict[keyValue.Key].AddRange(summedRefuels);
                    else
                        returnDict[keyValue.Key] = summedRefuels;
                }
            }

            returnDict = returnDict.OrderBy(d => d.Key).ToDictionary(k => k.Key, v => v.Value);
            return returnDict;
        }

        public override Dictionary<DateTime, List<OpDataTemporal>> GetRefuelDataTemporals(long? idLocation, long idMeter, DateTime _dataStartTime, DateTime _dataEndTime, Enums.AggregationType aggregationType, bool onlyJoinedRefuels = true, int commandTimeout = 0)
        {
            if (!idLocation.HasValue) return new Dictionary<DateTime, List<OpDataTemporal>>();

            Dictionary<DateTime, List<OpDataTemporal>> returnDict = GetRefuelDataTemporals(new long[] { idLocation.Value }, new long[] { idMeter }, _dataStartTime, _dataEndTime, null, aggregationType, onlyJoinedRefuels, commandTimeout);

            // Filtrowanie po idMeter
            returnDict = returnDict.Where(d => d.Value.FirstOrDefault(op => op.IdMeter.HasValue && op.IdMeter == idMeter) != null)
                .ToDictionary(d => d.Key, d => d.Value);

            return returnDict;
        }
        #endregion
        #endregion

        #region DataTemporal
        #region DeleteDataTemporal
        public override void DeleteDataTemporal(long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            //base.DeleteDataTemporal(SerialNbr, IdMeter, IdLocation, IdDataType, IdAggregationType, customWhereClause, commandTimeout); // zakomentowane, zeby dla bezpieczenstwa nie usuwac z bazy IMR obiektu DataTemporal pochodzacego z FP
        }
        #endregion

        #region SaveDataTemporal
        public override void SaveDataTemporal(OpDataTemporal[] toBeSaved, int commandTimeout = 0)
        {
            #region fuel_balance_sir2
            #region Sir2 Analysis Results
            long[] sir2DataTypes = new long[]
            {
                Common.DataType.AWSR_IRC_SIR2_DECISION,
                Common.DataType.AWSR_IRC_SIR2_DECISION_DATE,
                Common.DataType.AWSR_IRC_SIR2_INCONCLUSIVE_CAUSE,
                Common.DataType.AWSR_IRC_SIR2_LEAK_RATE,
                Common.DataType.AWSR_IRC_SIR2_LEAK_THRESHOLD,
                Common.DataType.AWSR_IRC_SIR2_MDLR,
                Common.DataType.AWSR_IRC_SIR2_METHOD_USED,
                Common.DataType.AWSR_IRC_SIR2_OFFSET,
                Common.DataType.AWSR_IRC_SIR2_OUTLIER_THRESHOLD,
                Common.DataType.AWSR_IRC_SIR2_REPORT,
            };

            var sirDecisionData = toBeSaved.Where(d => sir2DataTypes.Contains(d.IdDataType));
            if (sirDecisionData.Any())
            {
                var fuelBalanceSirListToSave = new List<FP.Objects.FuelBalanceSir>();

                var sirResultGroupedByMeters = sirDecisionData.Where(s => s.IdMeter.HasValue).GroupBy(s => s.IdMeter.Value);

                foreach (IGrouping<long, OpDataTemporal> sirResult in sirResultGroupedByMeters)
                {
                    var fuelBalanceSir = FP.Objects.FuelBalanceSir.ToFuelBalanceSir(sirResult.Select(d => d).ToList());
                    if (fuelBalanceSir != null)
                        fuelBalanceSirListToSave.Add(fuelBalanceSir);
                }

                if (fuelBalanceSirListToSave.Count > 0)
                    WEsbFP.SaveFuelBalancesSir(fuelBalanceSirListToSave);
            }

            toBeSaved = toBeSaved.Where(d => !sir2DataTypes.Contains(d.IdDataType)).ToArray();
            #endregion

            #region Sir2 OperatorDecision
            var sir2OperatorDecisionDataTypes = new long[]
            {
                Common.DataType.AWSR_PROBLEM_ALARM_ID,
                Common.DataType.AWSR_PROBLEM_ISSUE_ID,
                Common.DataType.AWSR_PROBLEM_OPERATOR_DECISION,
                Common.DataType.AWSR_PROBLEM_OPERATOR_DECISION_DESCR,
                Common.DataType.AWSR_PROBLEM_OPERATOR_DECISION_TIME,
                Common.DataType.AWSR_PROBLEM_OPERATOR_ID
            };

            List<OpDataTemporal> sir2OperatorDecisionData = toBeSaved
                .Where(d => sir2OperatorDecisionDataTypes.Contains(d.IdDataType) && d.IdAggregationType == (int)Enums.AggregationType.Month).ToList();

            if (sir2OperatorDecisionData.Any())
            {
                var fuelBalanceSirListToSave = new List<FP.Objects.FuelBalanceSir>();

                DateTime rangeStartDateToFind = ConvertTimeZoneToUtcTime(sir2OperatorDecisionData.Min(d => d.EndTime.Value));
                DateTime rangeEndDateToFind = ConvertTimeZoneToUtcTime(sir2OperatorDecisionData.Max(d => d.EndTime.Value));
                long[] locationIds = sir2OperatorDecisionData.Where(d => d.IdLocation.HasValue).Select(d => d.IdLocation.Value).Distinct().ToArray();

                List<FP.Objects.FuelBalanceSir> sirFuelBalanceList = WEsbFP.GetFuelBalanceSir2(locationIds, rangeStartDateToFind, rangeEndDateToFind);

                var operatorDecisionDataGroupedByMeters = sir2OperatorDecisionData.Where(s => s.IdMeter.HasValue).GroupBy(s => s.IdMeter.Value);

                foreach (IGrouping<long, OpDataTemporal> operatorDecisionDataForMeter in operatorDecisionDataGroupedByMeters)
                {
                    OpDataTemporal operatorDecision = operatorDecisionDataForMeter
                        .FirstOrDefault(d => d.IdDataType == Common.DataType.AWSR_PROBLEM_OPERATOR_DECISION);

                    if (operatorDecision != null && operatorDecision.EndTime.HasValue && operatorDecision.StartTime.HasValue && operatorDecision.IdMeter.HasValue)
                    {
                        var fuelBalanceSir = sirFuelBalanceList.FirstOrDefault(s =>
                                s.TankId == operatorDecision.IdMeter.Value &&
                                s.RangeStartDateUTC == ConvertTimeZoneToUtcTime(operatorDecision.StartTime) &&
                                s.RangeEndDateUTC == ConvertTimeZoneToUtcTime(operatorDecision.EndTime));

                        if (fuelBalanceSir != null)
                        {
                            fuelBalanceSir.UpdateOperatorDecision(operatorDecisionDataForMeter.ToList());
                            // Update existing SIR decision about operator decision
                            WEsbFP.SaveFuelBalancesSir(fuelBalanceSir);
                        }
                    }
                }
            }
            // Remove saved data temporals
            toBeSaved = toBeSaved.Where(d => !sir2OperatorDecisionDataTypes.Contains(d.IdDataType) && d.IdAggregationType == (int)Enums.AggregationType.Month).ToArray();
            #endregion
            #endregion

            #region data_temporal -- todo
            // todo
            //base.SaveDataTemporal(toBeSaved, commandTimeout); // zakomentowane, zeby dla bezpieczenstwa nie zapisywac do bazy IMR obiektu DataTemporal pochodzacego z FP
            #endregion
        }
        #endregion

        #region GetDataTemporalDevicePerformanceFilter
        public override List<OpDataTemporal> GetDataTemporalDevicePerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false,
                            long[] IdDataTemporal = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null,
                            int[] IdAggregationType = null, int[] IdDataSourceType = null,
                            int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return base.GetDataTemporalDevicePerformanceFilter(loadNavigationProperties, mergeIntoCache, IdDataTemporal, SerialNbr, IdDataType, IndexNbr,
                IdAggregationType, IdDataSourceType, Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout);
        }
        #endregion

        #region GetDataTemporalByDistributor
        public override List<OpDataTemporal> GetDataTemporalByDistributor(bool loadNavigationProperties = true,
                            Enums.ReferenceType FilterOption = Enums.ReferenceType.None, int[] IdDistributor = null, long[] IdDataType = null, int[] IdAggregationType = null,
                            string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            return base.GetDataTemporalByDistributor(loadNavigationProperties, FilterOption, IdDistributor, IdDataType, IdAggregationType,
                customWhereClause, autoTransaction, transactionLevel, commandTimeout);
        }
        #endregion
        #endregion

        #region DataArch
        public override void DeleteDataArchFilter(long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, long[] IdAlarmEvent = null, int[] Status = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
#if BASE_DATA_ARCH
            base.DeleteDataArchFilter(IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr, Time, IdDataSource, IdDataSourceType, IdAlarmEvent, Status, customWhereClause, autoTransaction, transactionLevel, commandTimeout);
#else
            //base.DeleteDataArchFilter(IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr, Time, IdDataSource, IdDataSourceType, IdAlarmEvent, Status, customWhereClause, autoTransaction, transactionLevel, commandTimeout); // zakomentowane, zeby dla bezpieczenstwa nie usuwac z bazy IMR obiektu DataArch pochodzacego z FP
#endif
        }

        public override long SaveDataArch(OpDataArch toBeSaved, bool useDBCollector = false)
        {
#if BASE_DATA_ARCH
            return base.SaveDataArch(toBeSaved, useDBCollector);
#else
            //return base.SaveDataArch(toBeSaved, useDBCollector); // zakomentowane, zeby dla bezpieczenstwa nie zapisywac do bazy IMR obiektu DataArch pochodzacego z FP
            return -1;
#endif
        }

        public override void SaveDataArch(OpDataArch[] toBeSaved, int commandTimeout = 0, bool useDBCollector = false)
        {
#if BASE_DATA_ARCH
            base.SaveDataArch(toBeSaved, commandTimeout, useDBCollector);
#else
            // base.SaveDataArch(toBeSaved, commandTimeout, useDBCollector); // zakomentowane, zeby dla bezpieczenstwa nie zapisywac do bazy IMR obiektu DataArch pochodzacego z FP
#endif
        }
        #endregion
    }
}