//#define BASE_DATA_ARCH

using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;
using DW = IMR.Suite.UI.Business.Objects.DW;
using FP = IMR.Suite.Services.WEsbProvider.FP;
using DWH = IMR.Suite.Services.WEsbProvider.DWH;
using Common = IMR.Suite.Common;
using IMR.Suite.UI.Business.Components.DW;

namespace IMR.Suite.UI.Business
{
    public partial class DataProviderFP
    {
        #region GetRefuelDataFilter
        /// <summary>
        /// Gets RefuelData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRefuelData funcion</param>
        /// <param name="IdRefuelData">Specifies filter for ID_REFUEL_DATA column</param>
        /// <param name="IdRefuel">Specifies filter for ID_REFUEL column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFUEL_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RefuelData list</returns>
        public override List<OpRefuelData> GetRefuelDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRefuelData = null, long[] IdRefuel = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            List<OpRefuelData> refuelDataFromDict = new List<OpRefuelData>();
            if (IdRefuelData != null && IdRefuelData.Length > 0) // Wezmy z cache skoro konkretne id potrzebne
            {
                foreach (var refuelDataId in IdRefuelData)
                {
                    OpRefuelData refuelData = RefuelDataDict.TryGetValue(refuelDataId);
                    if (refuelData != null)
                        refuelDataFromDict.Add(refuelData);
                }
            }
            else if (IdRefuel != null && IdRefuel.Length > 0)
            {
                Dictionary<long, long> idRefuelDict = IdRefuel.Distinct().ToDictionary(q => q);
                refuelDataFromDict.AddRange(RefuelDataDict.Where(q => idRefuelDict.ContainsKey(q.Value.IdRefuel)).Select(q => q.Value));
            }
            else // Trzeba wziasc wszystkie
            {
                refuelDataFromDict.AddRange(RefuelDataDict.Values.ToList());
            }

            // [!] Nie ma tu id location ani nawet id meter by pobra� co� z szyny...
            // [?] Trzeba by zrobi� metod� na szynie, kt�ra zwr�ci wszystkie dostawy z wybranego przedzia�u czasowego.

            #region Filtrowanie
            IEnumerable<OpRefuelData> refuelDataFilter = refuelDataFromDict.AsEnumerable();
            if (IdRefuel != null && IdRefuel.Length > 0)
                refuelDataFilter = refuelDataFilter.Where(d => IdRefuel.Contains(d.IdRefuel));
            if (IdDataType != null && IdDataType.Length > 0)
                refuelDataFilter = refuelDataFilter.Where(d => IdDataType.Contains(d.IdDataType));
            if (IndexNbr != null && IndexNbr.Length > 0)
                refuelDataFilter = refuelDataFilter.Where(d => IndexNbr.Contains(d.Index));
            if (Status != null && Status.Length > 0)
                refuelDataFilter = refuelDataFilter.Where(d => d.Status.HasValue &&  Status.Contains(d.Status.Value));
            if (StartTime != null)
            {
                #region StartTime
                switch (StartTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Null:
                        refuelDataFilter = refuelDataFilter.Where(d => !d.StartTime.HasValue);
                        break;
                    case TypeDateTimeCode.ModeType.Between:
                        if (StartTime.DateTime1.HasValue && StartTime.DateTime2.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value >= StartTime.DateTime1.Value && d.StartTime.Value <= StartTime.DateTime2.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Different:
                        if (StartTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value != StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        if (StartTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value == StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                        if (StartTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value > StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        if (StartTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value >= StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                        if (StartTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value < StartTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        if (StartTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.StartTime.HasValue && d.StartTime.Value <= StartTime.DateTime1.Value);
                        break;
                }
                #endregion
            }
            if (EndTime != null)
            {
                #region EndTime
                switch (EndTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Null:
                        refuelDataFilter = refuelDataFilter.Where(d => !d.EndTime.HasValue);
                        break;
                    case TypeDateTimeCode.ModeType.Between:
                        if (EndTime.DateTime1.HasValue && EndTime.DateTime2.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value >= EndTime.DateTime1.Value && d.EndTime.Value <= EndTime.DateTime2.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Different:
                        if (EndTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value != EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        if (EndTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value == EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                        if (EndTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value > EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        if (EndTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value >= EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                        if (EndTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value < EndTime.DateTime1.Value);
                        break;
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        if (EndTime.DateTime1.HasValue)
                            refuelDataFilter = refuelDataFilter.Where(d => d.EndTime.HasValue && d.EndTime.Value <= EndTime.DateTime1.Value);
                        break;
                }
                #endregion
            }
            #endregion

            return refuelDataFilter.ToList();
        }
        #endregion

        #region GetDataTemporalFilter
        /// <summary>
        /// Gets DataTemporal list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTemporal funcion</param>
        /// <param name="IdDataTemporal">Specifies filter for ID_DATA_TEMPORAL column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TEMPORAL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTemporal list</returns>
        public override List<OpDataTemporal> GetDataTemporalFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataTemporal = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, int[] IdAggregationType = null, long[] IdDataSource = null, int[] IdDataSourceType = null,
                            long[] IdAlarmEvent = null, int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            List<OpDataTemporal> returnList = new List<OpDataTemporal>();

            #region Fuel balance start/end time - WYLACZONE
            // propozycja podawania czasow do metod FuelBalance, zakomentowana
            //DateTime? fuelBalanceStartTime = null;
            //DateTime? fuelBalanceEndTime = null;
            //TypeDateTimeCode fuelBalanceDateTimeCode = null;
            //if (EndTime != null)
            //    fuelBalanceDateTimeCode = new TypeDateTimeCode() { Mode = EndTime.Mode, DateTime1 = EndTime.DateTime1, DateTime2 = EndTime.DateTime2 };
            //else if (StartTime != null)
            //    fuelBalanceDateTimeCode = new TypeDateTimeCode() { Mode = StartTime.Mode, DateTime1 = StartTime.DateTime1, DateTime2 = StartTime.DateTime2 };

            //if (fuelBalanceDateTimeCode != null)
            //{
            //    switch (fuelBalanceDateTimeCode.Mode)
            //    {
            //        case TypeDateTimeCode.ModeType.Between:
            //            fuelBalanceStartTime = fuelBalanceDateTimeCode.DateTime1;
            //            fuelBalanceEndTime = fuelBalanceDateTimeCode.DateTime2;
            //            break;
            //        case TypeDateTimeCode.ModeType.Equal:
            //            fuelBalanceStartTime = fuelBalanceDateTimeCode.DateTime1;
            //            fuelBalanceEndTime = fuelBalanceDateTimeCode.DateTime1;
            //            break;
            //        case TypeDateTimeCode.ModeType.Greater:
            //        case TypeDateTimeCode.ModeType.GreaterEqual:
            //            fuelBalanceStartTime = fuelBalanceDateTimeCode.DateTime1;
            //            break;
            //        case TypeDateTimeCode.ModeType.Lesser:
            //        case TypeDateTimeCode.ModeType.LesserEqual:
            //            fuelBalanceEndTime = fuelBalanceDateTimeCode.DateTime1;
            //            break;
            //    }
            //}

            //fuelBalanceStartTime = ConvertTimeZoneToUtcTime(fuelBalanceStartTime);
            //fuelBalanceEndTime = ConvertTimeZoneToUtcTime(fuelBalanceEndTime);
            #endregion
            #region Fuel balance start/end time - po staremu, WLACZONE
            DateTime? rangeStart = null, rangeEnd = null;
            DateTime? startTimeEqual = null;
            if (StartTime != null)
            {
                rangeStart = StartTime.DateTime1;
                rangeEnd = StartTime.DateTime2;

                if (StartTime.Mode == TypeDateTimeCode.ModeType.Equal)
                    startTimeEqual = StartTime.DateTime1;
            }

            DateTime? endTimeEqual = null;
            if (EndTime != null)
            {
                rangeStart = EndTime.DateTime1;
                rangeEnd = EndTime.DateTime2;

                if (EndTime.Mode == TypeDateTimeCode.ModeType.Equal)
                    endTimeEqual = EndTime.DateTime1;
            }
            #endregion

            #region Data temporal start/end times
            DateTime? dataTemporalStartFrom = null;
            DateTime? dataTemporalStartTo = null;
            DateTime? dataTemporalEndFrom = null;
            DateTime? dataTemporalEndTo = null;
            if (StartTime != null)
            {
                switch (StartTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Between:
                        dataTemporalStartFrom = StartTime.DateTime1;
                        dataTemporalStartTo = StartTime.DateTime2;
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        dataTemporalStartFrom = StartTime.DateTime1;
                        dataTemporalStartTo = StartTime.DateTime1;
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        dataTemporalStartFrom = StartTime.DateTime1;
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        dataTemporalStartTo = StartTime.DateTime1;
                        break;
                }
            }
            if (EndTime != null)
            {
                switch (EndTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Between:
                        dataTemporalEndFrom = EndTime.DateTime1;
                        dataTemporalEndTo = EndTime.DateTime2;
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        dataTemporalEndFrom = EndTime.DateTime1;
                        dataTemporalEndTo = EndTime.DateTime1;
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        dataTemporalEndFrom = EndTime.DateTime1;
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        dataTemporalEndTo = EndTime.DateTime1;
                        break;
                }
            }
            dataTemporalStartFrom = ConvertTimeZoneToUtcTime(dataTemporalStartFrom);
            dataTemporalStartTo = ConvertTimeZoneToUtcTime(dataTemporalStartTo);
            dataTemporalEndFrom = ConvertTimeZoneToUtcTime(dataTemporalEndFrom);
            dataTemporalEndTo = ConvertTimeZoneToUtcTime(dataTemporalEndTo);
            #endregion

            #region FuelBalanceDataTypes
            long[] fuelBalanceDataTypes = new long[]
            {
                Common.DataType.TEMPORAL_START_TIME,
                Common.DataType.TEMPORAL_END_TIME,
                Common.DataType.FUEL_DEPTH,
                Common.DataType.FUEL_TEMPERATURE,
                Common.DataType.AWSR_FUEL_POS_VOLUME_DISPENCED,
                Common.DataType.AWSR_DISPENSED_DISTR_OFFSET,
                Common.DataType.AWSR_OUTCOME_LOST_OFFSET,
                Common.DataType.AWSR_OUTCOME_RETURN_OFFSET,
                Common.DataType.AWSR_DISPENSED_POS_OFFSET,
                Common.DataType.METER_MEDIUM_REFERENCE_DENSITY,
                Common.DataType.AWSR_FUEL_REF_VOLUME,
                Common.DataType.AWSR_RECONCILIATION_REF_CURRENT_ERROR,
                Common.DataType.AWSR_RECONCILIATION_REF_CURRENT_PER_ERROR,
                Common.DataType.AWSR_FUEL_REF_VOLUME_DELIVERED,
                Common.DataType.AWSR_FUEL_RAW_REF_VOLUME_DISPENCED,
                Common.DataType.AWSR_FUEL_REF_VOLUME_OUTCOME,
                Common.DataType.AWSR_FUEL_VOLUME,
                Common.DataType.AWSR_RECONCILIATION_CURRENT_ERROR,
                Common.DataType.AWSR_RECONCILIATION_CURRENT_PER_ERROR,
                Common.DataType.AWSR_FUEL_VOLUME_DELIVERED,
                Common.DataType.AWSR_FUEL_RAW_VOLUME_DISPENCED,
                Common.DataType.AWSR_FUEL_VOLUME_OUTCOME,
                Common.DataType.AWSR_FUEL_VOLUME_DISPENCED,
                Common.DataType.AWSR_FUEL_REF_VOLUME_DISPENCED,
                Common.DataType.AWSR_RECONCILIATION_SALES_CURRENT_ERROR,
                Common.DataType.AWSR_RECONCILIATION_SALES_REF_CURRENT_ERROR,
                Common.DataType.AWSR_FUEL_COMP_VOLUME_DISPENCED,
                Common.DataType.FUEL_VOLUME,
                Common.DataType.AWSR_RECONCILIATION_DAY,
                Common.DataType.AWSR_RECONCILIATION_NOTES,
                Common.DataType.AWSR_RECONCILIATION_STATUS,
                Common.DataType.NOZZLE_TOTAL_COUNTER
            };
            #endregion
            #region SirDataTypes
            long[] sir2DataTypes = new long[]
            {
                Common.DataType.AWSR_IRC_SIR2_DECISION,
                Common.DataType.AWSR_IRC_SIR2_DECISION_DATE,
                Common.DataType.AWSR_IRC_SIR2_INCONCLUSIVE_CAUSE,
                Common.DataType.AWSR_IRC_SIR2_LEAK_RATE,
                Common.DataType.AWSR_IRC_SIR2_LEAK_THRESHOLD,
                Common.DataType.AWSR_IRC_SIR2_MDLR,
                Common.DataType.AWSR_IRC_SIR2_METHOD_USED,
                Common.DataType.AWSR_IRC_SIR2_OFFSET,
                Common.DataType.AWSR_IRC_SIR2_OUTLIER_THRESHOLD,
                Common.DataType.AWSR_IRC_SIR2_REPORT,
                Common.DataType.AWSR_PROBLEM_ALARM_ID,
                Common.DataType.AWSR_PROBLEM_ISSUE_ID,
                Common.DataType.AWSR_PROBLEM_OPERATOR_DECISION,
                Common.DataType.AWSR_PROBLEM_OPERATOR_DECISION_DESCR,
                Common.DataType.AWSR_PROBLEM_OPERATOR_DECISION_TIME,
                Common.DataType.AWSR_PROBLEM_OPERATOR_ID,
                Common.DataType.AWSR_PROBLEM_CLASS,
                Common.DataType.AWSR_PROBLEM_CRITICALITY,
                Common.DataType.AWSR_PROBLEM_NOTES,
                Common.DataType.AWSR_PROBLEM_PROBABILITY,
                Common.DataType.AWSR_PROBLEM_FUEL_VOLUME,
            };
            #endregion

            // Dotyczy agregatu dziennego i datatypow fuelBalanceDataTypes
            // Jesli IdAggregationType jest null lub puste to oznacza, ze chcemy wszystkie - czyli Day rowniez
            // Jesli IdDataType jest null lub puste to oznacza, ze chcemy wszystkie - czyli fuelBalanceDataTypes rowniez
            bool loadFuelBalance = (IdAggregationType == null || IdAggregationType.Length == 0 || IdAggregationType.Contains((int)Enums.AggregationType.Day))
                && (IdDataType == null || IdDataType.Length == 0 || IdDataType.Intersect(fuelBalanceDataTypes).Any());

            // Dotyczy agregatu miesiecznego i datatypow sir2DataTypes
            // Jesli IdAggregationType jest null lub puste to oznacza, ze chcemy wszystkie - czyli Month rowniez
            // Jesli IdDataType jest null lub puste to oznacza, ze chcemy wszystkie - czyli sir2DataTypes rowniez
            bool loadSir2 = (IdAggregationType == null || IdAggregationType.Length == 0 || IdAggregationType.Contains((int)Enums.AggregationType.Month))
                && (IdDataType == null || IdDataType.Length == 0 || IdDataType.Intersect(sir2DataTypes).Any());

            bool loadDataTemporal = IdAggregationType == null || IdAggregationType.Length == 0 || IdAggregationType.Any(q => q != (int)Enums.AggregationType.Day && q != (int)Enums.AggregationType.Month)
                || IdDataType == null || IdDataType.Length == 0 || IdDataType.Any(q => !fuelBalanceDataTypes.Contains(q) && !sir2DataTypes.Contains(q));

            // Metoda szyny do FuelBalance/Sir2 nie przyjmuje pustej listy idLocation, wiec doladowujemy
            long[] idLocation = IdLocation;
            if ((idLocation == null || idLocation.Length == 0) && (loadFuelBalance || loadSir2))
                idLocation = Components.CORE.LocationComponent.GetLocationIdsByMeterIds(this, IdMeter).ToArray();

            #region fuel_balance section
            if (loadFuelBalance)
            {
                // Pobieranie z szyny
                List<FP.Objects.FuelBalanceHeader> fuelBalancesHeaders = null;
                if (idLocation == null || idLocation.Length == 0)
                    fuelBalancesHeaders = WEsbFP.GetFuelBalances(idLocation, rangeStart, rangeEnd, commandTimeout);
                else
                {
                    fuelBalancesHeaders = new List<FP.Objects.FuelBalanceHeader>();
                    foreach (IEnumerable<long> id in idLocation.Partition(500))
                    {
                        List<FP.Objects.FuelBalanceHeader> tmpFuelBalancesHeaders = WEsbFP.GetFuelBalances(id.ToArray(), rangeStart, rangeEnd, commandTimeout);
                        if (tmpFuelBalancesHeaders != null)
                            fuelBalancesHeaders.AddRange(tmpFuelBalancesHeaders);
                    }
                }

                // Konwersja
                if (fuelBalancesHeaders != null)
                {
                    returnList.AddRange(FP.Objects.FuelBalanceHeader.ToListOpDataTemporal(this, fuelBalancesHeaders));
                    if (IdDataType == null || IdDataType.Length == 0 || IdDataType.Contains(Common.DataType.NOZZLE_TOTAL_COUNTER))
                    {
                        List<FP.Objects.FuelBalance> fuelBalanceList = fuelBalancesHeaders.Where(q => q.FuelBalances != null && q.FuelBalances.Any(w => w.Id.HasValue)).SelectMany(q => q.FuelBalances.Where(w => w.Id.HasValue).Select(w => w)).ToList();
                        List<long> fuelBalanceIdList = fuelBalanceList.Select(q => q.Id.Value).ToList();
                        if (fuelBalanceIdList.Count > 0)
                        {
                            List<FP.Objects.DispenserMeasurement> nozzleList = new List<FP.Objects.DispenserMeasurement>();
                            foreach (IEnumerable<long> id in fuelBalanceIdList.Partition(500))
                            {
                                List<FP.Objects.DispenserMeasurement> tmpNozzleList = WEsbFP.GetDispenserMeasurements(id.ToArray(), null, commandTimeout);
                                if (tmpNozzleList != null)
                                    nozzleList.AddRange(tmpNozzleList);
                            }
                            if (nozzleList != null)
                                returnList.AddRange(FP.Objects.DispenserMeasurement.ToListOpDataTemporal(this, nozzleList, fuelBalanceList));
                        }
                    }
                }
            }
            #endregion

            #region fuel_balance_sir2 section
            if (loadSir2)
            {
                if (startTimeEqual != null && endTimeEqual != null)
                {
                    rangeStart = startTimeEqual;
                    rangeEnd = endTimeEqual;
                }
                else if (StartTime != null)
                {
                    if (StartTime.DateTime1.HasValue)
                        rangeStart = StartTime.DateTime1.Value.AddDays(28);
                    if (StartTime.DateTime2.HasValue)
                        rangeEnd = StartTime.DateTime2.Value.AddDays(30);
                }

                if (EndTime != null)
                {
                    if (EndTime.DateTime1.HasValue)
                        rangeStart = EndTime.DateTime1.Value.AddDays(-1);
                }

                // Pobieranie z szyny
                List<FP.Objects.FuelBalanceSir> fuelBalanceSirList = null;
                if (idLocation == null || idLocation.Length == 0)
                    fuelBalanceSirList = WEsbFP.GetFuelBalanceSir2(idLocation, rangeStart, rangeEnd, commandTimeout);
                else
                {
                    fuelBalanceSirList = new List<FP.Objects.FuelBalanceSir>();
                    foreach (IEnumerable<long> id in idLocation.Partition(500))
                    {
                        List<FP.Objects.FuelBalanceSir> tmpFuelBalanceSirList = WEsbFP.GetFuelBalanceSir2(id.ToArray(), rangeStart, rangeEnd, commandTimeout);
                        if (tmpFuelBalanceSirList != null)
                            fuelBalanceSirList.AddRange(tmpFuelBalanceSirList);
                    }
                }

                // Konwersja
                if (fuelBalanceSirList != null)
                    returnList.AddRange(FP.Objects.FuelBalanceSir.ToListOpDataTemporal(this, fuelBalanceSirList));

                // Pobieranie reszty danych o alarmach
                List<FP.Objects.Alarm> alarms = null;
                if (idLocation == null || idLocation.Length == 0)
                    alarms = WEsbFP.GetAlarms(idLocation, null, rangeStart, rangeEnd);
                else
                {
                    alarms = new List<FP.Objects.Alarm>();
                    foreach (IEnumerable<long> id in idLocation.Partition(500))
                    {
                        List<FP.Objects.Alarm> tmpAlarms = WEsbFP.GetAlarms(id.ToArray(), null, rangeStart, rangeEnd);
                        if (tmpAlarms != null)
                            alarms.AddRange(tmpAlarms);
                    }
                }

                // Konwersja
                if (alarms != null)
                    returnList.AddRange(FP.Objects.Alarm.ToListOpDataTemporal(alarms));
            }
            #endregion
            
            #region data_temporal section
            if (loadDataTemporal)
            {
                List<DWH.Objects.DataTemporal> dataTemporalList = WEsbDWH.GetDataTemporalFilter(IdLocation, IdMeter, IdDataType, IdAggregationType, IdDataSourceType, IdDataSource, SerialNbr,
                    IndexNbr, Status, IdAlarmEvent, dataTemporalStartFrom, dataTemporalStartTo, dataTemporalEndFrom, dataTemporalEndTo, commandTimeout);
                if (dataTemporalList != null)
                {
                    if (loadFuelBalance)
                    {
                        // zaladowalismy FuelBalance - usun z wynikow wszystko co ma Day i pochodzi z fuelBalanceDataTypes
                        dataTemporalList = dataTemporalList.Where(q => q.IdAggregationType.HasValue && q.IdDataType.HasValue
                            && (q.IdAggregationType.Value != (int)Enums.AggregationType.Day || (q.IdAggregationType.Value == (int)Enums.AggregationType.Day
                            && !fuelBalanceDataTypes.Contains(q.IdDataType.Value)))).ToList();
                    }
                    if (loadSir2)
                    {
                        // zaladowalismy Sir2 - usun z wynikow wszystko co ma Month i pochodzi z sir2DataTypes
                        dataTemporalList = dataTemporalList.Where(q => q.IdAggregationType.HasValue && q.IdDataType.HasValue
                            && (q.IdAggregationType.Value != (int)Enums.AggregationType.Month || (q.IdAggregationType.Value == (int)Enums.AggregationType.Month
                            && !sir2DataTypes.Contains(q.IdDataType.Value)))).ToList();
                    }
                    List<OpDataTemporal> tempList = DWH.Objects.DataTemporal.ToListOpDataTemporal(dataTemporalList);
                    tempList.ForEach(q =>
                        {
                            q.StartTime = ConvertUtcTimeToTimeZone(q.StartTime);
                            q.EndTime = ConvertUtcTimeToTimeZone(q.EndTime);
                        });
                    returnList.AddRange(tempList);
                }
            }
            #endregion

            #region Filter OpDataTemporal list
            if (IdMeter != null && IdMeter.Length > 0)
                returnList = returnList.Where(d => d.IdMeter.HasValue && IdMeter.Contains(d.IdMeter.Value)).ToList();
            if (IdLocation != null && IdLocation.Length > 0)
                returnList = returnList.Where(d => d.IdLocation.HasValue && IdLocation.Contains(d.IdLocation.Value)).ToList();
            if (SerialNbr != null && SerialNbr.Length > 0)
                returnList = returnList.Where(d => d.SerialNbr.HasValue && SerialNbr.Contains(d.SerialNbr.Value)).ToList();
            if (IdDataType != null && IdDataType.Length > 0)
                returnList = returnList.Where(d => IdDataType.Contains(d.IdDataType)).ToList();
            if (IndexNbr != null && IndexNbr.Length > 0)
                returnList = returnList.Where(d => IndexNbr.Contains(d.IndexNbr)).ToList();
            if (IdDataSource != null && IdDataSource.Length > 0)
                returnList = returnList.Where(d => d.IdDataSource.HasValue && IdDataSource.Contains(d.IdDataSource.Value)).ToList();
            if (IdDataSourceType != null && IdDataSourceType.Length > 0)
                returnList = returnList.Where(d => d.IdDataSourceType.HasValue && IdDataSourceType.Contains(d.IdDataSourceType.Value)).ToList();
            if (IdAggregationType != null && IdAggregationType.Length > 0)
                returnList = returnList.Where(d => d.IdAggregationType.HasValue && IdAggregationType.Contains(d.IdAggregationType.Value)).ToList();
            if (Status != null && Status.Length > 0)
                returnList = returnList.Where(d => Status.Contains((int)d.DataStatus)).ToList();
            if (IdAlarmEvent != null && IdAlarmEvent.Length > 0)
                returnList = returnList.Where(d => d.IdAlarmEvent.HasValue && IdAlarmEvent.Contains(d.IdAlarmEvent.Value)).ToList();
            if (StartTime != null)
            {
                switch (StartTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Null:
                        returnList = returnList.Where(d => !d.StartTime.HasValue).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Between:
                        if (StartTime.DateTime1.HasValue && StartTime.DateTime2.HasValue)
                            returnList = returnList.Where(d => d.StartTime.HasValue && d.StartTime.Value >= StartTime.DateTime1.Value && d.StartTime.Value <= StartTime.DateTime2.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Different:
                        if (StartTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.StartTime.HasValue && d.StartTime.Value != StartTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        if (StartTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.StartTime.HasValue && d.StartTime.Value == StartTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                        if (StartTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.StartTime.HasValue && d.StartTime.Value > StartTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        if (StartTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.StartTime.HasValue && d.StartTime.Value >= StartTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                        if (StartTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.StartTime.HasValue && d.StartTime.Value < StartTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        if (StartTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.StartTime.HasValue && d.StartTime.Value <= StartTime.DateTime1.Value).ToList();
                        break;
                }
            }
            if (EndTime != null)
            {
                switch (EndTime.Mode)
                {
                    case TypeDateTimeCode.ModeType.Null:
                        returnList = returnList.Where(d => !d.EndTime.HasValue).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Between:
                        if (EndTime.DateTime1.HasValue && EndTime.DateTime2.HasValue)
                            returnList = returnList.Where(d => d.EndTime.HasValue && d.EndTime.Value >= EndTime.DateTime1.Value && d.EndTime.Value <= EndTime.DateTime2.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Different:
                        if (EndTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.EndTime.HasValue && d.EndTime.Value != EndTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        if (EndTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.EndTime.HasValue && d.EndTime.Value == EndTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                        if (EndTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.EndTime.HasValue && d.EndTime.Value > EndTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        if (EndTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.EndTime.HasValue && d.EndTime.Value >= EndTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                        if (EndTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.EndTime.HasValue && d.EndTime.Value < EndTime.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        if (EndTime.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.EndTime.HasValue && d.EndTime.Value <= EndTime.DateTime1.Value).ToList();
                        break;
                }
            }
            #endregion

            if (loadNavigationProperties && returnList.Count > 0)
                OpDataTemporal.LoadNavigationProperties(ref returnList, this);

            return returnList;
        }        
        #endregion

        #region GetDataArchFilter
        public override List<DW.OpDataArch> GetDataArchFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, 
            long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] 
            IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, 
            long[] IdAlarmEvent = null, int[] Status = null, long? topCount = null, string customWhereClause = null, 
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false, bool loadCustomData = true)
        {
#if BASE_DATA_ARCH
            return base.GetDataArchFilter(loadNavigationProperties, mergeIntoCache, IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr, Time, IdDataSource,
                IdDataSourceType, IdAlarmEvent, Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout, useDBCollector);
#else
            List<DW.OpDataArch> returnList = new List<DW.OpDataArch>();

            #region Data arch times
            DateTime? dataArchTimeFrom = null;
            DateTime? dataArchTimeTo = null;
            if (Time != null)
            {
                switch (Time.Mode)
                {
                    case TypeDateTimeCode.ModeType.Between:
                        dataArchTimeFrom = Time.DateTime1;
                        dataArchTimeTo = Time.DateTime2;
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        dataArchTimeFrom = Time.DateTime1;
                        dataArchTimeTo = Time.DateTime1;
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        dataArchTimeFrom = Time.DateTime1;
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        dataArchTimeTo = Time.DateTime1;
                        break;
                }
            }
            dataArchTimeFrom = ConvertTimeZoneToUtcTime(dataArchTimeFrom);
            dataArchTimeTo = ConvertTimeZoneToUtcTime(dataArchTimeTo);
            #endregion

            long[] eventMeterDataTypes = new long[]
            {
                Common.DataType.AWSR_FP_EVENT_DISCHARGED,
                Common.DataType.AWSR_FP_EVENT_DISCHARGED_VOLUME
            };

            // Jesli IdDataType jest null lub puste to oznacza, ze chcemy wszystkie - czyli eventMeterDataTypes rowniez
            bool loadEventMeter = IdDataType == null || IdDataType.Length == 0 || IdDataType.Intersect(eventMeterDataTypes).Any();

            bool loadDataArch = IdDataType == null || IdDataType.Length == 0 || IdDataType.Any(q => !eventMeterDataTypes.Contains(q));

            // Metoda szyny do EventMeter nie przyjmuje pustej listy idLocation, wiec doladowujemy
            long[] idLocation = IdLocation;
            if ((idLocation == null || idLocation.Length == 0) && loadEventMeter)
                idLocation = Components.CORE.LocationComponent.GetLocationIdsByMeterIds(this, IdMeter).ToArray();

            #region EventMeter
            if (loadEventMeter)
            {
                // Pobieranie z szyny
                List<FP.Objects.Event> events = null;
                if (idLocation == null || idLocation.Length == 0)
                    events = WEsbFP.GetEvents(idLocation, dataArchTimeFrom, dataArchTimeTo, commandTimeout);
                else
                {
                    events = new List<FP.Objects.Event>();
                    foreach (IEnumerable<long> id in idLocation.Partition(500))
                    {
                        List<FP.Objects.Event> tmpEvents = WEsbFP.GetEvents(id.ToArray(), dataArchTimeFrom, dataArchTimeTo, commandTimeout);
                        if (tmpEvents != null)
                            events.AddRange(tmpEvents);
                    }
                }

                // Konwersja
                if (events != null)
                    returnList.AddRange(FP.Objects.Event.ToListOpDataArch(this, events));
            }
            #endregion

            #region DataArch
            if (loadDataArch)
            {
                List<DWH.Objects.DataArch> dataArchList = WEsbDWH.GetDataArchFilter(IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType,
                    IndexNbr, dataArchTimeFrom, dataArchTimeTo, IdDataSource, IdDataSourceType, IdAlarmEvent, Status,
                    topCount, customWhereClause, commandTimeout);
                if (dataArchList != null)
                {
                    if (loadEventMeter)
                    {
                        // zaladowalismy FuelBalance - usun z wynikow wszystko co pochodzi z eventMeterDataTypes
                        dataArchList = dataArchList.Where(q => q.IdDataType.HasValue
                            && !eventMeterDataTypes.Contains(q.IdDataType.Value)).ToList();
                    }

                    List<DW.OpDataArch> tempList = DWH.Objects.DataArch.ToListOpDataArch(dataArchList);
                    tempList.ForEach(q =>
                    {
                        q.Time = ConvertUtcTimeToTimeZone(q.Time);
                    });
                    returnList.AddRange(tempList);
                }
            }
            #endregion

            #region Filter OpDataArch list
            if (IdMeter != null && IdMeter.Length > 0)
                returnList = returnList.Where(d => d.IdMeter.HasValue && IdMeter.Contains(d.IdMeter.Value)).ToList();
            if (IdLocation != null && IdLocation.Length > 0)
                returnList = returnList.Where(d => d.IdLocation.HasValue && IdLocation.Contains(d.IdLocation.Value)).ToList();
            if (SerialNbr != null && SerialNbr.Length > 0)
                returnList = returnList.Where(d => d.SerialNbr.HasValue && SerialNbr.Contains(d.SerialNbr.Value)).ToList();
            if (IdDataType != null && IdDataType.Length > 0)
                returnList = returnList.Where(d => IdDataType.Contains(d.IdDataType)).ToList();
            if (IndexNbr != null && IndexNbr.Length > 0)
                returnList = returnList.Where(d => IndexNbr.Contains(d.IndexNbr)).ToList();
            if (IdDataSource != null && IdDataSource.Length > 0)
                returnList = returnList.Where(d => d.IdDataSource.HasValue && IdDataSource.Contains(d.IdDataSource.Value)).ToList();
            if (IdDataSourceType != null && IdDataSourceType.Length > 0)
                returnList = returnList.Where(d => d.IdDataSourceType.HasValue && IdDataSourceType.Contains(d.IdDataSourceType.Value)).ToList();
            if (Status != null && Status.Length > 0)
                returnList = returnList.Where(d => Status.Contains((int)d.DataStatus)).ToList();
            if (IdAlarmEvent != null && IdAlarmEvent.Length > 0)
                returnList = returnList.Where(d => d.IdAlarmEvent.HasValue && IdAlarmEvent.Contains(d.IdAlarmEvent.Value)).ToList();
            if (Time != null)
            {
                switch (Time.Mode)
                {
                    case TypeDateTimeCode.ModeType.Null:
                        break;
                    case TypeDateTimeCode.ModeType.Between:
                        if (Time.DateTime1.HasValue && Time.DateTime2.HasValue)
                            returnList = returnList.Where(d => d.Time >= Time.DateTime1.Value && d.Time <= Time.DateTime2.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Different:
                        if (Time.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.Time != Time.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Equal:
                        if (Time.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.Time == Time.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Greater:
                        if (Time.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.Time > Time.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.GreaterEqual:
                        if (Time.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.Time >= Time.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.Lesser:
                        if (Time.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.Time < Time.DateTime1.Value).ToList();
                        break;
                    case TypeDateTimeCode.ModeType.LesserEqual:
                        if (Time.DateTime1.HasValue)
                            returnList = returnList.Where(d => d.Time <= Time.DateTime1.Value).ToList();
                        break;
                }
            }
            #endregion

            if (loadNavigationProperties && returnList.Count > 0)
                DW.OpDataArch.LoadNavigationProperties(ref returnList, this);
            if (loadCustomData && returnList.Count > 0)
                DW.OpDataArch.LoadCustomData(ref returnList, this);

            return returnList;
#endif
        }
        #endregion
    }
}