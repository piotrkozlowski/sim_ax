using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using IMR.Suite.Data.DB;
using IMR.Suite.Common;

using IMR.Suite.UI.Business.Objects;

using WEsbFP = IMR.Suite.Services.WEsbProvider.FP.WEsbProviderFP;
using WEsbDWH = IMR.Suite.Services.WEsbProvider.DWH.WEsbProviderDWH;

namespace IMR.Suite.UI.Business
{
    public partial class DataProviderFP : DataProvider
    {
        WEsbFP WEsbFP;
        WEsbDWH WEsbDWH;

        #region CtorFP
        public DataProviderFP(DBCommonCORE dbConnectionCore, string baseAddress, string username, string password)
            : base(dbConnectionCore) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, Enums.Language userLanguage, string baseAddress, string username, string password)
            : base(dbConnectionCore, userLanguage) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, Enums.Language userLanguage, TimeSpan timeZoneOffset, string baseAddress, string username, string password)
            : base(dbConnectionCore, userLanguage, timeZoneOffset) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, string baseAddress, string username, string password)
            : base(dbConnectionCore, dbConnectionDW) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, Enums.Language userLanguage, string baseAddress, string username, string password)
            : base(dbConnectionCore, dbConnectionDW, userLanguage) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, Enums.Language userLanguage, TimeSpan timeZoneOffset, string baseAddress, string username, string password)
            : base(dbConnectionCore, dbConnectionDW, userLanguage, timeZoneOffset) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, DBCommonDAQ dbConnectionDaq, string baseAddress, string username, string password)
            : base(dbConnectionCore, dbConnectionDW, dbConnectionDaq) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, DBCommonDAQ dbConnectionDaq, Enums.Language userLanguage, string baseAddress, string username, string password)
            : base(dbConnectionCore, dbConnectionDW, dbConnectionDaq, userLanguage) { CreateWEsbProvider(baseAddress, username, password); }
        public DataProviderFP(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, DBCommonDAQ dbConnectionDaq, Enums.Language userLanguage, string baseAddress, string username, string password, TimeSpan? timeZoneOffset, bool initializeUserPart = true, bool initializeDefaultDataTypes = true)
            : base(dbConnectionCore, dbConnectionDW, dbConnectionDaq, userLanguage, timeZoneOffset, initializeUserPart, initializeDefaultDataTypes) { CreateWEsbProvider(baseAddress, username, password); }
        #endregion

        #region CreateWEsbProvider
        private void CreateWEsbProvider(string baseAddress, string userName, string password)
        {
            WEsbFP = new WEsbFP(baseAddress, userName, password);
            WEsbDWH = new WEsbDWH(baseAddress, userName, password);
        }
        #endregion
    }
}
