﻿using System;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DAQ;
using IMR.Suite.Common;
using System.Data;
using DAQ = IMR.Suite.UI.Business.Objects.DAQ;

namespace IMR.Suite.UI.Business
{
    public partial class DataProvider
    {
        #region GetAuditFilterDAQ

        /// <summary>
        /// Gets Audit list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAudit funcion</param>
        /// <param name="IdAudit">Specifies filter for ID_AUDIT column</param>
        /// <param name="BatchId">Specifies filter for BATCH_ID column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="Key1">Specifies filter for KEY1 column</param>
        /// <param name="Key2">Specifies filter for KEY2 column</param>
        /// <param name="Key3">Specifies filter for KEY3 column</param>
        /// <param name="Key4">Specifies filter for KEY4 column</param>
        /// <param name="Key5">Specifies filter for KEY5 column</param>
        /// <param name="Key6">Specifies filter for KEY6 column</param>
        /// <param name="ColumnName">Specifies filter for COLUMN_NAME column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="Host">Specifies filter for HOST column</param>
        /// <param name="Application">Specifies filter for APPLICATION column</param>
        /// <param name="Contextinfo">Specifies filter for CONTEXTINFO column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_AUDIT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Audit list</returns>
        public virtual List<OpAudit> GetAuditFilterDAQ(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAudit = null, long[] BatchId = null, int[] ChangeType = null, string TableName = null, long[] Key1 = null, long[] Key2 = null,
                            long[] Key3 = null, long[] Key4 = null, long[] Key5 = null, long[] Key6 = null, string ColumnName = null,
                            TypeDateTimeCode Time = null, string User = null, string Host = null, string Application = null, string Contextinfo = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAuditFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAuditFilter"));
            }

            try
            {
                List<OpAudit> returnList = OpAudit.ConvertList(dbConnectionDaq.GetAuditFilter(IdAudit, BatchId, ChangeType, TableName, Key1, Key2,
                             Key3, Key4, Key5, Key6, ColumnName,
                             Time, User, Host, Application, Contextinfo, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAudit item in returnList)
                        AuditDictDAQ[item.IdAudit] = item;
                    AuditIsFullCacheDAQ = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAuditFilter", ex);
                throw;
            }

        }
        #endregion
		#region GetConnectionFilter
        /// <summary>
        /// Gets Connection list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConnection funcion</param>
        /// <param name="Id">Specifies filter for ID column</param>
        /// <param name="IdConnection">Specifies filter for ID_CONNECTION column</param>
        /// <param name="Order">Specifies filter for ORDER column</param>
        /// <param name="LinkTypeOut">Specifies filter for LINK_TYPE_OUT column</param>
        /// <param name="LinkTypeIn">Specifies filter for LINK_TYPE_IN column</param>
        /// <param name="RetryAttempt">Specifies filter for RETRY_ATTEMPT column</param>
        /// <param name="TimeAttempt">Specifies filter for TIME_ATTEMPT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Connection list</returns>
        public virtual List<OpConnection> GetConnectionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] Id  = null,int[] IdConnection  = null,int[] Order  = null,int[] LinkTypeOut  = null,int[] LinkTypeIn  = null,int[] RetryAttempt  = null,
					        int[] TimeAttempt  = null,string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConnectionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConnectionFilter"));
            }
	
	        try
	        {
		        List<OpConnection> returnList = OpConnection.ConvertList(dbConnectionDaq.GetConnectionFilter( Id , IdConnection , Order , LinkTypeOut , LinkTypeIn , RetryAttempt ,
					            TimeAttempt , Name , topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
		        if (mergeIntoCache)
                {
                    foreach (OpConnection item in returnList)
				        ConnectionDict[item.Id] = item;
                    ConnectionIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConnectionFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetDataArchTrashStatusFilter

        /// <summary>
        /// Gets DataArchTrashStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataArchTrashStatus funcion</param>
        /// <param name="IdDataArchTrashStatus">Specifies filter for ID_DATA_ARCH_TRASH_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH_TRASH_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataArchTrashStatus list</returns>
        public virtual List<OpDataArchTrashStatus> GetDataArchTrashStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataArchTrashStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataArchTrashStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchTrashStatusFilter"));
            }

            try
            {
                List<OpDataArchTrashStatus> returnList = OpDataArchTrashStatus.ConvertList(dbConnectionDaq.GetDataArchTrashStatusFilter(IdDataArchTrashStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataArchTrashStatus item in returnList)
                        DataArchTrashStatusDict[item.IdDataArchTrashStatus] = item;
                    DataArchTrashStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArchTrashStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPacketStatusFilter

        /// <summary>
        /// Gets PacketStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketStatus funcion</param>
        /// <param name="IdPacketStatus">Specifies filter for ID_PACKET_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKET_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketStatus list</returns>
        public virtual List<OpPacketStatus> GetPacketStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPacketStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPacketStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketStatusFilter"));
            }

            try
            {
                List<OpPacketStatus> returnList = OpPacketStatus.ConvertList(dbConnectionDaq.GetPacketStatusFilter(IdPacketStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPacketStatus item in returnList)
                        PacketStatusDict[item.IdPacketStatus] = item;
                    PacketStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteTableFilter
        /// <summary>
        /// Gets RouteTable list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteTable funcion</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Prefix">Specifies filter for PREFIX column</param>
        /// <param name="Hop">Specifies filter for HOP column</param>
        /// <param name="CostOut">Specifies filter for COST_OUT column</param>
        /// <param name="CostIn">Specifies filter for COST_IN column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteTable list</returns>
        public virtual List<OpRouteTable> GetRouteTableFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdRoute = null, string Address = null, int[] Prefix = null, string Hop = null, int[] CostOut = null, int[] CostIn = null,
                            string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteTableFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTableFilter"));
            }

            try
            {
                List<OpRouteTable> returnList = OpRouteTable.ConvertList(dbConnectionDaq.GetRouteTableFilter(IdRoute, Address, Prefix, Hop, CostOut, CostIn,
                             Name, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData);
                if (mergeIntoCache)
                {
                    foreach (OpRouteTable item in returnList)
                        RouteTableDict[item.IdRoute] = item;
                    RouteTableIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTableFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteTableBackupFilter

        /// <summary>
        /// Gets RouteTableBackup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteTableBackup funcion</param>
        /// <param name="Id">Specifies filter for ID column</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="IdRouteBackup">Specifies filter for ID_ROUTE_BACKUP column</param>
        /// <param name="Order">Specifies filter for ORDER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteTableBackup list</returns>
        public virtual List<OpRouteTableBackup> GetRouteTableBackupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] Id = null, int[] IdRoute = null, int[] IdRouteBackup = null, int[] Order = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteTableBackupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTableBackupFilter"));
            }

            try
            {
                List<OpRouteTableBackup> returnList = OpRouteTableBackup.ConvertList(dbConnectionDaq.GetRouteTableBackupFilter(Id, IdRoute, IdRouteBackup, Order, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteTableBackup item in returnList)
                        RouteTableBackupDict[item.Id] = item;
                    RouteTableBackupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTableBackupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTransmissionDriverDataFilter
        /// <summary>
        /// Gets TransmissionDriverData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionDriverData funcion</param>
        /// <param name="IdTransmissionDriverData">Specifies filter for ID_TRANSMISSION_DRIVER_DATA column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionDriverData list</returns>
        public virtual List<OpTransmissionDriverData> GetTransmissionDriverDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTransmissionDriverData = null, int[] IdTransmissionDriver = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTransmissionDriverDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverDataFilter"));
            }

            try
            {
                List<OpTransmissionDriverData> returnList = OpTransmissionDriverData.ConvertList(dbConnectionDaq.GetTransmissionDriverDataFilter(IdTransmissionDriverData, IdTransmissionDriver, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpTransmissionDriverData item in returnList)
                        TransmissionDriverDataDict[item.IdTransmissionDriverData] = item;
                    TransmissionDriverDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTransmissionDriverTypeFilter

        /// <summary>
        /// Gets TransmissionDriverType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionDriverType funcion</param>
        /// <param name="IdTransmissionDriverType">Specifies filter for ID_TRANSMISSION_DRIVER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionDriverType list</returns>
        public virtual List<OpTransmissionDriverType> GetTransmissionDriverTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionDriverType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTransmissionDriverTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverTypeFilter"));
            }

            try
            {
                List<OpTransmissionDriverType> returnList = OpTransmissionDriverType.ConvertList(dbConnectionDaq.GetTransmissionDriverTypeFilter(IdTransmissionDriverType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpTransmissionDriverType item in returnList)
                        TransmissionDriverTypeDict[item.IdTransmissionDriverType] = item;
                    TransmissionDriverTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTransmissionProtocolFilter

        /// <summary>
        /// Gets TransmissionProtocol list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionProtocol funcion</param>
        /// <param name="IdTransmissionProtocol">Specifies filter for ID_TRANSMISSION_PROTOCOL column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Order">Specifies filter for ORDER column</param>
        /// <param name="IdProtocolDriver">Specifies filter for ID_PROTOCOL_DRIVER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_PROTOCOL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionProtocol list</returns>
        public virtual List<OpTransmissionProtocol> GetTransmissionProtocolFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionProtocol = null, int[] IdTransmissionDriver = null, int[] Order = null, int[] IdProtocolDriver = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTransmissionProtocolFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionProtocolFilter"));
            }

            try
            {
                List<OpTransmissionProtocol> returnList = OpTransmissionProtocol.ConvertList(dbConnectionDaq.GetTransmissionProtocolFilter(IdTransmissionProtocol, IdTransmissionDriver, Order, IdProtocolDriver, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpTransmissionProtocol item in returnList)
                        TransmissionProtocolDict[item.IdTransmissionProtocol] = item;
                    TransmissionProtocolIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionProtocolFilter", ex);
                throw;
            }

        }
        #endregion
    }

    //User part
    public partial class DataProvider
    {
        #region GetSystemDataFilterDAQ

        /// <summary>
        /// Gets SystemData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSystemData funcion</param>
        /// <param name="IdSystemData">Specifies filter for ID_SYSTEM_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SYSTEM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SystemData list</returns>
        public virtual List<OpSystemData> GetSystemDataFilterDAQ(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSystemData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSystemDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSystemDataFilter"));
            }

            try
            {
                List<OpSystemData> returnList = OpSystemData.ConvertList(dbConnectionDaq.GetSystemDataFilter(IdSystemData, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSystemData item in returnList)
                        SystemDataDict[item.IdSystemData] = item;
                    SystemDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSystemDataFilter", ex);
                throw;
            }

        }
        #endregion
    }
}
