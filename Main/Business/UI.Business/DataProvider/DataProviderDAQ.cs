﻿using System;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DAQ;
using IMR.Suite.Common;
using System.Data;

namespace IMR.Suite.UI.Business
{
    public partial class DataProvider
    {
        #region Initialize
        //private void Initialize()
        //{
        //    DataArchCacheEnabled = true;
        //    DataArchTrashCacheEnabled = true;
        //    DataArchTrashStatusCacheEnabled = true;
        //    PacketCacheEnabled = true;
        //    TransmissionDriverCacheEnabled = true;
        //    TransmissionDriverTypeCacheEnabled = true;
        //    TransmissionProtocolCacheEnabled = true;
        //}
        #endregion

        #region Audit
        /// <summary>
        /// Indicates whether the AuditDict was filled trough GetAllAudit or GetAllAuditFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool AuditIsFullCacheDAQ { get; protected set; }

        /// <summary>
        /// Audit objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpAudit> AuditDictDAQ = new Dictionary<long, OpAudit>();

        /// <summary>
        /// Indicates whether the Audit is cached.
        /// </summary>
        public bool AuditCacheEnabledDAQ { get; set; }

        /// <summary>
        /// Gets all Audit objects
        /// </summary>
        public virtual List<OpAudit> AuditDAQ
        {
            get { return GetAllAuditDAQ(); }
        }

        /// <summary>
        /// Gets all Audit objects
        /// </summary>
        public virtual List<OpAudit> GetAllAuditDAQ()
        {
            try
            {
                if (AuditCacheEnabledDAQ) //cache enabled
                {
                    if (!AuditIsFullCacheDAQ)
                    {
                        List<OpAudit> objectList = OpAudit.ConvertList(dbConnectionDaq.GetAudit(), this);
                        if (objectList != null)
                        {
                            AuditDictDAQ = objectList.ToDictionary(o => o.IdAudit);
                            AuditIsFullCacheDAQ = true;
                        }
                    }
                    return new List<OpAudit>(AuditDictDAQ.Values);
                }
                else //cache disabled
                {
                    return OpAudit.ConvertList(dbConnectionDaq.GetAudit(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAudit", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <returns>Audit object</returns>
        public virtual OpAudit GetAuditDAQ(long Id)
        {
            return GetAuditDAQ(Id, false);
        }

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <returns>Audit list</returns>
        public virtual List<OpAudit> GetAuditDAQ(long[] Ids)
        {
            return GetAuditDAQ(Ids, false);
        }

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit object</returns>
        public virtual OpAudit GetAuditDAQ(long Id, bool queryDatabase)
        {
            return GetAuditDAQ(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit list</returns>
        public virtual List<OpAudit> GetAuditDAQ(long[] Ids, bool queryDatabase)
        {
            List<OpAudit> returnList = new List<OpAudit>();
            if (Ids != null && Ids.Length > 0)
            {
                if (AuditCacheEnabledDAQ && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpAudit AuditFoundInCache;
                        if (AuditDictDAQ.TryGetValue(Ids[i], out AuditFoundInCache)) //element founded in cache
                            returnList.Add(AuditFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpAudit item in OpAudit.ConvertList(dbConnectionDaq.GetAudit(idsNotInCache.ToArray()), this))
                            {
                                AuditDictDAQ[item.IdAudit] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAudit", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpAudit.ConvertList(dbConnectionDaq.GetAudit(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAudit", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Audit object to the database
        /// </summary>
        /// <param name="toBeSaved">Audit to save</param>
        /// <returns>Audit Id</returns>
        public virtual long SaveAuditDAQ(OpAudit toBeSaved)
        {
            try
            {
                long ret = dbConnectionDaq.SaveAudit(toBeSaved);
                if (AuditCacheEnabledDAQ)
                {
                    AuditDictDAQ[toBeSaved.IdAudit] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveAudit", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Audit object from the database
        /// </summary>
        /// <param name="toBeDeleted">Audit to delete</param>
        public virtual void DeleteAuditDAQ(OpAudit toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteAudit(toBeDeleted);
                if (AuditCacheEnabledDAQ)
                {
                    AuditDictDAQ.Remove(toBeDeleted.IdAudit);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelAudit", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the Audit object cache
        /// </summary>
        public virtual void ClearAuditCacheDAQ()
        {
            AuditDictDAQ.Clear();
            AuditIsFullCacheDAQ = false;
        }
        #endregion

        #region Connection
        /// <summary>
        /// Indicates whether the ConnectionDict was filled trough GetAllConnection or GetAllConnectionFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ConnectionIsFullCache { get; protected set; }

        /// <summary>
        /// Connection objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpConnection> ConnectionDict = new Dictionary<long, OpConnection>();

        /// <summary>
        /// Indicates whether the Connection is cached.
        /// </summary>
        public bool ConnectionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Connection objects
        /// </summary>
        public virtual List<OpConnection> Connection
        {
            get { return GetAllConnection(); }
        }

        /// <summary>
        /// Gets all Connection objects
        /// </summary>
        public virtual List<OpConnection> GetAllConnection()
        {
            try
            {
                if (ConnectionCacheEnabled) //cache enabled
                {
                    if (!ConnectionIsFullCache)
                    {
                        List<OpConnection> objectList = OpConnection.ConvertList(dbConnectionDaq.GetConnection(), this);
                        if (objectList != null)
                        {
                            ConnectionDict = objectList.ToDictionary(o => o.Id);
                            ConnectionIsFullCache = true;
                        }
                    }
                    return new List<OpConnection>(ConnectionDict.Values);
                }
                else //cache disabled
                {
                    return OpConnection.ConvertList(dbConnectionDaq.GetConnection(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConnection", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Id">Connection Id</param>
        /// <returns>Connection object</returns>
        public virtual OpConnection GetConnection(long Id)
        {
            return GetConnection(Id, false);
        }

        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Ids">Connection Ids</param>
        /// <returns>Connection list</returns>
        public virtual List<OpConnection> GetConnection(long[] Ids)
        {
            return GetConnection(Ids, false);
        }

        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Id">Connection Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Connection object</returns>
        public virtual OpConnection GetConnection(long Id, bool queryDatabase)
        {
            return GetConnection(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Connection by id
        /// </summary>
        /// <param name="Ids">Connection Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Connection list</returns>
        public virtual List<OpConnection> GetConnection(long[] Ids, bool queryDatabase)
        {
            List<OpConnection> returnList = new List<OpConnection>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConnectionCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpConnection ConnectionFoundInCache;
                        if (ConnectionDict.TryGetValue(Ids[i], out ConnectionFoundInCache)) //element founded in cache
                            returnList.Add(ConnectionFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpConnection item in OpConnection.ConvertList(dbConnectionDaq.GetConnection(idsNotInCache.ToArray()), this))
                            {
                                ConnectionDict[item.Id] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConnection", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpConnection.ConvertList(dbConnectionDaq.GetConnection(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConnection", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Connection object to the database
        /// </summary>
        /// <param name="toBeSaved">Connection to save</param>
        /// <returns>Connection Id</returns>
        public virtual long SaveConnection(OpConnection toBeSaved)
        {
            try
            {
                long ret = dbConnectionDaq.SaveConnection(toBeSaved);
                if (ConnectionCacheEnabled)
                {
                    ConnectionDict[toBeSaved.Id] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveConnection", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Connection object from the database
        /// </summary>
        /// <param name="toBeDeleted">Connection to delete</param>
        public virtual void DeleteConnection(OpConnection toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteConnection(toBeDeleted);
                if (ConnectionCacheEnabled)
                {
                    ConnectionDict.Remove(toBeDeleted.Id);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelConnection", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the Connection object cache
        /// </summary>
        public virtual void ClearConnectionCache()
        {
            ConnectionDict.Clear();
            ConnectionIsFullCache = false;
        }
        #endregion

        #region DataArchTrash

        #endregion

        #region DataArchTrashStatus
        /// <summary>
        /// Indicates whether the DataArchTrashStatusDict was filled trough GetAllDataArchTrashStatus or GetAllDataArchTrashStatusFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataArchTrashStatusIsFullCache { get; protected set; }

        /// <summary>
        /// DataArchTrashStatus objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpDataArchTrashStatus> DataArchTrashStatusDict = new Dictionary<int, OpDataArchTrashStatus>();

        /// <summary>
        /// Indicates whether the DataArchTrashStatus is cached.
        /// </summary>
        public virtual bool DataArchTrashStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataArchTrashStatus objects
        /// </summary>
        public virtual List<OpDataArchTrashStatus> DataArchTrashStatus
        {
            get { return GetAllDataArchTrashStatus(); }
        }

        /// <summary>
        /// Gets all DataArchTrashStatus objects
        /// </summary>
        public virtual List<OpDataArchTrashStatus> GetAllDataArchTrashStatus()
        {
            try
            {
                if (DataArchTrashStatusCacheEnabled) //cache enabled
                {
                    if (!DataArchTrashStatusIsFullCache)
                    {
                        List<OpDataArchTrashStatus> objectList = OpDataArchTrashStatus.ConvertList(dbConnectionDaq.GetDataArchTrashStatus(), this);
                        if (objectList != null)
                        {
                            DataArchTrashStatusDict = objectList.ToDictionary(o => o.IdDataArchTrashStatus);
                            DataArchTrashStatusIsFullCache = true;
                        }
                    }
                    return new List<OpDataArchTrashStatus>(DataArchTrashStatusDict.Values);
                }
                else //cache disabled
                {
                    return OpDataArchTrashStatus.ConvertList(dbConnectionDaq.GetDataArchTrashStatus(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArchTrashStatus", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Id">DataArchTrashStatus Id</param>
        /// <returns>DataArchTrashStatus object</returns>
        public virtual OpDataArchTrashStatus GetDataArchTrashStatus(int Id)
        {
            return GetDataArchTrashStatus(Id, false);
        }

        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Ids">DataArchTrashStatus Ids</param>
        /// <returns>DataArchTrashStatus list</returns>
        public virtual List<OpDataArchTrashStatus> GetDataArchTrashStatus(int[] Ids)
        {
            return GetDataArchTrashStatus(Ids, false);
        }

        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Id">DataArchTrashStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArchTrashStatus object</returns>
        public virtual OpDataArchTrashStatus GetDataArchTrashStatus(int Id, bool queryDatabase)
        {
            return GetDataArchTrashStatus(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataArchTrashStatus by id
        /// </summary>
        /// <param name="Ids">DataArchTrashStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArchTrashStatus list</returns>
        public virtual List<OpDataArchTrashStatus> GetDataArchTrashStatus(int[] Ids, bool queryDatabase)
        {
            List<OpDataArchTrashStatus> returnList = new List<OpDataArchTrashStatus>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataArchTrashStatusCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDataArchTrashStatus DataArchTrashStatusFoundInCache;
                        if (DataArchTrashStatusDict.TryGetValue(Ids[i], out DataArchTrashStatusFoundInCache)) //element founded in cache
                            returnList.Add(DataArchTrashStatusFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDataArchTrashStatus item in OpDataArchTrashStatus.ConvertList(dbConnectionDaq.GetDataArchTrashStatus(idsNotInCache.ToArray()), this))
                            {
                                DataArchTrashStatusDict[item.IdDataArchTrashStatus] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArchTrashStatus", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataArchTrashStatus.ConvertList(dbConnectionDaq.GetDataArchTrashStatus(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArchTrashStatus", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DataArchTrashStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">DataArchTrashStatus to save</param>
        /// <returns>DataArchTrashStatus Id</returns>
        public virtual int SaveDataArchTrashStatus(OpDataArchTrashStatus toBeSaved)
        {
            try
            {
                int ret = dbConnectionDaq.SaveDataArchTrashStatus(toBeSaved);
                if (DataArchTrashStatusCacheEnabled)
                {
                    DataArchTrashStatusDict[toBeSaved.IdDataArchTrashStatus] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDataArchTrashStatus", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the DataArchTrashStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataArchTrashStatus to delete</param>
        public virtual void DeleteDataArchTrashStatus(OpDataArchTrashStatus toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteDataArchTrashStatus(toBeDeleted);
                if (DataArchTrashStatusCacheEnabled)
                {
                    DataArchTrashStatusDict.Remove(toBeDeleted.IdDataArchTrashStatus);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDataArchTrashStatus", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the DataArchTrashStatus object cache
        /// </summary>
        public virtual void ClearDataArchTrashStatusCache()
        {
            DataArchTrashStatusDict.Clear();
            DataArchTrashStatusIsFullCache = false;
        }
        #endregion

        #region Packet
        /// <summary>
        /// Indicates whether the PacketDict was filled trough GetAllPacket or GetAllPacketFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketIsFullCache { get; protected set; }

        /// <summary>
        /// Packet objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpPacket> PacketDict = new Dictionary<long, OpPacket>();

        /// <summary>
        /// Indicates whether the Packet is cached.
        /// </summary>
        public virtual bool PacketCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Packet objects
        /// </summary>
        public virtual List<OpPacket> Packet
        {
            get { return GetAllPacket(); }
        }

        /// <summary>
        /// Gets all Packet objects
        /// </summary>
        public virtual List<OpPacket> GetAllPacket()
        {
            try
            {
                if (PacketCacheEnabled) //cache enabled
                {
                    if (!PacketIsFullCache)
                    {
                        List<OpPacket> objectList = OpPacket.ConvertList(dbConnectionDaq.GetPacket(), this);
                        if (objectList != null)
                        {
                            PacketDict = objectList.ToDictionary(o => o.IdPacket);
                            PacketIsFullCache = true;
                        }
                    }
                    return new List<OpPacket>(PacketDict.Values);
                }
                else //cache disabled
                {
                    return OpPacket.ConvertList(dbConnectionDaq.GetPacket(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacket", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Id">Packet Id</param>
        /// <returns>Packet object</returns>
        public virtual OpPacket GetPacket(long Id)
        {
            return GetPacket(Id, false);
        }

        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Ids">Packet Ids</param>
        /// <returns>Packet list</returns>
        public virtual List<OpPacket> GetPacket(long[] Ids)
        {
            return GetPacket(Ids, false);
        }

        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Id">Packet Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Packet object</returns>
        public virtual OpPacket GetPacket(long Id, bool queryDatabase)
        {
            return GetPacket(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Packet by id
        /// </summary>
        /// <param name="Ids">Packet Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Packet list</returns>
        public virtual List<OpPacket> GetPacket(long[] Ids, bool queryDatabase)
        {
            List<OpPacket> returnList = new List<OpPacket>();
            if (Ids != null && Ids.Length > 0)
            {
                if (PacketCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpPacket PacketFoundInCache;
                        if (PacketDict.TryGetValue(Ids[i], out PacketFoundInCache)) //element founded in cache
                            returnList.Add(PacketFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpPacket item in OpPacket.ConvertList(dbConnectionDaq.GetPacket(idsNotInCache.ToArray()), this))
                            {
                                PacketDict[item.IdPacket] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacket", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpPacket.ConvertList(dbConnectionDaq.GetPacket(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacket", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Packet object to the database
        /// </summary>
        /// <param name="toBeSaved">Packet to save</param>
        /// <returns>Packet Id</returns>
        public virtual long SavePacket(OpPacket toBeSaved)
        {
            try
            {
                long ret = dbConnectionDaq.SavePacket(toBeSaved);
                if (PacketCacheEnabled)
                {
                    PacketDict[toBeSaved.IdPacket] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SavePacket", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the Packet object from the database
        /// </summary>
        /// <param name="toBeDeleted">Packet to delete</param>
        public virtual void DeletePacket(OpPacket toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeletePacket(toBeDeleted);
                if (PacketCacheEnabled)
                {
                    PacketDict.Remove(toBeDeleted.IdPacket);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelPacket", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the Packet object cache
        /// </summary>
        public virtual void ClearPacketCache()
        {
            PacketDict.Clear();
            PacketIsFullCache = false;
        }
        #endregion

        #region PacketHistory
        /// <summary>
        /// Indicates whether the PacketHistoryDict was filled trough GetAllPacketHistory or GetAllPacketHistoryFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketHistoryIsFullCache { get; protected set; }

        /// <summary>
        /// PacketHistory objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpPacketHistory> PacketHistoryDict = new Dictionary<long, OpPacketHistory>();

        /// <summary>
        /// Indicates whether the PacketHistory is cached.
        /// </summary>
        public virtual bool PacketHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PacketHistory objects
        /// </summary>
        public virtual List<OpPacketHistory> PacketHistory
        {
            get { return GetAllPacketHistory(); }
        }

        /// <summary>
        /// Gets all PacketHistory objects
        /// </summary>
        public virtual List<OpPacketHistory> GetAllPacketHistory()
        {
            try
            {
                if (PacketHistoryCacheEnabled) //cache enabled
                {
                    if (!PacketHistoryIsFullCache)
                    {
                        List<OpPacketHistory> objectList = OpPacketHistory.ConvertList(dbConnectionDaq.GetPacketHistory(), this);
                        if (objectList != null)
                        {
                            PacketHistoryDict = objectList.ToDictionary(o => o.IdPacketHistory);
                            PacketHistoryIsFullCache = true;
                        }
                    }
                    return new List<OpPacketHistory>(PacketHistoryDict.Values);
                }
                else //cache disabled
                {
                    return OpPacketHistory.ConvertList(dbConnectionDaq.GetPacketHistory(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketHistory", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Id">PacketHistory Id</param>
        /// <returns>PacketHistory object</returns>
        public virtual OpPacketHistory GetPacketHistory(long Id)
        {
            return GetPacketHistory(Id, false);
        }

        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Ids">PacketHistory Ids</param>
        /// <returns>PacketHistory list</returns>
        public virtual List<OpPacketHistory> GetPacketHistory(long[] Ids)
        {
            return GetPacketHistory(Ids, false);
        }

        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Id">PacketHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketHistory object</returns>
        public virtual OpPacketHistory GetPacketHistory(long Id, bool queryDatabase)
        {
            return GetPacketHistory(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets PacketHistory by id
        /// </summary>
        /// <param name="Ids">PacketHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketHistory list</returns>
        public virtual List<OpPacketHistory> GetPacketHistory(long[] Ids, bool queryDatabase)
        {
            List<OpPacketHistory> returnList = new List<OpPacketHistory>();
            if (Ids != null && Ids.Length > 0)
            {
                if (PacketHistoryCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpPacketHistory PacketHistoryFoundInCache;
                        if (PacketHistoryDict.TryGetValue(Ids[i], out PacketHistoryFoundInCache)) //element founded in cache
                            returnList.Add(PacketHistoryFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpPacketHistory item in OpPacketHistory.ConvertList(dbConnectionDaq.GetPacketHistory(idsNotInCache.ToArray()), this))
                            {
                                PacketHistoryDict[item.IdPacketHistory] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketHistory", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpPacketHistory.ConvertList(dbConnectionDaq.GetPacketHistory(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketHistory", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the PacketHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">PacketHistory to save</param>
        /// <returns>PacketHistory Id</returns>
        public virtual long SavePacketHistory(OpPacketHistory toBeSaved)
        {
            try
            {
                long ret = dbConnectionDaq.SavePacketHistory(toBeSaved);
                if (PacketHistoryCacheEnabled)
                {
                    PacketHistoryDict[toBeSaved.IdPacketHistory] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SavePacketHistory", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the PacketHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">PacketHistory to delete</param>
        public virtual void DeletePacketHistory(OpPacketHistory toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeletePacketHistory(toBeDeleted);
                if (PacketHistoryCacheEnabled)
                {
                    PacketHistoryDict.Remove(toBeDeleted.IdPacketHistory);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelPacketHistory", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the PacketHistory object cache
        /// </summary>
        public virtual void ClearPacketHistoryCache()
        {
            PacketHistoryDict.Clear();
            PacketHistoryIsFullCache = false;
        }
        #endregion

        #region PacketStatus
        /// <summary>
        /// Indicates whether the PacketStatusDict was filled trough GetAllPacketStatus or GetAllPacketStatusFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketStatusIsFullCache { get; protected set; }

        /// <summary>
        /// PacketStatus objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpPacketStatus> PacketStatusDict = new Dictionary<int, OpPacketStatus>();

        /// <summary>
        /// Indicates whether the PacketStatus is cached.
        /// </summary>
        public virtual bool PacketStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all PacketStatus objects
        /// </summary>
        public virtual List<OpPacketStatus> PacketStatus
        {
            get { return GetAllPacketStatus(); }
        }

        /// <summary>
        /// Gets all PacketStatus objects
        /// </summary>
        public virtual List<OpPacketStatus> GetAllPacketStatus()
        {
            try
            {
                if (PacketStatusCacheEnabled) //cache enabled
                {
                    if (!PacketStatusIsFullCache)
                    {
                        List<OpPacketStatus> objectList = OpPacketStatus.ConvertList(dbConnectionDaq.GetPacketStatus(), this);
                        if (objectList != null)
                        {
                            PacketStatusDict = objectList.ToDictionary(o => o.IdPacketStatus);
                            PacketStatusIsFullCache = true;
                        }
                    }
                    return new List<OpPacketStatus>(PacketStatusDict.Values);
                }
                else //cache disabled
                {
                    return OpPacketStatus.ConvertList(dbConnectionDaq.GetPacketStatus(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketStatus", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Id">PacketStatus Id</param>
        /// <returns>PacketStatus object</returns>
        public virtual OpPacketStatus GetPacketStatus(int Id)
        {
            return GetPacketStatus(Id, false);
        }

        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Ids">PacketStatus Ids</param>
        /// <returns>PacketStatus list</returns>
        public virtual List<OpPacketStatus> GetPacketStatus(int[] Ids)
        {
            return GetPacketStatus(Ids, false);
        }

        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Id">PacketStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketStatus object</returns>
        public virtual OpPacketStatus GetPacketStatus(int Id, bool queryDatabase)
        {
            return GetPacketStatus(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets PacketStatus by id
        /// </summary>
        /// <param name="Ids">PacketStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>PacketStatus list</returns>
        public virtual List<OpPacketStatus> GetPacketStatus(int[] Ids, bool queryDatabase)
        {
            List<OpPacketStatus> returnList = new List<OpPacketStatus>();
            if (Ids != null && Ids.Length > 0)
            {
                if (PacketStatusCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpPacketStatus PacketStatusFoundInCache;
                        if (PacketStatusDict.TryGetValue(Ids[i], out PacketStatusFoundInCache)) //element founded in cache
                            returnList.Add(PacketStatusFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpPacketStatus item in OpPacketStatus.ConvertList(dbConnectionDaq.GetPacketStatus(idsNotInCache.ToArray()), this))
                            {
                                PacketStatusDict[item.IdPacketStatus] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketStatus", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpPacketStatus.ConvertList(dbConnectionDaq.GetPacketStatus(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketStatus", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the PacketStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">PacketStatus to save</param>
        /// <returns>PacketStatus Id</returns>
        public virtual int SavePacketStatus(OpPacketStatus toBeSaved)
        {
            try
            {
                int ret = dbConnectionDaq.SavePacketStatus(toBeSaved);
                if (PacketStatusCacheEnabled)
                {
                    PacketStatusDict[toBeSaved.IdPacketStatus] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SavePacketStatus", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the PacketStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">PacketStatus to delete</param>
        public virtual void DeletePacketStatus(OpPacketStatus toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeletePacketStatus(toBeDeleted);
                if (PacketStatusCacheEnabled)
                {
                    PacketStatusDict.Remove(toBeDeleted.IdPacketStatus);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelPacketStatus", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the PacketStatus object cache
        /// </summary>
        public virtual void ClearPacketStatusCache()
        {
            PacketStatusDict.Clear();
            PacketStatusIsFullCache = false;
        }
        #endregion

        #region RouteTable
        /// <summary>
        /// Indicates whether the RouteTableDict was filled trough GetAllRouteTable or GetAllRouteTableFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool RouteTableIsFullCache { get; protected set; }

        /// <summary>
        /// RouteTable objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpRouteTable> RouteTableDict = new Dictionary<int, OpRouteTable>();

        /// <summary>
        /// Indicates whether the RouteTable is cached.
        /// </summary>
        public bool RouteTableCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteTable objects
        /// </summary>
        public virtual List<OpRouteTable> RouteTable
        {
            get { return GetAllRouteTable(); }
        }

        /// <summary>
        /// Gets all RouteTable objects
        /// </summary>
        public virtual List<OpRouteTable> GetAllRouteTable()
        {
            try
            {
                if (RouteTableCacheEnabled) //cache enabled
                {
                    if (!RouteTableIsFullCache)
                    {
                        List<OpRouteTable> objectList = OpRouteTable.ConvertList(dbConnectionDaq.GetRouteTable(), this);
                        if (objectList != null)
                        {
                            RouteTableDict = objectList.ToDictionary(o => o.IdRoute);
                            RouteTableIsFullCache = true;
                        }
                    }
                    return new List<OpRouteTable>(RouteTableDict.Values);
                }
                else //cache disabled
                {
                    return OpRouteTable.ConvertList(dbConnectionDaq.GetRouteTable(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTable", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Id">RouteTable Id</param>
        /// <returns>RouteTable object</returns>
        public virtual OpRouteTable GetRouteTable(int Id)
        {
            return GetRouteTable(Id, false);
        }

        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Ids">RouteTable Ids</param>
        /// <returns>RouteTable list</returns>
        public virtual List<OpRouteTable> GetRouteTable(int[] Ids)
        {
            return GetRouteTable(Ids, false);
        }

        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Id">RouteTable Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTable object</returns>
        public virtual OpRouteTable GetRouteTable(int Id, bool queryDatabase)
        {
            return GetRouteTable(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets RouteTable by id
        /// </summary>
        /// <param name="Ids">RouteTable Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTable list</returns>
        public virtual List<OpRouteTable> GetRouteTable(int[] Ids, bool queryDatabase)
        {
            List<OpRouteTable> returnList = new List<OpRouteTable>();
            if (Ids != null && Ids.Length > 0)
            {
                if (RouteTableCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpRouteTable RouteTableFoundInCache;
                        if (RouteTableDict.TryGetValue(Ids[i], out RouteTableFoundInCache)) //element founded in cache
                            returnList.Add(RouteTableFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpRouteTable item in OpRouteTable.ConvertList(dbConnectionDaq.GetRouteTable(idsNotInCache.ToArray()), this))
                            {
                                RouteTableDict[item.IdRoute] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTable", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpRouteTable.ConvertList(dbConnectionDaq.GetRouteTable(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTable", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Deletes the RouteTable object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteTable to delete</param>
        public virtual void DeleteRouteTable(OpRouteTable toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteRouteTable(toBeDeleted);
                if (RouteTableCacheEnabled)
                {
                    RouteTableDict.Remove(toBeDeleted.IdRoute);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelRouteTable", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the RouteTable object cache
        /// </summary>
        public virtual void ClearRouteTableCache()
        {
            RouteTableDict.Clear();
            RouteTableIsFullCache = false;
        }
        #endregion

        #region RouteTableBackup
        /// <summary>
        /// Indicates whether the RouteTableBackupDict was filled trough GetAllRouteTableBackup or GetAllRouteTableBackupFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool RouteTableBackupIsFullCache { get; protected set; }

        /// <summary>
        /// RouteTableBackup objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpRouteTableBackup> RouteTableBackupDict = new Dictionary<int, OpRouteTableBackup>();

        /// <summary>
        /// Indicates whether the RouteTableBackup is cached.
        /// </summary>
        public bool RouteTableBackupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteTableBackup objects
        /// </summary>
        public virtual List<OpRouteTableBackup> RouteTableBackup
        {
            get { return GetAllRouteTableBackup(); }
        }

        /// <summary>
        /// Gets all RouteTableBackup objects
        /// </summary>
        public virtual List<OpRouteTableBackup> GetAllRouteTableBackup()
        {
            try
            {
                if (RouteTableBackupCacheEnabled) //cache enabled
                {
                    if (!RouteTableBackupIsFullCache)
                    {
                        List<OpRouteTableBackup> objectList = OpRouteTableBackup.ConvertList(dbConnectionDaq.GetRouteTableBackup(), this);
                        if (objectList != null)
                        {
                            RouteTableBackupDict = objectList.ToDictionary(o => o.Id);
                            RouteTableBackupIsFullCache = true;
                        }
                    }
                    return new List<OpRouteTableBackup>(RouteTableBackupDict.Values);
                }
                else //cache disabled
                {
                    return OpRouteTableBackup.ConvertList(dbConnectionDaq.GetRouteTableBackup(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTableBackup", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Id">RouteTableBackup Id</param>
        /// <returns>RouteTableBackup object</returns>
        public virtual OpRouteTableBackup GetRouteTableBackup(int Id)
        {
            return GetRouteTableBackup(Id, false);
        }

        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Ids">RouteTableBackup Ids</param>
        /// <returns>RouteTableBackup list</returns>
        public virtual List<OpRouteTableBackup> GetRouteTableBackup(int[] Ids)
        {
            return GetRouteTableBackup(Ids, false);
        }

        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Id">RouteTableBackup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTableBackup object</returns>
        public virtual OpRouteTableBackup GetRouteTableBackup(int Id, bool queryDatabase)
        {
            return GetRouteTableBackup(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets RouteTableBackup by id
        /// </summary>
        /// <param name="Ids">RouteTableBackup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RouteTableBackup list</returns>
        public virtual List<OpRouteTableBackup> GetRouteTableBackup(int[] Ids, bool queryDatabase)
        {
            List<OpRouteTableBackup> returnList = new List<OpRouteTableBackup>();
            if (Ids != null && Ids.Length > 0)
            {
                if (RouteTableBackupCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpRouteTableBackup RouteTableBackupFoundInCache;
                        if (RouteTableBackupDict.TryGetValue(Ids[i], out RouteTableBackupFoundInCache)) //element founded in cache
                            returnList.Add(RouteTableBackupFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpRouteTableBackup item in OpRouteTableBackup.ConvertList(dbConnectionDaq.GetRouteTableBackup(idsNotInCache.ToArray()), this))
                            {
                                RouteTableBackupDict[item.Id] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTableBackup", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpRouteTableBackup.ConvertList(dbConnectionDaq.GetRouteTableBackup(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTableBackup", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the RouteTableBackup object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteTableBackup to save</param>
        /// <returns>RouteTableBackup Id</returns>
        public virtual int SaveRouteTableBackup(OpRouteTableBackup toBeSaved)
        {
            try
            {
                int ret = dbConnectionDaq.SaveRouteTableBackup(toBeSaved);
                if (RouteTableBackupCacheEnabled)
                {
                    RouteTableBackupDict[toBeSaved.Id] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveRouteTableBackup", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the RouteTableBackup object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteTableBackup to delete</param>
        public virtual void DeleteRouteTableBackup(OpRouteTableBackup toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteRouteTableBackup(toBeDeleted);
                if (RouteTableBackupCacheEnabled)
                {
                    RouteTableBackupDict.Remove(toBeDeleted.Id);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelRouteTableBackup", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the RouteTableBackup object cache
        /// </summary>
        public virtual void ClearRouteTableBackupCache()
        {
            RouteTableBackupDict.Clear();
            RouteTableBackupIsFullCache = false;
        }
        #endregion

        #region TransmissionDriver
        /// <summary>
        /// Indicates whether the TransmissionDriverDict was filled trough GetAllTransmissionDriver or GetAllTransmissionDriverFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool TransmissionDriverIsFullCache { get; protected set; }

        /// <summary>
        /// TransmissionDriver objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpTransmissionDriver> TransmissionDriverDict = new Dictionary<int, OpTransmissionDriver>();

        /// <summary>
        /// Indicates whether the TransmissionDriver is cached.
        /// </summary>
        public virtual bool TransmissionDriverCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriver objects
        /// </summary>
        public virtual List<OpTransmissionDriver> TransmissionDriver
        {
            get { return GetAllTransmissionDriver(); }
        }

        /// <summary>
        /// Gets all TransmissionDriver objects
        /// </summary>
        public virtual List<OpTransmissionDriver> GetAllTransmissionDriver()
        {
            try
            {
                if (TransmissionDriverCacheEnabled) //cache enabled
                {
                    if (!TransmissionDriverIsFullCache)
                    {
                        List<OpTransmissionDriver> objectList = OpTransmissionDriver.ConvertList(dbConnectionDaq.GetTransmissionDriver(), this);
                        if (objectList != null)
                        {
                            TransmissionDriverDict = objectList.ToDictionary(o => o.IdTransmissionDriver);
                            TransmissionDriverIsFullCache = true;
                        }
                    }
                    return new List<OpTransmissionDriver>(TransmissionDriverDict.Values);
                }
                else //cache disabled
                {
                    return OpTransmissionDriver.ConvertList(dbConnectionDaq.GetTransmissionDriver(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriver", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <returns>TransmissionDriver object</returns>
        public virtual OpTransmissionDriver GetTransmissionDriver(int Id)
        {
            return GetTransmissionDriver(Id, false);
        }

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <returns>TransmissionDriver list</returns>
        public virtual List<OpTransmissionDriver> GetTransmissionDriver(int[] Ids)
        {
            return GetTransmissionDriver(Ids, false);
        }

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver object</returns>
        public virtual OpTransmissionDriver GetTransmissionDriver(int Id, bool queryDatabase)
        {
            return GetTransmissionDriver(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver list</returns>
        public virtual List<OpTransmissionDriver> GetTransmissionDriver(int[] Ids, bool queryDatabase)
        {
            List<OpTransmissionDriver> returnList = new List<OpTransmissionDriver>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TransmissionDriverCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTransmissionDriver TransmissionDriverFoundInCache;
                        if (TransmissionDriverDict.TryGetValue(Ids[i], out TransmissionDriverFoundInCache)) //element founded in cache
                            returnList.Add(TransmissionDriverFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTransmissionDriver item in OpTransmissionDriver.ConvertList(dbConnectionDaq.GetTransmissionDriver(idsNotInCache.ToArray()), this))
                            {
                                TransmissionDriverDict[item.IdTransmissionDriver] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriver", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTransmissionDriver.ConvertList(dbConnectionDaq.GetTransmissionDriver(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriver", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the TransmissionDriver object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriver to save</param>
        /// <returns>TransmissionDriver Id</returns>
        public virtual int SaveTransmissionDriver(OpTransmissionDriver toBeSaved)
        {
            try
            {
                int ret = dbConnectionDaq.SaveTransmissionDriver(toBeSaved);
                if (TransmissionDriverCacheEnabled)
                {
                    TransmissionDriverDict[toBeSaved.IdTransmissionDriver] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveTransmissionDriver", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the TransmissionDriver object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriver to delete</param>
        public virtual void DeleteTransmissionDriver(OpTransmissionDriver toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteTransmissionDriver(toBeDeleted);
                if (TransmissionDriverCacheEnabled)
                {
                    TransmissionDriverDict.Remove(toBeDeleted.IdTransmissionDriver);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelTransmissionDriver", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the TransmissionDriver object cache
        /// </summary>
        public virtual void ClearTransmissionDriverCache()
        {
            TransmissionDriverDict.Clear();
            TransmissionDriverIsFullCache = false;
        }
        #endregion

        #region TransmissionDriverData
        /// <summary>
        /// Indicates whether the TransmissionDriverDataDict was filled trough GetAllTransmissionDriverData or GetAllTransmissionDriverDataFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool TransmissionDriverDataIsFullCache { get; protected set; }

        /// <summary>
        /// TransmissionDriverData objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpTransmissionDriverData> TransmissionDriverDataDict = new Dictionary<long, OpTransmissionDriverData>();

        /// <summary>
        /// Indicates whether the TransmissionDriverData is cached.
        /// </summary>
        public bool TransmissionDriverDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriverData objects
        /// </summary>
        public virtual List<OpTransmissionDriverData> TransmissionDriverData
        {
            get { return GetAllTransmissionDriverData(); }
        }

        /// <summary>
        /// Gets all TransmissionDriverData objects
        /// </summary>
        public virtual List<OpTransmissionDriverData> GetAllTransmissionDriverData()
        {
            try
            {
                if (TransmissionDriverDataCacheEnabled) //cache enabled
                {
                    if (!TransmissionDriverDataIsFullCache)
                    {
                        List<OpTransmissionDriverData> objectList = OpTransmissionDriverData.ConvertList(dbConnectionDaq.GetTransmissionDriverData(), this);
                        if (objectList != null)
                        {
                            TransmissionDriverDataDict = objectList.ToDictionary(o => o.IdTransmissionDriverData);
                            TransmissionDriverDataIsFullCache = true;
                        }
                    }
                    return new List<OpTransmissionDriverData>(TransmissionDriverDataDict.Values);
                }
                else //cache disabled
                {
                    return OpTransmissionDriverData.ConvertList(dbConnectionDaq.GetTransmissionDriverData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverData", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Id">TransmissionDriverData Id</param>
        /// <returns>TransmissionDriverData object</returns>
        public virtual OpTransmissionDriverData GetTransmissionDriverData(long Id)
        {
            return GetTransmissionDriverData(Id, false);
        }

        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverData Ids</param>
        /// <returns>TransmissionDriverData list</returns>
        public virtual List<OpTransmissionDriverData> GetTransmissionDriverData(long[] Ids)
        {
            return GetTransmissionDriverData(Ids, false);
        }

        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Id">TransmissionDriverData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverData object</returns>
        public virtual OpTransmissionDriverData GetTransmissionDriverData(long Id, bool queryDatabase)
        {
            return GetTransmissionDriverData(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets TransmissionDriverData by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverData list</returns>
        public virtual List<OpTransmissionDriverData> GetTransmissionDriverData(long[] Ids, bool queryDatabase)
        {
            List<OpTransmissionDriverData> returnList = new List<OpTransmissionDriverData>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TransmissionDriverDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTransmissionDriverData TransmissionDriverDataFoundInCache;
                        if (TransmissionDriverDataDict.TryGetValue(Ids[i], out TransmissionDriverDataFoundInCache)) //element founded in cache
                            returnList.Add(TransmissionDriverDataFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTransmissionDriverData item in OpTransmissionDriverData.ConvertList(dbConnectionDaq.GetTransmissionDriverData(idsNotInCache.ToArray()), this))
                            {
                                TransmissionDriverDataDict[item.IdTransmissionDriverData] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverData", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTransmissionDriverData.ConvertList(dbConnectionDaq.GetTransmissionDriverData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverData", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the TransmissionDriverData object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriverData to save</param>
        /// <returns>TransmissionDriverData Id</returns>
        public virtual long SaveTransmissionDriverData(OpTransmissionDriverData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDaq.SaveTransmissionDriverData(toBeSaved, toBeSaved.DataType);
                if (TransmissionDriverDataCacheEnabled)
                {
                    TransmissionDriverDataDict[toBeSaved.IdTransmissionDriverData] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveTransmissionDriverData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the TransmissionDriverData object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriverData to delete</param>
        public virtual void DeleteTransmissionDriverData(OpTransmissionDriverData toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteTransmissionDriverData(toBeDeleted);
                if (TransmissionDriverDataCacheEnabled)
                {
                    TransmissionDriverDataDict.Remove(toBeDeleted.IdTransmissionDriverData);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelTransmissionDriverData", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the TransmissionDriverData object cache
        /// </summary>
        public virtual void ClearTransmissionDriverDataCache()
        {
            TransmissionDriverDataDict.Clear();
            TransmissionDriverDataIsFullCache = false;
        }
        #endregion

        #region TransmissionDriverType
        /// <summary>
        /// Indicates whether the TransmissionDriverTypeDict was filled trough GetAllTransmissionDriverType or GetAllTransmissionDriverTypeFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool TransmissionDriverTypeIsFullCache { get; protected set; }

        /// <summary>
        /// TransmissionDriverType objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpTransmissionDriverType> TransmissionDriverTypeDict = new Dictionary<int, OpTransmissionDriverType>();

        /// <summary>
        /// Indicates whether the TransmissionDriverType is cached.
        /// </summary>
        public virtual bool TransmissionDriverTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriverType objects
        /// </summary>
        public virtual List<OpTransmissionDriverType> TransmissionDriverType
        {
            get { return GetAllTransmissionDriverType(); }
        }

        /// <summary>
        /// Gets all TransmissionDriverType objects
        /// </summary>
        public virtual List<OpTransmissionDriverType> GetAllTransmissionDriverType()
        {
            try
            {
                if (TransmissionDriverTypeCacheEnabled) //cache enabled
                {
                    if (!TransmissionDriverTypeIsFullCache)
                    {
                        List<OpTransmissionDriverType> objectList = OpTransmissionDriverType.ConvertList(dbConnectionDaq.GetTransmissionDriverType(), this);
                        if (objectList != null)
                        {
                            TransmissionDriverTypeDict = objectList.ToDictionary(o => o.IdTransmissionDriverType);
                            TransmissionDriverTypeIsFullCache = true;
                        }
                    }
                    return new List<OpTransmissionDriverType>(TransmissionDriverTypeDict.Values);
                }
                else //cache disabled
                {
                    return OpTransmissionDriverType.ConvertList(dbConnectionDaq.GetTransmissionDriverType(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverType", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Id">TransmissionDriverType Id</param>
        /// <returns>TransmissionDriverType object</returns>
        public virtual OpTransmissionDriverType GetTransmissionDriverType(int Id)
        {
            return GetTransmissionDriverType(Id, false);
        }

        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverType Ids</param>
        /// <returns>TransmissionDriverType list</returns>
        public virtual List<OpTransmissionDriverType> GetTransmissionDriverType(int[] Ids)
        {
            return GetTransmissionDriverType(Ids, false);
        }

        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Id">TransmissionDriverType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverType object</returns>
        public virtual OpTransmissionDriverType GetTransmissionDriverType(int Id, bool queryDatabase)
        {
            return GetTransmissionDriverType(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets TransmissionDriverType by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverType list</returns>
        public virtual List<OpTransmissionDriverType> GetTransmissionDriverType(int[] Ids, bool queryDatabase)
        {
            List<OpTransmissionDriverType> returnList = new List<OpTransmissionDriverType>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TransmissionDriverTypeCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTransmissionDriverType TransmissionDriverTypeFoundInCache;
                        if (TransmissionDriverTypeDict.TryGetValue(Ids[i], out TransmissionDriverTypeFoundInCache)) //element founded in cache
                            returnList.Add(TransmissionDriverTypeFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTransmissionDriverType item in OpTransmissionDriverType.ConvertList(dbConnectionDaq.GetTransmissionDriverType(idsNotInCache.ToArray()), this))
                            {
                                TransmissionDriverTypeDict[item.IdTransmissionDriverType] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverType", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTransmissionDriverType.ConvertList(dbConnectionDaq.GetTransmissionDriverType(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverType", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the TransmissionDriverType object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriverType to save</param>
        /// <returns>TransmissionDriverType Id</returns>
        public virtual int SaveTransmissionDriverType(OpTransmissionDriverType toBeSaved)
        {
            try
            {
                int ret = dbConnectionDaq.SaveTransmissionDriverType(toBeSaved);
                if (TransmissionDriverTypeCacheEnabled)
                {
                    TransmissionDriverTypeDict[toBeSaved.IdTransmissionDriverType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveTransmissionDriverType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the TransmissionDriverType object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriverType to delete</param>
        public virtual void DeleteTransmissionDriverType(OpTransmissionDriverType toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteTransmissionDriverType(toBeDeleted);
                if (TransmissionDriverTypeCacheEnabled)
                {
                    TransmissionDriverTypeDict.Remove(toBeDeleted.IdTransmissionDriverType);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelTransmissionDriverType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the TransmissionDriverType object cache
        /// </summary>
        public virtual void ClearTransmissionDriverTypeCache()
        {
            TransmissionDriverTypeDict.Clear();
            TransmissionDriverTypeIsFullCache = false;
        }
        #endregion

        #region TransmissionProtocol
        /// <summary>
        /// Indicates whether the TransmissionProtocolDict was filled trough GetAllTransmissionProtocol or GetAllTransmissionProtocolFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool TransmissionProtocolIsFullCache { get; protected set; }

        /// <summary>
        /// TransmissionProtocol objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpTransmissionProtocol> TransmissionProtocolDict = new Dictionary<int, OpTransmissionProtocol>();

        /// <summary>
        /// Indicates whether the TransmissionProtocol is cached.
        /// </summary>
        public bool TransmissionProtocolCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionProtocol objects
        /// </summary>
        public virtual List<OpTransmissionProtocol> TransmissionProtocol
        {
            get { return GetAllTransmissionProtocol(); }
        }

        /// <summary>
        /// Gets all TransmissionProtocol objects
        /// </summary>
        public virtual List<OpTransmissionProtocol> GetAllTransmissionProtocol()
        {
            try
            {
                if (TransmissionProtocolCacheEnabled) //cache enabled
                {
                    if (!TransmissionProtocolIsFullCache)
                    {
                        List<OpTransmissionProtocol> objectList = OpTransmissionProtocol.ConvertList(dbConnectionDaq.GetTransmissionProtocol(), this);
                        if (objectList != null)
                        {
                            TransmissionProtocolDict = objectList.ToDictionary(o => o.IdTransmissionProtocol);
                            TransmissionProtocolIsFullCache = true;
                        }
                    }
                    return new List<OpTransmissionProtocol>(TransmissionProtocolDict.Values);
                }
                else //cache disabled
                {
                    return OpTransmissionProtocol.ConvertList(dbConnectionDaq.GetTransmissionProtocol(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionProtocol", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Id">TransmissionProtocol Id</param>
        /// <returns>TransmissionProtocol object</returns>
        public virtual OpTransmissionProtocol GetTransmissionProtocol(int Id)
        {
            return GetTransmissionProtocol(Id, false);
        }

        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Ids">TransmissionProtocol Ids</param>
        /// <returns>TransmissionProtocol list</returns>
        public virtual List<OpTransmissionProtocol> GetTransmissionProtocol(int[] Ids)
        {
            return GetTransmissionProtocol(Ids, false);
        }

        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Id">TransmissionProtocol Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionProtocol object</returns>
        public virtual OpTransmissionProtocol GetTransmissionProtocol(int Id, bool queryDatabase)
        {
            return GetTransmissionProtocol(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets TransmissionProtocol by id
        /// </summary>
        /// <param name="Ids">TransmissionProtocol Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionProtocol list</returns>
        public virtual List<OpTransmissionProtocol> GetTransmissionProtocol(int[] Ids, bool queryDatabase)
        {
            List<OpTransmissionProtocol> returnList = new List<OpTransmissionProtocol>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TransmissionProtocolCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTransmissionProtocol TransmissionProtocolFoundInCache;
                        if (TransmissionProtocolDict.TryGetValue(Ids[i], out TransmissionProtocolFoundInCache)) //element founded in cache
                            returnList.Add(TransmissionProtocolFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTransmissionProtocol item in OpTransmissionProtocol.ConvertList(dbConnectionDaq.GetTransmissionProtocol(idsNotInCache.ToArray()), this))
                            {
                                TransmissionProtocolDict[item.IdTransmissionProtocol] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionProtocol", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTransmissionProtocol.ConvertList(dbConnectionDaq.GetTransmissionProtocol(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionProtocol", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the TransmissionProtocol object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionProtocol to save</param>
        /// <returns>TransmissionProtocol Id</returns>
        public virtual int SaveTransmissionProtocol(OpTransmissionProtocol toBeSaved)
        {
            try
            {
                int ret = dbConnectionDaq.SaveTransmissionProtocol(toBeSaved);
                if (TransmissionProtocolCacheEnabled)
                {
                    TransmissionProtocolDict[toBeSaved.IdTransmissionProtocol] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveTransmissionProtocol", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the TransmissionProtocol object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionProtocol to delete</param>
        public virtual void DeleteTransmissionProtocol(OpTransmissionProtocol toBeDeleted)
        {
            try
            {
                dbConnectionDaq.DeleteTransmissionProtocol(toBeDeleted);
                if (TransmissionProtocolCacheEnabled)
                {
                    TransmissionProtocolDict.Remove(toBeDeleted.IdTransmissionProtocol);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelTransmissionProtocol", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the TransmissionProtocol object cache
        /// </summary>
        public virtual void ClearTransmissionProtocolCache()
        {
            TransmissionProtocolDict.Clear();
            TransmissionProtocolIsFullCache = false;
        }
        #endregion

    }
}
