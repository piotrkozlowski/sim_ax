﻿using System;
using System.Data;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;
using DW = IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.UI.Business
{
    public partial class DataProvider
    {
        #region GetAlarmFilter
        /// <summary>
        /// Gets Alarm list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarm funcion</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="IdDataTypeAlarm">Specifies filter for ID_DATA_TYPE_ALARM column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="TransmissionType">Specifies filter for TRANSMISSION_TYPE column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Alarm list</returns>
        public virtual List<DW.OpAlarm> GetAlarmFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarm = null, long[] IdAlarmEvent = null, int[] IdAlarmDef = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null,
                            int[] IdAlarmType = null, long[] IdDataTypeAlarm = null, int[] IdOperator = null, int[] IdAlarmStatus = null, string TransmissionType = null,
                            int[] IdTransmissionType = null, int[] IdAlarmGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmFilter"));
            }

            try
            {
                List<DW.OpAlarm> returnList = DW.OpAlarm.ConvertList(dbConnectionDw.GetAlarmFilter(IdAlarm, IdAlarmEvent, IdAlarmDef, SerialNbr, IdMeter, IdLocation,
                             IdAlarmType, IdDataTypeAlarm, IdOperator, IdAlarmStatus, TransmissionType,
                             IdTransmissionType, IdAlarmGroup, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (DW.OpAlarm item in returnList)
                        AlarmDictDW[item.IdAlarm] = item;
                    AlarmIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmEventFilterDW

        /// <summary>
        /// Gets AlarmEvent list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmEvent funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="IdDataTypeAlarm">Specifies filter for ID_DATA_TYPE_ALARM column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IsConfirmed">Specifies filter for IS_CONFIRMED column</param>
        /// <param name="ConfirmedBy">Specifies filter for CONFIRMED_BY column</param>
        /// <param name="ConfirmTime">Specifies filter for CONFIRM_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmEventDW list</returns>
        public List<DW.OpAlarmEvent> GetAlarmEventFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, long[] IdAlarmDef = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdAlarmType = null,
                            int[] IdDataTypeAlarm = null, TypeDateTimeCode Time = null, long[] IdIssue = null, bool? IsConfirmed = null, int[] ConfirmedBy = null,
                            TypeDateTimeCode ConfirmTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmEventFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmEventFilter"));
            }

            try
            {
                List<DW.OpAlarmEvent> returnList = DW.OpAlarmEvent.ConvertList(dbConnectionDw.GetAlarmEventFilterDW(IdAlarmEvent, IdAlarmDef, SerialNbr, IdMeter, IdLocation, IdAlarmType,
                             IdDataTypeAlarm, Time, IdIssue, IsConfirmed, ConfirmedBy,
                             ConfirmTime, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (DW.OpAlarmEvent item in returnList)
                        AlarmEventDictDW[item.IdAlarmEvent] = item;
                    AlarmEventIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmEventFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmHistoryFilter
        /// <summary>
        /// Gets AlarmHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmHistory funcion</param>
        /// <param name="IdAlarmHistory">Specifies filter for ID_ALARM_HISTORY column</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmHistory list</returns>
        public virtual List<DW.OpAlarmHistory> GetAlarmHistoryFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmHistory = null, long[] IdAlarm = null, int[] IdAlarmStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmHistoryFilter"));
            }

            try
            {
                List<DW.OpAlarmHistory> returnList = DW.OpAlarmHistory.ConvertList(dbConnectionDw.GetAlarmHistoryFilter(IdAlarmHistory, IdAlarm, IdAlarmStatus, StartTime, EndTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (DW.OpAlarmHistory item in returnList)
                        AlarmHistoryDictDW[item.IdAlarmHistory] = item;
                    AlarmHistoryIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmMessageFilter
        /// <summary>
        /// Gets AlarmMessage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmMessage funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="AlarmMessage">Specifies filter for ALARM_MESSAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmMessage list</returns>
        public virtual List<DW.OpAlarmMessage> GetAlarmMessageFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, int[] IdLanguage = null, string AlarmMessage = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmMessageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmMessageFilter"));
            }

            try
            {
                List<DW.OpAlarmMessage> returnList = DW.OpAlarmMessage.ConvertList(dbConnectionDw.GetAlarmMessageFilter(IdAlarmEvent, IdLanguage, AlarmMessage, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (DW.OpAlarmMessage item in returnList)
                        AlarmMessageDictDW[item.IdAlarmEvent] = item;
                    AlarmMessageIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmMessageFilter", ex);
                throw;
            }

        }
        #endregion

        #region GetAuditFilterDW

        /// <summary>
        /// Gets Audit list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAudit funcion</param>
        /// <param name="IdAudit">Specifies filter for ID_AUDIT column</param>
        /// <param name="BatchId">Specifies filter for BATCH_ID column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="Key1">Specifies filter for KEY1 column</param>
        /// <param name="Key2">Specifies filter for KEY2 column</param>
        /// <param name="Key3">Specifies filter for KEY3 column</param>
        /// <param name="Key4">Specifies filter for KEY4 column</param>
        /// <param name="Key5">Specifies filter for KEY5 column</param>
        /// <param name="Key6">Specifies filter for KEY6 column</param>
        /// <param name="ColumnName">Specifies filter for COLUMN_NAME column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="Host">Specifies filter for HOST column</param>
        /// <param name="Application">Specifies filter for APPLICATION column</param>
        /// <param name="Contextinfo">Specifies filter for CONTEXTINFO column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_AUDIT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Audit list</returns>
        public virtual List<OpAudit> GetAuditFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAudit = null, long[] BatchId = null, int[] ChangeType = null, string TableName = null, long[] Key1 = null, long[] Key2 = null,
                            long[] Key3 = null, long[] Key4 = null, long[] Key5 = null, long[] Key6 = null, string ColumnName = null,
                            TypeDateTimeCode Time = null, string User = null, string Host = null, string Application = null, string Contextinfo = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAuditFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAuditFilter"));
            }

            try
            {
                List<OpAudit> returnList = OpAudit.ConvertList(dbConnectionDw.GetAuditFilter(IdAudit, BatchId, ChangeType, TableName, Key1, Key2,
                             Key3, Key4, Key5, Key6, ColumnName,
                             Time, User, Host, Application, Contextinfo, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAudit item in returnList)
                        AuditDictDW[item.IdAudit] = item;
                    AuditIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAuditFilter", ex);
                throw;
            }

        }
        #endregion

        #region GetConsumerTransactionFilter

        /// <summary>
        /// Gets ConsumerTransaction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransaction funcion</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdTransactionType">Specifies filter for ID_TRANSACTION_TYPE column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="Timestamp">Specifies filter for TIMESTAMP column</param>
        /// <param name="ChargePrepaid">Specifies filter for CHARGE_PREPAID column</param>
        /// <param name="ChargeEc">Specifies filter for CHARGE_EC column</param>
        /// <param name="ChargeOwed">Specifies filter for CHARGE_OWED column</param>
        /// <param name="BalancePrepaid">Specifies filter for BALANCE_PREPAID column</param>
        /// <param name="BalanceEc">Specifies filter for BALANCE_EC column</param>
        /// <param name="BalanceOwed">Specifies filter for BALANCE_OWED column</param>
        /// <param name="MeterIndex">Specifies filter for METER_INDEX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransaction list</returns>
        public virtual List<DW.OpConsumerTransaction> GetConsumerTransactionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerTransaction = null, int[] IdConsumer = null, int[] IdOperator = null, int[] IdDistributor = null, int[] IdTransactionType = null, int[] IdTariff = null,
                            TypeDateTimeCode Timestamp = null, Double[] ChargePrepaid = null, Double[] ChargeEc = null, Double[] ChargeOwed = null, Double[] BalancePrepaid = null,
                            Double[] BalanceEc = null, Double[] BalanceOwed = null, Double[] MeterIndex = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                            long[] IdConsumerSettlement = null, int[] IdModule = null)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerTransactionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionFilter"));
            }

            try
            {
                List<DW.OpConsumerTransaction> returnList = DW.OpConsumerTransaction.ConvertList(dbConnectionDw.GetConsumerTransactionFilter(IdConsumerTransaction, IdConsumer, IdOperator, IdDistributor, IdTransactionType, IdTariff,
                             Timestamp, ChargePrepaid, ChargeEc, ChargeOwed, BalancePrepaid,
                             BalanceEc, BalanceOwed, MeterIndex, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout, IdConsumerSettlement, IdModule), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (DW.OpConsumerTransaction item in returnList)
                        ConsumerTransactionDict[item.IdConsumerTransaction] = item;
                    ConsumerTransactionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerTransactionDataFilter

        /// <summary>
        /// Gets ConsumerTransactionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransactionData funcion</param>
        /// <param name="IdConsumerTransactionData">Specifies filter for ID_CONSUMER_TRANSACTION_DATA column</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransactionData list</returns>
        public virtual List<DW.OpConsumerTransactionData> GetConsumerTransactionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTransactionData = null, int[] IdConsumerTransaction = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerTransactionDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionDataFilter"));
            }

            try
            {
                List<DW.OpConsumerTransactionData> returnList = DW.OpConsumerTransactionData.ConvertList(dbConnectionDw.GetConsumerTransactionDataFilter(IdConsumerTransactionData, IdConsumerTransaction, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (DW.OpConsumerTransactionData item in returnList)
                        ConsumerTransactionDataDict[item.IdConsumerTransactionData] = item;
                    ConsumerTransactionDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataSourceFilter

        /// <summary>
        /// Gets DataSource list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataSource funcion</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_SOURCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataSource list</returns>
        public virtual List<OpDataSource> GetDataSourceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataSource = null, int[] IdDataSourceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataSourceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataSourceFilter"));
            }

            try
            {
                List<OpDataSource> returnList = OpDataSource.ConvertList(dbConnectionDw.GetDataSourceFilter(IdDataSource, IdDataSourceType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataSource item in returnList)
                        DataSourceDict[item.IdDataSource] = item;
                    DataSourceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataSourceTypeFilter

        /// <summary>
        /// Gets DataSourceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataSourceType funcion</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_SOURCE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataSourceType list</returns>
        public virtual List<OpDataSourceType> GetDataSourceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataSourceType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataSourceTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataSourceTypeFilter"));
            }

            try
            {
                List<OpDataSourceType> returnList = OpDataSourceType.ConvertList(dbConnectionDw.GetDataSourceTypeFilter(IdDataSourceType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataSourceType item in returnList)
                        DataSourceTypeDict[item.IdDataSourceType] = item;
                    DataSourceTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataTemporalFilter

        /// <summary>
        /// Gets DataTemporal list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTemporal funcion</param>
        /// <param name="IdDataTemporal">Specifies filter for ID_DATA_TEMPORAL column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TEMPORAL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTemporal list</returns>
        public virtual List<OpDataTemporal> GetDataTemporalFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataTemporal = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, int[] IdAggregationType = null, long[] IdDataSource = null, int[] IdDataSourceType = null,
                            long[] IdAlarmEvent = null, int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTemporalFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTemporalFilter"));
            }

            try
            {
                List<OpDataTemporal> returnList = OpDataTemporal.ConvertList(dbConnectionDw.GetDataTemporalFilter(IdDataTemporal, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                             StartTime, EndTime, IdAggregationType, IdDataSource, IdDataSourceType,
                             IdAlarmEvent, Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataTemporal item in returnList)
                        DataTemporalDict[item.IdDataTemporal] = item;
                    DataTemporalIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTemporalFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeliveryAdviceFilter

        /// <summary>
        /// Gets DeliveryAdvice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryAdvice funcion</param>
        /// <param name="IdDeliveryAdvice">Specifies filter for ID_DELIVERY_ADVICE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_ADVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryAdvice list</returns>
        public virtual List<OpDeliveryAdvice> GetDeliveryAdviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryAdvice = null, string Name = null, long[] IdDescr = null, int[] Priority = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeliveryAdviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryAdviceFilter"));
            }

            try
            {
                List<OpDeliveryAdvice> returnList = OpDeliveryAdvice.ConvertList(dbConnectionDw.GetDeliveryAdviceFilter(IdDeliveryAdvice, Name, IdDescr, Priority, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeliveryAdvice item in returnList)
                        DeliveryAdviceDict[item.IdDeliveryAdvice] = item;
                    DeliveryAdviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryAdviceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeliveryBatchFilter

        /// <summary>
        /// Gets DeliveryBatch list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryBatch funcion</param>
        /// <param name="IdDeliveryBatch">Specifies filter for ID_DELIVERY_BATCH column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDeliveryBatchStatus">Specifies filter for ID_DELIVERY_BATCH_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="CommitDate">Specifies filter for COMMIT_DATE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="SuccessTotal">Specifies filter for SUCCESS_TOTAL column</param>
        /// <param name="FailedTotal">Specifies filter for FAILED_TOTAL column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_BATCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryBatch list</returns>
        public virtual List<OpDeliveryBatch> GetDeliveryBatchFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryBatch = null, TypeDateTimeCode CreationDate = null, string Name = null, int[] IdDeliveryBatchStatus = null, int[] IdOperator = null, TypeDateTimeCode CommitDate = null,
                            int[] IdDistributor = null, int[] SuccessTotal = null, int[] FailedTotal = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeliveryBatchFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryBatchFilter"));
            }

            try
            {
                List<OpDeliveryBatch> returnList = OpDeliveryBatch.ConvertList(dbConnectionDw.GetDeliveryBatchFilter(IdDeliveryBatch, CreationDate, Name, IdDeliveryBatchStatus, IdOperator, CommitDate,
                             IdDistributor, SuccessTotal, FailedTotal, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeliveryBatch item in returnList)
                        DeliveryBatchDict[item.IdDeliveryBatch] = item;
                    DeliveryBatchIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatchFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeliveryBatchStatusFilter

        /// <summary>
        /// Gets DeliveryBatchStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryBatchStatus funcion</param>
        /// <param name="IdDeliveryBatchStatus">Specifies filter for ID_DELIVERY_BATCH_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_BATCH_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryBatchStatus list</returns>
        public virtual List<OpDeliveryBatchStatus> GetDeliveryBatchStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryBatchStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeliveryBatchStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryBatchStatusFilter"));
            }

            try
            {
                List<OpDeliveryBatchStatus> returnList = OpDeliveryBatchStatus.ConvertList(dbConnectionDw.GetDeliveryBatchStatusFilter(IdDeliveryBatchStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeliveryBatchStatus item in returnList)
                        DeliveryBatchStatusDict[item.IdDeliveryBatchStatus] = item;
                    DeliveryBatchStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatchStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeliveryOrderFilter

        /// <summary>
        /// Gets DeliveryOrder list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryOrder funcion</param>
        /// <param name="IdDeliveryOrder">Specifies filter for ID_DELIVERY_ORDER column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="IdDeliveryBatch">Specifies filter for ID_DELIVERY_BATCH column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDeliveryAdvice">Specifies filter for ID_DELIVERY_ADVICE column</param>
        /// <param name="DeliveryVolume">Specifies filter for DELIVERY_VOLUME column</param>
        /// <param name="IsSuccesfull">Specifies filter for IS_SUCCESFULL column</param>
        /// <param name="OrderNo">Specifies filter for ORDER_NO column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_ORDER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryOrder list</returns>
        public virtual List<OpDeliveryOrder> GetDeliveryOrderFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeliveryOrder = null, TypeDateTimeCode CreationDate = null, int[] IdDeliveryBatch = null, long[] IdLocation = null, long[] IdMeter = null, int[] IdOperator = null,
                            int[] IdDeliveryAdvice = null, int[] DeliveryVolume = null, bool? IsSuccesfull = null, string OrderNo = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeliveryOrderFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryOrderFilter"));
            }

            try
            {
                List<OpDeliveryOrder> returnList = OpDeliveryOrder.ConvertList(dbConnectionDw.GetDeliveryOrderFilter(IdDeliveryOrder, CreationDate, IdDeliveryBatch, IdLocation, IdMeter, IdOperator,
                             IdDeliveryAdvice, DeliveryVolume, IsSuccesfull, OrderNo, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeliveryOrder item in returnList)
                        DeliveryOrderDict[item.IdDeliveryOrder] = item;
                    DeliveryOrderIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrderFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeliveryOrderHistoryFilter

        /// <summary>
        /// Gets DeliveryOrderHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeliveryOrderHistory funcion</param>
        /// <param name="IdDeliveryOrderHistory">Specifies filter for ID_DELIVERY_ORDER_HISTORY column</param>
        /// <param name="IdDeliveryOrder">Specifies filter for ID_DELIVERY_ORDER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IsSuccesfull">Specifies filter for IS_SUCCESFULL column</param>
        /// <param name="CommitDate">Specifies filter for COMMIT_DATE column</param>
        /// <param name="CommitResult">Specifies filter for COMMIT_RESULT column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY_ORDER_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeliveryOrderHistory list</returns>
        public virtual List<OpDeliveryOrderHistory> GetDeliveryOrderHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeliveryOrderHistory = null, int[] IdDeliveryOrder = null, int[] IdOperator = null, bool? IsSuccesfull = null, TypeDateTimeCode CommitDate = null, string CommitResult = null,
                            string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeliveryOrderHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryOrderHistoryFilter"));
            }

            try
            {
                List<OpDeliveryOrderHistory> returnList = OpDeliveryOrderHistory.ConvertList(dbConnectionDw.GetDeliveryOrderHistoryFilter(IdDeliveryOrderHistory, IdDeliveryOrder, IdOperator, IsSuccesfull, CommitDate, CommitResult,
                             Notes, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeliveryOrderHistory item in returnList)
                        DeliveryOrderHistoryDict[item.IdDeliveryOrderHistory] = item;
                    DeliveryOrderHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrderHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceConnectionFilter

        /// <summary>
        /// Gets DeviceConnection list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceConnection funcion</param>
        /// <param name="IdDeviceConnection">Specifies filter for ID_DEVICE_CONNECTION column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="SerialNbrParent">Specifies filter for SERIAL_NBR_PARENT column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_CONNECTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceConnection list</returns>
        public List<OpDeviceConnection> GetDeviceConnectionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceConnection = null, long[] SerialNbr = null, long[] SerialNbrParent = null, TypeDateTimeCode Time = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceConnectionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceConnectionFilter"));
            }

            try
            {
                List<OpDeviceConnection> returnList = OpDeviceConnection.ConvertList(dbConnectionDw.GetDeviceConnectionFilter(IdDeviceConnection, SerialNbr, SerialNbrParent, Time, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceConnection item in returnList)
                        DeviceConnectionDict[item.IdDeviceConnection] = item;
                    DeviceConnectionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceConnectionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceScheduleFilter

        /// <summary>
        /// Gets DeviceSchedule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceSchedule funcion</param>
        /// <param name="IdDeviceSchedule">Specifies filter for ID_DEVICE_SCHEDULE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="DayOfMonth">Specifies filter for DAY_OF_MONTH column</param>
        /// <param name="DayOfWeek">Specifies filter for DAY_OF_WEEK column</param>
        /// <param name="HourOfDay">Specifies filter for HOUR_OF_DAY column</param>
        /// <param name="MinuteOfHour">Specifies filter for MINUTE_OF_HOUR column</param>
        /// <param name="CommandCode">Specifies filter for COMMAND_CODE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_SCHEDULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceSchedule list</returns>
        public virtual List<OpDeviceSchedule> GetDeviceScheduleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceSchedule = null, long[] SerialNbr = null, int[] DayOfMonth = null, int[] DayOfWeek = null, int[] HourOfDay = null, int[] MinuteOfHour = null,
                            int[] CommandCode = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceScheduleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceScheduleFilter"));
            }

            try
            {
                List<OpDeviceSchedule> returnList = OpDeviceSchedule.ConvertList(dbConnectionDw.GetDeviceScheduleFilter(IdDeviceSchedule, SerialNbr, DayOfMonth, DayOfWeek, HourOfDay, MinuteOfHour,
                             CommandCode, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceSchedule item in returnList)
                        DeviceScheduleDict[item.IdDeviceSchedule] = item;
                    DeviceScheduleIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceScheduleFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDistributorPerformanceFilter

        /// <summary>
        /// Gets DistributorPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorPerformance funcion</param>
        /// <param name="IdDistributorPerformance">Specifies filter for ID_DISTRIBUTOR_PERFORMANCE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorPerformance list</returns>
        public virtual List<OpDistributorPerformance> GetDistributorPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDistributorPerformance = null, int[] IdDistributor = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDistributorPerformanceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorPerformanceFilter"));
            }

            try
            {
                List<OpDistributorPerformance> returnList = OpDistributorPerformance.ConvertList(dbConnectionDw.GetDistributorPerformanceFilter(IdDistributorPerformance, IdDistributor, IdAggregationType, IdDataType, IndexNbr, StartTime,
                             EndTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDistributorPerformance item in returnList)
                        DistributorPerformanceDict[item.IdDistributorPerformance] = item;
                    DistributorPerformanceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorPerformanceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetEtlFilter

        /// <summary>
        /// Gets Etl list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllEtl funcion</param>
        /// <param name="IdParam">Specifies filter for ID_PARAM column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PARAM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Etl list</returns>
        //public List<DW.OpEtl> GetEtlFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdParam = null, string Code = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        //{
        //    if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
        //    {
        //        IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetEtlFilter");
        //        throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetEtlFilter"));
        //    }

        //    try
        //    {
        //        List<DW.OpEtl> returnList = DW.OpEtl.ConvertList(dbConnectionDw.GetEtlFilterDW(IdParam, Code, Descr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
        //        if (mergeIntoCache)
        //        {
        //            foreach (DW.OpEtl item in returnList)
        //                EtlDictDW[item.IdParam] = item;
        //            EtlIsFullCache = true;
        //        }
        //        return returnList;
        //    }
        //    catch (Exception ex)
        //    {
        //        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtlFilter", ex);
        //        throw;
        //    }

        //}
        public List<OpEtl> GetEtlFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdParam = null, string Code = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetEtlFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetEtlFilter"));
            }

            try
            {
                List<OpEtl> returnList = OpEtl.ConvertList(dbConnectionDw.GetEtlFilterDW(IdParam, Code, Descr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpEtl item in returnList)
                        EtlDictDW[item.IdParam] = item;
                    EtlIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtlFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetEtlPerformanceFilter

        /// <summary>
        /// Gets EtlPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllEtlPerformance funcion</param>
        /// <param name="IdEtlPerformance">Specifies filter for ID_ETL_PERFORMANCE column</param>
        /// <param name="IdEtl">Specifies filter for ID_ETL column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ETL_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>EtlPerformance list</returns>
        public List<OpEtlPerformance> GetEtlPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdEtlPerformance = null, int[] IdEtl = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, int[] IdDataSourceType = null, string Code = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetEtlPerformanceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetEtlPerformanceFilter"));
            }

            try
            {
                List<OpEtlPerformance> returnList = OpEtlPerformance.ConvertList(dbConnectionDw.GetEtlPerformanceFilter(IdEtlPerformance, IdEtl, IdAggregationType, IdDataType, IndexNbr, StartTime,
                             EndTime, IdDataSourceType, Code, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpEtlPerformance item in returnList)
                        EtlPerformanceDict[item.IdEtlPerformance] = item;
                    EtlPerformanceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtlPerformanceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetImrServerPerformanceFilter

        /// <summary>
        /// Gets ImrServerPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllImrServerPerformance funcion</param>
        /// <param name="IdImrServerPerformance">Specifies filter for ID_IMR_SERVER_PERFORMANCE column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_IMR_SERVER_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ImrServerPerformance list</returns>
        public virtual List<OpImrServerPerformance> GetImrServerPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdImrServerPerformance = null, int[] IdImrServer = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetImrServerPerformanceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetImrServerPerformanceFilter"));
            }

            try
            {
                List<OpImrServerPerformance> returnList = OpImrServerPerformance.ConvertList(dbConnectionDw.GetImrServerPerformanceFilter(IdImrServerPerformance, IdImrServer, IdAggregationType, IdDataType, IndexNbr, StartTime,
                             EndTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpImrServerPerformance item in returnList)
                        ImrServerPerformanceDict[item.IdImrServerPerformance] = item;
                    ImrServerPerformanceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetImrServerPerformanceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationKpiFilter

        /// <summary>
        /// Gets LocationKpi list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRefuel funcion</param>
        /// <param name="IdLocationKpi">Specifies filter for ID_LOCATION_KPI column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="New">Specifies filter for NEW column</param>
        /// <param name="Operational">Specifies filter for OPERATIONAL column</param>
        /// <param name="Suspended">Specifies filter for SUSPENDED column</param>
        /// <param name="Pending">Specifies filter for PENDING column</param>
        /// <param name="KpiRelevant">Specifies filter for KPI_RELEVANT column</param>
        /// <param name="KpiIrrelevant">Specifies filter for KPI_IRRELEVANT column</param>
        /// <param name="KpiConforming">Specifies filter for KPI_CONFORMING column</param>
        /// <param name="KpiNonConforming">Specifies filter for KPI_NONCONFORMING column</param>
        /// <param name="SrtConforming">Specifies filter for SRT_CONFORMING column</param>
        /// <param name="IsMantenanceOnly">Specifies filter for IS_MAINTENANCE_ONLY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFUEL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Refuel list</returns>
        public virtual List<OpLocationKpi> GetLocationKpiFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, 
            long[] IdLocationKpi = null, int[] IdDistributor = null, TypeDateTimeCode Date = null,
            int[] New = null, int[] Operational = null, int[] Suspended = null, int[] Pending = null,
            int[] KpiRelevant = null, int[] KpiIrrelevant = null, long[] KpiConforming = null, long[] KpiNonConforming = null, int[] SrtConforming = null, bool? IsMantenanceOnly = null, 
            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationKpi");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationKpi"));
            }

            try
            {
                List<OpLocationKpi> returnList = OpLocationKpi.ConvertList(dbConnectionDw.GetLocationKpiFilter(IdLocationKpi, IdDistributor, Date, New, Operational, Suspended, Pending, 
                    KpiRelevant, KpiIrrelevant, KpiConforming, KpiNonConforming, SrtConforming, IsMantenanceOnly,
                    topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationKpi item in returnList)
                        LocationKpiDict[item.IdLocationKpi] = item;
                    RefuelIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationKpi", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationDistanceFilter

        /// <summary>
        /// Gets LocationDistance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationDistance funcion</param>
        /// <param name="IdLocationOrigin">Specifies filter for ID_LOCATION_ORIGIN column</param>
        /// <param name="IdLocationDest">Specifies filter for ID_LOCATION_DEST column</param>
        /// <param name="DistanceStraight">Specifies filter for DISTANCE_STRAIGHT column</param>
        /// <param name="DistanceFormula">Specifies filter for DISTANCE_FORMULA column</param>
        /// <param name="DistanceGoogle">Specifies filter for DISTANCE_GOOGLE column</param>
        /// <param name="LastGoogleUpdate">Specifies filter for LAST_GOOGLE_UPDATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_ORIGIN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationDistance list</returns>
        public virtual List<OpLocationDistance> GetLocationDistanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationOrigin = null, long[] IdLocationDest = null, Double[] DistanceStraight = null, Double[] DistanceFormula = null, Double[] DistanceGoogle = null, TypeDateTimeCode LastGoogleUpdate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationDistanceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationDistanceFilter"));
            }

            try
            {
                List<OpLocationDistance> returnList = OpLocationDistance.ConvertList(dbConnectionDw.GetLocationDistanceFilter(IdLocationOrigin, IdLocationDest, DistanceStraight, DistanceFormula, DistanceGoogle, LastGoogleUpdate,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationDistance item in returnList)
                        LocationDistanceDict[item.IdLocationOrigin] = item;
                    LocationDistanceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationDistanceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMappingFilterDW

        /// <summary>
        /// Gets Mapping list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMapping funcion</param>
        /// <param name="IdMapping">Specifies filter for ID_MAPPING column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdSource">Specifies filter for ID_SOURCE column</param>
        /// <param name="IdDestination">Specifies filter for ID_DESTINATION column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="Timestamp">Specifies filter for TIMESTAMP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MAPPING DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Mapping list</returns>
        public List<DW.OpMapping> GetMappingFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMapping = null, int[] IdReferenceType = null, long[] IdSource = null, long[] IdDestination = null, int[] IdImrServer = null, TypeDateTimeCode Timestamp = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMappingFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMappingFilter"));
            }

            try
            {
                List<DW.OpMapping> returnList = DW.OpMapping.ConvertList(dbConnectionDw.GetMappingFilterDW(IdMapping, IdReferenceType, IdSource, IdDestination, IdImrServer, Timestamp,
                             topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (DW.OpMapping item in returnList)
                        MappingDictDW[item.IdMapping] = item;
                    MappingIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMappingFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetModuleHistoryFilter

        /// <summary>
        /// Gets ModuleHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllModuleHistory funcion</param>
        /// <param name="IdModuleHistory">Specifies filter for ID_MODULE_HISTORY column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="CommonName">Specifies filter for COMMON_NAME column</param>
        /// <param name="Version">Specifies filter for VERSION column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdOperatorUpgrade">Specifies filter for ID_OPERATOR_UPGRADE column</param>
        /// <param name="IdOperatorAccept">Specifies filter for ID_OPERATOR_ACCEPT column</param>
        /// <param name="UpgradeDescription">Specifies filter for UPGRADE_DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MODULE_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ModuleHistory list</returns>
        public virtual List<OpModuleHistory> GetModuleHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdModuleHistory = null, int[] IdModule = null, string CommonName = null, string Version = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            int[] IdOperatorUpgrade = null, int[] IdOperatorAccept = null, string UpgradeDescription = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetModuleHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetModuleHistoryFilter"));
            }

            try
            {
                List<OpModuleHistory> returnList = OpModuleHistory.ConvertList(dbConnectionDw.GetModuleHistoryFilter(IdModuleHistory, IdModule, CommonName, Version, StartTime, EndTime,
                             IdOperatorUpgrade, IdOperatorAccept, UpgradeDescription, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpModuleHistory item in returnList)
                        ModuleHistoryDict[item.IdModuleHistory] = item;
                    ModuleHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetModuleHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMonitoringWatcherFilterDW

        /// <summary>
        /// Gets MonitoringWatcher list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcher funcion</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcher list</returns>
        public List<DW.OpMonitoringWatcher> GetMonitoringWatcherFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcher = null, int[] IdMonitoringWatcherType = null, bool? IsActive = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMonitoringWatcherFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherFilter"));
            }

            try
            {                
                List<DW.OpMonitoringWatcher> returnList = DW.OpMonitoringWatcher.ConvertList(dbConnectionDw.GetMonitoringWatcherFilterDW(IdMonitoringWatcher, IdMonitoringWatcherType, IsActive, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (DW.OpMonitoringWatcher item in returnList)
                        MonitoringWatcherDictDW[item.IdMonitoringWatcher] = item;
                    MonitoringWatcherIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherFilter", ex);
                throw;
            }

        }
        #endregion        
        #region GetMonitoringWatcherDataFilterDW

        /// <summary>
        /// Gets MonitoringWatcherData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherData funcion</param>
        /// <param name="IdMonitoringWatcherData">Specifies filter for ID_MONITORING_WATCHER_DATA column</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Index">Specifies filter for INDEX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherData list</returns>
        public List<DW.OpMonitoringWatcherData> GetMonitoringWatcherDataFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherData = null, int[] IdMonitoringWatcher = null, long[] IdDataType = null, int[] Index = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMonitoringWatcherDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherDataFilter"));
            }

            try
            {               
                List<DW.OpMonitoringWatcherData> returnList = DW.OpMonitoringWatcherData.ConvertList(dbConnectionDw.GetMonitoringWatcherDataFilterDW(IdMonitoringWatcherData, IdMonitoringWatcher, IdDataType, Index, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (DW.OpMonitoringWatcherData item in returnList)
                        MonitoringWatcherDataDictDW[item.IdMonitoringWatcherData] = item;
                    MonitoringWatcherDataIsFullCacheDW = true;
                }
                return returnList;                
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMonitoringWatcherTypeFilterDW

        /// <summary>
        /// Gets MonitoringWatcherType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherType funcion</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Plugin">Specifies filter for PLUGIN column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherType list</returns>
        public List<DW.OpMonitoringWatcherType> GetMonitoringWatcherTypeFilterDW(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherType = null, string Name = null, string Plugin = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMonitoringWatcherTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherTypeFilter"));
            }

            try
            {                
                List<DW.OpMonitoringWatcherType> returnList = DW.OpMonitoringWatcherType.ConvertList(dbConnectionDw.GetMonitoringWatcherTypeFilterDW(IdMonitoringWatcherType, Name, Plugin, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (DW.OpMonitoringWatcherType item in returnList)
                        MonitoringWatcherTypeDictDW[item.IdMonitoringWatcherType] = item;
                    MonitoringWatcherTypeIsFullCacheDW = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetProblemClassFilter

        /// <summary>
        /// Gets ProblemClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProblemClass funcion</param>
        /// <param name="IdProblemClass">Specifies filter for ID_PROBLEM_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROBLEM_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProblemClass list</returns>
        public virtual List<OpProblemClass> GetProblemClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProblemClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetProblemClassFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProblemClassFilter"));
            }

            try
            {
                List<OpProblemClass> returnList = OpProblemClass.ConvertList(dbConnectionDw.GetProblemClassFilter(IdProblemClass, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpProblemClass item in returnList)
                        ProblemClassDict[item.IdProblemClass] = item;
                    ProblemClassIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProblemClassFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRefuelDataFilter

        /// <summary>
        /// Gets RefuelData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRefuelData funcion</param>
        /// <param name="IdRefuelData">Specifies filter for ID_REFUEL_DATA column</param>
        /// <param name="IdRefuel">Specifies filter for ID_REFUEL column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFUEL_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RefuelData list</returns>
        public virtual List<OpRefuelData> GetRefuelDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRefuelData = null, long[] IdRefuel = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRefuelDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRefuelDataFilter"));
            }

            try
            {
                List<OpRefuelData> returnList = OpRefuelData.ConvertList(dbConnectionDw.GetRefuelDataFilter(IdRefuelData, IdRefuel, IdDataType, IndexNbr, StartTime, EndTime,
                             Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRefuelData item in returnList)
                        RefuelDataDict[item.IdRefuelData] = item;
                    RefuelDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuelDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetReportFilter

        /// <summary>
        /// Gets Report list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReport funcion</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="IdReportType">Specifies filter for ID_REPORT_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Report list</returns>
        public virtual List<OpReport> GetReportFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdReport = null, int[] IdReportType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
            bool loadCustomData = true, List<long> customDataTypes = null)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetReportFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportFilter"));
            }

            try
            {
                List<OpReport> returnList = OpReport.ConvertList(dbConnectionDw.GetReportFilter(IdReport, IdReportType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes);
                if (mergeIntoCache)
                {
                    foreach (OpReport item in returnList)
                        ReportDict[item.IdReport] = item;
                    ReportIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetReportDataFilter

        /// <summary>
        /// Gets ReportData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportData funcion</param>
        /// <param name="IdReportData">Specifies filter for ID_REPORT_DATA column</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportData list</returns>
        public virtual List<OpReportData> GetReportDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdReportData = null, int[] IdReport = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetReportDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportDataFilter"));
            }

            try
            {
                List<OpReportData> returnList = OpReportData.ConvertList(dbConnectionDw.GetReportDataFilter(IdReportData, IdReport, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpReportData item in returnList)
                        ReportDataDict[item.IdReportData] = item;
                    ReportDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetReportParamFilter

        /// <summary>
        /// Gets ReportParam list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportParam funcion</param>
        /// <param name="IdReportParam">Specifies filter for ID_REPORT_PARAM column</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="FieldName">Specifies filter for FIELD_NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdReference">Specifies filter for ID_REFERENCE column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="IsInputParam">Specifies filter for IS_INPUT_PARAM column</param>
        /// <param name="IsVisible">Specifies filter for IS_VISIBLE column</param>
        /// <param name="ColorBg">Specifies filter for COLOR_BG column</param>
        /// <param name="Format">Specifies filter for FORMAT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_PARAM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportParam list</returns>
        public virtual List<OpReportParam> GetReportParamFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdReportParam = null, int[] IdReport = null, string FieldName = null, long[] IdDescr = null, int[] IdUnit = null, string Name = null,
                            int[] IdReference = null, int[] IdDataTypeClass = null, bool? IsInputParam = null, bool? IsVisible = null, string ColorBg = null,
                            string Format = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetReportParamFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportParamFilter"));
            }

            try
            {
                List<OpReportParam> returnList = OpReportParam.ConvertList(dbConnectionDw.GetReportParamFilter(IdReportParam, IdReport, FieldName, IdDescr, IdUnit, Name,
                             IdReference, IdDataTypeClass, IsInputParam, IsVisible, ColorBg,
                             Format, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData);
                if (mergeIntoCache)
                {
                    foreach (OpReportParam item in returnList)
                        ReportParamDict[item.IdReportParam] = item;
                    ReportParamIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportParamFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetReportPerformanceFilter

        /// <summary>
        /// Gets ReportPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportPerformance funcion</param>
        /// <param name="IdReportPerformance">Specifies filter for ID_REPORT_PERFORMANCE column</param>
        /// <param name="IdReport">Specifies filter for ID_REPORT column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportPerformance list</returns>
        public virtual List<OpReportPerformance> GetReportPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdReportPerformance = null, int[] IdReport = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, int[] IdDataSourceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetReportPerformanceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportPerformanceFilter"));
            }

            try
            {
                List<OpReportPerformance> returnList = OpReportPerformance.ConvertList(dbConnectionDw.GetReportPerformanceFilter(IdReportPerformance, IdReport, IdAggregationType, IdDataType, IndexNbr, StartTime,
                             EndTime, IdDataSourceType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpReportPerformance item in returnList)
                        ReportPerformanceDict[item.IdReportPerformance] = item;
                    ReportPerformanceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportPerformanceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetReportTypeFilter

        /// <summary>
        /// Gets ReportType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReportType funcion</param>
        /// <param name="IdReportType">Specifies filter for ID_REPORT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportType list</returns>
        public virtual List<OpReportType> GetReportTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdReportType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetReportTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportTypeFilter"));
            }

            try
            {
                List<OpReportType> returnList = OpReportType.ConvertList(dbConnectionDw.GetReportTypeFilter(IdReportType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpReportType item in returnList)
                        ReportTypeDict[item.IdReportType] = item;
                    ReportTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataMissingAnalogReadoutsFilter

        /// <summary>
        /// Gets DataMissingAnalogReadouts list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataMissingAnalogReadouts funcion</param>
        /// <param name="IdDataMissingAnalogReadouts">Specifies filter for ID_DATA_MISSING_ANALOG_READOUTS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="SerialNbrParent">Specifies filter for SERIAL_NBR_PARENT column</param>
        /// <param name="MissingDataStartTime">Specifies filter for MISSING_DATA_START_TIME column</param>
        /// <param name="PacketsCount">Specifies filter for PACKETS_COUNT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_MISSING_ANALOG_READOUTS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataMissingAnalogReadouts list</returns>
        public virtual List<OpDataMissingAnalogReadouts> GetDataMissingAnalogReadoutsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataMissingAnalogReadouts = null, long[] SerialNbr = null, long[] SerialNbrParent = null, TypeDateTimeCode MissingDataStartTime = null, int[] PacketsCount = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataMissingAnalogReadoutsFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataMissingAnalogReadoutsFilter"));
            }

            try
            {
                List<OpDataMissingAnalogReadouts> returnList = OpDataMissingAnalogReadouts.ConvertList(dbConnectionDw.GetDataMissingAnalogReadoutsFilter(IdDataMissingAnalogReadouts, SerialNbr, SerialNbrParent, MissingDataStartTime, PacketsCount, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataMissingAnalogReadouts item in returnList)
                        DataMissingAnalogReadoutsDict[item.IdDataMissingAnalogReadouts] = item;
                    DataMissingAnalogReadoutsIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataMissingAnalogReadoutsFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPacketHourTransmissionDriverFilter

        /// <summary>
        /// Gets PacketHourTransmissionDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketHourTransmissionDriver funcion</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketHourTransmissionDriver list</returns>
        public virtual List<OpPacketHourTransmissionDriver> GetPacketHourTransmissionDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, int[] IdTransmissionType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPacketHourTransmissionDriverFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketHourTransmissionDriverFilter"));
            }

            try
            {
                List<OpPacketHourTransmissionDriver> returnList = OpPacketHourTransmissionDriver.ConvertList(dbConnectionDw.GetPacketHourTransmissionDriverFilter(IdTransmissionDriver, Date, IsIncoming, Packets, IdTransmissionType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPacketHourTransmissionDriver item in returnList)
                        PacketHourTransmissionDriverDict[item.IdTransmissionDriver] = item;
                    PacketHourTransmissionDriverIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketHourTransmissionDriverFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPacketHourAddressFilter

        /// <summary>
        /// Gets PacketHourAddress list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketHourAddress funcion</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ADDRESS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketHourAddress list</returns>
        public virtual List<OpPacketHourAddress> GetPacketHourAddressFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, string Address = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPacketHourAddressFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketHourAddressFilter"));
            }

            try
            {
                List<OpPacketHourAddress> returnList = OpPacketHourAddress.ConvertList(dbConnectionDw.GetPacketHourAddressFilter(Address, Date, IsIncoming, Packets, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPacketHourAddress item in returnList)
                        PacketHourAddressDict[item.Address] = item;
                    PacketHourAddressIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketHourAddressFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPacketHourDeviceFilter

        /// <summary>
        /// Gets PacketHourDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketHourDevice funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketHourDevice list</returns>
        public virtual List<OpPacketHourDevice> GetPacketHourDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPacketHourDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketHourDeviceFilter"));
            }

            try
            {
                List<OpPacketHourDevice> returnList = OpPacketHourDevice.ConvertList(dbConnectionDw.GetPacketHourDeviceFilter(SerialNbr, IdTransmissionDriver, Date, IsIncoming, Packets, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPacketHourDevice item in returnList)
                        PacketHourDeviceDict[item.SerialNbr] = item;
                    PacketHourDeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketHourDeviceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPacketTrashHourAddressFilter

        /// <summary>
        /// Gets PacketTrashHourAddress list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketTrashHourAddress funcion</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ADDRESS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketTrashHourAddress list</returns>
        public virtual List<OpPacketTrashHourAddress> GetPacketTrashHourAddressFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, string Address = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPacketTrashHourAddressFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketTrashHourAddressFilter"));
            }

            try
            {
                List<OpPacketTrashHourAddress> returnList = OpPacketTrashHourAddress.ConvertList(dbConnectionDw.GetPacketTrashHourAddressFilter(Address, Date, IsIncoming, Packets, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPacketTrashHourAddress item in returnList)
                        PacketTrashHourAddressDict[item.Address] = item;
                    PacketTrashHourAddressIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketTrashHourAddressFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPacketTrashHourDeviceFilter

        /// <summary>
        /// Gets PacketTrashHourDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketTrashHourDevice funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketTrashHourDevice list</returns>
        public virtual List<OpPacketTrashHourDevice> GetPacketTrashHourDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPacketTrashHourDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketTrashHourDeviceFilter"));
            }

            try
            {
                List<OpPacketTrashHourDevice> returnList = OpPacketTrashHourDevice.ConvertList(dbConnectionDw.GetPacketTrashHourDeviceFilter(SerialNbr, IdTransmissionDriver, Date, IsIncoming, Packets, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPacketTrashHourDevice item in returnList)
                        PacketTrashHourDeviceDict[item.SerialNbr] = item;
                    PacketTrashHourDeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketTrashHourDeviceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPacketTrashHourTransmissionDriverFilter

        /// <summary>
        /// Gets PacketTrashHourTransmissionDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPacketTrashHourTransmissionDriver funcion</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="Packets">Specifies filter for PACKETS column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PacketTrashHourTransmissionDriver list</returns>
        public virtual List<OpPacketTrashHourTransmissionDriver> GetPacketTrashHourTransmissionDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionDriver = null, TypeDateTimeCode Date = null, bool? IsIncoming = null, int[] Packets = null, int[] IdTransmissionType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPacketTrashHourTransmissionDriverFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPacketTrashHourTransmissionDriverFilter"));
            }

            try
            {
                List<OpPacketTrashHourTransmissionDriver> returnList = OpPacketTrashHourTransmissionDriver.ConvertList(dbConnectionDw.GetPacketTrashHourTransmissionDriverFilter(IdTransmissionDriver, Date, IsIncoming, Packets, IdTransmissionType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPacketTrashHourTransmissionDriver item in returnList)
                        PacketTrashHourTransmissionDriverDict[item.IdTransmissionDriver] = item;
                    PacketTrashHourTransmissionDriverIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPacketTrashHourTransmissionDriverFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTemperatureCoefficientFilter

        /// <summary>
        /// Gets TemperatureCoefficient list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTemperatureCoefficient funcion</param>
        /// <param name="IdTemperatureCoefficient">Specifies filter for ID_TEMPERATURE_COEFFICIENT column</param>
        /// <param name="Temperature">Specifies filter for TEMPERATURE column</param>
        /// <param name="Density">Specifies filter for DENSITY column</param>
        /// <param name="Value">Specifies filter for VALUE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TEMPERATURE_COEFFICIENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TemperatureCoefficient list</returns>
        public virtual List<OpTemperatureCoefficient> GetTemperatureCoefficientFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTemperatureCoefficient = null, Double[] Temperature = null, Double[] Density = null, Double[] Value = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTemperatureCoefficientFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTemperatureCoefficientFilter"));
            }

            try
            {
                List<OpTemperatureCoefficient> returnList = OpTemperatureCoefficient.ConvertList(dbConnectionDw.GetTemperatureCoefficientFilter(IdTemperatureCoefficient, Temperature, Density, Value, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTemperatureCoefficient item in returnList)
                        TemperatureCoefficientDict[item.IdTemperatureCoefficient] = item;
                    TemperatureCoefficientIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTemperatureCoefficientFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTransmissionDriverPerformanceFilter

        /// <summary>
        /// Gets TransmissionDriverPerformance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionDriverPerformance funcion</param>
        /// <param name="IdTransmissionDriverPerformance">Specifies filter for ID_TRANSMISSION_DRIVER_PERFORMANCE column</param>
        /// <param name="IdTransmissionDriver">Specifies filter for ID_TRANSMISSION_DRIVER column</param>
        /// <param name="IdAggregationType">Specifies filter for ID_AGGREGATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_DRIVER_PERFORMANCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionDriverPerformance list</returns>
        public virtual List<OpTransmissionDriverPerformance> GetTransmissionDriverPerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTransmissionDriverPerformance = null, int[] IdTransmissionDriver = null, int[] IdAggregationType = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode StartTime = null,
                            TypeDateTimeCode EndTime = null, int[] IdDataSourceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTransmissionDriverPerformanceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionDriverPerformanceFilter"));
            }

            try
            {
                List<OpTransmissionDriverPerformance> returnList = OpTransmissionDriverPerformance.ConvertList(dbConnectionDw.GetTransmissionDriverPerformanceFilter(IdTransmissionDriverPerformance, IdTransmissionDriver, IdAggregationType, IdDataType, IndexNbr, StartTime,
                             EndTime, IdDataSourceType, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTransmissionDriverPerformance item in returnList)
                        TransmissionDriverPerformanceDict[item.IdTransmissionDriverPerformance] = item;
                    TransmissionDriverPerformanceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverPerformanceFilter", ex);
                throw;
            }

        }
        #endregion
    }

    //User part
    public partial class DataProvider
    {
        #region GetReportDataTypeFilter

        /// <summary>
        /// Gets ReportDataType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdReportDataType">Specifies filter for ID_REPORT_DATA_TYPE column</param>
        /// <param name="Signature">Specifies filter for SIGNATURE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="DigitsAfterComma">Specifies filter for DIGITS_AFTER_COMMA column</param>
        /// <param name="SequenceNbr">Specifies filter for SEQUENCE_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REPORT_DATA_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReportDataType list</returns>
        public virtual List<OpReportDataType> GetReportDataTypeFilter(bool loadNavigationProperties = true, long[] IdReportDataType = null, string Signature = null, long[] IdDataType = null, int[] IdUnit = null, int[] DigitsAfterComma = null, int[] SequenceNbr = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetReportDataTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReportDataTypeFilter"));
            }

            try
            {
                List<OpReportDataType> returnList = OpReportDataType.ConvertList(dbConnectionDw.GetReportDataTypeFilter(IdReportDataType, Signature, IdDataType, IdUnit, DigitsAfterComma, SequenceNbr,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportDataTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSystemDataFilterDW

        /// <summary>
        /// Gets SystemData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdSystemData">Specifies filter for ID_SYSTEM_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SYSTEM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SystemData list</returns>
        public virtual List<OpSystemData> GetSystemDataFilterDW(bool loadNavigationProperties = true, long[] IdSystemData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSystemDataFilterDW");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSystemDataFilter"));
            }

            try
            {
                List<OpSystemData> returnList = OpSystemData.ConvertList(dbConnectionDw.GetSystemDataFilter(IdSystemData, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSystemDataFilter", ex);
                throw;
            }

        }
        #endregion
    }
}
