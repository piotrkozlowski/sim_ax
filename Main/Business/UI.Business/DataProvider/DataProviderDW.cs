﻿using System;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using System.Data;
using IMR.Suite.UI.Business.Objects.DW;
using CORE = IMR.Suite.UI.Business.Objects.CORE;
//using IMR.Suite.UI.Business.Objects.Domain.DW;
//using CORE = IMR.Suite.UI.Business.Objects.Domain.CORE;

namespace IMR.Suite.UI.Business
{
    public partial class DataProvider
    {
        #region Alarm
        /// <summary>
        /// Indicates whether the AlarmDict was filled trough GetAllAlarm or GetAllAlarmFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool AlarmIsFullCacheDW { get; protected set; }

        /// <summary>
        /// Alarm objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpAlarm> AlarmDictDW = new Dictionary<long, OpAlarm>();

        /// <summary>
        /// Indicates whether the Alarm is cached.
        /// </summary>
        public bool AlarmCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all Alarm objects
        /// </summary>
        public virtual List<OpAlarm> AlarmDW
        {
            get { return GetAllAlarmDW(); }
        }

        /// <summary>
        /// Gets all Alarm objects
        /// </summary>
        public virtual List<OpAlarm> GetAllAlarmDW()
        {
            try
            {
                if (AlarmCacheEnabledDW) //cache enabled
                {
                    if (!AlarmIsFullCacheDW)
                    {
                        List<OpAlarm> objectList = OpAlarm.ConvertList(dbConnectionDw.GetAlarm(), this);
                        if (objectList != null)
                        {
                            AlarmDictDW = objectList.ToDictionary(o => o.IdAlarm);
                            AlarmIsFullCacheDW = true;
                        }
                    }
                    return new List<OpAlarm>(AlarmDictDW.Values);
                }
                else //cache disabled
                {
                    return OpAlarm.ConvertList(dbConnectionDw.GetAlarm(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarm", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Id">Alarm Id</param>
        /// <returns>Alarm object</returns>
        public virtual OpAlarm GetAlarmDW(long Id)
        {
            return GetAlarmDW(Id, false);
        }

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Ids">Alarm Ids</param>
        /// <returns>Alarm list</returns>
        public virtual List<OpAlarm> GetAlarmDW(long[] Ids)
        {
            return GetAlarmDW(Ids, false);
        }

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Id">Alarm Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Alarm object</returns>
        public virtual OpAlarm GetAlarmDW(long Id, bool queryDatabase)
        {
            return GetAlarmDW(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Alarm by id
        /// </summary>
        /// <param name="Ids">Alarm Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Alarm list</returns>
        public virtual List<OpAlarm> GetAlarmDW(long[] Ids, bool queryDatabase)
        {
            List<OpAlarm> returnList = new List<OpAlarm>();
            if (Ids != null && Ids.Length > 0)
            {
                if (AlarmCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpAlarm AlarmFoundInCache;
                        if (AlarmDictDW.TryGetValue(Ids[i], out AlarmFoundInCache)) //element founded in cache
                            returnList.Add(AlarmFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpAlarm item in OpAlarm.ConvertList(dbConnectionDw.GetAlarm(idsNotInCache.ToArray()), this))
                            {
                                AlarmDictDW[item.IdAlarm] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarm", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpAlarm.ConvertList(dbConnectionDw.GetAlarm(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarm", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Alarm object to the database
        /// </summary>
        /// <param name="toBeSaved">Alarm to save</param>
        /// <returns>Alarm Id</returns>
        public virtual long SaveAlarmDw(OpAlarm toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveAlarm(toBeSaved);
                if (AlarmCacheEnabled)
                {
                    AlarmDictDW[toBeSaved.IdAlarm] = toBeSaved as OpAlarm; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveAlarm", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Alarm object from the database
        /// </summary>
        /// <param name="toBeDeleted">Alarm to delete</param>
        public virtual void DeleteAlarmDw(OpAlarm toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteAlarm(toBeDeleted);
                if (AlarmCacheEnabled)
                {
                    AlarmDictDW.Remove(toBeDeleted.IdAlarm);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelAlarm", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the Alarm object cache
        /// </summary>
        public virtual void ClearAlarmCacheDW()
        {
            AlarmDictDW.Clear();
            AlarmIsFullCacheDW = false;
        }
        #endregion

        #region AlarmEventDW
        /// <summary>
        /// Indicates whether the AlarmEventDictDW was filled trough GetAllAlarmEvent or GetAllAlarmEventFilter with mergeIntoCache = true
        /// </summary>
        public bool AlarmEventIsFullCacheDW { get; private set; }

        /// <summary>
        /// AlarmEventDW objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpAlarmEvent> AlarmEventDictDW = new Dictionary<long, OpAlarmEvent>();

        /// <summary>
        /// Indicates whether the AlarmEventDW is cached.
        /// </summary>
        public bool AlarmEventCacheEnabledDW { get; set; }

        /// <summary>
    /// Gets all AlarmEventDW objects
    /// </summary>
        public List<OpAlarmEvent> AlarmEventDW
        {
            get { return GetAllAlarmEventDW(); }
        }

        /// <summary>
        /// Gets all AlarmEventDW objects
        /// </summary>
        public List<OpAlarmEvent> GetAllAlarmEventDW()
        {
            try
            {
                if (AlarmEventCacheEnabledDW) //cache enabled
                {
                    if (!AlarmEventIsFullCacheDW)
                    {
                        List<OpAlarmEvent> objectList = OpAlarmEvent.ConvertList(dbConnectionDw.GetAlarmEventDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            AlarmEventDictDW = objectList.ToDictionary(o => o.IdAlarmEvent);
                            AlarmEventIsFullCacheDW = true;
                        }
                    }
                    return new List<OpAlarmEvent>(AlarmEventDictDW.Values);
                }
                else //cache disabled
                {
                    return OpAlarmEvent.ConvertList(dbConnectionDw.GetAlarmEventDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmEvent", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Id">AlarmEventDW Id</param>
        /// <returns>AlarmEventDW object</returns>
        public OpAlarmEvent GetAlarmEventDW(long Id)
        {
            return GetAlarmEventDW(Id, false);
        }

        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Ids">AlarmEventDW Ids</param>
        /// <returns>AlarmEventDW list</returns>
        public List<OpAlarmEvent> GetAlarmEventDW(long[] Ids)
        {
            return GetAlarmEventDW(Ids, false);
        }

        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Id">AlarmEventDW Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmEventDW object</returns>
        public OpAlarmEvent GetAlarmEventDW(long Id, bool queryDatabase)
        {
            return GetAlarmEventDW(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets AlarmEventDW by id
        /// </summary>
        /// <param name="Ids">AlarmEventDW Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmEventDW list</returns>
        public List<OpAlarmEvent> GetAlarmEventDW(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpAlarmEvent> returnList = new List<OpAlarmEvent>();
            if (Ids != null && Ids.Length > 0)
            {
                if (AlarmEventCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpAlarmEvent AlarmEventFoundInCacheDW;
                        if (AlarmEventDictDW.TryGetValue(Ids[i], out AlarmEventFoundInCacheDW)) //element founded in cache
                            returnList.Add(AlarmEventFoundInCacheDW);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpAlarmEvent item in OpAlarmEvent.ConvertList(dbConnectionDw.GetAlarmEventDW(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                AlarmEventDictDW[item.IdAlarmEvent] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmEvent", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpAlarmEvent.ConvertList(dbConnectionDw.GetAlarmEventDW(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmEvent", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the AlarmEventDW object to the database
        /// </summary>
        /// <param name="toBeSaved">AlarmEventDW to save</param>
        /// <returns>AlarmEventDW Id</returns>
        public long SaveAlarmEventDW(OpAlarmEvent toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveAlarmEventDW(toBeSaved);
                if (AlarmEventCacheEnabledDW)
                {
                    AlarmEventDictDW[toBeSaved.IdAlarmEvent] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveAlarmEvent", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the AlarmEvent object from the database
        /// </summary>
        /// <param name="toBeDeleted">AlarmEvent to delete</param>
        public void DeleteAlarmEventDW(OpAlarmEvent toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteAlarmEventDW(toBeDeleted);
                if (AlarmEventCacheEnabledDW)
                {
                    AlarmEventDictDW.Remove(toBeDeleted.IdAlarmEvent);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelAlarmEvent", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the AlarmEvent object cache
        /// </summary>
        public void ClearAlarmEventCacheDW()
        {
            AlarmEventDictDW.Clear();
            AlarmEventIsFullCacheDW = false;
        }
        #endregion

        #region AlarmHistory
        /// <summary>
        /// Indicates whether the AlarmHistoryDict was filled trough GetAllAlarmHistory or GetAllAlarmHistoryFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool AlarmHistoryIsFullCacheDW { get; protected set; }

        /// <summary>
        /// AlarmHistory objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpAlarmHistory> AlarmHistoryDictDW = new Dictionary<long, OpAlarmHistory>();

        /// <summary>
        /// Indicates whether the AlarmHistory is cached.
        /// </summary>
        public bool AlarmHistoryCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all AlarmHistory objects
        /// </summary>
        public virtual List<OpAlarmHistory> AlarmHistoryDW
        {
            get { return GetAllAlarmHistoryDW(); }
        }

        /// <summary>
        /// Gets all AlarmHistory objects
        /// </summary>
        public virtual List<OpAlarmHistory> GetAllAlarmHistoryDW()
        {
            try
            {
                if (AlarmHistoryCacheEnabledDW) //cache enabled
                {
                    if (!AlarmHistoryIsFullCacheDW)
                    {
                        List<OpAlarmHistory> objectList = OpAlarmHistory.ConvertList(dbConnectionDw.GetAlarmHistory(), this);
                        if (objectList != null)
                        {
                            AlarmHistoryDictDW = objectList.ToDictionary(o => o.IdAlarmHistory);
                            AlarmHistoryIsFullCacheDW = true;
                        }
                    }
                    return new List<OpAlarmHistory>(AlarmHistoryDictDW.Values);
                }
                else //cache disabled
                {
                    return OpAlarmHistory.ConvertList(dbConnectionDw.GetAlarmHistory(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmHistory", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Id">AlarmHistory Id</param>
        /// <returns>AlarmHistory object</returns>
        public virtual OpAlarmHistory GetAlarmHistoryDW(long Id)
        {
            return GetAlarmHistoryDW(Id, false);
        }

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Ids">AlarmHistory Ids</param>
        /// <returns>AlarmHistory list</returns>
        public virtual List<OpAlarmHistory> GetAlarmHistoryDW(long[] Ids)
        {
            return GetAlarmHistoryDW(Ids, false);
        }

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Id">AlarmHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmHistory object</returns>
        public virtual OpAlarmHistory GetAlarmHistoryDW(long Id, bool queryDatabase)
        {
            return GetAlarmHistoryDW(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets AlarmHistory by id
        /// </summary>
        /// <param name="Ids">AlarmHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmHistory list</returns>
        public virtual List<OpAlarmHistory> GetAlarmHistoryDW(long[] Ids, bool queryDatabase)
        {
            List<OpAlarmHistory> returnList = new List<OpAlarmHistory>();
            if (Ids != null && Ids.Length > 0)
            {
                if (AlarmHistoryCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpAlarmHistory AlarmHistoryFoundInCache;
                        if (AlarmHistoryDictDW.TryGetValue(Ids[i], out AlarmHistoryFoundInCache)) //element founded in cache
                            returnList.Add(AlarmHistoryFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpAlarmHistory item in OpAlarmHistory.ConvertList(dbConnectionDw.GetAlarmHistory(idsNotInCache.ToArray()), this))
                            {
                                AlarmHistoryDictDW[item.IdAlarmHistory] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmHistory", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpAlarmHistory.ConvertList(dbConnectionDw.GetAlarmHistory(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmHistory", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        /// <summary>
        /// Clears the AlarmHistory object cache
        /// </summary>
        public virtual void ClearAlarmHistoryCacheDW()
        {
            AlarmHistoryDictDW.Clear();
            AlarmHistoryIsFullCacheDW = false;
        }
        #endregion

        #region AlarmMessage
        /// <summary>
        /// Indicates whether the AlarmMessageDict was filled trough GetAllAlarmMessage or GetAllAlarmMessageFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool AlarmMessageIsFullCacheDW { get; protected set; }

        /// <summary>
        /// AlarmMessage objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpAlarmMessage> AlarmMessageDictDW = new Dictionary<long, OpAlarmMessage>();

        /// <summary>
        /// Indicates whether the AlarmMessage is cached.
        /// </summary>
        public bool AlarmMessageCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all AlarmMessage objects
        /// </summary>
        public virtual List<OpAlarmMessage> AlarmMessageDW
        {
            get { return GetAllAlarmMessageDW(); }
        }

        /// <summary>
        /// Gets all AlarmMessage objects
        /// </summary>
        public virtual List<OpAlarmMessage> GetAllAlarmMessageDW()
        {
            try
            {
                if (AlarmMessageCacheEnabledDW) //cache enabled
                {
                    if (!AlarmMessageIsFullCacheDW)
                    {
                        List<OpAlarmMessage> objectList = OpAlarmMessage.ConvertList(dbConnectionDw.GetAlarmMessage(), this);
                        if (objectList != null)
                        {
                            AlarmMessageDictDW = objectList.ToDictionary(o => o.IdAlarmEvent);
                            AlarmMessageIsFullCacheDW = true;
                        }
                    }
                    return new List<OpAlarmMessage>(AlarmMessageDictDW.Values);
                }
                else //cache disabled
                {
                    return OpAlarmMessage.ConvertList(dbConnectionDw.GetAlarmMessage(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmMessage", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Id">AlarmMessage Id</param>
        /// <returns>AlarmMessage object</returns>
        public virtual OpAlarmMessage GetAlarmMessageDW(long Id)
        {
            return GetAlarmMessageDW(Id, false);
        }

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Ids">AlarmMessage Ids</param>
        /// <returns>AlarmMessage list</returns>
        public virtual List<OpAlarmMessage> GetAlarmMessageDW(long[] Ids)
        {
            return GetAlarmMessageDW(Ids, false);
        }

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Id">AlarmMessage Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmMessage object</returns>
        public virtual OpAlarmMessage GetAlarmMessageDW(long Id, bool queryDatabase)
        {
            return GetAlarmMessageDW(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets AlarmMessage by id
        /// </summary>
        /// <param name="Ids">AlarmMessage Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>AlarmMessage list</returns>
        public virtual List<OpAlarmMessage> GetAlarmMessageDW(long[] Ids, bool queryDatabase)
        {
            List<OpAlarmMessage> returnList = new List<OpAlarmMessage>();
            if (Ids != null && Ids.Length > 0)
            {
                if (AlarmMessageCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpAlarmMessage AlarmMessageFoundInCache;
                        if (AlarmMessageDictDW.TryGetValue(Ids[i], out AlarmMessageFoundInCache)) //element founded in cache
                            returnList.Add(AlarmMessageFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpAlarmMessage item in OpAlarmMessage.ConvertList(dbConnectionDw.GetAlarmMessage(idsNotInCache.ToArray()), this))
                            {
                                AlarmMessageDictDW[item.IdAlarmEvent] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmMessage", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpAlarmMessage.ConvertList(dbConnectionDw.GetAlarmMessage(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmMessage", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Clears the AlarmMessage object cache
        /// </summary>
        public virtual void ClearAlarmMessageCacheDW()
        {
            AlarmMessageDictDW.Clear();
            AlarmMessageIsFullCacheDW = false;
        }
        #endregion

        #region AuditDW
        /// <summary>
        /// Indicates whether the AuditDict was filled trough GetAllAudit or GetAllAuditFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool AuditIsFullCacheDW { get; protected set; }

        /// <summary>
        /// Audit objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, CORE.OpAudit> AuditDictDW = new Dictionary<long, CORE.OpAudit>();

        /// <summary>
        /// Indicates whether the Audit is cached.
        /// </summary>
        public bool AuditCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all Audit objects
        /// </summary>
        public virtual List<CORE.OpAudit> AuditDW
        {
            get { return GetAllAuditDW(); }
        }

        /// <summary>
        /// Gets all Audit objects
        /// </summary>
        public virtual List<CORE.OpAudit> GetAllAuditDW()
        {
            try
            {
                if (AuditCacheEnabledDW) //cache enabled
                {
                    if (!AuditIsFullCacheDW)
                    {
                        List<CORE.OpAudit> objectList = CORE.OpAudit.ConvertList(dbConnectionDw.GetAudit(), this);
                        if (objectList != null)
                        {
                            AuditDictDW = objectList.ToDictionary(o => o.IdAudit);
                            AuditIsFullCacheDW = true;
                        }
                    }
                    return new List<CORE.OpAudit>(AuditDictDW.Values);
                }
                else //cache disabled
                {
                    return CORE.OpAudit.ConvertList(dbConnectionDw.GetAudit(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAudit", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <returns>Audit object</returns>
        public virtual CORE.OpAudit GetAuditDW(long Id)
        {
            return GetAuditDW(Id, false);
        }

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <returns>Audit list</returns>
        public virtual List<CORE.OpAudit> GetAuditDW(long[] Ids)
        {
            return GetAuditDW(Ids, false);
        }

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Id">Audit Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit object</returns>
        public virtual CORE.OpAudit GetAuditDW(long Id, bool queryDatabase)
        {
            return GetAuditDW(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Audit by id
        /// </summary>
        /// <param name="Ids">Audit Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Audit list</returns>
        public virtual List<CORE.OpAudit> GetAuditDW(long[] Ids, bool queryDatabase)
        {
            List<CORE.OpAudit> returnList = new List<CORE.OpAudit>();
            if (Ids != null && Ids.Length > 0)
            {
                if (AuditCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        CORE.OpAudit AuditFoundInCache;
                        if (AuditDictDW.TryGetValue(Ids[i], out AuditFoundInCache)) //element founded in cache
                            returnList.Add(AuditFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (CORE.OpAudit item in CORE.OpAudit.ConvertList(dbConnectionDw.GetAudit(idsNotInCache.ToArray()), this))
                            {
                                AuditDictDW[item.IdAudit] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAudit", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return CORE.OpAudit.ConvertList(dbConnectionDw.GetAudit(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAudit", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Audit object to the database
        /// </summary>
        /// <param name="toBeSaved">Audit to save</param>
        /// <returns>Audit Id</returns>
        public virtual long SaveAuditDW(CORE.OpAudit toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveAudit(toBeSaved);
                if (AuditCacheEnabledDW)
                {
                    AuditDictDW[toBeSaved.IdAudit] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveAudit", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Audit object from the database
        /// </summary>
        /// <param name="toBeDeleted">Audit to delete</param>
        public virtual void DeleteAuditDW(CORE.OpAudit toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteAudit(toBeDeleted);
                if (AuditCacheEnabledDW)
                {
                    AuditDictDW.Remove(toBeDeleted.IdAudit);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelAudit", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the Audit object cache
        /// </summary>
        public virtual void ClearAuditCacheDW()
        {
            AuditDictDW.Clear();
            AuditIsFullCacheDW = false;
        }
        #endregion

        #region ConsumerTransaction
        /// <summary>
        /// Indicates whether the ConsumerTransactionDict was filled trough GetAllConsumerTransaction or GetAllConsumerTransactionFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ConsumerTransactionIsFullCache { get; protected set; }

        /// <summary>
        /// ConsumerTransaction objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpConsumerTransaction> ConsumerTransactionDict = new Dictionary<int, OpConsumerTransaction>();

        /// <summary>
        /// Indicates whether the ConsumerTransaction is cached.
        /// </summary>
        public bool ConsumerTransactionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerTransaction objects
        /// </summary>
        public virtual List<OpConsumerTransaction> ConsumerTransaction
        {
            get { return GetAllConsumerTransaction(); }
        }

        /// <summary>
        /// Gets all ConsumerTransaction objects
        /// </summary>
        public virtual List<OpConsumerTransaction> GetAllConsumerTransaction()
        {
            try
            {
                if (ConsumerTransactionCacheEnabled) //cache enabled
                {
                    if (!ConsumerTransactionIsFullCache)
                    {
                        List<OpConsumerTransaction> objectList = OpConsumerTransaction.ConvertList(dbConnectionDw.GetConsumerTransaction(), this);
                        if (objectList != null)
                        {
                            ConsumerTransactionDict = objectList.ToDictionary(o => o.IdConsumerTransaction);
                            ConsumerTransactionIsFullCache = true;
                        }
                    }
                    return new List<OpConsumerTransaction>(ConsumerTransactionDict.Values);
                }
                else //cache disabled
                {
                    return OpConsumerTransaction.ConvertList(dbConnectionDw.GetConsumerTransaction(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransaction", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Id">ConsumerTransaction Id</param>
        /// <returns>ConsumerTransaction object</returns>
        public virtual OpConsumerTransaction GetConsumerTransaction(int Id)
        {
            return GetConsumerTransaction(Id, false);
        }

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Ids">ConsumerTransaction Ids</param>
        /// <returns>ConsumerTransaction list</returns>
        public virtual List<OpConsumerTransaction> GetConsumerTransaction(int[] Ids)
        {
            return GetConsumerTransaction(Ids, false);
        }

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Id">ConsumerTransaction Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransaction object</returns>
        public virtual OpConsumerTransaction GetConsumerTransaction(int Id, bool queryDatabase)
        {
            return GetConsumerTransaction(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Ids">ConsumerTransaction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransaction list</returns>
        public virtual List<OpConsumerTransaction> GetConsumerTransaction(int[] Ids, bool queryDatabase)
        {
            List<OpConsumerTransaction> returnList = new List<OpConsumerTransaction>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConsumerTransactionCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpConsumerTransaction ConsumerTransactionFoundInCache;
                        if (ConsumerTransactionDict.TryGetValue(Ids[i], out ConsumerTransactionFoundInCache)) //element founded in cache
                            returnList.Add(ConsumerTransactionFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpConsumerTransaction item in OpConsumerTransaction.ConvertList(dbConnectionDw.GetConsumerTransaction(idsNotInCache.ToArray()), this))
                            {
                                ConsumerTransactionDict[item.IdConsumerTransaction] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransaction", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpConsumerTransaction.ConvertList(dbConnectionDw.GetConsumerTransaction(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransaction", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ConsumerTransaction object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTransaction to save</param>
        /// <returns>ConsumerTransaction Id</returns>
        public virtual int SaveConsumerTransaction(OpConsumerTransaction toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveConsumerTransaction(toBeSaved);
                if (ConsumerTransactionCacheEnabled)
                {
                    ConsumerTransactionDict[toBeSaved.IdConsumerTransaction] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveConsumerTransaction", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ConsumerTransaction object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTransaction to delete</param>
        public virtual void DeleteConsumerTransaction(OpConsumerTransaction toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteConsumerTransaction(toBeDeleted);
                if (ConsumerTransactionCacheEnabled)
                {
                    ConsumerTransactionDict.Remove(toBeDeleted.IdConsumerTransaction);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelConsumerTransaction", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ConsumerTransaction object cache
        /// </summary>
        public virtual void ClearConsumerTransactionCache()
        {
            ConsumerTransactionDict.Clear();
            ConsumerTransactionIsFullCache = false;
        }
        #endregion

        #region ConsumerTransactionData
        /// <summary>
        /// Indicates whether the ConsumerTransactionDataDict was filled trough GetAllConsumerTransactionData or GetAllConsumerTransactionDataFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ConsumerTransactionDataIsFullCache { get; protected set; }

        /// <summary>
        /// ConsumerTransactionData objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpConsumerTransactionData> ConsumerTransactionDataDict = new Dictionary<long, OpConsumerTransactionData>();

        /// <summary>
        /// Indicates whether the ConsumerTransactionData is cached.
        /// </summary>
        public bool ConsumerTransactionDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ConsumerTransactionData objects
        /// </summary>
        public virtual List<OpConsumerTransactionData> ConsumerTransactionData
        {
            get { return GetAllConsumerTransactionData(); }
        }

        /// <summary>
        /// Gets all ConsumerTransactionData objects
        /// </summary>
        public virtual List<OpConsumerTransactionData> GetAllConsumerTransactionData()
        {
            try
            {
                if (ConsumerTransactionDataCacheEnabled) //cache enabled
                {
                    if (!ConsumerTransactionDataIsFullCache)
                    {
                        List<OpConsumerTransactionData> objectList = OpConsumerTransactionData.ConvertList(dbConnectionDw.GetConsumerTransactionData(), this);
                        if (objectList != null)
                        {
                            ConsumerTransactionDataDict = objectList.ToDictionary(o => o.IdConsumerTransactionData);
                            ConsumerTransactionDataIsFullCache = true;
                        }
                    }
                    return new List<OpConsumerTransactionData>(ConsumerTransactionDataDict.Values);
                }
                else //cache disabled
                {
                    return OpConsumerTransactionData.ConvertList(dbConnectionDw.GetConsumerTransactionData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionData", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionData Id</param>
        /// <returns>ConsumerTransactionData object</returns>
        public virtual OpConsumerTransactionData GetConsumerTransactionData(long Id)
        {
            return GetConsumerTransactionData(Id, false);
        }

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionData Ids</param>
        /// <returns>ConsumerTransactionData list</returns>
        public virtual List<OpConsumerTransactionData> GetConsumerTransactionData(long[] Ids)
        {
            return GetConsumerTransactionData(Ids, false);
        }

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Id">ConsumerTransactionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionData object</returns>
        public virtual OpConsumerTransactionData GetConsumerTransactionData(long Id, bool queryDatabase)
        {
            return GetConsumerTransactionData(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ConsumerTransactionData by id
        /// </summary>
        /// <param name="Ids">ConsumerTransactionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransactionData list</returns>
        public virtual List<OpConsumerTransactionData> GetConsumerTransactionData(long[] Ids, bool queryDatabase)
        {
            List<OpConsumerTransactionData> returnList = new List<OpConsumerTransactionData>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConsumerTransactionDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpConsumerTransactionData ConsumerTransactionDataFoundInCache;
                        if (ConsumerTransactionDataDict.TryGetValue(Ids[i], out ConsumerTransactionDataFoundInCache)) //element founded in cache
                            returnList.Add(ConsumerTransactionDataFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpConsumerTransactionData item in OpConsumerTransactionData.ConvertList(dbConnectionDw.GetConsumerTransactionData(idsNotInCache.ToArray()), this))
                            {
                                ConsumerTransactionDataDict[item.IdConsumerTransactionData] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionData", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpConsumerTransactionData.ConvertList(dbConnectionDw.GetConsumerTransactionData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionData", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ConsumerTransactionData object to the database
        /// </summary>
        /// <param name="toBeSaved">ConsumerTransactionData to save</param>
        /// <returns>ConsumerTransactionData Id</returns>
        public virtual long SaveConsumerTransactionData(OpConsumerTransactionData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveConsumerTransactionData(toBeSaved, toBeSaved.DataType);
                if (ConsumerTransactionDataCacheEnabled)
                {
                    ConsumerTransactionDataDict[toBeSaved.IdConsumerTransactionData] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveConsumerTransactionData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ConsumerTransactionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ConsumerTransactionData to delete</param>
        public virtual void DeleteConsumerTransactionData(OpConsumerTransactionData toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteConsumerTransactionData(toBeDeleted);
                if (ConsumerTransactionDataCacheEnabled)
                {
                    ConsumerTransactionDataDict.Remove(toBeDeleted.IdConsumerTransactionData);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelConsumerTransactionData", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ConsumerTransactionData object cache
        /// </summary>
        public virtual void ClearConsumerTransactionDataCache()
        {
            ConsumerTransactionDataDict.Clear();
            ConsumerTransactionDataIsFullCache = false;
        }
        #endregion

        #region DataTemporal
        /// <summary>
        /// Indicates whether the DataTemporalDict was filled trough GetAllDataTemporal or GetAllDataTemporalFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataTemporalIsFullCache { get; protected set; }

        /// <summary>
        /// DataTemporal objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDataTemporal> DataTemporalDict = new Dictionary<long, OpDataTemporal>();

        /// <summary>
        /// Indicates whether the DataTemporal is cached.
        /// </summary>
        public bool DataTemporalCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataTemporal objects
        /// </summary>
        public virtual List<OpDataTemporal> DataTemporal
        {
            get { return GetAllDataTemporal(); }
        }

        /// <summary>
        /// Gets all DataTemporal objects
        /// </summary>
        public virtual List<OpDataTemporal> GetAllDataTemporal()
        {
            try
            {
                if (DataTemporalCacheEnabled) //cache enabled
                {
                    if (!DataTemporalIsFullCache)
                    {
                        List<OpDataTemporal> objectList = OpDataTemporal.ConvertList(dbConnectionDw.GetDataTemporal(), this);
                        if (objectList != null)
                        {
                            DataTemporalDict = objectList.ToDictionary(o => o.IdDataTemporal);
                            DataTemporalIsFullCache = true;
                        }
                    }
                    return new List<OpDataTemporal>(DataTemporalDict.Values);
                }
                else //cache disabled
                {
                    return OpDataTemporal.ConvertList(dbConnectionDw.GetDataTemporal(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTemporal", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Id">DataTemporal Id</param>
        /// <returns>DataTemporal object</returns>
        public virtual OpDataTemporal GetDataTemporal(long Id)
        {
            return GetDataTemporal(Id, false);
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Ids">DataTemporal Ids</param>
        /// <returns>DataTemporal list</returns>
        public virtual List<OpDataTemporal> GetDataTemporal(long[] Ids)
        {
            return GetDataTemporal(Ids, false);
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Id">DataTemporal Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTemporal object</returns>
        public virtual OpDataTemporal GetDataTemporal(long Id, bool queryDatabase)
        {
            return GetDataTemporal(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Ids">DataTemporal Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTemporal list</returns>
        public virtual List<OpDataTemporal> GetDataTemporal(long[] Ids, bool queryDatabase)
        {
            List<OpDataTemporal> returnList = new List<OpDataTemporal>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataTemporalCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDataTemporal DataTemporalFoundInCache;
                        if (DataTemporalDict.TryGetValue(Ids[i], out DataTemporalFoundInCache)) //element founded in cache
                            returnList.Add(DataTemporalFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDataTemporal item in OpDataTemporal.ConvertList(dbConnectionDw.GetDataTemporal(idsNotInCache.ToArray()), this))
                            {
                                DataTemporalDict[item.IdDataTemporal] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTemporal", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataTemporal.ConvertList(dbConnectionDw.GetDataTemporal(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTemporal", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Gets DataTemporal by id
        /// </summary>
        /// <param name="Ids">DataTemporal Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTemporal list</returns>
        public virtual List<OpDataTemporal> GetGLTCDataForAllMeters()
        {
            List<OpDataTemporal> returnList = new List<OpDataTemporal>();

            try
            {
                return OpDataTemporal.ConvertList(dbConnectionDw.GetGLTCDataForAllMeters(), this);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_GetGLTCDataForAllMeters", ex);
                throw;
            }

            return returnList;
        }

        /// <summary>
        /// Saves the DataTemporal object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTemporal to save</param>
        /// <returns>DataTemporal Id</returns>
        public virtual long SaveDataTemporal(OpDataTemporal toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveDataTemporal(toBeSaved, toBeSaved.DataType);
                if (DataTemporalCacheEnabled)
                {
                    DataTemporalDict[toBeSaved.IdDataTemporal] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDataTemporal", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DataTemporal object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTemporal to delete</param>
        public virtual void DeleteDataTemporal(OpDataTemporal toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDataTemporal(toBeDeleted);
                if (DataTemporalCacheEnabled)
                {
                    DataTemporalDict.Remove(toBeDeleted.IdDataTemporal);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDataTemporal", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DataTemporal object cache
        /// </summary>
        public virtual void ClearDataTemporalCache()
        {
            DataTemporalDict.Clear();
            DataTemporalIsFullCache = false;
        }
        #endregion

        #region DeliveryAdvice
        /// <summary>
        /// Indicates whether the DeliveryAdviceDict was filled trough GetAllDeliveryAdvice or GetAllDeliveryAdviceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DeliveryAdviceIsFullCache { get; protected set; }

        /// <summary>
        /// DeliveryAdvice objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpDeliveryAdvice> DeliveryAdviceDict = new Dictionary<int, OpDeliveryAdvice>();

        /// <summary>
        /// Indicates whether the DeliveryAdvice is cached.
        /// </summary>
        public bool DeliveryAdviceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryAdvice objects
        /// </summary>
        public virtual List<OpDeliveryAdvice> DeliveryAdvice
        {
            get { return GetAllDeliveryAdvice(); }
        }

        /// <summary>
        /// Gets all DeliveryAdvice objects
        /// </summary>
        public virtual List<OpDeliveryAdvice> GetAllDeliveryAdvice()
        {
            try
            {
                if (DeliveryAdviceCacheEnabled) //cache enabled
                {
                    if (!DeliveryAdviceIsFullCache)
                    {
                        List<OpDeliveryAdvice> objectList = OpDeliveryAdvice.ConvertList(dbConnectionDw.GetDeliveryAdvice(), this);
                        if (objectList != null)
                        {
                            DeliveryAdviceDict = objectList.ToDictionary(o => o.IdDeliveryAdvice);
                            DeliveryAdviceIsFullCache = true;
                        }
                    }
                    return new List<OpDeliveryAdvice>(DeliveryAdviceDict.Values);
                }
                else //cache disabled
                {
                    return OpDeliveryAdvice.ConvertList(dbConnectionDw.GetDeliveryAdvice(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryAdvice", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Id">DeliveryAdvice Id</param>
        /// <returns>DeliveryAdvice object</returns>
        public virtual OpDeliveryAdvice GetDeliveryAdvice(int Id)
        {
            return GetDeliveryAdvice(Id, false);
        }

        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Ids">DeliveryAdvice Ids</param>
        /// <returns>DeliveryAdvice list</returns>
        public virtual List<OpDeliveryAdvice> GetDeliveryAdvice(int[] Ids)
        {
            return GetDeliveryAdvice(Ids, false);
        }

        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Id">DeliveryAdvice Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryAdvice object</returns>
        public virtual OpDeliveryAdvice GetDeliveryAdvice(int Id, bool queryDatabase)
        {
            return GetDeliveryAdvice(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeliveryAdvice by id
        /// </summary>
        /// <param name="Ids">DeliveryAdvice Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryAdvice list</returns>
        public virtual List<OpDeliveryAdvice> GetDeliveryAdvice(int[] Ids, bool queryDatabase)
        {
            List<OpDeliveryAdvice> returnList = new List<OpDeliveryAdvice>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeliveryAdviceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDeliveryAdvice DeliveryAdviceFoundInCache;
                        if (DeliveryAdviceDict.TryGetValue(Ids[i], out DeliveryAdviceFoundInCache)) //element founded in cache
                            returnList.Add(DeliveryAdviceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDeliveryAdvice item in OpDeliveryAdvice.ConvertList(dbConnectionDw.GetDeliveryAdvice(idsNotInCache.ToArray()), this))
                            {
                                DeliveryAdviceDict[item.IdDeliveryAdvice] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryAdvice", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeliveryAdvice.ConvertList(dbConnectionDw.GetDeliveryAdvice(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryAdvice", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeliveryAdvice object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryAdvice to save</param>
        /// <returns>DeliveryAdvice Id</returns>
        public virtual int SaveDeliveryAdvice(OpDeliveryAdvice toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveDeliveryAdvice(toBeSaved);
                if (DeliveryAdviceCacheEnabled)
                {
                    DeliveryAdviceDict[toBeSaved.IdDeliveryAdvice] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeliveryAdvice", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DeliveryAdvice object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryAdvice to delete</param>
        public virtual void DeleteDeliveryAdvice(OpDeliveryAdvice toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDeliveryAdvice(toBeDeleted);
                if (DeliveryAdviceCacheEnabled)
                {
                    DeliveryAdviceDict.Remove(toBeDeleted.IdDeliveryAdvice);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeliveryAdvice", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DeliveryAdvice object cache
        /// </summary>
        public virtual void ClearDeliveryAdviceCache()
        {
            DeliveryAdviceDict.Clear();
            DeliveryAdviceIsFullCache = false;
        }
        #endregion

        #region DeliveryBatch
        /// <summary>
        /// Indicates whether the DeliveryBatchDict was filled trough GetAllDeliveryBatch or GetAllDeliveryBatchFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DeliveryBatchIsFullCache { get; protected set; }

        /// <summary>
        /// DeliveryBatch objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpDeliveryBatch> DeliveryBatchDict = new Dictionary<int, OpDeliveryBatch>();

        /// <summary>
        /// Indicates whether the DeliveryBatch is cached.
        /// </summary>
        public bool DeliveryBatchCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryBatch objects
        /// </summary>
        public virtual List<OpDeliveryBatch> DeliveryBatch
        {
            get { return GetAllDeliveryBatch(); }
        }

        /// <summary>
        /// Gets all DeliveryBatch objects
        /// </summary>
        public virtual List<OpDeliveryBatch> GetAllDeliveryBatch()
        {
            try
            {
                if (DeliveryBatchCacheEnabled) //cache enabled
                {
                    if (!DeliveryBatchIsFullCache)
                    {
                        List<OpDeliveryBatch> objectList = OpDeliveryBatch.ConvertList(dbConnectionDw.GetDeliveryBatch(), this);
                        if (objectList != null)
                        {
                            DeliveryBatchDict = objectList.ToDictionary(o => o.IdDeliveryBatch);
                            DeliveryBatchIsFullCache = true;
                        }
                    }
                    return new List<OpDeliveryBatch>(DeliveryBatchDict.Values);
                }
                else //cache disabled
                {
                    return OpDeliveryBatch.ConvertList(dbConnectionDw.GetDeliveryBatch(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatch", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Id">DeliveryBatch Id</param>
        /// <returns>DeliveryBatch object</returns>
        public virtual OpDeliveryBatch GetDeliveryBatch(int Id)
        {
            return GetDeliveryBatch(Id, false);
        }

        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Ids">DeliveryBatch Ids</param>
        /// <returns>DeliveryBatch list</returns>
        public virtual List<OpDeliveryBatch> GetDeliveryBatch(int[] Ids)
        {
            return GetDeliveryBatch(Ids, false);
        }

        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Id">DeliveryBatch Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatch object</returns>
        public virtual OpDeliveryBatch GetDeliveryBatch(int Id, bool queryDatabase)
        {
            return GetDeliveryBatch(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeliveryBatch by id
        /// </summary>
        /// <param name="Ids">DeliveryBatch Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatch list</returns>
        public virtual List<OpDeliveryBatch> GetDeliveryBatch(int[] Ids, bool queryDatabase)
        {
            List<OpDeliveryBatch> returnList = new List<OpDeliveryBatch>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeliveryBatchCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDeliveryBatch DeliveryBatchFoundInCache;
                        if (DeliveryBatchDict.TryGetValue(Ids[i], out DeliveryBatchFoundInCache)) //element founded in cache
                            returnList.Add(DeliveryBatchFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDeliveryBatch item in OpDeliveryBatch.ConvertList(dbConnectionDw.GetDeliveryBatch(idsNotInCache.ToArray()), this))
                            {
                                DeliveryBatchDict[item.IdDeliveryBatch] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatch", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeliveryBatch.ConvertList(dbConnectionDw.GetDeliveryBatch(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatch", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeliveryBatch object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryBatch to save</param>
        /// <returns>DeliveryBatch Id</returns>
        public virtual int SaveDeliveryBatch(OpDeliveryBatch toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveDeliveryBatch(toBeSaved);
                if (DeliveryBatchCacheEnabled)
                {
                    DeliveryBatchDict[toBeSaved.IdDeliveryBatch] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeliveryBatch", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DeliveryBatch object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryBatch to delete</param>
        public virtual void DeleteDeliveryBatch(OpDeliveryBatch toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDeliveryBatch(toBeDeleted);
                if (DeliveryBatchCacheEnabled)
                {
                    DeliveryBatchDict.Remove(toBeDeleted.IdDeliveryBatch);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeliveryBatch", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DeliveryBatch object cache
        /// </summary>
        public virtual void ClearDeliveryBatchCache()
        {
            DeliveryBatchDict.Clear();
            DeliveryBatchIsFullCache = false;
        }
        #endregion

        #region DeliveryBatchStatus
        /// <summary>
        /// Indicates whether the DeliveryBatchStatusDict was filled trough GetAllDeliveryBatchStatus or GetAllDeliveryBatchStatusFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DeliveryBatchStatusIsFullCache { get; protected set; }

        /// <summary>
        /// DeliveryBatchStatus objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpDeliveryBatchStatus> DeliveryBatchStatusDict = new Dictionary<int, OpDeliveryBatchStatus>();

        /// <summary>
        /// Indicates whether the DeliveryBatchStatus is cached.
        /// </summary>
        public bool DeliveryBatchStatusCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryBatchStatus objects
        /// </summary>
        public virtual List<OpDeliveryBatchStatus> DeliveryBatchStatus
        {
            get { return GetAllDeliveryBatchStatus(); }
        }

        /// <summary>
        /// Gets all DeliveryBatchStatus objects
        /// </summary>
        public virtual List<OpDeliveryBatchStatus> GetAllDeliveryBatchStatus()
        {
            try
            {
                if (DeliveryBatchStatusCacheEnabled) //cache enabled
                {
                    if (!DeliveryBatchStatusIsFullCache)
                    {
                        List<OpDeliveryBatchStatus> objectList = OpDeliveryBatchStatus.ConvertList(dbConnectionDw.GetDeliveryBatchStatus(), this);
                        if (objectList != null)
                        {
                            DeliveryBatchStatusDict = objectList.ToDictionary(o => o.IdDeliveryBatchStatus);
                            DeliveryBatchStatusIsFullCache = true;
                        }
                    }
                    return new List<OpDeliveryBatchStatus>(DeliveryBatchStatusDict.Values);
                }
                else //cache disabled
                {
                    return OpDeliveryBatchStatus.ConvertList(dbConnectionDw.GetDeliveryBatchStatus(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatchStatus", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Id">DeliveryBatchStatus Id</param>
        /// <returns>DeliveryBatchStatus object</returns>
        public virtual OpDeliveryBatchStatus GetDeliveryBatchStatus(int Id)
        {
            return GetDeliveryBatchStatus(Id, false);
        }

        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Ids">DeliveryBatchStatus Ids</param>
        /// <returns>DeliveryBatchStatus list</returns>
        public virtual List<OpDeliveryBatchStatus> GetDeliveryBatchStatus(int[] Ids)
        {
            return GetDeliveryBatchStatus(Ids, false);
        }

        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Id">DeliveryBatchStatus Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatchStatus object</returns>
        public virtual OpDeliveryBatchStatus GetDeliveryBatchStatus(int Id, bool queryDatabase)
        {
            return GetDeliveryBatchStatus(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeliveryBatchStatus by id
        /// </summary>
        /// <param name="Ids">DeliveryBatchStatus Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryBatchStatus list</returns>
        public virtual List<OpDeliveryBatchStatus> GetDeliveryBatchStatus(int[] Ids, bool queryDatabase)
        {
            List<OpDeliveryBatchStatus> returnList = new List<OpDeliveryBatchStatus>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeliveryBatchStatusCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDeliveryBatchStatus DeliveryBatchStatusFoundInCache;
                        if (DeliveryBatchStatusDict.TryGetValue(Ids[i], out DeliveryBatchStatusFoundInCache)) //element founded in cache
                            returnList.Add(DeliveryBatchStatusFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDeliveryBatchStatus item in OpDeliveryBatchStatus.ConvertList(dbConnectionDw.GetDeliveryBatchStatus(idsNotInCache.ToArray()), this))
                            {
                                DeliveryBatchStatusDict[item.IdDeliveryBatchStatus] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatchStatus", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeliveryBatchStatus.ConvertList(dbConnectionDw.GetDeliveryBatchStatus(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryBatchStatus", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeliveryBatchStatus object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryBatchStatus to save</param>
        /// <returns>DeliveryBatchStatus Id</returns>
        public virtual int SaveDeliveryBatchStatus(OpDeliveryBatchStatus toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveDeliveryBatchStatus(toBeSaved);
                if (DeliveryBatchStatusCacheEnabled)
                {
                    DeliveryBatchStatusDict[toBeSaved.IdDeliveryBatchStatus] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeliveryBatchStatus", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DeliveryBatchStatus object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryBatchStatus to delete</param>
        public virtual void DeleteDeliveryBatchStatus(OpDeliveryBatchStatus toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDeliveryBatchStatus(toBeDeleted);
                if (DeliveryBatchStatusCacheEnabled)
                {
                    DeliveryBatchStatusDict.Remove(toBeDeleted.IdDeliveryBatchStatus);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeliveryBatchStatus", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DeliveryBatchStatus object cache
        /// </summary>
        public virtual void ClearDeliveryBatchStatusCache()
        {
            DeliveryBatchStatusDict.Clear();
            DeliveryBatchStatusIsFullCache = false;
        }
        #endregion

        #region DeliveryOrder
        /// <summary>
        /// Indicates whether the DeliveryOrderDict was filled trough GetAllDeliveryOrder or GetAllDeliveryOrderFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DeliveryOrderIsFullCache { get; protected set; }

        /// <summary>
        /// DeliveryOrder objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpDeliveryOrder> DeliveryOrderDict = new Dictionary<int, OpDeliveryOrder>();

        /// <summary>
        /// Indicates whether the DeliveryOrder is cached.
        /// </summary>
        public bool DeliveryOrderCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryOrder objects
        /// </summary>
        public virtual List<OpDeliveryOrder> DeliveryOrder
        {
            get { return GetAllDeliveryOrder(); }
        }

        /// <summary>
        /// Gets all DeliveryOrder objects
        /// </summary>
        public virtual List<OpDeliveryOrder> GetAllDeliveryOrder()
        {
            try
            {
                if (DeliveryOrderCacheEnabled) //cache enabled
                {
                    if (!DeliveryOrderIsFullCache)
                    {
                        List<OpDeliveryOrder> objectList = OpDeliveryOrder.ConvertList(dbConnectionDw.GetDeliveryOrder(), this);
                        if (objectList != null)
                        {
                            DeliveryOrderDict = objectList.ToDictionary(o => o.IdDeliveryOrder);
                            DeliveryOrderIsFullCache = true;
                        }
                    }
                    return new List<OpDeliveryOrder>(DeliveryOrderDict.Values);
                }
                else //cache disabled
                {
                    return OpDeliveryOrder.ConvertList(dbConnectionDw.GetDeliveryOrder(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrder", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Id">DeliveryOrder Id</param>
        /// <returns>DeliveryOrder object</returns>
        public virtual OpDeliveryOrder GetDeliveryOrder(int Id)
        {
            return GetDeliveryOrder(Id, false);
        }

        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Ids">DeliveryOrder Ids</param>
        /// <returns>DeliveryOrder list</returns>
        public virtual List<OpDeliveryOrder> GetDeliveryOrder(int[] Ids)
        {
            return GetDeliveryOrder(Ids, false);
        }

        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Id">DeliveryOrder Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrder object</returns>
        public virtual OpDeliveryOrder GetDeliveryOrder(int Id, bool queryDatabase)
        {
            return GetDeliveryOrder(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeliveryOrder by id
        /// </summary>
        /// <param name="Ids">DeliveryOrder Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrder list</returns>
        public virtual List<OpDeliveryOrder> GetDeliveryOrder(int[] Ids, bool queryDatabase)
        {
            List<OpDeliveryOrder> returnList = new List<OpDeliveryOrder>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeliveryOrderCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDeliveryOrder DeliveryOrderFoundInCache;
                        if (DeliveryOrderDict.TryGetValue(Ids[i], out DeliveryOrderFoundInCache)) //element founded in cache
                            returnList.Add(DeliveryOrderFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDeliveryOrder item in OpDeliveryOrder.ConvertList(dbConnectionDw.GetDeliveryOrder(idsNotInCache.ToArray()), this))
                            {
                                DeliveryOrderDict[item.IdDeliveryOrder] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrder", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeliveryOrder.ConvertList(dbConnectionDw.GetDeliveryOrder(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrder", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeliveryOrder object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryOrder to save</param>
        /// <returns>DeliveryOrder Id</returns>
        public virtual int SaveDeliveryOrder(OpDeliveryOrder toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveDeliveryOrder(toBeSaved);
                if (DeliveryOrderCacheEnabled)
                {
                    DeliveryOrderDict[toBeSaved.IdDeliveryOrder] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeliveryOrder", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DeliveryOrder object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryOrder to delete</param>
        public virtual void DeleteDeliveryOrder(OpDeliveryOrder toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDeliveryOrder(toBeDeleted);
                if (DeliveryOrderCacheEnabled)
                {
                    DeliveryOrderDict.Remove(toBeDeleted.IdDeliveryOrder);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeliveryOrder", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DeliveryOrder object cache
        /// </summary>
        public virtual void ClearDeliveryOrderCache()
        {
            DeliveryOrderDict.Clear();
            DeliveryOrderIsFullCache = false;
        }
        #endregion

        #region DeliveryOrderHistory
        /// <summary>
        /// Indicates whether the DeliveryOrderHistoryDict was filled trough GetAllDeliveryOrderHistory or GetAllDeliveryOrderHistoryFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DeliveryOrderHistoryIsFullCache { get; protected set; }

        /// <summary>
        /// DeliveryOrderHistory objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDeliveryOrderHistory> DeliveryOrderHistoryDict = new Dictionary<long, OpDeliveryOrderHistory>();

        /// <summary>
        /// Indicates whether the DeliveryOrderHistory is cached.
        /// </summary>
        public bool DeliveryOrderHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeliveryOrderHistory objects
        /// </summary>
        public virtual List<OpDeliveryOrderHistory> DeliveryOrderHistory
        {
            get { return GetAllDeliveryOrderHistory(); }
        }

        /// <summary>
        /// Gets all DeliveryOrderHistory objects
        /// </summary>
        public virtual List<OpDeliveryOrderHistory> GetAllDeliveryOrderHistory()
        {
            try
            {
                if (DeliveryOrderHistoryCacheEnabled) //cache enabled
                {
                    if (!DeliveryOrderHistoryIsFullCache)
                    {
                        List<OpDeliveryOrderHistory> objectList = OpDeliveryOrderHistory.ConvertList(dbConnectionDw.GetDeliveryOrderHistory(), this);
                        if (objectList != null)
                        {
                            DeliveryOrderHistoryDict = objectList.ToDictionary(o => o.IdDeliveryOrderHistory);
                            DeliveryOrderHistoryIsFullCache = true;
                        }
                    }
                    return new List<OpDeliveryOrderHistory>(DeliveryOrderHistoryDict.Values);
                }
                else //cache disabled
                {
                    return OpDeliveryOrderHistory.ConvertList(dbConnectionDw.GetDeliveryOrderHistory(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrderHistory", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Id">DeliveryOrderHistory Id</param>
        /// <returns>DeliveryOrderHistory object</returns>
        public virtual OpDeliveryOrderHistory GetDeliveryOrderHistory(long Id)
        {
            return GetDeliveryOrderHistory(Id, false);
        }

        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Ids">DeliveryOrderHistory Ids</param>
        /// <returns>DeliveryOrderHistory list</returns>
        public virtual List<OpDeliveryOrderHistory> GetDeliveryOrderHistory(long[] Ids)
        {
            return GetDeliveryOrderHistory(Ids, false);
        }

        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Id">DeliveryOrderHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrderHistory object</returns>
        public virtual OpDeliveryOrderHistory GetDeliveryOrderHistory(long Id, bool queryDatabase)
        {
            return GetDeliveryOrderHistory(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeliveryOrderHistory by id
        /// </summary>
        /// <param name="Ids">DeliveryOrderHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeliveryOrderHistory list</returns>
        public virtual List<OpDeliveryOrderHistory> GetDeliveryOrderHistory(long[] Ids, bool queryDatabase)
        {
            List<OpDeliveryOrderHistory> returnList = new List<OpDeliveryOrderHistory>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeliveryOrderHistoryCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDeliveryOrderHistory DeliveryOrderHistoryFoundInCache;
                        if (DeliveryOrderHistoryDict.TryGetValue(Ids[i], out DeliveryOrderHistoryFoundInCache)) //element founded in cache
                            returnList.Add(DeliveryOrderHistoryFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDeliveryOrderHistory item in OpDeliveryOrderHistory.ConvertList(dbConnectionDw.GetDeliveryOrderHistory(idsNotInCache.ToArray()), this))
                            {
                                DeliveryOrderHistoryDict[item.IdDeliveryOrderHistory] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrderHistory", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeliveryOrderHistory.ConvertList(dbConnectionDw.GetDeliveryOrderHistory(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryOrderHistory", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeliveryOrderHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">DeliveryOrderHistory to save</param>
        /// <returns>DeliveryOrderHistory Id</returns>
        public virtual long SaveDeliveryOrderHistory(OpDeliveryOrderHistory toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveDeliveryOrderHistory(toBeSaved);
                if (DeliveryOrderHistoryCacheEnabled)
                {
                    DeliveryOrderHistoryDict[toBeSaved.IdDeliveryOrderHistory] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeliveryOrderHistory", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DeliveryOrderHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeliveryOrderHistory to delete</param>
        public virtual void DeleteDeliveryOrderHistory(OpDeliveryOrderHistory toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDeliveryOrderHistory(toBeDeleted);
                if (DeliveryOrderHistoryCacheEnabled)
                {
                    DeliveryOrderHistoryDict.Remove(toBeDeleted.IdDeliveryOrderHistory);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeliveryOrderHistory", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DeliveryOrderHistory object cache
        /// </summary>
        public virtual void ClearDeliveryOrderHistoryCache()
        {
            DeliveryOrderHistoryDict.Clear();
            DeliveryOrderHistoryIsFullCache = false;
        }
        #endregion

        #region DeviceConnection
        /// <summary>
        /// Indicates whether the DeviceConnectionDict was filled trough GetAllDeviceConnection or GetAllDeviceConnectionFilter with mergeIntoCache = true
        /// </summary>
        public bool DeviceConnectionIsFullCache { get; private set; }

        /// <summary>
        /// DeviceConnection objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDeviceConnection> DeviceConnectionDict = new Dictionary<long, OpDeviceConnection>();

        /// <summary>
        /// Indicates whether the DeviceConnection is cached.
        /// </summary>
        public bool DeviceConnectionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceConnection objects
        /// </summary>
        public List<OpDeviceConnection> DeviceConnection
        {
            get { return GetAllDeviceConnection(); }
        }

        /// <summary>
        /// Gets all DeviceConnection objects
        /// </summary>
        public List<OpDeviceConnection> GetAllDeviceConnection()
        {
            try
            {
                if (DeviceConnectionCacheEnabled) //cache enabled
                {
                    if (!DeviceConnectionIsFullCache)
                    {
                        List<OpDeviceConnection> objectList = OpDeviceConnection.ConvertList(dbConnectionDw.GetDeviceConnection(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            DeviceConnectionDict = objectList.ToDictionary(o => o.IdDeviceConnection);
                            DeviceConnectionIsFullCache = true;
                        }
                    }
                    return new List<OpDeviceConnection>(DeviceConnectionDict.Values);
                }
                else //cache disabled
                {
                    return OpDeviceConnection.ConvertList(dbConnectionDw.GetDeviceConnection(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceConnection", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Id">DeviceConnection Id</param>
        /// <returns>DeviceConnection object</returns>
        public OpDeviceConnection GetDeviceConnection(long Id)
        {
            return GetDeviceConnection(Id, false);
        }

        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Ids">DeviceConnection Ids</param>
        /// <returns>DeviceConnection list</returns>
        public List<OpDeviceConnection> GetDeviceConnection(long[] Ids)
        {
            return GetDeviceConnection(Ids, false);
        }

        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Id">DeviceConnection Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceConnection object</returns>
        public OpDeviceConnection GetDeviceConnection(long Id, bool queryDatabase)
        {
            return GetDeviceConnection(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeviceConnection by id
        /// </summary>
        /// <param name="Ids">DeviceConnection Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceConnection list</returns>
        public List<OpDeviceConnection> GetDeviceConnection(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDeviceConnection> returnList = new List<OpDeviceConnection>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeviceConnectionCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDeviceConnection DeviceConnectionFoundInCache;
                        if (DeviceConnectionDict.TryGetValue(Ids[i], out DeviceConnectionFoundInCache)) //element founded in cache
                            returnList.Add(DeviceConnectionFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDeviceConnection item in OpDeviceConnection.ConvertList(dbConnectionDw.GetDeviceConnection(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                DeviceConnectionDict[item.IdDeviceConnection] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceConnection", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeviceConnection.ConvertList(dbConnectionDw.GetDeviceConnection(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceConnection", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeviceConnection object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceConnection to save</param>
        /// <returns>DeviceConnection Id</returns>
        public long SaveDeviceConnection(OpDeviceConnection toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveDeviceConnection(toBeSaved);
                if (DeviceConnectionCacheEnabled)
                {
                    DeviceConnectionDict[toBeSaved.IdDeviceConnection] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeviceConnection", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DeviceConnection object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceConnection to delete</param>
        public void DeleteDeviceConnection(OpDeviceConnection toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDeviceConnection(toBeDeleted);
                if (DeviceConnectionCacheEnabled)
                {
                    DeviceConnectionDict.Remove(toBeDeleted.IdDeviceConnection);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeviceConnection", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DeviceConnection object cache
        /// </summary>
        public void ClearDeviceConnectionCache()
        {
            DeviceConnectionDict.Clear();
            DeviceConnectionIsFullCache = false;
        }
        #endregion

        #region DeviceSchedule
        /// <summary>
        /// Indicates whether the DeviceScheduleDict was filled trough GetAllDeviceSchedule or GetAllDeviceScheduleFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DeviceScheduleIsFullCache { get; protected set; }

        /// <summary>
        /// DeviceSchedule objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpDeviceSchedule> DeviceScheduleDict = new Dictionary<int, OpDeviceSchedule>();

        /// <summary>
        /// Indicates whether the DeviceSchedule is cached.
        /// </summary>
        public bool DeviceScheduleCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceSchedule objects
        /// </summary>
        public virtual List<OpDeviceSchedule> DeviceSchedule
        {
            get { return GetAllDeviceSchedule(); }
        }

        /// <summary>
        /// Gets all DeviceSchedule objects
        /// </summary>
        public virtual List<OpDeviceSchedule> GetAllDeviceSchedule()
        {
            try
            {
                if (DeviceScheduleCacheEnabled) //cache enabled
                {
                    if (!DeviceScheduleIsFullCache)
                    {
                        List<OpDeviceSchedule> objectList = OpDeviceSchedule.ConvertList(dbConnectionDw.GetDeviceSchedule(), this);
                        if (objectList != null)
                        {
                            DeviceScheduleDict = objectList.ToDictionary(o => o.IdDeviceSchedule);
                            DeviceScheduleIsFullCache = true;
                        }
                    }
                    return new List<OpDeviceSchedule>(DeviceScheduleDict.Values);
                }
                else //cache disabled
                {
                    return OpDeviceSchedule.ConvertList(dbConnectionDw.GetDeviceSchedule(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceSchedule", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Id">DeviceSchedule Id</param>
        /// <returns>DeviceSchedule object</returns>
        public virtual OpDeviceSchedule GetDeviceSchedule(int Id)
        {
            return GetDeviceSchedule(Id, false);
        }

        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Ids">DeviceSchedule Ids</param>
        /// <returns>DeviceSchedule list</returns>
        public virtual List<OpDeviceSchedule> GetDeviceSchedule(int[] Ids)
        {
            return GetDeviceSchedule(Ids, false);
        }

        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Id">DeviceSchedule Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceSchedule object</returns>
        public virtual OpDeviceSchedule GetDeviceSchedule(int Id, bool queryDatabase)
        {
            return GetDeviceSchedule(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeviceSchedule by id
        /// </summary>
        /// <param name="Ids">DeviceSchedule Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceSchedule list</returns>
        public virtual List<OpDeviceSchedule> GetDeviceSchedule(int[] Ids, bool queryDatabase)
        {
            List<OpDeviceSchedule> returnList = new List<OpDeviceSchedule>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeviceScheduleCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDeviceSchedule DeviceScheduleFoundInCache;
                        if (DeviceScheduleDict.TryGetValue(Ids[i], out DeviceScheduleFoundInCache)) //element founded in cache
                            returnList.Add(DeviceScheduleFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDeviceSchedule item in OpDeviceSchedule.ConvertList(dbConnectionDw.GetDeviceSchedule(idsNotInCache.ToArray()), this))
                            {
                                DeviceScheduleDict[item.IdDeviceSchedule] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceSchedule", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeviceSchedule.ConvertList(dbConnectionDw.GetDeviceSchedule(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceSchedule", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeviceSchedule object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceSchedule to save</param>
        /// <returns>DeviceSchedule Id</returns>
        public virtual int SaveDeviceSchedule(OpDeviceSchedule toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveDeviceSchedule(toBeSaved);
                if (DeviceScheduleCacheEnabled)
                {
                    DeviceScheduleDict[toBeSaved.IdDeviceSchedule] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeviceSchedule", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DeviceSchedule object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceSchedule to delete</param>
        public virtual void DeleteDeviceSchedule(OpDeviceSchedule toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDeviceSchedule(toBeDeleted);
                if (DeviceScheduleCacheEnabled)
                {
                    DeviceScheduleDict.Remove(toBeDeleted.IdDeviceSchedule);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeviceSchedule", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DeviceSchedule object cache
        /// </summary>
        public virtual void ClearDeviceScheduleCache()
        {
            DeviceScheduleDict.Clear();
            DeviceScheduleIsFullCache = false;
        }
        #endregion

        #region DistributorPerformance
        /// <summary>
        /// Indicates whether the DistributorPerformanceDict was filled trough GetAllDistributorPerformance or GetAllDistributorPerformanceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DistributorPerformanceIsFullCache { get; protected set; }

        /// <summary>
        /// DistributorPerformance objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDistributorPerformance> DistributorPerformanceDict = new Dictionary<long, OpDistributorPerformance>();

        /// <summary>
        /// Indicates whether the DistributorPerformance is cached.
        /// </summary>
        public bool DistributorPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DistributorPerformance objects
        /// </summary>
        public virtual List<OpDistributorPerformance> DistributorPerformance
        {
            get { return GetAllDistributorPerformance(); }
        }

        /// <summary>
        /// Gets all DistributorPerformance objects
        /// </summary>
        public virtual List<OpDistributorPerformance> GetAllDistributorPerformance()
        {
            try
            {
                if (DistributorPerformanceCacheEnabled) //cache enabled
                {
                    if (!DistributorPerformanceIsFullCache)
                    {
                        List<OpDistributorPerformance> objectList = OpDistributorPerformance.ConvertList(dbConnectionDw.GetDistributorPerformance(), this);
                        if (objectList != null)
                        {
                            DistributorPerformanceDict = objectList.ToDictionary(o => o.IdDistributorPerformance);
                            DistributorPerformanceIsFullCache = true;
                        }
                    }
                    return new List<OpDistributorPerformance>(DistributorPerformanceDict.Values);
                }
                else //cache disabled
                {
                    return OpDistributorPerformance.ConvertList(dbConnectionDw.GetDistributorPerformance(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorPerformance", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Id">DistributorPerformance Id</param>
        /// <returns>DistributorPerformance object</returns>
        public virtual OpDistributorPerformance GetDistributorPerformance(long Id)
        {
            return GetDistributorPerformance(Id, false);
        }

        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Ids">DistributorPerformance Ids</param>
        /// <returns>DistributorPerformance list</returns>
        public virtual List<OpDistributorPerformance> GetDistributorPerformance(long[] Ids)
        {
            return GetDistributorPerformance(Ids, false);
        }

        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Id">DistributorPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorPerformance object</returns>
        public virtual OpDistributorPerformance GetDistributorPerformance(long Id, bool queryDatabase)
        {
            return GetDistributorPerformance(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DistributorPerformance by id
        /// </summary>
        /// <param name="Ids">DistributorPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DistributorPerformance list</returns>
        public virtual List<OpDistributorPerformance> GetDistributorPerformance(long[] Ids, bool queryDatabase)
        {
            List<OpDistributorPerformance> returnList = new List<OpDistributorPerformance>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DistributorPerformanceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDistributorPerformance DistributorPerformanceFoundInCache;
                        if (DistributorPerformanceDict.TryGetValue(Ids[i], out DistributorPerformanceFoundInCache)) //element founded in cache
                            returnList.Add(DistributorPerformanceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDistributorPerformance item in OpDistributorPerformance.ConvertList(dbConnectionDw.GetDistributorPerformance(idsNotInCache.ToArray()), this))
                            {
                                DistributorPerformanceDict[item.IdDistributorPerformance] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorPerformance", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDistributorPerformance.ConvertList(dbConnectionDw.GetDistributorPerformance(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorPerformance", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DistributorPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">DistributorPerformance to save</param>
        /// <returns>DistributorPerformance Id</returns>
        public virtual long SaveDistributorPerformance(OpDistributorPerformance toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveDistributorPerformance(toBeSaved, toBeSaved.DataType);
                if (DistributorPerformanceCacheEnabled)
                {
                    DistributorPerformanceDict[toBeSaved.IdDistributorPerformance] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDistributorPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DistributorPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">DistributorPerformance to delete</param>
        public virtual void DeleteDistributorPerformance(OpDistributorPerformance toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDistributorPerformance(toBeDeleted);
                if (DistributorPerformanceCacheEnabled)
                {
                    DistributorPerformanceDict.Remove(toBeDeleted.IdDistributorPerformance);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDistributorPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DistributorPerformance object cache
        /// </summary>
        public virtual void ClearDistributorPerformanceCache()
        {
            DistributorPerformanceDict.Clear();
            DistributorPerformanceIsFullCache = false;
        }
        #endregion

        #region EtlDW
        ///// <summary>
        ///// Indicates whether the EtlDict was filled trough GetAllEtl or GetAllEtlFilter with mergeIntoCache = true
        ///// </summary>
        //public bool EtlIsFullCacheDW { get; protected set; }

        ///// <summary>
        ///// Etl objects dictionary (Cache)
        ///// </summary>
        //public Dictionary<int, OpEtl> EtlDictDW = new Dictionary<int, OpEtl>();

        ///// <summary>
        ///// Indicates whether the Etl is cached.
        ///// </summary>
        //public bool EtlCacheEnabledDW { get; set; }

        ///// <summary>
        ///// Gets all Etl objects
        ///// </summary>
        //public List<OpEtl> EtlDW
        //{
        //    get { return GetAllEtlDW(); }
        //}

        ///// <summary>
        ///// Gets all Etl objects
        ///// </summary>
        //public List<OpEtl> GetAllEtlDW()
        //{
        //    try
        //    {
        //        if (EtlCacheEnabledDW) //cache enabled
        //        {
        //            if (!EtlIsFullCacheDW)
        //            {
        //                List<OpEtl> objectList = OpEtl.ConvertList(dbConnectionDw.GetEtlDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
        //                if (objectList != null)
        //                {
        //                    EtlDictDW = objectList.ToDictionary(o => o.IdParam);
        //                    EtlIsFullCacheDW = true;
        //                }
        //            }
        //            return new List<OpEtl>(EtlDictDW.Values);
        //        }
        //        else //cache disabled
        //        {
        //            return OpEtl.ConvertList(dbConnectionDw.GetEtlDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtl", ex);
        //        throw;
        //    }
        //}


        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Id">Etl Id</param>
        ///// <returns>Etl object</returns>
        //public OpEtl GetEtlDW(int Id)
        //{
        //    return GetEtlDW(Id, false);
        //}

        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Ids">Etl Ids</param>
        ///// <returns>Etl list</returns>
        //public List<OpEtl> GetEtlDW(int[] Ids)
        //{
        //    return GetEtlDW(Ids, false);
        //}

        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Id">Etl Id</param>
        ///// <param name="queryDatabase">If True, gets object from database</param>
        ///// <returns>Etl object</returns>
        //public OpEtl GetEtlDW(int Id, bool queryDatabase)
        //{
        //    return GetEtlDW(new int[] { Id }, queryDatabase).FirstOrDefault();
        //}

        ///// <summary>
        ///// Gets Etl by id
        ///// </summary>
        ///// <param name="Ids">Etl Ids</param>
        ///// <param name="queryDatabase">If True, gets object from database</param>
        ///// <returns>Etl list</returns>
        //public List<OpEtl> GetEtlDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        //{
        //    List<OpEtl> returnList = new List<OpEtl>();
        //    if (Ids != null && Ids.Length > 0)
        //    {
        //        if (EtlCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
        //        {
        //            List<int> idsNotInCacheDW = new List<int>();
        //            for (int i = 0; i < Ids.Length; i++)
        //            {
        //                OpEtl EtlFoundInCacheDW;
        //                if (EtlDictDW.TryGetValue(Ids[i], out EtlFoundInCacheDW)) //element founded in cache
        //                    returnList.Add(EtlFoundInCacheDW);
        //                else //element not found in cache, query database
        //                    idsNotInCacheDW.Add(Ids[i]);
        //            }
        //            if (idsNotInCacheDW.Count > 0)
        //            {
        //                try
        //                {
        //                    foreach (OpEtl item in OpEtl.ConvertList(dbConnectionDw.GetEtlDW(idsNotInCacheDW.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
        //                    {
        //                        EtlDictDW[item.IdParam] = item;
        //                        returnList.Add(item);
        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtl", ex);
        //                    throw;
        //                }
        //            }
        //        }
        //        else //cache disabled or user force to query database 
        //        {
        //            try
        //            {
        //                return OpEtl.ConvertList(dbConnectionDw.GetEtlDW(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
        //            }
        //            catch (Exception ex)
        //            {
        //                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtl", ex);
        //                throw;
        //            }
        //        }
        //    }
        //    return returnList;
        //}

        ///// <summary>
        ///// Saves the Etl object to the database
        ///// </summary>
        ///// <param name="toBeSaved">Etl to save</param>
        ///// <returns>Etl Id</returns>
        //public int SaveEtlDW(OpEtl toBeSaved)
        //{
        //    try
        //    {
        //        int ret = dbConnectionDw.SaveEtlDW(toBeSaved);
        //        if (EtlCacheEnabledDW)
        //        {
        //            EtlDictDW[toBeSaved.IdParam] = toBeSaved; //add or update element
        //        }
        //        return ret;
        //    }
        //    catch (Exception ex)
        //    {
        //        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveEtl", ex);
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// Deletes the Etl object from the database
        ///// </summary>
        ///// <param name="toBeDeleted">Etl to delete</param>
        //public void DeleteEtlDW(OpEtl toBeDeleted)
        //{
        //    try
        //    {
        //        dbConnectionDw.DeleteEtlDW(toBeDeleted);
        //        if (EtlCacheEnabledDW)
        //        {
        //            EtlDictDW.Remove(toBeDeleted.IdParam);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelEtl", ex);
        //        throw;
        //    }
        //}

        ///// <summary>
        ///// Clears the Etl object cache
        ///// </summary>
        //public void ClearEtlCacheDW()
        //{
        //    EtlDictDW.Clear();
        //    EtlIsFullCacheDW = false;
        //}

        /// <summary>
        /// Indicates whether the EtlDict was filled trough GetAllEtl or GetAllEtlFilter with mergeIntoCache = true
        /// </summary>
        public bool EtlIsFullCacheDW { get; protected set; }

        /// <summary>
        /// Etl objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, CORE.OpEtl> EtlDictDW = new Dictionary<int, CORE.OpEtl>();

        /// <summary>
        /// Indicates whether the Etl is cached.
        /// </summary>
        public bool EtlCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all Etl objects
        /// </summary>
        public List<CORE.OpEtl> EtlDW
        {
            get { return GetAllEtlDW(); }
        }

        /// <summary>
        /// Gets all Etl objects
        /// </summary>
        public List<CORE.OpEtl> GetAllEtlDW()
        {
            try
            {
                if (EtlCacheEnabledDW) //cache enabled
                {
                    if (!EtlIsFullCacheDW)
                    {
                        List<CORE.OpEtl> objectList = CORE.OpEtl.ConvertList(dbConnectionDw.GetEtlDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, true, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            EtlDictDW = objectList.ToDictionary(o => o.IdParam);
                            EtlIsFullCacheDW = true;
                        }
                    }
                    return new List<CORE.OpEtl>(EtlDictDW.Values);
                }
                else //cache disabled
                {
                    return CORE.OpEtl.ConvertList(dbConnectionDw.GetEtlDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, true, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtl", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Id">Etl Id</param>
        /// <returns>Etl object</returns>
        public CORE.OpEtl GetEtlDW(int Id)
        {
            return GetEtlDW(Id, false);
        }

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Ids">Etl Ids</param>
        /// <returns>Etl list</returns>
        public List<CORE.OpEtl> GetEtlDW(int[] Ids)
        {
            return GetEtlDW(Ids, false);
        }

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Id">Etl Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Etl object</returns>
        public CORE.OpEtl GetEtlDW(int Id, bool queryDatabase)
        {
            return GetEtlDW(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Etl by id
        /// </summary>
        /// <param name="Ids">Etl Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Etl list</returns>
        public List<CORE.OpEtl> GetEtlDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<CORE.OpEtl> returnList = new List<CORE.OpEtl>();
            if (Ids != null && Ids.Length > 0)
            {
                if (EtlCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCacheDW = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        CORE.OpEtl EtlFoundInCacheDW;
                        if (EtlDictDW.TryGetValue(Ids[i], out EtlFoundInCacheDW)) //element founded in cache
                            returnList.Add(EtlFoundInCacheDW);
                        else //element not found in cache, query database
                            idsNotInCacheDW.Add(Ids[i]);
                    }
                    if (idsNotInCacheDW.Count > 0)
                    {
                        try
                        {
                            foreach (CORE.OpEtl item in CORE.OpEtl.ConvertList(dbConnectionDw.GetEtlDW(idsNotInCacheDW.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                EtlDictDW[item.IdParam] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtl", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return CORE.OpEtl.ConvertList(dbConnectionDw.GetEtlDW(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtl", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Etl object to the database
        /// </summary>
        /// <param name="toBeSaved">Etl to save</param>
        /// <returns>Etl Id</returns>
        public int SaveEtlDW(CORE.OpEtl toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveEtlDW(toBeSaved);
                if (EtlCacheEnabledDW)
                {
                    EtlDictDW[toBeSaved.IdParam] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveEtl", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Etl object from the database
        /// </summary>
        /// <param name="toBeDeleted">Etl to delete</param>
        public void DeleteEtlDW(CORE.OpEtl toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteEtlDW(toBeDeleted);
                if (EtlCacheEnabledDW)
                {
                    EtlDictDW.Remove(toBeDeleted.IdParam);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelEtl", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the Etl object cache
        /// </summary>
        public void ClearEtlCacheDW()
        {
            EtlDictDW.Clear();
            EtlIsFullCacheDW = false;
        }
        #endregion

        #region EtlPerformance
        /// <summary>
        /// Indicates whether the EtlPerformanceDict was filled trough GetAllEtlPerformance or GetAllEtlPerformanceFilter with mergeIntoCache = true
        /// </summary>
        public bool EtlPerformanceIsFullCache { get; protected set; }

        /// <summary>
        /// EtlPerformance objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpEtlPerformance> EtlPerformanceDict = new Dictionary<long, OpEtlPerformance>();

        /// <summary>
        /// Indicates whether the EtlPerformance is cached.
        /// </summary>
        public bool EtlPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all EtlPerformance objects
        /// </summary>
        public List<OpEtlPerformance> EtlPerformance
        {
            get { return GetAllEtlPerformance(); }
        }

        /// <summary>
        /// Gets all EtlPerformance objects
        /// </summary>
        public List<OpEtlPerformance> GetAllEtlPerformance()
        {
            try
            {
                if (EtlPerformanceCacheEnabled) //cache enabled
                {
                    if (!EtlPerformanceIsFullCache)
                    {
                        List<OpEtlPerformance> objectList = OpEtlPerformance.ConvertList(dbConnectionDw.GetEtlPerformance(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            EtlPerformanceDict = objectList.ToDictionary(o => o.IdEtlPerformance);
                            EtlPerformanceIsFullCache = true;
                        }
                    }
                    return new List<OpEtlPerformance>(EtlPerformanceDict.Values);
                }
                else //cache disabled
                {
                    return OpEtlPerformance.ConvertList(dbConnectionDw.GetEtlPerformance(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtlPerformance", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Id">EtlPerformance Id</param>
        /// <returns>EtlPerformance object</returns>
        public OpEtlPerformance GetEtlPerformance(long Id)
        {
            return GetEtlPerformance(Id, false);
        }

        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Ids">EtlPerformance Ids</param>
        /// <returns>EtlPerformance list</returns>
        public List<OpEtlPerformance> GetEtlPerformance(long[] Ids)
        {
            return GetEtlPerformance(Ids, false);
        }

        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Id">EtlPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>EtlPerformance object</returns>
        public OpEtlPerformance GetEtlPerformance(long Id, bool queryDatabase)
        {
            return GetEtlPerformance(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets EtlPerformance by id
        /// </summary>
        /// <param name="Ids">EtlPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>EtlPerformance list</returns>
        public List<OpEtlPerformance> GetEtlPerformance(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpEtlPerformance> returnList = new List<OpEtlPerformance>();
            if (Ids != null && Ids.Length > 0)
            {
                if (EtlPerformanceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpEtlPerformance EtlPerformanceFoundInCache;
                        if (EtlPerformanceDict.TryGetValue(Ids[i], out EtlPerformanceFoundInCache)) //element founded in cache
                            returnList.Add(EtlPerformanceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpEtlPerformance item in OpEtlPerformance.ConvertList(dbConnectionDw.GetEtlPerformance(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                EtlPerformanceDict[item.IdEtlPerformance] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtlPerformance", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpEtlPerformance.ConvertList(dbConnectionDw.GetEtlPerformance(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtlPerformance", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the EtlPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">EtlPerformance to save</param>
        /// <returns>EtlPerformance Id</returns>
        public long SaveEtlPerformance(OpEtlPerformance toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveEtlPerformance(toBeSaved, toBeSaved.DataType);
                if (EtlPerformanceCacheEnabled)
                {
                    EtlPerformanceDict[toBeSaved.IdEtlPerformance] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveEtlPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the EtlPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">EtlPerformance to delete</param>
        public void DeleteEtlPerformance(OpEtlPerformance toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteEtlPerformance(toBeDeleted);
                if (EtlPerformanceCacheEnabled)
                {
                    EtlPerformanceDict.Remove(toBeDeleted.IdEtlPerformance);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelEtlPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the EtlPerformance object cache
        /// </summary>
        public void ClearEtlPerformanceCache()
        {
            EtlPerformanceDict.Clear();
            EtlPerformanceIsFullCache = false;
        }
        #endregion

        #region ImrServerPerformance
        /// <summary>
        /// Indicates whether the ImrServerPerformanceDict was filled trough GetAllImrServerPerformance or GetAllImrServerPerformanceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ImrServerPerformanceIsFullCache { get; protected set; }

        /// <summary>
        /// ImrServerPerformance objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpImrServerPerformance> ImrServerPerformanceDict = new Dictionary<long, OpImrServerPerformance>();

        /// <summary>
        /// Indicates whether the ImrServerPerformance is cached.
        /// </summary>
        public bool ImrServerPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ImrServerPerformance objects
        /// </summary>
        public virtual List<OpImrServerPerformance> ImrServerPerformance
        {
            get { return GetAllImrServerPerformance(); }
        }

        /// <summary>
        /// Gets all ImrServerPerformance objects
        /// </summary>
        public virtual List<OpImrServerPerformance> GetAllImrServerPerformance()
        {
            try
            {
                if (ImrServerPerformanceCacheEnabled) //cache enabled
                {
                    if (!ImrServerPerformanceIsFullCache)
                    {
                        List<OpImrServerPerformance> objectList = OpImrServerPerformance.ConvertList(dbConnectionDw.GetImrServerPerformance(), this);
                        if (objectList != null)
                        {
                            ImrServerPerformanceDict = objectList.ToDictionary(o => o.IdImrServerPerformance);
                            ImrServerPerformanceIsFullCache = true;
                        }
                    }
                    return new List<OpImrServerPerformance>(ImrServerPerformanceDict.Values);
                }
                else //cache disabled
                {
                    return OpImrServerPerformance.ConvertList(dbConnectionDw.GetImrServerPerformance(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetImrServerPerformance", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Id">ImrServerPerformance Id</param>
        /// <returns>ImrServerPerformance object</returns>
        public virtual OpImrServerPerformance GetImrServerPerformance(long Id)
        {
            return GetImrServerPerformance(Id, false);
        }

        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Ids">ImrServerPerformance Ids</param>
        /// <returns>ImrServerPerformance list</returns>
        public virtual List<OpImrServerPerformance> GetImrServerPerformance(long[] Ids)
        {
            return GetImrServerPerformance(Ids, false);
        }

        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Id">ImrServerPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServerPerformance object</returns>
        public virtual OpImrServerPerformance GetImrServerPerformance(long Id, bool queryDatabase)
        {
            return GetImrServerPerformance(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ImrServerPerformance by id
        /// </summary>
        /// <param name="Ids">ImrServerPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ImrServerPerformance list</returns>
        public virtual List<OpImrServerPerformance> GetImrServerPerformance(long[] Ids, bool queryDatabase)
        {
            List<OpImrServerPerformance> returnList = new List<OpImrServerPerformance>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ImrServerPerformanceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpImrServerPerformance ImrServerPerformanceFoundInCache;
                        if (ImrServerPerformanceDict.TryGetValue(Ids[i], out ImrServerPerformanceFoundInCache)) //element founded in cache
                            returnList.Add(ImrServerPerformanceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpImrServerPerformance item in OpImrServerPerformance.ConvertList(dbConnectionDw.GetImrServerPerformance(idsNotInCache.ToArray()), this))
                            {
                                ImrServerPerformanceDict[item.IdImrServerPerformance] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetImrServerPerformance", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpImrServerPerformance.ConvertList(dbConnectionDw.GetImrServerPerformance(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetImrServerPerformance", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ImrServerPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">ImrServerPerformance to save</param>
        /// <returns>ImrServerPerformance Id</returns>
        public virtual long SaveImrServerPerformance(OpImrServerPerformance toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveImrServerPerformance(toBeSaved, toBeSaved.DataType);
                if (ImrServerPerformanceCacheEnabled)
                {
                    ImrServerPerformanceDict[toBeSaved.IdImrServerPerformance] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveImrServerPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ImrServerPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">ImrServerPerformance to delete</param>
        public virtual void DeleteImrServerPerformance(OpImrServerPerformance toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteImrServerPerformance(toBeDeleted);
                if (ImrServerPerformanceCacheEnabled)
                {
                    ImrServerPerformanceDict.Remove(toBeDeleted.IdImrServerPerformance);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelImrServerPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ImrServerPerformance object cache
        /// </summary>
        public virtual void ClearImrServerPerformanceCache()
        {
            ImrServerPerformanceDict.Clear();
            ImrServerPerformanceIsFullCache = false;
        }
        #endregion

        #region LocationKpi
        /// <summary>
        /// Indicates whether the LocationKpiDict was filled trough GetAllLocationKpi or GetAllLocationKpiFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool LocationKpiIsFullCache { get; protected set; }

        /// <summary>
        /// LocationKpi objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpLocationKpi> LocationKpiDict = new Dictionary<long, OpLocationKpi>();

        /// <summary>
        /// Indicates whether the LocationKpi is cached.
        /// </summary>
        public bool LocationKpiCacheEnabled { get; set; }

        /// <summary>
        /// Gets all LocationKpi objects
        /// </summary>
        public virtual List<OpLocationKpi> LocationKpi
        {
            get { return GetAllLocationKpi(); }
        }

        /// <summary>
        /// Gets all LocationKpi objects
        /// </summary>
        public virtual List<OpLocationKpi> GetAllLocationKpi()
        {
            try
            {
                if (LocationKpiCacheEnabled) //cache enabled
                {
                    if (!LocationKpiIsFullCache)
                    {
                        List<OpLocationKpi> objectList = OpLocationKpi.ConvertList(dbConnectionDw.GetLocationKpi(), this);
                        if (objectList != null)
                        {
                            LocationKpiDict = objectList.ToDictionary(o => o.IdLocationKpi);
                            LocationKpiIsFullCache = true;
                        }
                    }
                    return new List<OpLocationKpi>(LocationKpiDict.Values);
                }
                else //cache disabled
                {
                    return OpLocationKpi.ConvertList(dbConnectionDw.GetLocationKpi(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuel", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Id">LocationKpi Id</param>
        /// <returns>LocationKpi object</returns>
        public virtual OpLocationKpi GetLocationKpi(long Id)
        {
            return GetLocationKpi(Id, false);
        }

        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Ids">LocationKpi Ids</param>
        /// <returns>Refuel list</returns>
        public virtual List<OpLocationKpi> GetLocationKpi(long[] Ids)
        {
            return GetLocationKpi(Ids, false);
        }

        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Id">LocationKpi Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationKpi object</returns>
        public virtual OpLocationKpi GetLocationKpi(long Id, bool queryDatabase)
        {
            return GetLocationKpi(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets LocationKpi by id
        /// </summary>
        /// <param name="Ids">LocationKpi Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationKpi list</returns>
        public virtual List<OpLocationKpi> GetLocationKpi(long[] Ids, bool queryDatabase)
        {
            List<OpLocationKpi> returnList = new List<OpLocationKpi>();
            if (Ids != null && Ids.Length > 0)
            {
                if (LocationKpiCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpLocationKpi LocationKpiFoundInCache;
                        if (LocationKpiDict.TryGetValue(Ids[i], out LocationKpiFoundInCache)) //element founded in cache
                            returnList.Add(LocationKpiFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpLocationKpi item in OpLocationKpi.ConvertList(dbConnectionDw.GetLocationKpi(idsNotInCache.ToArray()), this))
                            {
                                LocationKpiDict[item.IdLocationKpi] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationKpi", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpLocationKpi.ConvertList(dbConnectionDw.GetLocationKpi(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationKpi", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the LocationKpi object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationKpi to save</param>
        /// <returns>LocationKpi Id</returns>
        public virtual long SaveLocationKpi(OpLocationKpi toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveLocationKpi(toBeSaved);
                if (LocationKpiCacheEnabled)
                {
                    LocationKpiDict[toBeSaved.IdLocationKpi] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveLocationKpi", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the LocationKpi object from the database
        /// </summary>
        /// <param name="toBeDeleted">Refuel to delete</param>
        public virtual void DeleteLocationKpi(OpLocationKpi toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteLocationKpi(toBeDeleted);
                if (LocationKpiCacheEnabled)
                {
                    LocationKpiDict.Remove(toBeDeleted.IdLocationKpi);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelLocationKpi", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the LocationKpi object cache
        /// </summary>
        public virtual void ClearLocationKpi()
        {
            LocationKpiDict.Clear();
            LocationKpiIsFullCache = false;
        }
        #endregion

        #region LocationDistance
        /// <summary>
        /// Indicates whether the LocationDistanceDict was filled trough GetAllLocationDistance or GetAllLocationDistanceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool LocationDistanceIsFullCache { get; protected set; }

        /// <summary>
        /// LocationDistance objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpLocationDistance> LocationDistanceDict = new Dictionary<long, OpLocationDistance>();

        /// <summary>
        /// Indicates whether the LocationDistance is cached.
        /// </summary>
        public readonly bool LocationDistanceCacheEnabled = false; // zablokowany cache ze wzgledu na podwojny klucz na tabeli

        /// <summary>
        /// Gets all LocationDistance objects
        /// </summary>
        public virtual List<OpLocationDistance> LocationDistance
        {
            get { return GetAllLocationDistance(); }
        }

        /// <summary>
        /// Gets all LocationDistance objects
        /// </summary>
        public virtual List<OpLocationDistance> GetAllLocationDistance()
        {
            try
            {
                if (LocationDistanceCacheEnabled) //cache enabled
                {
                    if (!LocationDistanceIsFullCache)
                    {
                        List<OpLocationDistance> objectList = OpLocationDistance.ConvertList(dbConnectionDw.GetLocationDistance(), this);
                        if (objectList != null)
                        {
                            LocationDistanceDict = objectList.ToDictionary(o => o.IdLocationOrigin);
                            LocationDistanceIsFullCache = true;
                        }
                    }
                    return new List<OpLocationDistance>(LocationDistanceDict.Values);
                }
                else //cache disabled
                {
                    return OpLocationDistance.ConvertList(dbConnectionDw.GetLocationDistance(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationDistance", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Id">LocationDistance Id</param>
        /// <returns>LocationDistance object</returns>
        public virtual OpLocationDistance GetLocationDistance(long Id)
        {
            return GetLocationDistance(Id, false);
        }

        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Ids">LocationDistance Ids</param>
        /// <returns>LocationDistance list</returns>
        public virtual List<OpLocationDistance> GetLocationDistance(long[] Ids)
        {
            return GetLocationDistance(Ids, false);
        }

        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Id">LocationDistance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationDistance object</returns>
        public virtual OpLocationDistance GetLocationDistance(long Id, bool queryDatabase)
        {
            return GetLocationDistance(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets LocationDistance by id
        /// </summary>
        /// <param name="Ids">LocationDistance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>LocationDistance list</returns>
        public virtual List<OpLocationDistance> GetLocationDistance(long[] Ids, bool queryDatabase)
        {
            List<OpLocationDistance> returnList = new List<OpLocationDistance>();
            if (Ids != null && Ids.Length > 0)
            {
                if (LocationDistanceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpLocationDistance LocationDistanceFoundInCache;
                        if (LocationDistanceDict.TryGetValue(Ids[i], out LocationDistanceFoundInCache)) //element founded in cache
                            returnList.Add(LocationDistanceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpLocationDistance item in OpLocationDistance.ConvertList(dbConnectionDw.GetLocationDistance(idsNotInCache.ToArray()), this))
                            {
                                //LocationDistanceDict[item.IdLocationOrigingin] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationDistance", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpLocationDistance.ConvertList(dbConnectionDw.GetLocationDistance(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationDistance", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the LocationDistance object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationDistance to save</param>
        /// <returns>LocationDistance Id</returns>
        public virtual long SaveLocationDistance(OpLocationDistance toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveLocationDistance(toBeSaved);
                if (LocationDistanceCacheEnabled)
                {
                    LocationDistanceDict[toBeSaved.IdLocationOrigin] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveLocationDistance", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the LocationDistance object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationDistance to delete</param>
        public virtual void DeleteLocationDistance(OpLocationDistance toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteLocationDistance(toBeDeleted);
                if (LocationDistanceCacheEnabled)
                {
                    LocationDistanceDict.Remove(toBeDeleted.IdLocationOrigin);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelLocationDistance", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the LocationDistance object cache
        /// </summary>
        public virtual void ClearLocationDistanceCache()
        {
            LocationDistanceDict.Clear();
            LocationDistanceIsFullCache = false;
        }
        #endregion

        #region MappingDW
        /// <summary>
        /// Indicates whether the MappingDict was filled trough GetAllMapping or GetAllMappingFilter with mergeIntoCache = true
        /// </summary>
        public bool MappingIsFullCacheDW { get; protected set; }

        /// <summary>
        /// Mapping objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpMapping> MappingDictDW = new Dictionary<long, OpMapping>();

        /// <summary>
        /// Indicates whether the Mapping is cached.
        /// </summary>
        public bool MappingCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all Mapping objects
        /// </summary>
        public List<OpMapping> MappingDW
        {
            get { return GetAllMappingDW(); }
        }

        /// <summary>
        /// Gets all Mapping objects
        /// </summary>
        public List<OpMapping> GetAllMappingDW()
        {
            try
            {
                if (MappingCacheEnabledDW) //cache enabled
                {
                    if (!MappingIsFullCacheDW)
                    {
                        List<OpMapping> objectList = OpMapping.ConvertList(dbConnectionDw.GetMappingDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            MappingDictDW = objectList.ToDictionary(o => o.IdMapping);
                            MappingIsFullCacheDW = true;
                        }
                    }
                    return new List<OpMapping>(MappingDictDW.Values);
                }
                else //cache disabled
                {
                    return OpMapping.ConvertList(dbConnectionDw.GetMappingDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMapping", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Id">Mapping Id</param>
        /// <returns>Mapping object</returns>
        public OpMapping GetMappingDW(long Id)
        {
            return GetMappingDW(Id, false);
        }

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Ids">Mapping Ids</param>
        /// <returns>Mapping list</returns>
        public List<OpMapping> GetMappingDW(long[] Ids)
        {
            return GetMappingDW(Ids, false);
        }

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Id">Mapping Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Mapping object</returns>
        public OpMapping GetMappingDW(long Id, bool queryDatabase)
        {
            return GetMappingDW(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Mapping by id
        /// </summary>
        /// <param name="Ids">Mapping Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Mapping list</returns>
        public List<OpMapping> GetMappingDW(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMapping> returnList = new List<OpMapping>();
            if (Ids != null && Ids.Length > 0)
            {
                if (MappingCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpMapping MappingFoundInCacheDW;
                        if (MappingDictDW.TryGetValue(Ids[i], out MappingFoundInCacheDW)) //element founded in cache
                            returnList.Add(MappingFoundInCacheDW);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpMapping item in OpMapping.ConvertList(dbConnectionDw.GetMappingDW(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                MappingDictDW[item.IdMapping] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMapping", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpMapping.ConvertList(dbConnectionDw.GetMappingDW(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMapping", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Mapping object to the database
        /// </summary>
        /// <param name="toBeSaved">Mapping to save</param>
        /// <returns>Mapping Id</returns>
        public long SaveMappingDW(OpMapping toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveMappingDW(toBeSaved);
                if (MappingCacheEnabledDW)
                {
                    MappingDictDW[toBeSaved.IdMapping] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveMapping", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Mapping object from the database
        /// </summary>
        /// <param name="toBeDeleted">Mapping to delete</param>
        public void DeleteMappingDW(OpMapping toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteMappingDW(toBeDeleted);
                if (MappingCacheEnabledDW)
                {
                    MappingDictDW.Remove(toBeDeleted.IdMapping);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelMapping", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the Mapping object cache
        /// </summary>
        public void ClearMappingCacheDW()
        {
            MappingDictDW.Clear();
            MappingIsFullCacheDW = false;
        }
        #endregion

        #region ModuleHistory
        /// <summary>
        /// Indicates whether the ModuleHistoryDict was filled trough GetAllModuleHistory or GetAllModuleHistoryFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ModuleHistoryIsFullCache { get; protected set; }

        /// <summary>
        /// ModuleHistory objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpModuleHistory> ModuleHistoryDict = new Dictionary<int, OpModuleHistory>();

        /// <summary>
        /// Indicates whether the ModuleHistory is cached.
        /// </summary>
        public bool ModuleHistoryCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ModuleHistory objects
        /// </summary>
        public virtual List<OpModuleHistory> ModuleHistory
        {
            get { return GetAllModuleHistory(); }
        }

        /// <summary>
        /// Gets all ModuleHistory objects
        /// </summary>
        public virtual List<OpModuleHistory> GetAllModuleHistory()
        {
            try
            {
                if (ModuleHistoryCacheEnabled) //cache enabled
                {
                    if (!ModuleHistoryIsFullCache)
                    {
                        List<OpModuleHistory> objectList = OpModuleHistory.ConvertList(dbConnectionDw.GetModuleHistory(), this);
                        if (objectList != null)
                        {
                            ModuleHistoryDict = objectList.ToDictionary(o => o.IdModuleHistory);
                            ModuleHistoryIsFullCache = true;
                        }
                    }
                    return new List<OpModuleHistory>(ModuleHistoryDict.Values);
                }
                else //cache disabled
                {
                    return OpModuleHistory.ConvertList(dbConnectionDw.GetModuleHistory(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetModuleHistory", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Id">ModuleHistory Id</param>
        /// <returns>ModuleHistory object</returns>
        public virtual OpModuleHistory GetModuleHistory(int Id)
        {
            return GetModuleHistory(Id, false);
        }

        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Ids">ModuleHistory Ids</param>
        /// <returns>ModuleHistory list</returns>
        public virtual List<OpModuleHistory> GetModuleHistory(int[] Ids)
        {
            return GetModuleHistory(Ids, false);
        }

        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Id">ModuleHistory Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ModuleHistory object</returns>
        public virtual OpModuleHistory GetModuleHistory(int Id, bool queryDatabase)
        {
            return GetModuleHistory(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ModuleHistory by id
        /// </summary>
        /// <param name="Ids">ModuleHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ModuleHistory list</returns>
        public virtual List<OpModuleHistory> GetModuleHistory(int[] Ids, bool queryDatabase)
        {
            List<OpModuleHistory> returnList = new List<OpModuleHistory>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ModuleHistoryCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpModuleHistory ModuleHistoryFoundInCache;
                        if (ModuleHistoryDict.TryGetValue(Ids[i], out ModuleHistoryFoundInCache)) //element founded in cache
                            returnList.Add(ModuleHistoryFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpModuleHistory item in OpModuleHistory.ConvertList(dbConnectionDw.GetModuleHistory(idsNotInCache.ToArray()), this))
                            {
                                ModuleHistoryDict[item.IdModuleHistory] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetModuleHistory", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpModuleHistory.ConvertList(dbConnectionDw.GetModuleHistory(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetModuleHistory", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ModuleHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">ModuleHistory to save</param>
        /// <returns>ModuleHistory Id</returns>
        public virtual int SaveModuleHistory(OpModuleHistory toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveModuleHistory(toBeSaved);
                if (ModuleHistoryCacheEnabled)
                {
                    ModuleHistoryDict[toBeSaved.IdModuleHistory] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveModuleHistory", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ModuleHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">ModuleHistory to delete</param>
        public virtual void DeleteModuleHistory(OpModuleHistory toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteModuleHistory(toBeDeleted);
                if (ModuleHistoryCacheEnabled)
                {
                    ModuleHistoryDict.Remove(toBeDeleted.IdModuleHistory);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelModuleHistory", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ModuleHistory object cache
        /// </summary>
        public virtual void ClearModuleHistoryCache()
        {
            ModuleHistoryDict.Clear();
            ModuleHistoryIsFullCache = false;
        }
        #endregion

        #region MonitoringWatcherDW
        /// <summary>
        /// Indicates whether the MonitoringWatcherDict was filled trough GetAllMonitoringWatcher or GetAllMonitoringWatcherFilter with mergeIntoCache = true
        /// </summary>
        public bool MonitoringWatcherIsFullCacheDW { get; protected set; }

        /// <summary>
        /// MonitoringWatcher objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpMonitoringWatcher> MonitoringWatcherDictDW = new Dictionary<int, OpMonitoringWatcher>();

        /// <summary>
        /// Indicates whether the MonitoringWatcher is cached.
        /// </summary>
        public bool MonitoringWatcherCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcher objects
        /// </summary>
        public List<OpMonitoringWatcher> MonitoringWatcherDW
        {
            get { return GetAllMonitoringWatcherDW(); }
        }

        /// <summary>
        /// Gets all MonitoringWatcher objects
        /// </summary>
        public List<OpMonitoringWatcher> GetAllMonitoringWatcherDW()
        {
            try
            {
                if (MonitoringWatcherCacheEnabledDW) //cache enabled
                {
                    if (!MonitoringWatcherIsFullCacheDW)
                    {
                        List<OpMonitoringWatcher> objectList = OpMonitoringWatcher.ConvertList(dbConnectionDw.GetMonitoringWatcherDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            MonitoringWatcherDictDW = objectList.ToDictionary(o => o.IdMonitoringWatcher);
                            MonitoringWatcherIsFullCacheDW = true;
                        }
                    }
                    return new List<OpMonitoringWatcher>(MonitoringWatcherDictDW.Values);
                }
                else //cache disabled
                {
                    return OpMonitoringWatcher.ConvertList(dbConnectionDw.GetMonitoringWatcherDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcher", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Id">MonitoringWatcher Id</param>
        /// <returns>MonitoringWatcher object</returns>
        public OpMonitoringWatcher GetMonitoringWatcherDW(int Id)
        {
            return GetMonitoringWatcherDW(Id, false);
        }

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcher Ids</param>
        /// <returns>MonitoringWatcher list</returns>
        public List<OpMonitoringWatcher> GetMonitoringWatcherDW(int[] Ids)
        {
            return GetMonitoringWatcherDW(Ids, false);
        }

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Id">MonitoringWatcher Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcher object</returns>
        public OpMonitoringWatcher GetMonitoringWatcherDW(int Id, bool queryDatabase)
        {
            return GetMonitoringWatcherDW(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets MonitoringWatcher by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcher Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcher list</returns>
        public List<OpMonitoringWatcher> GetMonitoringWatcherDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMonitoringWatcher> returnList = new List<OpMonitoringWatcher>();
            if (Ids != null && Ids.Length > 0)
            {
                if (MonitoringWatcherCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpMonitoringWatcher MonitoringWatcherFoundInCache;
                        if (MonitoringWatcherDictDW.TryGetValue(Ids[i], out MonitoringWatcherFoundInCache)) //element founded in cache
                            returnList.Add(MonitoringWatcherFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpMonitoringWatcher item in OpMonitoringWatcher.ConvertList(dbConnectionDw.GetMonitoringWatcherDW(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                MonitoringWatcherDictDW[item.IdMonitoringWatcher] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcher", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpMonitoringWatcher.ConvertList(dbConnectionDw.GetMonitoringWatcherDW(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcher", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the MonitoringWatcher object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcher to save</param>
        /// <returns>MonitoringWatcher Id</returns>
        public int SaveMonitoringWatcherDW(OpMonitoringWatcher toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveMonitoringWatcherDW(toBeSaved);
                if (MonitoringWatcherCacheEnabledDW)
                {
                    MonitoringWatcherDictDW[toBeSaved.IdMonitoringWatcher] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveMonitoringWatcher", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the MonitoringWatcher object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcher to delete</param>
        public void DeleteMonitoringWatcherDW(OpMonitoringWatcher toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteMonitoringWatcherDW(toBeDeleted);
                if (MonitoringWatcherCacheEnabledDW)
                {
                    MonitoringWatcherDictDW.Remove(toBeDeleted.IdMonitoringWatcher);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelMonitoringWatcher", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the MonitoringWatcher object cache
        /// </summary>
        public void ClearMonitoringWatcherCacheDW()
        {
            MonitoringWatcherDictDW.Clear();
            MonitoringWatcherIsFullCacheDW = false;
        }
        #endregion

        #region MonitoringWatcherDataDW
        /// <summary>
        /// Indicates whether the MonitoringWatcherDataDict was filled trough GetAllMonitoringWatcherData or GetAllMonitoringWatcherDataFilter with mergeIntoCache = true
        /// </summary>
        public bool MonitoringWatcherDataIsFullCacheDW { get; protected set; }

        /// <summary>
        /// MonitoringWatcherData objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpMonitoringWatcherData> MonitoringWatcherDataDictDW = new Dictionary<int, OpMonitoringWatcherData>();

        /// <summary>
        /// Indicates whether the MonitoringWatcherData is cached.
        /// </summary>
        public bool MonitoringWatcherDataCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcherData objects
        /// </summary>
        public List<OpMonitoringWatcherData> MonitoringWatcherDataDW
        {
            get { return GetAllMonitoringWatcherDataDW(); }
        }

        /// <summary>
        /// Gets all MonitoringWatcherData objects
        /// </summary>
        public List<OpMonitoringWatcherData> GetAllMonitoringWatcherDataDW()
        {
            try
            {
                if (MonitoringWatcherDataCacheEnabledDW) //cache enabled
                {
                    if (!MonitoringWatcherDataIsFullCacheDW)
                    {
                        List<OpMonitoringWatcherData> objectList = OpMonitoringWatcherData.ConvertList(dbConnectionDw.GetMonitoringWatcherDataDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            MonitoringWatcherDataDictDW = objectList.ToDictionary(o => o.IdMonitoringWatcherData);
                            MonitoringWatcherDataIsFullCacheDW = true;
                        }
                    }
                    return new List<OpMonitoringWatcherData>(MonitoringWatcherDataDictDW.Values);
                }
                else //cache disabled
                {
                    return OpMonitoringWatcherData.ConvertList(dbConnectionDw.GetMonitoringWatcherDataDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherData", ex);
                throw;
            }
        }

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherData Id</param>
        /// <returns>MonitoringWatcherData object</returns>
        public OpMonitoringWatcherData GetMonitoringWatcherDataDW(int Id)
        {
            return GetMonitoringWatcherDataDW(Id, false);
        }

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherData Ids</param>
        /// <returns>MonitoringWatcherData list</returns>
        public List<OpMonitoringWatcherData> GetMonitoringWatcherDataDW(int[] Ids)
        {
            return GetMonitoringWatcherDataDW(Ids, false);
        }

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherData object</returns>
        public OpMonitoringWatcherData GetMonitoringWatcherDataDW(int Id, bool queryDatabase)
        {
            return GetMonitoringWatcherDataDW(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets MonitoringWatcherData by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherData list</returns>
        public List<OpMonitoringWatcherData> GetMonitoringWatcherDataDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMonitoringWatcherData> returnList = new List<OpMonitoringWatcherData>();
            if (Ids != null && Ids.Length > 0)
            {
                if (MonitoringWatcherDataCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpMonitoringWatcherData MonitoringWatcherDataFoundInCacheDW;
                        if (MonitoringWatcherDataDictDW.TryGetValue(Ids[i], out MonitoringWatcherDataFoundInCacheDW)) //element founded in cache
                            returnList.Add(MonitoringWatcherDataFoundInCacheDW);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpMonitoringWatcherData item in OpMonitoringWatcherData.ConvertList(dbConnectionDw.GetMonitoringWatcherDataDW(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                MonitoringWatcherDataDictDW[item.IdMonitoringWatcherData] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherData", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpMonitoringWatcherData.ConvertList(dbConnectionDw.GetMonitoringWatcherDataDW(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherData", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the MonitoringWatcherData object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcherData to save</param>
        /// <returns>MonitoringWatcherData Id</returns>
        public int SaveMonitoringWatcherDataDW(OpMonitoringWatcherData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                int ret = dbConnectionDw.SaveMonitoringWatcherDataDW(toBeSaved, toBeSaved.DataType);
                if (MonitoringWatcherDataCacheEnabledDW)
                {
                    MonitoringWatcherDataDictDW[toBeSaved.IdMonitoringWatcherData] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveMonitoringWatcherData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the MonitoringWatcherData object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcherData to delete</param>
        public void DeleteMonitoringWatcherDataDW(OpMonitoringWatcherData toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteMonitoringWatcherDataDW(toBeDeleted);
                if (MonitoringWatcherDataCacheEnabledDW)
                {
                    MonitoringWatcherDataDictDW.Remove(toBeDeleted.IdMonitoringWatcherData);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelMonitoringWatcherData", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the MonitoringWatcherData object cache
        /// </summary>
        public void ClearMonitoringWatcherDataCacheDW()
        {
            MonitoringWatcherDataDictDW.Clear();
            MonitoringWatcherDataIsFullCacheDW = false;
        }
        #endregion

        #region MonitoringWatcherTypeDW
        /// <summary>
        /// Indicates whether the MonitoringWatcherTypeDict was filled trough GetAllMonitoringWatcherType or GetAllMonitoringWatcherTypeFilter with mergeIntoCache = true
        /// </summary>
        public bool MonitoringWatcherTypeIsFullCacheDW { get; protected set; }

        /// <summary>
        /// MonitoringWatcherType objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpMonitoringWatcherType> MonitoringWatcherTypeDictDW = new Dictionary<int, OpMonitoringWatcherType>();

        /// <summary>
        /// Indicates whether the MonitoringWatcherType is cached.
        /// </summary>
        public bool MonitoringWatcherTypeCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all MonitoringWatcherType objects
        /// </summary>
        public List<OpMonitoringWatcherType> MonitoringWatcherTypeDW
        {
            get { return GetAllMonitoringWatcherTypeDW(); }
        }

        /// <summary>
        /// Gets all MonitoringWatcherType objects
        /// </summary>
        public List<OpMonitoringWatcherType> GetAllMonitoringWatcherTypeDW()
        {
            try
            {
                if (MonitoringWatcherTypeCacheEnabledDW) //cache enabled
                {
                    if (!MonitoringWatcherTypeIsFullCacheDW)
                    {
                        List<OpMonitoringWatcherType> objectList = OpMonitoringWatcherType.ConvertList(dbConnectionDw.GetMonitoringWatcherTypeDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            MonitoringWatcherTypeDictDW = objectList.ToDictionary(o => o.IdMonitoringWatcherType);
                            MonitoringWatcherTypeIsFullCacheDW = true;
                        }
                    }
                    return new List<OpMonitoringWatcherType>(MonitoringWatcherTypeDictDW.Values);
                }
                else //cache disabled
                {
                    return OpMonitoringWatcherType.ConvertList(dbConnectionDw.GetMonitoringWatcherTypeDW(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherType", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherType Id</param>
        /// <returns>MonitoringWatcherType object</returns>
        public OpMonitoringWatcherType GetMonitoringWatcherTypeDW(int Id)
        {
            return GetMonitoringWatcherTypeDW(Id, false);
        }

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherType Ids</param>
        /// <returns>MonitoringWatcherType list</returns>
        public List<OpMonitoringWatcherType> GetMonitoringWatcherTypeDW(int[] Ids)
        {
            return GetMonitoringWatcherTypeDW(Ids, false);
        }

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Id">MonitoringWatcherType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherType object</returns>
        public OpMonitoringWatcherType GetMonitoringWatcherTypeDW(int Id, bool queryDatabase)
        {
            return GetMonitoringWatcherTypeDW(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets MonitoringWatcherType by id
        /// </summary>
        /// <param name="Ids">MonitoringWatcherType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MonitoringWatcherType list</returns>
        public List<OpMonitoringWatcherType> GetMonitoringWatcherTypeDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMonitoringWatcherType> returnList = new List<OpMonitoringWatcherType>();
            if (Ids != null && Ids.Length > 0)
            {
                if (MonitoringWatcherTypeCacheEnabledDW && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpMonitoringWatcherType MonitoringWatcherTypeFoundInCache;
                        if (MonitoringWatcherTypeDictDW.TryGetValue(Ids[i], out MonitoringWatcherTypeFoundInCache)) //element founded in cache
                            returnList.Add(MonitoringWatcherTypeFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpMonitoringWatcherType item in OpMonitoringWatcherType.ConvertList(dbConnectionDw.GetMonitoringWatcherTypeDW(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                MonitoringWatcherTypeDictDW[item.IdMonitoringWatcherType] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherType", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpMonitoringWatcherType.ConvertList(dbConnectionDw.GetMonitoringWatcherTypeDW(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherType", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the MonitoringWatcherType object to the database
        /// </summary>
        /// <param name="toBeSaved">MonitoringWatcherType to save</param>
        /// <returns>MonitoringWatcherType Id</returns>
        public int SaveMonitoringWatcherTypeDW(OpMonitoringWatcherType toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveMonitoringWatcherTypeDW(toBeSaved);
                if (MonitoringWatcherTypeCacheEnabledDW)
                {
                    MonitoringWatcherTypeDictDW[toBeSaved.IdMonitoringWatcherType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveMonitoringWatcherType", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the MonitoringWatcherType object from the database
        /// </summary>
        /// <param name="toBeDeleted">MonitoringWatcherType to delete</param>
        public void DeleteMonitoringWatcherTypeDW(OpMonitoringWatcherType toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteMonitoringWatcherTypeDW(toBeDeleted);
                if (MonitoringWatcherTypeCacheEnabledDW)
                {
                    MonitoringWatcherTypeDictDW.Remove(toBeDeleted.IdMonitoringWatcherType);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelMonitoringWatcherType", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the MonitoringWatcherType object cache
        /// </summary>
        public void ClearMonitoringWatcherTypeCacheDW()
        {
            MonitoringWatcherTypeDictDW.Clear();
            MonitoringWatcherTypeIsFullCacheDW = false;
        }
        #endregion

        #region ProblemClass
        /// <summary>
        /// Indicates whether the ProblemClassDict was filled trough GetAllProblemClass or GetAllProblemClassFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ProblemClassIsFullCache { get; protected set; }

        /// <summary>
        /// ProblemClass objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpProblemClass> ProblemClassDict = new Dictionary<int, OpProblemClass>();

        /// <summary>
        /// Indicates whether the ProblemClass is cached.
        /// </summary>
        public bool ProblemClassCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ProblemClass objects
        /// </summary>
        public virtual List<OpProblemClass> ProblemClass
        {
            get { return GetAllProblemClass(); }
        }

        /// <summary>
        /// Gets all ProblemClass objects
        /// </summary>
        public virtual List<OpProblemClass> GetAllProblemClass()
        {
            try
            {
                if (ProblemClassCacheEnabled) //cache enabled
                {
                    if (!ProblemClassIsFullCache)
                    {
                        List<OpProblemClass> objectList = OpProblemClass.ConvertList(dbConnectionDw.GetProblemClass(), this);
                        if (objectList != null)
                        {
                            ProblemClassDict = objectList.ToDictionary(o => o.IdProblemClass);
                            ProblemClassIsFullCache = true;
                        }
                    }
                    return new List<OpProblemClass>(ProblemClassDict.Values);
                }
                else //cache disabled
                {
                    return OpProblemClass.ConvertList(dbConnectionDw.GetProblemClass(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProblemClass", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Id">ProblemClass Id</param>
        /// <returns>ProblemClass object</returns>
        public virtual OpProblemClass GetProblemClass(int Id)
        {
            return GetProblemClass(Id, false);
        }

        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Ids">ProblemClass Ids</param>
        /// <returns>ProblemClass list</returns>
        public virtual List<OpProblemClass> GetProblemClass(int[] Ids)
        {
            return GetProblemClass(Ids, false);
        }

        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Id">ProblemClass Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProblemClass object</returns>
        public virtual OpProblemClass GetProblemClass(int Id, bool queryDatabase)
        {
            return GetProblemClass(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ProblemClass by id
        /// </summary>
        /// <param name="Ids">ProblemClass Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ProblemClass list</returns>
        public virtual List<OpProblemClass> GetProblemClass(int[] Ids, bool queryDatabase)
        {
            List<OpProblemClass> returnList = new List<OpProblemClass>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ProblemClassCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpProblemClass ProblemClassFoundInCache;
                        if (ProblemClassDict.TryGetValue(Ids[i], out ProblemClassFoundInCache)) //element founded in cache
                            returnList.Add(ProblemClassFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpProblemClass item in OpProblemClass.ConvertList(dbConnectionDw.GetProblemClass(idsNotInCache.ToArray()), this))
                            {
                                ProblemClassDict[item.IdProblemClass] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProblemClass", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpProblemClass.ConvertList(dbConnectionDw.GetProblemClass(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProblemClass", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ProblemClass object to the database
        /// </summary>
        /// <param name="toBeSaved">ProblemClass to save</param>
        /// <returns>ProblemClass Id</returns>
        public virtual int SaveProblemClass(OpProblemClass toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveProblemClass(toBeSaved);
                if (ProblemClassCacheEnabled)
                {
                    ProblemClassDict[toBeSaved.IdProblemClass] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveProblemClass", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ProblemClass object from the database
        /// </summary>
        /// <param name="toBeDeleted">ProblemClass to delete</param>
        public virtual void DeleteProblemClass(OpProblemClass toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteProblemClass(toBeDeleted);
                if (ProblemClassCacheEnabled)
                {
                    ProblemClassDict.Remove(toBeDeleted.IdProblemClass);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelProblemClass", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ProblemClass object cache
        /// </summary>
        public virtual void ClearProblemClassCache()
        {
            ProblemClassDict.Clear();
            ProblemClassIsFullCache = false;
        }
        #endregion

        #region Refuel
        /// <summary>
        /// Indicates whether the RefuelDict was filled trough GetAllRefuel or GetAllRefuelFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool RefuelIsFullCache { get; protected set; }

        /// <summary>
        /// Refuel objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpRefuel> RefuelDict = new Dictionary<long, OpRefuel>();

        /// <summary>
        /// Indicates whether the Refuel is cached.
        /// </summary>
        public bool RefuelCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Refuel objects
        /// </summary>
        public virtual List<OpRefuel> Refuel
        {
            get { return GetAllRefuel(); }
        }

        /// <summary>
        /// Gets all Refuel objects
        /// </summary>
        public virtual List<OpRefuel> GetAllRefuel()
        {
            try
            {
                if (RefuelCacheEnabled) //cache enabled
                {
                    if (!RefuelIsFullCache)
                    {
                        List<OpRefuel> objectList = OpRefuel.ConvertList(dbConnectionDw.GetRefuel(), this);
                        if (objectList != null)
                        {
                            RefuelDict = objectList.ToDictionary(o => o.IdRefuel);
                            RefuelIsFullCache = true;
                        }
                    }
                    return new List<OpRefuel>(RefuelDict.Values);
                }
                else //cache disabled
                {
                    return OpRefuel.ConvertList(dbConnectionDw.GetRefuel(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuel", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Id">Refuel Id</param>
        /// <returns>Refuel object</returns>
        public virtual OpRefuel GetRefuel(long Id)
        {
            return GetRefuel(Id, false);
        }

        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Ids">Refuel Ids</param>
        /// <returns>Refuel list</returns>
        public virtual List<OpRefuel> GetRefuel(long[] Ids)
        {
            return GetRefuel(Ids, false);
        }

        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Id">Refuel Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Refuel object</returns>
        public virtual OpRefuel GetRefuel(long Id, bool queryDatabase)
        {
            return GetRefuel(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Refuel by id
        /// </summary>
        /// <param name="Ids">Refuel Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Refuel list</returns>
        public virtual List<OpRefuel> GetRefuel(long[] Ids, bool queryDatabase)
        {
            List<OpRefuel> returnList = new List<OpRefuel>();
            if (Ids != null && Ids.Length > 0)
            {
                if (RefuelCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpRefuel RefuelFoundInCache;
                        if (RefuelDict.TryGetValue(Ids[i], out RefuelFoundInCache)) //element founded in cache
                            returnList.Add(RefuelFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpRefuel item in OpRefuel.ConvertList(dbConnectionDw.GetRefuel(idsNotInCache.ToArray()), this))
                            {
                                RefuelDict[item.IdRefuel] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuel", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpRefuel.ConvertList(dbConnectionDw.GetRefuel(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuel", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Refuel object to the database
        /// </summary>
        /// <param name="toBeSaved">Refuel to save</param>
        /// <returns>Refuel Id</returns>
        public virtual long SaveRefuel(OpRefuel toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveRefuel(toBeSaved);
                if (RefuelCacheEnabled)
                {
                    RefuelDict[toBeSaved.IdRefuel] = toBeSaved; //add or update element
                }
                toBeSaved.DataList.ForEach(data => data.Refuel = toBeSaved);
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveRefuel", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Refuel object from the database
        /// </summary>
        /// <param name="toBeDeleted">Refuel to delete</param>
        public virtual void DeleteRefuel(OpRefuel toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteRefuel(toBeDeleted);
                if (RefuelCacheEnabled)
                {
                    RefuelDict.Remove(toBeDeleted.IdRefuel);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelRefuel", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the Refuel object cache
        /// </summary>
        public virtual void ClearRefuelCache()
        {
            RefuelDict.Clear();
            RefuelIsFullCache = false;
        }
        #endregion

        #region RefuelData
        /// <summary>
        /// Indicates whether the RefuelDataDict was filled trough GetAllRefuelData or GetAllRefuelDataFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool RefuelDataIsFullCache { get; protected set; }

        /// <summary>
        /// RefuelData objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpRefuelData> RefuelDataDict = new Dictionary<long, OpRefuelData>();

        /// <summary>
        /// Indicates whether the RefuelData is cached.
        /// </summary>
        public bool RefuelDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RefuelData objects
        /// </summary>
        public virtual List<OpRefuelData> RefuelData
        {
            get { return GetAllRefuelData(); }
        }

        /// <summary>
        /// Gets all RefuelData objects
        /// </summary>
        public virtual List<OpRefuelData> GetAllRefuelData()
        {
            try
            {
                if (RefuelDataCacheEnabled) //cache enabled
                {
                    if (!RefuelDataIsFullCache)
                    {
                        List<OpRefuelData> objectList = OpRefuelData.ConvertList(dbConnectionDw.GetRefuelData(), this);
                        if (objectList != null)
                        {
                            RefuelDataDict = objectList.ToDictionary(o => o.IdRefuelData);
                            RefuelDataIsFullCache = true;
                        }
                    }
                    return new List<OpRefuelData>(RefuelDataDict.Values);
                }
                else //cache disabled
                {
                    return OpRefuelData.ConvertList(dbConnectionDw.GetRefuelData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuelData", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Id">RefuelData Id</param>
        /// <returns>RefuelData object</returns>
        public virtual OpRefuelData GetRefuelData(long Id)
        {
            return GetRefuelData(Id, false);
        }

        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Ids">RefuelData Ids</param>
        /// <returns>RefuelData list</returns>
        public virtual List<OpRefuelData> GetRefuelData(long[] Ids)
        {
            return GetRefuelData(Ids, false);
        }

        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Id">RefuelData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RefuelData object</returns>
        public virtual OpRefuelData GetRefuelData(long Id, bool queryDatabase)
        {
            return GetRefuelData(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets RefuelData by id
        /// </summary>
        /// <param name="Ids">RefuelData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RefuelData list</returns>
        public virtual List<OpRefuelData> GetRefuelData(long[] Ids, bool queryDatabase)
        {
            List<OpRefuelData> returnList = new List<OpRefuelData>();
            if (Ids != null && Ids.Length > 0)
            {
                if (RefuelDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpRefuelData RefuelDataFoundInCache;
                        if (RefuelDataDict.TryGetValue(Ids[i], out RefuelDataFoundInCache)) //element founded in cache
                            returnList.Add(RefuelDataFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpRefuelData item in OpRefuelData.ConvertList(dbConnectionDw.GetRefuelData(idsNotInCache.ToArray()), this))
                            {
                                RefuelDataDict[item.IdRefuelData] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuelData", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpRefuelData.ConvertList(dbConnectionDw.GetRefuelData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuelData", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the RefuelData object to the database
        /// </summary>
        /// <param name="toBeSaved">RefuelData to save</param>
        /// <returns>RefuelData Id</returns>
        public virtual long SaveRefuelData(OpRefuelData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveRefuelData(toBeSaved, toBeSaved.DataType);
                if (RefuelDataCacheEnabled)
                {
                    RefuelDataDict[toBeSaved.IdRefuelData] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveRefuelData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the RefuelData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RefuelData to delete</param>
        public virtual void DeleteRefuelData(OpRefuelData toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteRefuelData(toBeDeleted);
                if (RefuelDataCacheEnabled)
                {
                    RefuelDataDict.Remove(toBeDeleted.IdRefuelData);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelRefuelData", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the RefuelData object cache
        /// </summary>
        public virtual void ClearRefuelDataCache()
        {
            RefuelDataDict.Clear();
            RefuelDataIsFullCache = false;
        }
        #endregion
        
        #region Report
        /// <summary>
        /// Indicates whether the ReportDict was filled trough GetAllReport or GetReportFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ReportIsFullCache { get; protected set; }

        /// <summary>
        /// Report objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpReport> ReportDict = new Dictionary<int, OpReport>();

        /// <summary>
        /// Indicates whether the Report is cached.
        /// </summary>
        public virtual bool ReportCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataSource objects
        /// </summary>
        public virtual List<OpReport> Report
        {
            get { return GetAllReport(); }
        }

        /// <summary>
        /// Gets all Report objects
        /// </summary>
        public virtual List<OpReport> GetAllReport(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (ReportCacheEnabled) //cache enabled
                {
                    if (!ReportIsFullCache)
                    {
                        List<OpReport> objectList = OpReport.ConvertList(dbConnectionDw.GetReport(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                        if (objectList != null)
                        {
                            ReportDict = objectList.ToDictionary(o => o.IdReport);
                            ReportIsFullCache = true;
                        }
                    }
                    return new List<OpReport>(ReportDict.Values);
                }
                else //cache disabled
                {
                    return OpReport.ConvertList(dbConnectionDw.GetReport(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReport", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Id">Report Id</param>
        /// <returns>Report object</returns>
        public virtual OpReport GetReport(int Id)
        {
            return GetReport(Id, false);
        }

        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Ids">Report Ids</param>
        /// <returns>Report list</returns>
        public virtual List<OpReport> GetReport(int[] Ids)
        {
            return GetReport(Ids, false);
        }

        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Id">Report Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Report object</returns>
        public virtual OpReport GetReport(int Id, bool queryDatabase)
        {
            return GetReport(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Report by id
        /// </summary>
        /// <param name="Ids">Report Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Report list</returns>
        public virtual List<OpReport> GetReport(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (ReportCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                List<OpReport> returnList = new List<OpReport>();
                List<int> idsNotInCache = new List<int>();
                for (int i = 0; i < Ids.Length; i++)
                {
                    OpReport reportFoundInCache;
                    if (ReportDict != null && ReportDict.TryGetValue(Ids[i], out reportFoundInCache)) //element founded in cache
                        returnList.Add(reportFoundInCache);
                    else //element not found in cache, query database
                        idsNotInCache.Add(Ids[i]);
                }
                if (idsNotInCache.Count > 0)
                {
                    try
                    {
                        foreach (OpReport item in OpReport.ConvertList(dbConnectionDw.GetReport(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                        {
                            ReportDict[item.IdReport] = item;
                            returnList.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReport", ex);
                        throw ex;
                    }
                }
                return returnList;
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpReport.ConvertList(dbConnectionDw.GetReport(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReport", ex);
                    throw ex;
                }
            }
        }
        
        /// <summary>
        /// Clears the Report object cache
        /// </summary>
        public virtual void ClearReportCache()
        {
            ReportDict.Clear();
            ReportIsFullCache = false;
        }
        #endregion

        #region ReportData
        /// <summary>
        /// Indicates whether the ReportDataDict was filled trough GetAllReportData or GetReportDataFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ReportDataIsFullCache { get; protected set; }

        /// <summary>
        /// ReportData objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpReportData> ReportDataDict = new Dictionary<long, OpReportData>();

        /// <summary>
        /// Indicates whether the ReportData is cached.
        /// </summary>
        public virtual bool ReportDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportData objects
        /// </summary>
        public virtual List<OpReportData> ReportData
        {
            get { return GetAllReportData(); }
        }

        /// <summary>
        /// Gets all ReportData objects
        /// </summary>
        public virtual List<OpReportData> GetAllReportData()
        {
            try
            {
                if (ReportDataCacheEnabled) //cache enabled
                {
                    if (!ReportDataIsFullCache)
                    {
                        List<OpReportData> objectList = OpReportData.ConvertList(dbConnectionDw.GetReportData(), this);
                        if (objectList != null)
                        {
                            ReportDataDict = objectList.ToDictionary(o => o.IdReportData);
                            ReportDataIsFullCache = true;
                        }
                    }
                    return new List<OpReportData>(ReportDataDict.Values);
                }
                else //cache disabled
                {
                    return OpReportData.ConvertList(dbConnectionDw.GetReportData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportData", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Id">ReportData Id</param>
        /// <returns>ReportData object</returns>
        public virtual OpReportData GetReportData(long Id)
        {
            return GetReportData(Id, false);
        }

        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Ids">ReportData Ids</param>
        /// <returns>ReportData list</returns>
        public virtual List<OpReportData> GetReportData(long[] Ids)
        {
            return GetReportData(Ids, false);
        }

        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Id">ReportData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportData object</returns>
        public virtual OpReportData GetReportData(long Id, bool queryDatabase)
        {
            return GetReportData(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ReportData by id
        /// </summary>
        /// <param name="Ids">ReportData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportData list</returns>
        public virtual List<OpReportData> GetReportData(long[] Ids, bool queryDatabase)
        {
            if (ReportDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                List<OpReportData> returnList = new List<OpReportData>();
                List<long> idsNotInCache = new List<long>();
                for (int i = 0; i < Ids.Length; i++)
                {
                    OpReportData reportdataFoundInCache;
                    if (ReportDataDict != null && ReportDataDict.TryGetValue(Ids[i], out reportdataFoundInCache)) //element founded in cache
                        returnList.Add(reportdataFoundInCache);
                    else //element not found in cache, query database
                        idsNotInCache.Add(Ids[i]);
                }
                if (idsNotInCache.Count > 0)
                {
                    try
                    {
                        foreach (OpReportData item in OpReportData.ConvertList(dbConnectionDw.GetReportData(idsNotInCache.ToArray()), this))
                        {
                            ReportDataDict[item.IdReportData] = item;
                            returnList.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportData", ex);
                        throw ex;
                    }
                }
                return returnList;
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpReportData.ConvertList(dbConnectionDw.GetReportData(Ids), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportData", ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Clears the ReportData object cache
        /// </summary>
        public virtual void ClearReportDataCache()
        {
            ReportDataDict.Clear();
        }
        #endregion

        #region ReportParam
        /// <summary>
        /// Indicates whether the ReportParamDict was filled trough GetAllReportParam or GetAllReportParamFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ReportParamIsFullCache { get; protected set; }

        /// <summary>
        /// ReportParam objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpReportParam> ReportParamDict = new Dictionary<int, OpReportParam>();

        /// <summary>
        /// Indicates whether the ReportParam is cached.
        /// </summary>
        public virtual bool ReportParamCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportParam objects
        /// </summary>
        public virtual List<OpReportParam> ReportParam
        {
            get { return GetAllReportParam(); }
        }

        /// <summary>
        /// Gets all ReportParam objects
        /// </summary>
        public virtual List<OpReportParam> GetAllReportParam()
        {
            try
            {
                if (ReportParamCacheEnabled) //cache enabled
                {
                    if (!ReportParamIsFullCache)
                    {
                        List<OpReportParam> objectList = OpReportParam.ConvertList(dbConnectionDw.GetReportParam(), this);
                        if (objectList != null)
                        {
                            ReportParamIsFullCache = true;
                            ReportParamDict = objectList.ToDictionary(o => o.IdReportParam);
                        }
                    }
                    return new List<OpReportParam>(ReportParamDict.Values);
                }
                else //cache disabled
                {
                    return OpReportParam.ConvertList(dbConnectionDw.GetReportParam(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportParam", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Id">ReportParam Id</param>
        /// <returns>ReportParam object</returns>
        public virtual OpReportParam GetReportParam(int Id)
        {
            return GetReportParam(Id, false);
        }

        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Ids">ReportParam Ids</param>
        /// <returns>ReportParam list</returns>
        public virtual List<OpReportParam> GetReportParam(int[] Ids)
        {
            return GetReportParam(Ids, false);
        }

        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Id">ReportParam Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportParam object</returns>
        public virtual OpReportParam GetReportParam(int Id, bool queryDatabase)
        {
            return GetReportParam(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ReportParam by id
        /// </summary>
        /// <param name="Ids">ReportParam Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportParam list</returns>
        public virtual List<OpReportParam> GetReportParam(int[] Ids, bool queryDatabase)
        {
            if (ReportParamCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                List<OpReportParam> returnList = new List<OpReportParam>();
                List<int> idsNotInCache = new List<int>();
                for (int i = 0; i < Ids.Length; i++)
                {
                    OpReportParam reportparamFoundInCache;
                    if (ReportParamDict != null && ReportParamDict.TryGetValue(Ids[i], out reportparamFoundInCache)) //element founded in cache
                        returnList.Add(reportparamFoundInCache);
                    else //element not found in cache, query database
                        idsNotInCache.Add(Ids[i]);
                }
                if (idsNotInCache.Count > 0)
                {
                    try
                    {
                        foreach (OpReportParam item in OpReportParam.ConvertList(dbConnectionDw.GetReportParam(idsNotInCache.ToArray()), this))
                        {
                            ReportParamDict[item.IdReportParam] = item;
                            returnList.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportParam", ex);
                        throw ex;
                    }
                }
                return returnList;
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpReportParam.ConvertList(dbConnectionDw.GetReportParam(Ids), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportParam", ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Saves the ReportParam object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportParam to save</param>
        /// <returns>ReportParam Id</returns>
        public virtual int SaveReportParam(OpReportParam toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveReportParam(toBeSaved);
                if (ReportParamCacheEnabled)
                {
                    ReportParamDict[toBeSaved.IdReportParam] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveReportParam", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the ReportParam object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportParam to delete</param>
        public virtual void DeleteReportParam(OpReportParam toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteReportParam(toBeDeleted);
                if (ReportParamCacheEnabled)
                {
                    ReportParamDict.Remove(toBeDeleted.IdReportParam);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelReportParam", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the ReportParam object cache
        /// </summary>
        public virtual void ClearReportParamCache()
        {
            ReportParamDict.Clear();
        }
        #endregion

        #region ReportPerformance
        /// <summary>
        /// Indicates whether the ReportPerformanceDict was filled trough GetAllReportPerformance or GetAllReportPerformanceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ReportPerformanceIsFullCache { get; protected set; }

        /// <summary>
        /// ReportPerformance objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpReportPerformance> ReportPerformanceDict = new Dictionary<long, OpReportPerformance>();

        /// <summary>
        /// Indicates whether the ReportPerformance is cached.
        /// </summary>
        public bool ReportPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportPerformance objects
        /// </summary>
        public virtual List<OpReportPerformance> ReportPerformance
        {
            get { return GetAllReportPerformance(); }
        }

        /// <summary>
        /// Gets all ReportPerformance objects
        /// </summary>
        public virtual List<OpReportPerformance> GetAllReportPerformance()
        {
            try
            {
                if (ReportPerformanceCacheEnabled) //cache enabled
                {
                    if (!ReportPerformanceIsFullCache)
                    {
                        List<OpReportPerformance> objectList = OpReportPerformance.ConvertList(dbConnectionDw.GetReportPerformance(), this);
                        if (objectList != null)
                        {
                            ReportPerformanceDict = objectList.ToDictionary(o => o.IdReportPerformance);
                            ReportPerformanceIsFullCache = true;
                        }
                    }
                    return new List<OpReportPerformance>(ReportPerformanceDict.Values);
                }
                else //cache disabled
                {
                    return OpReportPerformance.ConvertList(dbConnectionDw.GetReportPerformance(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportPerformance", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Id">ReportPerformance Id</param>
        /// <returns>ReportPerformance object</returns>
        public virtual OpReportPerformance GetReportPerformance(long Id)
        {
            return GetReportPerformance(Id, false);
        }

        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Ids">ReportPerformance Ids</param>
        /// <returns>ReportPerformance list</returns>
        public virtual List<OpReportPerformance> GetReportPerformance(long[] Ids)
        {
            return GetReportPerformance(Ids, false);
        }

        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Id">ReportPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportPerformance object</returns>
        public virtual OpReportPerformance GetReportPerformance(long Id, bool queryDatabase)
        {
            return GetReportPerformance(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ReportPerformance by id
        /// </summary>
        /// <param name="Ids">ReportPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportPerformance list</returns>
        public virtual List<OpReportPerformance> GetReportPerformance(long[] Ids, bool queryDatabase)
        {
            List<OpReportPerformance> returnList = new List<OpReportPerformance>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ReportPerformanceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpReportPerformance ReportPerformanceFoundInCache;
                        if (ReportPerformanceDict.TryGetValue(Ids[i], out ReportPerformanceFoundInCache)) //element founded in cache
                            returnList.Add(ReportPerformanceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpReportPerformance item in OpReportPerformance.ConvertList(dbConnectionDw.GetReportPerformance(idsNotInCache.ToArray()), this))
                            {
                                ReportPerformanceDict[item.IdReportPerformance] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportPerformance", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpReportPerformance.ConvertList(dbConnectionDw.GetReportPerformance(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportPerformance", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ReportPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportPerformance to save</param>
        /// <returns>ReportPerformance Id</returns>
        public virtual long SaveReportPerformance(OpReportPerformance toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveReportPerformance(toBeSaved, toBeSaved.DataType);
                if (ReportPerformanceCacheEnabled)
                {
                    ReportPerformanceDict[toBeSaved.IdReportPerformance] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveReportPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ReportPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportPerformance to delete</param>
        public virtual void DeleteReportPerformance(OpReportPerformance toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteReportPerformance(toBeDeleted);
                if (ReportPerformanceCacheEnabled)
                {
                    ReportPerformanceDict.Remove(toBeDeleted.IdReportPerformance);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelReportPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ReportPerformance object cache
        /// </summary>
        public virtual void ClearReportPerformanceCache()
        {
            ReportPerformanceDict.Clear();
            ReportPerformanceIsFullCache = false;
        }
        #endregion

        #region ReportType
        /// <summary>
        /// Indicates whether the ReportTypeDict was filled trough GetAllReportType or GetAllReportTypeFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ReportTypeIsFullCache { get; protected set; }

        /// <summary>
        /// ReportType objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpReportType> ReportTypeDict = new Dictionary<int, OpReportType>();

        /// <summary>
        /// Indicates whether the ReportType is cached.
        /// </summary>
        public virtual bool ReportTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportType objects
        /// </summary>
        public virtual List<OpReportType> ReportType
        {
            get { return GetAllReportType(); }
        }

        /// <summary>
        /// Gets all ReportType objects
        /// </summary>
        public virtual List<OpReportType> GetAllReportType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (ReportTypeCacheEnabled) //cache enabled
                {
                    if (!ReportTypeIsFullCache)
                    {
                        List<OpReportType> objectList = OpReportType.ConvertList(dbConnectionDw.GetReportType(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                        if (objectList != null)
                        {
                            ReportTypeDict = objectList.ToDictionary(o => o.IdReportType);
                            ReportTypeIsFullCache = true;
                        }
                    }
                    return new List<OpReportType>(ReportTypeDict.Values);
                }
                else //cache disabled
                {
                    return OpReportType.ConvertList(dbConnectionDw.GetReportType(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportType", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Id">ReportType Id</param>
        /// <returns>ReportType object</returns>
        public virtual OpReportType GetReportType(int Id)
        {
            return GetReportType(Id, false);
        }

        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Ids">ReportType Ids</param>
        /// <returns>ReportType list</returns>
        public virtual List<OpReportType> GetReportType(int[] Ids)
        {
            return GetReportType(Ids, false);
        }

        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Id">ReportType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportType object</returns>
        public virtual OpReportType GetReportType(int Id, bool queryDatabase)
        {
            return GetReportType(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ReportType by id
        /// </summary>
        /// <param name="Ids">ReportType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportType list</returns>
        public virtual List<OpReportType> GetReportType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (ReportTypeCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                List<OpReportType> returnList = new List<OpReportType>();
                List<int> idsNotInCache = new List<int>();
                for (int i = 0; i < Ids.Length; i++)
                {
                    OpReportType reporttypeFoundInCache;
                    if (ReportTypeDict != null && ReportTypeDict.TryGetValue(Ids[i], out reporttypeFoundInCache)) //element founded in cache
                        returnList.Add(reporttypeFoundInCache);
                    else //element not found in cache, query database
                        idsNotInCache.Add(Ids[i]);
                }
                if (idsNotInCache.Count > 0)
                {
                    try
                    {
                        foreach (OpReportType item in OpReportType.ConvertList(dbConnectionDw.GetReportType(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                        {
                            ReportTypeDict[item.IdReportType] = item;
                            returnList.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportType", ex);
                        throw ex;
                    }
                }
                return returnList;
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpReportType.ConvertList(dbConnectionDw.GetReportType(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportType", ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Saves the ReportType object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportType to save</param>
        /// <returns>ReportType Id</returns>
        public virtual int SaveReportType(OpReportType toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveReportType(toBeSaved);
                if (ReportTypeCacheEnabled)
                {
                    ReportTypeDict[toBeSaved.IdReportType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveReportType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the ReportType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportType to delete</param>
        public virtual void DeleteReportType(OpReportType toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteReportType(toBeDeleted);
                if (ReportTypeCacheEnabled)
                {
                    ReportTypeDict.Remove(toBeDeleted.IdReportType);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelReportType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the ReportType object cache
        /// </summary>
        public virtual void ClearReportTypeCache()
        {
            ReportTypeDict.Clear();
        }
        #endregion

        #region DataArch
        /// <summary>
        /// Indicates whether the DataArchDict was filled trough GetAllDataArch or GetAllDataArchFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataArchIsFullCache { get; protected set; }

        /// <summary>
        /// DataArch objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDataArch> DataArchDict = new Dictionary<long, OpDataArch>();

        /// <summary>
        /// Indicates whether the DataArch is cached.
        /// </summary>
        public virtual bool DataArchCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataArch objects
        /// </summary>
        public virtual List<OpDataArch> DataArch
        {
            get { return GetAllDataArch(); }
        }

        /// <summary>
        /// Gets all DataArch objects
        /// </summary>
        public virtual List<OpDataArch> GetAllDataArch()
        {
            try
            {
                if (DataArchCacheEnabled) //cache enabled
                {
                    if (!DataArchIsFullCache)
                    {
                        List<OpDataArch> objectList = OpDataArch.ConvertList(dbConnectionDw.GetDataArch(), this);
                        if (objectList != null)
                        {
                            DataArchDict = objectList.ToDictionary(o => o.IdDataArch);
                            DataArchIsFullCache = true;
                        }
                    }
                    return new List<OpDataArch>(DataArchDict.Values);
                }
                else //cache disabled
                {
                    return OpDataArch.ConvertList(dbConnectionDw.GetDataArch(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArch", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Id">DataArch Id</param>
        /// <returns>DataArch object</returns>
        public virtual OpDataArch GetDataArch(long Id)
        {
            return GetDataArch(Id, false);
        }

        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Ids">DataArch Ids</param>
        /// <returns>DataArch list</returns>
        public virtual List<OpDataArch> GetDataArch(long[] Ids)
        {
            return GetDataArch(Ids, false);
        }

        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Id">DataArch Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArch object</returns>
        public virtual OpDataArch GetDataArch(long Id, bool queryDatabase)
        {
            return GetDataArch(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataArch by id
        /// </summary>
        /// <param name="Ids">DataArch Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataArch list</returns>
        public virtual List<OpDataArch> GetDataArch(long[] Ids, bool queryDatabase)
        {
            List<OpDataArch> returnList = new List<OpDataArch>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataArchCacheEnabled && !queryDatabase) // cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDataArch DataArchFoundInCache;
                        if (DataArchDict.TryGetValue(Ids[i], out DataArchFoundInCache)) //element founded in cache
                            returnList.Add(DataArchFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDataArch item in OpDataArch.ConvertList(dbConnectionDw.GetDataArch(idsNotInCache.ToArray()), this))
                            {
                                DataArchDict[item.IdDataArch] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArch", ex);
                            throw ex;
                        }
                    }
                    return returnList;
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataArch.ConvertList(dbConnectionDw.GetDataArch(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArch", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Deletes the DataArch object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataArch to delete</param>
        public virtual void DeleteDataArch(OpDataArch toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDataArch(toBeDeleted);
                if (DataArchCacheEnabled)
                {
                    DataArchDict.Remove(toBeDeleted.IdDataArch);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDataArch", ex);
                throw ex;
            }
        }
        /// <summary>
        /// Clears the DataArch object cache
        /// </summary>
        public virtual void ClearDataArchCache()
        {
            DataArchDict.Clear();
        }
        #endregion

        #region DataSource
        /// <summary>
        /// Indicates whether the DataSourceDict was filled trough GetAllDataSource or GetAllDataSourceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataSourceIsFullCache { get; protected set; }

        /// <summary>
        /// DataSource objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDataSource> DataSourceDict = new Dictionary<long, OpDataSource>();

        /// <summary>
        /// Indicates whether the DataSource is cached.
        /// </summary>
        public virtual bool DataSourceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataSource objects
        /// </summary>
        public virtual List<OpDataSource> DataSource
        {
            get { return GetAllDataSource(); }
        }

        /// <summary>
        /// Gets all DataSource objects
        /// </summary>
        public virtual List<OpDataSource> GetAllDataSource()
        {
            try
            {
                if (DataSourceCacheEnabled) //cache enabled
                {
                    if (!DataSourceIsFullCache)
                    {
                        List<OpDataSource> objectList = OpDataSource.ConvertList(dbConnectionDw.GetDataSource(), this);
                        if (objectList != null)
                        {
                            DataSourceDict = objectList.ToDictionary(o => o.IdDataSource);
                            DataSourceIsFullCache = true;
                        }
                    }
                    return new List<OpDataSource>(DataSourceDict.Values);
                }
                else //cache disabled
                {
                    return OpDataSource.ConvertList(dbConnectionDw.GetDataSource(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSource", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Id">DataSource Id</param>
        /// <returns>DataSource object</returns>
        public virtual OpDataSource GetDataSource(long Id)
        {
            return GetDataSource(Id, false);
        }

        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Ids">DataSource Ids</param>
        /// <returns>DataSource list</returns>
        public virtual List<OpDataSource> GetDataSource(long[] Ids)
        {
            return GetDataSource(Ids, false);
        }

        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Id">DataSource Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSource object</returns>
        public virtual OpDataSource GetDataSource(long Id, bool queryDatabase)
        {
            return GetDataSource(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataSource by id
        /// </summary>
        /// <param name="Ids">DataSource Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSource list</returns>
        public virtual List<OpDataSource> GetDataSource(long[] Ids, bool queryDatabase)
        {
            List<OpDataSource> returnList = new List<OpDataSource>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataSourceCacheEnabled && !queryDatabase) // cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDataSource DataSourceFoundInCache;
                        if (DataSourceDict.TryGetValue(Ids[i], out DataSourceFoundInCache)) //element founded in cache
                            returnList.Add(DataSourceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDataSource item in OpDataSource.ConvertList(dbConnectionDw.GetDataSource(idsNotInCache.ToArray()), this))
                            {
                                DataSourceDict[item.IdDataSource] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSource", ex);
                            throw ex;
                        }
                    }
                    return returnList;
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataSource.ConvertList(dbConnectionDw.GetDataSource(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSource", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DataSource object to the database
        /// </summary>
        /// <param name="toBeSaved">DataSource to save</param>
        /// <returns>DataSource Id</returns>
        public virtual long SaveDataSource(OpDataSource toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveDataSource(toBeSaved);
                if (DataSourceCacheEnabled)
                {
                    DataSourceDict[toBeSaved.IdDataSource] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDataSource", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the DataSource object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataSource to delete</param>
        public virtual void DeleteDataSource(OpDataSource toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDataSource(toBeDeleted);
                if (DataSourceCacheEnabled)
                {
                    DataSourceDict.Remove(toBeDeleted.IdDataSource);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDataSource", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the DataSource object cache
        /// </summary>
        public virtual void ClearDataSourceCache()
        {
            DataSourceDict.Clear();
        }
        #endregion

        #region DataSourceType
        /// <summary>
        /// Indicates whether the DataSourceTypeDict was filled trough GetAllDataSourceType or GetAllDataSourceTypeFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataSourceTypeIsFullCache { get; protected set; }

        /// <summary>
        /// DataSourceType objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpDataSourceType> DataSourceTypeDict = new Dictionary<int, OpDataSourceType>();

        /// <summary>
        /// Indicates whether the DataSourceType is cached.
        /// </summary>
        public virtual bool DataSourceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataSourceType objects
        /// </summary>
        public virtual List<OpDataSourceType> DataSourceType
        {
            get { return GetAllDataSourceType(); }
        }

        /// <summary>
        /// Gets all DataSourceType objects
        /// </summary>
        public virtual List<OpDataSourceType> GetAllDataSourceType(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (DataSourceTypeCacheEnabled) //cache enabled
                {
                    if (!DataSourceTypeIsFullCache)
                    {
                        List<OpDataSourceType> objectList = OpDataSourceType.ConvertList(dbConnectionDw.GetDataSourceType(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                        if (objectList != null)
                        {
                            DataSourceTypeDict = objectList.ToDictionary(o => o.IdDataSourceType);
                            DataSourceTypeIsFullCache = true;
                        }
                    }
                    return new List<OpDataSourceType>(DataSourceTypeDict.Values);
                }
                else //cache disabled
                {
                    return OpDataSourceType.ConvertList(dbConnectionDw.GetDataSourceType(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceType", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Id">DataSourceType Id</param>
        /// <returns>DataSourceType object</returns>
        public virtual OpDataSourceType GetDataSourceType(int Id)
        {
            return GetDataSourceType(Id, false);
        }

        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Ids">DataSourceType Ids</param>
        /// <returns>DataSourceType list</returns>
        public virtual List<OpDataSourceType> GetDataSourceType(int[] Ids)
        {
            return GetDataSourceType(Ids, false);
        }

        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Id">DataSourceType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSourceType object</returns>
        public virtual OpDataSourceType GetDataSourceType(int Id, bool queryDatabase)
        {
            return GetDataSourceType(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataSourceType by id
        /// </summary>
        /// <param name="Ids">DataSourceType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSourceType list</returns>
        public virtual List<OpDataSourceType> GetDataSourceType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDataSourceType> returnList = new List<OpDataSourceType>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataSourceTypeCacheEnabled && !queryDatabase) // cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDataSourceType DataSourceTypeFoundInCache;
                        if (DataSourceTypeDict.TryGetValue(Ids[i], out DataSourceTypeFoundInCache)) //element founded in cache
                            returnList.Add(DataSourceTypeFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDataSourceType item in OpDataSourceType.ConvertList(dbConnectionDw.GetDataSourceType(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                DataSourceTypeDict[item.IdDataSourceType] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceType", ex);
                            throw ex;
                        }
                    }
                    return returnList;
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataSourceType.ConvertList(dbConnectionDw.GetDataSourceType(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceType", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DataSourceType object to the database
        /// </summary>
        /// <param name="toBeSaved">DataSourceType to save</param>
        /// <returns>DataSourceType Id</returns>
        public virtual int SaveDataSourceType(OpDataSourceType toBeSaved)
        {
            try
            {
                int ret = dbConnectionDw.SaveDataSourceType(toBeSaved);
                if (DataSourceTypeCacheEnabled)
                {
                    DataSourceTypeDict[toBeSaved.IdDataSourceType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDataSourceType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the DataSourceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataSourceType to delete</param>
        public virtual void DeleteDataSourceType(OpDataSourceType toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDataSourceType(toBeDeleted);
                if (DataSourceTypeCacheEnabled)
                {
                    DataSourceTypeDict.Remove(toBeDeleted.IdDataSourceType);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDataSourceType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the DataSourceType object cache
        /// </summary>
        public virtual void ClearDataSourceTypeCache()
        {
            DataSourceTypeDict.Clear();
        }
        #endregion

        #region DataMissingAnalogReadouts
        /// <summary>
        /// Indicates whether the DataMissingAnalogReadoutsDict was filled trough GetAllDataMissingAnalogReadouts or GetAllDataMissingAnalogReadoutsFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataMissingAnalogReadoutsIsFullCache { get; protected set; }

        /// <summary>
        /// DataMissingAnalogReadouts objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDataMissingAnalogReadouts> DataMissingAnalogReadoutsDict = new Dictionary<long, OpDataMissingAnalogReadouts>();

        /// <summary>
        /// Indicates whether the DataMissingAnalogReadouts is cached.
        /// </summary>
        public bool DataMissingAnalogReadoutsCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataMissingAnalogReadouts objects
        /// </summary>
        public virtual List<OpDataMissingAnalogReadouts> DataMissingAnalogReadouts
        {
            get { return GetAllDataMissingAnalogReadouts(); }
        }

        /// <summary>
        /// Gets all DataMissingAnalogReadouts objects
        /// </summary>
        public virtual List<OpDataMissingAnalogReadouts> GetAllDataMissingAnalogReadouts()
        {
            try
            {
                if (DataMissingAnalogReadoutsCacheEnabled) //cache enabled
                {
                    if (!DataMissingAnalogReadoutsIsFullCache)
                    {
                        List<OpDataMissingAnalogReadouts> objectList = OpDataMissingAnalogReadouts.ConvertList(dbConnectionDw.GetDataMissingAnalogReadouts(), this);
                        if (objectList != null)
                        {
                            DataMissingAnalogReadoutsDict = objectList.ToDictionary(o => o.IdDataMissingAnalogReadouts);
                            DataMissingAnalogReadoutsIsFullCache = true;
                        }
                    }
                    return new List<OpDataMissingAnalogReadouts>(DataMissingAnalogReadoutsDict.Values);
                }
                else //cache disabled
                {
                    return OpDataMissingAnalogReadouts.ConvertList(dbConnectionDw.GetDataMissingAnalogReadouts(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataMissingAnalogReadouts", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Id">DataMissingAnalogReadouts Id</param>
        /// <returns>DataMissingAnalogReadouts object</returns>
        public virtual OpDataMissingAnalogReadouts GetDataMissingAnalogReadouts(long Id)
        {
            return GetDataMissingAnalogReadouts(Id, false);
        }

        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Ids">DataMissingAnalogReadouts Ids</param>
        /// <returns>DataMissingAnalogReadouts list</returns>
        public virtual List<OpDataMissingAnalogReadouts> GetDataMissingAnalogReadouts(long[] Ids)
        {
            return GetDataMissingAnalogReadouts(Ids, false);
        }

        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Id">DataMissingAnalogReadouts Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataMissingAnalogReadouts object</returns>
        public virtual OpDataMissingAnalogReadouts GetDataMissingAnalogReadouts(long Id, bool queryDatabase)
        {
            return GetDataMissingAnalogReadouts(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataMissingAnalogReadouts by id
        /// </summary>
        /// <param name="Ids">DataMissingAnalogReadouts Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataMissingAnalogReadouts list</returns>
        public virtual List<OpDataMissingAnalogReadouts> GetDataMissingAnalogReadouts(long[] Ids, bool queryDatabase)
        {
            List<OpDataMissingAnalogReadouts> returnList = new List<OpDataMissingAnalogReadouts>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataMissingAnalogReadoutsCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDataMissingAnalogReadouts DataMissingAnalogReadoutsFoundInCache;
                        if (DataMissingAnalogReadoutsDict.TryGetValue(Ids[i], out DataMissingAnalogReadoutsFoundInCache)) //element founded in cache
                            returnList.Add(DataMissingAnalogReadoutsFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDataMissingAnalogReadouts item in OpDataMissingAnalogReadouts.ConvertList(dbConnectionDw.GetDataMissingAnalogReadouts(idsNotInCache.ToArray()), this))
                            {
                                DataMissingAnalogReadoutsDict[item.IdDataMissingAnalogReadouts] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataMissingAnalogReadouts", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataMissingAnalogReadouts.ConvertList(dbConnectionDw.GetDataMissingAnalogReadouts(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataMissingAnalogReadouts", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DataMissingAnalogReadouts object to the database
        /// </summary>
        /// <param name="toBeSaved">DataMissingAnalogReadouts to save</param>
        /// <returns>DataMissingAnalogReadouts Id</returns>
        public virtual long SaveDataMissingAnalogReadouts(OpDataMissingAnalogReadouts toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveDataMissingAnalogReadouts(toBeSaved);
                if (DataMissingAnalogReadoutsCacheEnabled)
                {
                    DataMissingAnalogReadoutsDict[toBeSaved.IdDataMissingAnalogReadouts] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDataMissingAnalogReadouts", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DataMissingAnalogReadouts object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataMissingAnalogReadouts to delete</param>
        public virtual void DeleteDataMissingAnalogReadouts(OpDataMissingAnalogReadouts toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDataMissingAnalogReadouts(toBeDeleted);
                if (DataMissingAnalogReadoutsCacheEnabled)
                {
                    DataMissingAnalogReadoutsDict.Remove(toBeDeleted.IdDataMissingAnalogReadouts);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDataMissingAnalogReadouts", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DataMissingAnalogReadouts object cache
        /// </summary>
        public virtual void ClearDataMissingAnalogReadoutsCache()
        {
            DataMissingAnalogReadoutsDict.Clear();
            DataMissingAnalogReadoutsIsFullCache = false;
        }
        #endregion

        #region TemperatureCoefficient
        /// <summary>
        /// Indicates whether the TemperatureCoefficientDict was filled trough GetAllTemperatureCoefficient or GetAllTemperatureCoefficientFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool TemperatureCoefficientIsFullCache { get; protected set; }

        /// <summary>
        /// TemperatureCoefficient objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpTemperatureCoefficient> TemperatureCoefficientDict = new Dictionary<long, OpTemperatureCoefficient>();

        /// <summary>
        /// Indicates whether the TemperatureCoefficient is cached.
        /// </summary>
        public virtual bool TemperatureCoefficientCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TemperatureCoefficient objects
        /// </summary>
        public virtual List<OpTemperatureCoefficient> TemperatureCoefficient
        {
            get { return GetAllTemperatureCoefficient(); }
        }

        /// <summary>
        /// Gets all TemperatureCoefficient objects
        /// </summary>
        public virtual List<OpTemperatureCoefficient> GetAllTemperatureCoefficient()
        {
            try
            {
                if (TemperatureCoefficientCacheEnabled) //cache enabled
                {
                    if (!TemperatureCoefficientIsFullCache)
                    {
                        List<OpTemperatureCoefficient> objectList = OpTemperatureCoefficient.ConvertList(dbConnectionDw.GetTemperatureCoefficient(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            TemperatureCoefficientDict = objectList.ToDictionary(o => o.IdTemperatureCoefficient);
                            TemperatureCoefficientIsFullCache = true;
                        }
                    }
                    return new List<OpTemperatureCoefficient>(TemperatureCoefficientDict.Values);
                }
                else //cache disabled
                {
                    return OpTemperatureCoefficient.ConvertList(dbConnectionDw.GetTemperatureCoefficient(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTemperatureCoefficient", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Id">TemperatureCoefficient Id</param>
        /// <returns>TemperatureCoefficient object</returns>
        public virtual OpTemperatureCoefficient GetTemperatureCoefficient(long Id)
        {
            return GetTemperatureCoefficient(Id, false);
        }

        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Ids">TemperatureCoefficient Ids</param>
        /// <returns>TemperatureCoefficient list</returns>
        public virtual List<OpTemperatureCoefficient> GetTemperatureCoefficient(long[] Ids)
        {
            return GetTemperatureCoefficient(Ids, false);
        }

        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Id">TemperatureCoefficient Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TemperatureCoefficient object</returns>
        public virtual OpTemperatureCoefficient GetTemperatureCoefficient(long Id, bool queryDatabase)
        {
            return GetTemperatureCoefficient(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets TemperatureCoefficient by id
        /// </summary>
        /// <param name="Ids">TemperatureCoefficient Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TemperatureCoefficient list</returns>
        public virtual List<OpTemperatureCoefficient> GetTemperatureCoefficient(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTemperatureCoefficient> returnList = new List<OpTemperatureCoefficient>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TemperatureCoefficientCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTemperatureCoefficient TemperatureCoefficientFoundInCache;
                        if (TemperatureCoefficientDict.TryGetValue(Ids[i], out TemperatureCoefficientFoundInCache)) //element founded in cache
                            returnList.Add(TemperatureCoefficientFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTemperatureCoefficient item in OpTemperatureCoefficient.ConvertList(dbConnectionDw.GetTemperatureCoefficient(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                TemperatureCoefficientDict[item.IdTemperatureCoefficient] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTemperatureCoefficient", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTemperatureCoefficient.ConvertList(dbConnectionDw.GetTemperatureCoefficient(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTemperatureCoefficient", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the TemperatureCoefficient object to the database
        /// </summary>
        /// <param name="toBeSaved">TemperatureCoefficient to save</param>
        /// <returns>TemperatureCoefficient Id</returns>
        public virtual long SaveTemperatureCoefficient(OpTemperatureCoefficient toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveTemperatureCoefficient(toBeSaved);
                if (TemperatureCoefficientCacheEnabled)
                {
                    TemperatureCoefficientDict[toBeSaved.IdTemperatureCoefficient] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveTemperatureCoefficient", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the TemperatureCoefficient object from the database
        /// </summary>
        /// <param name="toBeDeleted">TemperatureCoefficient to delete</param>
        public virtual void DeleteTemperatureCoefficient(OpTemperatureCoefficient toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteTemperatureCoefficient(toBeDeleted);
                if (TemperatureCoefficientCacheEnabled)
                {
                    TemperatureCoefficientDict.Remove(toBeDeleted.IdTemperatureCoefficient);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelTemperatureCoefficient", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the TemperatureCoefficient object cache
        /// </summary>
        public virtual void ClearTemperatureCoefficientCache()
        {
            TemperatureCoefficientDict.Clear();
            TemperatureCoefficientIsFullCache = false;
        }
        #endregion

        #region TransmissionDriver
        /// <summary>
        /// Indicates whether the TransmissionDriverDict was filled trough GetAllTransmissionDriver or GetAllTransmissionDriverFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool TransmissionDriverIsFullCacheDW { get; protected set; }

        /// <summary>
        /// TransmissionDriver objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, OpTransmissionDriver> TransmissionDriverDictDW = new Dictionary<int, OpTransmissionDriver>();

        /// <summary>
        /// Indicates whether the TransmissionDriver is cached.
        /// </summary>
        public virtual bool TransmissionDriverCacheEnabledDW { get; set; }

        /// <summary>
        /// Gets all TransmissionDriver objects
        /// </summary>
        public virtual List<OpTransmissionDriver> TransmissionDriverDW
        {
            get { return GetAllTransmissionDriverDW(); }
        }

        /// <summary>
        /// Gets all TransmissionDriver objects
        /// </summary>
        public virtual List<OpTransmissionDriver> GetAllTransmissionDriverDW(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (TransmissionDriverCacheEnabled) //cache enabled
                {
                    if (!TransmissionDriverIsFullCache)
                    {
                        List<OpTransmissionDriver> objectList = OpTransmissionDriver.ConvertList(dbConnectionDw.GetTransmissionDriver(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                        if (objectList != null)
                        {
                            TransmissionDriverDictDW = objectList.ToDictionary(o => o.IdTransmissionDriver);
                            TransmissionDriverIsFullCache = true;
                        }
                    }
                    return new List<OpTransmissionDriver>(TransmissionDriverDictDW.Values);
                }
                else //cache disabled
                {
                    return OpTransmissionDriver.ConvertList(dbConnectionDw.GetTransmissionDriver(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriver", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <returns>TransmissionDriver object</returns>
        public virtual OpTransmissionDriver GetTransmissionDriverDW(int Id)
        {
            return GetTransmissionDriverDW(Id, false);
        }

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <returns>TransmissionDriver list</returns>
        public virtual List<OpTransmissionDriver> GetTransmissionDriverDW(int[] Ids)
        {
            return GetTransmissionDriverDW(Ids, false);
        }

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Id">TransmissionDriver Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver object</returns>
        public virtual OpTransmissionDriver GetTransmissionDriverDW(int Id, bool queryDatabase)
        {
            return GetTransmissionDriverDW(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets TransmissionDriver by id
        /// </summary>
        /// <param name="Ids">TransmissionDriver Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriver list</returns>
        public virtual List<OpTransmissionDriver> GetTransmissionDriverDW(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTransmissionDriver> returnList = new List<OpTransmissionDriver>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TransmissionDriverCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTransmissionDriver TransmissionDriverFoundInCache;
                        if (TransmissionDriverDictDW.TryGetValue(Ids[i], out TransmissionDriverFoundInCache)) //element founded in cache
                            returnList.Add(TransmissionDriverFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTransmissionDriver item in OpTransmissionDriver.ConvertList(dbConnectionDw.GetTransmissionDriver(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                TransmissionDriverDictDW[item.IdTransmissionDriver] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriver", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTransmissionDriver.ConvertList(dbConnectionDw.GetTransmissionDriver(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriver", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }
        #endregion

        #region TransmissionDriverPerformance
        /// <summary>
        /// Indicates whether the TransmissionDriverPerformanceDict was filled trough GetAllTransmissionDriverPerformance or GetAllTransmissionDriverPerformanceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool TransmissionDriverPerformanceIsFullCache { get; protected set; }

        /// <summary>
        /// TransmissionDriverPerformance objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpTransmissionDriverPerformance> TransmissionDriverPerformanceDict = new Dictionary<long, OpTransmissionDriverPerformance>();

        /// <summary>
        /// Indicates whether the TransmissionDriverPerformance is cached.
        /// </summary>
        public bool TransmissionDriverPerformanceCacheEnabled { get; set; }

        /// <summary>
        /// Gets all TransmissionDriverPerformance objects
        /// </summary>
        public virtual List<OpTransmissionDriverPerformance> TransmissionDriverPerformance
        {
            get { return GetAllTransmissionDriverPerformance(); }
        }

        /// <summary>
        /// Gets all TransmissionDriverPerformance objects
        /// </summary>
        public virtual List<OpTransmissionDriverPerformance> GetAllTransmissionDriverPerformance()
        {
            try
            {
                if (TransmissionDriverPerformanceCacheEnabled) //cache enabled
                {
                    if (!TransmissionDriverPerformanceIsFullCache)
                    {
                        List<OpTransmissionDriverPerformance> objectList = OpTransmissionDriverPerformance.ConvertList(dbConnectionDw.GetTransmissionDriverPerformance(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            TransmissionDriverPerformanceDict = objectList.ToDictionary(o => o.IdTransmissionDriverPerformance);
                            TransmissionDriverPerformanceIsFullCache = true;
                        }
                    }
                    return new List<OpTransmissionDriverPerformance>(TransmissionDriverPerformanceDict.Values);
                }
                else //cache disabled
                {
                    return OpTransmissionDriverPerformance.ConvertList(dbConnectionDw.GetTransmissionDriverPerformance(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverPerformance", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Id">TransmissionDriverPerformance Id</param>
        /// <returns>TransmissionDriverPerformance object</returns>
        public virtual OpTransmissionDriverPerformance GetTransmissionDriverPerformance(long Id)
        {
            return GetTransmissionDriverPerformance(Id, false);
        }

        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverPerformance Ids</param>
        /// <returns>TransmissionDriverPerformance list</returns>
        public virtual List<OpTransmissionDriverPerformance> GetTransmissionDriverPerformance(long[] Ids)
        {
            return GetTransmissionDriverPerformance(Ids, false);
        }

        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Id">TransmissionDriverPerformance Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverPerformance object</returns>
        public virtual OpTransmissionDriverPerformance GetTransmissionDriverPerformance(long Id, bool queryDatabase)
        {
            return GetTransmissionDriverPerformance(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets TransmissionDriverPerformance by id
        /// </summary>
        /// <param name="Ids">TransmissionDriverPerformance Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>TransmissionDriverPerformance list</returns>
        public virtual List<OpTransmissionDriverPerformance> GetTransmissionDriverPerformance(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpTransmissionDriverPerformance> returnList = new List<OpTransmissionDriverPerformance>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TransmissionDriverPerformanceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTransmissionDriverPerformance TransmissionDriverPerformanceFoundInCache;
                        if (TransmissionDriverPerformanceDict.TryGetValue(Ids[i], out TransmissionDriverPerformanceFoundInCache)) //element founded in cache
                            returnList.Add(TransmissionDriverPerformanceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTransmissionDriverPerformance item in OpTransmissionDriverPerformance.ConvertList(dbConnectionDw.GetTransmissionDriverPerformance(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                TransmissionDriverPerformanceDict[item.IdTransmissionDriverPerformance] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverPerformance", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTransmissionDriverPerformance.ConvertList(dbConnectionDw.GetTransmissionDriverPerformance(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionDriverPerformance", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the TransmissionDriverPerformance object to the database
        /// </summary>
        /// <param name="toBeSaved">TransmissionDriverPerformance to save</param>
        /// <returns>TransmissionDriverPerformance Id</returns>
        public virtual long SaveTransmissionDriverPerformance(OpTransmissionDriverPerformance toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveTransmissionDriverPerformance(toBeSaved, toBeSaved.DataType);
                if (TransmissionDriverPerformanceCacheEnabled)
                {
                    TransmissionDriverPerformanceDict[toBeSaved.IdTransmissionDriverPerformance] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveTransmissionDriverPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the TransmissionDriverPerformance object from the database
        /// </summary>
        /// <param name="toBeDeleted">TransmissionDriverPerformance to delete</param>
        public virtual void DeleteTransmissionDriverPerformance(OpTransmissionDriverPerformance toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteTransmissionDriverPerformance(toBeDeleted);
                if (TransmissionDriverPerformanceCacheEnabled)
                {
                    TransmissionDriverPerformanceDict.Remove(toBeDeleted.IdTransmissionDriverPerformance);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelTransmissionDriverPerformance", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the TransmissionDriverPerformance object cache
        /// </summary>
        public virtual void ClearTransmissionDriverPerformanceCache()
        {
            TransmissionDriverPerformanceDict.Clear();
            TransmissionDriverPerformanceIsFullCache = false;
        }
        #endregion

        #region PacketHourTransmissionDriver
        /// <summary>
        /// Indicates whether the PacketHourTransmissionDriverDict was filled trough GetAllPacketHourTransmissionDriver or GetAllPacketHourTransmissionDriverFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketHourTransmissionDriverIsFullCache { get; protected set; }

        /// <summary>
        /// PacketHourTransmissionDriver objects dictionary (Cache)
        /// </summary>
        public Dictionary<int?, OpPacketHourTransmissionDriver> PacketHourTransmissionDriverDict = new Dictionary<int?, OpPacketHourTransmissionDriver>();

        /// <summary>
        /// Indicates whether the PacketHourTransmissionDriver is cached.
        /// </summary>
        public bool PacketHourTransmissionDriverCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketHourTransmissionDriver object cache
        /// </summary>
        public virtual void ClearPacketHourTransmissionDriverCache()
        {
            PacketHourTransmissionDriverDict.Clear();
            PacketHourTransmissionDriverIsFullCache = false;
        }
        #endregion

        #region PacketHourAddress
        /// <summary>
        /// Indicates whether the PacketHourAddressDict was filled trough GetAllPacketHourAddress or GetAllPacketHourAddressFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketHourAddressIsFullCache { get; protected set; }

        /// <summary>
        /// PacketHourAddress objects dictionary (Cache)
        /// </summary>
        public Dictionary<string, OpPacketHourAddress> PacketHourAddressDict = new Dictionary<string, OpPacketHourAddress>();

        /// <summary>
        /// Indicates whether the PacketHourAddress is cached.
        /// </summary>
        public bool PacketHourAddressCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketHourAddress object cache
        /// </summary>
        public virtual void ClearPacketHourAddressCache()
        {
            PacketHourAddressDict.Clear();
            PacketHourAddressIsFullCache = false;
        }
        #endregion

        #region PacketHourDevice
        /// <summary>
        /// Indicates whether the PacketHourDeviceDict was filled trough GetAllPacketHourDevice or GetAllPacketHourDeviceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketHourDeviceIsFullCache { get; protected set; }

        /// <summary>
        /// PacketHourDevice objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpPacketHourDevice> PacketHourDeviceDict = new Dictionary<long, OpPacketHourDevice>();

        /// <summary>
        /// Indicates whether the PacketHourDevice is cached.
        /// </summary>
        public bool PacketHourDeviceCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketHourDevice object cache
        /// </summary>
        public virtual void ClearPacketHourDeviceCache()
        {
            PacketHourDeviceDict.Clear();
            PacketHourDeviceIsFullCache = false;
        }
        #endregion

        #region PacketTrashHourAddress
        /// <summary>
        /// Indicates whether the PacketTrashHourAddressDict was filled trough GetAllPacketTrashHourAddress or GetAllPacketTrashHourAddressFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketTrashHourAddressIsFullCache { get; protected set; }

        /// <summary>
        /// PacketTrashHourAddress objects dictionary (Cache)
        /// </summary>
        public Dictionary<string, OpPacketTrashHourAddress> PacketTrashHourAddressDict = new Dictionary<string, OpPacketTrashHourAddress>();

        /// <summary>
        /// Indicates whether the PacketTrashHourAddress is cached.
        /// </summary>
        public bool PacketTrashHourAddressCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketTrashHourAddress object cache
        /// </summary>
        public virtual void ClearPacketTrashHourAddressCache()
        {
            PacketTrashHourAddressDict.Clear();
            PacketTrashHourAddressIsFullCache = false;
        }
        #endregion

        #region PacketTrashHourDevice
        /// <summary>
        /// Indicates whether the PacketTrashHourDeviceDict was filled trough GetAllPacketTrashHourDevice or GetAllPacketTrashHourDeviceFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketTrashHourDeviceIsFullCache { get; protected set; }

        /// <summary>
        /// PacketTrashHourDevice objects dictionary (Cache)
        /// </summary>
        public Dictionary<long?, OpPacketTrashHourDevice> PacketTrashHourDeviceDict = new Dictionary<long?, OpPacketTrashHourDevice>();

        /// <summary>
        /// Indicates whether the PacketTrashHourDevice is cached.
        /// </summary>
        public bool PacketTrashHourDeviceCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketTrashHourDevice object cache
        /// </summary>
        public virtual void ClearPacketTrashHourDeviceCache()
        {
            PacketTrashHourDeviceDict.Clear();
            PacketTrashHourDeviceIsFullCache = false;
        }
        #endregion

        #region PacketTrashHourTransmissionDriver
        /// <summary>
        /// Indicates whether the PacketTrashHourTransmissionDriverDict was filled trough GetAllPacketTrashHourTransmissionDriver or GetAllPacketTrashHourTransmissionDriverFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool PacketTrashHourTransmissionDriverIsFullCache { get; protected set; }

        /// <summary>
        /// PacketTrashHourTransmissionDriver objects dictionary (Cache)
        /// </summary>
        public Dictionary<int?, OpPacketTrashHourTransmissionDriver> PacketTrashHourTransmissionDriverDict = new Dictionary<int?, OpPacketTrashHourTransmissionDriver>();

        /// <summary>
        /// Indicates whether the PacketTrashHourTransmissionDriver is cached.
        /// </summary>
        public bool PacketTrashHourTransmissionDriverCacheEnabled { get; set; }

        /// <summary>
        /// Clears the PacketTrashHourTransmissionDriver object cache
        /// </summary>
        public virtual void ClearPacketTrashHourTransmissionDriverCache()
        {
            PacketTrashHourTransmissionDriverDict.Clear();
            PacketTrashHourTransmissionDriverIsFullCache = false;
        }
        #endregion
    }
}
