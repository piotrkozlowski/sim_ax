﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;
using IMR.Suite.UI.Business.Objects.Custom;

namespace IMR.Suite.UI.Business
{
    public partial class DataProvider
    {
        #region InvalidateDataFromDataArch
        public virtual bool InvalidateDataFromDataArch(long[] IdDataArch, int? IdImrServer = null, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return dbCollectorClient.InvalidateDataFromDataArch(ref dbCollectorSession, IdDataArch, IdImrServer);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionDw.InvalidateDataFromDataArch(IdDataArch);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "sp_InvalidateDataFromDataArch", ex);
                throw ex;
            }
        }
        #endregion

        #region ActionDataSource
        #region GetActionDataSourceFilter
        /// <summary>
        /// Gets ActionDataSource list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionDataSource list</returns>
        public virtual List<OpActionDataSource> GetActionDataSourceFilter(bool loadNavigationProperties = true, long[] IdAction = null, long[] IdDataSource = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionDataSourceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionDataSourceFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpActionDataSource> returnList = OpActionDataSource.ConvertList(dbCollectorClient.GetActionDataSourceFilter(ref dbCollectorSession, IdAction, IdDataSource,
                        topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionDataSource> returnList = OpActionDataSource.ConvertList(dbConnectionDw.GetActionDataSourceFilter(IdAction, IdDataSource, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else
                    return new List<OpActionDataSource>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionDataSourceFilter", ex);
                throw;
            }

        }
        #endregion
        #endregion

        #region CalibrationSession

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        public virtual List<OpCalibrationSession> GetAllCalibrationSessions()
        {
            try
            {
                return OpCalibrationSession.ConvertList(dbConnectionDw.GetAllCalibrationSessions());
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_GetAllCalibrationSession", ex);
                throw;
            }
        }

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        public virtual List<OpCalibrationSession> GetCalibrationSessions(int[] IdSessions)
        {
            try
            {
                return OpCalibrationSession.ConvertList(dbConnectionDw.GetCalibrationSessions(IdSessions));
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_GetCalibrationSession", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the Action object from the database
        /// </summary>
        /// <param name="toBeDeleted">Action to delete</param>
        public virtual void DeleteCalibrationSession(OpCalibrationSession toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteCalibrationSession(toBeDeleted);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_DeleteCalibrationSession", ex);
                throw;
            }
        }

        public virtual void DeleteCalibrationSessions(List<OpCalibrationSession> toBeDeleted)
        {
            try
            {
                foreach (var item in toBeDeleted)
                {
                    DeleteCalibrationSession(item);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_DeleteSession", ex);
                throw;
            }
        }
        #endregion

        #region CalibrationModules

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        public virtual List<OpCalibrationModule> GetAllCalibrationModules()
        {
            try
            {

                return OpCalibrationModule.ConvertList(dbConnectionDw.GetAllCalibrationModules());

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_GetAllCalibrationModules", ex);
                throw;
            }
        }
        
        public virtual int SaveCalibrationSession(IMR.Suite.UI.Business.Objects.DW.OpCalibrationSession Session)
        {
            try
            {

                return dbConnectionDw.SaveCalibrationSession(Session);

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_SaveCalibrationSession", ex);
                throw;
            }
        }

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        public virtual List<OpCalibrationModule> GetCalibrationModules(int[] IdModules)
        {
            try
            {
                return OpCalibrationModule.ConvertList(dbConnectionDw.GetCalibrationModules(IdModules));
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_GetCalibrationModules", ex);
                throw;
            }
        }
        #endregion

        #region CALIBRATION_TEST

        /// <summary>
        /// Gets all Calibration Modules objects
        /// </summary>
        public virtual List<OpCalibrationTest> GetCalibrationTests(long[] IdTests)
        {
            try
            {
                return OpCalibrationTest.ConvertList(dbConnectionDw.GetCalibrationTests(IdTests));
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_GetCalibrationTests", ex);
                throw;
            }
        }

        public virtual long? SaveCalibrationTest(OpCalibrationTest toBeSaved)
        {
            try
            {
                if (toBeSaved != null)
                {
                    return dbConnectionDw.SaveCalibrationTest(toBeSaved);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "CALIBRATION_DATA", ex);
                throw;
            }

            return null;
        }

        #endregion

        #region CalibrationData
        public virtual void SaveCalibrationData(OpCalibrationData[] toBeSaved, int commandTimeout = 0)
        {
            try
            {
                if (toBeSaved != null && toBeSaved.Count() > 0)
                {
                    dbConnectionDw.SaveCalibrationData(toBeSaved, commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "CALIBRATION_DATA", ex);
                throw;
            }
        }

        public virtual List<OpCalibrationData> GetCalibrationData(int IdCalibrationSession)
        {
            try
            {

                return OpCalibrationData.ConvertList(dbConnectionDw.GetCalibrationData(IdCalibrationSession)).ToList();

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_GetCalibrationData", ex);
                throw;
            }
        }

        public virtual void DeleteCalibrationDataSession(int toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteCalibrationDataSession(toBeDeleted);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "awsr_DeleteCalibrationDataSession", ex);
                throw;
            }
        }

        #endregion

        #region CheckIfTableExists
        public virtual bool CheckIfTableExistsDW(string name)
        {
            try
            {
                return dbConnectionDw.CheckIfTableExists(name);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_CheckIfTableExists", ex);
                throw ex;
            }
        }
        #endregion

        #region DataArch
        #region GetDataArchFilterAggregated
        /// <summary>
        /// Gets DataArch list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataArch funcion</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="AggregationType">Specifies aggregation type criteria</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataArch list</returns>
        public virtual List<OpDataArch> GetDataArchFilterAggregated(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                             TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, long[] IdAlarmEvent = null, int[] Status = null, int AggregationType = 0,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataArchFilterAggregated ");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilterAggregated "));
            }

            try
            {
                List<OpDataArch> returnList = OpDataArch.ConvertList(dbConnectionDw.GetDataArchFilterAggregated(IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                             Time, IdDataSource, IdDataSourceType, IdAlarmEvent, Status, AggregationType,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataArch item in returnList)
                        DataArchDict[item.IdDataArch] = item;
                    DataArchIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDataArchFilterAggregated", ex);
                throw;
            }

        }
        #endregion
        #region GetDataArchFilter

        /// <summary>
        /// Gets DataArch list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataArch funcion</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DataArch list</returns>
        public virtual List<OpDataArch> GetDataArchFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, long[] IdAlarmEvent = null, int[] Status = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false, bool loadCustomData = true)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataArchFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpDataArch> returnList = OpDataArch.ConvertList(dbCollectorClient.GetDataArchFilterDW(ref dbCollectorSession, IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                        Time, IdDataSource, IdDataSourceType, IdAlarmEvent, Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);

                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDataArch> returnList = OpDataArch.ConvertList(dbConnectionDw.GetDataArchFilter(IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                                 Time, IdDataSource, IdDataSourceType, IdAlarmEvent, Status,
                                 topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpDataArch item in returnList)
                            DataArchDict[item.IdDataArch] = item;
                        DataArchIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpDataArch>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArchFilter", ex);
                throw;
            }

        }
        #endregion
        #region SaveDataArch
        /// <summary>
        /// Saves the DataArch object to the database
        /// </summary>
        /// <param name="toBeSaved">DataArch to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>DataArch Id</returns>
        public virtual long SaveDataArch(OpDataArch toBeSaved, bool useDBCollector = false)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);

                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    Data.DB.DW.DB_DATA_ARCH db_dArch = new Data.DB.DW.DB_DATA_ARCH(toBeSaved);
                    Data.DB.DB_DATA_TYPE db_dArchDataType = new DB_DATA_TYPE(toBeSaved.DataType);
                    long ret = dbCollectorClient.SaveDataArchDW(ref dbCollectorSession, db_dArch, db_dArchDataType);
                    if (ret == 0)
                        return toBeSaved.IdDataArch;
                    return ret;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionDw.SaveDataArch(toBeSaved, toBeSaved.DataType);
                    if (DataArchCacheEnabled)
                    {
                        DataArchDict[toBeSaved.IdDataArch] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                else
                    return toBeSaved.IdDataArch;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDataArch", ex);
                throw;
            }
        }
        public virtual void SaveDataArch(OpDataArch[] toBeSaved, int commandTimeout = 0, bool useDBCollector = false)
        {
            try
            {
                if (toBeSaved != null && toBeSaved.Count() > 0)
                {
                    //if (useDBCollector && dbCollectorClient != null && dbCollectorSession != null && dbCollectorClient.State == System.ServiceModel.CommunicationState.Opened)
                    //{
                    //    Dictionary<long, IMR.Suite.UI.Business.Objects.CORE.OpDataType> dtDict = this.GetDataType(toBeSaved.Select(d => d.IdDataType).Distinct().ToArray()).ToDictionary(d => d.IdDataType);
                    //    foreach (OpDataArch dtItem in toBeSaved)
                    //        dtItem.DataType = dtDict[dtItem.IdDataType];

                    //    List<Data.DB.DW.DB_DATA_ARCH> dbDataArch = new List<Data.DB.DW.DB_DATA_ARCH>();
                    //    List<Data.DB.DB_DATA_TYPE> dbDataType = new List<Data.DB.DB_DATA_TYPE>();

                    //    foreach (OpDataArch dArch in toBeSaved)
                    //    {
                    //        Data.DB.DW.DB_DATA_ARCH db_dArch = new Data.DB.DW.DB_DATA_ARCH(dArch);
                    //        Data.DB.DB_DATA_TYPE db_dArchDataType = new DB_DATA_TYPE(dArch.DataType);
                    //        dbDataArch.Add(db_dArch);
                    //        dbDataType.Add(db_dArchDataType);
                    //    }
                    //    toBeSaved = OpDataArch.ConvertList(dbCollectorClient.SaveDataArchArrayDW(ref dbCollectorSession, dbDataArch.ToArray(), dbDataType.ToArray()), this, false).ToArray();
                    //}
                    //else
                    {
                        Dictionary<long, IMR.Suite.UI.Business.Objects.CORE.OpDataType> dtDict = this.GetDataType(toBeSaved.Select(d => d.IdDataType).Distinct().ToArray()).ToDictionary(d => d.IdDataType);
                        foreach (OpDataArch dtItem in toBeSaved)
                            dtItem.DataType = dtDict[dtItem.IdDataType];

                        dbConnectionDw.SaveDataArch(toBeSaved, dtDict.Values.ToArray(), commandTimeout);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "DATA_ARCH", ex);
                throw;
            }
        }
        #endregion
        #region DeleteDataArchFilter
        /// <summary>
        /// Deletes the DataArch objects from database
        /// </summary>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        public virtual void DeleteDataArchFilter(long[] IdDataArch = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, long[] IdAlarmEvent = null, int[] Status = null,
                            string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "DeleteDataArchFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataArchFilter"));
            }

            try
            {
                dbConnectionDw.DeleteDataArchFilter(IdDataArch, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr, Time, IdDataSource, IdDataSourceType, IdAlarmEvent, Status, customWhereClause, autoTransaction, transactionLevel, commandTimeout);

                if (DataArchCacheEnabled)
                {
                    List<OpDataArch> toRemoveList = DataArchDict.Select(d => d.Value).ToList();
                    if (IdDataArch != null)
                        toRemoveList = toRemoveList.Join(IdDataArch, o => o.IdDataArch, i => i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && SerialNbr != null)
                        toRemoveList = toRemoveList.Join(SerialNbr, o => o.SerialNbr, i => (long?)i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && IdMeter != null)
                        toRemoveList = toRemoveList.Join(IdMeter, o => o.IdMeter, i => (long?)i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && IdLocation != null)
                        toRemoveList = toRemoveList.Join(IdLocation, o => o.IdLocation, i => (long?)i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && IdDataType != null)
                        toRemoveList = toRemoveList.Join(IdDataType, o => o.IdDataType, i => i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && IndexNbr != null)
                        toRemoveList = toRemoveList.Join(IndexNbr, o => o.IndexNbr, i => i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && IdDataSource != null)
                        toRemoveList = toRemoveList.Join(IdDataSource, o => o.IdDataSource, i => (long?)i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && IdDataSourceType != null)
                        toRemoveList = toRemoveList.Join(IdDataSourceType, o => o.IdDataSourceType, i => (int?)i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && IdAlarmEvent != null)
                        toRemoveList = toRemoveList.Join(IdAlarmEvent, o => o.IdAlarmEvent, i => (long?)i, (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && Status != null)
                        toRemoveList = toRemoveList.Join(Status, o => o.STATUS, i => Convert.ToInt64(i), (o, i) => o).ToList();
                    if (toRemoveList.Count > 0 && Time != null)
                    {
                        if (Time.Mode == TypeDateTimeCode.ModeType.Between && Time.DateTime1 != null && Time.DateTime2 != null)
                            toRemoveList = toRemoveList.Where(d => d.Time >= Time.DateTime1 && d.Time <= Time.DateTime2).ToList();
                        else if (Time.Mode == TypeDateTimeCode.ModeType.Different && Time.DateTime1 != null)
                            toRemoveList = toRemoveList.Where(d => d.Time != Time.DateTime1).ToList();
                        else if (Time.Mode == TypeDateTimeCode.ModeType.Equal && Time.DateTime1 != null)
                            toRemoveList = toRemoveList.Where(d => d.Time == Time.DateTime1).ToList();
                        else if (Time.Mode == TypeDateTimeCode.ModeType.Lesser && Time.DateTime1 != null)
                            toRemoveList = toRemoveList.Where(d => d.Time < Time.DateTime1).ToList();
                        else if (Time.Mode == TypeDateTimeCode.ModeType.LesserEqual && Time.DateTime1 != null)
                            toRemoveList = toRemoveList.Where(d => d.Time <= Time.DateTime1).ToList();
                        else if (Time.Mode == TypeDateTimeCode.ModeType.GreaterEqual && Time.DateTime1 != null)
                            toRemoveList = toRemoveList.Where(d => d.Time >= Time.DateTime1).ToList();
                        else if (Time.Mode == TypeDateTimeCode.ModeType.Greater && Time.DateTime1 != null)
                            toRemoveList = toRemoveList.Where(d => d.Time > Time.DateTime1).ToList();
                    }

                    if (toRemoveList.Count > 0)
                        toRemoveList.ForEach(d => DataArchDict.Remove(d.IdDataArch));
                }

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DeleteDataArchFilter", ex);
                throw;
            }
        }
        #endregion
        #endregion

        #region EtlPerformance

        public virtual void DeleteEtlPerformance(int[] IdEtl = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteEtlPerformance"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdEtl == null || IdEtl.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");
            dbConnectionDw.DeleteEtlPerformance(IdEtl, IdDataType, IdAggregationType, customWhereClause, commandTimeout);
        }

        #endregion

        #region GetCurrentDataForDistributor
        /// <summary>
        /// Gets current data for distributor's devices (warning - all devices from specified DEVICE filter!)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdDistributor">Specifies filter for DEVICE.ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceType">Specifies filter for DEVICE.ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceStateType">Specifies filter for DEVICE.ID_DEVICE_STATE_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns></returns>
        public virtual List<OpData> GetCurrentDataForDistributor(bool loadNavigationProperties = true, int[] IdDistributor = null, int[] IdDeviceType = null, int[] IdDeviceStateType = null, long[] IdDataType = null, int[] IndexNbr = null, bool useDBCollector = false)
        {
            try
            {
                //if (useDBCollector && dbCollectorClient != null && dbCollectorSession != null && dbCollectorClient.State == System.ServiceModel.CommunicationState.Opened)
                //{
                //    // flaga loadNavigationProperties na false, bo NavigationProperties beda pobierane z podlaczonej bazy, a dane z DBCollectora mogly pochodzic z innego serwera
                //    List<OpData> returnList = OpData.ConvertList(dbCollectorClient.GetCurrentDataForDistributorDW(ref dbCollectorSession, IdDistributor, IdDeviceType, IdDeviceStateType, 
                //        IdDataType, IndexNbr), this, false);

                //    return returnList;
                //}
                //else
                {
                    List<OpData> returnList = OpData.ConvertList(dbConnectionDw.GetCurrentDataForDistributor(IdDistributor, IdDeviceType, IdDeviceStateType,
                        IdDataType, IndexNbr), this, loadNavigationProperties);

                    return returnList;
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetCurrentDataForDistributor", ex);
                throw;
            }
        }
        #endregion

        #region DistributorPerformance

        public virtual void DeleteDistributorPerformance(int[] IdDistributor = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdDistributor == null || IdDistributor.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");
            dbConnectionDw.DeleteDistributorPerformance(IdDistributor, IdDataType, IdAggregationType, customWhereClause, commandTimeout);
        }

        #endregion

        #region ImrServerPerformance

        public virtual void DeleteImrServerPerformance(int[] IdImrServer = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdImrServer == null || IdImrServer.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");
            dbConnectionDw.DeleteImrServerPerformance(IdImrServer, IdDataType, IdAggregationType, customWhereClause, commandTimeout);
        }

        #endregion

        #region GetDataFilterDW
        /// <summary>
        /// Gets Data list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdData">Specifies filter for ID_DATA column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>Data list</returns>
        public virtual List<OpData> GetDataFilterDW(bool loadNavigationProperties = true, long[] IdData = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, int[] Status = null, long topCount = 0, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpData> returnList = OpData.ConvertList(dbCollectorClient.GetDataFilterDW(ref dbCollectorSession, IdData, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                        Time, IdDataSource, IdDataSourceType, Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpData> returnList = OpData.ConvertList(dbConnectionDw.GetDataFilter(IdData, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                                 Time, IdDataSource, IdDataSourceType, Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else
                    return new List<OpData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFilter", ex);
                throw;
            }

        }
        #endregion

        #region GetDataFilterDW
        /// <summary>
        /// Gets Data list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdData">Specifies filter for ID_DATA column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataSourceType">Specifies filter for ID_DATA_SOURCE_TYPE column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Data list</returns>
        public virtual List<OpDataDw> GetDataDwFilterDW(bool loadNavigationProperties = true, long[] IdData = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            TypeDateTimeCode Time = null, long[] IdDataSource = null, int[] IdDataSourceType = null, int[] Status = null, long topCount = 0, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataDwFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));
            }

            try
            {
                List<OpDataDw> returnList = OpDataDw.ConvertList(dbConnectionDw.GetDataDwFilter(IdData, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                                 Time, IdDataSource, IdDataSourceType, Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFilter", ex);
                throw;
            }

        }
        #endregion

        #region SaveData
        /// <summary>
        /// Saves the Data object to the database
        /// </summary>
        /// <param name="toBeSaved">Data to save</param>
        /// <returns>Data Id</returns>
        public virtual long SaveData(OpData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveData(toBeSaved, toBeSaved.DataType);
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveData", ex);
                throw;
            }
        }
        #endregion
        #region DeleteData
        /// <summary>
        /// Deletes the Data object from the database
        /// </summary>
        /// <param name="toBeDeleted">Data to delete</param>
        public virtual void DeleteData(List<OpData> toBeDeleted)
        {
            try
            {
                foreach (OpData item in toBeDeleted)
                {
                    dbConnectionDw.DeleteData(item);
                    if (DataCacheEnabled)
                    {
                        DataDict.Remove(item.IdData);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelData", ex);
                throw;
            }
        }
        #endregion
        #region SaveDataSource
        /// <summary>
        /// 
        /// </summary>
        /// <param name="idOperator"></param>
        /// <param name="IdImrServer"></param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns></returns>
        public virtual long SaveDataSource(int idOperator, int? IdImrServer = null, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return dbCollectorClient.SaveDataSource(ref dbCollectorSession, idOperator, IdImrServer);
                }
                else
                {
                    return dbConnectionDw.SaveDataSource(idOperator);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDataSource", ex);
                throw ex;
            }
        }
        #endregion
        #region ReportDataType
        /// <summary>
        /// Indicates whether the ReportDataTypeDict was filled trough GetAllReportDataType or GetAllReportDataTypeFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ReportDataTypeIsFullCache { get; protected set; }

        /// <summary>
        /// ReportDataType objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, List<OpReportDataType>> ReportDataTypeDict = new Dictionary<long, List<OpReportDataType>>();

        /// <summary>
        /// Indicates whether the ReportDataType is cached.
        /// </summary>
        public bool ReportDataTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ReportDataType objects
        /// </summary>
        public virtual List<OpReportDataType> ReportDataType
        {
            get { return GetAllReportDataType(); }
        }

        /// <summary>
        /// Gets all ReportDataType objects
        /// </summary>
        public virtual List<OpReportDataType> GetAllReportDataType()
        {
            try
            {
                if (ReportDataTypeCacheEnabled) //cache enabled
                {
                    if (!ReportDataTypeIsFullCache)
                    {
                        List<OpReportDataType> objectList = OpReportDataType.ConvertList(dbConnectionDw.GetReportDataType(), this);
                        if (objectList != null)
                        {
                            foreach (var item in objectList)
                            {
                                if (!ReportDataTypeDict.ContainsKey(item.IdReportDataType))
                                    ReportDataTypeDict.Add(item.IdReportDataType, new List<OpReportDataType>());

                                ReportDataTypeDict[item.IdReportDataType].Add(item);
                            }
                            ReportDataTypeIsFullCache = true;
                        }
                    }
                    return ReportDataTypeDict.Values.SelectMany(d => d).ToList();
                }
                else //cache disabled
                {
                    return OpReportDataType.ConvertList(dbConnectionDw.GetReportDataType(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportDataType", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Id">ReportDataType Id</param>
        /// <returns>ReportDataType object</returns>
        public virtual OpReportDataType GetReportDataType(long Id)
        {
            return GetReportDataType(Id, false);
        }

        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Ids">ReportDataType Ids</param>
        /// <returns>ReportDataType list</returns>
        public virtual List<OpReportDataType> GetReportDataType(long[] Ids)
        {
            return GetReportDataType(Ids, false);
        }

        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Id">ReportDataType Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportDataType object</returns>
        public virtual OpReportDataType GetReportDataType(long Id, bool queryDatabase)
        {
            return GetReportDataType(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ReportDataType by id
        /// </summary>
        /// <param name="Ids">ReportDataType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ReportDataType list</returns>
        public virtual List<OpReportDataType> GetReportDataType(long[] Ids, bool queryDatabase)
        {
            List<OpReportDataType> returnList = new List<OpReportDataType>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ReportDataTypeCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpReportDataType> ReportDataTypeFoundInCache;
                        if (ReportDataTypeDict.TryGetValue(Ids[i], out ReportDataTypeFoundInCache)) //element founded in cache
                            returnList.AddRange(ReportDataTypeFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpReportDataType> objectList = OpReportDataType.ConvertList(dbConnectionDw.GetReportDataType(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                foreach (var item in objectList)
                                {
                                    if (!ReportDataTypeDict.ContainsKey(item.IdReportDataType))
                                        ReportDataTypeDict.Add(item.IdReportDataType, new List<OpReportDataType>());

                                    ReportDataTypeDict[item.IdReportDataType].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportDataType", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpReportDataType.ConvertList(dbConnectionDw.GetReportDataType(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReportDataType", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ReportDataType object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportDataType to save</param>
        /// <returns>ReportDataType Id</returns>
        public virtual long SaveReportDataType(OpReportDataType toBeSaved)
        {
            try
            {
                long ret = dbConnectionDw.SaveReportDataType(toBeSaved);
                if (ReportDataTypeCacheEnabled)
                {
                    if (ReportDataTypeDict.ContainsKey(toBeSaved.IdReportDataType))
                    {
                        if (ReportDataTypeDict[toBeSaved.IdReportDataType].Find(f => f == toBeSaved) == null)
                            ReportDataTypeDict[toBeSaved.IdReportDataType].Add(toBeSaved); //add element
                    }
                    else
                    {
                        ReportDataTypeDict.Add(toBeSaved.IdReportDataType, new List<OpReportDataType>());
                        ReportDataTypeDict[toBeSaved.IdReportDataType].Add(toBeSaved); //add element
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveReportDataType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the ReportDataType object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportDataType to delete</param>
        public virtual void DeleteReportDataType(OpReportDataType toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteReportDataType(toBeDeleted);
                if (ReportDataTypeCacheEnabled)
                {
                    ReportDataTypeDict.Remove(toBeDeleted.IdReportDataType);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelReportDataType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the ReportDataType object cache
        /// </summary>
        public virtual void ClearReportDataTypeCache()
        {
            ReportDataTypeDict.Clear();
            ReportDataTypeIsFullCache = false;
        }
        #endregion

        #region DataTemporal

        public virtual void DeleteDataTemporal(long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause) && (SerialNbr == null || SerialNbr.Count() == 0)
                && (IdMeter == null || IdMeter.Count() == 0)
                && (IdLocation == null || IdLocation.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");
            dbConnectionDw.DeleteDataTemporal(SerialNbr, IdMeter, IdLocation, IdDataType, IdAggregationType, customWhereClause, commandTimeout);
        }


        public virtual void SaveDataTemporal(OpDataTemporal[] toBeSaved, int commandTimeout = 0)
        {
            try
            {
                if (toBeSaved != null && toBeSaved.Count() > 0)
                {
                    Dictionary<long, IMR.Suite.UI.Business.Objects.CORE.OpDataType> dtDict = this.GetDataType(toBeSaved.Select(d => d.IdDataType).Distinct().ToArray()).ToDictionary(d => d.IdDataType);
                    foreach (OpDataTemporal dtItem in toBeSaved)
                        dtItem.DataType = dtDict[dtItem.IdDataType];

                    dbConnectionDw.SaveDataTemporal(toBeSaved, dtDict.Values.ToArray(), commandTimeout);

                    if (DataTemporalCacheEnabled)
                    {
                        foreach (OpDataTemporal dtItem in toBeSaved)
                            DataTemporalDict[dtItem.IdDataTemporal] = dtItem; //add or update element
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "DATA_TEMPORAL", ex);
                throw;
            }
        }

        public virtual List<OpDataTemporal> GetDataTemporalDevicePerformanceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false,
                            long[] IdDataTemporal = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null,
                            int[] IdAggregationType = null, int[] IdDataSourceType = null,
                            int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTemporalDevicePerformanceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTemporalFilter"));
            }

            try
            {
                List<OpDataTemporal> returnList = OpDataTemporal.ConvertList(dbConnectionDw.GetDataTemporalDevicePerformanceFilter(IdDataTemporal, SerialNbr, IdDataType, IndexNbr,
                             IdAggregationType, IdDataSourceType,
                             Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);

                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDataTemporalDevicePerformanceFilter", ex);
                throw;
            }
        }

        public virtual List<OpDataTemporal> GetDataTemporalByDistributor(bool loadNavigationProperties = true,
                            Enums.ReferenceType FilterOption = Enums.ReferenceType.None, int[] IdDistributor = null, long[] IdDataType = null, int[] IdAggregationType = null,
                            string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "imrse_u_GetDataTemporalByDistributor");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "imrse_u_GetDataTemporalByDistributor"));
            }
            if (FilterOption != Enums.ReferenceType.IdLocation && FilterOption != Enums.ReferenceType.IdMeter && FilterOption != Enums.ReferenceType.SerialNbr)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDataTemporalByDistributor", String.Format("Not supported filter option {0}", FilterOption.ToString()));
                throw new ApplicationException((String.Format("Not supported filter option {0} during execution {1}", FilterOption.ToString(), "imrse_u_GetDataTemporalByDistributor")));
            }

            try
            {
                List<OpDataTemporal> returnList = OpDataTemporal.ConvertList(dbConnectionDw.GetDataTemporalByDistributor(FilterOption, IdDistributor, IdDataType, IdAggregationType,
                             customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);

                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDataTemporalByDistributor", ex);
                throw;
            }
        }

        #endregion

        #region DataSourceData
        ///// <summary>
        ///// Indicates whether the DataSourceDataDict was filled trough GetAllDataSourceData or GetAllDataSourceDataFilter with mergeIntoCache = true
        ///// </summary>
        //public virtual bool DataSourceDataIsFullCache { get; protected set; }

        ///// <summary>
        ///// DataSourceData objects dictionary (Cache)
        ///// </summary>
        //public virtual Dictionary<long, OpDataSourceData> DataSourceDataDict = new Dictionary<long, OpDataSourceData>();

        ///// <summary>
        ///// Indicates whether the DataSourceData is cached.
        ///// </summary>
        //public virtual bool DataSourceDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataSourceData objects
        /// </summary>
        public virtual List<OpDataSourceData> DataSourceData
        {
            get { return GetAllDataSourceData(); }
        }

        /// <summary>
        /// Gets all DataSourceData objects
        /// </summary>
        public virtual List<OpDataSourceData> GetAllDataSourceData()
        {
            try
            {
                //if (DataSourceDataCacheEnabled) //cache enabled
                //{
                //    if (!DataSourceDataIsFullCache)
                //    {
                //        List<OpDataSourceData> objectList = OpDataSourceData.ConvertList(dbConnectionDw.GetDataSourceData(), this);
                //        if (objectList != null)
                //        {
                //            DataSourceDataDict = objectList.ToDictionary(o => o.IdDataSource);
                //            DataSourceDataIsFullCache = true;
                //        }
                //    }
                //    return new List<OpDataSourceData>(DataSourceDataDict.Values);
                //}
                //else //cache disabled
                {
                    return OpDataSourceData.ConvertList(dbConnectionDw.GetDataSourceData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceData", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets DataSourceData by id
        /// </summary>
        /// <param name="Id">DataSourceData Id</param>
        /// <returns>DataSourceData object</returns>
        public virtual OpDataSourceData GetDataSourceData(long Id)
        {
            //return GetDataSourceData(Id, false);
            return GetDataSourceData(new long[] { Id }).FirstOrDefault();
        }

        ///// <summary>
        ///// Gets DataSourceData by id
        ///// </summary>
        ///// <param name="Ids">DataSourceData Ids</param>
        ///// <returns>DataSourceData list</returns>
        //public virtual List<OpDataSourceData> GetDataSourceData(long[] Ids)
        //{
        //    return GetDataSourceData(Ids, false);
        //}

        ///// <summary>
        ///// Gets DataSourceData by id
        ///// </summary>
        ///// <param name="Id">DataSourceData Id</param>
        ///// <param name="queryDatabase">If True, gets object from database</param>
        ///// <returns>DataSourceData object</returns>
        //public virtual OpDataSourceData GetDataSourceData(long Id, bool queryDatabase)
        //{
        //    return GetDataSourceData(new long[] { Id }, queryDatabase).FirstOrDefault();
        //}

        /// <summary>
        /// Gets DataSourceData by id
        /// </summary>
        /// <param name="Ids">DataSourceData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataSourceData list</returns>
        public virtual List<OpDataSourceData> GetDataSourceData(long[] Ids)//, bool queryDatabase)
        {
            List<OpDataSourceData> returnList = new List<OpDataSourceData>();
            if (Ids != null && Ids.Length > 0)
            {
                //if (DataSourceDataCacheEnabled && !queryDatabase) // cache enabled and query database is false
                //{

                //    List<long> idsNotInCache = new List<long>();
                //    for (int i = 0; i < Ids.Length; i++)
                //    {
                //        OpDataSourceData DataSourceDataFoundInCache;
                //        if (DataSourceDataDict.TryGetValue(Ids[i], out DataSourceDataFoundInCache)) //element founded in cache
                //            returnList.Add(DataSourceDataFoundInCache);
                //        else //element not found in cache, query database
                //            idsNotInCache.Add(Ids[i]);
                //    }
                //    if (idsNotInCache.Count > 0)
                //    {
                //        try
                //        {
                //            foreach (OpDataSourceData item in OpDataSourceData.ConvertList(dbConnectionDw.GetDataSourceData(idsNotInCache.ToArray()), this))
                //            {
                //                DataSourceDataDict[item.IdDataSource] = item;
                //                returnList.Add(item);
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceData", ex);
                //            throw ex;
                //        }
                //    }
                //    return returnList;
                //}
                //else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataSourceData.ConvertList(dbConnectionDw.GetDataSourceData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceData", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DataSourceData object to the database
        /// </summary>
        /// <param name="toBeSaved">DataSourceData to save</param>
        /// <returns>DataSourceData Id</returns>
        public virtual long SaveDataSourceData(OpDataSourceData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                long ret = dbConnectionDw.SaveDataSourceData(toBeSaved, toBeSaved.DataType);
                //if (DataSourceDataCacheEnabled)
                //{
                //    DataSourceDataDict[toBeSaved.IdDataSource] = toBeSaved; //add or update element
                //}
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDataSourceData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DataSourceData object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataSourceData to delete</param>
        public virtual void DeleteDataSourceData(OpDataSourceData toBeDeleted)
        {
            try
            {
                dbConnectionDw.DeleteDataSourceData(toBeDeleted);
                //if (DataSourceDataCacheEnabled)
                //{
                //    DataSourceDataDict.Remove(toBeDeleted.IdDataSource);
                //}
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelDataSourceData", ex);
                throw ex;
            }
        }

        ///// <summary>
        ///// Clears the DataSourceData object cache
        ///// </summary>
        //public virtual void ClearDataSourceDataCache()
        //{
        //    DataSourceDataDict.Clear();
        //}

        #region GetDataSourceDataFilter
        /// <summary>
        /// Gets DataSourceData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_SOURCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataSourceData list</returns>
        public virtual List<OpDataSourceData> GetDataSourceDataFilter(bool loadNavigationProperties = true, long[] IdDataSource = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataSourceDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataSourceDataFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpDataSourceData> returnList = OpDataSourceData.ConvertList(dbCollectorClient.GetDataSourceDataFilter(ref dbCollectorSession, IdDataSource, IdDataType,
                        topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDataSourceData> returnList = OpDataSourceData.ConvertList(dbConnectionDw.GetDataSourceDataFilter(IdDataSource, IdDataType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    //if (mergeIntoCache)
                    //{
                    //    foreach (OpDataSourceData item in returnList)
                    //        DataSourceDataDict[item.IdDataSource] = item;
                    //    DataSourceDataIsFullCache = true;
                    //}
                    return returnList;
                }
                else
                    return new List<OpDataSourceData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataSourceDataFilter", ex);
                throw;
            }

        }
        #endregion
        #endregion

        #region Device
        #region GetDeviceInvoice

        public virtual DataTableCollection GetDeviceInvoice(int IdDistributor, DateTime StartDate, DateTime EndDate)
        {
            return dbConnectionDw.GetDeviceInvoice(IdDistributor, StartDate, EndDate);
        }

        #endregion
        #endregion
        #region DeviceSchedule

        #region SaveDeviceSchedule

        public virtual void SaveDeviceSchedule(long[] serialNbr, int commandTimeout = 0)
        {
            try
            {
                dbConnectionDw.SaveDeviceSchedule(serialNbr, commandTimeout);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "sp_SaveDeviceSchedule", ex);
                throw ex;
            }
        }
        #endregion

        #endregion

        #region GetFuelNozzlesForMeter

        public virtual List<long> GetFuelNozzlesForMeter(long IdMeter)
        {
            return dbConnectionDw.GetFuelNozzlesForMeter(IdMeter);
        }

        #endregion

        #region Refuel
        #region GetRefuelFilter

        /// <summary>
        /// Gets Refuel list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRefuel funcion</param>
        /// <param name="IdRefuel">Specifies filter for ID_REFUEL column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataSource">Specifies filter for ID_DATA_SOURCE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFUEL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Refuel list</returns>
        public virtual List<OpRefuel> GetRefuelFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdRefuel = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataSource = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
            long? topCount = null, string customWhereClause = null,
            List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRefuelFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRefuelFilter"));
            }

            try
            {
                List<OpRefuel> returnList = OpRefuel.ConvertList(dbConnectionDw.GetRefuelFilter(IdRefuel, IdMeter, IdLocation, IdDataSource, StartTime, EndTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData, customDataTypes);
                if (mergeIntoCache)
                {
                    foreach (OpRefuel item in returnList)
                        RefuelDict[item.IdRefuel] = item;
                    RefuelIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRefuelFilter", ex);
                throw;
            }

        }
        #endregion

        #region GetRefuelDataTemporals
        public virtual Dictionary<DateTime, List<OpDataTemporal>> GetRefuelDataTemporals(long[] idLocation, long[] idMeter, DateTime rangeStart, DateTime rangeEnd, string orderNumber, Enums.AggregationType aggrType, bool onlyJoinedRefuels = true, int commandTimeout = 0)
        {
            Dictionary<DateTime, List<OpDataTemporal>> retDict = new Dictionary<DateTime, List<OpDataTemporal>>();
            foreach (var id in idMeter)
            {
                retDict.Concat(GetRefuelDataTemporals(null, id, rangeStart, rangeEnd, aggrType, onlyJoinedRefuels, commandTimeout));
            }
            return retDict;
        }

        public virtual Dictionary<DateTime, List<OpDataTemporal>> GetRefuelDataTemporals(long? idLocation, long idMeter, DateTime _dataStartTime, DateTime _dataEndTime, Enums.AggregationType aggregationType, bool onlyJoinedRefuels = true, int commandTimeout = 0)
        {
            Dictionary<DateTime, List<OpDataTemporal>> deliveryDataTemporals = new Dictionary<DateTime, List<OpDataTemporal>>();

            /*
               DT: 101407, DS: 1 - brutto calculated
               DT: 101407, DS: 7 - brutto manual
               DT: 101410, DS: 1 - netto calculated
               DT: 101410, DS: 7 - netto manual
           */

            int[] dataSourceTypes = new int[] { (int)Enums.DataSourceType.ManualDataEntry, (int)Enums.DataSourceType.Interface, (int)Enums.DataSourceType.Estimation };
            DataSet refuelData = null;
            try
            {
                refuelData = dbConnectionDw.ExecuteProcedure("fp_GetDeliveryVolumeByReconciliation", commandTimeout, new object[] {
                            new long[] { idMeter }.Select(s => s as object).ToArray(),
                            _dataStartTime as object,
                            _dataEndTime as object,
                            dataSourceTypes.Select(s => s as object).ToArray(),
                            ((int)aggregationType) as object
                        });
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "fp_GetDeliveryVolumeByReconciliation", ex);
                throw;
            }
            if (refuelData != null)
            {
                foreach (DataRow row in refuelData.Tables[0].Rows)
                {
                    try
                    {
                        long? _idMeter = (long?)row.ItemArray[0];
                        long _idLocation = Convert.ToInt64(row.ItemArray[1]);
                        long _idDataType = Convert.ToInt64(row.ItemArray[2]);

                        DateTime? _startTime = (DateTime?)row.ItemArray[4];
                        DateTime? _endTime = (DateTime?)row.ItemArray[5];
                        object _value = row.ItemArray[6];
                        int? _idDataSourceType = (int?)row.ItemArray[7];
                        int? _idAggregationType = (int?)row.ItemArray[8];

                        OpDataTemporal opDataTempral = new OpDataTemporal()
                        {
                            IdMeter = _idMeter,
                            IdLocation = _idLocation,
                            IdDataType = _idDataType,
                            StartTime = ConvertUtcTimeToTimeZone(_startTime),
                            EndTime = ConvertUtcTimeToTimeZone(_endTime),
                            Value = _value,
                            IndexNbr = 0,
                            IdDataSourceType = _idDataSourceType,
                            IdAggregationType = _idAggregationType,
                            Unit = GetUnit((int)Enums.Unit.CubicMeter),
                            DataType = GetDataType(_idDataType),
                            DataStatus = Enums.DataStatus.Normal
                        };

                        if (aggregationType == Enums.AggregationType.Day)
                        {
                            DateTime? _refuelDate = row.ItemArray[3] as DateTime?;
                            if (_refuelDate != null && _refuelDate.HasValue)
                            {
                                if (deliveryDataTemporals.ContainsKey(_refuelDate.Value))
                                    deliveryDataTemporals[_refuelDate.Value].Add(opDataTempral);
                                else
                                {
                                    deliveryDataTemporals.Add(_refuelDate.Value, new List<OpDataTemporal>());
                                    deliveryDataTemporals[_refuelDate.Value].Add(opDataTempral);
                                }
                            }
                        }
                        else
                        {
                            if (opDataTempral.EndTime.HasValue)
                            {
                                if (deliveryDataTemporals.ContainsKey(opDataTempral.EndTime.Value))
                                    deliveryDataTemporals[opDataTempral.EndTime.Value].Add(opDataTempral);
                                else
                                {
                                    deliveryDataTemporals.Add(opDataTempral.EndTime.Value, new List<OpDataTemporal>());
                                    deliveryDataTemporals[opDataTempral.EndTime.Value].Add(opDataTempral);
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // Try next
                    }
                }
            }
            return deliveryDataTemporals;
        }
        #endregion

        #endregion

        #region Report

        #region SaveReport

        /// <summary>
        /// Saves the Report object to the database
        /// </summary>
        /// <param name="toBeSaved">Report to save</param>
        /// <returns>Report Id</returns>
        public virtual int SaveReport(OpReport toBeSaved, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_REPORT toBeSavedClient = new DB_REPORT((DB_REPORT)toBeSaved);
                    toBeSaved.ID_REPORT = toBeSavedClient.ID_REPORT = dbCollectorClient.SaveReport(ref dbCollectorSession, toBeSavedClient);
                }
                else
                {
                    toBeSaved.ID_REPORT = dbConnectionDw.SaveReport(toBeSaved);
                }
                if (ReportCacheEnabled)
                {
                    ReportDict[toBeSaved.IdReport] = toBeSaved; //add or update element
                }

                return toBeSaved.IdReport;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveReport", ex);
                throw ex;
            }
        }

        #endregion

        #region SaveReportData

        /// <summary>
        /// Saves the ReportData object to the database
        /// </summary>
        /// <param name="toBeSaved">ReportData to save</param>
        /// <returns>ReportData Id</returns>
        public virtual long SaveReportData(OpReportData toBeSaved, bool useDBCollector = false)
        {
            try
            {
                long ret = 0;

                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);

                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_REPORT_DATA toBeSavedClient = new DB_REPORT_DATA((DB_REPORT_DATA)toBeSaved);
                    ret = toBeSavedClient.ID_REPORT_DATA = dbCollectorClient.SaveReportData(ref dbCollectorSession, toBeSavedClient);
                }
                else
                {
                    ret = dbConnectionDw.SaveReportData(toBeSaved, toBeSaved.DataType);
                    if (ReportDataCacheEnabled)
                    {
                        ReportDataDict[toBeSaved.IdReportData] = toBeSaved; //add or update element
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveReportData", ex);
                throw;
            }
        }

        #endregion

        #region DeleteReport

        /// <summary>
        /// Deletes the Report object from the database
        /// </summary>
        /// <param name="toBeDeleted">Report to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted from connected data base or from DBCollector web service</param>
        public virtual void DeleteReport(OpReport toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_REPORT toBeDeletedClient = new DB_REPORT(toBeDeleted);
                    dbCollectorClient.DeleteReport(ref dbCollectorSession, toBeDeletedClient);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionDw.DeleteReport(toBeDeleted);
                }

                if (ReportCacheEnabled)
                {
                    ReportDict.Remove(toBeDeleted.IdReport);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelReport", ex);
                throw ex;
            }
        }

        #endregion

        #region DeleteReportData
        /// <summary>
        /// Deletes the ReportData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ReportData to delete</param>
        public virtual void DeleteReportData(OpReportData toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_REPORT_DATA clientToBeDeleted = new DB_REPORT_DATA(toBeDeleted);
                    dbCollectorClient.DeleteReportData(ref dbCollectorSession, clientToBeDeleted);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionDw.DeleteReportData(toBeDeleted);
                }

                if (ReportDataCacheEnabled)
                {
                    ReportDataDict.Remove(toBeDeleted.IdReportData);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelReportData", ex);
                throw ex;
            }
        }
        #endregion

        #endregion

        #region ReportPerformance

        public virtual void DeleteReportPerformance(int[] IdReport = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdReport == null || IdReport.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");
            dbConnectionDw.DeleteReportPerformance(IdReport, IdDataType, IdAggregationType, customWhereClause, commandTimeout);
        }

        #endregion

        #region TransmissionDriverPerformance

        public virtual void DeleteTransmissionDriverPerformance(int[] IdTransmissionDriver = null, long[] IdDataType = null, int[] IdAggregationType = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (IdTransmissionDriver == null || IdTransmissionDriver.Count() == 0)
                && (IdDataType == null || IdDataType.Count() == 0)
                && (IdAggregationType == null || IdAggregationType.Count() == 0))
                throw new ApplicationException("Not specified any parameter");
            dbConnectionDw.DeleteTransmissionDriverPerformance(IdTransmissionDriver, IdDataType, IdAggregationType, customWhereClause, commandTimeout);
        }

        #endregion

        #region SaveConsumerTransaction

        /// <summary>
        /// Gets last ConsumerTransaction object from the database, function makes request to DB
        /// </summary>
        /// <param name="idConsumer">Consumer to get data from</param>
        /// <returns>Consumer Transaction Object</returns>
        public virtual OpConsumerTransaction GetLastConsumerTransaction(int idConsumer, int? idTransactionType)
        {
            return GetConsumerTransactionFilter(
                        IdConsumer: new[] { idConsumer },
                        IdTransactionType: idTransactionType.HasValue ? new[] { idTransactionType.Value } : null,
                        topCount: 1).FirstOrDefault();
            //try
            //{
            //    return OpConsumerTransaction.ConvertList(dbConnectionDw.GetLastConsumerTransaction(IdConsumer, IdTransactionType), this).FirstOrDefault();
            //}
            //catch (Exception ex)
            //{
            //    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetLastConsumerTransaction", ex);
            //    throw ex;
            //}
        }

        /// <summary>
        /// Gets first ConsumerTransaction object from the database, function makes request to DB
        /// </summary>
        /// <param name="IdConsumer">Consumer to get data from</param>
        /// <returns>Consumer Transaction Object</returns>
        public virtual OpConsumerTransaction GetFirstConsumerTransaction(int IdConsumer, int? IdTransactionType, DateTime? startTime = null)
        {
            //try
            //{
            //    return OpConsumerTransaction.ConvertList(dbConnectionDw.GetFirstConsumerTransaction(IdConsumer, IdTransactionType, startTime), this).FirstOrDefault();
            //}
            //catch (Exception ex)
            //{
            //    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetFirstConsumerTransaction", ex);
            //    throw ex;
            //}
            var transactions = GetAllConsumerTransaction().Where(ct => ct.IdConsumer == IdConsumer);

            if (IdTransactionType.HasValue)
            {
                transactions.Where(ct => ct.IdTransactionType == IdTransactionType.Value);
            }

            if (startTime.HasValue)
            {
                transactions.Where(ct => ct.Timestamp >= startTime.Value);
            }

            return transactions.OrderBy(ct => ct.IdConsumerTransaction).FirstOrDefault();
        }
        #endregion

        #region SGM initialize data
        public virtual List<OpData> SgmGetDataForLocationFromGivenDistributor(int[] idDistributorArray, long[] idDataTypeArray)
        {
            try
            {
                return OpData.ConvertList(dbConnectionDw.SgmGetDataForLocationFromGivenDistributor(idDistributorArray, idDataTypeArray), this, true);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "sgm_GetDataForLocationFromGivenDistributor", ex);
                throw;
            }
        }

        #endregion
    }
}
