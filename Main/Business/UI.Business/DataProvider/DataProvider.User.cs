﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.ServiceModel;

using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using WCF = IMR.Suite.Services.Common.ImrWcfServiceReference;
using SESSION = IMR.Suite.Services.Common.ImrWcfServiceReference.SESSION;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;
using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.UI.Business.Objects.Custom;
using CORE = IMR.Suite.UI.Business.Objects.CORE;
using DW = IMR.Suite.UI.Business.Objects.DW;
//using IMR.Suite.UI.Business.Objects.Domain;
//using IMR.Suite.UI.Business.Objects.Domain.CORE;
//using IMR.Suite.UI.Business.Objects.Domain.DW;
//using IMR.Suite.UI.Business.Objects.Domain.Custom;
//using CORE = IMR.Suite.UI.Business.Objects.Domain.CORE;
//using DW = IMR.Suite.UI.Business.Objects.Domain.DW;

using AIUT = IMR.Suite.Common;

namespace IMR.Suite.UI.Business
{
    public partial class DataProvider
    {
        #region Members
        public virtual DBCommonCORE dbConnectionCore { get; private set; }
        public virtual DBCommonDAQ dbConnectionDaq { get; private set; }
        public virtual DBCommonDW dbConnectionDw { get; private set; }

        public virtual WCF.DBCollectorClient dbCollectorClient { get; set; }
        public WCF.SESSION dbCollectorSession;
        private string dbCollectorLogin = null;
        private string dbCollectorPassword = null;
        private string dbCollectorBinding = null;
        private string dbCollectorEndpoint = null;
        private Enums.Module dbCollectorModule = Enums.Module.Unknown;
        private string dbCollectorUserCulture = null;

        private bool dbCollectorClosed = false;
        private System.Timers.Timer DBCollectorKeepAliveTimer = null;
        #endregion

        #region Properties
        #region UserLanguage
        // default language: English
        public virtual Enums.Language UserLanguage { get; set; }
        #endregion
        #region TimeZoneInfo
        public virtual TimeZoneInfo TimeZoneInfo
        {
            get { return (dbConnectionCore != null && dbConnectionCore.TimeZoneInfo != null) ? dbConnectionCore.TimeZoneInfo : TimeZoneInfo.Local; }
            set { if (dbConnectionCore != null) dbConnectionCore.TimeZoneInfo = value;
                  if (dbConnectionDw != null) dbConnectionDw.TimeZoneInfo = value;
                  if (dbConnectionDaq != null) dbConnectionDaq.TimeZoneInfo = value;
                }
        }
        #endregion
        #region DateTimeNow
        public virtual DateTime DateTimeNow
        {
            get { return (dbConnectionCore != null) ? dbConnectionCore.DateTimeNow : DateTime.Now; }
        }
        #endregion
        #region DateTimeUtcNow
        public virtual DateTime DateTimeUtcNow
        {
            get { return (dbConnectionCore != null) ? dbConnectionCore.DateTimeUtcNow : DateTime.UtcNow; }
        }
        #endregion
        #region ConvertUtcTimeToTimeZone
        public virtual DateTime? ConvertUtcTimeToTimeZone(DateTime? dateTime)
        {
            if (dateTime == null) return null;
            return (dbConnectionCore != null) ? dbConnectionCore.ConvertUtcTimeToTimeZone(dateTime) : dateTime.Value.ToLocalTime();
        }
        public virtual DateTime ConvertUtcTimeToTimeZone(DateTime dateTime)
        {
            return (dbConnectionCore != null) ? dbConnectionCore.ConvertUtcTimeToTimeZone(dateTime) : dateTime.ToLocalTime();
        }
        #endregion
        #region ConvertTimeZoneToUtcTime
        public virtual DateTime? ConvertTimeZoneToUtcTime(DateTime? dateTime)
        {
            if (dateTime == null) return null;
            return (dbConnectionCore != null) ? dbConnectionCore.ConvertTimeZoneToUtcTime(dateTime) : dateTime.Value.ToUniversalTime();
        }
        public virtual DateTime ConvertTimeZoneToUtcTime(DateTime dateTime)
        {
            return (dbConnectionCore != null) ? dbConnectionCore.ConvertTimeZoneToUtcTime(dateTime) : dateTime.ToUniversalTime();
        }
        #endregion

        #region GetIMRServerVersion
        public virtual System.Version GetIMRServerVersion(Enums.DatabaseType dbType, int? idServer, bool useDBCollector = false)
        {
            System.Version version = null;

            bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
            if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
            {
                try
                {
                    object[] parameters = new object[]
                    {
                        (int)WCF.DBCollectorProperties.GetIMRServerReleaseVersion,
                        new object[]
                        {
                            (int)dbType,
                            idServer
                        }
                    };
                    object[] ret = dbCollectorClient.GetDBCollectorProperties(ref dbCollectorSession, parameters);
                    if (ret != null && ret.Length > 1 && ret[0] is int && Convert.ToInt32(ret[0]) == (int)WCF.DBCollectorProperties.GetIMRServerReleaseVersion)
                    {
                        if (ret[1] is object[] && (ret[1] as object[]).Length > 0 && (ret[1] as object[])[0] != null)
                            System.Version.TryParse((ret[1] as object[])[0].ToString(), out version);
                    }
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.DBCollectorClient, "GetIMRServerVersion", ex);
                    version = null;
                }
            }
            else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
            {
                switch (dbType)
                {
                    case Enums.DatabaseType.CORE:
                    case Enums.DatabaseType.CORE_IMRSC:
                        if (this.dbConnectionCore != null)
                            System.Version.TryParse(this.dbConnectionCore.IMRServerVersion, out version);
                        break;
                    case Enums.DatabaseType.DAQ:
                        if (this.dbConnectionDaq != null)
                            System.Version.TryParse(this.dbConnectionDaq.IMRServerVersion, out version);
                        break;
                    case Enums.DatabaseType.DW:
                        if (this.dbConnectionDw != null)
                            System.Version.TryParse(this.dbConnectionDw.IMRServerVersion, out version);
                        break;
                }
            }
            return version;
        }
        #endregion
        #region GetDBSchemaVersion
        public virtual string GetDBSchemaVersion(Enums.DatabaseType dbType)
        {
            switch (dbType)
            {
                case Enums.DatabaseType.CORE:
                case Enums.DatabaseType.CORE_IMRSC:
                    if (this.dbConnectionCore != null)
                        return this.dbConnectionCore.DBSchemaVersion;
                    break;
                case Enums.DatabaseType.DAQ:
                    if (this.dbConnectionDaq != null)
                        return this.dbConnectionDaq.DBSchemaVersion;
                    break;
                case Enums.DatabaseType.DW:
                    if (this.dbConnectionDw != null)
                        return this.dbConnectionDw.DBSchemaVersion;
                    break;
            }
            return null;
        }
        #endregion

        #region DBCollectorConnected

        private SESSION DummySession = null;//zawsze musi być nullem! - na potrzeby ConnectionCheck w DBCollectorConnected
        private bool DBCollectorConnected
        {
            get
            {
                if (this.dbCollectorClient != null)
                {
                    try
                    {
                        this.dbCollectorClient.ConnectionCheck(ref DummySession);
                    }
                    catch { }
                }
                if (this.dbCollectorClient != null && dbCollectorSession != null && this.dbCollectorClient.State == CommunicationState.Opened)
                    return true;

                IMRLog.AddToLog(EventID.Forms.DBCollectorNotConnected,
                    this.dbCollectorClient == null ? "<null>" : "OK",
                    dbCollectorSession == null ? "<null>" : "OK",
                    this.dbCollectorClient == null ? "<null>" : this.dbCollectorClient.State.ToString());
                return false;
            }
        }
        #endregion

        #region Filter
        private int[] DefaultDistributorFilter = new int[0];
        public virtual int[] DistributorFilter
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.DistributorFilter : DefaultDistributorFilter;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.DistributorFilter = value;
                if (dbConnectionDaq != null) dbConnectionDaq.DistributorFilter = value;
                if (dbConnectionDw != null) dbConnectionDw.DistributorFilter = value;
            }
        }
        private Dictionary<int, int> DefaultDistributorFilterDict = new Dictionary<int, int>();
        public virtual Dictionary<int, int> DistributorFilterDict
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.DistributorFilterDict : DefaultDistributorFilterDict;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.DistributorFilterDict = value;
                if (dbConnectionDaq != null) dbConnectionDaq.DistributorFilterDict = value;
                if (dbConnectionDw != null) dbConnectionDw.DistributorFilterDict = value;
            }
        }

        private long[] DefaultLocationFilter = new long[0];
        public virtual long[] LocationFilter
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.LocationFilter : DefaultLocationFilter;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.LocationFilter = value;
                if (dbConnectionDaq != null) dbConnectionDaq.LocationFilter = value;
                if (dbConnectionDw != null) dbConnectionDw.LocationFilter = value;
            }
        }
        private Dictionary<long, long> DefaultLocationFilterDict = new Dictionary<long, long>();
        public virtual Dictionary<long, long> LocationFilterDict
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.LocationFilterDict : DefaultLocationFilterDict;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.LocationFilterDict = value;
                if (dbConnectionDaq != null) dbConnectionDaq.LocationFilterDict = value;
                if (dbConnectionDw != null) dbConnectionDw.LocationFilterDict = value;
            }
        }

        private long[] DefaultDeviceFilter = new long[0];
        public virtual long[] DeviceFilter
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.DeviceFilter : DefaultDeviceFilter;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.DeviceFilter = value;
                if (dbConnectionDaq != null) dbConnectionDaq.DeviceFilter = value;
                if (dbConnectionDw != null) dbConnectionDw.DeviceFilter = value;
            }
        }
        private Dictionary<long, long> DefaultDeviceFilterDict = new Dictionary<long, long>();
        public virtual Dictionary<long, long> DeviceFilterDict
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.DeviceFilterDict : DefaultDeviceFilterDict;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.DeviceFilterDict = value;
                if (dbConnectionDaq != null) dbConnectionDaq.DeviceFilterDict = value;
                if (dbConnectionDw != null) dbConnectionDw.DeviceFilterDict = value;
            }
        }

        private long[] DefaultMeterFilter = new long[0];
        public virtual long[] MeterFilter
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.MeterFilter : DefaultMeterFilter;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.MeterFilter = value;
                if (dbConnectionDaq != null) dbConnectionDaq.MeterFilter = value;
                if (dbConnectionDw != null) dbConnectionDw.MeterFilter = value;
            }
        }
        private Dictionary<long, long> DefaultMeterFilterDict = new Dictionary<long, long>();
        public virtual Dictionary<long, long> MeterFilterDict
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.MeterFilterDict : DefaultMeterFilterDict;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.MeterFilterDict = value;
                if (dbConnectionDaq != null) dbConnectionDaq.MeterFilterDict = value;
                if (dbConnectionDw != null) dbConnectionDw.MeterFilterDict = value;
            }
        }

        private int[] DefaultTaskGroupFilter = new int[0];
        public virtual int[] TaskGroupFilter
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.TaskGroupFilter : DefaultTaskGroupFilter;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.TaskGroupFilter = value;
                if (dbConnectionDaq != null) dbConnectionDaq.TaskGroupFilter = value;
                if (dbConnectionDw != null) dbConnectionDw.TaskGroupFilter = value;
            }
        }
        private Dictionary<int, int> DefaultTaskGroupFilterDict = new Dictionary<int, int>();
        public virtual Dictionary<int, int> TaskGroupFilterDict
        {
            get
            {
                return dbConnectionCore != null ? dbConnectionCore.TaskGroupFilterDict : DefaultTaskGroupFilterDict;
            }
            set
            {
                if (dbConnectionCore != null) dbConnectionCore.TaskGroupFilterDict = value;
                if (dbConnectionDaq != null) dbConnectionDaq.TaskGroupFilterDict = value;
                if (dbConnectionDw != null) dbConnectionDw.TaskGroupFilterDict = value;
            }
        }
        #endregion

        public static List<long> ArticleDataTypes { get; set; }
        public static List<long> IssueDataTypes { get; set; }
        public static List<long> DeviceDataTypes { get; set; }
        public static List<long> MeterDataTypes { get; set; }
        public static List<long> MeterTypeDataTypes { get; set; }
        public static List<long> LocationDataTypes { get; set; }
        public static List<long> TaskDataTypes { get; set; }
        public static List<long> RouteDataTypes { get; set; }
        public static List<long> RoutePointDataTypes { get; set; }
        public static List<long> OperatorDataTypes { get; set; }
        public static List<long> DistributorDataTypes { get; set; }
        public static List<long> SystemDataTypes { get; set; }
        public static List<long> PackageDataTypes { get; set; }
        public static List<long> DepositoryElementDataTypes { get; set; }
        public static List<long> ReportDataTypes { get; set; }
        public static List<long> ConsumerDataTypes { get; set; }
        public static List<long> ConsumerTypeDataTypes { get; set; }
        public static List<long> TariffDataTypes { get; set; }
        public static List<long> RefuelDataTypes { get; set; }
        public static List<long> DeviceDriverDataTypes { get; set; }
        public static List<long> TransmissionDriverDataTypes { get; set; }
        public static List<long> ContractDataTypes { get; set; }
        public static List<long> ServicePackageDataTypes { get; set; }
        public static List<long> DiagnosticActionDataTypes { get; set; }
        public static List<long> DeviceInServiceListDataTypes { get; set; }
        public static List<long> DeviceOrderNumberDataTypes { get; set; }
        public static List<long> DeviceDetailsDataTypes { get; set; }
        public static List<long> MessageDataTypes { get; set; }
        public static List<long> ServiceListDataTypes { get; set; }
        public static List<long> ImrSeverDataTypes { get; set; }
        public static List<long> ServiceDataTypes { get; set; }
        public static List<long> ShippingListDataTypes { get; set; }
        public static List<long> SimCardDataTypes { get; set; }
        public static List<long> FaultCodeDataTypes { get; set; }
        public static List<long> LanguageDataTypes { get; set; }
        public static List<long> ViewDataTypes { get; set; }
        public static List<long> ViewColumnDataTypes { get; set; }

        public virtual List<long> IArticleDataTypes { get { return ArticleDataTypes; } set { ArticleDataTypes = value; } }
        public virtual List<long> IIssueDataTypes { get { return IssueDataTypes; } set { IssueDataTypes = value; } }
        public virtual List<long> IDeviceDataTypes { get { return DeviceDataTypes; } set { DeviceDataTypes = value; } }
        public virtual List<long> IMeterDataTypes { get { return MeterDataTypes; } set { MeterDataTypes = value; } }
        public virtual List<long> IMeterTypeDataTypes { get { return MeterTypeDataTypes; } set { MeterTypeDataTypes = value; } }
        public virtual List<long> ILocationDataTypes { get { return LocationDataTypes; } set { LocationDataTypes = value; } }
        public virtual List<long> ITaskDataTypes { get { return TaskDataTypes; } set { TaskDataTypes = value; } }
        public virtual List<long> IRouteDataTypes { get { return RouteDataTypes; } set { RouteDataTypes = value; } }
        public virtual List<long> IRoutePointDataTypes { get { return RoutePointDataTypes; } set { RoutePointDataTypes = value; } }
        public virtual List<long> IOperatorDataTypes { get { return OperatorDataTypes; } set { OperatorDataTypes = value; } }
        public virtual List<long> IDistributorDataTypes { get { return DistributorDataTypes; } set { DistributorDataTypes = value; } }
        public virtual List<long> ISystemDataTypes { get { return SystemDataTypes; } set { SystemDataTypes = value; } }
        public virtual List<long> IPackageDataTypes { get { return PackageDataTypes; } set { PackageDataTypes = value; } }
        public virtual List<long> IDepositoryElementDataTypes { get { return DepositoryElementDataTypes; } set { DepositoryElementDataTypes = value; } }
        public virtual List<long> IReportDataTypes { get { return ReportDataTypes; } set { ReportDataTypes = value; } }
        public virtual List<long> IConsumerDataTypes { get { return ConsumerDataTypes; } set { ConsumerDataTypes = value; } }
        public virtual List<long> IConsumerTypeDataTypes { get { return ConsumerTypeDataTypes; } set { ConsumerTypeDataTypes = value; } }
        public virtual List<long> ITariffDataTypes { get { return TariffDataTypes; } set { TariffDataTypes = value; } }
        public virtual List<long> IRefuelDataTypes { get { return RefuelDataTypes; } set { RefuelDataTypes = value; } }
        public virtual List<long> IDeviceDriverDataTypes { get { return DeviceDriverDataTypes; } set { DeviceDriverDataTypes = value; } }
        public virtual List<long> ITransmissionDriverDataTypes { get { return TransmissionDriverDataTypes; } set { TransmissionDriverDataTypes = value; } }
        public virtual List<long> IContractDataTypes { get { return ContractDataTypes; } set { ContractDataTypes = value; } }
        public virtual List<long> IServicePackageDataTypes { get { return ServicePackageDataTypes; } set { ServicePackageDataTypes = value; } }
        public virtual List<long> IDiagnosticActionDataTypes { get { return DiagnosticActionDataTypes; } set { DiagnosticActionDataTypes = value; } }
        public virtual List<long> IDeviceInServiceListDataTypes { get { return DeviceInServiceListDataTypes; } set { DeviceInServiceListDataTypes = value; } }
        public virtual List<long> IDeviceOrderNumberDataTypes { get { return DeviceOrderNumberDataTypes; } set { DeviceOrderNumberDataTypes = value; } }
        public virtual List<long> IDeviceDetailsDataTypes { get { return DeviceDetailsDataTypes; } set { DeviceDetailsDataTypes = value; } }
        public virtual List<long> IMessageDataTypes { get { return MessageDataTypes; } set { MessageDataTypes = value; } }
        public virtual List<long> IServiceListDataTypes { get { return ServiceListDataTypes; } set { ServiceListDataTypes = value; } }
        public virtual List<long> IImrSeverDataTypes { get { return ImrSeverDataTypes; } set { ImrSeverDataTypes = value; } }
        public virtual List<long> IServiceDataTypes { get { return ServiceDataTypes; } set { ServiceDataTypes = value; } }
        public virtual List<long> ISimCardDataTypes { get { return SimCardDataTypes; } set { SimCardDataTypes = value; } }
        public virtual List<long> IShippingListDataTypes { get { return ShippingListDataTypes; } set { ShippingListDataTypes = value; } }
        public virtual List<long> IFaultCodeDataTypes { get { return FaultCodeDataTypes; } set { FaultCodeDataTypes = value; } }
        public virtual List<long> ILanguageDataTypes { get { return LanguageDataTypes; } set { LanguageDataTypes = value; } }
        public virtual List<long> IViewDataTypes { get { return ViewDataTypes; } set { ViewDataTypes = value; } }
        public virtual List<long> IViewColumnDataTypes { get { return ViewColumnDataTypes; } set { ViewColumnDataTypes = value; } }

        #region SystemOwner
        /// <summary>
        /// Returns System Owner Distributor item if exists
        /// </summary>
        /// <returns>OpDistributor instance</returns>
        public virtual OpDistributor SystemOwner
        {
            get { return this.GetDistributor(1); }
        }
        #endregion

        public virtual DataLoadOptions LoadOptions { get; protected set; }
        public virtual bool LoadOptionsEnabled { get; protected set; }
        #endregion

        #region Ctor
        public DataProvider(DBCommonCORE dbConnectionCore)
            : this(dbConnectionCore, Enums.Language.Default) { }

        public DataProvider(DBCommonCORE dbConnectionCore, Enums.Language userLanguage)
            : this(dbConnectionCore, null, userLanguage) { }

        public DataProvider(DBCommonCORE dbConnectionCore, Enums.Language userLanguage, TimeSpan timeZoneOffset)
            : this(dbConnectionCore, null, userLanguage, timeZoneOffset) { }

        public DataProvider(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW)
            : this(dbConnectionCore, dbConnectionDW, null, Enums.Language.Default) { }

        public DataProvider(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, Enums.Language userLanguage)
            : this(dbConnectionCore, dbConnectionDW, null, userLanguage) { }

        public DataProvider(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, Enums.Language userLanguage, TimeSpan timeZoneOffset)
            : this(dbConnectionCore, dbConnectionDW, null, userLanguage, timeZoneOffset) { }

        public DataProvider(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, DBCommonDAQ dbConnectionDaq)
            : this(dbConnectionCore, dbConnectionDW, dbConnectionDaq, Enums.Language.Default) { }

        public DataProvider(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, DBCommonDAQ dbConnectionDaq, Enums.Language userLanguage)
            : this(dbConnectionCore, dbConnectionDW, dbConnectionDaq, userLanguage, null) { }

        public DataProvider(DBCommonCORE dbConnectionCore, DBCommonDW dbConnectionDW, DBCommonDAQ dbConnectionDaq, Enums.Language userLanguage, TimeSpan? timeZoneOffset,
                            bool initializeUserPart = true, bool initializeDefaultDataTypes = true)
        {
            this.dbConnectionCore = dbConnectionCore;
            this.dbConnectionDw = dbConnectionDW;
            this.dbConnectionDaq = dbConnectionDaq;
            this.UserLanguage = userLanguage;
            if (this.dbConnectionCore != null) this.dbConnectionCore.Language = userLanguage;
            if (this.dbConnectionDw != null) this.dbConnectionDw.Language = userLanguage;
            if (this.dbConnectionDaq != null) this.dbConnectionDaq.Language = userLanguage;
            if (timeZoneOffset != null) TimeZoneInfo = TimeZoneInfo.CreateCustomTimeZone("IMR", timeZoneOffset.Value, "IMR", "IMR");

            if (initializeUserPart)
                InitializeUserPart();
            if (initializeDefaultDataTypes)
                InitializeDefaultDataTypes();

            GetSystemDataVersions();
        }
        #endregion
        #region Close
        public virtual void Close()
        {
            if (dbConnectionCore != null)
            {
                dbConnectionCore.Dispose();
                dbConnectionCore = null;
            }
            if (dbConnectionDaq != null)
            {
                dbConnectionDaq.Dispose();
                dbConnectionDaq = null;
            }
            if (dbConnectionDw != null)
            {
                dbConnectionDw.Dispose();
                dbConnectionDw = null;
            }

            if (this.dbCollectorClient != null && this.dbCollectorClient.State != CommunicationState.Closed)
                this.dbCollectorClient.Abort();
        }
        #endregion
        #region ClearCache
        public virtual void ClearCache()
        {
            //wywolujemy wszystkie ClearCache
            MethodInfo[] methodFounded = this.GetType().GetMethods();
            foreach (MethodInfo clearCacheMethod in methodFounded.Where(p => p.Name.Contains("Clear") && p.Name.Contains("Cache") && !p.Name.Equals("ClearCache")))
            {
                clearCacheMethod.Invoke(this, null);
            }
        }
        #endregion

        #region InitializeDefaultDataTypes
        private void InitializeDefaultDataTypes()
        {
            //Override this values for each application separately.
            LocationDataTypes = new List<long>() { 
                    AIUT.DataType.LOCATION_NAME, 
                    AIUT.DataType.LOCATION_ZIP,
                    AIUT.DataType.LOCATION_CITY,
                    AIUT.DataType.LOCATION_ADDRESS,
                    AIUT.DataType.LOCATION_COUNTRY, 
                    AIUT.DataType.LOCATION_CID, 
                    AIUT.DataType.LOCATION_CREATION_DATE,
                    AIUT.DataType.LOCATION_LATITUDE,
                    AIUT.DataType.LOCATION_LONGITUDE,
                    AIUT.DataType.LOCATION_MEMO,
                    AIUT.DataType.LOCATION_IMR_SERVER_ID,
                    AIUT.DataType.LOCATION_ACTOR_ID,
                    AIUT.DataType.LOCATION_SOURCE_SERVER_ID,
                };

            DeviceDataTypes = new List<long>() { 
					AIUT.DataType.DEVICE_WARRANTY_DATE,
					//AIUT.DataType.DEVICE_PRODUCTION_DATE,
                    AIUT.DataType.DEVICE_ID_DELIVERY,
                    AIUT.DataType.DEVICE_FACTORY_NUMBER,
                    AIUT.DataType.AIR_CONFIG_ADDRESS_IMR_ADDRESS
                 };

            IssueDataTypes = new List<long>() { 
                    AIUT.DataType.ISSUE_WORK_ORDER_JCI_ID,    
                    AIUT.DataType.ISSUE_ATTACHMENT,    
                    AIUT.DataType.ISSUE_IMRMYSC_VISIBLE,
                    AIUT.DataType.ISSUE_SERVER_ID,
                    AIUT.DataType.ISSUE_REPORT_ID,
                };

            MeterDataTypes = new List<long>() { 
                    AIUT.DataType.METER_SERIAL_NUMBER,
                    AIUT.DataType.ATG_PARAMS_PRODUCT_CODE,
                    AIUT.DataType.METER_NUMBER,
                    AIUT.DataType.ATG_PARAMS_TANK_NUMBER,
                    AIUT.DataType.METER_NAME,
                    AIUT.DataType.METER_TANK_CAPACITY
                 };

            MeterTypeDataTypes = new List<long>()
            {
            };

            TaskDataTypes = new List<long>() { 
                    AIUT.DataType.TASK_FITTER_VISIT_DATE,    
                    AIUT.DataType.TASK_FITTER_PAYMENT_DONE,    
                    AIUT.DataType.TASK_CLIENT_PAYMENT_DONE,    
                    AIUT.DataType.TASK_ATTACHMENT,    
                    AIUT.DataType.TASK_IMRMYSC_VISIBLE,
                    AIUT.DataType.TASK_PAYMENT_AMOUNT,
                    AIUT.DataType.TASK_PAYMENT_NOTE,
                    AIUT.DataType.TASK_FITTER_PAYMENT_DATE,
                    AIUT.DataType.ID_FORM_TASK_TYPE,
                    AIUT.DataType.TASK_WARRANTY_SERVICE,
                    AIUT.DataType.TASK_ADDITIONAL_PAYMENT
                };

            OperatorDataTypes = new List<long>(){
                    AIUT.DataType.OPERATOR_MIN_LOG_LEVEL,
                    AIUT.DataType.OPERATOR_SOT_ADDITIONAL_PACKAGE_SEND,
                    AIUT.DataType.OPERATOR_IMR_SERVER_ID,
                    AIUT.DataType.OPERATOR_IMR_SERVER_OPERATOR_ID,
                    AIUT.DataType.OPERATOR_IMR_SERVER_ACTOR_ID,
                    AIUT.DataType.DOMAIN_GUID,
                    AIUT.DataType.OPERATOR_FREQ_REASON_ENABLED
                };

            DiagnosticActionDataTypes = new List<long>()
            {
            };

            DistributorDataTypes = new List<long>()
            {
                AIUT.DataType.DISTRIBUTOR_PROTECTOR_OPERATOR_ID,
                AIUT.DataType.DISTRIBUTOR_REPRESENTATIVE_ID_OPER,
                AIUT.DataType.DISTRIBUTOR_AUTOMATICALLY_SUSPEND_ISSUE,
                AIUT.DataType.DISTRIBUTOR_IMR_SERVER_ID,
            };

            SystemDataTypes = new List<long>()
            {
            };

            RouteDataTypes = new List<long>()
            {
                AIUT.DataType.ROUTE_ADDITIONAL_SYMBOL
            };

            RoutePointDataTypes = new List<long>()
            {
            };

            PackageDataTypes = new List<long>()
            {
                    AIUT.DataType.PACKAGE_ADDRESS,
                    AIUT.DataType.PACKAGE_DIRECTION
            };

            DepositoryElementDataTypes = new List<long>()
            {
                AIUT.DataType.DEPOSITORY_ELEMENT_PACKAGE_ITEM_ERROR_MSG,
                AIUT.DataType.DEPOSITORY_ELEMENT_MAGAZINE
            };

            ReportDataTypes = new List<long>()
            {
                    AIUT.DataType.REPORT_NAME,
                    AIUT.DataType.REPORT_DESCRIPTION,
                    AIUT.DataType.REPORT_STORED_PROCEDURE,
            };

            ConsumerTypeDataTypes = new List<long>()
            {
                    AIUT.DataType.CONSUMER_TYPE_AGREED_OWED,
                    AIUT.DataType.CONSUMER_TYPE_EMERGENCY_CREDIT,
                    AIUT.DataType.CONSUMER_TYPE_REPAYMENT_RULE,
                    AIUT.DataType.CONSUMER_TYPE_PREPAID_ENABLED,
                    
            };

            ConsumerDataTypes = new List<long>()
            {
                    AIUT.DataType.CONSUMER_CREDIT,
                    AIUT.DataType.CONSUMER_DEBT,
                    AIUT.DataType.CONSUMER_TAG_NO,
            };

            ContractDataTypes = new List<long>()
            {
                    AIUT.DataType.CONTRACT_ATTACHMENT,
                    AIUT.DataType.CONTRACT_FILE,
                    AIUT.DataType.CONTRACT_NEW_CONTRACT_ID
            };

            ServicePackageDataTypes = new List<long>()
            {
            };

            TariffDataTypes = new List<long>()
            {
                AIUT.DataType.TARIFF_PRODUCT_CODE,
                AIUT.DataType.TARIFF_CALOFIFIC_VALUE,
                AIUT.DataType.TARIFF_STANDING_CHARGE,
                AIUT.DataType.TARIFF_STANDING_CHARGE_VAT,
                AIUT.DataType.TARIFF_CARBON_TAX,
                AIUT.DataType.TARIFF_CARBON_TAX_VAT,
                AIUT.DataType.TARIFF_PRODUCT_CHARGE_VAT,
                AIUT.DataType.TARIFF_NOTES,
            };

            DeviceInServiceListDataTypes = new List<long>()
            {
                 AIUT.DataType.SERVICE_LIST_DEVICE_ATTACHMENT
            };

            ServiceListDataTypes = new List<long>()
            {
                AIUT.DataType.SERVICE_LIST_MAGAZINE,
            };

            DeviceDriverDataTypes = new List<long>() { };

            TransmissionDriverDataTypes = new List<long>() { };

            DeviceOrderNumberDataTypes = new List<long>()
            {
                AIUT.DataType.DEVICE_ORDER_NUMBER_ATTACHMENT,
                AIUT.DataType.DEVICE_ORDER_NUMBER_FILE_TYPE,
                AIUT.DataType.DEVICE_ORDER_NUMBER_FILE_MD5,
            };

            MessageDataTypes = new List<long>()
            {
                AIUT.DataType.MESSAGE_ATTACHMENT
            };

            ImrSeverDataTypes = new List<long>()
            {
                AIUT.DataType.IMR_SERVER_SYNCHRONIZABLE,
                AIUT.DataType.IMR_SERVER_DBCOLLECTOR_WEBSERVICE_ADDRESS,
                AIUT.DataType.IMR_SERVER_USER_PORTAL_WEBSERVICE_ADDRESS,
                AIUT.DataType.IMR_SERVER_IS_LOCALHOST,
            };

            LanguageDataTypes = new List<long>()
            {

            };

            SimCardDataTypes = new List<long>()
            {
                
            };
        }
        #endregion
        #region InitializeUserPart
        private void InitializeUserPart()
        {
            //ustawiamy wszystkim tabelom domyślnie cachowanie
            Type dataProviderType = this.GetType();
            MemberInfo[] propertyFounded = dataProviderType.GetMembers();
            foreach (MemberInfo cacheProperty in propertyFounded.Where(p => p.Name.Contains("CacheEnabled")))
            {
                switch (cacheProperty.MemberType)
                {
                    case MemberTypes.Field:
                        ((FieldInfo)cacheProperty).SetValue(this, true);
                        break;
                    case MemberTypes.Property:
                        ((PropertyInfo)cacheProperty).SetValue(this, true, null);
                        break;
                }
            }
        }
        #endregion
        #region GetSystemDataVersions
        private void GetSystemDataVersions()
        {
            if (this.dbConnectionCore != null)
            {
                try
                {
                    DB_SYSTEM_DATA[] systemData = this.dbConnectionCore.GetSystemDataFilter(new long[0],
                        new long[] { Common.DataType.SYSTEM_IMR_SERVER_VERSION, Common.DataType.SYSTEM_DB_SCHEMA_VERSION },
                        new int[0],
                        null, null, false, IsolationLevel.ReadUncommitted, 0);
                    DB_SYSTEM_DATA system = systemData.FirstOrDefault(q => q.ID_DATA_TYPE == Common.DataType.SYSTEM_IMR_SERVER_VERSION && q.VALUE != null);
                    if (system != null)
                        this.dbConnectionCore.IMRServerVersion = system.VALUE.ToString();
                    system = systemData.FirstOrDefault(q => q.ID_DATA_TYPE == Common.DataType.SYSTEM_DB_SCHEMA_VERSION && q.VALUE != null);
                    if (system != null)
                        this.dbConnectionCore.DBSchemaVersion = system.VALUE.ToString();
                }
                catch { }
            }
            if (this.dbConnectionDaq != null)
            {
                try
                {
                    DB_SYSTEM_DATA[] systemData = this.dbConnectionDaq.GetSystemDataFilter(new long[0],
                        new long[] { Common.DataType.SYSTEM_IMR_SERVER_VERSION, Common.DataType.SYSTEM_DB_SCHEMA_VERSION },
                        new int[0],
                        null, null, false, IsolationLevel.ReadUncommitted, 0);
                    DB_SYSTEM_DATA system = systemData.FirstOrDefault(q => q.ID_DATA_TYPE == Common.DataType.SYSTEM_IMR_SERVER_VERSION && q.VALUE != null);
                    if (system != null)
                        this.dbConnectionDaq.IMRServerVersion = system.VALUE.ToString();
                    system = systemData.FirstOrDefault(q => q.ID_DATA_TYPE == Common.DataType.SYSTEM_DB_SCHEMA_VERSION && q.VALUE != null);
                    if (system != null)
                        this.dbConnectionDaq.DBSchemaVersion = system.VALUE.ToString();
                }
                catch { }
            }
            if (this.dbConnectionDw != null)
            {
                try
                {
                    DB_SYSTEM_DATA[] systemData = this.dbConnectionDw.GetSystemDataFilter(new long[0],
                        new long[] { Common.DataType.SYSTEM_IMR_SERVER_VERSION, Common.DataType.SYSTEM_DB_SCHEMA_VERSION },
                        new int[0],
                        null, null, false, IsolationLevel.ReadUncommitted, 0);
                    DB_SYSTEM_DATA system = systemData.FirstOrDefault(q => q.ID_DATA_TYPE == Common.DataType.SYSTEM_IMR_SERVER_VERSION && q.VALUE != null);
                    if (system != null)
                        this.dbConnectionDw.IMRServerVersion = system.VALUE.ToString();
                    system = systemData.FirstOrDefault(q => q.ID_DATA_TYPE == Common.DataType.SYSTEM_DB_SCHEMA_VERSION && q.VALUE != null);
                    if (system != null)
                        this.dbConnectionDw.DBSchemaVersion = system.VALUE.ToString();
                }
                catch { }
            }
        }
        #endregion

        #region InitializeDBCollector
        private ManualResetEvent InitializeDBCollectorWaitHandle = new ManualResetEvent(true);
        public virtual bool InitializeDBCollector(string Binding, string Endpoint, Enums.Module Module, string Login, string Password, string UserCulture, int[] DistributorFilter = null)
        {
            bool initialized = false;
            try
            {
                bool blocked = !InitializeDBCollectorWaitHandle.WaitOne(0);
                if (blocked)
                {
                    InitializeDBCollectorWaitHandle.WaitOne();
                    initialized = DBCollectorConnected;
                }
                else
                {
                    InitializeDBCollectorWaitHandle.Reset();
                    dbCollectorLogin = Login;
                    dbCollectorPassword = Password;
                    dbCollectorBinding = Binding;
                    dbCollectorEndpoint = Endpoint;
                    dbCollectorModule = Module;
                    dbCollectorUserCulture = UserCulture;

                    DBCollectorCloseSession();
                    dbCollectorClosed = false;

                    dbCollectorClient = new Services.Common.ImrWcfServiceReference.DBCollectorClient(dbCollectorBinding, dbCollectorEndpoint);

                    dbCollectorClient.Endpoint.Binding.OpenTimeout = TimeSpan.FromMinutes(2);
                    dbCollectorClient.Endpoint.Binding.CloseTimeout = TimeSpan.FromMinutes(2);
                    dbCollectorClient.Endpoint.Binding.ReceiveTimeout = TimeSpan.MaxValue;
                    dbCollectorClient.Endpoint.Binding.SendTimeout = TimeSpan.MaxValue;
                    if (dbCollectorClient.Endpoint.Binding is System.ServiceModel.NetTcpBinding)
                        (dbCollectorClient.Endpoint.Binding as System.ServiceModel.NetTcpBinding).ReliableSession.InactivityTimeout = TimeSpan.MaxValue;

                   // dbCollectorClient.ChannelFactory.Faulted

                    dbCollectorSession = new Services.Common.ImrWcfServiceReference.SESSION();
                    dbCollectorSession.Status = Services.Common.ImrWcfServiceReference.SESSION_STATUS.OK;
                    dbCollectorSession.Time = DateTimeUtcNow;

                    dbCollectorClient.InitDBCollector(null, null, DistributorFilter, this.dbCollectorModule);

                    int timeSpanOffset = Convert.ToInt32(TimeZoneInfo.Local.GetUtcOffset(DateTime.Now).TotalMinutes);
                    dbCollectorSession = dbCollectorClient.Login(dbCollectorSession, dbCollectorLogin, RSA.Encrypt(dbCollectorPassword), dbCollectorUserCulture, timeSpanOffset);

                    DBCollectorKeepAliveTimer = new System.Timers.Timer(180000);
                    DBCollectorKeepAliveTimer.AutoReset = true;
                    DBCollectorKeepAliveTimer.Elapsed += new System.Timers.ElapsedEventHandler(DBCollectorKeepAliveTimer_Elapsed);
                    DBCollectorKeepAliveTimer.Start();
                    IMRLog.AddToLog(EventID.Forms.DBCollectorTimerInfo, "Started during initialization");
                    initialized = true;
                }
            }
            catch (Exception ex)
            {
                initialized = false;
                IMRLog.AddToLog(EventID.Forms.DBCollectorInitializeError, ex.Message, ex.StackTrace);
            }
            finally
            {
                InitializeDBCollectorWaitHandle.Set();
                IMRLog.AddToLog(EventID.Forms.DBCollectorInitializedInfo, initialized);
            }
            return initialized;
        }
        #endregion

        #region DBCollector
        #region DBCollectorKeepAliveTimer_Elapsed
        private void DBCollectorKeepAliveTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (DBCollectorConnected)
                dbCollectorClient.KeepAlive(ref dbCollectorSession);
            else
            {
                if (DBCollectorKeepAliveTimer != null)
                {
                    DBCollectorKeepAliveTimer.Stop();
                    IMRLog.AddToLog(EventID.Forms.DBCollectorTimerInfo, "Stopped in elapsed event");
                }
                IMRLog.AddToLog(EventID.Forms.DBCollectorKeepAliveNotConnected);
                DBCollectorReconnect(0);
            }
        }
        #endregion
        #region DBCollectorReconnect
        private void DBCollectorReconnect(int Retry)
        {
            IMRLog.AddToLog(EventID.Forms.DBCollectorReconnectInfo, Retry);
            if (Retry < 1)
            {
                if (!InitializeDBCollector(dbCollectorBinding, dbCollectorEndpoint, dbCollectorModule, dbCollectorLogin, dbCollectorPassword, dbCollectorUserCulture))
                {
                    Thread.Sleep(1000);
                    DBCollectorReconnect(Retry + 1);
                }
            }
            else
                DBCollectorCloseSession();
        }
        #endregion
        #region DBCollectorCloseSession
        private void DBCollectorCloseSession()
        {
            try
            {
                if (dbCollectorClient != null)
                    dbCollectorClient.Abort();
                if (DBCollectorKeepAliveTimer != null)
                {
                    DBCollectorKeepAliveTimer.Stop();
                    IMRLog.AddToLog(EventID.Forms.DBCollectorTimerInfo, "Stopped during closing session");
                }
            }
            catch (Exception ex) 
            {
                IMRLog.AddToLog(EventID.Forms.DBCollectorCloseError, ex.Message, ex.StackTrace);
            }
            finally
            {
                dbCollectorClient = null;
                dbCollectorClosed = true;
                IMRLog.AddToLog(EventID.Forms.DBCollectorClosed);
            }
        }
        #endregion
        #region IsDBCollectorEnabled
        public virtual bool? IsDBCollectorEnabled(bool useDBCollector, bool tryReconnecting = true)
        {
            if (!useDBCollector)
                return false;

            if (DBCollectorConnected) // jesli polaczeni - true
                return true;
            else if (!DBCollectorConnected && tryReconnecting) // jesli nie polaczeni oraz nie zamknelismy wczesniej sesji (np. przy Reconnect w KeepAlive)
                DBCollectorReconnect(0);

            if (DBCollectorConnected)
                return true;

            IMRLog.AddToLog(EventID.Forms.DBCollectorIsDBCollectorEnabledNull);

            // jesli cos sie nie powiodlo do tej pory, nie mamy polaczenia z WCF, to zwracane null, 
            // aby w funkcjach DataProvider korzystajacych z IsDBCollectorEnabled mozna bylo odpowiednio obsluzyc taka sytuacje - useDBCollector byl ustawiony na true, ale nie mamy polaczenia z WCF
            // moze byc istotne w przypadku funkcji Save czy Delete
            return null;
        }
        #endregion
        #endregion

        #region Actor
        /// <summary>
        /// Gets Actor by id
        /// </summary>
        /// <param name="Ids">Actor Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Actor list</returns>
        public virtual List<OpActor> GetActor(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,  bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpActor> returnList = new List<OpActor>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ActorCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpActor ActorFoundInCache;
                        if (ActorDict.TryGetValue(Ids[i], out ActorFoundInCache)) //element founded in cache
                            returnList.Add(ActorFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpActor item in OpActor.ConvertList(dbConnectionCore.GetActor(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                ActorDict[item.IdActor] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActor", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpActor.ConvertList(dbConnectionCore.GetActor(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActor", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        public virtual void SaveActorDetails(int? changeFromLastMinutes, long[] idActor, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveActorDetails(changeFromLastMinutes, idActor, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateActorDetails", ex);
                throw;
            }
        }

        public virtual void DeleteActorDetails(int idActor)
        {
            try
            {
                dbConnectionCore.DeleteActorDetails(idActor);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DeleteActorDetails", ex);
                throw;
            }
        }
        
        #endregion

        #region ActorInGroup
        /// <summary>
        /// Indicates whether the ActorInGroupDict was filled trough GetAllActorInGroup or GetAllActorInGroupFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ActorInGroupIsFullCache { get; protected set; }

        /// <summary>
        /// ActorInGroup objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, List<OpActorInGroup>> ActorInGroupDict = new Dictionary<int, List<OpActorInGroup>>();

        /// <summary>
        /// Indicates whether the ActorInGroup is cached.
        /// </summary>
        public bool ActorInGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActorInGroup objects
        /// </summary>
        public virtual List<OpActorInGroup> ActorInGroup
        {
            get { return GetAllActorInGroup(); }
        }

        /// <summary>
        /// Gets all ActorInGroup objects
        /// </summary>
        public virtual List<OpActorInGroup> GetAllActorInGroup()
        {
            try
            {
                if (ActorInGroupCacheEnabled) //cache enabled
                {
                    if (!ActorInGroupIsFullCache)
                    {
                        List<OpActorInGroup> objectList = OpActorInGroup.ConvertList(dbConnectionCore.GetActorInGroup(), this);
                        if (objectList != null)
                        {
                            foreach (var item in objectList)
                            {
                                if (!ActorInGroupDict.ContainsKey(item.IdActorGroup))
                                    ActorInGroupDict.Add(item.IdActorGroup, new List<OpActorInGroup>());

                                ActorInGroupDict[item.IdActorGroup].Add(item);
                            }
                            ActorInGroupIsFullCache = true;
                        }
                    }
                    return ActorInGroupDict.Values.SelectMany(w => w).ToList();
                }
                else //cache disabled
                {
                    return OpActorInGroup.ConvertList(dbConnectionCore.GetActorInGroup(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetActorInGroup", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Id">ActorInGroup Id</param>
        /// <returns>ActorInGroup object</returns>
        public virtual OpActorInGroup GetActorInGroup(int Id)
        {
            return GetActorInGroup(Id, false);
        }

        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Ids">ActorInGroup Ids</param>
        /// <returns>ActorInGroup list</returns>
        public virtual List<OpActorInGroup> GetActorInGroup(int[] Ids)
        {
            return GetActorInGroup(Ids, false);
        }

        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Id">ActorInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActorInGroup object</returns>
        public virtual OpActorInGroup GetActorInGroup(int Id, bool queryDatabase)
        {
            return GetActorInGroup(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ActorInGroup by id
        /// </summary>
        /// <param name="Ids">ActorInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActorInGroup list</returns>
        public virtual List<OpActorInGroup> GetActorInGroup(int[] Ids, bool queryDatabase)
        {
            List<OpActorInGroup> returnList = new List<OpActorInGroup>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ActorInGroupCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpActorInGroup> ActorInGroupFoundInCache;
                        if (ActorInGroupDict.TryGetValue(Ids[i], out ActorInGroupFoundInCache)) //element founded in cache
                            returnList.AddRange(ActorInGroupFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpActorInGroup> objectList = OpActorInGroup.ConvertList(dbConnectionCore.GetActorInGroup(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                ActorInGroupDict = new Dictionary<int, List<OpActorInGroup>>();
                                foreach (OpActorInGroup item in objectList)
                                {
                                    if (!ActorInGroupDict.ContainsKey(item.IdActorGroup))
                                        ActorInGroupDict.Add(item.IdActorGroup, new List<OpActorInGroup>());

                                    ActorInGroupDict[item.IdActorGroup].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetActorInGroup", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpActorInGroup.ConvertList(dbConnectionCore.GetActorInGroup(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetActorInGroup", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ActorInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">ActorInGroup to save</param>
        /// <returns>ActorInGroup Id</returns>
        public virtual int SaveActorInGroup(OpActorInGroup toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveActorInGroup(toBeSaved);
                if (ActorInGroupCacheEnabled)
                {
                    if (ActorInGroupDict.ContainsKey(toBeSaved.IdActorGroup))
                    {
                        if (ActorInGroupDict[toBeSaved.IdActorGroup].Find(f => f == toBeSaved) == null)
                            ActorInGroupDict[toBeSaved.IdActorGroup].Add(toBeSaved); //add element
                    }
                    else
                    {
                        ActorInGroupDict.Add(toBeSaved.IdActorGroup, new List<OpActorInGroup>());
                        ActorInGroupDict[toBeSaved.IdActorGroup].Add(toBeSaved); //add element
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveActorInGroup", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ActorInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActorInGroup to delete</param>
        public virtual void DeleteActorInGroup(OpActorInGroup toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteActorInGroup(toBeDeleted);
                if (ActorInGroupCacheEnabled)
                {
                    if (ActorInGroupDict.ContainsKey(toBeDeleted.IdActorGroup))
                    {
                        ActorInGroupDict[toBeDeleted.IdActorGroup].Remove(toBeDeleted); //add element
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelActorInGroup", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ActorInGroup object cache
        /// </summary>
        public virtual void ClearActorInGroupCache()
        {
            ActorInGroupDict.Clear();
            ActorInGroupIsFullCache = false;
        }
        #endregion

        #region Action
        #region GetAction
        /// <summary>
        /// Gets Action by id
        /// </summary>
        /// <param name="Ids">Action Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Action list</returns>
        public virtual List<OpAction> GetAction(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpAction> returnList = new List<OpAction>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ActionCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpAction ActionFoundInCache;
                        if (ActionDict.TryGetValue(Ids[i], out ActionFoundInCache)) //element founded in cache
                            returnList.Add(ActionFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpAction item in OpAction.ConvertList(dbConnectionCore.GetAction(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                ActionDict[item.IdAction] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAction", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpAction.ConvertList(dbConnectionCore.GetAction(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAction", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion
        #region GetActionFilter
        /// <summary>
        /// Gets Action list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">If True, load navigation properties</param>
        /// <param name="mergeIntoCache">If True, query data is merged into cache and available through GetAllAction funcion</param>
        /// <returns>Action list</returns>
        public virtual List<OpAction> GetActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAction = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdActionType = null, int[] IdActionStatus = null,
                            long[] IdActionData = null, long[] IdActionParent = null, long[] IdDataArch = null, int[] IdModule = null, int[] IdOperator = null,
                            int[] IdDistributor = null, Data.DB.TypeDateTimeCode StartDateFrom = null, Data.DB.TypeDateTimeCode StartDateTo = null, 
                            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                            bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpAction> returnList = OpAction.ConvertList(dbCollectorClient.GetActionFilter(ref dbCollectorSession,
                        IdAction, SerialNbr, IdMeter, IdLocation, IdActionType, IdActionStatus, IdActionData, IdActionParent, IdDataArch,
                        IdModule, IdOperator, IdDistributor ?? DistributorFilter, StartDateFrom, StartDateTo,
                        autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpAction> returnList = OpAction.ConvertList(dbConnectionCore.GetActionFilter(IdAction, SerialNbr, IdMeter, IdLocation, IdActionType, IdActionStatus,
                                 IdActionData, IdActionParent, IdDataArch, IdModule, IdOperator, IdDistributor, StartDateFrom, StartDateTo, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpAction item in returnList)
                            ActionDict[item.IdAction] = item;
                        ActionIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpAction>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetActionFilter", ex);
                throw ex;
            }

        }
        #endregion
        #region SaveAction
        /// <summary>
        /// Saves the Action object to the database
        /// </summary>
        /// <param name="toBeSaved">Action to save</param>
        /// <returns>Action Id</returns>
        public virtual long SaveAction(OpAction toBeSaved)
        {
            try
            {
                long ret = dbConnectionCore.SaveAction(toBeSaved);
                if (ActionCacheEnabled)
                {
                    ActionDict[toBeSaved.IdAction] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveAction", ex);
                throw;
            }
        }

        /// <summary>
        /// Saves the Action object to the database
        /// </summary>
        /// <param name="toBeSaved">Action to save</param>
        /// <param name="useDBCollector"></param>
        /// <returns>Action object</returns>
        public virtual OpAction SaveAction(OpAction toBeSaved, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_ACTION db_action = new DB_ACTION(toBeSaved);
                    DB_ACTION ret = dbCollectorClient.SaveAction(ref dbCollectorSession, db_action);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                    }
                    if (!throwDBCollEx)
                    {
                        if (ret != null)
                        {
                            toBeSaved.ID_SERVER = ret.ID_SERVER;
                            if (ret.ID_ACTION != 0)
                                toBeSaved.ID_ACTION = ret.ID_ACTION;
                        }
                        return toBeSaved;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveAction(toBeSaved);
                    if (ActionCacheEnabled)
                    {
                        ActionDict[toBeSaved.IdAction] = toBeSaved; //add or update element
                    }
                    toBeSaved.IdAction = ret;
                    toBeSaved.ID_SERVER = null;
                    return toBeSaved;
                }
                else
                    return toBeSaved;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveAction", ex);
                throw;
            }

            throw dbCollEx;
        }
        #endregion
        #endregion

        #region ActionData
        /// <summary>
        /// Indicates whether the ActionDataDict was filled trough GetAllActionData or GetAllActionDataFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ActionDataIsFullCache { get; protected set; }

        /// <summary>
        /// ActionData objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, List<OpActionData>> ActionDataDict = new Dictionary<long, List<OpActionData>>();

        /// <summary>
        /// Indicates whether the ActionData is cached.
        /// </summary>
        public virtual bool ActionDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionData objects
        /// </summary>
        public virtual List<OpActionData> ActionData
        {
            get { return GetAllActionData(); }
        }

        /// <summary>
        /// Gets all ActionData objects
        /// </summary>
        public virtual List<OpActionData> GetAllActionData()
        {
            try
            {
                if (ActionDataCacheEnabled) //cache enabled
                {
                    if (!ActionDataIsFullCache)
                    {
                        List<OpActionData> objectList = OpActionData.ConvertList(dbConnectionCore.GetActionData(), this);
                        if (objectList != null)
                        {
                            //ActionDataDict = objectList.ToDictionary(o => o.IdActionData);
                            foreach (var item in objectList)
                            {
                                if (!ActionDataDict.ContainsKey(item.IdActionData))
                                    ActionDataDict.Add(item.IdActionData, new List<OpActionData>());

                                ActionDataDict[item.IdActionData].Add(item);
                            }
                            ActionDataIsFullCache = true;
                        }
                    }
                    return ActionDataDict.Values.SelectMany(w => w).ToList();
                }
                else //cache disabled
                {
                    return OpActionData.ConvertList(dbConnectionCore.GetActionData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionData", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Id">ActionData Id</param>
        /// <returns>ActionData object</returns>
        public virtual OpActionData GetActionData(long Id)
        {
            return GetActionData(Id, false);
        }

        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Ids">ActionData Ids</param>
        /// <returns>ActionData list</returns>
        public virtual List<OpActionData> GetActionData(long[] Ids)
        {
            return GetActionData(Ids, false);
        }

        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Id">ActionData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionData object</returns>
        public virtual OpActionData GetActionData(long Id, bool queryDatabase)
        {
            return GetActionData(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ActionData by id
        /// </summary>
        /// <param name="Ids">ActionData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionData list</returns>
        public virtual List<OpActionData> GetActionData(long[] Ids, bool queryDatabase)
        {
            List<OpActionData> returnList = new List<OpActionData>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ActionDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpActionData> ActionDataFoundInCache;
                        if (ActionDataDict.TryGetValue(Ids[i], out ActionDataFoundInCache)) //element founded in cache
                            returnList.AddRange(ActionDataFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpActionData> objectList = OpActionData.ConvertList(dbConnectionCore.GetActionData(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                foreach (var item in objectList)
                                {
                                    if (!ActionDataDict.ContainsKey(item.IdActionData))
                                        ActionDataDict.Add(item.IdActionData, new List<OpActionData>());

                                    ActionDataDict[item.IdActionData].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionData", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpActionData.ConvertList(dbConnectionCore.GetActionData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionData", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        #region GetActionDataFilter

        /// <summary>
        /// Gets ActionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdActionData">Specifies filter for ID_ACTION_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>ActionData list</returns>
        public virtual List<OpActionData> GetActionDataFilter(bool loadNavigationProperties = true, long[] IdActionData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionDataFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpActionData> returnList = OpActionData.ConvertList(dbCollectorClient.GetActionDataFilter(ref dbCollectorSession, IdActionData, IdDataType, IndexNbr,
                        topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionData> returnList = OpActionData.ConvertList(dbConnectionCore.GetActionDataFilter(IdActionData, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else
                    return new List<OpActionData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionDataFilter", ex);
                throw;
            }

        }
        #endregion

        /// <summary>
        /// Saves the ActionData object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionData to save</param>
        /// <returns>ActionData Id</returns>
        public virtual long SaveActionData(OpActionData toBeSaved, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_ACTION_DATA db_actionData = new DB_ACTION_DATA(toBeSaved);
                    toBeSaved.ID_ACTION_DATA = dbCollectorClient.SaveActionData(ref dbCollectorSession, db_actionData);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                    }
                    if (!throwDBCollEx)
                    {
                        return toBeSaved.ID_ACTION_DATA;
                    }
                    else
                        return 0;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (toBeSaved.DataType == null)
                        toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                    long ret = dbConnectionCore.SaveActionData(toBeSaved, toBeSaved.DataType);
                    if (ActionDataCacheEnabled)
                    {
                        if (ActionDataDict.ContainsKey(toBeSaved.IdActionData))
                        {
                            if (ActionDataDict[toBeSaved.IdActionData].Find(f => f == toBeSaved) == null)
                                ActionDataDict[toBeSaved.IdActionData].Add(toBeSaved); //add element
                        }
                        else
                        {
                            ActionDataDict.Add(toBeSaved.IdActionData, new List<OpActionData>());
                            ActionDataDict[toBeSaved.IdActionData].Add(toBeSaved); //add element
                        }
                    }
                    return ret;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveActionData", ex);
                throw ex;
            }
        }
        public virtual long SaveActionData(long IdDataType, int IndexNbr, object Value)
        {
            long? inserterID = null;
            try
            {
                dbConnectionCore.SaveActionData(ref inserterID, IdDataType, IndexNbr, Value);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "dataservice_SaveActionData", ex);
                throw ex;
            }
            return inserterID.HasValue ? inserterID.Value : -1;
        }

        /// <summary>
        /// Deletes the ActionData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionData to delete</param>
        public virtual void DeleteActionData(OpActionData toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                Exception dbCollEx = null;
                bool throwDBCollEx = false;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_ACTION_DATA db_actionData = new DB_ACTION_DATA(toBeDeleted);
                    dbCollectorClient.DeleteActionData(ref dbCollectorSession, db_actionData);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                        if (throwDBCollEx)
                            throw dbCollEx;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.DeleteActionData(toBeDeleted);
                    if (ActionDataCacheEnabled)
                    {
                        if (ActionDataDict.ContainsKey(toBeDeleted.IdActionData))
                        {
                            ActionDataDict[toBeDeleted.IdActionData].RemoveAll(ad => ad.IdActionData == toBeDeleted.IdActionData && ad.IdDataType == toBeDeleted.IdDataType && ad.IndexNbr == toBeDeleted.IndexNbr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelActionData", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the ActionData object cache
        /// </summary>
        public virtual void ClearActionDataCache()
        {
            ActionDataDict.Clear();
            ActionDataIsFullCache = false;
        }
        #endregion

        #region ActionDef

        #region GetActionDefFilter

        /// <summary>
        /// Gets ActionDef list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionDef funcion</param>
        /// <param name="IdActionDef">Specifies filter for ID_ACTION_DEF column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="IdActionData">Specifies filter for ID_ACTION_DATA column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionDef list</returns>
        public virtual List<OpActionDef> GetActionDefFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionDef = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IdActionType = null,
                            long[] IdActionData = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionDefFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionDefFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpActionDef> returnList = OpActionDef.ConvertList(dbCollectorClient.GetActionDefFilter(ref dbCollectorSession, IdActionDef, SerialNbr, IdMeter, IdLocation, IdDataType, IdActionType,
                        IdActionData, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, useDBCollector: useDBCollector);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionDef> returnList = OpActionDef.ConvertList(dbConnectionCore.GetActionDefFilter(IdActionDef, SerialNbr, IdMeter, IdLocation, IdDataType, IdActionType,
                                 IdActionData, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpActionDef item in returnList)
                            ActionDefDict[item.IdActionDef] = item;
                        ActionDefIsFullCache = true;
                    }
                    return returnList;
                }
                else return new List<OpActionDef>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionDefFilter", ex);
                throw;
            }

        }
        #endregion

        #region DeleteActionDef

        /// <summary>
        /// Deletes the ActionDef object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionDef to delete</param>
        public virtual void DeleteActionDef(OpActionDef toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                Exception dbCollEx = null;
                bool throwDBCollEx = false;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_ACTION_DEF db_actiondef = new DB_ACTION_DEF(toBeDeleted);
                    dbCollectorClient.DeleteActionDef(ref dbCollectorSession, db_actiondef);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                        if (throwDBCollEx)
                            throw dbCollEx;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.DeleteActionDef(toBeDeleted);
                    if (ActionDefCacheEnabled)
                    {
                        ActionDefDict.Remove(toBeDeleted.IdActionDef);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelActionDef", ex);
                throw;
            }
        }

        #endregion

        #endregion

        #region ActionHistory
        #region GetActionHistoryFilter
        /// <summary>
        /// Gets ActionHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionHistory funcion</param>
        /// <param name="IdActionHistory">Specifies filter for ID_ACTION_HISTORY column</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="IdActionStatus">Specifies filter for ID_ACTION_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdActionHistoryData">Specifies filter for ID_ACTION_HISTORY_DATA column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Assembly">Specifies filter for ASSEMBLY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether objects should be loaded from connected database or from DBCollector web service</param>
        /// <returns>ActionHistory list</returns>
        public virtual List<OpActionHistory> GetActionHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionHistory = null, long[] IdAction = null, int[] IdActionStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long[] IdDescr = null,
                            long[] IdActionHistoryData = null, int[] IdModule = null, string Assembly = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionHistoryFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpActionHistory> returnList = OpActionHistory.ConvertList(dbCollectorClient.GetActionHistoryFilter(ref dbCollectorSession, IdActionHistory,
                        IdAction, IdActionStatus, StartTime, EndTime, IdDescr, IdActionHistoryData, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionHistory> returnList = OpActionHistory.ConvertList(dbConnectionCore.GetActionHistoryFilter(IdActionHistory, IdAction, IdActionStatus, StartTime, EndTime, IdDescr,
                                 IdActionHistoryData, IdModule, Assembly, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpActionHistory item in returnList)
                            ActionHistoryDict[item.IdActionHistory] = item;
                        ActionHistoryIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpActionHistory>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #endregion

        #region ActionHistoryData
        /// <summary>
        /// Indicates whether the ActionHistoryDataDict was filled trough GetAllActionHistoryData or GetAllActionHistoryDataFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ActionHistoryDataIsFullCache { get; protected set; }

        /// <summary>
        /// ActionHistoryData objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, List<OpActionHistoryData>> ActionHistoryDataDict = new Dictionary<long, List<OpActionHistoryData>>();

        /// <summary>
        /// Indicates whether the ActionHistoryData is cached.
        /// </summary>
        public bool ActionHistoryDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ActionHistoryData objects
        /// </summary>
        public virtual List<OpActionHistoryData> ActionHistoryData
        {
            get { return GetAllActionHistoryData(); }
        }

        /// <summary>
        /// Gets all ActionHistoryData objects
        /// </summary>
        public virtual List<OpActionHistoryData> GetAllActionHistoryData()
        {
            try
            {
                if (ActionHistoryDataCacheEnabled) //cache enabled
                {
                    if (!ActionHistoryDataIsFullCache)
                    {
                        List<OpActionHistoryData> objectList = OpActionHistoryData.ConvertList(dbConnectionCore.GetActionHistoryData(), this);
                        if (objectList != null)
                        {
                            ActionHistoryDataDict = new Dictionary<long, List<OpActionHistoryData>>();
                            foreach (var item in objectList)
                            {
                                if (!ActionHistoryDataDict.ContainsKey(item.IdActionHistoryData))
                                    ActionHistoryDataDict.Add(item.IdActionHistoryData, new List<OpActionHistoryData>());

                                ActionHistoryDataDict[item.IdActionHistoryData].Add(item);
                            }
                            ActionHistoryDataIsFullCache = true;
                        }
                    }
                    return ActionHistoryDataDict.Values.SelectMany(w => w).ToList();
                }
                else //cache disabled
                {
                    return OpActionHistoryData.ConvertList(dbConnectionCore.GetActionHistoryData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionHistoryData", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Id">ActionHistoryData Id</param>
        /// <returns>ActionHistoryData list</returns>
        public virtual List<OpActionHistoryData> GetActionHistoryData(long Id)
        {
            return GetActionHistoryData(Id, false);
        }

        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Ids">ActionHistoryData Ids</param>
        /// <returns>ActionHistoryData list</returns>
        public virtual List<OpActionHistoryData> GetActionHistoryData(long[] Ids)
        {
            return GetActionHistoryData(Ids, false);
        }

        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Id">ActionHistoryData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionHistoryData list</returns>
        public virtual List<OpActionHistoryData> GetActionHistoryData(long Id, bool queryDatabase)
        {
            return GetActionHistoryData(new long[] { Id }, queryDatabase);
        }

        /// <summary>
        /// Gets ActionHistoryData by id
        /// </summary>
        /// <param name="Ids">ActionHistoryData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ActionHistoryData list</returns>
        public virtual List<OpActionHistoryData> GetActionHistoryData(long[] Ids, bool queryDatabase)
        {
            List<OpActionHistoryData> returnList = new List<OpActionHistoryData>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ActionHistoryDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpActionHistoryData> ActionHistoryDataFoundInCache;
                        if (ActionHistoryDataDict.TryGetValue(Ids[i], out ActionHistoryDataFoundInCache)) //element founded in cache
                            returnList.AddRange(ActionHistoryDataFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpActionHistoryData> objectList = OpActionHistoryData.ConvertList(dbConnectionCore.GetActionHistoryData(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                ActionHistoryDataDict = new Dictionary<long, List<OpActionHistoryData>>();
                                foreach (var item in objectList)
                                {
                                    if (!ActionHistoryDataDict.ContainsKey(item.IdActionHistoryData))
                                        ActionHistoryDataDict.Add(item.IdActionHistoryData, new List<OpActionHistoryData>());

                                    ActionHistoryDataDict[item.IdActionHistoryData].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionHistoryData", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpActionHistoryData.ConvertList(dbConnectionCore.GetActionHistoryData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionHistoryData", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ActionHistoryData object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionHistoryData to save</param>
        /// <returns>ActionHistoryData Id</returns>
        public virtual long SaveActionHistoryData(OpActionHistoryData toBeSaved)
        {
            try
            {
                long ret = dbConnectionCore.SaveActionHistoryData(toBeSaved);
                if (ActionHistoryDataCacheEnabled)
                {
                    if (ActionHistoryDataDict.ContainsKey(toBeSaved.IdActionHistoryData))
                    {
                        if (ActionHistoryDataDict[toBeSaved.IdActionHistoryData].Find(f => f == toBeSaved) == null)
                            ActionHistoryDataDict[toBeSaved.IdActionHistoryData].Add(toBeSaved); //add element
                    }
                    else
                    {
                        ActionHistoryDataDict.Add(toBeSaved.IdActionHistoryData, new List<OpActionHistoryData>());
                        ActionHistoryDataDict[toBeSaved.IdActionHistoryData].Add(toBeSaved); //add element
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveActionHistoryData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the ActionHistoryData object from the database
        /// </summary>
        /// <param name="toBeDeleted">ActionHistoryData to delete</param>
        public virtual void DeleteActionHistoryData(OpActionHistoryData toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteActionHistoryData(toBeDeleted);
                if (ActionHistoryDataCacheEnabled)
                {
                    ActionHistoryDataDict.Remove(toBeDeleted.IdActionHistoryData);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelActionHistoryData", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ActionHistoryData object cache
        /// </summary>
        public virtual void ClearActionHistoryDataCache()
        {
            ActionHistoryDataDict.Clear();
            ActionHistoryDataIsFullCache = false;
        }
        #endregion

        #region ActionSmsMessage
        #region GetActionSmsMessageFilter
        /// <summary>
        /// Gets ActionSmsMessage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionSmsMessage funcion</param>
        /// <param name="IdActionSmsMessage">Specifies filter for ID_ACTION_SMS_MESSAGE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="IdTransmissionStatus">Specifies filter for ID_TRANSMISSION_STATUS column</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="Message">Specifies filter for MESSAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_SMS_MESSAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionSmsMessage list</returns>
        public virtual List<OpActionSmsMessage> GetActionSmsMessageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionSmsMessage = null, long[] SerialNbr = null, TypeDateTimeCode Time = null, bool? IsIncoming = null, int[] IdTransmissionStatus = null, long[] IdAction = null,
                            string Message = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionSmsMessageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionSmsMessageFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpActionSmsMessage> returnList = OpActionSmsMessage.ConvertList(dbCollectorClient.GetActionSmsMessageFilter(ref dbCollectorSession,
                        IdActionSmsMessage, SerialNbr, Time, IsIncoming, IdTransmissionStatus, IdAction, Message,
                        topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionSmsMessage> returnList = OpActionSmsMessage.ConvertList(dbConnectionCore.GetActionSmsMessageFilter(IdActionSmsMessage, SerialNbr, Time, IsIncoming, IdTransmissionStatus, IdAction,
                                 Message, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpActionSmsMessage item in returnList)
                            ActionSmsMessageDict[item.IdActionSmsMessage] = item;
                        ActionSmsMessageIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpActionSmsMessage>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionSmsMessageFilter", ex);
                throw;
            }

        }
        #endregion
        #endregion

        #region ActionSmsText

        #region GetActionSmsTextFilter

        /// <summary>
        /// Gets ActionSmsText list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionSmsText funcion</param>
        /// <param name="IdActionSmsText">Specifies filter for ID_ACTION_SMS_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="SmsText">Specifies filter for SMS_TEXT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IsIncoming">Specifies filter for IS_INCOMING column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_SMS_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionSmsText list</returns>
        public virtual List<OpActionSmsText> GetActionSmsTextFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionSmsText = null, int[] IdLanguage = null, string SmsText = null, long[] IdDataType = null, bool? IsIncoming = null, int[] IdTransmissionType = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionSmsTextFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionSmsTextFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpActionSmsText> returnList = OpActionSmsText.ConvertList(dbCollectorClient.GetActionSmsTextFilter(ref dbCollectorSession,
                        IdActionSmsText, IdLanguage, SmsText, IdDataType, IsIncoming, IdTransmissionType,
                                 topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionSmsText> returnList = OpActionSmsText.ConvertList(dbConnectionCore.GetActionSmsTextFilter(IdActionSmsText, IdLanguage, SmsText, IdDataType, IsIncoming, IdTransmissionType,
                                 topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpActionSmsText item in returnList)
                            ActionSmsTextDict[item.IdActionSmsText] = item;
                        ActionSmsTextIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpActionSmsText>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionSmsTextFilter", ex);
                throw;
            }

        }
        #endregion

        #endregion

        #region ActionType

        /// <summary>
        /// Gets all ActionType objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        public virtual List<OpActionType> GetAllActionType(bool useDBCollector = false, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpActionType.ConvertList(dbCollectorClient.GetAllActionType(ref dbCollectorSession), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (ActionTypeCacheEnabled) //cache enabled
                    {
                        if (!ActionTypeIsFullCache)
                        {
                            List<OpActionType> objectList = OpActionType.ConvertList(dbConnectionCore.GetActionType(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                            if (objectList != null)
                            {
                                ActionTypeDict = objectList.ToDictionary(o => o.IdActionType);
                                ActionTypeIsFullCache = true;
                            }
                        }
                        return new List<OpActionType>(ActionTypeDict.Values);
                    }
                    else //cache disabled
                    {
                        return OpActionType.ConvertList(dbConnectionCore.GetActionType(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                    }
                }
                else
                    return new List<OpActionType>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionType", ex);
                throw;
            }
        }

        #endregion

        #region ActionTypeGroupFilter

        #region GetActionTypeGroupFilter

        /// <summary>
        /// Gets ActionTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeGroup funcion</param>
        /// <param name="IdActionTypeGroup">Specifies filter for ID_ACTION_TYPE_GROUP column</param>
        /// <param name="IdActionTypeGroupType">Specifies filter for ID_ACTION_TYPE_GROUP_TYPE column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdParentGroup">Specifies filter for ID_PARENT_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>ActionTypeGroup list</returns>
        public virtual List<OpActionTypeGroup> GetActionTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeGroup = null, int[] IdActionTypeGroupType = null, int[] IdReferenceType = null, int[] IdParentGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionTypeGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeGroupFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpActionTypeGroup.ConvertList(dbCollectorClient.GetActionTypeGroupFilter(ref dbCollectorSession, IdActionTypeGroup, IdActionTypeGroupType, IdReferenceType, IdParentGroup, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionTypeGroup> returnList = OpActionTypeGroup.ConvertList(dbConnectionCore.GetActionTypeGroupFilter(IdActionTypeGroup, IdActionTypeGroupType, IdReferenceType, IdParentGroup, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpActionTypeGroup item in returnList)
                            ActionTypeGroupDict[item.IdActionTypeGroup] = item;
                        ActionTypeGroupIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpActionTypeGroup>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionTypeGroupFilter", ex);
                throw;
            }

        }
        #endregion

        #endregion

        #region ActionTypeInGroup

        #region GetActionTypeInGroupFilter

        /// <summary>
        /// Gets ActionTypeInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeInGroup funcion</param>
        /// <param name="IdActionTypeGroup">Specifies filter for ID_ACTION_TYPE_GROUP column</param>
        /// <param name="ActionTypeName">Specifies filter for ACTION_TYPE_NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>ActionTypeInGroup list</returns>
        public virtual List<OpActionTypeInGroup> GetActionTypeInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeGroup = null, string ActionTypeName = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionTypeInGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeInGroupFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpActionTypeInGroup.ConvertList(dbCollectorClient.GetActionTypeInGroupFilter(ref dbCollectorSession, IdActionTypeGroup, ActionTypeName, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpActionTypeInGroup> returnList = OpActionTypeInGroup.ConvertList(dbConnectionCore.GetActionTypeInGroupFilter(IdActionTypeGroup, ActionTypeName, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpActionTypeInGroup item in returnList)
                            ActionTypeInGroupDict[item.IdActionTypeGroup] = item;
                        ActionTypeInGroupIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpActionTypeInGroup>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionTypeInGroupFilter", ex);
                throw;
            }

        }
        #endregion

        #endregion

        #region Activity
        /// <summary>
        /// Saves the Activity object to the database
        /// </summary>
        /// <param name="toBeSaved">Activity to save</param>
        /// <returns>Activity Id</returns>
        public virtual int SaveActivity(OpActivity toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveActivity(toBeSaved);
                if (ActivityCacheEnabled)
                {
                    ActivityDict[toBeSaved.IdActivity] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveActivity", ex);
                throw;
            }
        }
        #endregion

        #region Alarm

        public virtual DataTable GetIMRSCAlarms(int IdLanguage, string AlarmModule)
        {

            return dbConnectionCore.GetIMRSCAlarms(IdLanguage, AlarmModule);
        }

        #endregion

        #region Audit

        public virtual List<long> GetAuditBatchFromTimePeriod(DateTime StartTime, DateTime EndTime, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return dbConnectionCore.GetAuditBatchFromTimePeriod(StartTime, EndTime, autoTransaction, transactionLevel, commandTimeout);
        }

        #endregion

        #region BillingConsumer

        public virtual List<OpBillingConsumer> GetBillingConsumer(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return GetBillingConsumer(
                            new int[0],
                            autoTransaction: autoTransaction,
                            transactionLevel: transactionLevel,
                            commandTimeout: commandTimeout
                         );
        }

        public virtual OpBillingConsumer GetBillingConsumer(int id, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return GetBillingConsumer(
                            new int[] { id },
                            autoTransaction: autoTransaction,
                            transactionLevel: transactionLevel,
                            commandTimeout: commandTimeout
                         ).FirstOrDefault();
        }

        public virtual List<OpBillingConsumer> GetBillingConsumer(int[] ids, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                return dbConnectionCore.GetBillingConsumer(ids,
                                                autoTransaction: autoTransaction,
                                                transactionLevel: transactionLevel,
                                                commandTimeout: commandTimeout)
                                       .Select(q => new OpBillingConsumer(q)).ToList();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "billing_GetBillingConsumer", ex);
                throw;
            }
        }

        #endregion

        #region ConsumerType

        /// <summary>
        /// Gets ConsumerType by id
        /// </summary>
        /// <param name="Ids">ConsumerType Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerType list</returns>
        public virtual List<OpConsumerType> GetConsumerType(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpConsumerType> returnList = new List<OpConsumerType>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConsumerTypeCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpConsumerType ConsumerTypeFoundInCache;
                        if (ConsumerTypeDict.TryGetValue(Ids[i], out ConsumerTypeFoundInCache)) //element founded in cache
                            returnList.Add(ConsumerTypeFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpConsumerType item in OpConsumerType.ConvertList(dbConnectionCore.GetConsumerType(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                ConsumerTypeDict[item.IdConsumerType] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerType", ex);
                            throw;
                        }
				}
        }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpConsumerType.ConvertList(dbConnectionCore.GetConsumerType(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerType", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        #endregion

        #region Consumer notification
        /// <summary>
        /// Gets ConsumerNotification by id
        /// </summary>
        /// <param name="Ids">ConsumerNotification Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotification list</returns>
        public virtual List<OpConsumerNotification> GetConsumerNotification(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpConsumerNotification> returnList = new List<OpConsumerNotification>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConsumerNotificationCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpConsumerNotification ConsumerNotificationFoundInCache;
                        if (ConsumerNotificationDict.TryGetValue(Ids[i], out ConsumerNotificationFoundInCache)) //element founded in cache
                            returnList.Add(ConsumerNotificationFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpConsumerNotification item in OpConsumerNotification.ConvertList(dbConnectionCore.GetConsumerNotification(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                ConsumerNotificationDict[item.IdConsumerNotification] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerNotification", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpConsumerNotification.ConvertList(dbConnectionCore.GetConsumerNotification(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerNotification", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion

        #region ConsumerNotificationHistory
        /// <summary>
        /// Gets ConsumerNotificationHistory by id
        /// </summary>
        /// <param name="Ids">ConsumerNotificationHistory Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerNotificationHistory list</returns>
        public virtual List<OpConsumerNotificationHistory> GetConsumerNotificationHistory(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpConsumerNotificationHistory> returnList = new List<OpConsumerNotificationHistory>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConsumerNotificationHistoryCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpConsumerNotificationHistory ConsumerNotificationHistoryFoundInCache;
                        if (ConsumerNotificationHistoryDict.TryGetValue(Ids[i], out ConsumerNotificationHistoryFoundInCache)) //element founded in cache
                            returnList.Add(ConsumerNotificationHistoryFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpConsumerNotificationHistory item in OpConsumerNotificationHistory.ConvertList(dbConnectionCore.GetConsumerNotificationHistory(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                ConsumerNotificationHistoryDict[item.IdConsumerNotificationHistory] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerNotificationHistory", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpConsumerNotificationHistory.ConvertList(dbConnectionCore.GetConsumerNotificationHistory(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerNotificationHistory", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion

        #region ConsumerNotificationTypeData

        public bool SaveConsumerPeriodCreditStatusNotification(int consumerId, int timePeriodType, DateTime startTime, DateTime executeTime, int? customInterval)
        {
            try
            {
                return dbConnectionCore.SaveConsumerPeriodCreditStatusNotification(consumerId, timePeriodType, startTime, executeTime, customInterval);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "billing_SaveConsumerPeriodCreditStatusNotification", ex);
                throw;
            }
        }

        public bool DeleteConsumerPeriodCreditStatusNotification(int consumerId)
        {
            try
            {
                return dbConnectionCore.DeleteConsumerPeriodCreditStatusNotification(consumerId);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "billing_DeleteConsumerPeriodCreditStatusNotification", ex);
                throw;
            }
        }

        #endregion ConsumerNotificationTypeData

        #region ConsumerPaymentModule
        /// <summary>
        /// Gets all ConsumerPaymentModule objects
        /// </summary>
        public virtual List<OpConsumerPaymentModule> GetAllConsumerPaymentModule(bool loadNavigationProperties = true)
        {
            try
            {
                if (ConsumerPaymentModuleCacheEnabled) //cache enabled
                {
                    if (!ConsumerPaymentModuleIsFullCache)
                    {
                        List<OpConsumerPaymentModule> objectList = OpConsumerPaymentModule.ConvertList(dbConnectionCore.GetConsumerPaymentModule(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                        if (objectList != null)
                        {
                            ConsumerPaymentModuleDict = objectList.ToDictionary(o => o.IdConsumerPaymentModule);
                            ConsumerPaymentModuleIsFullCache = true;
                        }
                    }
                    return new List<OpConsumerPaymentModule>(ConsumerPaymentModuleDict.Values);
                }
                else //cache disabled
                {
                    return OpConsumerPaymentModule.ConvertList(dbConnectionCore.GetConsumerPaymentModule(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerPaymentModule", ex);
                throw;
            }
        }

        /// <summary>
        /// Gets ConsumerPaymentModule by id
        /// </summary>
        /// <param name="Ids">ConsumerPaymentModule Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerPaymentModule list</returns>
        public virtual List<OpConsumerPaymentModule> GetConsumerPaymentModule(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpConsumerPaymentModule> returnList = new List<OpConsumerPaymentModule>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConsumerPaymentModuleCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpConsumerPaymentModule ConsumerPaymentModuleFoundInCache;
                        if (ConsumerPaymentModuleDict.TryGetValue(Ids[i], out ConsumerPaymentModuleFoundInCache)) //element founded in cache
                            returnList.Add(ConsumerPaymentModuleFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpConsumerPaymentModule item in OpConsumerPaymentModule.ConvertList(dbConnectionCore.GetConsumerPaymentModule(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                ConsumerPaymentModuleDict[item.IdConsumerPaymentModule] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerPaymentModule", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpConsumerPaymentModule.ConvertList(dbConnectionCore.GetConsumerPaymentModule(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerPaymentModule", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion

        #region ConsumerTransaction

        /// <summary>
        /// Gets ConsumerTransaction by id
        /// </summary>
        /// <param name="Ids">ConsumerTransaction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ConsumerTransaction list</returns>
        public virtual List<CORE.OpConsumerTransaction> GetConsumerTransactionCORE(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<CORE.OpConsumerTransaction> returnList = new List<CORE.OpConsumerTransaction>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ConsumerTransactionCacheEnabledCORE && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        CORE.OpConsumerTransaction ConsumerTransactionFoundInCache;
                        if (ConsumerTransactionDictCORE.TryGetValue(Ids[i], out ConsumerTransactionFoundInCache)) //element founded in cache
                            returnList.Add(ConsumerTransactionFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (CORE.OpConsumerTransaction item in CORE.OpConsumerTransaction.ConvertList(dbConnectionCore.GetConsumerTransaction(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                ConsumerTransactionDictCORE[item.IdConsumerTransaction] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransaction", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return CORE.OpConsumerTransaction.ConvertList(dbConnectionCore.GetConsumerTransaction(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransaction", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        #endregion

		#region CommandCodeParamMap
		
		/// <summary>
        /// Gets all CommandCodeParamMap objects
		/// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// </summary>
        public virtual List<OpCommandCodeParamMap> GetAllCommandCodeParamMap(bool useDBCollector = false)
        {
            try
            {
				bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
					return OpCommandCodeParamMap.ConvertList(dbCollectorClient.GetAllCommandCodeParamMap(ref dbCollectorSession), this);
				}
				else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
				{
					if (CommandCodeParamMapCacheEnabled) //cache enabled
					{
						if (!CommandCodeParamMapIsFullCache)
						{
							List<OpCommandCodeParamMap> objectList = OpCommandCodeParamMap.ConvertList(dbConnectionCore.GetCommandCodeParamMap(), this);
							if (objectList != null)
							{
								CommandCodeParamMapDict = objectList.ToDictionary(o => o.IdCommandCodeParamMap);
								CommandCodeParamMapIsFullCache = true;
							}
						}
						return new List<OpCommandCodeParamMap>(CommandCodeParamMapDict.Values);
					}
					else //cache disabled
					{
						return OpCommandCodeParamMap.ConvertList(dbConnectionCore.GetCommandCodeParamMap(), this);
					}
				}
				else
					return new List<OpCommandCodeParamMap>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetCommandCodeParamMap", ex);
                throw;
            }
        }
		
		#endregion
		
		#region CommandCodeType
		
		/// <summary>
        /// Gets all CommandCodeType objects
		/// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// </summary>
        public virtual List<OpCommandCodeType> GetAllCommandCodeType(bool useDBCollector = false)
        {
            try
            {
				bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
					return OpCommandCodeType.ConvertList(dbCollectorClient.GetAllCommandCodeType(ref dbCollectorSession), this);
				}
				else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
				{
					if (CommandCodeTypeCacheEnabled) //cache enabled
					{
						if (!CommandCodeTypeIsFullCache)
						{
							List<OpCommandCodeType> objectList = OpCommandCodeType.ConvertList(dbConnectionCore.GetCommandCodeType(), this);
							if (objectList != null)
							{
								CommandCodeTypeDict = objectList.ToDictionary(o => o.IdCommandCode);
								CommandCodeTypeIsFullCache = true;
							}
						}
						return new List<OpCommandCodeType>(CommandCodeTypeDict.Values);
					}
					else //cache disabled
					{
						return OpCommandCodeType.ConvertList(dbConnectionCore.GetCommandCodeType(), this);
					}
				}
				else
					return new List<OpCommandCodeType>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetCommandCodeType", ex);
                throw;
            }
        }
		
		#endregion
		
        #region Data
        #region GetDataFilter
        /// <summary>
        /// Gets Data list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllData funcion</param>
        /// <param name="IdData">Specifies filter for ID_DATA column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Status">Specifies filter for STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>Data list</returns>
        public virtual List<Objects.CORE.OpData> GetDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdData = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, long[] IdDataType = null, int[] IndexNbr = null,
                            int[] Status = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<Objects.CORE.OpData> returnList = Objects.CORE.OpData.ConvertList(dbCollectorClient.GetDataFilter(ref dbCollectorSession, IdData,
                        SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr, Status,
                        topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<Objects.CORE.OpData> returnList = Objects.CORE.OpData.ConvertList(dbConnectionCore.GetDataFilter(IdData, SerialNbr, IdMeter, IdLocation, IdDataType, IndexNbr,
                                 Status, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (Objects.CORE.OpData item in returnList)
                            DataDict[item.IdData] = item;
                        DataIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<Objects.CORE.OpData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region SaveData
        /// <summary>
        /// Saves the Data object to the database
        /// </summary>
        /// <param name="toBeSaved">Data to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>Data Id</returns>
        public virtual long SaveData(Objects.CORE.OpData toBeSaved, bool useDBCollector = false)
        {
            try
            {
                bool isNew = toBeSaved.IdData == 0;
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);

                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DATA db_data = new DB_DATA(toBeSaved);
                    DB_DATA_TYPE db_dDataType = new DB_DATA_TYPE(toBeSaved.DataType);
                    long ret = dbCollectorClient.SaveData(ref dbCollectorSession, db_data, db_dDataType);
                    InvokeDataChanged(toBeSaved, (isNew ? NotifyType.Insert : NotifyType.Update), typeof(Business.Objects.CORE.OpData));
                    if (ret == 0)
                        return toBeSaved.IdData;
                    return ret;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveData(toBeSaved, toBeSaved.DataType);
                    if (DataCacheEnabled)
                    {
                        DataDict[toBeSaved.IdData] = toBeSaved; //add or update element
                    }
                    InvokeDataChanged(toBeSaved, (isNew ? NotifyType.Insert : NotifyType.Update), typeof(Business.Objects.CORE.OpData));
                    return ret;
                }
                else
                    return toBeSaved.IdData;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveData", ex);
                throw;
            }
        }
        #endregion

        #region DeleteData

        /// <summary>
        /// Deletes the Data object from the database
        /// </summary>
        /// <param name="toBeDeleted">Data to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted in connected data base or in DBCollector web service</param>
        public virtual void DeleteData(IMR.Suite.UI.Business.Objects.CORE.OpData toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DATA operatorData = new DB_DATA((DB_DATA)toBeDeleted);
                    dbCollectorClient.DeleteData(ref dbCollectorSession, operatorData);

                    if (dbCollectorSession.Status == IMR.Suite.Services.Common.ImrWcfServiceReference.SESSION_STATUS.ERROR)
                    {
                        throw new Exception(dbCollectorSession.Error.Msg);
                    }
                }
                else
                {
                    dbConnectionCore.DeleteData(toBeDeleted);
                    if (DataCacheEnabled)
                    {
                        DataDict.Remove(toBeDeleted.IdData);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelData", ex);
                throw;
            }
        }

        #endregion

        #endregion

        #region DataFormatGroupDetails
        /// <summary>
        /// Indicates whether the DataFormatGroupDetailsDict was filled trough GetAllDataFormatGroupDetails or GetAllDataFormatGroupDetailsFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataFormatGroupDetailsIsFullCache { get; protected set; }

        /// <summary>
        /// DataFormatGroupDetails objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, List<OpDataFormatGroupDetails>> DataFormatGroupDetailsDict = new Dictionary<int, List<OpDataFormatGroupDetails>>();

        /// <summary>
        /// Indicates whether the DataFormatGroupDetails is cached.
        /// </summary>
        public bool DataFormatGroupDetailsCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DataFormatGroupDetails objects
        /// </summary>
        public virtual List<OpDataFormatGroupDetails> DataFormatGroupDetails
        {
            get { return GetAllDataFormatGroupDetails(); }
        }

        /// <summary>
        /// Gets all DataFormatGroupDetails objects
        /// </summary>
        public virtual List<OpDataFormatGroupDetails> GetAllDataFormatGroupDetails()
        {
            try
            {
                if (DataFormatGroupDetailsCacheEnabled) //cache enabled
                {
                    if (!DataFormatGroupDetailsIsFullCache)
                    {
                        List<OpDataFormatGroupDetails> objectList = OpDataFormatGroupDetails.ConvertList(dbConnectionCore.GetDataFormatGroupDetails(), this);
                        if (objectList != null)
                        {
                            foreach (OpDataFormatGroupDetails item in objectList)
                            {
                                if (!DataFormatGroupDetailsDict.ContainsKey(item.IdDataFormatGroup))
                                    DataFormatGroupDetailsDict.Add(item.IdDataFormatGroup, new List<OpDataFormatGroupDetails>());

                                DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item);
                            }
                            DataFormatGroupDetailsIsFullCache = true;
                        }
                    }
                    return DataFormatGroupDetailsDict.Values.SelectMany(w => w).ToList();
                }
                else //cache disabled
                {
                    return OpDataFormatGroupDetails.ConvertList(dbConnectionCore.GetDataFormatGroupDetails(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFormatGroupDetails", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Id">DataFormatGroupDetails Id</param>
        /// <returns>DataFormatGroupDetails object</returns>
        public virtual OpDataFormatGroupDetails GetDataFormatGroupDetails(int Id)
        {
            return GetDataFormatGroupDetails(Id, false);
        }

        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Ids">DataFormatGroupDetails Ids</param>
        /// <returns>DataFormatGroupDetails list</returns>
        public virtual List<OpDataFormatGroupDetails> GetDataFormatGroupDetails(int[] Ids)
        {
            return GetDataFormatGroupDetails(Ids, false);
        }

        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Id">DataFormatGroupDetails Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataFormatGroupDetails object</returns>
        public virtual OpDataFormatGroupDetails GetDataFormatGroupDetails(int Id, bool queryDatabase)
        {
            return GetDataFormatGroupDetails(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets DataFormatGroupDetails by id
        /// </summary>
        /// <param name="Ids">DataFormatGroupDetails Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataFormatGroupDetails list</returns>
        public virtual List<OpDataFormatGroupDetails> GetDataFormatGroupDetails(int[] Ids, bool queryDatabase)
        {
            List<OpDataFormatGroupDetails> returnList = new List<OpDataFormatGroupDetails>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataFormatGroupDetailsCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpDataFormatGroupDetails> DataFormatGroupDetailsFoundInCache;
                        if (DataFormatGroupDetailsDict.TryGetValue(Ids[i], out DataFormatGroupDetailsFoundInCache)) //element founded in cache
                            returnList.AddRange(DataFormatGroupDetailsFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpDataFormatGroupDetails> objectList = OpDataFormatGroupDetails.ConvertList(dbConnectionCore.GetDataFormatGroupDetails(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                foreach (OpDataFormatGroupDetails item in objectList)
                                {
                                    if (DataFormatGroupDetailsDict.ContainsKey(item.IdDataFormatGroup))
                                    {
                                        if (DataFormatGroupDetailsDict[item.IdDataFormatGroup] == null)
                                        {
                                            DataFormatGroupDetailsDict[item.IdDataFormatGroup] = new List<OpDataFormatGroupDetails>();
                                            DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                                        }
                                        else
                                        {
                                            if (DataFormatGroupDetailsDict[item.IdDataFormatGroup].Any(q => q.IdDataFormatGroup == item.IdDataFormatGroup && q.IdDataType == item.IdDataType))
                                            {
                                                DataFormatGroupDetailsDict[item.IdDataFormatGroup].RemoveAll(q => q.IdDataFormatGroup == item.IdDataFormatGroup && q.IdDataType == item.IdDataType);
                                                DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                                            }
                                            else
                                                DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                                        }
                                    }
                                    else
                                    {
                                        DataFormatGroupDetailsDict.Add(item.IdDataFormatGroup, new List<OpDataFormatGroupDetails>());
                                        DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                                    }

                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFormatGroupDetails", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataFormatGroupDetails.ConvertList(dbConnectionCore.GetDataFormatGroupDetails(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFormatGroupDetails", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DataFormatGroupDetails object to the database
        /// </summary>
        /// <param name="toBeSaved">DataFormatGroupDetails to save</param>
        /// <returns>DataFormatGroupDetails Id</returns>
        public virtual int SaveDataFormatGroupDetails(OpDataFormatGroupDetails toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDataFormatGroupDetails(toBeSaved);
                if (DataFormatGroupDetailsCacheEnabled)
                {
                    if (DataFormatGroupDetailsDict.ContainsKey(toBeSaved.IdDataFormatGroup))
                    {
                        if (DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup] == null)
                        {
                            DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup] = new List<OpDataFormatGroupDetails>();
                            DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup].Add(toBeSaved); //add element
                        }
                        else
                        {
                            if (DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup].Any(q => q.IdDataFormatGroup == toBeSaved.IdDataFormatGroup && q.IdDataType == toBeSaved.IdDataType))
                            {
                                DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup].RemoveAll(q => q.IdDataFormatGroup == toBeSaved.IdDataFormatGroup && q.IdDataType == toBeSaved.IdDataType);
                                DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup].Add(toBeSaved); //add element
                            }
                            else
                                DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup].Add(toBeSaved); //add element
                        }
                    }
                    else
                    {
                        DataFormatGroupDetailsDict.Add(toBeSaved.IdDataFormatGroup, new List<OpDataFormatGroupDetails>());
                        DataFormatGroupDetailsDict[toBeSaved.IdDataFormatGroup].Add(toBeSaved); //add element
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDataFormatGroupDetails", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DataFormatGroupDetails object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataFormatGroupDetails to delete</param>
        public virtual void DeleteDataFormatGroupDetails(OpDataFormatGroupDetails toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteDataFormatGroupDetails(toBeDeleted);
                if (DataFormatGroupDetailsCacheEnabled)
                {
                    if (DataFormatGroupDetailsDict.ContainsKey(toBeDeleted.IdDataFormatGroup))
                    {
                        if (DataFormatGroupDetailsDict[toBeDeleted.IdDataFormatGroup] != null && DataFormatGroupDetailsDict[toBeDeleted.IdDataFormatGroup].Any(q => q.IdDataFormatGroup == toBeDeleted.IdDataFormatGroup && q.IdDataType == toBeDeleted.IdDataType))
                            DataFormatGroupDetailsDict[toBeDeleted.IdDataFormatGroup].RemoveAll(q => q.IdDataFormatGroup == toBeDeleted.IdDataFormatGroup && q.IdDataType == toBeDeleted.IdDataType);
                        if (DataFormatGroupDetailsDict[toBeDeleted.IdDataFormatGroup] != null && DataFormatGroupDetailsDict[toBeDeleted.IdDataFormatGroup].Count == 0)
                            DataFormatGroupDetailsDict.Remove(toBeDeleted.IdDataFormatGroup);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelDataFormatGroupDetails", ex);
                throw;
            }
        }

        public virtual List<OpDataFormatGroupDetails> GetHierarchicalDataFormatGroupDetails(int IdOperator, int IdDistributor, int IdModule, bool loadNavigationProperties = true)
        {
            List<OpDataFormatGroupDetails> returnList = new List<OpDataFormatGroupDetails>();
            try
            {
                return OpDataFormatGroupDetails.ConvertList(dbConnectionCore.GetHierarchicalDataFormatGroupDetails(IdOperator, IdDistributor, IdModule), this, loadNavigationProperties);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetHierarchicalDataFormatGroupDetails", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the DataFormatGroupDetails object cache
        /// </summary>
        public virtual void ClearDataFormatGroupDetailsCache()
        {
            DataFormatGroupDetailsDict.Clear();
            DataFormatGroupDetailsIsFullCache = false;
        }
        #endregion

        #region DataTransfer

        #region SaveDataTransfer
        /// <summary>
        /// Saves the DataTransfer object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTransfer to save</param>
        /// <returns>DataTransfer Id</returns>
        public virtual long SaveDataTransfer(OpDataTransfer toBeSaved)
        {
            try
            {
                long ret = dbConnectionCore.SaveDataTransfer(toBeSaved);
                if (DataTransferCacheEnabled)
                {
                    DataTransferDict[toBeSaved.IdDataTransfer] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDataTransfer", ex);
                throw;
            }
        }


        public virtual void SaveDataTransfer(OpDataTransfer[] toBeSaved, int commandTimeout = 0)
        {
            try
            {
                if (toBeSaved != null && toBeSaved.Count() > 0)
                {
                    dbConnectionCore.SaveDataTransfer(toBeSaved, commandTimeout: commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "DATA_TRANSFER", ex);
                throw;
            }
        }

        #endregion

        #region DeleteDataTransfer

        public virtual void DeleteDataTransfer(long[] BatchId = null,
            string customWhereClause = null, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "DeleteDataTemporal"));
            if (String.IsNullOrEmpty(customWhereClause)
                && (BatchId == null || BatchId.Count() == 0))
                throw new ApplicationException("Not specified any parameter");
            dbConnectionCore.DeleteDataTransfer(BatchId, customWhereClause, commandTimeout);
        }

        #endregion

        #endregion

        #region Descr
        /// <summary>
        /// Indicates whether the DescrDict was filled trough GetAllDescr or GetDescrFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DescrIsFullCache { get; set; }

        /// <summary>
        /// Indicates whether the Descr is cached.
        /// </summary>
        public virtual bool DescrCacheEnabled { get; set; }

        /// <summary>
        /// Descr objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpDescr> DescrDict = new Dictionary<long, OpDescr>();

        /// <summary>
        /// Gets all Descr objects
        /// </summary>
        public virtual List<OpDescr> Descr
        {
            get { return GetAllDescr(); }
        }

        /// <summary>
        /// Gets all Descr objects
        /// </summary>
        public virtual List<OpDescr> GetAllDescr(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (DescrCacheEnabled) //cache enabled
                {
                    if (!DescrIsFullCache)
                    {
                        List<OpDescr> objectList = OpDescr.ConvertList(dbConnectionCore.GetDescr(UserLanguage, autoTransaction, transactionLevel, commandTimeout), this);
                        if (objectList != null)
                        {
                            DescrDict = objectList.ToDictionary(o => o.IdDescr);
                            DescrIsFullCache = true;
                        }
                    }
                    return new List<OpDescr>(DescrDict.Values);
                }
                else //cache disabled
                {
                    return OpDescr.ConvertList(dbConnectionCore.GetDescr(UserLanguage, autoTransaction, transactionLevel, commandTimeout), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDescr", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <returns>Descr object</returns>
        public virtual OpDescr GetDescr(long Id)
        {
            return GetDescr(Id, false, UserLanguage);
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <returns>Descr list</returns>
        public virtual List<OpDescr> GetDescr(long[] Ids)
        {
            return GetDescr(Ids, false, UserLanguage);
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="language">Language</param>
        /// <returns>Descr object</returns>
        public virtual OpDescr GetDescr(long Id, Enums.Language language)
        {
            return GetDescr(Id, false, language);
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <param name="language">Language</param>
        /// <returns>Descr list</returns>
        public virtual List<OpDescr> GetDescr(long[] Ids, Enums.Language language)
        {
            return GetDescr(Ids, false, language);
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr object</returns>
        public virtual OpDescr GetDescr(long Id, bool queryDatabase)
        {
            return GetDescr(new long[] { Id }, queryDatabase, UserLanguage).FirstOrDefault();
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr object</returns>
        public virtual OpDescr GetDescr(long[] Ids, bool queryDatabase)
        {
            return GetDescr(Ids, queryDatabase, UserLanguage).FirstOrDefault();
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Id">Descr Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr object</returns>
        public virtual OpDescr GetDescr(long Id, bool queryDatabase, Enums.Language language)
        {
            return GetDescr(new long[] { Id }, queryDatabase, language).FirstOrDefault();
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr list</returns>
        public virtual List<OpDescr> GetDescr(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            return GetDescr(Ids, queryDatabase, UserLanguage, autoTransaction, transactionLevel, commandTimeout);
        }

        /// <summary>
        /// Gets Descr by id
        /// </summary>
        /// <param name="Ids">Descr Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Descr list</returns>
        public virtual List<OpDescr> GetDescr(long[] Ids, bool queryDatabase, Enums.Language language, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (DescrCacheEnabled)
            {
                if (queryDatabase) DescrIsFullCache = false;
                List<OpDescr> returnList = new List<OpDescr>();
                List<long> idsNotInCache = new List<long>();
                for (int i = 0; i < Ids.Length; i++)
                {
                    OpDescr DescrFoundInCache;
                    if (DescrDict.TryGetValue(Ids[i], out DescrFoundInCache) && !queryDatabase) //element founded in cache
                        returnList.Add(DescrFoundInCache);
                    else //element not found in cache, query database
                        idsNotInCache.Add(Ids[i]);
                }
                if (idsNotInCache.Count > 0 && !DescrIsFullCache)
                {
                    try
                    {
                        foreach (OpDescr item in OpDescr.ConvertList(dbConnectionCore.GetDescr(idsNotInCache.ToArray(), language, autoTransaction, transactionLevel, commandTimeout), this, true, autoTransaction, transactionLevel, commandTimeout))
                        {
                            DescrDict[item.IdDescr] = item;
                            returnList.Add(item);
                        }
                        foreach (long idNotFound in idsNotInCache)
                        {
                            if (returnList.FirstOrDefault(d => d.IdDescr == idNotFound) == null)
                            {
                                //returnList.Add(new OpDescr(new DB_DESCR() { DESCRIPTION = "Description missing!" }));
#if DEBUG
                                System.Diagnostics.Debug.WriteLine("DESCR missing" + idNotFound);
#endif
                            }
                        }
                        //TODO bblach log o brakującym tłumaczeniu
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDescr", ex);
                        throw ex;
                    }
                }
                if (queryDatabase) DescrIsFullCache = true;
                return returnList;
            }
            else //cache disabled
            {
                try
                {
                    return OpDescr.ConvertList(dbConnectionCore.GetDescr(Ids, UserLanguage, autoTransaction, transactionLevel, commandTimeout), this, true, autoTransaction, transactionLevel, commandTimeout);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDescr", ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Saves the Descr object to the database
        /// </summary>
        /// <param name="toBeSaved">Descr to save</param>
        /// <returns>Descr Id</returns>
        public virtual long SaveDescr(OpDescr toBeSaved, long idDescrRangeStart = 0, long idDescrRangeStop = 0, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DESCR db_descr = new DB_DESCR(toBeSaved);
                    toBeSaved.ID_DESCR = dbCollectorClient.SaveDescr(ref dbCollectorSession, db_descr, idDescrRangeStart, idDescrRangeStop);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                    }
                    if (!throwDBCollEx)
                    {
                        return toBeSaved.ID_DESCR;
                    }
                    else
                        throw dbCollEx;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveDescr(toBeSaved, idDescrRangeStart, idDescrRangeStop);
                    if (DescrCacheEnabled && toBeSaved.IdLanguage == (int)UserLanguage)
                    {
                        DescrDict[toBeSaved.IdDescr] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                else
                    return 0;

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDescr", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the Descr
        /// </summary>
        /// <param name="toBeDeleted">Descr to delete</param>
        public virtual void DeleteDescr(OpDescr toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                Exception dbCollEx = null;
                bool throwDBCollEx = false;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DESCR db_descr = new DB_DESCR(toBeDeleted);
                    dbCollectorClient.DeleteDescr(ref dbCollectorSession, db_descr);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                        if (throwDBCollEx)
                            throw dbCollEx;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.DeleteDescr(toBeDeleted);
                    if (DescrCacheEnabled && toBeDeleted.IdLanguage == (int)UserLanguage)
                    {
                        DescrDict.Remove(toBeDeleted.IdDescr);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DeleteDescr", ex);
                throw ex;
            }
        }

        public virtual void ClearDescrCache()
        {
            DescrDict.Clear();
            DescrIsFullCache = false;
        }

        #region GetDescrFilter

        /// <summary>
        /// Gets Descr list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDescr funcion</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="ExtendedDescription">Specifies filter for EXTENDED_DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DESCR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Descr list</returns>
        public virtual List<OpDescr> GetDescrFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDescr = null, int[] IdLanguage = null, string Description = null, string ExtendedDescription = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDescrFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDescrFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpDescr> returnList = OpDescr.ConvertList(dbCollectorClient.GetDescrFilter(ref dbCollectorSession,
                        IdDescr, IdLanguage, Description, ExtendedDescription, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDescr> returnList = OpDescr.ConvertList(dbConnectionCore.GetDescrFilter(IdDescr, IdLanguage, Description, ExtendedDescription, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpDescr item in returnList)
                            DescrDict[item.IdDescr] = item;
                        DescrIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpDescr>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDescrFilter", ex);
                throw;
            }

        }
        #endregion

        #endregion

        #region DataTypeInGroup
        /// <summary>
        /// Indicates whether the DataTypeInGroupDict was filled trough GetAllDataTypeInGroup or GetAllDataTypeInGroupFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DataTypeInGroupIsFullCache { get; set; }

        /// <summary>
        /// DataTypeInGroup objects dictionary
        /// </summary>
        public Dictionary<int, List<OpDataTypeInGroup>> DataTypeInGroupDict = new Dictionary<int, List<OpDataTypeInGroup>>();

        /// <summary>
        /// Indicates whether the DataTypeInGroup is cached.
        /// </summary>
        public bool DataTypeInGroupCacheEnabled;

        /// <summary>
        /// Gets all DataTypeInGroup objects
        /// </summary>
        public virtual List<OpDataTypeInGroup> GetAllDataTypeInGroup()
        {
            try
            {
                if (DataTypeInGroupCacheEnabled) //cache enabled
                {
                    if (!DataTypeInGroupIsFullCache)
                    {
                        List<OpDataTypeInGroup> objectList = OpDataTypeInGroup.ConvertList(dbConnectionCore.GetDataTypeInGroup(), this);
                        if (objectList != null)
                        {
                            foreach (var item in objectList)
                            {
                                if (!DataTypeInGroupDict.ContainsKey(item.IdDataTypeGroup))
                                    DataTypeInGroupDict.Add(item.IdDataTypeGroup, new List<OpDataTypeInGroup>());

                                DataTypeInGroupDict[item.IdDataTypeGroup].Add(item);
                            }
                            DataTypeInGroupIsFullCache = true;
                        }
                    }
                    return DataTypeInGroupDict.Values.SelectMany(w => w).ToList();
                }
                else //cache disabled
                {
                    return OpDataTypeInGroup.ConvertList(dbConnectionCore.GetDataTypeInGroup(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeInGroup", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Id">DataTypeInGroup Id</param>
        /// <returns>DataTypeInGroup list</returns>
        public virtual List<OpDataTypeInGroup> GetDataTypeInGroup(int Id)
        {
            return GetDataTypeInGroup(Id, false);
        }

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DataTypeInGroup Ids</param>
        /// <returns>DataTypeInGroup list</returns>
        public virtual List<OpDataTypeInGroup> GetDataTypeInGroup(int[] Ids)
        {
            return GetDataTypeInGroup(Ids, false);
        }

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Id">DataTypeInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeInGroup list</returns>
        public virtual List<OpDataTypeInGroup> GetDataTypeInGroup(int Id, bool queryDatabase)
        {
            return GetDataTypeInGroup(new int[] { Id }, queryDatabase);
        }

        /// <summary>
        /// Gets DataTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DataTypeInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DataTypeInGroup object</returns>
        public virtual List<OpDataTypeInGroup> GetDataTypeInGroup(int[] Ids, bool queryDatabase)
        {
            List<OpDataTypeInGroup> returnList = new List<OpDataTypeInGroup>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DataTypeInGroupCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpDataTypeInGroup> DataTypeInGroupFoundInCache;
                        if (DataTypeInGroupDict.TryGetValue(Ids[i], out DataTypeInGroupFoundInCache)) //element founded in cache
                            returnList.AddRange(DataTypeInGroupFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpDataTypeInGroup> objectList = OpDataTypeInGroup.ConvertList(dbConnectionCore.GetDataTypeInGroup(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                foreach (var item in objectList)
                                {
                                    if (!DataTypeInGroupDict.ContainsKey(item.IdDataTypeGroup))
                                        DataTypeInGroupDict.Add(item.IdDataTypeGroup, new List<OpDataTypeInGroup>());

                                    DataTypeInGroupDict[item.IdDataTypeGroup].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeInGroup", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDataTypeInGroup.ConvertList(dbConnectionCore.GetDataTypeInGroup(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeInGroup", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DataTypeInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DataTypeInGroup to save</param>
        /// <returns>DataTypeInGroup Id</returns>
        public virtual int SaveDataTypeInGroup(OpDataTypeInGroup toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDataTypeInGroup(toBeSaved);
                if (DataTypeInGroupCacheEnabled)
                {
                    if (DataTypeInGroupDict.ContainsKey(toBeSaved.IdDataTypeGroup))
                    {
                        if (DataTypeInGroupDict[toBeSaved.IdDataTypeGroup].Find(f => f == toBeSaved) == null)
                            DataTypeInGroupDict[toBeSaved.IdDataTypeGroup].Add(toBeSaved); //add element
                    }
                    else
                    {
                        DataTypeInGroupDict.Add(toBeSaved.IdDataTypeGroup, new List<OpDataTypeInGroup>());
                        DataTypeInGroupDict[toBeSaved.IdDataTypeGroup].Add(toBeSaved); //add element
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDataTypeInGroup", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the DataTypeInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DataTypeInGroup to delete</param>
        public virtual void DeleteDataTypeInGroup(OpDataTypeInGroup toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteDataTypeInGroup(toBeDeleted);
                if (DataTypeInGroupCacheEnabled)
                {
                    DataTypeInGroupDict.Remove(toBeDeleted.IdDataTypeGroup);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelDataTypeInGroup", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the DataTypeInGroup object cache
        /// </summary>
        public virtual void ClearDataTypeInGroupCache()
        {
            DataTypeInGroupDict.Clear();
            DataTypeInGroupIsFullCache = false;
        }
        #endregion

        #region DepositoryElement

        public virtual void SaveMultipleDepositoryElement(List<OpDepositoryElement> toBeSaved)
        {
            try
            {
                dbConnectionCore.SaveMultipleDepositoryElement(toBeSaved.Select(d => (DB_DEPOSITORY_ELEMENT)d).ToList());
                if (DepositoryElementCacheEnabled)
                {
                    DepositoryElementIsFullCache = false;
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveMultipleDepositoryElement", ex);
                throw;
            }
        }

        public virtual List<OpDepositoryElement> GetTaskPackageDataForDistributor(List<int> IdDistributor, bool loadDeviceDetails = true, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (DepositoryElementCacheEnabled) //cache enabled
                {
                    List<OpDepositoryElement> objectList = null;
                    if (!DepositoryElementIsFullCache)
                    {
                        objectList = OpDepositoryElement.ConvertList(dbConnectionCore.GetTaskPackageDataForDistributor(IdDistributor.ToArray(), autoTransaction, transactionLevel, commandTimeout), this, loadDeviceDetails);
                        if (objectList != null)
                        {
                            foreach (OpDepositoryElement deItem in objectList)
                            {
                                if (DepositoryElementDict.ContainsKey(deItem.IdDepositoryElement))
                                    DepositoryElementDict[deItem.IdDepositoryElement] = deItem;
                                else
                                    DepositoryElementDict.Add(deItem.IdDepositoryElement, deItem);
                            }
                        }
                    }
                    return objectList;
                }
                else //cache disabled
                {
                    return OpDepositoryElement.ConvertList(dbConnectionCore.GetTaskPackageDataForDistributor(IdDistributor.ToArray()), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetTaskPackageDataForDistributor", ex);
                throw;
            }
        }

        public virtual DataTable GetMountedDevicesFromServicePool(int idDistributor, long serialNbrService)
        {
            try
            {
                return dbConnectionCore.GetMountedDevicesFromServicePool(idDistributor, serialNbrService);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetMountedDevicesFromServicePool", ex);
                throw;
            }
        }

        #endregion

        #region Device

        #region CheckDeviceState
        /// <summary>
        /// CheckDeviceState cheks if device has correct ID_DEVICE_STATE
        /// if has TESTED_OK on RadioDeviceTester
        /// </summary>
        /// <param name="serialNbr">Device serial number</param>
        /// <returns>1- device has correct params, 2 - device doesn't exists, 3 - wrong state type, 
        /// 4 - device doesn't exists on RadioDeviceTester, 5 - device has TESTED_OK = 0
        /// </returns>
        public virtual int CheckDeviceState(string serialNbr, int statusValidateOption = 0, object statusValidateParam = null)
        {
            return dbConnectionCore.CheckDeviceState(serialNbr, statusValidateOption, statusValidateParam);
        }
        #endregion

        #region FindDevice

        public virtual bool FindDevice(long serialNbr)
        {
            return dbConnectionCore.FindDevice(serialNbr);
        }

        #endregion

        #region CheckIfDeviceHasShippingList

        public virtual bool CheckIfDeviceHasShippingList(long serialNbr)
        {
            return dbConnectionCore.CheckIfDeviceHasShippingList(serialNbr);
        }

        #endregion

        public virtual DataTable GetMirroringDepositoryElements()//int idDistributor)
        {
            try
            {
                return dbConnectionCore.GetMirroringDepositoryElements();//idDistributor);
            }

            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_MirroringDepositoryElements", ex);
                throw;
            }
        }

        #region CheckDataStatusForDevice
        public virtual void CheckDataStatusForDevice(ulong? serialNbr)
        {
            dbConnectionCore.CheckDataStatusForDevice(serialNbr);
        }
        #endregion

        #region GetDeviceBatchNumbers

        public virtual List<string> GetDeviceBatchNumbers(bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                return dbConnectionCore.GetDeviceBatchNumbers(autoTransaction, transactionLevel, commandTimeout);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceBatchNumbers", ex);
                throw;
            }
        }

        #endregion

        #region GetDeviceManufacturingBatchNumbers

        public virtual List<string> GetDeviceManufacturingBatchNumbers(bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                return dbConnectionCore.GetDeviceManufacturingBatchNumbers(autoTransaction, transactionLevel, commandTimeout);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceManufacturingBatchNumbers", ex);
                throw;
            }
        }

        #endregion

        #region SaveDevice
        /// <summary>
        /// Saves the Device object to the database
        /// </summary>
        /// <param name="toBeSaved">Device to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>Device Id</returns>
        public virtual long SaveDevice(OpDevice toBeSaved, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DEVICE db_device = new DB_DEVICE(toBeSaved);
                    long ret = dbCollectorClient.SaveDevice(ref dbCollectorSession, db_device);
                    if (ret == 0)
                        return toBeSaved.SerialNbr;
                    return ret;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveDevice(toBeSaved);
                    if (DeviceCacheEnabled)
                    {
                        DeviceDict[toBeSaved.SerialNbr] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                else
                    return toBeSaved.SerialNbr;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDevice", ex);
                throw;
            }
        }
        #endregion
        #endregion

        #region DeviceDetails

        #region GetDeviceDetailsFilter

        /// <summary>
        /// Gets DeviceDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDetails funcion</param>
        /// <param name="IdDeviceDetails">Specifies filter for ID_DEVICE_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="FactoryNbr">Specifies filter for FACTORY_NBR column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="WarrantyDate">Specifies filter for WARRANTY_DATE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="ProductionDeviceOrderNumber">Specifies filter for PRODUCTION_DEVICE_ORDER_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDetails list</returns>
        public virtual List<OpDeviceDetails> GetDeviceDetailsFilterUser(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDetails = null, long[] SerialNbr = null, long[] IdLocation = null, string FactoryNbr = null, TypeDateTimeCode ShippingDate = null, TypeDateTimeCode WarrantyDate = null,
                    string Phone = null, int[] IdDeviceType = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, int[] IdDeviceStateType = null,
                    string ProductionDeviceOrderNumber = null, string DeviceBatchNumber = null, int[] IdDistributorOwner = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDetailsFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDetailsFilter"));
            }

            try
            {
                List<OpDeviceDetails> returnList = OpDeviceDetails.ConvertList(dbConnectionCore.GetDeviceDetailsFilterUser(IdDeviceDetails, SerialNbr, IdLocation, FactoryNbr, ShippingDate, WarrantyDate,
                     Phone, IdDeviceType, IdDeviceOrderNumber, IdDistributor, IdDeviceStateType,
                     ProductionDeviceOrderNumber, DeviceBatchNumber, IdDistributorOwner, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceDetails item in returnList)
                        DeviceDetailsDict[item.IdDeviceDetails] = item;
                    DeviceDetailsIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceDetailsFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceDetailsFilterCustom
        /// <summary>
        /// Gets DeviceDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDetails funcion</param>
        /// <param name="IdDeviceDetails">Specifies filter for ID_DEVICE_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="FactoryNbr">Specifies filter for FACTORY_NBR column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="WarrantyDate">Specifies filter for WARRANTY_DATE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="ProductionDeviceOrderNumber">Specifies filter for PRODUCTION_DEVICE_ORDER_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDetails list</returns>
        public virtual List<OpDeviceDetails> GetDeviceDetailsFilterCustom(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDetails = null, long[] SerialNbr = null, long[] IdLocation = null, string FactoryNbr = null, TypeDateTimeCode ShippingDate = null, TypeDateTimeCode WarrantyDate = null,
                    string Phone = null, int[] IdDeviceType = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, int[] IdDeviceStateType = null,
                    string ProductionDeviceOrderNumber = null, string DeviceBatchNumber = null, int[] IdDistributorOwner = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDetailsFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDetailsFilter"));
            }

            try
            {
                List<OpDeviceDetails> returnList = OpDeviceDetails.ConvertList(dbConnectionCore.GetDeviceDetailsFilterUser(IdDeviceDetails, SerialNbr, IdLocation, FactoryNbr, ShippingDate, WarrantyDate,
                     Phone, IdDeviceType, IdDeviceOrderNumber, IdDistributor, IdDeviceStateType,
                     ProductionDeviceOrderNumber, DeviceBatchNumber, IdDistributorOwner, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceDetails item in returnList)
                    {
                        if (item.SerialNbr.HasValue)
                            DeviceDetailsBySerialNbrDict[item.SerialNbr.Value] = item;
                    }
                    DeviceDetailsIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceDetailsFilter", ex);
                throw;
            }

        }
        #endregion

        #region DevicesInvoice

        public virtual DataTable DevicesInvoice(int IdDistributor, DateTime? ReferenceDate = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                return dbConnectionCore.DevicesInvoice(IdDistributor, ReferenceDate, autoTransaction, transactionLevel, commandTimeout);

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "sp_DevicesInvoice", ex);
                throw;
            }

        }
        #endregion

        #region GetDistributorAllowedDeviceAttributes

        public virtual Tuple<List<OpDeviceType>, List<OpDeviceOrderNumber>, List<string>, List<OpFaultCode>, List<OpShippingList>, List<string>> GetDistributorAllowedDeviceAttributes(int[] IdDistributor = null, bool loadNavigationProperties = true, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                Tuple<List<DB_DEVICE_TYPE>, List<DB_DEVICE_ORDER_NUMBER>, List<string>, List<DB_FAULT_CODE>, List<DB_SHIPPING_LIST>, List<string>> retDB = dbConnectionCore.GetDistributorAllowedDeviceAttributes(IdDistributor, autoTransaction, transactionLevel, commandTimeout);
                Tuple<List<OpDeviceType>, List<OpDeviceOrderNumber>, List<string>, List<OpFaultCode>, List<OpShippingList>, List<string>> retOp = new Tuple<List<OpDeviceType>, List<OpDeviceOrderNumber>, List<string>, List<OpFaultCode>, List<OpShippingList>, List<string>>(new List<OpDeviceType>(), new List<OpDeviceOrderNumber>(), new List<string>(), new List<OpFaultCode>(), new List<OpShippingList>(), new List<string>());
                retOp.Item1.AddRange(OpDeviceType.ConvertList(retDB.Item1.ToArray(), this, loadNavigationProperties));
                retOp.Item2.AddRange(OpDeviceOrderNumber.ConvertList(retDB.Item2.ToArray(), this, loadNavigationProperties));
                retOp.Item3.AddRange(retDB.Item3);
                retOp.Item4.AddRange(OpFaultCode.ConvertList(retDB.Item4.ToArray(), this, loadNavigationProperties));
                retOp.Item5.AddRange(OpShippingList.ConvertList(retDB.Item5.ToArray(), this, loadNavigationProperties));
                retOp.Item6.AddRange(retDB.Item6);
                return retOp;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDistributorAllowedDeviceAttributes", ex);
                throw;
            }

        }
        #endregion

        #region SaveDeviceDetails
        public virtual void SaveDeviceDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveDeviceDetails(changeFromLastMinutes, particularUnits, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateDeviceDetails", ex);
                throw;
            }
        }
        #endregion

        #endregion        

        #region DeviceDriverMemoryMap

        #region GetDeviceDriverMemoryMapFilter

        /// <summary>
        /// Gets DeviceDriverMemoryMap list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriverMemoryMap funcion</param>
        /// <param name="IdDeviceDriverMemoryMap">Specifies filter for ID_DEVICE_DRIVER_MEMORY_MAP column</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="FileName">Specifies filter for FILE_NAME column</param>
        /// <param name="FileVersion">Specifies filter for FILE_VERSION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER_MEMORY_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DeviceDriverMemoryMap list</returns>
        public virtual List<OpDeviceDriverMemoryMap> GetDeviceDriverMemoryMapFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceDriverMemoryMap = null, int[] IdDeviceDriver = null, string FileName = null, string FileVersion = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
         bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDriverMemoryMapFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverMemoryMapFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpDeviceDriverMemoryMap.ConvertList(dbCollectorClient.GetDeviceDriverMemoryMapFilter(ref dbCollectorSession, IdDeviceDriverMemoryMap, IdDeviceDriver, FileName, FileVersion, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDeviceDriverMemoryMap> returnList = OpDeviceDriverMemoryMap.ConvertList(dbConnectionCore.GetDeviceDriverMemoryMapFilter(IdDeviceDriverMemoryMap, IdDeviceDriver, FileName, FileVersion, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpDeviceDriverMemoryMap item in returnList)
                            DeviceDriverMemoryMapDict[item.IdDeviceDriverMemoryMap] = item;
                        DeviceDriverMemoryMapIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpDeviceDriverMemoryMap>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceDriverMemoryMapFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceDriverMemoryMapDataFilter

        /// <summary>
        /// Gets DeviceDriverMemoryMapData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriverMemoryMapData funcion</param>
        /// <param name="IdDeviceDriverMemoryMapData">Specifies filter for ID_DEVICE_DRIVER_MEMORY_MAP_DATA column</param>
        /// <param name="IdDdmmdParent">Specifies filter for ID_DDMMD_PARENT column</param>
        /// <param name="IdDeviceDriverMemoryMap">Specifies filter for ID_DEVICE_DRIVER_MEMORY_MAP column</param>
        /// <param name="IdOmb">Specifies filter for ID_OMB column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="IsRead">Specifies filter for IS_READ column</param>
        /// <param name="IsWrite">Specifies filter for IS_WRITE column</param>
        /// <param name="IsPutDataOmb">Specifies filter for IS_PUT_DATA_OMB column</param>
        /// <param name="IdDdmmdPutData">Specifies filter for ID_DDMMD_PUT_DATA column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER_MEMORY_MAP_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DeviceDriverMemoryMapData list</returns>
        public virtual List<OpDeviceDriverMemoryMapData> GetDeviceDriverMemoryMapDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDriverMemoryMapData = null, long[] IdDdmmdParent = null, int[] IdDeviceDriverMemoryMap = null, int[] IdOmb = null, string Name = null, long[] IdDataType = null,
                            int[] IndexNbr = null, bool? IsRead = null, bool? IsWrite = null, bool? IsPutDataOmb = null, long[] IdDdmmdPutData = null,
                    long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                    bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDriverMemoryMapDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverMemoryMapDataFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpDeviceDriverMemoryMapData.ConvertList(dbCollectorClient.GetDeviceDriverMemoryMapDataFilter(ref dbCollectorSession, IdDeviceDriverMemoryMapData, IdDdmmdParent, IdDeviceDriverMemoryMap, IdOmb, Name, IdDataType,
                                 IndexNbr, IsRead, IsWrite, IsPutDataOmb, IdDdmmdPutData,
                         IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDeviceDriverMemoryMapData> returnList = OpDeviceDriverMemoryMapData.ConvertList(dbConnectionCore.GetDeviceDriverMemoryMapDataFilter(IdDeviceDriverMemoryMapData, IdDdmmdParent, IdDeviceDriverMemoryMap, IdOmb, Name, IdDataType,
                                 IndexNbr, IsRead, IsWrite, IsPutDataOmb, IdDdmmdPutData,
                         IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpDeviceDriverMemoryMapData item in returnList)
                            DeviceDriverMemoryMapDataDict[item.IdDeviceDriverMemoryMapData] = item;
                        DeviceDriverMemoryMapDataIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpDeviceDriverMemoryMapData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceDriverMemoryMapDataFilter", ex);
                throw;
            }

        }
        #endregion

        #endregion

        #region DeviceHierarchy

        #region SaveDeviceHierarchy
        /// <summary>
        /// Saves the DeviceHierarchy object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceHierarchy to save</param>
        /// <returns>DeviceHierarchy Id</returns>
        public virtual long SaveDeviceHierarchy(OpDeviceHierarchy toBeSaved, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DEVICE_HIERARCHY hierarchy = new DB_DEVICE_HIERARCHY(toBeSaved);
                    hierarchy.ID_DEVICE_HIERARCHY = dbCollectorClient.SaveDeviceHierarchy(ref dbCollectorSession, hierarchy);
                    if (DeviceHierarchyCacheEnabled)
                    {
                        DeviceHierarchyDict[toBeSaved.IdDeviceHierarchy] = toBeSaved; //add or update element
                    }
                    return hierarchy.ID_DEVICE_HIERARCHY;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveDeviceHierarchy(toBeSaved);
                    if (DeviceHierarchyCacheEnabled)
                    {
                        DeviceHierarchyDict[toBeSaved.IdDeviceHierarchy] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                return
                    toBeSaved.IdDeviceHierarchy;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeviceHierarchy", ex);
                throw;
            }
        }
        #endregion

        #endregion

        #region DevicePattern

        public Dictionary<long, long> GetDevicesPattern()
        {
            try
            {
                return dbConnectionCore.GetDevicesPattern();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDevicePattern", ex);
                throw;
            }

        }

        #region GetDevicePattern

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Id">DevicePattern Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern object</returns>
        public virtual OpDevicePattern GetDevicePattern(long Id, bool useDBCollector = false)
        {
            return GetDevicePattern(Id, false, useDBCollector);
        }

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Ids">DevicePattern Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern list</returns>
        public virtual List<OpDevicePattern> GetDevicePattern(long[] Ids, bool useDBCollector = false)
        {
            return GetDevicePattern(Ids, false, useDBCollector);
        }

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Id">DevicePattern Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern object</returns>
        public virtual OpDevicePattern GetDevicePattern(long Id, bool queryDatabase, bool useDBCollector = false)
        {
            return GetDevicePattern(new long[] { Id }, queryDatabase, useDBCollector).FirstOrDefault();
        }

        /// <summary>
        /// Gets DevicePattern by id
        /// </summary>
        /// <param name="Ids">DevicePattern Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern list</returns>
        public virtual List<OpDevicePattern> GetDevicePattern(long[] Ids, bool queryDatabase, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpDevicePattern.ConvertList(dbCollectorClient.GetDevicePattern(ref dbCollectorSession, Ids), this);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDevicePattern> returnList = new List<OpDevicePattern>();
                    if (Ids != null && Ids.Length > 0)
                    {
                        if (DevicePatternCacheEnabled && !queryDatabase)// cache enabled and query database is false
                        {
                            List<long> idsNotInCache = new List<long>();
                            for (int i = 0; i < Ids.Length; i++)
                            {
                                OpDevicePattern DevicePatternFoundInCache;
                                if (DevicePatternDict.TryGetValue(Ids[i], out DevicePatternFoundInCache)) //element founded in cache
                                    returnList.Add(DevicePatternFoundInCache);
                                else //element not found in cache, query database
                                    idsNotInCache.Add(Ids[i]);
                            }
                            if (idsNotInCache.Count > 0)
                            {
                                try
                                {
                                    foreach (OpDevicePattern item in OpDevicePattern.ConvertList(dbConnectionCore.GetDevicePattern(idsNotInCache.ToArray()), this))
                                    {
                                        DevicePatternDict[item.IdPattern] = item;
                                        returnList.Add(item);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePattern", ex);
                                    throw;
                                }
                            }
                        }
                        else //cache disabled or user force to query database 
                        {
                            try
                            {
                                return OpDevicePattern.ConvertList(dbConnectionCore.GetDevicePattern(Ids), this);
                            }
                            catch (Exception ex)
                            {
                                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePattern", ex);
                                throw;
                            }
                        }
                    }
                    return returnList;
                }
                else
                    return new List<OpDevicePattern>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePattern", ex);
                throw;
            }
        }

        #endregion

        #region GetDevicePatternData

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Id">DevicePatternData Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData object</returns>
        public virtual List<OpDevicePatternData> GetDevicePatternData(long Id, bool useDBCollector = false)
        {
            return GetDevicePatternData(Id, true, useDBCollector);
        }

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Ids">DevicePatternData Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData list</returns>
        public virtual List<OpDevicePatternData> GetDevicePatternData(long[] Ids, bool useDBCollector = false)
        {
            return GetDevicePatternData(Ids, true, useDBCollector);
        }

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Id">DevicePatternData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData object</returns>
        public virtual List<OpDevicePatternData> GetDevicePatternData(long Id, bool queryDatabase, bool useDBCollector = false)
        {
            return GetDevicePatternData(new long[] { Id }, queryDatabase, useDBCollector);
        }

        /// <summary>
        /// Gets DevicePatternData by id
        /// </summary>
        /// <param name="Ids">DevicePatternData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePatternData list</returns>
        public virtual List<OpDevicePatternData> GetDevicePatternData(long[] Ids, bool queryDatabase, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpDevicePatternData.ConvertList(dbCollectorClient.GetDevicePatternData(ref dbCollectorSession, Ids), this);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDevicePatternData> returnList = new List<OpDevicePatternData>();
                    if (Ids != null && Ids.Length > 0)
                    {
                        if (DevicePatternDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                        {
                            List<long> idsNotInCache = new List<long>();
                            //                    for (int i = 0; i < Ids.Length; i++)
                            //                    {
                            //                        OpDevicePatternData DevicePatternDataFoundInCache;
                            //                        if (DevicePatternDataDict.TryGetValue(Ids[i], out DevicePatternDataFoundInCache)) //element founded in cache
                            //                            returnList.Add(DevicePatternDataFoundInCache);
                            //                        else //element not found in cache, query database
                            //                            idsNotInCache.Add(Ids[i]);
                            //                    }
                            if (idsNotInCache.Count > 0)
                            {
                                try
                                {
                                    foreach (OpDevicePatternData item in OpDevicePatternData.ConvertList(dbConnectionCore.GetDevicePatternData(idsNotInCache.ToArray()), this))
                                    {
                                        DevicePatternDataDict[item.IdPattern] = item;
                                        returnList.Add(item);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePatternData", ex);
                                    throw;
                                }
                            }
                        }
                        else //cache disabled or user force to query database 
                        {
                            try
                            {
                                return OpDevicePatternData.ConvertList(dbConnectionCore.GetDevicePatternData(Ids), this);
                            }
                            catch (Exception ex)
                            {
                                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePatternData", ex);
                                throw;
                            }
                        }
                    }
                    return returnList;
                }
                else
                    return new List<OpDevicePatternData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePatternData", ex);
                throw;
            }
        }

        #endregion

        #region GetDevicePatternFilter

        /// <summary>
        /// Gets DevicePattern list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDevicePattern funcion</param>
        /// <param name="IdPattern">Specifies filter for ID_PATTERN column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="CreatedBy">Specifies filter for CREATED_BY column</param>
        /// <param name="IdTemplate">Specifies filter for ID_TEMPLATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PATTERN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>DevicePattern list</returns>
        public virtual List<OpDevicePattern> GetDevicePatternFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPattern = null, long[] SerialNbr = null, int[] IdDistributor = null, string Name = null, TypeDateTimeCode CreationDate = null, int[] CreatedBy = null,
                            int[] IdTemplate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                            bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDevicePatternFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDevicePatternFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpDevicePattern.ConvertList(dbCollectorClient.GetDevicePatternFilter(ref dbCollectorSession, IdPattern, SerialNbr, IdDistributor, Name, CreationDate, CreatedBy,
                                 IdTemplate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDevicePattern> returnList = OpDevicePattern.ConvertList(dbConnectionCore.GetDevicePatternFilter(IdPattern, SerialNbr, IdDistributor, Name, CreationDate, CreatedBy,
                                 IdTemplate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpDevicePattern item in returnList)
                            DevicePatternDict[item.IdPattern] = item;
                        DevicePatternIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpDevicePattern>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePatternFilter", ex);
                throw;
            }

        }
        #endregion

        #endregion

        #region DeviceStateHistory
        #region GetDeviceStateHistoryFilter
        /// <summary>
        /// Gets DeviceStateHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceStateHistory funcion</param>
        /// <param name="IdDeviceState">Specifies filter for ID_DEVICE_STATE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_STATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceStateHistory list</returns>
        public virtual List<OpDeviceStateHistory> GetDeviceStateHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceState = null, long[] SerialNbr = null, int[] IdDeviceStateType = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceStateHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceStateHistoryFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpDeviceStateHistory> returnList = OpDeviceStateHistory.ConvertList(dbCollectorClient.GetDeviceStateHistoryFilter(ref dbCollectorSession,
                        IdDeviceState, SerialNbr, IdDeviceStateType, StartTime, EndTime, Notes,
                        topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpDeviceStateHistory> returnList = OpDeviceStateHistory.ConvertList(dbConnectionCore.GetDeviceStateHistoryFilter(IdDeviceState, SerialNbr, IdDeviceStateType, StartTime, EndTime, Notes,
                                 topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpDeviceStateHistory item in returnList)
                            DeviceStateHistoryDict[item.IdDeviceState] = item;
                        DeviceStateHistoryIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpDeviceStateHistory>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceStateHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceStateHistoryByDistributor

        public virtual List<OpDeviceStateHistory> GetDeviceStateHistoryByDistributor(int[] IdDistributor,
                                                                             bool IncludeShipping = true,
                                                                             bool IncludeService = true,
                                                                             bool loadNavigationProperties = false)
        {
            List<OpDeviceStateHistory> returnList = new List<OpDeviceStateHistory>();
            if (IdDistributor != null && IdDistributor.Length > 0)
            {
                try
                {
                    returnList = OpDeviceStateHistory.ConvertList(dbConnectionCore.GetDeviceStateHistoryByDistributor(IdDistributor, IncludeShipping, IncludeService), this, loadNavigationProperties);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceStateHistoryByDistributor", ex);
                    throw;
                }

                if (DeviceStateHistoryCacheEnabled && returnList != null)// cache enabled and query database is false
                {
                    foreach (OpDeviceStateHistory item in returnList)
                    {
                        DeviceStateHistoryDict[item.IdDeviceState] = item;
                    }
                }

            }
            return returnList;
        }
        #endregion
        #region SaveDeviceStateHistory
        /// <summary>
        /// Saves the DeviceStateHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceStateHistory to save</param>
        /// <returns>DeviceStateHistory Id</returns>
        public virtual long SaveDeviceStateHistory(OpDeviceStateHistory toBeSaved, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DEVICE_STATE_HISTORY db_deviceStateHistory = new DB_DEVICE_STATE_HISTORY(toBeSaved);
                    long ret = dbCollectorClient.SaveDeviceStateHistory(ref dbCollectorSession, db_deviceStateHistory);
                    if (ret == 0)
                        return toBeSaved.IdDeviceState;
                    return ret;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveDeviceStateHistory(toBeSaved);
                    if (DeviceStateHistoryCacheEnabled)
                    {
                        DeviceStateHistoryDict[toBeSaved.IdDeviceState] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                else
                    return toBeSaved.IdDeviceState;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveDeviceStateHistory", ex);
                throw;
            }
        }
        #endregion

        #region DeleteDeviceStateHistory

        /// <summary>
        /// Deletes the DeviceStateHistory object from the database
        /// </summary>
        /// <param name="useDBCollector">Indicates whether data should be deleted from connected data base or from DBCollector web service</param>
        /// <param name="toBeDeleted">DeviceStateHistory to delete</param>
        public virtual void DeleteDeviceStateHistory(OpDeviceStateHistory toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DEVICE_STATE_HISTORY history = new DB_DEVICE_STATE_HISTORY(toBeDeleted);
                    dbCollectorClient.DeleteDeviceStateHistory(ref dbCollectorSession, history);
                }
                else
                {
                    dbConnectionCore.DeleteDeviceStateHistory(toBeDeleted);
                }

                if (DeviceStateHistoryCacheEnabled)
                {
                    DeviceStateHistoryDict.Remove(toBeDeleted.IdDeviceState);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDeviceStateHistory", ex);
                throw;
            }
        }

        #endregion

        #endregion

        #region DeviceStateType
        /// <summary>
        /// Saves or adds the DeviceStateType object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceStateType to save</param>
        /// <returns>DeviceStateType Id</returns>
        public virtual int SaveDeviceStateType(OpDeviceStateType toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDeviceStateType(toBeSaved);
                if (DeviceStateTypeCacheEnabled)
                {
                    DeviceStateTypeDict[toBeSaved.IdDeviceStateType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDeviceStateType", ex);
                throw;
            }
        }
        #endregion

        #region DeviceTemplate
        #region DeviceTemplate
        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Id">DeviceTemplate Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate object</returns>
        public virtual OpDeviceTemplate GetDeviceTemplate(int Id, bool useDBCollector = false)
        {
            return GetDeviceTemplate(Id, false, useDBCollector);
        }

        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Ids">DeviceTemplate Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate list</returns>
        public virtual List<OpDeviceTemplate> GetDeviceTemplate(int[] Ids, bool useDBCollector = false)
        {
            return GetDeviceTemplate(Ids, false, useDBCollector);
        }

        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Id">DeviceTemplate Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate object</returns>
        public virtual OpDeviceTemplate GetDeviceTemplate(int Id, bool queryDatabase, bool useDBCollector = false)
        {
            return GetDeviceTemplate(new int[] { Id }, queryDatabase, useDBCollector).FirstOrDefault();
        }

        /// <summary>
        /// Gets DeviceTemplate by id
        /// </summary>
        /// <param name="Ids">DeviceTemplate Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplate list</returns>
        public virtual List<OpDeviceTemplate> GetDeviceTemplate(int[] Ids, bool queryDatabase, bool useDBCollector = false)
        {
            bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
            if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
            {
                try
                {
                    return OpDeviceTemplate.ConvertList(dbCollectorClient.GetDeviceTemplate(ref dbCollectorSession, Ids), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplate", ex);
                    throw;
                }
            }
            else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
            {
                List<OpDeviceTemplate> returnList = new List<OpDeviceTemplate>();
                if (Ids != null && Ids.Length > 0)
                {
                    if (DeviceTemplateCacheEnabled && !queryDatabase)// cache enabled and query database is false
                    {
                        List<int> idsNotInCache = new List<int>();
                        for (int i = 0; i < Ids.Length; i++)
                        {
                            OpDeviceTemplate DeviceTemplateFoundInCache;
                            if (DeviceTemplateDict.TryGetValue(Ids[i], out DeviceTemplateFoundInCache)) //element founded in cache
                                returnList.Add(DeviceTemplateFoundInCache);
                            else //element not found in cache, query database
                                idsNotInCache.Add(Ids[i]);
                        }
                        if (idsNotInCache.Count > 0)
                        {
                            try
                            {
                                foreach (OpDeviceTemplate item in OpDeviceTemplate.ConvertList(dbConnectionCore.GetDeviceTemplate(idsNotInCache.ToArray()), this))
                                {
                                    DeviceTemplateDict[item.IdTemplate] = item;
                                    returnList.Add(item);
                                }
                            }
                            catch (Exception ex)
                            {
                                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplate", ex);
                                throw;
                            }
                        }
                    }
                    else //cache disabled or user force to query database 
                    {
                        try
                        {
                            return OpDeviceTemplate.ConvertList(dbConnectionCore.GetDeviceTemplate(Ids), this);
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplate", ex);
                            throw;
                        }
                    }
                }
                return returnList;
            }
            else
                return new List<OpDeviceTemplate>();
        }
        #endregion
        #region DeviceTemplateData
        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Id">DeviceTemplateData Id</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData object</returns>
        public virtual List<OpDeviceTemplateData> GetDeviceTemplateData(int Id, bool useDBCollector = false)
        {
            return GetDeviceTemplateData(Id, true, useDBCollector);
        }

        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Ids">DeviceTemplateData Ids</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData list</returns>
        public virtual List<OpDeviceTemplateData> GetDeviceTemplateData(int[] Ids, bool useDBCollector = false)
        {
            return GetDeviceTemplateData(Ids, true, useDBCollector);
        }

        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Id">DeviceTemplateData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData object</returns>
        public virtual List<OpDeviceTemplateData> GetDeviceTemplateData(int Id, bool queryDatabase, bool useDBCollector = false)
        {
            return GetDeviceTemplateData(new int[] { Id }, queryDatabase, useDBCollector);
        }

        /// <summary>
        /// Gets DeviceTemplateData by id
        /// </summary>
        /// <param name="Ids">DeviceTemplateData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>DeviceTemplateData list</returns>
        public virtual List<OpDeviceTemplateData> GetDeviceTemplateData(int[] Ids, bool queryDatabase, bool useDBCollector = false)
        {
            bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
            if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
            {
                try
                {
                    return OpDeviceTemplateData.ConvertList(dbCollectorClient.GetDeviceTemplateData(ref dbCollectorSession, Ids), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplateData", ex);
                    throw;
                }
            }
            else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
            {
                List<OpDeviceTemplateData> returnList = new List<OpDeviceTemplateData>();
                if (Ids != null && Ids.Length > 0)
                {
                    if (DeviceTemplateDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                    {
                        List<int> idsNotInCache = new List<int>();
                        //for (int i = 0; i < Ids.Length; i++)
                        //{
                        //    OpDeviceTemplateData DeviceTemplateDataFoundInCache;
                        //    if (DeviceTemplateDataDict.TryGetValue(Ids[i], out DeviceTemplateDataFoundInCache)) //element founded in cache
                        //        returnList.Add(DeviceTemplateDataFoundInCache);
                        //    else //element not found in cache, query database
                        //        idsNotInCache.Add(Ids[i]);
                        //}
                        if (idsNotInCache.Count > 0)
                        {
                            try
                            {
                                foreach (OpDeviceTemplateData item in OpDeviceTemplateData.ConvertList(dbConnectionCore.GetDeviceTemplateData(idsNotInCache.ToArray()), this))
                                {
                                    DeviceTemplateDataDict[item.IdTemplate] = item;
                                    returnList.Add(item);
                                }
                            }
                            catch (Exception ex)
                            {
                                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplateData", ex);
                                throw;
                            }
                        }
                    }
                    else //cache disabled or user force to query database 
                    {
                        try
                        {
                            return OpDeviceTemplateData.ConvertList(dbConnectionCore.GetDeviceTemplateData(Ids), this);
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplateData", ex);
                            throw;
                        }
                    }
                }
                return returnList;
            }
            else
                return new List<OpDeviceTemplateData>();
        }
        #endregion
        #endregion

        #region DeviceType
        /// <summary>
        /// Saves the DeviceType object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceType to save</param>
        /// <returns>DeviceType Id</returns>
        public virtual int SaveDeviceType(OpDeviceType toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDeviceType(toBeSaved);
                if (DeviceTypeCacheEnabled)
                {
                    DeviceTypeDict[toBeSaved.IdDeviceType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDeviceType", ex);
                throw;
            }
        }
        #endregion

        #region DeviceTypeClass
        /// <summary>
        /// Saves the DeviceTypeClass object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeClass to save</param>
        /// <returns>DeviceTypeClass Id</returns>
        public virtual int SaveDeviceTypeClass(OpDeviceTypeClass toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDeviceTypeClass(toBeSaved);
                if (DeviceTypeClassCacheEnabled)
                {
                    DeviceTypeClassDict[toBeSaved.IdDeviceTypeClass] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDeviceTypeClass", ex);
                throw;
            }
        }
        #endregion

        #region DeviceTypeInGroup
        /// <summary>
        /// Indicates whether the DeviceTypeInGroupDict was filled trough GetAllDeviceTypeInGroup or GetAllDeviceTypeInGroupFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DeviceTypeInGroupIsFullCache { get; set; }

        /// <summary>
        /// DeviceTypeInGroup objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, List<OpDeviceTypeInGroup>> DeviceTypeInGroupDict = new Dictionary<int, List<OpDeviceTypeInGroup>>();

        /// <summary>
        /// Indicates whether the DeviceTypeInGroup is cached.
        /// </summary>
        public virtual bool DeviceTypeInGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all DeviceTypeInGroup objects
        /// </summary>
        public virtual List<OpDeviceTypeInGroup> DeviceTypeInGroup
        {
            get { return GetAllDeviceTypeInGroup(); }
        }

        /// <summary>
        /// Gets all DeviceTypeInGroup objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        public virtual List<OpDeviceTypeInGroup> GetAllDeviceTypeInGroup(bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpDeviceTypeInGroup.ConvertList(dbCollectorClient.GetAllDeviceTypeInGroup(ref dbCollectorSession), this);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (DeviceTypeInGroupCacheEnabled) //cache enabled
                    {
                        if (!DeviceTypeInGroupIsFullCache)
                        {
                            List<OpDeviceTypeInGroup> objectList = OpDeviceTypeInGroup.ConvertList(dbConnectionCore.GetDeviceTypeInGroup(), this);
                            if (objectList != null)
                            {
                                DeviceTypeInGroupDict = new Dictionary<int, List<OpDeviceTypeInGroup>>();
                                foreach (var item in objectList)
                                {
                                    if (!DeviceTypeInGroupDict.ContainsKey(item.IdDeviceTypeGroup))
                                        DeviceTypeInGroupDict.Add(item.IdDeviceTypeGroup, new List<OpDeviceTypeInGroup>());

                                    DeviceTypeInGroupDict[item.IdDeviceTypeGroup].Add(item);
                                }
                                DeviceTypeInGroupIsFullCache = true;
                            }
                        }
                        return DeviceTypeInGroupDict.Values.SelectMany(w => w).ToList();
                    }
                    else //cache disabled
                    {
                        return OpDeviceTypeInGroup.ConvertList(dbConnectionCore.GetDeviceTypeInGroup(), this);
                    }
                }
                else
                    return new List<OpDeviceTypeInGroup>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeInGroup", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Id">DeviceTypeInGroup Id</param>
        /// <returns>DeviceTypeInGroup list</returns>
        public virtual List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int Id)
        {
            return GetDeviceTypeInGroup(Id, false);
        }

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DeviceTypeInGroup Ids</param>
        /// <returns>DeviceTypeInGroup list</returns>
        public virtual List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int[] Ids)
        {
            return GetDeviceTypeInGroup(Ids, false);
        }

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Id">DeviceTypeInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeInGroup list</returns>
        public virtual List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int Id, bool queryDatabase)
        {
            return GetDeviceTypeInGroup(new int[] { Id }, queryDatabase);
        }

        /// <summary>
        /// Gets DeviceTypeInGroup by id
        /// </summary>
        /// <param name="Ids">DeviceTypeInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>DeviceTypeInGroup list</returns>
        public virtual List<OpDeviceTypeInGroup> GetDeviceTypeInGroup(int[] Ids, bool queryDatabase)
        {
            List<OpDeviceTypeInGroup> returnList = new List<OpDeviceTypeInGroup>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeviceTypeInGroupCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpDeviceTypeInGroup> DeviceTypeInGroupFoundInCache;
                        if (DeviceTypeInGroupDict.TryGetValue(Ids[i], out DeviceTypeInGroupFoundInCache)) //element founded in cache
                            returnList.AddRange(DeviceTypeInGroupFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpDeviceTypeInGroup> objectList = OpDeviceTypeInGroup.ConvertList(dbConnectionCore.GetDeviceTypeInGroup(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                DeviceTypeInGroupDict = new Dictionary<int, List<OpDeviceTypeInGroup>>();
                                foreach (var item in objectList)
                                {
                                    if (!DeviceTypeInGroupDict.ContainsKey(item.IdDeviceTypeGroup))
                                        DeviceTypeInGroupDict.Add(item.IdDeviceTypeGroup, new List<OpDeviceTypeInGroup>());

                                    DeviceTypeInGroupDict[item.IdDeviceTypeGroup].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeInGroup", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDeviceTypeInGroup.ConvertList(dbConnectionCore.GetDeviceTypeInGroup(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeInGroup", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the DeviceTypeInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeInGroup to save</param>
        /// <returns>DeviceTypeInGroup Id</returns>
        public virtual int SaveDeviceTypeInGroup(OpDeviceTypeInGroup toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDeviceTypeInGroup(toBeSaved);
                if (DeviceTypeInGroupCacheEnabled)
                {
                    if (DeviceTypeInGroupDict.ContainsKey(toBeSaved.IdDeviceTypeGroup))
                    {
                        if (DeviceTypeInGroupDict[toBeSaved.IdDeviceTypeGroup].Find(f => f == toBeSaved) == null)
                            DeviceTypeInGroupDict[toBeSaved.IdDeviceTypeGroup].Add(toBeSaved); //add element
                    }
                    else
                    {
                        DeviceTypeInGroupDict.Add(toBeSaved.IdDeviceTypeGroup, new List<OpDeviceTypeInGroup>());
                        DeviceTypeInGroupDict[toBeSaved.IdDeviceTypeGroup].Add(toBeSaved); //add element
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDeviceTypeInGroup", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the DeviceTypeInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">DeviceTypeInGroup to delete</param>
        public virtual void DeleteDeviceTypeInGroup(OpDeviceTypeInGroup toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteDeviceTypeInGroup(toBeDeleted);
                if (DeviceTypeInGroupCacheEnabled)
                {
                    DeviceTypeInGroupDict.Remove(toBeDeleted.IdDeviceTypeGroup);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelDeviceTypeInGroup", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the DeviceTypeInGroup object cache
        /// </summary>
        public virtual void ClearDeviceTypeInGroupCache()
        {
            DeviceTypeInGroupDict.Clear();
            DeviceTypeInGroupIsFullCache = false;
        }
        #endregion

        #region DeviceHierarchyPattern

        #region GetAllDeviceHierarchyPattern
        /// <summary>
        /// Gets all DeviceHierarchyPattern objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        public virtual List<OpDeviceHierarchyPattern> GetAllDeviceHierarchyPattern(bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    return OpDeviceHierarchyPattern.ConvertList(dbCollectorClient.GetAllDeviceHierarchyPattern(ref dbCollectorSession), this);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (DeviceHierarchyPatternCacheEnabled) //cache enabled
                    {
                        if (!DeviceHierarchyPatternIsFullCache)
                        {
                            List<OpDeviceHierarchyPattern> objectList = OpDeviceHierarchyPattern.ConvertList(dbConnectionCore.GetDeviceHierarchyPattern(), this);
                            if (objectList != null)
                            {
                                DeviceHierarchyPatternDict = objectList.ToDictionary(o => o.IdDeviceHierarchyPattern);
                                DeviceHierarchyPatternIsFullCache = true;
                            }
                        }
                        return new List<OpDeviceHierarchyPattern>(DeviceHierarchyPatternDict.Values);
                    }
                    else //cache disabled
                    {
                        return OpDeviceHierarchyPattern.ConvertList(dbConnectionCore.GetDeviceHierarchyPattern(), this);
                    }
                }
                else
                    return new List<OpDeviceHierarchyPattern>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceHierarchyPattern", ex);
                throw;
            }
        }
        #endregion

        #endregion

        #region Distributor
        /// <summary>
        /// Adds new distributor
        /// </summary>
        /// <param name="toBeSaved">Distributor to be added</param>
        public virtual bool AddDistributor(OpDistributor toBeSaved)
        {
            try
            {
                dbConnectionCore.AddDistributor(toBeSaved);
                if (DistributorCacheEnabled)
                {
                    if (DistributorDict != null)
                    {
                        if (DistributorDict.ContainsKey(toBeSaved.IdDistributor)) //element founded in cache
                            DistributorDict[toBeSaved.IdDistributor] = toBeSaved;
                        else
                            DistributorDict.Add(toBeSaved.IdDistributor, toBeSaved);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_AddDistributor", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Indicates whether the DistributorDict was filled trough GetAllDistributor or GetDistributorFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool DistributorIsFullCache { get; set; }

        /// <summary>
        /// Distributor objects dictionary
        /// </summary>
        public Dictionary<int, OpDistributor> DistributorDict = new Dictionary<int, OpDistributor>();

        /// <summary>
        /// Indicates whether the Distributor is cached.
        /// </summary>
        public virtual bool DistributorCacheEnabled { get; set; }

        /// <summary>
        /// Gets all Distributor objects
        /// </summary>
        public virtual List<OpDistributor> Distributor
        {
            get { return GetAllDistributor(); }
        }

        /// <summary>
        /// Gets all Distributor objects
        /// </summary>
        public virtual List<OpDistributor> GetAllDistributor(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (DistributorCacheEnabled) //cache enabled
                {
                    if (!DistributorIsFullCache)
                    {
                        List<OpDistributor> objectList = OpDistributor.ConvertList(dbConnectionCore.GetDistributor(autoTransaction, transactionLevel, commandTimeout), this, true, true, null, autoTransaction, transactionLevel, commandTimeout);
                        if (objectList != null)
                        {
                            DistributorDict = objectList.ToDictionary(d => d.IdDistributor);
                            DistributorIsFullCache = true;
                        }
                    }
                    return new List<OpDistributor>(DistributorDict.Values);
                }
                else //cache disabled
                {
                    return OpDistributor.ConvertList(dbConnectionCore.GetDistributor(autoTransaction, transactionLevel, commandTimeout), this, true, true, null, autoTransaction, transactionLevel, commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributor", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Id">Distributor Id</param>
        /// <returns>Distributor object</returns>
        public virtual OpDistributor GetDistributor(int Id)
        {
            return GetDistributor(Id, false);
        }

        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Ids">Distributor Ids</param>
        /// <returns>Distributor list</returns>
        public virtual List<OpDistributor> GetDistributor(int[] Ids)
        {
            return GetDistributor(Ids, false);
        }

        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Id">Distributor Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Distributor object</returns>
        public virtual OpDistributor GetDistributor(int Id, bool queryDatabase)
        {
            return GetDistributor(new int[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets Distributor by id
        /// </summary>
        /// <param name="Ids">Distributor Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Distributor list</returns>
        public virtual List<OpDistributor> GetDistributor(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (DistributorCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                List<OpDistributor> returnList = new List<OpDistributor>();
                List<int> idsNotInCache = new List<int>();
                for (int i = 0; i < Ids.Length; i++)
                {
                    OpDistributor distributorFoundInCache;
                    if (DistributorDict != null && DistributorDict.TryGetValue(Ids[i], out distributorFoundInCache)) //element founded in cache
                        returnList.Add(distributorFoundInCache);
                    else //element not found in cache, query database
                        idsNotInCache.Add(Ids[i]);
                }
                if (idsNotInCache.Count > 0)
                {
                    try
                    {
                        if (DistributorDict == null)
                            DistributorDict = new Dictionary<int, OpDistributor>();

                        foreach (OpDistributor item in OpDistributor.ConvertList(dbConnectionCore.GetDistributor(idsNotInCache.ToArray(), autoTransaction, transactionLevel, commandTimeout), this))
                        {
                            DistributorDict[item.IdDistributor] = item;
                            returnList.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDistributor", ex);
                        throw ex;
                    }
                }
                return returnList;
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpDistributor.ConvertList(dbConnectionCore.GetDistributor(Ids, autoTransaction, transactionLevel, commandTimeout), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDistributor", ex);
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Saves the Distributor object to the database
        /// </summary>
        /// <param name="toBeSaved">Distributor to save</param>
        /// <returns>Distributor Id</returns>
        public virtual int SaveDistributor(OpDistributor toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDistributor(toBeSaved);
                if (DistributorCacheEnabled && DistributorDict != null)
                {
                    DistributorDict[toBeSaved.IdDistributor] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDistributor", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the Distributor object from the database
        /// </summary>
        /// <param name="toBeDeleted">Distributor to delete</param>
        public virtual void DeleteDistributor(OpDistributor toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteDistributor(toBeDeleted);
                if (DistributorCacheEnabled && DistributorDict != null)
                {
                    DistributorDict.Remove(toBeDeleted.IdDistributor);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelDistributor", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the Distributor object cache
        /// </summary>
        public virtual void ClearDistributorCache()
        {
            DistributorDict.Clear();
            DistributorIsFullCache = false;
        }
        #endregion

        #region File
        /// <summary>
        /// Indicates whether the FileDict was filled trough GetAllFile or GetAllFileFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool FileIsFullCache { get; protected set; }

        /// <summary>
        /// File objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpFile> FileDict = new Dictionary<long, OpFile>();

        /// <summary>
        /// Indicates whether the File is cached.
        /// </summary>
        public bool FileCacheEnabled { get; set; }

        /// <summary>
        /// Gets all File objects
        /// </summary>
        public virtual List<OpFile> File
        {
            get { return GetAllFile(); }
        }

        /// <summary>
        /// Gets all File objects
        /// </summary>
        public virtual List<OpFile> GetAllFile(bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            try
            {
                if (FileCacheEnabled) //cache enabled
                {
                    if (!FileIsFullCache)
                    {
                        List<OpFile> objectList = OpFile.ConvertList(dbConnectionCore.GetFile(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                        if (objectList != null)
                        {
                            FileDict = objectList.ToDictionary(o => o.IdFile);
                            FileIsFullCache = true;
                        }
                    }
                    return new List<OpFile>(FileDict.Values);
                }
                else //cache disabled
                {
                    return OpFile.ConvertList(dbConnectionCore.GetFile(autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetFile", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Id">File Id</param>
        /// <returns>File object</returns>
        public virtual OpFile GetFile(long Id)
        {
            return GetFile(Id, false);
        }

        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Ids">File Ids</param>
        /// <returns>File list</returns>
        public virtual List<OpFile> GetFile(long[] Ids)
        {
            return GetFile(Ids, false);
        }

        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Id">File Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>File object</returns>
        public virtual OpFile GetFile(long Id, bool queryDatabase)
        {
            return GetFile(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets File by id
        /// </summary>
        /// <param name="Ids">File Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>File list</returns>
        public virtual List<OpFile> GetFile(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpFile> returnList = new List<OpFile>();
            if (Ids != null && Ids.Length > 0)
            {
                if (FileCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpFile FileFoundInCache;
                        if (FileDict.TryGetValue(Ids[i], out FileFoundInCache)) //element founded in cache
                            returnList.Add(FileFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpFile item in OpFile.ConvertList(dbConnectionCore.GetFile(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                FileDict[item.IdFile] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetFile", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpFile.ConvertList(dbConnectionCore.GetFile(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetFile", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Gets File list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFile funcion</param>
        /// <param name="IdFile">Specifies filter for ID_FILE column</param>
        /// <param name="PhysicalFileName">Specifies filter for PHYSICAL_FILE_NAME column</param>
        /// <param name="Size">Specifies filter for SIZE column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="Version">Specifies filter for VERSION column</param>
        /// <param name="LastDownloaded">Specifies filter for LAST_DOWNLOADED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>File list</returns>
        public virtual List<OpFile> GetFileFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdFile = null, string PhysicalFileName = null, long[] Size = null, TypeDateTimeCode InsertDate = null, string Description = null, string Version = null,
                            TypeDateTimeCode LastDownloaded = null, bool? IsBlocked = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetFileFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFileFilter"));
            }

            try
            {
                List<OpFile> returnList = OpFile.ConvertList(dbConnectionCore.GetFileFilter(IdFile, PhysicalFileName, Size, InsertDate, Description, Version,
                             LastDownloaded, IsBlocked, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpFile item in returnList)
                        FileDict[item.IdFile] = item;
                    FileIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetFileFilter", ex);
                throw;
            }

        }

        /// <summary>
        /// Saves the File object to the database
        /// </summary>
        /// <param name="toBeSaved">File to save</param>
        /// <returns>File Id</returns>
        public virtual long SaveFile(OpFile toBeSaved)
        {
            try
            {
                long ret = dbConnectionCore.SaveFile(toBeSaved);
                if (FileCacheEnabled)
                {
                    FileDict[toBeSaved.IdFile] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveFile", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the File object from the database
        /// </summary>
        /// <param name="toBeDeleted">File to delete</param>
        public virtual void DeleteFile(OpFile toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteFile(toBeDeleted);
                if (FileCacheEnabled)
                {
                    FileDict.Remove(toBeDeleted.IdFile);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelFile", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the File object cache
        /// </summary>
        public virtual void ClearFileCache()
        {
            FileDict.Clear();
            FileIsFullCache = false;
        }


        /// <summary>
        /// Gets File content
        /// </summary>
        /// <param name="file">File object for which to load file content</param>
        /// <returns>void</returns>
        public virtual void GetFileContent(OpFile file)
        {
            try
            {
                if (file == null)
                    return;

                dbConnectionCore.GetFileContent(file);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetFileContent", ex);
                throw;
            }
        }

        /// <summary>
        /// Saves the content of the File object to the database, this function does not save standart file properties
        /// </summary>
        /// <param name="toBeSaved">File to save</param>
        /// <returns>File Id</returns>
        public virtual void SaveFileContent(OpFile toBeSaved)
        {
            try
            {
                dbConnectionCore.SaveFileContent(toBeSaved);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveFileContent", ex);
                throw;
            }
        }

        #endregion

        #region GetDevice
        /// <summary>
        /// Gets Device by id
        /// </summary>
        /// <param name="Ids">Device Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>Device list</returns>
        public virtual List<OpDevice> GetDevice(long[] Ids, bool queryDatabase, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpDevice> returnList = new List<OpDevice>();
            if (Ids != null && Ids.Length > 0)
            {
                if (DeviceCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpDevice DeviceFoundInCache;
                        if (DeviceDict.TryGetValue(Ids[i], out DeviceFoundInCache)) //element founded in cache
                            returnList.Add(DeviceFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpDevice item in OpDevice.ConvertList(dbConnectionCore.GetDevice(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties, loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                DeviceDict[item.SerialNbr] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevice", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpDevice.ConvertList(dbConnectionCore.GetDevice(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties, loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevice", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion
        #region GetLocation
        /// <summary>
        /// Gets Location by id
        /// </summary>
        /// <param name="Ids">Location Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>Location list</returns>
        public virtual List<OpLocation> GetLocation(long[] Ids, bool queryDatabase, bool loadNavigationProperties = true, bool loadCustomData = true,
            bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpLocation> returnList = new List<OpLocation>();
            if (Ids != null && Ids.Length > 0)
            {
                if (LocationCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpLocation LocationFoundInCache;
                        if (LocationDict.TryGetValue(Ids[i], out LocationFoundInCache)) //element founded in cache
                            returnList.Add(LocationFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpLocation item in OpLocation.ConvertList(dbConnectionCore.GetLocation(idsNotInCache.ToArray()), this, loadNavigationProperties, loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                LocationDict[item.IdLocation] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocation", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpLocation.ConvertList(dbConnectionCore.GetLocation(Ids), this, loadNavigationProperties, loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocation", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion
        #region GetMeter
        /// <summary>
        /// Gets Meter by id
        /// </summary>
        /// <param name="Ids">Meter Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>Meter list</returns>
        public virtual List<OpMeter> GetMeter(long[] Ids, bool queryDatabase, bool loadNavigationProperties = true, bool loadCustomData = true, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpMeter> returnList = new List<OpMeter>();
            if (Ids != null && Ids.Length > 0)
            {
                if (MeterCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpMeter MeterFoundInCache;
                        if (MeterDict.TryGetValue(Ids[i], out MeterFoundInCache)) //element founded in cache
                            returnList.Add(MeterFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpMeter item in OpMeter.ConvertList(dbConnectionCore.GetMeter(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties, loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                MeterDict[item.IdMeter] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeter", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpMeter.ConvertList(dbConnectionCore.GetMeter(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties, loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeter", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion
        #region SaveMeterDetails
        public virtual void SaveMeterDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveMeterDetails(changeFromLastMinutes, particularUnits, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateLocationDetails", ex);
                throw;
            }
        }
        #endregion
        #region DeleteMeterDetails

        /// <summary>
        /// Deletes the MeterDetails by IdMeter.
        /// </summary>
        /// <param name="idMeter"></param>
        public virtual void DeleteMeterDetails(long idMeter)
        {
            try
            {
                dbConnectionCore.DeleteMeterDetails(idMeter);
                if (MeterCacheEnabled)
                {
                    MeterDict.Remove(idMeter);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DeleteMeterDetails", ex);
                throw;
            }
        }

        #endregion

        #region ImrServer
        /// <summary>
        /// Saves the ImrServer object to the database
        /// </summary>
        /// <param name="toBeSaved">ImrServer to save</param>
        /// <returns>ImrServer Id</returns>
        public virtual int SaveImrServer(OpImrServer toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveImrServer(toBeSaved);
                if (ImrServerCacheEnabled)
                {
                    ImrServerDict[toBeSaved.IdImrServer] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveImrServer", ex);
                throw;
            }
        }
        #endregion

        #region Issue

        public virtual void SaveIssueDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveIssueDetails(changeFromLastMinutes, particularUnits, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateIssueDetails", ex);
                throw;
            }
        }

        #endregion

        #region Language
        /// <summary>
        /// Saves the Language object to the database
        /// </summary>
        /// <param name="toBeSaved">Language to save</param>
        /// <returns>Language Id</returns>
        public virtual int SaveLanguage(OpLanguage toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveLanguage(toBeSaved);
                if (LanguageCacheEnabled)
                {
                    LanguageDict[toBeSaved.IdLanguage] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveLanguage", ex);
                throw;
            }
        }
        #endregion

        #region Location

        public virtual List<OpLocation> GetLocationMagazineDistinct(int[] IdLocationType, int[] IdDistributor, bool AllowBlankCid,
            bool loadNavigationProperties = true, bool mergeIntoCache = false,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            List<OpLocation> returnList = OpLocation.ConvertList(dbConnectionCore.GetLocationMagazineDistinct(IdLocationType, IdDistributor, AllowBlankCid, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
            if (mergeIntoCache && LocationCacheEnabled)
            {
                foreach (OpLocation item in returnList)
                    LocationDict[item.IdLocation] = item;
            }
            return returnList;
        }

        /// <summary>
        /// Saves the Location object to the database
        /// </summary>
        /// <param name="toBeSaved">Location to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>Location Id</returns>
        public virtual long SaveLocation(OpLocation toBeSaved, bool useDBCollector = false)
        {
            try
            {
                long ret = 0;
                bool isNew = toBeSaved.IdLocation == 0;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_LOCATION location = new DB_LOCATION(toBeSaved);
                    ret = dbCollectorClient.SaveLocation(ref dbCollectorSession, location);

                    if (ret != 0 && isNew && toBeSaved != null && toBeSaved.DataList != null)
                    {
                        toBeSaved.DataList.RemoveAll(x => x.ID_DATA_TYPE == IMR.Suite.Common.DataType.LOCATION_IMR_SERVER_ID);
                        List<Objects.CORE.OpData> mapped = GetDataFilter(loadNavigationProperties: false, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted,
                            IdLocation: new long[] { ret }, IdDataType: new long[] { IMR.Suite.Common.DataType.LOCATION_IMR_SERVER_ID, IMR.Suite.Common.DataType.LOCATION_SOURCE_SERVER_ID }).ToList();
                        if (mapped != null && mapped.Count > 0)
                        {
                            toBeSaved.DataList.AddRange(mapped);
                        }
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    ret = dbConnectionCore.SaveLocation(toBeSaved);
                }

                if (ret != 0)
                {
                    toBeSaved.IdLocation = ret;
                    if (LocationCacheEnabled)
                    {
                        LocationDict[toBeSaved.IdLocation] = toBeSaved; //add or update element
                    }
                }

                InvokeDataChanged(toBeSaved, (isNew ? NotifyType.Insert : NotifyType.Update), typeof(OpLocation));

                return toBeSaved.IdLocation;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveLocation", ex);
                throw;
            }
        }

        public virtual List<OpLocation> GetAll(bool? exludeDeletedAndUnused = null)
        {
            List<OpLocation> locationList = null;
            if (exludeDeletedAndUnused.HasValue && exludeDeletedAndUnused.Value == true)
            {
                locationList = this.GetAllLocation().Where(o => o.IdLocationStateType != (int)Enums.LocationState.DELETED && o.IdLocationStateType != (int)Enums.LocationState.UNUSED).ToList();
            }
            else
                locationList = this.GetAllLocation();

            return locationList;
        }

        public virtual void SaveLocationDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveLocationDetails(changeFromLastMinutes, particularUnits, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateLocationDetails", ex);
                throw;
            }
        }

        #endregion

        #region LocationStateType
        /// <summary>
        /// Saves the LocationStateType object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationStateType to save</param>
        /// <returns>LocationStateType Id</returns>
        public virtual int SaveLocationStateType(OpLocationStateType toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveLocationStateType(toBeSaved);
                if (LocationStateTypeCacheEnabled)
                {
                    LocationStateTypeDict[toBeSaved.IdLocationStateType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveLocationStateType", ex);
                throw;
            }
        }
        #endregion

        #region LocationType
        /// <summary>
        /// Saves the LocationType object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationType to save</param>
        /// <returns>LocationType Id</returns>
        public virtual int SaveLocationType(OpLocationType toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveLocationType(toBeSaved);
                if (LocationTypeCacheEnabled)
                {
                    LocationTypeDict[toBeSaved.IdLocationType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveLocationType", ex);
                throw;
            }
        }
        #endregion

        #region LocationEquipment

        /// <summary>
        /// Saves the LocationEquipment object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationEquipment to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>LocationEquipment Id</returns>
        public virtual long SaveLocationEquipment(OpLocationEquipment toBeSaved, bool useDBCollector = false)
        {
            try
            {
                long ret = 0;
                bool isNew = toBeSaved.IdLocationEquipment == 0;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_LOCATION_EQUIPMENT equipment = new DB_LOCATION_EQUIPMENT(toBeSaved);
                    ret = dbCollectorClient.SaveLocationEquipment(ref dbCollectorSession, equipment);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    ret = dbConnectionCore.SaveLocationEquipment(toBeSaved);
                }

                if (ret != 0)
                {
                    toBeSaved.IdLocationEquipment = ret;
                    if (LocationEquipmentCacheEnabled)
                    {
                        LocationEquipmentDict[toBeSaved.IdLocationEquipment] = toBeSaved; //add or update element
                    }
                }

                InvokeDataChanged(toBeSaved, (isNew ? NotifyType.Insert : NotifyType.Update), typeof(OpLocationEquipment));

                return toBeSaved.IdLocationEquipment;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveLocationEquipment", ex);
                throw;
            }
        }
        #endregion

        #region LocationHierarchy

        #region SaveLocationHierarchy

        /// <summary>
        /// Saves the LocationHierarchy object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationHierarchy to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>LocationHierarchy Id</returns>
        public virtual long SaveLocationHierarchy(OpLocationHierarchy toBeSaved, bool useDBCollector = false)
        {
            try
            {
                long ret = 0;
                bool isNew = toBeSaved.IdLocationHierarchy == 0;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_LOCATION_HIERARCHY toBeSavedClient = new DB_LOCATION_HIERARCHY(toBeSaved);
                    toBeSaved.IdLocationHierarchy = ret = dbCollectorClient.SaveLocationHierarchy(ref dbCollectorSession, toBeSavedClient);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    ret = dbConnectionCore.SaveLocationHierarchy(toBeSaved);
                    if (LocationHierarchyCacheEnabled)
                    {
                        LocationHierarchyDict[toBeSaved.IdLocationHierarchy] = toBeSaved; //add or update element
                    }
                }

                InvokeDataChanged(toBeSaved, (isNew ? NotifyType.Insert : NotifyType.Update), typeof(OpLocationHierarchy));
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveLocationHierarchy", ex);
                throw;
            }
        }

        #endregion

        #region DeleteLocationHierarchy

        /// <summary>
        /// Deletes the LocationHierarchy object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationHierarchy to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted in connected data base or in DBCollector web service</param>
        public virtual void DeleteLocationHierarchy(OpLocationHierarchy toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_LOCATION_HIERARCHY toBeDeletedClient = new DB_LOCATION_HIERARCHY(toBeDeleted);
                    dbCollectorClient.DeleteLocationHierarchy(ref dbCollectorSession, toBeDeletedClient);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.DeleteLocationHierarchy(toBeDeleted);
                    if (LocationHierarchyCacheEnabled)
                    {
                        LocationHierarchyDict.Remove(toBeDeleted.IdLocationHierarchy);
                    }
                }

                InvokeDataChanged(toBeDeleted, NotifyType.Delete, typeof(OpLocationHierarchy));
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelLocationHierarchy", ex);
                throw;
            }
        }

        #endregion

        #endregion

        #region LocationStateHistory
        /// <summary>
        /// Saves the LocationStateHistory object to the database
        /// </summary>
        /// <param name="toBeSaved">LocationStateHistory to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>LocationStateHistory Id</returns>
        public virtual long SaveLocationStateHistory(OpLocationStateHistory toBeSaved, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_LOCATION_STATE_HISTORY stateHistory = new DB_LOCATION_STATE_HISTORY(toBeSaved);
                    long ret = dbCollectorClient.SaveLocationStateHistory(ref dbCollectorSession, stateHistory);
                    if (ret == 0)
                        return toBeSaved.IdLocationState;
                    return ret;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveLocationStateHistory(toBeSaved);
                    if (LocationStateHistoryCacheEnabled)
                    {
                        LocationStateHistoryDict[toBeSaved.IdLocationState] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                else
                    return toBeSaved.IdLocationState;

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveLocationStateHistory", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the LocationStateHistory object from the database
        /// </summary>
        /// <param name="toBeDeleted">LocationStateHistory to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted in connected data base or in DBCollector web service</param>
        public virtual void DeleteLocationStateHistory(OpLocationStateHistory toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_LOCATION_STATE_HISTORY stateHistory = new DB_LOCATION_STATE_HISTORY(toBeDeleted);
                    dbCollectorClient.DeleteLocationStateHistory(ref dbCollectorSession, stateHistory);
                }
                else
                {
                    dbConnectionCore.DeleteLocationStateHistory(toBeDeleted);
                    if (LocationStateHistoryCacheEnabled)
                    {
                        LocationStateHistoryDict.Remove(toBeDeleted.IdLocationState);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelLocationStateHistory", ex);
                throw;
            }
        }
        #endregion

        #region MeterType
        /// <summary>
        /// Saves the MeterType object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterType to save</param>
        /// <returns>MeterType Id</returns>
        public virtual int SaveMeterType(OpMeterType toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveMeterType(toBeSaved);
                if (MeterTypeCacheEnabled)
                {
                    MeterTypeDict[toBeSaved.IdMeterType] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveMeterType", ex);
                throw;
            }
        }
        #endregion

        #region MeterTypeClass
        /// <summary>
        /// Saves the MeterTypeClass object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeClass to save</param>
        /// <returns>MeterTypeClass Id</returns>
        public virtual int SaveMeterTypeClass(OpMeterTypeClass toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveMeterTypeClass(toBeSaved);
                if (MeterTypeClassCacheEnabled)
                {
                    MeterTypeClassDict[toBeSaved.IdMeterTypeClass] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveMeterTypeClass", ex);
                throw;
            }
        }
        #endregion

        #region MeterTypeDeviceType
        /// <summary>
        /// Indicates whether the MeterTypeDeviceTypeDict was filled trough GetAllMeterTypeDeviceType or GetAllMeterTypeDeviceTypeFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool MeterTypeDeviceTypeIsFullCache { get; set; }

        /// <summary>
        /// MeterTypeDeviceType objects dictionary (Cache)
        /// </summary>
        public List<OpMeterTypeDeviceType> MeterTypeDeviceTypeDict = new List<OpMeterTypeDeviceType>();

        /// <summary>
        /// Indicates whether the MeterTypeDeviceType is cached.
        /// </summary>
        public virtual bool MeterTypeDeviceTypeCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterTypeDeviceType objects
        /// </summary>
        public virtual List<OpMeterTypeDeviceType> MeterTypeDeviceType
        {
            get { return GetAllMeterTypeDeviceType(); }
        }

        /// <summary>
        /// Gets all MeterTypeDeviceType objects
        /// </summary>
        public virtual List<OpMeterTypeDeviceType> GetAllMeterTypeDeviceType()
        {
            try
            {
                if (MeterTypeDeviceTypeCacheEnabled) //cache enabled
                {
                    if (!MeterTypeDeviceTypeIsFullCache)
                    {
                        List<OpMeterTypeDeviceType> objectList = OpMeterTypeDeviceType.ConvertList(dbConnectionCore.GetMeterTypeDeviceType(), this);
                        if (objectList != null)
                        {
                            MeterTypeDeviceTypeDict = objectList;
                            MeterTypeDeviceTypeIsFullCache = true;
                        }
                    }
                    return MeterTypeDeviceTypeDict;
                }
                else //cache disabled
                {
                    return OpMeterTypeDeviceType.ConvertList(dbConnectionCore.GetMeterTypeDeviceType(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetMeterTypeDeviceType", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets MeterTypeDeviceType by IdMeterType and IdDeviceType
        /// </summary>
        /// <param name="IdMeterType"></param>
        /// <param name="IdDeviceType"></param>
        /// <returns>MeterTypeDeviceType object</returns>
        public virtual OpMeterTypeDeviceType GetMeterTypeDeviceType(int idMeterType, int idDeviceType)
        {
            return GetMeterTypeDeviceType(idMeterType, idDeviceType, false).FirstOrDefault();
        }

        /// <summary>
        /// Gets MeterTypeDeviceType by IdMeterType or IdDeviceType
        /// </summary>
        /// <param name="IdMeterType"></param>
        /// <param name="IdDeviceType"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeDeviceType list</returns>
        public virtual List<OpMeterTypeDeviceType> GetMeterTypeDeviceType(int? idMeterType, int? idDeviceType)
        {
            return GetMeterTypeDeviceType(idMeterType, idDeviceType, false);
        }

        /// <summary>
        /// Gets MeterTypeDeviceType by IdMeterType or IdDeviceType
        /// </summary>
        /// <param name="IdMeterType"></param>
        /// <param name="IdDeviceType"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeDeviceType list</returns>
        public virtual List<OpMeterTypeDeviceType> GetMeterTypeDeviceType(int? idMeterType, int? idDeviceType, bool queryDatabase)
        {
            List<OpMeterTypeDeviceType> returnList = new List<OpMeterTypeDeviceType>();

            if (MeterTypeDeviceTypeCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                bool bNotInCache = false;
                List<OpMeterTypeDeviceType> MeterTypeDeviceTypeFoundInCache = new List<OpMeterTypeDeviceType>();
                if (idMeterType != null && idDeviceType != null)
                {
                    MeterTypeDeviceTypeFoundInCache = MeterTypeDeviceTypeDict.Where(w => w.IdMeterType == idMeterType.Value && w.IdDeviceType == idDeviceType.Value).ToList();
                    if (MeterTypeDeviceTypeFoundInCache.Count == 0)
                        bNotInCache = true;
                    else
                        returnList.AddRange(MeterTypeDeviceTypeFoundInCache);
                }
                else if (idMeterType != null && idDeviceType == null)
                {
                    MeterTypeDeviceTypeFoundInCache = MeterTypeDeviceTypeDict.Where(w => w.IdMeterType == idMeterType.Value).ToList();
                    if (MeterTypeDeviceTypeFoundInCache.Count == 0)
                        bNotInCache = true;
                    else
                        returnList.AddRange(MeterTypeDeviceTypeFoundInCache);
                }
                else if (idMeterType == null && idDeviceType != null)
                {
                    MeterTypeDeviceTypeFoundInCache = MeterTypeDeviceTypeDict.Where(w => w.IdDeviceType == idDeviceType.Value).ToList();
                    if (MeterTypeDeviceTypeFoundInCache.Count == 0)
                        bNotInCache = true;
                    else
                        returnList.AddRange(MeterTypeDeviceTypeFoundInCache);
                }
                else
                    bNotInCache = true;

                if (bNotInCache)
                {
                    try
                    {
                        foreach (OpMeterTypeDeviceType item in OpMeterTypeDeviceType.ConvertList(dbConnectionCore.GetMeterTypeDeviceType(idMeterType, idDeviceType), this))
                        {
                            if (!MeterTypeDeviceTypeDict.Contains(item))
                                MeterTypeDeviceTypeDict.Add(item);
                        }

                        if (idMeterType != null && idDeviceType != null)
                            MeterTypeDeviceTypeFoundInCache = MeterTypeDeviceTypeDict.Where(w => w.IdMeterType == idMeterType.Value && w.IdDeviceType == idDeviceType.Value).ToList();
                        else if (idMeterType != null && idDeviceType == null)
                            MeterTypeDeviceTypeFoundInCache = MeterTypeDeviceTypeDict.Where(w => w.IdMeterType == idMeterType.Value).ToList();
                        else if (idMeterType == null && idDeviceType != null)
                            MeterTypeDeviceTypeFoundInCache = MeterTypeDeviceTypeDict.Where(w => w.IdDeviceType == idDeviceType.Value).ToList();

                        returnList.AddRange(MeterTypeDeviceTypeFoundInCache);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetMeterTypeDeviceType", ex);
                        throw ex;
                    }
                }
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpMeterTypeDeviceType.ConvertList(dbConnectionCore.GetMeterTypeDeviceType(idMeterType, idDeviceType), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetMeterTypeDeviceType", ex);
                    throw ex;
                }
            }

            return returnList;
        }

        /// <summary>
        /// Saves the MeterTypeDeviceType object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeDeviceType to save</param>
        public virtual void SaveMeterTypeDeviceType(OpMeterTypeDeviceType toBeSaved)
        {
            try
            {
                dbConnectionCore.SaveMeterTypeDeviceType(toBeSaved);
                if (MeterTypeDeviceTypeCacheEnabled)
                {
                    if (!MeterTypeDeviceTypeDict.Contains(toBeSaved))
                        MeterTypeDeviceTypeDict.Add(toBeSaved);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveMeterTypeDeviceType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Deletes the MeterTypeDeviceType object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterTypeDeviceType to delete</param>
        public virtual void DeleteMeterTypeDeviceType(OpMeterTypeDeviceType toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteMeterTypeDeviceType(toBeDeleted);
                if (MeterTypeDeviceTypeCacheEnabled)
                {
                    MeterTypeDeviceTypeDict.Remove(toBeDeleted);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelMeterTypeDeviceType", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the MeterTypeDeviceType object cache
        /// </summary>
        public virtual void ClearMeterTypeDeviceTypeCache()
        {
            MeterTypeDeviceTypeDict.Clear();
            MeterTypeDeviceTypeIsFullCache = false;
        }
        #endregion

        #region MeterTypeInGroup
        /// <summary>
        /// Indicates whether the MeterTypeInGroupDict was filled trough GetAllMeterTypeInGroup or GetAllMeterTypeInGroupFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool MeterTypeInGroupIsFullCache { get; protected set; }

        /// <summary>
        /// MeterTypeInGroup objects dictionary (Cache)
        /// </summary>
        public Dictionary<int, List<OpMeterTypeInGroup>> MeterTypeInGroupDict = new Dictionary<int, List<OpMeterTypeInGroup>>();

        /// <summary>
        /// Indicates whether the MeterTypeInGroup is cached.
        /// </summary>
        public bool MeterTypeInGroupCacheEnabled { get; set; }

        /// <summary>
        /// Gets all MeterTypeInGroup objects
        /// </summary>
        public virtual List<OpMeterTypeInGroup> MeterTypeInGroup
        {
            get { return GetAllMeterTypeInGroup(); }
        }

        /// <summary>
        /// Gets all MeterTypeInGroup objects
        /// </summary>
        public virtual List<OpMeterTypeInGroup> GetAllMeterTypeInGroup()
        {
            try
            {
                if (MeterTypeInGroupCacheEnabled) //cache enabled
                {
                    if (!MeterTypeInGroupIsFullCache)
                    {
                        List<OpMeterTypeInGroup> objectList = OpMeterTypeInGroup.ConvertList(dbConnectionCore.GetMeterTypeInGroup(), this);
                        if (objectList != null)
                        {
                            MeterTypeInGroupDict = new Dictionary<int, List<OpMeterTypeInGroup>>();
                            foreach (var item in objectList)
                            {
                                if (!MeterTypeInGroupDict.ContainsKey(item.IdMeterTypeGroup))
                                    MeterTypeInGroupDict.Add(item.IdMeterTypeGroup, new List<OpMeterTypeInGroup>());

                                MeterTypeInGroupDict[item.IdMeterTypeGroup].Add(item);
                            }
                            MeterTypeInGroupIsFullCache = true;
                        }
                    }
                    return MeterTypeInGroupDict.Values.SelectMany(w => w).ToList();
                }
                else //cache disabled
                {
                    return OpMeterTypeInGroup.ConvertList(dbConnectionCore.GetMeterTypeInGroup(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeInGroup", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Id">MeterTypeInGroup Id</param>
        /// <returns>MeterTypeInGroup object</returns>
        public virtual List<OpMeterTypeInGroup> GetMeterTypeInGroup(int Id)
        {
            return GetMeterTypeInGroup(Id, false);
        }

        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Ids">MeterTypeInGroup Ids</param>
        /// <returns>MeterTypeInGroup list</returns>
        public virtual List<OpMeterTypeInGroup> GetMeterTypeInGroup(int[] Ids)
        {
            return GetMeterTypeInGroup(Ids, false);
        }

        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Id">MeterTypeInGroup Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeInGroup object</returns>
        public virtual List<OpMeterTypeInGroup> GetMeterTypeInGroup(int Id, bool queryDatabase)
        {
            return GetMeterTypeInGroup(new int[] { Id }, queryDatabase);
        }

        /// <summary>
        /// Gets MeterTypeInGroup by id
        /// </summary>
        /// <param name="Ids">MeterTypeInGroup Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>MeterTypeInGroup list</returns>
        public virtual List<OpMeterTypeInGroup> GetMeterTypeInGroup(int[] Ids, bool queryDatabase)
        {
            List<OpMeterTypeInGroup> returnList = new List<OpMeterTypeInGroup>();
            if (Ids != null && Ids.Length > 0)
            {
                if (MeterTypeInGroupCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpMeterTypeInGroup> MeterTypeInGroupFoundInCache;
                        if (MeterTypeInGroupDict.TryGetValue(Ids[i], out MeterTypeInGroupFoundInCache)) //element founded in cache
                            returnList.AddRange(MeterTypeInGroupFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpMeterTypeInGroup> objectList = OpMeterTypeInGroup.ConvertList(dbConnectionCore.GetMeterTypeInGroup(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                MeterTypeInGroupDict = new Dictionary<int, List<OpMeterTypeInGroup>>();
                                foreach (var item in objectList)
                                {
                                    if (!MeterTypeInGroupDict.ContainsKey(item.IdMeterTypeGroup))
                                        MeterTypeInGroupDict.Add(item.IdMeterTypeGroup, new List<OpMeterTypeInGroup>());

                                    MeterTypeInGroupDict[item.IdMeterTypeGroup].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeInGroup", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpMeterTypeInGroup.ConvertList(dbConnectionCore.GetMeterTypeInGroup(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeInGroup", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the MeterTypeInGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">MeterTypeInGroup to save</param>
        /// <returns>MeterTypeInGroup Id</returns>
        public virtual int SaveMeterTypeInGroup(OpMeterTypeInGroup toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveMeterTypeInGroup(toBeSaved);
                if (MeterTypeInGroupCacheEnabled)
                {
                    if (MeterTypeInGroupDict.ContainsKey(toBeSaved.IdMeterTypeGroup))
                    {
                        if (MeterTypeInGroupDict[toBeSaved.IdMeterTypeGroup].Find(f => f.MeterType == toBeSaved.MeterType) == null)
                            MeterTypeInGroupDict[toBeSaved.IdMeterTypeGroup].Add(toBeSaved); //add element
                    }
                    else
                    {
                        MeterTypeInGroupDict.Add(toBeSaved.IdMeterTypeGroup, new List<OpMeterTypeInGroup>());
                        MeterTypeInGroupDict[toBeSaved.IdMeterTypeGroup].Add(toBeSaved); //add element
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveMeterTypeInGroup", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the MeterTypeInGroup object from the database
        /// </summary>
        /// <param name="toBeDeleted">MeterTypeInGroup to delete</param>
        public virtual void DeleteMeterTypeInGroup(OpMeterTypeInGroup toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteMeterTypeInGroup(toBeDeleted);
                if (MeterTypeInGroupCacheEnabled)
                {
                    MeterTypeInGroupDict.Remove(toBeDeleted.IdMeterTypeGroup);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelMeterTypeInGroup", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the MeterTypeInGroup object cache
        /// </summary>
        public virtual void ClearMeterTypeInGroupCache()
        {
            MeterTypeInGroupDict.Clear();
            MeterTypeInGroupIsFullCache = false;
        }
        #endregion

        #region Module
        /// <summary>
        /// Saves the Module object to the database
        /// </summary>
        /// <param name="toBeSaved">Module to save</param>
        /// <returns>Module Id</returns>
        public virtual int SaveModule(OpModule toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveModule(toBeSaved);
                if (ModuleCacheEnabled)
                {
                    ModuleDict[toBeSaved.IdModule] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveModule", ex);
                throw;
            }
        }
        #endregion

        #region Operator
        /// <summary>
        /// Gets Operator by id
        /// </summary>
        /// <param name="Ids">Operator Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Operator list</returns>
        public virtual List<OpOperator> GetOperator(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpOperator> returnList = new List<OpOperator>();
            if (Ids != null && Ids.Length > 0)
            {
                if (OperatorCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpOperator OperatorFoundInCache;
                        if (OperatorDict.TryGetValue(Ids[i], out OperatorFoundInCache)) //element founded in cache
                            returnList.Add(OperatorFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpOperator item in OpOperator.ConvertList(dbConnectionCore.GetOperator(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                OperatorDict[item.IdOperator] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOperator", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpOperator.ConvertList(dbConnectionCore.GetOperator(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOperator", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #region GetOperatorFilter 

        //public virtual List<OpOperator> GetOperatorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOperator = null, int[] IdActor = null, int[] IdDistributor = null, long[] SerialNbr = null, string Login = null, string Password = null,
        //                    bool? IsBlocked = null, string Description = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        //{
        //    return GetOperatorFilter(loadNavigationProperties: loadNavigationProperties, loadCustomData: true, mergeIntoCache: mergeIntoCache,
        //    IdOperator: IdOperator, IdActor: IdActor, 
        //}
        /// <summary>
        /// Gets Operator list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperator funcion</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Login">Specifies filter for LOGIN column</param>
        /// <param name="Password">Specifies filter for PASSWORD column</param>
        /// <param name="IsBlocked">Specifies filter for IS_BLOCKED column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Operator list</returns>
        public virtual List<OpOperator> GetOperatorFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdOperator = null, int[] IdActor = null, int[] IdDistributor = null, long[] SerialNbr = null, string Login = null, string Password = null,
                            bool? IsBlocked = null, string Description = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetOperatorFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorFilter"));
            }

            try
            {
                List<OpOperator> returnList = OpOperator.ConvertList(dbConnectionCore.GetOperatorFilter(IdOperator, IdActor, IdDistributor, SerialNbr, Login, Password,
                    IsBlocked, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpOperator item in returnList)
                        OperatorDict[item.IdOperator] = item;
                    OperatorIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOperatorFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetOperatorShippingListMailingList
        public virtual List<OpOperator> GetOperatorShippingListMailingList(int IdDistributor, int OperationType)
        {
            List<OpOperator> returnList = new List<OpOperator>();
            if (IdDistributor >= 0)
            {
                try
                {
                    List<int> list = dbConnectionCore.GetOperatorShippingListMailingList(IdDistributor, OperationType);
                    if (list != null && list.Count > 0)
                    {

                        returnList = this.GetOperator(list.ToArray());
                    }

                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetOperatorShippingListMailingList", ex);
                    throw;
                }

            }
            return returnList;
        }
        #endregion
        #region DeleteOperatorData
        /// <summary>
        /// Deletes the OperatorData object from the database
        /// </summary>
        /// <param name="toBeDeleted">OperatorData to delete</param>
        /// <param name="useDBCollector">Indicates whether data should be deleted from connected data base or from DBCollector web service</param>
        public virtual void DeleteOperatorData(OpOperatorData toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_OPERATOR_DATA operatorData = new DB_OPERATOR_DATA((DB_OPERATOR_DATA)toBeDeleted);
                    dbCollectorClient.DeleteOperatorData(ref dbCollectorSession, operatorData);

                    if (dbCollectorSession.Status == IMR.Suite.Services.Common.ImrWcfServiceReference.SESSION_STATUS.ERROR)
                    {
                        throw new Exception(dbCollectorSession.Error.Msg);
                    }
                }
                else
                {
                    dbConnectionCore.DeleteOperatorData(toBeDeleted);
                    if (OperatorDataCacheEnabled)
                    {
                        OperatorDataDict.Remove(toBeDeleted.IdOperatorData);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelOperatorData", ex);
                throw;
            }
        }
        #endregion
        /// <summary>
        /// Saves the OperatorData object to the database
        /// </summary>
        /// <param name="toBeSaved">OperatorData to save</param>
        /// <param name="useDBCollector">Indicates whether data should be saved in connected data base or in DBCollector web service</param>
        /// <returns>OperatorData Id</returns>
        public virtual long SaveOperatorData(OpOperatorData toBeSaved, bool useDBCollector = false)
        {
            try
            {
                long ret = 0;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_OPERATOR_DATA local = new DB_OPERATOR_DATA(toBeSaved);
                    ret = dbCollectorClient.SaveOperatorData(ref dbCollectorSession, local);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (toBeSaved.DataType == null)
                        toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                    ret = dbConnectionCore.SaveOperatorData(toBeSaved, toBeSaved.DataType);
                }

                if (ret != 0)
                {
                    toBeSaved.IdOperatorData = ret;
                    if (OperatorDataCacheEnabled)
                    {
                        OperatorDataDict[toBeSaved.IdOperatorData] = toBeSaved; //add or update element
                    }
                }

                return toBeSaved.IdOperatorData;                
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveOperatorData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the OperatorDetails by IdOperator.
        /// </summary>
        /// <param name="idOperator"></param>
        public virtual void DeleteOperatorDetails(int idOperator)
        {
            try
            {
                dbConnectionCore.DeleteOperatorDetails(idOperator);
                if (OperatorCacheEnabled)
                {
                    OperatorDict.Remove(idOperator);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DeleteOperatorDetails", ex);
                throw;
            }
        }

        public virtual void SaveOperatorDetails(int? changeFromLastMinutes, long[] idOperator, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveOperatorDetails(changeFromLastMinutes, idOperator, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateOperatorDetails", ex);
                throw;
            }
        }
        
        #endregion

        #region OperatorRole
        /// <summary>
        /// Indicates whether the OperatorRole was filled trough GetAllOperatorRole or GetAllOperatorRoleFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool OperatorRoleIsFullCache { get; protected set; }

        /// <summary>
        /// Indicates whether the Activity is cached.
        /// </summary>
        public virtual bool OperatorRoleCacheEnabled { get; set; }

        /// <summary>
        /// OperatorRole objects dictionary (Cache)
        /// </summary>
        private List<OpOperatorRole> OperatorRoleDict = new List<OpOperatorRole>();


        /// <summary>
        /// Gets all OperatorRole objects
        /// </summary>
        public virtual List<OpOperatorRole> OperatorRole
        {
            get { return GetAllOperatorRole(); }
        }

        /// <summary>
        /// Gets all OperatorRole objects
        /// </summary>
        public virtual List<OpOperatorRole> GetAllOperatorRole()
        {
            try
            {
                if (OperatorRoleCacheEnabled) //cache enabled
                {
                    if (!OperatorRoleIsFullCache)
                    {
                        List<OpOperatorRole> objectList = OpOperatorRole.ConvertList(dbConnectionCore.GetOperatorRole(), this);
                        if (objectList != null)
                        {
                            OperatorRoleDict = objectList;
                            OperatorRoleIsFullCache = true;
                        }
                    }
                    return OperatorRoleDict;
                }
                else //cache disabled
                {
                    return OpOperatorRole.ConvertList(dbConnectionCore.GetOperatorRole(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetOperatorRole", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets OperatorRole by IdOperator and IdRole
        /// </summary>
        /// <param name="IdOperator"></param>
        /// <param name="IdRole"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>OperatorRole object</returns>
        public virtual List<OpOperatorRole> GetOperatorRole(int? IdOperator, int? IdRole, bool queryDatabase = false)
        {
            List<OpOperatorRole> returnList = new List<OpOperatorRole>();

            if (OperatorRoleCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                bool bNotInCache = false;
                if (IdOperator != null && IdRole != null)
                {
                    OpOperatorRole OperatorRoleFoundInCache = OperatorRoleDict.FirstOrDefault(w => w.IdOperator == IdOperator.Value && w.IdRole == IdRole.Value);
                    if (OperatorRoleFoundInCache == null)
                        bNotInCache = true;
                    else
                        returnList.Add(OperatorRoleFoundInCache);
                }
                else if (IdOperator != null && IdRole == null)
                {
                    List<OpOperatorRole> OperatorRoleFoundInCache = OperatorRoleDict.Where(w => w.IdOperator == IdOperator.Value).ToList();
                    if (OperatorRoleFoundInCache.Count == 0)
                        bNotInCache = true;
                    else
                        returnList.AddRange(OperatorRoleFoundInCache);
                }
                else if (IdOperator == null && IdRole != null)
                {
                    List<OpOperatorRole> OperatorRoleFoundInCache = OperatorRoleDict.Where(w => w.IdRole == IdRole.Value).ToList();
                    if (OperatorRoleFoundInCache.Count == 0)
                        bNotInCache = true;
                    else
                        returnList.AddRange(OperatorRoleFoundInCache);
                }
                else if (OperatorRoleDict.Count > 0)
                    returnList = OperatorRoleDict;
                else
                    bNotInCache = true;

                if (bNotInCache)
                {
                    try
                    {
                        foreach (OpOperatorRole item in OpOperatorRole.ConvertList(dbConnectionCore.GetOperatorRole(IdOperator, IdRole), this))
                        {
                            if (!OperatorRoleDict.Contains(item))
                                OperatorRoleDict.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetOperatorRole", ex);
                        throw ex;
                    }
                }
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpOperatorRole.ConvertList(dbConnectionCore.GetOperatorRole(IdOperator, IdRole), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetOperatorRole", ex);
                    throw ex;
                }
            }

            return returnList;
        }

        public virtual void SaveOperatorRole(OpOperatorRole toBeSaved)
        {
            try
            {
                dbConnectionCore.SaveOperatorRole(toBeSaved);
                if (OperatorRoleCacheEnabled)
                {
                    if (!OperatorRoleDict.Contains(toBeSaved))
                        OperatorRoleDict.Add(toBeSaved);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveOperatorRole", ex);
                throw ex;
            }
        }

        public virtual void DeleteOperatorRole(OpOperatorRole toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteOperatorRole(toBeDeleted);
                if (OperatorRoleCacheEnabled)
                {
                    OperatorRoleDict.Remove(toBeDeleted);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelOperatorRole", ex);
                throw ex;
            }
        }

        public virtual void DeleteOperatorRoleByOperatorAndRole(int idOperator, int idRole)
        {
            try
            {
                dbConnectionCore.DeleteOperatorRole(idOperator, idRole);
                if (OperatorRoleCacheEnabled)
                {
                    OperatorRoleDict.Clear();
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelOperatorRoleByOperatorAndRole", ex);
                throw ex;
            }
        }

        public virtual void ClearOperatorRoleCache()
        {
            OperatorRoleDict.Clear();
            OperatorRoleIsFullCache = false;
        }
        #endregion

        #region Protocol
        /// <summary>
        /// Saves the Protocol object to the database
        /// </summary>
        /// <param name="toBeSaved">Protocol to save</param>
        /// <returns>Protocol Id</returns>
        public virtual int SaveProtocol(OpProtocol toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveProtocol(toBeSaved);
                if (ProtocolCacheEnabled)
                {
                    ProtocolDict[toBeSaved.IdProtocol] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveProtocol", ex);
                throw;
            }
        }
        #endregion

        #region ReferenceType

        public virtual object GetReferenceObject(Enums.ReferenceType referenceType, object referenceValue, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            return Components.CORE.ReferenceTypeComponent.GetReferenceObject(this, referenceType, referenceValue, loadNavigationProperties, loadCustomData);
        }

        #endregion

        #region RoleGroup
        /// <summary>
        /// Indicates whether the OperatorRole was filled trough GetAllRoleGroup or GetAllRoleGroupFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool RoleGroupIsFullCache { get; protected set; }

        /// <summary>
        /// Indicates whether the Activity is cached.
        /// </summary>
        public virtual bool RoleGroupCacheEnabled { get; set; }

        /// <summary>
        /// RoleGroup objects dictionary (Cache)
        /// </summary>
        private List<OpRoleGroup> RoleGroupDict = new List<OpRoleGroup>();


        /// <summary>
        /// Gets all RoleGroup objects
        /// </summary>
        public virtual List<OpRoleGroup> RoleGroup
        {
            get { return GetAllRoleGroup(); }
        }

        /// <summary>
        /// Gets all RoleGroup objects
        /// </summary>
        public virtual List<OpRoleGroup> GetAllRoleGroup()
        {
            try
            {
                if (RoleGroupCacheEnabled) //cache enabled
                {
                    if (!RoleGroupIsFullCache)
                    {
                        List<OpRoleGroup> objectList = OpRoleGroup.ConvertList(dbConnectionCore.GetRoleGroup(), this);
                        if (objectList != null)
                        {
                            RoleGroupDict = objectList;
                            RoleGroupIsFullCache = true;
                        }
                    }
                    return RoleGroupDict;
                }
                else //cache disabled
                {
                    return OpRoleGroup.ConvertList(dbConnectionCore.GetRoleGroup(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetRoleGroup", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets RoleGroup by IdOperator and IdRole
        /// </summary>
        /// <param name="IdParentRole"></param>
        /// <param name="IdRole"></param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>RoleGroup object</returns>
        public virtual List<OpRoleGroup> GetRoleGroup(int? IdParentRole, int? IdRole, bool queryDatabase = false)
        {
            List<OpRoleGroup> returnList = new List<OpRoleGroup>();

            if (RoleGroupCacheEnabled && !queryDatabase)// cache enabled and query database is false
            {
                bool bNotInCache = false;
                if (IdParentRole != null && IdRole != null)
                {
                    OpRoleGroup RoleGroupFoundInCache = RoleGroupDict.FirstOrDefault(w => w.IdRoleParent == IdParentRole.Value && w.IdRole == IdRole.Value);
                    if (RoleGroupFoundInCache == null)
                        bNotInCache = true;
                    else
                        returnList.Add(RoleGroupFoundInCache);
                }
                else if (IdParentRole != null && IdRole == null)
                {
                    List<OpRoleGroup> RoleGroupFoundInCache = RoleGroupDict.Where(w => w.IdRoleParent == IdParentRole.Value).ToList();
                    if (RoleGroupFoundInCache.Count == 0)
                        bNotInCache = true;
                    else
                        returnList.AddRange(RoleGroupFoundInCache);
                }
                else if (IdParentRole == null && IdRole != null)
                {
                    List<OpRoleGroup> RoleGroupFoundInCache = RoleGroupDict.Where(w => w.IdRole == IdRole.Value).ToList();
                    if (RoleGroupFoundInCache.Count == 0)
                        bNotInCache = true;
                    else
                        returnList.AddRange(RoleGroupFoundInCache);
                }
                else if (RoleGroupDict.Count > 0)
                    returnList = RoleGroupDict;
                else
                    bNotInCache = true;

                if (bNotInCache)
                {
                    try
                    {
                        foreach (OpRoleGroup item in OpRoleGroup.ConvertList(dbConnectionCore.GetRoleGroup(IdParentRole, IdRole), this))
                        {
                            if (!RoleGroupDict.Contains(item))
                                RoleGroupDict.Add(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetRoleGroup", ex);
                        throw ex;
                    }
                }
            }
            else //cache disabled or user force to query database 
            {
                try
                {
                    return OpRoleGroup.ConvertList(dbConnectionCore.GetRoleGroup(IdParentRole, IdRole), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetRoleGroup", ex);
                    throw ex;
                }
            }

            return returnList;
        }

        public virtual void SaveRoleGroup(OpRoleGroup toBeSaved)
        {
            try
            {
                dbConnectionCore.SaveRoleGroup(toBeSaved);
                if (RoleGroupCacheEnabled)
                {
                    if (!RoleGroupDict.Contains(toBeSaved))
                        RoleGroupDict.Add(toBeSaved);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveRoleGroup", ex);
                throw ex;
            }
        }

        public virtual void DeleteRoleGroup(OpRoleGroup toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteRoleGroup(toBeDeleted);
                if (RoleGroupCacheEnabled)
                {
                    RoleGroupDict.Remove(toBeDeleted);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelRoleGroup", ex);
                throw ex;
            }
        }

        public virtual void ClearRoleGroupCache()
        {
            RoleGroupDict.Clear();
            RoleGroupIsFullCache = false;
        }
        #endregion

        #region Route
        
        public virtual void SaveRouteDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveRouteDetails(changeFromLastMinutes, particularUnits, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateRouteDetails", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the RouteDetails by IdRoute.
        /// </summary>
        /// <param name="idRoute"></param>
        public virtual void DeleteRouteDetails(long idRoute)
        {
            try
            {
                dbConnectionCore.DeleteRouteDetails(idRoute);
                if (RouteCacheEnabled)
                {
                    RouteDict.Remove(idRoute);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DeleteRouteDetails", ex);
                throw;
            }
        }

        #endregion

        #region RouteDefData
        /// <summary>
        /// Saves the RouteDefData object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDefData to save</param>
        /// <returns>RouteDefData Id</returns>
        public virtual void SaveRouteDefData(OpRouteDefData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                //long ret = 
                dbConnectionCore.SaveRouteDefData(toBeSaved, toBeSaved.DataType);
                //if (RouteDefLocationDataCacheEnabled)
                //{
                //    RouteDefLocationDataDict[toBeSaved.IdRouteDefLocation] = toBeSaved; //add or update element
                //}
                //return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveRouteDefData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the RouteDefData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDefData to delete</param>
        public virtual void DeleteRouteDefData(OpRouteDefData toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteRouteDefData(toBeDeleted);
                //if (RouteDefLocationDataCacheEnabled)
                //{
                //    RouteDefLocationDataDict.Remove(toBeDeleted.IdRouteDefLocation);
                //}
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelRouteDefData", ex);
                throw;
            }
        }
        #endregion

        #region RouteDefLocationData
        ///// <summary>
        ///// Indicates whether the RouteDefLocationDataDict was filled trough GetAllRouteDefLocationData or GetAllRouteDefLocationDataFilter with mergeIntoCache = true
        ///// </summary>
        //public virtual bool RouteDefLocationDataIsFullCache { get; protected set; }

        ///// <summary>
        ///// RouteDefLocationData objects dictionary (Cache)
        ///// </summary>
        //public Dictionary<long, OpRouteDefLocationData> RouteDefLocationDataDict = new Dictionary<long, OpRouteDefLocationData>();

        ///// <summary>
        ///// Indicates whether the RouteDefLocationData is cached.
        ///// </summary>
        //public virtual bool RouteDefLocationDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all RouteDefLocationData objects (Cache disabled)
        /// </summary>
        public virtual List<OpRouteDefLocationData> RouteDefLocationData
        {
            get { return GetAllRouteDefLocationData(); }
        }

        /// <summary>
        /// Gets all RouteDefLocationData objects (Cache disabled)
        /// </summary>
        public virtual List<OpRouteDefLocationData> GetAllRouteDefLocationData()
        {
            try
            {
                //if (RouteDefLocationDataCacheEnabled) //cache enabled
                //{
                //    if (!RouteDefLocationDataIsFullCache)
                //    {
                //        List<OpRouteDefLocationData> objectList = OpRouteDefLocationData.ConvertList(dbConnectionCore.GetRouteDefLocationData(), this);
                //        if (objectList != null)
                //        {
                //            RouteDefLocationDataDict = objectList.ToDictionary(o => o.IdRouteDefLocation);
                //            RouteDefLocationDataIsFullCache = true;
                //        }
                //    }
                //    return new List<OpRouteDefLocationData>(RouteDefLocationDataDict.Values);
                //}
                //else //cache disabled
                {
                    return OpRouteDefLocationData.ConvertList(dbConnectionCore.GetRouteDefLocationData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefLocationData", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Id">RouteDefLocationData Id</param>
        /// <returns>RouteDefLocationData object</returns>
        public virtual OpRouteDefLocationData GetRouteDefLocationData(long Id)
        {
            return GetRouteDefLocationData(Id, false);
        }

        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Ids">RouteDefLocationData Ids</param>
        /// <returns>RouteDefLocationData list</returns>
        public virtual List<OpRouteDefLocationData> GetRouteDefLocationData(long[] Ids)
        {
            return GetRouteDefLocationData(Ids, false);
        }

        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Id">RouteDefLocationData Id</param>
        /// <param name="queryDatabase">If True, gets object from database (Cache disabled)</param>
        /// <returns>RouteDefLocationData object</returns>
        public virtual OpRouteDefLocationData GetRouteDefLocationData(long Id, bool queryDatabase)
        {
            return GetRouteDefLocationData(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets RouteDefLocationData by id
        /// </summary>
        /// <param name="Ids">RouteDefLocationData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database (Cache disabled)</param>
        /// <returns>RouteDefLocationData list</returns>
        public virtual List<OpRouteDefLocationData> GetRouteDefLocationData(long[] Ids, bool queryDatabase)
        {
            List<OpRouteDefLocationData> returnList = new List<OpRouteDefLocationData>();
            if (Ids != null && Ids.Length > 0)
            {
                //if (RouteDefLocationDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                //{
                //    List<long> idsNotInCache = new List<long>();
                //    for (int i = 0; i < Ids.Length; i++)
                //    {
                //        OpRouteDefLocationData RouteDefLocationDataFoundInCache;
                //        if (RouteDefLocationDataDict.TryGetValue(Ids[i], out RouteDefLocationDataFoundInCache)) //element founded in cache
                //            returnList.Add(RouteDefLocationDataFoundInCache);
                //        else //element not found in cache, query database
                //            idsNotInCache.Add(Ids[i]);
                //    }
                //    if (idsNotInCache.Count > 0)
                //    {
                //        try
                //        {
                //            foreach (OpRouteDefLocationData item in OpRouteDefLocationData.ConvertList(dbConnectionCore.GetRouteDefLocationData(idsNotInCache.ToArray()), this))
                //            {
                //                RouteDefLocationDataDict[item.IdRouteDefLocation] = item;
                //                returnList.Add(item);
                //            }
                //        }
                //        catch (Exception ex)
                //        {
                //            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefLocationData", ex);
                //            throw;
                //        }
                //    }
                //}
                //else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpRouteDefLocationData.ConvertList(dbConnectionCore.GetRouteDefLocationData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefLocationData", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the RouteDefLocationData object to the database
        /// </summary>
        /// <param name="toBeSaved">RouteDefLocationData to save</param>
        /// <returns>RouteDefLocationData Id</returns>
        public virtual void SaveRouteDefLocationData(OpRouteDefLocationData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                //long ret = 
                dbConnectionCore.SaveRouteDefLocationData(toBeSaved, toBeSaved.DataType);
                //if (RouteDefLocationDataCacheEnabled)
                //{
                //    RouteDefLocationDataDict[toBeSaved.IdRouteDefLocation] = toBeSaved; //add or update element
                //}
                //return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveRouteDefLocationData", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the RouteDefLocationData object from the database
        /// </summary>
        /// <param name="toBeDeleted">RouteDefLocationData to delete</param>
        public virtual void DeleteRouteDefLocationData(OpRouteDefLocationData toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteRouteDefLocationData(toBeDeleted);
                //if (RouteDefLocationDataCacheEnabled)
                //{
                //    RouteDefLocationDataDict.Remove(toBeDeleted.IdRouteDefLocation);
                //}
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelRouteDefLocationData", ex);
                throw;
            }
        }

        ///// <summary>
        ///// Clears the RouteDefLocationData object cache
        ///// </summary>
        //public virtual void ClearRouteDefLocationDataCache()
        //{
        //    RouteDefLocationDataDict.Clear();
        //    RouteDefLocationDataIsFullCache = false;
        //}
        #endregion

        #region HolidayDate
        public virtual DateTime GetWorkingDate(DateTime startDate, int days, string countryCode)
        {
            return dbConnectionCore.GetWorkingDate(startDate, days, countryCode);
        }
        #endregion

        #region Schedule

        #region DeleteSchedule

        /// <summary>
        /// Deletes the Schedule object from the database
        /// </summary>
        /// <param name="toBeDeleted">Schedule to delete</param>
        public virtual void DeleteSchedule(OpSchedule toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                Exception dbCollEx = null;
                bool throwDBCollEx = false;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_SCHEDULE db_schedule = new DB_SCHEDULE(toBeDeleted);
                    dbCollectorClient.DeleteSchedule(ref dbCollectorSession, db_schedule);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                        if (throwDBCollEx)
                            throw dbCollEx;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.DeleteSchedule(toBeDeleted);
                    if (ScheduleCacheEnabled)
                    {
                        ScheduleDict.Remove(toBeDeleted.IdSchedule);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelSchedule", ex);
                throw;
            }
        }

        #endregion

        #region SaveSchedule

        /// <summary>
        /// Saves the Schedule object to the database
        /// </summary>
        /// <param name="toBeSaved">Schedule to save</param>
        /// <returns>Schedule Id</returns>
        public virtual int SaveSchedule(OpSchedule toBeSaved, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_SCHEDULE db_schedule = new DB_SCHEDULE(toBeSaved);
                    toBeSaved.IdSchedule = dbCollectorClient.SaveSchedule(ref dbCollectorSession, db_schedule);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                    }
                    if (!throwDBCollEx)
                    {
                        return toBeSaved.IdSchedule;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {

                    int ret = dbConnectionCore.SaveSchedule(toBeSaved);
                    if (ScheduleCacheEnabled)
                    {
                        ScheduleDict[toBeSaved.IdSchedule] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                else
                    return toBeSaved.IdSchedule;

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveSchedule", ex);
                throw;
            }
            throw dbCollEx;
        }

        #endregion

        #region GetScheduleFilter
        /// <summary>
        /// Gets Schedule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSchedule funcion</param>
        /// <param name="IdSchedule">Specifies filter for ID_SCHEDULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IdScheduleType">Specifies filter for ID_SCHEDULE_TYPE column</param>
        /// <param name="ScheduleInterval">Specifies filter for SCHEDULE_INTERVAL column</param>
        /// <param name="SubdayType">Specifies filter for SUBDAY_TYPE column</param>
        /// <param name="SubdayInterval">Specifies filter for SUBDAY_INTERVAL column</param>
        /// <param name="RelativeInterval">Specifies filter for RELATIVE_INTERVAL column</param>
        /// <param name="RecurrenceFactor">Specifies filter for RECURRENCE_FACTOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="TimesInUtc">Specifies filter for TIMES_IN_UTC column</param>
        /// <param name="LastRunDate">Specifies filter for LAST_RUN_DATE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SCHEDULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Schedule list</returns>
        public virtual List<OpSchedule> GetScheduleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSchedule = null, string Name = null, string Description = null, int[] IdScheduleType = null, int[] ScheduleInterval = null, int[] SubdayType = null,
                            int[] SubdayInterval = null, int[] RelativeInterval = null, int[] RecurrenceFactor = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, bool? TimesInUtc = null, TypeDateTimeCode LastRunDate = null, bool? IsActive = null,
                                long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetScheduleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetScheduleFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpSchedule> returnList = OpSchedule.ConvertList(dbCollectorClient.GetScheduleFilter(ref dbCollectorSession, IdSchedule, Name, Description, IdScheduleType, ScheduleInterval, SubdayType,
                                    SubdayInterval, RelativeInterval, RecurrenceFactor, StartDate, EndDate,
                                    StartTime, EndTime, TimesInUtc, LastRunDate, IsActive,
                                    topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpSchedule> returnList = OpSchedule.ConvertList(dbConnectionCore.GetScheduleFilter(IdSchedule, Name, Description, IdScheduleType, ScheduleInterval, SubdayType,
                                    SubdayInterval, RelativeInterval, RecurrenceFactor, StartDate, EndDate,
                                    StartTime, EndTime, TimesInUtc, LastRunDate, IsActive,
                                    topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpSchedule item in returnList)
                            ScheduleDict[item.IdSchedule] = item;
                        ScheduleIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpSchedule>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetScheduleFilter", ex);
                throw;
            }

        }
        #endregion
        #endregion

        #region ServiceListDevice

        #region GetServiceDeviceDiagnosticActions

        public virtual DataTable GetServiceDeviceDiagnosticActions(long[] SerialNbr, int IdLanguage)
        {
            return dbConnectionCore.GetServiceDeviceDiagnosticActions(SerialNbr, IdLanguage);
        }

        #endregion

        #region GetServiceDeviceServiceActions

        public virtual DataTable GetServiceDeviceServiceActions(long[] SerialNbr, int IdLanguage)
        {
            return dbConnectionCore.GetServiceDeviceServiceActions(SerialNbr, IdLanguage);
        }

        #endregion

        #endregion

        #region Tariff

        /// <summary>
        /// Gets Tariff by id
        /// </summary>
        /// <param name="Ids">Tariff Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Tariff list</returns>
        public virtual List<OpTariff> GetTariff(int[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpTariff> returnList = new List<OpTariff>();
            if (Ids != null && Ids.Length > 0)
            {
                if (TariffCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<int> idsNotInCache = new List<int>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpTariff TariffFoundInCache;
                        if (TariffDict.TryGetValue(Ids[i], out TariffFoundInCache)) //element founded in cache
                            returnList.Add(TariffFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpTariff item in OpTariff.ConvertList(dbConnectionCore.GetTariff(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                TariffDict[item.IdTariff] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTariff", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpTariff.ConvertList(dbConnectionCore.GetTariff(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTariff", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        #endregion

        #region Task

        public virtual List<OpTaskStatistics> GetTaskStatistics(int[] IdOperatorPerformer, bool loadNavigationProperties = true)
        {

            try
            {
                List<OpTaskStatistics> returnList = OpTaskStatistics.ConvertList(dbConnectionCore.GetTaskStatistics(IdOperatorPerformer),
                    this, loadNavigationProperties);
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetTasksStatistics", ex);
                throw;
            }

        }

        public virtual void SaveTaskDetails(int? changeFromLastMinutes, long[] particularUnits, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveTaskDetails(changeFromLastMinutes, particularUnits, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateTaskDetails", ex);
                throw;
            }
        }

        #endregion

        #region TaskData

        public virtual List<Tuple<OpTaskData, string>> GetTaskAssignedOperations(int[] IdTask, int IdLaguage)
        {
            List<Tuple<OpTaskData, string>> returnList = new List<Tuple<OpTaskData, string>>();
            if (IdTask != null && IdTask.Length > 0)
            {
                try
                {
                    Tuple<DB_TASK_DATA, string>[] list = dbConnectionCore.GetTaskAssignedOperations(IdTask, IdLaguage);
                    if (list != null)
                    {
                        foreach (Tuple<DB_TASK_DATA, string> tItem in list)
                        {
                            returnList.Add(new Tuple<OpTaskData, string>(new OpTaskData(tItem.Item1), tItem.Item2));
                        }
                    }

                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetTaskAssignedOperations", ex);
                    throw;
                }

            }
            return returnList;
        }

        public Dictionary<int, List<string>> GetTaskAssignedOperations2(int[] IdTask, int IdLaguage, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            Dictionary<int, List<string>> returnDict = new Dictionary<int, List<string>>();
            if (IdTask != null && IdTask.Length > 0)
            {
                try
                {
                    Tuple<int, string>[] list = dbConnectionCore.GetTaskAssignedOperations2(IdTask, IdLaguage, autoTransaction, transactionLevel, commandTimeout);
                    if (list != null)
                    {
                        list.Select(d => d.Item1).Distinct().ToList().ForEach(d => returnDict.Add(d, new List<string>()));
                        foreach (Tuple<int, string> tItem in list)
                        {
                            returnDict[tItem.Item1].Add(tItem.Item2);
                        }
                    }

                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetTaskAssignedOperations", ex);
                    throw;
                }

            }
            return returnDict;
        }

        #endregion

        #region WarrantyContract

        public virtual void SaveWarrantyContract(int idDeviceOrderNumber, int idContract, int idComponent, int months, int idWarrantyStartDateCalcMethod)
        {
            try
            {
                dbConnectionCore.SaveWarrantyContract(idDeviceOrderNumber, idContract, idComponent, months, idWarrantyStartDateCalcMethod);
                if (WarrantyContractCacheEnabled)
                {
                    //  WarrantyContractDict[toBeSaved.IdDeviceOrderNumber] = toBeSaved; //add or update element
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveWarrantyContract", ex);
                throw;
            }
        }

        public virtual void DeleteWarrantyContract(int idDeviceOrderNumber, int idContract, int idComponent)
        {
            try
            {
                dbConnectionCore.DeleteWarrantyContract(idDeviceOrderNumber, idContract, idComponent);
                if (WarrantyContractCacheEnabled)
                {
                    WarrantyContractDict.Clear();
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelWarrantyContract", ex);
                throw;
            }
        }

        #endregion

        #region Work
        #region GetWork
        /// <summary>
        /// Gets Work by id
        /// </summary>
        /// <param name="Ids">Work Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>Work list</returns>
        public virtual List<OpWork> GetWork(long[] Ids, bool queryDatabase, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            List<OpWork> returnList = new List<OpWork>();
            if (Ids != null && Ids.Length > 0)
            {
                if (WorkCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        OpWork WorkFoundInCache;
                        if (WorkDict.TryGetValue(Ids[i], out WorkFoundInCache)) //element founded in cache
                            returnList.Add(WorkFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpWork item in OpWork.ConvertList(dbConnectionCore.GetWork(idsNotInCache.ToArray(), autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout))
                            {
                                WorkDict[item.IdWork] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWork", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpWork.ConvertList(dbConnectionCore.GetWork(Ids, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWork", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }
        #endregion
        #region SaveWork
        /// <summary>
        /// Saves the Work object to the database
        /// </summary>
        /// <param name="toBeSaved">Work to save</param>
        /// <returns>Work Id</returns>
        public virtual long SaveWork(OpWork toBeSaved)
        {
            try
            {
                long ret = dbConnectionCore.SaveWork(toBeSaved);
                if (WorkCacheEnabled)
                {
                    WorkDict[toBeSaved.IdWork] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveWork", ex);
                throw;
            }
        }

        /// <summary>
        /// Saves the Work object to the database
        /// </summary>
        /// <param name="toBeSaved">Work to save</param>
        /// <param name="useDBCollector"></param>
        /// <returns>Work object</returns>
        public virtual OpWork SaveWork(OpWork toBeSaved, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    //DB_WORK db_work = new DB_WORK(toBeSaved);
                    //DB_WORK ret = dbCollectorClient.SaveWork(ref dbCollectorSession, db_work);
                    //if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    //{
                    //    switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                    //    {
                    //        case WCF.ERROR_CODE.IdsNotMapped:
                    //        case WCF.ERROR_CODE.ServerNotFound:
                    //        case WCF.ERROR_CODE.MSMQParamsNotFound:
                    //            dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                    //            throwDBCollEx = true;
                    //            break;
                    //    }
                    //}
                    //if (!throwDBCollEx)
                    //{
                    //    if (ret != null)
                    //    {
                    //        toBeSaved.ID_SERVER = ret.ID_SERVER;
                    //        if (ret.ID_WORK != 0)
                    //            toBeSaved.ID_WORK = ret.ID_WORK;
                    //    }
                    //    return toBeSaved;
                    //}
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    long ret = dbConnectionCore.SaveWork(toBeSaved);
                    if (WorkCacheEnabled)
                    {
                        WorkDict[toBeSaved.IdWork] = toBeSaved; //add or update element
                    }
                    toBeSaved.IdWork = ret;
                    toBeSaved.ID_SERVER = null;
                    return toBeSaved;
                }
                else
                    return toBeSaved;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveWork", ex);
                throw;
            }

            throw dbCollEx;
        }

        /// <summary>
        /// Bulk Insert/Update Work objects to the database
        /// </summary>
        /// <param name="toBeSaved">Work's to save</param>
        public virtual void SaveWork(OpWork[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (toBeSaved == null || toBeSaved.Count() == 0)
                return;

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    // Dlaczego tego jeszcze nie ma? :)
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpWork> toUpdate = toBeSaved.Where(x => x.IdWork > 0).ToList();
                    List<OpWork> toInsert = toBeSaved.Where(x => x.IdWork == 0).ToList();

                    dbConnectionCore.SaveWork(toBeSaved, updateSetSkipColumns, commandTimeout);

                    InvokeDataChanged(toInsert, NotifyType.Insert, typeof(OpWork));
                    InvokeDataChanged(toUpdate, NotifyType.Update, typeof(OpWork));
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "WORK", ex);
                throw;
            }
        }
        #endregion
        #region ChangeWorkStatus
        /// <summary>
        /// Bulk Update Status of Work objects in database
        /// </summary>
        /// <param name="toBeChanged">Work's to change</param>
        public virtual void ChangeWorkStatus(OpWork[] toBeChanged, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (toBeChanged == null || toBeChanged.Count() == 0)
                return;

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    // Dlaczego tego jeszcze nie ma? :)
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpWork> toUpdate = toBeChanged.Where(x => x.IdWork > 0).ToList();

                    dbConnectionCore.ChangeWorkStatus(toBeChanged, commandTimeout);

                    InvokeDataChanged(toUpdate, NotifyType.Update, typeof(OpWork));
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "WORK", ex);
                throw;
            }
        }
        #endregion
        #endregion
        #region WorkData
        /// <summary>
        /// Indicates whether the WorkDataDict was filled trough GetAllWorkData or GetAllWorkDataFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool WorkDataIsFullCache { get; protected set; }

        /// <summary>
        /// WorkData objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, List<OpWorkData>> WorkDataDict = new Dictionary<long, List<OpWorkData>>();

        /// <summary>
        /// Indicates whether the WorkData is cached.
        /// </summary>
        public virtual bool WorkDataCacheEnabled { get; set; }

        /// <summary>
        /// Gets all WorkData objects
        /// </summary>
        public virtual List<OpWorkData> WorkData
        {
            get { return GetAllWorkData(); }
        }

        /// <summary>
        /// Gets all WorkData objects
        /// </summary>
        public virtual List<OpWorkData> GetAllWorkData()
        {
            try
            {
                if (WorkDataCacheEnabled) //cache enabled
                {
                    if (!WorkDataIsFullCache)
                    {
                        List<OpWorkData> objectList = OpWorkData.ConvertList(dbConnectionCore.GetWorkData(), this);
                        if (objectList != null)
                        {
                            //WorkDataDict = objectList.ToDictionary(o => o.IdWorkData);
                            foreach (var item in objectList)
                            {
                                if (!WorkDataDict.ContainsKey(item.IdWorkData))
                                    WorkDataDict.Add(item.IdWorkData, new List<OpWorkData>());

                                WorkDataDict[item.IdWorkData].Add(item);
                            }
                            WorkDataIsFullCache = true;
                        }
                    }
                    return WorkDataDict.Values.SelectMany(w => w).ToList();
                }
                else //cache disabled
                {
                    return OpWorkData.ConvertList(dbConnectionCore.GetWorkData(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkData", ex);
                throw ex;
            }
        }


        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Id">WorkData Id</param>
        /// <returns>WorkData object</returns>
        public virtual OpWorkData GetWorkData(long Id)
        {
            return GetWorkData(Id, false);
        }

        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Ids">WorkData Ids</param>
        /// <returns>WorkData list</returns>
        public virtual List<OpWorkData> GetWorkData(long[] Ids)
        {
            return GetWorkData(Ids, false);
        }

        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Id">WorkData Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkData object</returns>
        public virtual OpWorkData GetWorkData(long Id, bool queryDatabase)
        {
            return GetWorkData(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets WorkData by id
        /// </summary>
        /// <param name="Ids">WorkData Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>WorkData list</returns>
        public virtual List<OpWorkData> GetWorkData(long[] Ids, bool queryDatabase)
        {
            List<OpWorkData> returnList = new List<OpWorkData>();
            if (Ids != null && Ids.Length > 0)
            {
                if (WorkDataCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        List<OpWorkData> WorkDataFoundInCache;
                        if (WorkDataDict.TryGetValue(Ids[i], out WorkDataFoundInCache)) //element founded in cache
                            returnList.AddRange(WorkDataFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            List<OpWorkData> objectList = OpWorkData.ConvertList(dbConnectionCore.GetWorkData(idsNotInCache.ToArray()), this);
                            if (objectList != null)
                            {
                                foreach (var item in objectList)
                                {
                                    if (!WorkDataDict.ContainsKey(item.IdWorkData))
                                        WorkDataDict.Add(item.IdWorkData, new List<OpWorkData>());

                                    WorkDataDict[item.IdWorkData].Add(item);
                                    returnList.Add(item);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkData", ex);
                            throw ex;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpWorkData.ConvertList(dbConnectionCore.GetWorkData(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkData", ex);
                        throw ex;
                    }
                }
            }
            return returnList;
        }


        /// <summary>
        /// Saves the WorkData object to the database
        /// </summary>
        /// <param name="toBeSaved">WorkData to save</param>
        /// <returns>WorkData Id</returns>
        public virtual long SaveWorkData(OpWorkData toBeSaved, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    //DB_WORK_DATA db_workData = new DB_WORK_DATA(toBeSaved);
                    //toBeSaved.ID_WORK_DATA = dbCollectorClient.SaveWorkData(ref dbCollectorSession, db_workData);
                    //if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    //{
                    //    switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                    //    {
                    //        case WCF.ERROR_CODE.IdsNotMapped:
                    //        case WCF.ERROR_CODE.ServerNotFound:
                    //        case WCF.ERROR_CODE.MSMQParamsNotFound:
                    //            dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                    //            throwDBCollEx = true;
                    //            break;
                    //    }
                    //}
                    //if (!throwDBCollEx)
                    //{
                    //    return toBeSaved.ID_WORK_DATA;
                    //}
                    //else
                    //    return 0;
                    return 0;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (toBeSaved.DataType == null)
                        toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);
                    long ret = dbConnectionCore.SaveWorkData(toBeSaved, toBeSaved.DataType);
                    if (WorkDataCacheEnabled)
                    {
                        if (WorkDataDict.ContainsKey(toBeSaved.IdWorkData))
                        {
                            if (WorkDataDict[toBeSaved.IdWorkData].Find(f => f == toBeSaved) == null)
                                WorkDataDict[toBeSaved.IdWorkData].Add(toBeSaved); //add element
                        }
                        else
                        {
                            WorkDataDict.Add(toBeSaved.IdWorkData, new List<OpWorkData>());
                            WorkDataDict[toBeSaved.IdWorkData].Add(toBeSaved); //add element
                        }
                    }
                    return ret;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveWorkData", ex);
                throw ex;
            }
        }
        /// <summary>
        /// Bulk Insert/Update WorkData objects to the database
        /// </summary>
        /// <param name="toBeSaved">WorkData's to save</param>
        public virtual void SaveWorkData(OpWorkData[] toBeSaved, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (toBeSaved == null || toBeSaved.Count() == 0)
                return;

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    // Dlaczego tego jeszcze nie ma? :)
                }
                else
                {
                    List<OpWorkData> toUpdate = toBeSaved.Where(x => x.IdWorkData > 0).ToList();
                    List<OpWorkData> toInsert = toBeSaved.Where(x => x.IdWorkData == 0).ToList();

                    Dictionary<long, OpDataType> dtDict = this.GetDataType(toBeSaved.Select(d => d.IdDataType).Distinct().ToArray()).ToDictionary(d => d.IdDataType);
                    foreach (OpWorkData dtItem in toBeSaved)
                        dtItem.DataType = dtDict[dtItem.IdDataType];

                    dbConnectionCore.SaveWorkData(toBeSaved, dtDict.Values.ToArray(), commandTimeout);

                    InvokeDataChanged(toInsert, NotifyType.Insert, typeof(OpWorkData));
                    InvokeDataChanged(toUpdate, NotifyType.Update, typeof(OpWorkData));
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "WORK_DATA", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the WorkData object from the database
        /// </summary>
        /// <param name="toBeDeleted">WorkData to delete</param>
        public virtual void DeleteWorkData(OpWorkData toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                Exception dbCollEx = null;
                bool throwDBCollEx = false;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    //DB_WORK_DATA db_workData = new DB_WORK_DATA(toBeDeleted);
                    //dbCollectorClient.DeleteWorkData(ref dbCollectorSession, db_workData);
                    //if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    //{
                    //    switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                    //    {
                    //        case WCF.ERROR_CODE.IdsNotMapped:
                    //        case WCF.ERROR_CODE.ServerNotFound:
                    //        case WCF.ERROR_CODE.MSMQParamsNotFound:
                    //            dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                    //            throwDBCollEx = true;
                    //            break;
                    //    }
                    //    if (throwDBCollEx)
                    //        throw dbCollEx;
                    //}
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.DeleteWorkData(toBeDeleted);
                    if (WorkDataCacheEnabled)
                    {
                        if (WorkDataDict.ContainsKey(toBeDeleted.IdWorkData))
                        {
                            WorkDataDict[toBeDeleted.IdWorkData].RemoveAll(ad => ad.IdWorkData == toBeDeleted.IdWorkData && ad.IdDataType == toBeDeleted.IdDataType && ad.IndexNbr == toBeDeleted.IndexNbr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_DelWorkData", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Clears the WorkData object cache
        /// </summary>
        public virtual void ClearWorkDataCache()
        {
            WorkDataDict.Clear();
            WorkDataIsFullCache = false;
        }
        #endregion
        #region WorkHistory
        #region SaveWorkHistory
        /// <summary>
        /// Bulk Insert/Update WorkHistory objects to the database
        /// </summary>
        /// <param name="toBeSaved">WorkHistory's to save</param>
        public virtual void SaveWorkHistory(OpWorkHistory[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (toBeSaved == null || toBeSaved.Count() == 0)
                return;

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    // Dlaczego tego jeszcze nie ma? :)
                }
                else
                {
                    dbConnectionCore.SaveWorkHistory(toBeSaved, updateSetSkipColumns, commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "WORK_HISTORY", ex);
                throw;
            }
        }
        #endregion
        #endregion
        #region WorkHistoryData
        /// <summary>
        /// Bulk Insert/Update WorkHistoryData objects to the database
        /// </summary>
        /// <param name="toBeSaved">WorkHistoryData's to save</param>
        public virtual void SaveWorkHistoryData(OpWorkHistoryData[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (toBeSaved == null || toBeSaved.Count() == 0)
                return;

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    // Dlaczego tego jeszcze nie ma? :)
                }
                else
                {
                    dbConnectionCore.SaveWorkHistoryData(toBeSaved, updateSetSkipColumns, commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "WORK_HISTORY_DATA", ex);
                throw;
            }
        }
        #endregion
        #region WorkType
        /// <summary>
        /// Gets all WorkType objects
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// </summary>
        public virtual List<OpWorkType> GetAllWorkType(bool useDBCollector = false, bool loadNavigationProperties = true, bool loadCustomData = true)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    //return OpWorkType.ConvertList(dbCollectorClient.GetAllWorkType(ref dbCollectorSession), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                    return new List<OpWorkType>();
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (WorkTypeCacheEnabled) //cache enabled
                    {
                        if (!WorkTypeIsFullCache)
                        {
                            List<OpWorkType> objectList = OpWorkType.ConvertList(dbConnectionCore.GetWorkType(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                            if (objectList != null)
                            {
                                WorkTypeDict = objectList.ToDictionary(o => o.IdWorkType);
                                WorkTypeIsFullCache = true;
                            }
                        }
                        return new List<OpWorkType>(WorkTypeDict.Values);
                    }
                    else //cache disabled
                    {
                        return OpWorkType.ConvertList(dbConnectionCore.GetWorkType(autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0), this, autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted, commandTimeout: 0);
                    }
                }
                else
                    return new List<OpWorkType>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkType", ex);
                throw;
            }
        }
        #endregion

        #region View

        #region ProceedViewOperation

        public virtual void ProceedViewOperation(List<OpViewOperation> ViewOperations)
        {

            try
            {
                if (ViewOperations != null)
                {
                    string insertFormat = "INSERT INTO [{0}] ({1}) VALUES ({2})";
                    string updateFormat = "UPDATE [{0}] SET {1} WHERE [{2}] = {3} AND {4}";
                    string deleteFormat = "DELETE FROM [{0}] WHERE [{1}] = {2} AND [{3}] = {4}";
                    string mergeFormat = "MERGE [{0}] AS v USING (SELECT {1}) AS vu ON v.[{2}] = {3} AND {4} WHEN MATCHED THEN UPDATE SET {5} WHEN NOT MATCHED THEN INSERT ({6}) VALUES ({7});"; // Merge statement must be terminaed with the semicolon (TSQL)

                    List<string> coreInsertClauses = new List<string>();
                    List<string> coreUpdateClauses = new List<string>();
                    List<string> coreMergeClauses = new List<string>();
                    List<string> coreDeleteClauses = new List<string>();

                    List<string> dwInsertClauses = new List<string>();
                    List<string> dwUpdateClauses = new List<string>();
                    List<string> dwMergeClauses = new List<string>();
                    List<string> dwDeleteClauses = new List<string>();

                    foreach (OpViewOperation voItem in ViewOperations)
                    {
                        if (voItem.ChangeAction == Enums.ViewOperation.InsertRow)
                        {
                            #region Insert

                            bool missingImrServerColumn = !voItem.CellValues.Exists(c => String.Equals(c.ColumnName, voItem.View.ImrServerColumn.Name));
                            string columnClause = String.Format("[{0}]", String.Join("],[", voItem.CellValues.Select(c => c.ColumnName)));
                            string valuesClause = String.Empty; 
                            foreach (OpViewOperation.OpViewOperationCellValue tItem in voItem.CellValues)
                                valuesClause += dbConnectionCore.PrepareDataValue(tItem.Value, tItem.DataType) + ",";
                            if (valuesClause.Length > 0)
                                valuesClause = valuesClause.Substring(0, valuesClause.Length - 1);

                            if (missingImrServerColumn)
                            {
                                columnClause += String.Format(",[{0}]", voItem.View.ImrServerColumn.Name);
                                valuesClause += "," + voItem.ImrServer.IdImrServer;
                            }

                            string insertClause = String.Format(insertFormat, voItem.View.Name, columnClause, valuesClause);

                            if (voItem.View.DatabaseType == Enums.DatabaseType.CORE)
                            {
                                coreInsertClauses.Add(insertClause);
                            }
                            else if (voItem.View.DatabaseType == Enums.DatabaseType.DW)
                            {
                                dwInsertClauses.Add(insertClause);
                            }
                            
                            IMRLog.AddToLog(EventID.DataProvider.PreparedToExecuteTSql, "INSERT", insertClause);
                            voItem.Statement = insertClause;

                            #endregion
                        }
                        else if (voItem.ChangeAction == Enums.ViewOperation.UpdateRow)
                        {
                            #region Update

                            List<string> setClause = new List<string>();
                            foreach (OpViewOperation.OpViewOperationCellValue tItem in voItem.CellValues)
                                setClause.Add(String.Format("[{0}] = {1}", tItem.ColumnName, dbConnectionCore.PrepareDataValue(tItem.Value, tItem.DataType)));

                            string updateClause = String.Format(updateFormat,
                                voItem.View.Name, String.Join(",", setClause), voItem.View.ImrServerColumn.Name, voItem.ImrServer.IdImrServer,
                                String.Format("[{0}] = {1}", voItem.KeyColumnName, dbConnectionCore.PrepareDataValue(voItem.ObjectId,voItem.ObjectIdDataType)));

                            if (voItem.View.DatabaseType == Enums.DatabaseType.CORE)
                            {
                                coreUpdateClauses.Add(updateClause);
                            }
                            else if (voItem.View.DatabaseType == Enums.DatabaseType.DW)
                            {
                                dwUpdateClauses.Add(updateClause);
                            }
                            
                            IMRLog.AddToLog(EventID.DataProvider.PreparedToExecuteTSql, "UPDATE", updateClause);
                            voItem.Statement = updateClause;

                            #endregion
                        }
                        else if (voItem.ChangeAction == Enums.ViewOperation.DeleteRow)
                        {
                            #region Delete

                            string deleteClause = String.Format(deleteFormat, voItem.View.Name,
                                voItem.View.PrimaryKeyColumn.Name, dbConnectionCore.PrepareDataValue(voItem.ObjectId, voItem.ObjectIdDataType),
                                voItem.View.ImrServerColumn.Name, voItem.ImrServer.IdImrServer);

                            if (voItem.View.DatabaseType == Enums.DatabaseType.CORE)
                            {
                                coreDeleteClauses.Add(deleteClause);
                            }
                            else if (voItem.View.DatabaseType == Enums.DatabaseType.DW)
                            {
                                dwDeleteClauses.Add(deleteClause);
                            }
                            
                            IMRLog.AddToLog(EventID.DataProvider.PreparedToExecuteTSql, "DELETE", deleteClause);
                            voItem.Statement = deleteClause;

                            #endregion
                        }
                        else //if(voItem.ChangeAction == Enums.ViewOperation.Merge)
                        {
                            #region Merge

                            string temp = string.Empty;

                            string mergeSelectClause = String.Empty;
                            string mergeUpdateClause = String.Empty;
                            string mergeInsertClause = String.Format(",[{0}],[{1}]", voItem.View.PrimaryKeyColumn.Name, voItem.View.ImrServerColumn.Name);//String.Empty;
                            string mergeInsertValuesClause = String.Format(",{0},{1}", dbConnectionCore.PrepareDataValue(voItem.ObjectId, voItem.ObjectIdDataType), voItem.ImrServer.IdImrServer);//String.Empty;

                            mergeSelectClause = String.Format("{0} AS [{1}], {2} AS [{3}]",
                                voItem.ImrServer.IdImrServer,
                                voItem.View.ImrServerColumn.Name,
                                dbConnectionCore.PrepareDataValue(voItem.ObjectId, voItem.ObjectIdDataType),
                                voItem.View.PrimaryKeyColumn.Name);

                           // mergeSelectClause = (String.IsNullOrEmpty(mergeSelectClause) ? "" : mergeSelectClause);
                            foreach(OpViewOperation.OpViewOperationCellValue tItem in voItem.CellValues)//.Where(v => !String.Equals(v.Item1, voItem.View.ImrServerColumn.Name) && !String.Equals(v.Item1, voItem.View.PrimaryKeyColumn.Name)))
                            {
                                object value = dbConnectionCore.PrepareDataValue(tItem.Value, tItem.DataType);
                                if (!String.Equals(tItem.ColumnName, voItem.View.ImrServerColumn.Name) && !String.Equals(tItem.ColumnName, voItem.View.PrimaryKeyColumn.Name))
                                {
                                    mergeSelectClause += String.Format(",{0} as [{1}]", value == null ? "null" : value, tItem.ColumnName);
                                    mergeInsertClause += String.Format(",[{0}]", tItem.ColumnName);
                                    mergeInsertValuesClause += String.Format(",{0}", value == null ? "null" : value);
                                }
                                mergeUpdateClause += String.Format(",v.[{0}] = {1}", tItem.ColumnName, value == null ? "null" : value);
                            }
                            if (mergeSelectClause.First() == ',')
                                mergeSelectClause = mergeSelectClause.Substring(1);
                            if (mergeInsertClause.First() == ',')
                                mergeInsertClause = mergeInsertClause.Substring(1);
                            if (mergeInsertValuesClause.First() == ',')
                                mergeInsertValuesClause = mergeInsertValuesClause.Substring(1);
                            if (mergeUpdateClause.First() == ',')
                                mergeUpdateClause = mergeUpdateClause.Substring(1);

                            string mergeClause = String.Format(mergeFormat, voItem.View.Name, mergeSelectClause,
                                voItem.View.ImrServerColumn.Name, voItem.ImrServer.IdImrServer,
                                String.Format("v.[{0}] = {1}", voItem.KeyColumnName, dbConnectionCore.PrepareDataValue(voItem.ObjectId, voItem.ObjectIdDataType)),//OnClause z wymuszeniem warunku na IdImrServer
                                mergeUpdateClause, mergeInsertClause, mergeInsertValuesClause);

                            if (voItem.View.DatabaseType == Enums.DatabaseType.CORE)
                            {
                                coreMergeClauses.Add(mergeClause);
                            }
                            else if (voItem.View.DatabaseType == Enums.DatabaseType.DW)
                            {
                                dwMergeClauses.Add(mergeClause);
                            }
                            
                            IMRLog.AddToLog(EventID.DataProvider.PreparedToExecuteTSql, "MERGE", mergeClause);
                            voItem.Statement = mergeClause;

                            /* Przykład
                                MERGE ACTOR_DETAILS as v
                                USING (SELECT 9 as [10045]) as vu
                                on v.[10045] = vu.[10045]
                                WHEN MATCHED THEN UPDATE SET v.[10080] = 'test';
                            */
                            #endregion
                        }
                    }

                    if (coreInsertClauses.Count > 0)
                        dbConnectionCore.ProceedViewOperationRow(coreInsertClauses.ToArray());
                    if (coreUpdateClauses.Count > 0)
                        dbConnectionCore.ProceedViewOperationRow(coreUpdateClauses.ToArray());
                    if (coreMergeClauses.Count > 0)
                        dbConnectionCore.ProceedViewOperationRow(coreMergeClauses.ToArray());
                    if (coreDeleteClauses.Count > 0)
                        dbConnectionCore.ProceedViewOperationRow(coreDeleteClauses.ToArray());
                    
                    if (dwInsertClauses.Count > 0)
                        dbConnectionDw.ProceedViewOperationRow(dwInsertClauses.ToArray());
                    if (dwUpdateClauses.Count > 0)
                        dbConnectionDw.ProceedViewOperationRow(dwUpdateClauses.ToArray());
                    if (dwMergeClauses.Count > 0)
                        dbConnectionDw.ProceedViewOperationRow(dwMergeClauses.ToArray());
                    if (dwDeleteClauses.Count > 0)
                        dbConnectionDw.ProceedViewOperationRow(dwDeleteClauses.ToArray());
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "ProceedViewOperation", ex);
                throw;
            }
        }
        
        #endregion

        #region ProceedViewChange

        public virtual void ProceedViewChange(OpView view, Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>[] viewChanges)
        {
            try
            {
                if (viewChanges != null)
                {
                    const string insertFormat = "IF NOT EXISTS (SELECT * FROM sys.columns WHERE [object_id] = OBJECT_ID(N'[dbo].[{0}]') AND [name] = '{1}') ALTER TABLE [dbo].[{0}] ADD [{1}] {2} {3}";
                    const string deleteFormat = "ALTER TABLE [{0}] DROP COLUMN [{1}]";

                    List<string> insertClauses = new List<string>();
                    List<string> deleteClauses = new List<string>();
                    foreach (Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool> voItem in viewChanges)
                    {
                        if (voItem.Item1 == Enums.ViewOperation.AddColumn)
                        {
                            #region Insert

                            string type = string.Empty;

                            switch (voItem.Item4)
                            {
                                case Enums.DataTypeClass.Boolean:
                                    type = "BIT";
                                    break;
                                case Enums.DataTypeClass.Date:
                                case Enums.DataTypeClass.Time:
                                case Enums.DataTypeClass.Datetime:
                                    type = "datetime";
                                    break;
                                case Enums.DataTypeClass.Integer:
                                    type = "bigint";
                                    break;
                                case Enums.DataTypeClass.Real:
                                    type = "real";
                                    break;
                                case Enums.DataTypeClass.Decimal:
                                    type = "decimal";
                                    break;
                                case Enums.DataTypeClass.Binary:
                                    type = "varbinary(max)";
                                    break;
                                case Enums.DataTypeClass.Object:
                                    type = "sql_variant";
                                    break;
                                case Enums.DataTypeClass.Xml:
                                case Enums.DataTypeClass.Unknown:
                                case Enums.DataTypeClass.Text:
                                default:
                                    type = "nvarchar(max)";
                                    break;
                            }

                            string insertClause = String.Format(insertFormat, voItem.Item2, voItem.Item3, type, voItem.Item5 ? "NULL" : "NOT NULL");
                            
                            insertClauses.Add(insertClause);
                            //voItem.Statement = insertClause;

                            IMRLog.AddToLog(EventID.DataProvider.PreparedToExecuteTSql, "INSERT", insertClause);

                            #endregion
                        }
                        else if (voItem.Item1 == Enums.ViewOperation.RemoveColumn)
                        {
                            #region Delete

                            string deleteClause = String.Format(deleteFormat, voItem.Item2, voItem.Item3);
                            
                            deleteClauses.Add(deleteClause);                            
                            //voItem.Statement = deleteClause;

                            IMRLog.AddToLog(EventID.DataProvider.PreparedToExecuteTSql, "DELETE", deleteClause);

                            #endregion
                        }
                    }

                    if (view.DatabaseType == Enums.DatabaseType.CORE)
                    {
                        if (insertClauses.Count > 0) dbConnectionCore.ProceedViewOperationRow(insertClauses.ToArray());
                        if (deleteClauses.Count > 0) dbConnectionCore.ProceedViewOperationRow(deleteClauses.ToArray());
                    }
                    else if (view.DatabaseType == Enums.DatabaseType.DW)
                    {
                        if (insertClauses.Count > 0) dbConnectionDw.ProceedViewOperationRow(insertClauses.ToArray());
                        if (deleteClauses.Count > 0) dbConnectionDw.ProceedViewOperationRow(deleteClauses.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "ProceedViewChange", ex);
                throw;
            }
        }

        #endregion

        #region ProceedViewCreation

        public virtual void ProceedViewCreation(OpView view)
        {
            if (view != null)
            {
                // {0} - Nazwa widoku/tabeli
                // {1} - klucze główne - kolumny
                // {2} - klucze główne - definicja PK
                const string viewFormat = "IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[{0}]') AND type in (N'U'))"
                                        + "\nBEGIN"
                                        + "\nCREATE TABLE [dbo].[{0}] ({1}, [ID_VIEW_KEY] bigint not null IDENTITY(1,1) CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED ({2}) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY])"
                                        + "\nEND"
                                        ;

                // {0} - nazwa, {1} - typ, {2} - not/null
                const string pkColumnFormat = "[{0}] [{1}] {2}";
                // {0} - nazwa, {1} - asc/desc
                const string pkKeyDefFormat = "[{0}] {1}";

                System.Func<int, string> DataTypeClassToDBType = (System.Func<int, string>)(dtc => {
                    string type = string.Empty;
                    switch ((Enums.DataTypeClass)dtc)
                    {
                        case Enums.DataTypeClass.Boolean:
                            type = "BIT";
                            break;
                        case Enums.DataTypeClass.Date:
                        case Enums.DataTypeClass.Time:
                        case Enums.DataTypeClass.Datetime:
                            type = "datetime";
                            break;
                        case Enums.DataTypeClass.Integer:
                            type = "bigint";
                            break;
                        case Enums.DataTypeClass.Real:
                            type = "real";
                            break;
                        case Enums.DataTypeClass.Decimal:
                            type = "decimal";
                            break;
                        case Enums.DataTypeClass.Binary:
                            type = "varbinary(max)";
                            break;
                        case Enums.DataTypeClass.Object:
                            type = "sql_variant";
                            break;
                        case Enums.DataTypeClass.Xml:
                        case Enums.DataTypeClass.Unknown:
                        case Enums.DataTypeClass.Text:
                        default:
                            type = "nvarchar(max)";
                            break;
                    }

                    return type;
                });

                string viewSql = string.Format(viewFormat,
                    view.Name,
                    string.Join(",", view.Columns.Where(x => x.PrimaryKey).Select(x => string.Format(pkColumnFormat, x.Name, DataTypeClassToDBType(x.DataType.IdDataTypeClass), "NOT NULL"))),
                    string.Join(",", view.Columns.Where(x => x.PrimaryKey).Select(x => string.Format(pkKeyDefFormat, x.Name, "ASC"))));

                if (view.DatabaseType == Enums.DatabaseType.CORE)
                {
                    dbConnectionCore.ProceedViewCreation(new string[] { viewSql });
                }
                else if (view.DatabaseType == Enums.DatabaseType.DW)
                {
                    dbConnectionDw.ProceedViewCreation(new string[] { viewSql });
                }

                List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>> columnsToAdd = new List<Tuple<Enums.ViewOperation, string, string, Enums.DataTypeClass, bool>>();
                foreach (OpViewColumn column in view.Columns.Where(x => !x.PrimaryKey))
                {
                    columnsToAdd.Add(Tuple.Create(Enums.ViewOperation.AddColumn, view.Name, column.Name, (Enums.DataTypeClass)column.DataType.IdDataTypeClass, true));
                }

                ProceedViewChange(view, columnsToAdd.ToArray());
            }
        }

        #endregion

        #region GetFilterFlexView

        public virtual Tuple<long, List<Tuple<long, Dictionary<string, object>>>> GetFilterFlexView(int PageNbr, int PageSize, OpView View,
            string customSelectClause, string customWhereClause, string customOrderClause)
        {
            long idArgs = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 8);
            if (View.DatabaseType == Enums.DatabaseType.CORE)
            {
                return (Tuple<long, List<Tuple<long, Dictionary<string, object>>>>)dbConnectionCore.DB.ExecuteProcedure(
                                        "imrse_u_GetFilterFlexView",
                                        new DB.AnalyzeDataSet(GetFilterFlexView),
                                        new DB.Parameter[] {
                                        new DB.InParameter("@PAGE_NBR", PageNbr),
                                        new DB.InParameter("@PAGE_SIZE", PageSize),
                                        new DB.InParameter("@PRIMARY_KEY_COLUMN_NAME", String.Format("[{0}]", View.PrimaryKeyColumn)),
                                        new DB.InParameter("@ID_IMR_SERVER_COLUMN_NAME", String.Format("[{0}]", View.ImrServerColumn)),
                                        new DB.InParameter("@CUSTOM_SELECT_CLAUSE", SqlDbType.NVarChar, customSelectClause, (String.IsNullOrEmpty(customSelectClause) ? 4000 : customSelectClause.Length)),
                                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, (String.IsNullOrEmpty(customWhereClause) ? 4000 : customWhereClause.Length)),
                                        new DB.InParameter("@CUSTOM_ORDER_CLAUSE", SqlDbType.NVarChar, customOrderClause, (String.IsNullOrEmpty(customOrderClause) ? 4000 : customOrderClause.Length)),
                                        new DB.InParameter("@VIEW_NAME", View.Name),
                                        new DB.InParameter("@ID_ARGS", idArgs)},
                                        false, IsolationLevel.ReadUncommitted, 180);
            }
            else
            {
                return (Tuple<long, List<Tuple<long, Dictionary<string, object>>>>)dbConnectionDw.DB.ExecuteProcedure(
                                        "imrse_u_GetFilterFlexView",
                                        new DB.AnalyzeDataSet(GetFilterFlexView),
                                        new DB.Parameter[] {
                                        new DB.InParameter("@PAGE_NBR", PageNbr),
                                        new DB.InParameter("@PAGE_SIZE", PageSize),
                                        new DB.InParameter("@PRIMARY_KEY_COLUMN_NAME", String.Format("[{0}]", View.PrimaryKeyColumn)),
                                        new DB.InParameter("@ID_IMR_SERVER_COLUMN_NAME", String.Format("[{0}]", View.ImrServerColumn)),
                                        new DB.InParameter("@CUSTOM_SELECT_CLAUSE", SqlDbType.NVarChar, customSelectClause, (String.IsNullOrEmpty(customSelectClause) ? 4000 : customSelectClause.Length)),
                                        new DB.InParameter("@CUSTOM_WHERE_CLAUSE", SqlDbType.NVarChar, customWhereClause, (String.IsNullOrEmpty(customWhereClause) ? 4000 : customWhereClause.Length)),
                                        new DB.InParameter("@CUSTOM_ORDER_CLAUSE", SqlDbType.NVarChar, customOrderClause, (String.IsNullOrEmpty(customOrderClause) ? 4000 : customOrderClause.Length)),
                                        new DB.InParameter("@VIEW_NAME", View.Name),
                                        new DB.InParameter("@ID_ARGS", idArgs)},
                                        false, IsolationLevel.ReadUncommitted, 180);
            }
        }

        private object GetFilterFlexView(DataSet QueryResult)
        {
            List<Tuple<long, Dictionary<string, object>>> retList = new List<Tuple<long, Dictionary<string, object>>>();
            TraceExecutionTime packageTraceTimes = null;
            long totalCount = 0;
            
            if (QueryResult.Tables != null && QueryResult.Tables.Count > 0)
            {
                string namesList = "";
                foreach (DataColumn dcItem in QueryResult.Tables[0].Columns)
                    namesList += dcItem.ColumnName + ",";
                int primaryFieldColumnIndex = 0;

                Dictionary<string, Enums.DataTypeClass> columnClassDict = new Dictionary<string, Enums.DataTypeClass>();
                #region ColumnClass

                foreach (DataColumn dcItem in QueryResult.Tables[0].Columns)
                {
                    Enums.DataTypeClass dtcItem = Enums.DataTypeClass.Unknown;
                    try
                    {
                        long idDataType = OpViewColumn.GetColumnIdDataType(dcItem.ColumnName);
                        if (idDataType > 0)
                        {
                            OpDataType dtItem = GetDataType(idDataType);
                            if (dtItem != null)
                            {
                                dtcItem = (Enums.DataTypeClass)dtItem.IdDataTypeClass;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                            
                    }
                    finally
                    {
                        columnClassDict[dcItem.ColumnName] = dtcItem;
                    }
                }
                #endregion

                foreach (DataRow drItem in QueryResult.Tables[0].Rows)
                {
                    Dictionary<string, object> dict = new Dictionary<string, object>();
                    long primaryKey = Convert.ToInt64(drItem[primaryFieldColumnIndex]);
                    object value = null;
                    foreach (DataColumn dcItem in QueryResult.Tables[0].Columns)
                    {
                        if (drItem[dcItem] == null || drItem[dcItem] == System.DBNull.Value)
                            value = null;
                        else
                            value = drItem[dcItem];
                        if (columnClassDict[dcItem.ColumnName] == Enums.DataTypeClass.Datetime)
                            value = dbConnectionCore.ConvertUtcTimeToTimeZone(Convert.ToDateTime(value));
                        dict.Add(dcItem.ColumnName, drItem[dcItem] == null || drItem[dcItem] == System.DBNull.Value ? null : drItem[dcItem]);
                    }
                    retList.Add(new Tuple<long, Dictionary<string, object>>(primaryKey, dict));
                }

                if (QueryResult.Tables.Count > 1 && QueryResult.Tables[1] != null && QueryResult.Tables[1].Rows != null && QueryResult.Tables[1].Rows.Count > 0)
                {
                    long totalCountTmp = 0;
                    if (long.TryParse(QueryResult.Tables[1].Rows[0][0].ToString(), out totalCountTmp))
                        totalCount = totalCountTmp;
                }
            }
            
            
            return new Tuple<long, List<Tuple<long, Dictionary<string, object>>>>(totalCount, retList);
        }
        
        #endregion

        #endregion

        #region AddActionWithActionDataListToQueue
        public virtual void AddActionWithActionDataListToQueue(long? IdAction, long IdActionType, int IdActionStatus, long? IdActionData,
            long? SerialNbr, long? IdMeter, long? IdLocation, int IdModule, long? IdOperator, string XmlActionData, string QueuePath, int? IdServer,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.Serializable, int commandTimeout = 0, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    try
                    {
                        dbCollectorClient.AddActionWithActionDataListToQueue(ref dbCollectorSession, IdAction, IdActionType, IdActionStatus, IdActionData, SerialNbr, IdMeter, IdLocation,
                            IdModule, IdOperator, XmlActionData, QueuePath, IdServer, autoTransaction, transactionLevel, commandTimeout);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.DBCollectorClient, "AddActionWithActionDataListToQueue", ex);
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.AddActionWithActionDataListToQueue(IdAction, IdActionType, IdActionStatus, IdActionData, SerialNbr, IdMeter, IdLocation, 
                        IdModule, IdOperator, XmlActionData, QueuePath, autoTransaction, transactionLevel, commandTimeout);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "sp_AddActionWithActionDataListToQueue", ex);
                throw;
            }
        }
        #endregion

        #region CheckIfDataBaseExists
        public virtual bool? CheckIfDataBaseExists(string name)
        {
            try
            {
                return dbConnectionCore.CheckIfDataBaseExists(name);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_CheckIfDatabaseExists", ex);
                throw ex;
            }
        }
        #endregion

        #region CheckIfTableExists
        public virtual bool CheckIfTableExists(string name)
        {
            try
            {
                return dbConnectionCore.CheckIfTableExists(name);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_CheckIfTableExists", ex);
                throw ex;
            }
        }
        #endregion

        #region CheckIfLinkedServerExists
        public virtual bool CheckIfLinkedServerExists(string name)
        {
            try
            {
                return dbConnectionCore.CheckIfLinkedServerExists(name);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_CheckIfLinkedServerExists", ex);
                throw ex;
            }
        }
        #endregion

        #region CheckIfLoginExists
        public virtual bool CheckIfLoginExists(string login)
        {
            try
            {
                return dbConnectionCore.CheckIfLoginExists(login);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_CheckIfLoginExists", ex);
                throw ex;
            }
        }
        #endregion

        #region CheckIfStoredProcedureExists
        public virtual bool CheckIfStoredProcedureExists(string storedProcedure, Enums.DatabaseType databaseType, int? idServer, bool useDBCollector = false)
        {
            bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
            if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
            {
                try
                {
                    return dbCollectorClient.CheckIfStoredProcedureExists(ref dbCollectorSession, storedProcedure, databaseType, idServer);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.DBCollectorClient, "CheckIfStoredProcedureExists", ex);
                }
            }
            else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
            {
                switch (databaseType)
                {
                    case Enums.DatabaseType.CORE:
                    case Enums.DatabaseType.CORE_IMRSC:
                        if (dbConnectionCore != null)
                            return dbConnectionCore.StoredProcedureExists(storedProcedure);
                        break;
                    case Enums.DatabaseType.DAQ:
                        if (dbConnectionDaq != null)
                            return dbConnectionDaq.StoredProcedureExists(storedProcedure);
                        break;
                    case Enums.DatabaseType.DW:
                        if (dbConnectionDw != null)
                            return dbConnectionDw.StoredProcedureExists(storedProcedure);
                        break;
                }
            }

            return false;
        }
        #endregion

        #region ManageDistributorCORE
        public virtual void ManageDistributorCORE(OpDistributor toBeSaved)
        {
            try
            {
                dbConnectionCore.ManageDistributorCORE(toBeSaved);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_ManageDistributorsCORE", ex);
                throw ex;
            }
        }
        #endregion

        #region AddDevice
        public virtual void AddDevice(OpDevice toBeSaved, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_DEVICE device = new DB_DEVICE(toBeSaved);
                    dbCollectorClient.AddDevice(ref dbCollectorSession, device);
                }
                else
                {
                    dbConnectionCore.AddDevice(toBeSaved);
                }

                if (DeviceCacheEnabled && DeviceDict != null)
                {
                    DeviceDict[toBeSaved.SerialNbr] = toBeSaved; //add or update element
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_AddDevice", ex);
                throw ex;
            }
        }
        #endregion

        #region AddDeviceType
        public virtual void AddDeviceType(OpDeviceType toBeSaved)
        {
            try
            {
                dbConnectionCore.AddDeviceType(toBeSaved);
                if (DeviceTypeCacheEnabled && DeviceTypeDict != null)
                {
                    DeviceTypeDict[toBeSaved.IdDeviceType] = toBeSaved; //add or update element
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_AddDeviceType", ex);
                throw ex;
            }
        }
        #endregion

        #region DataTypeValue
        public virtual DB.InParameter DataTypeValue(OpDataTypeClass.Enum dataTypeClass, object Value, string ParamName)
        {
            DB.InParameter ip = new DB.InNullParameter(ParamName);
            try
            {
                switch (dataTypeClass)
                {
                    case OpDataTypeClass.Enum.Integer:
                        if (Value != null)
                            ip = new DB.InParameter(ParamName, (int)Convert.ToInt32(Value));
                        else
                            ip = new DB.InParameter(ParamName, SqlDbType.Int, DBNull.Value);
                        break;
                    case OpDataTypeClass.Enum.Real:
                        if (Value != null)
                            ip = new DB.InParameter(ParamName, (Double)Convert.ToDouble(Value));
                        else
                            ip = new DB.InParameter(ParamName, SqlDbType.Real, DBNull.Value);
                        break;
                    case OpDataTypeClass.Enum.Xml:
                    case OpDataTypeClass.Enum.Text:
                        if (Value != null)
                            ip = new DB.InParameter(ParamName, Value.ToString());
                        else
                            ip = new DB.InParameter(ParamName, SqlDbType.Text, DBNull.Value);
                        break;
                    case OpDataTypeClass.Enum.Date:
                    case OpDataTypeClass.Enum.Time:
                    case OpDataTypeClass.Enum.Datetime:
                        if (Value != null)
                            ip = new DB.InParameter(ParamName, ConvertTimeZoneToUtcTime(((DateTime)Convert.ToDateTime(Value))));
                        else
                            ip = new DB.InParameter(ParamName, SqlDbType.DateTime, DBNull.Value);
                        break;
                    case OpDataTypeClass.Enum.Boolean:
                        if (Value != null)
                            ip = new DB.InParameter(ParamName, (bool)Convert.ToBoolean(Value));
                        else
                            ip = new DB.InParameter(ParamName, SqlDbType.Bit, DBNull.Value);
                        break;
                    case OpDataTypeClass.Enum.Object:
                    case OpDataTypeClass.Enum.Binary:
                        if (Value != null)
                            ip = new DB.InParameter(ParamName, SqlDbType.VarBinary, (byte[])Value, 8000);
                        else
                            ip = new DB.InParameter(ParamName, SqlDbType.VarBinary, DBNull.Value);
                        break;
                    case OpDataTypeClass.Enum.Color:
                        if (Value != null)
                            ip = new DB.InParameter(ParamName, SqlDbType.VarChar, (string)Value);
                        else
                            ip = new DB.InParameter(ParamName, SqlDbType.VarChar, DBNull.Value);
                        break;
                    default:
                        throw new Exception(String.Format("Unable to convert data type value of type {0} as data type class {1}", dataTypeClass, Value.GetType()));
                }
            }
            catch (Exception)
            {
                throw new Exception(String.Format("Trying to save data value as {0} when the given value is of type: {1}", dataTypeClass.ToString(), Value.GetType().ToString()));
            }
            return ip;
        }
        #endregion

        #region GetTriadaData
        public virtual System.Tuple<List<OpLocation>, List<OpMeter>, List<OpDevice>> GetTriadaData(List<OpLocation> locations, List<OpDataType> locationDataTypes, List<OpMeter> meters, List<OpDataType> meterDataTypes, List<OpDevice> devices, List<OpDataType> deviceDataTypes)
        {
            return GetTriadaData(locations, locationDataTypes.Select(s => s.IdDataType).ToList(),
                                 meters, meterDataTypes.Select(s => s.IdDataType).ToList(),
                                 devices, deviceDataTypes.Select(s => s.IdDataType).ToList());
        }
        public virtual System.Tuple<List<OpLocation>, List<OpMeter>, List<OpDevice>> GetTriadaData(List<OpLocation> locations, List<long> locationDataTypes, List<OpMeter> meters, List<long> meterDataTypes, List<OpDevice> devices, List<long> deviceDataTypes)//, System.Diagnostics.Stopwatch sw = null, System.Text.StringBuilder txt = null)
        {
            bool loadLocationHierarchy = (locationDataTypes != null && locationDataTypes.Exists(d => d == Common.DataType.HELPER_LOCATION_PARENT_GROUPS) ? true : false);
            if (locationDataTypes.Exists(d => d == Common.DataType.HELPER_LOCATION_PARENT_GROUPS))
                locationDataTypes.RemoveAll(d => d == Common.DataType.HELPER_LOCATION_PARENT_GROUPS);

            int commandTimeout = dbConnectionCore.ShortTimeout + (int)(((locations == null ? 0 : locations.Count) + (meters == null ? 0 : meters.Count) + (devices == null ? 0 : devices.Count)) / 1000);

            //if (sw != null) txt.AppendFormat("1: {0},", sw.ElapsedMilliseconds);
            System.Tuple<DB_LOCATION[], DB_METER[], DB_DEVICE[]> triadaTuple =
                dbConnectionCore.GetTriadaData(locations == null ? null : locations.Select(s => s.IdLocation).ToList(), locationDataTypes,
                                               meters == null ? null : meters.Select(s => s.IdMeter).ToList(), meterDataTypes,
                                               devices == null ? null : devices.Select(s => s.SerialNbr).ToList(), deviceDataTypes,
                                               commandTimeout: commandTimeout);//, sw, txt);

            //if (sw != null) txt.AppendFormat("2: {0},", sw.ElapsedMilliseconds);
            List<OpLocation> opLocations = OpLocation.ConvertList(triadaTuple.Item1, this, true, false);
            //if (sw != null) txt.AppendFormat("3: {0},", sw.ElapsedMilliseconds);
            List<OpMeter> opMeters = OpMeter.ConvertList(triadaTuple.Item2, this, true, false);
            //if (sw != null) txt.AppendFormat("4: {0},", sw.ElapsedMilliseconds);
            List<OpDevice> opDevices = OpDevice.ConvertList(triadaTuple.Item3, this, true, false);
            //if (sw != null) txt.AppendFormat("5: {0},", sw.ElapsedMilliseconds);


            List<OpLocationEquipment> equipments = GetLocationEquipmentFilter(EndTime: TypeDateTimeCode.Null(),
                IdLocation: new long[0], IdMeter: new long[0], SerialNbr: new long[0], // specjalnie, aby pominac filtry z DBCommon
                loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false).ToList();
            //if (sw != null) txt.AppendFormat("6: {0},", sw.ElapsedMilliseconds);
            List<OpLocationHierarchy> hierarchy = null;
            if (loadLocationHierarchy)
                hierarchy = GetLocationHierarchyFilter(IdLocationHierarchy: new long[0], IdLocationHierarchyParent: new long[0], IdLocation: new long[0],
                                                       loadNavigationProperties: false, mergeIntoCache: false, 
                                                       autoTransaction: false, transactionLevel: IsolationLevel.ReadUncommitted);
            Dictionary<long, OpLocation> locactionsDict;
            Dictionary<long, OpMeter> metersDict;
            Dictionary<long, OpDevice> devicesDict;
            try
            {
                locactionsDict = new Dictionary<long, OpLocation>(opLocations.ToDictionary(d => d.IdLocation));
            }
            catch (ArgumentException ex) 
            {
                var repetitions = opLocations.GroupBy(x => x.IdLocation).Where(c => c.Count() > 1).Select(q => new { Element = q.Key, Counter = q.Count() }).ToList();
                throw new ArgumentException(String.Join(";", repetitions) + ex.Message);
            }
            try
            {
                metersDict = new Dictionary<long, OpMeter>(opMeters.ToDictionary(d => d.IdMeter));
            }
            catch(ArgumentException ex)
            {
                var repetitions = opMeters.GroupBy(x => x.IdMeter).Where(c => c.Count() > 1).Select(q => new { Element = q.Key, Counter = q.Count() }).ToList();
                throw new ArgumentException(String.Join(";", repetitions) + ex.Message);
            }
            try
            {
                devicesDict = new Dictionary<long, OpDevice>(opDevices.ToDictionary(d => d.SerialNbr));
            }
            catch (ArgumentException ex) 
            {
                var repetitions = opDevices.GroupBy(x => x.SerialNbr).Where(c => c.Count() > 1).Select(q => new { Element = q.Key, Counter = q.Count() }).ToList();
                throw new ArgumentException(String.Join(";", repetitions) + ex.Message);
            }
            //if (sw != null) txt.AppendFormat("7: {0},", sw.ElapsedMilliseconds);

            #region Equipment

            foreach (OpLocationEquipment equ in equipments)
            {
                if (equ.SerialNbr.HasValue && devicesDict.ContainsKey(equ.SerialNbr.Value))
                {
                    if (equ.IdMeter.HasValue && metersDict.ContainsKey(equ.IdMeter.Value))
                    {
                        if (!devicesDict[equ.SerialNbr.Value].CurrentMeters.Contains(metersDict[equ.IdMeter.Value]))
                            devicesDict[equ.SerialNbr.Value].CurrentMeters.Add(metersDict[equ.IdMeter.Value]);
                    }
                    if (equ.IdLocation.HasValue && locactionsDict.ContainsKey(equ.IdLocation.Value))
                    {
                        if (!devicesDict[equ.SerialNbr.Value].CurrentLocations.Contains(locactionsDict[equ.IdLocation.Value]))
                            devicesDict[equ.SerialNbr.Value].CurrentLocations.Add(locactionsDict[equ.IdLocation.Value]);
                    }
                    devicesDict[equ.SerialNbr.Value].InstallationDate = equ.StartTime;
                }
                if (equ.IdLocation.HasValue && locactionsDict.ContainsKey(equ.IdLocation.Value))
                {
                    if (equ.IdMeter.HasValue && metersDict.ContainsKey(equ.IdMeter.Value))
                    {
                        if (!locactionsDict[equ.IdLocation.Value].CurrentMeters.Contains(metersDict[equ.IdMeter.Value]))
                            locactionsDict[equ.IdLocation.Value].CurrentMeters.Add(metersDict[equ.IdMeter.Value]);
                    }
                    if (equ.SerialNbr.HasValue && devicesDict.ContainsKey(equ.SerialNbr.Value))
                    {
                        if (!locactionsDict[equ.IdLocation.Value].CurrentDevices.Contains(devicesDict[equ.SerialNbr.Value]))
                            locactionsDict[equ.IdLocation.Value].CurrentDevices.Add(devicesDict[equ.SerialNbr.Value]);
                    }
                }
                if (equ.IdMeter.HasValue && metersDict.ContainsKey(equ.IdMeter.Value))
                {
                    if (equ.IdLocation.HasValue && locactionsDict.ContainsKey(equ.IdLocation.Value))
                    {
                        if (!metersDict[equ.IdMeter.Value].CurrentLocations.Contains(locactionsDict[equ.IdLocation.Value]))
                            metersDict[equ.IdMeter.Value].CurrentLocations.Add(locactionsDict[equ.IdLocation.Value]);
                    }
                    if (equ.SerialNbr.HasValue && devicesDict.ContainsKey(equ.SerialNbr.Value))
                    {
                        if (!metersDict[equ.IdMeter.Value].CurrentDevices.Contains(devicesDict[equ.SerialNbr.Value]))
                            metersDict[equ.IdMeter.Value].CurrentDevices.Add(devicesDict[equ.SerialNbr.Value]);
                    }
                    metersDict[equ.IdMeter.Value].InstallationDate = equ.StartTime;
                }
            }

            #endregion
            #region Hierarchy

            if (hierarchy != null)
            {
                DateTime dt1 = DateTime.Now;

                Dictionary<long, List<OpLocationHierarchy>> locationHierarchyByLocationDict = hierarchy
                    .GroupBy(q => q.IdLocation).ToDictionary(q => q.Key, q => q.ToList());
             //   Dictionary<long, List<OpLocationHierarchy>> locationHierarchyByHierarchyParentDict = hierarchy.Where(q => q.IdLocationHierarchyParent.HasValue)
                //       .GroupBy(q => q.IdLocationHierarchyParent.Value).ToDictionary(q => q.Key, q => q.ToList());
                Dictionary<long, List<OpLocationHierarchy>> locationHierarchyByHierarchyParentDict = hierarchy
                    .GroupBy(q => q.IdLocationHierarchy).ToDictionary(q => q.Key, q => q.ToList());
                
                DateTime dt2 = DateTime.Now;

                List<long> groupsToLoad = Components.CORE.LocationHierarchyComponent.GetParentIdLocations(locationHierarchyByLocationDict, locationHierarchyByHierarchyParentDict, hierarchy.Select(l => l.IdLocation).Distinct().ToList());
                if(groupsToLoad.Count > 0)
                {
                    Dictionary<long, OpLocation> locationGroups = GetLocation(groupsToLoad.ToArray()).ToDictionary(l => l.IdLocation);
                    foreach(OpLocationHierarchy hier in hierarchy)
                    {
                        if(locactionsDict.ContainsKey(hier.IdLocation))
                        {
                            List<long> locationParents = Components.CORE.LocationHierarchyComponent.GetParentIdLocations(locationHierarchyByLocationDict, locationHierarchyByHierarchyParentDict, new List<long>() { hier.IdLocation });
                            if(locationParents.Count > 0)
                            {
                                locationParents.ForEach(l => { if (locationGroups.ContainsKey(l) && !locactionsDict[hier.IdLocation].ParentGroups.Exists(pl => pl.IdLocation == l)) locactionsDict[hier.IdLocation].ParentGroups.Add(locationGroups[l]); });
                            }
                        }
                    }
                }
                TimeSpan ts1 = dt2 - dt1;
                TimeSpan ts2 = DateTime.Now - dt2;
            }

            #endregion
            //if (sw != null) txt.AppendFormat("8: {0},", sw.ElapsedMilliseconds);

            return System.Tuple.Create<List<OpLocation>, List<OpMeter>, List<OpDevice>>(opLocations, opMeters, opDevices);
        }
        #endregion
        #region GetAdditionalInfoForLocations
        public virtual void GetAdditionalInfoForLocations(List<OpLocation> locations)
        {
            dbConnectionCore.GetAdditionalInfoForLocations(locations.Cast<DB_LOCATION>().ToList());
        }
        #endregion

        #region Area

        private List<OpArea> _Area;
        public virtual int AreaCount
        {
            get
            {
                if (_Area == null)
                {
                    return 0;
                }
                return _Area.Count;
            }
        }
        public virtual List<OpArea> Area
        {
            get
            {
                if (_Area == null)
                    _Area = OpArea.CreateAreas(this);

                return _Area;
            }
        }

        public virtual List<OpArea> GetAllArea()
        {
            return (_Area == null) ? OpArea.CreateAreas(this) : _Area;
        }

        public virtual void ClearAreaCache()
        {
            _Area = null;
        }
        #endregion

        #region SaveProtocolDriver
        /// <summary>
        /// Saves the ProtocolDriver object to the database
        /// </summary>
        /// <param name="toBeSaved">ProtocolDriver to save</param>
        /// <returns>ProtocolDriver Id</returns>
        public virtual int SaveProtocolDriver(OpProtocolDriver toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveProtocolDriver(toBeSaved);
                if (ProtocolDriverCacheEnabled)
                {
                    ProtocolDriverDict[toBeSaved.IdProtocolDriver] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveProtocolDriver", ex);
                throw;
            }
        }
        #endregion

        #region SaveDeviceTypeGroup
        /// <summary>
        /// Saves the DeviceTypeGroup object to the database
        /// </summary>
        /// <param name="toBeSaved">DeviceTypeGroup to save</param>
        /// <returns>DeviceTypeGroup Id</returns>
        public virtual int SaveDeviceTypeGroup(OpDeviceTypeGroup toBeSaved)
        {
            try
            {
                int ret = dbConnectionCore.SaveDeviceTypeGroup(toBeSaved);
                if (DeviceTypeGroupCacheEnabled)
                {
                    DeviceTypeGroupDict[toBeSaved.IdDeviceTypeGroup] = toBeSaved; //add or update element
                }
                return ret;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDeviceTypeGroup", ex);
                throw;
            }
        }
        #endregion


        #region SaveActionDef
        /// <summary>
        /// Saves the ActionDef object to the database
        /// </summary>
        /// <param name="toBeSaved">ActionDef to save</param>
        /// <returns>ActionDef Id</returns>
        public virtual long SaveActionDef(OpActionDef toBeSaved, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_ACTION_DEF db_actionDef = new DB_ACTION_DEF(toBeSaved);
                    toBeSaved.ID_ACTION_DEF = dbCollectorClient.SaveActionDef(ref dbCollectorSession, db_actionDef);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                    }
                    if (!throwDBCollEx)
                    {
                        return toBeSaved.ID_ACTION_DEF;
                    }
                    else
                        throw dbCollEx;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    if (toBeSaved.DataType == null && toBeSaved.IdDataType.HasValue)
                        toBeSaved.DataType = GetDataType(toBeSaved.IdDataType.Value);
                    long ret = dbConnectionCore.SaveActionDef(toBeSaved, toBeSaved.DataType);
                    if (ActionDefCacheEnabled)
                    {
                        ActionDefDict[toBeSaved.IdActionDef] = toBeSaved; //add or update element
                    }
                    return ret;
                }
                else
                    return 0;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_SaveActionDef", ex);
                throw;
            }
        }
        #endregion

        #region ScheduleAction
        /// <summary>
        /// Indicates whether the ScheduleActionDict was filled trough GetAllScheduleAction or GetAllScheduleActionFilter with mergeIntoCache = true
        /// </summary>
        public virtual bool ScheduleActionIsFullCache { get; protected set; }

        /// <summary>
        /// ScheduleAction objects dictionary (Cache)
        /// </summary>
        public Dictionary<long, OpScheduleAction> ScheduleActionDict = new Dictionary<long, OpScheduleAction>();

        /// <summary>
        /// Indicates whether the ScheduleAction is cached.
        /// </summary>
        public bool ScheduleActionCacheEnabled { get; set; }

        /// <summary>
        /// Gets all ScheduleAction objects
        /// </summary>
        public virtual List<OpScheduleAction> ScheduleAction
        {
            get { return GetAllScheduleAction(); }
        }

        /// <summary>
        /// Gets all ScheduleAction objects (cache disabled)
        /// </summary>
        public virtual List<OpScheduleAction> GetAllScheduleAction()
        {
            try
            {
                if (ScheduleActionCacheEnabled) //cache enabled
                {
                    if (!ScheduleActionIsFullCache)
                    {
                        List<OpScheduleAction> objectList = OpScheduleAction.ConvertList(dbConnectionCore.GetScheduleAction(), this);
                        if (objectList != null)
                        {
                            ScheduleActionDict = objectList.ToDictionary(o => o.IdScheduleAction);
                            ScheduleActionIsFullCache = true;
                        }
                    }
                    return new List<OpScheduleAction>(ScheduleActionDict.Values);
                }
                else //cache disabled
                {
                    return OpScheduleAction.ConvertList(dbConnectionCore.GetScheduleAction(), this);
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetScheduleAction", ex);
                throw;
            }
        }


        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Id">ScheduleAction Id</param>
        /// <returns>ScheduleAction object</returns>
        public virtual OpScheduleAction GetScheduleAction(long Id)
        {
            return GetScheduleAction(Id, false);
        }

        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Ids">ScheduleAction Ids</param>
        /// <returns>ScheduleAction list</returns>
        public virtual List<OpScheduleAction> GetScheduleAction(long[] Ids)
        {
            return GetScheduleAction(Ids, false);
        }

        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Id">ScheduleAction Id</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ScheduleAction object</returns>
        public virtual OpScheduleAction GetScheduleAction(long Id, bool queryDatabase)
        {
            return GetScheduleAction(new long[] { Id }, queryDatabase).FirstOrDefault();
        }

        /// <summary>
        /// Gets ScheduleAction by id
        /// </summary>
        /// <param name="Ids">ScheduleAction Ids</param>
        /// <param name="queryDatabase">If True, gets object from database</param>
        /// <returns>ScheduleAction list</returns>
        public virtual List<OpScheduleAction> GetScheduleAction(long[] Ids, bool queryDatabase)
        {
            List<OpScheduleAction> returnList = new List<OpScheduleAction>();
            if (Ids != null && Ids.Length > 0)
            {
                if (ScheduleActionCacheEnabled && !queryDatabase)// cache enabled and query database is false
                {
                    List<long> idsNotInCache = new List<long>();
                    for (long i = 0; i < Ids.Length; i++)
                    {
                        OpScheduleAction ScheduleActionFoundInCache;
                        if (ScheduleActionDict.TryGetValue(Ids[i], out ScheduleActionFoundInCache)) //element founded in cache
                            returnList.Add(ScheduleActionFoundInCache);
                        else //element not found in cache, query database
                            idsNotInCache.Add(Ids[i]);
                    }
                    if (idsNotInCache.Count > 0)
                    {
                        try
                        {
                            foreach (OpScheduleAction item in OpScheduleAction.ConvertList(dbConnectionCore.GetScheduleAction(idsNotInCache.ToArray()), this))
                            {
                                ScheduleActionDict[item.IdScheduleAction] = item;
                                returnList.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetScheduleAction", ex);
                            throw;
                        }
                    }
                }
                else //cache disabled or user force to query database 
                {
                    try
                    {
                        return OpScheduleAction.ConvertList(dbConnectionCore.GetScheduleAction(Ids), this);
                    }
                    catch (Exception ex)
                    {
                        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetScheduleAction", ex);
                        throw;
                    }
                }
            }
            return returnList;
        }

        /// <summary>
        /// Saves the ScheduleAction object to the database
        /// </summary>
        /// <param name="toBeSaved">ScheduleAction to save</param>
        /// <returns>ScheduleAction Id</returns>
        public virtual int SaveScheduleAction(OpScheduleAction toBeSaved, bool useDBCollector = false)
        {
            Exception dbCollEx = null;
            bool throwDBCollEx = false;
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_SCHEDULE_ACTION db_scheduleAction = new DB_SCHEDULE_ACTION(toBeSaved);
                    db_scheduleAction.ID_SCHEDULE = dbCollectorClient.SaveScheduleAction(ref dbCollectorSession, db_scheduleAction);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                    }
                    if (!throwDBCollEx)
                    {
                        return toBeSaved.IdSchedule;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.SaveScheduleAction(toBeSaved);
                    if (ScheduleActionCacheEnabled)
                    {
                        ScheduleActionDict[toBeSaved.IdScheduleAction] = toBeSaved; //add or update element
                    }
                    return toBeSaved.IdSchedule;// ret;
                }
                else
                    return toBeSaved.IdSchedule;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveScheduleAction", ex);
                throw;
            }
            throw dbCollEx;
        }

        /// <summary>
        /// Deletes the ScheduleAction object from the database
        /// </summary>
        /// <param name="toBeDeleted">ScheduleAction to delete</param>
        public virtual void DeleteScheduleAction(OpScheduleAction toBeDeleted, bool useDBCollector = false)
        {
            try
            {
                Exception dbCollEx = null;
                bool throwDBCollEx = false;
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    DB_SCHEDULE_ACTION db_scheduleAction = new DB_SCHEDULE_ACTION(toBeDeleted);
                    dbCollectorClient.DeleteScheduleAction(ref dbCollectorSession, db_scheduleAction);
                    if (dbCollectorSession.Status == WCF.SESSION_STATUS.ERROR && dbCollectorSession.Error.Code is WCF.ERROR_CODE)
                    {
                        switch ((WCF.ERROR_CODE)dbCollectorSession.Error.Code)
                        {
                            case WCF.ERROR_CODE.IdsNotMapped:
                            case WCF.ERROR_CODE.ServerNotFound:
                            case WCF.ERROR_CODE.MSMQParamsNotFound:
                                dbCollEx = new Exception(dbCollectorSession.Error.Msg);
                                throwDBCollEx = true;
                                break;
                        }
                        if (throwDBCollEx)
                            throw dbCollEx;
                    }
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    dbConnectionCore.DeleteScheduleAction(toBeDeleted);
                    if (ScheduleActionCacheEnabled)
                    {
                        ScheduleActionDict.Remove(toBeDeleted.IdScheduleAction);
                    }
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelScheduleAction", ex);
                throw;
            }
        }

        /// <summary>
        /// Clears the ScheduleAction object cache
        /// </summary>
        public virtual void ClearScheduleActionCache()
        {
            ScheduleActionDict.Clear();
            ScheduleActionIsFullCache = false;
        }
        #endregion

        #region SimCard

        #region GetSimCardDataBySerialNbr

        public virtual List<OpDeviceSimCard> GetSimCardDataBySerialNbr(long[] SerialNbr, int[] IdDistributor, bool loadNavigationProperties = true)
        {

            try
            {
                List<OpDeviceSimCard> returnList = OpDeviceSimCard.ConvertList(dbConnectionCore.GetSimCardDataBySerialNbr(SerialNbr, IdDistributor),
                    this, loadNavigationProperties);
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetSimCardDataBySerialNbr", ex);
                throw;
            }

        }

        #endregion

        #region GetSimCardBySerialNbr

        public virtual List<OpDeviceSimCard> GetSimCardBySerialNbr(long[] SerialNbr, int[] IdDistributor, bool loadNavigationProperties = true)
        {

            try
            {
                List<OpDeviceSimCard> returnList = OpDeviceSimCard.ConvertList(dbConnectionCore.GetSimCardBySerialNbr(SerialNbr, IdDistributor), this, loadNavigationProperties);
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetSimCardBySerialNbr", ex);
                throw;
            }

        }

        #endregion
		
		#region SaveSimCard
		
		public virtual void SaveSimCard(OpSimCard[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false)
        {
			try
			{
				if (toBeSaved != null && toBeSaved.Count() > 0)
				{
                    List<OpSimCard> toUpdate = toBeSaved.Where(x => x.IdSimCard > 0).ToList();
                    List<OpSimCard> toInsert = toBeSaved.Where(x => x.IdSimCard == 0).ToList();

					dbConnectionCore.SaveSimCard(toBeSaved, updateSetSkipColumns, commandTimeout);

                    InvokeDataChanged(toInsert, NotifyType.Insert, typeof(OpSimCard));
                    InvokeDataChanged(toUpdate, NotifyType.Update, typeof(OpSimCard));
				}                
			}
			catch (Exception ex)
			{
				IMRLog.AddToLog(EventID.DataProvider.BulkSave, "SIM_CARD", ex);
                throw;
			}
        }
		
		#endregion
		
		#region SaveSimCardData

        public virtual void SaveSimCardData(OpSimCardData[] toBeSaved, List<string> updateSetSkipColumns = null, int commandTimeout = 0, bool useDBCollector = false)
        {
            try
            {
                if (toBeSaved != null && toBeSaved.Count() > 0)
                {
                    Dictionary<long, IMR.Suite.UI.Business.Objects.CORE.OpDataType> dtDict = this.GetDataType(toBeSaved.Select(d => d.IdDataType).Distinct().ToArray()).ToDictionary(d => d.IdDataType);
                    foreach (OpSimCardData dtItem in toBeSaved)
                        dtItem.DataType = dtDict[dtItem.IdDataType];

                    List<OpSimCardData> toUpdate = toBeSaved.Where(x => x.IdSimCardData > 0).ToList();
                    List<OpSimCardData> toInsert = toBeSaved.Where(x => x.IdSimCardData == 0).ToList();

                    dbConnectionCore.SaveSimCardData(toBeSaved, dtDict.Values.ToArray(), updateSetSkipColumns, commandTimeout);

                    InvokeDataChanged(toInsert, NotifyType.Insert, typeof(OpSimCardData));
                    InvokeDataChanged(toUpdate, NotifyType.Update, typeof(OpSimCardData));
                }
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.BulkSave, "SIM_CARD_DATA", ex);
                throw;
            }
        }
		
		#endregion

        #region SaveSimCardDetails

        public virtual void SaveSimCardDetails(int? changeFromLastMinutes, long[] idSimCard, long[] idDataType)
        {
            try
            {
                dbConnectionCore.SaveSimCardDetails(changeFromLastMinutes, idSimCard, idDataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_UpdateSimCardDetails", ex);
                throw;
            }
        }        

        #endregion

        #region DeleteSimCardDetails

        public virtual void DeleteSimCardDetails(int idSimCard)
        {
            try
            {
                dbConnectionCore.DeleteSimCardDetails(idSimCard);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DeleteSimCardDetails", ex);
                throw;
            }
        }
        
        #endregion

        #endregion

        #region GetHierarchicalProfiles
        /// <summary>
        /// Gets all profiles for provided elements, excluding Distributor profiles
        /// </summary>
        /// <returns>Dictionary with IdProfile as keys and array of { SerialNbr, IdMeter, IdLocation, profile parent SerialNbr, profile parent IdMeter, profile parent IdLocation } as values</returns>
        public Dictionary<int, List<long[]>> GetHierarchicalProfiles(IEnumerable<OpDevice> devices = null, IEnumerable<OpMeter> meters = null, IEnumerable<OpLocation> locations = null, IEnumerable<OpDistributor> distributors = null,
                                                                     int[] idProfile = null, long idDataType = IMR.Suite.Common.DataType.PROFILE_ID)
        {
            return GetHierarchicalProfiles(devices == null ? null : devices.Select(q => q.SerialNbr).ToList(),
                meters == null ? null : meters.Select(q => q.IdMeter).ToList(),
                locations == null ? null : locations.Select(q => q.IdLocation).ToList(),
                distributors == null ? null : distributors.Select(q => q.IdDistributor).ToList(),
                idProfile, idDataType);
        }
        /// <summary>
        /// Gets all profiles for provided elements, excluding Distributor profiles
        /// </summary>
        /// <returns>Dictionary with IdProfile as keys and array of { SerialNbr, IdMeter, IdLocation, profile parent SerialNbr, profile parent IdMeter, profile parent IdLocation } as values</returns>
        public Dictionary<int, List<long[]>> GetHierarchicalProfiles(List<long> devices = null, List<long> meters = null, List<long> locations = null, List<int> distributors = null,
                                                                     int[] idProfile = null, long idDataType = IMR.Suite.Common.DataType.PROFILE_ID)
        {
            return dbConnectionCore.GetHierarchicalProfiles(devices == null ? null : devices.ToArray(),
                                                            meters == null ? null : meters.ToArray(),
                                                            locations == null ? null : locations.ToArray(),
                                                            distributors == null ? null : distributors.ToArray(),
                                                            idProfile, idDataType);
        }
        #endregion

        #region GetShippingListDeviceWarranty

        public virtual List<OpShippingListDeviceWarrantyUser> GetShippingListDeviceWarrantyByDistributor(int idDistributor)
        {
            try
            {
                List<OpShippingListDeviceWarrantyUser> returnList = OpShippingListDeviceWarrantyUser.ConvertList(dbConnectionCore.GetShippingListDeviceWarrantyByDistributor(idDistributor), this);
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetShippingListDeviceWarrantyByDistributor", ex);
                throw;
            }
        }

        #endregion

        #region GetDeviceShippingAndServiceHistory

        public virtual List<OpDeviceShippingAndServiceHistoryUser> GetDeviceShippingAndServiceHistory(long serialNbr)
        {
            try
            {
                List<OpDeviceShippingAndServiceHistoryUser> returnList = OpDeviceShippingAndServiceHistoryUser.ConvertList(dbConnectionCore.GetDeviceShippingAndServiceHistory(serialNbr), this);
                return null;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceShippingAndServiceHistory", ex);
                throw;
            }
        }

        #endregion

        #region GetRouteDefByDistributor

        public virtual List<OpRouteDef> GetRouteDefByDistributor(int[] IdDistributor)
        {
            List<OpRouteDef> returnList = new List<OpRouteDef>();
            if (IdDistributor != null && IdDistributor.Length > 0)
            {
                try
                {
                    return OpRouteDef.ConvertList(dbConnectionCore.GetRouteDefByDistributor(IdDistributor), this);
                }
                catch (Exception ex)
                {
                    IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetRouteDefByDistributor", ex);
                    throw;
                }
            }
            return returnList;
        }

        #endregion

        // User Filter
        #region GetDeviceFilter
        /// <summary>
        /// Gets Device list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDevice funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="SerialNbrPattern">Specifies filter for SERIAL_NBR_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Device list</returns>
        public virtual List<OpDevice> GetDeviceFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdDeviceType = null, long[] SerialNbrPattern = null, long[] IdDescrPattern = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null,
                            int[] IdDeviceStateType = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceFilter"));
            }

            try
            {
                List<OpDevice> returnList = OpDevice.ConvertList(dbConnectionCore.GetDeviceFilter(SerialNbr, IdDeviceType, SerialNbrPattern, IdDescrPattern, IdDeviceOrderNumber, IdDistributor,
                                                           IdDeviceStateType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this,
                                                           loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes,
                                                           transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDevice item in returnList)
                        DeviceDict[item.SerialNbr] = item;
                    DeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceFilter", ex);
                throw;
            }

        }

        /*
        public virtual DataTable GetDeviceFilter(int[] IdDeviceType = null, int[] IdDistributor = null, int IdLanguage = 2,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {

            try
            {
                return dbConnectionCore.GetDeviceFilter(IdDeviceType, IdDistributor, IdLanguage, autoTransaction, transactionLevel, commandTimeout);

            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceFilter", ex);
                throw;
            }

        }*/
        #endregion
        #region GetLocationFilter
        /// <summary>
        /// Gets Location list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocation funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdLocationType">Specifies filter for ID_LOCATION_TYPE column</param>
        /// <param name="IdLocationPattern">Specifies filter for ID_LOCATION_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="InKpi">Specifies filter for IN_KPI column</param>
        /// <param name="AllowGrouping">Specifies filter for ALLOW_GROUPING column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>Location list</returns>
        public virtual List<OpLocation> GetLocationFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdLocation = null, int[] IdLocationType = null, long[] IdLocationPattern = null, long[] IdDescrPattern = null, int[] IdDistributor = null, int[] IdConsumer = null,
                            int[] IdLocationStateType = null, bool? InKpi = null, bool? AllowGrouping = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                            bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationFilter"));
            }

            try
            {
                List<OpLocation> returnList = new List<OpLocation>();
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    returnList = OpLocation.ConvertList(dbCollectorClient.GetLocationFilter(ref dbCollectorSession, IdLocation, IdLocationType, IdLocationPattern, IdDescrPattern, IdDistributor, IdConsumer,
                                                                     IdLocationStateType, InKpi, AllowGrouping, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout),
                                                                     this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes,
                                                                     transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    returnList = OpLocation.ConvertList(dbConnectionCore.GetLocationFilter(IdLocation, IdLocationType, IdLocationPattern, IdDescrPattern, IdDistributor, IdConsumer,
                                                                     IdLocationStateType, InKpi, AllowGrouping, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout),
                                                                     this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes,
                                                                     transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);


                }

                if (mergeIntoCache)
                {
                    foreach (OpLocation item in returnList)
                        LocationDict[item.IdLocation] = item;
                    LocationIsFullCache = true;
                }

                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationEquipmentFilter
        /// <summary>
        /// Gets LocationEquipment list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationEquipment funcion</param>
        /// <param name="IdLocationEquipment">Specifies filter for ID_LOCATION_EQUIPMENT column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_EQUIPMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or in DBCollector web service</param>
        /// <returns>LocationEquipment list</returns>
        public virtual List<OpLocationEquipment> GetLocationEquipmentFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdLocationEquipment = null, long[] IdLocation = null, long[] IdMeter = null, long[] SerialNbr = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                             bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationEquipmentFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationEquipmentFilter"));
            }

            try
            {
                List<OpLocationEquipment> returnList = OpLocationEquipment.ConvertList(dbConnectionCore.GetLocationEquipmentFilter(IdLocationEquipment, IdLocation, IdMeter, SerialNbr, StartTime, EndTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout),
                                                                                       this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData);

                if (mergeIntoCache)
                {
                    foreach (OpLocationEquipment item in returnList)
                        LocationEquipmentDict[item.IdLocationEquipment] = item;
                    LocationEquipmentIsFullCache = true;
                }

                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationEquipmentFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterFilter
        /// <summary>
        /// Gets Meter list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeter funcion</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdMeterPattern">Specifies filter for ID_METER_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="customDataTypes">Specifies custom Data Types to be load</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Meter list</returns>
        public virtual List<OpMeter> GetMeterFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, long[] IdMeter = null, int[] IdMeterType = null, long[] IdMeterPattern = null, long[] IdDescrPattern = null, int[] IdDistributor = null,
                                            long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterFilter"));
            }

            try
            {
                List<OpMeter> returnList = OpMeter.ConvertList(dbConnectionCore.GetMeterFilter(IdMeter, IdMeterType, IdMeterPattern, IdDescrPattern, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout),
                                                               this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes,
                                                               transactionLevel: transactionLevel, autoTransaction: autoTransaction, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMeter item in returnList)
                        MeterDict[item.IdMeter] = item;
                    MeterIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetScheduleActionFilter

        /// <summary>
        /// Gets ScheduleAction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllScheduleAction funcion</param>
        /// <param name="IdSchedule">Specifies filter for ID_SCHEDULE column</param>
        /// <param name="IdActionDef">Specifies filter for ID_ACTION_DEF column</param>
        /// <param name="IsEnabled">Specifies filter for IS_ENABLED column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SCHEDULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ScheduleAction list</returns>
        public virtual List<OpScheduleAction> GetScheduleActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdScheduleAction = null, int[] IdSchedule = null, long[] IdActionDef = null, bool? IsEnabled = null, int[] IdOperator = null, int[] IdProfile = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetScheduleActionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetScheduleActionFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpScheduleAction> returnList = OpScheduleAction.ConvertList(dbCollectorClient.GetScheduleActionFilter(ref dbCollectorSession, IdScheduleAction, IdSchedule, IdActionDef, IsEnabled, IdOperator, IdProfile, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, useDBCollector);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpScheduleAction> returnList = OpScheduleAction.ConvertList(dbConnectionCore.GetScheduleActionFilter(IdScheduleAction, IdSchedule, IdActionDef, IsEnabled, IdOperator, IdProfile, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpScheduleAction item in returnList)
                            ScheduleActionDict[item.IdScheduleAction] = item;
                        ScheduleActionIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpScheduleAction>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetScheduleActionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteDefLocationDataFilter
        /// <summary>
        /// Gets RouteDefLocationData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefLocationData funcion (Cache disabled)</param>
        /// <param name="IdRouteDefLocation">Specifies filter for ID_ROUTE_DEF_LOCATION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefLocationData list</returns>
        public virtual List<OpRouteDefLocationData> GetRouteDefLocationDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDefLocation = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteDefLocationDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefLocationDataFilter"));
            }

            try
            {
                List<OpRouteDefLocationData> returnList = OpRouteDefLocationData.ConvertList(dbConnectionCore.GetRouteDefLocationDataFilter(IdRouteDefLocation, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                //if (mergeIntoCache)
                //{
                //    foreach (OpRouteDefLocationData item in returnList)
                //        RouteDefLocationDataDict[item.IdRouteDefLocation] = item;
                //    RouteDefLocationDataIsFullCache = true;
                //}
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefLocationDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetShippingListDeviceFilterCustom

        /// <summary>
        /// Gets ShippingListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListDevice funcion</param>
        /// <param name="IdShippingListDevice">Specifies filter for ID_SHIPPING_LIST_DEVICE column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_DEVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListDevice list</returns>
        public virtual List<OpShippingListDevice> GetShippingListDeviceFilterCustom(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingListDevice = null, int[] IdShippingList = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdArticle = null, TypeDateTimeCode InsertDate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetShippingListDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListDeviceFilter"));
            }

            try
            {
                List<OpShippingListDevice> returnList = OpShippingListDevice.ConvertListCustom(dbConnectionCore.GetShippingListDeviceFilter(IdShippingListDevice, IdShippingList, SerialNbr, IdMeter, IdArticle, InsertDate,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpShippingListDevice item in returnList)
                        ShippingListDeviceDict[item.IdShippingListDevice] = item;
                    ShippingListDeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetShippingListDeviceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListDeviceFilterCustom

        /// <summary>
        /// Gets ServiceListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDevice funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="FailureDescription">Specifies filter for FAILURE_DESCRIPTION column</param>
        /// <param name="FailureSuggestion">Specifies filter for FAILURE_SUGGESTION column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="Photo">Specifies filter for PHOTO column</param>
        /// <param name="IdOperatorServicer">Specifies filter for ID_OPERATOR_SERVICER column</param>
        /// <param name="ServiceShortDescr">Specifies filter for SERVICE_SHORT_DESCR column</param>
        /// <param name="ServiceLongDescr">Specifies filter for SERVICE_LONG_DESCR column</param>
        /// <param name="PhotoServiced">Specifies filter for PHOTO_SERVICED column</param>
        /// <param name="AtexOk">Specifies filter for ATEX_OK column</param>
        /// <param name="PayForService">Specifies filter for PAY_FOR_SERVICE column</param>
        /// <param name="IdServiceStatus">Specifies filter for ID_SERVICE_STATUS column</param>
        /// <param name="IdDiagnosticStatus">Specifies filter for ID_DIAGNOSTIC_STATUS column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDevice list</returns>
        public virtual List<OpServiceListDevice> GetServiceListDeviceFilterCustom(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, long[] SerialNbr = null, string FailureDescription = null, string FailureSuggestion = null, string Notes = null, long[] Photo = null,
                            int[] IdOperatorServicer = null, string ServiceShortDescr = null, string ServiceLongDescr = null, long[] PhotoServiced = null, bool? AtexOk = null,
                            bool? PayForService = null, int[] IdServiceStatus = null, int[] IdDiagnosticStatus = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceFilter"));
            }

            try
            {
                List<OpServiceListDevice> returnList = OpServiceListDevice.ConvertListCustom(dbConnectionCore.GetServiceListDeviceFilterCustom(IdServiceList, SerialNbr, FailureDescription, FailureSuggestion, Notes, Photo,
                             IdOperatorServicer, ServiceShortDescr, ServiceLongDescr, PhotoServiced, AtexOk,
                             PayForService, IdServiceStatus, IdDiagnosticStatus, StartDate, EndDate,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceListDevice item in returnList)
                        ServiceListDeviceDict[new Tuple<int, long>(item.IdServiceList, item.SerialNbr)] = item;
                    ServiceListDeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListDeviceFilter", ex);
                throw;
            }

        }
        #endregion

        #region GetDeviceConnectionHourlyFilter
        public virtual List<Tuple<long, long, long>> GetDeviceConnectionHourlyFilter(long[] SerialNbr = null, long[] SerialNbrParent = null, DateTime? StartTime = null, DateTime? EndTime = null)
        {
            if (SerialNbr == null && SerialNbrParent == null)
                return new List<Tuple<long, long, long>>();
            try
            {
               // TypeDateTimeCode startTime = new TypeDateTimeCode() { Mode = TypeDateTimeCode.ModeType.Equal, DateTime1 = StartTime };
               // TypeDateTimeCode endTime = new TypeDateTimeCode() { Mode = TypeDateTimeCode.ModeType.Equal, DateTime1 = EndTime };
                return dbConnectionDw.GetDeviceConnectionHourlyFilter(SerialNbr, SerialNbrParent, StartTime, EndTime ).ToList();
               
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeviceConnectionHourlyFilter", ex);
                throw;
            }
        }

        #endregion

        #region DevicePatternConfig
        /// <summary>
        /// Gets all DevicePatternConfig objects
        /// </summary>
        public virtual List<OpDevicePatternData> GetAllDevicePatternConfig()
        {
            try
            {
                return OpDevicePatternData.ConvertList(dbConnectionCore.GetDevicePatternData(), this);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePatternData", ex);
                throw;
            }
        }

        /// <summary>
        /// Gets DevicePatternConfig by id
        /// </summary>
        /// <param name="Ids">DevicePatternConfig Ids</param>
        /// <returns>DevicePatternConfig list</returns>
        public virtual List<OpDevicePatternData> GetDevicePatternConfig(long[] Ids)
        {
            try
            {
                return OpDevicePatternData.ConvertList(dbConnectionCore.GetDevicePatternData(Ids), this);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePatternData", ex);
                throw;
            }
        }

        /// <summary>
        /// Saves the DevicePatternConfig object to the database
        /// </summary>
        /// <param name="toBeSaved">DevicePatternConfig to save</param>
        /// <returns>DevicePatternConfig Id</returns>
        public virtual void SaveDevicePatternConfig(OpDevicePatternData toBeSaved)
        {
            try
            {
                if (toBeSaved.DataType == null)
                    toBeSaved.DataType = GetDataType(toBeSaved.IdDataType);

                dbConnectionCore.SaveDevicePatternConfig(toBeSaved, toBeSaved.DataType);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_SaveDevicePatternConfig", ex);
                throw;
            }
        }

        /// <summary>
        /// Deletes the DevicePatternConfig object from the database
        /// </summary>
        /// <param name="toBeDeleted">DevicePatternConfig to delete</param>
        public virtual void DeleteDevicePatternConfig(OpDevicePatternData toBeDeleted)
        {
            try
            {
                dbConnectionCore.DeleteDevicePatternConfig(toBeDeleted);
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_DelDevicePatternConfig", ex);
                throw;
            }
        }
        #endregion

        #region Testowe
        public virtual void GetStatusForLocations(List<OpLocation> locations)
        {
            dbConnectionCore.GetStatusForLocations(locations.Cast<DB_LOCATION>().ToList());
        }

        public virtual List<OpOperator> RPGetOperatorsSOT()
        {
            return OpOperator.ConvertList(dbConnectionCore.GetOperator(), this);
        }
        #endregion


        #region GetDashboardData
        public virtual DataSet GetDashboardData(string Procedure, Enums.DashboardItemClass DashboardItemClass, long[] IdType, int IdOperator,
            Enums.Language Language = Enums.Language.Default,
            int[] IdDistributor = null, long[] IdLocation = null, long[] IdMeter = null, long[] SerialNbr = null,
            int CommandTimeout = 0)
        {
            return dbConnectionCore.GetDashboardData(Procedure: Procedure, DashboardItemClass: DashboardItemClass, IdType: IdType,
                IdOperator: IdOperator, Language: Language, IdDistributor: IdDistributor, IdLocation: IdLocation, IdMeter: IdMeter, SerialNbr: SerialNbr,
                CommandTimeout: CommandTimeout);
        }
        #endregion
        #region GetShippingListDevices
        public virtual DataTable GetShippingListDevices(int idShippingList)
        {
            return dbConnectionCore.GetShippingListDevices(idShippingList);
        }
        #endregion

        #region GetShippingListDeviceRadioDeviceTesterData
        public virtual DataTable GetShippingListDeviceRadioDeviceTesterData(int idShippingList)
        {
            return dbConnectionCore.GetShippingListDeviceRadioDeviceTesterData(idShippingList);
        }
        #endregion

        #region GetDeadlineDate

        public virtual DateTime? GetIssueDeadlineDate(int idIssue, DateTime? startDate = null)
        {
            try
            {
                DateTime? returnDate = dbConnectionCore.GetDeadlineDate(startDate, null, null, null, idIssue, null);
                return returnDate;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeadlineDate", ex);
                throw;
            }
        }

        public virtual DateTime? GetTaskDeadlineDate(int idTask, DateTime? startDate = null)
        {
            try
            {
                DateTime? returnDate = dbConnectionCore.GetDeadlineDate(startDate, null, null, null, null, idTask);
                return returnDate;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeadlineDate", ex);
                throw;
            }
        }

        public virtual DateTime? GetDeadlineDate(DateTime? startDate = null, bool? calendarDays = null, int? days = null, string countryCode = null, int? idIssue = null, int? idTask = null)
        {
            try
            {
                DateTime? returnDate = dbConnectionCore.GetDeadlineDate(startDate, calendarDays, days, countryCode, idIssue, idTask);
                return returnDate;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_u_GetDeadlineDate", ex);
                throw;
            }
        }

        #endregion

        #region class DataProviderContext
        public class DataProviderContext : IDisposable
        {
            #region Members
            private DataProvider dataProvider;
            #endregion

            #region Ctor
            public DataLoadOptions LoadOptions
            {
                get { return dataProvider.LoadOptions; }
                set { dataProvider.LoadOptions = value; }
            }

            public DataProviderContext(DataProvider dataprovider, bool loadOptionsEnabled)
            {
                this.dataProvider = dataprovider;
                this.dataProvider.LoadOptionsEnabled = loadOptionsEnabled;
                if (loadOptionsEnabled)
                {
                    this.dataProvider.LoadOptions = new DataLoadOptions();
                }
            }
            #endregion

            #region Implementation of IDisposable
            public void Dispose()
            {
                dataProvider.LoadOptions = null;
                dataProvider.LoadOptionsEnabled = false;
            }
            #endregion
        } 
        #endregion

        #region other

        #region CheckSimForDevicesForShipping
        public virtual int CheckSimForDevicesForShipping(int id)
        {
            return dbConnectionCore.CheckSimForDevicesForShippingList(id);
        }
        #endregion

        #region GetObjectIdServer
        public virtual int? GetObjectIdServer(long idObject, Common.Enums.ReferenceType referenceType, bool useDBCollector = false)
        {
            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    WCF.ObjectReferenceType type = WCF.ObjectReferenceType.None;

                    switch (referenceType)
                    {
                        case Enums.ReferenceType.SerialNbr:
                            type = WCF.ObjectReferenceType.Device;
                            break;
                        case Enums.ReferenceType.IdMeter:
                            type = WCF.ObjectReferenceType.Meter;
                            break;
                        case Enums.ReferenceType.IdLocation:
                            type = WCF.ObjectReferenceType.Location;
                            break;
                        case Enums.ReferenceType.IdDistributor:
                            type = WCF.ObjectReferenceType.Distributor;
                            break;
                    }

                    long? result = dbCollectorClient.GetObjectIdServer(ref dbCollectorSession, new long[] { idObject }, type);
                    return result.HasValue ? (int?)Convert.ToInt32(result.Value) : null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion
    }
}
