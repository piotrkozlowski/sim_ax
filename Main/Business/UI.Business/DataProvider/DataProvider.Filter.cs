using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using IMR.Suite.Data.DB;
using IMR.Suite.Common;
using IMR.Suite.Data.DB.Objects.CORE;
using IMR.Suite.UI.Business.Objects.CORE;

namespace IMR.Suite.UI.Business
{
    public partial class DataProvider
    { 
        #region GetActionFilter

        /// <summary>
        /// Gets Action list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAction funcion</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="IdActionStatus">Specifies filter for ID_ACTION_STATUS column</param>
        /// <param name="IdActionData">Specifies filter for ID_ACTION_DATA column</param>
        /// <param name="IdActionParent">Specifies filter for ID_ACTION_PARENT column</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Action list</returns>
        public virtual List<OpAction> GetActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAction = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdActionType = null, int[] IdActionStatus = null,
                            long[] IdActionData = null, long[] IdActionParent = null, long[] IdDataArch = null, int[] IdModule = null, int[] IdOperator = null,
                            TypeDateTimeCode CreationDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionFilter"));
            }

            try
            {
                List<OpAction> returnList = OpAction.ConvertList(dbConnectionCore.GetActionFilter(IdAction, SerialNbr, IdMeter, IdLocation, IdActionType, IdActionStatus,
                             IdActionData, IdActionParent, IdDataArch, IdModule, IdOperator,
                             CreationDate, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpAction item in returnList)
                        ActionDict[item.IdAction] = item;
                    ActionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActionSmsTextDataFilter

        /// <summary>
        /// Gets ActionSmsTextData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionSmsTextData funcion</param>
        /// <param name="IdActionSmsText">Specifies filter for ID_ACTION_SMS_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_SMS_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionSmsTextData list</returns>
        public virtual List<OpActionSmsTextData> GetActionSmsTextDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionSmsText = null, int[] IdLanguage = null, int[] ArgNbr = null, long[] IdDataType = null, int[] IdUnit = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionSmsTextDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionSmsTextDataFilter"));
            }

            try
            {
                List<OpActionSmsTextData> returnList = OpActionSmsTextData.ConvertList(dbConnectionCore.GetActionSmsTextDataFilter(IdActionSmsText, IdLanguage, ArgNbr, IdDataType, IdUnit, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpActionSmsTextData item in returnList)
                        ActionSmsTextDataDict[item.IdActionSmsText] = item;
                    ActionSmsTextDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionSmsTextDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActionStatusFilter

        /// <summary>
        /// Gets ActionStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionStatus funcion</param>
        /// <param name="IdActionStatus">Specifies filter for ID_ACTION_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionStatus list</returns>
        public virtual List<OpActionStatus> GetActionStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionStatusFilter"));
            }

            try
            {
                List<OpActionStatus> returnList = OpActionStatus.ConvertList(dbConnectionCore.GetActionStatusFilter(IdActionStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpActionStatus item in returnList)
                        ActionStatusDict[item.IdActionStatus] = item;
                    ActionStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActionTypeFilter
        /// <summary>
        /// Gets ActionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionType funcion</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="IdActionTypeClass">Specifies filter for ID_ACTION_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IsActionDataFixed">Specifies filter for IS_ACTION_DATA_FIXED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionType list</returns>
        public virtual List<OpActionType> GetActionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionType = null, int[] IdActionTypeClass = null, string Name = null, string PluginName = null, long[] IdDescr = null, bool? IsActionDataFixed = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeFilter"));
            }

            try
            {
                List<OpActionType> returnList = OpActionType.ConvertList(dbConnectionCore.GetActionTypeFilter(IdActionType, IdActionTypeClass, Name, PluginName, IdDescr, IsActionDataFixed,
                     topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpActionType item in returnList)
                        ActionTypeDict[item.IdActionType] = item;
                    ActionTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActionTypeClassFilter

        /// <summary>
        /// Gets ActionTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeClass funcion</param>
        /// <param name="IdActionTypeClass">Specifies filter for ID_ACTION_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionTypeClass list</returns>
        public virtual List<OpActionTypeClass> GetActionTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionTypeClassFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeClassFilter"));
            }

            try
            {
                List<OpActionTypeClass> returnList = OpActionTypeClass.ConvertList(dbConnectionCore.GetActionTypeClassFilter(IdActionTypeClass, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpActionTypeClass item in returnList)
                        ActionTypeClassDict[item.IdActionTypeClass] = item;
                    ActionTypeClassIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionTypeClassFilter", ex);
                throw;
            }

        }
        #endregion
        
        #region GetActionTypeGroupTypeFilter

        /// <summary>
        /// Gets ActionTypeGroupType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionTypeGroupType funcion</param>
        /// <param name="IdActionTypeGroupType">Specifies filter for ID_ACTION_TYPE_GROUP_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_TYPE_GROUP_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionTypeGroupType list</returns>
        public virtual List<OpActionTypeGroupType> GetActionTypeGroupTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActionTypeGroupType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionTypeGroupTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionTypeGroupTypeFilter"));
            }

            try
            {
                List<OpActionTypeGroupType> returnList = OpActionTypeGroupType.ConvertList(dbConnectionCore.GetActionTypeGroupTypeFilter(IdActionTypeGroupType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpActionTypeGroupType item in returnList)
                        ActionTypeGroupTypeDict[item.IdActionTypeGroupType] = item;
                    ActionTypeGroupTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionTypeGroupTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActivityFilter

        /// <summary>
        /// Gets Activity list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActivity funcion</param>
        /// <param name="IdActivity">Specifies filter for ID_ACTIVITY column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTIVITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Activity list</returns>
        public virtual List<OpActivity> GetActivityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActivity = null, string Name = null, long[] IdDescr = null, int[] IdReferenceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActivityFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActivityFilter"));
            }

            try
            {
                List<OpActivity> returnList = OpActivity.ConvertList(dbConnectionCore.GetActivityFilter(IdActivity, Name, IdDescr, IdReferenceType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpActivity item in returnList)
                        ActivityDict[item.IdActivity] = item;
                    ActivityIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActivityFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActorFilter

        /// <summary>
        /// Gets Actor list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActor funcion</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Surname">Specifies filter for SURNAME column</param>
        /// <param name="City">Specifies filter for CITY column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Postcode">Specifies filter for POSTCODE column</param>
        /// <param name="Email">Specifies filter for EMAIL column</param>
        /// <param name="Mobile">Specifies filter for MOBILE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Actor list</returns>
        public virtual List<OpActor> GetActorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActor = null, string Name = null, string Surname = null, string City = null, string Address = null, string Postcode = null,
                            string Email = null, string Mobile = null, string Phone = null, int[] IdLanguage = null, string Description = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActorFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActorFilter"));
            }

            try
            {
                List<OpActor> returnList = OpActor.ConvertList(dbConnectionCore.GetActorFilter(IdActor, Name, Surname, City, Address, Postcode,
                             Email, Mobile, Phone, IdLanguage, Description,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpActor item in returnList)
                        ActorDict[item.IdActor] = item;
                    ActorIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActorFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActorGroupFilter

        /// <summary>
        /// Gets ActorGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActorGroup funcion</param>
        /// <param name="IdActorGroup">Specifies filter for ID_ACTOR_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="BuiltIn">Specifies filter for BUILT_IN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTOR_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActorGroup list</returns>
        public virtual List<OpActorGroup> GetActorGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActorGroup = null, string Name = null, bool? BuiltIn = null, int[] IdDistributor = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActorGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActorGroupFilter"));
            }

            try
            {
                List<OpActorGroup> returnList = OpActorGroup.ConvertList(dbConnectionCore.GetActorGroupFilter(IdActorGroup, Name, BuiltIn, IdDistributor, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpActorGroup item in returnList)
                        ActorGroupDict[item.IdActorGroup] = item;
                    ActorGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActorGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmFilter

        /// <summary>
        /// Gets Alarm list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarm funcion</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Alarm list</returns>
        public virtual List<OpAlarm> GetAlarmFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarm = null, long[] IdAlarmEvent = null, int[] IdOperator = null, int[] IdAlarmGroup = null, int[] IdAlarmStatus = null, int[] IdTransmissionType = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmFilter"));
            }

            try
            {
                List<OpAlarm> returnList = OpAlarm.ConvertList(dbConnectionCore.GetAlarmFilter(IdAlarm, IdAlarmEvent, IdOperator, IdAlarmGroup, IdAlarmStatus, IdTransmissionType,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarm item in returnList)
                        AlarmDict[item.IdAlarm] = item;
                    AlarmIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmDataFilter

        /// <summary>
        /// Gets AlarmData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmData funcion</param>
        /// <param name="IdAlarmData">Specifies filter for ID_ALARM_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmData list</returns>
        public virtual List<OpAlarmData> GetAlarmDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmDataFilter"));
            }

            try
            {
                List<OpAlarmData> returnList = OpAlarmData.ConvertList(dbConnectionCore.GetAlarmDataFilter(IdAlarmData, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmData item in returnList)
                        AlarmDataDict[item.IdAlarmData] = item;
                    AlarmDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmDefFilter

        /// <summary>
        /// Gets AlarmDef list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmDef funcion</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="IdDataTypeAlarm">Specifies filter for ID_DATA_TYPE_ALARM column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="IdDataTypeConfig">Specifies filter for ID_DATA_TYPE_CONFIG column</param>
        /// <param name="IsEnabled">Specifies filter for IS_ENABLED column</param>
        /// <param name="IdAlarmText">Specifies filter for ID_ALARM_TEXT column</param>
        /// <param name="IdAlarmData">Specifies filter for ID_ALARM_DATA column</param>
        /// <param name="CheckLastAlarm">Specifies filter for CHECK_LAST_ALARM column</param>
        /// <param name="SuspensionTime">Specifies filter for SUSPENSION_TIME column</param>
        /// <param name="IsConfirmable">Specifies filter for IS_CONFIRMABLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmDef list</returns>
        public virtual List<OpAlarmDef> GetAlarmDefFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmDef = null, string Name = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdAlarmType = null,
                            long[] IdDataTypeAlarm = null, int[] IndexNbr = null, long[] IdDataTypeConfig = null, bool? IsEnabled = null, long[] IdAlarmText = null, long[] IdAlarmData = null,
                            bool? CheckLastAlarm = null, int[] SuspensionTime = null, bool? IsConfirmable = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmDefFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmDefFilter"));
            }

            try
            {
                List<OpAlarmDef> returnList = OpAlarmDef.ConvertList(dbConnectionCore.GetAlarmDefFilter(IdAlarmDef, Name, SerialNbr, IdMeter, IdLocation, IdAlarmType,
                             IdDataTypeAlarm, IndexNbr, IdDataTypeConfig, IsEnabled, IdAlarmText, IdAlarmData,
                             CheckLastAlarm, SuspensionTime, IsConfirmable, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmDef item in returnList)
                        AlarmDefDict[item.IdAlarmDef] = item;
                    AlarmDefIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmDefFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmDefGroupFilter

        /// <summary>
        /// Gets AlarmDefGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmDefGroup funcion</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="ConfirmLevel">Specifies filter for CONFIRM_LEVEL column</param>
        /// <param name="ConfirmTimeout">Specifies filter for CONFIRM_TIMEOUT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmDefGroup list</returns>
        public virtual List<OpAlarmDefGroup> GetAlarmDefGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmDef = null, int[] IdAlarmGroup = null, int[] IdTransmissionType = null, int[] ConfirmLevel = null, int[] ConfirmTimeout = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmDefGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmDefGroupFilter"));
            }

            try
            {
                List<OpAlarmDefGroup> returnList = OpAlarmDefGroup.ConvertList(dbConnectionCore.GetAlarmDefGroupFilter(IdAlarmDef, IdAlarmGroup, IdTransmissionType, ConfirmLevel, ConfirmTimeout, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmDefGroup item in returnList)
                    {
                        //AlarmDefGroupDict[item.IdAlarmDef] = item;
                        if (!AlarmDefGroupDict.ContainsKey(item.IdAlarmDef))
                            AlarmDefGroupDict.Add(item.IdAlarmDef, new List<OpAlarmDefGroup>());
                        AlarmDefGroupDict[item.IdAlarmDef].Add(item);
                    }
                   
                    AlarmDefGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmDefGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmEventFilter

        /// <summary>
        /// Gets AlarmEvent list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmEvent funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdAlarmDef">Specifies filter for ID_ALARM_DEF column</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmEvent list</returns>
        public virtual List<OpAlarmEvent> GetAlarmEventFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, long[] IdAlarmDef = null, long[] IdDataArch = null, bool? IsActive = null, long[] SerialNbr = null, long[] IdMeter = null,
                            long[] IdLocation = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmEventFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmEventFilter"));
            }

            try
            {
                List<OpAlarmEvent> returnList = OpAlarmEvent.ConvertList(dbConnectionCore.GetAlarmEventFilter(IdAlarmEvent, IdAlarmDef, IdDataArch, IsActive, SerialNbr, IdMeter,
                             IdLocation, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmEvent item in returnList)
                        AlarmEventDict[item.IdAlarmEvent] = item;
                    AlarmEventIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmEventFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmGroupFilter

        /// <summary>
        /// Gets AlarmGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmGroup funcion</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmGroup list</returns>
        public virtual List<OpAlarmGroup> GetAlarmGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmGroup = null, string Name = null, string Description = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmGroupFilter"));
            }

            try
            {
                List<OpAlarmGroup> returnList = OpAlarmGroup.ConvertList(dbConnectionCore.GetAlarmGroupFilter(IdAlarmGroup, Name, Description, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmGroup item in returnList)
                        AlarmGroupDict[item.IdAlarmGroup] = item;
                    AlarmGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmGroupOperatorFilter

        /// <summary>
        /// Gets AlarmGroupOperator list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmGroupOperator funcion</param>
        /// <param name="IdAlarmGroup">Specifies filter for ID_ALARM_GROUP column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmGroupOperator list</returns>
        public virtual List<OpAlarmGroupOperator> GetAlarmGroupOperatorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmGroup = null, int[] IdOperator = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmGroupOperatorFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmGroupOperatorFilter"));
            }

            try
            {
                List<OpAlarmGroupOperator> returnList = OpAlarmGroupOperator.ConvertList(dbConnectionCore.GetAlarmGroupOperatorFilter(IdAlarmGroup, IdOperator, IdDataType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmGroupOperator item in returnList)
                        AlarmGroupOperatorDict[item.IdAlarmGroup] = item;
                    AlarmGroupOperatorIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmGroupOperatorFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmHistoryFilter

        /// <summary>
        /// Gets AlarmHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmHistory funcion</param>
        /// <param name="IdAlarmHistory">Specifies filter for ID_ALARM_HISTORY column</param>
        /// <param name="IdAlarm">Specifies filter for ID_ALARM column</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="StartRuleHost">Specifies filter for START_RULE_HOST column</param>
        /// <param name="StartRuleUser">Specifies filter for START_RULE_USER column</param>
        /// <param name="EndRuleHost">Specifies filter for END_RULE_HOST column</param>
        /// <param name="EndRuleUser">Specifies filter for END_RULE_USER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmHistory list</returns>
        public virtual List<OpAlarmHistory> GetAlarmHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmHistory = null, long[] IdAlarm = null, int[] IdAlarmStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string StartRuleHost = null,
                            string StartRuleUser = null, string EndRuleHost = null, string EndRuleUser = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmHistoryFilter"));
            }

            try
            {
                List<OpAlarmHistory> returnList = OpAlarmHistory.ConvertList(dbConnectionCore.GetAlarmHistoryFilter(IdAlarmHistory, IdAlarm, IdAlarmStatus, StartTime, EndTime, StartRuleHost,
                             StartRuleUser, EndRuleHost, EndRuleUser, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmHistory item in returnList)
                        AlarmHistoryDict[item.IdAlarmHistory] = item;
                    AlarmHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmMessageFilter

        /// <summary>
        /// Gets AlarmMessage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmMessage funcion</param>
        /// <param name="IdAlarmEvent">Specifies filter for ID_ALARM_EVENT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="AlarmMessage">Specifies filter for ALARM_MESSAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmMessage list</returns>
        public virtual List<OpAlarmMessage> GetAlarmMessageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmEvent = null, int[] IdLanguage = null, string AlarmMessage = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmMessageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmMessageFilter"));
            }

            try
            {
                List<OpAlarmMessage> returnList = OpAlarmMessage.ConvertList(dbConnectionCore.GetAlarmMessageFilter(IdAlarmEvent, IdLanguage, AlarmMessage, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmMessage item in returnList)
                        AlarmMessageDict[item.IdAlarmEvent] = item;
                    AlarmMessageIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmMessageFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmStatusFilter

        /// <summary>
        /// Gets AlarmStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmStatus funcion</param>
        /// <param name="IdAlarmStatus">Specifies filter for ID_ALARM_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmStatus list</returns>
        public virtual List<OpAlarmStatus> GetAlarmStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmStatusFilter"));
            }

            try
            {
                List<OpAlarmStatus> returnList = OpAlarmStatus.ConvertList(dbConnectionCore.GetAlarmStatusFilter(IdAlarmStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmStatus item in returnList)
                        AlarmStatusDict[item.IdAlarmStatus] = item;
                    AlarmStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmTextFilter

        /// <summary>
        /// Gets AlarmText list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmText funcion</param>
        /// <param name="IdAlarmText">Specifies filter for ID_ALARM_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="AlarmText">Specifies filter for ALARM_TEXT column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmText list</returns>
        public virtual List<OpAlarmText> GetAlarmTextFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmText = null, int[] IdLanguage = null, string Name = null, string AlarmText = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmTextFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmTextFilter"));
            }

            try
            {
                List<OpAlarmText> returnList = OpAlarmText.ConvertList(dbConnectionCore.GetAlarmTextFilter(IdAlarmText, IdLanguage, Name, AlarmText, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmText item in returnList)
                        AlarmTextDict[item.IdAlarmText] = item;
                    AlarmTextIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmTextFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmTextDataTypeFilter

        /// <summary>
        /// Gets AlarmTextDataType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmTextDataType funcion</param>
        /// <param name="IdAlarmText">Specifies filter for ID_ALARM_TEXT column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_TEXT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmTextDataType list</returns>
        public virtual List<OpAlarmTextDataType> GetAlarmTextDataTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAlarmText = null, int[] IdLanguage = null, int[] ArgNbr = null, long[] IdDataType = null, int[] IdUnit = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmTextDataTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmTextDataTypeFilter"));
            }

            try
            {
                List<OpAlarmTextDataType> returnList = OpAlarmTextDataType.ConvertList(dbConnectionCore.GetAlarmTextDataTypeFilter(IdAlarmText, IdLanguage, ArgNbr, IdDataType, IdUnit, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmTextDataType item in returnList)
                        AlarmTextDataTypeDict[item.IdAlarmText] = item;
                    AlarmTextDataTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmTextDataTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAlarmTypeFilter

        /// <summary>
        /// Gets AlarmType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAlarmType funcion</param>
        /// <param name="IdAlarmType">Specifies filter for ID_ALARM_TYPE column</param>
        /// <param name="Symbol">Specifies filter for SYMBOL column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ALARM_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AlarmType list</returns>
        public virtual List<OpAlarmType> GetAlarmTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAlarmType = null, string Symbol = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAlarmTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAlarmTypeFilter"));
            }

            try
            {
                List<OpAlarmType> returnList = OpAlarmType.ConvertList(dbConnectionCore.GetAlarmTypeFilter(IdAlarmType, Symbol, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAlarmType item in returnList)
                        AlarmTypeDict[item.IdAlarmType] = item;
                    AlarmTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAlarmTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetArticleFilter

        /// <summary>
        /// Gets Article list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllArticle funcion</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="ExternalId">Specifies filter for EXTERNAL_ID column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IsUnique">Specifies filter for IS_UNIQUE column</param>
        /// <param name="Visible">Specifies filter for VISIBLE column</param>
        /// <param name="IsAiut">Specifies filter for IS_AIUT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ARTICLE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Article list</returns>
        public virtual List<OpArticle> GetArticleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdArticle = null, long[] ExternalId = null, string Name = null, string Description = null, bool? IsUnique = null, bool? Visible = null,
                            bool? IsAiut = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetArticleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetArticleFilter"));
            }

            try
            {
                List<OpArticle> returnList = OpArticle.ConvertList(dbConnectionCore.GetArticleFilter(IdArticle, ExternalId, Name, Description, IsUnique, Visible,
                             IsAiut, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpArticle item in returnList)
                        ArticleDict[item.IdArticle] = item;
                    ArticleIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetArticleFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetArticleDataFilter

        /// <summary>
        /// Gets ArticleData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllArticleData funcion</param>
        /// <param name="IdArticleData">Specifies filter for ID_ARTICLE_DATA column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ARTICLE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ArticleData list</returns>
        public virtual List<OpArticleData> GetArticleDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdArticleData = null, long[] IdArticle = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetArticleDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetArticleDataFilter"));
            }

            try
            {
                List<OpArticleData> returnList = OpArticleData.ConvertList(dbConnectionCore.GetArticleDataFilter(IdArticleData, IdArticle, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpArticleData item in returnList)
                        ArticleDataDict[item.IdArticleData] = item;
                    ArticleDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetArticleDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAtgTypeFilter

        /// <summary>
        /// Gets AtgType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAtgType funcion</param>
        /// <param name="IdAtgType">Specifies filter for ID_ATG_TYPE column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ATG_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>AtgType list</returns>
        public virtual List<OpAtgType> GetAtgTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdAtgType = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAtgTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAtgTypeFilter"));
            }

            try
            {
                List<OpAtgType> returnList = OpAtgType.ConvertList(dbConnectionCore.GetAtgTypeFilter(IdAtgType, Descr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAtgType item in returnList)
                        AtgTypeDict[item.IdAtgType] = item;
                    AtgTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAtgTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetAuditFilter

        /// <summary>
        /// Gets Audit list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllAudit funcion</param>
        /// <param name="IdAudit">Specifies filter for ID_AUDIT column</param>
        /// <param name="BatchId">Specifies filter for BATCH_ID column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="Key1">Specifies filter for KEY1 column</param>
        /// <param name="Key2">Specifies filter for KEY2 column</param>
        /// <param name="Key3">Specifies filter for KEY3 column</param>
        /// <param name="Key4">Specifies filter for KEY4 column</param>
        /// <param name="Key5">Specifies filter for KEY5 column</param>
        /// <param name="Key6">Specifies filter for KEY6 column</param>
        /// <param name="ColumnName">Specifies filter for COLUMN_NAME column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="Host">Specifies filter for HOST column</param>
        /// <param name="Application">Specifies filter for APPLICATION column</param>
        /// <param name="Contextinfo">Specifies filter for CONTEXTINFO column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_AUDIT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Audit list</returns>
        public virtual List<OpAudit> GetAuditFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdAudit = null, long[] BatchId = null, int[] ChangeType = null, string TableName = null, long[] Key1 = null, long[] Key2 = null,
                            long[] Key3 = null, long[] Key4 = null, long[] Key5 = null, long[] Key6 = null, string ColumnName = null,
                            TypeDateTimeCode Time = null, string User = null, string Host = null, string Application = null, string Contextinfo = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetAuditFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetAuditFilter"));
            }

            try
            {
                List<OpAudit> returnList = OpAudit.ConvertList(dbConnectionCore.GetAuditFilter(IdAudit, BatchId, ChangeType, TableName, Key1, Key2,
                             Key3, Key4, Key5, Key6, ColumnName,
                             Time, User, Host, Application, Contextinfo, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpAudit item in returnList)
                        AuditDict[item.IdAudit] = item;
                    AuditIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetAuditFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetCodeFilter

        /// <summary>
        /// Gets Code list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCode funcion</param>
        /// <param name="IdCode">Specifies filter for ID_CODE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdCodeType">Specifies filter for ID_CODE_TYPE column</param>
        /// <param name="CodeNbr">Specifies filter for CODE_NBR column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="ActivationTime">Specifies filter for ACTIVATION_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Code list</returns>
        public virtual List<OpCode> GetCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdCode = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdLocation = null, int[] IdCodeType = null, int[] CodeNbr = null,
                            string Code = null, bool? IsActive = null, TypeDateTimeCode ActivationTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetCodeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCodeFilter"));
            }

            try
            {
                List<OpCode> returnList = OpCode.ConvertList(dbConnectionCore.GetCodeFilter(IdCode, SerialNbr, IdMeter, IdLocation, IdCodeType, CodeNbr,
                             Code, IsActive, ActivationTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpCode item in returnList)
                        CodeDict[item.IdCode] = item;
                    CodeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetCodeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetCodeTypeFilter

        /// <summary>
        /// Gets CodeType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCodeType funcion</param>
        /// <param name="IdCodeType">Specifies filter for ID_CODE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdDataTypeFormat">Specifies filter for ID_DATA_TYPE_FORMAT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CODE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>CodeType list</returns>
        public virtual List<OpCodeType> GetCodeTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdCodeType = null, string Name = null, long[] IdDescr = null, int[] IdDataTypeFormat = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetCodeTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCodeTypeFilter"));
            }

            try
            {
                List<OpCodeType> returnList = OpCodeType.ConvertList(dbConnectionCore.GetCodeTypeFilter(IdCodeType, Name, IdDescr, IdDataTypeFormat, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpCodeType item in returnList)
                        CodeTypeDict[item.IdCodeType] = item;
                    CodeTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetCodeTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetCommandCodeParamMapFilter

        /// <summary>
        /// Gets CommandCodeParamMap list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCommandCodeParamMap funcion</param>
        /// <param name="IdCommandCodeParamMap">Specifies filter for ID_COMMAND_CODE_PARAM_MAP column</param>
        /// <param name="IdCommandCode">Specifies filter for ID_COMMAND_CODE column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="ByteOffset">Specifies filter for BYTE_OFFSET column</param>
        /// <param name="BitOffset">Specifies filter for BIT_OFFSET column</param>
        /// <param name="Length">Specifies filter for LENGTH column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdProtocol">Specifies filter for ID_PROTOCOL column</param>
        /// <param name="IsRequired">Specifies filter for IS_REQUIRED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_COMMAND_CODE_PARAM_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>CommandCodeParamMap list</returns>
        public virtual List<OpCommandCodeParamMap> GetCommandCodeParamMapFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdCommandCodeParamMap = null, long[] IdCommandCode = null, long[] ArgNbr = null, string Name = null, int[] ByteOffset = null, int[] BitOffset = null,
                            int[] Length = null, int[] IdDataTypeClass = null, int[] IdUnit = null, long[] IdDescr = null, int[] IdProtocol = null, bool? IsRequired = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetCommandCodeParamMapFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCommandCodeParamMapFilter"));
            }

            try
            {
                List<OpCommandCodeParamMap> returnList = OpCommandCodeParamMap.ConvertList(dbConnectionCore.GetCommandCodeParamMapFilter(IdCommandCodeParamMap, IdCommandCode, ArgNbr, Name, ByteOffset, BitOffset,
                             Length, IdDataTypeClass, IdUnit, IdDescr, IdProtocol, IsRequired, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpCommandCodeParamMap item in returnList)
                        CommandCodeParamMapDict[item.IdCommandCodeParamMap] = item;
                    CommandCodeParamMapIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetCommandCodeParamMapFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetCommandCodeTypeFilter

        /// <summary>
        /// Gets CommandCodeType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCommandCodeType funcion</param>
        /// <param name="IdCommandCode">Specifies filter for ID_COMMAND_CODE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_COMMAND_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>CommandCodeType list</returns>
        public virtual List<OpCommandCodeType> GetCommandCodeTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdCommandCode = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetCommandCodeTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCommandCodeTypeFilter"));
            }

            try
            {
                List<OpCommandCodeType> returnList = OpCommandCodeType.ConvertList(dbConnectionCore.GetCommandCodeTypeFilter(IdCommandCode, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpCommandCodeType item in returnList)
                        CommandCodeTypeDict[item.IdCommandCode] = item;
                    CommandCodeTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetCommandCodeTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetComponentFilter

        /// <summary>
        /// Gets Component list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllComponent funcion</param>
        /// <param name="IdComponent">Specifies filter for ID_COMPONENT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_COMPONENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Component list</returns>
        public virtual List<OpComponent> GetComponentFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdComponent = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetComponentFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetComponentFilter"));
            }

            try
            {
                List<OpComponent> returnList = OpComponent.ConvertList(dbConnectionCore.GetComponentFilter(IdComponent, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpComponent item in returnList)
                        ComponentDict[item.IdComponent] = item;
                    ComponentIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetComponentFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConfigurationProfileFilter

        /// <summary>
        /// Gets ConfigurationProfile list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfile funcion</param>
        /// <param name="IdConfigurationProfile">Specifies filter for ID_CONFIGURATION_PROFILE column</param>
        /// <param name="IdConfigurationProfileType">Specifies filter for ID_CONFIGURATION_PROFILE_TYPE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdOperatorCreated">Specifies filter for ID_OPERATOR_CREATED column</param>
        /// <param name="CreatedTime">Specifies filter for CREATED_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfile list</returns>
        public virtual List<OpConfigurationProfile> GetConfigurationProfileFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConfigurationProfile = null, int[] IdConfigurationProfileType = null, int[] IdDistributor = null, string Name = null, long[] IdDescr = null, int[] IdOperatorCreated = null,
                    TypeDateTimeCode CreatedTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConfigurationProfileFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileFilter"));
            }

            try
            {
                List<OpConfigurationProfile> returnList = OpConfigurationProfile.ConvertList(dbConnectionCore.GetConfigurationProfileFilter(IdConfigurationProfile, IdConfigurationProfileType, IdDistributor, Name, IdDescr, IdOperatorCreated,
                                CreatedTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConfigurationProfile item in returnList)
                        ConfigurationProfileDict[item.IdConfigurationProfile] = item;
                    ConfigurationProfileIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConfigurationProfileFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConfigurationProfileDataFilter

        /// <summary>
        /// Gets ConfigurationProfileData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileData funcion</param>
        /// <param name="IdConfigurationProfileData">Specifies filter for ID_CONFIGURATION_PROFILE_DATA column</param>
        /// <param name="IdConfigurationProfile">Specifies filter for ID_CONFIGURATION_PROFILE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileData list</returns>
        public virtual List<OpConfigurationProfileData> GetConfigurationProfileDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConfigurationProfileData = null, long[] IdConfigurationProfile = null, int[] IdDeviceType = null, int[] IdDeviceTypeProfileStep = null, long[] IdDataType = null, int[] IndexNbr = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConfigurationProfileDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileDataFilter"));
            }

            try
            {
                List<OpConfigurationProfileData> returnList = OpConfigurationProfileData.ConvertList(dbConnectionCore.GetConfigurationProfileDataFilter(IdConfigurationProfileData, IdConfigurationProfile, IdDeviceType, IdDeviceTypeProfileStep, IdDataType, IndexNbr,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConfigurationProfileData item in returnList)
                        ConfigurationProfileDataDict[item.IdConfigurationProfileData] = item;
                    ConfigurationProfileDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConfigurationProfileDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConfigurationProfileDeviceTypeFilter

        /// <summary>
        /// Gets ConfigurationProfileDeviceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileDeviceType funcion</param>
        /// <param name="IdConfigurationProfile">Specifies filter for ID_CONFIGURATION_PROFILE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileDeviceType list</returns>
        public virtual List<OpConfigurationProfileDeviceType> GetConfigurationProfileDeviceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConfigurationProfile = null, int[] IdDeviceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConfigurationProfileDeviceTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileDeviceTypeFilter"));
            }

            try
            {
                List<OpConfigurationProfileDeviceType> returnList = OpConfigurationProfileDeviceType.ConvertList(dbConnectionCore.GetConfigurationProfileDeviceTypeFilter(IdConfigurationProfile, IdDeviceType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConfigurationProfileDeviceType item in returnList)
                        ConfigurationProfileDeviceTypeDict[item.IdConfigurationProfile] = item;
                    ConfigurationProfileDeviceTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConfigurationProfileDeviceTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConfigurationProfileTypeFilter

        /// <summary>
        /// Gets ConfigurationProfileType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileType funcion</param>
        /// <param name="IdConfigurationProfileType">Specifies filter for ID_CONFIGURATION_PROFILE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileType list</returns>
        public virtual List<OpConfigurationProfileType> GetConfigurationProfileTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConfigurationProfileType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConfigurationProfileTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileTypeFilter"));
            }

            try
            {
                List<OpConfigurationProfileType> returnList = OpConfigurationProfileType.ConvertList(dbConnectionCore.GetConfigurationProfileTypeFilter(IdConfigurationProfileType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConfigurationProfileType item in returnList)
                        ConfigurationProfileTypeDict[item.IdConfigurationProfileType] = item;
                    ConfigurationProfileTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConfigurationProfileTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConfigurationProfileTypeStepFilter

        /// <summary>
        /// Gets ConfigurationProfileTypeStep list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConfigurationProfileTypeStep funcion</param>
        /// <param name="IdConfigurationProfileType">Specifies filter for ID_CONFIGURATION_PROFILE_TYPE column</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONFIGURATION_PROFILE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConfigurationProfileTypeStep list</returns>
        public virtual List<OpConfigurationProfileTypeStep> GetConfigurationProfileTypeStepFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConfigurationProfileType = null, int[] IdDeviceTypeProfileStep = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConfigurationProfileTypeStepFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConfigurationProfileTypeStepFilter"));
            }

            try
            {
                List<OpConfigurationProfileTypeStep> returnList = OpConfigurationProfileTypeStep.ConvertList(dbConnectionCore.GetConfigurationProfileTypeStepFilter(IdConfigurationProfileType, IdDeviceTypeProfileStep, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConfigurationProfileTypeStep item in returnList)
                        ConfigurationProfileTypeStepDict[item.IdConfigurationProfileType] = item;
                    ConfigurationProfileTypeStepIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConfigurationProfileTypeStepFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerFilter

        /// <summary>
        /// Gets Consumer list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumer funcion</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdConsumerType">Specifies filter for ID_CONSUMER_TYPE column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IsBlocked">Specifies filter for IS_BLOCKED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Consumer list</returns>
        public virtual List<OpConsumer> GetConsumerFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumer = null, int[] IdConsumerType = null, int[] IdActor = null, int[] IdDistributor = null, string Description = null, bool IsBlocked = false, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerFilter"));
            }

            try
            {
                List<OpConsumer> returnList = OpConsumer.ConvertList(dbConnectionCore.GetConsumerFilter(IdConsumer, IdConsumerType, IdActor, IdDistributor, Description, IsBlocked, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpConsumer item in returnList)
                        ConsumerDict[item.IdConsumer] = item;
                    ConsumerIsFullCache = true;
                }

                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerDataFilter

        /// <summary>
        /// Gets ConsumerData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerData funcion</param>
        /// <param name="IdConsumerData">Specifies filter for ID_CONSUMER_DATA column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerData list</returns>
        public virtual List<OpConsumerData> GetConsumerDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerData = null, int[] IdConsumer = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerDataFilter"));
            }

            try
            {
                List<OpConsumerData> returnList = OpConsumerData.ConvertList(dbConnectionCore.GetConsumerDataFilter(IdConsumerData, IdConsumer, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerData item in returnList)
                        ConsumerDataDict[item.IdConsumerData] = item;
                    ConsumerDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerNotificationFilter

        /// <summary>
        /// Gets ConsumerNotification list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerNotification funcion</param>
        /// <param name="IdConsumerNotification">Specifies filter for ID_CONSUMER_NOTIFICATION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdNotificationDeliveryType">Specifies filter for ID_NOTIFICATION_DELIVERY_TYPE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_NOTIFICATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerNotification list</returns>
        public virtual List<OpConsumerNotification> GetConsumerNotificationFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerNotification = null, int[] IdConsumer = null, int[] IdNotificationDeliveryType = null, int[] IdOperator = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadCustomData = true)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerNotificationFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerNotificationFilter"));
            }

            try
            {
                List<OpConsumerNotification> returnList = OpConsumerNotification.ConvertList(dbConnectionCore.GetConsumerNotificationFilter(IdConsumerNotification, IdConsumer, IdNotificationDeliveryType, IdOperator, StartTime, EndTime,
                    topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                
                if (mergeIntoCache)
                {
                    foreach (OpConsumerNotification item in returnList)
                        ConsumerNotificationDict[item.IdConsumerNotification] = item;
                    ConsumerNotificationIsFullCache = true;
                }

                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerNotificationFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerNotificationHistoryFilter
        /// <summary>
        /// Gets ConsumerNotificationHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerNotificationHistory funcion</param>
        /// <param name="IdConsumerNotificationHistory">Specifies filter for ID_CONSUMER_NOTIFICATION_HISTORY column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdNotificationDeliveryType">Specifies filter for ID_NOTIFICATION_DELIVERY_TYPE column</param>
        /// <param name="IdNotificationType">Specifies filter for ID_NOTIFICATION_TYPE column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="Body">Specifies filter for BODY column</param>
        /// <param name="DeliveryAddress">Specifies filter for DELIVERY_ADDRESS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_NOTIFICATION_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerNotificationHistory list</returns>
        public virtual List<OpConsumerNotificationHistory> GetConsumerNotificationHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerNotificationHistory  = null,int[] IdConsumer  = null,int[] IdNotificationDeliveryType  = null,int[] IdNotificationType  = null,TypeDateTimeCode Time = null,string Body = null,
					        string DeliveryAddress = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerNotificationHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerNotificationHistoryFilter"));
            }
	
	        try
	        {
		        List<OpConsumerNotificationHistory> returnList = OpConsumerNotificationHistory.ConvertList(dbConnectionCore.GetConsumerNotificationHistoryFilter( IdConsumerNotificationHistory , IdConsumer , IdNotificationDeliveryType , IdNotificationType , Time , Body ,
					         DeliveryAddress , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpConsumerNotificationHistory item in returnList)
				        ConsumerNotificationHistoryDict[item.IdConsumerNotificationHistory] = item;
                    ConsumerNotificationHistoryIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerNotificationHistoryFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetConsumerNotificationTypeDataFilter

        /// <summary>
        /// Gets ConsumerNotificationTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerNotificationTypeData funcion</param>
        /// <param name="IdConsumerNotificationTypeData">Specifies filter for ID_CONSUMER_NOTIFICATION_TYPE_DATA column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdNotificationType">Specifies filter for ID_NOTIFICATION_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_NOTIFICATION_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerNotificationTypeData list</returns>
        public List<OpConsumerNotificationTypeData> GetConsumerNotificationTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerNotificationTypeData = null, int[] IdConsumer = null, int[] IdNotificationType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerNotificationTypeDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerNotificationTypeDataFilter"));
            }

            try
            {
                List<OpConsumerNotificationTypeData> returnList = OpConsumerNotificationTypeData.ConvertList(dbConnectionCore.GetConsumerNotificationTypeDataFilter(IdConsumerNotificationTypeData, IdConsumer, IdNotificationType, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerNotificationTypeData item in returnList)
                        ConsumerNotificationTypeDataDict[item.IdConsumerNotificationTypeData] = item;
                    ConsumerNotificationTypeDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerNotificationTypeDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerPaymentModuleFilter

        /// <summary>
        /// Gets ConsumerPaymentModule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerPaymentModule funcion</param>
        /// <param name="IdConsumerPaymentModule">Specifies filter for ID_CONSUMER_PAYMENT_MODULE column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="PaymentSystemNumber">Specifies filter for PAYMENT_SYSTEM_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_PAYMENT_MODULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerPaymentModule list</returns>
        public virtual List<OpConsumerPaymentModule> GetConsumerPaymentModuleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerPaymentModule  = null,int[] IdConsumer  = null,int[] IdPaymentModule  = null,string PaymentSystemNumber = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerPaymentModuleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerPaymentModuleFilter"));
            }
	
	        try
	        {
		        List<OpConsumerPaymentModule> returnList = OpConsumerPaymentModule.ConvertList(dbConnectionCore.GetConsumerPaymentModuleFilter( IdConsumerPaymentModule , IdConsumer , IdPaymentModule , PaymentSystemNumber , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpConsumerPaymentModule item in returnList)
				        ConsumerPaymentModuleDict[item.IdConsumerPaymentModule] = item;
                    ConsumerPaymentModuleIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerPaymentModuleFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetConsumerTransactionFilter

        /// <summary>
        /// Gets last ConsumerTransaction object from the database, function makes request to DB
        /// </summary>
        /// <param name="idConsumer">Consumer to get data from</param>
        /// <returns>Consumer Transaction Object</returns>
        public virtual OpConsumerTransaction GetLastConsumerTransactionCORE(int idConsumer, int? idTransactionType)
        {
            return GetConsumerTransactionFilterCORE(
                        IdConsumer: new[] { idConsumer },
                        IdConsumerTransactionType: idTransactionType.HasValue ? new[] { idTransactionType.Value } : null,
                        topCount: 1).FirstOrDefault();
        }

        /// <summary>
        /// Gets ConsumerTransaction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransaction funcion</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdConsumerTransactionType">Specifies filter for ID_CONSUMER_TRANSACTION_TYPE column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="TransactionGuid">Specifies filter for TRANSACTION_GUID column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="IdAction">Specifies filter for ID_ACTION column</param>
        /// <param name="IdPrevious consumerTransaction">Specifies filter for ID_PREVIOUS_ CONSUMER_TRANSACTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransaction list</returns>
        public virtual List<OpConsumerTransaction> GetConsumerTransactionFilterCORE(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTransaction  = null,int[] IdConsumer  = null,int[] IdOperator  = null,int[] IdDistributor  = null,int[] IdConsumerTransactionType  = null,int[] IdTariff  = null,
					        TypeDateTimeCode Time = null,long[] IdConsumerSettlement  = null,int[] IdPaymentModule  = null,string TransactionGuid = null,long[] IdPacket  = null,
					        long[] IdAction  = null,long[] IdPreviousConsumerTransaction  = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerTransactionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionFilter"));
            }
	
	        try
	        {
		        List<OpConsumerTransaction> returnList = OpConsumerTransaction.ConvertList(dbConnectionCore.GetConsumerTransactionFilter( IdConsumerTransaction , IdConsumer , IdOperator , IdDistributor , IdConsumerTransactionType , IdTariff ,
					         Time , IdConsumerSettlement , IdPaymentModule , TransactionGuid , IdPacket ,
					         IdAction , IdPreviousConsumerTransaction , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpConsumerTransaction item in returnList)
				        ConsumerTransactionDictCORE[item.IdConsumerTransaction] = item;
                    ConsumerTransactionIsFullCacheCORE = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetConsumerTransactionDataFilter

        /// <summary>
        /// Gets ConsumerTransactionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransactionData funcion</param>
        /// <param name="IdConsumerTransactionData">Specifies filter for ID_CONSUMER_TRANSACTION_DATA column</param>
        /// <param name="IdConsumerTransaction">Specifies filter for ID_CONSUMER_TRANSACTION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransactionData list</returns>
        public virtual List<OpConsumerTransactionData> GetConsumerTransactionDataFilterCORE(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTransactionData  = null,long[] IdConsumerTransaction  = null,long[] IdDataType  = null,int[] IndexNbr  = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerTransactionDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionDataFilter"));
            }
	
	        try
	        {
		        List<OpConsumerTransactionData> returnList = OpConsumerTransactionData.ConvertList(dbConnectionCore.GetConsumerTransactionDataFilter( IdConsumerTransactionData , IdConsumerTransaction , IdDataType , IndexNbr , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpConsumerTransactionData item in returnList)
                        ConsumerTransactionDataDictCORE[item.IdConsumerTransactionData] = item;
                    ConsumerTransactionDataIsFullCacheCORE = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionDataFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetConsumerTransactionTypeFilter

        /// <summary>
        /// Gets ConsumerTransactionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTransactionType funcion</param>
        /// <param name="IdConsumerTransactionType">Specifies filter for ID_CONSUMER_TRANSACTION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TRANSACTION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTransactionType list</returns>
        public virtual List<OpConsumerTransactionType> GetConsumerTransactionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerTransactionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerTransactionTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTransactionTypeFilter"));
            }

            try
            {
                List<OpConsumerTransactionType> returnList = OpConsumerTransactionType.ConvertList(dbConnectionCore.GetConsumerTransactionTypeFilter(IdConsumerTransactionType, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerTransactionType item in returnList)
                        ConsumerTransactionTypeDict[item.IdConsumerTransactionType] = item;
                    ConsumerTransactionTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTransactionTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerTypeFilter

        /// <summary>
        /// Gets ConsumerType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerType funcion</param>
        /// <param name="IdConsumerType">Specifies filter for ID_CONSUMER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerType list</returns>
        public virtual List<OpConsumerType> GetConsumerTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerType = null, string Name = null, long[] IdDescr = null, long? topCount = null
            ,int[] IdDistributor = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTypeFilter"));
            }

            try
            {
                List<OpConsumerType> returnList = OpConsumerType.ConvertList(dbConnectionCore.GetConsumerTypeFilter(IdConsumerType, Name, IdDescr, topCount,IdDistributor, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerType item in returnList)
                        ConsumerTypeDict[item.IdConsumerType] = item;
                    ConsumerTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerTypeDataFilter

        /// <summary>
        /// Gets ConsumerTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerTypeData funcion</param>
        /// <param name="IdConsumerTypeData">Specifies filter for ID_CONSUMER_TYPE_DATA column</param>
        /// <param name="IdConsumerType">Specifies filter for ID_CONSUMER_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerTypeData list</returns>
        public virtual List<OpConsumerTypeData> GetConsumerTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerTypeData = null, int[] IdConsumerType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerTypeDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerTypeDataFilter"));
            }

            try
            {
                List<OpConsumerTypeData> returnList = OpConsumerTypeData.ConvertList(dbConnectionCore.GetConsumerTypeDataFilter(IdConsumerTypeData, IdConsumerType, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerTypeData item in returnList)
                        ConsumerTypeDataDict[item.IdConsumerTypeData] = item;
                    ConsumerTypeDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerTypeDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerSettlementFilter

        /// <summary>
        /// Gets ConsumerSettlement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlement funcion</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="IdTariffSettlementPeriod">Specifies filter for ID_TARIFF_SETTLEMENT_PERIOD column</param>
        /// <param name="SettlementTime">Specifies filter for SETTLEMENT_TIME column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdConsumerSettlementReason">Specifies filter for ID_CONSUMER_SETTLEMENT_REASON column</param>
        /// <param name="IdConsumerSettlementStatus">Specifies filter for ID_CONSUMER_SETTLEMENT_STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlement list</returns>
        public virtual List<OpConsumerSettlement> GetConsumerSettlementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerSettlement = null, int[] IdConsumer = null, int[] IdTariffSettlementPeriod = null, TypeDateTimeCode SettlementTime = null, int[] IdModule = null, int[] IdOperator = null,
                            int[] IdConsumerSettlementReason = null, int[] IdConsumerSettlementStatus = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerSettlementFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementFilter"));
            }

            try
            {
                List<OpConsumerSettlement> returnList = OpConsumerSettlement.ConvertList(dbConnectionCore.GetConsumerSettlementFilter(IdConsumerSettlement, IdConsumer, IdTariffSettlementPeriod, SettlementTime, IdModule, IdOperator,
             IdConsumerSettlementReason, IdConsumerSettlementStatus, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerSettlement item in returnList)
                        ConsumerSettlementDict[item.IdConsumerSettlement] = item;
                    ConsumerSettlementIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerSettlementFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerSettlementDocumentFilter

        /// <summary>
        /// Gets ConsumerSettlementDocument list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementDocument funcion</param>
        /// <param name="IdConsumerSettlementDocument">Specifies filter for ID_CONSUMER_SETTLEMENT_DOCUMENT column</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdSettlementDocumentType">Specifies filter for ID_SETTLEMENT_DOCUMENT_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_DOCUMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementDocument list</returns>
        public virtual List<OpConsumerSettlementDocument> GetConsumerSettlementDocumentFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConsumerSettlementDocument = null, long[] IdConsumerSettlement = null, int[] IdSettlementDocumentType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerSettlementDocumentFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementDocumentFilter"));
            }

            try
            {
                List<OpConsumerSettlementDocument> returnList = OpConsumerSettlementDocument.ConvertList(dbConnectionCore.GetConsumerSettlementDocumentFilter(IdConsumerSettlementDocument, IdConsumerSettlement, IdSettlementDocumentType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerSettlementDocument item in returnList)
                        ConsumerSettlementDocumentDict[item.IdConsumerSettlementDocument] = item;
                    ConsumerSettlementDocumentIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerSettlementDocumentFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerSettlementErrorFilter

        /// <summary>
        /// Gets ConsumerSettlementError list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementError funcion</param>
        /// <param name="IdConsumerSettlementError">Specifies filter for ID_CONSUMER_SETTLEMENT_ERROR column</param>
        /// <param name="IdConsumerSettlement">Specifies filter for ID_CONSUMER_SETTLEMENT column</param>
        /// <param name="IdConsumerSettlementErrorType">Specifies filter for ID_CONSUMER_SETTLEMENT_ERROR_TYPE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_ERROR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementError list</returns>
        public virtual List<OpConsumerSettlementError> GetConsumerSettlementErrorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementError = null, long[] IdConsumerSettlement = null, int[] IdConsumerSettlementErrorType = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerSettlementErrorFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementErrorFilter"));
            }

            try
            {
                List<OpConsumerSettlementError> returnList = OpConsumerSettlementError.ConvertList(dbConnectionCore.GetConsumerSettlementErrorFilter(IdConsumerSettlementError, IdConsumerSettlement, IdConsumerSettlementErrorType, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerSettlementError item in returnList)
                        ConsumerSettlementErrorDict[item.IdConsumerSettlementError] = item;
                    ConsumerSettlementErrorIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerSettlementErrorFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerSettlementErrorTypeFilter

        /// <summary>
        /// Gets ConsumerSettlementErrorType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementErrorType funcion</param>
        /// <param name="IdConsumerSettlementErrorType">Specifies filter for ID_CONSUMER_SETTLEMENT_ERROR_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_ERROR_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementErrorType list</returns>
        public virtual List<OpConsumerSettlementErrorType> GetConsumerSettlementErrorTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementErrorType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerSettlementErrorTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementErrorTypeFilter"));
            }

            try
            {
                List<OpConsumerSettlementErrorType> returnList = OpConsumerSettlementErrorType.ConvertList(dbConnectionCore.GetConsumerSettlementErrorTypeFilter(IdConsumerSettlementErrorType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerSettlementErrorType item in returnList)
                        ConsumerSettlementErrorTypeDict[item.IdConsumerSettlementErrorType] = item;
                    ConsumerSettlementErrorTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerSettlementErrorTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerSettlementReasonFilter

        /// <summary>
        /// Gets ConsumerSettlementReason list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementReason funcion</param>
        /// <param name="IdConsumerSettlementReason">Specifies filter for ID_CONSUMER_SETTLEMENT_REASON column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_REASON DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementReason list</returns>
        public virtual List<OpConsumerSettlementReason> GetConsumerSettlementReasonFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementReason = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerSettlementReasonFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementReasonFilter"));
            }

            try
            {
                List<OpConsumerSettlementReason> returnList = OpConsumerSettlementReason.ConvertList(dbConnectionCore.GetConsumerSettlementReasonFilter(IdConsumerSettlementReason, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerSettlementReason item in returnList)
                        ConsumerSettlementReasonDict[item.IdConsumerSettlementReason] = item;
                    ConsumerSettlementReasonIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerSettlementReasonFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConsumerSettlementStatusFilter

        /// <summary>
        /// Gets ConsumerSettlementStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConsumerSettlementStatus funcion</param>
        /// <param name="IdConsumerSettlementStatus">Specifies filter for ID_CONSUMER_SETTLEMENT_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IsError">Specifies filter for IS_ERROR column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONSUMER_SETTLEMENT_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ConsumerSettlementStatus list</returns>
        public virtual List<OpConsumerSettlementStatus> GetConsumerSettlementStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdConsumerSettlementStatus = null, string Name = null, bool? IsError = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConsumerSettlementStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConsumerSettlementStatusFilter"));
            }

            try
            {
                List<OpConsumerSettlementStatus> returnList = OpConsumerSettlementStatus.ConvertList(dbConnectionCore.GetConsumerSettlementStatusFilter(IdConsumerSettlementStatus, Name, IsError, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpConsumerSettlementStatus item in returnList)
                        ConsumerSettlementStatusDict[item.IdConsumerSettlementStatus] = item;
                    ConsumerSettlementStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConsumerSettlementStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetContentFilter

        /// <summary>
        /// Gets Content list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllContent funcion</param>
        /// <param name="IdContent">Specifies filter for ID_CONTENT column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONTENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Content list</returns>
        public virtual List<OpContent> GetContentFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdContent = null, int[] IdModule = null, string Name = null, string Description = null, int[] IdLanguage = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetContentFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetContentFilter"));
            }

            try
            {
                List<OpContent> returnList = OpContent.ConvertList(dbConnectionCore.GetContentFilter(IdContent, IdModule, Name, Description, IdLanguage, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpContent item in returnList)
                        ContentDict[item.IdContent] = item;
                    ContentIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetContentFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetContractFilter

        /// <summary>
        /// Gets Contract list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllContract funcion</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONTRACT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Contract list</returns>
        public virtual List<OpContract> GetContractFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdContract = null, string Name = null, string Descr = null, int[] IdDistributor = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetContractFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetContractFilter"));
            }

            try
            {
                List<OpContract> returnList = OpContract.ConvertList(dbConnectionCore.GetContractFilter(IdContract, Name, Descr, IdDistributor, StartDate, EndDate,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpContract item in returnList)
                        ContractDict[item.IdContract] = item;
                    ContractIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetContractFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetContractDataFilter

        /// <summary>
        /// Gets ContractData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllContractData funcion</param>
        /// <param name="IdContractData">Specifies filter for ID_CONTRACT_DATA column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONTRACT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ContractData list</returns>
        public virtual List<OpContractData> GetContractDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdContractData = null, int[] IdContract = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetContractDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetContractDataFilter"));
            }

            try
            {
                List<OpContractData> returnList = OpContractData.ConvertList(dbConnectionCore.GetContractDataFilter(IdContractData, IdContract, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpContractData item in returnList)
                        ContractDataDict[item.IdContractData] = item;
                    ContractDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetContractDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetConversionFilter

        /// <summary>
        /// Gets Conversion list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllConversion funcion</param>
        /// <param name="IdConversion">Specifies filter for ID_CONVERSION column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="ReferenceValue">Specifies filter for REFERENCE_VALUE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Slope">Specifies filter for SLOPE column</param>
        /// <param name="Bias">Specifies filter for BIAS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CONVERSION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Conversion list</returns>
        public virtual List<OpConversion> GetConversionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdConversion = null, int[] IdReferenceType = null, long[] ReferenceValue = null, long[] IdDataType = null, int[] IndexNbr = null, Double[] Slope = null,
                            Double[] Bias = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetConversionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetConversionFilter"));
            }

            try
            {
                List<OpConversion> returnList = OpConversion.ConvertList(dbConnectionCore.GetConversionFilter(IdConversion, IdReferenceType, ReferenceValue, IdDataType, IndexNbr, Slope,
                             Bias, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpConversion item in returnList)
                        ConversionDict[item.IdConversion] = item;
                    ConversionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetConversionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetCurrencyFilter

        /// <summary>
        /// Gets Currency list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllCurrency funcion</param>
        /// <param name="IdCurrency">Specifies filter for ID_CURRENCY column</param>
        /// <param name="Symbol">Specifies filter for SYMBOL column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_CURRENCY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Currency list</returns>
        public virtual List<OpCurrency> GetCurrencyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdCurrency = null, string Symbol = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetCurrencyFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetCurrencyFilter"));
            }

            try
            {
                List<OpCurrency> returnList = OpCurrency.ConvertList(dbConnectionCore.GetCurrencyFilter(IdCurrency, Symbol, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpCurrency item in returnList)
                        CurrencyDict[item.IdCurrency] = item;
                    CurrencyIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetCurrencyFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataArchFilter

        /// <summary>
        /// Gets DataArch list filtered CORE (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataArch funcion</param>
        /// <param name="IdDataArch">Specifies filter for ID_DATA_ARCH column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="IsAction">Specifies filter for IS_ACTION column</param>
        /// <param name="IsAlarm">Specifies filter for IS_ALARM column</param>
        /// <param name="IdPacket">Specifies filter for ID_PACKET column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_ARCH DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataArch list</returns>
        public virtual List<OpDataArch> GetDataArchFilter(bool loadNavigationProperties = true, long[] IdDataArch = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null, TypeDateTimeCode Time = null, bool? IsAction = null,
                            bool? IsAlarm = null, long[] IdPacket = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataArchFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataArchFilter"));
            }

            try
            {
                List<OpDataArch> returnList = OpDataArch.ConvertList(dbConnectionCore.GetDataArchFilter(IdDataArch, SerialNbr, IdDataType, IndexNbr, Time, IsAction,
                             IsAlarm, IdPacket, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                //if (mergeIntoCache)
                //{
                //    foreach (OpDataArch item in returnList)
                //        DataArchDict[item.IdDataArch] = item;
                //    DataArchIsFullCache = true;
                //}
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataArchFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataChangeFilter

        /// <summary>
        /// Gets DataChange list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataChange funcion</param>
        /// <param name="IdDataChange">Specifies filter for ID_DATA_CHANGE column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="ChangedId">Specifies filter for CHANGED_ID column</param>
        /// <param name="ChangedId2">Specifies filter for CHANGED_ID2 column</param>
        /// <param name="ChangedId3">Specifies filter for CHANGED_ID3 column</param>
        /// <param name="ChangedId4">Specifies filter for CHANGED_ID4 column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_CHANGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataChange list</returns>
        public virtual List<OpDataChange> GetDataChangeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataChange = null, string TableName = null, long[] ChangedId = null, long[] ChangedId2 = null, long[] ChangedId3 = null, long[] ChangedId4 = null,
                            int[] IndexNbr = null, int[] ChangeType = null, TypeDateTimeCode Time = null, string User = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataChangeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataChangeFilter"));
            }

            try
            {
                List<OpDataChange> returnList = OpDataChange.ConvertList(dbConnectionCore.GetDataChangeFilter(IdDataChange, TableName, ChangedId, ChangedId2, ChangedId3, ChangedId4,
                             IndexNbr, ChangeType, Time, User, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataChange item in returnList)
                        DataChangeDict[item.IdDataChange] = item;
                    DataChangeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataChangeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataFormatGroupFilter

        /// <summary>
        /// Gets DataFormatGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataFormatGroup funcion</param>
        /// <param name="IdDataFormatGroup">Specifies filter for ID_DATA_FORMAT_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_FORMAT_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataFormatGroup list</returns>
        public virtual List<OpDataFormatGroup> GetDataFormatGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataFormatGroup = null, string Name = null, int[] IdModule = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataFormatGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFormatGroupFilter"));
            }

            try
            {
                List<OpDataFormatGroup> returnList = OpDataFormatGroup.ConvertList(dbConnectionCore.GetDataFormatGroupFilter(IdDataFormatGroup, Name, IdModule, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataFormatGroup item in returnList)
                        DataFormatGroupDict[item.IdDataFormatGroup] = item;
                    DataFormatGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFormatGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataFormatGroupDetailsFilter

        /// <summary>
        /// Gets DataFormatGroupDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataFormatGroupDetails funcion</param>
        /// <param name="IdDataFormatGroup">Specifies filter for ID_DATA_FORMAT_GROUP column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IdDataTypeFormatIn">Specifies filter for ID_DATA_TYPE_FORMAT_IN column</param>
        /// <param name="IdDataTypeFormatOut">Specifies filter for ID_DATA_TYPE_FORMAT_OUT column</param>
        /// <param name="IdUnitIn">Specifies filter for ID_UNIT_IN column</param>
        /// <param name="IdUnitOut">Specifies filter for ID_UNIT_OUT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_FORMAT_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataFormatGroupDetails list</returns>
        public virtual List<OpDataFormatGroupDetails> GetDataFormatGroupDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataFormatGroup = null, long[] IdDataType = null, int[] IdDataTypeFormatIn = null, int[] IdDataTypeFormatOut = null, int[] IdUnitIn = null, int[] IdUnitOut = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataFormatGroupDetailsFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataFormatGroupDetailsFilter"));
            }

            try
            {
                List<OpDataFormatGroupDetails> returnList = OpDataFormatGroupDetails.ConvertList(dbConnectionCore.GetDataFormatGroupDetailsFilter(IdDataFormatGroup, IdDataType, IdDataTypeFormatIn, IdDataTypeFormatOut, IdUnitIn, IdUnitOut,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataFormatGroupDetails item in returnList)
                    {
                        if (DataFormatGroupDetailsDict.ContainsKey(item.IdDataFormatGroup))
                        {
                            if (DataFormatGroupDetailsDict[item.IdDataFormatGroup] == null)
                            {
                                DataFormatGroupDetailsDict[item.IdDataFormatGroup] = new List<OpDataFormatGroupDetails>();
                                DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                            }
                            else
                            {
                                if (DataFormatGroupDetailsDict[item.IdDataFormatGroup].Any(q => q.IdDataFormatGroup == item.IdDataFormatGroup && q.IdDataType == item.IdDataType))
                                {
                                    DataFormatGroupDetailsDict[item.IdDataFormatGroup].RemoveAll(q => q.IdDataFormatGroup == item.IdDataFormatGroup && q.IdDataType == item.IdDataType);
                                    DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                                }
                                else
                                    DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                            }
                        }
                        else
                        {
                            DataFormatGroupDetailsDict.Add(item.IdDataFormatGroup, new List<OpDataFormatGroupDetails>());
                            DataFormatGroupDetailsDict[item.IdDataFormatGroup].Add(item); //add element
                        }
                    }
                    DataFormatGroupDetailsIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataFormatGroupDetailsFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataTypeFilter

        /// <summary>
        /// Gets DataType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataType funcion</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdDataTypeFormat">Specifies filter for ID_DATA_TYPE_FORMAT column</param>
        /// <param name="IsArchiveOnly">Specifies filter for IS_ARCHIVE_ONLY column</param>
        /// <param name="IsRemoteRead">Specifies filter for IS_REMOTE_READ column</param>
        /// <param name="IsRemoteWrite">Specifies filter for IS_REMOTE_WRITE column</param>
        /// <param name="IsEditable">Specifies filter for IS_EDITABLE column</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataType list</returns>
        public virtual List<OpDataType> GetDataTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataType = null, string Name = null, long[] IdDescr = null, int[] IdDataTypeClass = null, int[] IdReferenceType = null, int[] IdDataTypeFormat = null,
                            bool? IsArchiveOnly = null, bool? IsRemoteRead = null, bool? IsRemoteWrite = null, bool? IsEditable = null, int[] IdUnit = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeFilter"));
            }

            try
            {
                List<OpDataType> returnList = OpDataType.ConvertList(dbConnectionCore.GetDataTypeFilter(IdDataType, Name, IdDescr, IdDataTypeClass, IdReferenceType, IdDataTypeFormat,
                             IsArchiveOnly, IsRemoteRead, IsRemoteWrite, IsEditable, IdUnit,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDataType item in returnList)
                        DataTypeDict[item.IdDataType] = item;
                    DataTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataTypeClassFilter

        /// <summary>
        /// Gets DataTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeClass funcion</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeClass list</returns>
        public virtual List<OpDataTypeClass> GetDataTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTypeClassFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeClassFilter"));
            }

            try
            {
                List<OpDataTypeClass> returnList = OpDataTypeClass.ConvertList(dbConnectionCore.GetDataTypeClassFilter(IdDataTypeClass, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDataTypeClass item in returnList)
                        DataTypeClassDict[item.IdDataTypeClass] = item;
                    DataTypeClassIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeClassFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataTypeFormatFilter

        /// <summary>
        /// Gets DataTypeFormat list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeFormat funcion</param>
        /// <param name="IdDataTypeFormat">Specifies filter for ID_DATA_TYPE_FORMAT column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="TextMinLength">Specifies filter for TEXT_MIN_LENGTH column</param>
        /// <param name="TextMaxLength">Specifies filter for TEXT_MAX_LENGTH column</param>
        /// <param name="NumberMinPrecision">Specifies filter for NUMBER_MIN_PRECISION column</param>
        /// <param name="NumberMaxPrecision">Specifies filter for NUMBER_MAX_PRECISION column</param>
        /// <param name="NumberMinScale">Specifies filter for NUMBER_MIN_SCALE column</param>
        /// <param name="NumberMaxScale">Specifies filter for NUMBER_MAX_SCALE column</param>
        /// <param name="NumberMinValue">Specifies filter for NUMBER_MIN_VALUE column</param>
        /// <param name="NumberMaxValue">Specifies filter for NUMBER_MAX_VALUE column</param>
        /// <param name="DatetimeFormat">Specifies filter for DATETIME_FORMAT column</param>
        /// <param name="RegularExpression">Specifies filter for REGULAR_EXPRESSION column</param>
        /// <param name="IsRequired">Specifies filter for IS_REQUIRED column</param>
        /// <param name="IdUniqueType">Specifies filter for ID_UNIQUE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_FORMAT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeFormat list</returns>
        public virtual List<OpDataTypeFormat> GetDataTypeFormatFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataTypeFormat = null, long[] IdDescr = null, int[] TextMinLength = null, int[] TextMaxLength = null, int[] NumberMinPrecision = null, int[] NumberMaxPrecision = null,
                            int[] NumberMinScale = null, int[] NumberMaxScale = null, Double[] NumberMinValue = null, Double[] NumberMaxValue = null, string DatetimeFormat = null,
                            string RegularExpression = null, bool? IsRequired = null, int[] IdUniqueType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTypeFormatFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeFormatFilter"));
            }

            try
            {
                List<OpDataTypeFormat> returnList = OpDataTypeFormat.ConvertList(dbConnectionCore.GetDataTypeFormatFilter(IdDataTypeFormat, IdDescr, TextMinLength, TextMaxLength, NumberMinPrecision, NumberMaxPrecision,
                             NumberMinScale, NumberMaxScale, NumberMinValue, NumberMaxValue, DatetimeFormat,
                             RegularExpression, IsRequired, IdUniqueType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDataTypeFormat item in returnList)
                        DataTypeFormatDict[item.IdDataTypeFormat] = item;
                    DataTypeFormatIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeFormatFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataTypeGroupFilter

        /// <summary>
        /// Gets DataTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeGroup funcion</param>
        /// <param name="IdDataTypeGroup">Specifies filter for ID_DATA_TYPE_GROUP column</param>
        /// <param name="IdDataTypeGroupType">Specifies filter for ID_DATA_TYPE_GROUP_TYPE column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdParentGroup">Specifies filter for ID_PARENT_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeGroup list</returns>
        public virtual List<OpDataTypeGroup> GetDataTypeGroupFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdDataTypeGroup = null, int[] IdDataTypeGroupType = null, int[] IdReferenceType = null, string Name = null, int[] IdParentGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTypeGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeGroupFilter"));
            }

            try
            {
                List<OpDataTypeGroup> returnList = OpDataTypeGroup.ConvertList(dbConnectionCore.GetDataTypeGroupFilter(IdDataTypeGroup, IdDataTypeGroupType, IdReferenceType, Name, IdParentGroup, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData: loadCustomData);
                if (mergeIntoCache)
                {
                    foreach (OpDataTypeGroup item in returnList)
                        DataTypeGroupDict[item.IdDataTypeGroup] = item;
                    DataTypeGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataTypeGroupTypeFilter

        /// <summary>
        /// Gets DataTypeGroupType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTypeGroupType funcion</param>
        /// <param name="IdDataTypeGroupType">Specifies filter for ID_DATA_TYPE_GROUP_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TYPE_GROUP_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTypeGroupType list</returns>
        public virtual List<OpDataTypeGroupType> GetDataTypeGroupTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDataTypeGroupType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTypeGroupTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTypeGroupTypeFilter"));
            }

            try
            {
                List<OpDataTypeGroupType> returnList = OpDataTypeGroupType.ConvertList(dbConnectionCore.GetDataTypeGroupTypeFilter(IdDataTypeGroupType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDataTypeGroupType item in returnList)
                        DataTypeGroupTypeDict[item.IdDataTypeGroupType] = item;
                    DataTypeGroupTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTypeGroupTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDataTransferFilter

        /// <summary>
        /// Gets DataTransfer list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDataTransfer funcion</param>
        /// <param name="IdDataTransfer">Specifies filter for ID_DATA_TRANSFER column</param>
        /// <param name="BatchId">Specifies filter for BATCH_ID column</param>
        /// <param name="ChangeType">Specifies filter for CHANGE_TYPE column</param>
        /// <param name="ChangeDirection">Specifies filter for CHANGE_DIRECTION column</param>
        /// <param name="IdSourceServer">Specifies filter for ID_SOURCE_SERVER column</param>
        /// <param name="IdDestinationServer">Specifies filter for ID_DESTINATION_SERVER column</param>
        /// <param name="TableName">Specifies filter for TABLE_NAME column</param>
        /// <param name="Key1">Specifies filter for KEY1 column</param>
        /// <param name="Key2">Specifies filter for KEY2 column</param>
        /// <param name="Key3">Specifies filter for KEY3 column</param>
        /// <param name="Key4">Specifies filter for KEY4 column</param>
        /// <param name="Key5">Specifies filter for KEY5 column</param>
        /// <param name="Key6">Specifies filter for KEY6 column</param>
        /// <param name="ColumnName">Specifies filter for COLUMN_NAME column</param>
        /// <param name="InsertTime">Specifies filter for INSERT_TIME column</param>
        /// <param name="ProceedTime">Specifies filter for PROCEED_TIME column</param>
        /// <param name="User">Specifies filter for USER column</param>
        /// <param name="Host">Specifies filter for HOST column</param>
        /// <param name="Application">Specifies filter for APPLICATION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DATA_TRANSFER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DataTransfer list</returns>
        public virtual List<OpDataTransfer> GetDataTransferFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDataTransfer = null, long[] BatchId = null, int[] ChangeType = null, int[] ChangeDirection = null, int[] IdSourceServer = null, int[] IdDestinationServer = null,
                            string TableName = null, long[] Key1 = null, long[] Key2 = null, long[] Key3 = null, long[] Key4 = null,
                            long[] Key5 = null, long[] Key6 = null, string ColumnName = null, TypeDateTimeCode InsertTime = null, TypeDateTimeCode ProceedTime = null,
                            string User = null, string Host = null, string Application = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDataTransferFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDataTransferFilter"));
            }

            try
            {
                List<OpDataTransfer> returnList = OpDataTransfer.ConvertList(dbConnectionCore.GetDataTransferFilter(IdDataTransfer, BatchId, ChangeType, ChangeDirection, IdSourceServer, IdDestinationServer,
                             TableName, Key1, Key2, Key3, Key4,
                             Key5, Key6, ColumnName, InsertTime, ProceedTime,
                             User, Host, Application, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDataTransfer item in returnList)
                        DataTransferDict[item.IdDataTransfer] = item;
                    DataTransferIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDataTransferFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDaylightSavingTimeFilter

        /// <summary>
        /// Gets DaylightSavingTime list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDaylightSavingTime funcion</param>
        /// <param name="IdDaylightSaving">Specifies filter for ID_DAYLIGHT_SAVING column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DAYLIGHT_SAVING DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DaylightSavingTime list</returns>
        public virtual List<OpDaylightSavingTime> GetDaylightSavingTimeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDaylightSaving = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDaylightSavingTimeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDaylightSavingTimeFilter"));
            }

            try
            {
                List<OpDaylightSavingTime> returnList = OpDaylightSavingTime.ConvertList(dbConnectionCore.GetDaylightSavingTimeFilter(IdDaylightSaving, StartTime, EndTime, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDaylightSavingTime item in returnList)
                        DaylightSavingTimeDict[item.IdDaylightSaving] = item;
                    DaylightSavingTimeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDaylightSavingTimeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeliveryFilter

        /// <summary>
        /// Gets Delivery list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDelivery funcion</param>
        /// <param name="IdDelivery">Specifies filter for ID_DELIVERY column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="UploadedFile">Specifies filter for UPLOADED_FILE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DELIVERY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Delivery list</returns>
        public virtual List<OpDelivery> GetDeliveryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDelivery = null, string Name = null, string Description = null, string UploadedFile = null, TypeDateTimeCode CreationDate = null, int[] IdShippingList = null,
                            int[] IdOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeliveryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeliveryFilter"));
            }

            try
            {
                List<OpDelivery> returnList = OpDelivery.ConvertList(dbConnectionCore.GetDeliveryFilter(IdDelivery, Name, Description, UploadedFile, CreationDate, IdShippingList,
                             IdOperator, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDelivery item in returnList)
                        DeliveryDict[item.IdDelivery] = item;
                    DeliveryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeliveryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDepositoryElementFilter

        /// <summary>
        /// Gets DepositoryElement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDepositoryElement funcion</param>
        /// <param name="IdDepositoryElement">Specifies filter for ID_DEPOSITORY_ELEMENT column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="IdDeviceTypeClass">Specifies filter for ID_DEVICE_TYPE_CLASS column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Removed">Specifies filter for REMOVED column</param>
        /// <param name="Ordered">Specifies filter for ORDERED column</param>
        /// <param name="Accepted">Specifies filter for ACCEPTED column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="ExternalSn">Specifies filter for EXTERNAL_SN column</param>
        /// <param name="IdReference">Specifies filter for ID_REFERENCE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEPOSITORY_ELEMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DepositoryElement list</returns>
        public virtual List<OpDepositoryElement> GetDepositoryElementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDepositoryElement = null, long[] IdLocation = null, int[] IdTask = null, int[] IdPackage = null, long[] IdArticle = null, int[] IdDeviceTypeClass = null,
                            int[] IdDeviceStateType = null, long[] SerialNbr = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, bool? Removed = null,
                            bool? Ordered = null, bool? Accepted = null, string Notes = null, string ExternalSn = null, int[] IdReference = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                            bool loadDeviceDetails = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDepositoryElementFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDepositoryElementFilter"));
            }

            try
            {
                List<OpDepositoryElement> returnList = OpDepositoryElement.ConvertList(dbConnectionCore.GetDepositoryElementFilter(IdDepositoryElement, IdLocation, IdTask, IdPackage, IdArticle, IdDeviceTypeClass,
                             IdDeviceStateType, SerialNbr, StartDate, EndDate, Removed,
                             Ordered, Accepted, Notes, ExternalSn, IdReference,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadDeviceDetails);
                if (mergeIntoCache)
                {
                    foreach (OpDepositoryElement item in returnList)
                        DepositoryElementDict[item.IdDepositoryElement] = item;
                    DepositoryElementIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDepositoryElementFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDepositoryElementDataFilter

        /// <summary>
        /// Gets DepositoryElementData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDepositoryElementData funcion</param>
        /// <param name="IdDepositoryElementData">Specifies filter for ID_DEPOSITORY_ELEMENT_DATA column</param>
        /// <param name="IdDepositoryElement">Specifies filter for ID_DEPOSITORY_ELEMENT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEPOSITORY_ELEMENT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DepositoryElementData list</returns>
        public virtual List<OpDepositoryElementData> GetDepositoryElementDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDepositoryElementData = null, int[] IdDepositoryElement = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDepositoryElementDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDepositoryElementDataFilter"));
            }

            try
            {
                List<OpDepositoryElementData> returnList = OpDepositoryElementData.ConvertList(dbConnectionCore.GetDepositoryElementDataFilter(IdDepositoryElementData, IdDepositoryElement, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDepositoryElementData item in returnList)
                        DepositoryElementDataDict[item.IdDepositoryElementData] = item;
                    DepositoryElementDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDepositoryElementDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDepotFilter

        /// <summary>
        /// Gets Depot list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDepot funcion</param>
        /// <param name="IdDepot">Specifies filter for ID_DEPOT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEPOT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Depot list</returns>
        public virtual List<OpDepot> GetDepotFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDepot = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDepotFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDepotFilter"));
            }

            try
            {
                List<OpDepot> returnList = OpDepot.ConvertList(dbConnectionCore.GetDepotFilter(IdDepot, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDepot item in returnList)
                        DepotDict[item.IdDepot] = item;
                    DepotIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDepotFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceDetailsFilter

        /// <summary>
        /// Gets DeviceDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDetails funcion</param>
        /// <param name="IdDeviceDetails">Specifies filter for ID_DEVICE_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="FactoryNbr">Specifies filter for FACTORY_NBR column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="WarrantyDate">Specifies filter for WARRANTY_DATE column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDetails list</returns>
        public virtual List<OpDeviceDetails> GetDeviceDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDetails = null, long[] SerialNbr = null, long[] IdLocation = null, string FactoryNbr = null, TypeDateTimeCode ShippingDate = null, TypeDateTimeCode WarrantyDate = null,
                            string Phone = null, int[] IdDeviceType = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, int[] IdDeviceStateType = null,
                            string ProductionDeviceOrderNumber = null, string DeviceBatchNumber = null, int[] IdDistributorOwner = null,
                            long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDetailsFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDetailsFilter"));
            }

            try
            {
                List<OpDeviceDetails> returnList = OpDeviceDetails.ConvertList(dbConnectionCore.GetDeviceDetailsFilter(IdDeviceDetails, SerialNbr, IdLocation, FactoryNbr, ShippingDate, WarrantyDate,
                             Phone, IdDeviceType, IdDeviceOrderNumber, IdDistributor, IdDeviceStateType, ProductionDeviceOrderNumber, DeviceBatchNumber, IdDistributorOwner,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceDetails item in returnList)
                        DeviceDetailsDict[item.IdDeviceDetails] = item;
                    DeviceDetailsIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceDetailsFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceDistributorHistoryFilter

        /// <summary>
        /// Gets DeviceDistributorHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDistributorHistory funcion</param>
        /// <param name="IdDeviceDistributorHistory">Specifies filter for ID_DEVICE_DISTRIBUTOR_HISTORY column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDistributorOwner">Specifies filter for ID_DISTRIBUTOR_OWNER column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DISTRIBUTOR_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDistributorHistory list</returns>
        public virtual List<OpDeviceDistributorHistory> GetDeviceDistributorHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDistributorHistory = null, long[] SerialNbr = null, int[] IdDistributor = null, int[] IdDistributorOwner = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            string Notes = null, int[] IdOperator = null, int[] IdServiceReferenceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDistributorHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDistributorHistoryFilter"));
            }

            try
            {
                List<OpDeviceDistributorHistory> returnList = OpDeviceDistributorHistory.ConvertList(dbConnectionCore.GetDeviceDistributorHistoryFilter(IdDeviceDistributorHistory, SerialNbr, IdDistributor, IdDistributorOwner, StartTime, EndTime,
                             Notes, IdOperator, IdServiceReferenceType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceDistributorHistory item in returnList)
                        DeviceDistributorHistoryDict[item.IdDeviceDistributorHistory] = item;
                    DeviceDistributorHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceDistributorHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceDriverFilter

        /// <summary>
        /// Gets DeviceDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriver funcion</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="AutoUpdate">Specifies filter for AUTO_UPDATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDriver list</returns>
        public virtual List<OpDeviceDriver> GetDeviceDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceDriver = null, string Name = null, long[] IdDescr = null, string PluginName = null,
                            bool? AutoUpdate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDriverFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverFilter"));
            }

            try
            {
                List<OpDeviceDriver> returnList = OpDeviceDriver.ConvertList(dbConnectionCore.GetDeviceDriverFilter(IdDeviceDriver, Name, IdDescr, PluginName,
                             AutoUpdate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceDriver item in returnList)
                        DeviceDriverDict[item.IdDeviceDriver] = item;
                    DeviceDriverIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceDriverFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceDriverDataFilter

        /// <summary>
        /// Gets DeviceDriverData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceDriverData funcion</param>
        /// <param name="IdDeviceDriverData">Specifies filter for ID_DEVICE_DRIVER_DATA column</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_DRIVER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceDriverData list</returns>
        public virtual List<OpDeviceDriverData> GetDeviceDriverDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceDriverData = null, int[] IdDeviceDriver = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceDriverDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceDriverDataFilter"));
            }

            try
            {
                List<OpDeviceDriverData> returnList = OpDeviceDriverData.ConvertList(dbConnectionCore.GetDeviceDriverDataFilter(IdDeviceDriverData, IdDeviceDriver, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceDriverData item in returnList)
                        DeviceDriverDataDict[item.IdDeviceDriverData] = item;
                    DeviceDriverDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceDriverDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceHierarchyFilter

        /// <summary>
        /// Gets DeviceHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchy funcion</param>
        /// <param name="IdDeviceHierarchy">Specifies filter for ID_DEVICE_HIERARCHY column</param>
        /// <param name="SerialNbrParent">Specifies filter for SERIAL_NBR_PARENT column</param>
        /// <param name="IdSlotType">Specifies filter for ID_SLOT_TYPE column</param>
        /// <param name="SlotNbr">Specifies filter for SLOT_NBR column</param>
        /// <param name="IdProtocolIn">Specifies filter for ID_PROTOCOL_IN column</param>
        /// <param name="IdProtocolOut">Specifies filter for ID_PROTOCOL_OUT column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchy list</returns>
        public virtual List<OpDeviceHierarchy> GetDeviceHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchy = null, long[] SerialNbrParent = null, int[] IdSlotType = null, int[] SlotNbr = null, int[] IdProtocolIn = null, int[] IdProtocolOut = null,
                            long[] SerialNbr = null, bool? IsActive = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceHierarchyFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyFilter"));
            }

            try
            {
                List<OpDeviceHierarchy> returnList = OpDeviceHierarchy.ConvertList(dbConnectionCore.GetDeviceHierarchyFilter(IdDeviceHierarchy, SerialNbrParent, IdSlotType, SlotNbr, IdProtocolIn, IdProtocolOut,
                             SerialNbr, IsActive, StartTime, EndTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceHierarchy item in returnList)
                        DeviceHierarchyDict[item.IdDeviceHierarchy] = item;
                    DeviceHierarchyIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceHierarchyFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceHierarchyGroupFilter

        /// <summary>
        /// Gets DeviceHierarchyGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchyGroup funcion</param>
        /// <param name="IdDeviceHierarchyGroup">Specifies filter for ID_DEVICE_HIERARCHY_GROUP column</param>
        /// <param name="DeviceHierarchySerialNbr">Specifies filter for DEVICE_HIERARCHY_SERIAL_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDepositoryLocation">Specifies filter for ID_DEPOSITORY_LOCATION column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="FinishDate">Specifies filter for FINISH_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchyGroup list</returns>
        public virtual List<OpDeviceHierarchyGroup> GetDeviceHierarchyGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchyGroup = null, string DeviceHierarchySerialNbr = null, int[] IdDistributor = null, long[] IdDepositoryLocation = null, TypeDateTimeCode CreationDate = null, TypeDateTimeCode FinishDate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceHierarchyGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyGroupFilter"));
            }

            try
            {
                List<OpDeviceHierarchyGroup> returnList = OpDeviceHierarchyGroup.ConvertList(dbConnectionCore.GetDeviceHierarchyGroupFilter(IdDeviceHierarchyGroup, DeviceHierarchySerialNbr, IdDistributor, IdDepositoryLocation, CreationDate, FinishDate,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceHierarchyGroup item in returnList)
                        DeviceHierarchyGroupDict[item.IdDeviceHierarchyGroup] = item;
                    DeviceHierarchyGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceHierarchyGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceHierarchyInGroupFilter

        /// <summary>
        /// Gets DeviceHierarchyInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchyInGroup funcion</param>
        /// <param name="IdDeviceHierarchyGroup">Specifies filter for ID_DEVICE_HIERARCHY_GROUP column</param>
        /// <param name="IdDeviceHierarchy">Specifies filter for ID_DEVICE_HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchyInGroup list</returns>
        public virtual List<OpDeviceHierarchyInGroup> GetDeviceHierarchyInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchyGroup = null, long[] IdDeviceHierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceHierarchyInGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyInGroupFilter"));
            }

            try
            {
                List<OpDeviceHierarchyInGroup> returnList = OpDeviceHierarchyInGroup.ConvertList(dbConnectionCore.GetDeviceHierarchyInGroupFilter(IdDeviceHierarchyGroup, IdDeviceHierarchy, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceHierarchyInGroup item in returnList)
                        DeviceHierarchyInGroupDict[item.IdDeviceHierarchyGroup] = item;
                    DeviceHierarchyInGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceHierarchyInGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceHierarchyPatternFilter

        /// <summary>
        /// Gets DeviceHierarchyPattern list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceHierarchyPattern funcion</param>
        /// <param name="IdDeviceHierarchyPattern">Specifies filter for ID_DEVICE_HIERARCHY_PATTERN column</param>
        /// <param name="IdDeviceTypeParent">Specifies filter for ID_DEVICE_TYPE_PARENT column</param>
        /// <param name="IdSlotType">Specifies filter for ID_SLOT_TYPE column</param>
        /// <param name="SlotNbrMin">Specifies filter for SLOT_NBR_MIN column</param>
        /// <param name="SlotNbrMax">Specifies filter for SLOT_NBR_MAX column</param>
        /// <param name="IdProtocolIn">Specifies filter for ID_PROTOCOL_IN column</param>
        /// <param name="IdProtocolOut">Specifies filter for ID_PROTOCOL_OUT column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDeviceTypeGroup">Specifies filter for ID_DEVICE_TYPE_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_HIERARCHY_PATTERN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceHierarchyPattern list</returns>
        public virtual List<OpDeviceHierarchyPattern> GetDeviceHierarchyPatternFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceHierarchyPattern = null, int[] IdDeviceTypeParent = null, int[] IdSlotType = null, int[] SlotNbrMin = null, int[] SlotNbrMax = null, int[] IdProtocolIn = null,
                            int[] IdProtocolOut = null, int[] IdDeviceType = null, int[] IdDeviceTypeGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceHierarchyPatternFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceHierarchyPatternFilter"));
            }

            try
            {
                List<OpDeviceHierarchyPattern> returnList = OpDeviceHierarchyPattern.ConvertList(dbConnectionCore.GetDeviceHierarchyPatternFilter(IdDeviceHierarchyPattern, IdDeviceTypeParent, IdSlotType, SlotNbrMin, SlotNbrMax, IdProtocolIn,
                             IdProtocolOut, IdDeviceType, IdDeviceTypeGroup, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceHierarchyPattern item in returnList)
                        DeviceHierarchyPatternDict[item.IdDeviceHierarchyPattern] = item;
                    DeviceHierarchyPatternIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceHierarchyPatternFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceOrderNumberFilter

        /// <summary>
        /// Gets DeviceOrderNumber list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceOrderNumber funcion</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="FirstQuarter">Specifies filter for FIRST_QUARTER column</param>
        /// <param name="SecondQuarter">Specifies filter for SECOND_QUARTER column</param>
        /// <param name="ThirdQuarter">Specifies filter for THIRD_QUARTER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceOrderNumber list</returns>
        public virtual List<OpDeviceOrderNumber> GetDeviceOrderNumberFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceOrderNumber = null, string Name = null, string FirstQuarter = null, string SecondQuarter = null, string ThirdQuarter = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadCustomData = true)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceOrderNumberFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceOrderNumberFilter"));
            }

            try
            {
                List<OpDeviceOrderNumber> returnList = OpDeviceOrderNumber.ConvertList(dbConnectionCore.GetDeviceOrderNumberFilter(IdDeviceOrderNumber, Name, FirstQuarter, SecondQuarter, ThirdQuarter, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceOrderNumber item in returnList)
                        DeviceOrderNumberDict[item.IdDeviceOrderNumber] = item;
                    DeviceOrderNumberIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceOrderNumberFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceOrderNumberDataFilter

        /// <summary>
        /// Gets DeviceOrderNumberData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceOrderNumberData funcion</param>
        /// <param name="IdDeviceOrderNumberData">Specifies filter for ID_DEVICE_ORDER_NUMBER_DATA column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceOrderNumberData list</returns>
        public virtual List<OpDeviceOrderNumberData> GetDeviceOrderNumberDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceOrderNumberData = null, int[] IdDeviceOrderNumber = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceOrderNumberDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceOrderNumberDataFilter"));
            }

            try
            {
                List<OpDeviceOrderNumberData> returnList = OpDeviceOrderNumberData.ConvertList(dbConnectionCore.GetDeviceOrderNumberDataFilter(IdDeviceOrderNumberData, IdDeviceOrderNumber, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceOrderNumberData item in returnList)
                        DeviceOrderNumberDataDict[item.IdDeviceOrderNumberData] = item;
                    DeviceOrderNumberDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceOrderNumberDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceOrderNumberInArticleFilter

        /// <summary>
        /// Gets DeviceOrderNumberInArticle list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceOrderNumberInArticle funcion</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceOrderNumberInArticle list</returns>
        public virtual List<OpDeviceOrderNumberInArticle> GetDeviceOrderNumberInArticleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceOrderNumber = null, long[] IdArticle = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceOrderNumberInArticleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceOrderNumberInArticleFilter"));
            }

            try
            {
                List<OpDeviceOrderNumberInArticle> returnList = OpDeviceOrderNumberInArticle.ConvertList(dbConnectionCore.GetDeviceOrderNumberInArticleFilter(IdDeviceOrderNumber, IdArticle, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceOrderNumberInArticle item in returnList)
                        DeviceOrderNumberInArticleDict[item.IdDeviceOrderNumber] = item;
                    DeviceOrderNumberInArticleIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceOrderNumberInArticleFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDevicePatternDataFilter

        /// <summary>
        /// Gets DevicePatternData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDevicePatternData funcion</param>
        /// <param name="IdPattern">Specifies filter for ID_PATTERN column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PATTERN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DevicePatternData list</returns>
        public virtual List<OpDevicePatternData> GetDevicePatternDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPattern = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDevicePatternDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDevicePatternDataFilter"));
            }

            try
            {
                List<OpDevicePatternData> returnList = OpDevicePatternData.ConvertList(dbConnectionCore.GetDevicePatternDataFilter(IdPattern, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDevicePatternData item in returnList)
                        DevicePatternDataDict[item.IdPattern] = item;
                    DevicePatternDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDevicePatternDataFilter", ex);
                throw;
            }

        }
        #endregion       
        #region GetDeviceSimCardHistoryFilter

        /// <summary>
        /// Gets DeviceSimCardHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceSimCardHistory funcion</param>
        /// <param name="IdDeviceSimCardHistory">Specifies filter for ID_DEVICE_SIM_CARD_HISTORY column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdSimCard">Specifies filter for ID_SIM_CARD column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_SIM_CARD_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceSimCardHistory list</returns>
        public virtual List<OpDeviceSimCardHistory> GetDeviceSimCardHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceSimCardHistory = null, long[] SerialNbr = null, int[] IdSimCard = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceSimCardHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceSimCardHistoryFilter"));
            }

            try
            {
                List<OpDeviceSimCardHistory> returnList = OpDeviceSimCardHistory.ConvertList(dbConnectionCore.GetDeviceSimCardHistoryFilter(IdDeviceSimCardHistory, SerialNbr, IdSimCard, StartTime, EndTime, Notes,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceSimCardHistory item in returnList)
                        DeviceSimCardHistoryDict[item.IdDeviceSimCardHistory] = item;
                    DeviceSimCardHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceSimCardHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTemplateFilter

        /// <summary>
        /// Gets DeviceTemplate list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTemplate funcion</param>
        /// <param name="IdTemplate">Specifies filter for ID_TEMPLATE column</param>
        /// <param name="IdTemplatePattern">Specifies filter for ID_TEMPLATE_PATTERN column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="FirmwareVersion">Specifies filter for FIRMWARE_VERSION column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CreatedBy">Specifies filter for CREATED_BY column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ConfirmedBy">Specifies filter for CONFIRMED_BY column</param>
        /// <param name="ConfirmDate">Specifies filter for CONFIRM_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TEMPLATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTemplate list</returns>
        public virtual List<OpDeviceTemplate> GetDeviceTemplateFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTemplate = null, int[] IdTemplatePattern = null, int[] IdDeviceType = null, string FirmwareVersion = null, string Name = null, int[] CreatedBy = null,
                            TypeDateTimeCode CreationDate = null, int[] ConfirmedBy = null, TypeDateTimeCode ConfirmDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTemplateFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTemplateFilter"));
            }

            try
            {
                List<OpDeviceTemplate> returnList = OpDeviceTemplate.ConvertList(dbConnectionCore.GetDeviceTemplateFilter(IdTemplate, IdTemplatePattern, IdDeviceType, FirmwareVersion, Name, CreatedBy,
                             CreationDate, ConfirmedBy, ConfirmDate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTemplate item in returnList)
                        DeviceTemplateDict[item.IdTemplate] = item;
                    DeviceTemplateIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplateFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTemplateDataFilter

        /// <summary>
        /// Gets DeviceTemplateData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTemplateData funcion</param>
        /// <param name="IdTemplate">Specifies filter for ID_TEMPLATE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TEMPLATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTemplateData list</returns>
        public virtual List<OpDeviceTemplateData> GetDeviceTemplateDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTemplate = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTemplateDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTemplateDataFilter"));
            }

            try
            {
                List<OpDeviceTemplateData> returnList = OpDeviceTemplateData.ConvertList(dbConnectionCore.GetDeviceTemplateDataFilter(IdTemplate, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTemplateData item in returnList)
                        DeviceTemplateDataDict[item.IdTemplate] = item;
                    DeviceTemplateDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTemplateDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceStateTypeFilter

        /// <summary>
        /// Gets DeviceStateType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceStateType funcion</param>
        /// <param name="IdDeviceStateType">Specifies filter for ID_DEVICE_STATE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_STATE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceStateType list</returns>
        public virtual List<OpDeviceStateType> GetDeviceStateTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceStateType = null, string Name = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceStateTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceStateTypeFilter"));
            }

            try
            {
                List<OpDeviceStateType> returnList = OpDeviceStateType.ConvertList(dbConnectionCore.GetDeviceStateTypeFilter(IdDeviceStateType, Name, IdDescr, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceStateType item in returnList)
                        DeviceStateTypeDict[item.IdDeviceStateType] = item;
                    DeviceStateTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceStateTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeFilter

        /// <summary>
        /// Gets DeviceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceType funcion</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDeviceTypeClass">Specifies filter for ID_DEVICE_TYPE_CLASS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="TwoWayTransAvailable">Specifies filter for TWO_WAY_TRANS_AVAILABLE column</param>
        /// <param name="IdDeviceDriver">Specifies filter for ID_DEVICE_DRIVER column</param>
        /// <param name="IdProtocol">Specifies filter for ID_PROTOCOL column</param>
        /// <param name="DefaultIdDeviceOderNumber">Specifies filter for DEFAULT_ID_DEVICE_ODER_NUMBER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceType list</returns>
        public virtual List<OpDeviceType> GetDeviceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceType = null, string Name = null, int[] IdDeviceTypeClass = null, long[] IdDescr = null, bool? TwoWayTransAvailable = null, int[] IdDeviceDriver = null,
                            int[] IdProtocol = null, int[] DefaultIdDeviceOderNumber = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeFilter"));
            }

            try
            {
                List<OpDeviceType> returnList = OpDeviceType.ConvertList(dbConnectionCore.GetDeviceTypeFilter(IdDeviceType, Name, IdDeviceTypeClass, IdDescr, TwoWayTransAvailable, IdDeviceDriver,
                             IdProtocol, DefaultIdDeviceOderNumber, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceType item in returnList)
                        DeviceTypeDict[item.IdDeviceType] = item;
                    DeviceTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeClassFilter

        /// <summary>
        /// Gets DeviceTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeClass funcion</param>
        /// <param name="IdDeviceTypeClass">Specifies filter for ID_DEVICE_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeClass list</returns>
        public virtual List<OpDeviceTypeClass> GetDeviceTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeClassFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeClassFilter"));
            }

            try
            {
                List<OpDeviceTypeClass> returnList = OpDeviceTypeClass.ConvertList(dbConnectionCore.GetDeviceTypeClassFilter(IdDeviceTypeClass, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTypeClass item in returnList)
                        DeviceTypeClassDict[item.IdDeviceTypeClass] = item;
                    DeviceTypeClassIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeClassFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeDataFilter

        /// <summary>
        /// Gets DeviceTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeData funcion</param>
        /// <param name="IdDeviceTypeData">Specifies filter for ID_DEVICE_TYPE_DATA column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeData list</returns>
        public virtual List<OpDeviceTypeData> GetDeviceTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDeviceTypeData = null, int[] IdDeviceType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeDataFilter"));
            }

            try
            {
                List<OpDeviceTypeData> returnList = OpDeviceTypeData.ConvertList(dbConnectionCore.GetDeviceTypeDataFilter(IdDeviceTypeData, IdDeviceType, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTypeData item in returnList)
                        DeviceTypeDataDict[item.IdDeviceTypeData] = item;
                    DeviceTypeDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeGroupFilter

        /// <summary>
        /// Gets DeviceTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeGroup funcion</param>
        /// <param name="IdDeviceTypeGroup">Specifies filter for ID_DEVICE_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeGroup list</returns>
        public virtual List<OpDeviceTypeGroup> GetDeviceTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeGroup = null, string Name = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeGroupFilter"));
            }

            try
            {
                List<OpDeviceTypeGroup> returnList = OpDeviceTypeGroup.ConvertList(dbConnectionCore.GetDeviceTypeGroupFilter(IdDeviceTypeGroup, Name, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTypeGroup item in returnList)
                        DeviceTypeGroupDict[item.IdDeviceTypeGroup] = item;
                    DeviceTypeGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeProfileMapFilter

        /// <summary>
        /// Gets DeviceTypeProfileMap list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileMap funcion</param>
        /// <param name="IdDeviceTypeProfileMap">Specifies filter for ID_DEVICE_TYPE_PROFILE_MAP column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="IdOperatorCreated">Specifies filter for ID_OPERATOR_CREATED column</param>
        /// <param name="CreatedTime">Specifies filter for CREATED_TIME column</param>
        /// <param name="IdOperatorModified">Specifies filter for ID_OPERATOR_MODIFIED column</param>
        /// <param name="ModifiedTime">Specifies filter for MODIFIED_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileMap list</returns>
        public virtual List<OpDeviceTypeProfileMap> GetDeviceTypeProfileMapFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileMap = null, int[] IdDeviceType = null, int[] IdOperatorCreated = null, TypeDateTimeCode CreatedTime = null, int[] IdOperatorModified = null, TypeDateTimeCode ModifiedTime = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeProfileMapFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileMapFilter"));
            }

            try
            {
                List<OpDeviceTypeProfileMap> returnList = OpDeviceTypeProfileMap.ConvertList(dbConnectionCore.GetDeviceTypeProfileMapFilter(IdDeviceTypeProfileMap, IdDeviceType, IdOperatorCreated, CreatedTime, IdOperatorModified, ModifiedTime,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTypeProfileMap item in returnList)
                        DeviceTypeProfileMapDict[item.IdDeviceTypeProfileMap] = item;
                    DeviceTypeProfileMapIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeProfileMapFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeProfileMapConfigFilter

        /// <summary>
        /// Gets DeviceTypeProfileMapConfig list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileMapConfig funcion</param>
        /// <param name="IdDeviceTypeProfileMap">Specifies filter for ID_DEVICE_TYPE_PROFILE_MAP column</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="IdDeviceTypeProfileStepKind">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP_KIND column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_MAP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileMapConfig list</returns>
        public virtual List<OpDeviceTypeProfileMapConfig> GetDeviceTypeProfileMapConfigFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileMap = null, int[] IdDeviceTypeProfileStep = null, int[] IdDeviceTypeProfileStepKind = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeProfileMapConfigFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileMapConfigFilter"));
            }

            try
            {
                List<OpDeviceTypeProfileMapConfig> returnList = OpDeviceTypeProfileMapConfig.ConvertList(dbConnectionCore.GetDeviceTypeProfileMapConfigFilter(IdDeviceTypeProfileMap, IdDeviceTypeProfileStep, IdDeviceTypeProfileStepKind, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTypeProfileMapConfig item in returnList)
                        DeviceTypeProfileMapConfigDict[item.IdDeviceTypeProfileMap] = item;
                    DeviceTypeProfileMapConfigIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeProfileMapConfigFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeProfileStepFilter

        /// <summary>
        /// Gets DeviceTypeProfileStep list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileStep funcion</param>
        /// <param name="IdDeviceTypeProfileStep">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_STEP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileStep list</returns>
        public virtual List<OpDeviceTypeProfileStep> GetDeviceTypeProfileStepFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileStep = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeProfileStepFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileStepFilter"));
            }

            try
            {
                List<OpDeviceTypeProfileStep> returnList = OpDeviceTypeProfileStep.ConvertList(dbConnectionCore.GetDeviceTypeProfileStepFilter(IdDeviceTypeProfileStep, Name, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTypeProfileStep item in returnList)
                        DeviceTypeProfileStepDict[item.IdDeviceTypeProfileStep] = item;
                    DeviceTypeProfileStepIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeProfileStepFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceTypeProfileStepKindFilter

        /// <summary>
        /// Gets DeviceTypeProfileStepKind list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceTypeProfileStepKind funcion</param>
        /// <param name="IdDeviceTypeProfileStepKind">Specifies filter for ID_DEVICE_TYPE_PROFILE_STEP_KIND column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_TYPE_PROFILE_STEP_KIND DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceTypeProfileStepKind list</returns>
        public virtual List<OpDeviceTypeProfileStepKind> GetDeviceTypeProfileStepKindFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceTypeProfileStepKind = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceTypeProfileStepKindFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceTypeProfileStepKindFilter"));
            }

            try
            {
                List<OpDeviceTypeProfileStepKind> returnList = OpDeviceTypeProfileStepKind.ConvertList(dbConnectionCore.GetDeviceTypeProfileStepKindFilter(IdDeviceTypeProfileStepKind, Name, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceTypeProfileStepKind item in returnList)
                        DeviceTypeProfileStepKindDict[item.IdDeviceTypeProfileStepKind] = item;
                    DeviceTypeProfileStepKindIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceTypeProfileStepKindFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDeviceWarrantyFilter

        /// <summary>
        /// Gets DeviceWarranty list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDeviceWarranty funcion</param>
        /// <param name="IdDeviceWarranty">Specifies filter for ID_DEVICE_WARRANTY column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdComponent">Specifies filter for ID_COMPONENT column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="WarrantyLength">Specifies filter for WARRANTY_LENGTH column</param>
        /// <param name="ShippingDate">Specifies filter for SHIPPING_DATE column</param>
        /// <param name="InstallationDate">Specifies filter for INSTALLATION_DATE column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_WARRANTY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DeviceWarranty list</returns>
        public virtual List<OpDeviceWarranty> GetDeviceWarrantyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceWarranty = null, int[] IdShippingList = null, long[] SerialNbr = null, int[] IdComponent = null, int[] IdContract = null, int[] WarrantyLength = null,
                            TypeDateTimeCode ShippingDate = null, TypeDateTimeCode InstallationDate = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDeviceWarrantyFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDeviceWarrantyFilter"));
            }

            try
            {
                List<OpDeviceWarranty> returnList = OpDeviceWarranty.ConvertList(dbConnectionCore.GetDeviceWarrantyFilter(IdDeviceWarranty, IdShippingList, SerialNbr, IdComponent, IdContract, WarrantyLength,
                             ShippingDate, InstallationDate, StartDate, EndDate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDeviceWarranty item in returnList)
                        DeviceWarrantyDict[item.IdDeviceWarranty] = item;
                    DeviceWarrantyIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDeviceWarrantyFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDiagnosticActionFilter

        /// <summary>
        /// Gets DiagnosticAction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticAction funcion</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DIAGNOSTIC_ACTION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticAction list</returns>
        public virtual List<OpDiagnosticAction> GetDiagnosticActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDiagnosticAction = null, int[] IdDeviceOrderNumber = null, int[] IdDistributor = null, string Name = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDiagnosticActionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionFilter"));
            }

            try
            {
                List<OpDiagnosticAction> returnList = OpDiagnosticAction.ConvertList(dbConnectionCore.GetDiagnosticActionFilter(IdDiagnosticAction, IdDeviceOrderNumber, IdDistributor, Name, StartDate, EndDate,
                             IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDiagnosticAction item in returnList)
                        DiagnosticActionDict[item.IdDiagnosticAction] = item;
                    DiagnosticActionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDiagnosticActionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDiagnosticActionDataFilter

        /// <summary>
        /// Gets DiagnosticActionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticActionData funcion</param>
        /// <param name="IdDiagnosticActionData">Specifies filter for ID_DIAGNOSTIC_ACTION_DATA column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DIAGNOSTIC_ACTION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticActionData list</returns>
        public virtual List<OpDiagnosticActionData> GetDiagnosticActionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDiagnosticActionData = null, int[] IdDiagnosticAction = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDiagnosticActionDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionDataFilter"));
            }

            try
            {
                List<OpDiagnosticActionData> returnList = OpDiagnosticActionData.ConvertList(dbConnectionCore.GetDiagnosticActionDataFilter(IdDiagnosticActionData, IdDiagnosticAction, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDiagnosticActionData item in returnList)
                        DiagnosticActionDataDict[item.IdDiagnosticActionData] = item;
                    DiagnosticActionDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDiagnosticActionDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDiagnosticActionInServiceFilter

        /// <summary>
        /// Gets DiagnosticActionInService list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticActionInService funcion</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticActionInService list</returns>
        public virtual List<OpDiagnosticActionInService> GetDiagnosticActionInServiceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdService = null, int[] IdDiagnosticAction = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDiagnosticActionInServiceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionInServiceFilter"));
            }

            try
            {
                List<OpDiagnosticActionInService> returnList = OpDiagnosticActionInService.ConvertList(dbConnectionCore.GetDiagnosticActionInServiceFilter(IdService, IdDiagnosticAction, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDiagnosticActionInService item in returnList)
                        DiagnosticActionInServiceDict[item.IdService] = item;
                    DiagnosticActionInServiceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDiagnosticActionInServiceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDiagnosticActionResultFilter

        /// <summary>
        /// Gets DiagnosticActionResult list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDiagnosticActionResult funcion</param>
        /// <param name="IdDiagnosticActionResult">Specifies filter for ID_DIAGNOSTIC_ACTION_RESULT column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="AllowInputText">Specifies filter for ALLOW_INPUT_TEXT column</param>
        /// <param name="DamageLevel">Specifies filter for DAMAGE_LEVEL column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DIAGNOSTIC_ACTION_RESULT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DiagnosticActionResult list</returns>
        public virtual List<OpDiagnosticActionResult> GetDiagnosticActionResultFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDiagnosticActionResult = null, int[] IdDiagnosticAction = null, string Name = null, bool? AllowInputText = null, int[] DamageLevel = null, long[] IdDescr = null,
                            int[] IdServiceReferenceType = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDiagnosticActionResultFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDiagnosticActionResultFilter"));
            }

            try
            {
                List<OpDiagnosticActionResult> returnList = OpDiagnosticActionResult.ConvertList(dbConnectionCore.GetDiagnosticActionResultFilter(IdDiagnosticActionResult, IdDiagnosticAction, Name, AllowInputText, DamageLevel, IdDescr,
                             IdServiceReferenceType, StartDate, EndDate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDiagnosticActionResult item in returnList)
                        DiagnosticActionResultDict[item.IdDiagnosticActionResult] = item;
                    DiagnosticActionResultIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDiagnosticActionResultFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDistributorFilter

        /// <summary>
        /// Gets Distributor list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributor funcion</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="City">Specifies filter for CITY column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Distributor list</returns>
        public virtual List<OpDistributor> GetDistributorFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, 
            int[] IdDistributor = null, string Name = null, string City = null, string Address = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null,
            bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDistributorFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorFilter"));
            }

            try
            {
                List<OpDistributor> returnList = OpDistributor.ConvertList(dbConnectionCore.GetDistributorFilter(IdDistributor, Name, City, Address, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpDistributor item in returnList)
                        DistributorDict[item.IdDistributor] = item;
                    DistributorIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDistributorDataFilter

        /// <summary>
        /// Gets DistributorData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorData funcion</param>
        /// <param name="IdDistributorData">Specifies filter for ID_DISTRIBUTOR_DATA column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorData list</returns>
        public virtual List<OpDistributorData> GetDistributorDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdDistributorData = null, int[] IdDistributor = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDistributorDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorDataFilter"));
            }

            try
            {
                List<OpDistributorData> returnList = OpDistributorData.ConvertList(dbConnectionCore.GetDistributorDataFilter(IdDistributorData, IdDistributor, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDistributorData item in returnList)
                        DistributorDataDict[item.IdDistributorData] = item;
                    DistributorDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDistributorHierarchyFilter

        /// <summary>
        /// Gets DistributorHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorHierarchy funcion</param>
        /// <param name="IdDistributorHierarchy">Specifies filter for ID_DISTRIBUTOR_HIERARCHY column</param>
        /// <param name="IdDistributorHierarchyParent">Specifies filter for ID_DISTRIBUTOR_HIERARCHY_PARENT column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorHierarchy list</returns>
        public virtual List<OpDistributorHierarchy> GetDistributorHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDistributorHierarchy = null, int[] IdDistributorHierarchyParent = null, int[] IdDistributor = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDistributorHierarchyFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorHierarchyFilter"));
            }

            try
            {
                List<OpDistributorHierarchy> returnList = OpDistributorHierarchy.ConvertList(dbConnectionCore.GetDistributorHierarchyFilter(IdDistributorHierarchy, IdDistributorHierarchyParent, IdDistributor, Hierarchy, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDistributorHierarchy item in returnList)
                        DistributorHierarchyDict[item.IdDistributorHierarchy] = item;
                    DistributorHierarchyIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorHierarchyFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetDistributorSupplierFilter

        /// <summary>
        /// Gets DistributorSupplier list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllDistributorSupplier funcion</param>
        /// <param name="IdDistributorSupplier">Specifies filter for ID_DISTRIBUTOR_SUPPLIER column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdSupplier">Specifies filter for ID_SUPPLIER column</param>
        /// <param name="IdProductCode">Specifies filter for ID_PRODUCT_CODE column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DISTRIBUTOR_SUPPLIER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>DistributorSupplier list</returns>
        public virtual List<OpDistributorSupplier> GetDistributorSupplierFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDistributorSupplier = null, int[] IdDistributor = null, int[] IdSupplier = null, int[] IdProductCode = null, int[] IdActor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetDistributorSupplierFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetDistributorSupplierFilter"));
            }

            try
            {
                List<OpDistributorSupplier> returnList = OpDistributorSupplier.ConvertList(dbConnectionCore.GetDistributorSupplierFilter(IdDistributorSupplier, IdDistributor, IdSupplier, IdProductCode, IdActor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpDistributorSupplier item in returnList)
                        DistributorSupplierDict[item.IdDistributorSupplier] = item;
                    DistributorSupplierIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetDistributorSupplierFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetErrorLogFilter

        /// <summary>
        /// Gets ErrorLog list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllErrorLog funcion</param>
        /// <param name="IdErrorLog">Specifies filter for ID_ERROR_LOG column</param>
        /// <param name="ApplicationId">Specifies filter for APPLICATION_ID column</param>
        /// <param name="ErrorDate">Specifies filter for ERROR_DATE column</param>
        /// <param name="ClassName">Specifies filter for CLASS_NAME column</param>
        /// <param name="FunctionName">Specifies filter for FUNCTION_NAME column</param>
        /// <param name="Source">Specifies filter for SOURCE column</param>
        /// <param name="Message">Specifies filter for MESSAGE column</param>
        /// <param name="StackTrace">Specifies filter for STACK_TRACE column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="Severity">Specifies filter for SEVERITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ERROR_LOG DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ErrorLog list</returns>
        public virtual List<OpErrorLog> GetErrorLogFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdErrorLog = null, int[] ApplicationId = null, TypeDateTimeCode ErrorDate = null, string ClassName = null, string FunctionName = null, string Source = null,
                            string Message = null, string StackTrace = null, string Description = null, int[] Severity = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetErrorLogFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetErrorLogFilter"));
            }

            try
            {
                List<OpErrorLog> returnList = OpErrorLog.ConvertList(dbConnectionCore.GetErrorLogFilter(IdErrorLog, ApplicationId, ErrorDate, ClassName, FunctionName, Source,
                             Message, StackTrace, Description, Severity, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpErrorLog item in returnList)
                        ErrorLogDict[item.IdErrorLog] = item;
                    ErrorLogIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetErrorLogFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetEtlFilter

        /// <summary>
        /// Gets Etl list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllEtl funcion</param>
        /// <param name="IdParam">Specifies filter for ID_PARAM column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PARAM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Etl list</returns>
        public virtual List<OpEtl> GetEtlFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdParam = null, string Code = null, string Descr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetEtlFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetEtlFilter"));
            }

            try
            {
                List<OpEtl> returnList = OpEtl.ConvertList(dbConnectionCore.GetEtlFilter(IdParam, Code, Descr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpEtl item in returnList)
                        EtlDict[item.IdParam] = item;
                    EtlIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetEtlFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetFaultCodeFilter

        /// <summary>
        /// Gets FaultCode list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCode funcion</param>
        /// <param name="IdFaultCode">Specifies filter for ID_FAULT_CODE column</param>
        /// <param name="IdFaultCodeType">Specifies filter for ID_FAULT_CODE_TYPE column</param>
        /// <param name="IdFaultCodeParent">Specifies filter for ID_FAULT_CODE_PARENT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdClientDescr">Specifies filter for ID_CLIENT_DESCR column</param>
        /// <param name="IdCompanyDescr">Specifies filter for ID_COMPANY_DESCR column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="FaultCodeHierarchy">Specifies filter for FAULT_CODE_HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCode list</returns>
        public virtual List<OpFaultCode> GetFaultCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdFaultCode = null, int[] IdFaultCodeType = null, int[] IdFaultCodeParent = null, string Name = null, long[] IdClientDescr = null, long[] IdCompanyDescr = null,
                            int[] Priority = null, string FaultCodeHierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetFaultCodeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeFilter"));
            }

            try
            {
                List<OpFaultCode> returnList = OpFaultCode.ConvertList(dbConnectionCore.GetFaultCodeFilter(IdFaultCode, IdFaultCodeType, IdFaultCodeParent, Name, IdClientDescr, IdCompanyDescr,
                             Priority, FaultCodeHierarchy, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpFaultCode item in returnList)
                        FaultCodeDict[item.IdFaultCode] = item;
                    FaultCodeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetFaultCodeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetFaultCodeDataFilter

        /// <summary>
        /// Gets FaultCodeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCodeData funcion</param>
        /// <param name="IdFaultCodeData">Specifies filter for ID_FAULT_CODE_DATA column</param>
        /// <param name="IdFaultCode">Specifies filter for ID_FAULT_CODE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCodeData list</returns>
        public virtual List<OpFaultCodeData> GetFaultCodeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdFaultCodeData = null, int[] IdFaultCode = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetFaultCodeDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeDataFilter"));
            }

            try
            {
                List<OpFaultCodeData> returnList = OpFaultCodeData.ConvertList(dbConnectionCore.GetFaultCodeDataFilter(IdFaultCodeData, IdFaultCode, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpFaultCodeData item in returnList)
                        FaultCodeDataDict[item.IdFaultCodeData] = item;
                    FaultCodeDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetFaultCodeDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetFaultCodeSummaryFilter

        /// <summary>
        /// Gets FaultCodeSummary list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCodeSummary funcion</param>
        /// <param name="IdFaultCodeSummary">Specifies filter for ID_FAULT_CODE_SUMMARY column</param>
        /// <param name="IdFaultCode">Specifies filter for ID_FAULT_CODE column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="ReferenceValue">Specifies filter for REFERENCE_VALUE column</param>
        /// <param name="FaultsCount">Specifies filter for FAULTS_COUNT column</param>
        /// <param name="TotalCount">Specifies filter for TOTAL_COUNT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE_SUMMARY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCodeSummary list</returns>
        public virtual List<OpFaultCodeSummary> GetFaultCodeSummaryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdFaultCodeSummary = null, int[] IdFaultCode = null, int[] IdServiceReferenceType = null, int[] FaultsCount = null, int[] TotalCount = null, int[] IdFaultCodeSummaryType = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetFaultCodeSummaryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeSummaryFilter"));
            }

            try
            {
                List<OpFaultCodeSummary> returnList = OpFaultCodeSummary.ConvertList(dbConnectionCore.GetFaultCodeSummaryFilter(IdFaultCodeSummary, IdFaultCode, IdServiceReferenceType, FaultsCount, TotalCount, IdFaultCodeSummaryType,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpFaultCodeSummary item in returnList)
                        FaultCodeSummaryDict[item.IdFaultCodeSummary] = item;
                    FaultCodeSummaryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetFaultCodeSummaryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetFaultCodeTypeFilter

        /// <summary>
        /// Gets FaultCodeType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllFaultCodeType funcion</param>
        /// <param name="IdFaultCodeType">Specifies filter for ID_FAULT_CODE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_FAULT_CODE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>FaultCodeType list</returns>
        public virtual List<OpFaultCodeType> GetFaultCodeTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdFaultCodeType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetFaultCodeTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetFaultCodeTypeFilter"));
            }

            try
            {
                List<OpFaultCodeType> returnList = OpFaultCodeType.ConvertList(dbConnectionCore.GetFaultCodeTypeFilter(IdFaultCodeType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpFaultCodeType item in returnList)
                        FaultCodeTypeDict[item.IdFaultCodeType] = item;
                    FaultCodeTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetFaultCodeTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetHolidayDateFilter

        /// <summary>
        /// Gets HolidayDate list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllHolidayDate funcion</param>
        /// <param name="Date">Specifies filter for DATE column</param>
        /// <param name="CountryCode">Specifies filter for COUNTRY_CODE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY DATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>HolidayDate list</returns>
        public virtual List<OpHolidayDate> GetHolidayDateFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, TypeDateTimeCode Date = null, string CountryCode = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetHolidayDateFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetHolidayDateFilter"));
            }

            try
            {
                List<OpHolidayDate> returnList = OpHolidayDate.ConvertList(dbConnectionCore.GetHolidayDateFilter(Date, CountryCode, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpHolidayDate item in returnList)
                        HolidayDateDict[item.Date] = item;
                    HolidayDateIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetHolidayDateFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetImrServerFilter

        /// <summary>
        /// Gets ImrServer list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllImrServer funcion</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="IpAddress">Specifies filter for IP_ADDRESS column</param>
        /// <param name="DnsName">Specifies filter for DNS_NAME column</param>
        /// <param name="WebserviceAddress">Specifies filter for WEBSERVICE_ADDRESS column</param>
        /// <param name="WebserviceTimeout">Specifies filter for WEBSERVICE_TIMEOUT column</param>
        /// <param name="Login">Specifies filter for LOGIN column</param>
        /// <param name="Password">Specifies filter for PASSWORD column</param>
        /// <param name="StandardDescription">Specifies filter for STANDARD_DESCRIPTION column</param>
        /// <param name="IsGood">Specifies filter for IS_GOOD column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="UtdcSsecUser">Specifies filter for UTDC_SSEC_USER column</param>
        /// <param name="UtdcSsecPass">Specifies filter for UTDC_SSEC_PASS column</param>
        /// <param name="Collation">Specifies filter for COLLATION column</param>
        /// <param name="DbName">Specifies filter for DB_NAME column</param>
        /// <param name="ImrServerVersion">Specifies filter for IMR_SERVER_VERSION column</param>
        /// <param name="IsImrlt">Specifies filter for IS_IMRLT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_IMR_SERVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ImrServer list</returns>
        public virtual List<OpImrServer> GetImrServerFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdImrServer = null, string IpAddress = null, string DnsName = null, string WebserviceAddress = null, int[] WebserviceTimeout = null, string Login = null,
                            string Password = null, string StandardDescription = null, bool? IsGood = null, TypeDateTimeCode CreationDate = null, string UtdcSsecUser = null,
                            string UtdcSsecPass = null, string Collation = null, string DbName = null, int[] ImrServerVersion = null, bool? IsImrlt = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetImrServerFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetImrServerFilter"));
            }

            try
            {
                List<OpImrServer> returnList = OpImrServer.ConvertList(dbConnectionCore.GetImrServerFilter(IdImrServer, IpAddress, DnsName, WebserviceAddress, WebserviceTimeout, Login,
                             Password, StandardDescription, IsGood, CreationDate, UtdcSsecUser,
                             UtdcSsecPass, Collation, DbName, ImrServerVersion, IsImrlt,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpImrServer item in returnList)
                        ImrServerDict[item.IdImrServer] = item;
                    ImrServerIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetImrServerFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetImrServerDataFilter

        /// <summary>
        /// Gets ImrServerData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllImrServerData funcion</param>
        /// <param name="IdImrServerData">Specifies filter for ID_IMR_SERVER_DATA column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_IMR_SERVER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ImrServerData list</returns>
        public virtual List<OpImrServerData> GetImrServerDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdImrServerData = null, int[] IdImrServer = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetImrServerDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetImrServerDataFilter"));
            }

            try
            {
                List<OpImrServerData> returnList = OpImrServerData.ConvertList(dbConnectionCore.GetImrServerDataFilter(IdImrServerData, IdImrServer, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpImrServerData item in returnList)
                        ImrServerDataDict[item.IdImrServerData] = item;
                    ImrServerDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetImrServerDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetIssueFilter

        /// <summary>
        /// Gets Issue list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssue funcion</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdOperatorRegistering">Specifies filter for ID_OPERATOR_REGISTERING column</param>
        /// <param name="IdOperatorPerformer">Specifies filter for ID_OPERATOR_PERFORMER column</param>
        /// <param name="IdIssueType">Specifies filter for ID_ISSUE_TYPE column</param>
        /// <param name="IdIssueStatus">Specifies filter for ID_ISSUE_STATUS column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="RealizationDate">Specifies filter for REALIZATION_DATE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ShortDescr">Specifies filter for SHORT_DESCR column</param>
        /// <param name="LongDescr">Specifies filter for LONG_DESCR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Issue list</returns>
        public virtual List<OpIssue> GetIssueFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssue = null, int[] IdActor = null, int[] IdOperatorRegistering = null, int[] IdOperatorPerformer = null, int[] IdIssueType = null, int[] IdIssueStatus = null,
                            int[] IdDistributor = null, TypeDateTimeCode RealizationDate = null, TypeDateTimeCode CreationDate = null, string ShortDescr = null, string LongDescr = null,
                            long[] IdLocation = null, TypeDateTimeCode Deadline = null, int[] IdPriority = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadIssueHistory = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetIssueFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueFilter"));
            }

            try
            {
                List<OpIssue> returnList = OpIssue.ConvertList(dbConnectionCore.GetIssueFilter(IdIssue, IdActor, IdOperatorRegistering, IdOperatorPerformer, IdIssueType, IdIssueStatus,
                             IdDistributor, RealizationDate, CreationDate, ShortDescr, LongDescr,
                             IdLocation, Deadline, IdPriority, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadIssueHistory, true, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpIssue item in returnList)
                        IssueDict[item.IdIssue] = item;
                    IssueIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetIssueFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetIssueDataFilter

        /// <summary>
        /// Gets IssueData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueData funcion</param>
        /// <param name="IdIssueData">Specifies filter for ID_ISSUE_DATA column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueData list</returns>
        public virtual List<OpIssueData> GetIssueDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdIssueData = null, int[] IdIssue = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetIssueDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueDataFilter"));
            }

            try
            {
                List<OpIssueData> returnList = OpIssueData.ConvertList(dbConnectionCore.GetIssueDataFilter(IdIssueData, IdIssue, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpIssueData item in returnList)
                        IssueDataDict[item.IdIssueData] = item;
                    IssueDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetIssueDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetIssueGroupFilter

        /// <summary>
        /// Gets IssueGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueGroup funcion</param>
        /// <param name="IdIssueGroup">Specifies filter for ID_ISSUE_GROUP column</param>
        /// <param name="IdParentGroup">Specifies filter for ID_PARENT_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="DateCreated">Specifies filter for DATE_CREATED column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueGroup list</returns>
        public List<OpIssueGroup> GetIssueGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueGroup = null, int[] IdParentGroup = null, string Name = null, TypeDateTimeCode DateCreated = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetIssueGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueGroupFilter"));
            }

            try
            {
                List<OpIssueGroup> returnList = OpIssueGroup.ConvertList(dbConnectionCore.GetIssueGroupFilter(IdIssueGroup, IdParentGroup, Name, DateCreated, IdDistributor, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpIssueGroup item in returnList)
                        IssueGroupDict[item.IdIssueGroup] = item;
                    IssueGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetIssueGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetIssueHistoryFilter

        /// <summary>
        /// Gets IssueHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueHistory funcion</param>
        /// <param name="IdIssueHistory">Specifies filter for ID_ISSUE_HISTORY column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdIssueStatus">Specifies filter for ID_ISSUE_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="ShortDescr">Specifies filter for SHORT_DESCR column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueHistory list</returns>
        public virtual List<OpIssueHistory> GetIssueHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueHistory = null, int[] IdIssue = null, int[] IdIssueStatus = null, int[] IdOperator = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            string Notes = null, string ShortDescr = null, TypeDateTimeCode Deadline = null, int[] IdPriority = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetIssueHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueHistoryFilter"));
            }

            try
            {
                List<OpIssueHistory> returnList = OpIssueHistory.ConvertList(dbConnectionCore.GetIssueHistoryFilter(IdIssueHistory, IdIssue, IdIssueStatus, IdOperator, StartDate, EndDate,
                             Notes, ShortDescr, Deadline, IdPriority, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpIssueHistory item in returnList)
                        IssueHistoryDict[item.IdIssueHistory] = item;
                    IssueHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetIssueHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetIssueStatusFilter

        /// <summary>
        /// Gets IssueStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueStatus funcion</param>
        /// <param name="IdIssueStatus">Specifies filter for ID_ISSUE_STATUS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueStatus list</returns>
        public virtual List<OpIssueStatus> GetIssueStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueStatus = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetIssueStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueStatusFilter"));
            }

            try
            {
                List<OpIssueStatus> returnList = OpIssueStatus.ConvertList(dbConnectionCore.GetIssueStatusFilter(IdIssueStatus, IdDescr, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpIssueStatus item in returnList)
                        IssueStatusDict[item.IdIssueStatus] = item;
                    IssueStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetIssueStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetIssueTypeFilter

        /// <summary>
        /// Gets IssueType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllIssueType funcion</param>
        /// <param name="IdIssueType">Specifies filter for ID_ISSUE_TYPE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ISSUE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>IssueType list</returns>
        public virtual List<OpIssueType> GetIssueTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdIssueType = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetIssueTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetIssueTypeFilter"));
            }

            try
            {
                List<OpIssueType> returnList = OpIssueType.ConvertList(dbConnectionCore.GetIssueTypeFilter(IdIssueType, IdDescr, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpIssueType item in returnList)
                        IssueTypeDict[item.IdIssueType] = item;
                    IssueTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetIssueTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLanguageDataFilter

        /// <summary>
        /// Gets LanguageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLanguageData funcion</param>
        /// <param name="IdLanguageData">Specifies filter for ID_LANGUAGE_DATA column</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LANGUAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LanguageData list</returns>
        public virtual List<OpLanguageData> GetLanguageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLanguageData = null, int[] IdLanguage = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLanguageDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLanguageDataFilter"));
            }

            try
            {
                List<OpLanguageData> returnList = OpLanguageData.ConvertList(dbConnectionCore.GetLanguageDataFilter(IdLanguageData, IdLanguage, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpLanguageData item in returnList)
                        LanguageDataDict[item.IdLanguageData] = item;
                    LanguageDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLanguageDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLanguageFilter

        /// <summary>
        /// Gets Language list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLanguage funcion</param>
        /// <param name="IdLanguage">Specifies filter for ID_LANGUAGE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CultureCode">Specifies filter for CULTURE_CODE column</param>
        /// <param name="CultureLangid">Specifies filter for CULTURE_LANGID column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LANGUAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Language list</returns>
        public virtual List<OpLanguage> GetLanguageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLanguage = null, string Name = null, string CultureCode = null, int[] CultureLangid = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLanguageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLanguageFilter"));
            }

            try
            {
                List<OpLanguage> returnList = OpLanguage.ConvertList(dbConnectionCore.GetLanguageFilter(IdLanguage, Name, CultureCode, CultureLangid, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpLanguage item in returnList)
                        LanguageDict[item.IdLanguage] = item;
                    LanguageIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLanguageFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationConsumerHistoryFilter

        /// <summary>
        /// Gets LocationConsumerHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationConsumerHistory funcion</param>
        /// <param name="IdLocationConsumerHistory">Specifies filter for ID_LOCATION_CONSUMER_HISTORY column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdConsumer">Specifies filter for ID_CONSUMER column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_CONSUMER_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationConsumerHistory list</returns>
        public virtual List<OpLocationConsumerHistory> GetLocationConsumerHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationConsumerHistory = null, long[] IdLocation = null, int[] IdConsumer = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationConsumerHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationConsumerHistoryFilter"));
            }

            try
            {
                List<OpLocationConsumerHistory> returnList = OpLocationConsumerHistory.ConvertList(dbConnectionCore.GetLocationConsumerHistoryFilter(IdLocationConsumerHistory, IdLocation, IdConsumer, StartTime, EndTime, Notes,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationConsumerHistory item in returnList)
                        LocationConsumerHistoryDict[item.IdLocationConsumerHistory] = item;
                    LocationConsumerHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationConsumerHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationDetailsFilter

        /// <summary>
        /// Gets LocationDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationDetails funcion</param>
        /// <param name="IdLocationDetails">Specifies filter for ID_LOCATION_DETAILS column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="Cid">Specifies filter for CID column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="City">Specifies filter for CITY column</param>
        /// <param name="Address">Specifies filter for ADDRESS column</param>
        /// <param name="Zip">Specifies filter for ZIP column</param>
        /// <param name="Country">Specifies filter for COUNTRY column</param>
        /// <param name="Latitude">Specifies filter for LATITUDE column</param>
        /// <param name="Longitude">Specifies filter for LONGITUDE column</param>
        /// <param name="ImrServer">Specifies filter for IMR_SERVER column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="LocationMemo">Specifies filter for LOCATION_MEMO column</param>
        /// <param name="LocationSourceServerId">Specifies filter for LOCATION_SOURCE_SERVER_ID column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdLocationType">Specifies filter for ID_LOCATION_TYPE column</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="InKpi">Specifies filter for IN_KPI column</param>
        /// <param name="AllowGrouping">Specifies filter for ALLOW_GROUPING column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <returns>LocationDetails list</returns>
        public virtual List<OpLocationDetails> GetLocationDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationDetails = null, long[] IdLocation = null, string Cid = null, string Name = null, string City = null, string Address = null,
                            string Zip = null, string Country = null, string Latitude = null, string Longitude = null, int[] ImrServer = null,
                            TypeDateTimeCode CreationDate = null, string LocationMemo = null, long[] LocationSourceServerId = null, int[] IdDistributor = null, int[] IdLocationType = null,
                            int[] IdLocationStateType = null, bool? InKpi = null, bool? AllowGrouping = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadCustomData = true)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationDetailsFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationDetailsFilter"));
            }

            try
            {
                List<OpLocationDetails> returnList = OpLocationDetails.ConvertList(dbConnectionCore.GetLocationDetailsFilter(IdLocationDetails, IdLocation, Cid, Name, City, Address,
                             Zip, Country, Latitude, Longitude, ImrServer,
                             CreationDate, LocationMemo, LocationSourceServerId, IdDistributor, IdLocationType,
                             IdLocationStateType, InKpi, AllowGrouping, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData);
                if (mergeIntoCache)
                {
                    foreach (OpLocationDetails item in returnList)
                        LocationDetailsDict[item.IdLocationDetails] = item;
                    LocationDetailsIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationDetailsFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationHierarchyFilter

        /// <summary>
        /// Gets LocationHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationHierarchy funcion</param>
        /// <param name="IdLocationHierarchy">Specifies filter for ID_LOCATION_HIERARCHY column</param>
        /// <param name="IdLocationHierarchyParent">Specifies filter for ID_LOCATION_HIERARCHY_PARENT column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationHierarchy list</returns>
        public virtual List<OpLocationHierarchy> GetLocationHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationHierarchy = null, long[] IdLocationHierarchyParent = null, long[] IdLocation = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationHierarchyFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationHierarchyFilter"));
            }

            try
            {
                List<OpLocationHierarchy> returnList = OpLocationHierarchy.ConvertList(dbConnectionCore.GetLocationHierarchyFilter(IdLocationHierarchy, IdLocationHierarchyParent, IdLocation, Hierarchy, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpLocationHierarchy item in returnList)
                        LocationHierarchyDict[item.IdLocationHierarchy] = item;
                    LocationHierarchyIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationHierarchyFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationSealFilter

        /// <summary>
        /// Gets LocationSeal list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationSeal funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdSeal">Specifies filter for ID_SEAL column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationSeal list</returns>
        public virtual List<OpLocationSeal> GetLocationSealFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocation = null, long[] IdSeal = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationSealFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationSealFilter"));
            }

            try
            {
                List<OpLocationSeal> returnList = OpLocationSeal.ConvertList(dbConnectionCore.GetLocationSealFilter(IdLocation, IdSeal, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationSeal item in returnList)
                        LocationSealDict[item.IdLocation] = item;
                    LocationSealIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationSealFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationStateHistoryFilter

        /// <summary>
        /// Gets LocationStateHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationStateHistory funcion</param>
        /// <param name="IdLocationState">Specifies filter for ID_LOCATION_STATE column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="InKpi">Specifies filter for IN_KPI column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_STATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationStateHistory list</returns>
        public virtual List<OpLocationStateHistory> GetLocationStateHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocationState = null, long[] IdLocation = null, int[] IdLocationStateType = null, bool? InKpi = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationStateHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationStateHistoryFilter"));
            }

            try
            {
                List<OpLocationStateHistory> returnList = OpLocationStateHistory.ConvertList(dbConnectionCore.GetLocationStateHistoryFilter(IdLocationState, IdLocation, IdLocationStateType, InKpi, StartDate, EndDate,
                             Notes, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationStateHistory item in returnList)
                        LocationStateHistoryDict[item.IdLocationState] = item;
                    LocationStateHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationStateHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationStateTypeFilter

        /// <summary>
        /// Gets LocationStateType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationStateType funcion</param>
        /// <param name="IdLocationStateType">Specifies filter for ID_LOCATION_STATE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_STATE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationStateType list</returns>
        public virtual List<OpLocationStateType> GetLocationStateTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationStateType = null, string Name = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationStateTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationStateTypeFilter"));
            }

            try
            {
                List<OpLocationStateType> returnList = OpLocationStateType.ConvertList(dbConnectionCore.GetLocationStateTypeFilter(IdLocationStateType, Name, IdDescr, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpLocationStateType item in returnList)
                        LocationStateTypeDict[item.IdLocationStateType] = item;
                    LocationStateTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationStateTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationSupplierFilter

        /// <summary>
        /// Gets LocationSupplier list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationSupplier funcion</param>
        /// <param name="IdLocationSupplier">Specifies filter for ID_LOCATION_SUPPLIER column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdDistributorSupplier">Specifies filter for ID_DISTRIBUTOR_SUPPLIER column</param>
        /// <param name="AccountNo">Specifies filter for ACCOUNT_NO column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_SUPPLIER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationSupplier list</returns>
        public virtual List<OpLocationSupplier> GetLocationSupplierFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationSupplier = null, long[] IdLocation = null, int[] IdDistributorSupplier = null, int[] AccountNo = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                            string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationSupplierFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationSupplierFilter"));
            }

            try
            {
                List<OpLocationSupplier> returnList = OpLocationSupplier.ConvertList(dbConnectionCore.GetLocationSupplierFilter(IdLocationSupplier, IdLocation, IdDistributorSupplier, AccountNo, StartTime, EndTime,
                             Notes, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationSupplier item in returnList)
                        LocationSupplierDict[item.IdLocationSupplier] = item;
                    LocationSupplierIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationSupplierFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationTariffFilter

        /// <summary>
        /// Gets LocationTariff list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationTariff funcion</param>
        /// <param name="IdLocationTariff">Specifies filter for ID_LOCATION_TARIFF column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_TARIFF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationTariff list</returns>
        public virtual List<OpLocationTariff> GetLocationTariffFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationTariff = null, long[] IdLocation = null, int[] IdTariff = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, string Notes = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationTariffFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationTariffFilter"));
            }

            try
            {
                List<OpLocationTariff> returnList = OpLocationTariff.ConvertList(dbConnectionCore.GetLocationTariffFilter(IdLocationTariff, IdLocation, IdTariff, StartTime, EndTime, Notes,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationTariff item in returnList)
                        LocationTariffDict[item.IdLocationTariff] = item;
                    LocationTariffIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationTariffFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationTypeFilter

        /// <summary>
        /// Gets LocationType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationType funcion</param>
        /// <param name="IdLocationType">Specifies filter for ID_LOCATION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationType list</returns>
        public virtual List<OpLocationType> GetLocationTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdLocationType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationTypeFilter"));
            }

            try
            {
                List<OpLocationType> returnList = OpLocationType.ConvertList(dbConnectionCore.GetLocationTypeFilter(IdLocationType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpLocationType item in returnList)
                        LocationTypeDict[item.IdLocationType] = item;
                    LocationTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationEntranceFilter

        /// <summary>
        /// Gets LocationEntrance list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationEntrance funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="EntranceIndexNbr">Specifies filter for ENTRANCE_INDEX_NBR column</param>
        /// <param name="SectionCount">Specifies filter for SECTION_COUNT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationEntrance list</returns>
        public virtual List<OpLocationEntrance> GetLocationEntranceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocation = null, int[] EntranceIndexNbr = null, int[] SectionCount = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationEntranceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationEntranceFilter"));
            }

            try
            {
                List<OpLocationEntrance> returnList = OpLocationEntrance.ConvertList(dbConnectionCore.GetLocationEntranceFilter(IdLocation, EntranceIndexNbr, SectionCount, Name, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationEntrance item in returnList)
                        LocationEntranceDict[item.IdLocation] = item;
                    LocationEntranceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationEntranceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetLocationEntranceFloorFilter

        /// <summary>
        /// Gets LocationEntranceFloor list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllLocationEntranceFloor funcion</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="EntranceIndexNbr">Specifies filter for ENTRANCE_INDEX_NBR column</param>
        /// <param name="FloorIndexNbr">Specifies filter for FLOOR_INDEX_NBR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>LocationEntranceFloor list</returns>
        public virtual List<OpLocationEntranceFloor> GetLocationEntranceFloorFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdLocation = null, int[] EntranceIndexNbr = null, int[] FloorIndexNbr = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetLocationEntranceFloorFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetLocationEntranceFloorFilter"));
            }

            try
            {
                List<OpLocationEntranceFloor> returnList = OpLocationEntranceFloor.ConvertList(dbConnectionCore.GetLocationEntranceFloorFilter(IdLocation, EntranceIndexNbr, FloorIndexNbr, Name, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpLocationEntranceFloor item in returnList)
                        LocationEntranceFloorDict[item.IdLocation] = item;
                    LocationEntranceFloorIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetLocationEntranceFloorFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMappingFilter

        /// <summary>
        /// Gets Mapping list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMapping funcion</param>
        /// <param name="IdMapping">Specifies filter for ID_MAPPING column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="IdSource">Specifies filter for ID_SOURCE column</param>
        /// <param name="IdDestination">Specifies filter for ID_DESTINATION column</param>
        /// <param name="IdImrServer">Specifies filter for ID_IMR_SERVER column</param>
        /// <param name="Timestamp">Specifies filter for TIMESTAMP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MAPPING DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Mapping list</returns>
        public virtual List<OpMapping> GetMappingFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMapping = null, int[] IdReferenceType = null, long[] IdSource = null, long[] IdDestination = null, int[] IdImrServer = null, TypeDateTimeCode Timestamp = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMappingFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMappingFilter"));
            }

            try
            {
                List<OpMapping> returnList = OpMapping.ConvertList(dbConnectionCore.GetMappingFilter(IdMapping, IdReferenceType, IdSource, IdDestination, IdImrServer, Timestamp,
                             topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMapping item in returnList)
                        MappingDict[item.IdMapping] = item;
                    MappingIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMappingFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMessageFilter

        /// <summary>
        /// Gets Message list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMessage funcion</param>
        /// <param name="IdMessage">Specifies filter for ID_MESSAGE column</param>
        /// <param name="IdMessageType">Specifies filter for ID_MESSAGE_TYPE column</param>
        /// <param name="IdMessageParent">Specifies filter for ID_MESSAGE_PARENT column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="MessageContent">Specifies filter for MESSAGE_CONTENT column</param>
        /// <param name="HasAttachment">Specifies filter for HAS_ATTACHMENT column</param>
        /// <param name="Points">Specifies filter for POINTS column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MESSAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Message list</returns>
        public virtual List<OpMessage> GetMessageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMessage = null, int[] IdMessageType = null, long[] IdMessageParent = null, int[] IdOperator = null, string MessageContent = null, bool? HasAttachment = null,
                            int[] Points = null, string Hierarchy = null, TypeDateTimeCode InsertDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMessageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMessageFilter"));
            }

            try
            {
                List<OpMessage> returnList = OpMessage.ConvertList(dbConnectionCore.GetMessageFilter(IdMessage, IdMessageType, IdMessageParent, IdOperator, MessageContent, HasAttachment,
                             Points, Hierarchy, InsertDate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpMessage item in returnList)
                        MessageDict[item.IdMessage] = item;
                    MessageIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMessageFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMessageDataFilter

        /// <summary>
        /// Gets MessageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMessageData funcion</param>
        /// <param name="IdMessageData">Specifies filter for ID_MESSAGE_DATA column</param>
        /// <param name="IdMessage">Specifies filter for ID_MESSAGE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MESSAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MessageData list</returns>
        public virtual List<OpMessageData> GetMessageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMessageData = null, long[] IdMessage = null, int[] IndexNbr = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMessageDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMessageDataFilter"));
            }

            try
            {
                List<OpMessageData> returnList = OpMessageData.ConvertList(dbConnectionCore.GetMessageDataFilter(IdMessageData, IdMessage, IndexNbr, IdDataType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpMessageData item in returnList)
                        MessageDataDict[item.IdMessageData] = item;
                    MessageDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMessageDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterFilter

        /// <summary>
        /// Gets Meter list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeter funcion</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdMeterPattern">Specifies filter for ID_METER_PATTERN column</param>
        /// <param name="IdDescrPattern">Specifies filter for ID_DESCR_PATTERN column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Meter list</returns>
        public virtual List<OpMeter> GetMeterFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMeter = null, int[] IdMeterType = null, long[] IdMeterPattern = null, long[] IdDescrPattern = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterFilter"));
            }

            try
            {
                List<OpMeter> returnList = OpMeter.ConvertList(dbConnectionCore.GetMeterFilter(IdMeter, IdMeterType, IdMeterPattern, IdDescrPattern, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMeter item in returnList)
                        MeterDict[item.IdMeter] = item;
                    MeterIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterTypeFilter

        /// <summary>
        /// Gets MeterType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterType funcion</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdMeterTypeClass">Specifies filter for ID_METER_TYPE_CLASS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterType list</returns>
        public virtual List<OpMeterType> GetMeterTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterType = null, string Name = null, long[] IdDescr = null, int[] IdMeterTypeClass = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeFilter"));
            }

            try
            {
                List<OpMeterType> returnList = OpMeterType.ConvertList(dbConnectionCore.GetMeterTypeFilter(IdMeterType, Name, IdDescr, IdMeterTypeClass, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMeterType item in returnList)
                        MeterTypeDict[item.IdMeterType] = item;
                    MeterTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterTypeClassFilter

        /// <summary>
        /// Gets MeterTypeClass list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeClass funcion</param>
        /// <param name="IdMeterTypeClass">Specifies filter for ID_METER_TYPE_CLASS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_CLASS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeClass list</returns>
        public virtual List<OpMeterTypeClass> GetMeterTypeClassFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterTypeClass = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterTypeClassFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeClassFilter"));
            }

            try
            {
                List<OpMeterTypeClass> returnList = OpMeterTypeClass.ConvertList(dbConnectionCore.GetMeterTypeClassFilter(IdMeterTypeClass, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMeterTypeClass item in returnList)
                        MeterTypeClassDict[item.IdMeterTypeClass] = item;
                    MeterTypeClassIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeClassFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterTypeDataFilter

        /// <summary>
        /// Gets MeterTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeData funcion</param>
        /// <param name="IdMeterTypeData">Specifies filter for ID_METER_TYPE_DATA column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeData list</returns>
        public virtual List<OpMeterTypeData> GetMeterTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdMeterTypeData = null, int[] IdMeterType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterTypeDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeDataFilter"));
            }

            try
            {
                List<OpMeterTypeData> returnList = OpMeterTypeData.ConvertList(dbConnectionCore.GetMeterTypeDataFilter(IdMeterTypeData, IdMeterType, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpMeterTypeData item in returnList)
                        MeterTypeDataDict[item.IdMeterTypeData] = item;
                    MeterTypeDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterTypeGroupFilter
        /// <summary>
        /// Gets MeterTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeGroup funcion</param>
        /// <param name="IdMeterTypeGroup">Specifies filter for ID_METER_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeGroup list</returns>
        public virtual List<OpMeterTypeGroup> GetMeterTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterTypeGroup = null, string Name = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterTypeGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeGroupFilter"));
            }

            try
            {
                List<OpMeterTypeGroup> returnList = OpMeterTypeGroup.ConvertList(dbConnectionCore.GetMeterTypeGroupFilter(IdMeterTypeGroup, Name, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpMeterTypeGroup item in returnList)
                        MeterTypeGroupDict[item.IdMeterTypeGroup] = item;
                    MeterTypeGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterTypeInGroupFilter
        /// <summary>
        /// Gets MeterTypeInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeInGroup funcion</param>
        /// <param name="IdMeterTypeGroup">Specifies filter for ID_METER_TYPE_GROUP column</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeInGroup list</returns>
        public virtual List<OpMeterTypeInGroup> GetMeterTypeInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterTypeGroup = null, int[] IdMeterType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterTypeInGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeInGroupFilter"));
            }

            try
            {
                List<OpMeterTypeInGroup> returnList = OpMeterTypeInGroup.ConvertList(dbConnectionCore.GetMeterTypeInGroupFilter(IdMeterTypeGroup, IdMeterType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpMeterTypeInGroup item in returnList)
                    {
                        if (!MeterTypeInGroupDict.ContainsKey(item.IdMeterTypeGroup))
                            MeterTypeInGroupDict[item.IdMeterTypeGroup] = new List<OpMeterTypeInGroup>();

                        MeterTypeInGroupDict[item.IdMeterTypeGroup].Add(item);
                    }
                    MeterTypeInGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeInGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMeterTypeDeviceTypeFilter

        /// <summary>
        /// Gets MeterTypeDeviceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMeterTypeDeviceType funcion</param>
        /// <param name="IdMeterType">Specifies filter for ID_METER_TYPE column</param>
        /// <param name="IdDeviceType">Specifies filter for ID_DEVICE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_METER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MeterTypeDeviceType list</returns>
        public virtual List<OpMeterTypeDeviceType> GetMeterTypeDeviceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMeterType = null, int[] IdDeviceType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMeterTypeDeviceTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMeterTypeDeviceTypeFilter"));
            }

            try
            {
                List<OpMeterTypeDeviceType> returnList = OpMeterTypeDeviceType.ConvertList(dbConnectionCore.GetMeterTypeDeviceTypeFilter(IdMeterType, IdDeviceType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpMeterTypeDeviceType item in returnList)
                        MeterTypeDeviceTypeDict[item.IdMeterType] = item;
                    MeterTypeDeviceTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMeterTypeDeviceTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMobileNetworkCodeFilter

        /// <summary>
        /// Gets MobileNetworkCode list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMobileNetworkCode funcion</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="Mmc">Specifies filter for MMC column</param>
        /// <param name="Mnc">Specifies filter for MNC column</param>
        /// <param name="Brand">Specifies filter for BRAND column</param>
        /// <param name="Operator">Specifies filter for OPERATOR column</param>
        /// <param name="Bands">Specifies filter for BANDS column</param>
        /// <param name="SmsServiceCenter">Specifies filter for SMS_SERVICE_CENTER column</param>
        /// <param name="ApnAddress">Specifies filter for APN_ADDRESS column</param>
        /// <param name="ApnLogin">Specifies filter for APN_LOGIN column</param>
        /// <param name="ApnPassword">Specifies filter for APN_PASSWORD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MobileNetworkCode list</returns>
        public virtual List<OpMobileNetworkCode> GetMobileNetworkCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] Code = null, string Mmc = null, string Mnc = null, string Brand = null, string Operator = null, string Bands = null,
                            string SmsServiceCenter = null, string ApnAddress = null, string ApnLogin = null, string ApnPassword = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMobileNetworkCodeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMobileNetworkCodeFilter"));
            }

            try
            {
                List<OpMobileNetworkCode> returnList = OpMobileNetworkCode.ConvertList(dbConnectionCore.GetMobileNetworkCodeFilter(Code, Mmc, Mnc, Brand, Operator, Bands,
                             SmsServiceCenter, ApnAddress, ApnLogin, ApnPassword, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpMobileNetworkCode item in returnList)
                        MobileNetworkCodeDict[item.Code] = item;
                    MobileNetworkCodeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMobileNetworkCodeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetModuleFilter

        /// <summary>
        /// Gets Module list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllModule funcion</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MODULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Module list</returns>
        public virtual List<OpModule> GetModuleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdModule = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetModuleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetModuleFilter"));
            }

            try
            {
                List<OpModule> returnList = OpModule.ConvertList(dbConnectionCore.GetModuleFilter(IdModule, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpModule item in returnList)
                        ModuleDict[item.IdModule] = item;
                    ModuleIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetModuleFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetModuleDataFilter

        /// <summary>
        /// Gets ModuleData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllModuleData funcion</param>
        /// <param name="IdModuleData">Specifies filter for ID_MODULE_DATA column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MODULE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ModuleData list</returns>
        public virtual List<OpModuleData> GetModuleDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdModuleData = null, int[] IdModule = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetModuleDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetModuleDataFilter"));
            }

            try
            {
                List<OpModuleData> returnList = OpModuleData.ConvertList(dbConnectionCore.GetModuleDataFilter(IdModuleData, IdModule, IdDataType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpModuleData item in returnList)
                        ModuleDataDict[item.IdModuleData] = item;
                    ModuleDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetModuleDataFilter", ex);
                throw;
            }

        }
        #endregion        
        #region GetMonitoringWatcherFilter

        /// <summary>
        /// Gets MonitoringWatcher list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcher funcion</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="IsActive">Specifies filter for IS_ACTIVE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcher list</returns>
        public List<OpMonitoringWatcher> GetMonitoringWatcherFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcher = null, int[] IdMonitoringWatcherType = null, bool? IsActive = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMonitoringWatcherFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherFilter"));
            }

            try
            {
                List<OpMonitoringWatcher> returnList = OpMonitoringWatcher.ConvertList(dbConnectionCore.GetMonitoringWatcherFilter(IdMonitoringWatcher, IdMonitoringWatcherType, IsActive, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMonitoringWatcher item in returnList)
                        MonitoringWatcherDict[item.IdMonitoringWatcher] = item;
                    MonitoringWatcherIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherFilter", ex);
                throw;
            }

        }
        #endregion        
        #region GetMonitoringWatcherDataFilter

        /// <summary>
        /// Gets MonitoringWatcherData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherData funcion</param>
        /// <param name="IdMonitoringWatcherData">Specifies filter for ID_MONITORING_WATCHER_DATA column</param>
        /// <param name="IdMonitoringWatcher">Specifies filter for ID_MONITORING_WATCHER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Index">Specifies filter for INDEX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherData list</returns>
        public List<OpMonitoringWatcherData> GetMonitoringWatcherDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherData = null, int[] IdMonitoringWatcher = null, long[] IdDataType = null, int[] Index = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMonitoringWatcherDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherDataFilter"));
            }

            try
            {
                List<OpMonitoringWatcherData> returnList = OpMonitoringWatcherData.ConvertList(dbConnectionCore.GetMonitoringWatcherDataFilter(IdMonitoringWatcherData, IdMonitoringWatcher, IdDataType, Index, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMonitoringWatcherData item in returnList)
                        MonitoringWatcherDataDict[item.IdMonitoringWatcherData] = item;
                    MonitoringWatcherDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetMonitoringWatcherTypeFilter

        /// <summary>
        /// Gets MonitoringWatcherType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllMonitoringWatcherType funcion</param>
        /// <param name="IdMonitoringWatcherType">Specifies filter for ID_MONITORING_WATCHER_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Plugin">Specifies filter for PLUGIN column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_MONITORING_WATCHER_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>MonitoringWatcherType list</returns>
        public List<OpMonitoringWatcherType> GetMonitoringWatcherTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdMonitoringWatcherType = null, string Name = null, string Plugin = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetMonitoringWatcherTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetMonitoringWatcherTypeFilter"));
            }

            try
            {
                List<OpMonitoringWatcherType> returnList = OpMonitoringWatcherType.ConvertList(dbConnectionCore.GetMonitoringWatcherTypeFilter(IdMonitoringWatcherType, Name, Plugin, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpMonitoringWatcherType item in returnList)
                        MonitoringWatcherTypeDict[item.IdMonitoringWatcherType] = item;
                    MonitoringWatcherTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetMonitoringWatcherTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetNotificationDeliveryTypeFilter

        /// <summary>
        /// Gets NotificationDeliveryType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllNotificationDeliveryType funcion</param>
        /// <param name="IdNotificationDeliveryType">Specifies filter for ID_NOTIFICATION_DELIVERY_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_NOTIFICATION_DELIVERY_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>NotificationDeliveryType list</returns>
        public virtual List<OpNotificationDeliveryType> GetNotificationDeliveryTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdNotificationDeliveryType  = null,string Name = null,long[] IdDescr  = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetNotificationDeliveryTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetNotificationDeliveryTypeFilter"));
            }
	
	        try
	        {
		        List<OpNotificationDeliveryType> returnList = OpNotificationDeliveryType.ConvertList(dbConnectionCore.GetNotificationDeliveryTypeFilter( IdNotificationDeliveryType , Name , IdDescr , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpNotificationDeliveryType item in returnList)
				        NotificationDeliveryTypeDict[item.IdNotificationDeliveryType] = item;
                    NotificationDeliveryTypeIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetNotificationDeliveryTypeFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetNotificationTypeFilter

        /// <summary>
        /// Gets NotificationType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllNotificationType funcion</param>
        /// <param name="IdNotificationType">Specifies filter for ID_NOTIFICATION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_NOTIFICATION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>NotificationType list</returns>
        public virtual List<OpNotificationType> GetNotificationTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdNotificationType  = null,string Name = null,long[] IdDescr  = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetNotificationTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetNotificationTypeFilter"));
            }
	
	        try
	        {
		        List<OpNotificationType> returnList = OpNotificationType.ConvertList(dbConnectionCore.GetNotificationTypeFilter( IdNotificationType , Name , IdDescr , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpNotificationType item in returnList)
				        NotificationTypeDict[item.IdNotificationType] = item;
                    NotificationTypeIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetNotificationTypeFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetOperatorActivityFilter

        /// <summary>
        /// Gets OperatorActivity list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperatorActivity funcion</param>
        /// <param name="IdOperatorActivity">Specifies filter for ID_OPERATOR_ACTIVITY column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdActivity">Specifies filter for ID_ACTIVITY column</param>
        /// <param name="Deny">Specifies filter for DENY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR_ACTIVITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OperatorActivity list</returns>
        public virtual List<OpOperatorActivity> GetOperatorActivityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdOperatorActivity = null, int[] IdOperator = null, int[] IdActivity = null, int[] IdModule = null, bool? Deny = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetOperatorActivityFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorActivityFilter"));
            }

            try
            {
                List<OpOperatorActivity> returnList = OpOperatorActivity.ConvertList(dbConnectionCore.GetOperatorActivityFilter(IdOperatorActivity, IdOperator, IdActivity, IdModule, Deny, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpOperatorActivity item in returnList)
                        OperatorActivityDict[item.IdOperatorActivity] = item;
                    OperatorActivityIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOperatorActivityFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetOperatorDataFilter

        /// <summary>
        /// Gets OperatorData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperatorData funcion</param>
        /// <param name="IdOperatorData">Specifies filter for ID_OPERATOR_DATA column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OperatorData list</returns>
        public virtual List<OpOperatorData> GetOperatorDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdOperatorData = null, int[] IdOperator = null, long[] IdDataType = null, int[] IndexNbr = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetOperatorDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorDataFilter"));
            }

            try
            {
                List<OpOperatorData> returnList = OpOperatorData.ConvertList(dbConnectionCore.GetOperatorDataFilter(IdOperatorData, IdOperator, IdDataType, IndexNbr, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpOperatorData item in returnList)
                        OperatorDataDict[item.IdOperatorData] = item;
                    OperatorDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOperatorDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetOperatorRoleFilter

        /// <summary>
        /// Gets OperatorRole list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOperatorRole funcion</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_OPERATOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OperatorRole list</returns>
        public virtual List<OpOperatorRole> GetOperatorRoleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOperator = null, int[] IdRole = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetOperatorRoleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOperatorRoleFilter"));
            }

            try
            {
                List<OpOperatorRole> returnList = OpOperatorRole.ConvertList(dbConnectionCore.GetOperatorRoleFilter(IdOperator, IdRole, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpOperatorRole item in returnList)
                        OperatorRoleDict[item.IdOperator] = item;
                    OperatorRoleIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOperatorRoleFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetOrderFilter

        /// <summary>
        /// Gets Order list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOrder funcion</param>
        /// <param name="IdOrder">Specifies filter for ID_ORDER column</param>
        /// <param name="IdOrderStateType">Specifies filter for ID_ORDER_STATE_TYPE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ORDER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Order list</returns>
        public virtual List<OpOrder> GetOrderFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOrder = null, int[] IdOrderStateType = null, int[] IdOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetOrderFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOrderFilter"));
            }

            try
            {
                List<OpOrder> returnList = OpOrder.ConvertList(dbConnectionCore.GetOrderFilter(IdOrder, IdOrderStateType, IdOperator, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpOrder item in returnList)
                        OrderDict[item.IdOrder] = item;
                    OrderIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOrderFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetOrderDataFilter

        /// <summary>
        /// Gets OrderData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOrderData funcion</param>
        /// <param name="IdOrderData">Specifies filter for ID_ORDER_DATA column</param>
        /// <param name="IdOrder">Specifies filter for ID_ORDER column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ORDER_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OrderData list</returns>
        public virtual List<OpOrderData> GetOrderDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdOrderData = null, int[] IdOrder = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetOrderDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOrderDataFilter"));
            }

            try
            {
                List<OpOrderData> returnList = OpOrderData.ConvertList(dbConnectionCore.GetOrderDataFilter(IdOrderData, IdOrder, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpOrderData item in returnList)
                        OrderDataDict[item.IdOrderData] = item;
                    OrderDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOrderDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetOrderStateTypeFilter

        /// <summary>
        /// Gets OrderStateType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllOrderStateType funcion</param>
        /// <param name="IdOrderStateType">Specifies filter for ID_ORDER_STATE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ORDER_STATE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>OrderStateType list</returns>
        public virtual List<OpOrderStateType> GetOrderStateTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdOrderStateType = null, string Name = null, long[] IdDescr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetOrderStateTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetOrderStateTypeFilter"));
            }

            try
            {
                List<OpOrderStateType> returnList = OpOrderStateType.ConvertList(dbConnectionCore.GetOrderStateTypeFilter(IdOrderStateType, Name, IdDescr, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpOrderStateType item in returnList)
                        OrderStateTypeDict[item.IdOrderStateType] = item;
                    OrderStateTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetOrderStateTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPackageFilter

        /// <summary>
        /// Gets Package list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackage funcion</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="SendDate">Specifies filter for SEND_DATE column</param>
        /// <param name="IdOperatorCreator">Specifies filter for ID_OPERATOR_CREATOR column</param>
        /// <param name="IdOperatorReceiver">Specifies filter for ID_OPERATOR_RECEIVER column</param>
        /// <param name="Received">Specifies filter for RECEIVED column</param>
        /// <param name="ReceiveDate">Specifies filter for RECEIVE_DATE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdPackageStatus">Specifies filter for ID_PACKAGE_STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Package list</returns>
        public virtual List<OpPackage> GetPackageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPackage = null, string Name = null, TypeDateTimeCode CreationDate = null, string Code = null, TypeDateTimeCode SendDate = null, int[] IdOperatorCreator = null,
                            int[] IdOperatorReceiver = null, bool? Received = null, TypeDateTimeCode ReceiveDate = null, int[] IdDistributor = null, int[] IdPackageStatus = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPackageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageFilter"));
            }

            try
            {
                List<OpPackage> returnList = OpPackage.ConvertList(dbConnectionCore.GetPackageFilter(IdPackage, Name, CreationDate, Code, SendDate, IdOperatorCreator,
                             IdOperatorReceiver, Received, ReceiveDate, IdDistributor, IdPackageStatus,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPackage item in returnList)
                        PackageDict[item.IdPackage] = item;
                    PackageIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPackageFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPackageStatusFilter

        /// <summary>
        /// Gets PackageStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackageStatus funcion</param>
        /// <param name="IdPackageStatus">Specifies filter for ID_PACKAGE_STATUS column</param>
        /// <param name="PackageStatus">Specifies filter for PACKAGE_STATUS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PackageStatus list</returns>
        public virtual List<OpPackageStatus> GetPackageStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPackageStatus = null, string PackageStatus = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPackageStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageStatusFilter"));
            }

            try
            {
                List<OpPackageStatus> returnList = OpPackageStatus.ConvertList(dbConnectionCore.GetPackageStatusFilter(IdPackageStatus, PackageStatus, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPackageStatus item in returnList)
                        PackageStatusDict[item.IdPackageStatus] = item;
                    PackageStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPackageStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPackageStatusHistoryFilter

        /// <summary>
        /// Gets PackageStatusHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackageStatusHistory funcion</param>
        /// <param name="IdPackageStatusHistory">Specifies filter for ID_PACKAGE_STATUS_HISTORY column</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="IdPackageStatus">Specifies filter for ID_PACKAGE_STATUS column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE_STATUS_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PackageStatusHistory list</returns>
        public virtual List<OpPackageStatusHistory> GetPackageStatusHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPackageStatusHistory = null, int[] IdPackage = null, int[] IdPackageStatus = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPackageStatusHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageStatusHistoryFilter"));
            }

            try
            {
                List<OpPackageStatusHistory> returnList = OpPackageStatusHistory.ConvertList(dbConnectionCore.GetPackageStatusHistoryFilter(IdPackageStatusHistory, IdPackage, IdPackageStatus, StartDate, EndDate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPackageStatusHistory item in returnList)
                        PackageStatusHistoryDict[item.IdPackageStatusHistory] = item;
                    PackageStatusHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPackageStatusHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPackageDataFilter

        /// <summary>
        /// Gets PackageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPackageData funcion</param>
        /// <param name="IdPackageData">Specifies filter for ID_PACKAGE_DATA column</param>
        /// <param name="IdPackage">Specifies filter for ID_PACKAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PACKAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PackageData list</returns>
        public virtual List<OpPackageData> GetPackageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPackageData = null, int[] IdPackage = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPackageDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPackageDataFilter"));
            }

            try
            {
                List<OpPackageData> returnList = OpPackageData.ConvertList(dbConnectionCore.GetPackageDataFilter(IdPackageData, IdPackage, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpPackageData item in returnList)
                        PackageDataDict[item.IdPackageData] = item;
                    PackageDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPackageDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetPaymentModuleFilter

        /// <summary>
        /// Gets PaymentModule list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPaymentModule funcion</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdPaymentModuleType">Specifies filter for ID_PAYMENT_MODULE_TYPE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PAYMENT_MODULE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PaymentModule list</returns>
        public virtual List<OpPaymentModule> GetPaymentModuleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPaymentModule  = null,string Name = null,int[] IdPaymentModuleType  = null,long[] IdDescr  = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPaymentModuleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPaymentModuleFilter"));
            }
	
	        try
	        {
		        List<OpPaymentModule> returnList = OpPaymentModule.ConvertList(dbConnectionCore.GetPaymentModuleFilter( IdPaymentModule , Name , IdPaymentModuleType , IdDescr , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpPaymentModule item in returnList)
				        PaymentModuleDict[item.IdPaymentModule] = item;
                    PaymentModuleIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPaymentModuleFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetPaymentModuleDataFilter

        /// <summary>
        /// Gets PaymentModuleData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPaymentModuleData funcion</param>
        /// <param name="IdPaymentModuleData">Specifies filter for ID_PAYMENT_MODULE_DATA column</param>
        /// <param name="IdPaymentModule">Specifies filter for ID_PAYMENT_MODULE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PAYMENT_MODULE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PaymentModuleData list</returns>
        public virtual List<OpPaymentModuleData> GetPaymentModuleDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdPaymentModuleData  = null,int[] IdPaymentModule  = null,long[] IdDataType  = null,int[] IndexNbr  = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPaymentModuleDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPaymentModuleDataFilter"));
            }
	
	        try
	        {
		        List<OpPaymentModuleData> returnList = OpPaymentModuleData.ConvertList(dbConnectionCore.GetPaymentModuleDataFilter( IdPaymentModuleData , IdPaymentModule , IdDataType , IndexNbr , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpPaymentModuleData item in returnList)
				        PaymentModuleDataDict[item.IdPaymentModuleData] = item;
                    PaymentModuleDataIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPaymentModuleDataFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetPaymentModuleTypeFilter

        /// <summary>
        /// Gets PaymentModuleType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPaymentModuleType funcion</param>
        /// <param name="IdPaymentModuleType">Specifies filter for ID_PAYMENT_MODULE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PAYMENT_MODULE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>PaymentModuleType list</returns>
        public virtual List<OpPaymentModuleType> GetPaymentModuleTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPaymentModuleType  = null,string Name = null,long[] IdDescr  = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0 )
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPaymentModuleTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPaymentModuleTypeFilter"));
            }
	
	        try
	        {
		        List<OpPaymentModuleType> returnList = OpPaymentModuleType.ConvertList(dbConnectionCore.GetPaymentModuleTypeFilter( IdPaymentModuleType , Name , IdDescr , topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
		        if (mergeIntoCache)
                {
                    foreach (OpPaymentModuleType item in returnList)
				        PaymentModuleTypeDict[item.IdPaymentModuleType] = item;
                    PaymentModuleTypeIsFullCache = true;
                }
		        return returnList;
	        }
	        catch(Exception ex)
	        {
		        IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPaymentModuleTypeFilter", ex);
                throw;
	        }
	
        }
        #endregion
        #region GetPriorityFilter

        /// <summary>
        /// Gets Priority list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllPriority funcion</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Value">Specifies filter for VALUE column</param>
        /// <param name="RealizationTime">Specifies filter for REALIZATION_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PRIORITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Priority list</returns>
        public virtual List<OpPriority> GetPriorityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdPriority = null, long[] IdDescr = null, int[] Value = null, int[] RealizationTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetPriorityFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetPriorityFilter"));
            }

            try
            {
                List<OpPriority> returnList = OpPriority.ConvertList(dbConnectionCore.GetPriorityFilter(IdPriority, IdDescr, Value, RealizationTime, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpPriority item in returnList)
                        PriorityDict[item.IdPriority] = item;
                    PriorityIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetPriorityFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetProductCodeFilter

        /// <summary>
        /// Gets ProductCode list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProductCode funcion</param>
        /// <param name="IdProductCode">Specifies filter for ID_PRODUCT_CODE column</param>
        /// <param name="Code">Specifies filter for CODE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Density">Specifies filter for DENSITY column</param>
        /// <param name="Alpha">Specifies filter for ALPHA column</param>
        /// <param name="CalorificValue">Specifies filter for CALORIFIC_VALUE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PRODUCT_CODE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProductCode list</returns>
        public virtual List<OpProductCode> GetProductCodeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProductCode = null, string Code = null, long[] IdDescr = null, int[] Density = null, Double[] Alpha = null, Double[] CalorificValue = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetProductCodeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProductCodeFilter"));
            }

            try
            {
                List<OpProductCode> returnList = OpProductCode.ConvertList(dbConnectionCore.GetProductCodeFilter(IdProductCode, Code, IdDescr, Density, Alpha, CalorificValue,
                    topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpProductCode item in returnList)
                        ProductCodeDict[item.IdProductCode] = item;
                    ProductCodeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProductCodeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetProfileFilter

        /// <summary>
        /// Gets Profile list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProfile funcion</param>
        /// <param name="IdProfile">Specifies filter for ID_PROFILE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROFILE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Profile list</returns>
        public virtual List<OpProfile> GetProfileFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProfile = null, string Name = null, long[] IdDescr = null, int[] IdModule = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetProfileFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProfileFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpProfile> returnList = OpProfile.ConvertList(dbConnectionCore.GetProfileFilter(IdProfile, Name, IdDescr, IdModule, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpProfile> returnList = OpProfile.ConvertList(dbConnectionCore.GetProfileFilter(IdProfile, Name, IdDescr, IdModule, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpProfile item in returnList)
                            ProfileDict[item.IdProfile] = item;
                        ProfileIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpProfile>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProfileFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetProfileDataFilter

        /// <summary>
        /// Gets ProfileData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProfileData funcion</param>
        /// <param name="IdProfileData">Specifies filter for ID_PROFILE_DATA column</param>
        /// <param name="IdProfile">Specifies filter for ID_PROFILE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROFILE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProfileData list</returns>
        public virtual List<OpProfileData> GetProfileDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdProfileData = null, int[] IdProfile = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetProfileDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProfileDataFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    List<OpProfileData> returnList = OpProfileData.ConvertList(dbConnectionCore.GetProfileDataFilter(IdProfileData, IdProfile, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpProfileData> returnList = OpProfileData.ConvertList(dbConnectionCore.GetProfileDataFilter(IdProfileData, IdProfile, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpProfileData item in returnList)
                            ProfileDataDict[item.IdProfileData] = item;
                        ProfileDataIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpProfileData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProfileDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetProtocolFilter

        /// <summary>
        /// Gets Protocol list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProtocol funcion</param>
        /// <param name="IdProtocol">Specifies filter for ID_PROTOCOL column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdProtocolDriver">Specifies filter for ID_PROTOCOL_DRIVER column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROTOCOL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Protocol list</returns>
        public virtual List<OpProtocol> GetProtocolFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProtocol = null, string Name = null, int[] IdProtocolDriver = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetProtocolFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProtocolFilter"));
            }

            try
            {
                List<OpProtocol> returnList = OpProtocol.ConvertList(dbConnectionCore.GetProtocolFilter(IdProtocol, Name, IdProtocolDriver, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpProtocol item in returnList)
                        ProtocolDict[item.IdProtocol] = item;
                    ProtocolIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProtocolFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetProtocolDriverFilter

        /// <summary>
        /// Gets ProtocolDriver list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllProtocolDriver funcion</param>
        /// <param name="IdProtocolDriver">Specifies filter for ID_PROTOCOL_DRIVER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_PROTOCOL_DRIVER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ProtocolDriver list</returns>
        public virtual List<OpProtocolDriver> GetProtocolDriverFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdProtocolDriver = null, string Name = null, long[] IdDescr = null, string PluginName = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetProtocolDriverFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetProtocolDriverFilter"));
            }

            try
            {
                List<OpProtocolDriver> returnList = OpProtocolDriver.ConvertList(dbConnectionCore.GetProtocolDriverFilter(IdProtocolDriver, Name, IdDescr, PluginName, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpProtocolDriver item in returnList)
                        ProtocolDriverDict[item.IdProtocolDriver] = item;
                    ProtocolDriverIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetProtocolDriverFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetReferenceTypeFilter

        /// <summary>
        /// Gets ReferenceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllReferenceType funcion</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_REFERENCE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ReferenceType list</returns>
        public virtual List<OpReferenceType> GetReferenceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdReferenceType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetReferenceTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetReferenceTypeFilter"));
            }

            try
            {
                List<OpReferenceType> returnList = OpReferenceType.ConvertList(dbConnectionCore.GetReferenceTypeFilter(IdReferenceType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpReferenceType item in returnList)
                        ReferenceTypeDict[item.IdReferenceType] = item;
                    ReferenceTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetReferenceTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRoleFilter

        /// <summary>
        /// Gets Role list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRole funcion</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROLE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Role list</returns>
        public virtual List<OpRole> GetRoleFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRole = null, string Name = null, int[] IdModule = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRoleFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoleFilter"));
            }

            try
            {
                List<OpRole> returnList = OpRole.ConvertList(dbConnectionCore.GetRoleFilter(IdRole, Name, IdModule, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRole item in returnList)
                        RoleDict[item.IdRole] = item;
                    RoleIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRoleFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRoleActivityFilter

        /// <summary>
        /// Gets RoleActivity list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoleActivity funcion</param>
        /// <param name="IdRoleActivity">Specifies filter for ID_ROLE_ACTIVITY column</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="IdActivity">Specifies filter for ID_ACTIVITY column</param>
        /// <param name="Deny">Specifies filter for DENY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROLE_ACTIVITY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoleActivity list</returns>
        public virtual List<OpRoleActivity> GetRoleActivityFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRoleActivity = null, int[] IdRole = null, int[] IdActivity = null, bool? Deny = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRoleActivityFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoleActivityFilter"));
            }

            try
            {
                List<OpRoleActivity> returnList = OpRoleActivity.ConvertList(dbConnectionCore.GetRoleActivityFilter(IdRoleActivity, IdRole, IdActivity, Deny, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRoleActivity item in returnList)
                        RoleActivityDict[item.IdRoleActivity] = item;
                    RoleActivityIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRoleActivityFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRoleGroupFilter

        /// <summary>
        /// Gets RoleGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoleGroup funcion</param>
        /// <param name="IdRoleParent">Specifies filter for ID_ROLE_PARENT column</param>
        /// <param name="IdRole">Specifies filter for ID_ROLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROLE_PARENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoleGroup list</returns>
        public virtual List<OpRoleGroup> GetRoleGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRoleParent = null, int[] IdRole = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRoleGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoleGroupFilter"));
            }

            try
            {
                List<OpRoleGroup> returnList = OpRoleGroup.ConvertList(dbConnectionCore.GetRoleGroupFilter(IdRoleParent, IdRole, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRoleGroup item in returnList)
                        RoleGroupDict[item.IdRoleParent] = item;
                    RoleGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRoleGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteFilter

        /// <summary>
        /// Gets Route list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoute funcion</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="Year">Specifies filter for YEAR column</param>
        /// <param name="Month">Specifies filter for MONTH column</param>
        /// <param name="Week">Specifies filter for WEEK column</param>
        /// <param name="IdOperatorExecutor">Specifies filter for ID_OPERATOR_EXECUTOR column</param>
        /// <param name="IdOperatorApproved">Specifies filter for ID_OPERATOR_APPROVED column</param>
        /// <param name="DateUploaded">Specifies filter for DATE_UPLOADED column</param>
        /// <param name="DateApproved">Specifies filter for DATE_APPROVED column</param>
        /// <param name="DateFinished">Specifies filter for DATE_FINISHED column</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ExpirationDate">Specifies filter for EXPIRATION_DATE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Route list</returns>
        public virtual List<OpRoute> GetRouteFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRoute = null, int[] IdRouteDef = null, int[] Year = null, int[] Month = null, int[] Week = null, int[] IdOperatorExecutor = null,
                            int[] IdOperatorApproved = null, TypeDateTimeCode DateUploaded = null, TypeDateTimeCode DateApproved = null, TypeDateTimeCode DateFinished = null, int[] IdRouteStatus = null,
                            int[] IdDistributor = null, int[] IdRouteType = null, TypeDateTimeCode CreationDate = null, TypeDateTimeCode ExpirationDate = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteFilter"));
            }

            try
            {
                List<OpRoute> returnList = OpRoute.ConvertList(dbConnectionCore.GetRouteFilter(IdRoute, IdRouteDef, Year, Month, Week, IdOperatorExecutor,
                             IdOperatorApproved, DateUploaded, DateApproved, DateFinished, IdRouteStatus,
                             IdDistributor, IdRouteType, CreationDate, ExpirationDate, Name, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpRoute item in returnList)
                        RouteDict[item.IdRoute] = item;
                    RouteIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteDataFilter

        /// <summary>
        /// Gets RouteData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteData funcion</param>
        /// <param name="IdRouteData">Specifies filter for ID_ROUTE_DATA column</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteData list</returns>
        public virtual List<OpRouteData> GetRouteDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteData = null, long[] IdRoute = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDataFilter"));
            }

            try
            {
                List<OpRouteData> returnList = OpRouteData.ConvertList(dbConnectionCore.GetRouteDataFilter(IdRouteData, IdRoute, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteData item in returnList)
                        RouteDataDict[item.IdRouteData] = item;
                    RouteDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteDefFilter


        /// <summary>
        /// Gets RouteDef list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDef funcion</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="ExpirationDate">Specifies filter for EXPIRATION_DATE column</param>
        /// <param name="DeleteDate">Specifies filter for DELETE_DATE column</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="AdHoc">Specifies filter for AD_HOC column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDef list</returns>
        public virtual List<OpRouteDef> GetRouteDefFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteDef = null, string Name = null, string Description = null, TypeDateTimeCode CreationDate = null, TypeDateTimeCode ExpirationDate = null, TypeDateTimeCode DeleteDate = null,
                            int[] IdRouteType = null, int[] IdRouteStatus = null, int[] IdOperator = null, bool? AdHoc = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteDefFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefFilter"));
            }

            try
            {
                List<OpRouteDef> returnList = OpRouteDef.ConvertList(dbConnectionCore.GetRouteDefFilter(IdRouteDef, Name, Description, CreationDate, ExpirationDate, DeleteDate,
                             IdRouteType, IdRouteStatus, IdOperator, AdHoc, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteDef item in returnList)
                        RouteDefDict[item.IdRouteDef] = item;
                    RouteDefIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteDefDataFilter

        /// <summary>
        /// Gets RouteDefData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefData funcion</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefData list</returns>
        public virtual List<OpRouteDefData> GetRouteDefDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteDef = null, long[] IdDataType = null, int[] IndexNbr = null
            , long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteDefDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefDataFilter"));
            }

            try
            {
                List<OpRouteDefData> returnList = OpRouteDefData.ConvertList(dbConnectionCore.GetRouteDefDataFilter(IdRouteDef, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteDefData item in returnList)
                        RouteDefDataDict[item.IdRouteDef] = item;
                    RouteDefDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteDefHistoryFilter

        /// <summary>
        /// Gets RouteDefHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefHistory funcion</param>
        /// <param name="IdRouteDefHistory">Specifies filter for ID_ROUTE_DEF_HISTORY column</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefHistory list</returns>
        public virtual List<OpRouteDefHistory> GetRouteDefHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDefHistory = null, int[] IdRouteDef = null, int[] IdRouteStatus = null, int[] IdOperator = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteDefHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefHistoryFilter"));
            }

            try
            {
                List<OpRouteDefHistory> returnList = OpRouteDefHistory.ConvertList(dbConnectionCore.GetRouteDefHistoryFilter(IdRouteDefHistory, IdRouteDef, IdRouteStatus, IdOperator, StartTime, EndTime,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteDefHistory item in returnList)
                        RouteDefHistoryDict[item.IdRouteDefHistory] = item;
                    RouteDefHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteDefLocationFilter

        /// <summary>
        /// Gets RouteDefLocation list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDefLocation funcion</param>
        /// <param name="IdRouteDefLocation">Specifies filter for ID_ROUTE_DEF_LOCATION column</param>
        /// <param name="IdRouteDef">Specifies filter for ID_ROUTE_DEF column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="OrderNbr">Specifies filter for ORDER_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEF_LOCATION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDefLocation list</returns>
        public virtual List<OpRouteDefLocation> GetRouteDefLocationFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDefLocation = null, int[] IdRouteDef = null, long[] IdLocation = null, int[] OrderNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteDefLocationFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDefLocationFilter"));
            }

            try
            {
                List<OpRouteDefLocation> returnList = OpRouteDefLocation.ConvertList(dbConnectionCore.GetRouteDefLocationFilter(IdRouteDefLocation, IdRouteDef, IdLocation, OrderNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteDefLocation item in returnList)
                        RouteDefLocationDict[item.IdRouteDefLocation] = item;
                    RouteDefLocationIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDefLocationFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteDeviceFilter

        /// <summary>
        /// Gets RouteDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteDevice funcion</param>
        /// <param name="IdRouteDevice">Specifies filter for ID_ROUTE_DEVICE column</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_DEVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteDevice list</returns>
        public virtual List<OpRouteDevice> GetRouteDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRouteDevice = null, long[] IdRoute = null, long[] SerialNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteDeviceFilter"));
            }

            try
            {
                List<OpRouteDevice> returnList = OpRouteDevice.ConvertList(dbConnectionCore.GetRouteDeviceFilter(IdRouteDevice, IdRoute, SerialNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteDevice item in returnList)
                        RouteDeviceDict[item.IdRouteDevice] = item;
                    RouteDeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteDeviceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteElementStatusFilter

        /// <summary>
        /// Gets RouteElementStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteElementStatus funcion</param>
        /// <param name="IdRouteElementStatus">Specifies filter for ID_ROUTE_ELEMENT_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_ELEMENT_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteElementStatus list</returns>
        public virtual List<OpRouteElementStatus> GetRouteElementStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteElementStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteElementStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteElementStatusFilter"));
            }

            try
            {
                List<OpRouteElementStatus> returnList = OpRouteElementStatus.ConvertList(dbConnectionCore.GetRouteElementStatusFilter(IdRouteElementStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteElementStatus item in returnList)
                        RouteElementStatusDict[item.IdRouteElementStatus] = item;
                    RouteElementStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteElementStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRoutePointFilter

        /// <summary>
        /// Gets RoutePoint list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoutePoint funcion</param>
        /// <param name="IdRoutePoint">Specifies filter for ID_ROUTE_POINT column</param>
        /// <param name="IdRoute">Specifies filter for ID_ROUTE column</param>
        /// <param name="Type">Specifies filter for TYPE column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="StartDateTime">Specifies filter for START_DATE_TIME column</param>
        /// <param name="EndDateTime">Specifies filter for END_DATE_TIME column</param>
        /// <param name="Latitude">Specifies filter for LATITUDE column</param>
        /// <param name="Longitude">Specifies filter for LONGITUDE column</param>
        /// <param name="KmCounter">Specifies filter for KM_COUNTER column</param>
        /// <param name="InvoiceNumber">Specifies filter for INVOICE_NUMBER column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_POINT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoutePoint list</returns>
        public virtual List<OpRoutePoint> GetRoutePointFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRoutePoint = null, long[] IdRoute = null, int[] Type = null, int[] IdTask = null, TypeDateTimeCode StartDateTime = null, TypeDateTimeCode EndDateTime = null,
                            Double[] Latitude = null, Double[] Longitude = null, int[] KmCounter = null, string InvoiceNumber = null, string Notes = null,
                            int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool loadCustomData = true)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRoutePointFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoutePointFilter"));
            }

            try
            {
                List<OpRoutePoint> returnList = OpRoutePoint.ConvertList(dbConnectionCore.GetRoutePointFilter(IdRoutePoint, IdRoute, Type, IdTask, StartDateTime, EndDateTime,
                             Latitude, Longitude, KmCounter, InvoiceNumber, Notes,
                             IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData);
                if (mergeIntoCache)
                {
                    foreach (OpRoutePoint item in returnList)
                        RoutePointDict[item.IdRoutePoint] = item;
                    RoutePointIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRoutePointFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRoutePointDataFilter

        /// <summary>
        /// Gets RoutePointData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRoutePointData funcion</param>
        /// <param name="IdRoutePointData">Specifies filter for ID_ROUTE_POINT_DATA column</param>
        /// <param name="IdRoutePoint">Specifies filter for ID_ROUTE_POINT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_POINT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RoutePointData list</returns>
        public virtual List<OpRoutePointData> GetRoutePointDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdRoutePointData = null, int[] IdRoutePoint = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRoutePointDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRoutePointDataFilter"));
            }

            try
            {
                List<OpRoutePointData> returnList = OpRoutePointData.ConvertList(dbConnectionCore.GetRoutePointDataFilter(IdRoutePointData, IdRoutePoint, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRoutePointData item in returnList)
                        RoutePointDataDict[item.IdRoutePointData] = item;
                    RoutePointDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRoutePointDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteStatusFilter

        /// <summary>
        /// Gets RouteStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteStatus funcion</param>
        /// <param name="IdRouteStatus">Specifies filter for ID_ROUTE_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteStatus list</returns>
        public virtual List<OpRouteStatus> GetRouteStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteStatusFilter"));
            }

            try
            {
                List<OpRouteStatus> returnList = OpRouteStatus.ConvertList(dbConnectionCore.GetRouteStatusFilter(IdRouteStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpRouteStatus item in returnList)
                        RouteStatusDict[item.IdRouteStatus] = item;
                    RouteStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteTypeFilter

        /// <summary>
        /// Gets RouteType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteType funcion</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteType list</returns>
        public virtual List<OpRouteType> GetRouteTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTypeFilter"));
            }

            try
            {
                List<OpRouteType> returnList = OpRouteType.ConvertList(dbConnectionCore.GetRouteTypeFilter(IdRouteType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteType item in returnList)
                        RouteTypeDict[item.IdRouteType] = item;
                    RouteTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetRouteTypeActionTypeFilter

        /// <summary>
        /// Gets RouteTypeActionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllRouteTypeActionType funcion</param>
        /// <param name="IdRouteTypeActionType">Specifies filter for ID_ROUTE_TYPE_ACTION_TYPE column</param>
        /// <param name="IdRouteType">Specifies filter for ID_ROUTE_TYPE column</param>
        /// <param name="IdActionType">Specifies filter for ID_ACTION_TYPE column</param>
        /// <param name="Default">Specifies filter for DEFAULT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ROUTE_TYPE_ACTION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>RouteTypeActionType list</returns>
        public virtual List<OpRouteTypeActionType> GetRouteTypeActionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdRouteTypeActionType = null, int[] IdRouteType = null, int[] IdActionType = null, bool? Default = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetRouteTypeActionTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetRouteTypeActionTypeFilter"));
            }

            try
            {
                List<OpRouteTypeActionType> returnList = OpRouteTypeActionType.ConvertList(dbConnectionCore.GetRouteTypeActionTypeFilter(IdRouteTypeActionType, IdRouteType, IdActionType, Default, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpRouteTypeActionType item in returnList)
                        RouteTypeActionTypeDict[item.IdRouteTypeActionType] = item;
                    RouteTypeActionTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetRouteTypeActionTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetScheduleTypeFilter

        /// <summary>
        /// Gets ScheduleType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllScheduleType funcion</param>
        /// <param name="IdScheduleType">Specifies filter for ID_SCHEDULE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SCHEDULE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ScheduleType list</returns>
        public virtual List<OpScheduleType> GetScheduleTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdScheduleType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetScheduleTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetScheduleTypeFilter"));
            }

            try
            {
                List<OpScheduleType> returnList = OpScheduleType.ConvertList(dbConnectionCore.GetScheduleTypeFilter(IdScheduleType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpScheduleType item in returnList)
                        ScheduleTypeDict[item.IdScheduleType] = item;
                    ScheduleTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetScheduleTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSealFilter

        /// <summary>
        /// Gets Seal list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSeal funcion</param>
        /// <param name="IdSeal">Specifies filter for ID_SEAL column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SEAL DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Seal list</returns>
        public virtual List<OpSeal> GetSealFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSeal = null, string SerialNbr = null, string Description = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSealFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSealFilter"));
            }

            try
            {
                List<OpSeal> returnList = OpSeal.ConvertList(dbConnectionCore.GetSealFilter(IdSeal, SerialNbr, Description, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSeal item in returnList)
                        SealDict[item.IdSeal] = item;
                    SealIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSealFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSettlementDocumentTypeFilter

        /// <summary>
        /// Gets SettlementDocumentType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSettlementDocumentType funcion</param>
        /// <param name="IdSettlementDocumentType">Specifies filter for ID_SETTLEMENT_DOCUMENT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="Active">Specifies filter for ACTIVE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SETTLEMENT_DOCUMENT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SettlementDocumentType list</returns>
        public virtual List<OpSettlementDocumentType> GetSettlementDocumentTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSettlementDocumentType = null, string Name = null, string Description = null, bool? Active = null, int[] IdDistributor = null, long[] IdDescr = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSettlementDocumentTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSettlementDocumentTypeFilter"));
            }

            try
            {
                List<OpSettlementDocumentType> returnList = OpSettlementDocumentType.ConvertList(dbConnectionCore.GetSettlementDocumentTypeFilter(IdSettlementDocumentType, Name, Description, Active, IdDistributor, IdDescr,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSettlementDocumentType item in returnList)
                        SettlementDocumentTypeDict[item.IdSettlementDocumentType] = item;
                    SettlementDocumentTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSettlementDocumentTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceFilter

        /// <summary>
        /// Gets Service list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllService funcion</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="DeleteDate">Specifies filter for DELETE_DATE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Service list</returns>
        public virtual List<OpService> GetServiceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdService = null, string Name = null, int[] IdDeviceOrderNumber = null, TypeDateTimeCode DeleteDate = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceFilter"));
            }

            try
            {
                List<OpService> returnList = OpService.ConvertList(dbConnectionCore.GetServiceFilter(IdService, Name, IdDeviceOrderNumber, DeleteDate, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpService item in returnList)
                        ServiceDict[item.IdService] = item;
                    ServiceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceActionResultFilter

        /// <summary>
        /// Gets ServiceActionResult list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceActionResult funcion</param>
        /// <param name="IdServiceActionResult">Specifies filter for ID_SERVICE_ACTION_RESULT column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="AllowInputText">Specifies filter for ALLOW_INPUT_TEXT column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_ACTION_RESULT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceActionResult list</returns>
        public virtual List<OpServiceActionResult> GetServiceActionResultFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceActionResult = null, int[] IdService = null, string Name = null, bool? AllowInputText = null, long[] IdDescr = null, int[] IdServiceReferenceType = null,
                            TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceActionResultFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceActionResultFilter"));
            }

            try
            {
                List<OpServiceActionResult> returnList = OpServiceActionResult.ConvertList(dbConnectionCore.GetServiceActionResultFilter(IdServiceActionResult, IdService, Name, AllowInputText, IdDescr, IdServiceReferenceType,
                             StartDate, EndDate, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceActionResult item in returnList)
                        ServiceActionResultDict[item.IdServiceActionResult] = item;
                    ServiceActionResultIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceActionResultFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceDataFilter

        /// <summary>
        /// Gets ServiceData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceData funcion</param>
        /// <param name="IdServiceData">Specifies filter for ID_SERVICE_DATA column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceData list</returns>
        public virtual List<OpServiceData> GetServiceDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceData = null, int[] IdService = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceDataFilter"));
            }

            try
            {
                List<OpServiceData> returnList = OpServiceData.ConvertList(dbConnectionCore.GetServiceDataFilter(IdServiceData, IdService, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceData item in returnList)
                        ServiceDataDict[item.IdServiceData] = item;
                    ServiceDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceCustomFilter

        /// <summary>
        /// Gets ServiceCustom list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceCustom funcion</param>
        /// <param name="IdServiceCustom">Specifies filter for ID_SERVICE_CUSTOM column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IndirectCost">Specifies filter for INDIRECT_COST column</param>
        /// <param name="IdFile">Specifies filter for ID_FILE column</param>
        /// <param name="CustomValue">Specifies filter for CUSTOM_VALUE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_CUSTOM DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceCustom list</returns>
        public virtual List<OpServiceCustom> GetServiceCustomFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceCustom = null, int[] IdServiceList = null, int[] IndirectCost = null, long[] IdFile = null, bool? CustomValue = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceCustomFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceCustomFilter"));
            }

            try
            {
                List<OpServiceCustom> returnList = OpServiceCustom.ConvertList(dbConnectionCore.GetServiceCustomFilter(IdServiceCustom, IdServiceList, IndirectCost, IdFile, CustomValue, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceCustom item in returnList)
                        ServiceCustomDict[item.IdServiceCustom] = item;
                    ServiceCustomIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceCustomFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceCustomDataFilter

        /// <summary>
        /// Gets ServiceCustomData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceCustomData funcion</param>
        /// <param name="IdServiceCustomData">Specifies filter for ID_SERVICE_CUSTOM_DATA column</param>
        /// <param name="IdServiceCustom">Specifies filter for ID_SERVICE_CUSTOM column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_CUSTOM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceCustomData list</returns>
        public virtual List<OpServiceCustomData> GetServiceCustomDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceCustomData = null, long[] IdServiceCustom = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceCustomDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceCustomDataFilter"));
            }

            try
            {
                List<OpServiceCustomData> returnList = OpServiceCustomData.ConvertList(dbConnectionCore.GetServiceCustomDataFilter(IdServiceCustomData, IdServiceCustom, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceCustomData item in returnList)
                        ServiceCustomDataDict[item.IdServiceCustomData] = item;
                    ServiceCustomDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceCustomDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceDiagnosticResultFilter

        /// <summary>
        /// Gets ServiceDiagnosticResult list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceDiagnosticResult funcion</param>
        /// <param name="IdServiceDiagnosticResult">Specifies filter for ID_SERVICE_DIAGNOSTIC_RESULT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IsServiceResult">Specifies filter for IS_SERVICE_RESULT column</param>
        /// <param name="IsDiagnosticResult">Specifies filter for IS_DIAGNOSTIC_RESULT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_DIAGNOSTIC_RESULT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceDiagnosticResult list</returns>
        public virtual List<OpServiceDiagnosticResult> GetServiceDiagnosticResultFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceDiagnosticResult = null, string Name = null, long[] IdDescr = null, bool? IsServiceResult = null, bool? IsDiagnosticResult = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceDiagnosticResultFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceDiagnosticResultFilter"));
            }

            try
            {
                List<OpServiceDiagnosticResult> returnList = OpServiceDiagnosticResult.ConvertList(dbConnectionCore.GetServiceDiagnosticResultFilter(IdServiceDiagnosticResult, Name, IdDescr, IsServiceResult, IsDiagnosticResult, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceDiagnosticResult item in returnList)
                        ServiceDiagnosticResultDict[item.IdServiceDiagnosticResult] = item;
                    ServiceDiagnosticResultIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceDiagnosticResultFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceInPackageFilter

        /// <summary>
        /// Gets ServiceInPackage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceInPackage funcion</param>
        /// <param name="IdServiceInPackage">Specifies filter for ID_SERVICE_IN_PACKAGE column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_IN_PACKAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceInPackage list</returns>
        public virtual List<OpServiceInPackage> GetServiceInPackageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceInPackage = null, long[] IdServicePackage = null, int[] IdService = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceInPackageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceInPackageFilter"));
            }

            try
            {
                List<OpServiceInPackage> returnList = OpServiceInPackage.ConvertList(dbConnectionCore.GetServiceInPackageFilter(IdServiceInPackage, IdServicePackage, IdService, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceInPackage item in returnList)
                        ServiceInPackageDict[item.IdServiceInPackage] = item;
                    ServiceInPackageIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceInPackageFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListFilter

        /// <summary>
        /// Gets ServiceList list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceList funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="IdOperatorReceiver">Specifies filter for ID_OPERATOR_RECEIVER column</param>
        /// <param name="IdOperatorServicer">Specifies filter for ID_OPERATOR_SERVICER column</param>
        /// <param name="CourierName">Specifies filter for COURIER_NAME column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="ShippingLetterNbr">Specifies filter for SHIPPING_LETTER_NBR column</param>
        /// <param name="ExpectedFinishDate">Specifies filter for EXPECTED_FINISH_DATE column</param>
        /// <param name="ServiceName">Specifies filter for SERVICE_NAME column</param>
        /// <param name="IdDocumentPackage">Specifies filter for ID_DOCUMENT_PACKAGE column</param>
        /// <param name="Confirmed">Specifies filter for CONFIRMED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceList list</returns>
        public virtual List<OpServiceList> GetServiceListFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, int[] IdDistributor = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null, int[] IdOperatorReceiver = null, int[] IdOperatorServicer = null,
                            string CourierName = null, int[] Priority = null, string ShippingLetterNbr = null, TypeDateTimeCode ExpectedFinishDate = null, string ServiceName = null,
                            int[] IdDocumentPackage = null, bool? Confirmed = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListFilter"));
            }

            try
            {
                List<OpServiceList> returnList = OpServiceList.ConvertList(dbConnectionCore.GetServiceListFilter(IdServiceList, IdDistributor, StartDate, EndDate, IdOperatorReceiver, IdOperatorServicer,
                             CourierName, Priority, ShippingLetterNbr, ExpectedFinishDate, ServiceName,
                             IdDocumentPackage, Confirmed, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceList item in returnList)
                        ServiceListDict[item.IdServiceList] = item;
                    ServiceListIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListDataFilter

        /// <summary>
        /// Gets ServiceListData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListData funcion</param>
        /// <param name="IdServiceListData">Specifies filter for ID_SERVICE_LIST_DATA column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListData list</returns>
        public virtual List<OpServiceListData> GetServiceListDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceListData = null, int[] IdServiceList = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDataFilter"));
            }

            try
            {
                List<OpServiceListData> returnList = OpServiceListData.ConvertList(dbConnectionCore.GetServiceListDataFilter(IdServiceListData, IdServiceList, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceListData item in returnList)
                        ServiceListDataDict[item.IdServiceListData] = item;
                    ServiceListDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListDeviceFilter

        /// <summary>
        /// Gets ServiceListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDevice funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="FailureDescription">Specifies filter for FAILURE_DESCRIPTION column</param>
        /// <param name="FailureSuggestion">Specifies filter for FAILURE_SUGGESTION column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="Photo">Specifies filter for PHOTO column</param>
        /// <param name="IdOperatorServicer">Specifies filter for ID_OPERATOR_SERVICER column</param>
        /// <param name="ServiceShortDescr">Specifies filter for SERVICE_SHORT_DESCR column</param>
        /// <param name="ServiceLongDescr">Specifies filter for SERVICE_LONG_DESCR column</param>
        /// <param name="PhotoServiced">Specifies filter for PHOTO_SERVICED column</param>
        /// <param name="AtexOk">Specifies filter for ATEX_OK column</param>
        /// <param name="PayForService">Specifies filter for PAY_FOR_SERVICE column</param>
        /// <param name="IdServiceStatus">Specifies filter for ID_SERVICE_STATUS column</param>
        /// <param name="IdDiagnosticStatus">Specifies filter for ID_DIAGNOSTIC_STATUS column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDevice list</returns>
        public virtual List<OpServiceListDevice> GetServiceListDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, long[] SerialNbr = null, string FailureDescription = null, string FailureSuggestion = null, string Notes = null, long[] Photo = null,
                            int[] IdOperatorServicer = null, string ServiceShortDescr = null, string ServiceLongDescr = null, long[] PhotoServiced = null, bool? AtexOk = null,
                            bool? PayForService = null, int[] IdServiceStatus = null, int[] IdDiagnosticStatus = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceFilter"));
            }

            try
            {
                List<OpServiceListDevice> returnList = OpServiceListDevice.ConvertList(dbConnectionCore.GetServiceListDeviceFilter(IdServiceList, SerialNbr, FailureDescription, FailureSuggestion, Notes, Photo,
                             IdOperatorServicer, ServiceShortDescr, ServiceLongDescr, PhotoServiced, AtexOk,
                             PayForService, IdServiceStatus, IdDiagnosticStatus, StartDate, EndDate,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceListDevice item in returnList)
                        ServiceListDeviceDict[new Tuple<int, long>(item.IdServiceList, item.SerialNbr)] = item;
                    ServiceListDeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListDeviceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListDeviceDataFilter

        /// <summary>
        /// Gets ServiceListDeviceData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceData funcion</param>
        /// <param name="IdServiceListDeviceData">Specifies filter for ID_SERVICE_LIST_DEVICE_DATA column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST_DEVICE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceData list</returns>
        public virtual List<OpServiceListDeviceData> GetServiceListDeviceDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceListDeviceData = null, int[] IdServiceList = null, long[] SerialNbr = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListDeviceDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceDataFilter"));
            }

            try
            {
                List<OpServiceListDeviceData> returnList = OpServiceListDeviceData.ConvertList(dbConnectionCore.GetServiceListDeviceDataFilter(IdServiceListDeviceData, IdServiceList, SerialNbr, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceListDeviceData item in returnList)
                        ServiceListDeviceDataDict[item.IdServiceListDeviceData] = item;
                    ServiceListDeviceDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListDeviceDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListDeviceActionFilter

        /// <summary>
        /// Gets ServiceListDeviceAction list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceAction funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdServiceCustom">Specifies filter for ID_SERVICE_CUSTOM column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceAction list</returns>
        public virtual List<OpServiceListDeviceAction> GetServiceListDeviceActionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdServiceList = null, long[] IdServicePackage = null, long[] IdServiceCustom = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListDeviceActionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceActionFilter"));
            }

            try
            {
                List<OpServiceListDeviceAction> returnList = OpServiceListDeviceAction.ConvertList(dbConnectionCore.GetServiceListDeviceActionFilter(SerialNbr, IdServiceList, IdServicePackage, IdServiceCustom, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceListDeviceAction item in returnList)
                        ServiceListDeviceActionDict[item.SerialNbr] = item;
                    ServiceListDeviceActionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListDeviceActionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListDeviceActionDetailsFilter

        /// <summary>
        /// Gets ServiceListDeviceActionDetails list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceActionDetails funcion</param>
        /// <param name="IdServiceListDeviceActionDetails">Specifies filter for ID_SERVICE_LIST_DEVICE_ACTION_DETAILS column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdService">Specifies filter for ID_SERVICE column</param>
        /// <param name="InputText">Specifies filter for INPUT_TEXT column</param>
        /// <param name="IdServiceActionResult">Specifies filter for ID_SERVICE_ACTION_RESULT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST_DEVICE_ACTION_DETAILS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceActionDetails list</returns>
        public virtual List<OpServiceListDeviceActionDetails> GetServiceListDeviceActionDetailsFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceListDeviceActionDetails = null, long[] SerialNbr = null, int[] IdServiceList = null, long[] IdServicePackage = null, int[] IdService = null, string InputText = null,
                            int[] IdServiceActionResult = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListDeviceActionDetailsFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceActionDetailsFilter"));
            }

            try
            {
                List<OpServiceListDeviceActionDetails> returnList = OpServiceListDeviceActionDetails.ConvertList(dbConnectionCore.GetServiceListDeviceActionDetailsFilter(IdServiceListDeviceActionDetails, SerialNbr, IdServiceList, IdServicePackage, IdService, InputText,
                             IdServiceActionResult, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceListDeviceActionDetails item in returnList)
                        ServiceListDeviceActionDetailsDict[item.IdServiceListDeviceActionDetails] = item;
                    ServiceListDeviceActionDetailsIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListDeviceActionDetailsFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceListDeviceDiagnosticFilter

        /// <summary>
        /// Gets ServiceListDeviceDiagnostic list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceListDeviceDiagnostic funcion</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdDiagnosticAction">Specifies filter for ID_DIAGNOSTIC_ACTION column</param>
        /// <param name="IdDiagnosticActionResult">Specifies filter for ID_DIAGNOSTIC_ACTION_RESULT column</param>
        /// <param name="InputText">Specifies filter for INPUT_TEXT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY SERIAL_NBR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceListDeviceDiagnostic list</returns>
        public virtual List<OpServiceListDeviceDiagnostic> GetServiceListDeviceDiagnosticFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] SerialNbr = null, int[] IdServiceList = null, int[] IdDiagnosticAction = null, int[] IdDiagnosticActionResult = null, string InputText = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceListDeviceDiagnosticFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceListDeviceDiagnosticFilter"));
            }

            try
            {
                List<OpServiceListDeviceDiagnostic> returnList = OpServiceListDeviceDiagnostic.ConvertList(dbConnectionCore.GetServiceListDeviceDiagnosticFilter(SerialNbr, IdServiceList, IdDiagnosticAction, IdDiagnosticActionResult, InputText, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceListDeviceDiagnostic item in returnList)
                        ServiceListDeviceDiagnosticDict[item.SerialNbr] = item;
                    ServiceListDeviceDiagnosticIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceListDeviceDiagnosticFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServicePackageFilter

        /// <summary>
        /// Gets ServicePackage list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServicePackage funcion</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="IsSingle">Specifies filter for IS_SINGLE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_PACKAGE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServicePackage list</returns>
        public virtual List<OpServicePackage> GetServicePackageFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServicePackage = null, int[] IdContract = null, string Name = null, string Descr = null, bool? IsSingle = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServicePackageFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServicePackageFilter"));
            }

            try
            {
                List<OpServicePackage> returnList = OpServicePackage.ConvertList(dbConnectionCore.GetServicePackageFilter(IdServicePackage, IdContract, Name, Descr, IsSingle, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServicePackage item in returnList)
                        ServicePackageDict[item.IdServicePackage] = item;
                    ServicePackageIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServicePackageFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServicePackageDataFilter

        /// <summary>
        /// Gets ServicePackageData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServicePackageData funcion</param>
        /// <param name="IdServicePackageData">Specifies filter for ID_SERVICE_PACKAGE_DATA column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="Min">Specifies filter for MIN column</param>
        /// <param name="Max">Specifies filter for MAX column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_PACKAGE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServicePackageData list</returns>
        public virtual List<OpServicePackageData> GetServicePackageDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServicePackageData = null, long[] IdServicePackage = null, long[] IdDataType = null, int[] IndexNbr = null, int[] Min = null, int[] Max = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServicePackageDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServicePackageDataFilter"));
            }

            try
            {
                List<OpServicePackageData> returnList = OpServicePackageData.ConvertList(dbConnectionCore.GetServicePackageDataFilter(IdServicePackageData, IdServicePackage, IdDataType, IndexNbr, Min, Max,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServicePackageData item in returnList)
                        ServicePackageDataDict[item.IdServicePackageData] = item;
                    ServicePackageDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServicePackageDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceReferenceTypeFilter

        /// <summary>
        /// Gets ServiceReferenceType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceReferenceType funcion</param>
        /// <param name="IdServiceReferenceType">Specifies filter for ID_SERVICE_REFERENCE_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_REFERENCE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceReferenceType list</returns>
        public virtual List<OpServiceReferenceType> GetServiceReferenceTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceReferenceType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceReferenceTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceReferenceTypeFilter"));
            }

            try
            {
                List<OpServiceReferenceType> returnList = OpServiceReferenceType.ConvertList(dbConnectionCore.GetServiceReferenceTypeFilter(IdServiceReferenceType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceReferenceType item in returnList)
                        ServiceReferenceTypeDict[item.IdServiceReferenceType] = item;
                    ServiceReferenceTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceReferenceTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceSummaryFilter

        /// <summary>
        /// Gets ServiceSummary list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceSummary funcion</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="Amount">Specifies filter for AMOUNT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceSummary list</returns>
        public virtual List<OpServiceSummary> GetServiceSummaryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdServiceList = null, long[] IdServicePackage = null, int[] Amount = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceSummaryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceSummaryFilter"));
            }

            try
            {
                List<OpServiceSummary> returnList = OpServiceSummary.ConvertList(dbConnectionCore.GetServiceSummaryFilter(IdServiceList, IdServicePackage, Amount, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceSummary item in returnList)
                        ServiceSummaryDict[item.IdServiceList] = item;
                    ServiceSummaryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceSummaryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetServiceSummaryDataFilter

        /// <summary>
        /// Gets ServiceSummaryData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllServiceSummaryData funcion</param>
        /// <param name="IdServiceSummaryData">Specifies filter for ID_SERVICE_SUMMARY_DATA column</param>
        /// <param name="IdServiceList">Specifies filter for ID_SERVICE_LIST column</param>
        /// <param name="IdServicePackage">Specifies filter for ID_SERVICE_PACKAGE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SERVICE_SUMMARY_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ServiceSummaryData list</returns>
        public virtual List<OpServiceSummaryData> GetServiceSummaryDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdServiceSummaryData = null, int[] IdServiceList = null, long[] IdServicePackage = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetServiceSummaryDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetServiceSummaryDataFilter"));
            }

            try
            {
                List<OpServiceSummaryData> returnList = OpServiceSummaryData.ConvertList(dbConnectionCore.GetServiceSummaryDataFilter(IdServiceSummaryData, IdServiceList, IdServicePackage, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpServiceSummaryData item in returnList)
                        ServiceSummaryDataDict[item.IdServiceSummaryData] = item;
                    ServiceSummaryDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetServiceSummaryDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSessionFilter

        /// <summary>
        /// Gets Session list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSession funcion</param>
        /// <param name="IdSession">Specifies filter for ID_SESSION column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="IdSessionStatus">Specifies filter for ID_SESSION_STATUS column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Session list</returns>
        public virtual List<OpSession> GetSessionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSession = null, int[] IdModule = null, int[] IdOperator = null, int[] IdSessionStatus = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSessionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionFilter"));
            }

            try
            {
                List<OpSession> returnList = OpSession.ConvertList(dbConnectionCore.GetSessionFilter(IdSession, IdModule, IdOperator, IdSessionStatus, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSession item in returnList)
                        SessionDict[item.IdSession] = item;
                    SessionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSessionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSessionDataFilter

        /// <summary>
        /// Gets SessionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionData funcion</param>
        /// <param name="IdSessionData">Specifies filter for ID_SESSION_DATA column</param>
        /// <param name="IdSession">Specifies filter for ID_SESSION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionData list</returns>
        public virtual List<OpSessionData> GetSessionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSessionData = null, long[] IdSession = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSessionDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionDataFilter"));
            }

            try
            {
                List<OpSessionData> returnList = OpSessionData.ConvertList(dbConnectionCore.GetSessionDataFilter(IdSessionData, IdSession, IdDataType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSessionData item in returnList)
                        SessionDataDict[item.IdSessionData] = item;
                    SessionDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSessionDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSessionEventFilter

        /// <summary>
        /// Gets SessionEvent list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionEvent funcion</param>
        /// <param name="IdSessionEvent">Specifies filter for ID_SESSION_EVENT column</param>
        /// <param name="IdSession">Specifies filter for ID_SESSION column</param>
        /// <param name="IdEvent">Specifies filter for ID_EVENT column</param>
        /// <param name="Time">Specifies filter for TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_EVENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionEvent list</returns>
        public virtual List<OpSessionEvent> GetSessionEventFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSessionEvent = null, long[] IdSession = null, int[] IdEvent = null, TypeDateTimeCode Time = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSessionEventFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionEventFilter"));
            }

            try
            {
                List<OpSessionEvent> returnList = OpSessionEvent.ConvertList(dbConnectionCore.GetSessionEventFilter(IdSessionEvent, IdSession, IdEvent, Time, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSessionEvent item in returnList)
                        SessionEventDict[item.IdSessionEvent] = item;
                    SessionEventIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSessionEventFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSessionEventDataFilter

        /// <summary>
        /// Gets SessionEventData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionEventData funcion</param>
        /// <param name="IdSessionEventData">Specifies filter for ID_SESSION_EVENT_DATA column</param>
        /// <param name="IdSessionEvent">Specifies filter for ID_SESSION_EVENT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_EVENT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionEventData list</returns>
        public virtual List<OpSessionEventData> GetSessionEventDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSessionEventData = null, long[] IdSessionEvent = null, long[] IdDataType = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSessionEventDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionEventDataFilter"));
            }

            try
            {
                List<OpSessionEventData> returnList = OpSessionEventData.ConvertList(dbConnectionCore.GetSessionEventDataFilter(IdSessionEventData, IdSessionEvent, IdDataType, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSessionEventData item in returnList)
                        SessionEventDataDict[item.IdSessionEventData] = item;
                    SessionEventDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSessionEventDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSessionStatusFilter

        /// <summary>
        /// Gets SessionStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSessionStatus funcion</param>
        /// <param name="IdSessionStatus">Specifies filter for ID_SESSION_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SESSION_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SessionStatus list</returns>
        public virtual List<OpSessionStatus> GetSessionStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSessionStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSessionStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSessionStatusFilter"));
            }

            try
            {
                List<OpSessionStatus> returnList = OpSessionStatus.ConvertList(dbConnectionCore.GetSessionStatusFilter(IdSessionStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSessionStatus item in returnList)
                        SessionStatusDict[item.IdSessionStatus] = item;
                    SessionStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSessionStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetShippingListFilter

        /// <summary>
        /// Gets ShippingList list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingList funcion</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="ContractNbr">Specifies filter for CONTRACT_NBR column</param>
        /// <param name="ShippingListNbr">Specifies filter for SHIPPING_LIST_NBR column</param>
        /// <param name="IdOperatorCreator">Specifies filter for ID_OPERATOR_CREATOR column</param>
        /// <param name="IdOperatorContact">Specifies filter for ID_OPERATOR_CONTACT column</param>
        /// <param name="IdOperatorQuality">Specifies filter for ID_OPERATOR_QUALITY column</param>
        /// <param name="IdOperatorProtocol">Specifies filter for ID_OPERATOR_PROTOCOL column</param>
        /// <param name="ShippingLetterNbr">Specifies filter for SHIPPING_LETTER_NBR column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdShippingListType">Specifies filter for ID_SHIPPING_LIST_TYPE column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="FinishDate">Specifies filter for FINISH_DATE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingList list</returns>
        public virtual List<OpShippingList> GetShippingListFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingList = null, string ContractNbr = null, string ShippingListNbr = null, int[] IdOperatorCreator = null, int[] IdOperatorContact = null, int[] IdOperatorQuality = null,
                            int[] IdOperatorProtocol = null, string ShippingLetterNbr = null, long[] IdLocation = null, int[] IdShippingListType = null, TypeDateTimeCode CreationDate = null,
                            TypeDateTimeCode FinishDate = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetShippingListFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListFilter"));
            }

            try
            {
                List<OpShippingList> returnList = OpShippingList.ConvertList(dbConnectionCore.GetShippingListFilter(IdShippingList, ContractNbr, ShippingListNbr, IdOperatorCreator, IdOperatorContact, IdOperatorQuality,
                             IdOperatorProtocol, ShippingLetterNbr, IdLocation, IdShippingListType, CreationDate,
                             FinishDate, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpShippingList item in returnList)
                        ShippingListDict[item.IdShippingList] = item;
                    ShippingListIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetShippingListFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetShippingListDataFilter

        /// <summary>
        /// Gets ShippingListData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListData funcion</param>
        /// <param name="IdShippingListData">Specifies filter for ID_SHIPPING_LIST_DATA column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListData list</returns>
        public virtual List<OpShippingListData> GetShippingListDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdShippingListData = null, int[] IdShippingList = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetShippingListDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListDataFilter"));
            }

            try
            {
                List<OpShippingListData> returnList = OpShippingListData.ConvertList(dbConnectionCore.GetShippingListDataFilter(IdShippingListData, IdShippingList, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpShippingListData item in returnList)
                        ShippingListDataDict[item.IdShippingListData] = item;
                    ShippingListDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetShippingListDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetShippingListDeviceFilter

        /// <summary>
        /// Gets ShippingListDevice list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListDevice funcion</param>
        /// <param name="IdShippingListDevice">Specifies filter for ID_SHIPPING_LIST_DEVICE column</param>
        /// <param name="IdShippingList">Specifies filter for ID_SHIPPING_LIST column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="IdMeter">Specifies filter for ID_METER column</param>
        /// <param name="IdArticle">Specifies filter for ID_ARTICLE column</param>
        /// <param name="InsertDate">Specifies filter for INSERT_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_DEVICE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListDevice list</returns>
        public virtual List<OpShippingListDevice> GetShippingListDeviceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingListDevice = null, int[] IdShippingList = null, long[] SerialNbr = null, long[] IdMeter = null, long[] IdArticle = null, TypeDateTimeCode InsertDate = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetShippingListDeviceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListDeviceFilter"));
            }

            try
            {
                List<OpShippingListDevice> returnList = OpShippingListDevice.ConvertList(dbConnectionCore.GetShippingListDeviceFilter(IdShippingListDevice, IdShippingList, SerialNbr, IdMeter, IdArticle, InsertDate,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpShippingListDevice item in returnList)
                        ShippingListDeviceDict[item.IdShippingListDevice] = item;
                    ShippingListDeviceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetShippingListDeviceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetShippingListTypeFilter

        /// <summary>
        /// Gets ShippingListType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllShippingListType funcion</param>
        /// <param name="IdShippingListType">Specifies filter for ID_SHIPPING_LIST_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SHIPPING_LIST_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ShippingListType list</returns>
        public virtual List<OpShippingListType> GetShippingListTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdShippingListType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetShippingListTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetShippingListTypeFilter"));
            }

            try
            {
                List<OpShippingListType> returnList = OpShippingListType.ConvertList(dbConnectionCore.GetShippingListTypeFilter(IdShippingListType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpShippingListType item in returnList)
                        ShippingListTypeDict[item.IdShippingListType] = item;
                    ShippingListTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetShippingListTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSimCardFilter

        /// <summary>
        /// Gets SimCard list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="loadCustomData">Indicates whether custom data should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSimCard funcion</param>
        /// <param name="IdSimCard">Specifies filter for ID_SIM_CARD column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="SerialNbr">Specifies filter for SERIAL_NBR column</param>
        /// <param name="Phone">Specifies filter for PHONE column</param>
        /// <param name="Pin">Specifies filter for PIN column</param>
        /// <param name="Puk">Specifies filter for PUK column</param>
        /// <param name="MobileNetworkCode">Specifies filter for MOBILE_NETWORK_CODE column</param>
        /// <param name="Ip">Specifies filter for IP column</param>
        /// <param name="ApnLogin">Specifies filter for APN_LOGIN column</param>
        /// <param name="ApnPassword">Specifies filter for APN_PASSWORD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SIM_CARD DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SimCard list</returns>
        public virtual List<OpSimCard> GetSimCardFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdSimCard = null, int[] IdDistributor = null, string SerialNbr = null, string Phone = null, string Pin = null, string Puk = null,
                            int[] MobileNetworkCode = null, string Ip = null, string ApnLogin = null, string ApnPassword = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSimCardFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSimCardFilter"));
            }

            try
            {
                List<OpSimCard> returnList = OpSimCard.ConvertList(dbConnectionCore.GetSimCardFilter(IdSimCard, IdDistributor, SerialNbr, Phone, Pin, Puk,
                        MobileNetworkCode, Ip, ApnLogin, ApnPassword, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, loadCustomData: loadCustomData, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpSimCard item in returnList)
                        SimCardDict[item.IdSimCard] = item;
                    SimCardIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSimCardFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSimCardDataFilter

        /// <summary>
        /// Gets SimCardData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSimCardData funcion</param>
        /// <param name="IdSimCardData">Specifies filter for ID_SIM_CARD_DATA column</param>
        /// <param name="IdSimCard">Specifies filter for ID_SIM_CARD column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SIM_CARD_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SimCardData list</returns>
        public virtual List<OpSimCardData> GetSimCardDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSimCardData = null, int[] IdSimCard = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSimCardDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSimCardDataFilter"));
            }

            try
            {
                List<OpSimCardData> returnList = OpSimCardData.ConvertList(dbConnectionCore.GetSimCardDataFilter(IdSimCardData, IdSimCard, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSimCardData item in returnList)
                        SimCardDataDict[item.IdSimCardData] = item;
                    SimCardDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSimCardDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSlaFilter

        /// <summary>
        /// Gets Sla list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSla funcion</param>
        /// <param name="IdSla">Specifies filter for ID_SLA column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IssueReceiveWd">Specifies filter for ISSUE_RECEIVE_WD column</param>
        /// <param name="IssueReceiveFd">Specifies filter for ISSUE_RECEIVE_FD column</param>
        /// <param name="TechnicalSupportWd">Specifies filter for TECHNICAL_SUPPORT_WD column</param>
        /// <param name="TechnicalSupportFd">Specifies filter for TECHNICAL_SUPPORT_FD column</param>
        /// <param name="IssueResponseWd">Specifies filter for ISSUE_RESPONSE_WD column</param>
        /// <param name="IssueResponseFd">Specifies filter for ISSUE_RESPONSE_FD column</param>
        /// <param name="SystemFailureRemoveWd">Specifies filter for SYSTEM_FAILURE_REMOVE_WD column</param>
        /// <param name="SystemFailureRemoveFd">Specifies filter for SYSTEM_FAILURE_REMOVE_FD column</param>
        /// <param name="ObjectFailureRemoveWd">Specifies filter for OBJECT_FAILURE_REMOVE_WD column</param>
        /// <param name="ObjectFailureRemoveFd">Specifies filter for OBJECT_FAILURE_REMOVE_FD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SLA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Sla list</returns>
        public virtual List<OpSla> GetSlaFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSla = null, int[] IdDescr = null, string Description = null, string IssueReceiveWd = null, string IssueReceiveFd = null, string TechnicalSupportWd = null,
                            string TechnicalSupportFd = null, int[] IssueResponseWd = null, int[] IssueResponseFd = null, int[] SystemFailureRemoveWd = null, int[] SystemFailureRemoveFd = null,
                            int[] ObjectFailureRemoveWd = null, int[] ObjectFailureRemoveFd = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSlaFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSlaFilter"));
            }

            try
            {
                List<OpSla> returnList = OpSla.ConvertList(dbConnectionCore.GetSlaFilter(IdSla, IdDescr, Description, IssueReceiveWd, IssueReceiveFd, TechnicalSupportWd,
                             TechnicalSupportFd, IssueResponseWd, IssueResponseFd, SystemFailureRemoveWd, SystemFailureRemoveFd,
                             ObjectFailureRemoveWd, ObjectFailureRemoveFd, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSla item in returnList)
                        SlaDict[item.IdSla] = item;
                    SlaIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSlaFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSlotTypeFilter

        /// <summary>
        /// Gets SlotType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSlotType funcion</param>
        /// <param name="IdSlotType">Specifies filter for ID_SLOT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SLOT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SlotType list</returns>
        public virtual List<OpSlotType> GetSlotTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSlotType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSlotTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSlotTypeFilter"));
            }

            try
            {
                List<OpSlotType> returnList = OpSlotType.ConvertList(dbConnectionCore.GetSlotTypeFilter(IdSlotType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSlotType item in returnList)
                        SlotTypeDict[item.IdSlotType] = item;
                    SlotTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSlotTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSupplierFilter

        /// <summary>
        /// Gets Supplier list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSupplier funcion</param>
        /// <param name="IdSupplier">Specifies filter for ID_SUPPLIER column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SUPPLIER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Supplier list</returns>
        public virtual List<OpSupplier> GetSupplierFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdSupplier = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSupplierFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSupplierFilter"));
            }

            try
            {
                List<OpSupplier> returnList = OpSupplier.ConvertList(dbConnectionCore.GetSupplierFilter(IdSupplier, Name, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSupplier item in returnList)
                        SupplierDict[item.IdSupplier] = item;
                    SupplierIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSupplierFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetSystemDataFilter

        /// <summary>
        /// Gets SystemData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllSystemData funcion</param>
        /// <param name="IdSystemData">Specifies filter for ID_SYSTEM_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_SYSTEM_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>SystemData list</returns>
        public virtual List<OpSystemData> GetSystemDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdSystemData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetSystemDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetSystemDataFilter"));
            }

            try
            {
                List<OpSystemData> returnList = OpSystemData.ConvertList(dbConnectionCore.GetSystemDataFilter(IdSystemData, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpSystemData item in returnList)
                        SystemDataDict[item.IdSystemData] = item;
                    SystemDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetSystemDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTariffFilter

        /// <summary>
        /// Gets Tariff list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTariff funcion</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TARIFF DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Tariff list</returns>
        public virtual List<OpTariff> GetTariffFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTariff = null, int[] IdDistributor = null, string Name = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTariffFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTariffFilter"));
            }

            try
            {
                List<OpTariff> returnList = OpTariff.ConvertList(dbConnectionCore.GetTariffFilter(IdTariff, IdDistributor, Name, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTariff item in returnList)
                        TariffDict[item.IdTariff] = item;
                    TariffIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTariffFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTariffDataFilter

        /// <summary>
        /// Gets TariffData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTariffData funcion</param>
        /// <param name="IdTariffData">Specifies filter for ID_TARIFF_DATA column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TARIFF_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TariffData list</returns>
        public virtual List<OpTariffData> GetTariffDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTariffData = null, int[] IdTariff = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTariffDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTariffDataFilter"));
            }

            try
            {
                List<OpTariffData> returnList = OpTariffData.ConvertList(dbConnectionCore.GetTariffDataFilter(IdTariffData, IdTariff, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpTariffData item in returnList)
                        TariffDataDict[item.IdTariffData] = item;
                    TariffDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTariffDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTariffSettlementPeriodFilter

        /// <summary>
        /// Gets TariffSettlementPeriod list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTariffSettlementPeriod funcion</param>
        /// <param name="IdTariffSettlementPeriod">Specifies filter for ID_TARIFF_SETTLEMENT_PERIOD column</param>
        /// <param name="IdTariff">Specifies filter for ID_TARIFF column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TARIFF_SETTLEMENT_PERIOD DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TariffSettlementPeriod list</returns>
        public virtual List<OpTariffSettlementPeriod> GetTariffSettlementPeriodFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTariffSettlementPeriod = null, int[] IdTariff = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTariffSettlementPeriodFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTariffSettlementPeriodFilter"));
            }

            try
            {
                List<OpTariffSettlementPeriod> returnList = OpTariffSettlementPeriod.ConvertList(dbConnectionCore.GetTariffSettlementPeriodFilter(IdTariffSettlementPeriod, IdTariff, StartTime, EndTime, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTariffSettlementPeriod item in returnList)
                        TariffSettlementPeriodDict[item.IdTariffSettlementPeriod] = item;
                    TariffSettlementPeriodIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTariffSettlementPeriodFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTaskFilter

        /// <summary>
        /// Gets Task list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTask funcion</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdTaskType">Specifies filter for ID_TASK_TYPE column</param>
        /// <param name="IdIssue">Specifies filter for ID_ISSUE column</param>
        /// <param name="IdTaskGroup">Specifies filter for ID_TASK_GROUP column</param>
        /// <param name="IdPlannedRoute">Specifies filter for ID_PLANNED_ROUTE column</param>
        /// <param name="IdOperatorRegistering">Specifies filter for ID_OPERATOR_REGISTERING column</param>
        /// <param name="IdOperatorPerformer">Specifies filter for ID_OPERATOR_PERFORMER column</param>
        /// <param name="IdTaskStatus">Specifies filter for ID_TASK_STATUS column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Accepted">Specifies filter for ACCEPTED column</param>
        /// <param name="IdOperatorAccepted">Specifies filter for ID_OPERATOR_ACCEPTED column</param>
        /// <param name="AcceptanceDate">Specifies filter for ACCEPTANCE_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="IdLocation">Specifies filter for ID_LOCATION column</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="TopicNumber">Specifies filter for TOPIC_NUMBER column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="OperationCode">Specifies filter for OPERATION_CODE column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Task list</returns>
        public virtual List<OpTask> GetTaskFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTask = null, int[] IdTaskType = null, int[] IdIssue = null, int[] IdTaskGroup = null, long[] IdPlannedRoute = null, int[] IdOperatorRegistering = null,
                            int[] IdOperatorPerformer = null, int[] IdTaskStatus = null, TypeDateTimeCode CreationDate = null, bool? Accepted = null, int[] IdOperatorAccepted = null,
                            TypeDateTimeCode AcceptanceDate = null, string Notes = null, TypeDateTimeCode Deadline = null, long[] IdLocation = null, int[] IdActor = null,
                            string TopicNumber = null, int[] Priority = null, long[] OperationCode = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0,
                            bool loadTaskHistory = false, bool loadCustomData = true, List<long> customDataTypes = null)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTaskFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskFilter"));
            }

            try
            {
                List<OpTask> returnList = OpTask.ConvertList(dbConnectionCore.GetTaskFilter(IdTask, IdTaskType, IdIssue, IdTaskGroup, IdPlannedRoute, IdOperatorRegistering,
                             IdOperatorPerformer, IdTaskStatus, CreationDate, Accepted, IdOperatorAccepted,
                             AcceptanceDate, Notes, Deadline, IdLocation, IdActor,
                             TopicNumber, Priority, OperationCode, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout),
                             this, loadNavigationProperties, loadTaskHistory, loadCustomData, customDataTypes: customDataTypes, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTask item in returnList)
                        TaskDict[item.IdTask] = item;
                    TaskIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTaskFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTaskDataFilter

        /// <summary>
        /// Gets TaskData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskData funcion</param>
        /// <param name="IdTaskData">Specifies filter for ID_TASK_DATA column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskData list</returns>
        public virtual List<OpTaskData> GetTaskDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdTaskData = null, int[] IdTask = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTaskDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskDataFilter"));
            }

            try
            {
                List<OpTaskData> returnList = OpTaskData.ConvertList(dbConnectionCore.GetTaskDataFilter(IdTaskData, IdTask, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpTaskData item in returnList)
                        TaskDataDict[item.IdTaskData] = item;
                    TaskDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTaskDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTaskGroupFilter

        /// <summary>
        /// Gets TaskGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskGroup funcion</param>
        /// <param name="IdTaskGroup">Specifies filter for ID_TASK_GROUP column</param>
        /// <param name="IdParrentGroup">Specifies filter for ID_PARRENT_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="DateCreated">Specifies filter for DATE_CREATED column</param>
        /// <param name="IdDistributor">Specifies filter for ID_DISTRIBUTOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskGroup list</returns>
        public virtual List<OpTaskGroup> GetTaskGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskGroup = null, int[] IdParrentGroup = null, string Name = null, TypeDateTimeCode DateCreated = null, int[] IdDistributor = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTaskGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskGroupFilter"));
            }

            try
            {
                List<OpTaskGroup> returnList = OpTaskGroup.ConvertList(dbConnectionCore.GetTaskGroupFilter(IdTaskGroup, IdParrentGroup, Name, DateCreated, IdDistributor, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTaskGroup item in returnList)
                        TaskGroupDict[item.IdTaskGroup] = item;
                    TaskGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTaskGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTaskHistoryFilter

        /// <summary>
        /// Gets TaskHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskHistory funcion</param>
        /// <param name="IdTaskHistory">Specifies filter for ID_TASK_HISTORY column</param>
        /// <param name="IdTask">Specifies filter for ID_TASK column</param>
        /// <param name="IdTaskStatus">Specifies filter for ID_TASK_STATUS column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="StartDate">Specifies filter for START_DATE column</param>
        /// <param name="EndDate">Specifies filter for END_DATE column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskHistory list</returns>
        public virtual List<OpTaskHistory> GetTaskHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskHistory = null, int[] IdTask = null, int[] IdTaskStatus = null, int[] IdOperator = null, TypeDateTimeCode StartDate = null, TypeDateTimeCode EndDate = null,
                            string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTaskHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskHistoryFilter"));
            }

            try
            {
                List<OpTaskHistory> returnList = OpTaskHistory.ConvertList(dbConnectionCore.GetTaskHistoryFilter(IdTaskHistory, IdTask, IdTaskStatus, IdOperator, StartDate, EndDate,
                             Notes, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTaskHistory item in returnList)
                        TaskHistoryDict[item.IdTaskHistory] = item;
                    TaskHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTaskHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTaskStatusFilter

        /// <summary>
        /// Gets TaskStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskStatus funcion</param>
        /// <param name="IdTaskStatus">Specifies filter for ID_TASK_STATUS column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="Description">Specifies filter for DESCRIPTION column</param>
        /// <param name="IsFinished">Specifies filter for IS_FINISHED column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskStatus list</returns>
        public virtual List<OpTaskStatus> GetTaskStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskStatus = null, long[] IdDescr = null, string Description = null, bool? IsFinished = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTaskStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskStatusFilter"));
            }

            try
            {
                List<OpTaskStatus> returnList = OpTaskStatus.ConvertList(dbConnectionCore.GetTaskStatusFilter(IdTaskStatus, IdDescr, Description, IsFinished, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTaskStatus item in returnList)
                        TaskStatusDict[item.IdTaskStatus] = item;
                    TaskStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTaskStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTaskTypeFilter

        /// <summary>
        /// Gets TaskType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskType funcion</param>
        /// <param name="IdTaskType">Specifies filter for ID_TASK_TYPE column</param>
        /// <param name="IdTaskTypeGroup">Specifies filter for ID_TASK_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="Remote">Specifies filter for REMOTE column</param>
        /// <param name="DurationEstimate">Specifies filter for DURATION_ESTIMATE column</param>
        /// <param name="IdOperatorCreator">Specifies filter for ID_OPERATOR_CREATOR column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="Authorized">Specifies filter for AUTHORIZED column</param>
        /// <param name="ImrOperator">Specifies filter for IMR_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskType list</returns>
        public virtual List<OpTaskType> GetTaskTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskType = null, int[] IdTaskTypeGroup = null, string Name = null, string Descr = null, bool? Remote = null, int[] DurationEstimate = null,
                            int[] IdOperatorCreator = null, TypeDateTimeCode CreationDate = null, bool? Authorized = null, bool? ImrOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTaskTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskTypeFilter"));
            }

            try
            {
                List<OpTaskType> returnList = OpTaskType.ConvertList(dbConnectionCore.GetTaskTypeFilter(IdTaskType, IdTaskTypeGroup, Name, Descr, Remote, DurationEstimate,
                             IdOperatorCreator, CreationDate, Authorized, ImrOperator, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTaskType item in returnList)
                        TaskTypeDict[item.IdTaskType] = item;
                    TaskTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTaskTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTaskTypeGroupFilter

        /// <summary>
        /// Gets TaskTypeGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTaskTypeGroup funcion</param>
        /// <param name="IdTaskTypeGroup">Specifies filter for ID_TASK_TYPE_GROUP column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Descr">Specifies filter for DESCR column</param>
        /// <param name="IdTaskTypeGroupParent">Specifies filter for ID_TASK_TYPE_GROUP_PARENT column</param>
        /// <param name="ILeft">Specifies filter for I_LEFT column</param>
        /// <param name="IRight">Specifies filter for I_RIGHT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TASK_TYPE_GROUP DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TaskTypeGroup list</returns>
        public virtual List<OpTaskTypeGroup> GetTaskTypeGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTaskTypeGroup = null, string Name = null, string Descr = null, int[] IdTaskTypeGroupParent = null, int[] ILeft = null, int[] IRight = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTaskTypeGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTaskTypeGroupFilter"));
            }

            try
            {
                List<OpTaskTypeGroup> returnList = OpTaskTypeGroup.ConvertList(dbConnectionCore.GetTaskTypeGroupFilter(IdTaskTypeGroup, Name, Descr, IdTaskTypeGroupParent, ILeft, IRight,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpTaskTypeGroup item in returnList)
                        TaskTypeGroupDict[item.IdTaskTypeGroup] = item;
                    TaskTypeGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTaskTypeGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTransmissionStatusFilter

        /// <summary>
        /// Gets TransmissionStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionStatus funcion</param>
        /// <param name="IdTransmissionStatus">Specifies filter for ID_TRANSMISSION_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionStatus list</returns>
        public virtual List<OpTransmissionStatus> GetTransmissionStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTransmissionStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionStatusFilter"));
            }

            try
            {
                List<OpTransmissionStatus> returnList = OpTransmissionStatus.ConvertList(dbConnectionCore.GetTransmissionStatusFilter(IdTransmissionStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpTransmissionStatus item in returnList)
                        TransmissionStatusDict[item.IdTransmissionStatus] = item;
                    TransmissionStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionStatusFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetTransmissionTypeFilter

        /// <summary>
        /// Gets TransmissionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllTransmissionType funcion</param>
        /// <param name="IdTransmissionType">Specifies filter for ID_TRANSMISSION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_TRANSMISSION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>TransmissionType list</returns>
        public virtual List<OpTransmissionType> GetTransmissionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdTransmissionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetTransmissionTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetTransmissionTypeFilter"));
            }

            try
            {
                List<OpTransmissionType> returnList = OpTransmissionType.ConvertList(dbConnectionCore.GetTransmissionTypeFilter(IdTransmissionType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpTransmissionType item in returnList)
                        TransmissionTypeDict[item.IdTransmissionType] = item;
                    TransmissionTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetTransmissionTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetUniqueTypeFilter

        /// <summary>
        /// Gets UniqueType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllUniqueType funcion</param>
        /// <param name="IdUniqueType">Specifies filter for ID_UNIQUE_TYPE column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="ProcName">Specifies filter for PROC_NAME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_UNIQUE_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>UniqueType list</returns>
        public virtual List<OpUniqueType> GetUniqueTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdUniqueType = null, long[] IdDescr = null, string ProcName = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetUniqueTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetUniqueTypeFilter"));
            }

            try
            {
                List<OpUniqueType> returnList = OpUniqueType.ConvertList(dbConnectionCore.GetUniqueTypeFilter(IdUniqueType, IdDescr, ProcName, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpUniqueType item in returnList)
                        UniqueTypeDict[item.IdUniqueType] = item;
                    UniqueTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetUniqueTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetUnitFilter

        /// <summary>
        /// Gets Unit list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllUnit funcion</param>
        /// <param name="IdUnit">Specifies filter for ID_UNIT column</param>
        /// <param name="IdUnitBase">Specifies filter for ID_UNIT_BASE column</param>
        /// <param name="Scale">Specifies filter for SCALE column</param>
        /// <param name="Bias">Specifies filter for BIAS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_UNIT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Unit list</returns>
        public virtual List<OpUnit> GetUnitFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdUnit = null, int[] IdUnitBase = null, Double[] Scale = null, Double[] Bias = null, string Name = null, long[] IdDescr = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetUnitFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetUnitFilter"));
            }

            try
            {
                List<OpUnit> returnList = OpUnit.ConvertList(dbConnectionCore.GetUnitFilter(IdUnit, IdUnitBase, Scale, Bias, Name, IdDescr,
                             topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpUnit item in returnList)
                        UnitDict[item.IdUnit] = item;
                    UnitIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetUnitFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionTypeFilter
        /// <summary>
        /// Gets VersionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionType funcion</param>
        /// <param name="IdVersionType">Specifies filter for ID_VERSION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionType list</returns>
        public virtual List<OpVersionType> GetVersionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionTypeFilter"));
            }

            try
            {
                List<OpVersionType> returnList = OpVersionType.ConvertList(dbConnectionCore.GetVersionTypeFilter(IdVersionType, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionType item in returnList)
                        VersionTypeDict[item.IdVersionType] = item;
                    VersionTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionStateFilter
        /// <summary>
        /// Gets VersionState list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionState funcion</param>
        /// <param name="IdVersionState">Specifies filter for ID_VERSION_STATE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_STATE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionState list</returns>
        public virtual List<OpVersionState> GetVersionStateFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionState = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionStateFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionStateFilter"));
            }

            try
            {
                List<OpVersionState> returnList = OpVersionState.ConvertList(dbConnectionCore.GetVersionStateFilter(IdVersionState, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionState item in returnList)
                        VersionStateDict[item.IdVersionState] = item;
                    VersionStateIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionStateFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionRequirementFilter
        /// <summary>
        /// Gets VersionRequirement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionRequirement funcion</param>
        /// <param name="IdVersionRequirement">Specifies filter for ID_VERSION_REQUIREMENT column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdVersionRequired">Specifies filter for ID_VERSION_REQUIRED column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_REQUIREMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionRequirement list</returns>
        public virtual List<OpVersionRequirement> GetVersionRequirementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionRequirement = null, long[] IdVersion = null, long[] IdVersionRequired = null, int[] IdOperator = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionRequirementFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionRequirementFilter"));
            }

            try
            {
                List<OpVersionRequirement> returnList = OpVersionRequirement.ConvertList(dbConnectionCore.GetVersionRequirementFilter(IdVersionRequirement, IdVersion, IdVersionRequired, IdOperator, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionRequirement item in returnList)
                        VersionRequirementDict[item.IdVersionRequirement] = item;
                    VersionRequirementIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionRequirementFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionNbrFormatFilter
        /// <summary>
        /// Gets VersionNbrFormat list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionNbrFormat funcion</param>
        /// <param name="IdVersionNbrFormat">Specifies filter for ID_VERSION_NBR_FORMAT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdDataTypeClass">Specifies filter for ID_DATA_TYPE_CLASS column</param>
        /// <param name="RegularExpression">Specifies filter for REGULAR_EXPRESSION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_NBR_FORMAT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionNbrFormat list</returns>
        public virtual List<OpVersionNbrFormat> GetVersionNbrFormatFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionNbrFormat = null, string Name = null, long[] IdDescr = null, int[] IdDataTypeClass = null, string RegularExpression = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionNbrFormatFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionNbrFormatFilter"));
            }

            try
            {
                List<OpVersionNbrFormat> returnList = OpVersionNbrFormat.ConvertList(dbConnectionCore.GetVersionNbrFormatFilter(IdVersionNbrFormat, Name, IdDescr, IdDataTypeClass, RegularExpression, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionNbrFormat item in returnList)
                        VersionNbrFormatDict[item.IdVersionNbrFormat] = item;
                    VersionNbrFormatIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionNbrFormatFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionHistoryFilter
        /// <summary>
        /// Gets VersionHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionHistory funcion</param>
        /// <param name="IdVersionHistory">Specifies filter for ID_VERSION_HISTORY column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdVersionState">Specifies filter for ID_VERSION_STATE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="Notes">Specifies filter for NOTES column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionHistory list</returns>
        public virtual List<OpVersionHistory> GetVersionHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionHistory = null, long[] IdVersion = null, int[] IdVersionState = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, int[] IdOperator = null,
                            string Notes = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionHistoryFilter"));
            }

            try
            {
                List<OpVersionHistory> returnList = OpVersionHistory.ConvertList(dbConnectionCore.GetVersionHistoryFilter(IdVersionHistory, IdVersion, IdVersionState, StartTime, EndTime, IdOperator,
                             Notes, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionHistory item in returnList)
                        VersionHistoryDict[item.IdVersionHistory] = item;
                    VersionHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionElementTypeHierarchyFilter
        /// <summary>
        /// Gets VersionElementTypeHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementTypeHierarchy funcion</param>
        /// <param name="IdVersionElementTypeHierarchy">Specifies filter for ID_VERSION_ELEMENT_TYPE_HIERARCHY column</param>
        /// <param name="IdVersionElementTypeHierarchyParent">Specifies filter for ID_VERSION_ELEMENT_TYPE_HIERARCHY_PARENT column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="Required">Specifies filter for REQUIRED column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_TYPE_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementTypeHierarchy list</returns>
        public virtual List<OpVersionElementTypeHierarchy> GetVersionElementTypeHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionElementTypeHierarchy = null, int[] IdVersionElementTypeHierarchyParent = null, int[] IdVersionElementType = null, bool? Required = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionElementTypeHierarchyFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementTypeHierarchyFilter"));
            }

            try
            {
                List<OpVersionElementTypeHierarchy> returnList = OpVersionElementTypeHierarchy.ConvertList(dbConnectionCore.GetVersionElementTypeHierarchyFilter(IdVersionElementTypeHierarchy, IdVersionElementTypeHierarchyParent, IdVersionElementType, Required, Hierarchy, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionElementTypeHierarchy item in returnList)
                        VersionElementTypeHierarchyDict[item.IdVersionElementTypeHierarchy] = item;
                    VersionElementTypeHierarchyIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionElementTypeHierarchyFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionElementTypeFilter
        /// <summary>
        /// Gets VersionElementType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementType funcion</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementType list</returns>
        public virtual List<OpVersionElementType> GetVersionElementTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionElementType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionElementTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementTypeFilter"));
            }

            try
            {
                List<OpVersionElementType> returnList = OpVersionElementType.ConvertList(dbConnectionCore.GetVersionElementTypeFilter(IdVersionElementType, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionElementType item in returnList)
                        VersionElementTypeDict[item.IdVersionElementType] = item;
                    VersionElementTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionElementTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionElementTypeDataFilter

        /// <summary>
        /// Gets VersionElementTypeData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementTypeData funcion</param>
        /// <param name="IdVersionElementTypeData">Specifies filter for ID_VERSION_ELEMENT_TYPE_DATA column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_TYPE_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementTypeData list</returns>
        public virtual List<OpVersionElementTypeData> GetVersionElementTypeDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementTypeData = null, int[] IdVersionElementType = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionElementTypeDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementTypeDataFilter"));
            }

            try
            {
                List<OpVersionElementTypeData> returnList = OpVersionElementTypeData.ConvertList(dbConnectionCore.GetVersionElementTypeDataFilter(IdVersionElementTypeData, IdVersionElementType, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionElementTypeData item in returnList)
                        VersionElementTypeDataDict[item.IdVersionElementTypeData] = item;
                    VersionElementTypeDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionElementTypeDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionElementHistoryFilter
        /// <summary>
        /// Gets VersionElementHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementHistory funcion</param>
        /// <param name="IdVersionElementHistory">Specifies filter for ID_VERSION_ELEMENT_HISTORY column</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementHistory list</returns>
        public virtual List<OpVersionElementHistory> GetVersionElementHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementHistory = null, int[] IdVersionElement = null, long[] IdVersion = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, int[] IdOperator = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionElementHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementHistoryFilter"));
            }

            try
            {
                List<OpVersionElementHistory> returnList = OpVersionElementHistory.ConvertList(dbConnectionCore.GetVersionElementHistoryFilter(IdVersionElementHistory, IdVersionElement, IdVersion, StartTime, EndTime, IdOperator,
                             topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionElementHistory item in returnList)
                        VersionElementHistoryDict[item.IdVersionElementHistory] = item;
                    VersionElementHistoryIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionElementHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionElementHierarchyFilter
        /// <summary>
        /// Gets VersionElementHierarchy list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementHierarchy funcion</param>
        /// <param name="IdVersionElementHierarchy">Specifies filter for ID_VERSION_ELEMENT_HIERARCHY column</param>
        /// <param name="IdVersionElementHierarchyParent">Specifies filter for ID_VERSION_ELEMENT_HIERARCHY_PARENT column</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="Hierarchy">Specifies filter for HIERARCHY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_HIERARCHY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementHierarchy list</returns>
        public virtual List<OpVersionElementHierarchy> GetVersionElementHierarchyFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementHierarchy = null, long[] IdVersionElementHierarchyParent = null, int[] IdVersionElement = null, string Hierarchy = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionElementHierarchyFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementHierarchyFilter"));
            }

            try
            {
                List<OpVersionElementHierarchy> returnList = OpVersionElementHierarchy.ConvertList(dbConnectionCore.GetVersionElementHierarchyFilter(IdVersionElementHierarchy, IdVersionElementHierarchyParent, IdVersionElement, Hierarchy, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionElementHierarchy item in returnList)
                        VersionElementHierarchyDict[item.IdVersionElementHierarchy] = item;
                    VersionElementHierarchyIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionElementHierarchyFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionElementDataFilter
        /// <summary>
        /// Gets VersionElementData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElementData funcion</param>
        /// <param name="IdVersionElementData">Specifies filter for ID_VERSION_ELEMENT_DATA column</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElementData list</returns>
        public virtual List<OpVersionElementData> GetVersionElementDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionElementData = null, int[] IdVersionElement = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionElementDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementDataFilter"));
            }

            try
            {
                List<OpVersionElementData> returnList = OpVersionElementData.ConvertList(dbConnectionCore.GetVersionElementDataFilter(IdVersionElementData, IdVersionElement, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionElementData item in returnList)
                        VersionElementDataDict[item.IdVersionElementData] = item;
                    VersionElementDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionElementDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionElementFilter
        /// <summary>
        /// Gets VersionElement list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionElement funcion</param>
        /// <param name="IdVersionElement">Specifies filter for ID_VERSION_ELEMENT column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="IdVersionNbrFormat">Specifies filter for ID_VERSION_NBR_FORMAT column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="ReferenceType">Specifies filter for REFERENCE_TYPE column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_ELEMENT DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionElement list</returns>
        public virtual List<OpVersionElement> GetVersionElementFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdVersionElement = null, int[] IdVersionElementType = null, int[] IdVersionNbrFormat = null, string Name = null, long[] IdDescr = null, int[] ReferenceType = null,
                            long[] IdVersion = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionElementFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionElementFilter"));
            }

            try
            {
                List<OpVersionElement> returnList = OpVersionElement.ConvertList(dbConnectionCore.GetVersionElementFilter(IdVersionElement, IdVersionElementType, IdVersionNbrFormat, Name, IdDescr, ReferenceType,
                             IdVersion, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionElement item in returnList)
                        VersionElementDict[item.IdVersionElement] = item;
                    VersionElementIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionElementFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionDataFilter
        /// <summary>
        /// Gets VersionData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersionData funcion</param>
        /// <param name="IdVersionData">Specifies filter for ID_VERSION_DATA column</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>VersionData list</returns>
        public virtual List<OpVersionData> GetVersionDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersionData = null, long[] IdVersion = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionDataFilter"));
            }

            try
            {
                List<OpVersionData> returnList = OpVersionData.ConvertList(dbConnectionCore.GetVersionDataFilter(IdVersionData, IdVersion, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersionData item in returnList)
                        VersionDataDict[item.IdVersionData] = item;
                    VersionDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetVersionFilter
        /// <summary>
        /// Gets Version list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllVersion funcion</param>
        /// <param name="IdVersion">Specifies filter for ID_VERSION column</param>
        /// <param name="IdVersionState">Specifies filter for ID_VERSION_STATE column</param>
        /// <param name="IdVersionElementType">Specifies filter for ID_VERSION_ELEMENT_TYPE column</param>
        /// <param name="IdVersionType">Specifies filter for ID_VERSION_TYPE column</param>
        /// <param name="VersionNbr">Specifies filter for VERSION_NBR column</param>
        /// <param name="VersionRaw">Specifies filter for VERSION_RAW column</param>
        /// <param name="ReleaseDate">Specifies filter for RELEASE_DATE column</param>
        /// <param name="IdOperatorPublisher">Specifies filter for ID_OPERATOR_PUBLISHER column</param>
        /// <param name="NotificationDate">Specifies filter for NOTIFICATION_DATE column</param>
        /// <param name="IdOperatorNotifier">Specifies filter for ID_OPERATOR_NOTIFIER column</param>
        /// <param name="ConfirmDate">Specifies filter for CONFIRM_DATE column</param>
        /// <param name="IdOperatorCofirming">Specifies filter for ID_OPERATOR_COFIRMING column</param>
        /// <param name="ReleaseNotes">Specifies filter for RELEASE_NOTES column</param>
        /// <param name="IdPriority">Specifies filter for ID_PRIORITY column</param>
        /// <param name="Deadline">Specifies filter for DEADLINE column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VERSION DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Version list</returns>
        public virtual List<OpVersion> GetVersionFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdVersion = null, int[] IdVersionState = null, int[] IdVersionElementType = null, int[] IdVersionType = null, string VersionNbr = null, long[] VersionRaw = null, TypeDateTimeCode ReleaseDate = null, int[] IdOperatorPublisher = null, TypeDateTimeCode NotificationDate = null, int[] IdOperatorNotifier = null, TypeDateTimeCode ConfirmDate = null,
                                                int[] IdOperatorCofirming = null, string ReleaseNotes = null, int[] IdPriority = null, TypeDateTimeCode Deadline = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null,
                                                long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetVersionFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetVersionFilter"));
            }

            try
            {
                List<OpVersion> returnList = OpVersion.ConvertList(dbConnectionCore.GetVersionFilter(IdVersion, IdVersionState, IdVersionElementType, IdVersionType, VersionNbr, VersionRaw,
                             ReleaseDate, IdOperatorPublisher, NotificationDate, IdOperatorNotifier, ConfirmDate,
                             IdOperatorCofirming, ReleaseNotes, IdPriority, Deadline, StartTime,
                             EndTime, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpVersion item in returnList)
                        VersionDict[item.IdVersion] = item;
                    VersionIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetVersionFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetViewFilter

        /// <summary>
        /// Gets View list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllView funcion</param>
        /// <param name="IdView">Specifies filter for ID_VIEW column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdReferenceType">Specifies filter for ID_REFERENCE_TYPE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>View list</returns>
        public List<OpView> GetViewFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdView = null, string Name = null, int[] IdReferenceType = null, long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetViewFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewFilter"));
            }

            try
            {
                List<OpView> returnList = OpView.ConvertList(dbConnectionCore.GetViewFilter(IdView, Name, IdReferenceType, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), 
                    this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes,
                    autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpView item in returnList)
                        ViewDict[item.IdView] = item;
                    ViewIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetViewFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetViewDataFilter

        /// <summary>
        /// Gets ViewData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewData funcion</param>
        /// <param name="IdViewData">Specifies filter for ID_VIEW_DATA column</param>
        /// <param name="IdView">Specifies filter for ID_VIEW column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewData list</returns>
        public List<OpViewData> GetViewDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdViewData = null, int[] IdView = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetViewDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewDataFilter"));
            }

            try
            {
                List<OpViewData> returnList = OpViewData.ConvertList(dbConnectionCore.GetViewDataFilter(IdViewData, IdView, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpViewData item in returnList)
                        ViewDataDict[item.IdViewData] = item;
                    ViewDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetViewDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetViewColumnFilter

        /// <summary>
        /// Gets ViewColumn list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumn funcion</param>
        /// <param name="IdViewColumn">Specifies filter for ID_VIEW_COLUMN column</param>
        /// <param name="IdView">Specifies filter for ID_VIEW column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="Table">Specifies filter for TABLE column</param>
        /// <param name="PrimaryKey">Specifies filter for PRIMARY_KEY column</param>
        /// <param name="Size">Specifies filter for SIZE column</param>
        /// <param name="IdViewColumnSource">Specifies filter for ID_VIEW_COLUMN_SOURCE column</param>
        /// <param name="IdViewColumnType">Specifies filter for ID_VIEW_COLUMN_TYPE column</param>
        /// <param name="Key">Specifies filter for KEY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumn list</returns>
        public List<OpViewColumn> GetViewColumnFilter(bool loadNavigationProperties = true, bool loadCustomData = true, bool mergeIntoCache = false, int[] IdViewColumn = null, int[] IdView = null, long[] IdDataType = null, string Name = null, string Table = null, bool? PrimaryKey = null,
                            long[] Size = null, int[] IdViewColumnSource = null, int[] IdViewColumnType = null, bool? Key = null, int[] IdKeyViewColumn = null,
                            long? topCount = null, string customWhereClause = null, List<long> customDataTypes = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetViewColumnFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnFilter"));
            }

            try
            {
                List<OpViewColumn> returnList = OpViewColumn.ConvertList(dbConnectionCore.GetViewColumnFilter(IdViewColumn, IdView, IdDataType, Name, Table, PrimaryKey,
                             Size, IdViewColumnSource, IdViewColumnType, Key, IdKeyViewColumn,
                             topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout),
                        this, loadNavigationProperties: loadNavigationProperties, loadCustomData: loadCustomData, customDataTypes: customDataTypes,
                        autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpViewColumn item in returnList)
                        ViewColumnDict[item.IdViewColumn] = item;
                    ViewColumnIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetViewColumnFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetViewColumnDataFilter

        /// <summary>
        /// Gets ViewColumnData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumnData funcion</param>
        /// <param name="IdViewColumnData">Specifies filter for ID_VIEW_COLUMN_DATA column</param>
        /// <param name="IdViewColumn">Specifies filter for ID_VIEW_COLUMN column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumnData list</returns>
        public List<OpViewColumnData> GetViewColumnDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdViewColumnData = null, int[] IdViewColumn = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetViewColumnDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnDataFilter"));
            }

            try
            {
                List<OpViewColumnData> returnList = OpViewColumnData.ConvertList(dbConnectionCore.GetViewColumnDataFilter(IdViewColumnData, IdViewColumn, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpViewColumnData item in returnList)
                        ViewColumnDataDict[item.IdViewColumnData] = item;
                    ViewColumnDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetViewColumnDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetViewColumnSourceFilter

        /// <summary>
        /// Gets ViewColumnSource list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumnSource funcion</param>
        /// <param name="IdViewColumnSource">Specifies filter for ID_VIEW_COLUMN_SOURCE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN_SOURCE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumnSource list</returns>
        public List<OpViewColumnSource> GetViewColumnSourceFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdViewColumnSource = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetViewColumnSourceFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnSourceFilter"));
            }

            try
            {
                List<OpViewColumnSource> returnList = OpViewColumnSource.ConvertList(dbConnectionCore.GetViewColumnSourceFilter(IdViewColumnSource, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpViewColumnSource item in returnList)
                        ViewColumnSourceDict[item.IdViewColumnSource] = item;
                    ViewColumnSourceIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetViewColumnSourceFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetViewColumnTypeFilter

        /// <summary>
        /// Gets ViewColumnType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllViewColumnType funcion</param>
        /// <param name="IdViewColumnType">Specifies filter for ID_VIEW_COLUMN_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_VIEW_COLUMN_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ViewColumnType list</returns>
        public List<OpViewColumnType> GetViewColumnTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdViewColumnType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetViewColumnTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetViewColumnTypeFilter"));
            }

            try
            {
                List<OpViewColumnType> returnList = OpViewColumnType.ConvertList(dbConnectionCore.GetViewColumnTypeFilter(IdViewColumnType, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpViewColumnType item in returnList)
                        ViewColumnTypeDict[item.IdViewColumnType] = item;
                    ViewColumnTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetViewColumnTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetWarrantyContractFilter

        /// <summary>
        /// Gets WarrantyContract list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWarrantyContract funcion</param>
        /// <param name="IdDeviceOrderNumber">Specifies filter for ID_DEVICE_ORDER_NUMBER column</param>
        /// <param name="IdComponent">Specifies filter for ID_COMPONENT column</param>
        /// <param name="IdContract">Specifies filter for ID_CONTRACT column</param>
        /// <param name="WarrantyTime">Specifies filter for WARRANTY_TIME column</param>
        /// <param name="IdWarrantyStartDateCalculationMethod">Specifies filter for ID_WARRANTY_START_DATE_CALCULATION_METHOD column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_DEVICE_ORDER_NUMBER DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WarrantyContract list</returns>
        public virtual List<OpWarrantyContract> GetWarrantyContractFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdDeviceOrderNumber = null, int[] IdComponent = null, int[] IdContract = null, int[] WarrantyTime = null, int[] IdWarrantyStartDateCalculationMethod = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWarrantyContractFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWarrantyContractFilter"));
            }

            try
            {
                List<OpWarrantyContract> returnList = OpWarrantyContract.ConvertList(dbConnectionCore.GetWarrantyContractFilter(IdDeviceOrderNumber, IdComponent, IdContract, WarrantyTime, IdWarrantyStartDateCalculationMethod, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpWarrantyContract item in returnList)
                        WarrantyContractDict[item.IdDeviceOrderNumber] = item;
                    WarrantyContractIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWarrantyContractFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetWmbusTransmissionTypeFilter

        /// <summary>
        /// Gets WmbusTransmissionType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWmbusTransmissionType funcion</param>
        /// <param name="IdWmbusTransmissionType">Specifies filter for ID_WMBUS_TRANSMISSION_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WMBUS_TRANSMISSION_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WmbusTransmissionType list</returns>
        public virtual List<OpWmbusTransmissionType> GetWmbusTransmissionTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWmbusTransmissionType = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWmbusTransmissionTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWmbusTransmissionTypeFilter"));
            }

            try
            {
                List<OpWmbusTransmissionType> returnList = OpWmbusTransmissionType.ConvertList(dbConnectionCore.GetWmbusTransmissionTypeFilter(IdWmbusTransmissionType, Name, IdDescr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpWmbusTransmissionType item in returnList)
                        WmbusTransmissionTypeDict[item.IdWmbusTransmissionType] = item;
                    WmbusTransmissionTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWmbusTransmissionTypeFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetWorkFilter
        /// <summary>
        /// Gets Work list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWork funcion</param>
        /// <param name="IdWork">Specifies filter for ID_WORK column</param>
        /// <param name="IdWorkType">Specifies filter for ID_WORK_TYPE column</param>
        /// <param name="IdWorkStatus">Specifies filter for ID_WORK_STATUS column</param>
        /// <param name="IdWorkData">Specifies filter for ID_WORK_DATA column</param>
        /// <param name="IdWorkParent">Specifies filter for ID_WORK_PARENT column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="IdOperator">Specifies filter for ID_OPERATOR column</param>
        /// <param name="CreationDate">Specifies filter for CREATION_DATE column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>Work list</returns>
        public virtual List<OpWork> GetWorkFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWork = null, long[] IdWorkType = null, int[] IdWorkStatus = null,
                            long[] IdWorkData = null, long[] IdWorkParent = null, int[] IdModule = null, int[] IdOperator = null,
                            TypeDateTimeCode CreationDate = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWorkFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkFilter"));
            }

            try
            {
                List<OpWork> returnList = OpWork.ConvertList(dbConnectionCore.GetWorkFilter(IdWork, IdWorkType, IdWorkStatus,
                             IdWorkData, IdWorkParent, IdModule, IdOperator,
                             CreationDate, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpWork item in returnList)
                        WorkDict[item.IdWork] = item;
                    WorkIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetWorkHistoryFilter
        /// <summary>
        /// Gets WorkHistory list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkHistory funcion</param>
        /// <param name="IdWorkHistory">Specifies filter for ID_WORK_HISTORY column</param>
        /// <param name="IdWork">Specifies filter for ID_WORK column</param>
        /// <param name="IdWorkStatus">Specifies filter for ID_WORK_STATUS column</param>
        /// <param name="StartTime">Specifies filter for START_TIME column</param>
        /// <param name="EndTime">Specifies filter for END_TIME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="IdWorkHistoryData">Specifies filter for ID_WORK_HISTORY_DATA column</param>
        /// <param name="IdModule">Specifies filter for ID_MODULE column</param>
        /// <param name="Assembly">Specifies filter for ASSEMBLY column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_HISTORY DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether objects should be loaded from connected database or from DBCollector web service</param>
        /// <returns>WorkHistory list</returns>
        public virtual List<OpWorkHistory> GetWorkHistoryFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWorkHistory = null, long[] IdWork = null, int[] IdWorkStatus = null, TypeDateTimeCode StartTime = null, TypeDateTimeCode EndTime = null, long[] IdDescr = null,
                                                                int[] IdModule = null, string Assembly = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWorkHistoryFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkHistoryFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    //List<OpWorkHistory> returnList = OpWorkHistory.ConvertList(dbCollectorClient.GetWorkHistoryFilter(ref dbCollectorSession, IdWorkHistory,
                    //    IdWork, IdWorkStatus, StartTime, EndTime, IdDescr, IdWorkHistoryData, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    //return returnList;
                    return new List<OpWorkHistory>();
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpWorkHistory> returnList = OpWorkHistory.ConvertList(dbConnectionCore.GetWorkHistoryFilter(IdWorkHistory, IdWork, IdWorkStatus, StartTime, EndTime, IdDescr,
                                 IdModule, Assembly, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    if (mergeIntoCache)
                    {
                        foreach (OpWorkHistory item in returnList)
                            WorkHistoryDict[item.IdWorkHistory] = item;
                        WorkHistoryIsFullCache = true;
                    }
                    return returnList;
                }
                else
                    return new List<OpWorkHistory>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkHistoryFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetWorkHistoryDataFilter
        /// <summary>
        /// Gets WorkHistoryData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkHistoryData funcion</param>
        /// <param name="IdWorkHistoryData">Specifies filter for ID_WORK_HISTORY_DATA column</param>
        /// <param name="IdWorkHistory">Specifies filter for ID_WORK_HISTORY column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_HISTORY_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkHistoryData list</returns>
        public List<OpWorkHistoryData> GetWorkHistoryDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdWorkHistoryData = null, long[] IdWorkHistory = null, int[] ArgNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = false, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWorkHistoryDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkHistoryDataFilter"));
            }

            try
            {
                List<OpWorkHistoryData> returnList = OpWorkHistoryData.ConvertList(dbConnectionCore.GetWorkHistoryDataFilter(IdWorkHistoryData, IdWorkHistory, ArgNbr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpWorkHistoryData item in returnList)
                        WorkHistoryDataDict[item.IdWorkHistoryData] = item;
                    WorkHistoryDataIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkHistoryDataFilter", ex);
                throw;
            }
        }
        #endregion
        #region GetWorkStatusFilter
        /// <summary>
        /// Gets WorkStatus list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkStatus funcion</param>
        /// <param name="IdWorkStatus">Specifies filter for ID_WORK_STATUS column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_STATUS DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkStatus list</returns>
        public virtual List<OpWorkStatus> GetWorkStatusFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdWorkStatus = null, string Name = null, long[] IdDescr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWorkStatusFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkStatusFilter"));
            }

            try
            {
                List<OpWorkStatus> returnList = OpWorkStatus.ConvertList(dbConnectionCore.GetWorkStatusFilter(IdWorkStatus, Name, IdDescr, topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpWorkStatus item in returnList)
                        WorkStatusDict[item.IdWorkStatus] = item;
                    WorkStatusIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkStatusFilter", ex);
                throw;
            }
        }
        #endregion
        #region GetWorkTypeFilter
        /// <summary>
        /// Gets WorkType list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkType funcion</param>
        /// <param name="IdWorkType">Specifies filter for ID_WORK_TYPE column</param>
        /// <param name="Name">Specifies filter for NAME column</param>
        /// <param name="PluginName">Specifies filter for PLUGIN_NAME column</param>
        /// <param name="IdDescr">Specifies filter for ID_DESCR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_TYPE DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkType list</returns>
        public virtual List<OpWorkType> GetWorkTypeFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdWorkType = null, string Name = null, string PluginName = null, long[] IdDescr = null,
                             long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWorkTypeFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkTypeFilter"));
            }

            try
            {
                List<OpWorkType> returnList = OpWorkType.ConvertList(dbConnectionCore.GetWorkTypeFilter(IdWorkType, Name, PluginName, IdDescr,
                     topCount, customWhereClause, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout), this, loadNavigationProperties: loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpWorkType item in returnList)
                        WorkTypeDict[item.IdWorkType] = item;
                    WorkTypeIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkTypeFilter", ex);
                throw;
            }

        }
        #endregion

        #region GetWorkOrderJciFilter

        /// <summary>
        /// Gets WorkOrderJci list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllWorkOrderJci funcion</param>
        /// <param name="IdWorkOrderJci">Specifies filter for ID_WORK_ORDER_JCI column</param>
        /// <param name="Wonum">Specifies filter for WONUM column</param>
        /// <param name="Sentdatetime">Specifies filter for SENTDATETIME column</param>
        /// <param name="Wodesc">Specifies filter for WODESC column</param>
        /// <param name="Ivrcode">Specifies filter for IVRCODE column</param>
        /// <param name="Subcasdesc">Specifies filter for SUBCASDESC column</param>
        /// <param name="Problemdesc">Specifies filter for PROBLEMDESC column</param>
        /// <param name="Priority">Specifies filter for PRIORITY column</param>
        /// <param name="Typedesc">Specifies filter for TYPEDESC column</param>
        /// <param name="Statusdesc">Specifies filter for STATUSDESC column</param>
        /// <param name="Reportdatetime">Specifies filter for REPORTDATETIME column</param>
        /// <param name="Rfdatetime">Specifies filter for RFDATETIME column</param>
        /// <param name="Ponumber">Specifies filter for PONUMBER column</param>
        /// <param name="Supplierid">Specifies filter for SUPPLIERID column</param>
        /// <param name="Suppliername">Specifies filter for SUPPLIERNAME column</param>
        /// <param name="Siteid">Specifies filter for SITEID column</param>
        /// <param name="Sitename">Specifies filter for SITENAME column</param>
        /// <param name="Sitephone">Specifies filter for SITEPHONE column</param>
        /// <param name="Equipid">Specifies filter for EQUIPID column</param>
        /// <param name="Equipdesc">Specifies filter for EQUIPDESC column</param>
        /// <param name="Downtime">Specifies filter for DOWNTIME column</param>
        /// <param name="Lumpsum">Specifies filter for LUMPSUM column</param>
        /// <param name="Disclaimertext">Specifies filter for DISCLAIMER_TEXT column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_ORDER_JCI DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>WorkOrderJci list</returns>
        public virtual List<OpWorkOrderJci> GetWorkOrderJciFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdWorkOrderJci = null, string Wonum = null, TypeDateTimeCode Sentdatetime = null, string Wodesc = null, string Ivrcode = null, string Subcasdesc = null,
                            string Problemdesc = null, int[] Priority = null, string Typedesc = null, string Statusdesc = null, TypeDateTimeCode Reportdatetime = null,
                            TypeDateTimeCode Rfdatetime = null, string Ponumber = null, string Supplierid = null, string Suppliername = null, string Siteid = null,
                            string Sitename = null, string Sitephone = null, string Equipid = null, string Equipdesc = null, string Downtime = null,
                            string Lumpsum = null, string Disclaimertext = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWorkOrderJciFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkOrderJciFilter"));
            }

            try
            {
                List<OpWorkOrderJci> returnList = OpWorkOrderJci.ConvertList(dbConnectionCore.GetWorkOrderJciFilter(IdWorkOrderJci, Wonum, Sentdatetime, Wodesc, Ivrcode, Subcasdesc,
                             Problemdesc, Priority, Typedesc, Statusdesc, Reportdatetime,
                             Rfdatetime, Ponumber, Supplierid, Suppliername, Siteid,
                             Sitename, Sitephone, Equipid, Equipdesc, Downtime,
                             Lumpsum, Disclaimertext, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties, autoTransaction: autoTransaction, transactionLevel: transactionLevel, commandTimeout: commandTimeout);
                if (mergeIntoCache)
                {
                    foreach (OpWorkOrderJci item in returnList)
                        WorkOrderJciDict[item.IdWorkOrderJci] = item;
                    WorkOrderJciIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkOrderJciFilter", ex);
                throw;
            }

        }
        #endregion
    }

    //User Part
    public partial class DataProvider
    {
        #region GetActorInGroupFilter

        /// <summary>
        /// Gets ActorInGroup list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActorInGroup funcion</param>
        /// <param name="IdActor">Specifies filter for ID_ACTOR column</param>
        /// <param name="IdActorGroup">Specifies filter for ID_ACTOR_GROUP column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTOR DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActorInGroup list</returns>
        public virtual List<OpActorInGroup> GetActorInGroupFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, int[] IdActor = null, int[] IdActorGroup = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActorInGroupFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActorInGroupFilter"));
            }

            try
            {
                List<OpActorInGroup> returnList = OpActorInGroup.ConvertList(dbConnectionCore.GetActorInGroupFilter(IdActor, IdActorGroup, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpActorInGroup item in returnList)
                    {
                        if (!ActorInGroupDict.ContainsKey(item.IdActorGroup))
                            ActorInGroupDict.Add(item.IdActorGroup, new List<OpActorInGroup>());

                        ActorInGroupDict[item.IdActorGroup].Add(item);
                    }
                    ActorInGroupIsFullCache = true;
                }
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActorInGroupFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetActionHistoryDataFilter
        /// <summary>
        /// Gets ActionHistoryData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="mergeIntoCache">Indicated whether query result is merged into cache and available through GetAllActionHistoryData funcion</param>
        /// <param name="IdActionHistoryData">Specifies filter for ID_ACTION_HISTORY_DATA column</param>
        /// <param name="ArgNbr">Specifies filter for ARG_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_ACTION_HISTORY_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <returns>ActionHistoryData list</returns>
        public virtual List<OpActionHistoryData> GetActionHistoryDataFilter(bool loadNavigationProperties = true, bool mergeIntoCache = false, long[] IdActionHistoryData = null, int[] ArgNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetActionHistoryDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetActionHistoryDataFilter"));
            }

            try
            {
                List<OpActionHistoryData> returnList = OpActionHistoryData.ConvertList(dbConnectionCore.GetActionHistoryDataFilter(IdActionHistoryData, ArgNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                if (mergeIntoCache)
                {
                    foreach (OpActionHistoryData item in returnList)
                    {
                        if (!ActionHistoryDataDict.ContainsKey(item.IdActionHistoryData))
                            ActionHistoryDataDict.Add(item.IdActionHistoryData, new List<OpActionHistoryData>());

                        ActionHistoryDataDict[item.IdActionHistoryData].Add(item);
                    }
                    ActorInGroupIsFullCache = true;
                } 
                return returnList;
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetActionHistoryDataFilter", ex);
                throw;
            }

        }
        #endregion
        #region GetWorkDataFilter
        /// <summary>
        /// Gets WorkData list filtered (database query)
        /// </summary>
        /// <param name="loadNavigationProperties">Indicates whether navigation properties should be loaded</param>
        /// <param name="IdWorkData">Specifies filter for ID_WORK_DATA column</param>
        /// <param name="IdDataType">Specifies filter for ID_DATA_TYPE column</param>
        /// <param name="IndexNbr">Specifies filter for INDEX_NBR column</param>
        /// <param name="topCount">Specifies number of rows returned by query (SELECT TOP(topCount)). Additional sorting is applied to the query (ORDER BY ID_WORK_DATA DESC)</param>
        /// <param name="customWhereClause">Specifies additional filter criteria (SQL format)</param>
        /// <param name="autoTransaction">Indicates whether query is executed within transaction (Default is true)</param>
        /// <param name="transactionLevel">Specifies transaction level if autoTransaction is enabled</param>
        /// <param name="commandTimeout">Specifies query timeout in seconds. If not specified, default timeout is applied</param>
        /// <param name="useDBCollector">Indicates whether data should be loaded from connected data base or from DBCollector web service</param>
        /// <returns>WorkData list</returns>
        public virtual List<OpWorkData> GetWorkDataFilter(bool loadNavigationProperties = true, long[] IdWorkData = null, long[] IdDataType = null, int[] IndexNbr = null, long? topCount = null, string customWhereClause = null, bool autoTransaction = true, IsolationLevel transactionLevel = IsolationLevel.ReadUncommitted, int commandTimeout = 0, bool useDBCollector = false)
        {
            if (!String.IsNullOrEmpty(customWhereClause) && customWhereClause.Length > 4000)
            {
                IMRLog.AddToLog(EventID.DataProvider.CustomWhereClauseOutOfRange, "GetWorkDataFilter");
                throw new ApplicationException(String.Format("Parameter 'customWhereClause' exceed maximum allowed length (4000) during execution {0}", "GetWorkDataFilter"));
            }

            try
            {
                bool? dbCollectorEnabled = IsDBCollectorEnabled(useDBCollector);
                if (dbCollectorEnabled.HasValue && dbCollectorEnabled.Value)
                {
                    //List<OpWorkData> returnList = OpWorkData.ConvertList(dbCollectorClient.GetWorkDataFilter(ref dbCollectorSession, IdWorkData, IdDataType, IndexNbr,
                    //    topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    //return returnList;
                    return new List<OpWorkData>();
                }
                else if (dbCollectorEnabled.HasValue && !dbCollectorEnabled.Value)
                {
                    List<OpWorkData> returnList = OpWorkData.ConvertList(dbConnectionCore.GetWorkDataFilter(IdWorkData, IdDataType, IndexNbr, topCount, customWhereClause, autoTransaction, transactionLevel, commandTimeout), this, loadNavigationProperties);
                    return returnList;
                }
                else
                    return new List<OpWorkData>();
            }
            catch (Exception ex)
            {
                IMRLog.AddToLog(EventID.DataProvider.StoredProcedure, "imrse_GetWorkDataFilter", ex);
                throw;
            }
        }
        #endregion
    }
}
