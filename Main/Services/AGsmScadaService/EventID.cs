﻿using IMR.Suite.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMR.Suite.Services.AGsmScada
{
    public class EventID
    {
        #region AGsmScada   1000-1099
        public class AGsmScada
        {
            public static readonly LogData ModuleUp = new LogData(1000, LogLevel.Info, "Module {0} started at {1}.");
            public static readonly LogData ModuleUpError = new LogData(1001, LogLevel.Fatal, "Initialization failed. Exception: '{0}'.");
            public static readonly LogData ModuleDown = new LogData(1002, LogLevel.Info, "Module {0} stopped at {1}.");
            public static readonly LogData DBConnectionCreated = new LogData(1003, LogLevel.Info, "Successfully created connection to {0} server {1} database.");
            public static readonly LogData DataProviderCreated = new LogData(1004, LogLevel.Info, "Successfully created DataProvider.");

            public static readonly LogData ModbusSlaveRequestReceived = new LogData(1100, LogLevel.Info, "ModbusSlaveRequest: {0} from {1}");
            public static readonly LogData DataStoreReadFrom = new LogData(1101, LogLevel.Info, "DataStoreReadFrom: ModbusDataType:{0} Address:{1} Count:{2}");

        }
        #endregion

        public class WorkerTaskProc
        {
            public static readonly LogData TaskStart = new LogData(3000, LogLevel.Info, "WorkerTask start");
            public static readonly LogData TaskEnd = new LogData(3001, LogLevel.Info, "WorkerTask end");
            public static readonly LogData Exception = new LogData(3002, LogLevel.Fatal, "WorkerTask Exception: {0}");
            public static readonly LogData StopEventSignaled = new LogData(3003, LogLevel.Info, "WorkerTask Stop Event are signaled");

            public static readonly LogData FillHoldingRegistersException = new LogData(3010, LogLevel.Error, "Exception durring Fill Modbus Registers for IdLocations={0}, EX={1}");
        }
    }
}
