﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;

using IMR.Suite.Common;
using IMR.Suite.Data.DB;
using IMR.Suite.UI.Business;
using IMR.Suite.UI.Business.Components.CORE;
using IMR.Suite.UI.Business.Objects;
using IMR.Suite.UI.Business.Objects.CORE;

using Modbus.Data;
using Modbus.Device;
using Modbus.Utility;

using DW = IMR.Suite.UI.Business.Objects.DW;

namespace IMR.Suite.Services.AGsmScada
{
    public class AGsmScada
    {
        #region Members
        private IDataProvider DataProvider { get; set; }
        private string CultureName { get; set; }
        private int TimeZoneOffset = 0;

        int DataArchRefreshTime = 1;
        string ImrUserName = @"AGSMSCADA";
        string ImrUserPassword = @"bdf99542927dfa27253d64ae9c57db89";

        int ListenPort = 50502;
        string ListenIP = "127.0.0.1";

        internal ModbusSlave slave;

        internal Task WorkerTask = null;
        internal readonly ManualResetEvent StopEvent = new ManualResetEvent(false);
        internal DateTime LastWorkedTime = DateTime.UtcNow;
        #endregion
        #region Properties

        #endregion



        #region Constructor
        public AGsmScada()
        {
        }
        #endregion
        #region Start
        public void Start()
        {
            ListenPort = Convert.ToInt32(ConfigurationManager.AppSettings["ListenPort"]);
            ListenIP = ConfigurationManager.AppSettings["ListenIP"];
            if (ConfigurationManager.AppSettings.AllKeys.Contains("DataArchRefreshTime"))
                DataArchRefreshTime = Convert.ToInt32(ConfigurationManager.AppSettings["DataArchRefreshTime"]);
            if (ConfigurationManager.AppSettings.AllKeys.Contains("ImrUserName"))
                ImrUserName = ConfigurationManager.AppSettings["ImrUserName"];
            if (ConfigurationManager.AppSettings.AllKeys.Contains("ImrUserPassword"))
                ImrUserPassword = ConfigurationManager.AppSettings["ImrUserPassword"];

            #region DBConnection
            DBCommonCORE dbConnectionCore = null;
            DBCommonDW dbConnectionDw = null;
            DBCommonDAQ dbConnectionDaq = null;

            dbConnectionCore = new DBCommonCORE("Suite.CORE", 60, 720);
            if (!dbConnectionCore.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB CORE on server: {0}", dbConnectionCore.DB.Server));
            }
            Log(EventID.AGsmScada.DBConnectionCreated, dbConnectionCore.DB.Server, "CORE");

            dbConnectionDw = new DBCommonDW("Suite.DW", 60, 720); ;
            if (!dbConnectionDw.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB DW on server: {0}", dbConnectionDw.DB.Server));
            }
            Log(EventID.AGsmScada.DBConnectionCreated, dbConnectionDw.DB.Server, "DW");

            dbConnectionDaq = new DBCommonDAQ("Suite.DAQ", 60, 720); ;
            if (!dbConnectionDaq.Connected)
            {
                throw new Exception(string.Format("Could not connect to DB DAQ on server: {0}", dbConnectionDaq.DB.Server));
            }
            Log(EventID.AGsmScada.DBConnectionCreated, dbConnectionDaq.DB.Server, "DAQ");

            this.DataProvider = new DataProvider(dbConnectionCore, dbConnectionDw, dbConnectionDaq,
                !String.IsNullOrEmpty(this.CultureName) ? Enums.Language.Default.GetLanguage(this.CultureName) : Enums.Language.Default,
                (this.TimeZoneOffset == 0 ? null : (TimeSpan?)TimeSpan.FromMinutes(this.TimeZoneOffset)));

            Log(EventID.AGsmScada.DataProviderCreated);
            #endregion
            #region DataProviderInitialization
            //this.DataProvider.IDeviceDataTypes = new List<long>()
            //    {
            //    };
            #endregion
            #region Słowniki
            System.Threading.Tasks.Task tt = System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                DataProvider.GetAllDescr();
                DataProvider.GetAllUnit();
                DataProvider.GetAllLanguage();
                DataProvider.GetAllReferenceType();
                DataProvider.GetAllDataTypeClass();
                DataProvider.GetAllDataType();
                DataProvider.GetAllMeterTypeClass();
                DataProvider.GetAllMeterType();
                DataProvider.GetAllLocationType();
            });
            System.Threading.Tasks.Task.WaitAll(tt);
            #endregion

            long? idSession = null; int? idOperator = null;
            List<Tuple<int, object, bool>> permissionsToCheck = new List<Tuple<int, object, bool>>()
                        {
                            new Tuple<int, object, bool>(Activity.LOGIN, null, false),
                        };
            Enums.OperatorLoginStatus loginStatus = OperatorComponent.Login(DataProvider as DataProvider, ImrUserName, ImrUserPassword, true, (int)Enums.Module.AGsmScada, ref idSession, ref idOperator, permissionsToCheck,
                                                                            checkValidity: false, checkBlockedOperator: false, checkFailedLogin: false, checkPasswordSalt: false, checkLogInSession: false);
            if (loginStatus != Enums.OperatorLoginStatus.Valid)
            {
                throw new Exception();
            }

            OpOperator loggedOperator = DataProvider.GetOperatorFilter(IdOperator: new int[] { idOperator.Value }, loadNavigationProperties: true).First();
            //loggedOperator.Roles = OperatorComponent.GetRoles(DataProvider as DataProvider, loggedOperator, Enums.Module.AGsmScada);
            //loggedOperator.Activities = OperatorComponent.GetActivities(DataProvider as DataProvider, loggedOperator, loggedOperator.Roles, Enums.Module.AGsmScada);

            int[] allowedDistributors = RoleComponent.FilterByPermission(DataProvider.GetAllDistributor(), Activity.ALLOWED_DISTRIBUTOR, loggedOperator).Select(w => w.IdDistributor).ToArray();
            if (allowedDistributors.Length == 0)
                allowedDistributors = new int[] { loggedOperator.IdDistributor };

            if (DataProvider.dbConnectionCore != null)
                DataProvider.dbConnectionCore.DistributorFilter = allowedDistributors;
            if (DataProvider.dbConnectionDw != null)
                DataProvider.dbConnectionDw.DistributorFilter = allowedDistributors;
            if (DataProvider.dbConnectionDaq != null)
                DataProvider.dbConnectionDaq.DistributorFilter = allowedDistributors;



            // create and start the TCP slave
            TcpListener slaveTcpListener = new TcpListener(IPAddress.Parse(ListenIP), ListenPort);
            slaveTcpListener.Start();

            byte slaveId = 1;
            slave = ModbusTcpSlave.CreateTcp(slaveId, slaveTcpListener);
            slave.ModbusSlaveRequestReceived += OnModbusSlaveRequestReceived;
            slave.DataStore = DataStoreFactory.CreateDefaultDataStore();
            slave.DataStore.DataStoreReadFrom += OnDataStoreReadFrom;

            Task.Factory.StartNew(() => { slave.ListenAsync().GetAwaiter().GetResult(); });
            if (WorkerTask != null)
                Stop();
            WorkerTask = System.Threading.Tasks.Task.Factory.StartNew(() => WorkerTaskProc(), CancellationToken.None, System.Threading.Tasks.TaskCreationOptions.LongRunning, System.Threading.Tasks.TaskScheduler.Default);
        }
        #endregion
        #region Stop
        public void Stop()
        {
            StopEvent.Set();
            if (WorkerTask != null)
                WorkerTask = null;
        }
        #endregion


        #region OnModbusSlaveRequestReceived
        private void OnModbusSlaveRequestReceived(object sender, ModbusSlaveRequestEventArgs e)
        {
            Log(EventID.AGsmScada.ModbusSlaveRequestReceived, e.Message, e.Message.SlaveAddress);
        }
        #endregion
        #region OnDataStoreReadFrom
        private void OnDataStoreReadFrom(object sender, DataStoreEventArgs e)
        {
            Log(EventID.AGsmScada.DataStoreReadFrom, e.ModbusDataType, e.StartAddress, e.Data.Option == DiscriminatedUnionOption.A ? e.Data.A.Count : e.Data.B.Count);
        }
        #endregion

        #region WorkerTaskProc
        internal void WorkerTaskProc()
        {
            Log(EventID.WorkerTaskProc.TaskStart);

            WaitHandle[] Events = new WaitHandle[] { StopEvent };

            while (!StopEvent.WaitOne(0, false))
            {
                try
                {
                    if (WaitHandle.WaitAny(Events, 1, false) == 0) // StopEvent
                        break;

                    #region GetLocations & LocationsEquipments
                    List<OpLocation> opLocations = DataProvider.GetLocationFilter(IdLocation: null, loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false, autoTransaction: false);
                    // Jeśli potrzeba będzie fitrowania po AllowedLoaction to trzeba będzie zrobić jak w TMUserPortal, na razie filtrujemy tylko po LocationStateType
                    opLocations = opLocations.Where(w => w.IdLocationType.In((int)Enums.LocationType.CustomerLocation) && w.IdLocationStateType.In((int)Enums.LocationState.NEW, (int)Enums.LocationState.SCHEDULED, (int)Enums.LocationState.PENDING, (int)Enums.LocationState.OPERATIONAL, (int)Enums.LocationState.SUSPENDED)).Distinct(d => d.IdLocation).ToList();
                    DataProvider.GetLocationFilter(IdLocation: opLocations.Select(s => s.IdLocation).ToArray(), loadNavigationProperties: true, loadCustomData: true, mergeIntoCache: true, autoTransaction: false);
                    List<OpLocationEquipment> opLocationEquipments = DataProvider.GetLocationEquipmentFilter(loadNavigationProperties: true, loadCustomData: true, mergeIntoCache: false, autoTransaction: false,
                                                                                                             IdLocation: opLocations.Select(s => s.IdLocation).ToArray(),
                                                                                                             EndTime: Data.DB.TypeDateTimeCode.Null());
                    #endregion
                    #region GetDevices
                    List<OpDevice> opDevices = DataProvider.GetDeviceFilter(loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false, autoTransaction: false, SerialNbr: null).Where(w => w.SerialNbr != w.SerialNbrPattern).ToList();
                    List<OpLocationEquipment> opLocationEquipmentsForDevices = opLocationEquipments.Join(opLocations, p => p.IdLocation, q => q.IdLocation, (p, q) => p)
                                                                                                   .Join(opDevices, p => p.SerialNbr, q => q.SerialNbr, (p, q) => p).ToList();
                    opDevices = opDevices.Join(opLocationEquipmentsForDevices, q => q.SerialNbr, p => p.SerialNbr, (q, p) => q).ToList();
                    DataProvider.GetDeviceFilter(loadNavigationProperties: true, loadCustomData: true, mergeIntoCache: true, autoTransaction: false, SerialNbr: opDevices.Select(s => s.SerialNbr).ToArray());
                    #endregion
                    #region GetMeters
                    List<OpMeter> opMeters = DataProvider.GetMeterFilter(loadNavigationProperties: false, loadCustomData: false, mergeIntoCache: false, autoTransaction: false, IdMeter: null);
                    // Na razie nie filtruję po IdMeterTypeClass
                    //Dictionary<int, OpMeterType> MeterTypeDict = DataProvider.GetAllMeterType().ToDictionary(l => l.IdMeterType);
                    //opMeters.ForEach(f => f.MeterType = MeterTypeDict.TryGetValue(f.IdMeterType));
                    //opMeters = opMeters.Where(w => w.MeterType.IdMeterTypeClass.In(new int[] { (int)Enums.MeterTypeClass.I2CPressure, (int)Enums.MeterTypeClass.I2CPressureTemperature })).ToList();
                    List<OpLocationEquipment> opLocationEquipmentsForMeter = opLocationEquipments.Join(opLocations, p => p.IdLocation, q => q.IdLocation, (p, q) => p)
                                                                                                 .Join(opMeters, p => p.IdMeter, q => q.IdMeter, (p, q) => p).ToList();
                    opMeters = opMeters.Join(opLocationEquipmentsForMeter, q => q.IdMeter, p => p.IdMeter, (q, p) => q).ToList();
                    DataProvider.GetMeterFilter(loadNavigationProperties: true, loadCustomData: true, mergeIntoCache: true, autoTransaction: false, IdMeter: opMeters.Select(s => s.IdMeter).ToArray()).ToList();
                    #endregion
                    #region Get configs (data) for Meters
                    List<OpData> dataForMeters = DataProvider.GetDataFilter(SerialNbr: null, IdLocation: null,
                        IdMeter: opMeters.Select(s => s.IdMeter).ToArray(),
                        IdDataType: new long[]
                           {
                             DataType.METER_LO_UP_EVENT_ENABLE,
                             DataType.METER_LOLO_UP_EVENT_ENABLE,
                             DataType.METER_HI_UP_EVENT_ENABLE,
                             DataType.METER_HIHI_UP_EVENT_ENABLE,
                             DataType.METER_HIHI_PRESSURE_VALUE,
                             DataType.METER_HI_PRESSURE_VALUE,
                             DataType.METER_LOLO_PRESSURE_VALUE,
                             DataType.METER_LO_PRESSURE_VALUE,
                           }
                       );
                    #endregion

                    #region GetLatestData for Locations and Meters
                    List<IMR.Suite.UI.Business.Objects.DW.OpData> latestDataForLocations = DataProvider.GetDataFilterDW(SerialNbr: null, IdMeter: null,
                        IdLocation: opLocations.Select(s => s.IdLocation).ToArray(),
                        IdDataType: new long[]
                            {
                              DataType.GSM_QUALITY,
                              DataType.BATTERY_PERCENTAGE_USAGE,
                            }
                        );

                    List<IMR.Suite.UI.Business.Objects.DW.OpData> latestDataForMeters = DataProvider.GetDataFilterDW(SerialNbr: null, IdLocation: null,
                        IdMeter: opMeters.Select(s => s.IdMeter).ToArray(),
                        IdDataType: new long[]
                           {
                             DataType.AVG_PRESSURE,
                             //DataType.ANALOG_VALUE_LO_UP_EVENT,
                             //DataType.ANALOG_VALUE_LOLO_UP_EVENT,
                             //DataType.ANALOG_VALUE_HI_UP_EVENT,
                             //DataType.ANALOG_VALUE_HIHI_UP_EVENT,
                             //DataType.DEVICE_ANALOG_CONFIG_LO_DOWN_EVENT_ENABLE,
                             //DataType.DEVICE_ANALOG_CONFIG_LOLO_DOWN_EVENT_ENABLE,
                             //DataType.DEVICE_ANALOG_CONFIG_HI_DOWN_EVENT_ENABLE,
                             //DataType.DEVICE_ANALOG_CONFIG_HIHI_DOWN_EVENT_ENABLE,
                           }
                       );
                    #endregion

                    Dictionary<OpLocation, List<OpLocationEquipment>> locationsDict = opLocationEquipments.GroupBy(g => g.Location).ToDictionary(d => d.Key, d => d.ToList());
                    foreach (KeyValuePair<OpLocation, List<OpLocationEquipment>> kvp in locationsDict)
                    {
                        try
                        {
                            #region Check CID location
                            int cidNumber = 0;
                            int.TryParse(kvp.Key.CID, out cidNumber);

                            if (cidNumber < 100 || cidNumber > 1000)
                                continue;
                            #endregion
                            #region Get Main Device
                            OpLocationEquipment deviceEquipment = kvp.Value.FirstOrDefault(w => w.SerialNbr.HasValue);
                            if (deviceEquipment == null)
                                continue;
                            #endregion

                            #region Get GSM and Battery Levels
                            DW.OpData opGSMLevel = latestDataForLocations.Where(w => w.IdLocation == kvp.Key.IdLocation && w.SerialNbr == deviceEquipment.SerialNbr && w.IdDataType == DataType.GSM_QUALITY).OrderByDescending(o => o.Time).FirstOrDefault();
                            DW.OpData opBatteryLevel = latestDataForLocations.Where(w => w.IdLocation == kvp.Key.IdLocation && w.SerialNbr == deviceEquipment.SerialNbr && w.IdDataType == DataType.BATTERY_PERCENTAGE_USAGE).OrderByDescending(o => o.Time).FirstOrDefault();
                            #endregion
                            #region Get Inlet Meter and LatestData
                            OpLocationEquipment inletMeter = kvp.Value.FirstOrDefault(w => w.Meter != null && !string.IsNullOrWhiteSpace(w.Meter.TankName) && w.Meter.TankName.ToUpper().Contains("INGRESSO"));
                            DW.OpData opInletPreasure = null;//, opInletLowPreassure = null, opInletLowLowPreassure = null, opInletHighPreassure = null, opInletHighHighPreassure = null;
                            OpData opInletLowPreasureValue = null, opInletLowLowPreasureValue = null, opInletHiPreasureValue = null, opInletHiHiPreasureValue = null;
                            OpData opInletLowEnabled = null, opInletLowLowEnabled = null, opInletHiEnabled = null, opInletHiHiEnabled = null;
                            if (inletMeter != null)
                            {
                                opInletPreasure = latestDataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.AVG_PRESSURE).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opInletLowPreassure = latestDataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_LO_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opInletLowLowPreassure = latestDataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_LOLO_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opInletHighPreassure = latestDataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_HI_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opInletHighHighPreassure = latestDataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_HIHI_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                opInletLowEnabled = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_LO_UP_EVENT_ENABLE).FirstOrDefault();
                                opInletLowLowEnabled = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_LOLO_UP_EVENT_ENABLE).FirstOrDefault();
                                opInletHiEnabled = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_HI_UP_EVENT_ENABLE).FirstOrDefault();
                                opInletHiHiEnabled = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_HIHI_UP_EVENT_ENABLE).FirstOrDefault();

                                opInletLowPreasureValue = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_LO_PRESSURE_VALUE).FirstOrDefault();
                                opInletLowLowPreasureValue = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_LOLO_PRESSURE_VALUE).FirstOrDefault();
                                opInletHiPreasureValue = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_HI_PRESSURE_VALUE).FirstOrDefault();
                                opInletHiHiPreasureValue = dataForMeters.Where(w => w.IdMeter == inletMeter.IdMeter && w.IdDataType == DataType.METER_HIHI_PRESSURE_VALUE).FirstOrDefault();
                            }
                            #endregion
                            #region Get OutletMeter and LatestData
                            OpLocationEquipment outlet1Meter = kvp.Value.FirstOrDefault(w => w.Meter != null && !string.IsNullOrWhiteSpace(w.Meter.TankName) && w.Meter.TankName.ToUpper().Contains("USCITA"));
                            DW.OpData opOutlet1Preasure = null;//, opOutlet1LowPreassure = null, opOutlet1LowLowPreassure = null, opOutlet1HighPreassure = null, opOutlet1HighHighPreassure = null;
                            OpData opOutlet1LowEnabled = null, opOutlet1LowLowEnabled = null, opOutlet1HiEnabled = null, opOutlet1HiHiEnabled = null;
                            OpData opOutlet1LowPreasureValue = null, opOutlet1LowLowPreasureValue = null, opOutlet1HiPreasureValue = null, opOutlet1HiHiPreasureValue = null;
                            if (outlet1Meter != null)
                            {
                                opOutlet1Preasure = latestDataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.AVG_PRESSURE).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opOutlet1LowPreassure = latestDataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_LO_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opOutlet1LowLowPreassure = latestDataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_LOLO_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opOutlet1HighPreassure = latestDataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_HI_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                //opOutlet1HighHighPreassure = latestDataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.ANALOG_VALUE_HIHI_UP_EVENT).OrderByDescending(o => o.Time).FirstOrDefault();
                                opOutlet1LowEnabled = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_LO_UP_EVENT_ENABLE).FirstOrDefault();
                                opOutlet1LowLowEnabled = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_LOLO_UP_EVENT_ENABLE).FirstOrDefault();
                                opOutlet1HiEnabled = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_HI_UP_EVENT_ENABLE).FirstOrDefault();
                                opOutlet1HiHiEnabled = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_HIHI_UP_EVENT_ENABLE).FirstOrDefault();

                                opOutlet1LowPreasureValue = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_LO_PRESSURE_VALUE).FirstOrDefault();
                                opOutlet1LowLowPreasureValue = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_LOLO_PRESSURE_VALUE).FirstOrDefault();
                                opOutlet1HiPreasureValue = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_HI_PRESSURE_VALUE).FirstOrDefault();
                                opOutlet1HiHiPreasureValue = dataForMeters.Where(w => w.IdMeter == outlet1Meter.IdMeter && w.IdDataType == DataType.METER_HIHI_PRESSURE_VALUE).FirstOrDefault();
                            }
                            #endregion

                            #region Fill Modbus HoldingRegisters
                            int modbusIndexRegisterCount = 18;
                            int modbusIndexRegisterStart = (cidNumber - 101) * modbusIndexRegisterCount;
                            #region HR[1,2] = MainDevice SerialNumber (hex)
                            ushort[] modbusSerialNbr = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes(Convert.ToUInt64(deviceEquipment.SerialNbr.Value.ToString(), 16)));
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 1] = modbusSerialNbr[0];
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 2] = modbusSerialNbr[1];
                            #endregion
                            #region HR[3,4] = Inlet Preassure [float]
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 3] = 0;
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 4] = 0;
                            if (opInletPreasure != null)
                            {
                                float fInletPreassure = opInletPreasure.GetValue<float>();
                                ushort[] modbusInletPreassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes((float)fInletPreassure));
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 3] = modbusInletPreassure[0];
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 4] = modbusInletPreassure[1];
                            }
                            #endregion
                            #region HR[5,6] = Outlet1 Preassure [float]
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 5] = 0;
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 6] = 0;
                            if (opOutlet1Preasure != null)
                            {
                                float fOutlet1Preassure = opOutlet1Preasure.GetValue<float>();
                                ushort[] modbusOutlet1Preassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes(fOutlet1Preassure));
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 5] = modbusOutlet1Preassure[0];
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 6] = modbusOutlet1Preassure[1];
                            }
                            #endregion
                            #region HR[7,8] = Outlet2 Preassure [float]
                            float fOutlet2Preassure = 0;
                            ushort[] modbusOutlet2Preassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes(fOutlet2Preassure));
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 7] = modbusOutlet2Preassure[0];
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 8] = modbusOutlet2Preassure[1];
                            #endregion
                            #region  HR[9,10] = Outlet3 Preassure [float]
                            float fOutlet3Preassure = 0;
                            ushort[] modbusOutlet3Preassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes(fOutlet3Preassure));
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 9] = modbusOutlet3Preassure[0];
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 10] = modbusOutlet3Preassure[1];
                            #endregion
                            #region HR[11] = GSM Level [ushort]
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 11] = 0;
                            if (opGSMLevel != null)
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 11] = opGSMLevel.GetValue<ushort>();
                            #endregion
                            #region HR[12] = Battery Level [ushort]
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 12] = 0;
                            if (opBatteryLevel != null)
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 12] = opBatteryLevel.GetValue<ushort>();
                            #endregion
                            #region HR[13-16] - InletPreassure TimeStamp (13-Month, 14-Day, 15-Hour, 16-Minute)
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 13] = 0;
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 14] = 0;
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 15] = 0;
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 16] = 0;
                            if (opInletPreasure != null)
                            {
                                DateTime timeStamp = opInletPreasure.Time;
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 13] = (ushort)timeStamp.Month;
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 14] = (ushort)timeStamp.Day;
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 15] = (ushort)timeStamp.Hour;
                                slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 16] = (ushort)timeStamp.Minute;
                            }
                            #endregion

                            #region HR[17] - Inlet/Outlet Alarms [ushort bits]
                            ushort uAlarms1 = 0;
                            if (opInletLowEnabled != null && opInletLowEnabled.GetValue<bool>())
                                if (opInletLowPreasureValue != null && opInletPreasure.GetValue<float>() < opInletLowPreasureValue.GetValue<float>() )
                                    uAlarms1 |= 0x0001;
                            if (opInletLowLowEnabled != null && opInletLowLowEnabled.GetValue<bool>())
                                if (opInletLowLowPreasureValue != null && opInletPreasure.GetValue<float>() < opInletLowLowPreasureValue.GetValue<float>())
                                    uAlarms1 |= 0x0002;
                            if (opInletHiEnabled != null && opInletHiEnabled.GetValue<bool>())
                                if (opInletHiPreasureValue != null && opInletPreasure.GetValue<float>() > opInletHiPreasureValue.GetValue<float>())
                                    uAlarms1 |= 0x0004;
                            if (opInletHiHiEnabled != null && opInletHiHiEnabled.GetValue<bool>())
                                if (opInletHiHiPreasureValue != null && opInletPreasure.GetValue<float>() > opInletHiHiPreasureValue.GetValue<float>())
                                    uAlarms1 |= 0x0008;
                            if (opOutlet1LowEnabled != null && opOutlet1LowEnabled.GetValue<bool>())
                                if (opOutlet1LowPreasureValue != null && opOutlet1Preasure.GetValue<float>() < opOutlet1LowPreasureValue.GetValue<float>())
                                    uAlarms1 |= 0x0010;
                            if (opOutlet1LowLowEnabled != null && opOutlet1LowLowEnabled.GetValue<bool>())
                                if (opOutlet1LowLowPreasureValue != null && opOutlet1Preasure.GetValue<float>() < opOutlet1LowLowPreasureValue.GetValue<float>())
                                    uAlarms1 |= 0x0020;
                            if (opOutlet1HiEnabled != null && opOutlet1HiEnabled.GetValue<bool>())
                                if (opOutlet1HiPreasureValue != null && opOutlet1Preasure.GetValue<float>() > opOutlet1HiPreasureValue.GetValue<float>())
                                    uAlarms1 |= 0x0040;
                            if (opOutlet1HiHiEnabled != null && opOutlet1HiHiEnabled.GetValue<bool>())
                                if (opOutlet1HiHiPreasureValue != null && opOutlet1Preasure.GetValue<float>() > opOutlet1HiHiPreasureValue.GetValue<float>())
                                    uAlarms1 |= 0x0080;
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 17] = uAlarms1;
                            #endregion
                            #region HR[18] - Inlet/Outlet digitals [ushort bits]
                            ushort uAlarms2 = 0;
                            slave.DataStore.HoldingRegisters[modbusIndexRegisterStart + 18] = uAlarms2;
                            #endregion
                            #endregion
                        }
                        catch(Exception ex)
                        {
                            Log(EventID.WorkerTaskProc.FillHoldingRegistersException, kvp.Key.IdLocation, ex);
                        }
                    }

                    #region Tests
                    /*
                    SN[] serials = new SN[] { SN.FromDBValue(52436090), SN.FromDBValue(52436095) };

                    //AVG_PRESSURE - Preasure Inlet/outlet
                    //GSM_QUALITY
                    //BATTERY_PERCENTAGE_USAGE
                    //ANALOG_VALUE_LO_UP_EVENT - LowPreasureInletOutlet
                    //ANALOG_VALUE_LOLO_UP_EVENT - LowLowinletOutlet
                    //ANALOG_VALUE_HI_UP_EVENT - HighInletOutLet
                    //ANALOG_VALUE_HIHI_UP_EVENT - HighHighInletOutlet

                    int frameLen = 18;
                    for (int i = 0; i < serials.Length; i++)
                    {

                        ushort[] modbusSerialNbr = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes(serials[i].Value.Value));
                        slave.DataStore.HoldingRegisters[i * frameLen + 1] = modbusSerialNbr[0];
                        slave.DataStore.HoldingRegisters[i * frameLen + 2] = modbusSerialNbr[1];

                        float fInletPreassure = (float)rnd.NextDouble();
                        ushort[] modbusInletPreassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes((float)fInletPreassure));
                        slave.DataStore.HoldingRegisters[i * frameLen + 3] = modbusInletPreassure[0];
                        slave.DataStore.HoldingRegisters[i * frameLen + 4] = modbusInletPreassure[1];

                        float fOut1Preassure = (float)rnd.NextDouble();
                        ushort[] modbusOut1Preassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes((float)fOut1Preassure));
                        slave.DataStore.HoldingRegisters[i * frameLen + 5] = modbusOut1Preassure[0];
                        slave.DataStore.HoldingRegisters[i * frameLen + 6] = modbusOut1Preassure[1];

                        float fOut2Preassure = (float)rnd.NextDouble();
                        ushort[] modbusOut2Preassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes((float)fOut2Preassure));
                        slave.DataStore.HoldingRegisters[i * frameLen + 7] = modbusOut2Preassure[0];
                        slave.DataStore.HoldingRegisters[i * frameLen + 8] = modbusOut2Preassure[1];

                        float fOut3Preassure = (float)rnd.NextDouble();
                        ushort[] modbusOut3Preassure = ModbusUtility.NetworkBytesToHostUInt16(BitConverter.GetBytes((float)fOut3Preassure));
                        slave.DataStore.HoldingRegisters[i * frameLen + 9] = modbusOut3Preassure[0];
                        slave.DataStore.HoldingRegisters[i * frameLen + 10] = modbusOut3Preassure[1];

                        ushort uGSMLevel = (ushort)rnd.Next();
                        slave.DataStore.HoldingRegisters[i * frameLen + 11] = uGSMLevel;

                        ushort uBatteryLevel = (ushort)rnd.Next();
                        slave.DataStore.HoldingRegisters[i * frameLen + 12] = uBatteryLevel;

                        DateTime timeStamp = DateTime.Now;
                        slave.DataStore.HoldingRegisters[i * frameLen + 13] = (ushort)timeStamp.Month;
                        slave.DataStore.HoldingRegisters[i * frameLen + 14] = (ushort)timeStamp.Day;
                        slave.DataStore.HoldingRegisters[i * frameLen + 15] = (ushort)timeStamp.Hour;
                        slave.DataStore.HoldingRegisters[i * frameLen + 16] = (ushort)timeStamp.Minute;

                        ushort uAlarms1 = (ushort)rnd.Next(); // BITOWE
                        slave.DataStore.HoldingRegisters[i * frameLen + 17] = uAlarms1;

                        ushort uAlarms2 = (ushort)rnd.Next(); // BITOWE
                        slave.DataStore.HoldingRegisters[i * frameLen + 18] = uAlarms2;
                    }
                    */
                    #endregion
                }
                catch (ThreadAbortException) // To wystepuje jesli dll'ka byla zaladowana do innego AppDomain i wykonane App.UnLoad
                {
                }
                catch (Exception ex)
                {
                    Log(EventID.WorkerTaskProc.Exception, ex);
                }

                LastWorkedTime = DateTime.UtcNow;
                if (WaitForEvent(Events, TimeSpan.FromMinutes(DataArchRefreshTime)))
                    break;  // wychodzimy jeśli nastąpiło zatrzymanie taska
            }

            if (StopEvent.WaitOne(0, false))
                Log(EventID.WorkerTaskProc.StopEventSignaled);

            Log(EventID.WorkerTaskProc.TaskEnd);
        }

        #region WaitForEvent
        private bool WaitForEvent(WaitHandle[] Events, TimeSpan sleepTime)
        {
            int WaitResult = WaitHandle.WaitAny(Events, sleepTime, false);
            #region switch (WaitResult)
            switch (WaitResult)
            {
                case 0: //StopEvent
                    return true;
            }
            #endregion
            return false;
        }
        #endregion
        #endregion

        #region static Log
        public static void Log(LogData EventData, params object[] Parameters)
        {
            Logger.Add(Enums.Module.AGsmScada, EventData, Parameters);
        }
        #endregion
    }
}
