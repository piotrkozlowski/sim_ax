﻿using IMR.Suite.Common;
using System;

namespace IMR.Suite.Services.AGsmScada
{
    public partial class AGsmScadaSrvc : dotService
    {
        #region Members
        AGsmScada agsmScada { get; set; }
        #endregion

        #region Constructor
        public AGsmScadaSrvc()
        {
            m_Srvc = this;

            m_sServiceName = "AGsmScada";
            m_sDisplayName = "IMR.Suite.Services.AGsmScada";
            m_sDescription = "AGsmScada Service";
            m_sDependedOn = new string[] { "MSSQLServer", "MSMQ" };
        }
        public AGsmScadaSrvc(string[] dependedOn) : this()
        {
            m_sDependedOn = dependedOn;
        }
        #endregion

        #region OnStart
        protected override void OnStart(string[] args)
        {
            try
            {
                AGsmScada.Log(EventID.AGsmScada.ModuleUp, AssemblyWrapper.NameAndVersion, Environment.MachineName);
                agsmScada = new AGsmScada();
                agsmScada.Start();
            }
            catch(Exception ex)
            {
                AGsmScada.Log(EventID.AGsmScada.ModuleUpError, ex.Message);
                if (agsmScada != null)
                    agsmScada.Stop();
                Stop(1);
            }
        }
        #endregion
        #region OnStop
        protected override void OnStop()
        {
            AGsmScada.Log(EventID.AGsmScada.ModuleDown, AssemblyWrapper.NameAndVersion, Environment.MachineName);
            if (agsmScada != null)
                agsmScada.Stop();
        }
        #endregion
        #region OnShutdown
        protected override void OnShutdown()
        {
            OnStop();
        }
        #endregion

        #region static MainHandlerException
        static void MainHandlerException(object sender, UnhandledExceptionEventArgs args)
        {
            string Message = "MainHandlerExcpetion: ";

            Exception e = (Exception)args.ExceptionObject;

            Message += "\nMessage: ";
            Message += e.Message;
            if (e.InnerException != null)
            {
                Message += "\nInnerMessage: ";
                Message += e.InnerException.Message;
            }
            Message += "\n:StackSource ";
            Message += e.StackTrace;
            Console.WriteLine(Message);
            System.Diagnostics.EventLog.WriteEntry(
                 "ViewsManager", Message, System.Diagnostics.EventLogEntryType.Error);

            System.Environment.Exit(-1);
        }
        #endregion

        #region Entry Function - Main - do not touch
        [MTAThread]
        static void Main(string[] args)
        {
            IMR.Suite.Common.CmdLineArgs cmdLine = new IMR.Suite.Common.CmdLineArgs(args);

            // Instalacja: 
            // 1. IMR.Suite.Services.AGsmScada.exe -i -ServiceNoDep    -->Dla serwisu na zdalnej maszynie bez żadnych Dependencies
            // 2. IMR.Suite.Services.AGsmScada.exe -i -ServiceNoSqlDep -->Dla serwisu na zdalnej maszynie bez Dependencies do SQL
            // 3. IMR.Suite.Services.AGsmScada.exe -i                  -->Dla serwisu gdzie SQL i MSMQ jest zainstalowane

            if (cmdLine["ServiceNoSqlDep"] != null)
                new AGsmScadaSrvc(new string[] { "MSMQ" }).RegisterService(args);
            else if (cmdLine["ServiceNoDep"] == null)
                new AGsmScadaSrvc(new string[] { "MSSQLServer", "MSMQ" }).RegisterService(args);
            else
                new AGsmScadaSrvc(null).RegisterService(args);
        }
        #endregion Entry Function - Main - do not touch
    }
    #region AGsmScadaSrvcInstaller - potrzebne dla 'zewnętrznych' instalatorów jak np. InstallUtil
    [System.ComponentModel.RunInstallerAttribute(true)]
    public class AGsmScadaSrvcInstaller : dotService.dotServiceInstaller
    {
        public AGsmScadaSrvcInstaller() { this.m_Srvc = new AGsmScadaSrvc(); }
    }
    #endregion
}
