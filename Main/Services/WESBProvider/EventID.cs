﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using IMR.Suite.Common;

namespace IMR.Suite.Services.WEsbProvider
{
    public class EventID
    {
        #region WEsbProviderFP 1000 - 1199
        public class WEsbProviderFP
        {
            public static readonly LogData ClientRequest = new LogData(6000, LogLevel.Info, "Function: {0}, Request resource: {1}");
            public static readonly LogData ClientResponse = new LogData(6001, LogLevel.Info, "Function: {0}, Status code: {1}, Response length: {2}, Uri: {3}");
            public static readonly LogData ClientResponseError = new LogData(6002, LogLevel.Info, "Function: {0}, Status code: {1}, Response length: {2}, Uri: {3}, Response: {4}");
            public static readonly LogData ClientError = new LogData(6003, LogLevel.Error, "Function: {0}, Message: {1}, Exception: {2}");
        }
        #endregion

        #region WEsbProviderFP 1200 - 1399
        public class WEsbProviderDWH
        {
            public static readonly LogData ClientRequest = new LogData(6000, LogLevel.Info, "Function: {0}, Request resource: {1}");
            public static readonly LogData ClientResponse = new LogData(6001, LogLevel.Info, "Function: {0}, Status code: {1}, Response length: {2}, Uri: {3}");
            public static readonly LogData ClientResponseError = new LogData(6002, LogLevel.Info, "Function: {0}, Status code: {1}, Response length: {2}, Uri: {3}, Response: {4}");
            public static readonly LogData ClientError = new LogData(6003, LogLevel.Error, "Function: {0}, Message: {1}, Exception: {2}");
        }
        #endregion

        #region DataArch 1400 - 1404
        public class DataArch
        {
            public static readonly LogData ConversionFieldMissingError = new LogData(1400, LogLevel.Error, "Field missing: {0}");
            public static readonly LogData ConversionArgumentError = new LogData(1401, LogLevel.Error, "Argument {0} passed to conversion is null. StackTrace: {1}");
        }
        #endregion

        #region DataTemporal 1405 - 1409
        public class DataTemporal
        {
            public static readonly LogData Debug = new LogData(1405, LogLevel.Debug, "{0}");
            public static readonly LogData Error = new LogData(1406, LogLevel.Error, "Exception: {0}");
            public static readonly LogData ConversionFieldMissingError = new LogData(1407, LogLevel.Error, "Field missing: {0}");
            public static readonly LogData ConversionArgumentError = new LogData(1408, LogLevel.Error, "Argument {0} passed to conversion is null. StackTrace: {1}");
        }
        #endregion
    }
}