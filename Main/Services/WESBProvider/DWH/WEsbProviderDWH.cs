﻿using IMR.Suite.Common;
using Newtonsoft.Json;
using RestSharp;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IMR.Suite.Services.WEsbProvider.DWH
{
    public partial class WEsbProviderDWH
    {
        #region Members
        private readonly string BaseURL;
        private readonly string Username;
        private readonly string Password;
        #endregion

        #region ctor
        public WEsbProviderDWH(string baseAddress)
        {
            BaseURL = baseAddress;
        }
        public WEsbProviderDWH(string baseAddress, string username, string passwd)
        {
            BaseURL = baseAddress;
            Username = username;
            Password = passwd;
        }
        public WEsbProviderDWH(string baseAddress, string username, string passwd, bool decrypt = false)
        {
            BaseURL = baseAddress;
            if (decrypt)
            {
                Username = RSA.Decrypt(username);
                Password = RSA.Decrypt(passwd);
            }
            else
            {
                Username = username;
                Password = passwd;
            }
        }
        #endregion

        #region CreateRestClient
        private RestClient ConectRestClient()
        {
            RestClient client = new RestClient(BaseURL);

            if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
                client.Authenticator = new RestSharp.Authenticators.HttpBasicAuthenticator(Username, Password);

            return client;
        }
        #endregion

        #region PrepareRequest
        private RestRequest PrepareRequest(Method method, string rootElement, string resource, string methodNameToLog, int commandTimeout = 0)
        {
            var request = new RestRequest(method);
            request.Resource = string.Format("{0}/{1}", rootElement, resource);
            request.RootElement = rootElement;
            if (commandTimeout > 0)
            {
                request.ReadWriteTimeout = request.Timeout = commandTimeout * 1000;
            }

            Logger.Add(EventID.WEsbProviderFP.ClientRequest, methodNameToLog, request.Resource);
            return request;
        }
        #endregion

        #region ExecuteRequest
        private T ExecuteRequest<T>(RestRequest request, string logMessageName)
        {
            Task dataFillTask = null;
            try
            {
                var client = ConectRestClient();

                IRestResponse response = null;
                dataFillTask = new Task(() =>
                {
                    try
                    {
                        response = client.Execute(request);
                    }
                    catch (ThreadInterruptedException) { }
                });
                dataFillTask.Start();
                dataFillTask.Wait();

                var content = response.Content;

                Logger.Add(EventID.WEsbProviderFP.ClientResponse, logMessageName, response.StatusCode, response.ContentLength, response.ResponseUri);
                if (response.ErrorException != null)
                    throw new Exception(string.Format("REST response error. {0}", response.ErrorException.Message));

                var result = JsonConvert.DeserializeObject<T>(content);

                return result;
            }
            catch (ThreadInterruptedException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, logMessageName, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
            catch (ThreadAbortException ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, logMessageName, ex.Message, ex.StackTrace);
                if (dataFillTask != null)
                    dataFillTask.Wait(1000);
                throw;
            }
        }
        #endregion

        #region AddCollectionToParameters
        private void AddCollectionToParameters<T>(ref RestRequest request, IEnumerable<T> collection, string paramName)
        {
            if (request == null) throw new ArgumentNullException("Referenced request is null");
            if (collection != null)
            {
                foreach (var item in collection)
                    request.AddParameter(paramName, item);
            }
        }
        #endregion
        #region AddDateTimeParameter
        private void AddDateTimeParameter(ref RestRequest request, DateTime? dt, string paramName)
        {
            if (request == null) throw new ArgumentNullException("Referenced request is null");
            if (dt.HasValue)
            {
                long ms = DateTimer.GetMsUtcFromDtUtc(dt.Value);
                request.AddParameter(paramName, ms);
            }
        }
        #endregion
        #region AddParameter
        private void AddParameter<T>(ref RestRequest request, T param, string paramName)
        {
            if (request == null) throw new ArgumentNullException("Referenced request is null");
            if (param != null)
            {
                if (param is DateTime || param is DateTime?)
                    AddDateTimeParameter(ref request, param as DateTime?, paramName);
                else
                    request.AddParameter(paramName, param);
            }
        }
        #endregion
    }
}
