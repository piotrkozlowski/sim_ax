﻿using IMR.Suite.Common;
using RestSharp;
using System;
using System.Collections.Generic;

namespace IMR.Suite.Services.WEsbProvider.DWH
{
    public partial class WEsbProviderDWH
    {
        public Objects.ActiveMQStatistics GetActiveMQStatisticsSevenDays()
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            return GetActiveMQStatistics<Objects.ActiveMQStatistics>("queueStat7d", methodName);
        }

        public Objects.ActiveMQStatistics GetActiveMQStatisticsOneMonth()
        {
            string methodName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            return GetActiveMQStatistics<Objects.ActiveMQStatistics>("queueStat1m", methodName);
        }

        private T GetActiveMQStatistics<T>(string type, string methodName)
        {
            try
            {
                RestRequest request = PrepareRequest(Method.GET, "dataarch", "getStat", methodName);
                AddParameter(ref request, type, "stat");
                return ExecuteRequest<T>(request, methodName);
            }
            catch (Exception ex)
            {
                Logger.Add(EventID.WEsbProviderFP.ClientError, methodName, ex.Message, ex.StackTrace);
                return default(T);
            }
        }
    }
}
