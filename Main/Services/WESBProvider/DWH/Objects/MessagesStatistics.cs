﻿using Newtonsoft.Json;

using System;
using System.Collections.Generic;

namespace IMR.Suite.Services.WEsbProvider.DWH.Objects
{
    /// <summary>
    /// Income message statistic of data table.
    /// </summary>
    public class MessageStatistic
    {
        [JsonProperty("time")]
        public long? Time { get; set; }

        [JsonProperty("id_location")]
        public int? IdLocation { get; set; }

        [JsonProperty("mesg_count")]
        public int? MessagesCount { get; set; }

        #region Custom properties
        [JsonIgnore]
        public DateTime? TimeUtc { get { return Time.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(Time * 1000) : null; } }

        [JsonIgnore]
        public DateTime? TimeLocal { get { return Time.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(Time * 1000) : null; } }
        #endregion

        public override string ToString()
        {
            return string.Format("IdLocation: {0} - Time: {1} - Count: {2}", IdLocation, TimeUtc, MessagesCount);
        }
    }

    /// <summary>
    /// Income messages statistics for data table.
    /// </summary>
    public class DataTableStatistics
    {
        [JsonProperty("type")]
        public string DataTableType { get; set; }

        [JsonProperty("stat")]
        public IList<MessageStatistic> Statistics { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - Count: {1}", DataTableType, Statistics.Count);
        }
    }

    /// <summary>
    /// Message statistics from one day for all defined data tables.
    /// </summary>
    public class MessagesStatisticsOneDay
    {
        [JsonProperty("mesg_1d")]
        public IList<DataTableStatistics> OneDayStatistics { get; set; }

        public override string ToString()
        {
            return string.Format("OneDayStatistics - Count: {0}", OneDayStatistics.Count);
        }
    }

    /// <summary>
    /// Message statistics from four days for all defined data tables.
    /// </summary>
    public class MessagesStatisticsFourDays
    {
        [JsonProperty("mesg_4d")]
        public IList<DataTableStatistics> FourDaysStatistics { get; set; }

        public override string ToString()
        {
            return string.Format("FourDaysStatistics - Count: {0}", FourDaysStatistics.Count);
        }
    }
}
