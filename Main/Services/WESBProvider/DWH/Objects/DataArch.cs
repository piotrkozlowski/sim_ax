﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;

namespace IMR.Suite.Services.WEsbProvider.DWH.Objects
{
    #region DataArch
    public class DataArch
    {
        #region Properties
        [JsonProperty("id")]
        public long? Id { get; set; }
        [JsonProperty("serialNbr")]
        public long? SerialNbr { get; set; }
        [JsonProperty("idMeter")]
        public long? IdMeter { get; set; }
        [JsonProperty("idLocation")]
        public long? IdLocation { get; set; }
        [JsonProperty("indexNbr")]
        public int? IndexNbr { get; set; }
        [JsonProperty("idDataType")]
        public long? IdDataType { get; set; }
        [JsonProperty("value")]
        public object Value { get; set; }
        [JsonProperty("time")]
        public long? Time { get; set; }
        [JsonProperty("status")]
        public int? Status { get; set; }

        [JsonProperty("idDataSource")]
        public long? IdDataSource { get; set; }
        [JsonProperty("idDataSourceType")]
        public int? IdDataSourceType { get; set; }
        [JsonProperty("idAlarmEvent")]
        public int? IdAlarmEvent { get; set; }
        [JsonProperty("isCompletePortal")]
        public bool? IsCompletePortal { get; set; }

        [JsonProperty("isCompleteDwh")]
        public bool? IsCompleteDWH { get; set; }
        [JsonProperty("valueType")]
        public string ValueType { get; set; }
        [JsonProperty("idMessage")]
        public string IdMessage { get; set; }
        [JsonProperty("idPacket")]
        public long? IdPacket { get; set; }
        [JsonProperty("isAction")]
        public bool? IsAction { get; set; }
        [JsonProperty("isAlarm")]
        public bool IsAlarm { get; set; }
        #endregion

        #region Custom Properties
        [JsonIgnore]
        public DateTime? TimeUTC { get { return Time.HasValue ? Common.DateTimer.GetDtUtcFromMsUtc(Time) : null; } }
        [JsonIgnore]
        public DateTime? TimeLocal { get { return Time.HasValue ? Common.DateTimer.GetDtLocalFromMsUtc(Time) : null; } }
        #endregion

        #region Methods

        #region ToOpDataArch
        /// <summary>
        /// Convert DWH DataArch business object to OpDataArch business object.
        /// </summary>
        /// <param name="dataArch">DataArch business object.</param>
        /// <exception cref="ArgumentNullException">When Id or IdDataType or Time is null.</exception>
        /// <returns>OpDataArch business object.</returns>
        public static OpDataArch ToOpDataArch(DataArch dataArch)
        {
            if (dataArch == null)
                throw new ArgumentNullException("dataArch");

            if (!dataArch.Id.HasValue)
                throw new MissingFieldException("DataArch", "Id");
            else if (!dataArch.IdDataType.HasValue)
                throw new MissingFieldException("DataArch", "IdDataType");
            else if (!dataArch.Time.HasValue)
                throw new MissingFieldException("DataArch", "Time");
            
            return new OpDataArch(new Data.DB.DW.DB_DATA_ARCH()
            {
                ID_ALARM_EVENT = dataArch.IdAlarmEvent,
                ID_DATA_ARCH = dataArch.Id.Value,
                ID_DATA_SOURCE = dataArch.IdDataSource,
                ID_DATA_SOURCE_TYPE = dataArch.IdDataSourceType,
                ID_DATA_TYPE = dataArch.IdDataType.Value,
                ID_LOCATION = dataArch.IdLocation,
                ID_METER = dataArch.IdMeter,
                //ID_SERVER = data_arch.
                INDEX_NBR = dataArch.IndexNbr.HasValue ? dataArch.IndexNbr.Value : 0,
                SERIAL_NBR = dataArch.SerialNbr,
                STATUS = dataArch.Status.HasValue ? dataArch.Status.Value : (int)Enums.DataStatus.Unknown,
                TIME = dataArch.TimeUTC.Value,
                VALUE = dataArch.Value
            });
        }
        #endregion

        #region ToListOpDataArch
        /// <summary>
        /// Converts list of DWH.DataArch objects to DW.OpDataArch objects list.
        /// </summary>
        /// <param name="dataArchList">List of DWH.DataArch objects.</param>
        /// <exception cref="ArgumentNullException">Throwed when input list is null.</exception>
        /// <returns>List of DW.OpDataArch object list.</returns>
        public static List<OpDataArch> ToListOpDataArch(IEnumerable<DataArch> dataArchList)
        {
            if (dataArchList == null)
                throw new ArgumentNullException("dataArchList");

            var list = new List<OpDataArch>();
            foreach (var dataArch in dataArchList)
            {
                try
                {
                    list.Add(ToOpDataArch(dataArch));
                }
                catch (MissingFieldException ex)
                {
                    Logger.Add(EventID.DataArch.ConversionFieldMissingError, ex.Message);
                }
                catch (ArgumentNullException ex)
                {
                    Logger.Add(EventID.DataArch.ConversionArgumentError, ex.ParamName, ex.StackTrace);
                }
            }
            return list;
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is DataArch)
                return this.Id == ((DataArch)obj).Id;
            else
                return base.Equals(obj);
        }
        #endregion

        #region operator ==
        public static bool operator ==(DataArch left, DataArch right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        #endregion

        #region operator !=
        public static bool operator !=(DataArch left, DataArch right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }
        #endregion

        #region GetHashCode
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        #endregion

        #endregion
    }
    #endregion
}
