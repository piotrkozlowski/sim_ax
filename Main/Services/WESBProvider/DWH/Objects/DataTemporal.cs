﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using IMR.Suite.UI.Business.Objects.DW;
using IMR.Suite.Common;

namespace IMR.Suite.Services.WEsbProvider.DWH.Objects
{
    public class DataTemporal
    {
        #region Properties
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("serialNbr")]
        public long? SerialNbr { get; set; }

        [JsonProperty("idMeter")]
        public long? IdMeter { get; set; }

        [JsonProperty("idLocation")]
        public long? IdLocation { get; set; }

        [JsonProperty("indexNbr")]
        public int? IndexNbr { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("status")]
        public int? Status { get; set; }

        [JsonProperty("idDataSource")]
        public long? IdDataSource { get; set; }

        [JsonProperty("idDataSourceType")]
        public int? IdDataSourceType { get; set; }

        [JsonProperty("idAlarmEvent")]
        public long? IdAlarmEvent { get; set; }
        
        [JsonProperty("idDataType")]
        public long? IdDataType { get; set; }
                
        [JsonProperty("endTime")]
        public long? EndTime { get; set; }
        
        [JsonProperty("startTime")]
        public long? StartTime { get; set; }

        [JsonProperty("idAggregationType")]
        public int? IdAggregationType { get; set; }
        
        [JsonProperty("valueType")]
        public string ValueTypeString { get; set; }

        [JsonProperty("idMessage")]
        public string IdMessage { get; set; }

        [JsonProperty("isCompletePortal")]
        public bool? IsCompletePortal { get; set; }

        [JsonProperty("isCompleteDwh")]
        public bool? IsCompleteDwh { get; set; }
        #endregion 

        #region Custom Properties
        [JsonIgnore]
        public DateTime? StartTimeUTC { get { return StartTime.HasValue ? DateTimer.GetDtUtcFromMsUtc(StartTime) : null; } }
        [JsonIgnore]
        public DateTime? StartTimeLocal { get { return StartTime.HasValue ? DateTimer.GetDtLocalFromMsUtc(StartTime) : null; } }

        [JsonIgnore]
        public DateTime? EndTimeUTC { get { return EndTime.HasValue ? DateTimer.GetDtUtcFromMsUtc(EndTime) : null; } }
        [JsonIgnore]
        public DateTime? EndTimeLocal { get { return EndTime.HasValue ? DateTimer.GetDtLocalFromMsUtc(EndTime) : null; } }

        [JsonIgnore]
        public Type ValueType {
            get
            {
                if (!string.IsNullOrEmpty(ValueTypeString)) { return Type.GetType(ValueTypeString); }
                else return null;
            }
            set
            {
                if (value != null) ValueTypeString = value.FullName;
            }
        }
        #endregion
        
        #region ToOpDataTemporal
        public static OpDataTemporal ToOpDataTemporal(DataTemporal dataTemporal)
        {
            if (dataTemporal == null)
                throw new ArgumentNullException("dataTemporal");

            if (dataTemporal.IdDataType.HasValue == false)
                throw new MissingFieldException("DataTemporal", "IdDataType");

            return new OpDataTemporal(new Data.DB.DB_DATA_TEMPORAL()
            {
                END_TIME = dataTemporal.EndTimeUTC,
                ID_AGGREGATION_TYPE = dataTemporal.IdAggregationType,
                ID_ALARM_EVENT = dataTemporal.IdAlarmEvent,
                ID_DATA_SOURCE = dataTemporal.IdDataSource,
                ID_DATA_SOURCE_TYPE = dataTemporal.IdDataSourceType,
                ID_DATA_TEMPORAL = dataTemporal.Id,
                ID_DATA_TYPE = dataTemporal.IdDataType.Value,
                ID_LOCATION = dataTemporal.IdLocation,
                ID_METER = dataTemporal.IdMeter,
                INDEX_NBR = dataTemporal.IndexNbr.HasValue ? dataTemporal.IndexNbr.Value : 0,
                SERIAL_NBR = dataTemporal.SerialNbr,
                START_TIME = dataTemporal.StartTimeUTC,
                STATUS = dataTemporal.Status.HasValue ? dataTemporal.Status.Value : (int)Enums.DataStatus.Unknown,
                VALUE = dataTemporal.Value
            });
        }
        #endregion

        #region ToListOpDataTemporal
        public static List<OpDataTemporal> ToListOpDataTemporal(List<DataTemporal> dataTemporalList)
        {
            List<OpDataTemporal> list = new List<OpDataTemporal>();
            if (dataTemporalList == null || dataTemporalList.Count == 0) return list;
            foreach (var dataTemporal in dataTemporalList)
            {
                try
                {
                    list.Add(ToOpDataTemporal(dataTemporal));
                }
                catch (MissingFieldException ex)
                {
                    Logger.Add(EventID.DataTemporal.ConversionFieldMissingError, ex.Message);
                }
                catch (ArgumentNullException ex)
                {
                    Logger.Add(EventID.DataTemporal.ConversionArgumentError, ex.ParamName, ex.StackTrace);
                }
                catch (Exception ex)
                {
                    Logger.Add(EventID.DataTemporal.Error, ex);
                }
            }
            return list;
        }
        #endregion

        #region override Equals
        public override bool Equals(object obj)
        {
            if (obj is DataTemporal)
                return this.Id == ((DataTemporal)obj).Id;
            else
                return base.Equals(obj);
        }
        #endregion

        #region operator ==
        public static bool operator ==(DataTemporal left, DataTemporal right)
        {
            if ((object)left == null)
                return ((object)right == null);
            return left.Equals(right);
        }
        #endregion

        #region operator !=
        public static bool operator !=(DataTemporal left, DataTemporal right)
        {
            if ((object)left == null)
                return ((object)right != null);
            return !left.Equals(right);
        }
        #endregion

        #region GetHashCode
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        #endregion
    }
}
